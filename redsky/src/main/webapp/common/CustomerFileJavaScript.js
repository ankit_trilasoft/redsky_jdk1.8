<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>


<script language="JavaScript" type="text/javascript">
document.getElementById("hidImagePlus").style.display="none";
<c:if test="${checkAccessQuotation}">
<c:if test="${empty customerFile.id}">
if(document.forms['customerFileForm'].elements['customerFile.moveType'].value==''){
document.forms['customerFileForm'].elements['moves'].value="BookedMove";
document.forms['customerFileForm'].elements['customerFile.moveType'].value="BookedMove";
document.getElementById('BookedMove').checked=true;
}else{
	document.forms['customerFileForm'].elements['moves'].value='${customerFile.moveType}';
	document.forms['customerFileForm'].elements['customerFile.moveType'].value='${customerFile.moveType}';
	document.getElementById('${customerFile.moveType}').checked=true;	
}
</c:if>
</c:if>
window.onload = function() {
    if('${stateshitFlag}'!= "1"){
        document.forms['customerFileForm'].elements['customerFile.homeCountry'].value='${customerFile.homeCountry}';
       gethomeState(document.forms['customerFileForm'].elements['customerFile.homeCountry']);
     } 
      hcountry='${customerFile.homeCountry}';
       if(hcountry == 'India' || hcountry == 'Canada' || hcountry == 'United States'){
     document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
     document.forms['customerFileForm'].elements['customerFile.homeState'].value='${customerFile.homeState}';
     }else{
     document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
     document.forms['customerFileForm'].elements['customerFile.homeState'].value ="";
      }     
	if(${isNetworkRecord}){
		document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].className = 'input-textUpper';
		document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].className = 'input-textUpper';		
		document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].setAttribute('readonly','readonly');
		document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].setAttribute('readonly','readonly');
		var el = document.getElementById('customerfile.bookingAgentPopUp');
		el.onclick = false;
	    document.images['customerfile.bookingAgentPopUp'].src = 'images/navarrow.gif';
	} 
	if(${isNetworkRecord}){
	document.forms['customerFileForm'].elements['customerFile.originAgentCode'].className = 'input-textUpper';
	document.forms['customerFileForm'].elements['customerFile.originAgentContact'].className = 'input-textUpper';
	document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].className = 'input-textUpper';		
	document.forms['customerFileForm'].elements['customerFile.originAgentCode'].setAttribute('readonly','readonly');
	document.forms['customerFileForm'].elements['customerFile.originAgentContact'].setAttribute('readonly','readonly');
	document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].setAttribute('readonly','readonly');
	var el = document.getElementById('openpopup.img');
	el.onclick = false;
    document.images['openpopup.img'].src = 'images/navarrow.gif';	
 } 
		 
}

function updateBillToName(){
	var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var billToCodeVal = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	//var sequenceNo = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	if(billToCodeVal != '' || billToCodeVal !=null){
		var url="updateBillToNameVal.html?ajax=1&decorator=simple&popup=true&id=${customerFile.id}&sequenceNo=${customerFile.sequenceNumber}&lastNameNew="+encodeURI(lastName) +"&billToCode="+encodeURI(billToCodeVal);
		http511.open("GET", url, true); 
	   	http511.onreadystatechange = handleHttp901; 
	   	http511.send(null);	
	}
}

function findActiveLength(type,fieldValue,result){
	var len=0;
	if(type=='ASML'){
		<c:forEach var="assignment1" items="${assignmentTypesASML_isactive}" varStatus="loopStatus">
       	var entKey = "<c:out value="${assignment1.key}"/>";
      	var entvalue = "<c:out value="${assignment1.value}"/>";
      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
      		len++;
      	}
		</c:forEach>
	}else if(type=='STATE'){
		var res = result.split("@");
		for(i=0;i<res.length;i++){
		var	stateVal = res[i].split("#");
			if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==fieldValue)){
				len++;
			}
		}
	}else{
		<c:forEach var="assignment1" items="${assignmentTypes_isactive}" varStatus="loopStatus">
       	var entKey = "<c:out value="${assignment1.key}"/>";
      	var entvalue = "<c:out value="${assignment1.value}"/>";
      	if(entKey!=null && entKey!=undefined && (entKey.split('~')[1]=='Active' || entKey.split('~')[0]==fieldValue)){
      		len++;
      	}
		</c:forEach>		
	}
	return len;
}
function focusDate(target){
	document.forms['customerFileForm'].elements[target].focus();
}
function EmailPortDate(){
	var EmailPort = document.forms['customerFileForm'].elements['customerFile.email'].value;
	document.forms['customerFileForm'].elements['customerFile.email'].value = EmailPort;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value=EmailPort;
	} }
</script>
<script language="JavaScript" type="text/javascript">
function myDate() {
	var HH2;
	var HH1;
	var MM2;
	var MM1;
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	if((tim1.substring(0,tim1.length-2)).length == 1){
		HH1 = '0'+tim1.substring(0,tim1.length-2);
	}else {
		HH1 = tim1.substring(0,tim1.length-2);
	}
	MM1 = tim1.substring(tim1.length-2,tim1.length);
	if((tim2.substring(0,tim1.length-2)).length == 1){
		HH2 = '0'+tim2.substring(0,tim2.length-2);
	}else {
		HH2 = tim2.substring(0,tim2.length-2);
	}
	MM2 = tim2.substring(tim2.length-2,tim2.length);
	document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = HH1 + ':' + MM1;
	document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = HH2 + ':' + MM2;

	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off"); 
} 
var httpRefJobType = getHTTPObjectRefJobType();
function getHTTPObjectRefJobType(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
} 
function setCountry(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
		var url="getRefJobTypeCountryList.html?ajax=1&decorator=simple&popup=true&job="+encodeURI(job) +"&compDivision="+encodeURI(compDivision);		
        httpRefJobType.open("GET", url, true);
        httpRefJobType.onreadystatechange = httpRefJobTypeCountryName;
        httpRefJobType.send(null);		
	} }
function httpRefJobTypeCountryName(){
             if (httpRefJobType.readyState == 4){
                var results = httpRefJobType.responseText
                results = results.trim();             
                var res = results.replace("[",'');
                res = res.replace("]",'');
                res=res.split("~");                             
               if(res.length>1 && res[0]!=''&&res[1]!=''){
 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];  					 
 					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value=res[1]; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountry'].value=res[1];
 					enableStateListOrigin();zipCode();
 					enableStateListDestin();state();
 					 getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);
 	                 getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
 					if(res[2]=='Y'){
 	 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
 	 					document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';
 	 					}else{
 	 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
 	 					document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
 	 					}
				}else{
					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = '';
					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value=''; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = ''; 					
 					document.forms['customerFileForm'].elements['customerFile.originCountry'].value='';
					 document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
				     document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
				     document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
				     document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
                 }  }    } 
function setStatas(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		
		if(job=='UVL' || job=='MVL' || job=='DOM' || job=='LOC' || job=='OFF' || job=='VAI' || job=='STO' || job=='MLL' || job=='HVY'){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
		 } }	 }
function copyCompanyToDestination(){
if((document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value).trim() ==''){
		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value =  document.forms['customerFileForm'].elements['customerFile.originCompany'].value
		document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value 
 }
}
<configByCorp:fieldVisibility componentId="component.field.State.NotManadotry">
	<c:set var="stateShowOrHide" value="Y" />
    </configByCorp:fieldVisibility>
function setStateReset(){
	var destinationAbc ='${customerFile.destinationCountry}';
	var originAbc ='${customerFile.originCountry}';
	var enbState = '${enbState}';
	var index = (enbState.indexOf(originAbc)> -1);
	var index1 = (enbState.indexOf(destinationAbc)> -1);

	if(index != ''){
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
		getOriginStateReset(originAbc);
			var el = document.getElementById('zipCodeRequiredTrue');
			var el1 = document.getElementById('zipCodeRequiredFalse');
			var st1=document.getElementById('originStateRequiredFalse');
			var st2=document.getElementById('originStateRequiredTrue');
			if(originAbc == 'United States'){
				el.style.display = 'block';		
				el1.style.display = 'none';	
				st2.style.display = 'block';		
				st1.style.display = 'none';
			}else if(originAbc == 'India'){
			    <c:if test="${stateShowOrHide=='Y'}">
				st2.style.display = 'none';		
				st1.style.display = 'block';
				</c:if>
				 <c:if test="${stateShowOrHide!='Y'}">
				 st2.style.display = 'block';		
				st1.style.display = 'none';
				</c:if>
				el.style.display = 'none';
			}else{
				el.style.display = 'none';
				el1.style.display = 'block';
				st1.style.display = 'block';		
				st2.style.display = 'none';	
			
			}
	}else{
		document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
	}

	if(index1 != ''){
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
		getDestinationStateReset(destinationAbc);
		var ds1 = document.getElementById('destinationStateRequiredTrue');
		var ds2 = document.getElementById('destinationStateRequiredFalse');
		if(destinationAbc == 'United States'){
			ds1.style.display = 'block';		
			ds2.style.display = 'none';
		}else if(destinationAbc == 'India'){
		     <c:if test="${stateShowOrHide=='Y'}">
			 ds1.style.display = 'none';		
			 ds2.style.display = 'block';
			 </c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
				ds1.style.display = 'block';		
			    ds2.style.display = 'none';
			</c:if>
		}else{
			ds1.style.display = 'none';
			ds2.style.display = 'block';
		}
	}else{
		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
	}
}
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;
function autoPopulate_customerFile_originCountry(targetElement) {		
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States') 	{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India') 	{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada') {
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India'  ){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
			if(oriCountry == 'United States' ){
			document.forms['customerFileForm'].elements['customerFile.originZip'].focus();
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			if(oriCountry == 'Netherlands'|| oriCountry == 'Austria' ||oriCountry == 'Belgium' ||oriCountry == 'Bulgaria' ||oriCountry == 'Croatia' ||oriCountry == 'Cyprus' ||oriCountry == 'Czech Republic' ||
			oriCountry == 'Denmark' ||oriCountry == 'Estonia' ||oriCountry == 'Finland' ||oriCountry == 'France' ||oriCountry == 'Germany' ||oriCountry == 'Greece' ||oriCountry == 'Hungary' ||
			oriCountry == 'Iceland' ||oriCountry == 'Ireland' ||oriCountry == 'Italy' ||oriCountry == 'Latvia' ||oriCountry == 'Lithuania' ||oriCountry == 'Luxembourg' ||oriCountry == 'Malta' ||
			oriCountry == 'Poland' ||oriCountry == 'Portugal' ||oriCountry == 'Romania' ||oriCountry == 'Slovakia' ||oriCountry == 'Slovenia' ||oriCountry == 'Spain' ||oriCountry == 'Sweden' ||
			oriCountry == 'Turkey' ||oriCountry == 'United Kingdom' ){
				document.forms['customerFileForm'].elements['customerFile.originZip'].focus();
			}
			autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}  }
	function autoPopulate_customerFile_destinationCountry(targetElement) {  
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States') {
			dCountryCode = "USA";
		}
		if(dCountry == 'India') 	{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada') {
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
			if(dCountry == 'United States'){
			document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus();
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
			if(dCountry== 'Netherlands'|| dCountry== 'Austria' ||dCountry== 'Belgium' ||dCountry== 'Bulgaria' ||dCountry== 'Croatia' ||dCountry== 'Cyprus' ||dCountry== 'Czech Republic' ||
			dCountry== 'Denmark' ||dCountry== 'Estonia' ||dCountry== 'Finland' ||dCountry== 'France' ||dCountry== 'Germany' ||dCountry== 'Greece' ||dCountry== 'Hungary' ||
			dCountry== 'Iceland' ||dCountry== 'Ireland' ||dCountry== 'Italy' ||dCountry== 'Latvia' ||dCountry== 'Lithuania' ||dCountry== 'Luxembourg' ||dCountry== 'Malta' ||
			dCountry== 'Poland' ||dCountry== 'Portugal' ||dCountry== 'Romania' ||dCountry== 'Slovakia' ||dCountry== 'Slovenia' ||dCountry== 'Spain' ||dCountry== 'Sweden' ||
			dCountry== 'Turkey' ||dCountry== 'United Kingdom' ){
				document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus();
			}
			autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		} 	}
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			} } }
	function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
			 } else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			} } }
	function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.statusDate'].value=datam;
	}
	function autoPopulate_customerFile_approvedDate(targetElement, w) { 
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.billApprovedDate'].value=datam;
	}	 
   function openOriginLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
        var address = "";
       /* if(country == 'United States'){ Commented out for ticket number: 7022*/
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        /*}  else  {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        } Commented out for ticket number: 7022*/
        address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.originState'].value; 
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 function openDestinationLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
       	var address = "";
       /* if(country == 'United States')
        { Commented out for ticket number: 7022 */
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
       /* } else {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        } Commented out for ticket number: 7022 */
       	address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
    	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 function desableCustomerFileStatus(tar){
			document.forms['customerFileForm'].elements['customerFile.moveType'].value = tar.value;
			//document.forms['customerFileForm'].elements['customerFile.status'].readOnly = true;
			//document.forms['customerFileForm'].elements['customerFile.status'].className = 'input-textUpper';
		}
	 
	 function changeQuoteBookingDate(tar){
			var moveType=document.forms['customerFileForm'].elements['moves'].value;
		    var mydate=new Date();
			var daym;
			var year=mydate.getFullYear()
			var y=""+year;
			if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec";
			
			var daym=mydate.getDate()
			if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4);
			
		    
			<c:if test="${checkAccessQuotation}">
			if(moveType=='Quote'){
			document.forms['customerFileForm'].elements['customerFile.bookingDate'].value="";
			}
			if(moveType=='BookedMove'){
				document.forms['customerFileForm'].elements['customerFile.bookingDate'].value=datam;
				
			}
			</c:if>
	 }
	function generatePortalId() {
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		var emailID = document.forms['customerFileForm'].elements['customerFile.email'].value;
		if(checkPortalBox==true && emailID !=''){
			var emailID2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
			emailID=emailID+','+emailID2;
		}
		var emailTypeFlag=false;
		var emailTypeVOERFlag=false;
		var emailTypeBOURFlag=false;
		var emailTypeINTMFlag=false;
		var emailTypeEnglishFlag=false;
		var germanCheck=false;
		try{
			germanCheck=document.forms['customerFileForm'].elements['checkGermanForCustomerSave'].checked;
		}catch(e){}
		var englishIntmCheck=false;		
		try{
			englishIntmCheck=document.forms['customerFileForm'].elements['checkEnglishForCustomerSave'].checked;
		}catch(e){}
		
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeflag">
			emailTypeFlag = true;
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeVOERflag">
		  emailTypeVOERFlag=true;
	    </configByCorp:fieldVisibility>
	    <configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeBOURflag">
	        emailTypeBOURFlag=true;
	    </configByCorp:fieldVisibility>
	    if(germanCheck){
	    	 emailTypeINTMFlag=true;
	    }
	    if(englishIntmCheck){
	    	emailTypeEnglishFlag=true;
	    }	    
		var portalIdName = document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value;		
		var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;		
		if(checkPortalStatus !='CNCL' && checkPortalStatus !='CLOSED'){
			if(emailID!=''){
				if(portalIdName!=''){
					window.open('sendMail.html?id=${customerFile.id}&emailTo='+emailID+'&userID='+portalIdName+'&emailTypeFlag='+emailTypeFlag+'&emailTypeVOERFlag='+emailTypeVOERFlag+'&emailTypeBOURFlag='+emailTypeBOURFlag+'&emailTypeINTMFlag='+emailTypeINTMFlag+'&emailTypeEnglishFlag='+emailTypeEnglishFlag+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
				} else{
					alert("Please create ID for Customer Portal");
				}
			}else {
				alert("Please enter primary email");
				document.forms['customerFileForm'].elements['customerFile.email'].focus();
			}
		} else{
			alert("You can't Reset password for Customer Portal ID because job is Closed/Cancelled");
			document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		} 
	} 
	function updateOriginSoAdd() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
        javascript:openWindow('changeOriginSoAdd.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;	    
	    }
		} 
	function updateOriginAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateOriginAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 	}
	function updateDestinAddSo() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('updateDestinAddSo.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 	}
	function updateDestinAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateDestinAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		} } 
		function disabledSOBtn() {
			try{
			document.forms['customerFileForm'].originSoUpdate.disabled=true;
			document.forms['customerFileForm'].originSoTicketUpdate.disabled=true;}catch(e){}
		}
	function disabledSODestinBtn() 	{
		try{
			document.forms['customerFileForm'].originSoDestinUpdate.disabled=true;
			document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=true;}catch(e){}
		}
function disabledSOBtnReset() 	{
			document.forms['customerFileForm'].originSoUpdate.disabled=false;
			document.forms['customerFileForm'].originSoTicketUpdate.disabled=false;
		}
function disabledSODestinBtnReset() {
			document.forms['customerFileForm'].originSoDestinUpdate.disabled=false;
			document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=false;
		}
	function checkPortalBoxId() {
	    var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked;
		if(checkPortalBox == true){
		if(checkPortalStatus =='CNCL' || checkPortalStatus =='CLOSED')
		{
		alert("You can't Activate the ID for Customer Portal because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		} } } 
function checkPortalBoxSecondaryId() {
	    var secondaryEmail=document.forms['customerFileForm'].elements['customerFile.email2'].value;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		if(checkPortalBox == true && secondaryEmail==''){
		alert("Please select secondary email in origin section.");
		document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked=false;
		} } 	
function scheduleSurvey()   {
           var jobTypeSchedule =document.forms['customerFileForm'].elements['customerFile.job'].value;
           var addressLine1=document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
           var addressLine2=document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
          	addressLine1= addressLine1.replace('#','');
          	addressLine2= addressLine2.replace('#','');
          	if(addressLine2!=''){
          	addressLine1=addressLine1+', '+addressLine2;}
           var city=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
           var country=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
           var zipCode=document.forms['customerFileForm'].elements['customerFile.originZip'].value;
           var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
           var survey=document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var targetAddress=addressLine1+', '+city+', '+state+', '+zipCode+', '+country;
           var surveyTime="";
	       var surveyTime2="";
	       var estimated="";
	       if(country=='Hong Kong'){
             zipCode=' ';
             targetAddress=addressLine1+', '+city+', '+state+','+country; 
                }	       
           if(jobTypeSchedule=='' || addressLine1=='' || city =='' || country=='' || zipCode=='')
           { 
           alert("Please select job type, address line1, country, city, postal code in origin section.");
           }
           else if(survey !='')
           {
           var agree= confirm("WARNING: Survey already scheduled, do you want to reschedule? Hit OK to reschedule and Cancel to keep current date.");
           if(agree){
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }           
           }  else{
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }   }
        function comptetiveSurveyEmail(val) {
          var comp = document.forms['customerFileForm'].elements['customerFile.comptetive'].value;
		if(comp=="Y"){
		document.forms['customerFileForm'].Button1.disabled=false;
		} else {
		document.forms['customerFileForm'].Button1.disabled=true;
		}    }
function validate_email(targetElement) {
with (targetElement)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {
   return false;
  } else {return true}
} }       
  function sendSurveyEmail(){
    	if(document.forms['customerFileForm'].elements['customerFile.email'].value !=''){
    		var emailSurveyFlag=false;
    		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailflagForSSCW">
    			emailSurveyFlag = true;
    		</configByCorp:fieldVisibility>
  		  if (validate_email(document.forms['customerFileForm'].elements['customerFile.email'])== true) {
           var estimator = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
           var survey = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyDutch = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyFrench = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyGerman = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyBourEnglish = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyDutchTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyGermanTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
           var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
           var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
           var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
           var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
           var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
           if (hour<12){
           		surveyTime=hour+":"+min+"AM";
           } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
               surveyTime=(hour-12)+":"+min+"PM";
           }else{
        	 //Putting if block for #7598(14 Feb 2013)        	   
        	   var emaitime=false;
        	   <configByCorp:fieldVisibility componentId="component.tab.customerFile.surveyTime">        	  
        	   emaitime=true;        	   
        	   </configByCorp:fieldVisibility>
        	   if(emaitime){
        		   if(hour==12){
                  	  hour=0;                    	  
                    }  
        	   }
        	   surveyTime=hour+":"+min+"PM";        	   
           }
            if (hour2<12){
           		surveyTime2=hour2+":"+min2+"AM";
           } else if (hour2>12) {
               surveyTime2=(hour2-12)+":"+min2+"PM";
           }else{
        	 //Putting if block for #7598(14 Feb 2013)
        	   var emaitime1=false;
        	   <configByCorp:fieldVisibility componentId="component.tab.customerFile.surveyTime">        	  
        	   emaitime1=true;
        	   </configByCorp:fieldVisibility>
        	   if(emaitime1){
        	   if(hour2==12){
              	  hour2=0;              	
                }
        	   }
               surveyTime2=hour2+":"+min2+"PM";
           }
           if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
           alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
           } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
    	    var systemDate = new Date();
    	 // For English mail except BOUR
			var mySplitResult = survey.split("-");
		   	var day = mySplitResult[0];
		   	var month = mySplitResult[1];
		   	var month13 = mySplitResult[1];
		   	var year = mySplitResult[2];
		 	if(month == 'Jan'){ month = "01";
		   	}else if(month == 'Feb'){ month = "02";
		   	}else if(month == 'Mar'){ month = "03"
		   	}else if(month == 'Apr'){ month = "04"
		   	}else if(month == 'May'){ month = "05"
		   	}else if(month == 'Jun'){ month = "06"
		   	}else if(month == 'Jul'){ month = "07"
		   	}else if(month == 'Aug'){ month = "08"
		   	}else if(month == 'Sep'){ month = "09"
		   	}else if(month == 'Oct'){ month = "10"
		   	}else if(month == 'Nov'){ month = "11"
		   	}else if(month == 'Dec'){ month = "12";
		   	}
		   	var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			var finalDate = month+"/"+day+"/"+year;
			var finalDate11 = month13+"/"+day+"/"+year;
			//alert("initial:"+finalDate11)
		   	survey = finalDate.split("/");
		   	var enterDate = new Date(survey[0]+"/"+survey[1]+"/20"+survey[2]);
		  	var sendDate = weekday[enterDate.getDay()]+" "+finalDate;
			var sentMonthDate= weekday[enterDate.getDay()]+" "+finalDate11;
			//alert("final format:"+sentMonthDate)
			// For Dutch mail
		  	var mySplitDutchResult = surveyDutch.split("-");
		   	var dayDutch = mySplitDutchResult[0];
		   	var monthDutch = mySplitDutchResult[1];
		   	var yearDutch = mySplitDutchResult[2];
		  	if(monthDutch == 'Jan'){ monthDutch = "Januari";
			}else if(monthDutch == 'Feb'){ monthDutch = "Februari";
			}else if(monthDutch == 'Mar'){ monthDutch = "Maart"
			}else if(monthDutch == 'Apr'){ monthDutch = "April"
			}else if(monthDutch == 'May'){ monthDutch = "Mei"
			}else if(monthDutch == 'Jun'){ monthDutch = "Juni"
			}else if(monthDutch == 'Jul'){ monthDutch = "Juli"
			}else if(monthDutch == 'Aug'){ monthDutch = "Augustus"
			}else if(monthDutch == 'Sep'){ monthDutch = "September"
			}else if(monthDutch == 'Oct'){ monthDutch = "Oktober"
			}else if(monthDutch == 'Nov'){ monthDutch = "November"
			}else if(monthDutch == 'Dec'){ monthDutch = "December";
			}
		  	var weekDutchday=new Array(7);
		  	weekDutchday[0]="zondag";
		  	weekDutchday[1]="maandag";
		  	weekDutchday[2]="dinsdag";
		  	weekDutchday[3]="woensdag";
		  	weekDutchday[4]="donderdag";
		  	weekDutchday[5]="vrijdag";
		  	weekDutchday[6]="zaterdag";
		  	surveyDutch=surveyDutch.replace("-"," ");
		  	surveyDutch=surveyDutch.replace("-"," ");		  	
		  	var finalDate1 = dayDutch+"/"+monthDutch+"/20"+yearDutch; 
		   	var surveyDutchDate = finalDate1.replace("/"," ");
		   	surveyDutchDate = surveyDutchDate.replace("/"," "); 
		  	var sendDutchDate = weekDutchday[enterDate.getDay()]+" "+surveyDutchDate;  
		  	
		  	// For french mail
		  	
		  	var mySplitFrenchResult = surveyFrench.split("-");
		   	var dayFrench = mySplitFrenchResult[0];
		   	var monthFrench = mySplitFrenchResult[1];
		   	var yearFrench = mySplitFrenchResult[2];
		  	if(monthFrench == 'Jan'){ monthFrench = "janvier";
			}else if(monthFrench == 'Feb'){ monthFrench = "f�vrier";
			}else if(monthFrench == 'Mar'){ monthFrench = "mars"
			}else if(monthFrench == 'Apr'){ monthFrench = "avril"
			}else if(monthFrench == 'May'){ monthFrench = "mai"
			}else if(monthFrench == 'Jun'){ monthFrench = "juin"
			}else if(monthFrench == 'Jul'){ monthFrench = "juillet"
			}else if(monthFrench == 'Aug'){ monthFrench = "ao�t"
			}else if(monthFrench == 'Sep'){ monthFrench = "septembre"
			}else if(monthFrench == 'Oct'){ monthFrench = "octobre"
			}else if(monthFrench == 'Nov'){ monthFrench = "novembre"
			}else if(monthFrench == 'Dec'){ monthFrench = "d�cembre";
			}
		  	var weekFrenchday=new Array(7);
		  	weekFrenchday[0]="dimanche";
		  	weekFrenchday[1]="lundi";
		  	weekFrenchday[2]="mardi";
		  	weekFrenchday[3]="mecredi";
		  	weekFrenchday[4]="jeudi";
		  	weekFrenchday[5]="vendredi";
		  	weekFrenchday[6]="samedi";
		  	surveyFrench=surveyFrench.replace("-"," ");
		  	surveyFrench=surveyFrench.replace("-"," ");		  	
		  	var finalDateFrench = dayFrench+"/"+monthFrench+"/20"+yearFrench; 
		   	var surveyFrenchDate = finalDateFrench.replace("/"," ");
		   	surveyFrenchDate = surveyFrenchDate.replace("/"," "); 
		  	var sendFrenchDate = weekFrenchday[enterDate.getDay()]+" "+surveyFrenchDate;  
		  	
			// For German mail
		  	var mySplitGermanResult = surveyGerman.split("-");
		   	var dayGerman = mySplitGermanResult[0];
		   	var monthGerman = mySplitGermanResult[1];
		   	var yearGerman = mySplitGermanResult[2];
		  	if(monthGerman == 'Jan'){ monthGerman = "Januar";
			}else if(monthGerman == 'Feb'){ monthGerman = "Februar";
			}else if(monthGerman == 'Mar'){ monthGerman = "M�rz"
			}else if(monthGerman == 'Apr'){ monthGerman = "April"
			}else if(monthGerman == 'May'){ monthGerman = "Mai"
			}else if(monthGerman == 'Jun'){ monthGerman = "Juni"
			}else if(monthGerman == 'Jul'){ monthGerman = "Juli"
			}else if(monthGerman == 'Aug'){ monthGerman = "August"
			}else if(monthGerman == 'Sep'){ monthGerman = "September"
			}else if(monthGerman == 'Oct'){ monthGerman = "Oktober"
			}else if(monthGerman == 'Nov'){ monthGerman = "November"
			}else if(monthGerman == 'Dec'){ monthGerman = "Dezember";
			}
		  	var weekGermanday=new Array(7);
		  	weekGermanday[0]="Montag";
		  	weekGermanday[1]="Dienstag";
		  	weekGermanday[2]="Mittwoch";
		  	weekGermanday[3]="Donnerstag";
		  	weekGermanday[4]="Freitag";
		  	weekGermanday[5]="Samstag";
		  	weekGermanday[6]="Sonntag";
		  	surveyGerman=surveyGerman.replace("-"," ");
		  	surveyGerman=surveyGerman.replace("-"," ");		  	
		  	var finalDate1 = dayGerman+"."+month+".20"+yearGerman; 
		  	var sendGermanDate = weekGermanday[enterDate.getDay()]+", "+finalDate1+"/ "+surveyGermanTime+" Uhr";  
		  	
         // For BOUR English mail 
		  	var mySplitResult11 = surveyBourEnglish.split("-");
		   	var day11 = mySplitResult11[0];
		   	var month11 = mySplitResult11[1];
		   	var year11 = mySplitResult11[2];
		 	if(month11 == 'Jan'){  month11 = "January";
		   	}else if(month11 == 'Feb'){ month11 = "February";
		   	}else if(month11 == 'Mar'){ month11 = "March"
		   	}else if(month11 == 'Apr'){ month11 = "April"
		   	}else if(month11 == 'May'){ month11 = "May"
		   	}else if(month11 == 'Jun'){ month11 = "June"
		   	}else if(month11 == 'Jul'){ month11 = "July"
		   	}else if(month11 == 'Aug'){ month11 = "August"
		   	}else if(month11 == 'Sep'){ month11 = "September"
		   	}else if(month11 == 'Oct'){ month11 = "October"
		   	}else if(month11 == 'Nov'){ month11 = "November"
		   	}else if(month11 == 'Dec'){ month11 = "December";
		   	}
		   	var dayth=day11;
		   	var weekday11=new Array(7);
			weekday11[0]="Sunday";
			weekday11[1]="Monday";
			weekday11[2]="Tuesday";
			weekday11[3]="Wednesday";
			weekday11[4]="Thursday";
			weekday11[5]="Friday";
			weekday11[6]="Saturday";
			if(day11==11||day11==12||day11==13){
				day11=day11+"th";
			}else{
				var rem=day11%10;
			switch(rem){
			case 1:
				day11=day11+"st";
			    break;
			case 2:
				day11=day11+"nd";
				break;
			case 3:
				day11=day11+"rd";
				break;
             default :
            	 day11=day11+"th";			
			}
			}
			var finalDate111 = month11+" "+day11+" 20"+year11;
			var finalDate11 = month11+"/"+dayth+"/"+year11;
			surveyBourEnglish = finalDate11.split("/");
		   	var enterDate11 = new Date(surveyBourEnglish[0]+"/"+surveyBourEnglish[1]+"/20"+surveyBourEnglish[2]);
		  	var sendDate11 = weekday11[enterDate11.getDay()]+", "+finalDate111;
		  	
		  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
		  	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
		  	if(daysApart < 0){
		    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
		    	return false;
		  	}
		  	if(survey != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			if (hour < 0  || hour > 23) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					return false;
				}
				if (min<0 || min > 59) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					return false;
				}
				if(systemDate.getHours() > hour){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
			}  }   }
		    try{
		    var checkBourEnglish = document.forms['customerFileForm'].elements['checkBourEnglish'].checked;  
		    }catch(e){} 
		    var checkGerman=false;
		    try{
	            checkGerman = document.forms['customerFileForm'].elements['checkGerman'].checked;
			    }catch(e){} 
		    try{
		  	var checkEnglish = document.forms['customerFileForm'].elements['checkEnglish'].checked;
            var checkDutch = document.forms['customerFileForm'].elements['checkDutch'].checked;
		    }catch(e){} 
            var agree= confirm("Press OK to Send Survey Email, else press Cancel.");	  	   
            if(agree){
            var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
            var email2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
			var firstName = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
			var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
			var surveyDate = document.forms['customerFileForm'].elements['customerFile.survey'].value; 
			var updatedBy = document.forms['customerFileForm'].elements['customerFile.updatedBy'].value;
			var subject = "Confirmation for "+firstName+"'s survey on "+surveyDate;
			if(checkEnglish==true || checkDutch==true || checkGerman==true){
				if(email !='' && email2 !='' ){
					 email = email+","+email2;
				}else if(email2 !=''){
					 email =email2;
				}else{
					email =email;
				}
				window.open('sendSurveyEmail.html?id=${customerFile.id}&emailTo='+email+'&estimator='+estimator+'&surveyDates='+sendDate+'&surveyMonths='+sentMonthDate+'&firstName='+firstName+'&lastName='+lastName+'&surveyTime='+surveyTime+'&surveyDutchTime='+surveyDutchTime+'&sendDutchDate='+sendDutchDate+'&surveyGermanTime='+surveyGermanTime+'&sendGermanDate='+sendGermanDate+'&surveyTime2='+surveyTime2+'&emailSurveyFlag='+emailSurveyFlag+'&checkEnglish='+checkEnglish+'&checkDutch='+checkDutch+'&checkGerman='+checkGerman+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
			}else if(checkBourEnglish == true){ 
				 email = email+";"+email2;
				 var prflang = document.forms['customerFileForm'].elements['customerFile.customerLanguagePreference'].value;
				 if(prflang=='fr'){
					 sendDate11=sendFrenchDate;
			        }if(prflang=='nl'){
			        	sendDate11 =sendDutchDate;
			        }
				var url="sendSurveyClientEmail.html?ajax=1&decorator=simple&popup=true&id=${customerFile.id}&estimator="+estimator+'&checkBourEnglish='+checkBourEnglish+'&surveyDates='+sendDate11+'&firstName='+firstName+'&lastName='+lastName+'&surveyTime='+surveyTime+'&surveyTime2='+surveyTime2;
				http221.open("GET", url, true);
		        http221.onreadystatechange = function(){handleHttpResponseSentMail(email,sendDate11,firstName, lastName);};
		        http221.send(null);	
			}else{
				alert("Please select Language!");
				}
			  } else {             
	  	    }	  	   
            } else {
            alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
            }            
            } else {
             alert("Invalid Origin's Primary Email Id");
            } 
            } else {
            alert("Primary Email is not yet entered - Cannot send Survey Email to the customer");
            }   
        }
  function handleHttpResponseSentMail(emailTo,surveyDates,firstName, lastName){
	  var checkBourEnglish = document.forms['customerFileForm'].elements['checkBourEnglish'].checked; 
	  var prflang = document.forms['customerFileForm'].elements['customerFile.customerLanguagePreference'].value;
      if (http221.readyState == 4){
    	var textMessage ="";
	    var results= http221.responseText
	    results = results.trim();     
	    if(results!=''){ textMessage = results;}else{textMessage = '';}	      
      	if(checkBourEnglish==true){
			var msgText1=textMessage;
	        var subject = "Confirmation for survey on "+surveyDates;
	        if(prflang=='fr'){
	        	subject="Confimation visite technique pour le "+surveyDates;
	        }if(prflang=='nl'){
	        	subject ="Bevestiging bezoek op "+surveyDates;
	        }
			var mailto_link = 'mailto:'+encodeURIComponent(emailTo)+'?subject='+encodeURIComponent(subject)+'&body='+encodeURIComponent(msgText1);		
			win = window.open(mailto_link,'emailWindow'); 
			if (win && win.open &&!win.closed) win.close(); 
		}		
     }
  }
  function getHTTPObject221(){
	  var xmlhttp;
	    if(window.XMLHttpRequest){
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject){
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp){
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
  }
  var http221 = getHTTPObject221();
	function completeTimeString() {	
		stime1 = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
		stime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
		stime3="";
		stime4="";
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2;
			} } 
	
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1']!=undefined){
		stime3 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
		}
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2']!=undefined){
		stime4 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
		}
		
		if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
			if(stime3.length==1 || stime3.length==2){
				if(stime3.length==2){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = stime3 + ":00";
				}
				if(stime3.length==1){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = "0" + stime3 + ":00";
				}
			}else{
				if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1']!=undefined){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = stime3 + "00";
				}
			}
		}
		
		else{
			if(stime3.indexOf(":") == -1 && stime3.length==3){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
			}
			if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
			}
			if(stime3.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value = "0" + stime3;
			}
		}
		if(stime4.substring(stime4.indexOf(":")+1,stime4.length) == "" || stime4.length==1 || stime4.length==2){
			if(stime4.length==1 || stime4.length==2){
				if(stime4.length==2){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = stime4 + ":00";
				}
				if(stime4.length==1){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = "0" + stime4 + ":00";
				}
			}else{
				if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2']!=undefined){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = stime4 + "00";
				}
			}
		}
		
		else{
			if(stime4.indexOf(":") == -1 && stime4.length==3){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = "0" + stime4.substring(0,1) + ":" + stime4.substring(1,stime4.length);
			}
			if(stime4.indexOf(":") == -1 && (stime4.length==4 || stime4.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = stime4.substring(0,2) + ":" + stime4.substring(2,4);
			}
			if(stime4.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value = "0" + stime4;
			}
		}
		
		stime5="";
		stime6="";
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3']!=undefined){
			stime5 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;	
		}
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4']!=undefined){
		stime6= document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
		}
		
		if(stime5.substring(stime5.indexOf(":")+1,stime5.length) == "" || stime5.length==1 || stime5.length==2){
			if(stime5.length==1 || stime5.length==2){
				if(stime5.length==2){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = stime5 + ":00";
				}
				if(stime5.length==1){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = "0" + stime5 + ":00";
				}
			}else{
				if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3']!=undefined){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = stime5 + "00";
				}
			}
		}
		
		else{
			if(stime5.indexOf(":") == -1 && stime5.length==3){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = "0" + stime5.substring(0,1) + ":" + stime5.substring(1,stime5.length);
			}
			if(stime5.indexOf(":") == -1 && (stime5.length==4 || stime5.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = stime5.substring(0,2) + ":" + stime5.substring(2,4);
			}
			if(stime5.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value = "0" + stime5;
			}
		}
		if(stime6.substring(stime6.indexOf(":")+1,stime6.length) == "" || stime6.length==1 || stime6.length==2){
			if(stime6.length==1 || stime6.length==2){
				if(stime6.length==2){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = stime6 + ":00";
				}
				if(stime6.length==1){
					document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = "0" + stime6 + ":00";
				}
			}else{
				if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4']!=undefined){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = stime6 + "00";
				}
			}
		}
		
		else{
			if(stime6.indexOf(":") == -1 && stime6.length==3){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = "0" + stime6.substring(0,1) + ":" + stime6.substring(1,stime6.length);
			}
			if(stime6.indexOf(":") == -1 && (stime6.length==4 || stime6.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = stime6.substring(0,2) + ":" + stime6.substring(2,4);
			}
			if(stime6.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value = "0" + stime6;
			}
		}
		
	}
function IsValidTime(clickType) {
	var OACompanyCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
	var DACompanyCode=	document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value; 
	<c:if test="${hasOADADataSecuritySet}">  
	var  partnerSetList= '<c:out value="${partnerCodeOADADataSecuritySet}"/>'; 
	if(OACompanyCode==''){
		OACompanyCode='No'
	}
	if(DACompanyCode==''){
		DACompanyCode='No'
	} 
	if(!((partnerSetList.indexOf(OACompanyCode)> -1) || (partnerSetList.indexOf(DACompanyCode)> -1))){ 
		alert("Please fill 'Employer Name' as "+partnerSetList+" in origin or destination address details"); 
		return false; 
	} 
	</c:if>
	<c:if test="${checkContractChargesMandatory=='1'}">
	var contractStr = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	if(contractStr==''){
		alert("Contract is a required field");
		return false;
	}
	</c:if>
	<c:if test="${checkContractChargesMandatory=='2'}">
	var contractStr = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	if(contractStr==''){
		var agree= confirm("Contract has not been selected. would you like to select the contract Ok/Cancel.");
		 if(agree){
			 document.getElementById('contract').focus();
			 return false;
		 }
	}
	</c:if>
	<configByCorp:fieldVisibility componentId="component.contract.CustomerFile.alert">
	var contractStr = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	</configByCorp:fieldVisibility>
	var newPage= document.forms['customerFileForm'].elements['newPage'].value;
	 if(newPage=='N')
			{
	     var checkrelo=submitRelo();
			}	
		else 
			{
	    var checkrelo=submitReloNew();
			} 
	
	
	if(!checkrelo){
		return false;
	}
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var timeStr = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null){
		alert("Time is not in a valid format. Please use HH:MM format");
		document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
		return false;
	}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];
if (second=="") { second = null; }
if (ampm=="") { ampm = null }
if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
// **************Check for Survey Time2*************************
var time2Str = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}

//**************Check for Preffered Time1*************************
<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">        	  


var prefTime1Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
var prefMatchTime1Array = prefTime1Str.match(timePat);
if (prefMatchTime1Array == null) {
alert("Time is not in a valid format. please Use HH:MM format1");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
hourTime2 = prefMatchTime1Array[1];
minuteTime2 = prefMatchTime1Array[2];
secondTime2 = prefMatchTime1Array[4];
ampmTime2 = prefMatchTime1Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Preffered Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].focus();
return false;
}

//**************Check for Preffered Time2*************************
var prefTime2Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
var prefTime3Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
var prefTime4Str = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
var prefMatchTime2Array = prefTime2Str.match(timePat);
var prefMatchTime3Array = prefTime3Str.match(timePat);
var prefMatchTime4Array = prefTime4Str.match(timePat);
if ((prefMatchTime2Array == null) ||(prefMatchTime3Array == null) || (prefMatchTime4Array == null))  {
alert("Time is not in a valid format. please Use HH:MM format1");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
(prefMatchTime2Array == null)
return false;
}
hourTime2 = prefMatchTime2Array[1];
minuteTime2 = prefMatchTime2Array[2];
secondTime2 = prefMatchTime2Array[4];
ampmTime2 = prefMatchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Preffered Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].focus();
return false;
}
//**************Check for Preffered Time3*************************
hourTime2 = prefMatchTime3Array[1];
minuteTime2 = prefMatchTime3Array[2];
secondTime2 = prefMatchTime3Array[4];
ampmTime2 = prefMatchTime3Array[6];
if (hourTime2 < 0  || hourTime2 > 23) {
	alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	if (minuteTime2<0 || minuteTime2 > 59) {
	alert ("'Preffered Survey' time must between 0 to 59 (Min)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
	alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
	document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].focus();
	return false;
	}
	//**************Check for Preffered Time4*************************
	hourTime2 = prefMatchTime4Array[1];
	minuteTime2 = prefMatchTime4Array[2];
	secondTime2 = prefMatchTime4Array[4];
	ampmTime2 = prefMatchTime4Array[6];
	if (hourTime2 < 0  || hourTime2 > 23) {
		alert("'Preffered Survey' time must between 0 to 23:59 (Hrs)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		if (minuteTime2<0 || minuteTime2 > 59) {
		alert ("'Preffered Survey' time must between 0 to 59 (Min)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
		alert ("'Preffered Survey' time must between 0 to 59 (Sec)");
		document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].focus();
		return false;
		}
		</configByCorp:fieldVisibility>
if(!(clickType == 'save')){
	progressBarAutoSave('1');
		if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${customerFile.id}"/>';
	      		var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
	      		var shipNumber = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	      		var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	           if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
					noSaveAction = 'customerServiceOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
					noSaveAction = 'customerRateOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
					noSaveAction = 'surveysList.html?id1='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
					noSaveAction = 'showAccountPolicy.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
					openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',900,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
					openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
					noSaveAction = 'editImfEntitlement.html?cid='+id1;
					}
	             
	          processAutoSave(document.forms['customerFileForm'], 'saveCustomerFile!saveOnTabChange.html', noSaveAction);	          
		}else{
		var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
		var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		var shipNumber = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
		if (document.forms['customerFileForm'].elements['formStatus'].value == '1'){
			var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='customerFileDetail.heading'/>");
			if(agree){
				document.forms['customerFileForm'].action = 'saveCustomerFile!saveOnTabChange.html';
				document.forms['customerFileForm'].submit();
			}else{
				if(id1 != ''){
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
					location.href = 'customerServiceOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
					location.href = 'customerRateOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
					location.href = 'surveysList.html?id1='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
					location.href = 'showAccountPolicy.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
					openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',900,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
					openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
					location.href = 'editImfEntitlement.html?cid='+id1;
					} } }
		}else{
		if(id1 != ''){
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
					location.href = 'customerServiceOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
					location.href = 'customerRateOrders.html?id='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
					location.href = 'surveysList.html?id1='+id1;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
					location.href = 'showAccountPolicy.html?id='+id1+'&jobNumber=${customerFile.sequenceNumber}&code='+billCode;
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
					openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',740,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
					openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
					}
				if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
					location.href = 'editImfEntitlement.html?cid='+id1;
					}				
		}
		}
	}
	}
	}
function notExists(){
	alert("The customer information has not been saved yet, please save customer information to continue");
}
function openOrderInitiationPopWindow(){
	window.openWindow('openOrderInitiationBillToCode.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
}

function findOrderInitiationBillToCode(){
	var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	bCode=bCode.trim();
	if(bCode!='') {
	    var url="findOrderInitiationBillToCode.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
	    http5551.open("GET", url, true); 
	    http5551.onreadystatechange = findOrderInitiationBillToCodeHttp; 
	    http5551.send(null);	     
     }else{
    	 document.forms['customerFileForm'].elements['customerFile.billToName'].value ="";
     }
 }
function findOrderInitiationBillToCodeHttp(){
    if (http5551.readyState == 4){    	
    	var results = http5551.responseText
    	results = results.trim();
    	results=results.replace("[","");
        results=results.replace("]","");
    	if(results.length >1){
		 	document.forms['customerFileForm'].elements['customerFile.billToName'].value = results;
		 	showOrHide(0);
		 	checkAccountCodeForAssignmentType();
		 	fillAccountCodeByBillToCode();
		 	fillBillToAuthority(); 
    	}else{
    	    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    		alert("You are not authorize to select bill to code "+billToCode+ ".")
    		document.forms['customerFileForm'].elements['customerFile.billToCode'].value="${customerFile.billToCode}";
    		document.forms['customerFileForm'].elements['customerFile.billToName'].value="${customerFile.billToName}";
    		showOrHide(0);
    	}
       }
	}

function openPopWindow(target){
	var targetName=target.name;
	if(targetName == 'P'){
		if(document.forms['customerFileForm'].elements['destinationcopyaddress'].checked){
			var destinationCountryVal = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
			if(destinationCountryVal == null || destinationCountryVal == '' || destinationCountryVal == undefined){
				alert('Please Select Destination Country.');
				document.forms['customerFileForm'].elements['customerFile.destinationCountry'].focus();
				return false;
			}
		}
		else{
			var originCountryVal = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
			if(originCountryVal == null || originCountryVal == '' || originCountryVal == undefined){
				alert('Please Select Origin Country.');
				document.forms['customerFileForm'].elements['customerFile.originCountry'].focus();
				return false;
			}
		}
	}
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
	var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
	var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
	var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
	var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
	var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
	var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
	var compDiv = '';
	<c:if  test="${companies == 'Yes'}">
	compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	</c:if>
	//var companyDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
	var checkAddress=document.forms['customerFileForm'].elements['destinationcopyaddress'].checked;
	var str="0";
	var socialSecurityNumVal = "";
	var orgAddress = "";
	var destAddress = "";
	<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
   		str="1";
   		socialSecurityNumVal = document.forms['customerFileForm'].elements['customerFile.socialSecurityNumber'].value;
   	</configByCorp:fieldVisibility>
   	
   	if((str=="1") && (socialSecurityNumVal!="" && socialSecurityNumVal!=undefined)){
   		orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status+'~'+socialSecurityNumVal;
   	}else{
   		orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
   	}
	var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
	var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
	var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
	var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
	var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
	var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
	if((str=="1") && (socialSecurityNumVal!="" && socialSecurityNumVal!=undefined)){
		destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status+'~'+socialSecurityNumVal;	
	}else{
		destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
	}
	if(targetName=='P' && checkAddress==true){
		createPrivateParty(destAddress);
	}else if(targetName=='P' && checkAddress==false){
		createPrivateParty(orgAddress);
	}else{
		javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&onlyAC=y&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
	}
}
function openOriginalCompanyPopUp(){
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
	var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
	var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
	var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
	var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
	var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
	var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
	var compDiv = '';
	<c:if  test="${companies == 'Yes'}">
	compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	</c:if>
	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
   	orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
   
	var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
	var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
	var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
	var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
	var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
	var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
	destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
	
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originalCompanyName&fld_code=customerFile.originalCompanyCode');
}
function validateEmail(target){
  	var x = target.value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if(target.value!=null && target.value!=''){
	    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
	    	alert('Invalid Email Address');
		    target.value='';
	        return false;
	    }
    }
}
function sendEmailNew(target){
 	var originEmail = target;
	var subject = 'C/F# ${customerFile.sequenceNumber}';
	subject = subject+" "+document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	subject = subject+" "+ document.forms['customerFileForm'].elements['customerFile.lastName'].value; 
   	var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);				
	//win = window.open(mailto_link,'emailWindow');
	//if (win && win.open &&!win.closed) win.close(); 
   	window.location.href = mailto_link;
}
function createPrivateParty(targetElement) {
	var flagbilltocode = document.forms['customerFileForm'].elements['flagForPrivateBillToCode'].value="true";
	document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value=true;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	if(last==''){
		alert('Last Name is required field.');
	}else{
		showOrHide(1);
		var url = "privatePartyCode.html";
		var parameters = "ajax=1&decorator=simple&popup=true&partnerAdderss="+encodeURIComponent(targetElement);
		http12.open("POST", url, true); 
		http12.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		http12.setRequestHeader("Content-length", parameters .length);
		
		http12.onreadystatechange = handleHttpResponseP;
		http12.send(parameters);
	}
}	
function handleHttpResponseP(){
	if (http12.readyState == 4) {
	      var results = http12.responseText
          results = results.trim();
          results=results.replace("[","");
          results=results.replace("]","");
          var res = results.split("#"); 
          if(res[0] != 'null'){
          document.forms['customerFileForm'].elements['customerFile.billToCode'].value=res[0];
		  document.forms['customerFileForm'].elements['customerFile.billToName'].value=res[1]+' '+res[2];
		  showOrHide(0);
          }
  }
	<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
		fillAccountFlag();
	</configByCorp:fieldVisibility>
} 
function openAccountPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
var companyDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
var checkAddress=document.forms['customerFileForm'].elements['destinationcopyaddress'].checked;
var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
var compDiv = '';

<c:if  test="${companies == 'Yes'}">
compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
</c:if>
<c:if test="${cmmDmmAgent}">
	var networkContractType = document.forms['customerFileForm'].elements['customerFile.contractType'].value;
	var contracts = document.forms['customerFileForm'].elements['customerFile.contract'].value;
	if(networkContractType!=null && networkContractType!='' && networkContractType=='DMM'){
		javascript:openWindow('findNetworkBillToCodeList.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
	}else if(contracts!=null && contracts!=''){
		    var url="findDMMTypeContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
		    dmmTypeContractHTTP1.open("GET", url, true);
		    dmmTypeContractHTTP1.onreadystatechange = function(){handleHttpResponseDmmTypeContract1(first,last,phone,email,compDiv,orgAddress,destAddress);};
		    dmmTypeContractHTTP1.send(null);
	}else{
		javascript:openWindow('partnersPopup.html?partnerType=AC&flag=4&onlyAC=y&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
	}

</c:if>
<c:if test="${!cmmDmmAgent}">
javascript:openWindow('partnersPopup.html?partnerType=AC&flag=4&onlyAC=y&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
</c:if>	
}

function handleHttpResponseDmmTypeContract1(first,last,phone,email,compDiv,orgAddress,destAddress){
    if (dmmTypeContractHTTP1.readyState == 4) {
       var results = dmmTypeContractHTTP1.responseText
       results = results.trim();
       if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		javascript:openWindow('findNetworkBillToCodeList.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
	       	 }else{

	       		javascript:openWindow('partnersPopup.html?partnerType=AC&flag=4&onlyAC=y&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
	       	 }
       }else{
    	   javascript:openWindow('partnersPopup.html?partnerType=AC&flag=4&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
       }
    }
}
function openBookingAgentPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
javascript:openWindow('bookingAgentPopup.html?partnerType=AG&customerVendor=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');
}
function openBookingAgentBookingAgentSetPopWindow(){
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	javascript:openWindow('bookingAgentPopupWithBookingAgentSet.html?partnerType=AG&customerVendor=true&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');
	}
</script>
<SCRIPT LANGUAGE="JavaScript">
function findBillToNameOnload(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2OnLoad;
     http2.send(null);
     } }
function handleHttpResponse2OnLoad(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length >= 2){
	           		if(res[2] == 'Approved'){
	           				if(res[3]== '' || res[3] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[3] == 'null'){
	           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[1];
 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[1];
		           		}else{
		           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
		           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
		           			}else{
		           				alert('Please select company division.');
		           			} 
		           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
						 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
						 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
		           		}	
	           		}else if(res[2] != 'Approved'){
	           			alert("Bill To code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
	           		}
	           		var code=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
                   var name=document.forms['customerFileForm'].elements['customerFile.accountName'].value;
	               if(code=="" || name==""){  
	            fillAccountCodeByBillToCode(); 
              }
	           }else{
				document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
				document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
				document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
			   }   }   }
function findCityStateNotPrimary(targetField, position){
if(targetField=='OZ'){
	 var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
	 var zipCode=document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	 } else if (targetField=='DZ'){
	 var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
	 var zipCode=document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	 }
	 if(zipCode!=''){
	 var url="findCityStateNotPrimary.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode)+"&zipType="+encodeURI(targetField)+"&countryForZipCode="+encodeURI(countryCode);
     ajax_showTooltip(url,position);	
 }	}
function goToUrlZip(id,targetField){
if(targetField=='OZ'){
	document.forms['customerFileForm'].elements['customerFile.originCity'].value = id;
	document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	 } else if (targetField=='DZ'){
	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value = id;
	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	} }
  function findCityState1(targetElement,targetField){
	 var zipCode = targetElement.value;
	 if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipCodeList').style.display = 'none';
     }   } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } }
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }
 function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value; 
      if(targetField=='OZ'){
       var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
        if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{        
       }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
          if( document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value=='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{       
             }      }   }        
  function findCityState(targetElement,targetField){
	 var zipCode = targetElement.value;
	 if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
     document.getElementById('zipCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipCodeList').style.display = 'none';
     }   } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     if (countryCode!='USA'){
     document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (zipCode =='' || zipCode == undefined){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } else if (countryCode=='USA' && (zipCode =='' || zipCode == undefined)){
      document.getElementById('zipDestCodeList').style.display = 'none';
     } }
       var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode=${countryCode}&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
       }                  
 function handleHttpResponseCityState(targetField){
             if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                if(res!='' && res!=undefined){
                	res = res.split("#");
                }
                if(targetField=='OZ'){
	           	if(res!=undefined && res[3]=='P') {
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=res[2];
	           	 if(document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=='') {
	           		 document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	           	 	document.getElementById('zipCodeList').style.display = 'none';
	             }else if(res!=undefined && res[3]!='P') {
	            	document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.destinationState'].value=res[2];
	           	  if (document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	           	} } } else { } }
// End Of Method
function findBillToName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    var accountSearchValidation=document.forms['customerFileForm'].elements['accountSearchValidation'].value; 
    if(billToCode!=''){ 
    	<c:if test="${accountSearchValidation}">
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
    	</c:if>	
    	<c:if test="${!accountSearchValidation}">
    	var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
    	</c:if>	
	     httpBilltoCode.open("GET", url, true);
	     httpBilltoCode.onreadystatechange = function(){ handleHttpResponse2(billToCode);};
	     httpBilltoCode.send(null);
     }
}
function findPaymentMethod(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     	http22.open("GET", url, true);
     	http22.onreadystatechange = handleHttpResponsePM;
     	http22.send(null);
    } }
function findAccToName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
    }
    var accountSearchValidation = document.forms['customerFileForm'].elements['accountSearchValidation'].value;
    if(billToCode!=''){
    	 <c:if test="${accountSearchValidation}">
     var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
     </c:if>
     <c:if test="${!accountSearchValidation}">
     var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
     </c:if>
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponseAcc;
     http2.send(null);
   }  
}
function findPricingBillingPaybaleBILLName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    var url="pricingBillingPaybaleName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode) + "&jobType=" + encodeURI(job) +"&compDivision="+encodeURI(compDivision);
    http8.open("GET", url, true);
    http8.onreadystatechange = handleHttpResponse2001;
    http8.send(null);
}
function copyBillToCode(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    var accountSearchValidation = document.forms['customerFileForm'].elements['accountSearchValidation'].value;
  	 <c:if test="${accountSearchValidation}">
	var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
	</c:if>
	 <c:if test="${!accountSearchValidation}">
		var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&accountSearchValidation="+accountSearchValidation;
		</c:if>
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCopyBill;
    http4.send(null);
}
function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
function getDestinationCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}
function getInsurance(){
	  var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	  if(billToCode!=''){
		var url="InsuranceGetAccount.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
	    http255.open("GET", url, true);
	    http255.onreadystatechange = handleHttpResponseInsurance;
	    http255.send(null);
	  }
	
}
function getInsuranceOnLoad(){
	//alert(${checkInsuranceList});
	if(${checkInsuranceList==true}){
	      //alert("true");
		 document.getElementById('ins1').style.display = 'block';
		 //document.getElementById('ins2').style.display = 'none'; 
		 InsuranceVlaueOnLoad();
		 checkInsuranceOnLoad();
	}
		
}
function checkInsuranceOnLoad(){
	//alert(${chkInsurList});
	if(${chkInsurList==false}){		 
		 document.forms['customerFileForm'].elements['customerFile.noInsurance'].disabled = true;	
	 }else{
		 document.forms['customerFileForm'].elements['customerFile.noInsurance'].disabled = false; 
	 }
}
function InsuranceVlaueOnLoad(){
	var results='${insuranceValueList}';
	 var res = results.replace("[",'');
     res = res.replace("]",'');
	 res = res.split(",");
   var targetElement=document.forms['customerFileForm'].elements['customerFile.noInsurance'];
	     targetElement.length= res.length;
		 for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     			//alert(res[i]);
	     		document.forms['customerFileForm'].elements['customerFile.noInsurance'].options[i].text =res[i].trim();
	     		document.forms['customerFileForm'].elements['customerFile.noInsurance'].options[i].value= res[i].trim();
	     		if (document.getElementById("noInsurance").options[i].value == '${customerFile.noInsurance}'){
					   document.getElementById("noInsurance").options[i].defaultSelected = true;
					}
		         }
    }
}
function handleHttpResponseInsurance(){
	 if (http255.readyState == 4){
		 var results = http255.responseText
         results = results.trim();
		 if(results!='' && (results==true || results=='true')){
			 document.getElementById('ins1').style.display = 'block';
			 //document.getElementById('ins2').style.display = 'none'; 
			 InsuranceVlaue();
			 checkInsurance();
		 }else{
			 document.forms['customerFileForm'].elements['customerFile.noInsurance'].value="";
		 }
	 }
}
function InsuranceVlaue(){
	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var CFID = document.forms['customerFileForm'].elements['customerFile.id'].value;
	var url="InsuranceGetValue.html?ajax=1&decorator=simple&popup=true&id="+CFID+"&partnerCode=" + encodeURI(billToCode);
    http257.open("GET", url, true);
    http257.onreadystatechange = handleHttpResponseInsuranceValue;
    http257.send(null);	
}
function handleHttpResponseInsuranceValue(){
	 var results = http257.responseText
      results = results.trim();
	 var res = results.replace("[",'');
     res = res.replace("]",'');
	 res = res.split(",");
   var targetElement=document.forms['customerFileForm'].elements['customerFile.noInsurance'];
	     targetElement.length= res.length;
		 for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     		document.forms['customerFileForm'].elements['customerFile.noInsurance'].options[i].text =res[i].trim();
	     		document.forms['customerFileForm'].elements['customerFile.noInsurance'].options[i].value= res[i].trim();
	     		if (document.getElementById("noInsurance").options[i].value == '${customerFile.noInsurance}'){
					   document.getElementById("noInsurance").options[i].defaultSelected = true;
					}
		         }
    }
}
function checkInsurance(){
	 var bookingAgent= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	 var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	 var url="insurancebookingAgent.html?ajax=1&decorator=simple&popup=true&billToCode="+billToCode+"&partnerCode="+encodeURI(bookingAgent);
	    http256.open("GET", url, true);
	    http256.onreadystatechange = handleHttpResponsebookingInsurance;
	    http256.send(null);
	
}
function handleHttpResponsebookingInsurance(){
	 if (http256.readyState == 4){
		 var results = http256.responseText
        results = results.trim();
		 if(results == false || results == 'false'){
			  //document.getElementById('noInsurance').readOnly =true; 
			// document.forms['customerFileForm'].elements['customerFile.noInsurance'].className = 'input-textUpper';
			 document.forms['customerFileForm'].elements['customerFile.noInsurance'].disabled = true;	
		 }else{
			 document.forms['customerFileForm'].elements['customerFile.noInsurance'].disabled = false; 
		 }
	 }
}
function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}
 					try{	
 					document.forms['customerFileForm'].elements['customerFile.destinationCountry'].select();
 					}catch(e){}
				}}}
    function findCompanyDivisionByBookAg(){
    var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    var compDivision = '${customerFile.companyDivision}';
    var cid = '${customerFile.id}';
    var moveTypeStringValue = '${customerFile.moveType}';
    var flagFor = '${flagForCopyFamily}';
	var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)+"&compDivision="+encodeURI(compDivision)+"&cid="+encodeURI(cid)+"&moveTypeStringValue="+encodeURI(moveTypeStringValue)+"&flagForCopyFamily="+encodeURI(flagFor);
	http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
}	
function handleHttpResponse4444(){
		    if (http6.readyState == 4){
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@"); 
                var targetElement = document.forms['customerFileForm'].elements['customerFile.companyDivision'];
					targetElement.length = res.length;
 					for(i=0;i<res.length;i++){
 						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].text = res[i];
						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].value = res[i];
					}
					 if(res.length-1 == 1){
							document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[1].selected=true;
							}else if(res.length-1 > 1){ 
								document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[0].selected=true;
							}else{
								
							}
 					getJobList();
                }
		    findCompanyCodeAndJobFromCompanyDivision();
		    <configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
				fillAccountFlag();
			</configByCorp:fieldVisibility>
        }
function findCompanyCodeAndJobFromCompanyDivision(){
	var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findCompanyCodeAndJobFromCompanyDivisionAjax.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)
		httpCompanyDivisionAjax.open("GET", url, true);
    	httpCompanyDivisionAjax.onreadystatechange = handleHttpResponseCompanyDivisionAjax;
    	httpCompanyDivisionAjax.send(null);
}

function handleHttpResponseCompanyDivisionAjax(){
	if (httpCompanyDivisionAjax.readyState == 4){
       var results = httpCompanyDivisionAjax.responseText
       results = results.trim();
       if(results!=''){
    	   var res = results.split('~')
    	   if(res[0]!=null && res[0]!='' && res[0]!='NA'){
    		   document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = res[0];
    	   }
    	   if(res[1]!=null && res[1]!='' && res[1]!='NA'){
    		   document.forms['customerFileForm'].elements['customerFile.job'].value = res[1];
    	   }
       }
	}
}
var httpCompanyDivisionAjax = getHTTPObject();
function findResetCompanyDivisionByBookAg(){
	<c:if  test="${companies == 'Yes'}">
	var bookCode= '${customerFile.bookingAgentCode}';	
    if(bookCode!=''){
    var compDivision = '${customerFile.companyDivision}';
    var cid = '${customerFile.id}';
    var moveTypeStringValue = '${customerFile.moveType}';
    var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)+"&compDivision="+encodeURI(compDivision)+"&cid="+encodeURI(cid)+"&moveTypeStringValue="+encodeURI(moveTypeStringValue);
    http6.open("GET", url, true);
    http6.onreadystatechange = resethandleHttpResponse4444;
    http6.send(null);
    }
    </c:if>
}	
function resethandleHttpResponse4444()
        {
		    if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@"); 
                var targetElement = document.forms['customerFileForm'].elements['customerFile.companyDivision'];
					targetElement.length = res.length;
 					for(i=0;i<res.length;i++){
 						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].text = res[i];
						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].value = res[i];
						if (document.getElementById("companyDivision").options[i].value == '${customerFile.companyDivision}'){
		 					   document.getElementById("companyDivision").options[i].defaultSelected = true;
		 				}
					}
                	getJobList();
                }	
        }
function getContractONLOad() {
	var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
	document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var custCompanyDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    if(custJobType!=''){
	//
    	 targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];			
			//
			var contarctlist='${contracts}';
		    contarctlist=contarctlist.replace("[","");
		    contarctlist=contarctlist.replace("]","");    
		    var contarctlistarr=contarctlist.split(",");
			var tempContract='${customerFile.contract}';
			try{			
			tempContract=tempContract.trim();
			}catch(e){}		    
		    targetElement.length = contarctlistarr.length;
			//document.forms['customerFileForm'].elements['customerFile.contract'].options[0].text = '';
			//document.forms['customerFileForm'].elements['customerFile.contract'].options[0].value = '';

				for(var i=0;i<contarctlistarr.length;i++){
				if(contarctlistarr[i] == ''){					
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
				}
				else{	
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =contarctlistarr[i].trim();
					document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =contarctlistarr[i].trim();
				} }
				
				 document.getElementById("contract").value = tempContract;
			//
				// document.getElementById("contract").value = tempContract; 
				 findDefaultSettingFromPartnerOnload();
    	//
	} }
function getContract() {
	var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
	document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var custCompanyDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if(custJobType!=''){
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&companyDivision="+encodeURI(custCompanyDivision)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse3;
     http5.send(null);
	} }
function getContractReset() {
	var custJobType = '${customerFile.job}';
    document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = '${customerFile.billToCode}';
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var custCompanyDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&companyDivision="+encodeURI(custCompanyDivision)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
     http555.open("GET", url, true);
     http555.onreadystatechange = handleHttpResponse5555555;
     http555.send(null); 
}
function findBookingAgentName(){
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if(bookingAgentCode==''){
    	document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
    }
    if(bookingAgentCode!=''){
    	showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } 
}
function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode); 
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}	

function getDestinationState(targetElement){ 
	var country = targetElement.value; 
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http31111.open("GET", url, true);
     http31111.onreadystatechange = handleHttpResponse6;
     http31111.send(null);
}
function getDestinationStateReset(target){
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6666666666;
     http3.send(null);
}
function getOriginStateReset(target) {
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	 var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse555555;
     httpState.send(null);
}
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
function handleHttpResponseCoordinator(){
	 if (httpCoordinator.readyState == 4){
         var results = httpCoordinator.responseText
         results = results.trim();
         document.forms['customerFileForm'].elements['customerFile.coordinator'].value = results;
         //alert(results);
	 }
}
<c:set var="accountHollValidation" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
<c:set var="accountHollValidation" value="Y" />
</configByCorp:fieldVisibility>
function handleHttpResponse2(billToCode){
             if (httpBilltoCode.readyState == 4){
                var results = httpBilltoCode.responseText
                results = results.trim(); 
                var res = results.split("~"); 
                if(res.length >= 2){ 
	           		if(res[5]== 'Approved'){
	           			if(res[2] == '0'){
	           				if(res[1]== '' || res[1] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[1] == 'null' || res[1] == '#')
	           				{
		           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[0];
	 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0];
	 							var test = res[4];
	 							var x=document.getElementById("billPayMethod")
	  							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;	
	 							
	 								for(var a = 0; a < x.length; a++){
	 									if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
	 										document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
	 									}}
	 							showOrHide(0);
	 							showPartnerAlert('onchange',billToCode,'hidBillTo');
	 						}else{
	 							showOrHide(0);
	 							if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
			           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
			           			}else{
			           				alert('Please select company division.');
			           			} 
			           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].select();							 	
							 	showPartnerAlert('onchange','','hidBillTo');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		           				alert('The Vendor cannot be selected as credit limit exceeded.');
		           				document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].select();
							 	showPartnerAlert('onchange','','hidBillTo');
		           			}
		           			if(res[3] > 0){
		           				if(res[1]== '' || res[1]== '#' || res[1] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[1] == 'null')
		           				{
			           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[0];
		 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0];
		 							var test = res[4];
		 							var x=document.getElementById("billPayMethod")
		  							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
		  							if(billPayMethod == 0){
		 								for(var a = 0; a < x.length; a++){
		 									if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
		 										document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
		 									}}}
		 							showOrHide(0);
	 								alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");	 								
	 								showPartnerAlert('onchange',billToCode,'hidBillTo');
	 							}else{
	 								showOrHide(0); 
				           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
				           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
				           			}else{
				           				alert('Please select company division.');
				           			} 
				           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
								 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
								 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
								 	showPartnerAlert('onchange','','hidBillTo');
			           			}}}
	           			fillAccountCodeByBillToCode();
		            }else{
		            	showOrHide(0);
		            	alert("Bill To code not approved");    
						document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
						document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
						document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
						document.forms['customerFileForm'].elements['customerFile.billToName'].select();
						showPartnerAlert('onchange','','hidBillTo');
					}
	           	}else{
		           	showOrHide(0);
		            alert("Bill To code not valid");  
		            document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
					<c:if test="${accountHollValidation=='N'}">
					document.forms['customerFileForm'].elements['customerFile.accountCode'].value = "";
			        document.forms['customerFileForm'].elements['customerFile.accountName'].value = "";
					</c:if>
					document.forms['customerFileForm'].elements['customerFile.billToName'].select();
					showPartnerAlert('onchange','','hidBillTo');
			 }   } }
function handleHttpResponsePM(){
   	if (http22.readyState == 4){
       	var results = http22.responseText
       	results = results.trim();
       	var res = results.split("#");
       	if(res.length >= 2){
	       	var test = res[6];
			var x=document.getElementById("billPayMethod")
			var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
			if(billPayMethod == 0){
				for(var a = 0; a < x.length; a++){
					if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
						document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
} } } }	 } }
var testAuditorStatus='';
function handleHttpResponse2001(){
             if (http8.readyState == 4){
                var results = http8.responseText
                results = results.trim();
                var res = results.split("#"); 
                	if(res.length >= 3){ 
                			var test = res[0].trim();
  							var x=document.getElementById("personPricing")
  							var personPricing = document.getElementById('personPricing').selectedIndex; 
 							for(var a = 0; a < x.length; a++) {
 								if(test == document.forms['customerFileForm'].elements['customerFile.personPricing'].options[a].value.trim()) {
 									document.forms['customerFileForm'].elements['customerFile.personPricing'].options[a].selected="true";
 								}
 							} 
 							var testBilling = res[1].trim();
  							var y=document.getElementById("personBilling")
  							var personBilling = document.getElementById('personBilling').selectedIndex; 
 							for(var a = 0; a < y.length; a++) {
 								if(testBilling == document.forms['customerFileForm'].elements['customerFile.personBilling'].options[a].value.trim())
 								{
 									document.forms['customerFileForm'].elements['customerFile.personBilling'].options[a].selected="true";
 								}
 							} 
 							var testPayable = res[2].trim();
  							var z=document.getElementById("personPayable")
  							var personPayable = document.getElementById('personPayable').selectedIndex; 
 							for(var a = 0; a < z.length; a++) {
 								if(testPayable == document.forms['customerFileForm'].elements['customerFile.personPayable'].options[a].value.trim())
 								{
 									document.forms['customerFileForm'].elements['customerFile.personPayable'].options[a].selected="true";
 								}}
 							var testAuditor = res[3].trim();
		           		    var t=document.getElementById("auditor")
  							var personAuditor = document.getElementById('auditor').selectedIndex; 
 							for(var a = 0; a < t.length; a++) {
 								if(testAuditor == document.forms['customerFileForm'].elements['customerFile.auditor'].options[a].value.trim())
 								{
 								    testAuditorStatus='change';
 									document.forms['customerFileForm'].elements['customerFile.auditor'].options[a].selected="true";
 								} }
 							if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value == ''){
 								if(res.length > 3){
 									var coordinator = res[4].trim();
 		 							document.forms['customerFileForm'].elements['customerFile.coordinator'].value = coordinator; 
 								}
 							}
 							if(res[6].trim()!= null && res[6].trim()!='' && res[6].trim()!= '#'){
 			                 	document.forms['customerFileForm'].elements['customerFile.internalBillingPerson'].value=res[6].trim();
 			               	}
 							}  }}
function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
				 	}try{
				 	document.forms['customerFileForm'].elements['customerFile.originCountry'].select();
				 	}catch(e){}		     	 
 				}else{ 
                 }  }}
function handleHttpResponseCopyBill(){
   if (http4.readyState == 4){
          var results = http4.responseText
          results = results.trim(); 
          if(results.length >=1){
            var code=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
            var name=document.forms['customerFileForm'].elements['customerFile.accountName'].value;
             if(code=="" || name==""){
           	   document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	           document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
             }  }  }}
function newFunctionForCountryState(){
var destinationAbc ='${customerFile.destinationCountry}';
var originAbc ='${customerFile.originCountry}';
var homeAbc='${customerFile.homeCountry}';
var enbState = '${enbState}';
var index = (enbState.indexOf(originAbc)> -1);
var index1 = (enbState.indexOf(destinationAbc)> -1);
var index2 = (enbState.indexOf(homeAbc)> -1);
if(index != ''){
document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
getOriginStateReset(originAbc);
}
else{
document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
}

if(index1 != ''){
document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
getDestinationStateReset(destinationAbc);
} 
else{
document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
}
if(index2 != ''){
document.forms['customerFileForm'].elements['customerFile.homeState'].disabled =false;
getHomeStateReset(homeAbc);
}else{
document.forms['customerFileForm'].elements['customerFile.homeState'].disabled =true;
}}
function getHomeStateReset(target){
	var country = target;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
document.forms['customerFileForm'].elements['customerFile.homeCountry'].value ='${customerFile.homeCountry}';
var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
http3333333.open("GET", url, true);
http3333333.onreadystatechange = handleHttpResponse77777777;
http3333333.send(null);
} 
function handleHttpResponse77777777(){
if (http3333333.readyState == 4){
var results = http3333333.responseText
results = results.trim();
var selectVal='${customerFile.homeState}';
var len=findActiveLength('STATE',selectVal,results);
var res = results.split("@");
targetElement = document.forms['customerFileForm'].elements['customerFile.homeState'];
targetElement.length = len+1;
document.forms['customerFileForm'].elements['customerFile.homeState'].options[0].text = '';
document.forms['customerFileForm'].elements['customerFile.homeState'].options[0].value = '';
var j=1;
for(i=0;i<res.length;i++)
{
if(res[i] == ''){
}else{
stateVal = res[i].split("#");
if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
document.forms['customerFileForm'].elements['customerFile.homeState'].options[j].text = stateVal[1];
document.forms['customerFileForm'].elements['customerFile.homeState'].options[j].value = stateVal[0];
if (document.getElementById("homeState").options[j].value == '${customerFile.homeState}'){
document.getElementById("homeState").options[j].defaultSelected = true;
}j++;}}}
document.getElementById("homeState").value = '${customerFile.homeState}';
}}   
function handleHttpResponseAcc(){
       if (http2.readyState == 4){
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#");  
          if(res.length>= 2){ 
           		if(res[2] == 'Approved'){
           			if(res[3]== '' || res[3] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[3] == 'null'){
           				document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[1];
           				document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		           	}else{
		           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
		           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
		           			}else{
		           				alert('Please select company division.');
		           			} 
		           			document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
							document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
							document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		           }
           		}else{
           			alert("Account code is not approved" ); 
				    document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
					document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
					document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
           		}
           					
          }else{
               alert("Account code not valid");    
			   document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
			   document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
			   document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		   }   }  }
  function handleHttpResponse3(){
		 if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
				var tempContract='${customerFile.contract}';
				try{			
				tempContract=tempContract.trim();
				}catch(e){}
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =res[i].trim();
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =res[i].trim();
					} }
					 document.getElementById("contract").value = tempContract; 
					 findDefaultSettingFromPartner();
		         }
}
function handleHttpResponse5555555(){
		 if (http555.readyState == 4){
                var results = http555.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =res[i].trim();
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =res[i].trim();
					}
				}
         }
}            
function handleHttpResponse4(){
		if (http2.readyState == 4) {
              var results = http2.responseText
              results = results.trim();
              var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value = res[1];
                   		document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Booking Agent code not valid");
                     document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
               }
      }
}        
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                var selectVal='${customerFile.originState}';
                var len=findActiveLength('STATE',selectVal,results);
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					}else{
						stateVal = res[i].split("#");
						if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
							document.forms['customerFileForm'].elements['customerFile.originState'].options[j].text = stateVal[1];
							document.forms['customerFileForm'].elements['customerFile.originState'].options[j].value = stateVal[0]; 
							if (document.getElementById("originState").options[j].value == '${customerFile.originState}'){
							   document.getElementById("originState").options[j].defaultSelected = true;
							}
							j++;
						}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ 
					document.getElementById("originState").value = '${customerFile.originState}';
					}
             }
        }  
   function handleHttpResponse555555(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                var selectVal='${customerFile.originState}';
                var len=findActiveLength('STATE',selectVal,results);
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[j].value = stateVal[0];
					
					if (document.getElementById("originState").options[j].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[j].defaultSelected = true;
					} j++;} }}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }       
function handleHttpResponse6(){
		if (http31111.readyState == 4){
                var results = http31111.responseText
                results = results.trim();
                var selectVal='${customerFile.destinationState}';
                var len=findActiveLength('STATE',selectVal,results);
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].value = stateVal[0]; 
					if (document.getElementById("destinationState").options[j].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[j].defaultSelected = true;
					} j++;}} }
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value = '${customerFile.destinationState}';
					   }
        }
}  
function handleHttpResponse6666666666(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                var selectVal='${customerFile.destinationState}';
                var len=findActiveLength('STATE',selectVal,results);                
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].value = '';
				var j=1;				
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[j].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[j].defaultSelected = true;
					} j++;}} }
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
        } }      
function checkPriceContract() {
    var accountCodeFromContract="";
    var accountCodeName="";
	var contracts = document.forms['customerFileForm'].elements['customerFile.contract'].value;
    contracts=contracts.trim(); 
    <c:forEach var="entry" items="${contractAccountMap}">
	if('${entry.key}'==contracts){
		accountCodeName='${entry.value}';
	}
    </c:forEach>
    if(accountCodeName.trim() !=''){
		var  accountCodeNameData = accountCodeName.split("~");
		document.forms['customerFileForm'].elements['customerFile.accountCode'].value=accountCodeNameData[0]; 
		document.forms['customerFileForm'].elements['customerFile.accountName'].value=accountCodeNameData[1];
		accountCodeFromContract='Yes';
	}else{
		fillAccountCodeByContract();
	}
    if(contracts!=''){
    var url="priceContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
     httpCheckPriceContract.open("GET", url, true);
     httpCheckPriceContract.onreadystatechange = function(){handleHttpResponse222333(accountCodeFromContract);};
     httpCheckPriceContract.send(null);
    }
} 
function handleHttpResponse222333(param) { 
	if (httpCheckPriceContract.readyState == 4)  {
                var results = httpCheckPriceContract.responseText
                results = results.trim();
                var res = results.split("#");                           
                if(res.length>=1)
                {
                	var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
                	var partnerCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
					if(billPayMethod == 0){
                document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5];
					}else if(billPayMethod != 0 && (partnerCode==null || partnerCode=='')){
				    	 document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value= res[5]; 
				     }
			     }  
                if(param==''){ 
                findDMMTypeAccountCode();} }  } 
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}      
   var http45= getHTTPObject();
   var http46= getHTTPObject();
   var http49= getHTTPObject();
	var http2 = getHTTPObject();
	var http50 = getHTTPObject50();
	var http51 = getHTTPObject51();
	var http5551 = getHTTPObject51(); 
    var http333 = getHTTPObject1();
    var http3333333= getHTTPObject1();
	var http4 = getHTTPObject3();
	var http444 = getHTTPObject3();
	var http5 = getHTTPObject();
	var http6 = getHTTPObject2();
    var http8 = getHTTPObject8();
     var http9 = getHTTPObject9();
    var httpState = getHTTPObjectState()
    var http22 = getHTTPObject22();
    var http33 = getHTTPObject33();
     var http99 = getHTTPObject();
     var http2010=getHTTPObject();
     var http31111 = getHTTPObject1();
    var httpBilltoCode = getHTTPObjectBilltoCode();
    var http255=getHTTPObject();
    var http256=getHTTPObject();
    var http257=getHTTPObject33();
    var http66 = getHTTPObject();
    var http77= getHTTPObject();
    var http88 = getHTTPObject();
    //var http12= getHTTPObject();
    function getXMLHttpRequestObject()
    {
      var xmlhttp;
      if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        try {
          xmlhttp = new XMLHttpRequest();
        } catch (e) {
          xmlhttp = false;
        }
      }
      return xmlhttp;
    }
    var http12= new getXMLHttpRequestObject();
    
function getHTTPObjectBilltoCode()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
var httpCheckPriceContract = getHTTPObjectPriceContract();
function getHTTPObjectPriceContract()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
 var http555 = getHTTPObject555();   
function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
 function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}
function checkStatusSO(){
     	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
     	var oldStatus=document.forms['customerFileForm'].elements['oldCustStatus'].value; 
     	var sequenceNumberForStatus=document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
     	if(status=='CLOSED'||status=='REOPEN' || status=='HOLD'|| status=='CNCL' || status=='NEW'){
     	if(status=='CLOSED' ){
     		var url="getServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
     		http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseServiceOrderStatus; 
     		http5.send(null); 
       	}else if( status=='CNCL'){
         	var url="checkServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
       	 	http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseStatus; 
     		http5.send(null);  
       	} 
       	 else if( status=='HOLD'){
       	  if(oldStatus=='NEW'){
       	      document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=100;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  }else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  }
       	   else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to Hold");
         	}
       	}
       	else if(status=='REOPEN') {
       	  if(oldStatus=='HOLD'){
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  }else if(oldStatus=='CNCL'){
       	       alert("There are Service Orders that are cancelled against this File, please reopen them individually.")
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  } 
       	  else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  } 
       	  else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               alert("You can't change current status to Reopen");
       	   }  }
      	else if(status=='NEW'){
      	   if(oldStatus=='DWNLD'){
      	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  }
       	  else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to New");
         	} }  } 
     if(status!='CLOSED' && status!='REOPEN' && status!='HOLD' && status !='CNCL' && status!='NEW'){
     document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
     alert("You can select only Closed, Reopen, Hold, Cancelled Status");
     }
     coordinatorRequiredStatusJob();
}
function checkStatus(){
     	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
     	var oldStatus=document.forms['customerFileForm'].elements['oldCustStatus'].value; 
     	var sequenceNumberForStatus=document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
     	if(status=='CLOSED'||status=='REOPEN' || status=='HOLD'|| status=='CNCL' || status=='NEW'){
     	if(status=='CLOSED' ){
     		var url="getServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
     		http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseServiceOrderStatus; 
     		http5.send(null); 
       	}else if( status=='CNCL'){
         	var url="checkWorkTicketStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
       	 	http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseStatusWT; 
     		http5.send(null);  
       	} 
       	 else if( status=='HOLD'){
       	  if(oldStatus=='NEW'){
       	      document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=100;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  }  else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to Hold");
         	} 	}
       	else if(status=='REOPEN') {
       	  if(oldStatus=='HOLD'){
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  }else if(oldStatus=='CNCL'){
       	       alert("There are Service Orders that are cancelled against this File, please reopen them individually.")
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  }else if(oldStatus=='CLOSED'){
       		   document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
    	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
    	       cheackStatusReason();
       	  } 
       	  else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  }  else {
       		<c:choose>
       		<c:when test="${statusChange=='Y'}">
       		
       		</c:when>
       		<c:otherwise>
       		document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
            alert("You can't change current status to Reopen");
       		</c:otherwise>
       		</c:choose>
       	   } 
      	}
      	else if(status=='NEW'){
      	   if(oldStatus=='DWNLD'){
      	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else if(status==oldStatus){
       	  document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
       	  }  else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to New");
         	}  }  
      } 
     if(status!='CLOSED' && status!='REOPEN' && status!='HOLD' && status !='CNCL' && status!='NEW'){
     document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
     alert("You can select only Closed, Reopen, Hold, Cancelled Status");
     }
     coordinatorRequiredStatusJob();
}
function handleHttpResponseServiceOrderStatus(){
             if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
              	res = results.replace("[",'');
               res = res.replace("]",'');
               if(res!=''){ 
	               alert('File cannot be closed as all corresponding service order/s are not closed');
	               document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               }  else {
               document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
               autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']); 
               }
             } 
}
 function handleHttpResponseStatusWT(){
 if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
               results = results.replace("[",'');
               results = results.replace("]",'');
               results= results.trim();  
               if(results != '0'){ 
  					alert('Work Ticket is Open for this Customer File , Customer File cannot be cancelled');
	                document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               }  else {
               checkStatusSO();
               }  }
		}
function handleHttpResponseStatus(){
 if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
               res = results.replace("[",'');
               res = res.replace("]",'');
               if(res!=0){
               if(res==1){ 
                var agree = confirm("There is "+res+" Service Order not cancelled for this Customer File. Do you want to cancel?.");
                } else {
                var agree = confirm("There are "+res+" Service Order/s not cancelled for this Customer File. Do you want to cancel all?.");
                }
	           if(agree){
	           document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
	           } else { 
	                 document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
	              } 
               }  else {
               document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
               autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']); 
               }  }
}
function sendEmail(target){
     	var originEmail = target;
   		var subject = 'C/F# ${customerFile.sequenceNumber}';
   		subject = subject+" ${customerFile.firstName}";
   		subject = subject+" ${customerFile.lastName}"; 
	   var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);				
		win = window.open(mailto_link,'emailWindow');
		if (win && win.open &&!win.closed) win.close(); 
}
function copyDestination2Email(){
    var EmailPort = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value=EmailPort;
	}   
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
document.getElementById(autocompleteDivId).style.display = "none";
if(document.getElementById(paertnerCodeId).value==''){
	document.getElementById(partnerNameId).value="";	
}
}

function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
	lastName=lastName.replace("~","'");
	document.getElementById(partnerNameId).value=lastName;
	document.getElementById(paertnerCodeId).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";	
	if(paertnerCodeId=='customerFileBookingAgentCode'){
		findCoordinatorByBA();
		findBookingAgentName();
		findCompanyDivisionByBookAg();		
	}else if(paertnerCodeId=='originAgentCode'){
		findOriginAgentName();
	}else if(paertnerCodeId=='billToCodeId'){
		billtoCheck();
	//checkAccountCodeForAssignmentType();
		//fillBillToAuthority();
	//	billToCodeFieldValueCopy();
		//checkForBlank();
	}else if(paertnerCodeId=='accountCodeIdCF'){
		findDMMTypeAccountCode();
	}
}
function cheackStatusReason()
{   
      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.status'].value; 
      if(customerFileStatus=='CLOSED' || customerFileStatus=='CNCL') { 
        document.getElementById("hidStatusReg").style.display="block";
	    document.getElementById("hidStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidStatusReg").style.display="none";
        document.getElementById("hidStatusReg1").style.display="none"; 
     } 
}
function openStandardAddressesPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.originMobile&fld_ninthDescription=customerFile.originCity&fld_eigthDescription=customerFile.originFax&fld_seventhDescription=customerFile.originHomePhone&fld_sixthDescription=customerFile.originDayPhone&fld_fifthDescription=customerFile.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=customerFile.originCountry&fld_secondDescription=customerFile.originAddress3&fld_description=customerFile.originAddress2&fld_code=customerFile.originAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="originAddSection";
}
function openStandardAddressesDestinationPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.destinationMobile&fld_ninthDescription=customerFile.destinationCity&fld_eigthDescription=customerFile.destinationFax&fld_seventhDescription=customerFile.destinationHomePhone&fld_sixthDescription=customerFile.destinationDayPhone&fld_fifthDescription=customerFile.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=customerFile.destinationCountry&fld_secondDescription=customerFile.destinationAddress3&fld_description=customerFile.destinationAddress2&fld_code=customerFile.destinationAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}
function enableState(){
var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
var AddSection = document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
	if(id!=''){
	document.forms['customerFileForm'].originSoUpdate.disabled=true;
	document.forms['customerFileForm'].originSoTicketUpdate.disabled=true;
	}
	var originCountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value
	getOriginCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(originCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);
}
if(AddSection=='destinAddSection'){
if(id!=''){
document.forms['customerFileForm'].originSoDestinUpdate.disabled=true;
document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=true;
}
	var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value
	getDestinationCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);
} 
}
function handleHttpResponse11(){
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                var selectVal='${customerFile.destinationState}';
                var len=findActiveLength('STATE',selectVal,results);
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
                var stdAddDestinState=document.forms['customerFileForm'].elements['stdAddDestinState'].value;
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.destinationState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++) {
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[j].value = stateVal[0]; 
					if (document.getElementById("destinationState").options[j].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[j].defaultSelected = true;
					} j++;} }}
					document.getElementById("destinationState").value = stdAddDestinState;
					if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
			}
			}enableStateListDestin();state();
		}
		function handleHttpResponse12(){
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                var selectVal='${customerFile.originState}';
                var len=findActiveLength('STATE',selectVal,results);                
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
                var stdAddOriginState=document.forms['customerFileForm'].elements['stdAddOriginState'].value;
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.originState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[j].value = stateVal[0];
					
					if (document.getElementById("originState").options[j].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[j].defaultSelected = true;
					}j++; } }}
					document.getElementById("originState").value = stdAddOriginState;
					if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
			 }
			else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
			 }
		} 
	 }enableStateListOrigin();zipCode();
}
var http52 = getHTTPObject52();
	function getHTTPObject52() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53() {
    var xmlhttp;
    if(window.XMLHttpRequest)   {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http66 = getHTTPObject66();
	function getHTTPObject66() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    } else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)   {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpRef = getHTTPObjectRef();
function getHTTPObjectRef(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }   }
    return xmlhttp;
}
function getRefCoord(){
	if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findCoordRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRef.open("GET", url, true);
	     	httpRef.onreadystatechange = handleHttpResponseRef;
	     	httpRef.send(null);
	    } }}
	function handleHttpResponseRef(){	
	if (httpRef.readyState == 4){
		   var cordVal = "";
           var results = httpRef.responseText
           results = results.trim();
           res = results.split("~");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
			}else{
				cordVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.coordinator'].value = cordVal[0];
			}}}}		
function getRefSale(){
	if(document.forms['customerFileForm'].elements['customerFile.estimator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findSaleRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRefSale.open("GET", url, true);
	     	httpRefSale.onreadystatechange = handleHttpResponseRefSale;
	     	httpRefSale.send(null);
	    } }}
function handleHttpResponseRefSale(){	
	if (httpRefSale.readyState == 4){
		   var saleVal = "";
           var results = httpRefSale.responseText
           results = results.trim();
           res = results.split("@");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].value = '';
			}else{
				saleVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.estimator'].value = saleVal[0];
			}}}}
function getNewCoordAndSale(temp){
	var cfContractType="${customerFile.contractType}";
	var checkUTSI="${company.UTSI}";
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var checkSoFlag = false;
    var bookAgCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
   	if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&cfContractType="+encodeURI(cfContractType)+"&checkUTSI="+encodeURI(checkUTSI)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkSoFlag="+encodeURI(checkSoFlag);
     	http50.open("GET", url, true); 
     	http50.onreadystatechange = function(){ handleHttpResponse50(temp);};
     	http50.send(null);
    } else{
    	document.forms['customerFileForm'].elements['customerFile.coordinator'].value='';
    }
   	} 
var pricingStatus='';
var billingStatus='';
var payableStatus='';
var approvedByStatus=''; 
function handleHttpResponse50(temp){	
		if (http50.readyState == 4){
			var tempCoord=document.forms['customerFileForm'].elements['customerFile.coordinator'].value;
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.coordinator'];
               	targetElement.length = res.length;               	
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[0].text = '';
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[0].value = '';
				var found=false;
 				for(i=1;i<res.length;i++){
						if(res[i] != ''){
						stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = stateVal[0];
						}
						if(!found && (stateVal[0]==tempCoord)){
							found=true;
						}
				}
 				
 				if(found){
 					document.forms['customerFileForm'].elements['customerFile.coordinator'].value=tempCoord;
 				}else{
 					document.forms['customerFileForm'].elements['customerFile.coordinator'].value="";
 				}
 				
				if(temp=='load'){
					<c:if test="${not empty customerFile.id}">
					//document.getElementById("coordinator").options[found].defaultSelected = true;
					</c:if>
                }
                else{
                getRefCoord();
                getRefSale();
                }
				var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				if(billToCode=='' || perPricing=='' ){
				pricingStatus='change';
				getPricing(temp);
				}
				else if(billToCode=='' || perBilling==''){
				billingStatus='change';
				getBilling(temp);
				}
				else if(billToCode=='' || perApprovedBy==''){
				approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
        }
}
function getPricing(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = function(){handleHttpResponse52(temp);};
     	http52.send(null);
    }  
}
function handleHttpResponse52(temp){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim(); 
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = stateVal[0];
					}
					}
				if(('${customerFile.personPricing}' != '' || '${customerFile.personPricing}' != null) && '${customerFile.personPricing}'.length > 0){
					document.getElementById("personPricing").value = '${customerFile.personPricing}';
				}
             	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perBilling==''){
				 billingStatus='change';
				getBilling(temp);
				}
				else if(billToCode=='' || perApprovedBy==''){
				approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
   function getBilling(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = function(){handleHttpResponse53(temp);};
     	http53.send(null);
    } 
}
function handleHttpResponse53(temp){
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.personBilling}' != '' || '${customerFile.personBilling}' != null) && '${customerFile.personBilling}'.length > 0){
					document.getElementById("personBilling").value = '${customerFile.personBilling}';
 				}
        var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perApprovedBy==''){
				 approvedByStatus='change';
				getApprovedBy(temp);
				}
				else if(billToCode=='' || perPayable==''){
				payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
    function getPayable(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = function(){handleHttpResponse54(temp);};
     	http54.send(null);
    } 
}
function handleHttpResponse54(temp){
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.personPayable}' != '' || '${customerFile.personPayable}' != null) && '${customerFile.personPayable}'.length > 0){
					document.getElementById("personPayable").value = '${customerFile.personPayable}';
 				}
          } 
          if(temp=='change'){
          fillResponsiblePerson();
          }
  }
   function getApprovedBy(temp){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findApprovedBy.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = function(){handleHttpResponse55(temp);};
     	http55.send(null);
    } 
}
function handleHttpResponse55(temp){
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.approvedBy'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = stateVal[0];
					}
					}
 				if(('${customerFile.approvedBy}' != '' || '${customerFile.approvedBy}' != null) && '${customerFile.approvedBy}'.length > 0){
					document.getElementById("approvedBy").value = '${customerFile.approvedBy}';
 				}
        var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
				var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
				var perApprovedBy=document.forms['customerFileForm'].elements['customerFile.approvedBy'].value;
				 if(billToCode=='' || perPayable==''){
				 payableStatus='change';
				getPayable(temp);
				}
				else if(temp=='change'){
				fillResponsiblePerson();
				}
        }
  }
   function fillResponsiblePerson(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var companyDivision = document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
   	if(job!=''){
     	var url = "fillResponsiblePerson.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&companyDivision="+encodeURI(companyDivision);
     	http66.open("GET", url, true);
     	http66.onreadystatechange = handleHttpResponse66;
     	http66.send(null);
    } 
}
 function handleHttpResponse66(){ 
   if (http66.readyState == 4){
                var results = http66.responseText
                results = results.trim();
                results=results.replace("[",''); 
                results = results.replace("]",''); 
                var res = results.split("#");
                var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
                var perBilling=document.forms['customerFileForm'].elements['customerFile.personBilling'].value;
                var perAuditor=document.forms['customerFileForm'].elements['customerFile.auditor'].value;
                var perPricing =document.forms['customerFileForm'].elements['customerFile.personPricing'].value;
				var perPayable=document.forms['customerFileForm'].elements['customerFile.personPayable'].value;
				//alert("Response1: " + res[0].length + "Response2: " + res[1].length +"Response3: " + res[2].length +"Response4: " + res[3].length);
				if(res[0]!= null && res[0]!='' && res[0]!= 'undefined'){
					if(perBilling=='' || billToCode=='' || billingStatus=='change' ){
						billingStatus='';
	                	document.forms['customerFileForm'].elements['customerFile.personBilling'].value=res[0];
	                }
  				}
  				if(res[1]!= null && res[1]!='' && res[1]!= 'undefined'){
		            if((perAuditor=='' || billToCode=='') && testAuditorStatus != 'change'){
		                document.forms['customerFileForm'].elements['customerFile.auditor'].value=res[1];
		            }
  				}
  				if(res[2]!= null && res[2]!='' && res[2]!= 'undefined'){
		            if(perPricing=='' || billToCode=='' || pricingStatus=='change'){
		            	pricingStatus='';
		                document.forms['customerFileForm'].elements['customerFile.personPricing'].value=res[2];
		            }
  				}
  				if(res[3]!= null && res[3]!='' && res[3]!= 'undefined'){
		            if(perPayable=='' || billToCode=='' || payableStatus=='change'){
		            	payableStatus='';
		                document.forms['customerFileForm'].elements['customerFile.personPayable'].value=res[3];
					}
  				}
           //      if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value=='')
               	if(res[4]!= null && res[4]!='' && res[4]!= 'undefined'){
	               	 document.forms['customerFileForm'].elements['customerFile.coordinator'].value=res[4];
               	}
               	if(res[5]!= null && res[5]!='' && res[5]!= 'undefined'){
                 	document.forms['customerFileForm'].elements['customerFile.estimator'].value=res[5];
               	}
               	if(res[6]!= null && res[6]!='' && res[6]!= 'undefined'){
                 	document.forms['customerFileForm'].elements['customerFile.internalBillingPerson'].value=res[6];
               	}
        }
   }
 var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
 function getJobList(){
	var comDiv = "";
	if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != null){
		comDiv = document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = handleHttpResponseJob;
   	httpJob.send(null);
}
function handleHttpResponseJob(){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElementValue=document.forms['customerFileForm'].elements['customerFile.job'].value;
        targetElement = document.forms['customerFileForm'].elements['customerFile.job'];
        targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = stateVal[0];
			}
		}
		document.getElementById("job").value = '${customerFile.job}';
		if(document.getElementById("reset").value!='reset'){
		document.forms['customerFileForm'].elements['customerFile.job'].value=targetElementValue;
		}else{
		document.getElementById("reset").value='';
		}
	}
}  
function coordinatorRequiredStatusJob(){
		var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forAllJob.showCordinator">
		str="1";
		</configByCorp:fieldVisibility>
      var jobNote=document.forms['customerFileForm'].elements['customerFile.job'].value;
      var statusNote=document.forms['customerFileForm'].elements['customerFile.status'].value;      
		var el = document.getElementById('coordRequiredTrue');
		var el1 = document.getElementById('coordRequiredFalse');
		if((str=="1") && (jobNote !='' || statusNote == 'STORG' || statusNote == 'DELIV'|| statusNote == 'CLAIM'|| statusNote == 'ACTIVE'|| statusNote == 'DWNLD')){
			el.style.display = 'block';		
			el1.style.display = 'none';	
		}else if(str=="1"){
			el.style.display = 'none';
			el1.style.display = 'block';
		}
		if((str=="0") && (jobNote == 'INT' || jobNote == 'GST' || statusNote == 'STORG' || statusNote == 'DELIV'|| statusNote == 'CLAIM'|| statusNote == 'ACTIVE'|| statusNote == 'DWNLD')){
				el.style.display = 'block';		
				el1.style.display = 'none';
			}else if(str=="0"){
				el.style.display = 'none';
				el1.style.display = 'block';
			}
}
function zipCode(){
        var originCountryCode=document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
		var el = document.getElementById('zipCodeRequiredTrue');
		var el1 = document.getElementById('zipCodeRequiredFalse');
		var st1=document.getElementById('originStateRequiredFalse');
		var st2=document.getElementById('originStateRequiredTrue');
		if(originCountryCode == 'United States'){
			el.style.display = 'block';		
			el1.style.display = 'none';	
			st2.style.display = 'block';		
			st1.style.display = 'none';
		}else if(originCountryCode == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
		    st2.style.display = 'none';		
			st1.style.display = 'block';
			</c:if>
		    <c:if test="${stateShowOrHide!='Y'}">
		    st2.style.display = 'block';		
			st1.style.display = 'none';
			</c:if>
		el.style.display = 'none';
		}else{
			el.style.display = 'none';
			el1.style.display = 'block';
			st1.style.display = 'block';		
			st2.style.display = 'none';	
		}
}
function state(){
        var destinationCountryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
		var ds1 = document.getElementById('destinationStateRequiredTrue');
		var ds2 = document.getElementById('destinationStateRequiredFalse');
		if(destinationCountryCode == 'United States'){
			ds1.style.display = 'block';		
			ds2.style.display = 'none';
		}else if(destinationCountryCode == 'India'){
		    <c:if test="${stateShowOrHide=='Y'}">
			ds1.style.display = 'none';		
			ds2.style.display = 'block';
			</c:if>
			 <c:if test="${stateShowOrHide!='Y'}">
			ds1.style.display = 'block';		
			ds2.style.display = 'none';
			</c:if>
		}else{
			ds1.style.display = 'none';
			ds2.style.display = 'block';
		}
}
function findCustomInfo(position) {
 var custid=document.forms['customerFileForm'].elements['customerFile.id'].value;
 var url="customInfo.html?ajax=1&decorator=simple&popup=true&custid=" + encodeURI(custid)
  ajax_showTooltip(url,position);	
  } 
  
 function viewcustom(){
 alert("No details available for this Country");
 }
  function findDocInfo(position) {
 var custid=document.forms['customerFileForm'].elements['customerFile.id'].value;
 ///alert(custid+"custid");
 var url="docInfo.html?ajax=1&decorator=simple&popup=true&custid=" + encodeURI(custid)
  ajax_showTooltip(url,position);	
  } 
  function viewcustom(){
 alert("No details available for this Country");
 }
 
 function massageFamily(){
 alert("Please Save Customer File first before adding Family members.");
 }
 function massageAddress(){
	 alert("Please Save Customer File first before adding Additional address.");
	 }
 function cheackVisaRequired()
 {   
      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.visaRequired'].value; 
      if(customerFileStatus=='Yes' ) { 
        document.getElementById("hidVisaStatusReg").style.display="block";
	    document.getElementById("hidVisaStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidVisaStatusReg").style.display="none";
        document.getElementById("hidVisaStatusReg1").style.display="none";
     } 
 }
 function setContractValidFlag(temp){
  document.forms['customerFileForm'].elements['contractValidFlag'].value=temp;
 }
function qryHowOld(varAsOfDate, varBirthDate)  {
   var dtAsOfDate;
   var dtBirth;
   var dtAnniversary;
   var intSpan;
   var intYears;
   var intMonths;
   var intWeeks;
   var intDays;
   var intHours;
   var intMinutes;
   var intSeconds;
   var strHowOld;
   dtBirth = new Date(varBirthDate);
   dtAsOfDate = new Date(varAsOfDate);
   if ( dtAsOfDate >= dtBirth )
      {
      intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
                  dtAsOfDate.getUTCMinutes() * 60000 +
                  dtAsOfDate.getUTCSeconds() * 1000    ) -
                ( dtBirth.getUTCHours() * 3600000 +
                  dtBirth.getUTCMinutes() * 60000 +
                  dtBirth.getUTCSeconds() * 1000       )
      // start at as of date and look backwards for anniversary 

      // if as of day (date) is after birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is on or after birth time
      if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
           ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) )
         {
         // most recent day (date) anniversary is in as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth(),
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );
         } else {
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth() - 1,
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );
         intMonths = dtAsOfDate.getUTCMonth() - 1;
         if ( intMonths == -1 )
            intMonths = 11;
         while ( dtAnniversary.getUTCMonth() != intMonths )
            dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );
         }
      if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
         {
         intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();
         intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();
         } else {
         intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;
         intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();
         }     
      intSpan = dtAsOfDate - dtAnniversary; 
      intDays = Math.floor(intSpan / 86400000);
      intSpan = intSpan - (intDays * 86400000);
      intHours = Math.floor(intSpan / 3600000);    
      intSpan = intSpan - (intHours * 3600000);
      intMinutes = Math.floor(intSpan / 60000);
      intSpan = intSpan - (intMinutes * 60000);
      intSeconds = Math.floor(intSpan / 1000);
      if ( intYears >= 0 )
         if ( intYears > 1 )
            strHowOld = intYears.toString() + 'Y';
         else
            strHowOld = intYears.toString() + 'Y';
      else
         strHowOld = '';

      if ( intMonths > 0 )
         if ( intMonths > 1 )
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
         else
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
    
      if ( intDays > 0 )
         if ( intDays > 1 )
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
         else
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
      }
   else
      strHowOld = ''  
   return strHowOld
   }   		
function calcDays() {
 document.forms['customerFileForm'].elements['customerFile.duration'].value="";
 var date2 = document.forms['customerFileForm'].elements['customerFile.contractStart'].value;	 
 var date1 = document.forms['customerFileForm'].elements['customerFile.contractEnd'].value; 
 
  var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   } else if(month == 'Feb')
   {
       month = "02";
   } else if(month == 'Mar')
   {
       month = "03"
   } else if(month == 'Apr')
   {
       month = "04"
   }  else if(month == 'May')
   {
       month = "05"
   } else if(month == 'Jun')
   {
       month = "06"
   }  else if(month == 'Jul')
   {
       month = "07"
   } else if(month == 'Aug')
   {
       month = "08"
   }  else if(month == 'Sep')
   {
       month = "09"
   }  else if(month == 'Oct')
   {
       month = "10"
   }  else if(month == 'Nov')
   {
       month = "11"
   }  else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var finalDatevalue = day+"-"+month+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan') {
       month2 = "01";
   } else if(month2 == 'Feb') {
       month2 = "02";
   }  else if(month2 == 'Mar') {
       month2 = "03"
   }  else if(month2 == 'Apr') {
       month2 = "04"
   }  else if(month2 == 'May') {
       month2 = "05"
   }  else if(month2 == 'Jun') {
       month2 = "06"
   }   else if(month2 == 'Jul') {
       month2 = "07"
   }  else if(month2 == 'Aug') {
       month2 = "08"
   }  else if(month2 == 'Sep') {
       month2 = "09"
   }  else if(month2 == 'Oct') {
       month2 = "10"
   }  else if(month2 == 'Nov') {
       month2 = "11"
   }  else if(month2 == 'Dec') {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var finalDate2Value = day2+"-"+month2+"-"+year2;
  var Cdate1 = Calendar.parseDate(finalDatevalue,false);
  var Cdate2 = Calendar.parseDate(finalDate2Value,false);
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]).getFullYear();
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]).getFullYear();
  var diff = Math.round((sDate-eDate)/86400000);
  var  daysApart=0;
  if(document.forms['customerFileForm'].elements['customerFile.contractEnd'].value!=null && document.forms['customerFileForm'].elements['customerFile.contractEnd'].value!='' && Cdate1<Cdate2)
  {
    alert("Contract Start Date must be less than Contract End Date");
    document.forms['customerFileForm'].elements['customerFile.duration'].value='';  
    document.forms['customerFileForm'].elements['customerFile.contractEnd'].value=""; 
    }else{
  daysApart=qryHowOld(Cdate1, Cdate2);
  }
  document.forms['customerFileForm'].elements['customerFile.duration'].value = daysApart; 

  if(document.forms['customerFileForm'].elements['customerFile.duration'].value=='NaN')
   {
     document.forms['customerFileForm'].elements['customerFile.duration'].value = '';
   } 
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value = '';
}

function forDays(){
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value ='1';
} 

function checkAssignmentType(){ 
 if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=="Other"){ 
 document.getElementById('otherContract').style.display = 'block'; 
 }else
 {
 document.getElementById('otherContract').style.display ='none'; 
 } 
 }
 function openHomeCountryLocation(){
	    var city = document.forms['customerFileForm'].elements['customerFile.homeCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
        var zip = "";
        var address = "";
        address = address.replace("#","-");
        var state = "";
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
 function autoPopulate_customerFile_homeCountry(targetElement) {   
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States'){
			dCountryCode = "USA";
		}
		if(dCountry == 'India'){
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada'){
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
			if(dCountry == 'United States'){
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.homeState'].value = '';
		}
	} 
 function gethomeState(targetElement){
	var country = targetElement.value;
	var countryCode = "";	
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findActiveStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http333.open("GET", url, true);
     http333.onreadystatechange = handleHttpResponsehomeState;
     http333.send(null);
}	
 function handleHttpResponsehomeState(){
		if (http333.readyState == 4){
                var results = http333.responseText
                results = results.trim();
                var selectVal='${customerFile.homeState}';
                var len=findActiveLength('STATE',selectVal,results);                
                var res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.homeState'];
                targetElement.length = len+1;
                document.forms['customerFileForm'].elements['customerFile.homeState'].options[0].text = '';
                document.forms['customerFileForm'].elements['customerFile.homeState'].options[0].value = '';
				var j=1;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
					}else{
					stateVal = res[i].split("#");
					if(stateVal!=null && stateVal!=undefined && stateVal.length>=3 && (stateVal[2]=='Active' || stateVal[0]==selectVal)){
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[j].value = stateVal[0];
					
					if (document.getElementById("homeState").options[j].value == '${customerFile.homeState}'){
					   document.getElementById("homeState").options[j].defaultSelected = true;
					}j++;
					}
					}}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("homeState").value = '';
					}
					else{ document.getElementById("homeState").value == '${customerFile.homeState}';
					   }
        }
}
 function saveValidation(){
	 var newPage= document.forms['customerFileForm'].elements['newPage'].value;
	 if(newPage=='N')
			{
	     submitRelo();
			}	
		else 
			{
	     submitReloNew();
			} }

function submitRelo()
  { 
  if(document.forms['customerFileForm'].elements['parentAgentForOrderinitiation'].value=='true'){
document.forms['customerFileForm'].elements['removalRelocationService.isIta60'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta60'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta1m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta1m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta2m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta2m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta3m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta3m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isTransportAllow'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isTransportAllow'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isStorage'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isStorage'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isUsdollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isUsdollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isDollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isDollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOtherCurrency'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOtherCurrency'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.usdollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.usdollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.dollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.dollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherCurrencyAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherCurrencyAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherServicesRequired'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherServicesRequired'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.orientationTrip'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.orientationTrip'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.authorizedDays'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.authorizedDays'].value;
document.forms['customerFileForm'].elements['removalRelocationService.additionalNotes'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.additionalNotes'].value;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
<configByCorp:fieldVisibility componentId="component.field.removalRelocationSer.immigration">
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;
</configByCorp:fieldVisibility>
  }
var visaRequired= document.forms['customerFileForm'].elements['customerFile.visaRequired'].value;
var visaStatus= document.forms['customerFileForm'].elements['customerFile.visaStatus'].value;
if(visaRequired==""){
			alert("Visa Required is a required field .");
			return false;
		}
		if(visaRequired!= "" && visaRequired=="Yes"){
			if(visaStatus==""){
			alert("Visa Status is a required field .");
			return false;
			}
		}
if(document.forms['customerFileForm'].elements['customerFile.petsInvolved'].checked==true){
		if(document.forms['customerFileForm'].elements['customerFile.petType'].value==""){
			alert("Type Of Pet is a required field in Entitlement");
			return false;
			}
		}

if ('${mandatorySource}'=="Y"){
	if(document.forms['customerFileForm'].elements['customerFile.source'].value==""){
		alert("Source is a required field.");
		return false;
		}
}
<configByCorp:fieldVisibility componentId="component.contract.reloJob.tenancyBilling">
	if(document.forms['customerFileForm'].elements['customerFile.comptetive'].value==""){
		alert("Competitive is a required field.");
		return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
var accouncode=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
var flagPrivateParty=document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value;
var accountCodeValidationFlag=document.forms['customerFileForm'].elements['accountCodeValidationFlag'].value;
if(accouncode=='' && flagPrivateParty!='true' && accountCodeValidationFlag!='Y'){
	alert("Account Code is a required field.");
	return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.accountCode.allFile.validation">
var accountUnipCode= document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
if(accountUnipCode==''){
	alert("Account Code is a required field.");
	return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory">
if(document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value==""){
	alert("OriginHome Phone Number is a required field.");
	return false;
}

if(document.forms['customerFileForm'].elements['customerFile.originMobile'].value==""){
	alert("Origin Mobile Number is a required field.");
	return false;
}

</configByCorp:fieldVisibility>
return true;	
} 
function submitReloNew()
{ 
if(document.forms['customerFileForm'].elements['parentAgentForOrderinitiation'].value=='true'){
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
<configByCorp:fieldVisibility componentId="component.field.removalRelocationSer.immigration">
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;
</configByCorp:fieldVisibility>
}
var visaRequired= document.forms['customerFileForm'].elements['customerFile.visaRequired'].value;
var visaStatus= document.forms['customerFileForm'].elements['customerFile.visaStatus'].value;
if(visaRequired==""){
			alert("Visa Required is a required field .");
			return false;
		}
		if(visaRequired!= "" && visaRequired=="Yes"){
			if(visaStatus==""){
			alert("Visa Status is a required field .");
			return false;
			}
		}
if(document.forms['customerFileForm'].elements['customerFile.petsInvolved'].checked==true){
		if(document.forms['customerFileForm'].elements['customerFile.petType'].value==""){
			alert("Type Of Pet is a required field in Entitlement");
			return false;
			}
		}

if ('${mandatorySource}'=="Y"){
	if(document.forms['customerFileForm'].elements['customerFile.source'].value==""){
		alert("Source is a required field.");
		return false;
		}
}
<configByCorp:fieldVisibility componentId="component.contract.reloJob.tenancyBilling">
	if(document.forms['customerFileForm'].elements['customerFile.comptetive'].value==""){
		alert("Competitive is a required field.");
		return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
var accouncode=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
var flagPrivateParty=document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value;
var accountCodeValidationFlag=document.forms['customerFileForm'].elements['accountCodeValidationFlag'].value;
if(accouncode=='' && flagPrivateParty!='true' && accountCodeValidationFlag!='Y'){
	alert("Account Code is a required field.");
	return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.accountCode.allFile.validation">
var accountUnipCode= document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
if(accountUnipCode==''){
	alert("Account Code is a required field.");
	return false;
	}
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory">
if(document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value==""){
	alert("OriginHome Phone Number is a required field.");
	return false;
}

if(document.forms['customerFileForm'].elements['customerFile.originMobile'].value==""){
	alert("Origin Mobile Number is a required field.");
	return false;
}

</configByCorp:fieldVisibility>
return true;
}
function checkAllRelo(){ 
<c:if test="${serviceRelosCheck!=''}">	
var len = document.forms['customerFileForm'].elements['checkV'].length;
var check=true;
for (i = 0; i < len; i++){ 
        if(document.forms['customerFileForm'].elements['checkV'][i].checked == false ){
        check=false;
        i=len+1
        }
    } 
    if(check){
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=true;
    }else{
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=false;
    }
</c:if>    
}
function checkAll(temp){
    document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = "";
    var len = document.forms['customerFileForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){
        document.forms['customerFileForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){
        document.forms['customerFileForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
}   
function selectColumn(targetElement){  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' + rowId;
      document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
     var userCheckStatus=document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( rowId , '' );
     document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
     } 
    }
 function checkColumn(){ 
  var userCheckStatus ="";
  userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
  var index = userCheckStatus.indexOf("#"); 
        while(index != -1){ 
            userCheckStatus = userCheckStatus.replace("#", ",");
            index = userCheckStatus.indexOf("#");
            } 
userCheckStatus=userCheckStatus.trim();
	  	    if(userCheckStatus.indexOf(",")==0)
	  		 {
	  	    	userCheckStatus=userCheckStatus.replace(",", "");
	  		 } 
if(userCheckStatus!=""){ 
document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value="";
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
     var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' +column[i];   
     try{ 
       document.getElementById(column[i]).checked=true;
       } catch(e){}	
} 
}
if(userCheckStatus==""){
}
} 
function getHomeCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http444.open("GET", url, true);
    http444.onreadystatechange = httpHomeCountryName;
    http444.send(null);
}
function httpHomeCountryName(){
     if (http444.readyState == 4){
                var results = http444.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.homeCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='N';
 					}	
 					try{
 					document.forms['customerFileForm'].elements['customerFile.homeCountry'].select();
 					}catch(e){}
				}
             }
}
</script>
<script type="text/javascript">

var httpCoord=getHttpObjectCoord();

function getHttpObjectCoord(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function changeReset(){
   document.getElementById("reset").value='reset';
 }
 
function getResetCoord(){
  var job='${customerFile.job}';
    if(job!=''){
    	var cfContractType="${customerFile.contractType}";
    	var checkUTSI="${company.UTSI}";
    	var checkSoFlag = false;
        var bookAgCode = '${customerFile.bookingAgentCode}';
    url="findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&cfContractType="+encodeURI(cfContractType)+"&checkUTSI="+encodeURI(checkUTSI)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkSoFlag="+encodeURI(checkSoFlag);
    httpCoord.open("GET", url, true);
    httpCoord.onreadystatechange=httpResponseCoord;
    httpCoord.send(null);
  }
}
function httpResponseCoord(){	
		if (httpCoord.readyState == 4){
                var results = httpCoord.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = stateVal[0];
					}
				if (document.getElementById("coordinator").options[i].value == '${customerFile.coordinator}'){
					   document.getElementById("coordinator").options[i].defaultSelected = true;
					}
			}	
        }
}
function autoPopulateSecondryEmail(){
  if(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value == '')
     {
        document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value=document.forms['customerFileForm'].elements['customerFile.email2'].value;
     }
}
function checkPetInvolve(result){
if(result.checked==true){
document.getElementById('petTypeId').style.display ='block';
}if(result.checked!=true){document.getElementById('petTypeId').style.display ='none';
}
}
 var httpPer=getHTTPObject();
 var httpVoxme = getHTTPObject();
 function checkForBlank(){
 var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
 var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
  var compDivision=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	  if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
    	var url="pricingBillingPaybaleNameByJob.html?ajax=1&decorator=simple&popup=true&jobType="+encodeURI(job) +"&compDivision="+encodeURI(compDivision);
    	httpPer.open("GET", url, true);
    httpPer.onreadystatechange=httppricingBillingPaybaleByJob;
    httpPer.send(null);
    }
 }
 function httppricingBillingPaybaleByJob(){
 if (httpPer.readyState == 4){
                var results = httpPer.responseText
                results = results.trim();
                res = results.split("#");
                for(i=0;i<res.length;i++){
                	if(res[0]!= null && res[0]!='' && res[0]!= undefined){
	               		 document.forms['customerFileForm'].elements['customerFile.personPricing'].value=res[0];
                	}
                	if(res[1]!= null && res[1]!='' && res[1]!= undefined){
	               		document.forms['customerFileForm'].elements['customerFile.personBilling'].value=res[1];
                	}
                	if(res[2]!= null && res[2]!='' && res[2]!= undefined){
	            		 document.forms['customerFileForm'].elements['customerFile.personPayable'].value=res[2];
                	}
                    if(res[3]!= null && res[3]!='' && res[3]!= undefined){
	               		document.forms['customerFileForm'].elements['customerFile.auditor'].value=res[3];
                    }
                    if(res[4]!= null && res[4]!='' && res[4]!= undefined){
	               		document.forms['customerFileForm'].elements['customerFile.internalBillingPerson'].value=res[4];
                    }
                }
         }
 }
function sendMailFromVoxme(target){     
			var estimator = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
           var survey = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyDutch = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var sendsurveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyDutchTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
           var sendsurveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
           var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
           var surveyType = document.forms['customerFileForm'].elements['customerFile.surveyType'].value;
           var voxmeInvTag = 'No';
           <configByCorp:fieldVisibility componentId="component.customerFile.voxme.invTag">
           		voxmeInvTag = 'Yes';
           </configByCorp:fieldVisibility>
           var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
           var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
           var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
           var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
          if (hour<12){
           		surveyTime=hour+":"+min+"AM";
           } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
               surveyTime=(hour-12)+":"+min+"PM";
           }else{
               surveyTime=hour+":"+min+"PM";
           }
            if (hour2<12){
           		surveyTime2=hour2+":"+min2+"AM";
           } else if (hour2>12) {
               surveyTime2=(hour2-12)+":"+min2+"PM";
           }else{
               surveyTime2=hour2+":"+min2+"PM";
           }
           if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
           alert("Survey is not yet Scheduled - Cannot send Survey Email");
           } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
	        var systemDate = new Date();
			var mySplitResult = survey.split("-");
		   	var day = mySplitResult[0];
		   	var month = mySplitResult[1];
		   	var year = mySplitResult[2];
		 	if(month == 'Jan'){
		       month = "01";
		   	}else if(month == 'Feb'){
		       month = "02";
		   	}else if(month == 'Mar'){
		       month = "03"
		   	}else if(month == 'Apr'){
		       month = "04"
		   	}else if(month == 'May'){
		       month = "05"
		   	}else if(month == 'Jun'){
		       month = "06"
		   	}else if(month == 'Jul'){
		       month = "07"
		   	}else if(month == 'Aug'){
		       month = "08"
		   	}else if(month == 'Sep'){
		       month = "09"
		   	}else if(month == 'Oct'){
		       month = "10"
		   	}else if(month == 'Nov'){
		       month = "11"
		   	}else if(month == 'Dec'){
		       month = "12";
		   	}
		 	var finalDate = month+"/"+day+"/"+year;
		   	survey = finalDate.split("/");
		   	var enterDate = new Date(month+"/"+day+"/20"+year);
		  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
		   	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
		   	if(daysApart < 0){
		    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
		    	return false;
		  	}
		  	if(survey != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			if (hour < 0  || hour > 23) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					alert('Please enter correct time.');
					return false;
				}
				if (min<0 || min > 59) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					alert('Please enter correct time.');
					return false;
				}
				if(systemDate.getHours() > hour){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				   }  }   }
		  	if(target=='MoveToCloud' && surveyType==''){
		  		alert("Please select Survey Type.");
				return false;	
		  	}
		  	var agree= confirm("Press OK to Send Survey Email, else press Cancel.");	  	   
            if(agree){
            	if(target=='Voxme'){
            	var url='sendVoxmeMail.html?ajax=1&id=${customerFile.id}&estimator='+estimator+'&surveyDates='+finalDate+'&surveyTime='+sendsurveyTime+'&surveyTime2='+sendsurveyTime2+'&voxmeInvTag='+voxmeInvTag+'&decorator=simple&popup=true';
            	httpVoxme.open("GET", url, true);
        		httpVoxme.onreadystatechange=httpVoxmeMailSender;
        		httpVoxme.send(null);
            	}else{
            		var url='sendMoveToCloud.html?ajax=1&id=${customerFile.id}&taskType='+surveyType+'&estimator='+estimator+'&surveyDates='+finalDate+'&surveyTime='+sendsurveyTime+'&surveyTime2='+sendsurveyTime2+'&voxmeInvTag='+voxmeInvTag+'&decorator=simple&popup=true';
                	httpVoxme.open("GET", url, true);
            		httpVoxme.onreadystatechange=httpVoxmeMailSender;
            		httpVoxme.send(null);
            	}
       	}else {             
	  	    }	  	   
            } else {
            alert("Survey is not yet Scheduled - Cannot send Survey Email.");
            }       
} 
function httpVoxmeMailSender(){
	if (httpVoxme.readyState == 4){
		      var results = httpVoxme.responseText
	           results = results.trim();
	           if(results=='sent'){
	        	}else{
	        	}
	           location.reload(); 
	           }
}
function quotesToGoMethod(){
    var fname=document.forms['customerFileForm'].elements['customerFile.firstName'].value;
    var lastname=document.forms['customerFileForm'].elements['customerFile.lastName'].value;
    var originDayPhone=document.forms['customerFileForm'].elements['customerFile.originDayPhone'].value;
    var originHomePhone=document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
    var originMobile=document.forms['customerFileForm'].elements['customerFile.originMobile'].value;
     if(fname!='' && lastname!='' && (originDayPhone!='' || originHomePhone!='' || originMobile!=''))
    {
     	var url = "saveQuotationFileToGoMethodCustomerFile.html";
     	document.forms['customerFileForm'].action = url;
        document.forms['customerFileForm'].submit();
    } else {
        alert("Please enter the missing information for: \nFirst Name\nLast Name\nWork Phone or Home Phone or Mobile Phone");
    }          
    
}
</script>
<script type="text/javascript">
function changeComptetive(targetElement){
	var moveTypeVal=targetElement.value;
	if(moveTypeVal=='Quote'){
		document.forms['customerFileForm'].elements['customerFile.comptetive'].value='Y';
	}else{
		document.forms['customerFileForm'].elements['customerFile.comptetive'].value='';
	}
}
function showQuotationStatus(targetElement,type){	
	var moveTypeVal="";
	if(type=='L'){
		moveTypeVal=targetElement;
	}else{
		moveTypeVal=targetElement.value;
	}
	if(moveTypeVal=='Quote'){
		if(document.getElementById('quotationStatusShow')!=undefined){
		document.getElementById('quotationStatusShow').style.display="block";
		}
		if(document.getElementById('quotationStatusLabel')!=undefined){
		document.getElementById('quotationStatusLabel').style.display="block";
		}
	}else{
		if(document.getElementById('quotationStatusShow')!=undefined){
		document.getElementById('quotationStatusShow').style.display="none";
		}
		if(document.getElementById('quotationStatusLabel')!=undefined){
		document.getElementById('quotationStatusLabel').style.display="none";
		}
	}
	var quotationStatus= document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
	if((moveTypeVal=='Quote' && quotationStatus=='Rejected') || (moveTypeVal=='Quote' && quotationStatus=='Cancelled')){
		document.getElementById('statusReasonId').style.display="block";
		document.getElementById('statusReasonId1').style.display="block";
	}else{
		if(document.getElementById('statusReasonId')!=undefined){
		document.getElementById('statusReasonId').style.display="none";
		}
		if(document.getElementById('statusReasonId1')!=undefined){
		document.getElementById('statusReasonId1').style.display="none";
		}
	}
}
function showStatusReason(targetElement){	
	var quotationStatus= targetElement.value;	
	<configByCorp:fieldVisibility componentId="component.field.qfStatusReason.showVOER">	
	if(quotationStatus=='Rejected' || quotationStatus=='Cancelled'){
		document.getElementById('statusReasonId').style.display="block";
		document.getElementById('statusReasonId1').style.display="block";
	}else{
		document.getElementById('statusReasonId').style.display="none";
		document.getElementById('statusReasonId1').style.display="none";
	}
	</configByCorp:fieldVisibility>
}
function resetQuotationStatus(){
	<c:if test="${not empty customerFile.quoteAcceptenceDate}">
		<s:text id="quoteAcceptenceDateValue1" name="${FormDateValue}"> 
			<s:param name="value" value="customerFile.quoteAcceptenceDate" />
		</s:text>
		<c:set var="quoteAcceptenceDateTemp" value="${quoteAcceptenceDateValue1}"/> 
	</c:if>
	var mydate=new Date();
	var daym;
	var year=mydate.getFullYear()
	var y=""+year;
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if(month == 1)month="Jan";
	if(month == 2)month="Feb";
	if(month == 3)month="Mar";
	if(month == 4)month="Apr";
	if(month == 5)month="May";
	if(month == 6)month="Jun";
	if(month == 7)month="Jul";
	if(month == 8)month="Aug";
	if(month == 9)month="Sep";
	if(month == 10)month="Oct";
	if(month == 11)month="Nov";
	if(month == 12)month="Dec";
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym;
	var datam = daym+"-"+month+"-"+y.substring(2,4);	
	var  quotationStatus=document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value ;
	if(quotationStatus=='Accepted'){
		document.forms['customerFileForm'].elements['customerFile.quoteAcceptenceDate'].value=datam;
		document.forms['customerFileForm'].elements['customerFile.bookingDate'].value=datam;
	}else{
		<c:if test="${not empty customerFile.quoteAcceptenceDate}">
		document.forms['customerFileForm'].elements['customerFile.quoteAcceptenceDate'].value='${quoteAcceptenceDateTemp}';
		</c:if>
		<c:if test="${empty customerFile.quoteAcceptenceDate}">
		document.forms['customerFileForm'].elements['customerFile.quoteAcceptenceDate'].value='';
		</c:if>
		document.forms['customerFileForm'].elements['customerFile.bookingDate'].value='';
	}
}
function checkQuotation(){ 
	var bookCode=document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	 var  quotationStatus=document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value ; 
	 <c:set var="closedCompanyDivisionVar" value="${closeCompanyDivisionList}" />
		 <c:if test="${fn1:indexOf(closedCompanyDivisionVar,customerFile.companyDivision)>=0}"> 
	 if(quotationStatus=='Accepted'){
		alert("Quote cannot be accepted as Company Division is closed.");
		document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
	 }else{
		 resetQuotationStatus(); 
	 }
	 </c:if>
	 <c:if test="${fn1:indexOf(closedCompanyDivisionVar,customerFile.companyDivision)<0}"> 
	if(bookCode!=''&& quotationStatus=='Accepted'){
		var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
		http45.open("GET", url, true);
		http45.onreadystatechange = handleHttpResponse453;
		http45.send(null);	
	}else{
		resetQuotationStatus();
	}
	</c:if>
}
	function handleHttpResponse453(){
		if (http45.readyState == 4) {
	          var results = http45.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){
	       			if(res[2] == 'Approved'){ 
	       				var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	       			 var comp = document.forms['customerFileForm'].elements['customerFile.comptetive'].value;
	       				if(comp=='Y'){
		   					if(billToCode.trim()==''){
		   						checkQuotesBaCheck();
		   					}else{
		   						checkQuotationBillToCode();
		   					}
	       				}
	       				if(comp!='Y'){
	       					checkQuotationBillToCode();
	       				}
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");	
	           			<c:if test="${not empty customerFile.quotationStatus}">
	           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
	           			</c:if>
	           			<c:if test="${empty customerFile.quotationStatus}">
	           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value="New";
	           			</c:if>
	           		}			
	      		}else{
	           		alert("Booking Agent code not valid");
           			<c:if test="${not empty customerFile.quotationStatus}">
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
           			</c:if>
           			<c:if test="${empty customerFile.quotationStatus}">
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value="New";
           			</c:if>
		   		}
	     resetQuotationStatus();
	  }
	} 
	function checkQuotationBillToCode(){
		var billToCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
			var url="findBookAgentCodeStatus.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(billToCode);
			http46.open("GET", url, true);
			http46.onreadystatechange = handleHttpResponse463;
			http46.send(null);	
		}
	function handleHttpResponse463(){
		if (http46.readyState == 4) {
	          var results = http46.responseText
	          results = results.trim();
	          var res = results.split("@"); 
	       		if(res.size() >= 2){ 
	       			if(res[2] == 'Approved'){
	       				checkQuotesBaCheck();
	           		}else{
	           			alert("Quote cannot be accepted as selected partners have not been approved yet");
	           			<c:if test="${not empty customerFile.quotationStatus}">
	           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
	           			</c:if>
	           			<c:if test="${empty customerFile.quotationStatus}">
	           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value="New";
	           			</c:if>
	           		}			
	      		}else{
	           		alert("Bill To code not valid");
           			<c:if test="${not empty customerFile.quotationStatus}">
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
           			</c:if>
           			<c:if test="${empty customerFile.quotationStatus}">
           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value="New";
           			</c:if>
		   		}
	       		resetQuotationStatus();
	  }
	}
	function checkQuotesBaCheck(){
		var id='${customerFile.id}';
		var url="findQuotesBookAgentStatus.html?ajax=1&decorator=simple&popup=true&id="+encodeURI(id);
		http49.open("GET", url, true);
		http49.onreadystatechange = handleHttpResponse493;
		http49.send(null);
	} 
	function handleHttpResponse493(){
		if (http49.readyState == 4) {
	          var results = http49.responseText
	          results = results.trim();
	       		if(results!=''){ 	           			
	           			document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value='${customerFile.quotationStatus}';
	      		}else{
			      		if('${QuotesNumber}'>1 && document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value =='Accepted'){
			           	openWindow('customerFileServiceOrdersStatus.html?id=${customerFile.id}&checkAccessQuotation=${checkAccessQuotation}&forQuotation=QC&decorator=popup&popup=true',900,300);
			           	}
		   		}
	      resetQuotationStatus();	     
	  }
	}
</script>
<script type="text/javascript">
function winOpenOrigin(){
	<c:if test="${agentClassificationShow=='N'}">
   		openWindow('originPartners.html?origin=${customerFile.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=customerFile.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originAgentName&fld_code=customerFile.originAgentCode');	
   		document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
	</c:if>
	<c:if test="${agentClassificationShow=='Y'}">
		openWindow('ClassifiedAgentPartners.html?origin=${customerFile.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=customerFile.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originAgentName&fld_code=customerFile.originAgentCode'); 
		document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
	</c:if>
}
function findOriginAgentName(){
    var originAgentCode = document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value;
    if(originAgentCode==''){
    	document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
    }
    if(originAgentCode!=''){
    <c:if test="${agentSearchValidation}">
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&pType=CRNOTALLOWED&partnerCode=" + encodeURI(originAgentCode);
     </c:if>
     <c:if test="${!agentSearchValidation}">
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(originAgentCode);
     </c:if>
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse44;
     http2.send(null);
    } 
}
function handleHttpResponse44(){
	<configByCorp:fieldVisibility componentId="component.field.trackingstatus.classifiedAgent">		
		<c:set var="classifiedAgentOriginValidation" value="Y" />
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.customerFile.notClassified">		
		<c:set var="classifiedAgentOriginValidation" value="N" />
	</configByCorp:fieldVisibility>
	if (http2.readyState == 4) {
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#"); 
          if(res.length>2){
            	if(res[2] == 'Approved'){
            		<c:if test="${classifiedAgentOriginValidation=='Y'}">
            		if(res[11]=='' && res[8]=='T'){
            			alert("This Agent cannot be processed as it is not classified.")
            			document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
            			document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value="";
            			document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value ="";
            			document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
            			return false;
            		}else{
            			document.forms['customerFileForm'].elements['customerFile.originAgentName'].value = res[1];
            			document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value = res[4];
            			document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
                   	   getCustomerContactInfo();
                   }
            		</c:if>
            		<c:if test="${classifiedAgentOriginValidation!='Y'}">
	            		document.forms['customerFileForm'].elements['customerFile.originAgentName'].value = res[1];
	           			document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value = res[4];
	               		document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
	               	   getCustomerContactInfo();
             		</c:if>
            	}else{
           			alert("Origin Agent code is not approved" ); 
				    document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
				    document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value="";
				    document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value ="";
				    document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
				 showPartnerAlert('onchange','','hidTSOriginAgent');
           		}  
            }else{
                 alert("Origin Agent code not valid");
                 document.forms['customerFileForm'].elements['customerFile.originAgentName'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value ="";
				 document.forms['customerFileForm'].elements['customerFile.originAgentCode'].select();
				 showPartnerAlert('onchange','','hidTSOriginAgent');
           }
  }
}    
function buttondisabledext(){
	var billToCode='${customerFile.billToCode}';
	if(billToCode!=''){
		document.forms['customerFileForm'].elements['P'].disabled=true;
	}else{
	    document.forms['customerFileForm'].elements['P'].disabled=false;
	}
}

function iehack(){
	if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ 
		var ieversion=new Number(RegExp.$1) 
			var elementList = document.getElementsByTagName('*');
			var imageList = document.getElementsByTagName('img');
			 for(var i = 0 ; i < imageList.length ; i++){
				 if(imageList[i].nodeName == 'IMG'){
					if(imageList[i].src.indexOf('calender.png') > 0){
						imageList[i].src = 'images/navarrow.gif'; 
						var elementID  = imageList[i].id;
						if(elementID.indexOf('trigger')>0){
							imageList[i].style.display = 'none';
						}
					}
					if(imageList[i].src.indexOf('open-popup.gif')>0){ 
						imageList[i].src = 'images/navarrow.gif';
						imageList[i].style.display = 'none';
					}
					if(imageList[i].src.indexOf('notes_empty1.jpg')>0){
						imageList[i].src = 'images/navarrow.gif';
						imageList[i].style.display = 'none';
					}
					if(imageList[i].src.indexOf('notes_open1.jpg')>0){
						imageList[i].src = 'images/navarrow.gif';
						imageList[i].style.display = 'none';
					}
					if(imageList[i].src.indexOf('images/nav')>0){
						imageList[i].src = 'images/navarrow.gif';
						imageList[i].style.display = 'none';
					}
				 }
			 }
			 for(var i in elementList){
				 if(elementList[i].type == 'text'){
						 elementList[i].readonly =true;
						 elementList[i].className = 'input-textUpper';
					}
				 if(elementList[i].type == 'checkbox'){
					 elementList[i].disabled =true;
					}
				 if(elementList[i].type == 'select-one'){
						 elementList[i].disabled =true;
						 elementList[i].style.className = 'list-menu';
					}
				 if(elementList[i].type=='button'){
						 elementList[i].style.display = 'none';
						var fieldSetArray = document.getElementsByTagName('fieldset');
						for(var j = 0 ; j < fieldSetArray.length ; j++)
								fieldSetArray[j].style.display = 'none'; 
					}	
			}
			 <sec-auth:authComponent componentId="module.script.form.agentScript">	
				var fieldList = ['actualSurveyDate','survey','surveyTime','surveyTime2','initialContactDate'];
				enableFieldsForOriginAgent("${originAgentList}",fieldList,'originAgentCode');
				document.getElementById('survey_trigger').style.display = 'inline';
				document.getElementById('survey_trigger').src = 'images/calender.png';
				document.getElementById('actualSurveyDate_trigger').style.display = 'inline';
				document.getElementById('actualSurveyDate_trigger').src = 'images/calender.png';
				document.getElementById('initialContactDate_trigger').style.display = 'inline';
				document.getElementById('initialContactDate_trigger').src = 'images/calender.png';        
			</sec-auth:authComponent>
			document.getElementById('customerFileServiceAirAgentAccount').style.display = 'inline';
	  }
}

function trap(){ 
if(document.images){
    var totalImages = document.images.length;
    	for (var i=0;i<totalImages;i++){
				if(document.images[i].src.indexOf('calender.png')>0){
					var el = document.getElementById(document.images[i].id); 
					var strDate="0";
					<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
					strDate="1";
					</configByCorp:fieldVisibility>	
					if(el !=null && ((el.getAttribute("id")=='originalCompanyHiringDate_trigger') || (el.getAttribute("id")=='contractEnd_trigger') || (el.getAttribute("id")=='contractStart_trigger')) && strDate=="1" ){
						
					}else{
						document.images[i].src = 'images/navarrow.gif'; 
						if((el.getAttribute("id")).indexOf('trigger')>0){
							var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
							el.setAttribute("id",newId);
						}
				}
				}
				if(document.images[i].src.indexOf('open-popup.gif')>0){ 
					var str="0";
					<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
						str="1";
					</configByCorp:fieldVisibility>	
						var el = document.getElementById(document.images[i].id);
						if(el !=null && ((el.getAttribute("id")=='billToCode.popupImage') || (el.getAttribute("id")=='images/plus-small.png') ||(el.getAttribute("id")=='originalCompanyCode.popupImage') || (el.getAttribute("id")=='currentEmpCode.popupImage') || (el.getAttribute("id")=='originCompanyCode.popupImage') || (el.getAttribute("id")=='destCompanyCode.popupImage')) && str=="1"){ 
							
						}else{
						try{
						el.onclick = false;
						}catch(e){}
					    document.images[i].src = 'images/navarrow.gif';
						}
				}
				if(document.images[i].src.indexOf('quotes-to-go.png')>0){ 
					
					var el = document.getElementById(document.images[i].id);
					//alert(el)
					try{
					el.onclick = false;
					}catch(e){}
				    document.images[i].src = 'images/navarrow.gif';
			     }            
				if(document.images[i].src.indexOf('notes_empty1.jpg')>0){
						var el = document.getElementById(document.images[i].id);
						//alert(el)
						try{
						el.onclick = false;
						}catch(e){}
				        document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('notes_open1.jpg')>0){
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick =false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
				if(document.images[i].src.indexOf('images/nav')>0){
					var el = document.getElementById(document.images[i].id);
					try{
					el.onclick = false;
					}catch(e){}
					document.images[i].src = 'images/navarrow.gif';
				}
			}
			var elementsLen=document.forms['customerFileForm'].elements.length;
for(i=0;i<=elementsLen-1;i++){
		var element = document.forms['customerFileForm'].elements[i];
		if(element.type=='text'){
					element.readOnly =true;
					element.className = 'input-textUpper';
					element.onclick =false;
					element.onkeyup =false;
		}else{
				document.forms['customerFileForm'].elements[i].disabled=true;
		}
		if(document.forms['customerFileForm'].elements[i].type=='button' || document.forms['customerFileForm'].elements[i].type=='submit' || document.forms['customerFileForm'].elements[i].type=='reset')
		{
			document.forms['customerFileForm'].elements[i].style.display = 'none';
			var fieldSetArray = document.getElementsByTagName('fieldset');
			for(var j = 0 ; j < fieldSetArray.length ; j++)
					fieldSetArray[j].style.display = 'none'; 
		}						
	} 
  document.getElementById('customerFileServiceAirAgentAccount').style.display = 'block';
  }
  document.forms['customerFileForm'].setAttribute('bypassDirtyCheck',false);
  <configByCorp:fieldVisibility componentId="component.accportal.editableFields">
    
	document.forms['customerFileForm'].elements['customerFile.billToReference'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.billToReference'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.costCenter'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.costCenter'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.socialSecurityNumber'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.socialSecurityNumber'].className = 'input-text';
	document.getElementById('customerFileServiceAirAgentAccount').disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.serviceAir'].disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.serviceSurface'].disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.serviceAuto'].disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.serviceStorage'].disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.serviceDomestic'].disabled=false; 
	<configByCorp:fieldVisibility componentId="component.tab.customerFile.servicePovCheck">
	document.forms['customerFileForm'].elements['customerFile.servicePov'].disabled=false; 
	</configByCorp:fieldVisibility>
	document.forms['customerFileForm'].elements['customerFile.visaRequired'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.visaRequired'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.visaStatus'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.visaStatus'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.petsInvolved'].disabled=false; 
	document.forms['customerFileForm'].elements['customerFile.petType'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.petType'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.orderComment'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.orderComment'].className = 'input-text'; 
	<c:forEach var="entitlementMap" items="${entitlementMap}">
	var fieldName='chk_${entitlementMap.key}' 
	document.forms['customerFileForm'].elements[fieldName].disabled=false;
	</c:forEach>
	document.forms['customerFileForm'].elements['customerFile.localHr'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.localHr'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.localHrPhone'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.localHrPhone'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.localHrEmail'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.localHrEmail'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.authorizedBy'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.authorizedBy'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.authorizedPhone'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.authorizedPhone'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.authorizedEmail'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.authorizedEmail'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.assignmentEndReason'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.assignmentEndReason'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.duration'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.duration'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.costCenter'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.costCenter'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.assignmentType'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.assignmentType'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.otherContract'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.otherContract'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.homeCountry'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.homeCountry'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.homeState'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.homeState'].className = 'list-menu';
	document.forms['customerFileForm'].elements['customerFile.homeCity'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.homeCity'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.jobFunction'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.jobFunction'].className = 'input-text';
	document.forms['customerFileForm'].elements['customerFile.backToBackAssignment'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.backToBackAssignment'].className = 'list-menu'; 
	document.forms['customerFileForm'].elements['customerFile.entitled'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.entitled'].className = 'input-text'; 
	document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.originalCompanyCode'].className = 'input-text'; 
	document.forms['customerFileForm'].elements['customerFile.originalCompanyName'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.originalCompanyName'].className = 'input-text'; 
	document.forms['customerFileForm'].elements['customerFile.firstInternationalMove'].disabled=false;
	document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].className = 'input-text'; 
	document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].readOnly=false;
	document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].className = 'input-text'; 
	
	document.forms['customerFileForm'].elements['saveIkea'].style.display = 'block';
	document.forms['customerFileForm'].elements['saveIkea'].disabled=false;
	
</configByCorp:fieldVisibility>
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	  }	        
}
function enableStateListHome(){ 
	var homCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(homCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
	  }	        
}
</script>
<script language="JavaScript" type="text/javascript">
try{
	var str="0";
	<configByCorp:fieldVisibility componentId="component.field.forAllJob.showCordinator">
	str="1";
	</configByCorp:fieldVisibility>
	var el = document.getElementById('coordRequiredTrue');
	var el1 = document.getElementById('coordRequiredFalse');
	if(str=="1"){
		el.style.display = 'block';		
		el1.style.display = 'none';
	}else if(str=="0"){
		el.style.display = 'none';
		el1.style.display = 'block';
	}	
}catch(e){}
</script>
<script type="text/javascript">
	animatedcollapse.addDiv('address', 'fade=1,hide=1')
	animatedcollapse.addDiv('alternative', 'fade=1,hide=1')
	animatedcollapse.addDiv('billing', 'fade=1,hide=1')
	animatedcollapse.addDiv('entitlement', 'fade=1,hide=1')
	animatedcollapse.addDiv('crm', 'fade=1,hide=1')
	animatedcollapse.addDiv('cportal', 'fade=1,hide=1')
	animatedcollapse.addDiv('family', 'fade=1,hide=1')
	animatedcollapse.addDiv('benefits', 'fade=1,hide=1')
	animatedcollapse.addDiv('removal', 'fade=1,hide=1')
	animatedcollapse.addDiv('additional', 'fade=1,hide=1')
	animatedcollapse.init()
</script>
<script type="text/javascript">
try{
 <c:if test="${usertype=='ACCOUNT' || usertype=='AGENT' || usertype=='PARTNER'}">	
	trap() ;
 </c:if>
 <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
	///navigationVal();
	    trap();
	    document.getElementById("rateImage").style.display = "none"; 
	</sec-auth:authComponent>
}catch(e){}
try{
state();
document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
}
catch(e){}
</script>

<script type="text/javascript">
checkAccountCodeForAssignmentTypeOnlOad();
coordinatorRequiredStatusJob();
cheackVisaRequired(); 
checkAssignmentType();
zipCode();
	try{
	document.forms['customerFileForm'].elements['customerFile.statusDate'].select();
	}
	catch(e){}
	</script>
<script type="text/javascript">
	try{
	 }
	 catch(e){}
	 try{
 	if('${stateshitFlag}'!= "1"){
 	getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
 	 }
 	 }
	 catch(e){}
 	try{
	 }
	 catch(e){}
	try{
  	if('${stateshitFlag}'!= "1"){
  	getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);
  	 }  }
	 catch(e){}
  	try{
	<c:if test="${usertype!='AGENT'}">
	getContractONLOad();
	</c:if>
	 }
	 catch(e){}
	try{
	<c:if test="${customerFile.status=='CNCL' || customerFile.status=='CLOSED'}">
	document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
	</c:if>
	 }
	 catch(e){}
	try{
	<c:if test="${empty customerFile.id}">
	autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
	</c:if>
	 }
	 catch(e){}
	try{
	 cheackStatusReason();
	 }
	 catch(e){}
 </script>
<script type="text/javascript">
try{
checkColumn();
}
catch(e){}
try{
checkAllRelo();
}
catch(e){}
try{
checkPetInvolve(document.forms['customerFileForm'].elements['customerFile.petsInvolved']);
}
catch(e){}
function checkParentAgentForOrderinitiation(){
	if(document.getElementById('parentAgentForOrderinitiation').value=='true'){
	   document.getElementById('parentAgentForOrderinitiationDiv').style.display = 'block';
	   document.getElementById('entitlementDiv').style.display = 'none';
	}if(document.getElementById('parentAgentForOrderinitiation').value=='false'){
	   document.getElementById('parentAgentForOrderinitiationDiv').style.display = 'none';
	   document.getElementById('entitlementDiv').style.display = 'block';}
}
checkParentAgentForOrderinitiation();
	try{
	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off");
	}
	catch(e){}
	try{
	if('${stateshitFlag}'!= 1){
	getNewCoordAndSale('load');
	}}
	catch(e){}
	try{
 var ocountry=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
 }
 catch(e){}
 try{
 var dcountry=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
 }
 catch(e){}
 try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	 if(oriCon=="")	 {
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('originState').disabled = false; 
				document.getElementById('originState').value='${customerFile.originState}';
			}else{
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";
			} 
		}
	 }
catch(e){}
try{
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	 var enbState = '${enbState}';
	 if(desCon=="")	 {
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
	 }else{
	  		if(enbState.indexOf(desCon)> -1){
				document.getElementById('destinationState').disabled = false;		
				document.getElementById('destinationState').value='${customerFile.destinationState}';
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";
			}
	 	}
	}
catch(e){}	
try{
	var desCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	 var enbState = '${enbState}';
	 if(desCon=="")	 {
			document.getElementById('homeState').disabled = true;
			document.getElementById('homeState').value ="";
	 }else{
	  		if(enbState.indexOf(desCon)> -1){
				document.getElementById('homeState').disabled = false;		
				document.getElementById('homeState').value='${customerFile.homeState}';
			}else{
				document.getElementById('homeState').disabled = true;
				document.getElementById('homeState').value ="";
			}
	 	}
	}
catch(e){}
function billtoCheck(){
	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var billToName = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
    if(billToCode!=''){
    	showOrHide(1)
	}
	getInsurance();
	changeStatus();
	<c:choose>
	<c:when test="${hasDataSecuritySet}">
	findOrderInitiationBillToCode();
	</c:when>
	<c:otherwise>
	findBillToName();
	</c:otherwise>
	</c:choose>
	getContract();
	findPricingBillingPaybaleBILLName();
}	
function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} }    
showOrHide(0);
</script>
<script type="text/javascript">
    var fieldName = document.forms['customerFileForm'].elements['field'].value;
    var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
		animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('crm', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('family', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('benefits', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('removal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
		animatedcollapse.init()
	}
	if(fieldName1!=''){
		document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
		animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('crm', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('family', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('benefits', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('removal', 'fade=0,persist=0,show=1')
		animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
		animatedcollapse.init()
	}
 </script>
 
<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();
	 <c:if test="${usertype=='ACCOUNT' || usertype=='AGENT' || usertype=='PARTNER'}">	
		 iehack();
 	 </c:if>
	function executeTrap(){
		 <c:if test="${usertype=='ACCOUNT' || usertype=='AGENT' || usertype=='PARTNER'}">	
			 trap();
		 </c:if>
		 <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
			///navigationVal();
			    trap();
			    document.getElementById("rateImage").style.display = "none"; 
			   
			</sec-auth:authComponent>
	}
	function setVipImageReset(){
			if('${customerFile.vip}'.toString() == 'true')
				document.getElementById('customerFile.vip').checked = true;	
			else
				document.getElementById('customerFile.vip').checked = false;	
			setVipImage();
	}
	function setVipImage(){
		var imgElement = document.getElementById('vipImage');
		if(document.getElementById('customerFile.vip').checked == true){
			imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
			imgElement.style.display = 'block';
		}
		else{
			if('${customerFile.vip}' == 'true' && document.getElementById('customerFile.vip').checked == true){
				imgElement.src = '${pageContext.request.contextPath}'+'/images/vip_icon.png';
				imgElement.style.display = 'block';
			}
			else{
				imgElement.src = '';
				imgElement.style.display = 'none';
			}
		}
	}
	window.onload = setVipImage();
	if (window.addEventListener)
		 window.addEventListener("load", function(){
			 		executeTrap();
		 }, false)
	var fieldList = ['actualSurveyDate','survey','surveyTime','surveyTime2','initialContactDate'];
<sec-auth:authComponent componentId="module.script.form.agentScript">	
if (window.addEventListener)
	 window.addEventListener("load", function(){
		 		enableFieldsForOriginAgent("${originAgentList}",fieldList,'originAgentCode');
	 }, false)

function enableElements(){
	var isValid = (IsValidTime('save')==false)?false:true;
	if(isValid){
		var elementList = document.getElementsByTagName('*');
		for(var i = 0; i < elementList.length; i++){
			if(elementList[i].type == 'text' || elementList[i].type == 'textarea'){
				elementList[i].readOnly=false;
			}
			if(elementList[i].type == 'select-one'){
				elementList[i].disabled = false;
			}
			else{
				elementList[i].disabled = false;
			}
		}
	}
}
function enableFieldsForOriginAgent(agentList,fieldList,fieldToValidate){
	if((agentList != null || agentList != undefined) && agentList != ''){
		var originAgentList = agentList.split(',');
		for(var i = 0; i < originAgentList.length; i++){
			var agentCode = originAgentList[i].replace("'","").replace("'","");
			var validateField = document.getElementById(fieldToValidate);
			if(validateField.value != '' &&  agentCode != '' && validateField.value == agentCode ){
				for(var j = 0; j < fieldList.length; j++){
					document.getElementById(fieldList[j]).readOnly = false;
					document.getElementById(fieldList[j]).className = 'input-text';
					validateField.readOnly = true;
					var inputType = document.getElementsByTagName('input');
					for(var k=0;k<inputType.length;k++){
						if(inputType[k].type == 'submit' || inputType[k].type == 'reset'){
							inputType[k].style.display = 'inline';
							inputType[k].disabled = false;
						}
					}
					if(document.getElementById(fieldList[j]+'_trigge') != null){
						document.getElementById(fieldList[j]+'_trigge').src = 'images/calender.png';
						document.getElementById(fieldList[j]+'_trigge').setAttribute("id",fieldList[j]+'_trigger');
					}
				}
			}
		}
		 var fieldName = document.forms['customerFileForm'].elements['field'].value;
		  var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
 if(fieldName!=''){
				document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
				animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('crm', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('family', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('benefits', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('removal', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
				animatedcollapse.init()
			}
			if(fieldName1!=''){
				document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
				animatedcollapse.addDiv('address', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('alternative', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('billing', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('entitlement', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('crm', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('cportal', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('family', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('benefits', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('removal', 'fade=0,persist=0,show=1')
				animatedcollapse.addDiv('additional', 'fade=0,persist=0,show=1')
				animatedcollapse.init()
			}
	}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
}
</sec-auth:authComponent>	

function clickOnRequest(){
	document.getElementById("customerFileForm_button_save").click();
}
function setAssignmentReset(){
	setTimeout("checkAccountCodeForAssignmentType()",500);
}
var r1={
		 'quotes':/['\"'&'\\']/g
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
<script type="text/javascript">
try{	
	getInsuranceOnLoad();	
	var moveTypeVal = '${customerFile.moveType}';	
	 <c:if test='${moveTypeFlag == "Y" }'>
	 if(moveTypeVal=='Quote'){
		 document.getElementById('Quote').checked = true;
		 document.getElementById('BookedMove').checked = false;
	 }else{
		 document.getElementById('Quote').checked = false;
		 document.getElementById('BookedMove').checked = true;
	 }
	</c:if>
	showQuotationStatus(moveTypeVal,'L');
	var quotationStatus= document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;	
	if(quotationStatus=='Rejected' || quotationStatus=='Cancelled'){
		if(document.getElementById('statusReasonId')!=undefined){
		document.getElementById('statusReasonId').style.display="block";
		}
		if(document.getElementById('statusReasonId1')!=undefined){
		document.getElementById('statusReasonId1').style.display="block";
		}
	}else{
		if(document.getElementById('statusReasonId')!=undefined){
		document.getElementById('statusReasonId').style.display="none";
		}
		if(document.getElementById('statusReasonId1')!=undefined){
		document.getElementById('statusReasonId1').style.display="none";
		}
	}
}catch(e){}
</script>
<script type="text/javascript">
function updateAllSo(){
	var cordName = document.forms['customerFileForm'].elements['customerFile.coordinator'].value;
	var custid = '${customerFile.id}';	
	if(cordName!=''){
		var type="Would you like to update coordinator in all SO's.";
	 	var agree = confirm(type+"\n"+"Click Ok to continue or Click Cancel.");
		if(agree){
			var url="updateCoordNameInAllSoAjax.html?ajax=1&decorator=simple&popup=true&custid=${customerFile.id}&cordName="+cordName;
		    http511.open("GET", url, true); 
		   	http511.onreadystatechange = handleHttp901; 
		   	http511.send(null);	     
		}else{
			return false;
 		}	
	}else{
		alert('Please select coordinator to continue.');
	}
}
function handleHttp901(){
if (http511.readyState == 4){ 
	var results = http511.responseText
   }
}
var http511 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
if (!xmlhttp)
{
    xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
}
}
return xmlhttp;
}
</script>
<script type="text/javascript">
function autoCompleterAjaxCallOriCity(){
var stateNameOri = document.forms['customerFileForm'].elements['customerFile.originState'].value;
var countryName = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
var cityNameOri = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
var countryNameOri = "";
<c:forEach var="entry" items="${countryCod}">
if(countryName=="${entry.value}"){
	countryNameOri="${entry.key}";
}
</c:forEach>	
if(cityNameOri!=''){
	var data = 'leadCaptureAutocompleteOriAjax.html?ajax=1&stateNameOri='+stateNameOri+'&countryNameOri='+countryNameOri+'&cityNameOri='+cityNameOri+'&decorator=simple&popup=true';
		$("#originCity").autocomplete({
			source: data	
		});		
}
}
function autoCompleterAjaxCallDestCity(){
	var stateNameDes = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	var countryName = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var cityNameDes = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var countryNameDes = "";
	<c:forEach var="entry" items="${countryCod}">
	if(countryName=="${entry.value}"){
		countryNameDes="${entry.key}";
	}
	</c:forEach>
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#destinationCity" ).autocomplete({				 
	      source: data		      
	    });
	}
}

function calendarICSFunction(){	
	 var str="0";
	<configByCorp:fieldVisibility componentId="component.field.prefSurveyDate.showCorpId">
		str="1";
	</configByCorp:fieldVisibility>
	var cfId = '${customerFile.sequenceNumber}';
	var surveyType = 'CF';
    var surveyDateCF = document.forms['customerFileForm'].elements['customerFile.survey'].value;
    var consultantName = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
    var surveyTimeFrom = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
    var surveyTimeTo = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
    if(str=="0"){
	    if(cfId==''){
	    	alert('Please save Customer File to continue.');
	    }else if(consultantName==''){
	    	alert('Please select Consultant to continue.');
	    }else if(surveyDateCF==''){
	    	alert('Please select Survey date to continue.');
	    }else if(surveyTimeFrom==''){
	    	alert('Please fill Survey From Time to continue.');
	    }else if(surveyTimeTo==''){
	        	alert('Please fill Survey To Time to continue.');
	    }else{
	 		var url = "appointmentICSCF.html?cfId="+cfId+"&consultantName="+consultantName+"&surveyDateCF="+surveyDateCF+"&surveyTimeFrom="+surveyTimeFrom+"&surveyTimeTo="+surveyTimeTo+"&surveyType="+surveyType+"";
	    	location.href=url;
	    	}
    	}else{
    	    var surveyDate1CF = document.forms['customerFileForm'].elements['customerFile.prefSurveyDate1'].value;
    	    var surveyDate2CF = document.forms['customerFileForm'].elements['customerFile.prefSurveyDate2'].value;
    	    var prefSurveyTime1 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
    	    var prefSurveyTime2 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
    	    var prefSurveyTime3 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
    	    var prefSurveyTime4 = document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
    		 if(cfId==''){
    		    	alert('Please save Customer File to continue.');
    		    }else if(consultantName==''){
    		    	alert('Please select Consultant to continue.');
    		    }else if(surveyDateCF==''){
    		    	alert('Please select Survey date to continue.');
    		    }else if(surveyTimeFrom==''){
    		    	alert('Please fill Survey From Time to continue.');
    		    }else if(surveyTimeTo==''){
    		        	alert('Please fill Survey To Time to continue.');    		    
    		    }else{
    		 		var url = "appointmentICSCF.html?cfId="+cfId+"&consultantName="+consultantName+"&surveyDateCF="+surveyDateCF+"&surveyTimeFrom="+surveyTimeFrom+"&surveyTimeTo="+surveyTimeTo+"&surveyType="+surveyType+"" +
    		 				"&surveyDate1CF="+surveyDate1CF+"&surveyDate2CF="+surveyDate2CF+"&prefSurveyTime1="+prefSurveyTime1+"&prefSurveyTime2="+prefSurveyTime2+"&prefSurveyTime3="+prefSurveyTime3+"&prefSurveyTime4="+prefSurveyTime4+"";
    		    	location.href=url;
    		    	}
    	}
 	}
</script>
<script language="JavaScript" type="text/javascript">
window.onload = function() {
	try{
		var tem1="";
		var tem2="";
		var tem3="";
		var tem4="";
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1']!=undefined){
		 tem1=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value;
		}
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2']!=undefined){
		 tem2=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value;
		}
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3']!=undefined){
		 tem3=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value;
		}
		if(document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4']!=undefined){
		 tem4=document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value;
		}

		if(tem1=='' && document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1']!=undefined)
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime1'].value='00:00';
		}
		if(tem2=='' && document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2']!=undefined )
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime2'].value='00:00';	
		}
		if(tem3=='' && document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3']!=undefined)
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime3'].value='00:00';
			}
		if(tem4=='' && document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4']!=undefined)
		{
			document.forms['customerFileForm'].elements['customerFile.prefSurveyTime4'].value='00:00';
		}
	}catch(e){}
}
</script>
<script language="JavaScript" type="text/javascript">
function sendToMOL(target){
	var seqNum = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var lastName = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var subject = "";
	if(target=='Starline'){
		subject = "Starline MovesOnline PIN Quote Link"; 
	}else{
		subject="Highland MovesOnline PIN Quote Link";
	}
	var body = "Please follow the link below to fill out your household inventory using MovesOnline:<br/><br/>";
	
	var agree= confirm("Press OK to Send Email to "+target+" MOL, else press Cancel.");	  	   
    if(agree){
    	var url="sendMOLMail.html?sequenceNumber="+seqNum+"&email="+email+"&lastName="+lastName+"&subject="+subject+"&body="+body+"&filter="+target+"&decorator=simple&popup=true";
    	httpVoxme.open("GET", url, true);
		httpVoxme.onreadystatechange=httpMOLMailSender;
		httpVoxme.send(null);
   	}else {             
	    }
}

function httpMOLMailSender(){
	if (httpVoxme.readyState == 4){
		      var results = httpVoxme.responseText
	           results = results.trim();
	           if(results=='sent'){
	        	}else{
	        	}
	        }
	}

function findCoordinatorByBA(){
	var cfContractType="${customerFile.contractType}";
	var checkUTSI="${company.UTSI}";
    var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var bookAgCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    var checkSoFlag = false;
	if(custJobType!='' && bookAgCode!=''){
	$.ajax({
        type: "POST",
        url: "checkNetworkCoordinatorForCf.html?ajax=1&decorator=simple&popup=true",
        data: {custJobType:custJobType,cfContractType:cfContractType,checkUTSI:checkUTSI,bookAgCode:bookAgCode,checkSoFlag:checkSoFlag},
        success: function (data, textStatus, jqXHR) {
           results = data.trim();
           res = results.split("~");
         	targetElement = document.forms['customerFileForm'].elements['customerFile.coordinator'];
          	targetElement.length = res.length;               	
			document.forms['customerFileForm'].elements['customerFile.coordinator'].options[0].text = '';
			document.forms['customerFileForm'].elements['customerFile.coordinator'].options[0].value = '';
			var found=false;
			 var tempCoord=document.forms['customerFileForm'].elements['customerFile.coordinator'].value;
			 var j=1;
			for(i=0;i<res.length;i++){
				var stateVal;
					if(res[i] != '' && res[i].split("#").length>1 && res[i].split("#")[0]!=undefined && res[i].split("#")[0]!=null && res[i].split("#")[0]!=''){
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.coordinator'].options[j].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.coordinator'].options[j].value = stateVal[0];
					j++;
					}
					if(!found && stateVal!=undefined && (stateVal[0]==tempCoord)){
						found=true;
					}
			}
			if(found){
				document.forms['customerFileForm'].elements['customerFile.coordinator'].value=tempCoord;
			}else{
				document.forms['customerFileForm'].elements['customerFile.coordinator'].value="";
			}
        },
      error: function (data, textStatus, jqXHR) {
              }
      });
}else{
	document.forms['customerFileForm'].elements['customerFile.coordinator'].value='';
}}

	hideAddressDetails();
	function hideAddressDetails(){
	<c:if test="${usertype=='AGENT'}">	
		 var img =  document.getElementById("viewDetails");
		 img.style.visibility = 'hidden';
		 </c:if>
		}
	function viewPartnerDetails(position,codeId) {
	    var partnerCode = "";
	    var originalCorpID='${customerFile.corpID}';
	    partnerCode =  document.getElementById(codeId).value;
	    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
	    ajax_showTooltip(url,position);
	    return false
	}
function viewPartnerDetailsForBillToCode(position) {
    var partnerCode = "";
    var originalCorpID='${customerFile.corpID}';
    partnerCode =  document.getElementById("billToCodeId").value;
    var url="viewPartnerDetailsForBillTo.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+"&sessionCorpID=" + encodeURI(originalCorpID);
    ajax_showTooltip(url,position);
    return false
}


function fillAccountFlag(){
	var billtoCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var bookingAgentCode=document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	if(billtoCode !='' && bookingAgentCode!=''){
		var url="checkPrivateParty.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(billtoCode)+"&bookingAgentCode="+encodeURI(bookingAgentCode);		
		http221.open("GET", url, true);
		http221.onreadystatechange = httpPrivatepartyCheck;
		http221.send(null);		
	}  }
function httpPrivatepartyCheck(){if (http221.readyState == 4){
    var results = http221.responseText
    results = results.trim();             
    var res = results.replace("[",'');
    res = res.replace("]",'');
    res=res.split("~");
    var privateCheck=res[0].replace(",",'');
    //alert("1-"+res)
    //alert("2-"+res[1])
    //alert("3-"+privateCheck)
    var bookingAgentCode=document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if((res!=undefined || res!=null) && res.length>1){
    	if((privateCheck=='1' || privateCheck==1)){
    		
    			document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value=true;
        		document.forms['customerFileForm'].elements['accountCodeValidationFlag'].value='Y';
        		if(document.getElementById('accountValidation')!=null || document.getElementById('accountValidation')!=undefined){
        		var el = document.getElementById('accountValidation');
        		el.style.visibility='hidden';
    		}
    		
    	}
    	if((privateCheck=='0' || privateCheck==0)){
    		if(res[1]==bookingAgentCode){
    			if(document.getElementById('accountValidation')!=null || document.getElementById('accountValidation')!=undefined){
    				var el = document.getElementById('accountValidation');
    				el.style.visibility='visible';
    				}	
    		
    		document.forms['customerFileForm'].elements['accountCodeValidationFlag'].value='N';
    		document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value=false;
    		}else{
    			document.forms['customerFileForm'].elements['isPrivatePartyFlag'].value=true;
        		document.forms['customerFileForm'].elements['accountCodeValidationFlag'].value='Y';
        		if(document.getElementById('accountValidation')!=null || document.getElementById('accountValidation')!=undefined){
        		var el = document.getElementById('accountValidation');
        		el.style.visibility='hidden';
        		}
    		}
    			
    	}
    	
    	
    	else{
    		
    	}
    }
  }    }
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
function fillAccountFlagReset(){
	var billtoCode='${customerFile.billToCode}';
	var bookingAgentCode='${customerFile.bookingAgentCode}';
	if(billtoCode !='' && bookingAgentCode!=''){
		var url="checkPrivateParty.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(billtoCode)+"&bookingAgentCode="+encodeURI(bookingAgentCode);		
		http221.open("GET", url, true);
		http221.onreadystatechange = httpPrivatepartyCheck;
		http221.send(null);		
	} 
}
</configByCorp:fieldVisibility>
<c:if test="${not empty customerFile.id}">
<configByCorp:fieldVisibility componentId="component.field.accountCode.Validation">
fillAccountFlag();
</configByCorp:fieldVisibility>
</c:if>

var dmmTypeContractHTTP = getHTTPObjectDMMtypeContract();
var dmmTypeContractHTTP1 = getHTTPObjectDMMtypeContract1();
function findDMMTypeAccountCode(){
	var networkBillToCode = document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
	if(networkBillToCode!=''){
		<c:if test="${cmmDmmAgent}">
		var networkContractType = document.forms['customerFileForm'].elements['customerFile.contractType'].value;
		var contracts = document.forms['customerFileForm'].elements['customerFile.contract'].value;
		if(networkContractType!=null && networkContractType!='' && networkContractType=='DMM'){
			checkNetworkAccountName();
		}else if(contracts!=null && contracts!=''){
			    var url="findDMMTypeContract.html?ajax=1&decorator=simple&popup=true&contracts=" + encodeURI(contracts);
			    dmmTypeContractHTTP.open("GET", url, true);
			    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseDmmTypeContract;
			    dmmTypeContractHTTP.send(null);
		}else{
			findAccToName();
		}
	    </c:if>
	    <c:if test="${!cmmDmmAgent}">
	    	findAccToName();
	    </c:if>
	}else{
		document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
	}
}
function handleHttpResponseDmmTypeContract(){
     if (dmmTypeContractHTTP.readyState == 4) {
        var results = dmmTypeContractHTTP.responseText
        results = results.trim(); 
        if(results.length>1){ 
	       	 if(results=='TRUE'){
	       		checkNetworkAccountName();
	       	 }else{
	       		findAccToName();
	       	 }
        }else{
        	findAccToName();
        }
     }
}

function checkNetworkAccountName(){ 
	var networkBillToCode = document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
	networkBillToCode=networkBillToCode.trim();
    if(networkBillToCode==''){
      document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
    }
	if(networkBillToCode!=''){
	    var url="findNetworkBillToName.html?ajax=1&decorator=simple&popup=true&networkPartnerCode=''&networkBillToCode=" + encodeURI(networkBillToCode);
	    dmmTypeContractHTTP.open("GET", url, true);
	    dmmTypeContractHTTP.onreadystatechange = handleHttpResponseNetworkBillToName;
	    dmmTypeContractHTTP.send(null);
    }
}
function handleHttpResponseNetworkBillToName(){
	if (dmmTypeContractHTTP.readyState == 4){
        var results = dmmTypeContractHTTP.responseText
        results = results.trim();
        var res = results.split("#"); 
        if(res.length >= 2){ 
        		if(res[2] == 'Approved'){
        			document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[1];
        		}else{
        			alert("Account code is not approved as per pricing contract" ); 
        			document.forms['customerFileForm'].elements['customerFile.accountCode'].value=""; 
        			document.forms['customerFileForm'].elements['customerFile.accountName'].value=""; 
        		}			
        }else{
            alert("Account code not valid as per pricing contract");
            document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
            document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
		   }  } 
}
function getHTTPObjectDMMtypeContract(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObjectDMMtypeContract1(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
function reseEmails(agentType,str){
	str=str.trim();
	if(str.length == 0){
		resetContactEmail(agentType);
	}  
		var con=document.forms['customerFileForm'].elements['customerFile.originAgentContact'].value ;
		con=con.trim(); 
		var email=document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value ;
		var pcode=document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value ;
		if(con!=null &&  con.length>0){			
			var url="chkPartnerAgentRUC.html?ajax=1&decorator=simple&popup=true&partCode="+pcode+"&conEmail="+email+"&contactName="+con;
			http234.open("GET", url, true);
			http234.onreadystatechange = handleHttpchkPartnerAgentRUC;
			http234.send(null);
		}else{
			document.getElementById("hidActiveAP").style.display="none";
			document.forms['customerFileForm'].elements['customerFile.originAgentContactImgFlag'].value='';
		} } 
function handleHttpchkPartnerAgentRUC(){ 
	if (http234.readyState == 4){
		var results = http234.responseText
        results = results.trim();		
			if(results=='AG'){	
				document.getElementById("hidActiveAP").style.display="block";	
				document.forms['customerFileForm'].elements['customerFile.originAgentContactImgFlag'].value='true';
			}else{ 
				document.getElementById("hidActiveAP").style.display="none";
				document.forms['customerFileForm'].elements['customerFile.originAgentContactImgFlag'].value='';	
			}  } }
function checkEmail(fieldId) {
	var email = document.forms['customerFileForm'].elements[fieldId];
	var filter = /^([a-zA-Z0-9_\.\-\'])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email.value)) {
			alert('Please provide a valid email address');
			document.forms['customerFileForm'].elements[fieldId].value='';
			document.forms['customerFileForm'].elements[fieldId].focus();
			return false;
		} }
function getContactInfo(agentType,partnerCode){
	 if(agentType.length > 0 && partnerCode.length >0){
	 openWindow('userCustomerContacts.html?agentType='+agentType+'&partnerCode='+partnerCode+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=customerFile.originAgentEmail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=customerFile.originAgentVanlineCode&fld_description=customerFile.originAgent&fld_code=customerFile.originAgentCode',1000,550);
}}  

function showAddressImage(){
    var agentCode=document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value; 
    if(agentCode==''){ 
    
    document.getElementById("hidImagePlus").style.display="none";
    document.forms['customerFileForm'].elements['customerFile.originAgentContact'].value='';
    document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value='';
    document.forms['customerFileForm'].elements['customerFile.originAgentPhoneNumber'].value='';
   
    } else if(agentCode!='') {
     document.getElementById("hidImagePlus").style.display="block";
    } }

function resetContactEmail(agentType){
	if(agentType == 'OA'){
		document.forms['customerFileForm'].elements['customerFile.originAgentContact'].value = '';
		document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value = '';
		document.forms['customerFileForm'].elements['customerFile.originAgentPhoneNumber'].value = '';
		
	}}
try{
	var str="0";
	<configByCorp:fieldVisibility componentId="component.contract.reloJob.tenancyBilling">
		str="1";
	</configByCorp:fieldVisibility>
	var el = document.getElementById('competitiveRequiredTrue');
	var el1 = document.getElementById('competitiveRequiredFalse');
	if(str=="1"){
		el.style.display = 'block';		
		el1.style.display = 'none';
	}else if(str=="0"){
		el.style.display = 'none';
		el1.style.display = 'block';
	}	
}catch(e){}
function getCustomerContactInfo()
{      
        var originAgentCode = document.forms['customerFileForm'].elements['customerFile.originAgentCode'].value; 
	    var url="getContactInfo.html?ajax=1&decorator=simple&popup=true&originAgentInfo="+originAgentCode;
	    http7.open("GET", url, true);
	    http7.onreadystatechange =   handleHttpResponseDefaultContact;
        http7.send(null);
}
function handleHttpResponseDefaultContact() {  
	
	  if (http7.readyState == 4) {
		
		       var results = http7.responseText ;  
	           results = results.trim();   
               var res = results.split("~"); 
            if(res.length>1 )
          	   {
              if(res[0]!=undefined &&  res[0]!="undefined" && res[0] !=""  ){
              	   document.forms['customerFileForm'].elements['customerFile.originAgentContact'].value = res[0];
         		}if(res[1]!=undefined &&  res[1]!="undefined" &&  res[1]!="" ){
              	   document.forms['customerFileForm'].elements['customerFile.originAgentEmail'].value = res[1];
         		} 
         		if(res[2]!=undefined &&  res[2]!="undefined" && res[2]!=""){
              	   document.forms['customerFileForm'].elements['customerFile.originAgentPhoneNumber'].value = res[2];
         		}
         		}
          	   
	       	}}	
	
	var http7 = getHTTPObjectst();
	function getHTTPObjectst() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
      xmlhttp = new XMLHttpRequest();
  }
  else if (window.ActiveXObject)  {
      xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      if (!xmlhttp)  {
          xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
      }   } 
  return xmlhttp;
} 
	function opencurrentEmployeeCodePopUp(){
		var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
		var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
		var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
		var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
		var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
		var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
		var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
		var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
		var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
		var compDiv = '';
		<c:if  test="${companies == 'Yes'}">
		compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
		</c:if>
		var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
	   	orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
	   
		var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
		var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
		var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
		var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
		var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
		var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
		var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
		destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
		
		javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.currentEmploymentName&fld_code=customerFile.currentEmploymentCode');
	}
	function openOriginCompanyCodePopUp(){
		var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
		var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
		var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
		var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
		var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
		var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
		var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
		var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
		var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
		var compDiv = '';
		<c:if  test="${companies == 'Yes'}">
		compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
		</c:if>
		var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
	   	orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
	   
		var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
		var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
		var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
		var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
		var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
		var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
		var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
		destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
		<c:if test="${!hasOADADataSecuritySet}"> 
		javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originCompany&fld_code=customerFile.originCompanyCode');
		</c:if>
		<c:if test="${hasOADADataSecuritySet}"> 
		 window.openWindow('openOrderInitiationBillToCode.html?OADAAccountPortalValidation=yes&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.originCompany&fld_code=customerFile.originCompanyCode');
		</c:if>
	}
	function openDestinationCompanyCodePopUp(){
		var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
		var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
		var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
		var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
		var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
		var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
		var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
		var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
		var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
		var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
		var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
		var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
		var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
		var compDiv = '';
		<c:if  test="${companies == 'Yes'}">
		compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
		</c:if>
		var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
	   	orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
	   
		var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
		var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
		var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
		var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
		var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
		var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
		var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
		var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
		var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
		var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
		destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+compDiv+'~'+status;
		<c:if test="${!hasOADADataSecuritySet}"> 
		javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.destinationCompany&fld_code=customerFile.destinationCompanyCode');
		</c:if>
		<c:if test="${hasOADADataSecuritySet}"> 
		window.openWindow('openOrderInitiationBillToCode.html?OADAAccountPortalValidation=yes&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.destinationCompany&fld_code=customerFile.destinationCompanyCode');
		</c:if>
	}
	function findOriginCompanyName(){
    var originCompanyCode = document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value;
    if(originCompanyCode==''){
    	document.forms['customerFileForm'].elements['customerFile.originCompany'].value="";
    }
    if(originCompanyCode!=''){
    	showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(originCompanyCode);
     http66.open("GET", url, true);
     http66.onreadystatechange = handleHttpResponse47;
     http66.send(null);
    } 
}
function handleHttpResponse47(){
		if (http66.readyState == 4) {
              var results = http66.responseText
              results = results.trim();
              var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['customerFileForm'].elements['customerFile.originCompany'].value = res[1];
                   		document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].select();
                   		copyCompanyToDestination();
                	}else{
                		showOrHide(0);
	           			alert("Origin Company code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.originCompany'].value="";
					 document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Origin Company code not valid");
                     document.forms['customerFileForm'].elements['customerFile.originCompany'].value="";
					 document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].select();
               }
      }
}    
	function findDestinationCompanyName(){
    var destinCompanyCode = document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value;
    if(destinCompanyCode==''){
    	document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value="";
    }
    if(destinCompanyCode!=''){
    	showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(destinCompanyCode);
     http77.open("GET", url, true);
     http77.onreadystatechange = handleHttpResponse478;
     http77.send(null);
    } 
}
function handleHttpResponse478(){
		if (http77.readyState == 4) {
              var results = http77.responseText
              results = results.trim();
              var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = res[1];
                   		document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Destination Company code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value="";
					 document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Destination Company code not valid");
                     document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value="";
					 document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].select();
               }
      }
}  
	function findCureentEmployName(){
    var currentEmployCode = document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value;
    if(currentEmployCode==''){
    	document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value="";
    }
    if(currentEmployCode!=''){
    	showOrHide(1)
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(currentEmployCode);
     http88.open("GET", url, true);
     http88.onreadystatechange = handleHttpResponse4789;
     http88.send(null);
    } 
}
function handleHttpResponse4789(){
		if (http88.readyState == 4) {
              var results = http88.responseText
              results = results.trim();
              var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
                		showOrHide(0);
	           			document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value = res[1];
                   		document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].select();
                	}else{
                		showOrHide(0);
	           			alert("Current Employment code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].select();
	           		}  
                }else{
                	showOrHide(0);
                     alert("Current Employment code not valid");
                     document.forms['customerFileForm'].elements['customerFile.currentEmploymentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.currentEmploymentCode'].select();
               }
      }
}  
function findDestinationName(){
    var destinCompany = document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value;
    if(destinCompany==''){
    	document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].value="";
    }}
function findOriginName(){
    var originCompany = document.forms['customerFileForm'].elements['customerFile.originCompany'].value;
    if(originCompany==''){
    	document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].value="";
    }}
<configByCorp:fieldVisibility componentId="component.accportal.editableFields">
function enableAccPortalElements(){
   var isValid = (IsValidTime('save')==false)?false:true;
	if(isValid){
		var elementList = document.getElementsByTagName('*');
		for(var i = 0; i < elementList.length; i++){
			if(elementList[i].type == 'text' || elementList[i].type == 'textarea'){
				elementList[i].readOnly=false;
			}
			if(elementList[i].type == 'select-one'){
				elementList[i].disabled = false;
			}
			else{
				elementList[i].disabled = false;
			}
		}
	}else{
		return false; 
	}
	
	
}
</configByCorp:fieldVisibility>
</script>
