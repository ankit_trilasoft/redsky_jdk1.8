<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Tool Tip</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>
<display:table name="driverList" class="table" requestURI="" id="driverList" >
<c:set var="partnerName" value="${driverList.partnerName}"/>
    <display:column title="Partner&nbsp;Code"><a onclick='goToUrlAccDetails("${driverList.partnerCode}","${fn:escapeXml(partnerName)}"),ajax_hideTooltip();' ><c:out value="${driverList.partnerCode}" /></a></display:column>
    <display:column title="Partner&nbsp;Name"><c:out value="${driverList.partnerName}" /></display:column>
</display:table>
