<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Update Additional Info</title>   
    <meta name="heading" content="Update Additional Info"/> 
<style>
 

 </style>
<script type="text/javascript">
function closeWindow()
{
	self.close();
}
<c:if test="${hitFlag=='1'}">
pick();
</c:if>
function pick() {	
		parent.window.opener.document.location.reload();
		window.close();
   }

function checkRequredField(){
	var clickType=document.forms['emailConfigPopup'].elements['configType'].value;
	if(clickType=='surveyfrom'){
		return checkRequiredFieldForSurvey();
	}else if(clickType=='recipient'){
		return checkRequiredFieldForRecipient();
	}else if(clickType=='subject'){
		return checkRequiredFieldForSubject();
	}else if(clickType=='body'){
		return checkRequiredFieldForBody();
	}else if(clickType=='signature'){
		return checkRequiredFieldForSignature();
	}else{
		return true;
	}
}
function checkRequiredFieldForSurvey(){
	 var surveyId = document.forms['emailConfigPopup'].elements['surveyId'].value;
	 var surveyName = document.forms['emailConfigPopup'].elements['surveyName'].value;
	 var surveyUrl = document.forms['emailConfigPopup'].elements['surveyUrl'].value;
	 var jobId = document.forms['emailConfigPopup'].elements['jobId'].value;
	 var transferre = document.forms['emailConfigPopup'].elements['transferre'].value;
	 var recommend = document.forms['emailConfigPopup'].elements['recommend'].value;
	 var destination = document.forms['emailConfigPopup'].elements['destination'].value;
	 var origin = document.forms['emailConfigPopup'].elements['origin'].value;
	 var overall = document.forms['emailConfigPopup'].elements['overall'].value;
	 var premove = document.forms['emailConfigPopup'].elements['premove'].value;
	 var jobIdType = document.forms['emailConfigPopup'].elements['jobIdType'].value;
	 var jobIdSize = document.forms['emailConfigPopup'].elements['jobIdSize'].value;
	 var transferreType = document.forms['emailConfigPopup'].elements['transferreType'].value;
	 var transferreSize = document.forms['emailConfigPopup'].elements['transferreSize'].value;
	 var recommendType = document.forms['emailConfigPopup'].elements['recommendType'].value;
	 var recommendSize = document.forms['emailConfigPopup'].elements['recommendSize'].value;
	 var destinationType = document.forms['emailConfigPopup'].elements['destinationType'].value;
	 var destinationSize = document.forms['emailConfigPopup'].elements['destinationSize'].value;
	 var originType = document.forms['emailConfigPopup'].elements['originType'].value;
	 var originSize = document.forms['emailConfigPopup'].elements['originSize'].value;
	 var overallType = document.forms['emailConfigPopup'].elements['overallType'].value;
	 var overallSize = document.forms['emailConfigPopup'].elements['overallSize'].value;
	 var premoveType = document.forms['emailConfigPopup'].elements['premoveType'].value;
	 var premoveSize = document.forms['emailConfigPopup'].elements['premoveSize'].value;

	 var coordinator = document.forms['emailConfigPopup'].elements['coordinator'].value;
	 var coordinatorType = document.forms['emailConfigPopup'].elements['coordinatorType'].value;
	 var coordinatorSize = document.forms['emailConfigPopup'].elements['coordinatorSize'].value;
	 
	 var nps = document.forms['emailConfigPopup'].elements['nps'].value;
	 var npsType = document.forms['emailConfigPopup'].elements['npsType'].value;
	 var npsSize = document.forms['emailConfigPopup'].elements['npsSize'].value;

	 if(surveyId==''){
			alert("Please Select Survey Id");
			return false;
		}else if(surveyName==''){
			alert("Please Select Name of Survey");
			return false;
		}else if(surveyUrl==''){
			alert("Please Select Url");
			return false;
		}else if(jobId==''){
			alert("Please Select JOBID");
			return false;
		}else if(transferre==''){
			alert("Please Select TRANSFERRE");
			return false;
		}else if(recommend==''){
			alert("Please Select RECOMMEND");
			return false;
		}else if(destination==''){
			alert("Please Select DESTINATION");
			return false;
		}else if(coordinator==''){
			alert("Please Select COORDINATOR");
			return false;
		}else if(origin==''){
			alert("Please Select ORIGIN");
			return false;
		}else if(overall==''){
			alert("Please Select OVERALL");
			return false;
		}else if(premove==''){
			alert("Please Select PREMOVE");
			return false;
		}else if(jobIdType==''){
			alert("Please Select JOBID TYPE");
			return false;
		}else if(jobIdSize==''){
			alert("Please Select JOBID SIZE");
			return false;
		}else if(transferreType==''){
			alert("Please Select TRANSFERRE TYPE");
			return false;
		}else if(transferreSize==''){
			alert("Please Select TRANSFERRE SIZE");
			return false;
		}else if(recommendType==''){
			alert("Please Select RECOMMEND TYPE");
			return false;
		}else if(recommendSize==''){
			alert("Please Select RECOMMEND SIZE");
			return false;
		}else if(destinationType==''){
			alert("Please Select DESTINATION TYPE");
			return false;
		}else if(destinationSize==''){
			alert("Please Select DESTINATION SIZE");
			return false;
		}else if(coordinatorType==''){
			alert("Please Select COORDINATOR TYPE");
			return false;
		}else if(coordinatorSize==''){
			alert("Please Select COORDINATOR SIZE");
			return false;
		}else if(originType==''){
			alert("Please Select ORIGIN TYPE");
			return false;
		}else if(originSize==''){
			alert("Please Select ORIGIN SIZE");
			return false;
		}else if(overallType==''){
			alert("Please Select OVERALL TYPE");
			return false;
		}else if(overallSize==''){
			alert("Please Select OVERALL SIZE");
			return false;
		}else if(premoveType==''){
			alert("Please Select PREMOVE TYPE");
			return false;
		}else if(premoveSize==''){
			alert("Please Select PREMOVE SIZE");
			return false;
			/* }else if(nps==''){
			alert("Please Select NPS");
			return false;
		}else if(npsType==''){
			alert("Please Select NPS TYPE");
			return false;
		}else if(npsSize==''){
			alert("Please Select NPS SIZE");
			return false; */
		}else if((jobIdType=='TEXT')&&(parseInt(jobIdSize)>1)){
			alert("SIZE should be 1 for JOBID Text TYPE.");   
			return false;
		}else if((transferreType=='TEXT')&&(parseInt(transferreSize)>1)){
			alert("SIZE should be 1 for TRANSFERRE Text TYPE.");   
			return false;
		}else if((recommendType=='TEXT')&&(parseInt(recommendSize)>1)){
			alert("SIZE should be 1 for RECOMMEND Text TYPE.");   
			return false;
		}else if((destinationType=='TEXT')&&(parseInt(destinationSize)>1)){
			alert("SIZE should be 1 for DESTINATION Text TYPE.");   
			return false;
		}else if((coordinatorType=='TEXT')&&(parseInt(coordinatorSize)>1)){
			alert("SIZE should be 1 for COORDINATOR Text TYPE.");   
			return false;
		}else if((originType=='TEXT')&&(parseInt(originSize)>1)){
			alert("SIZE should be 1 for ORIGIN Text TYPE.");   
			return false;
		}else if((overallType=='TEXT')&&(parseInt(overallSize)>1)){
			alert("SIZE should be 1 for OVERALL Text TYPE.");   
			return false;
		}else if((premoveType=='TEXT')&&(parseInt(premoveSize)>1)){
			alert("SIZE should be 1 for PREMOVE Text TYPE.");   
			return false;
		}else if(((jobIdType=='TEXT')||(jobIdType=='RADIO'))&&(jobId.indexOf(',')>-1)){
			alert("Only one value can be insert for JOBID Radio/Text TYPE. ");  
			return false;
		}else if(((transferreType=='TEXT')||(transferreType=='RADIO'))&&(transferre.indexOf(',')>-1)){
			alert("Only one value can be insert for TRANSFERRE Radio/Text TYPE. ");  
			return false;
		}else if(((recommendType=='TEXT')||(recommendType=='RADIO'))&&(recommend.indexOf(',')>-1)){
			alert("Only one value can be insert for RECOMMEND Radio/Text TYPE. ");   
			return false;
		}else if(((destinationType=='TEXT')||(destinationType=='RADIO'))&&(destination.indexOf(',')>-1)){
			alert("Only one value can be insert for DESTINATION Radio/Text TYPE. ");  
			return false;
		}else if(((coordinatorType=='TEXT')||(coordinatorType=='RADIO'))&&(coordinator.indexOf(',')>-1)){
			alert("Only one value can be insert for COORDINATOR Radio/Text TYPE. ");  
			return false;
		}else if(((originType=='TEXT')||(originType=='RADIO'))&&(origin.indexOf(',')>-1)){
			alert("Only one value can be insert for ORIGIN Radio/Text TYPE. ");  
			return false;
		}else if(((overallType=='TEXT')||(overallType=='RADIO'))&&(overall.indexOf(',')>-1)){
			alert("Only one value can be insert for OVERALL Radio/Text TYPE. ");   
			return false;
		}else if(((premoveType=='TEXT')||(premoveType=='RADIO'))&&(premove.indexOf(',')>-1)){
			alert("Only one value can be insert for PREMOVE Radio/Text TYPE. ");   
			return false;
		}else if((jobIdType=='DROPDOWN')&&(jobId.split(',').length==parseInt(jobIdSize))){
			alert("SIZE should be equal to No of insert values in JOBID Dropdown TYPE field");   
			return false;
		}else if((transferreType=='DROPDOWN')&&(transferre.split(',').length!=parseInt(transferreSize))){
			alert("SIZE should be equal to No of insert values in TRANSFERRE Dropdown TYPE field"); 
			return false;
		}else if((recommendType=='DROPDOWN')&&(recommend.split(',').length!=parseInt(recommendSize))){
			alert("SIZE should be equal to No of insert values in RECOMMEND Dropdown TYPE field"); 
			return false;
		}else if((destinationType=='DROPDOWN')&&(destination.split(',').length!=parseInt(destinationSize))){
			alert("SIZE should be equal to No of insert values in DESTINATION Dropdown TYPE field"); 
			return false;
		}else if((coordinatorType=='DROPDOWN')&&(coordinator.split(',').length!=parseInt(coordinatorSize))){
			alert("SIZE should be equal to No of insert values in COORDINATOR Dropdown TYPE field"); 
			return false;
		}else if((originType=='DROPDOWN')&&(origin.split(',').length!=parseInt(originSize))){
			alert("SIZE should be equal to No of insert values in ORIGIN Dropdown TYPE field");    
			return false;
		}else if((overallType=='DROPDOWN')&&(overall.split(',').length!=parseInt(overallSize))){
			alert("SIZE should be equal to No of insert values in OVERALL Dropdown TYPE field"); 
			return false;
		}else if((premoveType=='DROPDOWN')&&(premove.split(',').length!=parseInt(premoveSize))){
			alert("SIZE should be equal to No of insert values in PREMOVE Dropdown TYPE field"); 
			return false;
		}else if(((npsType=='TEXT')||(npsType=='RADIO'))&&(nps.indexOf(',')>-1)){
			alert("Only one value can be insert for NPS Radio/Text TYPE. ");   
			return false;
		}else if((npsType=='DROPDOWN')&&(nps.split(',').length!=parseInt(npsSize))){
			alert("SIZE should be equal to No of insert values in NPS Dropdown TYPE field"); 
			return false;
		}else{
			correctFieldAllValue();
			return true;
		}
}
function correctFieldValue(target,field){
      var newIds=target;
      newIds=newIds.replace(",,",",");
  	  while(newIds.indexOf(",,")>-1)
	  {
		newIds=newIds.replace(",,",",");
	  }
	  var len=newIds.length-1;
	  if(len==newIds.lastIndexOf(",")){
	      newIds=newIds.substring(0,len);
	  }
	  if(newIds.indexOf(",")==0){
		  newIds=newIds.substring(1,newIds.length);
	  }
	  newIds=newIds.trim();
	  document.forms['emailConfigPopup'].elements[field].value=newIds;
}
function correctFieldAllValue(){
	 var jobId = document.forms['emailConfigPopup'].elements['jobId'].value;
	 var transferre = document.forms['emailConfigPopup'].elements['transferre'].value;
	 var recommend = document.forms['emailConfigPopup'].elements['recommend'].value;
	 var destination = document.forms['emailConfigPopup'].elements['destination'].value;
	 var coordinator = document.forms['emailConfigPopup'].elements['coordinator'].value;
	 var origin = document.forms['emailConfigPopup'].elements['origin'].value;
	 var overall = document.forms['emailConfigPopup'].elements['overall'].value;
	 var premove = document.forms['emailConfigPopup'].elements['premove'].value;
	 var nps = document.forms['emailConfigPopup'].elements['nps'].value;
	 correctFieldValue(jobId,'jobId');
	 correctFieldValue(transferre,'transferre');
	 correctFieldValue(recommend,'recommend');
	 correctFieldValue(destination,'destination');
	 correctFieldValue(coordinator,'coordinator');
	 correctFieldValue(origin,'origin');
	 correctFieldValue(overall,'overall');
	 correctFieldValue(premove,'premove'); 
	 correctFieldValue(nps,'nps'); 
}
function checkRequiredFieldForRecipient(){
	 var surveyEmailName = document.forms['emailConfigPopup'].elements['surveyEmailName'].value;
	 var surveyEmailDesription = document.forms['emailConfigPopup'].elements['surveyEmailDesription'].value;
	 if(surveyEmailName==''){
			alert("Please Select Name Of Recipient");
			return false;
		}else if(surveyEmailDesription==''){
			alert("Please Select Description Of Recipient");
			return false;
		}else{
			return true;
		}
}
function checkRequiredFieldForSubject(){
	 var surveyEmailName = document.forms['emailConfigPopup'].elements['surveyEmailName'].value;
	 var surveyEmailDesription = document.forms['emailConfigPopup'].elements['surveyEmailDesription'].value;
	 if(surveyEmailName==''){
			alert("Please Select Name Of Subject");
			return false;
		}else if(surveyEmailDesription==''){
			alert("Please Select Description Of Subject");
			return false;
		}else{
			return true;
		}
}
function checkRequiredFieldForBody(){
	 var surveyEmailName = document.forms['emailConfigPopup'].elements['surveyEmailName'].value;
	 var surveyEmailDesription = document.forms['emailConfigPopup'].elements['surveyEmailDesription'].value;
	 if(surveyEmailName==''){
			alert("Please Select Name Of Body");
			return false;
		}else if(surveyEmailDesription==''){
			alert("Please Select Description Of Body");
			return false;
		}else{
			return true;
		}
}
function checkRequiredFieldForSignature(){
	 var surveyEmailName = document.forms['emailConfigPopup'].elements['surveyEmailName'].value;
	 var surveyEmailDesription = document.forms['emailConfigPopup'].elements['surveyEmailDesription'].value;
	 if(surveyEmailName==''){
			alert("Please Select Name Of Signature");
			return false;
		}else if(surveyEmailDesription==''){
			alert("Please Select Description Of Signature");
			return false;
		}else{
			return true;
		}
}

</script>
 </head>
<s:form name="emailConfigPopup" action="saveEmailPopup" onsubmit="return checkRequredField();" method="post" validate="true">
<s:hidden name="configType" value="${configType}"/>
<c:set var="buttons">   

 <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;margin-left:10px;margin-top:5px;" key="button.save"/>
 <input type="button" class="cssbutton" style="width:55px; height:25px;" onclick="closeWindow();" value="Cancel"/>
</c:set> 

			<c:choose>
							<c:when test="${configType=='surveyfrom'}">
							
							<s:hidden name="refmasterUrlId"/>
							<s:hidden name="refmasterJobId"/>
							<s:hidden name="refmasterTransferreId"/>
							<s:hidden name="refmasterRecommendId"/>
							<s:hidden name="refmasterDestinationId"/>
							<s:hidden name ="refmasterCoordinatorId"/>
							<s:hidden name="refmasterOriginId"/>
							<s:hidden name="refmasterOverallId"/>
							<s:hidden name="refmasterPremoveId"/>
							<s:hidden name="refmasterNpsId"/>
							<s:hidden name="type"/>
							
								<table>
								<tr><td colspan="6" class="listwhitetext" align="left" style="font-weight:800; ">Add details-QUALITYSURVEYURL</td></tr>	
									<tr>
										<td  align="right" class="listwhitetext" >Survey ID
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px" colspan="4">
										<s:textfield id="surveyId" name="surveyId" cssClass="input-text" />
										</td>
									</tr>
									<tr>
										<td  align="right" class="listwhitetext" >Name Of Survey
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="surveyName" name="surveyName" cssClass="input-text" />
										</td>
										<td colspan="3">
										<table style="margin:0px;padding:0px;">
										<tr>
										<td  align="right" class="listwhitetext" >URL<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textarea rows="3" cols="17" id="surveyUrl" name="surveyUrl" cssClass="textarea" />
										</td>
										</tr>
										</table>
										</td>
									</tr>
	<tr><td class="listwhitetext" align="left" style="font-weight:800; ">Add details-ANSWERIDPARAM</td><td></td><td class="listwhitetext" align="left" style="font-weight:800; ">TYPE</td><td class="listwhitetext" align="left" style="font-weight:800; ">SIZE</td></tr>									
									<tr>
										<td  align="right" class="listwhitetext" >JOBID
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px" >
										<s:textfield id="jobId" name="jobId" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'jobId');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="jobIdType" id="jobIdType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
								
										<s:select name="jobIdSize" id="jobIdSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
										
									</tr>

									<tr>
										<td  align="right" class="listwhitetext" >TRANSFERRE
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="transferre" name="transferre" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'transferre');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="transferreType" id="transferreType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="transferreSize" id="transferreSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									<tr>
										<td  align="right" class="listwhitetext" >RECOMMEND
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="recommend" name="recommend" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'recommend');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="recommendType" id="recommendType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="recommendSize" id="recommendSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									<tr>
										<td  align="right" class="listwhitetext" >DESTINATION
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="destination" name="destination" cssClass="input-text"  cssStyle="width:150px" onchange="correctFieldValue(this.value,'destination');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="destinationType" id="destinationType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="destinationSize" id="destinationSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
																		<tr>
										<td  align="right" class="listwhitetext" >COORDINATOR
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="coordinator" name="coordinator" cssClass="input-text"  cssStyle="width:150px" onchange="correctFieldValue(this.value,'coordinator');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="coordinatorType" id="coordinatorType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="coordinatorSize" id="coordinatorSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									
									<tr>
										<td  align="right" class="listwhitetext" >ORIGIN
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="origin" name="origin" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'origin');"/>
										</td>
										<td class="listwhitetext" width="10px">
									
										<s:select name="originType" id="originType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="originSize" id="originSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									<tr>
										<td  align="right" class="listwhitetext" >OVERALL
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="overall" name="overall" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'overall');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="overallType" id="overallType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="overallSize" id="overallSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									<tr>
										<td  align="right" class="listwhitetext" >PREMOVE
										<font color="red" size="2">*</font>
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="premove" name="premove" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'premove');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="premoveType" id="premoveType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="premoveSize" id="premoveSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									
									<tr>
										<td  align="right" class="listwhitetext" >NPS
										<!-- <font color="red" size="2">*</font> -->
										</td>
										<td class="listwhitetext" width="10px">
										<s:textfield id="nps" name="nps" cssClass="input-text" cssStyle="width:150px" onchange="correctFieldValue(this.value,'nps');"/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="npsType" id="npsType" list="{'TEXT','RADIO','DROPDOWN'}"  cssClass="list-menu"  cssStyle="width:100px" headerKey="" headerValue=""/>
										</td>
										<td class="listwhitetext" width="10px">
										
										<s:select name="npsSize" id="npsSize" list="{'1','2','3','4','5','6','7','8','9','10'}"  cssClass="list-menu"  cssStyle="width:50px" headerKey="" headerValue=""/>
										</td>
									
									</tr>
									
									<tr><td></td><td colspan="2">
									<c:out value="${buttons}" escapeXml="false" />
									</td></tr>   
								</table>
							</c:when> 
							<c:when test="${configType=='recipient'}">
								<table>
								<tr><td  colspan="6" class="listwhitetext" align="left" style="font-weight:800; ">Add details-QSURVEYRECIPIENT</td></tr>
								<tr><td  align="right" class="listwhitetext" >Name<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textfield id="surveyEmailName" name="surveyEmailName" cssClass="input-text" /></td></tr>
								<tr><td class="listwhitetext"  align="right" >Description<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textarea rows="3" cols="17" id="surveyEmailDesription" name="surveyEmailDesription" cssClass="textarea" /></td>
								</tr>
								<tr><td></td><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr>   
								</table>
							</c:when>
							<c:when test="${configType=='subject'}">
								<table>
								<tr><td  colspan="6" class="listwhitetext" align="left" style="font-weight:800; ">Add details-QSURVEYSUBJECT</td></tr>
								<tr><td  align="right" class="listwhitetext" >Name<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textfield id="surveyEmailName" name="surveyEmailName" cssClass="input-text" /></td></tr>
								<tr><td class="listwhitetext"  align="right" >Description<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textarea rows="3" cols="17" id="surveyEmailDesription" name="surveyEmailDesription" cssClass="textarea" /></td></tr>
								<tr><td></td><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr>   
								</table>
							</c:when>
							<c:when test="${configType=='body'}">
								<table>
								<tr><td colspan="6" class="listwhitetext" align="left" style="font-weight:800; ">Add details-QSURVEYBODY</td></tr>
								<tr><td  align="right" class="listwhitetext" >Name<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textfield id="surveyEmailName" name="surveyEmailName" cssClass="input-text" /></td></tr>
								<tr><td class="listwhitetext"  align="right" >Description<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textarea rows="3" cols="17" id="surveyEmailDesription" name="surveyEmailDesription" cssClass="textarea" /></td></tr>
								<tr><td></td><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr>   
								</table>
							</c:when>
							<c:when test="${configType=='signature'}">
								<table>
								<tr><td colspan="6" class="listwhitetext" align="left" style="font-weight:800; ">Add details-QSURVEYSIGNATURE</td></tr>
								<tr><td  align="right" class="listwhitetext" >Name<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textfield id="surveyEmailName" name="surveyEmailName" cssClass="input-text" /></td></tr>
								<tr><td class="listwhitetext"  align="right" >Description<font color="red" size="2">*</font></td>
								<td class="listwhitetext" width="10px"><s:textarea rows="3" cols="17" id="surveyEmailDesription" name="surveyEmailDesription" cssClass="textarea" /></td></tr>
								<tr><td></td><td colspan="2"><c:out value="${buttons}" escapeXml="false" /></td></tr>   
								</table>
							</c:when>
					       <c:otherwise>
					       
					       </c:otherwise>
			</c:choose>	       

</s:form>
 <script type="text/javascript">

</script>