package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.SurveyResponseDtlDao;
import com.trilasoft.app.model.SurveyResponseDtl;

public class SurveyResponseDtlDaoHibernate extends GenericDaoHibernate<SurveyResponseDtl, Long> implements SurveyResponseDtlDao{
	public SurveyResponseDtlDaoHibernate()
	{
		super(SurveyResponseDtl.class);
	}

}
