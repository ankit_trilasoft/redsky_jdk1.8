package com.trilasoft.app.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.aopalliance.intercept.*;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.SessionFactoryUtils;
import com.trilasoft.app.webapp.action.LoginAction;

public class SessionCorpIDInterceptor extends HibernateAccessor implements
		MethodInterceptor {

	private static final String serviceOrderFilters = "mode,routing,job,billtocode,brokercode,originagentcode,originsubagentcode,destinationagentcode,destinationsubagentcode,companydivision, vendorcode";
	private static final String customerFileFilters = "billtocode,companydivision";
	private static final String workTicketFilters = "jobtype,companydivision";
	private static final String notesFilters = "accportal,partnerportal";

	// private UserManager userManager;
	private DataSecurityFilterService dataSecurityFilterService; 
	private LoginAction loginAction;
	private List corpIdList;
	public Object invoke(MethodInvocation method) throws Throwable {
		// String sessionCorpID = "GLOBAL";
		// String sessionCorpID = sessionCorpIDHolder.getSessionCorpID();
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		Filter agentFilter=null;
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		long start = System.currentTimeMillis();
		try {
			Object result = method.proceed();
			return result;
		} finally {
			long end = System.currentTimeMillis();
			long timeMs = end - start;
			
		}
		}


	
	public void enableFilters() throws Exception {
		// String sessionCorpID = "GLOBAL";
		// String sessionCorpID = sessionCorpIDHolder.getSessionCorpID();
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		
		if (auth != null) { 
			User user = (User) auth.getPrincipal();
			if(user !=null){
			String sessionCorpID = user.getCorpID(); 
			Map filterMap=user.getFilterMap();
			if(filterMap!=null && (!(filterMap.isEmpty()))){
				Filter agentFilter=null;
				Filter partnerAccessCorpIdsFilter=null;
				Filter partnerCorpID=null;
				Filter refMasterCorpID=null;
				Filter partnerPrivateCorpID=null;
				Filter todoRuleCorpID=null;
				Filter serviceOrderJobFilter = null;
				Filter serviceOrderBillToCodeFilter = null;
				Filter serviceOrderModeFilter=null;
				Filter serviceOrderRoutingFilter=null;
				Filter notesAccountPortalFilter=null;
				Filter notesPartnerPortalFilter=null;
				Filter serviceOrderInlandFilter=null;
				Filter serviceOrderBILLTOVENDORFilter=null;
				Filter serviceOrderNetworkSOFilter=null;
				Filter serviceOrderCompanyDivisionFilter=null;
				Filter serviceOrderRloPartnerPortalFilter=null;
				Filter serviceOrderSalesPortalFilter=null;
				Filter serviceOrderSalesBookingPortalFilter=null;
				Filter serviceOrderBookingAgentCodeFilter=null;
				Filter customerfileOriginDestinationCompanyCodeFilter=null; 
				
				Session session = SessionFactoryUtils.getSession(this
						.getSessionFactory(), false);
				session.enableFilter("corpID").setParameter("forCorpID",
						sessionCorpID);
				if(filterMap.containsKey("partnerAccessCorpIdsFilter")){
				HashMap valueMap=	(HashMap)filterMap.get("partnerAccessCorpIdsFilter");
				if(valueMap!=null && valueMap.containsKey("forCorpID")){
					List partnerAccessCorpIdsList=	 new ArrayList();
					partnerAccessCorpIdsList= (List)valueMap.get("forCorpID");
					partnerAccessCorpIdsFilter = session.enableFilter("partnerAccessCorpIdsFilter");
					partnerAccessCorpIdsFilter.setParameterList("forCorpID", partnerAccessCorpIdsList);
				}
				}
				if(filterMap.containsKey("partnerCorpID")){
					HashMap valueMap=	(HashMap)filterMap.get("partnerCorpID");
					if(valueMap!=null && valueMap.containsKey("forCorpID")){
						List partnerCorpIDList = new ArrayList();
						partnerCorpIDList= (List)valueMap.get("forCorpID");
						partnerCorpID = session.enableFilter("partnerCorpID");
						partnerCorpID.setParameterList("forCorpID", partnerCorpIDList);
					}
					}
				 if(filterMap.containsKey("refMasterCorpID")){
					HashMap valueMap=	(HashMap)filterMap.get("refMasterCorpID");
					if(valueMap!=null && valueMap.containsKey("partnerCorpID")){
						List partnerCorpIDList = new ArrayList();
						partnerCorpIDList= (List)valueMap.get("partnerCorpID");
						refMasterCorpID = session.enableFilter("refMasterCorpID");
						refMasterCorpID.setParameterList("partnerCorpID", partnerCorpIDList);
					}
					}
				   if(filterMap.containsKey("partnerPrivateCorpID")){
						HashMap valueMap=	(HashMap)filterMap.get("partnerPrivateCorpID");
						if(valueMap!=null && valueMap.containsKey("privateCorpID")){
							List partnerCorpIDList = new ArrayList();
							partnerCorpIDList= (List)valueMap.get("privateCorpID");
							partnerPrivateCorpID = session.enableFilter("partnerPrivateCorpID");
							partnerPrivateCorpID.setParameterList("privateCorpID", partnerCorpIDList);
							
						}
						}
				     if(filterMap.containsKey("todoRuleCorpID")){
						HashMap valueMap=	(HashMap)filterMap.get("todoRuleCorpID");
						if(valueMap!=null && valueMap.containsKey("todoRuleCorpIDValue")){
							List partnerCorpIDList = new ArrayList();
							partnerCorpIDList= (List)valueMap.get("todoRuleCorpIDValue");
							todoRuleCorpID = session.enableFilter("todoRuleCorpID");
							todoRuleCorpID.setParameterList("todoRuleCorpIDValue", partnerCorpIDList);
						}
						}
				     if(filterMap.containsKey("serviceOrderJobFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderJobFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderJobFilter = session.enableFilter("serviceOrderJobFilter");
								serviceOrderJobFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderBillToCodeFilter = session.enableFilter("serviceOrderBillToCodeFilter");
								serviceOrderBillToCodeFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderModeFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderModeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderModeFilter = session.enableFilter("serviceOrderModeFilter");
								serviceOrderModeFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderRoutingFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderRoutingFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderRoutingFilter = session.enableFilter("serviceOrderRoutingFilter");
								serviceOrderRoutingFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("notesAccountPortalFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("notesAccountPortalFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									boolean partnerCorpIDList = false;
								partnerCorpIDList= (Boolean)entry.getValue();
								notesAccountPortalFilter = session.enableFilter("notesAccountPortalFilter");
								notesAccountPortalFilter.setParameter(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("notesPartnerPortalFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("notesPartnerPortalFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									boolean partnerCorpIDList = false;
								partnerCorpIDList= (Boolean)entry.getValue();
								notesPartnerPortalFilter = session.enableFilter("notesPartnerPortalFilter");
								notesPartnerPortalFilter.setParameter(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderInlandFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderInlandFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderInlandFilter = session.enableFilter("serviceOrderInlandFilter");
								serviceOrderInlandFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderBILLTOVENDORFilter~billtocode")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBILLTOVENDORFilter~billtocode");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (serviceOrderBILLTOVENDORFilter == null) {
								serviceOrderBILLTOVENDORFilter = session.enableFilter("serviceOrderBILLTOVENDORFilter");
								}
								serviceOrderBILLTOVENDORFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderBILLTOVENDORFilter~vendorcode")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBILLTOVENDORFilter~vendorcode");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (serviceOrderBILLTOVENDORFilter == null) {
								serviceOrderBILLTOVENDORFilter = session.enableFilter("serviceOrderBILLTOVENDORFilter");
								}
								serviceOrderBILLTOVENDORFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("customerfileOriginDestinationCompanyCodeFilter~origincompanycode")){
							HashMap valueMap=	(HashMap)filterMap.get("customerfileOriginDestinationCompanyCodeFilter~origincompanycode");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (customerfileOriginDestinationCompanyCodeFilter == null) {
									customerfileOriginDestinationCompanyCodeFilter = session.enableFilter("customerfileOriginDestinationCompanyCodeFilter");
								}
								customerfileOriginDestinationCompanyCodeFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("customerfileOriginDestinationCompanyCodeFilter~destinationcompanycode")){
							HashMap valueMap=	(HashMap)filterMap.get("customerfileOriginDestinationCompanyCodeFilter~destinationcompanycode");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (customerfileOriginDestinationCompanyCodeFilter == null) {
									customerfileOriginDestinationCompanyCodeFilter = session.enableFilter("customerfileOriginDestinationCompanyCodeFilter");
								}
								customerfileOriginDestinationCompanyCodeFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderNetworkSOFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderNetworkSOFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									boolean partnerCorpIDList = false;
								partnerCorpIDList= (Boolean)entry.getValue();
								serviceOrderNetworkSOFilter = session.enableFilter("serviceOrderNetworkSOFilter");
								serviceOrderNetworkSOFilter.setParameter(key, partnerCorpIDList);
								}
							}
							}
				     if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderCompanyDivisionFilter = session.enableFilter("serviceOrderCompanyDivisionFilter");
								serviceOrderCompanyDivisionFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				      
				     if(filterMap.containsKey("serviceOrderRloPartnerPortalFilter")){
				    	 HashSet valueSet=	(HashSet)filterMap.get("serviceOrderRloPartnerPortalFilter");	
				    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
				    	Iterator iterator = valueSet.iterator();
				    	while (iterator.hasNext()) {
							String  mapkey = (String)iterator.next(); 
					    if(filterMap.containsKey(mapkey)){	
				    	 HashMap valueMap=	(HashMap)filterMap.get(mapkey);
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (serviceOrderRloPartnerPortalFilter == null) {
									serviceOrderRloPartnerPortalFilter = initializeRloPartnerPortalFilter(session, serviceOrderRloPartnerPortalFilter);
									
								} 
								serviceOrderRloPartnerPortalFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
							} 
				    	 }
				     }
				     if(filterMap.containsKey("serviceOrderSalesPortalFilter")){
				    	 HashSet valueSet=	(HashSet)filterMap.get("serviceOrderSalesPortalFilter");	
				    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
				    	Iterator iterator = valueSet.iterator();
				    	while (iterator.hasNext()) {
							String  mapkey = (String)iterator.next(); 
					    if(filterMap.containsKey(mapkey)){	
				    	 HashMap valueMap=	(HashMap)filterMap.get(mapkey);
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (serviceOrderSalesPortalFilter == null) {
									serviceOrderSalesPortalFilter = initializeSalesPortalFilter(session, serviceOrderSalesPortalFilter);
									
								} 
								serviceOrderSalesPortalFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
							} 
				    	 }
				      
				     }
				     if(filterMap.containsKey("serviceOrderSalesBookingPortalFilter")){

				    	 HashSet valueSet=	(HashSet)filterMap.get("serviceOrderSalesBookingPortalFilter");	
				    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
				    	Iterator iterator = valueSet.iterator();
				    	while (iterator.hasNext()) {
							String  mapkey = (String)iterator.next(); 
					    if(filterMap.containsKey(mapkey)){	
				    	 HashMap valueMap=	(HashMap)filterMap.get(mapkey);
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (serviceOrderSalesBookingPortalFilter == null) {
									serviceOrderSalesBookingPortalFilter = initializeSalesBookingPortalFilter(session, serviceOrderSalesBookingPortalFilter);
									
								} 
								serviceOrderSalesBookingPortalFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
							} 
				    	 }
				      
				      
				     }  
				     if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								serviceOrderBookingAgentCodeFilter = session.enableFilter("serviceOrderBookingAgentCodeFilter");
								serviceOrderBookingAgentCodeFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
				     
				     if(filterMap.containsKey("agentFilter")){
				    	 HashSet valueSet=	(HashSet)filterMap.get("agentFilter");	
				    	 if(valueSet!=null && (!(valueSet.isEmpty()))){
				    	Iterator iterator = valueSet.iterator();
				    	while (iterator.hasNext()) {
				    		String  mapkey = (String)iterator.next(); 
						    if(filterMap.containsKey(mapkey)){	
							HashMap valueMap=	(HashMap)filterMap.get(mapkey);
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey();
									List partnerCorpIDList = new ArrayList();
								partnerCorpIDList= (List)entry.getValue();
								if (agentFilter == null) {
									agentFilter = initializeAgentFilter(session, agentFilter);
									
								}
								agentFilter.setParameterList(key, partnerCorpIDList);
								}
							}
							}
							}
				    	 }
				    	 }
				     
				
			}else{
			loginAction.userlogfileSave(user);
			Map localFilterMap = new HashMap();
			HashSet rloServiceSet = new HashSet();
			HashSet agentSet = new HashSet();
			Map localFilterValueMap = new HashMap();
			Filter agentFilter=null; 
			Session session = SessionFactoryUtils.getSession(this
					.getSessionFactory(), false);
			session.enableFilter("corpID").setParameter("forCorpID",
					sessionCorpID);
			localFilterValueMap.put("forCorpID", sessionCorpID);
			localFilterMap.put("corpID", localFilterValueMap);
			String userType=user.getUserType();
			System.out.println("login time forCorpID "+sessionCorpID);
			if((userType.equalsIgnoreCase("PARTNER")||userType.equalsIgnoreCase("AGENT"))&& (sessionCorpID.equalsIgnoreCase("TSFT"))){
				//System.out.println("corpIdList  "+corpIdList);
				if(corpIdList!=null && (!(corpIdList.isEmpty()))){
				
			}else{
				corpIdList=dataSecurityFilterService.PartnerAccessCorpIdList(); 		
			}
				
			}
			Filter partnerAccessCorpIdsFilter=null;
			if((userType.equalsIgnoreCase("PARTNER")||userType.equalsIgnoreCase("AGENT")) && (sessionCorpID.equalsIgnoreCase("TSFT"))){
				List partnerAccessCorpIdsList=	 new ArrayList();
				partnerAccessCorpIdsList = getFixedLengthList(partnerAccessCorpIdsList, "-- NO FILTER --");
		    	getFixedLengthList(partnerAccessCorpIdsList,sessionCorpID);
				//partnerAccessCorpIdsList.add(sessionCorpID); 
				//List corpIdList=dataSecurityFilterService.PartnerAccessCorpIdList(); 
				Iterator iterator=corpIdList.iterator();
				while(iterator.hasNext()){
				String corpid=	iterator.next().toString();
				//partnerAccessCorpIdsList.add(corpid);
				getFixedLengthList(partnerAccessCorpIdsList,corpid);
				}
				partnerAccessCorpIdsFilter = session.enableFilter("partnerAccessCorpIdsFilter");
				partnerAccessCorpIdsFilter.setParameterList("forCorpID", partnerAccessCorpIdsList);
				localFilterValueMap= new HashMap();
				localFilterValueMap.put("forCorpID", partnerAccessCorpIdsList);
				localFilterMap.put("partnerAccessCorpIdsFilter", localFilterValueMap);
				
		   }else{ 
			  List partnerAccessCorpIdsList=	 new ArrayList();
			  partnerAccessCorpIdsList = getFixedLengthList(partnerAccessCorpIdsList, "-- NO FILTER --");
		      getFixedLengthList(partnerAccessCorpIdsList,sessionCorpID);
			 // partnerAccessCorpIdsList.add(sessionCorpID); 
			  partnerAccessCorpIdsFilter = session.enableFilter("partnerAccessCorpIdsFilter");
			  partnerAccessCorpIdsFilter.setParameterList("forCorpID", partnerAccessCorpIdsList);
			  localFilterValueMap= new HashMap();
			  localFilterValueMap.put("forCorpID", partnerAccessCorpIdsList);
			  localFilterMap.put("partnerAccessCorpIdsFilter", localFilterValueMap); 
				}
			
			
			Filter partnerCorpID=null;
			String parentCorpID=dataSecurityFilterService.getParentCorpId(sessionCorpID);
			if((userType.equalsIgnoreCase("PARTNER")||userType.equalsIgnoreCase("AGENT")) && (sessionCorpID.equalsIgnoreCase("TSFT"))){
				List partnerCorpIDList = new ArrayList();
				partnerCorpIDList = getFixedLengthList(partnerCorpIDList, "-- NO FILTER --");
		    	getFixedLengthList(partnerCorpIDList,sessionCorpID);
		    	getFixedLengthList(partnerCorpIDList,"TSFT");
				//partnerCorpIDList.add(sessionCorpID);
				//partnerCorpIDList.add("TSFT");
				//List corpIdList=dataSecurityFilterService.PartnerAccessCorpIdList(); 
				Iterator iterator=corpIdList.iterator();
				while(iterator.hasNext()){
				String corpid=	iterator.next().toString();
				getFixedLengthList(partnerCorpIDList,corpid);
				//partnerCorpIDList.add(corpid);
				}
				partnerCorpID = session.enableFilter("partnerCorpID");
	            partnerCorpID.setParameterList("forCorpID", partnerCorpIDList);
	            localFilterValueMap= new HashMap();
				localFilterValueMap.put("forCorpID", partnerCorpIDList);
				localFilterMap.put("partnerCorpID", localFilterValueMap);
				
		   }else{  
			List partnerCorpIDList = new ArrayList();
			partnerCorpIDList = getFixedLengthList(partnerCorpIDList, "-- NO FILTER --");
	    	getFixedLengthList(partnerCorpIDList,sessionCorpID);
	    	getFixedLengthList(partnerCorpIDList,"TSFT");
	    	getFixedLengthList(partnerCorpIDList,parentCorpID);
           // partnerCorpIDList.add("TSFT");
           // partnerCorpIDList.add(sessionCorpID);
           // partnerCorpIDList.add(parentCorpID);
            partnerCorpID = session.enableFilter("partnerCorpID");
            partnerCorpID.setParameterList("forCorpID", partnerCorpIDList);
            localFilterValueMap= new HashMap();
			localFilterValueMap.put("forCorpID", partnerCorpIDList);
			localFilterMap.put("partnerCorpID", localFilterValueMap);
		   }
            Filter refMasterCorpID=null;
            
            if((userType.equalsIgnoreCase("PARTNER")||userType.equalsIgnoreCase("AGENT")) && (sessionCorpID.equalsIgnoreCase("TSFT"))){
			
            	List refMasterCorpIDList=	 new ArrayList();
		    	refMasterCorpIDList = getFixedLengthList(refMasterCorpIDList, "-- NO FILTER --");
		    	getFixedLengthList(refMasterCorpIDList,sessionCorpID);
		    	getFixedLengthList(refMasterCorpIDList,"TSFT");
            	
				//List corpIdList=dataSecurityFilterService.PartnerAccessCorpIdList(); 
				Iterator iterator=corpIdList.iterator();
				while(iterator.hasNext()){
					String corpid=	iterator.next().toString();
					getFixedLengthList(refMasterCorpIDList,corpid);
	            }
				
				
				
				refMasterCorpID = session.enableFilter("refMasterCorpID");
	            refMasterCorpID.setParameterList("partnerCorpID", refMasterCorpIDList);
	            localFilterValueMap= new HashMap();
				localFilterValueMap.put("partnerCorpID", refMasterCorpIDList);
				localFilterMap.put("refMasterCorpID", localFilterValueMap);
				
		   }else{ 
			   
			   List refMasterCorpIDList=	 new ArrayList();
		    	refMasterCorpIDList = getFixedLengthList(refMasterCorpIDList, "-- NO FILTER --");
		    	getFixedLengthList(refMasterCorpIDList,sessionCorpID);
		    	getFixedLengthList(refMasterCorpIDList,"TSFT");
		    	getFixedLengthList(refMasterCorpIDList,parentCorpID);
				   
			   
            refMasterCorpID = session.enableFilter("refMasterCorpID");
            refMasterCorpID.setParameterList("partnerCorpID", refMasterCorpIDList);
            localFilterValueMap= new HashMap();
			localFilterValueMap.put("partnerCorpID", refMasterCorpIDList);
			localFilterMap.put("refMasterCorpID", localFilterValueMap);
		   }
            Filter partnerPrivateCorpID=null;
            List partnerPrivateCorpIDList = new ArrayList();
            partnerPrivateCorpIDList = getFixedLengthList(partnerPrivateCorpIDList, "-- NO FILTER --");
	    	getFixedLengthList(partnerPrivateCorpIDList,sessionCorpID);
	    	getFixedLengthList(partnerPrivateCorpIDList,parentCorpID);
           // partnerPrivateCorpIDList.add(parentCorpID);
            //partnerPrivateCorpIDList.add(sessionCorpID);
            partnerPrivateCorpID = session.enableFilter("partnerPrivateCorpID");
            partnerPrivateCorpID.setParameterList("privateCorpID", partnerPrivateCorpIDList);
            localFilterValueMap= new HashMap();
			localFilterValueMap.put("privateCorpID", partnerPrivateCorpIDList);
			localFilterMap.put("partnerPrivateCorpID", localFilterValueMap);
            
            Filter todoRuleCorpID=null;
            List todoRuleCorpIDList = new ArrayList();
            todoRuleCorpIDList = getFixedLengthList(todoRuleCorpIDList, "-- NO FILTER --");
	    	getFixedLengthList(todoRuleCorpIDList,sessionCorpID);
	    	getFixedLengthList(todoRuleCorpIDList,"TSFT");
            //todoRuleCorpIDList.add(sessionCorpID);
            //todoRuleCorpIDList.add("TSFT");
            todoRuleCorpID = session.enableFilter("todoRuleCorpID");
            todoRuleCorpID.setParameterList("todoRuleCorpIDValue", todoRuleCorpIDList);
            localFilterValueMap= new HashMap();
			localFilterValueMap.put("todoRuleCorpIDValue", todoRuleCorpIDList);
			localFilterMap.put("todoRuleCorpID", localFilterValueMap);
            Map<String, List> childAgentCodeMap = dataSecurityFilterService.getChildAgentCodeMap();
            List dataSecuritySet= dataSecurityFilterService.getFilterData(user
					.getId(), sessionCorpID); 
			Iterator it = dataSecuritySet.iterator();
			Filter serviceOrderJobFilter = null;
			Filter serviceOrderBillToCodeFilter = null;
			Filter serviceOrderModeFilter=null;
			Filter serviceOrderRoutingFilter=null;
			Filter notesAccountPortalFilter=null;
			Filter notesPartnerPortalFilter=null;
			Filter serviceOrderInlandFilter=null;
			Filter serviceOrderBILLTOVENDORFilter=null;
			Filter serviceOrderNetworkSOFilter=null;
			Filter serviceOrderCompanyDivisionFilter=null;
			Filter serviceOrderRloPartnerPortalFilter=null;
			Filter serviceOrderSalesPortalFilter=null;
			Filter serviceOrderSalesBookingPortalFilter=null;
			Filter serviceOrderBookingAgentCodeFilter=null;
			Filter customerfileOriginDestinationCompanyCodeFilter=null;
			// Filter customerFileFilter =
			// session.enableFilter("customerFileFilter").setParameter("forCorpID", sessionCorpID);
			// Filter workTicketFilter = session.enableFilter("workTicketFilter").setParameter("forCorpID", sessionCorpID);
			Map<String, List> fieldNameCounterMap = new HashMap<String, List>();

			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				String tableName = row[0].toString().trim();
				String fieldName = row[2].toString().trim();
				String filterValues = row[1].toString().trim();
				String dataSetName = row[3].toString().trim();
				List fieldNameList = new ArrayList();
				
				fieldNameList = (fieldNameCounterMap.get(tableName + "." + fieldName) == null) ? (new ArrayList()) : fieldNameCounterMap.get(tableName + "." + fieldName);
				if (dataSetName.startsWith("DATA_SECURITY_SET_AGENT_")){
					
					if (fieldName.equalsIgnoreCase("bookingagentcode")|| fieldName.equalsIgnoreCase("brokercode") || fieldName.equalsIgnoreCase("originagentcode") || fieldName.equalsIgnoreCase("originsubagentcode") || fieldName.equalsIgnoreCase("destinationagentcode") || fieldName.equalsIgnoreCase("destinationsubagentcode") || fieldName.equalsIgnoreCase("forwardercode") ) {
						if (agentFilter == null) {
							agentFilter = initializeAgentFilter(session, agentFilter);
							
						}
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						agentFilter.setParameterList(fieldName.toLowerCase() , fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("agentFilter~"+fieldName.trim().toLowerCase(), localFilterValueMap);
						agentSet.add("agentFilter~"+fieldName.trim().toLowerCase());
						localFilterMap.put("agentFilter", agentSet);
					}
					
				}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_CF_ORIGINDESTINATIONCOMPANYCODE_"))
				{
					if (fieldName.equalsIgnoreCase("originCompanyCode") || fieldName.equalsIgnoreCase("destinationCompanyCode")) {
						if (customerfileOriginDestinationCompanyCodeFilter == null) {
							customerfileOriginDestinationCompanyCodeFilter = session.enableFilter("customerfileOriginDestinationCompanyCodeFilter"); 
						}
						fieldNameList = getFixedLengthList(fieldNameList, filterValues); 
						customerfileOriginDestinationCompanyCodeFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("customerfileOriginDestinationCompanyCodeFilter~"+fieldName.toLowerCase(), localFilterValueMap);
					}
				}
				
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_MODE_"))
				{
					if (fieldName.equalsIgnoreCase("mode")) {
						if (serviceOrderModeFilter == null) {
							serviceOrderModeFilter = session
									.enableFilter("serviceOrderModeFilter");
													
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderModeFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderModeFilter", localFilterValueMap);
					}
					
				}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_COMPANYDIVISION_"))
				{
					if (fieldName.equalsIgnoreCase("companydivision")) {
						if (serviceOrderCompanyDivisionFilter == null) {
							serviceOrderCompanyDivisionFilter = session
									.enableFilter("serviceOrderCompanyDivisionFilter");
													
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderCompanyDivisionFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderCompanyDivisionFilter", localFilterValueMap);
					}
					
				}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_RLOVENDORCODE_"))
				{
					if (fieldName.equalsIgnoreCase("CAR_vendorCode")|| fieldName.equalsIgnoreCase("COL_vendorCode") || fieldName.equalsIgnoreCase("TRG_vendorCode") || fieldName.equalsIgnoreCase("HOM_vendorCode") || fieldName.equalsIgnoreCase("RNT_vendorCode") || fieldName.equalsIgnoreCase("SET_vendorCode") || fieldName.equalsIgnoreCase("LAN_vendorCode") || fieldName.equalsIgnoreCase("MMG_vendorCode") || fieldName.equalsIgnoreCase("ONG_vendorCode") || fieldName.equalsIgnoreCase("PRV_vendorCode") || fieldName.equalsIgnoreCase("AIO_vendorCode") || fieldName.equalsIgnoreCase("EXP_vendorCode") || fieldName.equalsIgnoreCase("RPT_vendorCode") || fieldName.equalsIgnoreCase("SCH_schoolSelected") || fieldName.equalsIgnoreCase("TAX_vendorCode") || fieldName.equalsIgnoreCase("TAC_vendorCode") || fieldName.equalsIgnoreCase("TEN_vendorCode") || fieldName.equalsIgnoreCase("VIS_vendorCode")|| fieldName.equalsIgnoreCase("WOP_vendorCode")|| fieldName.equalsIgnoreCase("REP_vendorCode") || fieldName.equalsIgnoreCase("RLS_vendorCode") || fieldName.equalsIgnoreCase("CAT_vendorCode") || fieldName.equalsIgnoreCase("CLS_vendorCode") || fieldName.equalsIgnoreCase("CHS_vendorCode") || fieldName.equalsIgnoreCase("DPS_vendorCode") || fieldName.equalsIgnoreCase("HSM_vendorCode") || fieldName.equalsIgnoreCase("PDT_vendorCode") || fieldName.equalsIgnoreCase("RCP_vendorCode") || fieldName.equalsIgnoreCase("SPA_vendorCode") || fieldName.equalsIgnoreCase("TCS_vendorCode") || fieldName.equalsIgnoreCase("MTS_vendorCode") || fieldName.equalsIgnoreCase("DSS_vendorCode") || fieldName.equalsIgnoreCase("HOB_vendorCode") || fieldName.equalsIgnoreCase("FLB_vendorCode") || fieldName.equalsIgnoreCase("FRL_vendorCode") || fieldName.equalsIgnoreCase("APU_vendorCode") || fieldName.equalsIgnoreCase("INS_vendorCode") || fieldName.equalsIgnoreCase("INP_vendorCode") || fieldName.equalsIgnoreCase("EDA_vendorCode") || fieldName.equalsIgnoreCase("TAS_vendorCode")) {
						if (serviceOrderRloPartnerPortalFilter == null) {
							//serviceOrderRloPartnerPortalFilter = session.enableFilter("serviceOrderRloPartnerPortalFilter");
							serviceOrderRloPartnerPortalFilter = initializeRloPartnerPortalFilter(session, serviceOrderRloPartnerPortalFilter);
													
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderRloPartnerPortalFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderRloPartnerPortalFilter~"+fieldName.toLowerCase(), localFilterValueMap);
						rloServiceSet.add("serviceOrderRloPartnerPortalFilter~"+fieldName.toLowerCase());
						localFilterMap.put("serviceOrderRloPartnerPortalFilter", rloServiceSet);
					}
					
				}else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_SALESPORTAL_")){
					if (fieldName.equalsIgnoreCase("salesMan")) {
						if(serviceOrderSalesPortalFilter==null){
							serviceOrderSalesPortalFilter = initializeSalesPortalFilter(session, serviceOrderSalesPortalFilter);
						}
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderSalesPortalFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderSalesPortalFilter~"+fieldName.toLowerCase(), localFilterValueMap);
						rloServiceSet.add("serviceOrderSalesPortalFilter~"+fieldName.toLowerCase());
						localFilterMap.put("serviceOrderSalesPortalFilter", rloServiceSet);
					}else if(fieldName.equalsIgnoreCase("bookingagentcode")){						
						if(serviceOrderSalesBookingPortalFilter==null){
							serviceOrderSalesBookingPortalFilter = initializeSalesBookingPortalFilter(session, serviceOrderSalesBookingPortalFilter);
						}
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderSalesBookingPortalFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderSalesBookingPortalFilter~"+fieldName.toLowerCase(), localFilterValueMap);
						rloServiceSet.add("serviceOrderSalesBookingPortalFilter~"+fieldName.toLowerCase());
						localFilterMap.put("serviceOrderSalesBookingPortalFilter", rloServiceSet);
					}
				}else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_ROUTING_"))
				{
					if (fieldName.equalsIgnoreCase("routing")) {
						if (serviceOrderRoutingFilter == null) {
							serviceOrderRoutingFilter = session.enableFilter("serviceOrderRoutingFilter");
														
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderRoutingFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderRoutingFilter", localFilterValueMap);
					}
					
				}
				
				else if (dataSetName.startsWith("DATA_SECURITY_SET_SO_JOB_"))
				{
					if (fieldName.equalsIgnoreCase("job")) {
						if (serviceOrderJobFilter == null) {
							serviceOrderJobFilter = session.enableFilter("serviceOrderJobFilter");
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderJobFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderJobFilter", localFilterValueMap);
					}
					
				}
				
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOCODE_"))
				{
					if (fieldName.equalsIgnoreCase("billtocode")) {
						try{
							//List tempAgentCode = dataSecurityFilterService.getChildAgentCode(filterValues,sessionCorpID,parentCorpID);
							List	tempAgentCode=null;
							if(childAgentCodeMap!=null && childAgentCodeMap.containsKey("TSFT~"+filterValues.trim())){
								 tempAgentCode=childAgentCodeMap.get("TSFT~"+filterValues.trim());
							}else if(childAgentCodeMap!=null && childAgentCodeMap.containsKey(parentCorpID.trim()+"~"+filterValues.trim())){
								 tempAgentCode=childAgentCodeMap.get(parentCorpID.trim()+"~"+filterValues.trim());
							}else{
								 tempAgentCode=childAgentCodeMap.get(sessionCorpID.trim()+"~"+filterValues.trim());
							}
							if(tempAgentCode!=null && !tempAgentCode.isEmpty()){
								for (int i=0; i < tempAgentCode.size(); i++) {
									fieldNameList = getFixedLengthList(fieldNameList, tempAgentCode.get(i).toString());
								}
							}
							}catch (Exception e) {
								System.out.println("Error in DATA_SECURITY_SET_SO_BILLTOCODE_________");
								e.printStackTrace();
							}
						if (serviceOrderBillToCodeFilter == null) {
							serviceOrderBillToCodeFilter = session.enableFilter("serviceOrderBillToCodeFilter");
													
						}
						
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderBillToCodeFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderBillToCodeFilter", localFilterValueMap);
					}
					
				}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_BOOKINGAGENTCODE_"))
				{
					if (fieldName.equalsIgnoreCase("bookingagentcode")) {
						if(serviceOrderBookingAgentCodeFilter== null){
							serviceOrderBookingAgentCodeFilter =  session.enableFilter("serviceOrderBookingAgentCodeFilter");
						}
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderBookingAgentCodeFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderBookingAgentCodeFilter", localFilterValueMap);
					}
				}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_INLANDCODE_"))
				{
					if (fieldName.equalsIgnoreCase("inlandCode")) {
						if (serviceOrderInlandFilter == null) {
							serviceOrderInlandFilter = session.enableFilter("serviceOrderInlandFilter");
													
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderInlandFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderInlandFilter", localFilterValueMap);
					}
					
				}
				
				else if(dataSetName.startsWith("DATA_SECURITY_SET_SO_BILLTOVENDORCODE_"))
				{
					if (fieldName.equalsIgnoreCase("billtocode") || fieldName.equalsIgnoreCase("vendorcode")) {
						if (serviceOrderBILLTOVENDORFilter == null) {
							serviceOrderBILLTOVENDORFilter = session.enableFilter("serviceOrderBILLTOVENDORFilter");
							List billtocodeList = new ArrayList();
							billtocodeList.add("--NO-FILTER--");
							List vendorcodeList = new ArrayList();
							vendorcodeList.add("--NO-FILTER--");
							serviceOrderBILLTOVENDORFilter.setParameterList("billtocode", billtocodeList)
							.setParameterList("vendorcode", vendorcodeList);
							
						}
						//fieldNameList.add(filterValues);
						fieldNameList = getFixedLengthList(fieldNameList, filterValues);
						serviceOrderBILLTOVENDORFilter.setParameterList(fieldName.toLowerCase(), fieldNameList);
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase(), fieldNameList);
						localFilterMap.put("serviceOrderBILLTOVENDORFilter~"+fieldName.toLowerCase(), localFilterValueMap);
					}
											
				}
				
				
				
				
				else if (dataSetName.startsWith("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL")){
						
						if (fieldName.equalsIgnoreCase("accportal")) {
							if (notesAccountPortalFilter == null) {
								notesAccountPortalFilter = session.enableFilter("notesAccountPortalFilter");
								notesAccountPortalFilter.setParameter("accportal",
										new Boolean(false));
							}
							notesAccountPortalFilter.setParameter(fieldName.toLowerCase()
									, new Boolean(filterValues));
							localFilterValueMap= new HashMap();
							localFilterValueMap.put(fieldName.toLowerCase(), new Boolean(filterValues));
							localFilterMap.put("notesAccountPortalFilter", localFilterValueMap);
						}
								
					}
				else if(dataSetName.startsWith("DATA_SECURITY_SET_NOTES_PARTNERPORTAL")){
						
						if (fieldName.equalsIgnoreCase("partnerportal")) {
							if (notesPartnerPortalFilter == null) {
								notesPartnerPortalFilter = session.enableFilter("notesPartnerPortalFilter");
								notesPartnerPortalFilter.setParameter("partnerportal",
										new Boolean(false));
							}
							notesPartnerPortalFilter.setParameter(fieldName.toLowerCase()
									, new Boolean(filterValues));
							localFilterValueMap= new HashMap();
							localFilterValueMap.put(fieldName.toLowerCase() , new Boolean(filterValues));
							localFilterMap.put("notesPartnerPortalFilter", localFilterValueMap);
						}
						
					}
				
				else if (dataSetName.startsWith("DATA_SECURITY_SET_SO_NETWORKSO_PARTNERPORTAL")){
					
					if (fieldName.equalsIgnoreCase("networkso")) {
						if (serviceOrderNetworkSOFilter == null) {
							serviceOrderNetworkSOFilter = session.enableFilter("serviceOrderNetworkSOFilter");
							serviceOrderNetworkSOFilter.setParameter("networkso",
									new Boolean(false));
						}
						serviceOrderNetworkSOFilter.setParameter(fieldName.toLowerCase()
								, new Boolean(filterValues));
						localFilterValueMap= new HashMap();
						localFilterValueMap.put(fieldName.toLowerCase() , new Boolean(filterValues));
						localFilterMap.put("serviceOrderNetworkSOFilter", localFilterValueMap);
					}
					
				}
				
				
				
				
			
					fieldNameCounterMap.put(tableName + "." + fieldName,	fieldNameList);
				
					}
			
			user.setFilterMap(localFilterMap);
			
			} 
			}else{  
			}
				
			}

		
		}
	
	
	
	private List getFixedLengthList(List fieldNameList, String filterValues) {
		if (fieldNameList.isEmpty()) {
			for (int i=0; i < 500; i++) fieldNameList.add("--NO-FILTER--");
		}
		
		for (int i=0; i < 500; i++) {
			if (fieldNameList.get(i).equals("--NO-FILTER--")) {
				fieldNameList.set(i, filterValues) ;
				break;
			}
		}
		
		return fieldNameList;
	}
    private Filter initializeRloPartnerPortalFilter(Session session, Filter serviceOrderRloPartnerPortalFilter){
	List carvendorcodeList = new ArrayList();
	getFixedLengthList(carvendorcodeList, "--NO-FILTER--");
	List colvendorCodeList = new ArrayList();
	getFixedLengthList(colvendorCodeList, "--NO-FILTER--");
	List trgvendorCodeList = new ArrayList();
	getFixedLengthList(trgvendorCodeList, "--NO-FILTER--");
	List homvendorCodeList = new ArrayList();
	getFixedLengthList(homvendorCodeList, "--NO-FILTER--");
	List rntvendorCodeList = new ArrayList();
	getFixedLengthList(rntvendorCodeList, "--NO-FILTER--");
	List setvendorcodeList = new ArrayList();
	getFixedLengthList(setvendorcodeList, "--NO-FILTER--");
	List lanvendorcodeList = new ArrayList();
	getFixedLengthList(lanvendorcodeList, "--NO-FILTER--");
	List mmgvendorcodeList = new ArrayList();
	getFixedLengthList(mmgvendorcodeList, "--NO-FILTER--");
	List ongvendorcodeList = new ArrayList();
	getFixedLengthList(ongvendorcodeList, "--NO-FILTER--");
	List prvvendorcodeList = new ArrayList();
	getFixedLengthList(prvvendorcodeList, "--NO-FILTER--");
	List aiovendorcodeList = new ArrayList();
	getFixedLengthList(aiovendorcodeList, "--NO-FILTER--");
	List expvendorcodeList = new ArrayList();
	getFixedLengthList(expvendorcodeList, "--NO-FILTER--");
	List rptvendorcodeList = new ArrayList();
	getFixedLengthList(rptvendorcodeList, "--NO-FILTER--");
	List schvendorcodeList = new ArrayList();
	getFixedLengthList(schvendorcodeList, "--NO-FILTER--");
	List taxvendorcodeList = new ArrayList();
	getFixedLengthList(taxvendorcodeList, "--NO-FILTER--");
	List tacvendorcodeList = new ArrayList();
	getFixedLengthList(tacvendorcodeList, "--NO-FILTER--");
	List tenvendorcodeList = new ArrayList();
	getFixedLengthList(tenvendorcodeList, "--NO-FILTER--");	
	List visvendorcodeList = new ArrayList();
	getFixedLengthList(visvendorcodeList, "--NO-FILTER--"); 
	List wopvendorCodeList = new ArrayList();
	getFixedLengthList(wopvendorCodeList, "--NO-FILTER--");
	List repvendorCodeList = new ArrayList();
	getFixedLengthList(repvendorCodeList, "--NO-FILTER--");

	List rlsvendorcodeList = new ArrayList(); 
	getFixedLengthList(rlsvendorcodeList, "--NO-FILTER--");
	List catvendorcodeList = new ArrayList(); 
	getFixedLengthList(catvendorcodeList, "--NO-FILTER--");
	List clsvendorcodeList = new ArrayList(); 
	getFixedLengthList(clsvendorcodeList, "--NO-FILTER--");
	List chsvendorcodeList = new ArrayList(); 
	getFixedLengthList(chsvendorcodeList, "--NO-FILTER--");
	List dpsvendorcodeList = new ArrayList(); 
	getFixedLengthList(dpsvendorcodeList, "--NO-FILTER--");
	List hsmvendorcodeList = new ArrayList(); 
	getFixedLengthList(hsmvendorcodeList, "--NO-FILTER--");
	List pdtvendorcodeList = new ArrayList(); 
	getFixedLengthList(pdtvendorcodeList, "--NO-FILTER--");
	List rcpvendorcodeList = new ArrayList(); 
	getFixedLengthList(rcpvendorcodeList, "--NO-FILTER--");
	List spavendorcodeList = new ArrayList(); 
	getFixedLengthList(spavendorcodeList, "--NO-FILTER--");
	List tcsvendorcodeList = new ArrayList(); 
	getFixedLengthList(tcsvendorcodeList, "--NO-FILTER--");
	List mtsvendorcodeList = new ArrayList(); 
	getFixedLengthList(mtsvendorcodeList, "--NO-FILTER--");
	List dssvendorcodeList = new ArrayList(); 
	getFixedLengthList(dssvendorcodeList, "--NO-FILTER--");
	List hobvendorcodeList = new ArrayList(); 
	getFixedLengthList(hobvendorcodeList, "--NO-FILTER--");
	List flbvendorcodeList = new ArrayList(); 
	getFixedLengthList(flbvendorcodeList, "--NO-FILTER--");
	List frlvendorCodeList = new ArrayList();
	getFixedLengthList(frlvendorCodeList, "--NO-FILTER--");
	List apuvendorCodeList = new ArrayList();
	getFixedLengthList(apuvendorCodeList, "--NO-FILTER--");
	List insvendorCodeList = new ArrayList();
	getFixedLengthList(insvendorCodeList, "--NO-FILTER--");
	List inpvendorCodeList = new ArrayList();
	getFixedLengthList(inpvendorCodeList, "--NO-FILTER--");
	List edavendorCodeList = new ArrayList();
	getFixedLengthList(edavendorCodeList, "--NO-FILTER--");
	List tasvendorCodeList = new ArrayList();
	getFixedLengthList(tasvendorCodeList, "--NO-FILTER--");
	
	session.disableFilter("serviceOrderRloPartnerPortalFilter");
	serviceOrderRloPartnerPortalFilter = session.enableFilter("serviceOrderRloPartnerPortalFilter");
	
	serviceOrderRloPartnerPortalFilter
	.setParameterList("car_vendorcode",carvendorcodeList)
	.setParameterList("col_vendorcode", colvendorCodeList)
	.setParameterList("trg_vendorcode", trgvendorCodeList)
	.setParameterList("hom_vendorcode", homvendorCodeList)
	.setParameterList("rnt_vendorcode", rntvendorCodeList)
	.setParameterList("set_vendorcode", setvendorcodeList)
	.setParameterList("lan_vendorcode", lanvendorcodeList)
	.setParameterList("mmg_vendorcode", mmgvendorcodeList)
	.setParameterList("ong_vendorcode", ongvendorcodeList)
	.setParameterList("prv_vendorcode", prvvendorcodeList)
	.setParameterList("aio_vendorcode", aiovendorcodeList)
	.setParameterList("exp_vendorcode", expvendorcodeList)
	.setParameterList("rpt_vendorcode", rptvendorcodeList)
	.setParameterList("sch_schoolselected", schvendorcodeList)
	.setParameterList("tax_vendorcode", taxvendorcodeList)
	.setParameterList("tac_vendorcode", tacvendorcodeList)
	.setParameterList("ten_vendorcode", tenvendorcodeList)
	.setParameterList("vis_vendorcode", visvendorcodeList)	
	.setParameterList("wop_vendorcode", wopvendorCodeList)	
	.setParameterList("rep_vendorcode", repvendorCodeList)
	.setParameterList("rls_vendorcode", rlsvendorcodeList)
	.setParameterList("cat_vendorcode", catvendorcodeList)
	.setParameterList("cls_vendorcode", clsvendorcodeList)
	.setParameterList("chs_vendorcode", chsvendorcodeList)
	.setParameterList("dps_vendorcode", dpsvendorcodeList)
	.setParameterList("hsm_vendorcode", hsmvendorcodeList)
	.setParameterList("pdt_vendorcode", pdtvendorcodeList)
	.setParameterList("rcp_vendorcode", rcpvendorcodeList)
	.setParameterList("spa_vendorcode", spavendorcodeList)
	.setParameterList("tcs_vendorcode", tcsvendorcodeList)
	.setParameterList("mts_vendorcode", mtsvendorcodeList)
	.setParameterList("dss_vendorcode", dssvendorcodeList)
	.setParameterList("hob_vendorcode", hobvendorcodeList)
	.setParameterList("flb_vendorcode", flbvendorcodeList)
	.setParameterList("frl_vendorcode", frlvendorCodeList)
	.setParameterList("apu_vendorcode", apuvendorCodeList)
	.setParameterList("ins_vendorcode", insvendorCodeList)
	.setParameterList("inp_vendorcode", inpvendorCodeList)
	.setParameterList("eda_vendorcode", edavendorCodeList)
	.setParameterList("tas_vendorcode", tasvendorCodeList);
     return serviceOrderRloPartnerPortalFilter;
      }
    private Filter initializeSalesPortalFilter(Session session, Filter serviceOrderSalesPortalFilter){
    	List salesManList = new ArrayList();
    	getFixedLengthList(salesManList, "--NO-FILTER--");    	
    	session.disableFilter("serviceOrderSalesPortalFilter");
    	serviceOrderSalesPortalFilter = session.enableFilter("serviceOrderSalesPortalFilter");
    	serviceOrderSalesPortalFilter.setParameterList("salesman", salesManList);
    	return serviceOrderSalesPortalFilter;    	
    }
    private Filter initializeSalesBookingPortalFilter(Session session, Filter serviceOrderSalesBookingPortalFilter){
    	List bookingagentcodeList = new ArrayList();
    	getFixedLengthList(bookingagentcodeList, "--NO-FILTER--");    	
    	session.disableFilter("serviceOrderSalesBookingPortalFilter");
    	serviceOrderSalesBookingPortalFilter = session.enableFilter("serviceOrderSalesBookingPortalFilter");
    	serviceOrderSalesBookingPortalFilter.setParameterList("bookingagentcode", bookingagentcodeList);
    	return serviceOrderSalesBookingPortalFilter;    	
    }
	private Filter initializeAgentFilter(Session session, Filter agentFilter) {
		List bookingagentcodeList = new ArrayList();
		//bookingagentcodeList.add("--NO-FILTER--");
		getFixedLengthList(bookingagentcodeList, "--NO-FILTER--");
		
		List brokercodeList = new ArrayList();
		//brokercodeList.add("--NO-FILTER--");
		getFixedLengthList(brokercodeList, "--NO-FILTER--");
		
		List originagentcodeList = new ArrayList();
		//originagentcodeList.add("--NO-FILTER--");
		getFixedLengthList(originagentcodeList, "--NO-FILTER--");
		
		List originsubagentcodeList = new ArrayList();
		//originsubagentcodeList.add("--NO-FILTER--");
		getFixedLengthList(originsubagentcodeList, "--NO-FILTER--");
		
		List destinationagentcodeList = new ArrayList();
		//destinationagentcodeList.add("--NO-FILTER--");
		getFixedLengthList(destinationagentcodeList, "--NO-FILTER--");
		
		List destinationsubagentcodeList = new ArrayList();
		//destinationsubagentcodeList.add("--NO-FILTER--");
		getFixedLengthList(destinationsubagentcodeList, "--NO-FILTER--");
		
		List forwardercodeList = new ArrayList();
		//forwardercodeList.add("--NO-FILTER--");
		getFixedLengthList(forwardercodeList, "--NO-FILTER--");
		
		session.disableFilter("agentFilter");
		agentFilter = session.enableFilter("agentFilter");
		
		agentFilter.setParameterList("bookingagentcode",bookingagentcodeList)
		.setParameterList("brokercode",brokercodeList)
		.setParameterList("originagentcode",originagentcodeList)
		.setParameterList("originsubagentcode",originsubagentcodeList)
		.setParameterList("destinationagentcode",destinationagentcodeList)
		.setParameterList("destinationsubagentcode",destinationsubagentcodeList)
		.setParameterList("forwardercode",forwardercodeList);
		return agentFilter;
	}


	/**
	 * Return a Session for use by this interceptor.
	 * 
	 * @see SessionFactoryUtils#getSession
	 */
	protected Session getSession() {
		return SessionFactoryUtils.getSession(getSessionFactory(),
				getEntityInterceptor(), getJdbcExceptionTranslator());
	}

	public void setDataSecurityFilterService(
			DataSecurityFilterService dataSecurityFilterService) {
		this.dataSecurityFilterService = dataSecurityFilterService;
	}



	public void setLoginAction(LoginAction loginAction) {
		this.loginAction = loginAction;
	}


	

}
