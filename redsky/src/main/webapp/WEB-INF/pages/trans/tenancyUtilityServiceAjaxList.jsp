<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>   
    <title></title>   
    <meta name="heading" content=""/> 
    <style type="text/css">
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
  
</head> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:if test="${utilityServiceDetailList!='[]'}">
	
	<fieldset style="margin-left:109px;width:1080px;margin-bottom:5px;">
	<legend>&nbsp;Additional&nbsp;Utilities</legend>
	    <display:table name="utilityServiceDetailList" class="table" requestURI="" id="utilityServiceDetailList"  pagesize="25" style="margin-left:0px;width:auto!important;margin-bottom:10px;">
	    <display:column title="#" style="width:10px;"><c:out value="${utilityServiceDetailList_rowNum}"/></display:column>
	    <display:column title="Utility" style="width:175px;">
		    <c:choose> 
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_Gas_Water'}">
				<c:out value="Water" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_Gas'}">
				<c:out value="Gas" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_Electricity'}">
				<c:out value="Electricity" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_TV_Internet_Phone'}">
				<c:out value="TV/Internet/Phone" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_mobilePhone'}">
				<c:out value="Mobile Phone" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_furnitureRental'}">
				<c:out value="Furniture Rental" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_cleaningServices'}">
				<c:out value="Cleaning Service" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_parkingPermit'}">
				<c:out value="Parking Permit" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_communityTax'}">
				<c:out value="Community Tax" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_insurance'}">
				<c:out value="Insurance" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_gardenMaintenance'}">
				<c:out value="Garden Maintenance" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_Miscellaneous'}">
				<c:out value="Miscellaneous" />
			</c:when>
			<c:when test="${utilityServiceDetailList.parentFieldName=='TEN_Utility_Gas_Electric'}">
				<c:out value="Gas/Electric" />
			</c:when>
			<c:otherwise>
			</c:otherwise>
			</c:choose>
		
	    </display:column>
	    <display:column title="T-Code" style="width:102px;">
	    	<input  class="input-text" type="text" name="TEN_utilityVendorCode${utilityServiceDetailList.id}" id="TEN_utilityVendorCode${utilityServiceDetailList.id}"  value="${utilityServiceDetailList.TEN_utilityVendorCode}" size="8" maxlength="8" readonly="true" onchange="updateUtilityService('${utilityServiceDetailList.id}','TEN_utilityVendorCode',this);"/>&nbsp;<img class="openpopup" style="text-align:right;vertical-align:top;" onclick="findVendorCode('TEN_utilityVendorCode${utilityServiceDetailList.id}','TEN_utilityVendorName${utilityServiceDetailList.id}','${utilityServiceDetailList.id}');" id="openpopup5.img" src="<c:url value='/images/open-popup.gif'/>" />
	    </display:column>
	    <display:column title="Vendor Name" style="width: 102px;">
	    	<input  class="input-text" type="text" name="TEN_utilityVendorName${utilityServiceDetailList.id}" id="TEN_utilityVendorName${utilityServiceDetailList.id}"  value="${utilityServiceDetailList.TEN_utilityVendorName}" size="25" maxlength="80" onkeyup="findUtilityVendor('TEN_utilityVendorName${utilityServiceDetailList.id}','TEN_utilityVendorCode${utilityServiceDetailList.id}','utilityVendorNameDivId${utilityServiceDetailList.id}',' and (isAccount=true or isAgent=true or isPrivateParty=true or isVendor=true or isOwnerOp=true or isCarrier=true )','${utilityServiceDetailList.id}',event);"/>
	    	<div id="utilityVendorNameDivId${utilityServiceDetailList.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
	    </display:column>
	    <display:column title="Amount" style="width: 105px;">
	    	<input  class="input-text" style="text-align:right" type="text" name="TEN_utilityAllowanceAmount"  value="${utilityServiceDetailList.TEN_utilityAllowanceAmount}" size="10" maxlength="10" onkeydown="return onlyRateAllowed(event)" onchange="updateUtilityService('${utilityServiceDetailList.id}','TEN_utilityAllowanceAmount',this);"/>
	    </display:column>
	    <display:column title="Currency" style="width:70px;">
		    <select class="list-menu" style="width:60px;" onchange="updateUtilityService('${utilityServiceDetailList.id}','TEN_utilityAllowanceCurrency',this);"> 
								<option value="<c:out value='' />">
								<c:out value=""></c:out>
							    </option>
				<c:forEach var="chrms" items="${currency}" varStatus="loopStatus">
			           <c:choose>
		                        <c:when test="${chrms.key == utilityServiceDetailList.TEN_utilityAllowanceCurrency}">
		                        <c:set var="selectedInd" value=" selected"></c:set>
		                        </c:when>
		                        <c:otherwise>
		                        <c:set var="selectedInd" value=""></c:set>
		                        </c:otherwise>
	                            </c:choose>
			      <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                    <c:out value="${chrms.value}"></c:out>
			                    </option>
				</c:forEach> 
			</select>
	    </display:column>
	    <display:column title="VAT&nbsp;Desc&nbsp;" style="width:90px;">
		    <select class="list-menu" name="TEN_utilityVatDesc" id="TEN_utilityVatDesc${utilityServiceDetailList.id}" style="width:80px;" onchange="getVatPercent1('${utilityServiceDetailList.id}');updateUtilityService('${utilityServiceDetailList.id}','TEN_utilityVatDesc',this);"> 
								
				<c:forEach var="chrms" items="${euVatList}" varStatus="loopStatus">
			           <c:choose>
		                        <c:when test="${chrms.key == utilityServiceDetailList.TEN_utilityVatDesc}">
		                        <c:set var="selectedInd" value=" selected"></c:set>
		                        </c:when>
		                        <c:otherwise>
		                        <c:set var="selectedInd" value=""></c:set>
		                        </c:otherwise>
	                            </c:choose>
			      <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                    <c:out value="${chrms.value}"></c:out>
			                    </option>
				</c:forEach> 
			</select>
			<input type="hidden" id="TEN_utilityVatPercent${utilityServiceDetailList.id}"  name="TEN_utilityVatPercent${utilityServiceDetailList.id}" />
	    </display:column>
	    <display:column title="Billing Cycle" style="width:72px;">
		    <select class="list-menu" style="width:60px;" onchange="updateUtilityService('${utilityServiceDetailList.id}','TEN_billingCycleUtility',this);"> 
								<option value="<c:out value='' />">
								<c:out value=""></c:out>
							    </option>
				<c:forEach var="chrms" items="${billingCycle}" varStatus="loopStatus">
			           <c:choose>
		                        <c:when test="${chrms.key == utilityServiceDetailList.TEN_billingCycleUtility}">
		                        <c:set var="selectedInd" value=" selected"></c:set>
		                        </c:when>
		                        <c:otherwise>
		                        <c:set var="selectedInd" value=""></c:set>
		                        </c:otherwise>
	                            </c:choose>
			      <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
			                    <c:out value="${chrms.value}"></c:out>
			                    </option>
				</c:forEach> 
			</select>
	    </display:column>
	    <display:column  title="Billed Through" style="width:90px;">
    	 	<fmt:parseDate pattern="yyyy-MM-dd" value="${utilityServiceDetailList.TEN_utilityBillThrough}" var="parsedUtilityBillThroughDate" />
			<fmt:formatDate pattern="dd-MMM-yy" value="${parsedUtilityBillThroughDate}" var="formattedUtilityBillThroughDate" />    
    		<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="TEN_utilityBillThrough${utilityServiceDetailList.id}" name="TEN_utilityBillThrough${utilityServiceDetailList.id}" readonly="true" value="${formattedUtilityBillThroughDate}" />
    		<img id="TEN_utilityBillThrough${utilityServiceDetailList.id}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateUtilityServiceBillThroughDate('${utilityServiceDetailList.id}','TEN_utilityBillThrough');"/>
    	</display:column>
    	
    	<display:column  title="Utility&nbsp;End&nbsp;Date" style="width:90px;">
    	 	<fmt:parseDate pattern="yyyy-MM-dd" value="${utilityServiceDetailList.TEN_utilityEndDate}" var="parsedUtilityEndDate" />
			<fmt:formatDate pattern="dd-MMM-yy" value="${parsedUtilityEndDate}" var="formattedUtilityEndDate" />    
    		<input type="text" class="input-textUpper" style="text-align:right;width:60px" id="TEN_utilityEndDate${utilityServiceDetailList.id}" name="TEN_utilityEndDate${utilityServiceDetailList.id}" readonly="true" value="${formattedUtilityEndDate}" />
    		<img id="TEN_utilityEndDate${utilityServiceDetailList.id}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="updateUtilityServiceBillThroughDate('${utilityServiceDetailList.id}','TEN_utilityEndDate');"/>
    	</display:column>
    
	</display:table></fieldset>
</c:if>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="trackingUrl"/>


<script type="text/javascript">
/* setOnSelectBasedMethods(["updateBillThroughDate()"]);
setCalendarFunctionality(); */ 
</script>

<script type="text/javascript">

function onlyFloatNumsAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==110) || (keyCode==67) || (keyCode==86); 
}
function checkFloat(temp,targetFieldId){ 
		var check='';  
		var i; 
		var s = temp.value;
		var fieldName = temp.name;  
		if(temp.value>100){
		alert("You cannot enter more than 100% VAT ")
		document.getElementById("TEN_utilityVatPercent"+targetFieldId).select();
		document.getElementById("TEN_utilityVatPercent"+targetFieldId).value='0.00'; 
		return false;
		} 
		
		var count = 0;
		var countArth = 0;
		for (i = 0; i < s.length; i++)
		{   
		    var c = s.charAt(i); 
		    if(c == '.')  {
		    	count = count+1
		    }
		    if(c == '-')    {
		    	countArth = countArth+1
		    }
		    if((i!=0)&&(c=='-')) 	{
		   	  alert("Invalid data in vat%." ); 
		   	document.getElementById("TEN_utilityVatPercent"+targetFieldId).select();
		   	document.getElementById("TEN_utilityVatPercent"+targetFieldId).value=''; 
		   	  return false;
		   	} 
		    if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))   {
		    	check='Invalid'; 
		    }  } 
		if(check=='Invalid'){ 
		alert("Invalid data in vat%." ); 
		ddocument.getElementById("TEN_utilityVatPercent"+targetFieldId).select();
		document.getElementById("TEN_utilityVatPercent"+targetFieldId).value=''; 
		return false;
		}  else{
		s=Math.round(s*100)/100;
		var value=""+s;
		if(value.indexOf(".") == -1){
		value=value+".00";
		} 
		if((value.indexOf(".")+3 != value.length)){
		value=value+"0";
		}
		document.getElementById("TEN_utilityVatPercent"+targetFieldId).value=value;
		updateUtilityService(targetFieldId,'TEN_utilityVatPercent',document.getElementById("TEN_utilityVatPercent"+targetFieldId).value);
		return true;
	}  
}
function getVatPercent1(targetFieldId){
	<c:if test="${systemDefaultVatCalculation=='true'}">
	     <c:forEach var="entry" items="${euVatPercentList}">
	        if(document.getElementById("TEN_utilityVatDesc"+targetFieldId).value=='${entry.key}') {
	        	document.getElementById("TEN_utilityVatPercent"+targetFieldId).value='${entry.value}'; 
	       	}
	    </c:forEach>
	      if(document.getElementById("TEN_utilityVatDesc"+targetFieldId).value==''){
	    	  document.getElementById("TEN_utilityVatPercent"+targetFieldId).value=0;
	      }
      updateUtilityService(targetFieldId,'TEN_utilityVatPercent',document.getElementById("TEN_utilityVatPercent"+targetFieldId).value);
     </c:if>
}
function updateUtilityService(id,fieldName,targetElement){
		var fieldValue = "";
		fieldValue = targetElement.value;
		if(fieldValue==undefined){
			fieldValue = targetElement;
		}
		var updateQuery = "update TenancyUtilityService set "+fieldName+"='"+fieldValue+"' where id="+id+" ";
		new Ajax.Request('/redsky/updateUtilityServiceAjax.html?ajax=1&updateQuery='+updateQuery+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			    var response = transport.responseText || "no response text";
			    },
			    onFailure: function(){ 
				    }
			  });
	}
function updateUtilityServiceVendorName(id,fieldName,targetElement){
	var fieldValue = "";
	fieldValue = targetElement;
	if(fieldValue.indexOf("&")){
		fieldValue = fieldValue.replace("&", "~");
	}
	new Ajax.Request('/redsky/updateUtilityServiceAjax.html?ajax=1&vendorCodeVal='+fieldValue+'&serviceOrderId='+id+'&decorator=simple&popup=true',
		  {
		    method:'get',
		    onSuccess: function(transport){
		    var response = transport.responseText || "no response text";
		    },
		    onFailure: function(){ 
			    }
		  });
}
function updateUtilityServiceByPopup(){
	updateUtilityService(tempId,'TEN_utilityVendorCode',document.getElementById("TEN_utilityVendorCode"+tempId).value);
	updateUtilityServiceVendorName(tempId,'TEN_utilityVendorName',document.getElementById("TEN_utilityVendorName"+tempId).value);
}
var tempId="";
function findVendorCode(vendorCode, vendorName,id){
	tempId=id;
	openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+vendorName+'&fld_code='+vendorCode);
}
var targetField="";
function updateUtilityServiceBillThroughDate(id,field){
	tempId=id;
	targetField = field;
}
function updateBillThroughDate(){
	var date = document.getElementById(targetField+tempId).value;
	var day = '';
    var month = '';
    var year = '';
    var finalDate ='';
    var billDate = null;
	 var mySplitResult = date.split("-");
	    day = mySplitResult[0];
	    month = mySplitResult[1];
	    year = mySplitResult[2];
	  if(month == 'Jan')  { month = "01";
	   }  else if(month == 'Feb') { month = "02";
	   } else if(month == 'Mar') { month = "03"
	   }   else if(month == 'Apr')  { month = "04"
	   }  else if(month == 'May') { month = "05"
	   }  else if(month == 'Jun') { month = "06"
	   }  else if(month == 'Jul') { month = "07"
	   } else if(month == 'Aug') {month = "08"
	   } else if(month == 'Sep')  {month = "09"
	   } else if(month == 'Oct')  {month = "10"
	   }  else if(month == 'Nov')  { month = "11"
	   } else if(month == 'Dec')  {month = "12";
	   }
	    
    	var yr="20";
		var time="00:00:00";
		billDate = yr+year+"-"+month+"-"+day+" "+time;
	updateUtilityService(tempId,targetField,billDate);
}

function findUtilityVendor(nameId,CodeId,DivId,partnerTypeWhereClause,id,evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;  
	if(keyCode!=9){
		var nameValue=document.getElementById(nameId).value;
		if(nameValue.length<=1){
			document.getElementById(CodeId).value="";	
		}
		
		if(nameValue.length>=3){
			$.get("partnerDetailsPricingAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", 
					{partnerNameAutoCopmlete: nameValue,partnerNameId:nameId,paertnerCodeId:CodeId,autocompleteDivId:DivId,autocompleteCondition:partnerTypeWhereClause,pricingAccountLineId:id},
					function(data){
						document.getElementById(DivId).style.display = "block";
						$("#"+DivId).html(data);
					});	
		}else{
			document.getElementById(DivId).style.display = "none";	
		}
	}else{
		document.getElementById(DivId).style.display = "none";
	}
}

function closeMypricingDiv(autocompleteDivId,partnerNameId,paertnerCodeId){	
	document.getElementById(autocompleteDivId).style.display = "none";	
	if(document.getElementById(paertnerCodeId).value==''){
		document.getElementById(partnerNameId).value="";	
	}
}

function copyPartnerPricingDetails(partnercode,lastName,vendorName,vendorCode,autocompleteDivId,id){
	lastName=lastName.replace("~","'");
	document.getElementById(vendorName).value=lastName;
	document.getElementById(vendorCode).value=partnercode;
	document.getElementById(autocompleteDivId).style.display = "none";
	updateUtilityService(id,'TEN_utilityVendorCode',document.getElementById("TEN_utilityVendorCode"+id).value);
	updateUtilityService(id,'TEN_utilityVendorName',document.getElementById("TEN_utilityVendorName"+id).value);
} 
</script>