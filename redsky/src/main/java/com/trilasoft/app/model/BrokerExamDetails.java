package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="brokerexamdetails")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class BrokerExamDetails extends BaseObject {
	
	private Long id;
	private String corpId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn; 
	private String sequenceNumber;
	private String shipNumber;
	private Long serviceOrderId;
	private String ship;
	private String examName;
    private String examLocation;
    private Date dateIn;
    private Date dateOut;
    private BigDecimal amount= new BigDecimal(0);
    private String billOrCod;
    private ServiceOrder serviceOrder;

	@Override
	public String toString() {
		return new ToStringBuilder(this)
		.append("corpId", corpId)
		.append("id",id)
		.append("serviceOrderId", serviceOrderId)
		.append("sequenceNumber", sequenceNumber)
		.append("ship", ship)
		.append("shipNumber", shipNumber)
		.append("createdBy", createdBy)
		.append("updatedBy", updatedBy)
		.append("createdOn", createdOn)
		.append("updatedOn", updatedOn)
		.append("examName", examName)
		.append("examLocation", examLocation)
		.append("dateIn", dateIn)
		.append("dateOut", dateOut)
		.append("amount", amount)
		.append("billOrCod", billOrCod)
		.toString();
}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof BrokerExamDetails))
			return false;
		BrokerExamDetails castOther = (BrokerExamDetails) other;
		return new EqualsBuilder()
				.append(corpId, castOther.corpId)
				.append(id,castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship,castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(createdBy,castOther.createdBy)
				.append(updatedBy,castOther.updatedBy)
				.append(createdOn,castOther.createdOn)
				.append(updatedOn,castOther.updatedOn)
				.append(examName, castOther.examName)
				.append(examLocation, castOther.examLocation)
				.append(dateIn, castOther.dateIn)
				.append(dateOut, castOther.dateOut)
				.append(amount, castOther.amount)
				.append(billOrCod, castOther.billOrCod)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(corpId)
				.append(id)
				.append(serviceOrderId)
				.append(sequenceNumber)
				.append(ship)
				.append(shipNumber)
				.append(createdBy)
				.append(updatedBy)
				.append(createdOn)
				.append(updatedOn)
				.append(examName)
				.append(examLocation)
				.append(dateIn)
				.append(dateOut)
				.append(amount)
				.append(billOrCod)
				.toHashCode();
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public String getShip() {
		return ship;
	}

	public void setShip(String ship) {
		this.ship = ship;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getExamLocation() {
		return examLocation;
	}

	public void setExamLocation(String examLocation) {
		this.examLocation = examLocation;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public String getBillOrCod() {
		return billOrCod;
	}

	public void setBillOrCod(String billOrCod) {
		this.billOrCod = billOrCod;
	}
	@ManyToOne@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

}
