package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.UniversalManagerImpl;

import com.trilasoft.app.dao.PagingLookupDao;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class PagingLookupManagerImpl extends UniversalManagerImpl implements
        PagingLookupManager {
    private PagingLookupDao pagingDao;
    
    public PagingLookupManagerImpl (){
    	System.out.print("");
    }

    /**
     * Method that allows setting the DAO to talk to the data store with.
     * 
     * @param dao
     *            the dao implementation
     */
    public void setLookupDao(PagingLookupDao pagingDao) {
        super.dao = pagingDao;
        this.pagingDao = pagingDao;
    }

    @SuppressWarnings("unchecked")
    public ExtendedPaginatedList getAllRecordsPage(Class clazz,
            ExtendedPaginatedList paginatedList, List<SearchCriterion> searchCriteria) {
        List results = pagingDao.getAllRecordsPage(clazz, paginatedList
                .getFirstRecordIndex(), paginatedList.getPageSize(), paginatedList
                .getSortDirection(), paginatedList.getSortCriterion(), searchCriteria);
        paginatedList.setList(results);
        paginatedList.setTotalNumberOfRows(pagingDao.getAllRecordsCount(clazz, searchCriteria));
        return paginatedList;
    }

    public PagingLookupDao getPagingDao() {
        return pagingDao;
    }

    public void setPagingDao(PagingLookupDao pagingDao) {
        this.pagingDao = pagingDao;
    }

}
