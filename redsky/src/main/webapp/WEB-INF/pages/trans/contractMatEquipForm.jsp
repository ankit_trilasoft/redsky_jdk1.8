<%@ include file="/common/taglibs.jsp"%>   

<head>   
    
<title>Resource Details</title>
<meta name="heading" content="Resource Details"/> 
<style>
input[type="checkbox"] {margin:0px;padding:0px;}
</style>
<script type="text/javascript">
	window.onload = function() {
		<% int count=0; %>
	}
</script> 
</head>
<s:form id="itemJEquipForm" name="itemJEquipForm" action="" method="post" validate="true"> 
<s:hidden key="itemsJEquip.id"/> 
<s:hidden name="tempChargeId"/>
	<s:hidden name="type" value="<%=request.getParameter("type") %>"/>
    <s:hidden name="descript" value="<%=request.getParameter("descript") %>"/>
    <s:hidden name="controlled" value="<%=request.getParameter("controlled") %>"/>  
    <s:hidden key="resourceContractCharges.id"/>
<div id="layer4" style="width:100%;">
<div id="newmnav">
		<ul>
		 <li id="newmnav1" style="background:#FFF"><a  class="current"><span>Resource Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>	
  			<li><a href="contractMaters.html"><span>Resource List</span></a></li>
  			<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${itemsJEquip.id}&tableName=itemsjequip&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
  		</ul>
		</div><div class="spn">&nbsp;</div>
</div>
<div id="Layer1" style="width:100%;" onkeydown="changeStatus();"> 
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
					<tbody>
						<tr>							
							<!--<td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.contract'/></td>
							<td align="left"><s:textfield name='itemsJEquip.contract' size="25" maxlength="100" required="true" cssClass="input-text" tabindex="1"/></td>-->
							<td align="right" class="listwhitetext" width="75">Category&nbsp;Type<font color="red" size="2">*</font></td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="itemsJEquip.type" list="%{resourceCategory}" cssStyle="width:155px" tabindex="" />
					<%-- 	<td width="50px"/> --%>	
							<td align="right" class="listwhitetext" width="200"><fmt:message key='itemsJEquip.descript'/><font color="red" size="2">*</font></td>
							<td align="left" class="listwhitetext"><s:textfield name="itemsJEquip.descript" onkeyup="checkChar();" onchange="checkDupliResource()" size="30" maxlength="50" required="true" cssClass="input-text" tabindex="3"/></td>
						</tr>
						<tr><td style="height:8px;"></td>
						</tr>
						<tr>
						<td colspan="10">
						<fieldset>
						<legend>Access Control</legend>
						<table class="detailTabLabel" cellspacing="5" cellpadding="0" border="0">						
							<tr>								
							<td align="right" class="listwhitetext" width="">Controlled</td>
							<td class="listwhitetext" valign="bottom"><s:checkbox key="itemsJEquip.controlled" fieldValue="true" /></td>					
							<td align="right" class="listwhitetext" style="padding-left:10px;">Template</td>
							<td class="listwhitetext" valign="bottom"><s:checkbox key="itemsJEquip.template" fieldValue="true" /></td>
							
							<td align="right" class="listwhitetext" style="padding-left:10px;">Frequently&nbsp;Used&nbsp;Resources</td>
                            <td class="listwhitetext" valign="bottom"><s:checkbox key="itemsJEquip.frequentlyUsedResources" fieldValue="true" /></td>
                            
                            <td align="right" class="listwhitetext" style="padding-left:10px;">Resources&nbsp;For&nbsp;Account&nbsp;Portal</td>
                            <td class="listwhitetext" valign="bottom"><s:checkbox key="itemsJEquip.resourceForAccPortal" fieldValue="true" /></td>
							
							<td align="right" class="listwhitetext" style="padding-left:10px;">Print On Form</td>
							<td class="listwhitetext" valign="bottom"><s:checkbox key="itemsJEquip.printOnForm" fieldValue="true" /></td>
							</tr>
						</table>
						</fieldset>
							
							</td>
						</tr>
						<tr><td style="height:10px; !height:25px;"></td></tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Price</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:0px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td colspan="5">
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0">
					<tbody>
					<tr><td height="3px"></td></tr>
					<sec-auth:authComponent componentId="module.button.contractCharge.ResourcePrice" >
					<tr><td align="left" colspan="10">
						<display:table name="contractChargeList" class="table" requestURI="" id="contractCharge"  pagesize="50" style="width:100%;margin-bottom:5px;">
						   <c:if test="${empty  contractCharge.id}">
							    <display:column title="Contract" > 
							    <s:textfield name="resourceContractCharges.contract" id="contract" value="${contractCharge.contract}" onchange="setCharge(this,'charge');" size="35" readonly="true" maxlength="100" required="true" cssClass="input-text" tabindex="1"/>
								<img id="contractMatEquip.contract" align="middle" style="vertical-align:top;" class="openpopup" width="17" height="20" onclick="openContractPopWindow();document.forms['itemJEquipForm'].elements['itemsJEquip.contract'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
								<% count++; %>
							    </display:column>
							    <display:column title="Charge" > 
							    <s:textfield name="resourceContractCharges.charge" id="charge" value="${contractCharge.charge}" readonly="true" size="25" maxlength="100" required="true" cssClass="input-text" tabindex="1"/>
								<img id="contractMatEquip.charge" align="middle" style="vertical-align:top;" class="openpopup" width="17" height="20" onclick="openChargesPopWindow();document.forms['itemJEquipForm'].elements['contractChargeList.charge'].focus();" src="<c:url value='/images/open-popup.gif'/>" />
							    </display:column>
						    </c:if>
						    <c:if test="${not empty  contractCharge.id}">
							    <display:column title="Contract" > 
							    <s:textfield name="resourceContractCharges.contract_${contractCharge.id}" id='itemsJEquip.contract_${contractCharge.id}' onchange="setCharge(this,'itemsJEquip.charge_${contractCharge.id}');" value="${contractCharge.contract}" readonly="true" size="35" maxlength="100" required="true" cssClass="input-text" tabindex="1"/>
								<img id="contractMatEquip.contract" style="vertical-align:top;" align="middle" class="openpopup" width="17" height="20" onclick="openContractPopWindow(${contractCharge_rowNum},${contractCharge.id});" src="<c:url value='/images/open-popup.gif'/>" />
								<% count++; %>
							    </display:column>
							    <display:column title="Charge" > 
							    <s:textfield name="resourceContractCharges.charge_${contractCharge.id}" id="itemsJEquip.charge_${contractCharge.id}" value="${contractCharge.charge}" readonly="true" size="25" maxlength="100" required="true" cssClass="input-text" tabindex="1"/>
								<img id="contractMatEquip.charge" style="vertical-align:top;" align="middle" class="openpopup" width="17" height="20" onclick="openChargesPopWindow(${contractCharge_rowNum},${contractCharge.id});" src="<c:url value='/images/open-popup.gif'/>" />
							    </display:column>
							     <display:column title="Remove" style="width: 15px;">
									<a><img align="middle" title="User List" onclick="confirmSubmit('${contractCharge.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
								</display:column>
						    </c:if>
						</display:table>
							<input type="button" class="cssbutton1" onclick="addLine();" value="Add" style="width:55px; height:25px" tabindex="83"/>
<%-- 						<% if(count > 0){ %> 
								<input type="button" class="cssbutton1" onclick="saveContractGrid()" value="Save" style="width:80px; height:25px" tabindex="83"/>
							<%} %>
--%>	
					</td>
					</tr>
					</sec-auth:authComponent>
					<tr>
						<td colspan="10">
							
						</td>
					</tr>
						<tr>
						<td width="10px" ></td>
							<td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.qty'/></td>
							<td align="left" class="listwhitetext"><s:textfield cssStyle="text-align:right" name="itemsJEquip.qty" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" required="true" size="2" maxlength="5"cssClass="input-text" tabindex="5"/> </td>
			               <td align="right" class="listwhitetext" width="67px"><fmt:message key='itemsJEquip.otCharge'/></td>
					       <td><s:textfield size="2" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="5" name="itemsJEquip.otCharge"  cssClass="input-text" tabindex="6"/></td> 			 
			               <td align="right" class="listwhitetext" width="70px"><fmt:message key='itemsJEquip.charge'/></td>
					   	   <td><s:textfield size="2" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"  onkeyup="valid(this,'special')" maxlength="5" name="itemsJEquip.charge"  cssClass="input-text" tabindex="7"/>  </td> 			 
			          	   <td align="right" class="listwhitetext" width="70px"><fmt:message key='itemsJEquip.dtCharge'/></td>
			           	   <td><s:textfield size="2" cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"   onkeyup="valid(this,'special')" maxlength="5"  name="itemsJEquip.dtCharge"  cssClass="input-text" tabindex="8"/>  </td> 			 
			               </tr>
			               
			               <tr>
			               <td width="10px"></td>
			               <td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.packLabour'/></td>
			               <td><s:textfield size="2" cssStyle="text-align:right"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);"  onkeyup="valid(this,'special')" maxlength="5" name="itemsJEquip.packLabour"  cssClass="input-text" tabindex="9"/>  </td> 			 
			               <td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.cost'/><font color="red" size="2">*</font></td>
			               <td><s:textfield cssStyle="text-align:right" size="2"  maxlength="6"  name="itemsJEquip.cost"  onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')"  onchange="fillCost();" cssClass="input-text" tabindex="10"/>  </td> 			 
			               <td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.bucket'/></td>
			               <td><s:textfield cssStyle="text-align:right" size="2"  onkeydown="return onlyNumsAllowed(event)"   maxlength="5" name="itemsJEquip.bucket"   cssClass="input-text" tabindex="11"/>  </td> 			            
				            <td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.payCat'/></td>
				            <td><s:textfield cssStyle="text-align:right" onkeydown="return onlyNumsAllowed(event)" onblur="return isFloat(this);" onkeyup="valid(this,'special')" maxlength="2" size="2" name="itemsJEquip.payCat"  cssClass="input-text" tabindex="12"/>  </td> 			 
				            </tr>
		   				<tr>
		   				<td width="10px"></td>
				            <td align="right" class="listwhitetext"><fmt:message key='itemsJEquip.gl'/></td>
						    <td colspan="3"><s:textfield cssStyle="text-align:right;width:171px;!width:166px"   onkeyup="valid(this,'special')" maxlength="20"  name="itemsJEquip.gl"  cssClass="input-text" tabindex="13"/></td>
						    <td colspan="" align="right" class="listwhitetext"><fmt:message key='itemsJEquip.glType'/></td>			 
				            <td colspan="2"><s:textfield cssStyle="text-align:right" size="10" onkeydown="return onlyNumsAllowed(event)" onkeyup="valid(this,'special')" maxlength="7"  name="itemsJEquip.glType"  cssClass="input-text" tabindex="14"/>  </td> 			 
					    </tr>
					    <tr><td height="10px"></td></tr>
		    		</tbody>
		    	</table>
		    </td>
		</tr>
	</tbody>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='itemsJEquip.createdOn'/></b></td>
							<td valign="top">
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${itemsJEquip.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="itemsJEquip.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${itemsJEquip.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='itemsJEquip.createdBy' /></b></td>
							<c:if test="${not empty itemsJEquip.id}">
								<s:hidden name="itemsJEquip.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{itemsJEquip.createdBy}"/></td>
							</c:if>
							<c:if test="${empty itemsJEquip.id}">
								<s:hidden name="itemsJEquip.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='itemsJEquip.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${itemsJEquip.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="itemsJEquip.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${itemsJEquip.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='itemsJEquip.updatedBy' /></b></td>
							<c:if test="${not empty itemsJEquip.id}">
								<s:hidden name="itemsJEquip.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{itemsJEquip.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty itemsJEquip.id}">
								<s:hidden name="itemsJEquip.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
</div>

<c:set var="button1">
<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" key="button.save" onclick="return saveMatEquip();"/>   
<s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset" tabindex="20"/> 
</c:set>
<c:out value="${button1}" escapeXml="false" /> 
</div> 
<s:hidden name="description"/>
<s:hidden name="secondDescription"/>
<s:hidden name="thirdDescription"/>
<s:hidden name="fourthDescription"/>
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription" />
</s:form>  

<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<SCRIPT LANGUAGE="JavaScript">

String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}

function validate(){
	var descript=document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value;
	var type=document.forms['itemJEquipForm'].elements['itemsJEquip.type'].value;
	 if(type==''){
	     alert('Category Type is Required Field');
	    return false;
	}else if(descript== ''){
			alert('Resource is Required Field');
			return false;
	} else {
		return fillCost();
	}
}
function fillCost(){
	if(document.forms['itemJEquipForm'].elements['itemsJEquip.cost'].value == ''){
		alert('Please fill the Cost per Unit.');
		return false;
	}
}

function changeStatus(){
    document.forms['itemJEquipForm'].elements['formStatus'].value = '1';
}

function onlyNumsAllowed(evt, strList, bAllow){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==110); 
}
function onlyTimeFormatAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
}
function isFloat(targetElement){   
	var i;
	var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        if (c == ".") {
        
        }else{
        alert("Only numbers are allowed here");
        //alert(targetElement.id);
        document.getElementById(targetElement.id).value='';
        document.getElementById(targetElement.id).select();
        return false;
        }
    }
  }
  return true;
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

function checkDupliResource (){	
	   var descript = document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value; 
	   if(descript!='') { 
		   var url="duplicateResource.html?ajax=1&decorator=simple&popup=true&descript="+encodeURI(descript);
		   http51.open("GET", url, true); 
		   http51.onreadystatechange = handleHttp90; 
		   http51.send(null); 
	   }
	 }
function handleHttp90(){
 if (http51.readyState == 4){
   var results = http51.responseText;
   results = results.trim();
   results=results.replace('[','');
   results=results.replace(']','');
   if(results == 'Y'){    	  
     	alert('A Record for this Resource already exist. Please select a new Resource.');
   	document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value='';
   }else{
 	  document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].selected=true;
   }
 }
} 
var http51 = getHTTPObject();
function getHTTPObject()
{
 var xmlhttp;
 if(window.XMLHttpRequest)
 {
     xmlhttp = new XMLHttpRequest();
 }
 else if (window.ActiveXObject)
 {
     xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
     if (!xmlhttp)
     {
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
     }
 }
 return xmlhttp;
}
</script>

<script type="text/javascript">
function openContractPopWindow(str,id){
	if(str == undefined){
		var contract = 'contract';
		javascript:openWindow('openContractPopup.html?itemType='+"<%=request.getParameter("itemType")%>"+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code='+contract);
//		document.getElementById('charge').value='';
		document.forms['itemJEquipForm'].elements['tempChargeId'].value='charge';
	}else{
		var contract = 'itemsJEquip.contract_'+id;
		javascript:openWindow('openContractPopup.html?itemType='+"<%=request.getParameter("itemType")%>"+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code='+contract);
		var charge = 'itemsJEquip.charge_'+id;
		document.forms['itemJEquipForm'].elements['tempChargeId'].value=charge;
	}
}

function resetChargeTemp(){
	var contractVal = document.forms['itemJEquipForm'].elements['tempChargeId'].value;
	document.getElementById(contractVal).value="";	
}
function openChargesPopWindow(str,id){
	if(str == undefined){
		var contract = 'contract';
		var charge = 'charge';
		var contractVal = document.forms['itemJEquipForm'].elements[contract].value;
		if(contractVal.trim().length > 0)
			javascript:openWindow('openChargesPopup.html?contractType=' + escape(contractVal) + '&itemType='+"<%=request.getParameter("itemType")%>"+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code='+charge);
		else{
			alert("Please Select a Contract Type");
			return false;
		}
	}else{
		var contract = 'itemsJEquip.contract_'+id;
		var charge = 'itemsJEquip.charge_'+id;
		var contractVal = document.forms['itemJEquipForm'].elements[contract].value;
		if(contractVal != null && contractVal !='')
			javascript:openWindow('openChargesPopup.html?contractType=' + escape(contractVal) + '&itemType='+"<%=request.getParameter("itemType")%>"+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code='+charge);
		else{
			alert("Please Select a Contract Type");
			return false;
		}
	}
}

function addLine(){
	var id = '${itemsJEquip.id}';
	if(id.length == 0){
		alert('Please Save Resource first before adding contracts & charges.');
		return false;
	}
	if(document.getElementById('contract') != null){
		var contract = document.getElementById('contract').value;
		if(contract.trim().length == 0){
			return;
		}
	}
	var contractCharge = getContractChargeValue();
	contractCharge = contractCharge.split('&').join('~');
	var url = "addRowToContractChargeGrid.html?parentId=${itemsJEquip.id}&contractCharge="+contractCharge+"&btnType=addrow"
	document.forms['itemJEquipForm'].action = url;
	document.forms['itemJEquipForm'].submit();
}

function getContractChargeValue(){
	var contractChrg='';
	<c:forEach items="${contractChargeList}" var="name"> 
		var id = '${name.id}';
		var conId = 'itemsJEquip.contract_'+id;
		var chargeId = 'itemsJEquip.charge_'+id;
		if(document.getElementById(conId) != null){
			var contract = document.getElementById(conId).value;
			var charge = document.getElementById(chargeId).value;
			if(contract.trim().length > 0){
				contractChrg = contractChrg+'@'+contract+'*'+charge;
			}
		}
	</c:forEach>

 if(document.getElementById('contract') != null && document.getElementById('contract').value != null){
	 var contract = document.getElementById('contract').value;
	 var charge = document.getElementById('charge').value;
	 if(contract.trim().length >0){
		 contractChrg = contractChrg+'@'+contract+'*'+charge;
	 }
	 
 }
 return contractChrg;
}

function saveContractGrid(){
	var no='<%= count %>';
	var contractCharge = getContractChargeValue();
	if(contractCharge.trim().length == 0){
		alert('Enter Contract');
	}else{
		var url = "saveContractChargeGrid.html?parentId=${itemsJEquip.id}&contractCharge="+contractCharge+"&btnType=addrow";
		document.forms['itemJEquipForm'].action = url;
		document.forms['itemJEquipForm'].submit();
	}
}

function saveMatEquip(){
	var status =  validate();
	if(status != false){
		var contractCharge = getContractChargeValue();
		var url = "saveMatEquip.html?contractCharge="+contractCharge;
		document.forms['itemJEquipForm'].action = url;
		document.forms['itemJEquipForm'].submit();
	}else{
		return false;
	}
}

function setCharge(contract, chargeId){
	if(contract.value != null || contract.value != ''){
		 document.getElementById(chargeId).value='';
	}
}

function confirmSubmit(contractId){
	var agree=confirm("Are you sure you wish to remove this Contract ?");
	if (agree){
		var url = 'deleteContractCharge.html?id=${itemsJEquip.id}&contractId='+contractId;
		location.href = url;
	 }else{
		return false;
	}
}

function blankCharge(chargeId){
//	document.getElementById(chargeId).value = '' ;
}

function checkChar() {
	var capString = document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value;
	
	 valid(document.forms['itemJEquipForm'].elements['itemsJEquip.descript'],'special');
		 var chArr = new Array("%","&","'","|","\\","\"");
	 	 var origString = document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value;
	 	for ( var i = 0; i < chArr.length; i++ ) {
	 		origString = origString.split(chArr[i]).join(''); 
	 	}
	 	document.forms['itemJEquipForm'].elements['itemsJEquip.descript'].value = origString;
}
</script>