/**
 * Business Service Interface to handle communication between web and persistence layer. Gets WorkTicket information based on WorkTicket id.
 * 
 * @param WorkTicketId
 *             the WorkTicket's id This interface represents the basic actions on "WorkTicket" object in Redsky that allows for WorkTicket of Shipment.
 * @Interface Name WorkTicketManager
 * @Author Sangeeta Dwivedi
 * @Version V01.0
 * @Since 1.0
 * @Date 01-Dec-2008
 */

package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;

public interface WorkTicketManager extends GenericManager<WorkTicket, Long> {
	
	public List<WorkTicket> findByLastName(String lastName);
	public List<WorkTicket> findByTicket(Long ticket, String lastName, String firstName, String shipNumber, String warehouse, String service, Date date1, Date date2);
	public Long findMaximum(String corpId);
	public List findMaximumId();
	public List findMaximumship(String shipNumber);
	public Set<WorkTicket> findByCustomerTicket(Long ticket, String sequenceNumber);
	public List getWarehouse(String shipNumber);
	public List findAllTickets();
	public List findContainer(String containerNumber, String shipNumber);
	public List findBillComlete(String shipNumber);
	public String findTotalEstimatedCartons(Long ticket);
	public List getStorageList(String shipNumber);
	public List findBillingPayMethod(String shipNumber);
	public List findBillingPayMethods(String payMethod);
	public List getDailyOperationalLimit(Date date1, Date date2, String corpId);
	public Boolean checkDate1(String warehouse, String service, Date date1, Long id, String corpId, Double estimatedVolume, Double estimatedWeight, Date date2);
	public Boolean getDateDiff(Date date1, Long minServiceDay);
	public String getDailyOpsLimit(Date date1, Date date2, Long dailyCount, Long dailyCountPC, String sessionCorpID, String hubID, Double estimatedWeight, Double estimatedCubicFeet, Double dailyMaxEstWt, Double dailyMaxEstVol, Long id);
	public Boolean getCheckedWareHouse(String wareHouse, String corpId);
	public List getWorkPlanList(Date fromDate, Date toDate, String hubID,String wHouse,String wareHouseUtilization, String corpId);
	public List findMaximumTicket(String shipNm);
	public List findMinimumTicket(String shipNm);
	public List findCountTicket(String shipNm);
	public List goSOChild(Long serviceOrderId, String corpID, Long id);
	public List goNextSOChild(Long serviceOrderId, String corpID, Long ticket);
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long ticket);
	public List<WorkTicket> getWorkTicketByTicekt(Long ticket);
	public void updateServiceOrderInland(String vendorCode, Long id, String sessionCorpID);
	public void updateTrackingStatusBeginLoad(Date date1, String shipNumber, String sessionCorpID);
	public void updateTrackingStatusLoadA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus, String loginUser);
	public void updateBillingStorageOut(Date date1, String shipNumber, String sessionCorpID);
	public List countReleaseStorageDate(String shipNumber);
	public void updateTrackingStatusDeliveryA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus);
	public void updateStorageReleasedDate(String shipNumber, String sessionCorpID, String updatedBy);
	public void updateTrackingStatusDeliveryShipper(Date date1, String shipNumber, String sessionCorpID);
	public List checkCompanyDivisionAgentCode(String corpID);
	public void updateTrackingStatusLeftWhon(Date date1, String shipNumber, String sessionCorpID);
	public void updateTrackingStatusPackA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus);
	public List locationReleaseStorageDate(String shipNumber, String sessionCorpID);
	public String getStateDesc(String check);
	public List getStateDescList(String originCountry);
	public List getDispathMapList(Date date1, Date date2, String sessionCorpID, Boolean originMap,Boolean destinMap,String hubId);
	public void updateBillingGSTAccess1Service(Date date1, String shipNumber, String sessionCorpID);
	public void updateBillingGSTAccess2Service(Date date1, String shipNumber, String sessionCorpID);
	public void updateBillingGSTAccess3Service(Date date1, String shipNumber, String sessionCorpID);
	public OperationsHubLimits getOperationsHub(String warehouse, String sessionCorpID);
	public List getHubIdList(String sessionCorpID, String hubId);
	public List getOpsList(Date fromDate, String hubID, String corpID);
	public List actualizedTicketWareHouse(String wareHouse, String shipNumber, String sessionCorpID);
	public void billingWareHouse(String wareHouse, String shipNumber, String sessionCorpID);
	public List findWorkTicketID(String ticket,String corpId);
	public List findWorkTicketStorageList(Date date1, Date date2, String service1, String warehouse, String sessionCorpID);
	public String findAccountingHold(String billToCode);
	public List findStorageUnit(Long ticket, String shipNumber);
	public void updateStoForHandout(String handout, String ids, Long hoTicket);
	public List findWorkTicketService(Long ticket, String sessionCorpID);
	public void updateTicketStatus(Long ticket, String sessionCorpID);
	public String getWorkTicketIdByStorageTicket(Long ticket, String sessionCorpID);
	public void changeHandoutAccess(Long ticket, String sessionCorpID);
	public List totalHandOutVolumePieces(Long ticket, String ids, String sessionCorpID);
	public List handoutWeightandVolume(Long ticket, String shipNumber,String sessionCorpID);
	public String getAddress(Long id, String sessionCorpID,String addType);
	public String getAddressDescription(Long cid, String sessionCorpID,String descType);
	public String getStandardAddressDescription(Long StdID,String sessionCorpID);
	public List getCrewNameByContractor(Long ticket, String sessionCorpID);
	public List getWorkTicketToTransfer(String shipNumber, String sessionCorpID);
	public List ticketTransfer(String shipNumber, String sessionCorpID,Long ticket,Long id);
	public List getdriverTicket(Date fromDate, Date toDate,String userParentAgent, String corpId);
	public List findWorkTicketResourceList(Long id);
	public void updateTicketStatusPending(String tickets, String sessionCorpID,String targetActual);
	public List findServiceDesc(String code,String sessionCorpID);
	public String findWorkTicketReview(String shipnum,String sessionCorpID);
	public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID);
	public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID);
	public List getLabel4Value(String ticketWarehouse,String sessionCorpID);
	public void updateResourceMiscellaneous(String tktService,String userName,String sequenceNumber,String sessionCorpID);
	public String findTargetActualByTicketNum(Long ticket,String sessionCorpID);
	public List getAllRecord(Long id, String sessionCorpID);
	public void updateBillToAuthority( String billToAuthority, Date authUpdated,Long id);
	public Long findRemoteWorkTicket(String ticket);
	public List getWorkTicketList(String serviceType,String wareHouse,Date date, String sessionCorpID,String mode,String military,String job,String billToCode);
	public List findCrewFirstLastName(Long ticket,String sessionCorpID);
	public String findVendorCodeWithName(String code, String sessionCorpID);
	public String findCoordinatorByShipNum(String shipNumber,String sessionCorpID);
	public String findWorkTicketIdList(String shipNumber,String sessionCorpID);
	public String getHubValueWorkPlanList(String hubID, String corpId);
	public String getCustomsTicketList(Long StdID, String corpId);
	public List findDailySheetList(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public String findCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public List findAvailableCrews(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public List searchAvailableTrucks(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public List findOriginAddWorkTicket(Long oId,String sessionCorpID);
	public String findOMDCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public void updateDailySheetNote(String wRHouse, Date dailySheetDate,String sessionCorpID,String wDailySheetNotes);
	public String findDailySheetNotes(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public List findAvailableCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket);
	public List findAssignedCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket);
	public List findAbsentCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID);
	public Long findUserDetailsFromCrew(String asgCrew,String sessionCorpID);
	public Long findUserDetailsFromTS(String crewValue,Date dailySheetDate,String sessionCorpID,String dSTicket);
	public List findAvailableTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String dSTicket);
	public List findAssignedTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket);
	public List findTruckDetailsFromTO(String truckNo,Date dailySheetDate,String sessionCorpID,String dSTicket);
	public Long findTruckDetailsFromTruck(String truckNo,String sessionCorpID);	
	public Map<String, List> getDSDispatch(String dailySheetwRHouse,Date dailySheetCalenderDate,String sessionCorpID);
	public void updateTrackingStatusSitDestinationIn(Date date1, Long id, String sessionCorpID);
	public Long findWorkTicketIdByTicket(String dSTicket,String sessionCorpID);
	public List findScheduleResourceId(String dSTicket,String wRHouse, Date dailySheetDate,String sessionCorpID);
	public String findAbsentCrew(String asgCrew, Date dailySheetDate,String sessionCorpID);
	public AccessInfo getAccessinfo(String sessionCorpID, Long sid);	
	public String findcrewNoAndCrewThreshHold(Date date1, Date date2,String wareHouse,String sessionCorpID,BigDecimal noOfCrewFromPeople,String oldTicketNumber);
	public BigDecimal findSumOfrequiredCrew(String oldTicketNumber,String wareHouse,Date date1, Date date2,String sessionCorpID);
	public BigDecimal findAbsentCrewFromtimeSheet(Date date1, Date date2,String sessionCorpID);
	public Map<String, String> findCrewCalendarList(String crewCalendarwRHouse,Date crewCalendarDate,String sessionCorpID);
	public Map<Map,Map> findRequiredCrewValue(Date date1, Date date2,String hub,String sessionCorpID);
	public void updateTicketStatusPackADetails(Date Date2, Long soId,String sessionCorpID);
	public String statusOfWorkTicket(Long ticket,String sessionCorpID);
	public Long getDailyOpsLimitForOi(Long dailyCount, String typeService,Date dateFormat, String hubID, String sessionCorpID);
	public List getAllTicketByDate(String typeService,Date dateFormat, String hubID, String sessionCorpID, String itemsjbkequipId);
	public List checkQuantityInOperation(String resorceTypeOI,String resorceDescriptionOI, String hubID, String sessionCorpID);
	public Long msgForThreshold(Long dailyCount,Long dailyCountPC,Date dateFormat, String hubID, String sessionCorpID);
	public List getServiceForOI( String ticketId,String sessionCorpID ,String shipNumber);
	public void updateStatusOfWorkTicket(String returnAjaxStringValue,String shipNumber, String corpID);
	public List workTicketList(Long serviceOrderId);
	public List planningCalendarList(String sessionCorpID,String wareHouses);
	public List getAllDataItemList(Long workTicketId, String shipNumber);
	public String getTruckNumber(Long workTicket, String sessionCorpID);
	public String getServiceAndWareHouseByTicket(String workTicket,String sessionCorpID);
	public void updateDateByTicket(Long ticket,Date date1,String sessionCorpID,String updatedBy,Boolean updateValue);
	public List planningCalendarListForUnip(String sessionCorpID,String wareHouses,String printStart,String printEnd);
	public String findCrewEmail(String wRHouse, Date dailySheetDate,String sessionCorpID,Long ticket);
}
