package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.ExtractColumnMgmt;
import com.trilasoft.app.model.NetworkDataFields;
import com.trilasoft.app.service.ExtractColumnMgmtManager;

public class ExtractColumnManagementAction  extends BaseAction{
	
	private String sessionCorpID;
	private ExtractColumnMgmt extractColumnMgmt;
	private ExtractColumnMgmtManager extractColumnMgmtManager;
	private Long id;
	private List reportNameList;
	private List tableNameList;
	private List fieldNameList;
	private List displayNameList;
	private List tableNameByReportNameList;
	private List extractColumnPropertiesList;
	private List corpIdList;
	private String reportName;
	private String tableName;
	private String fieldNameSearch;
	private String displayName;
	private Date createdOn;
	private String corpID;
	private String oldCorpId;
	private String corpIdSearch;
	
	
	public ExtractColumnManagementAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	@SkipValidation
	public String list(){
		extractColumnPropertiesList = extractColumnMgmtManager.searchDefaultList(sessionCorpID);
		getComboList(sessionCorpID);  
		return SUCCESS;
	}
	
	public String save(){
		getComboList(sessionCorpID);
		try{
			String corpId = extractColumnMgmt.getCorpID();
		boolean isNew = (extractColumnMgmt.getId() == null);
		if (isNew) {
			extractColumnMgmt.setCreatedOn(new Date());
			extractColumnMgmt.setCreatedBy(getRequest().getRemoteUser());
			if(sessionCorpID.equalsIgnoreCase("TSFT")){
				extractColumnMgmt.setCorpID(corpId);
			}else{
				extractColumnMgmt.setCorpID(sessionCorpID);
			}
			Long columnSequenceNumber = extractColumnMgmtManager.getExtractColMgmtSeqNo(extractColumnMgmt.getReportName());
			extractColumnMgmt.setColumnSequenceNumber(columnSequenceNumber);
			String fieldName = extractColumnMgmt.getFieldName();
			String displayFieldName = extractColumnMgmt.getDisplayName().trim();
			displayFieldName = displayFieldName.replace(" ", "_");
			extractColumnMgmt.setDisplayName(displayFieldName);
			fieldName = getFormatedExtractColMgmtFieldNameUpdate(fieldName, displayFieldName);
			extractColumnMgmt.setFieldName(fieldName);
			boolean flag1 = extractColumnMgmtManager.checkExtractColumnMgmtRecord(extractColumnMgmt.getReportName(), extractColumnMgmt.getTableName(), fieldName, extractColumnMgmt.getCorpID());
				if(flag1){
					extractColumnMgmt = extractColumnMgmtManager.save(extractColumnMgmt);
					String key = (isNew) ? "extractColumnMgmt.added" : "extractColumnMgmt.updated";
				    saveMessage(getText(key));
				}else{
					errorMessage("Not to save, because record all ready exist.");
					return INPUT;
				}
			} else {
				if(sessionCorpID.equalsIgnoreCase("TSFT")){
					if(oldCorpId.trim().equalsIgnoreCase(corpId.trim())){
						extractColumnMgmt.setCorpID(corpId);
					}else{
						Long columnSequenceNumber = extractColumnMgmtManager.getExtractColMgmtSeqNo(extractColumnMgmt.getReportName());
						extractColumnMgmt.setColumnSequenceNumber(columnSequenceNumber);
						extractColumnMgmt.setCorpID(corpId);
					}
				}else{
					extractColumnMgmt.setCorpID(sessionCorpID);
				}
				extractColumnMgmt.setUpdatedOn(new Date());
				extractColumnMgmt.setUpdatedBy(getRequest().getRemoteUser());
				String fieldName = extractColumnMgmt.getFieldName();
				String displayFieldName = extractColumnMgmt.getDisplayName().trim();
				displayFieldName = displayFieldName.replace(" ", "_");
				extractColumnMgmt.setDisplayName(displayFieldName);
				String formattedFieldName = getFormatedExtractColMgmtFieldNameUpdate(fieldName, displayFieldName);
				extractColumnMgmt.setFieldName(formattedFieldName);
				ExtractColumnMgmt extractColumnMgmt1 = extractColumnMgmtManager.get(extractColumnMgmt.getId());
				if(extractColumnMgmt.getReportName().equalsIgnoreCase(extractColumnMgmt1.getReportName()) && extractColumnMgmt.getTableName().equalsIgnoreCase(extractColumnMgmt1.getTableName()) && extractColumnMgmt.getFieldName().equalsIgnoreCase(extractColumnMgmt1.getFieldName()) && extractColumnMgmt.getCorpID().equalsIgnoreCase(extractColumnMgmt1.getCorpID())){
					extractColumnMgmt = extractColumnMgmtManager.save(extractColumnMgmt);
					String key = (isNew) ? "extractColumnMgmt.added" : "extractColumnMgmt.updated";
				    saveMessage(getText(key));
				}else{
					boolean flag1 = extractColumnMgmtManager.checkExtractColumnMgmtRecord(extractColumnMgmt.getReportName(), extractColumnMgmt.getTableName(), formattedFieldName, extractColumnMgmt.getCorpID());
					if(flag1){
						extractColumnMgmt = extractColumnMgmtManager.save(extractColumnMgmt);
						String key = (isNew) ? "extractColumnMgmt.added" : "extractColumnMgmt.updated";
					    saveMessage(getText(key));
					}else{
						String old_id =  extractColumnMgmtManager.checkExtractColumnMgmtRecordId(extractColumnMgmt.getReportName(), extractColumnMgmt.getTableName(), formattedFieldName, extractColumnMgmt.getCorpID());
						String session_id = extractColumnMgmt.getId()+"";
						// WHEN FIELD NAME CONTAIN SINGLE COTES('') AT THAT SITUATION I SET THE VALUE OF old_id="000"
						if(session_id.equals(old_id) || old_id.equals("000")){
							extractColumnMgmt = extractColumnMgmtManager.save(extractColumnMgmt);	
							String key = (isNew) ? "extractColumnMgmt.added" : "extractColumnMgmt.updated";
							saveMessage(getText(key));
						}else{
							System.out.println("from else part of flag");
						errorMessage("Not to save, because record all ready exist.");
						return INPUT;
						}
					}
				}
			}
	    }catch(Exception e){
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getComboList(String sessionCorpID){
		reportNameList = extractColumnMgmtManager.getReportName(sessionCorpID);
		tableNameList = extractColumnMgmtManager.getTableName(sessionCorpID);
		fieldNameList = extractColumnMgmtManager.getFieldNameList(sessionCorpID);
		displayNameList = extractColumnMgmtManager.getDisplayNameList(sessionCorpID);
		corpIdList = extractColumnMgmtManager.getCorpIdList();
		return SUCCESS;
	}
	
	@SkipValidation
	public String findTablesNameByReportName() {
		ExtractColumnMgmt extractColumnMgmt = new ExtractColumnMgmt();
		tableNameList = new ArrayList();
		tableNameList = extractColumnMgmtManager.getTableNameByReportName(reportName,sessionCorpID);
		
		return SUCCESS;
	}
	
	public String edit(){
		getComboList(sessionCorpID);
		if(id !=null){
			extractColumnMgmt = extractColumnMgmtManager.get(id);
			oldCorpId = extractColumnMgmt.getCorpID();
		}else{
			extractColumnMgmt = new ExtractColumnMgmt();
			extractColumnMgmt.setCorpID(sessionCorpID);
			extractColumnMgmt.setCreatedOn(new Date());
			extractColumnMgmt.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String delete(){
		extractColumnMgmtManager.remove(id);
		saveMessage("Record deleted successfully.");
		return SUCCESS;
	}
	
	@SkipValidation
	public String search(){
		getComboList(sessionCorpID);
		boolean tsftflag;
		if(corpIdSearch != null){
			tsftflag = true;
			extractColumnPropertiesList = new ArrayList();
			extractColumnPropertiesList = extractColumnMgmtManager.searchFilterList(reportName, tableName, fieldNameSearch, displayName, corpIdSearch, tsftflag);
			}else{
				tsftflag = false;
				extractColumnPropertiesList = new ArrayList();
				extractColumnPropertiesList = extractColumnMgmtManager.searchFilterList(reportName, tableName, fieldNameSearch, displayName, sessionCorpID, tsftflag);
			}
		return SUCCESS;
	}
	
	public String getFormatedExtractColMgmtFieldNameUpdate(String fieldName, String displayFieldName){
		String formatedFieldNameUpdated = "";
		String [] fieldName1 = fieldName.split(" as ");
		formatedFieldNameUpdated = fieldName1[0]+" as " + displayFieldName;
		return formatedFieldNameUpdated;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public ExtractColumnMgmt getExtractColumnMgmt() {
		return extractColumnMgmt;
	}

	public void setExtractColumnMgmt(ExtractColumnMgmt extractColumnMgmt) {
		this.extractColumnMgmt = extractColumnMgmt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ExtractColumnMgmtManager getExtractColumnMgmtManager() {
		return extractColumnMgmtManager;
	}

	public void setExtractColumnMgmtManager(
			ExtractColumnMgmtManager extractColumnMgmtManager) {
		this.extractColumnMgmtManager = extractColumnMgmtManager;
	}

	public List getReportNameList() {
		return reportNameList;
	}

	public void setReportNameList(List reportNameList) {
		this.reportNameList = reportNameList;
	}

	public List getTableNameList() {
		return tableNameList;
	}

	public void setTableNameList(List tableNameList) {
		this.tableNameList = tableNameList;
	}

	public List getTableNameByReportNameList() {
		return tableNameByReportNameList;
	}

	public void setTableNameByReportNameList(List tableNameByReportNameList) {
		this.tableNameByReportNameList = tableNameByReportNameList;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public List getExtractColumnPropertiesList() {
		return extractColumnPropertiesList;
	}

	public void setExtractColumnPropertiesList(List extractColumnPropertiesList) {
		this.extractColumnPropertiesList = extractColumnPropertiesList;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public List getFieldNameList() {
		return fieldNameList;
	}

	public void setFieldNameList(List fieldNameList) {
		this.fieldNameList = fieldNameList;
	}

	public List getCorpIdList() {
		return corpIdList;
	}

	public void setCorpIdList(List corpIdList) {
		this.corpIdList = corpIdList;
	}

	/*public String getCorp_id() {
		return corp_id;
	}

	public void setCorp_id(String corp_id) {
		this.corp_id = corp_id;
	}

	public String getH_corp_id() {
		return h_corp_id;
	}

	public void setH_corp_id(String h_corp_id) {
		this.h_corp_id = h_corp_id;
	}*/

	public List getDisplayNameList() {
		return displayNameList;
	}

	public void setDisplayNameList(List displayNameList) {
		this.displayNameList = displayNameList;
	}

	public String getFieldNameSearch() {
		return fieldNameSearch;
	}

	public void setFieldNameSearch(String fieldNameSearch) {
		this.fieldNameSearch = fieldNameSearch;
	}

	public String getOldCorpId() {
		return oldCorpId;
	}

	public void setOldCorpId(String oldCorpId) {
		this.oldCorpId = oldCorpId;
	}

	public String getCorpIdSearch() {
		return corpIdSearch;
	}

	public void setCorpIdSearch(String corpIdSearch) {
		this.corpIdSearch = corpIdSearch;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	/*public String getH_corpid_login() {
		return h_corpid_login;
	}

	public void setH_corpid_login(String h_corpid_login) {
		this.h_corpid_login = h_corpid_login;
	}*/
	
}
