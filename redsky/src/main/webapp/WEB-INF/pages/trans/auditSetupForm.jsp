<%@ include file="/common/taglibs.jsp"%> 
<head>
<meta name="heading" content="<fmt:message key='auditSetupForm.title'/>"/> 
<title><fmt:message key="auditSetupForm.title"/></title> 
</head>
<s:form id="auditSetupForm" name="auditSetupForm" action="saveauditSetup" onsubmit="return checkAlertData();" method="post" validate="true">
<s:hidden name="auditFieldName"/>
<s:hidden name="auditTableName"/>
<s:hidden name="auditAuditable"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.auditList' }">
<c:redirect url="/auditSetups.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="Layer1" style="width:100%" onkeydown="changeStatus()">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Audit Setup<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="setReturnString('gototab.auditList');return autoSave();"><span>Audit Setup List</span></a></li>
		    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${auditSetup.id}&tableName=auditSetup&decorator=popup&popup=true','audit','height=400,width=790,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		    <!--<li><a href="auditSetups.html"><span>Audit Setup List</span></a></li>
		  --></ul>
		</div>
<div class="spnblk" style="margin-top:-10px;">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
<div class="top" style="margin-top:12px;!margin-top:-2px;"><span></span></div>
   <div class="center-content">	
 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  	<td align="left" height="3px" colspan="9"></td>
		  	<td align="left" rowspan="9">
		  	<fieldset style="padding:8px;">
		  	<legend>Alert Management</legend>
		  	<table border="0"  style="margin:0px;" cellpadding="0" cellspacing="0">
		  	<tr>		  	 
		  	  <td align="left"" class="listwhitetext" colspan="2"><s:hidden key="auditSetup.isAlert" cssStyle="vertical-align:sub;margin-top:2px;" onclick="changeStatus()"/></td>
		  	  <td align="left" width="25px" ></td>		  	  
		  	 </tr>
		  	 <tr>		  	 
		  	  <td align="left" class="listwhitetext" colspan="2"><s:radio name="auditSetup.alertValue" list="%{alertValue}" cssStyle="vertical-align:sub;margin-top:2px;" onclick="changeStatus()"/></td>
		  	  <td align="left" width="25px" ></td>		  	  
		  	 </tr>
		  	 <tr><td align="left" height="5px" ></td></tr>
		  	 <tr>
		  	  <td align="left" class="listwhitetext">Alert Role&nbsp;</td>	
		  	  <td align="left" colspan="1"><s:select list="%{prulerole}" value="%{multiRoles}" id="alertRole" cssStyle="width: 140px; border: 1px solid rgb(33, 157, 209); font-size: 11px;" multiple="true" headerKey=" " headerValue=" " name="auditSetup.alertRole"  cssClass="multiselect"   onchange="changeStatus();"/></td>
		  	 </tr>
		  	</table>
		  	</fieldset>
		  	</td>
		  	</tr>
		  	<tr>
		  	 <td align="right"><fmt:message key="auditSetup.corpID"/></td>
		  	 <td align="left" colspan="2"><s:textfield name="auditSetup.corpID"  value="${sessionCorpID}" cssStyle="width: 155px;"  cssClass="input-text" readonly="true"/></td>
		  	<td align="left"></td>
		  	
		  	</tr>
		  	<tr>
		  	 <td align="right" style="padding-left:5px;"><fmt:message key="auditSetup.tableName"/><font color="red" size="2">*</font></td>
		  		<td align="left" colspan="2"><s:select list="%{tableList}"  cssStyle="width:160px;"  headerKey=" " headerValue=" " name="auditSetup.tableName"    onkeydown="return onlyCharsAllowed(event)" cssClass="list-menu"  onchange="changeStatus();getContract(this);"/></td>	
		  		<td align="left"></td>
		  
		  	</tr>
		  	<tr>
		  	  <td align="right"><fmt:message key="auditSetup.fieldName"/><font color="red" size="2">*</font></td>
		  	  <td align="left" colspan="2"><s:select list="%{fieldList}" id="fieldName" cssStyle="width:160px;"  headerKey=" " headerValue=" " name="auditSetup.fieldName"   onkeydown="return onlyCharsAllowed(event)" cssClass="list-menu"  onblur="copyToDiscription();" onchange="changeStatus();copyToDiscription();"/></td>
		  	  <td align="right" colspan="5"><s:checkbox key="auditSetup.auditable" fieldValue="true" onclick="changeStatus()"/></td>
		  	  <td align="left" width="25px"><fmt:message key="auditSetup.auditable"/></td>    
		  	<tr>	  	
			</tbody>
		  </table>
				<table class="detailTabLabel" border="0">
				  <tbody>
				  <tr>
				  <td align="left" valign="top" class="listwhitetext"><b><fmt:message key="auditSetup.description"/></b>&nbsp;</td>
				  <td align="left"><s:textarea name="auditSetup.description" cssStyle="width:465px;height:60px;" cssClass="textarea" readonly="false"/></td>
		  			</tr>
	            </tbody>
			 </table>	
		  </td>
		</tr>
		</tbody>
		</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>		
		  
		  <table class="detailTabLabel" border="0">		  
		  </table>		  
		  <tr><td>
 		  <table class="detailTabLabel" border="0" style="width:730px">
				<tbody>
					<tr>
					   <td align="left" class="listwhitetext" width="50px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
						<td align="left" class="listwhitetext" width="50px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:60px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<s:text id="auditSetupFormattedValue" name="${FormDateValue}"><s:param name="value" value="auditSetup.createdOn" /></s:text>
							<td valign="top"><s:hidden name="auditSetup.createdOn" value="%{auditSetupFormattedValue}" /></td>
							<td style="width:140px"><fmt:formatDate value="${auditSetup.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty auditSetup.id}">
								<s:hidden name="auditSetup.createdBy"/>
								<td style="width:75px"><s:label name="createdBy" value="%{auditSetup.createdBy}"/></td>
							</c:if>
							<c:if test="${empty auditSetup.id}">
								<s:hidden name="auditSetup.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<s:text id="auditSetupupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="auditSetup.updatedOn" /></s:text>
							<td valign="top"><s:hidden name="auditSetup.updatedOn" value="%{auditSetupupdatedOnFormattedValue}" /></td>
							<td style="width:140px"><fmt:formatDate value="${auditSetup.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty auditSetup.id}">
								<s:hidden name="auditSetup.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{auditSetup.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty auditSetup.id}">
								<s:hidden name="auditSetup.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>							
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>
			</td>
			</tr>		  
		  <table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  	<td align="left" height="3px"></td>
		  	</tr>	  	
		     <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button" method="save" key="button.save" cssStyle="width:55px; height:25px" />  
        		</td>
       			<td align="left">
       			<input type="button" class="cssbutton1" style="width:55px; height:25px" onclick="location.href='<c:url value="/auditSetupForm.html"/>'" value="<fmt:message key="button.add"/>"/>
        		</td>
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset" cssStyle="width:55px; height:25px"/> 
        		</td>        		
       	  	</tr>		  	
		  </tbody>
		  </table>
		</div>
<s:hidden name="auditSetup.id"/>
<s:hidden name="tableNames" value="%{auditSetup.tableName}"/>
</s:form>

<script language="JavaScript">
function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
</script>	
<script>
function changeStatus(){
	document.forms['auditSetupForm'].elements['formStatus'].value = '1';
}
function autoSave(clickType){
	var check = checkTableFieldname();
	if(check && !(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){
			var noSaveAction = '<c:out value="${auditSetup.id}"/>';
			var id1 = document.forms['auditSetupForm'].elements['auditSetup.id'].value;
			if(document.forms['auditSetupForm'].elements['gotoPageString'].value == 'gototab.auditList'){
			noSaveAction = 'auditSetups.html';
		}
		processAutoSave(document.forms['auditSetupForm'], 'saveauditSetup!saveOnTabChange.html', noSaveAction);
		}else{
			var id1 = document.forms['auditSetupForm'].elements['auditSetup.id'].value;
			if (document.forms['auditSetupForm'].elements['formStatus'].value == '1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the Audit Setup Form");
				if(agree){
					document.forms['auditSetupForm'].action = 'saveauditSetup!saveOnTabChange.html';
					document.forms['auditSetupForm'].submit();
				}else{
					if(id1 != ''){
					if(document.forms['auditSetupForm'].elements['gotoPageString'].value == 'gototab.auditList'){
						location.href = 'auditSetups.html';
						}
				}
		}
	}else{
		if(id1 != ''){
					if(document.forms['auditSetupForm'].elements['gotoPageString'].value == 'gototab.auditList'){	
							location.href = 'auditSetups.html';
							}
					}
			}
		}
	}
}
 
function getContract(targetElement) {
	var tableNames = targetElement.value;
	// alert(tableNames);
	var url="findfieldListAudit.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    // alert(url);
    	http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
}

function handleHttpResponse3()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                // alert(results);
                res = results.split("@");
                targetElement = document.forms['auditSetupForm'].elements['auditSetup.fieldName'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['auditSetupForm'].elements['auditSetup.fieldName'].options[i].text = '';
					document.forms['auditSetupForm'].elements['auditSetup.fieldName'].options[i].value = '';
					}else{
					// contractVal = res[i].split("#");
					document.forms['auditSetupForm'].elements['auditSetup.fieldName'].options[i].text = res[i];
					document.forms['auditSetupForm'].elements['auditSetup.fieldName'].options[i].value = res[i];
					}
					document.getElementById("fieldName").value = '${auditSetup.fieldName}';
					}
             }
        }
        
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
      
    function checkAlertData()
    {
     var isAlert=document.getElementsByName('auditSetup.alertValue');
     var des='';
     var role =' ';
 		for (var i = 0, length = isAlert.length; i < length; i++) { if (isAlert[i].checked) {des=isAlert[i].value;}}	
		
   	 var isAlertRole=document.forms['auditSetupForm'].elements['auditSetup.alertRole'];
     	for (var i = 0; i < isAlertRole.options.length; i++) {
     		  if (isAlertRole.options[i].selected) {
         		 if(role==' '){
         			role =  isAlertRole.options[i].value;
         		 }
         		 else{
         			role = role+','+isAlertRole.options[i].value;
         		 }
     		   }
     	}
     if((des =='All Updates' || des=='Network Update')&& (role==' '))
     {		
         	document.forms['auditSetupForm'].elements['auditSetup.alertRole'].value='';
     }
       
      return checkTableFieldname1();
    }
    function copyToDiscription()
    {
    var fieldName= document.forms['auditSetupForm'].elements['auditSetup.fieldName'].value;
    document.forms['auditSetupForm'].elements['auditSetup.description'].value=fieldName;
    }

   function checkTableFieldname(){
	   var tblName = document.forms['auditSetupForm'].elements['auditSetup.tableName'].value;
		var fldName = document.forms['auditSetupForm'].elements['auditSetup.fieldName'].value;
		if(tblName.trim() =='' ){
			alert('Please select table name.');
			return false;
		}else if(fldName.trim() =='' ){
			alert('Please select field name.');
			return false;
		} else
			return true;	
	   }
   function checkTableFieldname1(){
	   var tblName = document.forms['auditSetupForm'].elements['auditSetup.tableName'].value;
		var fldName = document.forms['auditSetupForm'].elements['auditSetup.fieldName'].value;
		if(tblName.trim() =='' ){
			alert('Please select table name.');
			return false;
		}else if(fldName.trim() =='' ){
			alert('Please select field name.');
			return false;
		} else
			return true;	
	   } 
</script>