package com.trilasoft.app.service.impl;

import com.trilasoft.app.dao.LocationDao;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.service.LocationManager;
import org.appfuse.service.impl.GenericManagerImpl;
import java.util.List;

public class LocationManagerImpl extends GenericManagerImpl<Location, Long> implements LocationManager {
	LocationDao locationDao;

	public LocationManagerImpl(LocationDao locationDao) {
		super(locationDao);
		this.locationDao = locationDao;
	}

	public List<Location> findByLocation(String locationId,String type, String occupied, String capacity, String corpId) {
		return locationDao.findByLocation(locationId, type,occupied,capacity,corpId);
	}
	
	public List<Location> findByLocationWH(String locationId, String occupied, String type, String warehouse){
		return locationDao.findByLocationWH(locationId, occupied, type, warehouse);
	}
	
	public List getOccupiedList(){
		return locationDao.getOccupiedList();
	}
	
	public Location getByLocation(String locationId){
		return locationDao.getByLocation(locationId);
	}
	
	public List locationList(String corpId){
		return locationDao.locationList(corpId);
	}
	
	public List findByLocationWHMove(String locationId, String occupied, String type, String warehouse){
		return locationDao.findByLocationWHMove(locationId, occupied, type, warehouse);
	}

	public List<Location> findByLocationWithWarehouse(String locationId, String type,String occupieType, String capacity, String sessionCorpID,
			String warehouse) {
		
		return locationDao.findByLocationWithWarehouse(locationId, type,occupieType,capacity,sessionCorpID,warehouse);
	}
	public List findLocationList(String sessionCorpID, String whouse,String code) {		
		return locationDao.findLocationList(sessionCorpID,whouse,code);
	}

	public List searchLocationForStorage(String locationId, String type,String sessionCorpID, String warehouse) {
		return locationDao.searchLocationForStorage(locationId,type,sessionCorpID,warehouse);
	}
	public List<Location> findByLocationRearrList(String sessionCorpID,String locationId,String type, String warehouse){
		return locationDao.findByLocationRearrList(sessionCorpID,locationId,type,warehouse);
	}
	public List<Location> findAllLocationId(String sessionCorpID){
		return locationDao.findAllLocationId(sessionCorpID);
		
	}
	public List workTicketByLocation(String locationId){
		return locationDao.workTicketByLocation(locationId);
	}
}
