package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.AdAddressesDetailsManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.webapp.util.GetSortedMap;

public class AdAddressesDetailsAction extends BaseAction implements Preparable{
	private Long id;
	private String sessionCorpID;
	private AdAddressesDetails adAddressesDetails;
	private AdAddressesDetailsManager adAddressesDetailsManager;
	private Long customerFileId;
	private RefMasterManager refMasterManager;
	private Map<String,String> addressType=new HashMap<String, String>();	
	private CustomerFileManager customerFileManager;
	private CustomerFile customerFile;
	private Map<String,String> ocountry=new HashMap<String, String>();
	private Map<String,String> ostates=new HashMap<String, String>();
	private String enbState;
	private Map<String,String> countryCod=new HashMap<String, String>();
	private Map<String,String> ocountry_isactive=new HashMap<String, String>();
	private Map<String,String> addressType_isactive=new HashMap<String, String>();
	
	private ToDoRuleManager toDoRuleManager;
	/* Added for ticket number: 5878 */
	private String usertype;
	
	static final Logger logger = Logger.getLogger(CustomerFileAction.class);
	public void prepare() throws Exception { 
		enbState = customerFileManager.enableStateList1(sessionCorpID);
	}
	public String getComboList(String corpID){ 
	 	//addressType = refMasterManager.findByParameter(corpID, "ADDRESSTYPE");
	 	//ocountry = refMasterManager.findByParameter(corpID, "COUNTRY");
	 	ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
	 	//countryCod = refMasterManager.findByParameter(corpID, "COUNTRY");
	 	String parameters="'ADDRESSTYPE','COUNTRY'";
	 	 List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpID,parameters);
    	 for (RefMasterDTO refObj : allParamValue) {
    		 	if(refObj.getParameter().trim().equals("COUNTRY")){
	 				ocountry.put(refObj.getCode().trim(),refObj.getDescription().trim());
	 				countryCod.put(refObj.getCode().trim(),refObj.getDescription().trim());
	 				ocountry_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus(),refObj.getDescription().trim());
		 		}else if(refObj.getParameter().trim().equals("ADDRESSTYPE")){
		 			addressType.put(refObj.getCode().trim(),refObj.getDescription().trim());
		 			addressType_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus(),refObj.getDescription().trim());
	 		    }
    	 }
    	 addressType_isactive=GetSortedMap.sortByValues(addressType_isactive);
    	 ocountry_isactive=GetSortedMap.sortByValues(ocountry_isactive);
	 	 return SUCCESS;
	}
	public AdAddressesDetailsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		usertype = user.getUserType();
		this.sessionCorpID = user.getCorpID(); 
		
	}
	public String edit() {
		getComboList(sessionCorpID);
		customerFile=customerFileManager.get(customerFileId);
		if (id != null) {
			adAddressesDetails = adAddressesDetailsManager.get(id);
			//ostates = customerFileManager.findDefaultStateList(adAddressesDetails.getCountry(), sessionCorpID);
		} else {
			
			adAddressesDetails = new AdAddressesDetails(); 
			//ostates = customerFileManager.findDefaultStateList("", sessionCorpID);
			adAddressesDetails.setCreatedOn(new Date());
			adAddressesDetails.setUpdatedOn(new Date());
			adAddressesDetails.setCustomerFileId(customerFileId);
		} 
		return SUCCESS;
	}
	private CustomerFileAction customerFileAction;
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (adAddressesDetails.getId() == null);  
				if (isNew) { 
					adAddressesDetails.setCreatedOn(new Date());
		        } 
				adAddressesDetails.setCorpID(sessionCorpID);
				adAddressesDetails.setUpdatedOn(new Date());
				adAddressesDetails.setUpdatedBy(getRequest().getRemoteUser()); 
				//ostates = customerFileManager.findDefaultStateList(adAddressesDetails.getCountry(), sessionCorpID);
				adAddressesDetails=adAddressesDetailsManager.save(adAddressesDetails);  
				customerFile = customerFileManager.get(adAddressesDetails.getCustomerFileId());
				String key = (isNew) ?"Address details have been saved." :"Address details have been saved." ;
			    try{
			    	if(customerFile.getIsNetworkRecord()){
						List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
						Set set = new HashSet(linkedSequenceNumber);
						List linkedSeqNum = new ArrayList(set);
						List<Object> records=findRecords(linkedSeqNum,customerFile);
						//List<Object> records=findRecords(linkedSequenceNumber,customerFile);
						synchornizeAdAddressesDetails(records,adAddressesDetails,isNew,customerFile);
						
					}
			    	/*Long StartTime = System.currentTimeMillis();
	    			String agentShipNumber = toDoRuleManager.getLinkedShipnumber(customerFile.getSequenceNumber(),sessionCorpID);
	    			if(agentShipNumber!=null && !agentShipNumber.equals(""))
	    				customerFileAction.createIntegrationLogInfo(agentShipNumber,"");
	    			
	    				
	    			Long timeTaken =  System.currentTimeMillis() - StartTime;
	    			logger.warn("\n\nTime taken for craeting line in integration log info table : "+timeTaken+"\n\n");*/
	    		
			    }catch(Exception ex){
						System.out.println("\n\n\n\n error------->"+ex);
				}
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	private List findLinkerSequenceNumber(CustomerFile customerFile) {
		List linkedSequnceNumberList= new ArrayList();
		if(customerFile.getBookingAgentSequenceNumber()==null || customerFile.getBookingAgentSequenceNumber().equals("") ){
			linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getSequenceNumber(), "Primary");
		}else{
			linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getBookingAgentSequenceNumber(), "Secondary");
		}
		return linkedSequnceNumberList;
	}
	private List<Object> findRecords(List linkedSequenceNumber,CustomerFile customerFileLocal) {
		List<Object> recordList= new ArrayList();
		Iterator it =linkedSequenceNumber.iterator();
		while(it.hasNext()){
			String sequenceNumber= it.next().toString();
			CustomerFile customerFileRemote=customerFileManager.getForOtherCorpid(Long.parseLong(customerFileManager.findRemoteCustomerFile(sequenceNumber).toString()));
			recordList.add(customerFileRemote);
		}
		return recordList;
	}
	
	private void synchornizeAdAddressesDetails(List<Object> records, AdAddressesDetails adAddressesDetails, boolean isNew,CustomerFile customerFile) {
		Iterator  it=records.iterator();
		while(it.hasNext()){
			CustomerFile customerFileToRecods=(CustomerFile)it.next();
			if(!(customerFile.getSequenceNumber().equals(customerFileToRecods.getSequenceNumber()))){
			try{
				if(isNew){
					AdAddressesDetails adAddressesDetailsNew = new AdAddressesDetails();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(adAddressesDetailsNew, adAddressesDetails);
    				
    				adAddressesDetailsNew.setCreatedBy("Networking");
    				adAddressesDetailsNew.setUpdatedBy("Networking");
    				adAddressesDetailsNew.setCreatedOn(new Date());
    				adAddressesDetailsNew.setUpdatedOn(new Date());
    				adAddressesDetailsNew.setCustomerFileId(customerFileToRecods.getId());
    				adAddressesDetailsNew.setCorpID(customerFileToRecods.getCorpID());
    				adAddressesDetailsNew.setNetworkId(adAddressesDetails.getId());
    				adAddressesDetailsNew.setId(null);
    				adAddressesDetailsNew = adAddressesDetailsManager.save(adAddressesDetailsNew);
    				
    				//adAddressesDetails.setNetworkId(adAddressesDetails.getId());
    				//adAddressesDetails=adAddressesDetailsManager.save(adAddressesDetails);
    				int i=adAddressesDetailsManager.updateAdAddressesDetailsNetworkId(adAddressesDetails.getId());
				}else{
					AdAddressesDetails adAddressesDetailsTo = adAddressesDetailsManager.getForOtherCorpid(adAddressesDetailsManager.findRemoteAdAddressesDetails(adAddressesDetails.getNetworkId(),customerFileToRecods.getId()));
					Long id = adAddressesDetailsTo.getId();
					String createdBy=adAddressesDetailsTo.getCreatedBy();
					Date createdOn=adAddressesDetailsTo.getCreatedOn();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(adAddressesDetailsTo, adAddressesDetails);
    				adAddressesDetailsTo.setUpdatedBy(adAddressesDetails.getCorpID()+":"+getRequest().getRemoteUser());
    				adAddressesDetailsTo.setUpdatedOn(new Date());
    				adAddressesDetailsTo.setCustomerFileId(customerFileToRecods.getId());
    				adAddressesDetailsTo.setCorpID(customerFileToRecods.getCorpID()); 
    				adAddressesDetailsTo.setId(id);
    				adAddressesDetailsTo.setCreatedBy(createdBy);
    				adAddressesDetailsTo.setCreatedOn(createdOn);
    				adAddressesDetailsTo = adAddressesDetailsManager.save(adAddressesDetailsTo);
				}
					
			}catch(Exception ex){
				System.out.println("\n\n\n\n Exception while copying");
				ex.printStackTrace();
			}
				
			}
			
		}
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public AdAddressesDetails getAdAddressesDetails() {
		return adAddressesDetails;
	}
	public void setAdAddressesDetails(AdAddressesDetails adAddressesDetails) {
		this.adAddressesDetails = adAddressesDetails;
	}
	public AdAddressesDetailsManager getAdAddressesDetailsManager() {
		return adAddressesDetailsManager;
	}
	public void setAdAddressesDetailsManager(
			AdAddressesDetailsManager adAddressesDetailsManager) {
		this.adAddressesDetailsManager = adAddressesDetailsManager;
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getAddressType() {
		return addressType;
	}
	public void setAddressType(Map<String, String> addressType) {
		this.addressType = addressType;
	}
	public CustomerFileManager getCustomerFileManager() {
		return customerFileManager;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public Map<String, String> getOstates() {
		return ostates;
	}
	public void setOstates(Map<String, String> ostates) {
		this.ostates = ostates;
	}
	
	public String getEnbState() {
		return enbState;
	}
	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}
	public Map<String, String> getCountryCod() {
		return countryCod;
	}
	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}
	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}
	public CustomerFileAction getCustomerFileAction() {
		return customerFileAction;
	}
	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}
	public Map<String, String> getOcountry_isactive() {
		return ocountry_isactive;
	}
	public void setOcountry_isactive(Map<String, String> ocountry_isactive) {
		this.ocountry_isactive = ocountry_isactive;
	}
	public Map<String, String> getAddressType_isactive() {
		return addressType_isactive;
	}
	public void setAddressType_isactive(Map<String, String> addressType_isactive) {
		this.addressType_isactive = addressType_isactive;
	}
}
