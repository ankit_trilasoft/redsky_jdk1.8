

delimiter $$

CREATE trigger redsky.trigger_add_history_partnerprivate
BEFORE UPDATE on redsky.partnerprivate
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



   IF (OLD.corpid <> NEW.corpid ) THEN
      select CONCAT(fieldNameList,'partnerprivate.corpid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
  END IF;
  
  IF (OLD.billingMoment <> NEW.billingMoment ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingMoment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingMoment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingMoment,'~') into newValueList;
  END IF;
  
  IF (OLD.storageEmailType <> NEW.storageEmailType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.storageEmailType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmailType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmailType,'~') into newValueList;
  END IF;


   IF (OLD.warehouse <> NEW.warehouse ) THEN
      select CONCAT(fieldNameList,'partnerprivate.warehouse~') into fieldNameList;
      select CONCAT(oldValueList,OLD.warehouse,'~') into oldValueList;
      select CONCAT(newValueList,NEW.warehouse,'~') into newValueList;
  END IF;

  IF (OLD.rutTaxNumber <> NEW.rutTaxNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.rutTaxNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rutTaxNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rutTaxNumber,'~') into newValueList;
  END IF;


   IF (OLD.billingInstruction <> NEW.billingInstruction ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingInstruction~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstruction,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstruction,'~') into newValueList;
  END IF;


   IF (OLD.billingInstructionCode <> NEW.billingInstructionCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingInstructionCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingInstructionCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingInstructionCode,'~') into newValueList;
  END IF;


   IF (OLD.coordinator <> NEW.coordinator ) THEN
      select CONCAT(fieldNameList,'partnerprivate.coordinator~') into fieldNameList;
      select CONCAT(oldValueList,OLD.coordinator,'~') into oldValueList;
      select CONCAT(newValueList,NEW.coordinator,'~') into newValueList;
  END IF;


   IF (OLD.salesMan <> NEW.salesMan ) THEN
      select CONCAT(fieldNameList,'partnerprivate.salesMan~') into fieldNameList;
      select CONCAT(oldValueList,OLD.salesMan,'~') into oldValueList;
      select CONCAT(newValueList,NEW.salesMan,'~') into newValueList;
  END IF;


   IF (OLD.abbreviation <> NEW.abbreviation ) THEN
      select CONCAT(fieldNameList,'partnerprivate.abbreviation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.abbreviation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.abbreviation,'~') into newValueList;
  END IF;


   IF (OLD.billPayOption <> NEW.billPayOption ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billPayOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billPayOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billPayOption,'~') into newValueList;
  END IF;


   IF (OLD.billPayType <> NEW.billPayType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billPayType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billPayType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billPayType,'~') into newValueList;
  END IF;


   IF (OLD.billToGroup <> NEW.billToGroup ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billToGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToGroup,'~') into newValueList;
  END IF;


   IF (OLD.needAuth <> NEW.needAuth ) THEN
      select CONCAT(fieldNameList,'partnerprivate.needAuth~') into fieldNameList;
      select CONCAT(oldValueList,OLD.needAuth,'~') into oldValueList;
      select CONCAT(newValueList,NEW.needAuth,'~') into newValueList;
  END IF;


   IF (OLD.partnerCode <> NEW.partnerCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.partnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
  END IF;


   IF (OLD.storageBillingGroup <> NEW.storageBillingGroup ) THEN
      select CONCAT(fieldNameList,'partnerprivate.storageBillingGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageBillingGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageBillingGroup,'~') into newValueList;
  END IF;


   IF (OLD.accountHolder <> NEW.accountHolder ) THEN
      select CONCAT(fieldNameList,'partnerprivate.accountHolder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountHolder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountHolder,'~') into newValueList;
  END IF;


   IF (OLD.paymentMethod <> NEW.paymentMethod ) THEN
      select CONCAT(fieldNameList,'partnerprivate.paymentMethod~') into fieldNameList;
      select CONCAT(oldValueList,OLD.paymentMethod,'~') into oldValueList;
      select CONCAT(newValueList,NEW.paymentMethod,'~') into newValueList;
  END IF;


   IF (OLD.payOption <> NEW.payOption ) THEN
      select CONCAT(fieldNameList,'partnerprivate.payOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payOption,'~') into newValueList;
  END IF;


   IF (OLD.multiAuthorization <> NEW.multiAuthorization ) THEN
      select CONCAT(fieldNameList,'partnerprivate.multiAuthorization~') into fieldNameList;
      select CONCAT(oldValueList,OLD.multiAuthorization,'~') into oldValueList;
      select CONCAT(newValueList,NEW.multiAuthorization,'~') into newValueList;
  END IF;


   IF (OLD.companyDivision <> NEW.companyDivision ) THEN
      select CONCAT(fieldNameList,'partnerprivate.companyDivision~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDivision,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDivision,'~') into newValueList;
  END IF;


   IF (OLD.billingUser <> NEW.billingUser ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingUser~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingUser,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingUser,'~') into newValueList;
  END IF;


   IF (OLD.payableUser <> NEW.payableUser ) THEN
      select CONCAT(fieldNameList,'partnerprivate.payableUser~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableUser,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableUser,'~') into newValueList;
  END IF;


   IF (OLD.pricingUser <> NEW.pricingUser ) THEN
      select CONCAT(fieldNameList,'partnerprivate.pricingUser~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pricingUser,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pricingUser,'~') into newValueList;
  END IF;


   IF (OLD.partnerPortalId <> NEW.partnerPortalId ) THEN
      select CONCAT(fieldNameList,'partnerprivate.partnerPortalId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerPortalId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerPortalId,'~') into newValueList;
  END IF;


   IF (OLD.associatedAgents <> NEW.associatedAgents ) THEN
      select CONCAT(fieldNameList,'partnerprivate.associatedAgents~') into fieldNameList;
      select CONCAT(oldValueList,OLD.associatedAgents,'~') into oldValueList;
      select CONCAT(newValueList,NEW.associatedAgents,'~') into newValueList;
  END IF;


   IF (OLD.creditTerms <> NEW.creditTerms ) THEN
      select CONCAT(fieldNameList,'partnerprivate.creditTerms~') into fieldNameList;
      select CONCAT(oldValueList,OLD.creditTerms,'~') into oldValueList;
      select CONCAT(newValueList,NEW.creditTerms,'~') into newValueList;
  END IF;


   IF (OLD.acctDefaultJobType <> NEW.acctDefaultJobType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.acctDefaultJobType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.acctDefaultJobType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.acctDefaultJobType,'~') into newValueList;
  END IF;


   IF (OLD.firstName <> NEW.firstName ) THEN
      select CONCAT(fieldNameList,'partnerprivate.firstName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
  END IF;


   IF (OLD.lastName <> NEW.lastName ) THEN
      select CONCAT(fieldNameList,'partnerprivate.lastName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
  END IF;


   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'partnerprivate.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF;


   IF (OLD.driverAgency <> NEW.driverAgency ) THEN
      select CONCAT(fieldNameList,'partnerprivate.driverAgency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverAgency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverAgency,'~') into newValueList;
  END IF;


   IF (OLD.validNationalCode <> NEW.validNationalCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.validNationalCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.validNationalCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.validNationalCode,'~') into newValueList;
  END IF;


   IF (OLD.partnerPublicId <> NEW.partnerPublicId ) THEN
      select CONCAT(fieldNameList,'partnerprivate.partnerPublicId') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerPublicId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerPublicId,'~') into newValueList;
  END IF;


   IF (OLD.cardNumber <> NEW.cardNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.cardNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cardNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cardNumber,'~') into newValueList;
  END IF;


   IF (OLD.cardStatus <> NEW.cardStatus ) THEN
      select CONCAT(fieldNameList,'partnerprivate.cardStatus~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cardStatus,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cardStatus,'~') into newValueList;
  END IF;


   IF (OLD.accountId <> NEW.accountId ) THEN
      select CONCAT(fieldNameList,'partnerprivate.accountId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountId,'~') into newValueList;
  END IF;


   IF (OLD.customerId <> NEW.customerId ) THEN
      select CONCAT(fieldNameList,'partnerprivate.customerId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerId,'~') into newValueList;
  END IF;


   IF (OLD.licenseNumber <> NEW.licenseNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.licenseNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.licenseNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.licenseNumber,'~') into newValueList;
  END IF;


   IF (OLD.licenseState <> NEW.licenseState ) THEN
      select CONCAT(fieldNameList,'partnerprivate.licenseState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.licenseState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.licenseState,'~') into newValueList;
  END IF;


   IF (OLD.creditCurrency <> NEW.creditCurrency ) THEN
      select CONCAT(fieldNameList,'partnerprivate.creditCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.creditCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.creditCurrency,'~') into newValueList;
  END IF;


   IF (OLD.taxId <> NEW.taxId ) THEN
      select CONCAT(fieldNameList,'partnerprivate.taxId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.taxId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.taxId,'~') into newValueList;
  END IF;


   IF (OLD.taxIdType <> NEW.taxIdType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.taxIdType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.taxIdType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.taxIdType,'~') into newValueList;
  END IF;


   IF (OLD.accountManager <> NEW.accountManager ) THEN
      select CONCAT(fieldNameList,'partnerprivate.accountManager~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountManager,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountManager,'~') into newValueList;
  END IF;


   IF (OLD.extReference <> NEW.extReference ) THEN
      select CONCAT(fieldNameList,'partnerprivate.extReference~') into fieldNameList;
      select CONCAT(oldValueList,OLD.extReference,'~') into oldValueList;
      select CONCAT(newValueList,NEW.extReference,'~') into newValueList;
  END IF;


   IF (OLD.auditor <> NEW.auditor ) THEN
      select CONCAT(fieldNameList,'partnerprivate.auditor~') into fieldNameList;
      select CONCAT(oldValueList,OLD.auditor,'~') into oldValueList;
      select CONCAT(newValueList,NEW.auditor,'~') into newValueList;
  END IF;


   IF (OLD.emergencyContact <> NEW.emergencyContact ) THEN
      select CONCAT(fieldNameList,'partnerprivate.emergencyContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emergencyContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emergencyContact,'~') into newValueList;
  END IF;


   IF (OLD.emergencyEmail <> NEW.emergencyEmail ) THEN
      select CONCAT(fieldNameList,'partnerprivate.emergencyEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emergencyEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emergencyEmail,'~') into newValueList;
  END IF;


   IF (OLD.emergencyPhone <> NEW.emergencyPhone ) THEN
      select CONCAT(fieldNameList,'partnerprivate.emergencyPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emergencyPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emergencyPhone,'~') into newValueList;
  END IF;


   IF (OLD.fleet <> NEW.fleet ) THEN
      select CONCAT(fieldNameList,'partnerprivate.fleet~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fleet,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fleet,'~') into newValueList;
  END IF;


   IF (OLD.truckID <> NEW.truckID ) THEN
      select CONCAT(fieldNameList,'partnerprivate.truckID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.truckID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.truckID,'~') into newValueList;
  END IF;


   IF (OLD.driverID <> NEW.driverID ) THEN
      select CONCAT(fieldNameList,'partnerprivate.driverID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverID,'~') into newValueList;
  END IF;


   IF (OLD.commission <> NEW.commission ) THEN
      select CONCAT(fieldNameList,'partnerprivate.commission~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commission,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commission,'~') into newValueList;
  END IF;


   IF (OLD.payTo <> NEW.payTo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.payTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payTo,'~') into newValueList;
  END IF;


   IF (OLD.corp <> NEW.corp ) THEN
      select CONCAT(fieldNameList,'partnerprivate.corp~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corp,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corp,'~') into newValueList;
  END IF;


   IF (OLD.mc <> NEW.mc ) THEN
      select CONCAT(fieldNameList,'partnerprivate.mc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mc,'~') into newValueList;
  END IF;


   IF (OLD.trailerID <> NEW.trailerID ) THEN
      select CONCAT(fieldNameList,'partnerprivate.trailerID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.trailerID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.trailerID,'~') into newValueList;
  END IF;


   IF (OLD.classcode <> NEW.classcode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.classcode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.classcode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.classcode,'~') into newValueList;
  END IF;


   IF (OLD.vatNumber <> NEW.vatNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.vatNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vatNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vatNumber,'~') into newValueList;
  END IF;


   IF (OLD.UTSCompanyType <> NEW.UTSCompanyType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.UTSCompanyType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.UTSCompanyType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.UTSCompanyType,'~') into newValueList;
  END IF;


   IF (OLD.billingCurrency <> NEW.billingCurrency ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCurrency,'~') into newValueList;
  END IF;


   IF (OLD.driverType <> NEW.driverType ) THEN
      select CONCAT(fieldNameList,'partnerprivate.driverType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.driverType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.driverType,'~') into newValueList;
  END IF;


   IF (OLD.bankAccountNumber <> NEW.bankAccountNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.bankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankAccountNumber,'~') into newValueList;
  END IF;



   IF (OLD.bankCode <> NEW.bankCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.bankCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankCode,'~') into newValueList;
  END IF;



   IF (OLD.ratingScale <> NEW.ratingScale ) THEN
      select CONCAT(fieldNameList,'partnerprivate.ratingScale~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ratingScale,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ratingScale,'~') into newValueList;
  END IF;



   IF (OLD.customerFeedback <> NEW.customerFeedback ) THEN
      select CONCAT(fieldNameList,'partnerprivate.customerFeedback~') into fieldNameList;
      select CONCAT(oldValueList,OLD.customerFeedback,'~') into oldValueList;
      select CONCAT(newValueList,NEW.customerFeedback,'~') into newValueList;
  END IF;



   IF (OLD.description1 <> NEW.description1 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.description1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description1,'~') into newValueList;
  END IF;



   IF (OLD.description2 <> NEW.description2 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.description2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description2,'~') into newValueList;
  END IF;



   IF (OLD.description3 <> NEW.description3 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.description3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description3,'~') into newValueList;
  END IF;



   IF (OLD.link1 <> NEW.link1 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.link1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.link1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.link1,'~') into newValueList;
  END IF;



   IF (OLD.link2 <> NEW.link2 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.link2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.link2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.link2,'~') into newValueList;
  END IF;



   IF (OLD.link3 <> NEW.link3 ) THEN
      select CONCAT(fieldNameList,'partnerprivate.link3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.link3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.link3,'~') into newValueList;
  END IF;



   IF (OLD.claimsUser <> NEW.claimsUser ) THEN
      select CONCAT(fieldNameList,'partnerprivate.claimsUser~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimsUser,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimsUser,'~') into newValueList;
  END IF;



   IF (OLD.commissionPlan <> NEW.commissionPlan ) THEN
      select CONCAT(fieldNameList,'partnerprivate.commissionPlan~') into fieldNameList;
      select CONCAT(oldValueList,OLD.commissionPlan,'~') into oldValueList;
      select CONCAT(newValueList,NEW.commissionPlan,'~') into newValueList;
  END IF;



   IF (OLD.lumpPerUnit <> NEW.lumpPerUnit ) THEN
      select CONCAT(fieldNameList,'partnerprivate.lumpPerUnit~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lumpPerUnit,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lumpPerUnit,'~') into newValueList;
  END IF;



   IF (OLD.minReplacementvalue <> NEW.minReplacementvalue ) THEN
      select CONCAT(fieldNameList,'partnerprivate.minReplacementvalue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minReplacementvalue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minReplacementvalue,'~') into newValueList;
  END IF;



   IF (OLD.storageEmail <> NEW.storageEmail ) THEN
      select CONCAT(fieldNameList,'partnerprivate.storageEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.storageEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.storageEmail,'~') into newValueList;
  END IF;



   IF (OLD.collection <> NEW.collection ) THEN
      select CONCAT(fieldNameList,'partnerprivate.collection~') into fieldNameList;
      select CONCAT(oldValueList,OLD.collection,'~') into oldValueList;
      select CONCAT(newValueList,NEW.collection,'~') into newValueList;
  END IF;



   IF (OLD.localName <> NEW.localName ) THEN
      select CONCAT(fieldNameList,'partnerprivate.localName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.localName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.localName,'~') into newValueList;
  END IF;






   IF ((OLD.creditExpiry <> NEW.creditExpiry) or (OLD.creditExpiry is null and NEW.creditExpiry is not null)
      or (OLD.creditExpiry is not null and NEW.creditExpiry is null)) THEN
       select CONCAT(fieldNameList,'partnerprivate.creditExpiry~') into fieldNameList;
       IF(OLD.creditExpiry is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.creditExpiry is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.creditExpiry is not null) THEN
        	select CONCAT(oldValueList,OLD.creditExpiry,'~') into oldValueList;
     	END IF;
      	IF(NEW.creditExpiry is not null) THEN
        	select CONCAT(newValueList,NEW.creditExpiry,'~') into newValueList;
      	END IF;
   END IF;






   IF ((OLD.creditDateCheck <> NEW.creditDateCheck) or (OLD.creditDateCheck is null and NEW.creditDateCheck is not null)
      or (OLD.creditDateCheck is not null and NEW.creditDateCheck is null)) THEN
       select CONCAT(fieldNameList,'partnerprivate.creditDateCheck~') into fieldNameList;
       IF(OLD.creditDateCheck is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.creditDateCheck is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.creditDateCheck is not null) THEN
        	select CONCAT(oldValueList,OLD.creditDateCheck,'~') into oldValueList;
     	END IF;
      	IF(NEW.creditDateCheck is not null) THEN
        	select CONCAT(newValueList,NEW.creditDateCheck,'~') into newValueList;
      	END IF;
   END IF;


 

   IF ((OLD.birth <> NEW.birth) or (OLD.birth is null and NEW.birth is not null)
      or (OLD.birth is not null and NEW.birth is null)) THEN
       select CONCAT(fieldNameList,'partnerprivate.birth~') into fieldNameList;
       IF(OLD.birth is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.birth is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.birth is not null) THEN
        	select CONCAT(oldValueList,OLD.birth,'~') into oldValueList;
     	END IF;
      	IF(NEW.birth is not null) THEN
        	select CONCAT(newValueList,NEW.birth,'~') into newValueList;
      	END IF;
   END IF;




   IF ((OLD.hire <> NEW.hire) or (OLD.hire is null and NEW.hire is not null)
      or (OLD.hire is not null and NEW.hire is null)) THEN
       select CONCAT(fieldNameList,'partnerprivate.hire~') into fieldNameList;
       IF(OLD.hire is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.hire is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.hire is not null) THEN
        	select CONCAT(oldValueList,OLD.hire,'~') into oldValueList;
     	END IF;
      	IF(NEW.hire is not null) THEN
        	select CONCAT(newValueList,NEW.hire,'~') into newValueList;
      	END IF;
   END IF;




   IF ((OLD.termination <> NEW.termination) or (OLD.termination is null and NEW.termination is not null)
      or (OLD.termination is not null and NEW.termination is null)) THEN
       select CONCAT(fieldNameList,'partnerprivate.termination~') into fieldNameList;
       IF(OLD.termination is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.termination is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.termination is not null) THEN
        	select CONCAT(oldValueList,OLD.termination,'~') into oldValueList;
     	END IF;
      	IF(NEW.termination is not null) THEN
        	select CONCAT(newValueList,NEW.termination,'~') into newValueList;
      	END IF;
   END IF;




  IF (OLD.qc <> NEW.qc ) THEN
       select CONCAT(fieldNameList,'partnerprivate.qc~') into fieldNameList;
       IF(OLD.qc = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.qc = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.qc = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.qc = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.payableUploadCheck <> NEW.payableUploadCheck ) THEN
       select CONCAT(fieldNameList,'partnerprivate.payableUploadCheck~') into fieldNameList;
       IF(OLD.payableUploadCheck = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.payableUploadCheck = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.payableUploadCheck = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.payableUploadCheck = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.invoiceUploadCheck <> NEW.invoiceUploadCheck ) THEN
       select CONCAT(fieldNameList,'partnerprivate.invoiceUploadCheck~') into fieldNameList;
       IF(OLD.invoiceUploadCheck = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.invoiceUploadCheck = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.invoiceUploadCheck = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.invoiceUploadCheck = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.partnerPortalActive <> NEW.partnerPortalActive ) THEN
       select CONCAT(fieldNameList,'partnerprivate.partnerPortalActive~') into fieldNameList;
       IF(OLD.partnerPortalActive = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.partnerPortalActive = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.partnerPortalActive = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.partnerPortalActive = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.viewChild <> NEW.viewChild ) THEN
       select CONCAT(fieldNameList,'partnerprivate.viewChild~') into fieldNameList;
       IF(OLD.viewChild = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.viewChild = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.viewChild = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.viewChild = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.accountingDefault <> NEW.accountingDefault ) THEN
       select CONCAT(fieldNameList,'partnerprivate.accountingDefault~') into fieldNameList;
       IF(OLD.accountingDefault = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accountingDefault = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accountingDefault = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accountingDefault = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.stopNotAuthorizedInvoices <> NEW.stopNotAuthorizedInvoices ) THEN
       select CONCAT(fieldNameList,'partnerprivate.stopNotAuthorizedInvoices~') into fieldNameList;
       IF(OLD.stopNotAuthorizedInvoices = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.stopNotAuthorizedInvoices = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.stopNotAuthorizedInvoices = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.stopNotAuthorizedInvoices = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.doNotCopyAuthorizationSO <> NEW.doNotCopyAuthorizationSO ) THEN
       select CONCAT(fieldNameList,'partnerprivate.doNotCopyAuthorizationSO~') into fieldNameList;
       IF(OLD.doNotCopyAuthorizationSO = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.doNotCopyAuthorizationSO = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.doNotCopyAuthorizationSO = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.doNotCopyAuthorizationSO = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.emailSentAlert <> NEW.emailSentAlert ) THEN
       select CONCAT(fieldNameList,'partnerprivate.emailSentAlert~') into fieldNameList;
       IF(OLD.emailSentAlert = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.emailSentAlert = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.emailSentAlert = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.emailSentAlert = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.cardSettelment <> NEW.cardSettelment ) THEN
       select CONCAT(fieldNameList,'partnerprivate.cardSettelment~') into fieldNameList;
       IF(OLD.cardSettelment = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.cardSettelment = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.cardSettelment = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.cardSettelment = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.directDeposit <> NEW.directDeposit ) THEN
       select CONCAT(fieldNameList,'partnerprivate.directDeposit~') into fieldNameList;
       IF(OLD.directDeposit = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.directDeposit = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.directDeposit = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.directDeposit = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.fuelPurchase <> NEW.fuelPurchase ) THEN
       select CONCAT(fieldNameList,'partnerprivate.fuelPurchase~') into fieldNameList;
       IF(OLD.fuelPurchase = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.fuelPurchase = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.fuelPurchase = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.fuelPurchase = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.expressCash <> NEW.expressCash ) THEN
       select CONCAT(fieldNameList,'partnerprivate.expressCash~') into fieldNameList;
       IF(OLD.expressCash = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.expressCash = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.expressCash = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.expressCash = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.yruAccess <> NEW.yruAccess ) THEN
       select CONCAT(fieldNameList,'partnerprivate.yruAccess~') into fieldNameList;
       IF(OLD.yruAccess = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.yruAccess = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.yruAccess = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.yruAccess = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.creditCheck <> NEW.creditCheck ) THEN
       select CONCAT(fieldNameList,'partnerprivate.creditCheck~') into fieldNameList;
       IF(OLD.creditCheck = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.creditCheck = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.creditCheck = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.creditCheck = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.insuranceAuthorized <> NEW.insuranceAuthorized ) THEN
       select CONCAT(fieldNameList,'partnerprivate.insuranceAuthorized~') into fieldNameList;
       IF(OLD.insuranceAuthorized = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.insuranceAuthorized = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.insuranceAuthorized = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.insuranceAuthorized = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.progressiveType <> NEW.progressiveType ) THEN
       select CONCAT(fieldNameList,'partnerprivate.progressiveType~') into fieldNameList;
       IF(OLD.progressiveType = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.progressiveType = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.progressiveType = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.progressiveType = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.accountingHold <> NEW.accountingHold ) THEN
       select CONCAT(fieldNameList,'partnerprivate.accountingHold~') into fieldNameList;
       IF(OLD.accountingHold = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accountingHold = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accountingHold = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accountingHold = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





  IF (OLD.grpAllowed <> NEW.grpAllowed ) THEN
       select CONCAT(fieldNameList,'partnerprivate.grpAllowed~') into fieldNameList;
       IF(OLD.grpAllowed = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.grpAllowed = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.grpAllowed = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.grpAllowed = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.noDispatch <> NEW.noDispatch ) THEN
       select CONCAT(fieldNameList,'partnerprivate.noDispatch~') into fieldNameList;
       IF(OLD.noDispatch = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noDispatch = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noDispatch = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noDispatch = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.portalDefaultAccess <> NEW.portalDefaultAccess ) THEN
       select CONCAT(fieldNameList,'partnerprivate.portalDefaultAccess~') into fieldNameList;
       IF(OLD.portalDefaultAccess = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.portalDefaultAccess = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.portalDefaultAccess = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.portalDefaultAccess = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

   

  IF (OLD.noTransfereeEvaluation <> NEW.noTransfereeEvaluation ) THEN
       select CONCAT(fieldNameList,'partnerprivate.noTransfereeEvaluation~') into fieldNameList;
       IF(OLD.noTransfereeEvaluation = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noTransfereeEvaluation = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noTransfereeEvaluation = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noTransfereeEvaluation = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.oneRequestPerCF <> NEW.oneRequestPerCF ) THEN
       select CONCAT(fieldNameList,'partnerprivate.oneRequestPerCF~') into fieldNameList;
       IF(OLD.oneRequestPerCF = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.oneRequestPerCF = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.oneRequestPerCF = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.oneRequestPerCF = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



   

  IF (OLD.excludeFromParentUpdate <> NEW.excludeFromParentUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFromParentUpdate~') into fieldNameList;
       IF(OLD.excludeFromParentUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromParentUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromParentUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromParentUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludeFromParentPolicyUpdate <> NEW.excludeFromParentPolicyUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFromParentPolicyUpdate~') into fieldNameList;
       IF(OLD.excludeFromParentPolicyUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromParentPolicyUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromParentPolicyUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromParentPolicyUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludeFromParentFaqUpdate <> NEW.excludeFromParentFaqUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFromParentFaqUpdate~') into fieldNameList;
       IF(OLD.excludeFromParentFaqUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromParentFaqUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromParentFaqUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromParentFaqUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.soAllDrivers <> NEW.soAllDrivers ) THEN
       select CONCAT(fieldNameList,'partnerprivate.soAllDrivers~') into fieldNameList;
       IF(OLD.soAllDrivers = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.soAllDrivers = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.soAllDrivers = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.soAllDrivers = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludeFromParentQualityUpdate <> NEW.excludeFromParentQualityUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFromParentQualityUpdate~') into fieldNameList;
       IF(OLD.excludeFromParentQualityUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromParentQualityUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromParentQualityUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromParentQualityUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludeFromParentQualityUpdateRelo <> NEW.excludeFromParentQualityUpdateRelo ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFromParentQualityUpdateRelo~') into fieldNameList;
       IF(OLD.excludeFromParentQualityUpdateRelo = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFromParentQualityUpdateRelo = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFromParentQualityUpdateRelo = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFromParentQualityUpdateRelo = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludePublicFromParentUpdate <> NEW.excludePublicFromParentUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludePublicFromParentUpdate~') into fieldNameList;
       IF(OLD.excludePublicFromParentUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludePublicFromParentUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludePublicFromParentUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludePublicFromParentUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.excludePublicReloSvcsParentUpdate <> NEW.excludePublicReloSvcsParentUpdate ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludePublicReloSvcsParentUpdate~') into fieldNameList;
       IF(OLD.excludePublicReloSvcsParentUpdate = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludePublicReloSvcsParentUpdate = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludePublicReloSvcsParentUpdate = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludePublicReloSvcsParentUpdate = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.lumpSum <> NEW.lumpSum ) THEN
       select CONCAT(fieldNameList,'partnerprivate.lumpSum~') into fieldNameList;
       IF(OLD.lumpSum = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.lumpSum = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.lumpSum = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.lumpSum = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.detailedList <> NEW.detailedList ) THEN
       select CONCAT(fieldNameList,'partnerprivate.detailedList~') into fieldNameList;
       IF(OLD.detailedList = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.detailedList = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.detailedList = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.detailedList = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.noInsurance <> NEW.noInsurance ) THEN
       select CONCAT(fieldNameList,'partnerprivate.noInsurance~') into fieldNameList;
       IF(OLD.noInsurance = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.noInsurance = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.noInsurance = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.noInsurance = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

  IF (OLD.converted <> NEW.converted ) THEN
       select CONCAT(fieldNameList,'partnerprivate.converted~') into fieldNameList;
       IF(OLD.converted = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.converted = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.converted = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.converted = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


   

    IF ((OLD.longPercentage <> NEW.longPercentage) or (OLD.longPercentage is null and NEW.longPercentage is not null)
      or (OLD.longPercentage is not null and NEW.longPercentage is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.longPercentage~') into fieldNameList;
        IF(OLD.longPercentage is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.longPercentage is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.longPercentage is not null) then
            select CONCAT(oldValueList,OLD.longPercentage,'~') into oldValueList;
        END IF;
        IF(NEW.longPercentage is not null) then
            select CONCAT(newValueList,NEW.longPercentage,'~') into newValueList;
        END IF;
   END IF;     


   

    IF ((OLD.creditAmount <> NEW.creditAmount) or (OLD.creditAmount is null and NEW.creditAmount is not null)
      or (OLD.creditAmount is not null and NEW.creditAmount is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.creditAmount~') into fieldNameList;
        IF(OLD.creditAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.creditAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.creditAmount is not null) then
            select CONCAT(oldValueList,OLD.creditAmount,'~') into oldValueList;
        END IF;
        IF(NEW.creditAmount is not null) then
            select CONCAT(newValueList,NEW.creditAmount,'~') into newValueList;
        END IF;
   END IF;     



   

    IF ((OLD.cargoInsurance <> NEW.cargoInsurance) or (OLD.cargoInsurance is null and NEW.cargoInsurance is not null)
      or (OLD.cargoInsurance is not null and NEW.cargoInsurance is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.cargoInsurance~') into fieldNameList;
        IF(OLD.cargoInsurance is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.cargoInsurance is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.cargoInsurance is not null) then
            select CONCAT(oldValueList,OLD.cargoInsurance,'~') into oldValueList;
        END IF;
        IF(NEW.cargoInsurance is not null) then
            select CONCAT(newValueList,NEW.cargoInsurance,'~') into newValueList;
        END IF;
   END IF;     



   

    IF ((OLD.liabilityInsurance <> NEW.liabilityInsurance) or (OLD.liabilityInsurance is null and NEW.liabilityInsurance is not null)
      or (OLD.liabilityInsurance is not null and NEW.liabilityInsurance is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.liabilityInsurance~') into fieldNameList;
        IF(OLD.liabilityInsurance is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.liabilityInsurance is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.liabilityInsurance is not null) then
            select CONCAT(oldValueList,OLD.liabilityInsurance,'~') into oldValueList;
        END IF;
        IF(NEW.liabilityInsurance is not null) then
            select CONCAT(newValueList,NEW.liabilityInsurance,'~') into newValueList;
        END IF;
   END IF;     


   

    IF ((OLD.personalEscrow <> NEW.personalEscrow) or (OLD.personalEscrow is null and NEW.personalEscrow is not null)
      or (OLD.personalEscrow is not null and NEW.personalEscrow is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.personalEscrow~') into fieldNameList;
        IF(OLD.personalEscrow is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.personalEscrow is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.personalEscrow is not null) then
            select CONCAT(oldValueList,OLD.personalEscrow,'~') into oldValueList;
        END IF;
        IF(NEW.personalEscrow is not null) then
            select CONCAT(newValueList,NEW.personalEscrow,'~') into newValueList;
        END IF;
   END IF;     


   

    IF ((OLD.companyEscrow <> NEW.companyEscrow) or (OLD.companyEscrow is null and NEW.companyEscrow is not null)
      or (OLD.companyEscrow is not null and NEW.companyEscrow is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.companyEscrow~') into fieldNameList;
        IF(OLD.companyEscrow is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.companyEscrow is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.companyEscrow is not null) then
            select CONCAT(oldValueList,OLD.companyEscrow,'~') into oldValueList;
        END IF;
        IF(NEW.companyEscrow is not null) then
            select CONCAT(newValueList,NEW.companyEscrow,'~') into newValueList;
        END IF;
   END IF;     


   

    IF ((OLD.availableCredit <> NEW.availableCredit) or (OLD.availableCredit is null and NEW.availableCredit is not null)
      or (OLD.availableCredit is not null and NEW.availableCredit is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.availableCredit~') into fieldNameList;
        IF(OLD.availableCredit is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.availableCredit is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.availableCredit is not null) then
            select CONCAT(oldValueList,OLD.availableCredit,'~') into oldValueList;
        END IF;
        IF(NEW.availableCredit is not null) then
            select CONCAT(newValueList,NEW.availableCredit,'~') into newValueList;
        END IF;
   END IF;     


   

    IF ((OLD.lumpSumAmount <> NEW.lumpSumAmount) or (OLD.lumpSumAmount is null and NEW.lumpSumAmount is not null)
      or (OLD.lumpSumAmount is not null and NEW.lumpSumAmount is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.lumpSumAmount~') into fieldNameList;
        IF(OLD.lumpSumAmount is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.lumpSumAmount is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.lumpSumAmount is not null) then
            select CONCAT(oldValueList,OLD.lumpSumAmount,'~') into oldValueList;
        END IF;
        IF(NEW.lumpSumAmount is not null) then
            select CONCAT(newValueList,NEW.lumpSumAmount,'~') into newValueList;
        END IF;
   END IF;     

   IF (OLD.usaFlagRequired <> NEW.usaFlagRequired ) THEN
      select CONCAT(fieldNameList,'partnerprivate.usaFlagRequired~') into fieldNameList;
      select CONCAT(oldValueList,OLD.usaFlagRequired,'~') into oldValueList;
      select CONCAT(newValueList,NEW.usaFlagRequired,'~') into newValueList;
  END IF;
  
     IF (OLD.dot <> NEW.dot ) THEN
      select CONCAT(fieldNameList,'partnerprivate.dot~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dot,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dot,'~') into newValueList;
  END IF;
  
  IF (OLD.compensationYear <> NEW.compensationYear ) THEN
      select CONCAT(fieldNameList,'partnerprivate.compensationYear~') into fieldNameList;
      select CONCAT(oldValueList,OLD.compensationYear,'~') into oldValueList;
      select CONCAT(newValueList,NEW.compensationYear,'~') into newValueList;
  END IF;

  IF (OLD.claimBy <> NEW.claimBy ) THEN
      select CONCAT(fieldNameList,'partnerprivate.claimBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimBy,'~') into newValueList;
  END IF;

     IF (OLD.accountHolderForRelo <> NEW.accountHolderForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.accountHolderForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountHolderForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountHolderForRelo,'~') into newValueList;
  END IF;

     IF (OLD.billingUserForRelo <> NEW.billingUserForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billingUserForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingUserForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingUserForRelo,'~') into newValueList;
  END IF;

IF (OLD.accountManagerForRelo <> NEW.accountManagerForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.accountManagerForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.accountManagerForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.accountManagerForRelo,'~') into newValueList;
  END IF;
	
IF (OLD.payableUserForRelo <> NEW.payableUserForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.payableUserForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payableUserForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payableUserForRelo,'~') into newValueList;
  END IF;

   IF (OLD.pricingUserForRelo <> NEW.pricingUserForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.pricingUserForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pricingUserForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pricingUserForRelo,'~') into newValueList;
  END IF;

IF (OLD.auditorForRelo <> NEW.auditorForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.auditorForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.auditorForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.auditorForRelo,'~') into newValueList;
  END IF;

  IF (OLD.claimsUserForRelo <> NEW.claimsUserForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.claimsUserForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.claimsUserForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.claimsUserForRelo,'~') into newValueList;
  END IF;

   IF (OLD.coordinatorForRelo <> NEW.coordinatorForRelo ) THEN
      select CONCAT(fieldNameList,'partnerprivate.coordinatorForRelo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.coordinatorForRelo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.coordinatorForRelo,'~') into newValueList;
  END IF;

   IF (OLD.emailPrintOption <> NEW.emailPrintOption ) THEN
      select CONCAT(fieldNameList,'partnerprivate.emailPrintOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.emailPrintOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.emailPrintOption,'~') into newValueList;
  END IF;

   IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'partnerprivate.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;

   IF (OLD.networkPartnerCode <> NEW.networkPartnerCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.networkPartnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerCode,'~') into newValueList;
  END IF;

  IF (OLD.source <> NEW.source ) THEN
      select CONCAT(fieldNameList,'partnerprivate.source~') into fieldNameList;
      select CONCAT(oldValueList,OLD.source,'~') into oldValueList;
      select CONCAT(newValueList,NEW.source,'~') into newValueList;
  END IF;

     IF (OLD.vendorCode <> NEW.vendorCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.vendorCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCode,'~') into newValueList;
  END IF;

IF (OLD.insuranceHas <> NEW.insuranceHas ) THEN
      select CONCAT(fieldNameList,'partnerprivate.insuranceHas~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceHas,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceHas,'~') into newValueList;
  END IF;

 IF (OLD.insuranceOption <> NEW.insuranceOption ) THEN
      select CONCAT(fieldNameList,'partnerprivate.insuranceOption~') into fieldNameList;
      select CONCAT(oldValueList,OLD.insuranceOption,'~') into oldValueList;
      select CONCAT(newValueList,NEW.insuranceOption,'~') into newValueList;
  END IF;

IF (OLD.defaultVat <> NEW.defaultVat ) THEN
      select CONCAT(fieldNameList,'partnerprivate.defaultVat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.defaultVat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.defaultVat,'~') into newValueList;
  END IF;

  IF (OLD.billToAuthorization <> NEW.billToAuthorization ) THEN
      select CONCAT(fieldNameList,'partnerprivate.billToAuthorization~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billToAuthorization,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billToAuthorization,'~') into newValueList;
  END IF;

   IF (OLD.networkPartnerName <> NEW.networkPartnerName ) THEN
      select CONCAT(fieldNameList,'partnerprivate.networkPartnerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerName,'~') into newValueList;
  END IF;

  IF (OLD.vendorName <> NEW.vendorName ) THEN
      select CONCAT(fieldNameList,'partnerprivate.vendorName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorName,'~') into newValueList;
  END IF;

   IF (OLD.NPSscore <> NEW.NPSscore ) THEN
      select CONCAT(fieldNameList,'partnerprivate.NPSscore~') into fieldNameList;
      select CONCAT(oldValueList,OLD.NPSscore,'~') into oldValueList;
      select CONCAT(newValueList,NEW.NPSscore,'~') into newValueList;
  END IF;

 IF (OLD.vatBillingGroup <> NEW.vatBillingGroup ) THEN
      select CONCAT(fieldNameList,'partnerprivate.vatBillingGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vatBillingGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vatBillingGroup,'~') into newValueList;
  END IF;

 IF (OLD.privateCurrency <> NEW.privateCurrency ) THEN
      select CONCAT(fieldNameList,'partnerprivate.privateCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateCurrency,'~') into newValueList;
  END IF;

    IF (OLD.privateBankCode <> NEW.privateBankCode ) THEN
      select CONCAT(fieldNameList,'partnerprivate.privateBankCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateBankCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateBankCode,'~') into newValueList;
  END IF;

 IF (OLD.privateBankAccountNumber <> NEW.privateBankAccountNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.privateBankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateBankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateBankAccountNumber,'~') into newValueList;
  END IF;

IF (OLD.privateVatNumber <> NEW.privateVatNumber ) THEN
      select CONCAT(fieldNameList,'partnerprivate.privateVatNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateVatNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateVatNumber,'~') into newValueList;
  END IF;

    IF (OLD.customerSurveyEmail <> NEW.customerSurveyEmail ) THEN
       select CONCAT(fieldNameList,'partnerprivate.customerSurveyEmail~') into fieldNameList;
       IF(OLD.customerSurveyEmail = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.customerSurveyEmail = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.customerSurveyEmail = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.customerSurveyEmail = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

     IF (OLD.excludeFinancialExtract <> NEW.excludeFinancialExtract ) THEN
       select CONCAT(fieldNameList,'partnerprivate.excludeFinancialExtract~') into fieldNameList;
       IF(OLD.excludeFinancialExtract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.excludeFinancialExtract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.excludeFinancialExtract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.excludeFinancialExtract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
	
   
   
   
   
   
   
   
   
   
    IF (OLD.agentClassification <> NEW.agentClassification ) THEN
      select CONCAT(fieldNameList,'partnerprivate.privateVatNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.privateVatNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.privateVatNumber,'~') into newValueList;
  END IF;
  
  IF (OLD.rddBased <> NEW.rddBased ) THEN
      select CONCAT(fieldNameList,'partnerprivate.rddBased~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rddBased,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rddBased,'~') into newValueList;
  END IF;
  
  IF (OLD.rddDays <> NEW.rddDays ) THEN
      select CONCAT(fieldNameList,'partnerprivate.rddDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.rddDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.rddDays,'~') into newValueList;
  END IF;
  
  IF (OLD.internalBillingPerson <> NEW.internalBillingPerson ) THEN
      select CONCAT(fieldNameList,'partnerprivate.internalBillingPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.internalBillingPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.internalBillingPerson,'~') into newValueList;
  END IF;
  
  
  IF ((OLD.minimumMargin <> NEW.minimumMargin) or (OLD.minimumMargin is null and NEW.minimumMargin is not null)
      or (OLD.minimumMargin is not null and NEW.minimumMargin is null)) THEN
        select CONCAT(fieldNameList,'partnerprivate.minimumMargin~') into fieldNameList;
        IF(OLD.minimumMargin is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.minimumMargin is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.minimumMargin is not null) then
            select CONCAT(oldValueList,OLD.minimumMargin,'~') into oldValueList;
        END IF;
        IF(NEW.minimumMargin is not null) then
            select CONCAT(newValueList,NEW.minimumMargin,'~') into newValueList;
        END IF;
   END IF;     
   
CALL add_tblHistory (OLD.id,"partnerprivate", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$
