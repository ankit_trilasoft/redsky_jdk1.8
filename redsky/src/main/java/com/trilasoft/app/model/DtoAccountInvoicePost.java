package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.util.Date;
public class DtoAccountInvoicePost {
	private String recInvoiceNumber;
	private BigDecimal actualRevenueTolalInv = new BigDecimal(0);
	private String billToName;
	private Date recPostDate;
	private String shipNumber;
	private String recPostDateCount;
	private String companyDivision;
	private String paymentSentCount;
	private Date invoiceDate;
	private Date invoiceDateEnd;
	
	public Date getInvoiceDateEnd() {
		return invoiceDateEnd;
	}
	public void setInvoiceDateEnd(Date invoiceDateEnd) {
		this.invoiceDateEnd = invoiceDateEnd;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public BigDecimal getActualRevenueTolalInv() {
		return actualRevenueTolalInv;
	}
	public void setActualRevenueTolalInv(BigDecimal actualRevenueTolalInv) {
		this.actualRevenueTolalInv = actualRevenueTolalInv;
	}
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	public String getRecInvoiceNumber() {
		return recInvoiceNumber;
	}
	public void setRecInvoiceNumber(String recInvoiceNumber) {
		this.recInvoiceNumber = recInvoiceNumber;
	}
	public Date getRecPostDate() {
		return recPostDate;
	}
	public void setRecPostDate(Date recPostDate) {
		this.recPostDate = recPostDate;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	/**
	 * @return the recPostDateCount
	 */
	public String getRecPostDateCount() {
		return recPostDateCount;
	}
	/**
	 * @param recPostDateCount the recPostDateCount to set
	 */
	public void setRecPostDateCount(String recPostDateCount) {
		this.recPostDateCount = recPostDateCount;
	}
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public String getPaymentSentCount() {
		return paymentSentCount;
	}
	public void setPaymentSentCount(String paymentSentCount) {
		this.paymentSentCount = paymentSentCount;
	}

}
