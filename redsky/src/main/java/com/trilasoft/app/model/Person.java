package com.trilasoft.app.model;

import java.util.Date;

import org.appfuse.model.BaseObject;   

import javax.persistence.Entity;   
import javax.persistence.GenerationType;   
import javax.persistence.Id;   
import javax.persistence.GeneratedValue;   
import javax.persistence.Column;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;   
@Entity
@Table(name="person")
public class Person extends BaseObject {   
    private Long id;   
    public String firstName;   
    public String lastName;
    public String age;
    public Date myDate;
    
    @Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("firstName",
				firstName).append("lastName", lastName).append("age", age)
				.append("myDate", myDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Person))
			return false;
		Person castOther = (Person) other;
		return new EqualsBuilder().append(id, castOther.id).append(firstName,
				castOther.firstName).append(lastName, castOther.lastName)
				.append(age, castOther.age).append(myDate, castOther.myDate)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(firstName).append(
				lastName).append(age).append(myDate).toHashCode();
	}
	@Column(name="first_name", length=50)
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(name="last_name", length=50)
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	@Column
	public Date getMyDate() {
		return myDate;
	}
	public void setMyDate(Date myDate) {
		this.myDate = myDate;
	}
	@Column( length = 20)
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}   
  
}  

