<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
    <title><fmt:message key="userGuidesDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='userGuidesDetail.heading'/>"/>
    <style type="text/css">
    #mainPopup {
	padding-left:0px;
	padding-right:0px;
	
}
div#page {
margin:0;
padding:0;
text-align:center;
}
</style>
</head>

<s:form id="userGuidesForm" name="userGuidesForm"  action="saveUserGuides.html?true" method="post" validate="true" enctype="multipart/form-data">
<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a class="current"><span>User&nbsp;Guides&nbsp;Detail</span></a></li>
		     <li><a href="userGuidesPage.html?true&corpID=${sessionCorpID}"><span>User&nbsp;Guides&nbsp;List</span></a></li>
		  
		  </ul>
</div>

<s:hidden name="prevDisplayOrder" value="${userGuides.displayOrder}"></s:hidden>
<s:hidden name="userGuides.id" />
<s:hidden name="corpID" value="<%= request.getParameter("corpID")%>"/>
<c:if test="${not empty userGuides.fileName}">
<s:hidden name="fileFileName1" value="${userGuides.fileName}"/>
<s:hidden name="location1" value="${userGuides.location}"/>
</c:if>
<div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">  
<table cellspacing="2" cellpadding="3" border="0" class="detailTabLabel">
		  <tbody>  	
		  	<tr>
				<td class="listwhitetext" align="right" width="62">Module<font color="red" size="2">*</font></td>
	  			<td align="left" width="175"><s:select cssClass="list-menu" name="userGuides.module" list="%{userGuidesModule}" cssStyle="width:175px" headerKey="" headerValue=""/> </td>
	  			<td class="listwhitetext" align="right" width="62">Display&nbsp;Order#<font color="red" size="2">*</font></td>
	  			<td colspan="4"><s:textfield name="userGuides.displayOrder" size="3" maxlength='5' required="true" cssClass="input-text" onkeydown="return onlyNumberAllowed(event)"/></td>
	  			<td class="listwhitetext" align="right" colspan="2">Visible <s:checkbox name="userGuides.visible"  value ="${userGuides.visible }" cssStyle="vertical-align:middle;"/> </td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right" >Document&nbsp;Name<font color="red" size="2">*</font></td>
    			<td colspan="6"><s:textarea name="userGuides.documentName" cssStyle="height:70px;width:311px;" cssClass="textarea" /></td>
    			<td class="listwhitetext" align="right" colspan="2">Video<s:checkbox name="userGuides.videoFlag" value ="${userGuides.videoFlag}" cssStyle="vertical-align:middle;"/></td>
    			<td class="listwhitetext">(* Mark It's Video)</td>
    			
	  		</tr>
	  		<tr>
	  		<td class="listwhitetext"  align="right" >Manual&nbsp;Upload</td>
	  		<td colspan="3"><s:file name="file" label="%{getText('userGuidesForm.file')}"  size="33" />
			</td>
			<c:if test="${not empty userGuides.id}">
	  		<td colspan="6"><b><c:out value="${userGuides.fileName}"/></b>
	  		<a onclick="javascript:openWindow('UserGuidesResourceMgmtImageServletAction.html?id=${userGuides.id}&decorator=popup&popup=true',900,600);">
    		<c:if test="${not empty userGuides.fileName}">
    		<img src="images/downarrow.png" border="0" align="middle" style="cursor:pointer;"/></c:if></a> 
	  		</td>
	  		</c:if>
	  		</tr>
	  		<tr>
			<td class="listwhitetext" align="right" width="62">CorpID&nbsp;Checks<font color="red" size="2">*</font></td>
			<td colspan="8">
			<table cellspacing="0" cellpadding="0" border="0" style="margin:0px;">
		   <tbody>  	
		  	<tr>
	  		<td align="left" colspan="4"><s:select cssClass="list-menu"  name="userGuides.corpidChecks"  list="%{corpidChecks}" cssStyle="width:180px;" headerKey="" headerValue=""/> </td>
	  		<td class="listwhitetext" align="right" width="60">&nbsp;Corp&nbsp;Id<font color="red" size="2">*</font>&nbsp;</td>
    		<td align="left"><s:select cssClass="list-menu" name="userGuides.corpId" list="%{distinctCorpId}" cssStyle="width:72px" headerKey="" headerValue=""/></td>
	  		</tr>
	  		</tbody>
	  		</table>
	  		</td>
	  		<tr><td height="20"></td></tr>
	  		</tbody>
	  		</table>	  		
	  		
	  		</div>
	  		<div class="bottom-header"><span></span></div>
</div>
</div>
<table width="780px">
			<tbody>
				<tr>
				      <td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${userGuides.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="userGuides.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${userGuides.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
					
					
					<td align="right" class="listwhitetext" width="100"><b><fmt:message key='contractPolicy.createdBy' /></b></td>
					
					<c:if test="${not empty userGuides.id}">
						<s:hidden name="userGuides.createdBy"/>
						<td ><s:label name="createdBy" value="%{userGuides.createdBy}"/></td>
					</c:if>
					<c:if test="${empty userGuides.id}">
						<s:hidden name="userGuides.createdBy" value="${pageContext.request.remoteUser}"/>
						<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedOn'/></b></td>
					<s:hidden name="userGuides.updatedOn"/>
					<td width="130"><fmt:formatDate value="${userGuides.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedBy' /></b></td>
					<c:if test="${not empty userGuides.id}">
								<s:hidden name="userGuides.updatedBy"/>
								<td><s:label name="updatedBy" value="%{userGuides.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty userGuides.id}">
								<s:hidden name="userGuides.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
				</tr>
			</tbody>
			</table>  
<s:submit cssClass="cssbuttonA" method="save" key="button.save" onclick="return fieldValidation();" theme="simple" />
<c:if test="${not empty userGuides.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editUserGuides.html?true&corpID=true"/>'" 
	        value="<fmt:message  key="button.add"/>" tabindex="88"/> 
         	</c:if>
<s:reset cssClass="cssbutton1" key="Reset" />
</s:form>
<script type="text/javascript">
function fieldValidation(){
	if(document.forms['userGuidesForm'].elements['userGuides.module'].value==''){
		alert("Select module to continue.....");
	return false;	
	}else if(document.forms['userGuidesForm'].elements['userGuides.displayOrder'].value==''){
		alert("Select Display Order to continue.....");
		return false;
	}else if(document.forms['userGuidesForm'].elements['userGuides.documentName'].value==''){
		alert("Select Document Name to continue.....");
		return false;
	}else if(document.forms['userGuidesForm'].elements['userGuides.corpidChecks'].value==''){
		alert("Select Corpid Checks to continue.....");
		return false;
	}else if(document.forms['userGuidesForm'].elements['userGuides.corpId'].value==''){
		alert("Select CorpId to continue.....");
		return false;
	}
	return true;
}
</script>
<script type="text/javascript">
if(document.forms['userGuidesForm'].elements['userGuides.corpidChecks'].value==null || document.forms['userGuidesForm'].elements['userGuides.corpidChecks'].value==''){	
		document.forms['userGuidesForm'].elements['userGuides.corpidChecks'].value='All';
}
</script>
<script type="text/javascript">
if(document.forms['userGuidesForm'].elements['userGuides.corpId'].value==null || document.forms['userGuidesForm'].elements['userGuides.corpId'].value==''){	
	document.forms['userGuidesForm'].elements['userGuides.corpId'].value='TSFT';
}
</script>
<script>

function onlyNumberAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  ( keyCode==110)||( keyCode==109); 
}

</script>
