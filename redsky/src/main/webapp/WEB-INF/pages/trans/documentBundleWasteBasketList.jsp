<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<head>   
    <title>Document Bundle List</title>   
    <meta name="heading" content="Document Bundle List"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
 <style>
 span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
</style>

</head>


<s:form id="searchForm" name="searchForm" action="searchDocumentBundle.html" > 

		<div id="newmnav" style="margin-bottom:0px;!margin-bottom:-13px;">
		  
		  	<ul>
		    	<li><a href="documentBundleList.html?tabId=setUp" /><span>Setup</span></a></li>
		  	</ul>
		  	<ul>
		    	<li id="newmnav1"><a class="current" ><span>Waste Basket</span></li>
		  	</ul>
		  
		</div>
		<div class="spnblk">&nbsp;</div>
		
<display:table name="documentBundleList" class="table" requestURI="" id="documentBundleList" pagesize="10" style="width:99%;margin-top:1px;margin-left: 5px; " >
	<display:column  style="width:5px;">	
		<img id="show-${documentBundleList.id}" onclick ="showChild('${documentBundleList.id}','child${documentBundleList.id}',this,this.id,'${documentBundleList.formsId}');" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		<img id="hide-${documentBundleList.id}" onclick ="hideChild('${documentBundleList.id}','${documentBundleList_rowNum}',this.id);" src="${pageContext.request.contextPath}/images/minus1-small.png" style="display: none;" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
	<display:column title="Bundle Name" style="width: 90px;">
		<a href="editDocumentBundle.html?id=${documentBundleList.id}"> ${documentBundleList.bundleName}</a>
	</display:column>
	<display:column title="Send Point" style="width: 90px;">
		<c:if test="${not empty documentBundleList.sendPointTableName && not empty documentBundleList.sendPointFieldName}">
			<c:out value="${documentBundleList.sendPointTableName}.${documentBundleList.sendPointFieldName}"></c:out>
		</c:if>
		<c:if test="${not empty documentBundleList.sendPointTableName && empty documentBundleList.sendPointFieldName}">
			<c:out value="${documentBundleList.sendPointTableName}."></c:out>
		</c:if>
		<c:if test="${empty documentBundleList.sendPointTableName && not empty documentBundleList.sendPointFieldName}">
			<c:out value=".${documentBundleList.sendPointFieldName}"></c:out>
		</c:if>
		<c:if test="${empty documentBundleList.sendPointTableName && empty documentBundleList.sendPointFieldName}">
			<c:out value=""></c:out>
		</c:if>
	</display:column>
	<display:column property="routing" title="Routing" style="width:50px;"></display:column>
	<display:column property="job" title="Job Type" style="width:50px;"></display:column>
	<display:column property="mode" title="Mode" style="width:50px;"></display:column>
	<display:column title="Recover" style="width:15px;">
			<a><img align="middle" onclick="recover('${documentBundleList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recover.png"/></a>
	</display:column>
	<display:column title="Remove" style="width: 10px;">
		<a><img align="middle" onclick="confirmSubmit('${documentBundleList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	
</display:table>

</div>
</s:form>
<script type="text/javascript">
function recover(documentId){
	var agree=confirm("Are you sure you wish to Recover this row?");
	if (agree){
		var url = 'recoverDocumentBundle.html?&id='+documentId;
		location.href = url;
	 }else{
		return false;
	}
}

function confirmSubmit(documentId){
	var agree=confirm("Are you sure you wish to delete this row?");
	if (agree){
		var url = 'deleteDocumentBundle.html?&id='+documentId;
		location.href = url;
	 }else{
		return false;
	}
}

function showChild(id,divId,position,rowId,formsId){
	document.getElementById('show-'+id).style.display='none';
	document.getElementById('hide-'+id).style.display='block';
	
	var table=document.getElementById("documentBundleList");
	var rownum = document.getElementById(rowId);
	var myrow = rownum.parentNode;
	var newrow = myrow.parentNode.rowIndex;
	val = newrow+1;
	var row=table.insertRow(val);
	row.setAttribute("id","ch"+id);
	row.setAttribute("name",id);
	row.innerHTML = "<td colspan=\"10\"><div id="+id+"></div></td>";
	
	if(formsId!=null && !formsId==''){
	var url='documentBundleChildListAjax.html?ajax=1&reportsId='+formsId+'&decorator=simple&popup=true';
	new Ajax.Request(url,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			       var mydiv = document.getElementById(id);
			      mydiv.innerHTML = response;
			      mydiv.style.display='block';
			    },
			    onFailure: function(){
				    alert('Something went wrong...') 
				    }
			  });
	}else{
		  var mydiv = document.getElementById(id);
	      mydiv.innerHTML = 'Nothing Found To Display.';
	      mydiv.style.display='block';
	}
	     return 0;
}

function hideChild(id,rowNum,rowId){
	val = "ch"+id;
	var table = document.getElementById('documentBundleList');
    var rowCount = table.rows.length;
    for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        if(row != undefined && row != null && val == row.id){
        	table.deleteRow(i);
        	document.getElementById('show-'+id).style.display='block';
        	document.getElementById('hide-'+id).style.display='none';
        }
    }
}
</script>
