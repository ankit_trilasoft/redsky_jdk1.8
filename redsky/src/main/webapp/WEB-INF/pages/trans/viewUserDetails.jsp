<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="agentRequest.title"/></title>   
    <meta name="heading" content="<fmt:message key="agentRequest.title"/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
      <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
   
<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		
	</style>
   <style> 
.btn-group-sm > .btn, .btn-sm {    
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12) !important;
}
.btn.focus, .btn:focus, .btn:hover {outline:none !important;}

.alert { margin-bottom: 0px;}

#modalAlert{
    padding: 10px!important;
    overflow-y: inherit!important;
    margin-top: 0px!important;
}
.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
   .key_so_dashboard { background: url("images/key_partner_list.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 23px; margin-left:22%; }
    #overlay190 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}

.btn-outline {
    background-color: transparent;
    color: inherit;
    transition: all .5s;
}

.btn-primary {
color: #fff;
border-color: #308ee0;
}

.btn-primary.btn-outline {
    color: #308ee0;
}

.btn-success.btn-outline {
    color: #5cb85c;
}

.btn-info.btn-outline {
    color: #5bc0de;
}

.btn-warning.btn-outline {
    color: #f0ad4e;
}

.btn-danger.btn-outline {
    color: #d9534f;
}

.btn-primary.btn-outline:hover,
.btn-success.btn-outline:hover,
.btn-info.btn-outline:hover,
.btn-warning.btn-outline:hover,
.btn-danger.btn-outline:hover {
    color: #fff;
    </style>
 
</head>

<div class="modal-dialog"  id="myModal" style="width:1200px;height:1200px">
 <div class="modal-content">
 <div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Agent Template Details</h4>
      </div>
     
<div class="modal-body" id="myModal">
<div class="modal fade" id="showErrorModal" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg">
											</div>
									    </div>
									    <div class="modal-footer">
									     <button type="button" id="spanghclose" class="btn btn-default" data-dismiss="modal" onclick="refersh();" >Close</button>
									     </div>
								</div>
							</div>
						</div>
<div class="modal fade" id="caseModal" aria-hidden="true">
							
</div>
 <s:form id="UserDetailsForm" action="searchAgentRequest.html" method="post" > 
<s:hidden name="userRequestId" value="<%=request.getParameter("id") %>" />
   <c:if test="${not empty vieUserList}">
<s:set name="vieUserList" value="vieUserList" scope="request"/>
<div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Duplicate Details Found</h4>
      </div>
<display:table name="vieUserList" id="vieUserList" class="table" requestURI="" export="false" defaultsort="2" pagesize="10" style="width:99%;margin:0px 0px 10px 5px;">  		
<display:column title="User Name" style="width:65px"> 
<%-- <a href="javascript: void(0)" onclick= "getValue('${fn:replace(userContactList.fullName,"'","\\'")}','${userContactList.email}','${userContactList.phoneNumber}','${userContactList.userType}','${userContactList.enabled}');"> --%> 
	    	 <c:out value="${vieUserList.username}" />
	    </display:column>
 
<%--  <display:column property="username" sortable="true" title="User Name" style="width:65px"/> 
 --%>	    <display:column  title="Full Name" style="width:65px"> 
<%-- <a href="javascript: void(0)" onclick= "getValue('${fn:replace(userContactList.fullName,"'","\\'")}','${userContactList.email}','${userContactList.phoneNumber}','${userContactList.userType}','${userContactList.enabled}');"> --%> 
	    	 <c:out value="${vieUserList.fullName}" />
	    </display:column>
        <display:column property="jobFunction"  title="Job Function" style="width:65px"> <c:out value="${vieUserList.jobFunction}" /></display:column>  
        <display:column property="userTitle"  title="Job Title" style="width:65px"><c:out value="${vieUserList.userTitle}" /></display:column>
	    <display:column  title="Email Address" style="width:65px"> 
<%-- 	    	<a href="javascript: void(0)" onclick= "sendEmail('${userContactList.email}');"> --%>
	    	 <c:out value="${vieUserList.email}" /></a>
	    </display:column>
	    <display:column  title="Phone&nbsp;Number" style="width:65px"> 
	    	  	 <c:out value="${vieUserList.phoneNumber}" /></a>
	    </display:column>
	
   
	
</display:table>
</c:if>
 <c:if test="${empty vieUserList}">
<div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Duplicate Details Not Found</h4>
      </div>
</c:if>
<table style="padding-top:10px !important;">
<tr>
<td align="center"  style="width:180px; font-size:14px; border:none;">Action Need To Be Taken</td>
<td style="border:none;"><s:select  cssClass="list-menu" id="status" name="status" list="{'Approved','Rejected'}" onchange="statusValue(this);" headerKey="" headerValue="" cssStyle="width:75px; background:#e5e5e5; border-radius:3px; margin-left:6px;" />
</td>
</tr>
</table>
  <div class="modal-footer">
<button type="button" id="spanghclose" class="btn btn-default" data-dismiss="modal" onclick="refersh();" >Close</button>
</div>	
<div id="overlay190">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing .......Please wait<br></font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>							    
</s:form>


</div> 
</div></div>


<script type="text/javascript">
var http2 = getHTTPObject();
function getHTTPObject(){
	var xmlhttp;
	if(window.XMLHttpRequest)
	{
	    xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
	    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	    if (!xmlhttp)
	    {
	        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	    }
	}
	return xmlhttp;
	}  
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay190"].visibility='hide';
        else
           document.getElementById("overlay190").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay190"].visibility='show';
       else
          document.getElementById("overlay190").style.visibility='visible';
   }
}
var target;
var id;

function statusValue(target){
	 

		
	    var userid = document.forms['UserDetailsForm'].elements['userRequestId'].value;
		
	  var position='';
	  var url="updatedUserData.html?ajax=1&decorator=simple&popup=true&userStatus=" + encodeURI(target.value)+"&id="+userid;
	
		document.forms['UserDetailsForm'].action = url;
		document.forms['UserDetailsForm'].submit();

}

  function  refersh()
  {
	  var modal = document.getElementById('myModal');
  	  modal.style.display = "none";
  	  window.location.reload(true);
  }

     
var http2 = getHTTPObject();

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay190"].visibility='hide';
        else
           document.getElementById("overlay190").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay190"].visibility='show';
       else
          document.getElementById("overlay190").style.visibility='visible';
   }
}

showOrHide(0);
/* function sendEmail1(target){
The below agent “Star International Movers” is already existing in our system RedSky. T164581 is the agent code for the same.
	var originEmail = target;
	var subject = 'C/F# ${customerFile.sequenceNumber}';
	subject = subject+" ${customerFile.firstName}";
	subject = subject+" ${customerFile.lastName}";
	
var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
win = window.open(mailto_link,'emailWindow'); 
if (win && win.open &&!win.closed) win.close(); 
}  */
//showOrHide(0);

</script>
