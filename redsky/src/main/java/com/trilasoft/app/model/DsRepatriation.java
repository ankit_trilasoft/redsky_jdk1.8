package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="dsrepatriation")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DsRepatriation {
	private Long id;
	private Long serviceOrderId;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String vendorCode;
	private String vendorName;
	private String vendorContact;
	private String vendorEmail;
	private Date serviceStartDate;
	private Date serviceEndDate;
	private Date providerNotificationDate;
	private Date providerNotification;
	private Date serviceConfirmedDate1;
	private Date serviceConfirmedDate2;
	private String serviceProviderInformation;
	private Date utilitiesShutOff;
	private Date moveOutDate;
	private Date securityDepositReturnedDate;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorContact", vendorContact)
				.append("vendorEmail", vendorEmail).append("serviceStartDate",
						serviceStartDate).append("serviceEndDate",
						serviceEndDate).append("providerNotificationDate",
						providerNotificationDate).append(
						"providerNotification", providerNotification).append(
						"serviceConfirmedDate1", serviceConfirmedDate1).append(
						"serviceConfirmedDate2", serviceConfirmedDate2).append(
						"serviceProviderInformation",
						serviceProviderInformation).append("utilitiesShutOff",
						utilitiesShutOff).append("moveOutDate", moveOutDate)
				.append("securityDepositReturnedDate",
						securityDepositReturnedDate).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsRepatriation))
			return false;
		DsRepatriation castOther = (DsRepatriation) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				serviceOrderId, castOther.serviceOrderId).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorContact,
						castOther.vendorContact).append(vendorEmail,
						castOther.vendorEmail).append(serviceStartDate,
						castOther.serviceStartDate).append(serviceEndDate,
						castOther.serviceEndDate).append(
						providerNotificationDate,
						castOther.providerNotificationDate).append(
						providerNotification, castOther.providerNotification)
				.append(serviceConfirmedDate1, castOther.serviceConfirmedDate1)
				.append(serviceConfirmedDate2, castOther.serviceConfirmedDate2)
				.append(serviceProviderInformation,
						castOther.serviceProviderInformation).append(
						utilitiesShutOff, castOther.utilitiesShutOff).append(
						moveOutDate, castOther.moveOutDate).append(
						securityDepositReturnedDate,
						castOther.securityDepositReturnedDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId).append(
				corpID).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(vendorCode).append(vendorName)
				.append(vendorContact).append(vendorEmail).append(
						serviceStartDate).append(serviceEndDate).append(
						providerNotificationDate).append(providerNotification)
				.append(serviceConfirmedDate1).append(serviceConfirmedDate2)
				.append(serviceProviderInformation).append(utilitiesShutOff)
				.append(moveOutDate).append(securityDepositReturnedDate)
				.toHashCode();
	}
	@Column(length=30)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Date getMoveOutDate() {
		return moveOutDate;
	}
	public void setMoveOutDate(Date moveOutDate) {
		this.moveOutDate = moveOutDate;
	}
	@Column
	public Date getProviderNotification() {
		return providerNotification;
	}
	public void setProviderNotification(Date providerNotification) {
		this.providerNotification = providerNotification;
	}
	@Column
	public Date getProviderNotificationDate() {
		return providerNotificationDate;
	}
	public void setProviderNotificationDate(Date providerNotificationDate) {
		this.providerNotificationDate = providerNotificationDate;
	}
	@Column
	public Date getSecurityDepositReturnedDate() {
		return securityDepositReturnedDate;
	}
	public void setSecurityDepositReturnedDate(Date securityDepositReturnedDate) {
		this.securityDepositReturnedDate = securityDepositReturnedDate;
	}
	@Column
	public Date getServiceConfirmedDate1() {
		return serviceConfirmedDate1;
	}
	public void setServiceConfirmedDate1(Date serviceConfirmedDate1) {
		this.serviceConfirmedDate1 = serviceConfirmedDate1;
	}
	@Column
	public Date getServiceConfirmedDate2() {
		return serviceConfirmedDate2;
	}
	public void setServiceConfirmedDate2(Date serviceConfirmedDate2) {
		this.serviceConfirmedDate2 = serviceConfirmedDate2;
	}
	@Column
	public Date getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	@Column(length=30)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column(length=200)
	public String getServiceProviderInformation() {
		return serviceProviderInformation;
	}
	public void setServiceProviderInformation(String serviceProviderInformation) {
		this.serviceProviderInformation = serviceProviderInformation;
	}
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getUtilitiesShutOff() {
		return utilitiesShutOff;
	}
	public void setUtilitiesShutOff(Date utilitiesShutOff) {
		this.utilitiesShutOff = utilitiesShutOff;
	}
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column(length=255)
	public String getVendorContact() {
		return vendorContact;
	}
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	@Column(length=65)
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	@Column(length=255)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

}
