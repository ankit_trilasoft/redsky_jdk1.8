package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="dspreviewtrip")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class DsPreviewTrip extends BaseObject{
	private Long id;
	private Long serviceOrderId;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date updatedOn;
	private Date createdOn;
	private String vendorCode;
	private String vendorName;
	private String vendorContact;
	private String vendorEmail;
	private Date serviceStartDate;
	private Date serviceEndDate;
	private Date travelPlannerNotification ;
	private Date travelDeparture  ;
	private Date travelArrival  ;
	private String hotelDetails ;
	private Date providerNotificationAndServicesAuthorized  ;
	private Date  meetGgreetAtAirport  ;
	private String areaOrientationDays;
	private String flightNumber;
	private String flightArrivalTime;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"serviceOrderId", serviceOrderId).append("corpID", corpID)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn).append("createdOn", createdOn)
				.append("vendorCode", vendorCode).append("vendorName",
						vendorName).append("vendorContact", vendorContact)
				.append("vendorEmail", vendorEmail).append("serviceStartDate",
						serviceStartDate).append("serviceEndDate",
						serviceEndDate).append("travelPlannerNotification",
						travelPlannerNotification).append("travelDeparture",
						travelDeparture).append("travelArrival", travelArrival)
				.append("hotelDetails", hotelDetails).append(
						"providerNotificationAndServicesAuthorized",
						providerNotificationAndServicesAuthorized).append(
						"meetGgreetAtAirport", meetGgreetAtAirport).append(
						"areaOrientationDays", areaOrientationDays).append(
						"flightNumber", flightNumber).append(
						"flightArrivalTime", flightArrivalTime).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof DsPreviewTrip))
			return false;
		DsPreviewTrip castOther = (DsPreviewTrip) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				serviceOrderId, castOther.serviceOrderId).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(updatedOn,
						castOther.updatedOn).append(createdOn,
						castOther.createdOn).append(vendorCode,
						castOther.vendorCode).append(vendorName,
						castOther.vendorName).append(vendorContact,
						castOther.vendorContact).append(vendorEmail,
						castOther.vendorEmail).append(serviceStartDate,
						castOther.serviceStartDate).append(serviceEndDate,
						castOther.serviceEndDate).append(
						travelPlannerNotification,
						castOther.travelPlannerNotification).append(
						travelDeparture, castOther.travelDeparture).append(
						travelArrival, castOther.travelArrival).append(
						hotelDetails, castOther.hotelDetails).append(
						providerNotificationAndServicesAuthorized,
						castOther.providerNotificationAndServicesAuthorized)
				.append(meetGgreetAtAirport, castOther.meetGgreetAtAirport)
				.append(areaOrientationDays, castOther.areaOrientationDays)
				.append(flightNumber, castOther.flightNumber).append(
						flightArrivalTime, castOther.flightArrivalTime)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId).append(
				corpID).append(createdBy).append(updatedBy).append(updatedOn)
				.append(createdOn).append(vendorCode).append(vendorName)
				.append(vendorContact).append(vendorEmail).append(
						serviceStartDate).append(serviceEndDate).append(
						travelPlannerNotification).append(travelDeparture)
				.append(travelArrival).append(hotelDetails).append(
						providerNotificationAndServicesAuthorized).append(
						meetGgreetAtAirport).append(areaOrientationDays)
				.append(flightNumber).append(flightArrivalTime).toHashCode();
	}
	@Column(length=2)
	public String getAreaOrientationDays() {
		return areaOrientationDays;
	}
	public void setAreaOrientationDays(String areaOrientationDays) {
		this.areaOrientationDays = areaOrientationDays;
	}
	
	
	@Column(length=20)
	public String getFlightNumber() {
		return flightNumber;
	}
	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}
	@Column(length=30)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=5000)
	public String getHotelDetails() {
		return hotelDetails;
	}
	public void setHotelDetails(String hotelDetails) {
		this.hotelDetails = hotelDetails;
	}
	
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public Date getMeetGgreetAtAirport() {
		return meetGgreetAtAirport;
	}
	public void setMeetGgreetAtAirport(Date meetGgreetAtAirport) {
		this.meetGgreetAtAirport = meetGgreetAtAirport;
	}
	
	
	@Column
	public String getFlightArrivalTime() {
		return flightArrivalTime;
	}
	@Column
	public Date getProviderNotificationAndServicesAuthorized() {
		return providerNotificationAndServicesAuthorized;
	}
	public void setProviderNotificationAndServicesAuthorized(
			Date providerNotificationAndServicesAuthorized) {
		this.providerNotificationAndServicesAuthorized = providerNotificationAndServicesAuthorized;
	}
	
	@Column
	public Date getServiceEndDate() {
		return serviceEndDate;
	}
	public void setServiceEndDate(Date serviceEndDate) {
		this.serviceEndDate = serviceEndDate;
	}
	
	@Column
	public Date getServiceStartDate() {
		return serviceStartDate;
	}
	public void setServiceStartDate(Date serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}
	
	@Column
	public Date getTravelArrival() {
		return travelArrival;
	}
	public void setTravelArrival(Date travelArrival) {
		this.travelArrival = travelArrival;
	}
	
	@Column
	public Date getTravelDeparture() {
		return travelDeparture;
	}
	public void setTravelDeparture(Date travelDeparture) {
		this.travelDeparture = travelDeparture;
	}
	
	@Column
	public Date getTravelPlannerNotification() {
		return travelPlannerNotification;
	}
	public void setTravelPlannerNotification(Date travelPlannerNotification) {
		this.travelPlannerNotification = travelPlannerNotification;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=25)
	public String getVendorCode() {
		return vendorCode;
	}
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	@Column(length=225)
	public String getVendorContact() {
		return vendorContact;
	}
	public void setVendorContact(String vendorContact) {
		this.vendorContact = vendorContact;
	}
	@Column(length=65)
	public String getVendorEmail() {
		return vendorEmail;
	}
	public void setVendorEmail(String vendorEmail) {
		this.vendorEmail = vendorEmail;
	}
	
	@Column(length=255)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column(length=20)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public void setFlightArrivalTime(String flightArrivalTime) {
		this.flightArrivalTime = flightArrivalTime;
	}
}
