<%@ include file="/common/taglibs.jsp"%>  

<head>   
    <title><fmt:message key="roleAccessList.title"/></title>   
    <meta name="heading" content="<fmt:message key='roleAccessList.heading'/>"/>   
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
<c:set var="buttons">  

     <input type="button" class="cssbutton" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editRoleAccess.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set>   


      
<s:form id="roleAccessListForm" method="post"> 
<div id="otabs">
<ul>
<li><a class="current"><span>Role Accesss List</span></a></li>
</ul>
</div><div class="spnblk">&nbsp;</div>
<div id="Layer1">
   <display:table name="roleAccesss" class="table" requestURI="" id="roleAccessList" defaultsort="2" export="true" pagesize="10">   
   
    <display:column property="id" sortable="true" titleKey="roleAccess.id" url="/editRoleAccess.html" paramId="id" paramProperty="id"/>
    <display:column property="role" sortable="true" titleKey="roleAccess.role"/>
    <display:column property="action" sortable="true" titleKey="roleAccess.action"/>
    <display:column property="menu1" sortable="true" titleKey="roleAccess.menu1"/>
    <display:column property="menu2" sortable="true" titleKey="roleAccess.menu2"/>
  	<display:column property="menu3" sortable="true" titleKey="roleAccess.menu3"/>
    <display:column property="menu4" sortable="true" titleKey="roleAccess.menu4"/>
  	<display:column property="menu5" sortable="true" titleKey="roleAccess.menu5"/>
    <display:setProperty name="export.excel.filename" value="roleAccess List.xls"/>   
    <display:setProperty name="export.csv.filename" value="roleAccess List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="roleAccess List.pdf"/>   
</display:table>   
  </div>
<c:out value="${buttons}" escapeXml="false" />   
</s:form> 
<table><tr>
	<td style="height:150px;!height:180px"></td></tr>
	</table> 

<script type="text/javascript">   
    highlightTableRows("roleAccessList");   
</script>  
