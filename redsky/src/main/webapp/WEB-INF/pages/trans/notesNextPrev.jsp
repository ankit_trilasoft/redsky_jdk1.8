<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Notes List</title>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Notes List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="notesNextPrevList" class="table" requestURI="" id="notesNextPrevList" export="false" defaultsort="1" >
	<display:column title="Note Type" ><a href="#" onclick="goToUrl(${notesNextPrevList.id});" ><c:out value="${notesNextPrevList.noteType}" /></a></display:column>   
		<display:column property="noteSubType" titleKey="notes.noteSubType" style="width:10px" />
		<display:column property="subject" titleKey="notes.subject" style="width:10px"/>  
	</display:table>
</div>
