ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `originContactLastName` VARCHAR(35) AFTER `ugwIntId`,ADD COLUMN `contactLastName` VARCHAR(35) AFTER `originContactLastName`;

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'Administration', 'Administration', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Extract', 'Extract', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Form/Report', 'Form/Report', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'haulingManagement', 'Hauling Management', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Partner', 'Partner', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'referenceFunction', 'Reference Function', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Resource', 'Resource', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'scheduleResources', 'Schedule Resources', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'serviceOrder', 'Service Order', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'sO/MgmtInfo', 'SO/Mgmt Info', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'Vanline', 'Vanline', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar'),
('TSFT', 'workTicket', 'Work Ticket', 20, 'MODULE','en',now(),'kkumar',now(),'kkumar');
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','MODULE',20,true,now(),'kkumar',now(),'kkumar',1,1,'');

