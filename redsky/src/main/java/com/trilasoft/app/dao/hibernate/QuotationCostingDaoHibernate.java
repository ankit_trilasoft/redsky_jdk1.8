package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.QuotationCostingDao;
import com.trilasoft.app.model.QuotationCosting;


public class QuotationCostingDaoHibernate extends GenericDaoHibernate<QuotationCosting, Long> implements QuotationCostingDao {

	
	public QuotationCostingDaoHibernate() {   
        super(QuotationCosting.class);   
    }   
 
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from QuotationCosting where id=?",id);
    }
    
    public List<QuotationCosting> findBySequenceNumber(String sequenceNumber) {
    	return getHibernateTemplate().find("from QuotationCosting where sequenceNumber=?",sequenceNumber);
    }

}
