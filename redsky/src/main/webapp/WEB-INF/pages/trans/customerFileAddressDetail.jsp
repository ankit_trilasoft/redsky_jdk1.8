<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>  
 <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
											<tr><td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr class="subcontenttabChild">
											<td ><b>&nbsp;Origin</b></td>											
											</tr>
											</table>
											</td></tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" style="width:95px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td width="65px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCompany'/></td>
																</tr>
																<tr>
																<td align="left" width="80"></td>
															    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress1" onblur="titleCase(this)"  cssStyle="width:230px" maxlength="100" tabindex="55" onkeydown="disabledSOBtn();"/></td>
																 <td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text"	id="originCompanyCodeId" name="customerFile.originCompanyCode" tabindex="32" cssStyle="width:80px;" maxlength="8"
							                                     onchange="valid(this,'special');findOriginCompanyName();" />
		                                                         <img style="vertical-align:top;padding-right:5px;" class="openpopup" id="originCompanyCode.popupImage" width="17" height="20" onclick="openOriginCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.originCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	                                                             <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="originCompanyNameId" name="customerFile.originCompany"  cssStyle="width:250px;" maxlength="280"  tabindex="56" onkeydown="disabledSOBtn();" onchange="findOriginName();copyCompanyToDestination();" /> 
	                                                               <configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
		                                                           <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetails(this,'originCompanyCodeId');" src="<c:url value='/images/address2.png'/>" />
				                                                  </configByCorp:fieldVisibility>
		                                                          <div id="originCompanyNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
		                                                           </td>
															    </tr>
																</table>
																</td>
															</tr>
															<tr>
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
																<tr>
																<td align="left" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress2" onblur="titleCase(this)"  cssStyle="width:230px" maxlength="100" tabindex="57" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																</tr>
																<tr>
																<td align="left" ></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress3" cssStyle="width:230px" onblur="titleCase(this)" maxlength="100" tabindex="58" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																</tr>
																</table>
																</td>
																<td style="margin:0px; padding: 0px;" >
																<c:if test="${not empty customerFile.id}">
																<fieldset style="padding-top:4px;width:347px;">
																<legend>Update Address</legend>
																	<table class="detailTabLabel" cellspacing="0" cellpadding="0">
																	<tr>
																	<td width="12px">&nbsp;</td>
													                <td><input type="button" name="originSoUpdate" value="In All S/Os" class="cssbuttonB" style="!width:90px;width:100px;" tabindex="59" onclick="updateOriginSoAdd();" /></td>												               
													              	<td width="5px">&nbsp;</td>
													                <td><input type="button" name="originSoTicketUpdate" value="In All S/Os and Tickets" style="!width:120px;width:170px;" class="cssbuttonB" tabindex="60" onclick="updateOriginAddSoAndTkt();" /></td>
													               </tr>
													               </table>
													               </fieldset>
													               </c:if>
													               </td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td align="left" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left" ></td>
																<td width="57px"></td>
																<td align="left" id="originStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originState' /></td>
																<td align="left" id="originStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originState' /><font color="red" size="2">*</font></td>
																</tr>
																<tr>
																<td align="left" style="width:100px">&nbsp;</td>
																<td>
																<configByCorp:customDropDown listType="map" list="${ocountry_isactive}" fieldValue="${customerFile.originCountry}" 
																			attribute="class=list-menu name=customerFile.originCountry id=ocountry style=width:235px onkeydown=disabledSOBtn(); onchange=zipCode();changeStatus();getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this);disabledSOBtn();enableStateListOrigin();  headerKey='' headerValue='' tabindex=61 "/>							
																</td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
																<td width="57px"></td>
																<td>
																<configByCorp:customDropDown listType="map" list="${ostates_isactive}" fieldValue="${customerFile.originState}" 
															   attribute="class=list-menu id=originState name=customerFile.originState style=width:368px onkeydown=disabledSOBtn(); onchange=changeStatus();autoPopulate_customerFile_originCityCode(this);disabledSOBtn(); headerKey='' headerValue='' tabindex=62 "/>							
																</td>
																</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:100px"></td>
															<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/><font color="red" size="2">*</font></td>
																<td align="left" style="width:5px"></td>
																<td align="left" style="width:10px"></td>
																<td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td>
																<td align="left" id="zipCodeRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.originZip' /><font color="red" size="2">*</font></td>
																</tr>
															<tr>
															<td style="width:100px"></td>
															<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" id="originCity" cssStyle="width:137px;"  maxlength="30"   onkeypress="" onkeyup="autoCompleterAjaxCallOriCity();" onkeydown="disabledSOBtn();" onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this,'special');" tabindex="63"/></td>
																<td align="left" class="listwhitetext" style="width:10px"><div id="zipCodeList" style="vertical-align:middle;"><a><img class="openpopup" id="navigation8" onclick="findCityStateNotPrimary('OZ', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></td>
																<td align="left" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" cssStyle="width:54px;" maxlength="10" onchange="findCityStateOfZipCode(this,'OZ'),disabledSOBtn();" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="findCityStateOfZipCode(this,'OZ');valid(this,'special');" tabindex="64"/></div></td>
																<td align="left" style="width:5px"></td>
																<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
																<td>
                                                            <a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover=""  >&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></font></a>
                                                            </td>
                                                            </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																<td align="left" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:120px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/><configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
															
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/><configByCorp:fieldVisibility componentId="component.field.customerFile.phoneNumberMandatory"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td align="left" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" cssStyle="width:155px;"  maxlength="20"  tabindex="65" onkeydown="disabledSOBtn();"/></td>
																<td style="width:10px"> </td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" cssStyle="width:62px;" maxlength="10" onkeydown="disabledSOBtn();" tabindex="66"/></td>
																<td align="left" style="width:20px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone"  cssStyle="width:100px;" maxlength="20"  tabindex="67" onkeydown="disabledSOBtn();"/></td>
																<td align="left" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:100px;" maxlength="25"  tabindex="68" onkeydown="disabledSOBtn();"/></td>
																<td align="left" style="width:25px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" cssStyle="width:120px;" maxlength="20"  tabindex="69" onkeydown="disabledSOBtn();"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/></td>
																<td align="left" style="width:74px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td align="left" style="width:15px"></td>
																<td></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" style="width:100px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" cssStyle="width:231px;" maxlength="65" tabindex="70" onchange="EmailPortDate();" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:25px"><a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.email'].value)">&nbsp;<img class="openpopup" onclick="" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></a></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" cssStyle="width:218px;" maxlength="65" tabindex="71" onkeydown="disabledSOBtn();" onchange="autoPopulateSecondryEmail();"/></td>
																<td align="left" class="listwhitetext" style="width:23px">&nbsp;<a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.email2'].value)"><img class="openpopup" onclick="" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/></a></td>
																<td align="left" style="width:1px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.originPreferredContactTime" cssStyle="width:120px;" maxlength="30" tabindex="72" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="position:absolute;right:45px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countOriginNotes == '0' || countOriginNotes == '' || countOriginNotes == null}">
																		<td style="position:absolute;right:45px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="position:absolute;right:45px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
														</tbody>
													</table>
													</td>
												</tr>
												<tr><td style="width:100px; height:10px;" colspan="7"></td></tr>
												<tr><td align="center" colspan="15" class="vertlinedata"></td></tr>
												<tr><td>
											<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr class="subcontenttabChild">
												<td><b>&nbsp;Destination</b></td>
												</tr>
												</table>
												</td>
												</tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" style="width:80px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td width="65px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCompany'/></td>
																</tr>
																<tr>
																<td align="left" width="95px"></td>
															   <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress1" onblur="titleCase(this)"  cssStyle="width:230px;" maxlength="100" tabindex="73" onkeydown="disabledSODestinBtn();"/></td>
																<td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text"	id="destCompanyCodeID" name="customerFile.destinationCompanyCode" tabindex="32" cssStyle="width:80px;" maxlength="8"
						                                           onchange="valid(this,'special');findDestinationCompanyName();" />
		                                                          <img style="vertical-align:top;padding-right:5px;" class="openpopup" id="destCompanyCode.popupImage" width="17" height="20" onclick="openDestinationCompanyCodePopUp();document.forms['customerFileForm'].elements['customerFile.destinationCompanyCode'].focus();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	                                                              <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="destinCompanyNameId" name="customerFile.destinationCompany" cssStyle="width:250px;" maxlength="280" tabindex="74" onkeydown="disabledSODestinBtn();" onchange="findDestinationName();"/>
	                                                                <configByCorp:fieldVisibility componentId="component.customerFile.IkeaRequest">
		                                                         <img align="top" id="viewDetails" class="openpopup" width="17" height="20" title="View details"	onclick="viewPartnerDetails(this,'destCompanyCodeID');" src="<c:url value='/images/address2.png'/>" />
				                                                  </configByCorp:fieldVisibility> 
		                                                          <div id="destinCompanyNameDiv" class="autocomplete" style="z-index: 9999; position: absolute; margin-top: 25px; left: 226px;"></div>
																</td>
															     </tr>
																</table>
																</td>
															</tr>
															<tr>
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
																<tr>
																<td align="left" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress2" onblur="titleCase(this)"  cssStyle="width:230px;" maxlength="100" tabindex="75" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																</tr>
																<tr>
																<td align="left"></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress3" onblur="titleCase(this)" cssStyle="width:230px;" maxlength="100" tabindex="76" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																</tr>
																</table>
																</td>
																<td style="margin:0px; padding: 0px;" >
																<c:if test="${not empty customerFile.id}">
																<fieldset style="padding-top:4px;width:347px;">
																<legend>Update Address</legend>
																	<table class="detailTabLabel" cellspacing="0" cellpadding="0">
																	<tr>
																	<td width="12px">&nbsp;</td>															
   													                <td ><input type="button" name="originSoDestinUpdate" value="In All S/Os" style="!width:90px;width:100px;" class="cssbuttonB" onclick="updateDestinAddSo();" /></td>
													               <td width="5px">&nbsp;</td>
													                <td ><input type="button" name="originSoTicketDestinUpdate" value="In All S/Os and Tickets" style="!width:120px;width:170px;" class="cssbuttonB" onclick="updateDestinAddSoAndTkt();" /></td>
													                </tr>
													               </table>
													               </fieldset>
													               </c:if>	
													               </td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
																<td align="left" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left" ></td>
																<td width="57px"></td>
																<td align="left" id="destinationStateRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationState' /></td>
																<td align="left" id="destinationStateRequiredTrue" class="listwhitetext"><fmt:message key='customerFile.destinationState' /><font color="red" size="2">*</font></td>
																</tr>
																<tr>
																<td align="left" style="width:100px">&nbsp;</td>
																<td>
																<configByCorp:customDropDown listType="map" list="${dcountry_isactive}" fieldValue="${customerFile.destinationCountry}" 
																			attribute="class=list-menu name=customerFile.destinationCountry id=dcountry style=width:235px onkeydown=disabledSODestinBtn(); onchange=state();changeStatus();getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);getDestinationState(this);disabledSODestinBtn();enableStateListDestin();  headerKey='' headerValue='' tabindex=77 "/>																
																</td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();"/></td>
																<td width="57px"></td>
																<td>
																<configByCorp:customDropDown listType="map" list="${dstates_isactive}" fieldValue="${customerFile.destinationState}" 
																			attribute="class=list-menu id=destinationState name=customerFile.destinationState style=width:368px onkeydown=disabledSODestinBtn(); onchange=changeStatus();autoPopulate_customerFile_destinationCityCode(this);disabledSODestinBtn(); headerKey='' headerValue='' tabindex=78"/>																																							
																</td>
																</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td style="width:100px"></td>
													        <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/><font color="red" size="2">*</font></td>
														    <td align="left" style="width:5px"></td>
														    <td align="left" style="width:10px"></td>
														    <td align="left"  class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															</tr>
													       <tr>
													        <td style="width:100px"></td>
													        <td><s:textfield cssClass="input-text upper-case"  name="customerFile.destinationCity" id="destinationCity" cssStyle="width:137px;" maxlength="30"  onkeyup="autoCompleterAjaxCallDestCity();" onkeypress="" onkeydown="disabledSODestinBtn();" onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this,'special');" tabindex="79"/></td>
															<td align="left"><div id="zipDestCodeList" style="vertical-align:middle;"><a><img id="navigation9" class="openpopup" onclick="findCityStateNotPrimary('DZ', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Zip Code List" title="Zip Code List" /></a></div></td>
															<td align="left" class="listwhitetext" style="width:10px"></td>
															<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" cssStyle="width:54px" maxlength="10" onchange="findCityStateOfZipCode(this,'DZ'),disabledSODestinBtn();" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="findCityStateOfZipCode(this,'DZ');valid(this,'special');" tabindex="80"/></td>
															<td align="left" style="width:10px"></td>
															<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
															<td>
                                                            <a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="">&nbsp;&nbsp;<font style="font-size:11px;font-weight:bold;text-decoration:underline;cursor: pointer;"><spam >Look-Up Canadian Postal Code</spam></font></a>
                                                            </td>
                                                            </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:120px"><fmt:message key='customerFile.destinationDayPhoneExt'/></td>
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="left" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
															</tr>
															<tr>	
																<td align="left" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" cssStyle="width:155px;" maxlength="20"  tabindex="81"  onkeydown="disabledSODestinBtn();"/></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" cssStyle="width:62px;" maxlength="10" tabindex="82"  onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" style="width:20px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" cssStyle="width:100px;" maxlength="20"  tabindex="83"  onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" cssStyle="width:100px;" maxlength="25"  tabindex="84" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" style="width:25px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" cssStyle="width:120px;" maxlength="20"  tabindex="85" onkeydown="disabledSODestinBtn();"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td align="left" style="width:74px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td align="left" style="width:15px"></td>
																<td></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left"style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" style="width:100px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" cssStyle="width:231px;" maxlength="65" tabindex="86"  onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:25px"><a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value)">&nbsp;<img class="openpopup" onclick="" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail}"/></a></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" cssStyle="width:218px;" maxlength="65" tabindex="87" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:23px">&nbsp;<a href="javascript:sendEmail(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value)"><img class="openpopup" onclick="" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail2}"/></a></td>
																<td align="left" style="width:1px"></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destPreferredContactTime" cssStyle="width:120px;" maxlength="30" tabindex="88" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="position:absolute;right:45px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countDestinationNotes == '0' || countDestinationNotes == '' || countDestinationNotes == null}">
																		<td style="position:absolute;right:45px;" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="position:absolute;right:45px;" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,600);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
															<tr><td style="width:100px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
