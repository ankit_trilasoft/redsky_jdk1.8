package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CrewCapacityDao;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.service.CrewCapacityManager;


public class CrewCapacityManagerImpl extends GenericManagerImpl<CrewCapacity, Long> implements CrewCapacityManager{
	CrewCapacityDao crewCapacityDao;
	public CrewCapacityManagerImpl(CrewCapacityDao crewCapacityDao){
		super(crewCapacityDao);
		this.crewCapacityDao=crewCapacityDao;
	}
	
	public List<CrewCapacity> findRecordByHub(String crewGroupval, String sessionCorpID){
		return crewCapacityDao.findRecordByHub(crewGroupval, sessionCorpID);
	}
	public List findAllCrewGroup(String corpId){
		return crewCapacityDao.findAllCrewGroup(corpId);
	}
	public List findOldGroupName(String sessionCorpID, String gName){
		return crewCapacityDao.findOldGroupName(sessionCorpID,gName);
	}
	public Map<String,String> findExistingJobName(String sessionCorpID){
		return crewCapacityDao.findExistingJobName(sessionCorpID);
	}
	public List findParentHubByWarehouse(String warehouse,String sessionCorpID){
		return crewCapacityDao.findParentHubByWarehouse(warehouse,sessionCorpID);
	}
	public CrewCapacity findGroupNameFromCrewCapacity(String jobType, String sessionCorpID){
		return crewCapacityDao.findGroupNameFromCrewCapacity(jobType,sessionCorpID);
	}

}
