<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="bookStorageList.title" /></title>
<meta name="heading" content="<fmt:message key='bookStorageList.heading'/>" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>

<!--  
<c:set var="buttons">
	<input type="button" onclick="Validate(this.form);"
		value='<fmt:message key="button.access"/>' />

	<input type="button" onclick="Validate12(this.form);"
		value='<fmt:message key="button.release"/>' />

	<input type="button" onclick="Validate13(this.form);"
		value='<fmt:message key="button.move"/>' />





</c:set>
-->
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
 <configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<s:form id="storageListForm" action="searchBookStorages" method="post">
<div id="Layer1" style="width:100%">
<s:hidden name="serviceOrder.id" />
<s:hidden name="noteID" value="%{workTicket.ticket}"/>
<c:set var="noteID" value="%{workTicket.ticket}" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
			<div id="newmnav" style="!margin-bottom:7px;">
		    <ul>
		    <sec-auth:authComponent componentId="module.tab.workTicket.serviceorderTab">
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.billingTab"> 
			  	<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.accountingTab">
			  <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>  
		      </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		      <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
			      <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			   </c:when>
			    <c:otherwise> 
		          <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		        </c:otherwise>
		      </c:choose>  
		      </sec-auth:authComponent>
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
             <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>	      
		      <sec-auth:authComponent componentId="module.tab.workTicket.forwardingTab">
			  <c:if test="${forwardingTabVal!='Y'}"> 
	   				<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
	  			<c:if test="${forwardingTabVal=='Y'}">
	  				<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  			</c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.domesticTab">
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.workTicket.statusTab">
			 <c:if test="${serviceOrder.job =='RLO'}"> 
	 			<li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}"> 
					<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			</c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.ticketTab">		
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			  </sec-auth:authComponent>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </sec-auth:authComponent>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.workTicket.customerFileTab">
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			  </sec-auth:authComponent>
			</ul>
		</div>
		<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 3px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
		<div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=${workTicket.id}"><span>Work Ticket</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Location List</span></a></li>
						<li><a href="searchLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=${workTicket.id}"><span>Add Item To Location</span></a></li>
					  	<li><a href="storages.html?id=${workTicket.id}"><span>Access/ Release Items</span></a></li>
					  	<li><a href="storagesMove.html?id=${workTicket.id}"><span>Move Items</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${workTicket.id}&jobNumber=${serviceOrder.shipNumber}&noteID=${workTicket.ticket}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					  	
					</ul>
		</div><div class="spn">&nbsp;</div>
	
	
	<table width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
			<div id="content" align="center" style="!margin-top:6px;">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							
							<td class="listwhitetext">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhitetext">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
				</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<s:set name="bookStorages" value="bookStorages" scope="request" />
	<div id="otabs">
				  <ul>
				    <li><a class="current"><span>List</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 				
	<display:table name="bookStorages" class="table" requestURI=" " id="bookStorageList" export="true" pagesize="10" style="width:100%">
		<display:column property="locationId" sortable="true" titleKey="bookStorage.locationId" url="/editBookStorage.html?id1=${workTicket.id}&ticket=${workTicket.ticket}" paramId="id" paramProperty="id"/>
		<display:column property="what" sortable="true" titleKey="bookStorage.what" />
 		<display:column property="description" sortable="true" titleKey="bookStorage.description" />
 		<display:column property="releaseDate" sortable="true" titleKey="storage.releaseDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
        <display:column property="pieces" headerClass="containeralign" style="text-align: right;" sortable="true" titleKey="bookStorage.pieces" />
		<display:column property="containerId" sortable="true" titleKey="bookStorage.containerId" />
		<display:column property="itemTag" sortable="true" titleKey="bookStorage.itemTag" />
		<display:column property="oldLocation" sortable="true" titleKey="bookStorage.oldLocation" />		
		<display:column sortable="true" title="Weight">
				<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0"
                  groupingUsed="true" value="${bookStorageList.measQuantity}" /></div>
         </display:column>
		<display:column property="unit" sortable="true" title="Unit" />
		<display:column sortable="true" title="Volume">
				<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0"
                  groupingUsed="true" value="${bookStorageList.volume}" /></div>
         </display:column>
		<display:column property="volUnit" sortable="true" title="Unit" />

		<display:setProperty name="paging.banner.item_name" value="bookStorage" />
		<display:setProperty name="paging.banner.items_name" value="bookStorage" />
		<display:setProperty name="export.excel.filename" value="BookStorage List.xls" />
		<display:setProperty name="export.csv.filename" value="BookStorage List.csv" />
		<display:setProperty name="export.pdf.filename" value="BookStorage List.pdf" />
	</display:table>
	</td>
		</tr>
	</tbody>
</table>
</div>   
	
	<c:set var="buttons">
	

	
	
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />

</s:form>

<script type="text/javascript"> 
    highlightTableRows("bookStorageList");  
</script>
<script language="JavaScript" type="text/javascript">

function Validate(form){
try{
	if(confirm("Are You Ready to assign These Item")){
	  	form.action="bookStorages.html";
		form.submit();
	}else{
	  	form.action="bookStorages.html";
	  	form.submit();
	}  
}
catch(e){}
}
function Validate12(form) {
try{
	if(confirm("Are You Ready to mark  These Item to release")) {
  		form.action="bookStorages.html";
 		form.submit();
	}else{
	  form.action="bookStorages.html";
	  form.submit();
	}   
}
catch(e){}
}
function Validate13(form){
try{
	if(confirm("Are You Ready to move  These Item to new location")){
  		form.action="locationacs.html";
		form.submit();
	}else{
	  form.action="locationacs.html";
	  form.submit();
	}   
}
catch(e){}
}
try{
<script type="text/javascript">   
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/bookStorages.html?id=${workTicket.id}" ></c:redirect>
		</c:if>
}
catch(e){}		
		
</script>

