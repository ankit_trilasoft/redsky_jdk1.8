alter table trackingstatus add(documentationCutOff datetime default null, passportReceived  datetime default null,einReceived  datetime default null);

insert  into networkdatafields(modelName,fieldName,transactionType,createdBy,createdOn,updatedBy,updatedOn,useSysDefault,toModelName,toFieldName,type) values('AccountLine','contract','Create','amishra',now(),'amishra',now(),false,'','',''),('AccountLine','contract','Update','amishra',now(),'amishra',now(),false,'','','');

ALTER TABLE `redsky`.`billing` ADD COLUMN `vendorCode1` VARCHAR(8) AFTER `estMoveCost`,ADD COLUMN `vendorName1` VARCHAR(255) AFTER `vendorCode1`,ADD COLUMN `insuranceCharge1` VARCHAR(25) AFTER `vendorName1`;

ALTER TABLE `redsky`.`carton` ADD COLUMN `description` VARCHAR(20) AFTER `totalTareWeight`;

ALTER TABLE `redsky`.`partneraccountref` MODIFY COLUMN `accountCrossReference` VARCHAR(20)   DEFAULT NULL;

ALTER TABLE `redsky`.`accountline` MODIFY COLUMN `actgCode` VARCHAR(20) ;

ALTER TABLE `redsky`.`billing` MODIFY COLUMN `actgCode` VARCHAR(20)  ;

ALTER TABLE `redsky`.`servicepartner` MODIFY COLUMN `actgCode` VARCHAR(20)  ;

ALTER TABLE `redsky`.`trackingstatus` MODIFY COLUMN `actgCode` VARCHAR(20)   DEFAULT NULL, MODIFY COLUMN `actgCodeForDis` VARCHAR(20)  ;

CREATE TABLE  `redsky`.`modulepermissions` (`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,`role` varchar(225) DEFAULT NULL,`pageaction_id` bigint(20) DEFAULT NULL,`permission` int(11) DEFAULT NULL,`createdBy` varchar(82) DEFAULT NULL,`createdOn` datetime DEFAULT NULL,`updatedBy` varchar(82) DEFAULT NULL,`updatedOn` datetime DEFAULT NULL,`corpId` varchar(15) DEFAULT NULL,PRIMARY KEY (`id`));
 
CREATE TABLE  `redsky`.`pageaction` (`id` bigint(20) NOT NULL AUTO_INCREMENT,`actionuri` varchar(50) DEFAULT NULL,`actionclass` varchar(250) NOT NULL,`method` varchar(50) DEFAULT NULL,`description` varchar(50) DEFAULT NULL,`createdBy` varchar(50) DEFAULT NULL,`createdOn` datetime DEFAULT NULL,`updatedBy` varchar(50) DEFAULT NULL,`updatedOn` datetime DEFAULT NULL,`module` varchar(50) DEFAULT NULL,PRIMARY KEY (`id`));
 
CREATE TABLE  `redsky`.`pageactionpermission` (`pageaction_id` bigint(20) NOT NULL,`menuitem_id` bigint(20) NOT NULL,`permission` int(11) NOT NULL, PRIMARY KEY (`pageaction_id`,`menuitem_id`));

ALTER TABLE `redsky`.`refzipgeocodemap` MODIFY COLUMN `country` VARCHAR(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

 

 


