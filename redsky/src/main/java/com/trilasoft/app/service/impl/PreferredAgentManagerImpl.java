package com.trilasoft.app.service.impl;

import java.util.List;

import com.trilasoft.app.dao.PreferredAgentDao;
import com.trilasoft.app.model.PreferredAgent;
import com.trilasoft.app.service.PreferredAgentManager;

import org.appfuse.service.impl.GenericManagerImpl;

public class PreferredAgentManagerImpl extends GenericManagerImpl<PreferredAgent, Long> implements PreferredAgentManager {
	PreferredAgentDao preferredAgentDao;

	public PreferredAgentManagerImpl(PreferredAgentDao preferredAgentDao) {
		super(preferredAgentDao);
		this.preferredAgentDao = preferredAgentDao;
	}
	
	public List getPreferredAgentListByPartnerCode(String sessionCorpID, String partnerCode){
		return preferredAgentDao.getPreferredAgentListByPartnerCode(sessionCorpID,partnerCode);
	}
	public List findPreferredAgentsList(String partnerCode,String agentCode,String agentName,String status,String corpID,String agentGroup){
		return preferredAgentDao.findPreferredAgentsList( partnerCode, agentCode, agentName, status, corpID,agentGroup);
	}
	public List findSearchAgentDetailsAutocompleteList(String searchName,String corpID,String fieldType,String partnerCode){
		return preferredAgentDao.findSearchAgentDetailsAutocompleteList( searchName, corpID,fieldType,partnerCode);
	}
	public List findPartnerCode(String partnerCode, String sessionCorpID){
		return preferredAgentDao.findPartnerCode( partnerCode, sessionCorpID);
	}
	public List checkForPartnerCodeExists(String partnerCode, String sessionCorpID,String agentCode){
		return preferredAgentDao.checkForPartnerCodeExists( partnerCode, sessionCorpID, agentCode);
	}
	public List findAgentGroupList(String sessionCorpID){
		return preferredAgentDao.findAgentGroupList(sessionCorpID);
	}
	public List getAgentGroupDataList(String sessionCorpID,String agentGroup){
		return preferredAgentDao.getAgentGroupDataList(sessionCorpID,agentGroup);
	}
	public void updateAgentStatus(Long preIdNum, Boolean status){
		preferredAgentDao.updateAgentStatus(preIdNum, status);
	}
	
}