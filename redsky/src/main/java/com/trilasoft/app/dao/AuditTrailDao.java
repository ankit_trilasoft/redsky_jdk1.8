package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AuditTrail;

public interface AuditTrailDao extends GenericDao<AuditTrail, Long> {

	public List getAuditTrailList(Long xtranId, String tableName, String corpID,String from,String secondID);
	public List getAuditTrailListNew(Long xtranId, String tableName, String corpID);
	public List getAlertHistoryList(String corpID,int daystoManageAlerts);
	public List findByAlertListFilter(String sessionCorpID,String alertUser,int daystoManageAlerts);
	public List getAlertTaskList(Long xtranId,String tableName,String sessionCorpID,String relatedTask,String sequenceNumber);
	public List findContainerCartonVehicleRoutingConsigneeList(Long sid,String tableName,String sessionCorpID);
	public List getAuditAccountContactList(String corpID,String partnerCode,String tableName);
	public List findByAlertListFilterForExtCoordinator(String sessionCorpID,String alertUser,int daystoManageAlerts,String bookingAgentPopup,String billToCodePopup);
}
