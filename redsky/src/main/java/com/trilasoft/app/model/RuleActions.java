package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="todoruleactions")
/*@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")*/
@FilterDef(name = "todoRuleCorpID", parameters = { @ParamDef(name = "todoRuleCorpIDValue", type = "string") })
@Filter(name = "todoRuleCorpID", condition = "corpID in (:todoRuleCorpIDValue) ")
public class RuleActions  extends BaseObject{
	
	private Long id;
	private String corpID;	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Long ruleId;
	private String messagedisplayed;
	private Boolean status = new Boolean(false);
	private String rolelist;
	private String emailNotification;
	private String noteType;
	private String noteSubType;
	private String manualEmail;
	private String displayToUser;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("ruleId", ruleId)
				.append("messagedisplayed", messagedisplayed)
				.append("status", status)
				.append("rolelist", rolelist)
				.append("emailNotification", emailNotification)
				.append("noteType", noteType)
				.append("noteSubType", noteSubType)
				.append("manualEmail", manualEmail)
				.append("displayToUser", displayToUser)
				.toString();
	}



	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RuleActions))
			return false;
		RuleActions castOther = (RuleActions) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(ruleId, castOther.ruleId)
				.append(messagedisplayed, castOther.messagedisplayed)
				.append(status, castOther.status)
				.append(rolelist, castOther.rolelist)
				.append(emailNotification, castOther.emailNotification)
				.append(noteType, castOther.noteType)
				.append(noteSubType, castOther.noteSubType)
				.append(manualEmail, castOther.manualEmail)
				.append(displayToUser, castOther.displayToUser)
				.isEquals();
	}



	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(ruleId)
				.append(messagedisplayed).append(status)
				.append(rolelist).append(emailNotification)
				.append(noteType)
				.append(noteSubType)
				.append(manualEmail)
				.append(displayToUser)
				.toHashCode();
	}



	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	public Date getUpdatedOn() {
		return updatedOn;
	}
	
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	
	public String getMessagedisplayed() {
		return messagedisplayed;
	}

	public void setMessagedisplayed(String messagedisplayed) {
		this.messagedisplayed = messagedisplayed;
	}
	
	public String getRolelist() {
		return rolelist;
	}

	public void setRolelist(String rolelist) {
		this.rolelist = rolelist;
	}
	
	public String getNoteSubType() {
		return noteSubType;
	}
	public void setNoteSubType(String noteSubType) {
		this.noteSubType = noteSubType;
	}
	
	public String getNoteType() {
		return noteType;
	}
	public void setNoteType(String noteType) {
		this.noteType = noteType;
	}
	

	public String getEmailNotification() {
		return emailNotification;
	}
	public void setEmailNotification(String emailNotification) {
		this.emailNotification = emailNotification;
	}
	
	public String getManualEmail() {
		return manualEmail;
	}


	public void setManualEmail(String manualEmail) {
		this.manualEmail = manualEmail;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @return the displayToUser
	 */
	@Column
	public String getDisplayToUser() {
		return displayToUser;
	}

	/**
	 * @param displayToUser the displayToUser to set
	 */
	public void setDisplayToUser(String displayToUser) {
		this.displayToUser = displayToUser;
	}


}