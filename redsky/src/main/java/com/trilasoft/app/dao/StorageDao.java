package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   
import com.trilasoft.app.model.Storage;    
import java.util.List;   
  
public interface StorageDao extends GenericDao<Storage, Long> {   
	   public List<Storage> findByMeasQuantity(Double measQuantity); 
	   public List<Storage> findByLocation(String locationId); 
	   public List<Storage> findByItemNumber(String itemNumber);
	   //public List<Storage> findByTicket(Long ticket);
	   public List<Storage> findByJobNumber(String jobNumber);
	   public List findMaximum();
	   public List<Storage> findByItemNumber(String itemNumber,String jobNumber, String locationId, Long ticket);
	   public List getByIdNum(Long idNum);
	   public List getTraceStorageList();
	   
	   public List<Storage> search(String itemNumber, String jobNumber, String locationId, String storageId, Long ticket, String firstName, String lastName, String tag, String corpId);
	   public List findStorageByShipNumber(String shipNumber);
	   public List storageExtract(String corpId,String jobSTLFlag,String salesPortalStorageReport);
	   public List findLocationByStorage(String storageId, String sessionCorpID);
	   public List<Storage> findByLocationRearrange(String storageId,String locationId);
	   public List storageLibraryVolumeList(String storageId);
	   public List dummyStorageList(String storageId,String corpId);
	   public void deleteDummyUsedRecord(String storageId, String sessionCorpID);
	   public List handOutList(String ticket, String sessionCorpID);
	   public void updateStorageForLocation(String sessionCorpID,String storageId,String locationId);
	   public List getRecordsForBookSto(String sessionCorpID, String storageId,Long ticket);
	   public List getLocationId(String storageId);
	   public List<Storage> findByLocationInternalRearrange(String storageId,String locationId, Long ticket, String mode);
	   public List<Storage> findStoragesByLocation(String locationId);
}

