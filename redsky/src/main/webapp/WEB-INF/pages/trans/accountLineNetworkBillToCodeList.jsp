<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if> 

<style>
  span.pagelinks {display:block;font-size:0.85em;margin-bottom:5px;!margin-bottom:-1px;margin-top:-16px;!margin-top:-36px;
  text-align:right;width:100%;
}
</style>
<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['partnerListForm'].elements['partner.lastName'].value = "";
			document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
			document.forms['partnerListForm'].elements['partner.extReference'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
			document.forms['partnerListForm'].elements['partner.billingState'].value = "";
			document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
			document.forms['partnerListForm'].elements['partner.aliasName'].value = "";
			 <c:if test="${partnerType == 'AG'}">  
				document.forms['partnerListForm'].elements['vanlineCodeSearch'].value = "";
				</c:if>
}
function partnerDetail(id)
{
	 
	try{
		 // var orgAdd=encodeURIComponent("${orgAddress}");
		  var orgAdd='${orgAddress}';
		    orgAdd = orgAdd.replace( /"/g, '%22' );
		   // orgAdd = orgAdd.replace( /~/g, '%7E' );
		    //orgAdd = orgAdd.replace( /&/g, '%26' );
		   // orgAdd = orgAdd.replace( /'/g, '%27' );
		    orgAdd=encodeURIComponent(orgAdd);
		  //var destAdd=encodeURIComponent("${destAddress}");
		   var destAdd='${destAddress}';
		      destAdd = destAdd.replace( /"/g, '%22' );
		    //  destAdd = destAdd.replace( /~/g, '%7E' );
		     // destAdd = destAdd.replace( /&/g, '%26' );
		      //destAdd = destAdd.replace( /'/g, '%27' );
		      destAdd=encodeURIComponent(destAdd);
		  var firstName= encodeURI("${firstName}");
		  var lastName= encodeURI("${lastName}");
		  location.href="viewNetworkPartner.html?id="+id+"&networkPartnerCode=${networkPartnerCode}&partnerType=AC&firstName="+firstName+"&lastName="+lastName+"&phone=${phone}&email=${email}&mode=view&flag=${flag}&compDiv=${compDiv}&type=TT&orgAddress="+orgAdd+"&destAddress="+destAdd+"&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}";
		  } 
		  catch(e){}
}
</script>
</head> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>
<s:form id="partnerListForm" action="findNetworkBillToCodeSearchList.html?decorator=popup&popup=true" method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="networkPartnerCode" value="<%=request.getParameter("networkPartnerCode")%>"/>

<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" /> 
<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if> 
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">

<table class="table" style="width:99%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th>External Ref.</th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<c:if test="${partnerType == 'AG'}">  
<th>Vanline Code</th>
</c:if>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partner.partnerCode" size="5" cssClass="input-text" onchange="valid(this,'special');" />
			</td>
			<td>
			    <s:textfield name="partner.extReference" size="10" cssClass="input-text" onchange="valid(this,'special');" />
			</td>
			<td>
			    <s:textfield name="partner.lastName" size="15" cssClass="input-text" onchange="valid(this,'special');"/>
			</td>
			<td>
			    <s:textfield name="partner.aliasName" size="15" cssClass="input-text" />
			</td>
			<c:if test="${partnerType == 'AG'}">
			<td>
			    <s:textfield name="vanlineCodeSearch" size="12" cssClass="input-text" />
			</td>			
			</c:if>
			<td>
			    <s:textfield name="partner.billingCountryCode" size="7" cssClass="input-text" onchange="valid(this,'special');"/>
			</td>
			<td>
			    <s:textfield name="partner.billingCountry" size="18" cssClass="input-text" onchange="valid(this,'special');"/>
			</td>
			<td>
			    <s:textfield name="partner.billingState" size="12" cssClass="input-text" onchange="valid(this,'special');"/>
			</td>
		</tr>
		<tr>
			<td colspan="5"></td>
			<td width="130px" colspan="3" style="border-left: hidden;text-align:right;padding-right:5px;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</s:form>
<div id="newmnav"> 
    <ul>
     <li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAccount();"><span>Billing Parties</span></a></li>
    </ul> 
</div><div class="spn" style="">&nbsp;</div>
<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="1" pagesize="10" 
		style="margin-top:3px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode"
		href="editPartnerAddForm.html?partnerType=${partnerType}" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name" sortable="true" style="width:345px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
    <display:column property="aliasName" title="Alias Name" sortable="true"></display:column>
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.jpg"  onclick="openOriginLocation('${partnerList.billingAddress1}','${partnerList.billingAddress2}','${partnerList.billingCity}','${partnerList.billingZip}','${partnerList.billingState}','${partnerList.billingCountry}');"/></a></display:column>  
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:140px"/>
    
  <%--   
    <c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'CR' || partnerType == 'OO'}">  
    <display:column property="accountCrossReference" sortable="true" titleKey="partner.accountCrossReference" style="width:140px"/>
    </c:if>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>--%>
 <%-- 	<c:if test="${param.popup}">
    	<display:column style="width:105px;cursor:pointer;"><A onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?id=${partnerList.id}&partnerType=${partnerType}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:if>--%> 
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/> 
<script language="javascript" type="text/javascript">
	function openOriginLocation(address1,address2,city,zip,state,country) {
		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
	} 
</script>