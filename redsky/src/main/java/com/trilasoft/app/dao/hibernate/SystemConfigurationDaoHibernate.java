package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;




import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Hibernate;
import org.hibernate.Query;

import com.trilasoft.app.dao.SystemConfigurationDao;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.SystemConfiguration;



public class SystemConfigurationDaoHibernate extends GenericDaoHibernate<SystemConfiguration, Long> implements SystemConfigurationDao {

	
	public SystemConfigurationDaoHibernate() {    
        super(SystemConfiguration.class);   
    }   
    public List checkById(Long id) {
    	return getHibernateTemplate().find("select id from SystemConfiguration where id=?",id);
    }
    public List<SystemConfiguration>getList()
    {
    	return getHibernateTemplate().find("find SystemConfiguration");
    }
    public List<DataCatalog>searchByTableAndField(String tableName,String fieldName,String corpID,String description,String auditable,String defineByToDoRule,String isdateField,String visible,String charge) 
    {
    	List list1 = new ArrayList();
    	/*String MasterList="";
    	//System.out.println("select ifnull(c.id,m.id),ifnull(c.auditable,m.auditable),ifnull(c.corpID,m.corpID), ifnull(c.tableName,m.tableName),ifnull(c.fieldName,m.fieldName), ifnull(c.defineByToDoRule,m.defineByToDoRule),ifnull(c.isdateField,m.isdateField),ifnull(c.description,m.description),ifnull(c.rulesFiledName,m.rulesFiledName),ifnull(c.visible,m.visible) ,ifnull(c.charge,m.charge) FROM datacatalog m LEFT OUTER JOIN datacatalog c ON (m.corpID = 'TSFT' and c.corpID = '"+corpID+"' and c.defineByToDoRule like '"+defineByToDoRule.replaceAll("'", "''")+"%' and c.isdateField like '"+isdateField.replaceAll("'", "''")+"%' and c.auditable like '"+auditable.replaceAll("'", "''")+"%' and c.visible like '"+visible.replaceAll("'", "''")+"%' and c.charge like '"+charge.replaceAll("'", "''")+"%' and m.tableName = c.tableName and m.fieldName = c.fieldName) where m.tableName like '"+tableName.replaceAll("'", "''")+"%' AND m.fieldName like '"+fieldName.replaceAll("'", "''")+"%' AND m.auditable like '"+auditable.replaceAll("'", "''")+"%' AND m.description like '"+description.replaceAll("'", "''")+"%' AND m.defineByToDoRule like '"+defineByToDoRule.replaceAll("'", "''")+"%' AND m.isdateField like '"+isdateField.replaceAll("'", "''")+"%' AND m.visible like '"+visible.replaceAll("'", "''")+"%' AND m.charge like '"+charge.replaceAll("'", "''")+"%'");
		String tsftCheck="select count(*) from datacatalog where corpID = '"+corpID+"'  and tableName like '%"+tableName.replaceAll("'", "''")+"%' AND fieldName like '%"+fieldName.replaceAll("'", "''")+"%' " ;
		List listMasterList1= this.getSession().createSQLQuery(tsftCheck).list();
		 if(listMasterList1!=null && !(listMasterList1.isEmpty()) && listMasterList1.get(0)!=null){
			 Integer count=Integer.parseInt(listMasterList1.get(0).toString());
			 if(count>0){
				 //System.out.println("in corpid itself..................");
				 MasterList="select id as id ,auditable,corpID,tableName,fieldName,defineByToDoRule,isdateField,description,rulesFiledName,visible ,charge " +
					" FROM datacatalog where corpID in ('"+corpID+"')" ;
			}else{
				 MasterList="select id as id ,auditable,corpID,tableName,fieldName,defineByToDoRule,isdateField,description,rulesFiledName,visible ,charge " +
					" FROM datacatalog where corpID = 'TSFT'" ;
			 }
		 }*/
    	 
    	      String MasterList="select id as id ,auditable,corpID,tableName,fieldName,defineByToDoRule,isdateField,description,rulesFiledName,visible ,charge " +
				" FROM datacatalog where corpID = 'TSFT'" ;
						if(!defineByToDoRule.equals("")){
						MasterList=MasterList+" and defineByToDoRule='"+defineByToDoRule+"' " ;	
						}
						if(!isdateField.equals("")){
						MasterList=MasterList+" and isdateField='"+isdateField+"'";
						}
						if(!auditable.equals("")){
						MasterList=MasterList+" and auditable='"+auditable+"'";
						}
						if(!visible.equals("")){
						MasterList=MasterList+" and visible ='"+visible+"'";
						}
						MasterList=MasterList+" and tableName like '%"+tableName.replaceAll("'", "''")+"%' AND fieldName like '%"+fieldName.replaceAll("'", "''")+"%' " ;
						if(!auditable.equals("")){
							MasterList=MasterList+" and auditable='"+auditable+"'";
							}
						MasterList=MasterList+" AND description like '%"+description.replaceAll("'", "''")+"%'";
						if(!defineByToDoRule.equals("")){
							MasterList=MasterList+" and defineByToDoRule='"+defineByToDoRule+"' " ;	
							}
						if(!isdateField.equals("")){
							MasterList=MasterList+" and isdateField='"+isdateField+"'";
							}
						if(!visible.equals("")){
							MasterList=MasterList+" and visible ='"+visible+"'";
							}
					    
						MasterList=MasterList+" AND charge like '"+charge.replaceAll("'", "''")+"%'";
						
						MasterList=MasterList+" Union  select id as id ,auditable,'',tableName,fieldName,defineByToDoRule,isdateField,description,rulesFiledName,visible ,charge " +
				        " FROM datacatalog where corpID = '"+corpID+"'" ;
						if(!defineByToDoRule.equals("")){
						MasterList=MasterList+" and defineByToDoRule='"+defineByToDoRule+"' " ;	
						}
						if(!isdateField.equals("")){
						MasterList=MasterList+" and isdateField='"+isdateField+"'";
						}
						if(!auditable.equals("")){
						MasterList=MasterList+" and auditable='"+auditable+"'";
						}
						if(!visible.equals("")){
						MasterList=MasterList+" and visible ='"+visible+"'";
						}
						MasterList=MasterList+" and tableName like '%"+tableName.replaceAll("'", "''")+"%' AND fieldName like '%"+fieldName.replaceAll("'", "''")+"%' " ;
						if(!auditable.equals("")){
							MasterList=MasterList+" and auditable='"+auditable+"'";
							}
						MasterList=MasterList+" AND description like '%"+description.replaceAll("'", "''")+"%'";
						if(!defineByToDoRule.equals("")){
							MasterList=MasterList+" and defineByToDoRule='"+defineByToDoRule+"' " ;	
							}
						if(!isdateField.equals("")){
							MasterList=MasterList+" and isdateField='"+isdateField+"'";
							}
						if(!visible.equals("")){
							MasterList=MasterList+" and visible ='"+visible+"'";
							}
					    
						MasterList=MasterList+" AND charge like '"+charge.replaceAll("'", "''")+"%' order by tableName,fieldName,corpID ";		
		List listMasterList= this.getSession().createSQLQuery(MasterList)
		.addScalar("id",Hibernate.LONG)
		.addScalar("auditable")
		.addScalar("corpID")
		.addScalar("tableName")
		.addScalar("fieldName")
		.addScalar("defineByToDoRule")
		.addScalar("isdateField")
		.addScalar("description",Hibernate.TEXT)
		.addScalar("rulesFiledName")
		.addScalar("visible")
		.addScalar("charge")
		.list();
		String samePartnerCode="";
		Iterator it=listMasterList.iterator();
		while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           DataCatalog dataCatalog=new DataCatalog();
	           dataCatalog.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){dataCatalog.setAuditable("");}else{dataCatalog.setAuditable(row[1].toString());}
	           if(row[2] == null || row[2].toString().trim().equals("")){dataCatalog.setCorpID(corpID);}else{dataCatalog.setCorpID(row[2].toString());}
	           if(row[3] == null){dataCatalog.setTableName("");}else{dataCatalog.setTableName(row[3].toString());}
	           if(row[4] == null){dataCatalog.setFieldName("");}else{dataCatalog.setFieldName(row[4].toString());}
	           String newPartnerCode=dataCatalog.getTableName()+"~"+dataCatalog.getFieldName();
	           if(row[5] == null){dataCatalog.setDefineByToDoRule("");}else{dataCatalog.setDefineByToDoRule(row[5].toString());}
	           if(row[6] == null){dataCatalog.setIsdateField("");}else{dataCatalog.setIsdateField(row[6].toString());}
	           if(row[7] == null){dataCatalog.setDescription("");}else{dataCatalog.setDescription(row[7].toString());}
	           if(row[8] == null){dataCatalog.setRulesFiledName("");}else{dataCatalog.setRulesFiledName(row[8].toString());}
	           if(row[9] == null){dataCatalog.setVisible("");}else{dataCatalog.setVisible(row[9].toString());}
	           if(row[10] == null){dataCatalog.setCharge("");}else{dataCatalog.setCharge(row[10].toString());}
	           if(samePartnerCode!=null && newPartnerCode !=null && (!(samePartnerCode.trim().equalsIgnoreCase(newPartnerCode.trim())))){
	           list1.add(dataCatalog);
	           }
	           samePartnerCode=newPartnerCode;
	       }
		if(list1.isEmpty()){
			//System.out.println("\n\n list is empty-->>");
			return null;
		}else{
			//System.out.println("\n\n list is full-->>");
			return list1;
		}
		
    	//return getHibernateTemplate().find("from SystemConfiguration where tableName like '"+tableName+"%' AND fieldName like '"+fieldName+"%'");
    }
    public List configurableTable(String b)
	{
		return getHibernateTemplate().find("SELECT distinct(tableName)  FROM DataCatalog  where configerable='true'");
	}
	public List configurableField(String tableName,String b)
	{
		return getHibernateTemplate().find("SELECT distinct(fieldName) from DataCatalog where tableName="+"'"+tableName+"'"+"AND configerable='true'");
	}

	public List<DataCatalog> getMasterList(String corpID) {
		List list1 = new ArrayList();
		String MasterList="select ifnull(c.id,m.id),ifnull(c.auditable,m.auditable),ifnull(c.corpID,m.corpID), ifnull(c.tableName,m.tableName),ifnull(c.fieldName,m.fieldName), ifnull(c.defineByToDoRule,m.defineByToDoRule),ifnull(c.isdateField,m.isdateField),ifnull(c.description,m.description),ifnull(c.rulesFiledName,m.rulesFiledName),ifnull(c.visible,m.visible) ,ifnull(c.charge,m.charge) FROM datacatalog m LEFT OUTER JOIN datacatalog c ON (m.corpID = 'TSFT' and c.corpID = '"+corpID+"' and m.tableName = c.tableName and m.fieldName = c.fieldName) where m.corpID = 'TSFT'";
		List listMasterList= this.getSession().createSQLQuery(MasterList)
		.addScalar("ifnull(c.id,m.id)")
		.addScalar("ifnull(c.auditable,m.auditable)")
		.addScalar("ifnull(c.corpID,m.corpID)")
		.addScalar("ifnull(c.tableName,m.tableName)")
		.addScalar("ifnull(c.fieldName,m.fieldName)")
		.addScalar("ifnull(c.defineByToDoRule,m.defineByToDoRule)")
		.addScalar("ifnull(c.isdateField,m.isdateField)")
		.addScalar("ifnull(c.description,m.description)",Hibernate.TEXT)
		.addScalar("ifnull(c.rulesFiledName,m.rulesFiledName)")
		.addScalar("ifnull(c.visible,m.visible)")
		.addScalar("ifnull(c.charge,m.charge)")
		.list();
		
		Iterator it=listMasterList.iterator();
		while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           DataCatalog dataCatalog=new DataCatalog();
	           dataCatalog.setId(Long.parseLong(row[0].toString()));
	           if(row[1] == null){dataCatalog.setAuditable("");}else{dataCatalog.setAuditable(row[1].toString());}
	           if(row[2] == null){dataCatalog.setCorpID("");}else{dataCatalog.setCorpID(row[2].toString());}
	           if(row[3] == null){dataCatalog.setTableName("");}else{dataCatalog.setTableName(row[3].toString());}
	           if(row[4] == null){dataCatalog.setFieldName("");}else{dataCatalog.setFieldName(row[4].toString());}
	           if(row[5] == null){dataCatalog.setDefineByToDoRule("");}else{dataCatalog.setDefineByToDoRule(row[5].toString());}
	           if(row[6] == null){dataCatalog.setIsdateField("");}else{dataCatalog.setIsdateField(row[6].toString());}
	           if(row[7] == null){dataCatalog.setDescription("");}else{dataCatalog.setDescription(row[7].toString());}
	           if(row[8] == null){dataCatalog.setRulesFiledName("");}else{dataCatalog.setRulesFiledName(row[8].toString());}
	           if(row[9] == null){dataCatalog.setVisible("");}else{dataCatalog.setVisible(row[9].toString());}
	           if(row[10] == null){dataCatalog.setCharge("");}else{dataCatalog.setCharge(row[10].toString());}
	           
	           list1.add(dataCatalog);
	       }	
		return list1;
		
	}
}