package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.ChargesDao;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.CountryDeviation;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.service.ChargesManager;

public class ChargesManagerImpl extends GenericManagerImpl<Charges, Long> implements ChargesManager {   
		ChargesDao chargesDao;   
	  
	    public ChargesManagerImpl(ChargesDao chargesDao) {   
	        super(chargesDao);   
	        this.chargesDao = chargesDao;   
	    }   
	    public List<Charges> findByEstimateActual(String estimateActual,String contract)
	    {
	    	return chargesDao.findByEstimateActual(estimateActual,contract);
	    }
	    
	    public List<Charges> findForCharges(String charge,String contract,String description,String corpID,String gl,String expGl,String com) {   
	        return chargesDao.findForCharges(charge,contract,description,corpID,gl,expGl,com);   
	    }

	    public List findMaxId() {
	    	
			return chargesDao.findMaxId();
		}    
	    
	    public List<Charges> findByChargeCode(String charge,String description){
	    	return chargesDao.findByChargeCode(charge,description);
	    }
	    public List<Charges> findForContract(String contract,String corpID){
	    	return chargesDao.findForContract(contract,corpID);
	    }
	    
		public List<Charges> findForContractChargeList(String contract,String corpID,String originCountry,String destCountry,String serviceMode,String category){
	    	return chargesDao.findForContractChargeList(contract, corpID, originCountry, destCountry,serviceMode,category);
	    } 
	    
	    public List<Charges> findForChargesContract(String charge,String description,String contract,String corpID,String originCountry,String destCountry,String serviceMode){
	    	return chargesDao.findForChargesContract(charge,description,contract,corpID,originCountry,destCountry,serviceMode);
	    }
		public Map<String,String> findQuantityField(String sessionCorpID,String charge) {
			return chargesDao.findQuantityField(sessionCorpID,charge);
		}
		public List<RateGrid> findRateGrid(Long chargeId) {
			return chargesDao.findRateGrid(chargeId);
		}
		public List getCharge(String charge,String billingContract,String corpId)
		{
			return chargesDao.getCharge( charge, billingContract,corpId);
		}
		public Map<String, String> findPriceField(String sessionCorpID,String charge) {
			return chargesDao.findPriceField(sessionCorpID,charge);
		}
		public List getQuantity(String fieldName, String shipNumber) {
			return chargesDao.getQuantity(fieldName, shipNumber);
		}
		public List<RateGrid> findRateFromRateGrid(String quantity, String charge, String multquantity, String distance, String twoDGridUnit) {
			return chargesDao.findRateFromRateGrid(quantity, charge, multquantity, distance, twoDGridUnit); 
		}
		public List findChargeId(String charge, String contract) {
			return chargesDao.findChargeId(charge, contract);	
		}
		public List getChargeRadio(String charge,String billingContract,String sessionCorpID){
			return chargesDao.getChargeRadio(charge, billingContract,sessionCorpID);
		}
		public List getChargeEntitle(String charge, String billingContract) {
			return chargesDao.getChargeEntitle(charge, billingContract);
		}
		public List getChargeEstimate(String charge,String billingContract,String sessionCorpID){
			return chargesDao.getChargeEstimate(charge, billingContract,sessionCorpID);
		}
		public List findChargeCode(String charge, String contract) {
			return chargesDao.findChargeCode(charge, contract);			
		}
		public List searchID(){
			return chargesDao.searchID();
		}
		public List findChargesByContract(String sessionCorpID) {
			// TODO Auto-generated method stub
			return chargesDao.findChargesByContract(sessionCorpID);
		}
		public List getBreakPoints(String chargeId, String sessionCorpID) {
			return chargesDao.getBreakPoints(chargeId, sessionCorpID);
		}
		public List getFirstQuantity(String chargeId, String sessionCorpID, String multquantity, String stringDistance, String qty) {
			return chargesDao.getFirstQuantity(chargeId, sessionCorpID, multquantity,  stringDistance,  qty);
		}
		public List findCount(String chargeCode, String contractName) {
			return chargesDao.findCount(chargeCode, contractName);
		}
		public List findDefaultTemplateContract(String contract, String sessionCorpID) {
			
			return  chargesDao.findDefaultTemplateContract(contract, sessionCorpID);
		}
		public List findDefaultTemplateChargeCode(String chargeCode, String contract) {
			
			return  chargesDao.findDefaultTemplateChargeCode(chargeCode, contract);
		}
		public List checkChargeForContractAvailability(String chargeCde, String contractCde, String sessionCorpID) {
			return chargesDao.checkChargeForContractAvailability(chargeCde, contractCde, sessionCorpID);
		}
		public List findForInternalCostChargeList(String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode) {
			return chargesDao.findForInternalCostChargeList(accountCompanyDivision, sessionCorpID, originCountry, destinationCountry, serviceMode);
		}
		public List findForChargesInternalCost(String chargeCode, String description, String accountCompanyDivision, String sessionCorpID, String originCountry, String destinationCountry, String serviceMode) {
			return chargesDao.findForChargesInternalCost(chargeCode, description, accountCompanyDivision, sessionCorpID, originCountry, destinationCountry, serviceMode);
		}
		public List findInternalCostChargeCodeChargeCode(String chargeCode, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.findInternalCostChargeCodeChargeCode(chargeCode, accountCompanyDivision, sessionCorpID);
		}
		public List findRadioValueInternalCost(String charge, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.findRadioValueInternalCost(charge, accountCompanyDivision, sessionCorpID);
		}
		public List getInternalCostsCharge(String charge, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.getInternalCostsCharge(charge, accountCompanyDivision, sessionCorpID);
		}
		public List findInternalCostsChargeId(String charge, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.findInternalCostsChargeId(charge, accountCompanyDivision, sessionCorpID);
		}
		public List getInternalCostsChargeEntitle(String charge, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.getInternalCostsChargeEntitle(charge, accountCompanyDivision, sessionCorpID);
		}
		public List getInternalCostChargeEstimate(String charge, String accountCompanyDivision, String sessionCorpID) {
			return chargesDao.getInternalCostChargeEstimate(charge, accountCompanyDivision, sessionCorpID);
		}
		public List findInsuranceLHFAmount(String insuranceLHFAmount, String chargeid) {
			return chargesDao.findInsuranceLHFAmount(insuranceLHFAmount, chargeid);
		} 
		public void deleteRateGridInvalidRecord(Long chargeId){
			chargesDao.deleteRateGridInvalidRecord(chargeId);
		}
		public List defaultChargesByComDiv(String comDiv, String corpId){
			return chargesDao.defaultChargesByComDiv(comDiv,corpId);
			
		}
		public List findChargeCodeCostElement(String chargeCode, String contract, String sessionCorpID, String jobType, String routing, String soCompanyDivision) {
			return chargesDao.findChargeCodeCostElement(chargeCode, contract, sessionCorpID, jobType, routing,soCompanyDivision);
		}
		public List findInternalCostChargeCodeCostElement(String chargeCode, String accountCompanyDivision, String sessionCorpID, String jobType, String routing , String soCompanyDivision) {
			return chargesDao.findInternalCostChargeCodeCostElement(chargeCode, accountCompanyDivision, sessionCorpID,jobType,routing,soCompanyDivision);
		}
		public String findDeviation(String chargeid, String originCountryCode, String purchase, String sessionCorpID) {
			return chargesDao.findDeviation(chargeid, originCountryCode, purchase, sessionCorpID);
		}
		public String findMaxDeviation(String chargeid, String originCountryCode, String destinationCountryCode, String purchase, String sessionCorpID) {
			return chargesDao.findMaxDeviation(chargeid, originCountryCode, destinationCountryCode, purchase, sessionCorpID);
		}
		public String findSumDeviation(String chargeid, String originCountryCode, String destinationCountryCode, String purchase, String sessionCorpID) {
			return chargesDao.findSumDeviation(chargeid, originCountryCode, destinationCountryCode, purchase, sessionCorpID);
		}
		public List findVatCalculationForCharge(String corpId){
			return chargesDao.findVatCalculationForCharge(corpId);
		}
		public List findChargesRespectToContract(String contract,String sessionCorpID){
			return chargesDao.findChargesRespectToContract(contract, sessionCorpID);
		}
		public List getAllGridForCharges(String chargeId , String corpId,	String contract){
			return chargesDao.getAllGridForCharges(chargeId, corpId, contract);
		}
		public List findBuyRateFromRateGrid(String quantity, String charge, String multquantity, String distance, String twoDGridUnit) {
			return chargesDao.findBuyRateFromRateGrid(quantity, charge, multquantity, distance, twoDGridUnit);
		}
		public int countContractCharges(String contract,String sessionCorpID){
			return chargesDao.countContractCharges(contract, sessionCorpID);
		}
		public List findChargeIdForOtherCorpid(String charge, String contract, String externalCorpID) {
			return chargesDao.findChargeIdForOtherCorpid(charge, contract, externalCorpID);
		}
		public List<CountryDeviation> findCountryDeviation(Long chargeId){
			return chargesDao.findCountryDeviation(chargeId);
		}
		public String findCharges(String charge,String contract,String sessionCorpID){
			return chargesDao.findCharges(charge,contract,sessionCorpID);
		}
		public List getSaleCommisionableList(String commissionContract,String commissionChargeCode,String commissionJobType,String commissionCompanyDivision,Boolean commissionIsActive,Boolean commissionIsCommissionable,String sessionCorpID){
			return chargesDao.getSaleCommisionableList(commissionContract, commissionChargeCode, commissionJobType, commissionCompanyDivision, commissionIsActive, commissionIsCommissionable, sessionCorpID);
		}
		public void updateSaleCommission(Long commisionId,Boolean commissionIsCommissionable,String commission,Boolean isThirdParty,Boolean isGrossMargin,String sessionCorpID, String userName){
			chargesDao.updateSaleCommission(commisionId,commissionIsCommissionable,commission,isThirdParty,isGrossMargin,sessionCorpID,userName);
		}
		public List<Charges> findChargeForSaleCommission(String contract,String chargeCode,String description,String sessionCorpID){
			return chargesDao.findChargeForSaleCommission(contract, chargeCode, description, sessionCorpID);
		}
		public List<Charges> findChargesList(String charge, String contract,String sessionCorpID) {
			return chargesDao.findChargesList(charge,contract,sessionCorpID);
		}
		public String findPopulateCostElementData(String charge, String contract,String sessionCorpID){
			return chargesDao.findPopulateCostElementData(charge, contract, sessionCorpID);
		}
		public Map<String,String> findChargeValExcludeMap(String contract,String sessionCorpID,String shipNumber){
			return chargesDao.findChargeValExcludeMap(contract, sessionCorpID, shipNumber);
		}
		public void updateChargeStatus(Long soIdNum, Boolean status){
			chargesDao.updateChargeStatus(soIdNum, status);
		}
		 public List<Charges> findContractForCopyCharges(String contract,String corpID){
		    	return chargesDao.findContractForCopyCharges(contract,corpID);
		    }
		 public List checkChargeCodeListValueNew(String chargeCode,String contract,String corpID){
				return chargesDao.checkChargeCodeListValueNew(chargeCode,contract,corpID);
			}
		 
		 public void findDeleteChargeRateGridOfOtherCorpId(String contract,String agentsCorpId){
			 chargesDao.findDeleteChargeRateGridOfOtherCorpId(contract,agentsCorpId);
		 }
		 
		 public List getTotalChargeCount(String contractName,String sessionCorpID){
			 return chargesDao.getTotalChargeCount(contractName,sessionCorpID);
		 }
		 
		 public List getPresentChargeCountInAgentInstance(String contractName,String agentCorpId){
			 return chargesDao.getPresentChargeCountInAgentInstance(contractName,agentCorpId);
		 }
		 
		 public String callCopyCharge(String sessionCorpID,String contract,String contractCopy, String updatedBy,String copyAgentAccount){
			 return chargesDao.callCopyCharge(sessionCorpID,contract,contractCopy,updatedBy,copyAgentAccount);
		 }
		public List findForPopupChargesContract(String charge, String description, String contract, String corpID, String originCountry, String destCountry, String serviceMode) {
			return chargesDao.findForPopupChargesContract(charge, description, contract, corpID, originCountry, destCountry, serviceMode);
		}
		 

}
