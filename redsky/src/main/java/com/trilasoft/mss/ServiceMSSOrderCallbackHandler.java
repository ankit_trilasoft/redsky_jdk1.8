
/**
 * ServiceMSSOrderCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.5.5  Built on : May 28, 2011 (08:30:56 CEST)
 */

    package com.trilasoft.mss;

    /**
     *  ServiceMSSOrderCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServiceMSSOrderCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServiceMSSOrderCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServiceMSSOrderCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getQuote method
            * override this method for handling normal response from getQuote operation
            */
           public void receiveResultgetQuote(
                    com.trilasoft.mss.webservices.GetQuoteResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuote operation
           */
            public void receiveErrorgetQuote(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQuotePrices method
            * override this method for handling normal response from getQuotePrices operation
            */
           public void receiveResultgetQuotePrices(
                    com.trilasoft.mss.webservices.GetQuotePricesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuotePrices operation
           */
            public void receiveErrorgetQuotePrices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for placeOrderCo method
            * override this method for handling normal response from placeOrderCo operation
            */
           public void receiveResultplaceOrderCo(
                    com.trilasoft.mss.webservices.PlaceOrderCoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from placeOrderCo operation
           */
            public void receiveErrorplaceOrderCo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getDestinationServices method
            * override this method for handling normal response from getDestinationServices operation
            */
           public void receiveResultgetDestinationServices(
                    com.trilasoft.mss.webservices.GetDestinationServicesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getDestinationServices operation
           */
            public void receiveErrorgetDestinationServices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getMSSDateTimeString method
            * override this method for handling normal response from getMSSDateTimeString operation
            */
           public void receiveResultgetMSSDateTimeString(
                    com.trilasoft.mss.webservices.GetMSSDateTimeStringResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getMSSDateTimeString operation
           */
            public void receiveErrorgetMSSDateTimeString(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getPhoneNumberTypes method
            * override this method for handling normal response from getPhoneNumberTypes operation
            */
           public void receiveResultgetPhoneNumberTypes(
                    com.trilasoft.mss.webservices.GetPhoneNumberTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getPhoneNumberTypes operation
           */
            public void receiveErrorgetPhoneNumberTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for placeOrder method
            * override this method for handling normal response from placeOrder operation
            */
           public void receiveResultplaceOrder(
                    com.trilasoft.mss.webservices.PlaceOrderResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from placeOrder operation
           */
            public void receiveErrorplaceOrder(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrderPrices method
            * override this method for handling normal response from getOrderPrices operation
            */
           public void receiveResultgetOrderPrices(
                    com.trilasoft.mss.webservices.GetOrderPricesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrderPrices operation
           */
            public void receiveErrorgetOrderPrices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQuotePdfCo method
            * override this method for handling normal response from getQuotePdfCo operation
            */
           public void receiveResultgetQuotePdfCo(
                    com.trilasoft.mss.webservices.GetQuotePdfCoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuotePdfCo operation
           */
            public void receiveErrorgetQuotePdfCo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQuotePdf method
            * override this method for handling normal response from getQuotePdf operation
            */
           public void receiveResultgetQuotePdf(
                    com.trilasoft.mss.webservices.GetQuotePdfResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuotePdf operation
           */
            public void receiveErrorgetQuotePdf(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrderNotes method
            * override this method for handling normal response from getOrderNotes operation
            */
           public void receiveResultgetOrderNotes(
                    com.trilasoft.mss.webservices.GetOrderNotesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrderNotes operation
           */
            public void receiveErrorgetOrderNotes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCustomerMobileAffiliates method
            * override this method for handling normal response from getCustomerMobileAffiliates operation
            */
           public void receiveResultgetCustomerMobileAffiliates(
                    com.trilasoft.mss.webservices.GetCustomerMobileAffiliatesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCustomerMobileAffiliates operation
           */
            public void receiveErrorgetCustomerMobileAffiliates(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getReadOnlyOrderURL method
            * override this method for handling normal response from getReadOnlyOrderURL operation
            */
           public void receiveResultgetReadOnlyOrderURL(
                    com.trilasoft.mss.webservices.GetReadOnlyOrderURLResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getReadOnlyOrderURL operation
           */
            public void receiveErrorgetReadOnlyOrderURL(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQuoteCo method
            * override this method for handling normal response from getQuoteCo operation
            */
           public void receiveResultgetQuoteCo(
                    com.trilasoft.mss.webservices.GetQuoteCoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuoteCo operation
           */
            public void receiveErrorgetQuoteCo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCustomerContacts method
            * override this method for handling normal response from getCustomerContacts operation
            */
           public void receiveResultgetCustomerContacts(
                    com.trilasoft.mss.webservices.GetCustomerContactsResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCustomerContacts operation
           */
            public void receiveErrorgetCustomerContacts(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOriginServices method
            * override this method for handling normal response from getOriginServices operation
            */
           public void receiveResultgetOriginServices(
                    com.trilasoft.mss.webservices.GetOriginServicesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOriginServices operation
           */
            public void receiveErrorgetOriginServices(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getQuotePdf2 method
            * override this method for handling normal response from getQuotePdf2 operation
            */
           public void receiveResultgetQuotePdf2(
                    com.trilasoft.mss.webservices.GetQuotePdf2Response result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getQuotePdf2 operation
           */
            public void receiveErrorgetQuotePdf2(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrderURLByPONum method
            * override this method for handling normal response from getOrderURLByPONum operation
            */
           public void receiveResultgetOrderURLByPONum(
                    com.trilasoft.mss.webservices.GetOrderURLByPONumResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrderURLByPONum operation
           */
            public void receiveErrorgetOrderURLByPONum(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCustomerBillTos method
            * override this method for handling normal response from getCustomerBillTos operation
            */
           public void receiveResultgetCustomerBillTos(
                    com.trilasoft.mss.webservices.GetCustomerBillTosResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCustomerBillTos operation
           */
            public void receiveErrorgetCustomerBillTos(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for validateLogin method
            * override this method for handling normal response from validateLogin operation
            */
           public void receiveResultvalidateLogin(
                    com.trilasoft.mss.webservices.ValidateLoginResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from validateLogin operation
           */
            public void receiveErrorvalidateLogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCustomerAffiliates method
            * override this method for handling normal response from getCustomerAffiliates operation
            */
           public void receiveResultgetCustomerAffiliates(
                    com.trilasoft.mss.webservices.GetCustomerAffiliatesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCustomerAffiliates operation
           */
            public void receiveErrorgetCustomerAffiliates(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getServiceCategoryTypes method
            * override this method for handling normal response from getServiceCategoryTypes operation
            */
           public void receiveResultgetServiceCategoryTypes(
                    com.trilasoft.mss.webservices.GetServiceCategoryTypesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getServiceCategoryTypes operation
           */
            public void receiveErrorgetServiceCategoryTypes(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getOrderURL method
            * override this method for handling normal response from getOrderURL operation
            */
           public void receiveResultgetOrderURL(
                    com.trilasoft.mss.webservices.GetOrderURLResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getOrderURL operation
           */
            public void receiveErrorgetOrderURL(java.lang.Exception e) {
            }
                


    }
    