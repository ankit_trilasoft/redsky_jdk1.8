<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>  
<head>   
    <title>FAQ List</title>   
    <meta name="heading" content="FAQ List"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-16px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;

}
</style> 
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript"> 
animatedcollapse.addDiv('faq', 'fade=1,hide=0')
animatedcollapse.init()
</script>
<script type="text/javascript"> 

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this FAQ?");
	var id = targetElement;
	var lName="${lastName}";
	if (agree){
		location.href="deleteFrequentlyAskedAdmin.html?partnerCode=${partnerCode}&partnerType=AC&partnerId=${partnerId}&lastName="+encodeURIComponent(lName)+"&status=${status}&id="+id;		
	}else{
		return false;
	}
}
function addForm(){
		location.href="editFrequentlyAsked.html";		
}
function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=FAQ&excludeFPU=TRUE&partnerCode=${partnerCode}";
		  http10.open("GET", url, true); 
		  http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=FAQ&excludeFPU=FALSE&partnerCode=${partnerCode}";
		  http10.open("GET", url, true); 
	      http10.onreadystatechange = handleHttpResponse; 
	      http10.send(null); 
	 }
}
function handleHttpResponse(){
    if (http10.readyState == 4)
    {
    			  var results = http10.responseText
	               results = results.trim();					               
				if(results=="")	{
				
				}
				else{
				
				} 
    } 
}
function updateChildFromParent()
{
		  var url="updateChildFromParent.html?ajax=1&decorator=simple&popup=true&partnerCode=${partnerCode}&pageListType=FAQ";
		  http11.open("GET", url, true); 
		  http11.onreadystatechange = handleHttpResponse1; 
	      http11.send(null); 
}
function handleHttpResponse1(){
    if (http11.readyState == 4)
    {
    			  var results = http11.responseText
	               results = results.trim();					               
				if(results!="")	{
				alert(results);				
				} 
    } 
}
var http11 = getHTTPObject();
var http10 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>
</head>
<s:form id="frequentlyAskedQuestionsListPage" action="" method="post" validate="true">


	<s:hidden name="partnerType" value="AC"/>
	<s:hidden name="partnerCode" value="${partnerCode}"/>
	<s:hidden name="partnerId" value="${partnerId}"/>
	<c:set var="partnerCode" value="${partnerCode}"/>
	<c:set var="partnerId" value="${partnerId}"/>
	<c:set var="lastName" value="${lastName}" />
<c:url value="editFrequentlyAskedQuestions.html" var="url">
<c:param name="partnerCode" value="${partnerCode}"/>
<c:param name="partnerType" value="AC"/>
<c:param name="partnerId" value="${partnerId}"/>
<c:param name="lastName" value="${lastName}"/>
<c:param name="status" value="${status}"/>
</c:url>
	
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>
		<c:choose>
					<c:when test="${usertype=='ACCOUNT'}">
					<li><a href="searchCPortalMgmts.html"><span>Cportal Docs</span></a></li>
					</c:when>
					<c:otherwise>
					<li><a href="cPortalResourceMgmts.html"><span>Cportal Docs</span></a></li>
					</c:otherwise>
					</c:choose>
							
		<li><a href="contractFilePolicy.html"><span>Policy</span></a></li>
		<li id="newmnav1" style="background:#FFF "><a class="current" style="margin:0px;"><span>FAQ</span></a></li>
	
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
	</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
<div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom:3px;width:100%;">
    <tr>
	<td height="0" width="100%" align="left" >		
<div   style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"> FAQ
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div> 
</td>
</tr>
</table>
<table  width="100%" cellspacing="1" cellpadding="0" border="0" class="detailTabLabel">
	<tbody>
	<tr><td height="5"></td></tr>
		<tr>
			<td>				
<display:table name="frequentlyAskedQuestionsList" defaultsort="4" class="table" id="frequentlyAskedQuestionsList" requestURI="" pagesize="25" style="width:100%;">
<display:column sortable="true" sortProperty="sequenceNumber" title="Sequence#" style="width:70px;">
<c:url value="editFrequentlyAsked.html" var="url1" >
<c:param name="id" value="${frequentlyAskedQuestionsList.id}"/>
  </c:url>
      <c:out value="${frequentlyAskedQuestionsList.sequenceNumber}" />
                  </display:column>
 <display:column sortable="true" style="width:350px" title="Question"  maxLength="80" >
 <a href="${url1}"><c:out value="${frequentlyAskedQuestionsList.question}"  /></a>
 </display:column> 	
	<display:column property="answer" sortable="true" title="Answer" maxLength="80"/>
	<display:column property="jobType" sortable="true" title="Job&nbsp;Type" />
	<display:column property="language" sortable="true" title="Language" style="width:70px"/>
	<c:if test="${usertype!='ACCOUNT'}">	
	<display:column title="Remove" style="width: 15px;" >
	<a><img align="middle" onclick="confirmSubmit(${frequentlyAskedQuestionsList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
	</display:column>
	</c:if>
</display:table>
			</td>
		</tr>
	</tbody>
</table> 

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:set var="buttons"> 	
<c:if test="${usertype!='ACCOUNT'}">	
    <input type="button" class="cssbutton" style="width:55px; height:25px;margin-left:20px;" onclick="addForm();" value="<fmt:message key="button.add"/>"/>
    </c:if> 
</c:set> 
<!--<c:if test="${usertype!='ACCOUNT'}">
<table width="100%">
											<tr align="right">
											<td align="right"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="updateChildFromParent();"/></td><td></td>
											</tr>
											<tr align="right">
											<td align="right" class="listwhitetext">Exclude from Parent updates</td>
											<td class="listwhitetext" width="8px" align="right"><s:checkbox name="excludeFromParent" id="excludeFPU" value="${excludeFPU}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>
														 		  	   	 	               	
											</tr>												
											</table>
											</c:if>
--><c:out value="${buttons}" escapeXml="false" /> 
  	
  	

</s:form>

