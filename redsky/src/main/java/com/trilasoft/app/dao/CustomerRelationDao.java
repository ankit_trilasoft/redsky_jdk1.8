package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CustomerRelation; 

public interface CustomerRelationDao extends GenericDao<CustomerRelation, Long>{

	public CustomerRelation getCrmDetails(Long cid,String sessionCorpID);
}
