package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.BrokerDetails;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.BrokerDetailsManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.CustomManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class BrokerDetailsAction extends BaseAction implements Preparable {
	private ServiceOrder serviceOrder;
	private ServiceOrderManager serviceOrderManager;
	private Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private Billing billing;
	private BillingManager billingManager;
	private CustomerFile customerFile;
	private ConsigneeInstruction consigneeInstruction;
	private ConsigneeInstructionManager consigneeInstructionManager;
	private List consigneeInstructionList;
	private List consigneeShiperList;
	private List consigneeShipmentLocationList;
	private Long sid;
	static final Logger logger = Logger.getLogger(BrokerDetailsAction.class);
	Date currentdate = new Date();
	private String consigneeInstructionCode="";
	private String shipNum;
	private String DaCode;
	private String sACode;
	private String osACode;
	private CustomerFileManager customerFileManager;
	private String shipSize;
	private String minShip;
	private String countShip;
	private CustomManager customManager;
	private String countBondedGoods;
	private BrokerDetails brokerDetails;
	private BrokerDetailsManager brokerDetailsManager;
	private List vendorAgentList;
	private String sessionCorpID;

	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	public BrokerDetailsAction() {
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal();
	this.sessionCorpID = user.getCorpID();
	}
	private String forwardingAjaxUrl;
	private String forwardingAjaxVal;
	private String hitFlag;
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		serviceOrder = serviceOrderManager.get(sid);
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		miscellaneous = miscellaneousManager.get(sid);
    	trackingStatus = trackingStatusManager.get(sid); 
    	billing=billingManager.get(sid);
    	customerFile = serviceOrder.getCustomerFile();
		//getComboList(sessionCorpID);
		boolean isNew = (brokerDetails.getId() == null);
		if(isNew){
			brokerDetails.setCreatedOn(new Date());
			brokerDetails.setCreatedBy(getRequest().getRemoteUser());
        }
		
		if((brokerDetails.getVendorCode()!=null) && (!brokerDetails.getVendorCode().toString().trim().equals(""))){
			try{
				vendorAgentList=customerFileManager.findByBillToCode(brokerDetails.getVendorCode(), sessionCorpID);
			if(vendorAgentList.size()>0 && !vendorAgentList.isEmpty()){
				brokerDetails.setVendorName(vendorAgentList.get(0).toString());
			}
			}catch (NullPointerException nullExc){
				nullExc.printStackTrace();						
			}
			catch (Exception e){
				e.printStackTrace();
			}
		}
        
		brokerDetails.setUpdatedOn(new Date());
		brokerDetails.setUpdatedBy(getRequest().getRemoteUser());
		brokerDetails.setShipNumber(serviceOrder.getShipNumber());
		brokerDetails.setShip(serviceOrder.getShip());
		brokerDetails.setSequenceNumber(serviceOrder.getSequenceNumber());
		brokerDetails.setCorpID(serviceOrder.getCorpID());
		brokerDetails.setId(serviceOrder.getId());
		brokerDetails=brokerDetailsManager.save(brokerDetails);
		String key = (isNew) ? "brokerDetails.added" : "brokerDetails.updated";
		saveMessage(getText(key));
		shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        hitFlag="1";
        if((forwardingAjaxVal!=null && !forwardingAjaxVal.equalsIgnoreCase("")) && forwardingAjaxVal.equalsIgnoreCase("Yes")){
        	forwardingAjaxUrl = "?id="+serviceOrder.getId();
        }
		if (!isNew) {
			return INPUT;
			
		} else {
			return SUCCESS;
		}		
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public ServiceOrderManager getServiceOrderManager() {
		return serviceOrderManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public MiscellaneousManager getMiscellaneousManager() {
		return miscellaneousManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public TrackingStatusManager getTrackingStatusManager() {
		return trackingStatusManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public BillingManager getBillingManager() {
		return billingManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ConsigneeInstruction getConsigneeInstruction() {
		return consigneeInstruction;
	}

	public void setConsigneeInstruction(ConsigneeInstruction consigneeInstruction) {
		this.consigneeInstruction = consigneeInstruction;
	}

	public ConsigneeInstructionManager getConsigneeInstructionManager() {
		return consigneeInstructionManager;
	}

	public void setConsigneeInstructionManager(
			ConsigneeInstructionManager consigneeInstructionManager) {
		this.consigneeInstructionManager = consigneeInstructionManager;
	}

	public List getConsigneeInstructionList() {
		return consigneeInstructionList;
	}

	public void setConsigneeInstructionList(List consigneeInstructionList) {
		this.consigneeInstructionList = consigneeInstructionList;
	}

	public List getConsigneeShiperList() {
		return consigneeShiperList;
	}

	public void setConsigneeShiperList(List consigneeShiperList) {
		this.consigneeShiperList = consigneeShiperList;
	}

	public List getConsigneeShipmentLocationList() {
		return consigneeShipmentLocationList;
	}

	public void setConsigneeShipmentLocationList(List consigneeShipmentLocationList) {
		this.consigneeShipmentLocationList = consigneeShipmentLocationList;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public String getConsigneeInstructionCode() {
		return consigneeInstructionCode;
	}

	public void setConsigneeInstructionCode(String consigneeInstructionCode) {
		this.consigneeInstructionCode = consigneeInstructionCode;
	}

	public String getShipNum() {
		return shipNum;
	}

	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}

	public String getDaCode() {
		return DaCode;
	}

	public void setDaCode(String daCode) {
		DaCode = daCode;
	}

	public String getsACode() {
		return sACode;
	}

	public void setsACode(String sACode) {
		this.sACode = sACode;
	}

	public String getOsACode() {
		return osACode;
	}

	public void setOsACode(String osACode) {
		this.osACode = osACode;
	}

	public CustomerFileManager getCustomerFileManager() {
		return customerFileManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public String getShipSize() {
		return shipSize;
	}

	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}

	public String getMinShip() {
		return minShip;
	}

	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}

	public String getCountShip() {
		return countShip;
	}

	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}

	public CustomManager getCustomManager() {
		return customManager;
	}

	public void setCustomManager(CustomManager customManager) {
		this.customManager = customManager;
	}

	public String getCountBondedGoods() {
		return countBondedGoods;
	}

	public void setCountBondedGoods(String countBondedGoods) {
		this.countBondedGoods = countBondedGoods;
	}

	public BrokerDetails getBrokerDetails() {
		return brokerDetails;
	}

	public void setBrokerDetails(BrokerDetails brokerDetails) {
		this.brokerDetails = brokerDetails;
	}

	public BrokerDetailsManager getBrokerDetailsManager() {
		return brokerDetailsManager;
	}

	public void setBrokerDetailsManager(BrokerDetailsManager brokerDetailsManager) {
		this.brokerDetailsManager = brokerDetailsManager;
	}

	public static Logger getLogger() {
		return logger;
	}

	public String getForwardingAjaxUrl() {
		return forwardingAjaxUrl;
	}

	public void setForwardingAjaxUrl(String forwardingAjaxUrl) {
		this.forwardingAjaxUrl = forwardingAjaxUrl;
	}

	public String getForwardingAjaxVal() {
		return forwardingAjaxVal;
	}

	public void setForwardingAjaxVal(String forwardingAjaxVal) {
		this.forwardingAjaxVal = forwardingAjaxVal;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public List getVendorAgentList() {
		return vendorAgentList;
	}

	public void setVendorAgentList(List vendorAgentList) {
		this.vendorAgentList = vendorAgentList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
	

}
