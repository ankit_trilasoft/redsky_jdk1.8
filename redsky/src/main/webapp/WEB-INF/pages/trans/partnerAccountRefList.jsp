<%@ include file="/common/taglibs.jsp"%> 
 
<head> 
    <title><fmt:message key="partnerAccountRef.title"/></title> 
    <meta name="heading" content="<fmt:message key='partnerAccountRef.heading'/>"/>         
<script>
function goToCustomerDetail(targetValue){
		var pType=document.forms['serviceForm1'].elements['partnerType'].value;
        document.forms['serviceForm1'].elements['id'].value = targetValue;
        document.forms['serviceForm1'].action = 'editPartnerAccountRef.html?from=list&partnerType='+pType;
        document.forms['serviceForm1'].submit();
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	var partnerCodeForRef=document.forms['serviceForm1'].elements['abc'].value;
	var partnerType=document.forms['serviceForm1'].elements['partnerType'].value;
	if (agree){
	     location.href = "deleteAcctRef.html?id="+encodeURI(targetElement)+"&partnerCodeForRef="+encodeURI(partnerCodeForRef)+"&partnerType="+encodeURI(partnerType);
     }else{
		return false;
	}
}

function addAccRef(){
	var error = '${errMessage}';
	var pcodeForRef = document.forms['serviceForm1'].elements['abc'].value;
	var pType= document.forms['serviceForm1'].elements['partnerType'].value;
	if(error==''){
		location.href="editPartnerAccountRef.html?partnerCodeForRef="+encodeURI(pcodeForRef)+"&partnerType="+encodeURI(pType);
	}else{
		alert(error);
	}
}

</script>
</head> 

<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="serviceForm1" > 

<c:set var ="abc" value="<%=request.getParameter("partnerCodeForRef") %>"/>
<s:hidden name="abc"  value="<%=request.getParameter("partnerCodeForRef") %>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partner.id"/>
<c:set var="buttons"> 
    <input type="button" class="cssbutton" style="margin-right: 5px; width:45px;" 
        onclick="addAccRef()" 
        value="<fmt:message key="button.add"/>"/> 
</c:set>
<div id="layer4" style="width:100%;">
<div id="newmnav"> 
<ul>
<c:if test="${partnerType == 'AG'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
	<li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
	<c:if test="${sessionCorpID!='TSFT' }">
	<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
	</c:if>
</c:if>
<c:if test="${partnerType == 'VN'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Vendor Detail</span></a></li>
	<c:if test="${sessionCorpID!='TSFT' }">
	<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
	</c:if>
</c:if>
<c:if test="${partnerType == 'CR'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Carrier Detail</span></a></li>
</c:if>
<c:if test="${partnerType == 'AC'}">
<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AC"><span>Account Detail</span></a></li>
<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
<c:if test="${sessionCorpID!='TSFT' }">
<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerCode=${partner.partnerCode}&partnerType=${partnerType}"><span>Additional Info</span></a></li>
</c:if>
<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
<li><a href="editProfileInfo.html?partnerId=${partner.id}&partnerCode=${partner.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
</configByCorp:fieldVisibility>
</c:if>
<c:if test="${partnerType == 'PP'}">
<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=PP"><span>Private Party Detail</span></a></li>
</c:if>
<li id="newmnav1" style="background:#FFF "><a class="current"><span>Acct Ref List</span></a></li>
	
<c:if test="${usertype!='ACCOUNT'}">
	<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
		<c:if test="${partnerType == 'AC'}">
			<c:if test="${not empty partner.id}">
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
				<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
					<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
				<c:if test="${partner.partnerPortalActive == true}">
					 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation"><li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users</span></a></li></configByCorp:fieldVisibility>
				</c:if>
			</c:if>
		</c:if>
	</sec-auth:authComponent>
</c:if>
<c:if test="${partnerType == 'AG'}">
	<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
	<li><a href="baseList.html?id=${partner.id}"><span>Base</span></a></li>
	<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
	<%--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
		 <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
	 </c:if>
	 <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
		 <li><a href="partnerRateGrids.html?partnerId=${partner.id}"><span>Rate Matrix</span></a></li>
	 </c:if> 
--%></c:if>
<c:if test="${partnerType == 'OO'}">
	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Owner Ops Detail</span></a></li>
	<li><a href="standardDeductionsList.html?id=${partner.id}&partnerType=OO"><span>Standard Deductions</span></a></li>
</c:if>
<c:if test="${partnerType != 'AG'}">
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<c:if test='${paramValue == "View"}'>
		<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
	</c:if>
	<c:if test='${paramValue != "View"}'>
		<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
	</c:if>
</c:if>
<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC' }">
		<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
	<c:if test="${partner.partnerPortalActive == true  && partnerType == 'AG'}">
	       <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	       	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
	       	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
	       	</configByCorp:fieldVisibility>
	</c:if>
	  <c:if test="${partnerType == 'AG'}">
			
			       <li id="AGR"><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
	
	</c:if>
				<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partner.id}">
<c:url value="frequentlyAskedQuestionsList.html" var="url">
<c:param name="partnerCode" value="${partner.partnerCode}"/>
<c:param name="partnerType" value="AC"/>
<c:param name="partnerId" value="${partner.id}"/>
<c:param name="lastName" value="${partner.lastName}"/>
<c:param name="status" value="${partner.status}"/>
</c:url>
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partner.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>			
		</c:if>	
</ul>
</div>
<div class="spn">&nbsp;</div>
   <div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
		<tr>
		  	<td align="right" class="listwhitetext">Code</td>
	<td><s:textfield key="partner.partnerCode" required="true" readonly="true" cssClass="input-textUpper"/></td>
	<td align="right" class="listwhitetext" width="70px">Name</td>
	<td><s:textfield key="partner.lastName" required="true" size="35" readonly="true" cssClass="input-textUpper"/></td>
	</tr>
	</table>
	
	</div>
<div class="bottom-header" style="margin-top:40px;"><span></span></div>
</div>
</div>
<s:set name="partnerAccountRefs" value="partnerAccountRefs" scope="request"/> 
	<display:table name="partnerAccountRefs" class="table" requestURI="" id="partnerAccountRefList" export="false" pagesize="25" style="width:99%;margin-left:5px;"> 
	    <display:column   sortable="true" title="Type" >
	    <c:if test="${partnerAccountRefList.refType=='P'}">
	    <a onclick="goToCustomerDetail(${partnerAccountRefList.id});" style="cursor:pointer"><c:out value="Payable" /></a>
 		</c:if>
 		 <c:if test="${partnerAccountRefList.refType=='R'}">
	    <a onclick="goToCustomerDetail(${partnerAccountRefList.id});" style="cursor:pointer"><c:out value="Receivable" /></a>
 		</c:if>
 		<c:if test="${partnerAccountRefList.refType=='S'}">
	    <a onclick="goToCustomerDetail(${partnerAccountRefList.id});" style="cursor:pointer"><c:out value="Suspense" /></a>
 		</c:if>
 		</display:column>
 		<display:column sortable="true" titleKey="partnerAccountRef.accountCrossReference" property="accountCrossReference"/> 
 		<c:if test="${companyDivisionAcctgCodeUnique=='Y'}">
    	<display:column sortable="true" titleKey="partnerAccountRef.companyDivision" property="companyDivision"></display:column>
    	</c:if>
 		<sec-auth:authComponent componentId="module.field.partnerAccountRef.allFields">
	 		<display:column title="Remove" style="width:45px; text-align: center;">
				<a><img align="middle" onclick="confirmSubmit(${partnerAccountRefList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
			</display:column>
		</sec-auth:authComponent>
 	</display:table>
<sec-auth:authComponent componentId="module.field.partnerAccountRef.allFields"> 	
	<c:out value="${buttons}" escapeXml="false" /> 
</sec-auth:authComponent>	
<s:hidden name="id"></s:hidden>
</div> 
 </s:form>
<script type="text/javascript"> 
</script> 
<script type="text/javascript">
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}" ></c:redirect>
		</c:if>
		}
		<c:if test="${partnerType=='AG'}">
		var checkRoleAG = 0;
		//Modified By subrat BUG #6471 
		<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
			checkRoleAG  = 14;
		</sec-auth:authComponent>
		if(checkRoleAG < 14){
			document.getElementById('AGR').style.visibility = 'visible';
		}else{
			document.getElementById('AGR').style.visibility = 'hidden';
		}
		</c:if>
		catch(e){}
</script>   