package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="quotation")
public class Quotation extends BaseObject{
	
	private String corpID;
	private Long id;
	private BigDecimal idNumber;
	private String shipmentNumber;
	
	private String quote;
	private String quoteType;
	private String vendorName;	
	private String originCountry;
	private String originCity;
	private String billToName;
	
	private String commodity;	
	private String mode;	
	private String equipment;	
	private String packingMode;	
	private BigDecimal actualNetWeight;	
	private BigDecimal actualCubicFeet;
	private String currency;
	
	private String destSerBasis;
	private String destSerUnit;
	private String destSerRate;
	private String destSerPrice;
	
	private String thcBasis;
	private String thcUnit;
	private String thcRate;
	private String thcPrice;
	
	private String customBasis;
	private String customeUnit;
	private String customRate;
	private String customPrice;
	
	private String autoBasis;
	private String autoUnit;
	private String autoRate;
	private String autoPrice;
	
	private String totalPrice;
	
	
	private String quoteStatus;	
	
	private Boolean checkStatus;
	
	private String checked;
	
	//private long maximumRow;
	private long minId;
	private long maxId;
	
	private long sid;
	private long cid;
	private long pqId;
	
	
	private BigDecimal NetWeightkg;
	private BigDecimal NetWeightcbm;
	
	private String sequenceNumber;
	
//	New Field 
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	
	
	
	
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID).append("id",
				id).append("idNumber", idNumber).append("shipmentNumber",
				shipmentNumber).append("quote", quote).append("quoteType",
				quoteType).append("vendorName", vendorName).append(
				"originCountry", originCountry)
				.append("originCity", originCity).append("billToName",
						billToName).append("commodity", commodity).append(
						"mode", mode).append("equipment", equipment).append(
						"packingMode", packingMode).append("actualNetWeight",
						actualNetWeight).append("actualCubicFeet",
						actualCubicFeet).append("currency", currency).append(
						"destSerBasis", destSerBasis).append("destSerUnit",
						destSerUnit).append("destSerRate", destSerRate).append(
						"destSerPrice", destSerPrice).append("thcBasis",
						thcBasis).append("thcUnit", thcUnit).append("thcRate",
						thcRate).append("thcPrice", thcPrice).append(
						"customBasis", customBasis).append("customeUnit",
						customeUnit).append("customRate", customRate).append(
						"customPrice", customPrice).append("autoBasis",
						autoBasis).append("autoUnit", autoUnit).append(
						"autoRate", autoRate).append("autoPrice", autoPrice)
				.append("totalPrice", totalPrice).append("quoteStatus",
						quoteStatus).append("checkStatus", checkStatus).append(
						"checked", checked).append("minId", minId).append(
						"maxId", maxId).append("sid", sid).append("cid", cid)
				.append("pqId", pqId).append("NetWeightkg", NetWeightkg)
				.append("NetWeightcbm", NetWeightcbm).append("sequenceNumber",
						sequenceNumber).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("valueDate", valueDate)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Quotation))
			return false;
		Quotation castOther = (Quotation) other;
		return new EqualsBuilder().append(corpID, castOther.corpID).append(id,
				castOther.id).append(idNumber, castOther.idNumber).append(
				shipmentNumber, castOther.shipmentNumber).append(quote,
				castOther.quote).append(quoteType, castOther.quoteType).append(
				vendorName, castOther.vendorName).append(originCountry,
				castOther.originCountry).append(originCity,
				castOther.originCity).append(billToName, castOther.billToName)
				.append(commodity, castOther.commodity).append(mode,
						castOther.mode).append(equipment, castOther.equipment)
				.append(packingMode, castOther.packingMode).append(
						actualNetWeight, castOther.actualNetWeight).append(
						actualCubicFeet, castOther.actualCubicFeet).append(
						currency, castOther.currency).append(destSerBasis,
						castOther.destSerBasis).append(destSerUnit,
						castOther.destSerUnit).append(destSerRate,
						castOther.destSerRate).append(destSerPrice,
						castOther.destSerPrice).append(thcBasis,
						castOther.thcBasis).append(thcUnit, castOther.thcUnit)
				.append(thcRate, castOther.thcRate).append(thcPrice,
						castOther.thcPrice).append(customBasis,
						castOther.customBasis).append(customeUnit,
						castOther.customeUnit).append(customRate,
						castOther.customRate).append(customPrice,
						castOther.customPrice).append(autoBasis,
						castOther.autoBasis).append(autoUnit,
						castOther.autoUnit)
				.append(autoRate, castOther.autoRate).append(autoPrice,
						castOther.autoPrice).append(totalPrice,
						castOther.totalPrice).append(quoteStatus,
						castOther.quoteStatus).append(checkStatus,
						castOther.checkStatus).append(checked,
						castOther.checked).append(minId, castOther.minId)
				.append(maxId, castOther.maxId).append(sid, castOther.sid)
				.append(cid, castOther.cid).append(pqId, castOther.pqId)
				.append(NetWeightkg, castOther.NetWeightkg).append(
						NetWeightcbm, castOther.NetWeightcbm).append(
						sequenceNumber, castOther.sequenceNumber).append(
						createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(valueDate,
						castOther.valueDate).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(idNumber)
				.append(shipmentNumber).append(quote).append(quoteType).append(
						vendorName).append(originCountry).append(originCity)
				.append(billToName).append(commodity).append(mode).append(
						equipment).append(packingMode).append(actualNetWeight)
				.append(actualCubicFeet).append(currency).append(destSerBasis)
				.append(destSerUnit).append(destSerRate).append(destSerPrice)
				.append(thcBasis).append(thcUnit).append(thcRate).append(
						thcPrice).append(customBasis).append(customeUnit)
				.append(customRate).append(customPrice).append(autoBasis)
				.append(autoUnit).append(autoRate).append(autoPrice).append(
						totalPrice).append(quoteStatus).append(checkStatus)
				.append(checked).append(minId).append(maxId).append(sid)
				.append(cid).append(pqId).append(NetWeightkg).append(
						NetWeightcbm).append(sequenceNumber).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn).append(
						valueDate).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=10 )
	public BigDecimal getActualCubicFeet() {
		return actualCubicFeet;
	}
	public void setActualCubicFeet(BigDecimal actualCubicFeet) {
		this.actualCubicFeet = actualCubicFeet;
	}
	@Column( length=10 )
	public BigDecimal getActualNetWeight() {
		return actualNetWeight;
	}
	public void setActualNetWeight(BigDecimal actualNetWeight) {
		this.actualNetWeight = actualNetWeight;
	}
	@Column( length=10 )
	public String getAutoBasis() {
		return autoBasis;
	}
	public void setAutoBasis(String autoBasis) {
		this.autoBasis = autoBasis;
	}
	@Column( length=30 )
	public String getAutoPrice() {
		return autoPrice;
	}
	public void setAutoPrice(String autoPrice) {
		this.autoPrice = autoPrice;
	}
	@Column( length=10 )
	public String getAutoRate() {
		return autoRate;
	}
	public void setAutoRate(String autoRate) {
		this.autoRate = autoRate;
	}
	@Column( length=10 )
	public String getAutoUnit() {
		return autoUnit;
	}
	public void setAutoUnit(String autoUnit) {
		this.autoUnit = autoUnit;
	}
	@Column( length=50 )
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	@Column( length=50 )
	public String getCommodity() {
		return commodity;
	}
	public void setCommodity(String commodity) {
		this.commodity = commodity;
	}
	@Column( length=10 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column( length=10 )
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=20 )
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column( length=10 )
	public String getCustomBasis() {
		return customBasis;
	}
	public void setCustomBasis(String customBasis) {
		this.customBasis = customBasis;
	}
	@Column( length=10 )
	public String getCustomeUnit() {
		return customeUnit;
	}
	public void setCustomeUnit(String customeUnit) {
		this.customeUnit = customeUnit;
	}
	@Column( length=30 )
	public String getCustomPrice() {
		return customPrice;
	}
	public void setCustomPrice(String customPrice) {
		this.customPrice = customPrice;
	}
	@Column( length=10 )
	public String getCustomRate() {
		return customRate;
	}
	public void setCustomRate(String customRate) {
		this.customRate = customRate;
	}
	@Column( length=10 )
	public String getDestSerBasis() {
		return destSerBasis;
	}
	public void setDestSerBasis(String destSerBasis) {
		this.destSerBasis = destSerBasis;
	}
	@Column( length=30 )
	public String getDestSerPrice() {
		return destSerPrice;
	}
	public void setDestSerPrice(String destSerPrice) {
		this.destSerPrice = destSerPrice;
	}
	@Column( length=10 )
	public String getDestSerRate() {
		return destSerRate;
	}
	public void setDestSerRate(String destSerRate) {
		this.destSerRate = destSerRate;
	}
	@Column( length=10 )
	public String getDestSerUnit() {
		return destSerUnit;
	}
	public void setDestSerUnit(String destSerUnit) {
		this.destSerUnit = destSerUnit;
	}
	@Column( length=20 )
	public String getEquipment() {
		return equipment;
	}
	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}
	@Column( length=20 )
	public BigDecimal getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(BigDecimal idNumber) {
		this.idNumber = idNumber;
	}
	@Column( length=30 )
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column( length=25 )
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	@Column( length=25 )
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	@Column( length=25 )
	public String getPackingMode() {
		return packingMode;
	}
	public void setPackingMode(String packingMode) {
		this.packingMode = packingMode;
	}
	@Column( length=25 )
	public String getQuote() {
		return quote;
	}
	public void setQuote(String quote) {
		this.quote = quote;
	}
	
	@Column( length=25 )
	public String getQuoteStatus() {
		return quoteStatus;
	}
	public void setQuoteStatus(String quoteStatus) {
		this.quoteStatus = quoteStatus;
	}
	
	@Column( length=25 )
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	
	@Column( length=10 )
	public String getShipmentNumber() {
		return shipmentNumber;
	}
	public void setShipmentNumber(String shipmentNumber) {
		this.shipmentNumber = shipmentNumber;
	}
	
	@Column( length=10 )
	public String getThcBasis() {
		return thcBasis;
	}
	public void setThcBasis(String thcBasis) {
		this.thcBasis = thcBasis;
	}
	
	@Column( length=30 )
	public String getThcPrice() {
		return thcPrice;
	}
	public void setThcPrice(String thcPrice) {
		this.thcPrice = thcPrice;
	}
	
	@Column( length=10 )
	public String getThcRate() {
		return thcRate;
	}
	public void setThcRate(String thcRate) {
		this.thcRate = thcRate;
	}
	
	@Column( length=10 )
	public String getThcUnit() {
		return thcUnit;
	}
	public void setThcUnit(String thcUnit) {
		this.thcUnit = thcUnit;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column( length=10 )
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column( length=10 )
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column(length=60)
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column(length=30)
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Column(length=10)
	public long getMaxId() {
		return maxId;
	}
	public void setMaxId(long maxId) {
		this.maxId = maxId;
	}
	
	@Column(length=10)
	public long getMinId() {
		return minId;
	}
	public void setMinId(long minId) {
		this.minId = minId;
	}
	public long getCid() {
		return cid;
	}
	public void setCid(long cid) {
		this.cid = cid;
	}
	public long getSid() {
		return sid;
	}
	public void setSid(long sid) {
		this.sid = sid;
	}
	@Column(length=10)
	public BigDecimal getNetWeightcbm() {
		return NetWeightcbm;
	}
	public void setNetWeightcbm(BigDecimal netWeightcbm) {
		NetWeightcbm = netWeightcbm;
	}
	@Column(length=10)
	public BigDecimal getNetWeightkg() {
		return NetWeightkg;
	}
	public void setNetWeightkg(BigDecimal netWeightkg) {
		NetWeightkg = netWeightkg;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public Boolean getCheckStatus() {
		return checkStatus;
	}
	public void setCheckStatus(Boolean checkStatus) {
		this.checkStatus = checkStatus;
	}
	public long getPqId() {
		return pqId;
	}
	public void setPqId(long pqId) {
		this.pqId = pqId;
	}
	public String getChecked() {
		return checked;
	}
	public void setChecked(String checked) {
		this.checked = checked;
	}
	
	
	
	
	
	

	

	

}
