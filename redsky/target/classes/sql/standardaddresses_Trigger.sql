
delimiter $$

CREATE trigger redsky.trigger_add_history_standardaddresses
BEFORE UPDATE on redsky.standardaddresses
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'standardaddresses.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;





   IF (OLD.job <> NEW.job ) THEN
      select CONCAT(fieldNameList,'standardaddresses.job~') into fieldNameList;
      select CONCAT(oldValueList,OLD.job,'~') into oldValueList;
      select CONCAT(newValueList,NEW.job,'~') into newValueList;
  END IF;




   IF (OLD.addressLine1 <> NEW.addressLine1 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.addressLine1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.addressLine1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.addressLine1,'~') into newValueList;
  END IF;




   IF (OLD.addressLine2 <> NEW.addressLine2 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.addressLine2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.addressLine2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.addressLine2,'~') into newValueList;
  END IF;




   IF (OLD.addressLine3 <> NEW.addressLine3 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.addressLine3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.addressLine3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.addressLine3,'~') into newValueList;
  END IF;




   IF (OLD.state <> NEW.state ) THEN
      select CONCAT(fieldNameList,'standardaddresses.state~') into fieldNameList;
      select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
      select CONCAT(newValueList,NEW.state,'~') into newValueList;
  END IF;




   IF (OLD.countryCode <> NEW.countryCode ) THEN
      select CONCAT(fieldNameList,'standardaddresses.countryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.countryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.countryCode,'~') into newValueList;
  END IF;




   IF (OLD.zip <> NEW.zip ) THEN
      select CONCAT(fieldNameList,'standardaddresses.zip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.zip,'~') into newValueList;
  END IF;




   IF (OLD.dayPhone <> NEW.dayPhone ) THEN
      select CONCAT(fieldNameList,'standardaddresses.dayPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.dayPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.dayPhone,'~') into newValueList;
  END IF;




   IF (OLD.homePhone <> NEW.homePhone ) THEN
      select CONCAT(fieldNameList,'standardaddresses.homePhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.homePhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.homePhone,'~') into newValueList;
  END IF;




   IF (OLD.faxPhone <> NEW.faxPhone ) THEN
      select CONCAT(fieldNameList,'standardaddresses.faxPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.faxPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.faxPhone,'~') into newValueList;
  END IF;




   IF (OLD.city <> NEW.city ) THEN
      select CONCAT(fieldNameList,'standardaddresses.city~') into fieldNameList;
      select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
      select CONCAT(newValueList,NEW.city,'~') into newValueList;
  END IF;


      IF (OLD.mobPhone <> NEW.mobPhone ) THEN
      select CONCAT(fieldNameList,'standardaddresses.mobPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mobPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mobPhone,'~') into newValueList;
  END IF;



  
  
  
  
  
  
  
   IF (OLD.residenceType <> NEW.residenceType ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residenceType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residenceType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residenceType,'~') into newValueList;
  END IF; 
  
  IF (OLD.residenceDeposit1 <> NEW.residenceDeposit1 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residenceDeposit1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residenceDeposit1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residenceDeposit1,'~') into newValueList;
  END IF; 
  
  IF (OLD.residencePurpose1 <> NEW.residencePurpose1 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residencePurpose1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residencePurpose1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residencePurpose1,'~') into newValueList;
  END IF; 
  
  IF (OLD.residenceDeposit2 <> NEW.residenceDeposit2 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residenceDeposit2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residenceDeposit2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residenceDeposit2,'~') into newValueList;
  END IF; 
  
  IF (OLD.residencePurpose2 <> NEW.residencePurpose2 ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residencePurpose2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residencePurpose2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residencePurpose2,'~') into newValueList;
  END IF; 
  
  IF (OLD.residencePayType <> NEW.residencePayType ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residencePayType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residencePayType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residencePayType,'~') into newValueList;
  END IF; 
   
   IF (OLD.residenceNotes <> NEW.residenceNotes ) THEN
      select CONCAT(fieldNameList,'standardaddresses.residenceNotes~') into fieldNameList;
      select CONCAT(oldValueList,OLD.residenceNotes,'~') into oldValueList;
      select CONCAT(newValueList,NEW.residenceNotes,'~') into newValueList;
  END IF; 
  
   IF (OLD.email <> NEW.email ) THEN
      select CONCAT(fieldNameList,'standardaddresses.email~') into fieldNameList;
      select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
      select CONCAT(newValueList,NEW.email,'~') into newValueList;
  END IF; 
  
   IF (OLD.contactPersonEmail <> NEW.contactPersonEmail ) THEN
      select CONCAT(fieldNameList,'standardaddresses.contactPersonEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactPersonEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactPersonEmail,'~') into newValueList;
  END IF; 
  
   IF (OLD.contactPerson <> NEW.contactPerson ) THEN
      select CONCAT(fieldNameList,'standardaddresses.contactPerson~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactPerson,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactPerson,'~') into newValueList;
  END IF; 
  
   IF (OLD.contactPersonPhone <> NEW.contactPersonPhone ) THEN
      select CONCAT(fieldNameList,'standardaddresses.contactPersonPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactPersonPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactPersonPhone,'~') into newValueList;
  END IF;


 IF ((OLD.depositAmount1 <> NEW.depositAmount1) or (OLD.depositAmount1 is null and NEW.depositAmount1 is not null)
      or (OLD.depositAmount1 is not null and NEW.depositAmount1 is null)) THEN
        select CONCAT(fieldNameList,'standardaddresses.depositAmount1~') into fieldNameList;
        IF(OLD.depositAmount1 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.depositAmount1 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.depositAmount1 is not null) then
            select CONCAT(oldValueList,OLD.depositAmount1,'~') into oldValueList;
        END IF;
        IF(NEW.depositAmount1 is not null) then
            select CONCAT(newValueList,NEW.depositAmount1,'~') into newValueList;
        END IF;
    END IF;


 IF ((OLD.depositAmount2 <> NEW.depositAmount2) or (OLD.depositAmount2 is null and NEW.depositAmount2 is not null)
      or (OLD.depositAmount2 is not null and NEW.depositAmount2 is null)) THEN
        select CONCAT(fieldNameList,'standardaddresses.depositAmount2~') into fieldNameList;
        IF(OLD.depositAmount2 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.depositAmount2 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.depositAmount2 is not null) then
            select CONCAT(oldValueList,OLD.depositAmount2,'~') into oldValueList;
        END IF;
        IF(NEW.depositAmount2 is not null) then
            select CONCAT(newValueList,NEW.depositAmount2,'~') into newValueList;
        END IF;
    END IF;
	

    IF ((OLD.residenceNotes <> NEW.residenceNotes) or (OLD.residenceNotes is null and NEW.residenceNotes is not null)
      or (OLD.residenceNotes is not null and NEW.residenceNotes is null)) THEN
        select CONCAT(fieldNameList,'standardaddresses.residenceNotes~') into fieldNameList;
        IF(OLD.residenceNotes is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.residenceNotes is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.residenceNotes is not null) then
            select CONCAT(oldValueList,OLD.residenceNotes,'~') into oldValueList;
        END IF;
        IF(NEW.residenceNotes is not null) then
            select CONCAT(newValueList,NEW.residenceNotes,'~') into newValueList;
        END IF;
    END IF;
 
CALL add_tblHistory (OLD.id,"standardaddresses", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;
