
package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.service.AddressManager;
import com.trilasoft.app.dao.AddressDao;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.AddressManager;
import com.trilasoft.app.dao.AddressDao;
import com.trilasoft.app.dao.AccrualProcessDao;
import com.trilasoft.app.model.Partner;



public class AddressManagerImpl  extends GenericManagerImpl<Partner, Long> implements AddressManager {

	
	AddressDao addressDao;
     
    public AddressManagerImpl(AddressDao addressDao) { 
        super(addressDao); 
        this.addressDao = addressDao; 
    } 
    
    public List search(String partnerCode,String originalCorpID,String accountLineBillingParam)    {
    	
    	return addressDao.search(partnerCode,originalCorpID,accountLineBillingParam);
    }
    
    public List searchBookerAddress(String shipNumberA){
    	return addressDao.searchBookerAddress(shipNumberA);
    }
    
    public List findOriginAddress(String sequenceNumber, String corpID){
    return addressDao.findOriginAddress(sequenceNumber, corpID);
	}

	public List partnerAddressDetails(String partnerCode, String corpID) {
		return addressDao.partnerAddressDetails(partnerCode, corpID);
	}
}
