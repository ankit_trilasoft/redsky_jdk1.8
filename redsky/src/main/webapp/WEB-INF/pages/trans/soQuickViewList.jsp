<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="min-width:300px;">
<tr valign="top"> 	
	<td align="left"><b>SO Quick View List</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="customerSO" class="table" requestURI="" id="serviceOrderList" >
	<display:column title="Ship" style="">
		<a href="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrderList.id}&decorator=popup&popup=true',950,350)">
			<c:out value="${serviceOrderList.ship}" />
		</a>
	</display:column>   
		<display:column property="lastName" title="Last&nbsp;Name" style=""/>  
		<display:column property="coordinator"  title="Coordinator" style=""/>  
		<display:column property="billToName"  title="Bill&nbsp;to&nbsp;Party" style="width:140px"/>   
	</display:table>
</div>
