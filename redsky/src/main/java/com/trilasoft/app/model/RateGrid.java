package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "rategrid")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class RateGrid extends BaseObject {

	private Long id;

	private String corpID;

	public String contractName;

	public String charge;

	public double quantity1;
	
	public double quantity2;

	public String rate1;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	
	public String buyRate;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("contractName", contractName)
				.append("charge", charge).append("quantity1", quantity1)
				.append("quantity2", quantity2).append("rate1", rate1)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("buyRate", buyRate).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RateGrid))
			return false;
		RateGrid castOther = (RateGrid) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(contractName, castOther.contractName)
				.append(charge, castOther.charge)
				.append(quantity1, castOther.quantity1)
				.append(quantity2, castOther.quantity2)
				.append(rate1, castOther.rate1)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(buyRate, castOther.buyRate).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(contractName).append(charge).append(quantity1)
				.append(quantity2).append(rate1).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(buyRate).toHashCode();
	}

	@Column(length = 50)
	public String getCharge() {
		return charge;
	}

	public void setCharge(String charge) {
		this.charge = charge;
	}

	@Column(length = 50)
	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	@Column(length = 50)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 50)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 50)
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 7)
	public double getQuantity1() {
		return quantity1;
	}

	public void setQuantity1(double quantity1) {
		this.quantity1 = quantity1;
	}

	@Column(length = 50)
	public String getRate1() {
		return rate1;
	}

	public void setRate1(String rate1) {
		this.rate1 = rate1;
	}

	@Column(length = 7)
	public double getQuantity2() {
		return quantity2;
	}

	public void setQuantity2(double quantity2) {
		this.quantity2 = quantity2;
	}
	
	@Column(length = 50)
	public String getBuyRate() {
		return buyRate;
	}

	public void setBuyRate(String buyRate) {
		this.buyRate = buyRate;
	}


}
