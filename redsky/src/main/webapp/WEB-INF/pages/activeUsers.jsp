<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="activeUsers.title"/></title>
    <meta name="heading" content="<fmt:message key='activeUsers.heading'/>"/>
<style type="text/css">

span.pagelinks {display:block;font-size:0.9em;margin-bottom:5px;margin-top:-20px;text-align:right;width:98%;} 
#otabs{margin-left: 40px; position:relative;margin-bottom: 12px;!margin-bottom:-10px;}
</style>
</head>

<%-- 
<body id="activeUsers"/>

<p><fmt:message key="activeUsers.message"/></p>

<div class="separator"></div>
<input type="button" onclick="location.href='mainMenu.html'" value="<fmt:message key="button.done"/>"/>
--%>
<div id="layer1" style="width:100%">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>View Active Users Detail&nbsp;</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<%--<table border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
<%--<tr>
<td><img style="curser:default" src="${pageContext.request.contextPath}/images/uleft.gif" /></td>
<td style="background-image:url(images/umiddle.gif); width:100%; background-repeat: repeat-x;"><span class="user-heading">View Active Users Detail&nbsp;</span><img src="${pageContext.request.contextPath}/images/arrow-bg.gif" /></td>
<td><img style="curser:default" src="${pageContext.request.contextPath}/images/uright.gif" /></td></tr>
<tr><td height="2"></td></tr>
</table>--%>
<display:table name="applicationScope.userNames" id="user" cellspacing="0" cellpadding="0" defaultsort="1" class="table" pagesize="50" requestURI="" style="margin-top:1px;!margin-top:0px; ">
<display:column style="width: 30%" titleKey="user.username" sortable="true">
<img style="curser:default" src="<c:url value="/images/user_micon.gif" />">
<c:out value="${user.username}" escapeXml="true"/>
</display:column>
<display:column titleKey="activeUsers.fullName" sortable="true" style="font-size: 9px;">
        <c:out value="${user.firstName} ${user.lastName}" escapeXml="true"/>

        <c:if test="${not empty user.email}">
        <a href="mailto:<c:out value="${user.email}"/>" style="text-decoration: none;">
            <img align="top" src="<c:url value="/images/email-user.gif"/>"   alt="<fmt:message key="icon.email"/>" class="icon"/><u>Email</u></a>
        </c:if>
    </display:column>
</display:table>
</div>