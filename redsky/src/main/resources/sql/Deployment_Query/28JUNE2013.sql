// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `contractReceived` DATETIME DEFAULT NULL AFTER `serviceCompleteDate`;

update t20VisionLogInfo set error='' where corpId='UTSI';

ALTER TABLE `redsky`.`t20VisionLogInfo` ADD COLUMN `serviceOrderId` BIGINT(20) UNSIGNED AFTER `error`;

ALTER TABLE `redsky`.`loss` ADD COLUMN `idNumber` VARCHAR(10) DEFAULT '' ;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `converted` BIT(1) DEFAULT b'0' AFTER `collection`;

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','destinationAgentExSO','Update','gverma',now(),'gverma',now());


Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','destinationSubAgentExSO','Update','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','originAgentExSO','Update','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','originSubAgentExSO','Update','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','networkAgentExSO','Update','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','bookingAgentExSO','Update','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','destinationAgentExSO','Create','gverma',now(),'gverma',now());


Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','destinationSubAgentExSO','Create','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','originAgentExSO','Create','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','originSubAgentExSO','Create','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','networkAgentExSO','Create','gverma',now(),'gverma',now());

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('TrackingStatus','bookingAgentExSO','Create','gverma',now(),'gverma',now());

ALTER TABLE `redsky`.`integrationloginfo` ADD COLUMN `corpId` VARCHAR(10) AFTER `ugwwBA`;

ALTER TABLE `redsky`.`ugwwactiontracker` ADD INDEX `Index_2`(`sonumber`);

Insert into 
networkdatafields(modelName,fieldName,transactionType,createdby,createdOn,updatedby,updatedOn)
values('CustomerFile','bookingAgentCode','Update','gverma',now(),'gverma',now());

update networkdatafields set type='CMM/DMM' where modelname='CustomerFile' and  FieldName='partnerEntitle';

 


