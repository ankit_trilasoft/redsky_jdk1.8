package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PrintDailyPackage;

public interface PrintDailyPackageManager extends GenericManager<PrintDailyPackage, Long> {
	public List<PrintDailyPackage> getByCorpId(String corpId,String tabId);
	public List<PrintDailyPackage> findChildByParentId(long id);
	public void deleteByParentId(long id);
	public List findAutoCompleterList(String jrxmlName,String corpId);
	public List<PrintDailyPackage>  searchPrintPackage(String formName,String corpId,String serviceType,String serviceMode,String military);
	public void deleteChild(String corpId);
}
