<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>   
<s:form id="partnerDetailsForm" method="post" > 
<c:set var="autocompleteDivId" value="<%= request.getParameter("autocompleteDivId")%>" />
<s:hidden name="autocompleteDivId" value="<%= request.getParameter("autocompleteDivId")%>" />
<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow-y:auto;height:215px;">
 <display:table name="partnerDetailsAutoComplete" class="table" id="partnerDetailsAutoComplete" style="width:100%;"> 
   	<display:column title="Code" >
   	<c:set var="d" value="'"/>
     <c:set var="des" value="${fn:replace(partnerDetailsAutoComplete.lastname,d, '~')}" />
   	<a onclick="copyPartnerDetails('${partnerDetailsAutoComplete.partnercode}','${des}','${partnerNameId}','${paertnerCodeId}','${autocompleteDivId}');">${partnerDetailsAutoComplete.partnercode}</a>
   	</display:column>
  	<display:column property="lastname" title="Name" ></display:column>
 <c:choose>
<c:when test="${(autocompleteDivId=='trackingOriginAgentNameDivId')||autocompleteDivId=='trackingDestinationAgentNameDivId'||autocompleteDivId=='trackingOriginSubAgentNameDivId'||autocompleteDivId=='trackingDestinationSubAgentNameDivId'||autocompleteDivId=='bookingAgentNameDiv'||autocompleteDivId=='originAgentNameDiv'||autocompleteDivId=='soBookingAgentNameDivId' }"> 
	<display:column property="terminalCountry" title="Country" ></display:column>	
	<display:column property="terminalState" title="State" ></display:column>
	<display:column property="terminalCity" title="City" ></display:column>
	</c:when>
	<c:otherwise>
	<display:column property="billingcountry" title="Country" ></display:column>	
	<display:column property="billingstate" title="State" ></display:column>
	<display:column property="billingcity" title="City" ></display:column>
	</c:otherwise>
	</c:choose>
 	<c:if test="${acctRefVisiable=='Yes'}"> 
 	<display:column title="Acct Ref #" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerDetailsAutoComplete.partnercode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    </display:column>
    </c:if>
 	<c:if test="${agentClassificationShow==true}">
 		<c:choose>
			<c:when test="${partnerDetailsAutoComplete.agentClassification=='Primary'}">
				<display:column title="Agent Classification" style="background:#98dfaf">
    				<c:out value="${partnerDetailsAutoComplete.agentClassification}" />
    			</display:column>
    		</c:when>
    		<c:when test="${partnerDetailsAutoComplete.agentClassification=='Secondary'}">
    			<display:column title="Agent Classification" style="background:#f19973">
    				<c:out value="${partnerDetailsAutoComplete.agentClassification}" />
    			</display:column>
    		</c:when>
    		<c:when test="${partnerDetailsAutoComplete.agentClassification=='COD'}">
    			<display:column title="Agent Classification" style="background:#e5e547">
    				<c:out value="${partnerDetailsAutoComplete.agentClassification}" />
    			</display:column>
    		</c:when>
    		<c:when test="${partnerDetailsAutoComplete.agentClassification=='OTHER'}">
    			<display:column title="Agent Classification" style="background:#ffcf56">
    				<c:out value="${partnerDetailsAutoComplete.agentClassification}" />
    			</display:column>
    		</c:when>
    		<c:otherwise>
    			<display:column title="Agent Classification" style="background:none">
    				<c:out value="${partnerDetailsAutoComplete.agentClassification}" />
    			</display:column>
    		</c:otherwise>
    	</c:choose>
	</c:if>
	<display:column  title="Type" style="width:90px; whitespace: nowrap;" >
	<c:choose>
	<c:when test="${not empty partnerDetailsAutoComplete.bookingAgentCode}">
	<c:out value="${partnerDetailsAutoComplete.partnerType}"></c:out>
	<img class="openpopup" style="text-align:right;vertical-align:top;" src="${pageContext.request.contextPath}/images/rslogo_small_14.png" />
	</c:when>
	<c:otherwise>
	<c:out value="${partnerDetailsAutoComplete.partnerType}"></c:out>
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column property="aliasName" title="Alias Name" ></display:column>
</display:table>
</div>
</td>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyDiv('${autocompleteDivId}','${partnerNameId}','${paertnerCodeId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
<c:if test="${not empty partnerDetailsAutoComplete}">
	<s:hidden id="partnerDetailsAutoCompleteListSize" value="false"/>
</c:if>
<c:if test="${empty partnerDetailsAutoComplete}">
	<s:hidden id="partnerDetailsAutoCompleteListSize" value="true"/>
</c:if>

</s:form>