<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    <link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
	<link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">

<style>

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-25px;
padding-left:40px;
text-align:right;
width:94%;
}
</style>

</head>
<!-- 
<c:set var="forms"> 
<s:form id="partnerListDetailForm" action='${empty param.popup?"editPartner.html":"editPartner.html?decorator=popup&popup=true"}' method="post" >  
<s:hidden name="id"/>
<s:submit cssClass="button" method="edit" key="button.viewDetail"/>
</s:form>
</c:set>
 -->
  

 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom:10px;" align="top"  key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   
<s:form id="partnerListForm" action="searchAgentPopup.html?decorator=popup&popup=true" method="post" >  
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />

<!-- 
action='${empty param.popup?"partnersPopup.html":"partnersPopup.html?decorator=popup&popup=true"}'

<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>



Enhansment after 27-02-2008 -->

<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="firstName" value="<%= request.getParameter("firstName")%>" />
<s:hidden name="firstName" value="<%= request.getParameter("firstName")%>" />
<c:set var="lastName" value="<%= request.getParameter("lastName")%>" />
<s:hidden name="lastName" value="<%= request.getParameter("lastName")%>" />

<c:set var="compDiv" value="<%= request.getParameter("compDiv")%>" />
<c:set var="flag" value="<%= request.getParameter("flag")%>" />

<s:hidden name="phone" value="<%= request.getParameter("phone")%>" />
<c:set var="phone" value="<%=request.getParameter("phone") %>"/>
<s:hidden name="email" value="<%= request.getParameter("email")%>" />
<c:set var="email" value="<%=request.getParameter("email") %>"/>

<!-- Enhansment after 27-02-2008 -->

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>

<div id="Layer1" style="width:100%">


				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">

<table class="table" style="width:99%;">
<thead>
<tr>
<th><fmt:message key="partner.partnerCode"/></th>
<th><fmt:message key="partner.name"/></th>
<th>Country Code</th>
<th>Country Name</th>
<th><fmt:message key="partner.billingState"/></th>

<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
</tr></thead>	
		<tbody>
		<tr>
			<td>
			    <s:textfield name="partnerCode" size="20" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partnerLastName" size="24" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partnerBillingCountryCode" size="15" cssClass="text medium"/>
			</td>
			<td>
			    <s:textfield name="partnerBillingCountry" size="24" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="partnerBillingState" size="24" cssClass="text medium"/>
			</td>
		</tr>
		<tr>
			<td colspan="4"></td>
			<td width="130px" style="border-left: hidden;">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

<div id="newmnav">   
 <ul>
 <c:choose>
 	<c:when test="${not empty param.popup && partnerType == 'PP'}">  
 		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setPrivate();"><span>Private<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAccount();"><span>Account</span></a></li>
		<li><a onclick="setAgent();"><span>Agents</span></a></li>
		 <!-- 
		  <li><a href="partnersPopup.html?partnerType=AC&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Account</span></a></li>
		  <li><a href="partnersPopup.html?partnerType=AG&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Agents</span></a></li>
		 --> 
	</c:when>
	<c:when test="${not empty param.popup &&partnerType == 'AC'}"> 
		<li><a onclick="setPrivate();"><span>Private</span></a></li> 
		<!--  <li><a href="partnersPopup.html?partnerType=PP&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Private</span></a></li>-->
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAccount();"><span>Account<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<li><a onclick="setAgent();"><span>Agents</span></a></li> 
		<!-- <li><a href="partnersPopup.html?partnerType=AG&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Agents</span></a></li>-->
	</c:when>
	<c:when test="${not empty param.popup &&partnerType == 'AG'}"> 
		<li><a onclick="setPrivate();"><span>Private</span></a></li>
		<li><a onclick="setAccount();"><span>Account</span></a></li>
		<!--  
		<li><a href="partnersPopup.html?partnerType=PP&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Private</span></a></li>
		<li><a href="partnersPopup.html?partnerType=AC&decorator=popup&popup=true&flag=${flag}&compDiv=${compDiv}"><span>Account</span></a></li>
		-->
		<li id="newmnav1" style="background:#FFF"><a class="current" onclick="setAgent();"><span>Agents<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</c:when>
</c:choose>
</ul>
</div><div class="spn" style="width:800px">&nbsp;</div><br>

<s:set name="agents" value="partners" scope="request"/>  
<display:table name="agents" class="table" requestURI="" id="partnerList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:100%" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>    
    <display:column titleKey="partner.name"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
     <c:if test="${(partnerType == 'AG')}">  
    <display:column title="Map" style="width:45px; text-align: center;"><a><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation('${partnerList.terminalAddress1}','${partnerList.terminalAddress2}','${partnerList.terminalCity}','${partnerList.terminalZip}','${partnerList.terminalState}','${partnerList.terminalCountry}');"/></a></display:column>  
    	<display:column title="Acct Ref #" sortable="true" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerList.partnerCode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    	</display:column>
    </c:if>
    <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:35px"/>
    <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:35px"/>
    <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:110px"/>
   
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
   
        
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>   
  
    <display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
<c:if test="${partnerType == 'PP'}">  
<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="addPP();" value="<fmt:message key="button.add"/>"/> 
</c:if>
</div>


<c:set var="isTrue" value="false" scope="session"/>


<!--

			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartner.html?id=${myMessageList.id} " >Addresses</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerDetail.html?id=${myMessageList.id} " >Details</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" align="center" class="content-tab"><a href="editPartnerRemarks.html?id=${myMessageList.id} " >Remarks</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
 -->
 
</s:form>

<script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['partnerListForm'].elements['partner.lastName'].value = "";
	document.forms['partnerListForm'].elements['partner.partnerCode'].value = "";
	document.forms['partnerListForm'].elements['partner.billingCountryCode'].value = "";
	document.forms['partnerListForm'].elements['partner.billingState'].value = "";
	document.forms['partnerListForm'].elements['partner.billingCountry'].value = "";
}

function setAccount(){
	document.forms['partnerListForm'].elements['partnerType'].value = "AC";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function setPrivate(){
	document.forms['partnerListForm'].elements['partnerType'].value = "PP";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function setAgent(){
	document.forms['partnerListForm'].elements['partnerType'].value = "AG";
	document.forms['partnerListForm'].elements['findFor'].value = "account";
	document.forms['partnerListForm'].submit();
}
function openOriginLocation(address1,address2,city,zip,state,country) {
	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address1+','+address2+','+city+','+zip+','+state+','+country);
}
</script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
	<script type="text/javascript" src="scripts/ajax.js"></script>
	<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
	
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>

<script>
this.onclick = function() {
   new Draggable('ajax_tooltipObj', 
                {starteffect: effectFunction('ajax_tooltipObj')});
   ajax_tooltipObj.style.cursor = "move";
}

function effectFunction(element)
{
   new Effect.Opacity(element, {from:0, to:1.0, duration:0.8});
}
</script>


<script type="text/javascript">   
  function addPP()
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  location.href='editPartnerFormPopup.html?firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&flag=${flag}&compDiv=${compDiv}&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';  	
  } 
  catch(e){}
  }  
  function PrivatePartyDetail(id)
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  location.href='viewPartner.html?id='+id+'&partnerType=${partnerType}&firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&mode=view&flag=${flag}&compDiv=${compDiv}&type=TT&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';
  } 
  catch(e){}
  }
  function partnerDetail(id)
  {
  try{
  var firstName= encodeURI("${firstName}");
  var lastName= encodeURI("${lastName}");
  //var id= encodeURI("${partnerList.id}");
  location.href='editPrivatePartner.html?id='+id+'&partnerType=${partnerType}&firstName='+firstName+'&lastName='+lastName+'&phone=${phone}&email=${email}&mode=view&flag=${flag}&compDiv=${compDiv}&type=TT&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}';
  } 
  catch(e){}
  }
</script>