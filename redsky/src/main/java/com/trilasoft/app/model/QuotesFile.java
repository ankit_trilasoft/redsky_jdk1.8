package com.trilasoft.app.model;


import org.appfuse.model.BaseObject;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="quotesfile")
public class QuotesFile extends BaseObject {

	         
	private String providedAs;           
	private String divideMultiply;           
	private String type;            
	private String billToCode;      
	private String providedBy;        
	private String firstName;
	private String lastName;
	private String perItem;       
	private String billToName;        
	private String contract;
	private String item;           
	private String description;          
	private String comment1;           		
	private String comment2;           		
	private String corpId;
	private Long id;
	private Long quotesCode;
	private Long sequenceNumber;
	private BigDecimal rate;		
	private BigDecimal amount;		
	private BigDecimal per;		
	private BigDecimal discount;		
	private BigDecimal Quantity;		
	private Date dated;
	private Date sentWb;		
	private int lineId;
	
	private int days;	  	
	private int ticket;	  	
	private int ridnum;	 	
	private int idnum; 	
	private int invnum;
	private String code;	        	
	private String sentFile;
	private BigDecimal entlAmt;
	private BigDecimal intTicket;
	private BigDecimal shortHaul;
	private BigDecimal estimate;
	private Date posted;		
	private Date sentAcct;		
	private Date billed;		
	private Date stgBegin;		
	private Date stgEnd;		
	private Date estDate;		
	private Date estSent;
	private String comment;
	private String keyer;
	private String sortGroup;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("providedAs", providedAs)
				.append("divideMultiply", divideMultiply).append("type", type)
				.append("billToCode", billToCode).append("providedBy",
						providedBy).append("firstName", firstName).append(
						"lastName", lastName).append("perItem", perItem)
				.append("billToName", billToName).append("contract", contract)
				.append("item", item).append("description", description)
				.append("comment1", comment1).append("comment2", comment2)
				.append("corpId", corpId).append("id", id).append("quotesCode",
						quotesCode).append("sequenceNumber", sequenceNumber)
				.append("rate", rate).append("amount", amount).append("per",
						per).append("discount", discount).append("Quantity",
						Quantity).append("dated", dated).append("sentWb",
						sentWb).append("lineId", lineId).append("days", days)
				.append("ticket", ticket).append("ridnum", ridnum).append(
						"idnum", idnum).append("invnum", invnum).append("code",
						code).append("sentFile", sentFile).append("entlAmt",
						entlAmt).append("intTicket", intTicket).append(
						"shortHaul", shortHaul).append("estimate", estimate)
				.append("posted", posted).append("sentAcct", sentAcct).append(
						"billed", billed).append("stgBegin", stgBegin).append(
						"stgEnd", stgEnd).append("estDate", estDate).append(
						"estSent", estSent).append("comment", comment).append(
						"keyer", keyer).append("sortGroup", sortGroup).append(
						"createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof QuotesFile))
			return false;
		QuotesFile castOther = (QuotesFile) other;
		return new EqualsBuilder().append(providedAs, castOther.providedAs)
				.append(divideMultiply, castOther.divideMultiply).append(type,
						castOther.type)
				.append(billToCode, castOther.billToCode).append(providedBy,
						castOther.providedBy).append(firstName,
						castOther.firstName).append(lastName,
						castOther.lastName).append(perItem, castOther.perItem)
				.append(billToName, castOther.billToName).append(contract,
						castOther.contract).append(item, castOther.item)
				.append(description, castOther.description).append(comment1,
						castOther.comment1)
				.append(comment2, castOther.comment2).append(corpId,
						castOther.corpId).append(id, castOther.id).append(
						quotesCode, castOther.quotesCode).append(
						sequenceNumber, castOther.sequenceNumber).append(rate,
						castOther.rate).append(amount, castOther.amount)
				.append(per, castOther.per)
				.append(discount, castOther.discount).append(Quantity,
						castOther.Quantity).append(dated, castOther.dated)
				.append(sentWb, castOther.sentWb).append(lineId,
						castOther.lineId).append(days, castOther.days).append(
						ticket, castOther.ticket).append(ridnum,
						castOther.ridnum).append(idnum, castOther.idnum)
				.append(invnum, castOther.invnum).append(code, castOther.code)
				.append(sentFile, castOther.sentFile).append(entlAmt,
						castOther.entlAmt).append(intTicket,
						castOther.intTicket).append(shortHaul,
						castOther.shortHaul).append(estimate,
						castOther.estimate).append(posted, castOther.posted)
				.append(sentAcct, castOther.sentAcct).append(billed,
						castOther.billed).append(stgBegin, castOther.stgBegin)
				.append(stgEnd, castOther.stgEnd).append(estDate,
						castOther.estDate).append(estSent, castOther.estSent)
				.append(comment, castOther.comment).append(keyer,
						castOther.keyer).append(sortGroup, castOther.sortGroup)
				.append(createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(providedAs).append(divideMultiply)
				.append(type).append(billToCode).append(providedBy).append(
						firstName).append(lastName).append(perItem).append(
						billToName).append(contract).append(item).append(
						description).append(comment1).append(comment2).append(
						corpId).append(id).append(quotesCode).append(
						sequenceNumber).append(rate).append(amount).append(per)
				.append(discount).append(Quantity).append(dated).append(sentWb)
				.append(lineId).append(days).append(ticket).append(ridnum)
				.append(idnum).append(invnum).append(code).append(sentFile)
				.append(entlAmt).append(intTicket).append(shortHaul).append(
						estimate).append(posted).append(sentAcct)
				.append(billed).append(stgBegin).append(stgEnd).append(estDate)
				.append(estSent).append(comment).append(keyer)
				.append(sortGroup).append(createdBy).append(updatedBy).append(
						createdOn).append(updatedOn).toHashCode();
	}
	@Column(nullable=false, precision=15, scale=5)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	@Column( length=8 )
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	@Column( length=40 )
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	@Column( length=6 )
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column( length=80 )
	public String getComment1() {
		return comment1;
	}
	public void setComment1(String comment1) {
		this.comment1 = comment1;
	}
	@Column( length=80 )
	public String getComment2() {
		return comment2;
	}
	public void setComment2(String comment2) {
		this.comment2 = comment2;
	}
	@Column( length=20 )
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column
	public Date getDated() {
		return dated;
	}
	public void setDated(Date dated) {
		this.dated = dated;
	}
	@Column
	public int getDays() {
		return days;
	}
	public void setDays(int days) {
		this.days = days;
	}
	@Column( length=45 )
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(nullable=false, precision=15, scale=5)
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Column( length=8 )
	public String getDivideMultiply() {
		return divideMultiply;
	}
	public void setDivideMultiply(String divideMultiply) {
		this.divideMultiply = divideMultiply;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getEntlAmt() {
		return entlAmt;
	}
	public void setEntlAmt(BigDecimal entlAmt) {
		this.entlAmt = entlAmt;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getEstimate() {
		return estimate;
	}
	
	public void setEstimate(BigDecimal estimate) {
		this.estimate = estimate;
	}
	@Column( length=15 )
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String name) {
		firstName = name;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column 
	public int getIdnum() {
		return idnum;
	}
	public void setIdnum(int idnum) {
		this.idnum = idnum;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getIntTicket() {
		return intTicket;
	}
	public void setIntTicket(BigDecimal intTicket) {
		this.intTicket = intTicket;
	}
	@Column
	public int getInvnum() {
		return invnum;
	}
	public void setInvnum(int invnum) {
		this.invnum = invnum;
	}
	@Column( length=40 )
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	@Column @GeneratedValue(strategy = GenerationType.AUTO)
	public int getLineId() {
		return lineId;
	}
	public void setLineId(int lineId) {
		this.lineId = lineId;
	}
	@Column( length=40 )
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String name) {
		lastName = name;
	}
	@Column(nullable=false, precision=15, scale=5)
	public BigDecimal getPer() {
		return per;
	}
	public void setPer(BigDecimal per) {
		this.per = per;
	}
	@Column ( length=15 )
	public String getPerItem() {
		return perItem;
	}
	public void setPerItem(String perItem) {
		this.perItem = perItem;
	}
	@Column ( length=8 )
	public String getProvidedBy() {
		return providedBy;
	}
	public void setProvidedBy(String providedBy) {
		this.providedBy = providedBy;
	}
	@Column ( length=1 )
	public String getProvidedAs() {
		return providedAs;
	}
	public void setProvidedAs(String providedAs) {
		this.providedAs = providedAs;
	}
	@Column(nullable=false, precision=15, scale=5)
	public BigDecimal getQuantity() {
		return Quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		Quantity = quantity;
	}
	@Column @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getQuotesCode() {
		return quotesCode;
	}
	public void setQuotesCode(Long quotesCode) {
		this.quotesCode = quotesCode;
	}
	@Column
	public int getRidnum() {
		return ridnum;
	}
	public void setRidnum(int ridnum) {
		this.ridnum = ridnum;
	}
	@Column(nullable=false, precision=15, scale=5)
	public BigDecimal getRate() {
		return rate;
	}
	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}
	@Column ( length=50 )
	public String getSentFile() {
		return sentFile;
	}
	public void setSentFile(String sent_file) {
		sentFile = sent_file;
	}
	@Column
	public Date getSentWb() {
		return sentWb;
	}
	public void setSentWb(Date sentWb) {
		this.sentWb = sentWb;
	}
	@Column
	public Long getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(Long sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(precision=15, scale=5)
	public BigDecimal getShortHaul() {
		return shortHaul;
	}
	
	public void setShortHaul(BigDecimal shortHaul) {
		this.shortHaul = shortHaul;
	}
	
	@Column
	public int getTicket() {
		return ticket;
	}
	public void setTicket(int ticket) {
		this.ticket = ticket;
	}
	@Column ( length=7 )
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column
	public Date getBilled() {
		return billed;
	}
	public void setBilled(Date billed) {
		this.billed = billed;
	}
	@Column (length=15 )
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getEstDate() {
		return estDate;
	}
	public void setEstDate(Date estDate) {
		this.estDate = estDate;
	}
	@Column
	public Date getEstSent() {
		return estSent;
	}
	public void setEstSent(Date estSent) {
		this.estSent = estSent;
	}
	@Column
	public Date getPosted() {
		return posted;
	}
	public void setPosted(Date posted) {
		this.posted = posted;
	}
	@Column
	public Date getSentAcct() {
		return sentAcct;
	}
	public void setSentAcct(Date sentAcct) {
		this.sentAcct = sentAcct;
	}
	@Column
	public Date getStgBegin() {
		return stgBegin;
	}
	public void setStgBegin(Date stgBegin) {
		this.stgBegin = stgBegin;
	}
	@Column
	public Date getStgEnd() {
		return stgEnd;
	}
	public void setStgEnd(Date stgEnd) {
		this.stgEnd = stgEnd;
	}
	@Column (length=45 )
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Column (length=15 )
	public String getKeyer() {
		return keyer;
	}
	public void setKeyer(String keyer) {
		this.keyer = keyer;
	}
	@Column (length=2 )
	public String getSortGroup() {
		return sortGroup;
	}
	public void setSortGroup(String sortGroup) {
		this.sortGroup = sortGroup;
	}
	@Column (length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column (length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

}


	
	
	