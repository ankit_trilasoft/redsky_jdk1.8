<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title>Default Account Line Upload</title>   
    <meta name="heading" content="Default Account Line Upload"/>  
    <style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<style type="text/css">
/* collapse */
div.error, span.error, li.error, div.message {
width:400px;
padding:5px;
}
</style>
</head> 
 <DIV ID="layerH">
 <table border="0" width="100%" align="middle" style="margin:0px;">
 <tr>
 <td align="center">
	<font size="2" color="#1666C9"><b><blink>Uploading...</blink></b></font>
	</td>
	</tr>
	</table>
</DIV>
<c:if test="${hitFlag == '1'}">
	<div class="message">${msgDisplay}</div>
</c:if>
<s:form id="fileUploadForm" name="fileUploadForm" enctype="multipart/form-data" method="post" validate="true">
<s:hidden name="hitFlag" value="${hitFlag}"/>
<s:hidden name="btnType" value="<%=request.getParameter("buttonType") %>"/>
<s:hidden name="uploadButton1" value="<%=request.getParameter("uploadButton") %>"/>
<c:set var="uploadButton1"  value="<%=request.getParameter("uploadButton") %>"/>
<s:hidden name="fileExt" />
 <div id="Layer3" style="width:100%; margin:0px; padding:0px">
 <div id="newmnav">
 <ul>
 <c:choose>
 <c:when test="${empty hitFlag}">
 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Accounting&nbsp;Template&nbsp;File&nbsp;Upload<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
 </c:when>
 <c:otherwise>
 <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Duplicate&nbsp;Accounting&nbsp;Template&nbsp;</span></a></li>
 </c:otherwise>
 </c:choose>
</ul> 
</div>
		<div class="spn spnSF">&nbsp;</div> 
        <div id="content" align="center">
        <div id="liquid-round">
        <div class="top" style="margin-top:0px;!margin-top:5px;"><span></span></div>
        <div class="center-content">
        <c:if test="${empty hitFlag}"> 
				<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						<tr>
							<td align="right" class="listwhitetext" width="30px"></td>
							<td align="right" class="listwhitetext">File To Upload <img src="${pageContext.request.contextPath}/images/external.png" style="cursor:auto;"/></td>
							<td colspan="4" width="200px"> <s:file name="file" size="25"/></td>
							<td align="right" class="listwhitetext strbg" width="130px"><a href="${pageContext.request.contextPath}/images/SampleUploadFile_DefaultAccountline.xls" >Sample Template </a></td>
						</tr> 
						<tr><td height="20px"></td></tr>  
						<tr>
						<td align="right" class="listwhitetext" width="30px"></td>						
						<td align="right" colspan="2">
						<input type="button" class="cssbutton" name="uploadFile" value="Upload File" cssStyle="width:100px; height:27px" onclick="return ValidateUpload();"/>
                                     
                        </td>
                        <td>
                        <s:reset cssClass="cssbutton1" name="reset"  cssStyle="width:55px; height:27px" key="Reset"/></td>                       
                        </tr>
                        <tr><td height="10px"></td></tr>  
					</tbody>
				</table>
			</c:if>
			<c:if test="${hitFlag == '1'}">
	    	<c:if test="${not empty defaultAccountlineList}">
	    	<table cellspacing="1" cellpadding="2" border="0" style="margin:0px; ">
					<tbody>  
						
						<tr>													
							<td align="right" colspan="2">
							<s:submit cssClass="cssbuttonA"  key="Delete" name="delete"  cssStyle="width:55px;" onclick="return DeleteDuplicateData();" />
                        	</td>
                        	<td>
                        		<input type="button" class="cssbutton1" value="Cancel" onclick="moveforParentPage();"/>
                        	</td>                       
                        </tr>
						<tr><td height="0px"></td></tr> 
						<tr>
							<display:table name="defaultAccountlineList" class="table" requestURI="" id="defaultAccountlineList" defaultsort="" >
							<display:column    title="#" paramId="id" paramProperty="id" headerClass="containeralign" style="text-align:right;">
  								<c:out value='${defaultAccountlineList_rowNum}'/>
  							</display:column>   
  	 						<display:column property="contract"  title="Contract"  />
     						<display:column property="chargeCode"  title="ChargeCode"  /> 
     						<display:column property="billToName"  title="Bill To Name"  />
     						<display:column property="vendorName"  title="Vendor&nbsp;Name"  />
     						<display:column property="rate"  title="Rate"  />
    						<display:column title="Check" >
								<input type="checkbox" name="deleteTemplateId" id="deleteTemplateId${defaultAccountlineList.id}" value="${defaultAccountlineList.id}" checked="checked" onclick="return false" /> 
							</display:column>  
							</display:table> 
						</tr>
					</tbody>
					</table>	
 			</c:if>
		</c:if>     
	    </div>
       <div class="bottom-header"><span></span></div>
       </div>
       </div>
       </div>  
</s:form>
<script>
function progressBar(tar){
showOrHide(tar);
} 

function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
   try{
   document.getElementById("successMessages").style.visibility='hidden'; 
   }catch (e) { 
   }
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}
function ValidateUpload(){
	var fileValue = document.forms['fileUploadForm'].elements['file'].value; 
	if(fileValue==null || fileValue==''){
		alert("Please select file to process.");
		return false;
	}
			
	else{ 
		var extension=fileValue.substring(fileValue.indexOf('.')+1, fileValue.length);
			document.forms['fileUploadForm'].elements['fileExt'].value = extension;
			if(extension=='xls' || extension=='xlsx'){
				submitForUpload();
				progressBar('1');
				return true;
       		}else{
         		alert( "Please select a valid file format.xls and .xlsx are only allowed" ) ;
         		return false;
         	}
	}
} 


var basestring="window"+new Date().getTime();

function submitForUpload(){
	var btn =document.forms['fileUploadForm'].elements['uploadFile'].value;
	document.forms['fileUploadForm'].elements['uploadFile'].disabled=true;
	document.forms['fileUploadForm'].elements['reset'].disabled=true;
	var url="";
	if(btn=='Upload File'){
		 url = "uploadFileDefaultAccountline.html?&uploadButton=yes&decorator=popup&popup=true";
	}
	if(btn=='uploadReplace'){
		 url = "uploadDefaultAccountlineReplaceFile.html?&uploadButton=yes&decorator=popup&popup=true";
	}
	document.forms['fileUploadForm'].action = url;
	document.forms['fileUploadForm'].submit();
}
</script>
<script type="text/javascript">  
 showOrHide(0);
</script>
<script type="text/javascript">
function DeleteDuplicateData(){
		 var url1 = "uploadFileDefaultAccountline.html?decorator=popup&popup=true";
		document.forms['fileUploadForm'].action = url1;
		document.forms['fileUploadForm'].submit();
}



function moveforParentPage(){ 
	parent.window.opener.document.location.reload();
	window.close(); 
}
<c:if test="${empty defaultAccountlineList}">
 <c:if test="${hitFlag == '1'}"> 
         parent.window.opener.document.location.reload();
         window.close();
       </c:if> 
   </c:if> 
</script>
