<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="refMasterList.title" /></title>
<meta name="heading"
	content="<fmt:message key='refMasterList.heading'/>" />
</head>


<c:set var="buttons">
	<input type="button" style="margin-right: 5px"
		onclick="location.href='<c:url value="/editRefMasterwh.html"/>'"
		value="<fmt:message key="button.add"/>" />

	<input type="button"
		onclick="location.href='<c:url value="/mainMenu.html"/>'"
		value="<fmt:message key="button.done"/>" />



</c:set>
<s:form id="searchForm" action="searchRefMasters" method="post">
	<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
				<td id="searchLabelCenter" width="90">
				<div align="center" class="content-head">Search</div>
				</td>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<table class="table" width="90%">
		<thead>
			<tr>
				<th><fmt:message key="refMaster.parameter" /></th>

				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield name="refMaster.parameter" required="true"
					cssClass="text medium" /></td>

				<td><s:submit cssClass="button" method="search"
					key="button.search" /></td>
			</tr>
		</tbody>
	</table>

	<c:out value="${searchresults}" escapeXml="false" />



	<s:set name="refMasters" value="refMasters" scope="request" />
	<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
				<td id="searchLabelCenter" width="90">
				<div align="center" class="content-head">List</div>
				</td>
				<td width="7"><img width="7" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	<display:table name="refMasters" class="table" requestURI=""
		id="refMasterList" export="true" pagesize="10">
		
		<display:column property="code" sortable="true"
			titleKey="refMaster.code" url="/editRefMasterwh.html" paramId="id" paramProperty="id"/>
		<display:column property="description" sortable="true"
			titleKey="refMaster.description" />
		<display:column property="bucket" sortable="true"
			titleKey="refMaster.bucket" />
		<display:column property="bucket2" sortable="true"
			titleKey="refMaster.bucket2" />
		<display:column property="parameter" sortable="true"
			titleKey="refMaster.parameter" />
		<display:column property="fieldLength" sortable="true"
			titleKey="refMaster.fieldLength" />
		<display:column property="number" sortable="true"
			titleKey="refMaster.number" />


		<display:setProperty name="paging.banner.item_name" value="refMaster" />
		<display:setProperty name="paging.banner.items_name" value="refMasters" />
		<display:setProperty name="export.excel.filename"
			value="RefMaster List.xls" />
		<display:setProperty name="export.csv.filename"
			value="RefMaster List.csv" />
		<display:setProperty name="export.pdf.filename"
			value="RefMaster List.pdf" />
	</display:table>
	<c:set var="buttons">
		<input type="button" style="margin-right: 5px"
			onclick="location.href='<c:url value="/editRefMasterwh.html"/>'"
			value="<fmt:message key="button.add"/>" />

		<input type="button"
			onclick="location.href='<c:url value="/mainMenu.html"/>'"
			value="<fmt:message key="button.done"/>" />
	</c:set>
	<c:out value="${buttons}" escapeXml="false" />
</s:form>
<script type="text/javascript"> 
    highlightTableRows("refMasterList"); 
</script>



