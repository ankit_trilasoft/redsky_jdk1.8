<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<html>
<head>
<script language="JavaScript">

function printPage(){ 
  document.getElementById("buttonList").style.display="none"; 
  window.print();  
  document.getElementById("buttonList").style.display="block"; 
}

</script>
 <decorator:head/>
</head>
<decorator:getProperty property="body.id" writeEntireProperty="true"/>
<decorator:getProperty property="body.class" writeEntireProperty="true"/>

<div style="background:transparent url(images/close.gif);background-repeat:no-repeat; width:20px; height:20px;cursor:pointer; position:absolute;top:-10px;!top:10px; right:5px;" onclick="javascript:parent.hideTerms();">

</div>	
<s:form id="dataExtractSupport" name="dataExtractSupport" method="post"  action="" validate="true">
<table border="0" width="100%">
<tr>
<td align="center">
<B><FONT SIZE="3" COLOR="#5C5C5C" style="text-align:justify;font-family:Arial, Helvetica, sans-serif;font-size:16px;" >
Rate Matrix Usage Agreement
</FONT></B>
</td>
</tr>
</table>
<div id="layer1" style="width: 100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style=""><span></span></div>
   <div class="center-content">
<table border="0" cellpadding="2" cellspacing="0" style=" " align="center">
<tr> 
<td align="left" style="" class="" colspan="2">${rateGridRules}
</td>
</tr>
<tr height="3px"></tr>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table><tr> 
<td><div id="buttonList"> 
<input type="button" class="cssbutton" style="width:52px; height:25px" value="Print" onClick="printPage()"> 
 <input type="button" class="cssbutton" style="width:55px; height:25px" value="Close" onclick="javascript:parent.hideTerms();" />
</div>
</td> 
<td>

</td>
</tr></table>
</div> 
</s:form>

</html>
