/**
 * Implementation of WorkTicketManager interface.
 * Set the Dao for communication with the data layer.
 * @param 		dao
 * This class represents the basic "WorkTicket" object in Redsky that allows for WorkTicket  of Shipment.
 * @Class Name	WorkTicketManagerImpl
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.WorkTicketDao;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.WorkTicketManager;

public class WorkTicketManagerImpl extends GenericManagerImpl<WorkTicket, Long> implements WorkTicketManager { 
	WorkTicketDao workTicketDao;
	public WorkTicketManagerImpl(WorkTicketDao workTicketDao) {
		super(workTicketDao);
		this.workTicketDao = workTicketDao;
	}
	public Long findMaximum(String corpId) {
		return workTicketDao.findMaximum(corpId);
	}
	public List findMaximumship(String shipNumber) {
		return workTicketDao.findMaximumship(shipNumber);
	}
	public List findMaximumId() {
		return workTicketDao.findMaximumId();
	}
	public List<WorkTicket> findByLastName(String lastName) {
		return workTicketDao.findByLastName(lastName);
	}
	public List<WorkTicket> findByTicket(Long ticket, String lastName, String firstName, String shipNumber, String warehouse, String service, Date date1, Date date2) {
		return workTicketDao.findByTicket(ticket, lastName, firstName, shipNumber, warehouse, service, date1, date2);
	}
	public Set<WorkTicket> findByCustomerTicket(Long ticket, String sequenceNumber) {
		return workTicketDao.findByCustomerTicket(ticket, sequenceNumber);

	}
	public List getWarehouse(String shipNumber) {
		return workTicketDao.getWarehouse(shipNumber);
	}
	public List findContainer(String containerNumber, String shipNumber) {
		return workTicketDao.findContainer(containerNumber, shipNumber);
	}
	public List findBillComlete(String shipNumber) {
		return workTicketDao.findBillComlete(shipNumber);
	}
	public List findAllTickets() {
		return workTicketDao.findAllTickets();
	}
	public String findTotalEstimatedCartons(Long ticket) {
		return workTicketDao.findTotalEstimatedCartons(ticket);
	}
	public List getStorageList(String shipNumber) {
		return workTicketDao.getStorageList(shipNumber);
	}
	public List findBillingPayMethod(String shipNumber) {
		return workTicketDao.findBillingPayMethod(shipNumber);
	}
	public List findBillingPayMethods(String payMethod) {
		return workTicketDao.findBillingPayMethods(payMethod);
	}
	public List getDailyOperationalLimit(Date date1, Date date2, String corpId) {
		return workTicketDao.getDailyOperationalLimit(date1, date2, corpId);
	}
	public Boolean checkDate1(String warehouse, String service, Date date1, Long id, String corpId, Double estimatedVolume, Double estimatedWeight, Date date2){
		return workTicketDao.checkDate1(warehouse, service, date1, id, corpId, estimatedVolume, estimatedWeight, date2);
	}
	public Boolean getDateDiff(Date date1, Long minServiceDay) {
		return workTicketDao.getDateDiff(date1, minServiceDay);
	}
	public String getDailyOpsLimit(Date date1, Date date2, Long dailyCount, Long dailyCountPC, String sessionCorpID, String hubID, Double estimatedWeight, Double estimatedCubicFeet, Double dailyMaxEstWt, Double dailyMaxEstVol, Long id){
		return workTicketDao.getDailyOpsLimit(date1, date2, dailyCount, dailyCountPC, sessionCorpID, hubID, estimatedWeight, estimatedCubicFeet, dailyMaxEstWt, dailyMaxEstVol, id);
	}
	public Boolean getCheckedWareHouse(String wareHouse, String corpId){
		return workTicketDao.getCheckedWareHouse(wareHouse, corpId);
	}
	public List getWorkPlanList(Date fromDate, Date toDate, String hubID,String wHouse,String wareHouseUtilization, String corpId) {
		return workTicketDao.getWorkPlanList(fromDate, toDate, hubID,wHouse, wareHouseUtilization, corpId);
	}
	public List findMaximumTicket(String shipNm){
		return workTicketDao.findMaximumTicket(shipNm);
	}
	public List findMinimumTicket(String shipNm){
		return workTicketDao.findMinimumTicket(shipNm);
	}
	public List findCountTicket(String shipNm){
		return workTicketDao.findCountTicket(shipNm);
	}
	public List goSOChild(Long serviceOrderId, String corpID, Long id){
		return workTicketDao.goSOChild(serviceOrderId, corpID, id);
	}
	public List goNextSOChild(Long serviceOrderId, String corpID, Long ticket){
		return workTicketDao.goNextSOChild(serviceOrderId, corpID, ticket);
	}
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long ticket){
		return workTicketDao.goPrevSOChild(serviceOrderId, corpID, ticket);
	}
	public List<WorkTicket> getWorkTicketByTicekt(Long ticket) {
		return workTicketDao.getWorkTicketByTicekt(ticket);
	}
	public void updateServiceOrderInland(String vendorCode, Long id, String sessionCorpID) {
		workTicketDao.updateServiceOrderInland(vendorCode, id, sessionCorpID);
	}
	public void updateTrackingStatusBeginLoad(Date date1, String shipNumber, String sessionCorpID){
		workTicketDao.updateTrackingStatusBeginLoad(date1, shipNumber,sessionCorpID);
	}
	public void updateTrackingStatusLoadA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus, String loginUser)
	{
		workTicketDao.updateTrackingStatusLoadA(date1, shipNumber,sessionCorpID,updateSoStatus, loginUser);
	}
	public void updateBillingStorageOut(Date date1, String shipNumber, String sessionCorpID)
	{
		workTicketDao.updateBillingStorageOut(date1, shipNumber, sessionCorpID);
	}
	public List countReleaseStorageDate(String shipNumber)
	{
		return workTicketDao.countReleaseStorageDate(shipNumber);
	}
	public void updateTrackingStatusDeliveryA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus)
	{
		workTicketDao.updateTrackingStatusDeliveryA(date1, shipNumber, sessionCorpID,updateSoStatus);
	}
	public void updateStorageReleasedDate(String shipNumber, String sessionCorpID, String updatedBy){
		workTicketDao.updateStorageReleasedDate(shipNumber, sessionCorpID,updatedBy);
	}
	public void updateTrackingStatusDeliveryShipper(Date date1, String shipNumber, String sessionCorpID){
		workTicketDao.updateTrackingStatusDeliveryShipper(date1, shipNumber, sessionCorpID);
	}
	public List checkCompanyDivisionAgentCode(String corpID){
		return workTicketDao.checkCompanyDivisionAgentCode(corpID);
	}
	public void updateTrackingStatusLeftWhon(Date date1, String shipNumber, String sessionCorpID)
	{
		workTicketDao.updateTrackingStatusLeftWhon(date1, shipNumber, sessionCorpID);
	}
	public void updateTrackingStatusPackA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus){
		workTicketDao.updateTrackingStatusPackA(date1, shipNumber, sessionCorpID,updateSoStatus);
	}
	public List locationReleaseStorageDate(String shipNumber, String sessionCorpID)
	{
		return workTicketDao.locationReleaseStorageDate(shipNumber, sessionCorpID);
	}
	public String getStateDesc(String check) {
		return workTicketDao.getStateDesc(check);
	}
	public List getStateDescList(String originCountry) {
		return workTicketDao.getStateDescList(originCountry);
	}
	public List getDispathMapList(Date date1, Date date2, String sessionCorpID, Boolean originMap,Boolean destinMap,String hubId) {
		return workTicketDao.getDispathMapList(date1, date2, sessionCorpID, originMap, destinMap, hubId);
	}
	public void updateBillingGSTAccess1Service(Date date1, String shipNumber, String sessionCorpID){
		workTicketDao.updateBillingGSTAccess1Service(date1, shipNumber, sessionCorpID);
	}
	public void updateBillingGSTAccess2Service(Date date1, String shipNumber, String sessionCorpID){
		workTicketDao.updateBillingGSTAccess2Service(date1, shipNumber, sessionCorpID);
	}
	public void updateBillingGSTAccess3Service(Date date1, String shipNumber, String sessionCorpID){
		workTicketDao.updateBillingGSTAccess3Service(date1, shipNumber, sessionCorpID);
	}
	public OperationsHubLimits getOperationsHub(String warehouse, String sessionCorpID) {
		return workTicketDao.getOperationsHub(warehouse, sessionCorpID);
	}
	public List getHubIdList(String sessionCorpID, String hubId){
		return workTicketDao.getHubIdList(sessionCorpID, hubId);
	}
	public List getOpsList(Date fromDate, String hubID, String corpID) {
		return workTicketDao.getOpsList(fromDate, hubID, corpID);
	}
	public List actualizedTicketWareHouse(String wareHouse, String shipNumber, String sessionCorpID){
	   return workTicketDao.actualizedTicketWareHouse(wareHouse, shipNumber, sessionCorpID);
	}
	public void billingWareHouse(String wareHouse, String shipNumber, String sessionCorpID){
		workTicketDao.billingWareHouse(wareHouse, shipNumber, sessionCorpID);
	}
	public List findWorkTicketID(String ticket,String corpId){
		return workTicketDao.findWorkTicketID(ticket, corpId);
	}
	public List findWorkTicketStorageList(Date date1, Date date2,String service, String warehouse, String sessionCorpID) {
		return workTicketDao.findWorkTicketStorageList(date1, date2,service, warehouse, sessionCorpID);
	}
	public String findAccountingHold(String billToCode) {
		return workTicketDao.findAccountingHold(billToCode);
	}
	
	public List findStorageUnit(Long ticket, String shipNumber){
		return workTicketDao.findStorageUnit(ticket, shipNumber);
	}
	public void updateStoForHandout(String handout, String ids, Long hoTicket) {
		workTicketDao.updateStoForHandout(handout, ids, hoTicket);		
	}
	public List findWorkTicketService(Long ticket, String sessionCorpID){
		return workTicketDao.findWorkTicketService(ticket, sessionCorpID);
	}
	public void updateTicketStatus(Long ticket, String sessionCorpID){
		workTicketDao.updateTicketStatus(ticket, sessionCorpID);
	}
	public String getWorkTicketIdByStorageTicket(Long ticket, String sessionCorpID){
		return workTicketDao.getWorkTicketIdByStorageTicket(ticket, sessionCorpID);
	}
	public void changeHandoutAccess(Long ticket , String sessionCorpID){
		workTicketDao.changeHandoutAccess(ticket,sessionCorpID);		
	}
	public List totalHandOutVolumePieces(Long ticket, String ids, String sessionCorpID) {
		return workTicketDao.totalHandOutVolumePieces(ticket, ids, sessionCorpID);
	}
	public List handoutWeightandVolume(Long ticket, String shipNumber,String sessionCorpID) {
		return workTicketDao.handoutWeightandVolume(ticket, shipNumber, sessionCorpID);
	}
	public String getAddress(Long id, String sessionCorpID,String addType) {
		return workTicketDao.getAddress(id, sessionCorpID, addType);
	}
	public String getAddressDescription(Long cid, String sessionCorpID,String descType){
		return workTicketDao.getAddressDescription(cid, sessionCorpID, descType);
	}
	public String getStandardAddressDescription(Long StdID,String sessionCorpID){
		return workTicketDao.getStandardAddressDescription(StdID,sessionCorpID);
	}
	public List getCrewNameByContractor(Long ticket, String sessionCorpID){
		return workTicketDao.getCrewNameByContractor(ticket, sessionCorpID);
	}
	public List getWorkTicketToTransfer(String shipNumber, String sessionCorpID) {
		return workTicketDao.getWorkTicketToTransfer(shipNumber, sessionCorpID);
	}
	public List ticketTransfer(String shipNumber, String sessionCorpID,Long ticket, Long id) {
		return workTicketDao.ticketTransfer(shipNumber, sessionCorpID,ticket, id);
	}
	public List findWorkTicketResourceList(Long id){
		return workTicketDao.findWorkTicketResourceList(id);
	}
	public void updateTicketStatusPending(String tickets, String sessionCorpID,String targetActual){
		workTicketDao.updateTicketStatusPending(tickets, sessionCorpID,targetActual);
	}
	public List findServiceDesc(String code,String sessionCorpID){
		return workTicketDao.findServiceDesc(code, sessionCorpID);
	}
	public String findWorkTicketReview(String shipnum,String sessionCorpID){
		return workTicketDao.findWorkTicketReview(shipnum, sessionCorpID);
	}
	public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID){
		return workTicketDao.findFullCityAdd(shipNumberForCity,ocityForCity,sessionCorpID);
	}
	public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID){
		return workTicketDao.findFullDestinationCityAdd(shipNumberForCity,dcityForCity,sessionCorpID);
	}
	public List getLabel4Value(String ticketWarehouse,String sessionCorpID){
		return workTicketDao.getLabel4Value(ticketWarehouse,sessionCorpID);
	}
	public void updateResourceMiscellaneous(String tktService,String userName,String sequenceNumber,String sessionCorpID){
		workTicketDao.updateResourceMiscellaneous(tktService,userName,sequenceNumber,sessionCorpID);
	}
	
	public String findTargetActualByTicketNum(Long ticket,String sessionCorpID){
		return workTicketDao.findTargetActualByTicketNum(ticket,sessionCorpID);
	}
	public List getAllRecord(Long id, String sessionCorpID){
		return workTicketDao.getAllRecord(id,sessionCorpID);
	}
	public void updateBillToAuthority( String billToAuthority, Date authUpdated,Long id){
		     workTicketDao.updateBillToAuthority( billToAuthority,  authUpdated, id);
	}
	public Long findRemoteWorkTicket(String ticket){
		return workTicketDao.findRemoteWorkTicket(ticket);
	}
	public List getWorkTicketList(String serviceType,String wareHouse,Date date, String sessionCorpID,String mode,String military,String job,String billToCode){
		return workTicketDao.getWorkTicketList(serviceType, wareHouse, date, sessionCorpID,mode,military,job,billToCode);
	}
	public List findCrewFirstLastName(Long ticket,String sessionCorpID){
		return workTicketDao.findCrewFirstLastName(ticket, sessionCorpID);
	}
	public String findVendorCodeWithName(String code, String sessionCorpID){
		return workTicketDao.findVendorCodeWithName(code, sessionCorpID);
	}
	public String findCoordinatorByShipNum(String shipNumber,String sessionCorpID){
		return workTicketDao.findCoordinatorByShipNum(shipNumber, sessionCorpID);
	}
	public List getdriverTicket(Date fromDate, Date toDate,String userParentAgent, String corpId){
		return workTicketDao.getdriverTicket(fromDate,toDate,userParentAgent,corpId);
	}
	public String findWorkTicketIdList(String shipNumber, String sessionCorpID) {
		return workTicketDao.findWorkTicketIdList(shipNumber,sessionCorpID);
	}
	public String getHubValueWorkPlanList(String hubID, String corpId){
		return workTicketDao.getHubValueWorkPlanList(hubID,corpId);
	}
	public String getCustomsTicketList(Long StdID, String corpId){
		return workTicketDao.getCustomsTicketList(StdID,corpId);
	}
	public List findDailySheetList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findDailySheetList(wRHouse,dailySheetDate,sessionCorpID);
	}
	public String findCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findCrewNoList(wRHouse,dailySheetDate,sessionCorpID);	
	}
	public List findAvailableCrews(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findAvailableCrews(wRHouse,dailySheetDate,sessionCorpID);
	}
	public List searchAvailableTrucks(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.searchAvailableTrucks(wRHouse,dailySheetDate,sessionCorpID);	
	}
	public List findOriginAddWorkTicket(Long oId,String sessionCorpID){
		return workTicketDao.findOriginAddWorkTicket(oId,sessionCorpID);	
	}
	public String findOMDCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findOMDCrewNoList(wRHouse,dailySheetDate,sessionCorpID);
	}
	public void updateDailySheetNote(String wRHouse, Date dailySheetDate,String sessionCorpID,String wDailySheetNotes){
		workTicketDao.updateDailySheetNote(wRHouse,dailySheetDate,sessionCorpID,wDailySheetNotes);
	}
	public String findDailySheetNotes(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findDailySheetNotes(wRHouse,dailySheetDate,sessionCorpID);
	}
	public List findAvailableCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		return workTicketDao.findAvailableCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);
	}
	public List findAssignedCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		return workTicketDao.findAssignedCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);
	}
	public List findAbsentCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findAbsentCrewsForEditing(wRHouse,dailySheetDate,sessionCorpID);
	}
	public Long findUserDetailsFromCrew(String asgCrew,String sessionCorpID){
		return workTicketDao.findUserDetailsFromCrew(asgCrew,sessionCorpID);
	}
	public Long findUserDetailsFromTS(String crewValue,Date dailySheetDate,String sessionCorpID,String dSTicket){
		return workTicketDao.findUserDetailsFromTS(crewValue,dailySheetDate,sessionCorpID,dSTicket);
	}
	public List findAvailableTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String dSTicket){
		return workTicketDao.findAvailableTruckForEditing(wRHouse,dailySheetDate,sessionCorpID,dSTicket);
	}
	public List findAssignedTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		return workTicketDao.findAssignedTruckForEditing(wRHouse,dailySheetDate,sessionCorpID,wTicket);
	}
	public List findTruckDetailsFromTO(String truckNo,Date dailySheetDate,String sessionCorpID,String dSTicket){
		return workTicketDao.findTruckDetailsFromTO(truckNo,dailySheetDate,sessionCorpID,dSTicket);
	}
	public Long findTruckDetailsFromTruck(String truckNo,String sessionCorpID){
		return workTicketDao.findTruckDetailsFromTruck(truckNo,sessionCorpID);
	}
	public Map<String, List> getDSDispatch(String dailySheetwRHouse,Date dailySheetCalenderDate,String sessionCorpID){
		return workTicketDao.getDSDispatch(dailySheetwRHouse,dailySheetCalenderDate,sessionCorpID);
	}

	public void updateTrackingStatusSitDestinationIn(Date date1, Long id, String sessionCorpID) {
		workTicketDao.updateTrackingStatusSitDestinationIn(date1, id, sessionCorpID);		
	}
	public Long findWorkTicketIdByTicket(String dSTicket,String sessionCorpID){
		return workTicketDao.findWorkTicketIdByTicket(dSTicket,sessionCorpID);
	}
	public List findScheduleResourceId(String dSTicket,String wRHouse, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findScheduleResourceId(dSTicket,wRHouse,dailySheetDate,sessionCorpID);
	}
	public String findAbsentCrew(String asgCrew, Date dailySheetDate,String sessionCorpID){
		return workTicketDao.findAbsentCrew(asgCrew,dailySheetDate,sessionCorpID);
	}
	public AccessInfo getAccessinfo(String sessionCorpID, Long sid){
		return workTicketDao.getAccessinfo(sessionCorpID,sid);
	}
	public String findcrewNoAndCrewThreshHold(Date date1, Date date2,String wareHouse,String sessionCorpID,BigDecimal noOfCrewFromPeople,String oldTicketNumber){
		return workTicketDao.findcrewNoAndCrewThreshHold(date1,date2,wareHouse,sessionCorpID,noOfCrewFromPeople,oldTicketNumber);
	}
	public BigDecimal findSumOfrequiredCrew(String oldTicketNumber,String wareHouse,Date date1, Date date2,String sessionCorpID){
		return workTicketDao.findSumOfrequiredCrew(oldTicketNumber,wareHouse,date1,date2,sessionCorpID);
	}
	public BigDecimal findAbsentCrewFromtimeSheet(Date date1, Date date2,String sessionCorpID){
		return workTicketDao.findAbsentCrewFromtimeSheet(date1,date2,sessionCorpID);
	}
	public Map<String, String> findCrewCalendarList(String crewCalendarwRHouse,Date crewCalendarDate,String sessionCorpID){
		return workTicketDao.findCrewCalendarList(crewCalendarwRHouse,crewCalendarDate,sessionCorpID);
	}
	public Map<Map,Map> findRequiredCrewValue(Date date1, Date date2,String hub,String sessionCorpID){
		return workTicketDao.findRequiredCrewValue(date1,date2,hub,sessionCorpID);
	}
	public void updateTicketStatusPackADetails(Date Date2, Long soId,String sessionCorpID){
		workTicketDao.updateTicketStatusPackADetails( Date2, soId, sessionCorpID);
	}
	public String statusOfWorkTicket(Long ticket,String sessionCorpID){
	return	workTicketDao.statusOfWorkTicket(ticket, sessionCorpID);
	}
	
	public Long getDailyOpsLimitForOi(Long dailyCount, String typeService,Date dateFormat, String hubID, String sessionCorpID)
	{
		return	workTicketDao.getDailyOpsLimitForOi(dailyCount, typeService, dateFormat, hubID, sessionCorpID);
	}

	public List getAllTicketByDate(String typeService,Date dateFormat, String hubID, String sessionCorpID, String itemsjbkequipId) {
		return	workTicketDao.getAllTicketByDate(typeService, dateFormat, hubID, sessionCorpID, itemsjbkequipId);
	}
	
	public List checkQuantityInOperation(String resorceTypeOI,String resorceDescriptionOI, String hubID, String sessionCorpID)
	{
		return	workTicketDao.checkQuantityInOperation(resorceTypeOI, resorceDescriptionOI, hubID, sessionCorpID);
	}
	public Long msgForThreshold(Long dailyCount,Long dailyCountPC,Date dateFormat, String hubID, String sessionCorpID){
		return	workTicketDao.msgForThreshold(dailyCount,dailyCountPC,dateFormat,hubID,sessionCorpID);
	}
	public List getServiceForOI( String ticketId,String sessionCorpID ,String shipNumber)
	{
		return	workTicketDao.getServiceForOI(ticketId, sessionCorpID, shipNumber);
	}

	public void updateStatusOfWorkTicket(String returnAjaxStringValue,String shipNumber, String corpID){
		workTicketDao.updateStatusOfWorkTicket(returnAjaxStringValue,shipNumber,corpID);
	}
	public List workTicketList(Long serviceOrderId){
		return	workTicketDao.workTicketList(serviceOrderId);
	}
	public List planningCalendarList(String sessionCorpID,String wareHouses){
		return	workTicketDao.planningCalendarList(sessionCorpID,wareHouses);
	}
	
	public List getAllDataItemList(Long workTicketId, String shipNumber){
		return	workTicketDao.getAllDataItemList(workTicketId,shipNumber);
	}
	public String getTruckNumber(Long workTicket, String sessionCorpID){
		return workTicketDao.getTruckNumber(workTicket, sessionCorpID);
	}
	public String getServiceAndWareHouseByTicket(String workTicket,String sessionCorpID){
		return workTicketDao.getServiceAndWareHouseByTicket(workTicket, sessionCorpID);
	}
	public void updateDateByTicket(Long ticket,Date date1,String sessionCorpID,String updatedBy,Boolean updateValue){
		workTicketDao.updateDateByTicket(ticket,date1,sessionCorpID,updatedBy,updateValue);
	}
	public List planningCalendarListForUnip(String sessionCorpID,String wareHouses,String printStart,String printEnd){
		return	workTicketDao.planningCalendarListForUnip(sessionCorpID,wareHouses,printStart,printEnd);
	}
	public String findCrewEmail(String wRHouse, Date dailySheetDate,String sessionCorpID,Long ticket){
		return	workTicketDao.findCrewEmail(wRHouse, dailySheetDate, sessionCorpID, ticket);
	}
}
