<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Email Storage Invoice</title>
<meta name="heading" content="Email Storage Invoice" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script>
<script language="javascript" type="text/javascript">
function fieldValidate(){
       if(document.forms['workPlanForm'].elements['fromDate'].value==''){
	      	alert("Please select the from date"); 
	     	return false;
	   }if(document.forms['workPlanForm'].elements['toDate'].value==''){
	       	alert("Please select the to date "); 
	       	return false;
	   }
}  

function calcDays(){
  document.forms['workPlanForm'].elements['formStatus'].value = '1';

 var date2 = document.forms['workPlanForm'].elements['fromDate'].value;	 
 var date1 = document.forms['workPlanForm'].elements['toDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];

  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }

   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];

   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");

  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);

  if(daysApart < 0){
    alert("Planning Period From Date must be less than To Date");
    //document.forms['workPlanForm'].elements['fromDate'].value='';
    document.forms['workPlanForm'].elements['toDate'].value='';
  } 
 	document.forms['workPlanForm'].elements['checkDaysClickTicket'].value = '';
	
}


function forDays(){
 document.forms['workPlanForm'].elements['checkDaysClickTicket'].value =1; 
}
</script>
</head>

<s:form name="workPlanForm" id="workPlanForm" action="emailInvoiceList" method="post" validate="true">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden id="checkDaysClickTicket" name="checkDaysClickTicket" />

<s:hidden name="formStatus" value=""/>
<div id="layer1" style="width:80%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Invoice</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="" width="100%">	
	
	<tr><td height="5px">&nbsp;</td></tr>	
	<tr>		  		
			<td align="right" width="130px" class="listwhitetext" style="padding-bottom:5px">Storage Invoice From</td>	
		  	<td align="left" class="listwhitetext">
	  	  	<td width="160px" style="padding-bottom:5px; padding-left:5px;"> 	 
		  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{fromDate}" size="10" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);" /> 
		  		<img id="fromDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />
		  	</td>
			<td align="right" class="listwhitetext" style="padding-bottom:5px"> To</td>	
		  	<td align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;">
		  		<s:textfield cssClass="input-text" id="toDate" name="toDate" value="%{toDate}" size="10" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);"  onselect="calcDays()" /> 
		  		<img id="toDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" />
		  	</td>
	</tr>
	<tr><td height="20px"></td></tr>
	<tr>	
	  	<td width=""></td>			
		<td colspan="3"><s:submit cssClass="cssbutton" cssStyle="width:155px; height:25px" align="top" value="Email Invoice" onclick="return fieldValidate();"/></td>
	</tr>
	<tr><td height="20"></td></tr>	 				
</table> 
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>    
</s:form>
<script type="text/javascript">
	<c:if test="${hitflag == 1}">
		alert('Email has been sent successfully.');
	</c:if>
</script>
 <script type="text/javascript">
 	setOnSelectBasedMethods(["calcDays(),forDays()"]);
	setCalendarFunctionality();
 </script>
