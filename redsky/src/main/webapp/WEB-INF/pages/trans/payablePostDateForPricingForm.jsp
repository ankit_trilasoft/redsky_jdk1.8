<%@ include file="/common/taglibs.jsp"%>
<%@page import="java.util.*" %>
<%@page import="java.text.*" %>
<title><fmt:message key="activeWorkTicketList.title" /></title>
<meta name="heading" content="<fmt:message key='activeWorkTicketList.heading'/>" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
 
<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
.mainDetailTable {
border:2px solid #74B3DC;
}

</style>
<% 
	Date date11 = new Date();
	SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd");
	String dt11=sdfDestination.format(date11);
%>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->	
<script type="text/javascript">
function findPayAccDateToFormat(){
	document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value = document.forms['payablePostDateForm'].elements['openPeriod'].value
	document.forms['payablePostDateForm'].elements['payAccDateDammy'].value = document.forms['payablePostDateForm'].elements['openPeriod'].value
}
var form_submitted = false;
function submitFormChrck() { 
  if (form_submitted)
	    {
	      alert ("Your form has already been submitted. Please wait...");
	      return false;
 }else {
  form_submitted = true;	 
  <c:if test="${currentOpenPeriod}">
  if(document.forms['payablePostDateForm'].elements['openPeriod'].value == ''){
	  alert("Please select the Posting Date.") 
	  form_submitted = false;
	  return false;
  }
  </c:if>
 }
}

function pick() {
	var accountingLocation=encodeURI(window.opener.location);
	var accountindex=accountingLocation.indexOf("pricingList.html");
	if(accountindex>=0){
		   window.opener.location.href = window.opener.location.href;
		   parent.window.opener.progressBarAutoSave('0'); 
		   parent.window.close();
	}else{
	var parentName = parent.window.opener.document.forms[0].name;
  		 window.opener.document.forms[parentName].elements['buttonType'].value= "invoice"; 
  	     document.cookie = 'buttonType' + "=" + escape ('invoice'); 
  	    parent.window.opener.findSoAllPricing();
  		window.close();
	}
	}
window.onbeforeunload = function () {
	parent.window.opener.progressBarAutoSave('0'); 
	}	
function checkFormDate()
	{ 
      var fdate = document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value;
      document.forms['payablePostDateForm'].elements['payAccDateForm'].value=fdate;
      if(fdate=='')
      {
       document.forms['payablePostDateForm'].elements['payAccDateForm'].value=''; 
      } 
    }
</script>
<script type="text/javascript">
function calcDateFlexibility(){
	<c:if test="${!postingDateStop}">
	<c:if test="${!currentOpenPeriod}">
	  if(document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value==''||document.forms['payablePostDateForm'].elements['payAccDateDammy'].value=='')
	  {
	  //alert('in if date blank')
	  }
	else{
		 var date4 = document.forms['payablePostDateForm'].elements['payAccDateDammy'].value; 
		 var date1 = document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value; 
		 var date1SplitResult = date1.split("-");
		 var day1 = date1SplitResult[0];
		 var month1 = date1SplitResult[1];
		 var year1 = date1SplitResult[2];
		   	 year1 = '20'+year1;
			 if(month1 == 'Jan'){ month1 = "01";  }
	    else if(month1 == 'Feb'){ month1 = "02";  }
		else if(month1 == 'Mar'){ month1 = "03";  }
		else if(month1 == 'Apr'){ month1 = "04";  }
		else if(month1 == 'May'){ month1 = "05";  }
		else if(month1 == 'Jun'){ month1 = "06";  }
		else if(month1 == 'Jul'){ month1 = "07";  }
		else if(month1 == 'Aug'){ month1 = "08";  }
		else if(month1 == 'Sep'){ month1 = "09";  }
		else if(month1 == 'Oct'){ month1 = "10";  }																					
		else if(month1 == 'Nov'){ month1 = "11";  }
		else if(month1 == 'Dec'){ month1 = "12";  }
				
		 var date2 = document.forms['payablePostDateForm'].elements['payAccDateDammy'].value; 
		 var date2SplitResult = date2.split("-");
		 var day2 = date2SplitResult[0];
		 var month2 = date2SplitResult[1];
		 var year2 = date2SplitResult[2];
		   	 year2 = '20'+year2;
			 if(month2 == 'Jan'){ month2 = "01";  }
	    else if(month2 == 'Feb'){ month2 = "02";  }
		else if(month2 == 'Mar'){ month2 = "03";  }
		else if(month2 == 'Apr'){ month2 = "04";  }
		else if(month2 == 'May'){ month2 = "05";  }
		else if(month2 == 'Jun'){ month2 = "06";  }
		else if(month2 == 'Jul'){ month2 = "07";  }
		else if(month2 == 'Aug'){ month2 = "08";  }
		else if(month2 == 'Sep'){ month2 = "09";  }
		else if(month2 == 'Oct'){ month2 = "10";  }																					
		else if(month2 == 'Nov'){ month2 = "11";  }
		else if(month2 == 'Dec'){ month2 = "12";  }

		var currentD = '<%= dt11 %>';
	    var date3SplitResult = currentD.split("-");
 		var day3 = date3SplitResult[2];
 		var month3 = date3SplitResult[1];
 		var year3 = date3SplitResult[0]; 		

 		var selectedDate = new Date(month1+"/"+day1+"/"+year1);
 		var sysDefaultDate = new Date(month2+"/"+day2+"/"+year2);		
 		var currentDate = new Date(month3+"/"+day3+"/"+year3);
 		var daysApartNew = Math.round((sysDefaultDate-currentDate)/86400000);
 		var daysApart = Math.round((selectedDate-currentDate)/86400000);
		var daysApartCurrent = Math.round((sysDefaultDate-selectedDate)/86400000);
		  if(daysApartNew<=0){
	 		  if((daysApart<0)&&(daysApartCurrent!=0)){
		 			 document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value=date4;
		 			document.forms['payablePostDateForm'].elements['payAccDateToFormat'].focus();
		 		    alert("You cannot enter posting date less than the current date."); 
		 		  }
 		  }else{
	 		  if(daysApart<0){
	 				 document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value=date4;
 					document.forms['payablePostDateForm'].elements['payAccDateToFormat'].focus();
 			    	alert("You cannot enter posting date less than the current date."); 
 		  			}
 		  }
			/*
 		  else{
 	 		  if((parseInt(month1)>=parseInt(month2))&&(parseInt(year1)>=parseInt(year2))){
 	 		  }else{
 	  		    alert("You can not enter Posting Date less than System Default current month"); 
 	 		    document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value=date4;
 	 		  }  
 		  }*/
 		   		 
	}
	</c:if>
	</c:if>
}
function calcDate()
  { 
	<c:if test="${!postingDateStop}"> 
	<c:if test="${!currentOpenPeriod}">
  if(document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value==''||document.forms['payablePostDateForm'].elements['payAccDateDammy'].value=='')
  {
  //alert('in if date blank')
  }
else
{ 
 var date2 = document.forms['payablePostDateForm'].elements['payAccDateDammy'].value; 
 var date1 = document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value; 
 var date3 = document.forms['payablePostDateForm'].elements['payAccDateDammy'].value;
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
    var month1 = 0;
    var month2 = month;
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
        month1 = 1;
   }
   else if(month == 'Feb')
   {
       month = "02";
       month1 = 2;
   }
   else if(month == 'Mar')
   {
       month = "03"
       month1 = 3;
   }
   else if(month == 'Apr')
   {
       month = "04"
       month1 = 4;
   }
   else if(month == 'May')
   {
       month = "05"
       month1 = 5;
   }
   else if(month == 'Jun')
   {
       month = "06"
       month1 = 6;
   }
   else if(month == 'Jul')
   {
       month = "07"
       month1 = 7;
   }
   else if(month == 'Aug')
   {
       month = "08"
       month1 = 8;
   }
   else if(month == 'Sep')
   {
       month = "09"
       month1 = 9;
   }
   else if(month == 'Oct')
   {
       month = "10"
       month1 = 10;
   }
   else if(month == 'Nov')
   {
       month = "11"
       month1 = 11;
   }
   else if(month == 'Dec')
   {
       month = "12";
       month1 = 12;
   }
   var year1 = '20'+year;
   var day1 = getLastDayOfMonth(month1,year1); 
   var date4 = day1+"-"+month2+"-"+year; 
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  //document.forms['accountLineForms'].elements['accountLine.storageDays'].value = daysApart; 
  var flag = 0;
  if(daysApart<0)
  {
    alert("You can not enter Posting Date less than system default Posting Date "); 
     flag = 1;
    document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value=date3;
  }
  if(flag == 0)
  {
   if(day1 != day)
   {
    alert("Posting date should be last day of the month.\n\nSystem will change the posting date as "+date4); 
   	document.forms['payablePostDateForm'].elements['payAccDateToFormat'].value = date4;
   }
  }
// document.forms['accountLineForms'].elements['checkForDaysClick'].value = '';
}
  </c:if>
  </c:if>
}

function getLastDayOfMonth(month,year)
{
    var day;
    switch(month)
    {
        case 1 :
        case 3 :
        case 5 :
        case 7 :
        case 8 :
        case 10 :
        case 12 :
            day = 31;
            break;
        case 4 :
        case 6 :
        case 9 :
        case 11 :
               day = 30;
            break;
        case 2 :
            if( ( (year % 4 == 0) && ( year % 100 != 0) )
                           || (year % 400 == 0) )
                day = 29;
            else
                day = 28;
            break;

    }
    return day;

}
</script>
<script type="text/javascript">
   function getHTTPObject()
  {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();   
</script>  
<script type="text/javascript">  

 function invoiceStatusCheck(rowId,vendoreCode,targetElement,payablePostingCheck,checkMultiCompDiv) 
   {
	 var corpId1='${accCorpID}';
	   if((payablePostingCheck.trim()=='Y')&&(corpId1=='UTSI')){
		   var targetId=targetElement.id;
		   alert("Invoice Number Already Booked For Vendor. Please Check and Re-enter.")
		   document.getElementById(targetId).checked=false;
	   }else if((checkMultiCompDiv.trim()=='Y')&&(corpId1=='SSCW')){
		   var targetId=targetElement.id;
		   alert("Vendor Invoice # "+targetElement.value+", cannot be posted in different company divisions. Kindly correct it to Proceed")
		   targetElement.checked=false;		   
	   }else{
    invoiceNumber=targetElement.value;
   	rowId = invoiceNumber+'~'+vendoreCode; 
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['payablePostDateForm'].elements['invoiceCheck'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['payablePostDateForm'].elements['invoiceCheck'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['payablePostDateForm'].elements['invoiceCheck'].value = userCheckStatus + '`' + rowId;
      document.forms['payablePostDateForm'].elements['invoiceCheck'].value = userCheckStatus.replace( '``' , '`' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['payablePostDateForm'].elements['invoiceCheck'].value;
     var userCheckStatus=document.forms['payablePostDateForm'].elements['invoiceCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['payablePostDateForm'].elements['invoiceCheck'].value = userCheckStatus.replace( '``' , '`' );
     }
     invoiceCheck();
	   }	 
    }
    
    function invoiceCheck(){
    var invoiceCheck = document.forms['payablePostDateForm'].elements['invoiceCheck'].value;
    invoiceCheck=invoiceCheck.trim(); 
    if(invoiceCheck=='')
    {
    document.forms['payablePostDateForm'].elements['postDate_Payable'].disabled=true;
    }
    else if(invoiceCheck=='`')
    {
    document.forms['payablePostDateForm'].elements['postDate_Payable'].disabled=true;
    }
    else if(invoiceCheck!='')
    {
      document.forms['payablePostDateForm'].elements['postDate_Payable'].disabled=false;
    }
    }
    function cheackAccountInterface()
	{
	    var serviceOrderId = document.forms['payablePostDateForm'].elements['sid'].value;
        var url="updateBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
        http2.open("GET", url, true);
        http2.onreadystatechange = handleHttpResponse;
        http2.send(null); 
        var accountInterface = document.forms['payablePostDateForm'].elements['accountInterface'].value; 
        if(accountInterface=='Y')
         {  
            document.getElementById("hid2").style.display="block"; 
         }
        else 
         { 	
           document.getElementById("hid2").style.display="none";
         } 
   }
   function handleHttpResponse()
     {

             if (http2.readyState == 4)
             {
                 var result= http2.responseText         
              }
     }	
   
   function message(){
	
	alert('Please review Company Division in Service Order');
	window.close();

}
	function trap(){
		 if(document.images)
		    {
		      var totalImages = document.images.length;
		      	for (var i=0;i<totalImages;i++) {
			      	try{
			      		if(document.images[i].src.indexOf('calender.png')>0){ 
							document.images[i].src = 'images/navarrow.gif';
							var el = document.getElementById(document.images[i].id);
							if((el.getAttribute("id")).indexOf('trigger')>0){
								el.removeAttribute("id");
							} } }
		      	catch(e){}
					} }  }
 </script>

</head>
<s:form id="payablePostDateForm" name="payablePostDateForm" action="savePayPostDateForPricing.html?&btntype=yes&decorator=popup&popup=true" method="post" onsubmit="return submitFormChrck()">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="serviceOrder.bookingAgentCheckedBy" />
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="invoiceCheck" />
 <s:hidden name="accountInterface" value="${accountInterface}"/>
<s:set name="accountVedorInvoiceList" value="accountVedorInvoiceList" scope="request"/> 
<table style="width:100%" border="0"><tr><td width="25%"></td><td>
<table border="0" class="mainDetailTable" cellpadding="0" cellspacing="0"style="width:390px"> 
	<tr>
	<td>
	<table  border="0" align="left"  cellpadding="0" cellspacing="0" class="detailTabLabel"  style="width:390px">
	<tr>
	<td align="left" colspan="2" class="listwhitetext"><b>This Service Order will be Processed for the following information: </b></td>  
	</tr>
	<tr><td height="20px"></td></tr>
	<tr>
	<td align="left" class="listwhitetext"><b>The Company Division for this Service Order is : </b></td>  
	<td width="100px"><s:textfield cssClass="input-textUpper" cssStyle="text-align:left" name="serviceOrder.companyDivision" size="10" maxlength="15" tabindex="9" readonly="true"/></td>
	</tr>
	<tr><td height="10px"></td></tr>
	<tr>
	<td align="left" class="listwhitetext"><b>The Booking Code for this Service Order is : </b></td>  
	<td width="100px"><s:textfield cssClass="input-textUpper" cssStyle="text-align:left" name="serviceOrder.bookingAgentCode" size="10" maxlength="15" tabindex="9" readonly="true"/></td>
	</tr>
	<tr><td height="10px"></td></tr>
	<tr>
	<td align="left" class="listwhitetext" colspan="2"><b>The Booking Agent name is : </b>  
	<s:textfield cssClass="input-textUpper" cssStyle="text-align:left" name="serviceOrder.bookingAgentName" size="36" tabindex="9" readonly="true"/></td>
	</tr>
	<tr><td height="10px"></td></tr></table> 
	</td></tr>
	<tr></td>
	<td>
	<table  border="0" align="left"  cellpadding="1" cellspacing="1" class="detailTabLabel"  >
	<tr><td height="5px"></td></tr>
	<tr><td><div id="bookingCreated" style="align:center" ><table><tr>
	<td align="left" class="listwhitetext" width="360px"><b>Is the above information correct?  </b>
	<input type="button" class="cssbutton1"  value="Yes" name="yes" onclick="cheackAccountInterface();"/>  
     <input type="button" class="cssbutton1" value="No" onclick="message();"/>   
     </td>
	</tr></td></table></div></td></tr>
	</table>
	</td>
	</tr> 
	</table></td></tr></table>
<div id="hid2" style="align:center" >
<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0" style="margin-top:15px "> 
	<tr><td width="222px"></td>
	<td>
	<table  border="0" align="left"  cellpadding="0" cellspacing="0" class="detailTabLabel"  style="width:370px"><tr>
	<td align="left" class="listwhitetext"><b>Please select the accountlines for posting: </b></td> 
	</tr></table> 
	</td>
	</tr></table>
	<table border="0"> 
	<tr><td width="37%"></td>
	<td>
<display:table name="accountVedorInvoiceList" class="table" requestURI="" id="accountVedorInvoiceList" style="width:370px" defaultsort="2" pagesize="100" >   
 		
		<display:column property="invoiceNumber" sortable="true" title="Vendor Invoice#" style="width:70px" />
		<%-- <display:column property="vendorCode" sortable="true" title="Bill To" maxLength="15"  style="width:120px"/> --%>
	    <display:column property="estimateVendorName" sortable="true" title="Vendor Name" maxLength="30"  style="width:120px"/>  
        <display:column property="actualExpenseSum" sortable="true" title="Total Amt"  style="width:70px"/>
        <display:column property="localExpenseSum" sortable="true" title="Local Amt"  style="width:70px"/>    	   
        <display:column title=""   style="width:10px">  
	   <input type="checkbox" value="${accountVedorInvoiceList.invoiceNumber}" id="${accountVedorInvoiceList.invoiceNumber}" style="margin-left:10px;"  onclick="invoiceStatusCheck('xxxx','${accountVedorInvoiceList.vendorCode}',this,'${accountVedorInvoiceList.payablePostingCheck}','${accountVedorInvoiceList.checkMultiCompDiv}')" />
        </display:column>
</display:table> 
</td>
</tr>
</table>
	<table border="0" class="detailTabLabel" cellpadding="0" cellspacing="0"> 
	<tr><td width="223px"></td>
	<td>
	<%-- <table><tr>
	<td align="right" class="listwhitetext"><b>You have selected the following # of accountlines for posting:  <c:out value="${numForPayPostDate} "/></b></td> 
	</tr></table>--%>
	<table class="detailTabLabel" cellpadding="0" cellspacing="2"><tr>
	<td align="right" class="listwhitetext"><b>Please review the posting date, current default is displayed:</b></td> 
	</tr></table>
	<table  border="0" align="center"  class="mainDetailTable" style="width:372px">
	<tbody>
	<tr><td height="10"></td></tr>
	<tr> 
		<td align="right"  class="listwhitebox" width="64px">Posting Date</td>
		<c:choose>
			<c:when test="${currentOpenPeriod}">
				<td align="left"><s:select cssClass="list-menu" name="openPeriod" list="%{openPeriodList}" headerKey=""	headerValue="" onchange="findPayAccDateToFormat();"/></td>
				<s:hidden id="payAccDateToFormat"  name="payAccDateToFormat" />
			</c:when>
			<c:otherwise>
		<c:if test="${payAccDateToFormat!=''&& payAccDateToFormat!=null}">
		<s:text id="payAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="payAccDateToFormat"/></s:text>
		<td width="130px" >
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
			<s:textfield cssClass="input-textUpper" id="payAccDateToFormat"  name="payAccDateToFormat" value="%{payAccDateToFormat}"  size="8" maxlength="11" onblur="checkFormDate(),calcDateFlexibility();" readonly="true"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
			<s:textfield cssClass="input-textUpper" id="payAccDateToFormat"  name="payAccDateToFormat" value="%{payAccDateToFormat}"  size="8" maxlength="11" onblur="checkFormDate(),calcDate();" readonly="true"/>
		</c:if>
		<img id="payAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		<c:if test="${payAccDateToFormat==''|| payAccDateToFormat==null}">
		<td width="130px">
		<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
			<s:textfield cssClass="input-textUpper" id="payAccDateToFormat" name="payAccDateToFormat" size="8" maxlength="11"  onblur="checkFormDate(),calcDateFlexibility();" readonly="true"/>
		</c:if>
		<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
			<s:textfield cssClass="input-textUpper" id="payAccDateToFormat" name="payAccDateToFormat" size="8" maxlength="11"  onblur="checkFormDate(),calcDate();" readonly="true"/>
		</c:if>
		<img id="payAccDateToFormat_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
		</c:if>
		</c:otherwise>
		</c:choose>
		</tr> 
		</tbody></table>
		<table><tbody>
		<tr>
		<td align="left"><input type="button" class="cssbutton1" style="" value="Cancel" onclick="window.close();"/>  
        </td>
        <td align="left">
        <c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
        	<s:submit cssClass="cssbutton1" type="button" value="Apply Payb. Posting Date For Selected Invoice" name="postDate_Payable"  cssStyle="width:280px;" onclick="checkFormDate();" onmouseover="calcDateFlexibility();checkFormDate();" />
        </c:if>  
        <c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
			<s:submit cssClass="cssbutton1" type="button" value="Apply Payb. Posting Date For Selected Invoice" name="postDate_Payable"  cssStyle="width:280px;" onclick="checkFormDate();" onmouseover="calcDate();checkFormDate();" />
        </c:if>
        </td>
		</tr> 
	</tbody></table>
	</td></tr></table></div>
	<s:hidden name="payAccDateForm"/> 
	<s:hidden id="payAccDateDammy"  name="payAccDateDammy" value="%{payAccDateToFormat}" /> 
	<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
	<div id="mydiv" style="position:absolute"></div>
	 </s:form>
	 <script type="text/javascript"> 
	 try{
		 <c:if test="${postingDateStop}"> 
		 trap();
		 </c:if>
		 }
	 catch(e){}
	try{
	 if(document.forms['payablePostDateForm'].elements['serviceOrder.bookingAgentCheckedBy'].value=='')
	 {
	 document.getElementById("hid2").style.display="none";
	  document.getElementById("bookingCreated").style.display="block";
	  }
	  }catch(e){}
	  try{
	  if(document.forms['payablePostDateForm'].elements['serviceOrder.bookingAgentCheckedBy'].value!='')
	 {
	 document.getElementById("hid2").style.display="block";
	  document.getElementById("bookingCreated").style.display="none";
	  }
	  }
	  catch(e){}
try{
invoiceCheck();
}
catch(e){}
try{
if(document.forms['payablePostDateForm'].elements['btntype'].value=='yes'){
pick();
}
}
catch(e){}
<c:if test="${company.postingDateFlexibility!=null && company.postingDateFlexibility==true}">
setOnSelectBasedMethods(["checkFormDate(),calcDateFlexibility()"]);
</c:if>
<c:if test="${company.postingDateFlexibility==null || company.postingDateFlexibility==false}">
setOnSelectBasedMethods(["checkFormDate(),calcDate()"]);
</c:if>
setCalendarFunctionality();
</script>