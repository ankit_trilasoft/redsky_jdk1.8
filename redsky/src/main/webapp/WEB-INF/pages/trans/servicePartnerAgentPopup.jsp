<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>  

<head>   
    <title>Partner List</title>   
    <meta name="heading" content="<fmt:message key='chargesList.heading'/>"/> 
<style>

span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:1px;
	margin-top:-18px;
	!margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}
</style>
<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['agentListPartnerForm'].elements['partnerCode'].value = "";
	document.forms['agentListPartnerForm'].elements['partnerlastName'].value = "";
	document.forms['agentListPartnerForm'].elements['partneraliasName'].value = "";
	document.forms['agentListPartnerForm'].elements['partnerbillingCountryCode'].value = "";
	document.forms['agentListPartnerForm'].elements['partnerbillingState'].value = "";
	
	}
</script>   
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top"  key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="agentListPartnerForm" action='searchAgentPartnerList.html?soCorpID=${soCorpID}&decorator=popup&popup=true' method="post" >
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>

<th>Code</th>
<th><fmt:message key="partner.name"/></th>
<th>Alias Name</th>
<th><fmt:message key="partner.billingCountryCode"/></th>
<th><fmt:message key="partner.billingState"/></th>
</tr></thead>	
		<tbody>
		<tr>
		<tr>
			<td>
			    <s:textfield name="partnerCode" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partnerlastName" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partneraliasName" cssClass="input-text" />
			</td>
			<td>
			    <s:textfield name="partnerbillingCountryCode" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			<td>
			    <s:textfield name="partnerbillingState" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			</td>
			
		</tr>
		<tr>
		<td colspan="4"></td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Carrier List</span></a></li>
				  </ul>
				</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="partnerList" value="partnerList" scope="request"/>
<display:table name="partnerList" class="table" requestURI="" id="partnerList"  defaultsort="2" pagesize="20"   >   
		
 <display:column  sortable="true" titleKey="partner.partnerCode"  >
 <c:set var="des" value="${partnerList.firstName} ${partnerList.lastName}"/>
     <a onclick='setParentValue("${partnerList.partnerCode}","${fn:escapeXml(des)}");'><c:out value="${partnerList.partnerCode}"/></a
 </display:column><>    
 <display:column titleKey="partner.name" sortable="true" style="width:345px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
   <display:column title="Alias Name" sortable="true" style="width:345px"><c:out value="${partnerList.aliasName}" /></display:column>
  <display:column property="billingCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
  <display:column property="billingState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
   <display:column property="billingCity" sortable="true" titleKey="partner.billingCity" style="width:140px"/>  
  
    <display:setProperty name="export.excel.filename" value="Charges List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Charges List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Charges List.pdf"/> 
 
</display:table> 

</s:form>
<script type="text/javascript">
function setParentValue(charge,description){
	var codeId='${code}';
	var nameId='${Name}';
	if(codeId!='' && nameId!=''){
		window.opener.document.getElementById(codeId).value=charge; 
		window.opener.document.getElementById(nameId).value=description;
	}else{
	window.opener.document.getElementById("newcharge").value=charge; 
	window.opener.document.getElementById("newDescription").value=description;
	}
	//window.opener.setContractValue(); 
	 self.close();
}
////window.close();
</script>