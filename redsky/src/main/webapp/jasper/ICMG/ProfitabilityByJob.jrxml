<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ProfitabilityByJob" pageWidth="595" pageHeight="842" columnWidth="595" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="12c97e58-df32-4b21-97aa-79bc49113343">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="868"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Begin Date" class="java.util.Date"/>
	<parameter name="End Date" class="java.util.Date"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select 'ProfitabilityByJob-R-ICMG.jrxml',
serviceorder.shipNumber AS 'Job Reference',
billing.contract AS 'Contract',
billing.billToName AS 'Bill to Name',
serviceorder.coordinator AS 'Coordinator',
round(serviceorder.actualRevenue,2) AS 'Actual Revenue',
round(serviceorder.actualExpense,2) AS 'Actual Cost',
round((serviceorder.actualRevenue-serviceorder.actualExpense),2) AS 'Profit',
round((((serviceorder.actualRevenue-serviceorder.actualExpense)*100)/(serviceorder.actualRevenue)),0) AS 'Margin',
serviceorder.job serviceorder_job
FROM serviceorder serviceorder
left outer join billing billing on serviceorder.id=billing.id and billing.corpID=$P{Corporate ID}
WHERE serviceorder.corpID=$P{Corporate ID}
AND (date_format(serviceorder.statusDate,'%Y-%m-%d') BETWEEN date_format($P{Begin Date},'%Y-%m-%d') and date_format($P{End Date},'%Y-%m-%d'))
AND (serviceorder.actualRevenue<>0 || serviceorder.actualExpense<>0)
group by serviceorder.id
order by 10]]>
	</queryString>
	<field name="ProfitabilityByJob-R-ICMG.jrxml" class="java.lang.String"/>
	<field name="Job Reference" class="java.lang.String"/>
	<field name="Contract" class="java.lang.String"/>
	<field name="Bill to Name" class="java.lang.String"/>
	<field name="Coordinator" class="java.lang.String"/>
	<field name="Actual Revenue" class="java.math.BigDecimal"/>
	<field name="Actual Cost" class="java.math.BigDecimal"/>
	<field name="Profit" class="java.math.BigDecimal"/>
	<field name="Margin" class="java.math.BigDecimal"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<variable name="Actual Revenue" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Revenue}]]></variableExpression>
	</variable>
	<variable name="Actual Cost" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{Actual Cost}]]></variableExpression>
	</variable>
	<variable name="Profit" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{Profit}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="59" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" mode="Opaque" x="0" y="0" width="595" height="41" uuid="b1864e4b-c639-438a-ba21-97f2754c6932"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="Calibri" size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job Profitability Report]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="0" y="41" width="81" height="18" backcolor="#CCCCCC" uuid="fb76423c-a727-4cfd-953f-692fdc6124c2"/>
				<box leftPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job Reference]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-19" mode="Opaque" x="392" y="41" width="75" height="18" backcolor="#CCCCCC" uuid="1b14b743-db92-4ba8-8c6d-9314e8ee6cc7"/>
				<box rightPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Actual Cost]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Opaque" x="317" y="41" width="75" height="18" backcolor="#CCCCCC" uuid="bea18a3a-b555-4c9c-932d-e9ad45f661c1"/>
				<box rightPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Actual Revenue]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Opaque" x="532" y="41" width="63" height="18" backcolor="#FFFFCC" uuid="de52512e-fb2e-4ef8-8713-5b01aec40018"/>
				<box rightPadding="9">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Margin]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-31" mode="Opaque" x="467" y="41" width="65" height="18" backcolor="#FFFFCC" uuid="bf68f11c-66f7-436c-9b56-c4a8ac37e120"/>
				<box rightPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Profit]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-32" mode="Opaque" x="81" y="41" width="94" height="18" backcolor="#CCCCCC" uuid="0e1561d5-e47a-4d62-8dba-7d91ad3fa5d6"/>
				<box leftPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Contract]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-33" mode="Opaque" x="175" y="41" width="92" height="18" backcolor="#CCCCCC" uuid="7c610e92-e924-42e5-a7d4-fdfd68401561"/>
				<box leftPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill to Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-34" mode="Opaque" x="267" y="41" width="50" height="18" backcolor="#CCCCCC" uuid="5f4828e6-1435-4f3e-90ec-ab4545fa55ba"/>
				<box leftPadding="1">
					<bottomPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Coordinator]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="22" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-9" stretchType="RelativeToTallestObject" x="392" y="1" width="75" height="21" uuid="54da607a-5dac-497d-a110-2f99434844f2"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Actual Cost}.compareTo(BigDecimal.ZERO) > 0 ? $F{Actual Cost}.toString() : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-21" stretchType="RelativeToTallestObject" x="0" y="1" width="81" height="21" uuid="1f4e9739-6893-453a-a23a-f88fa3d8757b"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Job Reference}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-24" stretchType="RelativeToTallestObject" mode="Opaque" x="532" y="1" width="63" height="21" backcolor="#FFFFCC" uuid="f598d214-2759-4d19-9c44-51982fe68c76"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Margin}.intValue()==0 ? "" : ($F{Margin}.intValue()+"%")]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-31" stretchType="RelativeToTallestObject" mode="Opaque" x="467" y="1" width="65" height="21" backcolor="#FFFFCC" uuid="eac1f0dc-5e95-4d66-8d04-f78a0e45bd93"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Profit}.compareTo(BigDecimal.ZERO) != 0 ? $F{Profit}.toString() : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-32" stretchType="RelativeToTallestObject" x="81" y="1" width="94" height="21" uuid="6c7b5a50-09d9-43f6-9768-948bc8720d93"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Contract}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-33" stretchType="RelativeToTallestObject" x="175" y="1" width="92" height="21" uuid="ac4eb379-b263-4b60-9a0f-629d9ce53d56"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Bill to Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-34" stretchType="RelativeToTallestObject" x="267" y="1" width="50" height="21" uuid="7c734d80-8994-4fca-980c-f6a4bd7df047"/>
				<box leftPadding="1"/>
				<textElement>
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Coordinator}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-43" stretchType="RelativeToTallestObject" x="317" y="1" width="75" height="21" uuid="b2774a06-52bb-429b-8bfb-49970c0b414a"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Actual Revenue}.compareTo(BigDecimal.ZERO) > 0 ? $F{Actual Revenue}.toString() : ""]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<lastPageFooter>
		<band height="21" splitType="Prevent">
			<textField isStretchWithOverflow="true" evaluationTime="Report" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-37" stretchType="RelativeToTallestObject" x="392" y="0" width="75" height="21" uuid="c5b25202-7fd5-49e1-8f88-9740a5ef51cf"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Actual Cost}.compareTo(BigDecimal.ZERO) > 0 ? $V{Actual Cost}.toString() : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-38" stretchType="RelativeToTallestObject" mode="Opaque" x="467" y="0" width="65" height="21" backcolor="#FFFFCC" uuid="92f67110-cf39-43ca-b99b-760b84157189"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Profit}.compareTo(BigDecimal.ZERO)!= 0 ? $V{Profit}.toString() : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-41" stretchType="RelativeToTallestObject" mode="Opaque" x="532" y="0" width="63" height="21" backcolor="#FFFFCC" uuid="bbe4c864-98bd-4e07-ada7-361f3f8e0648"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" pattern="#,##0.00" isBlankWhenNull="true">
				<reportElement key="textField-44" stretchType="RelativeToTallestObject" x="317" y="0" width="75" height="21" uuid="e87df82a-e300-46a1-b5ba-80b78c108082"/>
				<box rightPadding="1"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{Actual Revenue}.compareTo(BigDecimal.ZERO) > 0 ? $V{Actual Revenue}.toString() : ""]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" evaluationTime="Report" isBlankWhenNull="true">
				<reportElement key="textField-45" stretchType="RelativeToTallestObject" x="0" y="0" width="317" height="21" uuid="e7655252-50f2-4363-b76b-189dd0555107"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Right">
					<font fontName="Calibri" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA["Grand Total"]]></textFieldExpression>
			</textField>
		</band>
	</lastPageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
