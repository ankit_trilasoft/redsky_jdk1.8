<%@ include file="/common/taglibs.jsp"%>
<title>Pricing Currency Form</title>
<meta name="heading" content="Pricing Currency Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 
</script>


<s:form id="accountLineCurrencyRecForm" name="accountLineCurrencyRecForm" action="" onsubmit="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="tempAccList"/>
 <s:hidden name="accountLine.revisionRevenueAmount"  /> 
  <c:if test="${contractType}">	
  <s:hidden name="accountLine.revisionSellQuantity"  /> 
  </c:if>
   <c:if test="${!contractType}">	
 <s:hidden name="accountLine.revisionQuantity"  /> 
  </c:if>
 <s:hidden name="accountLine.basis"  />
 <s:hidden name="accountLine.revisionDiscount"/> 
  <s:hidden name="accountLine.revisionSellRate"/> 
 <s:hidden name="aid" value="${accountLine.id}" />  
<s:hidden name="formStatus"  />
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Sell&nbsp;Rate&nbsp;Currency&nbsp;Data</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    
<table border="0" style="margin:0px;padding:0px;">
										<c:if test="${contractType}">																											 
										 <tr>
										 <td colspan="9">															 											
										 <table class="detailTabLabel" style="margin:0px;">
										 <tr>															
										 <td width="1px"></td>
										 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
										 <td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.revisionContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.revisionContractExchangeRate');calculateRecCurrency('accountLine.revisionContractRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" headerValue="" tabindex="12"  /></td>
										 <td align="right" width="" class="listwhitetext">Value&nbsp;Date</td>

										 <c:if test="${not empty accountLine.revisionContractValueDate}"> 
										  <s:text id="accountLineFormattedRevisionContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionContractValueDate"/></s:text>
										  <td ><s:textfield id="revisionContractValueDate" name="accountLine.revisionContractValueDate" value="%{accountLineFormattedRevisionContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
										  <img id="revisionContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										 </td>
										 </c:if>
										     <c:if test="${empty accountLine.revisionContractValueDate}">
										   <td><s:textfield id="revisionContractValueDate" name="accountLine.revisionContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/>
										   <img id="revisionContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										   </td>
										     </c:if>
                                            <td  align="right" class="listwhitetext" width="55px">Ex.&nbsp;Rate</td>
										 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionContractExchangeRate"  size="8" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.revisionContractRate','','','');" /></td>   			
                                            <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionContractRate" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.revisionContractRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" /></td>	 
                                            <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionContractRateAmmount" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true"  onblur=""/></td>	 
										 </tr></table>															
										 </td>															
										 </tr>
										</c:if> 
										
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">

<tr>

													 <c:choose>
													 <c:when test="${contractType}">
													 <td align="right" class="listwhitetext" width="102">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
													 </c:when>
													 <c:otherwise>
													 <td align="right" class="listwhitetext">Currency</td>
													 </c:otherwise>
													 </c:choose>

<td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.revisionSellCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.revisionSellExchangeRate');calculateRecCurrency('accountLine.revisionContractRate','','','');" headerValue="" tabindex="18"  /></td>
<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
<c:if test="${not empty accountLine.revisionSellValueDate}"> 
<s:text id="accountLineRevisionSellValueDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionSellValueDate"/></s:text>
<td width="110px" ><s:textfield id="revisionSellValueDate" name="accountLine.revisionSellValueDate" value="%{accountLineRevisionSellValueDateFormattedValue}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="revisionSellValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<c:if test="${empty accountLine.revisionSellValueDate}">
<td width="110px"><s:textfield id="revisionSellValueDate" name="accountLine.revisionSellValueDate" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="revisionSellValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<td  align="right" class="listwhitetext">Ex.&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionSellExchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculateRecCurrency('accountLine.revisionContractRate','','','');"/></td>
<td align="right" class="listwhitetext" width="80px">Sell&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionSellLocalRate" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="changeStatus();calculateRecCurrency('accountLine.revisionSellLocalRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" onblur=""/></td>	 
<td align="right" class="listwhitetext" width="86px">Curr&nbsp;Amount</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionSellLocalAmount" size="8" maxlength="12" readonly="true"/></td>	 
</tr> 
</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="calculateRecRate();"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<script type="text/javascript">
function findExchangeRateRateOnActualization(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
 var daym;
 var year=mydate.getFullYear()
 var y=""+year;
 if (year < 1000)
 year+=1900
 var day=mydate.getDay()
 var month=mydate.getMonth()+1
 if(month == 1)month="Jan";
 if(month == 2)month="Feb";
 if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
    <c:if test="${contractType}">	
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.forms['accountLineCurrencyRecForm'].elements[field1.trim().split('~')[0]].value;
		document.forms['accountLineCurrencyRecForm'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['accountLineCurrencyRecForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}
		try{
		currency=document.forms['accountLineCurrencyRecForm'].elements[field1.trim().split('~')[1]].value;
		document.forms['accountLineCurrencyRecForm'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['accountLineCurrencyRecForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
		</c:if>
	</c:if>
}
</script>
<script type="text/javascript">
<c:if test="${(((!discountUserFlag.pricingActual && !discountUserFlag.pricingRevision) &&  (accountLine.revisionExpense!='0.00' || accountLine.revisionRevenueAmount!='0.00' || accountLine.actualRevenue!='0.00' || accountLine.actualExpense!='0.00' ||accountLine.distributionAmount!='0.00' || accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || ((discountUserFlag.pricingActual || discountUserFlag.pricingRevision) &&  (accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE')) || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking'))}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
var elementsLen=document.forms['accountLineCurrencyRecForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyRecForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyRecForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyRecForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyRecForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>

<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">
function calculateRecRate(){ 
	var aid=document.forms['accountLineCurrencyRecForm'].elements['aid'].value;
	 window.opener.setFieldValue('revisionRevenueAmount'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionRevenueAmount'].value);
	 window.opener.setFieldValue('revisionSellRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value);
	 <c:if test="${contractType}">
	 window.opener.setFieldValue('revisionSellQuantity'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellQuantity'].value);
	 </c:if>
	 <c:if test="${!contractType}">
	 window.opener.setFieldValue('revisionQuantity'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value);
	 </c:if>
    <c:if test="${contractType}">
	 window.opener.setFieldValue('revisionContractCurrency'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractCurrency'].value);
	 window.opener.setFieldValue('revisionContractExchangeRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractExchangeRate'].value);
	 window.opener.setFieldValue('revisionContractRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRate'].value);
	 window.opener.setFieldValue('revisionContractRateAmmount'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRateAmmount'].value);
	 window.opener.setFieldValue('revisionContractValueDate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractValueDate'].value);
    </c:if>  
	 window.opener.setFieldValue('revisionSellCurrency'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellCurrency'].value);
	 window.opener.setFieldValue('revisionSellExchangeRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellExchangeRate'].value);
	 window.opener.setFieldValue('revisionSellLocalRate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalRate'].value);
	 window.opener.setFieldValue('revisionSellLocalAmount'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value);
	 window.opener.setFieldValue('revisionSellValueDate'+aid,document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value);
	 window.opener.getId(aid);
    if(document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value=='2'){
    window.opener.calculateRevisionBlock(aid);
    window.opener.changeStatus();
    }
		window.close();
}
function changeStatus() {
	   document.forms['accountLineCurrencyRecForm'].elements['formStatus'].value = '2'; 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function updateExchangeRate(currency,field){
	document.forms['accountLineCurrencyRecForm'].elements[field].value=findExchangeRateGlobal(currency.value);
}
function currentDateGlobal(){
	 var mydate=new Date();
    var daym;
    var year=mydate.getFullYear()
    var y=""+year;
    if (year < 1000)
    year+=1900
    var day=mydate.getDay()
    var month=mydate.getMonth()+1
    if(month == 1)month="Jan";
    if(month == 2)month="Feb";
    if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}

function calculateRecCurrency(target,field1,field2,field3) {
	 if(field1!='' && field2!='' & field3!=''){
		 rateOnActualizationDate(field1,field2,field3);
		 }	   
	 var basis=document.forms['accountLineCurrencyRecForm'].elements['accountLine.basis'].value;
	 var baseRateVal=1;
	var revisionQuantity=0.00;
	 <c:if test="${contractType}">
	 revisionQuantity=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellQuantity'].value;
	 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		 revisionQuantity = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellQuantity'].value=1;
        } 	 
	 </c:if>
	 <c:if test="${!contractType}">
	 revisionQuantity=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value;
	 if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		 revisionQuantity = document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionQuantity'].value=1;
        } 	 
	 </c:if>		 	
    var revisionDiscount=0.00;
    var revisionSellRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value;
    try{
    	revisionDiscount=document.forms['pricingListForm'].elements['accountLine.revisionDiscount'].value;
    }catch(e){}
    if( basis=="cwt" || basis=="%age"){
      	 baseRateVal=100;
    }else if(basis=="per 1000"){
      	 baseRateVal=1000;
    } else {
      	 baseRateVal=1;  	
	 }		
<c:if test="${contractType}">
if(target=='accountLine.revisionContractRate'){
	var revisionContractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractCurrency'].value;		
	var revisionContractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractExchangeRate'].value;
	var revisionContractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRate'].value;
	var revisionContractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRateAmmount'].value;
	var revisionContractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractValueDate'].value;
	if(revisionContractCurrency!=''){
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
		revisionContractRateAmmount=(revisionQuantity*revisionContractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRateAmmount'].value=revisionContractRateAmmount;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value=revisionContractRate/revisionContractExchangeRate;
	}
	   var revisionSellCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellCurrency'].value;		
	   var revisionSellExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellExchangeRate'].value;
	   var revisionSellLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalRate'].value;
	   var revisionSellLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value;
	   var revisionSellValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value;
	   revisionSellRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value;
	   	if(revisionSellCurrency!=''){
	   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
	   		revisionSellLocalRate=revisionSellRate*revisionSellExchangeRate;
	   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalRate'].value=revisionSellLocalRate;
	   		revisionSellLocalAmount=(revisionQuantity*revisionSellLocalRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmount;   	 		
	}
}else if(target=='accountLine.revisionSellLocalRate'){
	var revisionSellCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellCurrency'].value;		
	var revisionSellExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellExchangeRate'].value;
	var revisionSellLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalRate'].value;
	var revisionSellLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value;
	var revisionSellValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value;
	   
	   	if(revisionSellCurrency!=''){
	   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
	   		revisionSellLocalAmount=(revisionQuantity*revisionSellLocalRate)/baseRateVal;
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmount; 
	   	 document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value=revisionSellLocalRate/revisionSellExchangeRate; 		
	}	
	
		var revisionContractCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractCurrency'].value;		
		var revisionContractExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractExchangeRate'].value;
		var revisionContractRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRate'].value;
		var revisionContractRateAmmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRateAmmount'].value;
		var revisionContractValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractValueDate'].value;
	revisionSellRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value;
	if(revisionContractCurrency!=''){
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractValueDate'].value=currentDateGlobal();
		revisionContractRate=revisionSellRate*revisionContractExchangeRate;
   		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRate'].value=revisionContractRate;
   		revisionContractRateAmmount=(revisionQuantity*revisionContractRate)/baseRateVal;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionContractRateAmmount'].value=revisionContractRateAmmount;
	}
}else{

}
</c:if>
<c:if test="${!contractType}">
	var revisionSellCurrency=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellCurrency'].value;		
	var revisionSellExchangeRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellExchangeRate'].value;
	var revisionSellLocalRate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalRate'].value;
	var revisionSellLocalAmount=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value;
	var revisionSellValueDate=document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value;
	if(revisionSellCurrency!=''){
		document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellValueDate'].value=currentDateGlobal();
		revisionSellLocalAmount=(revisionQuantity*revisionSellLocalRate)/baseRateVal;
   	    if(revisionDiscount!=0.00){
   	    	revisionSellLocalAmount=(revisionSellLocalAmount*revisionDiscount)/100;
   	    }  
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellLocalAmount'].value=revisionSellLocalAmount;
	   document.forms['accountLineCurrencyRecForm'].elements['accountLine.revisionSellRate'].value=revisionSellLocalRate/revisionSellExchangeRate; 		
	}
	</c:if>
}
</script>
