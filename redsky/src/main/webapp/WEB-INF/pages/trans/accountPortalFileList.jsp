<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="myFileList.title"/></title>   
    <meta name="heading" content="<fmt:message key='myFileList.heading'/>"/> 
    
  <style>  
.table thead th, .tableHeaderTable td {
background:#BCD2EF url(images/bg_listheader.png) repeat-x scroll 0 0;
border-color:#3dafcb #3dafcb #3dafcb -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(images/bg_listheader.png) repeat scroll 0%;
border-color:#3dafcb rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}



.table tr.odd {
color:#000d47;
}

.table tr.even {
color:#000000;
background-color:{#e4e6f2}
}

.table tr:hover {
color:#0b0148;
background-color:{#fffcd6}
}

#newmnav{margin-left: 40px;margin-bottom: 0px;_z-index:9999; !z-index:9999; }
#newmnav ul{margin: 0;padding: 0;list-style: none;}
#newmnav li {float: left;margin: 0 3px 0 0;white-space: nowrap;background:#afd6fa url(images/tabbg.png);font:bold 12px arial,verdana;line-height:1.25em;padding: 0px 0px 0px 0px;}
#newmnav a {float: left;padding: 0 0 0 9px;background:url(images/tabn1new.png) no-repeat 0% 0%;text-decoration: none;color: #404040;}
#newmnav a:hover{background-position: 0 -28px;background: url(images/tabn1new.png) no-repeat 0% 0%;color: #404040;}
#newmnav a.current {background-position: 0 -28px;background: url(images/1hm.png);color: #404040;font:bold 12px arial,verdana; _margin:2px 0 -11px 0;margin:2px 0 9px 0;}
#newmnav a span {float: left;background:url(images/tabn2new.png) no-repeat 100% 0;padding: 5px 9px 4px 0; }
#newmnav a:hover span{background-position: 100% -28px;background:url(images/tabn2new.png) no-repeat 100% 0;cursor: pointer;}
#newmnav a.current span{background-position: 100% -28px;background: url(images/2hm.png) no-repeat 100% 0;cursor: pointer;}
#newmnav a.current:hover span {cursor: default;}


#newmnav1{margin-left: 40px;margin-bottom: 0px; _z-index:9999; !z-index:9999;}
#newmnav1 ul{margin: 0;padding: 0;list-style: none;}
#newmnav1 li {float: left;margin: 0 3px 0 0;white-space: nowrap;background:#c1c1c1 url(images/tabbg.png);font:bold 12px arial,verdana;line-height:1.25em;padding: 0px 0px 0px 0px;}
#newmnav1 a {float: left;padding: 0 0 0 9px;background:url(images/1hm.png) no-repeat 0% 0%;text-decoration: none;color: #f38621;margin:2px 0 0px 0;}
#newmnav1 a:hover{background-position: 0 -28px;background: url(images/1hm.png) no-repeat 0% 0%;color: #f38621;}
#newmnav1 a.current {background-position: 0 -28px;background: url(images/1hm.png);color: #f38621;font:bold 12px arial,verdana; margin:0px 0 -2px 0;_margin:1px 0 -3px 0; }
#newmnav1 a span {float: left;background:url(images/2hm.png) no-repeat 100% 0;padding: 5px 9px 4px 0; }
#newmnav1 a:hover span{background-position: 100% -28px;background:url(images/2hm.png) no-repeat 100% 0;cursor: pointer;}
#newmnav1 a.current span{background-position: 100% -28px;background: url(images/2hm.png) no-repeat 100% 0;cursor: pointer;}
#newmnav1 a.current:hover span {cursor: default;}
.spn{clear:both; line-height:0.0085em; background:transparent;}



 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>
    
    
</head>
<div id="Layer5" style="width:100%">
<s:form id="accountFileList"  name="accountFileList" action="" method="post" >
<s:hidden name="id" value="${customerFile.id}"/>
<c:set var="id"  value="${customerFile.id}"/>
<c:set var="buttons">   
	<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15"
         onclick="window.open('uploadAgentsFile!start.html?id=${id}&myFileFor=CF&decorator=popup&popup=true','','width=750,height=570');"
         value="<fmt:message key="button.upload"/>"/>
        </c:set> 
<div id="newmnav">
		  <ul> 
		    <li id="newmnav1" style="background:#FFF "><a href="accountPortalFiles.html?id=${customerFile.id}" class="current" /><span>File Documents</span></a></li>
		    <c:if test="${agentHoFlag == 'yes'}">
		    <li ><a href="editAgentHoOrder.html?id=${customerFile.id}" ><span>Order Detail</span></a></li> 
		    </c:if>
		    <c:if test="${agentHoFlag != 'yes'}">
		    <c:if test="${asmlFlag == 'asml'}">
		     <li ><a href="editAsmlOrder.html?id=${customerFile.id}" ><span>Order Detail</span></a></li> 
		    </c:if>
		    <c:if test="${asmlFlag == 'order'}"> 
		     <li ><a href="editOrderInitiation.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		   </c:if>
		   <c:if test="${asmlFlag == 'ikea'}"> 
		     <li ><a href="editIkeaOrder.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		   </c:if>
		   </c:if>
		 </div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
		
<div style="!margin-top:7px;">
<display:table name="myFiles" class="table" requestURI="" id="myFileList" pagesize="25" style="width:670px;margin-top:2px;" >
   <display:column property="fileType" sortable="true" titleKey="myFile.fileType" style="width:108px;"/>
   <display:column property="description" sortable="true" title="description" style="width:108px;"/>
   <display:column titleKey="myFile.fileFileName"  maxLength="50" style="width:155px;"><a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a></display:column>
</display:table></div>

<c:out value="${buttons}" escapeXml="false" />
</s:form>
</div>