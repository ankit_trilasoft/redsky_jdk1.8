<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<meta name="heading" content="<fmt:message key='countryDeviation.heading'/>"/> 

<title><fmt:message key="countryDeviation.title"/></title> 
</head>

</head>
<body >
<s:form id="countryDeviation" name="countryDeviation"  method="post">
<s:hidden name="charge" value="<%=request.getParameter("charge")%>" />
<s:hidden name="contract" value="<%=request.getParameter("contract")%>" />
<s:hidden name="checkData" />
<s:hidden name="saveBntType1" value="<%=request.getParameter("saveBntType")%>"  />
<c:set var="saveBntType1" value="<%= request.getParameter("saveBntType") %>" />
<c:set var="charge1" value="<%= request.getParameter("charge") %>" />
<c:set var="contract1" value="<%= request.getParameter("contract") %>" />
<s:hidden name="delDeviationId" />
<c:set var="btn" value="<%= request.getParameter("btn") %>" />
<div id="Layer1" style="width: 400px">
<div id="otabs" style="float:left;margin-bottom:12px;">
<ul>
	<li><a class="current"><span>Country Deviation</span></a></li>
	</ul>
	</div>
	<div style="padding-top:2px;"><s:textfield name="charges.charge" readonly="true"  cssClass="input-textUpper" maxlength="8" size="20" /></div>
  <div class="spn">&nbsp;&nbsp;&nbsp;&nbsp;</div> 
<display:table name="countryDeviationList" class="table" requestURI="" id="deviationList"   export="false" pagesize="50" style="width:125%;margin-top:0px;" >
	<c:if test="${not empty deviationList.id}">
	<display:column title="">
		<input type="checkbox" name="deviationId" id="${deviationList.id }" value="${deviationList.id }"  />
	</display:column>
	<display:column title="Country Code">
		<s:select cssClass="list-menu"  name="countryCode${deviationList.id }" id="countryCode${deviationList.id }" list="%{country}"  headerKey="" headerValue=" " cssStyle="width:150px" onchange="traceData('${deviationList.id }');" /> 
	</display:column>
	<display:column title="Deviation%"> 
		<s:textfield cssStyle="text-align:right" name="deviation${deviationList.id}" id="deviation${deviationList.id}" required="true" value="${deviationList.deviation}" onchange="return chkBlank(this),traceData('${deviationList.id }');" cssClass="input-text" maxlength="8" size="20" onblur="return checkFloat(this)"  />
	</display:column>
	</c:if>
	<c:if test="${empty deviationList.id}">
		<display:column title="">
			<input type="checkbox" name="newId" id="newId" value="" disabled="disabled"/>
	</display:column>
	<display:column title="Country Code">
		<s:select cssClass="list-menu"  name="newCountryCode" id="newCountryCode" list="%{country}" headerKey=" " headerValue=" " cssStyle="width:150px" onchange="traceData('new');" /> 
	</display:column>
	<display:column title="Deviation%"> 
		<s:textfield cssStyle="text-align:right" name="newDeviation" id="newDeviation" required="true" value="${deviationList.deviation}" onchange="return chkBlank(this),traceData('new');;" cssClass="input-text" maxlength="8" size="20" onblur="return checkFloat(this)" />
	</display:column>
	</c:if>
</display:table>

<table class="detailTabLabel" border="0">
			<tbody>  	
				  <tr>
				  	<c:if test="${btn=='sell' }" >
				  		<td align="left" height="3px" colspan="4">Purchase Deviation<input type="checkbox" name="purchase" disabled="disabled"/></td>
		  			</c:if>
		  			<c:if test="${btn=='buy' }" >
				  		<td align="left" height="3px" colspan="4">Purchase Deviation<input type="checkbox" name="purchase" checked="true" disabled="disabled"/></td>
		  			</c:if>
		  		</tr>
		  	
		  	<tr>		  	
			  	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Add" onclick="addDeviationRow()" /> </td> 
			  	<td><input type="button" class="cssbutton" style="width:55px; height:25px"  value="Remove" onclick="delDeviationRow()"  /> </td> 
			  	<td><input type="button" class="cssbutton" style="width:55px; height:25px" value="Save" onclick="saveDeviationRow()" /> </td>
			</tr>
		 </tbody>
  </table>
  </div>
<c:if test="${saveBntType1=='yes'}"> 
<c:redirect url="/countryDeviations.html?charge=${charge1 }&contract=${contract1 }&btn=${btn}&deviationType=${btn}&decorator=popup&popup=true"/>
</c:if> 
   
</s:form>

<script language="javascript" >
function addDeviationRow(){
	var url = "addRowToDeviation.html?btn=${btn}&deviationType=${btn}&decorator=popup&popup=true";
	document.forms['countryDeviation'].action = url;
	document.forms['countryDeviation'].submit();
}

function saveDeviationRow(){
	var url = "saveDeviation.html?btn=${btn}&deviationType=${btn}&saveBntType=yes&decorator=popup&popup=true";
	document.forms['countryDeviation'].action = url;
	document.forms['countryDeviation'].submit();
}

function delDeviationRow(){
	var chckedVal="";
	var chkObj = document.forms['countryDeviation'].elements['deviationId'];
	if(chkObj.length>1){
	for(var i=0; i<chkObj.length; i++){
		if(chkObj[i].checked){
			chckedVal=chckedVal+","+chkObj[i].value;
		}
	}
	chckedVal = chckedVal.replace(chckedVal.charAt(0),'').trim();
	}
	else{
	if(document.forms['countryDeviation'].elements['deviationId'].checked){
		chckedVal=document.forms['countryDeviation'].elements['deviationId'].value
	 }
	} 
	if(chckedVal==''){
			alert('Please check value to remove.');
			return false;
	}else{
			document.forms['countryDeviation'].elements['delDeviationId'].value= chckedVal;
			var url = "delDeviation.html?btn=${btn}&deviationType=${btn}&decorator=popup&popup=true";
			document.forms['countryDeviation'].action = url;
			document.forms['countryDeviation'].submit();
	}
}

function traceData(szDivID){
		var dfd = document.forms['countryDeviation'].elements['checkData'].value;
		if(dfd == ''){
			document.forms['countryDeviation'].elements['checkData'].value = szDivID;
		}else{
			if(dfd.indexOf(szDivID) >-1){
			var splitDFD = dfd.split(',');
			var dfd = '';
			for(i = 0; i<splitDFD.length ; i++){
				if(splitDFD[i].indexOf(szDivID) > -1){
					splitDFD[i] = szDivID
				}
				if(dfd == ''){
					dfd = splitDFD[i];
				}else{
					dfd = dfd+','+splitDFD[i];
				}
			}
		}
		else{
			dfd=document.forms['countryDeviation'].elements['checkData'].value = dfd + ',' + szDivID;
		}
 			document.forms['countryDeviation'].elements['checkData'].value = dfd.replace( ',,' , ',' );
		}
}

function selectedCode(){
	 try{
		 <c:forEach items="${countryDeviationList}" var="tempDeviationList"> 
			var temp = '${tempDeviationList.id}';
			var tempCode= '${tempDeviationList.countryCode}';
			document.getElementById('countryCode'+temp).value=tempCode;
	  	</c:forEach>
		 }
	 catch(e){} 
} 
function chkBlank(target){
	var val= target.value;
	if(val==''){
		document.getElementById(target.id).value='0';
		document.getElementById(target.id).focus();
		alert('Please enter the deviation value.');
		return false;
	}
}
function checkFloat(temp)
  { 
    var check='';  
    var i;
    var dotcheck=0;
	var s = temp.value;
	s=s.trim();
	if(s=="." ||s=="" ){
	check='Invalid'; 
	}
	if(s.indexOf(".") == -1){
	dotcheck=eval(s.length)
	}else{
	dotcheck=eval(s.indexOf(".")-1)
	}
	var fieldName = temp.name;  
	var count = 0;
	var countArth = 0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if(c == '-')
        {
        	countArth = countArth+1
        }
        if((i!=0)&&(c=='-'))
       	{
       	  alert("Invalid data in Deviation%." ); 
          document.forms['countryDeviation'].elements[fieldName].select();
          document.forms['countryDeviation'].elements[fieldName].value='0';
       	  return false;
       	}
        	
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1'))) 
        {
        	check='Invalid'; 
        }
    }
    if(dotcheck>15){
    check='tolong';
    }
    if(check=='Invalid'){ 
    alert("Invalid data in Deviation%" ); 
    document.forms['countryDeviation'].elements[fieldName].select();
    document.forms['countryDeviation'].elements[fieldName].value='0';
    return false;
    } else if(check=='tolong'){ 
    alert("Data to long in Deviation%." ); 
     document.forms['countryDeviation'].elements[fieldName].select();
    document.forms['countryDeviation'].elements[fieldName].value='0';
    return false;
    } else{
    s=Math.round(s*100)/100;
    var value=""+s;
    if(value.indexOf(".") == -1){
    value=value+".00";
    } 
    if((value.indexOf(".")+3 != value.length)){
    value=value+"0";
    }
    document.forms['countryDeviation'].elements[fieldName].value=value;
    return true;
    }
    }
function checkReadOnly(){
    var chkCorpId= '${contracts.owner}';
    var corpId='${deviationCorpId}';
    if(corpId!=chkCorpId){
      var len = document.forms['countryDeviation'].length;
      for (var i=0 ;i<len ;i++){ 
    	  document.forms['countryDeviation'].elements[i].disabled = true;
      }
   }
}
</script>

<script language="javascript" type="text/javascript">
try{
	selectedCode();
}
catch(e){}
try{checkReadOnly();}
catch(e){}
</script>
  </body>
   