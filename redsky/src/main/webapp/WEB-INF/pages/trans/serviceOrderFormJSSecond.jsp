<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<SCRIPT LANGUAGE="JavaScript"> 
function getHTTPObjectInternalCostVendorCode() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var httpInternalCost = getHTTPObjectInternalCostVendorCode(); 
function findInternalCostVendorCode(aid){
	var vendorCode='vendorCode'+aid;
	var estimateVendorName='estimateVendorName'+aid;
	var actgCode='actgCode'+aid;
    var rev = document.forms['serviceOrderForm'].elements['category'+aid].value;
    var companyDivision = document.forms['serviceOrderForm'].elements['companyDivision'+aid].value; 
    if(rev == "Internal Cost"){
    	document.forms['serviceOrderForm'].elements[vendorCode].value=""; 
    	document.forms['serviceOrderForm'].elements[estimateVendorName].value="";  
	    var url="findInternalCostVendorCode.html?ajax=1&decorator=simple&popup=true&companyDivision="+encodeURI(companyDivision);
	    httpInternalCost.open("GET", url, true);
	    httpInternalCost.onreadystatechange = function(){ httpInternalCostVendorCode(vendorCode,estimateVendorName,actgCode);};
	    httpInternalCost.send(null);
   } }
function httpInternalCostVendorCode(vendorCode,estimateVendorName,actgCode) { 
    if (httpInternalCost.readyState == 4)  {
        var result= httpInternalCost.responseText
        result = result.trim();           
        if(result !=""){
            document.forms['serviceOrderForm'].elements[vendorCode].value= result;      
        	checkAccountInterface(vendorCode,estimateVendorName,actgCode);
         }  } }   
function autoCompleterAjaxCallDestCity(){
	var stateNameDes = document.forms['serviceOrderForm'].elements['serviceOrder.destinationState'].value;
	var countryName = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountry'].value;
	var cityNameDes = document.forms['serviceOrderForm'].elements['serviceOrder.destinationCity'].value;
	var countryNameDes = "";
	var countryList = JSON.parse('${jsonCountryCodeText}');
	var countryNameDes = getKeyByValue(countryList,countryName);
	if(cityNameDes!=''){
	var data = 'leadCaptureAutocompleteDestAjax.html?ajax=1&stateNameDes='+stateNameDes+'&countryNameDes='+countryNameDes+'&cityNameDes='+cityNameDes+'&decorator=simple&popup=true';
	$( "#destinationCity" ).autocomplete({				 
	      source: data		      
	    });
	}
}
function findQuotationForms(position) { 
	  var url="findQuotationFormsAtQuotesAjax.html?ajax=1&decorator=simple&popup=true&sid=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${serviceOrder.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&formReportFlag=F";
	  ajax_showTooltip(url,position);	
}
function validatefields1(id,val){
	var url='viewReportWithParam.html?formFrom=list&id='+id+'&fileType='+val+'&jobNumber=${serviceOrder.shipNumber}'+'&qFNumber=${serviceOrder.shipNumber}&preferredLanguage=${customerFile.customerLanguagePreference}';
	location.href=url;	  
}
function winClose1(id,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal,preferredLanguage){
	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&preferredLanguage=${customerFile.customerLanguagePreference}&custID="+custID;
	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
}
 function  findBookingAgent(temp) {
	 var msg='' ;
	   var validationForInv= ${oAdAWeightVolumeValidationForInv};
       var subOaDaValidationForInv = ${subOADAValidationForInv};
       var agentFlag=${ownBookingAgentFlag};
	   if(subOaDaValidationForInv == true || validationForInv == true){
	   if(validationForInv == true){
      	if('${trackingStatus.originAgentCode}'== null || '${trackingStatus.originAgentCode}'== ''){
      	if(msg==''){
     		msg = "Kindly fill Origin Agent" ;
     		}else{
     			msg=msg+", Origin Agent";
     		}
      	}
      	if('${trackingStatus.destinationAgentCode}'== null || '${trackingStatus.destinationAgentCode}'== ''){
      		if(msg==''){
      		msg = "Kindly fill Destination Agent" ;
      		}else{
      			msg=msg+", Destination Agent";
      		}
      	}
      	if('${miscellaneous.actualNetWeight}'== null || '${miscellaneous.actualNetWeight}'== ''){
      		if(msg==''){
          		msg = "Kindly fill Actual Net Weight";
          		}else{
          			msg=msg+", Actual Net Weight";
          		}
      	}
      	if('${miscellaneous.netActualCubicFeet}'== null || '${miscellaneous.netActualCubicFeet}'== ''){
      		if(msg==''){
          		msg = "Kindly fill Net Actual CubicFeet";
          		}else{
          			msg=msg+", Net Actual CubicFeet";
          		}
      	}}
   		 if(subOaDaValidationForInv == true){
		if(agentFlag == true){
	   if('${trackingStatus.originSubAgentCode}'== null || '${trackingStatus.originSubAgentCode}'== ''){
         	if(msg==''){
         		msg = "Kindly fill Sub Origin Agent" ;
         		}else{
         			msg=msg+", Sub Origin Agent";
         		}
         	}
         	if('${trackingStatus.destinationSubAgentCode}'== null || '${trackingStatus.destinationSubAgentCode}'== ''){
         		if(msg==''){
         		msg = "Kindly fill Sub Destination Agent" ;
         		}else{
         			msg=msg+", Sub Destination Agent";
         		}
         	}
			}}   
      	if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '' || ((subOaDaValidationForInv == true) &&(agentFlag == true) && ('${trackingStatus.originSubAgentCode}'== '' || '${trackingStatus.destinationSubAgentCode}'== ''))){
      		if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '')
  		 	{
  			var agree = confirm(msg+" details. To add those now click 'OK' otherwise click 'Cancel'.");
  		}else{
  			var agree = confirm(msg+" details. To add those now click 'OK' otherwise click 'Cancel' to continue invoicing.");
  			}
  		if(agree){		      			
  			var url = "editTrackingStatus.html?id=${serviceOrder.id}";
  			location.href = url;
  			return false;
  	}else{
  		if('${trackingStatus.originAgentCode}'== '' || '${trackingStatus.destinationAgentCode}'== '' || '${miscellaneous.actualNetWeight}'== '' || '${miscellaneous.netActualCubicFeet}'== '')
			{
  				return false;
  	} 	}} } 
	if(temp=='postingDate'){
	          <c:if test="${accountInterface=='Y'}">
	           <configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
		      	    <c:set var="closedCompanyDivision" value="Y" />
		          </configByCorp:fieldVisibility>
		            <c:if test="${closedCompanyDivision=='Y'}">
		              findAccountCompanyDivisionForPayPost();
		       </c:if> 
	          <c:if test="${closedCompanyDivision!='Y'}">	
	           var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
	           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
	           http8.open("GET", url, true);
	           http8.onreadystatechange = handleHttpResponse1488;
	           http8.send(null); 
	          </c:if>
	           </c:if> 
	          <c:if test="${accountInterface=='N'||accountInterface==''}">
	            findPayableStatus();
	          </c:if>
	    }
        if(temp=='invoiceGen'){
	        var CWMSCommission=true; 
        	<configByCorp:fieldVisibility componentId="component.button.CWMS.commission">
	        CWMSCommission=false;
	        <c:if test="${commissionJobName == 'Yes'}">
        	 <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
        		<c:if test="${empty compDivForBookingAgentList}"> 
        		findAccountCompanyDivision();
        		</c:if>
        		<c:if test="${not empty compDivForBookingAgentList}">
        		if('${fn:length(serviceOrder.salesMan)}' == 0){
	           		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
	           		return false;
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
	           		alert('Billing Person & Forwarder is not assigned to this order.');
	           		return false;		           		
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' != 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
	           		alert('Billing Person is not assigned to this order.');
	           		return false;		           		
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' != 0))){                        			           	
	           		alert('Forwarder is not assigned to this order.');
	           		return false;		           		
	           	}else{
	           		findAccountCompanyDivision(); 
		        }
        		</c:if>
        	 </c:if>
        	 <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
        		if('${fn:length(serviceOrder.salesMan)}' == 0){
	           		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
	           		return false;
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
	           		alert('Billing Person & Forwarder is not assigned to this order.');
	           		return false;		           		
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' != 0)&&('${fn:length(billing.personBilling)}' == 0))){                        			           	
	           		alert('Billing Person is not assigned to this order.');
	           		return false;		           		
	           	}else if((('${serviceOrder.companyDivision}'=='AIF')||('${serviceOrder.companyDivision}'=='CAJ'))&&(('${fn:length(trackingStatus.forwarderCode)}' == 0)&&('${fn:length(billing.personBilling)}' != 0))){                        			           	
	           		alert('Forwarder is not assigned to this order.');
	           		return false;		           		
	           	}else{
	           		findAccountCompanyDivision(); 
		        }
           	</c:if>
           	</c:if>
           	<c:if test="${commissionJobName != 'Yes'}">
           	findAccountCompanyDivision(); 
           	</c:if>
  		</configByCorp:fieldVisibility>
  		<configByCorp:fieldVisibility componentId="component.button.commission.automation">
        CWMSCommission=false;
        <c:if test="${commissionJobName == 'Yes'}">
    	 <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
    		<c:if test="${empty compDivForBookingAgentList}"> 
    		findAccountCompanyDivision();
    		</c:if>
    		<c:if test="${not empty compDivForBookingAgentList}">
    		if('${fn:length(serviceOrder.salesMan)}' == 0){
           		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
           		return false;
           	}else{
           		findAccountCompanyDivision(); 
	           	}
    		</c:if>
    	 </c:if>
    	 <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}">
   		if('${fn:length(serviceOrder.salesMan)}' == 0){
       		alert('Sales Person is not assigned to this order.\n Please select in SO Detail to proceed further.');
       		return false;
       	}else{
       		findAccountCompanyDivision(); 
           	}
       	</c:if>
       	</c:if>
       	<c:if test="${commissionJobName != 'Yes'}">
       	findAccountCompanyDivision(); 
       	</c:if>
		</configByCorp:fieldVisibility>
  		 if(CWMSCommission){ 
	        findAccountCompanyDivision(); 
  		 }  }  }
    var httpVatExclude=getHTTPObject77(); 
    var http999 = getHTTPObject77(); 			
 var http666= getHTTPObject77(); 
 var http8 = getHTTPObject77(); 
 var http7 = getHTTPObject77(); 
 var httpCMMAgent = getHTTPObject77(); 	
 var httpCMMAgentInvoiceCheck =	getHTTPObject77(); 
 var httpCheckZeroforInvoice = getHTTPObject77();
    function handleHttpResponse1488() {
         var bookingagentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
         if (http8.readyState == 4)
         {
            var results = http8.responseText
            results = results.trim();
            results = results.replace('[','');
            results=results.replace(']','');  
            if(results=='0')
            {
             alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.")
            } else if(results!='0') {
            findPayableStatus();
            } } } 
       function  findAccountCompanyDivisionForPayPost() {
      	 progressBarAutoSave('1');
          var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
         var url="findAccountCompanyDivision.html?ajax=1&companyDivisionPayPostFlag=Y&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
         http88.open("GET", url, true);
         http88.onreadystatechange = handleHttpResponseAccountCompanyDivisionForPayPost;
         http88.send(null);
       }
     function handleHttpResponseAccountCompanyDivisionForPayPost(){
      if (http88.readyState == 4)  {
              var results = http88.responseText
              results = results.trim();
              results = results.replace('[','');
              results=results.replace(']',''); 
              if(results.length>2)  { 
              	var checkCloseCompanyDivision=false;
              	var closeDivision="";
                var res = results.split("@"); 
                for (i=0;i<res.length;i++){ 
              	  if(!checkCloseCompanyDivision){
              	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
              		  checkCloseCompanyDivision=true;
              		  closeDivision=res[i];
              	  }   }  }
                if(checkCloseCompanyDivision){
              	  alert("Payable cannot be posted to accounting as the company division "+closeDivision+" has been closed ");
              	  progressBarAutoSave('0');
                }else{
                   var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }
                } else{
                	var serviceOrderId = '${serviceOrder.id}';
		           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
		           http8.open("GET", url, true);
		           http8.onreadystatechange = handleHttpResponse1488;
		           http8.send(null); 
                }  }  }  
    function  findPayableStatus() { 
    var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    var url="payableStatusList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    http7.open("GET", url, true);
    http7.onreadystatechange = handleHttpResponse148;
    http7.send(null);  
    }
    function handleHttpResponse148() {
            if (http7.readyState == 4)  {
               var results = http7.responseText
               results = results.trim(); 
               var res = results.split("#");  
               if(res[0]=='No')  { 
               	if(res[1]=='0') 	{
               		findPayPostDate();
               	}
               	else if(res[2]=='0')  	{
               	    findPayPostDate();
               	}
               	else {
                		alert("The status is not approved ");
                	}
				}
                else if(results=='Yes')  { 
                	findPayPostDate();
                }  } }		        
    function findPayPostDate()  {
    	 	var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    	     var url="payPostDateList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId);
    	     http2.open("GET", url, true);
    	     http2.onreadystatechange = handleHttpResponse115;
    	     http2.send(null); 
    	  }
    	  function handleHttpResponse115() { 
    	             if (http2.readyState == 4) {
    	                var results = http2.responseText
    	                results = results.trim();
    	                results = results.replace('[','');
    	                results=results.replace(']','');  
    	                if(results.length>5)  { 
    	                 findPayableInvoice(); 
    	 				} else {
    	                   alert("No Payable lines found for Posting"); 
    	                   progressBarAutoSave('0');
    	                 }   }   }
    	  function findPayableInvoice() { 
    		  var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    		     var url="payableInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    		     http3.open("GET", url, true);
    		     http3.onreadystatechange = handleHttpResponse126;
    		     http3.send(null); 
    		  } 
    		  function handleHttpResponse126() { 
    		             if (http3.readyState == 4) {
    		                var results = http3.responseText
    		                results = results.trim();
    		                results = results.replace('[','');
    		                results=results.replace(']','');  
    		                if(results.length>5)
    		                { 
    		                 alert("There are payables with missing vendor invoice#");
    		 				} else {
    		                   findPayableVendor(); 
    		                 }  }   } 	
    		  function findPayableVendor() { 
    			     var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			     var url="payableVendorList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    			     http4.open("GET", url, true);
    			     http4.onreadystatechange = handleHttpResponse136;
    			     http4.send(null); 
    			  } 
    			  function handleHttpResponse136() { 
    			             if (http4.readyState == 4)  {
    			                var results = http4.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']','');   
    			                if(results.length>2)
    			                { 
    			                 alert("The vendor code is missing");
    			 				} else {
    			                 findPayableCharge();
    			                 }   }  }
    			  	function findPayableCharge() 
    			    { 
    			  		var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			     var url="payableChargeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    			     http5.open("GET", url, true);
    			     http5.onreadystatechange = handleHttpResponse146;
    			     http5.send(null); 
    			  }   
    			  function handleHttpResponse146()  { 
    			             if (http5.readyState == 4)  {
    			                var results = http5.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']','');  
    			                if(results.length>2)
    			                { 
    			                 alert("The Charge code is missing");
    			 				}
    			                 else
    			                 { 
    			                 	findPayableGl();  
    			                 }  }    }  
    			  function findPayableGl()    { 
    				  var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			     var url="payableGLCodeList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    			     http6.open("GET", url, true);
    			     http6.onreadystatechange = handleHttpResponse147;
    			     http6.send(null); 
    			  }   
    			    function handleHttpResponse147()  { 
    			             if (http6.readyState == 4)  {
    			                var results = http6.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']','');   
    			                if(results.length>2)
    			                { 
    			                 alert("The GL code is missing in the 'Payable Detail' of 'Accounting Transfer Information' section");
    			 				}
    			                 else
    			                 { 
    			                 	var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			                	var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			                    document.forms['serviceOrderForm'].elements['buttonType'].value = "invoice";
    			                    if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){
    			                     var agree = confirm("The billing has been completed, do you want to still post accountlines?");
    			                      if(agree)
    			                      {
    			                             openWindow('findPayPostDateForPricing.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
    			                      }else { 
    			                        }
    			                     }else{
    			                     openWindow('findPayPostDateForPricing.html?sid='+sid+'&decorator=popup&popup=true',900,300); 
    			                     }   }   }    }	 
    			    function  findAccountCompanyDivision() {
    			    	progressBarAutoSave('1');
    			    		var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			           var url="findAccountCompanyDivision.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId)+"&billingCMMContractType=${billingCMMContractType}"; 
    			           http88.open("GET", url, true);
    			           http88.onreadystatechange = handleHttpResponseAccountCompanyDivision;
    			           http88.send(null);
    			         }
    			       function handleHttpResponseAccountCompanyDivision(){
    			        if (http88.readyState == 4)   {
    			                var results = http88.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']','');   
    			                if(results.length>2)
    			                {
    			                	var checkCloseCompanyDivision=false;
    			                	 var res = results.split("@");
    			                	<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
    			                	  var closeDivision="";
    			                      
    			                      for (i=0;i<res.length;i++){ 
    			                	  if(!checkCloseCompanyDivision){
    			                	  if(res[i] !='' && (('${closeCompanyDivision}').indexOf(res[i])>=0)){ 
    			                		  checkCloseCompanyDivision=true;
    			                		  closeDivision=res[i];
    			                	  }
    			                      }
    			                      }
    			                    </configByCorp:fieldVisibility>
    			                  if(checkCloseCompanyDivision){
    			                	  alert("Cannot generate invoice to accounting as the company division "+closeDivision+" has been closed ");
    			                	  progressBarAutoSave('0');
    			                  }else{
    			                  var res = results.split("@");  
    			                  if(res.length == 2)
    			                  {
    			                  document.forms['serviceOrderForm'].elements['companyDivisionForTicket'].value=res[1];    
    			                  <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
    					           var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    					           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    					           http8.open("GET", url, true);
    					           http8.onreadystatechange = handleHttpResponse1489;
    					           http8.send(null); 
    					          </c:if>
    					          <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
    					           var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    					           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    					           http8.open("GET", url, true);
    					           http8.onreadystatechange = handleHttpResponse1499;
    					           http8.send(null); 
    					          </c:if>
    			                  }
    			                  else{
    			                	  <c:if test="${singleCompanyDivisionInvoicing}">
    			                	  alert("Cannot generate invoice as multiple company divisions found for invoicing, please fix this.");
    			                	  progressBarAutoSave('0');
    			                	  </c:if>
    			                	  <c:if test="${!singleCompanyDivisionInvoicing}">	  
    			                      var agree = confirm("The new invoice has different company divisions, do you want to still proceed and put these in the same invoice #?");
    			                      if(agree)
    			                      {
    			                          <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
    						                  var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    						                  var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    						                  http8.open("GET", url, true);
    						                  http8.onreadystatechange = handleHttpResponse1489;
    						                  http8.send(null); 
    					                  </c:if>
    					                  <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
    								           var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    								           var url="findBookingAgent.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    								           http8.open("GET", url, true);
    								           http8.onreadystatechange = handleHttpResponse1499;
    								           http8.send(null); 
    							          </c:if>
    			                      }else { 
    			                    	  progressBarAutoSave('0');
    			                        }
    			                	  </c:if>
    			                  }
    			                  }
    			                  } else{
    			                   alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
    			                   progressBarAutoSave('0');
    			                  }
    			       }
    			       }	 

    			       function handleHttpResponse1489() {
    			             var bookingagentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    			             if (http8.readyState == 4)
    			             {
    			                var results = http8.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']',''); 
    			                if(results=='0')
    			                {
    			                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.");
    			                 progressBarAutoSave('0');
    			                }
    			                else if(results!='0')
    			                {
    			                 <c:if test="${multiCurrency!='Y'}"> 
    			                   findGenerateInvoice();
    			                 </c:if> 
    			                 <c:if test="${multiCurrency=='Y'}"> 
    			                   findbillingCurrency();
    			                 </c:if>
    			                }
    			             }
    			        }
    			        function handleHttpResponse1499() {
    			             var bookingagentCode = document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value;
    			             if (http8.readyState == 4)
    			             {
    			                var results = http8.responseText
    			                results = results.trim();
    			                results = results.replace('[','');
    			                results=results.replace(']',''); 
    			                if(results=='0')
    			                {
    			                 alert("The Booking Agent Code "+bookingagentCode+" in Service Order is not valid, please correct this before proceeding.");
    			                 progressBarAutoSave('0');
    			                }
    			                else if(results!='0')
    			                {
    			                <c:if test="${multiCurrency!='Y'}"> 
    			                  findDistAmtGenerateInvoice();
    			                </c:if> 
    			                <c:if test="${multiCurrency=='Y'}"> 
    			                  findbillingCurrency();
    			                 </c:if> 
    			                }
    			             }
    			        }

    			        function findbillingCurrency(){
    			            var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			            var url="findbillingCurrency.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
    			            http33.open("GET", url, true);
    			            http33.onreadystatechange = handleHttpResponseCurrency;
    			            http33.send(null);
    			           }
    			            
    			            function handleHttpResponseCurrency(){ 
    			             if (http33.readyState == 4)
    			                 {
    			                    var results = http33.responseText 
    			                    results = results.trim();  
    			                    if(results.length>2)
    			                    { 
    			                      var res = results.split("@");
    			                      if(res.length == 2)
    			                      { 
    			                         <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
    			                          findGenerateInvoice();
    			                         </c:if>
    			                        <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    			                          findDistAmtGenerateInvoice(); 
    			                        </c:if> 
    			                      } else {
    			                    	  <c:choose>
    			                    	  <c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
    			                    	  <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
    			                             findGenerateInvoice();
    			                          </c:if>
    			                          <c:if test="${fn:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
    			                             findDistAmtGenerateInvoice(); 
    			                          </c:if> 
    			                    	 </c:when>
    			    	              	<c:otherwise>
    			                      	 alert("Invoice cannot be generated as the Billing currency of all the account line(s) do not match.");
    			                      	progressBarAutoSave('0');
    			                      	</c:otherwise>
    			    	                </c:choose>
    			                      }  
    			     				} else  { 
    			     					alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
    			     					progressBarAutoSave('0');
    			                     }
    			                 }
    			            }

    			            function findGenerateInvoice(){
    			                var billToCode = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			                 var url="actualInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(billToCode);
    			                 http3.open("GET", url, true);
    			                 http3.onreadystatechange = handleHttpResponse141;
    			                 http3.send(null);
    			            }

    			            function handleHttpResponse141() { 
    			                         if (http3.readyState == 4)
    			                         {
    			                            var results = http3.responseText 
    			                            results = results.trim();  
    			                            
    			                            if(results.length>2){
    			                            <c:choose>
    			                          	<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
    			                          	findCMMAgentBilltoCode();
    			                          	</c:when>
    			                      		<c:otherwise>    
    			                              var res = results.split("@"); 
    			                              if(res.length == 2){  
    			                                document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value=res[1];
    			                                findBookingAgentNameGen(res[1]); 
    			                              } else {
    			                            	  alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.");
    			                            	  progressBarAutoSave('0');
    			                              }
    			                              </c:otherwise>
    			                            	</c:choose> 
    			             				} else  { 
    			                               alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.'); 
    			                               progressBarAutoSave('0'); 
    			             				}
    			                         }
    			                    }

    			            function findDistAmtGenerateInvoice(){ 
    			                var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			                 var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
    			                 http3.open("GET", url, true);
    			                 http3.onreadystatechange = handleHttpResponse1114;
    			                 http3.send(null);
    			            }

    			            function handleHttpResponse1114()
    			                    { 
    			                     if (http3.readyState == 4)
    			                         {
    			                            var results = http3.responseText 
    			                            results = results.trim();   
    			                            if(results.length>2) {
    			                            	<c:choose>
    			                              	<c:when test="${trackingStatus.accNetworkGroup && billingCMMContractType}"> 
    			                              	findCMMAgentBilltoCode();
    			                              	</c:when>
    			                          		<c:otherwise>  
    			                              var res = results.split("@");
    			                              if(res.length == 2)  {  
    			                                document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value=res[1]; 
    			                                findDistAmtBookAgent(res[1]); 
    			                              }  else  {
    			                            	  alert("Cannot generate invoice as Multiple Bill Tos found for invoicing, please fix this.");
    			                            	  progressBarAutoSave('0');
    			                              }
    			                              </c:otherwise>
    			                          	  </c:choose>  
    			             				}  else  { 
    			                               alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
    			                               progressBarAutoSave('0');
    			                             }
    			                         }
    			                    }		        			            
    			            function findCMMAgentBilltoCode(){
    			       	     var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			       	     var url="findCMMAgentBilltoCode.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
    			       	     httpCMMAgent.open("GET", url, true);
    			       	     httpCMMAgent.onreadystatechange = CMMAgentBilltoCodeResponse;
    			       	     httpCMMAgent.send(null);
    			         }      
    			         function CMMAgentBilltoCodeResponse(){
    			       	  if (httpCMMAgent.readyState == 4)
    			             {
    			                var results = httpCMMAgent.responseText 
    			                results = results.trim(); 
    			                var res = results.split("#"); 
    			                if(res[0]!='0'){
    			               	 if(res[0] == '1'){
    			          				if(res[1] == '1'){
    			            	  			document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value=res[2];
    			            	  			findBookingAgentNameGen(res[2]); 
    			            	  		}else{
    			            	  			alert('Cannot generate invoice as Multiple Currency Found.');
    			            	  			progressBarAutoSave('0');
    			            	  		}	
    			          			}else{
    			              			alert('Cannot generate invoice as Multiple Bill to Found.');
    			              			progressBarAutoSave('0');
    			          			} 
    			                }else  { 
    			                    alert('Nothing found to invoice Or Not able to generate an invoice for the chosen Bill To.');
    			                    progressBarAutoSave('0');
    			                }
    			             }
    			         } 

    			         function findBookingAgentNameGen(target){ 
    			             var billToCode=target; 
    			             var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			             var url="accountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
    			             http2.open("GET", url, true);
    			             http2.onreadystatechange = handleHttpResponse414;
    			             http2.send(null);
    			        }
    			         function handleHttpResponse414() { 
    			        	    if (http2.readyState == 4){
    			        	       var results = http2.responseText
    			        	       results = results.trim();
    			        	       results = results.replace('[','');
    			        	       results=results.replace(']','');     
    			        	       var res = results.split("@");  
    			        	       if(res[1].trim()==''){ 
    			        		         var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			        		         var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			        		         if(res[4].trim()!=''){
    			        		       	  	alert("Cannot Invoice, as Bill to Code is missing in line # "+res[4]); 
    			        		         }else{
    			        				   alert("Cannot Invoice, as Bill to Code is missing");
    			        		   		 } 	
    			        		         progressBarAutoSave('0');
    			        		      }
    			        		      else if(res[2].trim()==''){
    			        					if(res[4].trim()!=''){
    			        		             	  alert("Cannot Invoice, as Charge Code is missing in line # "+res[4]); 
    			        		             }else{
    			        				 		  alert("Cannot Invoice, as Charge Code is missing");
    			        		             }
    			        					progressBarAutoSave('0');
    			        				 }
    			        			     <c:if test="${accountInterface=='Y'}">
    			        					else if(res[3].trim()==''){
    			        						if(res[4].trim()!=''){
    			        			            	  alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[4]); 
    			        			            }else{
    			        							  alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
    			        			       	    }
    			        						progressBarAutoSave('0');
    			        				   }
    			        				</c:if>																													
    			        			    else{ 
    			        		        //findBillToAuthority();
    			        			    getBilltoCodeforGenerateInvoice();
    			        		      }
    			        	    }
    			        	}

    			         function findDistAmtBookAgent(target){  
    			             var billToCode=target; 
    			             var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			             var url="distAmtAccountList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(target);
    			             http2.open("GET", url, true);
    			             http2.onreadystatechange = function(){handleHttpResponse444412(target);} ;
    			             http2.send(null);
    			        }
    			        function handleHttpResponse444412(target)
    			                {

    			                     if (http2.readyState == 4)
    			                     {
    			                        var results = http2.responseText
    			                        results = results.trim();
    			                        results = results.replace('[','');
    			                        results=results.replace(']','');  
    			                        var res = results.split("@");  
    			                        if(res[1].trim()=='')  { 
    			                          var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			                          var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			                          if(res[6].trim()!=''){
    			                        	  alert("Cannot Invoice, as Bill to Code is missing in line # "+res[6]);  
    			                              }else{
    			         				   alert("Cannot Invoice, as Bill to Code is missing"); 	
    			                              }	
    			                          progressBarAutoSave('0');
    			         				} else if(res[2].trim()=='')
    			         				{
    			         					if(res[6].trim()!=''){
    			         	                	  alert("Cannot Invoice, as Charge Code is missing in line # "+res[6]);  
    			         	                      }else{
    			         				         alert("Cannot Invoice, as Charge Code is missing");
    			         	                      }
    			         					progressBarAutoSave('0');
    			         				}
    			                        <c:if test="${accountInterface=='Y'}">
    			         				else if(res[3].trim()=='')
    			         				{
    			         					if(res[6].trim()!=''){
    			        	                	  alert("Cannot Invoice, as GL Code of Receivable Detail is missing in line # "+res[6]);  
    			        	                      }else{
    			         				 alert("Cannot Invoice, as GL Code is missing in the 'Receivable Detail' of 'Accounting Transfer Information' section");
    			        	                      }
    			         					progressBarAutoSave('0');
    			          				} 
    			         				</c:if>
    			         				else if(res[4].trim()!=res[5].trim())
    			         				{
    			         				   alert("Actual revenue total ("+ res[4]+") and distribution amount total ("+res[5]+") are not same. So, cannot generate invoice.");
    			         				  progressBarAutoSave('0');
    			         				}
    			                        else
    			                         {
    			                            //findBillToAuthority();
    			                        	checkZeroAmountForInvoice(target);
    			                         }
    			                     }
    			                }
    			        function findBillToAuthority(){ 
    			            var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			            var  billToCodeForTicket=document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value
    			            var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			            var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			            var url="findBillToAuthority.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
    			            http666.open("GET", url, true);
    			            http666.onreadystatechange = handleHttpResponseBillToAuthority;
    			            http666.send(null);
    			       }

    			       function handleHttpResponseBillToAuthority() { 
    			                    if (http666.readyState == 4)
    			                    {
    			                       var results = http666.responseText 
    			                       results = results.trim(); 
    			                       results = results.replace('[','');
    			                       results=results.replace(']','');  
    			                      
    			                       if(results==0) { 
    			                           document.forms['serviceOrderForm'].elements['buttonType'].value = "invoice";
    			                           <c:choose>
    			                           <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
    			                            var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
   			                                if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
   			                                findBillToAuthState();
   			                                }else{
   			                                 validateChargeVatExclude(); 
   			                                }
    			                           </c:when>
    			                           <c:otherwise>
    			                           if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){
    			                            alert("Billing has been marked as complete for this service order.");
    			                            progressBarAutoSave('0');
    			                            }else{
    			                             var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			                            if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
    			                            findBillToAuthState();
    			                            }else{
    			                            validateChargeVatExclude(); 
    			                           }
    			                           }
    			                           </c:otherwise>
    			                           </c:choose>
    			                       } else if(results!=0){
    			                       var  billToCodeForTicket=document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value
    			                       var  billToAuthority=document.forms['serviceOrderForm'].elements['billing.billToAuthority'].value
    			                       if(billToAuthority==''){
    			                       alert("The following account "+billToCodeForTicket+" does not have authorization #, So Invoicing cannot be done.");
    			                       progressBarAutoSave('0');
    			                       } else if(billToAuthority!=''){
    			                    	   <c:choose>
    			                    	   <c:when test="${!trackingStatus.accNetworkGroup && billingCMMContractType && trackingStatus.soNetworkGroup }">
    			                    	    var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
   			                                if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
   			                                findBillToAuthState();
   			                                }else{
   			                           	      validateChargeVatExclude(); 
   			                                }
    			                    	   </c:when>
    			                    	   <c:otherwise>
    			                           if(document.forms['serviceOrderForm'].elements['billing.billComplete'].value!=''){
    			                            alert("Billing has been marked as complete for this service order.");
    			                            progressBarAutoSave('0');
    			                            }else{
    			                            var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			                            if(jobtypeForTicket=='STO'||jobtypeForTicket=='TPS'||jobtypeForTicket=='STL'){
    			                            findBillToAuthState();
    			                            }else{
    			                           	 validateChargeVatExclude(); 
    			                           }
    			                           }
    			                           </c:otherwise>
    			                           </c:choose> 
    			                       }
    			                       }
    			                    }
    			               }
    			       function findBillToAuthState(){ 
    			    	     var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			    	     var  billToCodeForTicket=document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value
    			    	     var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			    	     var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			    	     var url="findBillToAuthState.html?ajax=1&decorator=simple&popup=true&billToCodeForTicket="+billToCodeForTicket+"&sid=" + encodeURI(sid);
    			    	     http999.open("GET", url, true);
    			    	     http999.onreadystatechange = handleHttpResponseBillToAuthState;
    			    	     http999.send(null);
    			    	}

    			    	function handleHttpResponseBillToAuthState() { 
    			    	             if (http999.readyState == 4)
    			    	             {
    			    	                var results = http999.responseText 
    			    	                results = results.trim(); 
    			    	                results = results.replace('[','');
    			    	                results=results.replace(']','');   
    			    	                if(results!='STATE'&& results!='state') { 
    			    	                	 validateChargeVatExclude(); 
    			    	                 } else if(results=='STATE'|| results=='state'){
    			    	                    alert("WARNING: Confirm that access was not billed on export authorization.")
    			    	                    validateChargeVatExclude(); 
    			    	                    }
    			    	                }
    			    	}
    			        function validateChargeVatExclude(){
    			     	   
    			     	   <c:if test="${systemDefaultVatCalculation == 'true'}">
    			     	     var serviceOrderId = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			              var url="validateChargeVatExclude.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(serviceOrderId); 
    			              httpVatExclude.open("GET", url, true);
    			              httpVatExclude.onreadystatechange = handleHttpResponseVatExclude;
    			              httpVatExclude.send(null);
    			        	  </c:if>
    			        	<c:if test="${systemDefaultVatCalculation != 'true'}">
    			        	   var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			            var  billToCodeForTicket=document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value
    			            var  companyDivisionForTicket=document.forms['serviceOrderForm'].elements['companyDivisionForTicket'].value
    			      	   var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			     	   var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			            document.forms['serviceOrderForm'].elements['buttonType'].value = "invoice"; 
    			            openWindow('activeWorkTicketsForPricing.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&companyDivisionForTicket='+companyDivisionForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
    			            </c:if>
    			        }
    			        function  handleHttpResponseVatExclude(){
    			     	   if (httpVatExclude.readyState == 4)
    			            {
    			                
    			               var results = httpVatExclude.responseText 
    			               results = results.trim();
    			               results = results.replace('[','');
    			               results=results.replace(']',''); 
    			               if(results!='')
    			               {
    			             	  alert('Please enter Receivable VAT for the Account line No. '+ results);
    			             	 progressBarAutoSave('0');
    			               }else{
    			             	  var  jobtypeForTicket=document.forms['serviceOrderForm'].elements['serviceOrder.job'].value
    			                   var  billToCodeForTicket=document.forms['serviceOrderForm'].elements['billToCodeForTicket'].value
    			                   var  companyDivisionForTicket=document.forms['serviceOrderForm'].elements['companyDivisionForTicket'].value
    			             	  var shipNumberForTickets=document.forms['serviceOrderForm'].elements['serviceOrder.shipNumber'].value;
    			            	      var sid=document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
    			                   document.forms['serviceOrderForm'].elements['buttonType'].value = "invoice"; 
    			                   openWindow('activeWorkTicketsForPricing.html?ajax=1&buttonType=invoice&shipNumberForTickets='+shipNumberForTickets+'&sid='+sid+'&billToCodeForTicket='+billToCodeForTicket+'&companyDivisionForTicket='+companyDivisionForTicket+'&jobtypeForTicket='+jobtypeForTicket+'&decorator=popup&popup=true',900,500);
    			               }
    			        }
    			        }

    			        function showBillingDatails(id,type,position) {
    			        	var url="showBillingDatailsFields.html?ajax=1&decorator=simple&popup=true&aid=" + encodeURI(id)+"&showBillingType=" + encodeURI(type);
    			        	ajax_showTooltip(url,position);	
    			        	 var containerdiv =  document.getElementById("para1").scrollLeft;
    			    	  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
    			    	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	 
    			        	  }
    			        
    			        function findUserPermission1(name,AccDivision,position) { 
    			        	  var accountLinecompanyDivision=AccDivision;
    			        	  var url="reportBySubModuleAccountLineList1.html?ajax=1&decorator=simple&popup=true&recInvNumb=" + encodeURI(name)+"&id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode =${billing.billToCode}&accountLinecompanyDivision="+accountLinecompanyDivision+"&reportModule=serviceOrder&reportSubModule=Accounting";
    			        	  ajax_showTooltip(url,position);	
    			        	  }
    			        function openReport(id,claimNumber,invNum,jobNumber,bookNumber,reportName,docsxfer,jobType){
    			        	window.open('viewFormParam.html?id='+id+'&invoiceNumber='+invNum+'&claimNumber='+claimNumber+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&bookNumber='+bookNumber+'&noteID=${noteID}&custID=${custID}&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
    			         }
    			        function validatefields(id,invNum,jobNumber,reportName,docsxfer,jobType,val){
    			        	<c:set var="checkfromfilecabinetinvoicereport" value="N"/>
    			        	<configByCorp:fieldVisibility componentId="component.accountline.Invoice.FileCabinetInvoiceReport">
    			        		<c:set var="checkfromfilecabinetinvoicereport" value="Y"/>
    			        	</configByCorp:fieldVisibility>
    			        	var url="";
    			        	<c:choose>
    			        	<c:when test="${checkfromfilecabinetinvoicereport=='Y'}">   	
    			        	url='viewReportWithParam.html?formFrom=list&id='+id+'&invoiceNumber='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'&checkfromfilecabinetinvoicereportflag=Y';
    			        	</c:when>
    			        	<c:otherwise>
    			        	 url='viewReportWithParam.html?formFrom=list&id='+id+'&invoiceNumber='+invNum+'&cid=${customerFile.id}&jobType='+jobType+'&jobNumber='+jobNumber+'&fileType='+val+'&reportModule=serviceOrder&reportSubModule=Accounting&reportName='+reportName+'&docsxfer='+docsxfer+'';
    			        	</c:otherwise>
    			        	</c:choose>
    			        	location.href=url;	  
    			        }
    			        function winClose(id,invNum,claimNumber,cid,jobNumber,regNumber,bookNumber,noteID,custID,emailOut,reportModule,reportSubModule,formReportFlag,formNameVal){
    			        	var url = "viewFormParamEmailSetup.html?decorator=popup&popup=true&id="+id+"&invoiceNumber="+invNum+"&jobNumber="+jobNumber+"&regNumber="+regNumber+"&docsxfer="+emailOut+"&reportModule="+reportModule+"&reportSubModule="+reportSubModule+"&formReportFlag="+formReportFlag+"&formNameVal="+formNameVal+"&claimNumber="+claimNumber+"&cid="+cid+"&bookNumber="+bookNumber+"&noteID="+noteID+"&custID="+custID;
    			        	window.open(url,'accountProfileForm','height=650,width=750,top=1,left=200, scrollbars=yes,resizable=yes').focus();
    			        }
    			        /*     # 9770 - Generating invoice and posting payables from the Pricing screen              End                */		   

function selectAllRollInvoice(str){
	var id = idList.split(",");
	  for (i=0;i<id.length;i++){
		  if(str.checked == true){
			  document.forms['serviceOrderForm'].elements['rollUpInInvoice'+id[i].trim()].checked = true;
		  }else{
			  document.forms['serviceOrderForm'].elements['rollUpInInvoice'+id[i].trim()].checked = false;
		  }
	  }
}
    			        
function checkGroupAgeJobType(){
    try{
  	var job=document.getElementById('jobService').value;
 		if(job=='GRP'){
			document.getElementById('serviceOrder.grpPref').checked = true;
		}else{
			document.getElementById('serviceOrder.grpPref').checked = false;
		}
    }catch(e){}
}
//****************************************//
function calculateEstimateByChargeCodeBlock(aid,type1){
    //Code For estimate Est	 
	 estimateBuyByChargeCodeFX(aid);
    //Code For estimate Sell	 
	  <c:if test="${multiCurrency=='Y'}">
	  estimateSellByChargeCodeFX(aid);
	  </c:if>
	  //Code For Estimate Pass Percentage   
	  try{
		  calculateestimatePassPercentage('estimateRevenueAmount'+aid,'estimateExpense'+aid,'estimatePassPercentage'+aid);
	  }catch(e){}
	  // Code Gross Section
	  	var estimatedTotalRevenue=0.00;
		var estimatedTotalExpense=0.00;
		var tempEstimatedTotalExpense=0.00;
		var tempEstimatedTotalRevenue=0.00;
		var estimatedGrossMargin=0.00;
		var estimatedGrossMarginPercentage=0.00;
		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			if(document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].value!=""){
	 			tempEstimatedTotalRevenue=document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+accArr[g]].value;
			}
	 		estimatedTotalRevenue=estimatedTotalRevenue+parseFloat(tempEstimatedTotalRevenue);
	 		if(document.forms['serviceOrderForm'].elements['estimateExpense'+accArr[g]].value!=""){
	 			tempEstimatedTotalExpense=document.forms['serviceOrderForm'].elements['estimateExpense'+accArr[g]].value;
	 		}
	 		estimatedTotalExpense=estimatedTotalExpense+parseFloat(tempEstimatedTotalExpense);
		}
	estimatedGrossMargin=estimatedTotalRevenue-estimatedTotalExpense;
	try{
		if((estimatedTotalRevenue==0)||(estimatedTotalRevenue==0.0)||(estimatedTotalRevenue==0.00)){
			estimatedGrossMarginPercentage=0.00;
		}else{
			estimatedGrossMarginPercentage=(estimatedGrossMargin*100)/estimatedTotalRevenue;
		}
	}catch(e){
		estimatedGrossMarginPercentage=0.00;
		}
	 <c:if test="${not empty serviceOrder.id}">
	document.forms['serviceOrderForm'].elements['estimatedTotalRevenue'+${serviceOrder.id}].value=Math.round(estimatedTotalRevenue*10000)/10000;
	document.forms['serviceOrderForm'].elements['estimatedTotalExpense'+${serviceOrder.id}].value=Math.round(estimatedTotalExpense*10000)/10000;
	document.forms['serviceOrderForm'].elements['estimatedGrossMargin'+${serviceOrder.id}].value=Math.round(estimatedGrossMargin*10000)/10000;
	document.forms['serviceOrderForm'].elements['estimatedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(estimatedGrossMarginPercentage*10000)/10000;
	</c:if>	 
	 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	 calculateVatSection('EST',aid,type1); 
	 </c:if>
}
function estimateBuyByChargeCodeFX(aid){
		var roundVal=0.00;
		var estimatePayableContractCurrency=document.forms['serviceOrderForm'].elements['estimatePayableContractCurrencyNew'+aid].value;		
		var estimatePayableContractExchangeRate=document.forms['serviceOrderForm'].elements['estimatePayableContractExchangeRateNew'+aid].value;
		var estimatePayableContractRate=document.forms['serviceOrderForm'].elements['estimatePayableContractRateNew'+aid].value;
		var estimatePayableContractRateAmmount=document.forms['serviceOrderForm'].elements['estimatePayableContractRateAmmountNew'+aid].value;
		var estimateCurrency=document.forms['serviceOrderForm'].elements['estCurrencyNew'+aid].value;
		var estimateExchangeRate=document.forms['serviceOrderForm'].elements['estExchangeRateNew'+aid].value;
		var estimateLocalRate=document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value;
		var estimateLocalAmount=document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value;
		
	 	if(estimatePayableContractCurrency!=''){
	 		roundVal=estimatePayableContractRate/estimatePayableContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['estimateRate'+aid].value=roundVal;
	 		if(estimateCurrency!=''){
		 		document.forms['serviceOrderForm'].elements['estLocalRateNew'+aid].value=roundVal*estimateExchangeRate;
		 	}
	 		roundVal=estimatePayableContractRateAmmount/estimatePayableContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['estimateExpense'+aid].value=roundVal;
	 		if(estimateCurrency!=''){
	 		document.forms['serviceOrderForm'].elements['estLocalAmountNew'+aid].value=roundVal*estimateExchangeRate;
	 		}
	 	}
}
function estimateSellByChargeCodeFX(aid){
		var roundVal=0.00;
		var estimateContractCurrency=document.forms['serviceOrderForm'].elements['estimateContractCurrencyNew'+aid].value;		
		var estimateContractExchangeRate=document.forms['serviceOrderForm'].elements['estimateContractExchangeRateNew'+aid].value;
		var estimateContractRate=document.forms['serviceOrderForm'].elements['estimateContractRateNew'+aid].value;
		var estimateContractRateAmmount=document.forms['serviceOrderForm'].elements['estimateContractRateAmmountNew'+aid].value;
		var estimateSellCurrency=document.forms['serviceOrderForm'].elements['estSellCurrencyNew'+aid].value;		
		var estimateSellExchangeRate=document.forms['serviceOrderForm'].elements['estSellExchangeRateNew'+aid].value;
		var estimateSellLocalRate=document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value;
		var estimateSellLocalAmount=document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value;
		
	 	if(estimateContractCurrency!=''){
	 		roundVal=estimateContractRate/estimateContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['estimateSellRate'+aid].value=roundVal;
	 		if(estimateSellCurrency!=''){
		 		document.forms['serviceOrderForm'].elements['estSellLocalRateNew'+aid].value=roundVal*estimateSellExchangeRate;
		 	}
	 		roundVal=estimateContractRateAmmount/estimateContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['estimateRevenueAmount'+aid].value=roundVal;
	 		if(estimateSellCurrency!=''){
	 		document.forms['serviceOrderForm'].elements['estSellLocalAmountNew'+aid].value=roundVal*estimateSellExchangeRate;
	 		}
	 	}
}

 function calculateRevisionByChargeCodeBlock(aid,type1){
     //Code For revision Est	 
	 revisionBuyByChargeCodeFX(aid);
     //Code For revision Sell	 
	  <c:if test="${multiCurrency=='Y'}">
	  revisionSellByChargeCodeFX(aid);
	  </c:if>
	  //Code For Revision Pass Percentage   
	    calculateRevisionPassPercentage('revisionRevenueAmount'+aid,'revisionExpense'+aid,'revisionPassPercentage'+aid);
	  // Code Gross Section
 	  	var revisedTotalRevenue=0.00;
 		var revisedTotalExpense=0.00;
 		var tempRevisedTotalExpense=0.00;
 		var tempRevisedTotalRevenue=0.00;
 		var revisedGrossMargin=0.00;
 		var revisedGrossMarginPercentage=0.00;
 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			if(document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].value!=""){
	 			tempRevisedTotalRevenue=document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+accArr[g]].value;
			}
	 		revisedTotalRevenue=revisedTotalRevenue+parseFloat(tempRevisedTotalRevenue);
	 		if(document.forms['serviceOrderForm'].elements['revisionExpense'+accArr[g]].value!=""){
	 			tempRevisedTotalExpense=document.forms['serviceOrderForm'].elements['revisionExpense'+accArr[g]].value;
	 		}
	 		revisedTotalExpense=revisedTotalExpense+parseFloat(tempRevisedTotalExpense);
		}
	revisedGrossMargin=revisedTotalRevenue-revisedTotalExpense;
	try{
		if((revisedTotalRevenue==0)||(revisedTotalRevenue==0.0)||(revisedTotalRevenue==0.00)){
			revisedGrossMarginPercentage=0.00;
		}else{
			revisedGrossMarginPercentage=(revisedGrossMargin*100)/revisedTotalRevenue;
		}
	}catch(e){
		revisedGrossMarginPercentage=0.00;
		}
	 <c:if test="${not empty serviceOrder.id}">
	document.forms['serviceOrderForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=Math.round(revisedTotalRevenue*10000)/10000;
	document.forms['serviceOrderForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=Math.round(revisedTotalExpense*10000)/10000;
	document.forms['serviceOrderForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=Math.round(revisedGrossMargin*10000)/10000;
	document.forms['serviceOrderForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(revisedGrossMarginPercentage*10000)/10000;
	</c:if>	 
	 <c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
	 <c:if test="${systemDefaultVatCalculationNew=='Y'}">
	 calculateVatSection('REV',aid,type1); 
	 </c:if>
	 </c:if>   
 }
 function revisionBuyByChargeCodeFX(aid){
	 	var roundVal=0.00;
		var revisionPayableContractCurrency=document.forms['serviceOrderForm'].elements['revisionPayableContractCurrency'+aid].value;		
		var revisionPayableContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionPayableContractExchangeRate'+aid].value;
		var revisionPayableContractRate=document.forms['serviceOrderForm'].elements['revisionPayableContractRate'+aid].value;
		var revisionPayableContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionPayableContractRateAmmount'+aid].value;
		var revisionCurrency=document.forms['serviceOrderForm'].elements['revisionCurrency'+aid].value;		
		var revisionExchangeRate=document.forms['serviceOrderForm'].elements['revisionExchangeRate'+aid].value;
		var revisionLocalRate=document.forms['serviceOrderForm'].elements['revisionLocalRate'+aid].value;
		var revisionLocalAmount=document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value;
		
	 	if(revisionPayableContractCurrency!=''){
	 		roundVal=revisionPayableContractRate/revisionPayableContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['revisionRate'+aid].value=roundVal;
	 		if(revisionCurrency!=''){
		 		document.forms['serviceOrderForm'].elements['revisionLocalRate'+aid].value=roundVal*revisionExchangeRate;
		 	}
	 		roundVal=revisionPayableContractRateAmmount/revisionPayableContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['revisionExpense'+aid].value=roundVal;
	 		if(revisionCurrency!=''){
	 		document.forms['serviceOrderForm'].elements['revisionLocalAmount'+aid].value=roundVal*revisionExchangeRate;
	 		}
	 	}
 }
 function revisionSellByChargeCodeFX(aid){
	 	var roundVal=0.00;
		var revisionContractCurrency=document.forms['serviceOrderForm'].elements['revisionContractCurrency'+aid].value;		
		var revisionContractExchangeRate=document.forms['serviceOrderForm'].elements['revisionContractExchangeRate'+aid].value;
		var revisionContractRate=document.forms['serviceOrderForm'].elements['revisionContractRate'+aid].value;
		var revisionContractRateAmmount=document.forms['serviceOrderForm'].elements['revisionContractRateAmmount'+aid].value;
		var revisionSellCurrency=document.forms['serviceOrderForm'].elements['revisionSellCurrency'+aid].value;		
		var revisionSellExchangeRate=document.forms['serviceOrderForm'].elements['revisionSellExchangeRate'+aid].value;
		var revisionSellLocalRate=document.forms['serviceOrderForm'].elements['revisionSellLocalRate'+aid].value;
		var revisionSellLocalAmount=document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value;
		
	 	if(revisionContractCurrency!=''){
	 		roundVal=revisionContractRate/revisionContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['revisionSellRate'+aid].value=roundVal;
	 		if(revisionSellCurrency!=''){
		 		document.forms['serviceOrderForm'].elements['revisionSellLocalRate'+aid].value=roundVal*revisionSellExchangeRate;
		 	}
	 		roundVal=revisionContractRateAmmount/revisionContractExchangeRate;
	 		document.forms['serviceOrderForm'].elements['revisionRevenueAmount'+aid].value=roundVal;
	 		if(revisionSellCurrency!=''){
	 		document.forms['serviceOrderForm'].elements['revisionSellLocalAmount'+aid].value=roundVal*revisionSellExchangeRate;
	 		}
	 	}

 }

	function calculateActualByChargeCodeBlock(aid,type1){
	    //Code For estimate Est	 
		 actualBuyByChargeCodeFX(aid);

	    //Code For estimate Sell	 
		  <c:if test="${multiCurrency=='Y'}">
		  actualSellByChargeCodeFX(aid);
		  </c:if>
		    var actualTotalRevenue=0.00;
	 		var actualTotalExpense=0.00;
	 		var tempActualTotalExpense=0.00;
	 		var tempActualTotalRevenue=0.00;
	 		var actualGrossMargin=0.00;
	 		var actualGrossMarginPercentage=0.00;
	 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
			var accArr=aidListIds.split(",");
			for(var g=0;g<accArr.length;g++){
			 	tempActualTotalRevenue="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=''){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}			 	
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
			 	tempActualTotalExpense="0.00";
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=''){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
			 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
			}
		actualGrossMargin=actualTotalRevenue-actualTotalExpense;
		try{
			if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
				actualGrossMarginPercentage=0.00;
			}else{
				actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
			}
		}catch(e){
			actualGrossMarginPercentage=0.00;
			}
		 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['projectedActualRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['projectedActualExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;		
		</c:if>
 	  	 actualTotalRevenue=0.00;
 		 actualTotalExpense=0.00;
 		 tempActualTotalExpense=0.00;
 		 tempActualTotalRevenue=0.00;
 		 actualGrossMargin=0.00;
 		 actualGrossMarginPercentage=0.00;
 		var tempRecInvoiceNumber='';
		var tempPayingStatus=''; 		 
 		for(var g=0;g<accArr.length;g++){
	 		tempRecInvoiceNumber=document.forms['serviceOrderForm'].elements['recInvoiceNumber'+accArr[g]].value; 
	 	 	if(tempRecInvoiceNumber!=undefined && tempRecInvoiceNumber!=null && tempRecInvoiceNumber.trim()!=''){
			 	if(document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value!=""){
			 		tempActualTotalRevenue=document.forms['serviceOrderForm'].elements['actualRevenue'+accArr[g]].value;
			 	}
			 	actualTotalRevenue=actualTotalRevenue+parseFloat(tempActualTotalRevenue);
	 	 	}
	 	 	tempPayingStatus=document.forms['serviceOrderForm'].elements['payingStatus'+accArr[g]].value;
	 	 	if(tempPayingStatus!=undefined && tempPayingStatus!=null && tempPayingStatus.trim()!='' &&( tempPayingStatus.trim()=='A' || tempPayingStatus.trim()=='I')){
			 	if(document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value!=""){
			 		tempActualTotalExpense=document.forms['serviceOrderForm'].elements['actualExpense'+accArr[g]].value;
			 	}
		 	actualTotalExpense=actualTotalExpense+parseFloat(tempActualTotalExpense);
	 	 	} 	
 		}
	actualGrossMargin=actualTotalRevenue-actualTotalExpense;
	try{
		if((actualTotalRevenue==0)||(actualTotalRevenue==0.0)||(actualTotalRevenue==0.00)){
			actualGrossMarginPercentage=0.00;
		}else{
			actualGrossMarginPercentage=(actualGrossMargin*100)/actualTotalRevenue;
		}
	}catch(e){
		actualGrossMarginPercentage=0.00;
		}		
	 <c:if test="${not empty serviceOrder.id}">
		document.forms['serviceOrderForm'].elements['actualTotalRevenue'+${serviceOrder.id}].value=Math.round(actualTotalRevenue*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualTotalExpense'+${serviceOrder.id}].value=Math.round(actualTotalExpense*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMargin'+${serviceOrder.id}].value=Math.round(actualGrossMargin*10000)/10000;
		document.forms['serviceOrderForm'].elements['actualGrossMarginPercentage'+${serviceOrder.id}].value=Math.round(actualGrossMarginPercentage*10000)/10000;
		</c:if>
		  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('REC',aid,type1);
		  </c:if>
		  <c:if test="${systemDefaultVatCalculationNew=='Y'}">
		   calculateVatSection('PAY',aid,type1);
		  </c:if>
		  </c:if>
	}
	function actualBuyByChargeCodeFX(aid){
			var roundVal=0.00;
			var payableContractCurrency=document.forms['serviceOrderForm'].elements['payableContractCurrency'+aid].value;		
			var payableContractExchangeRate=document.forms['serviceOrderForm'].elements['payableContractExchangeRate'+aid].value;
			var payableContractRateAmmount=document.forms['serviceOrderForm'].elements['payableContractRateAmmount'+aid].value;
			var exchangeRate=document.forms['serviceOrderForm'].elements['exchangeRate'+aid].value;						
			var localAmount=document.forms['serviceOrderForm'].elements['localAmount'+aid].value;
		 	if(payableContractCurrency!=''){
		 		roundVal=(payableContractRateAmmount/payableContractExchangeRate);
		 		document.forms['serviceOrderForm'].elements['actualExpense'+aid].value=roundVal
		 		if(exchangeRate!=''){
		 		roundVal=roundVal*exchangeRate;
		 		document.forms['serviceOrderForm'].elements['localAmount'+aid].value=roundVal;
		 		}
		 	}
	}
	function actualSellByChargeCodeFX(aid){
			var roundVal=0.00;
			var contractCurrency=document.forms['serviceOrderForm'].elements['contractCurrency'+aid].value;		
			var contractExchangeRate=document.forms['serviceOrderForm'].elements['contractExchangeRate'+aid].value;
			var contractRate=document.forms['serviceOrderForm'].elements['contractRate'+aid].value;
			var contractRateAmmount=document.forms['serviceOrderForm'].elements['contractRateAmmount'+aid].value;
			var recRateCurrency=document.forms['serviceOrderForm'].elements['recRateCurrency'+aid].value;		
			var recRateExchange=document.forms['serviceOrderForm'].elements['recRateExchange'+aid].value;
			var recCurrencyRate=document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value;
			var actualRevenueForeign=document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value;
			
		 	if(contractCurrency!=''){
		 		roundVal=contractRate/contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['recRate'+aid].value=roundVal;
		 		if(recRateCurrency!=''){
			 		document.forms['serviceOrderForm'].elements['recCurrencyRate'+aid].value=roundVal*recRateExchange;
			 	}
		 		roundVal=contractRateAmmount/contractExchangeRate;
		 		document.forms['serviceOrderForm'].elements['actualRevenue'+aid].value=roundVal;
		 		if(recRateCurrency!=''){
		 		document.forms['serviceOrderForm'].elements['actualRevenueForeign'+aid].value=roundVal*recRateExchange;
		 		}}}
	function vatDescrEnable(){
 		var aidListIds=document.forms['serviceOrderForm'].elements['aidListIds'].value;
		var accArr=aidListIds.split(",");
		for(var g=0;g<accArr.length;g++){
			  var vatExclude=document.forms['serviceOrderForm'].elements['VATExclude'+accArr[g]].value;					 
			  try{
			  if(vatExclude=='true' || vatExclude==true){
  				  document.forms['serviceOrderForm'].elements['payVatDescr'+accArr[g]].value="";
				  document.forms['serviceOrderForm'].elements['recVatDescr'+accArr[g]].value="";							  							 
				  document.forms['serviceOrderForm'].elements['payVatDescr'+accArr[g]].disabled=true;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+accArr[g]].disabled=true;
				  <c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['serviceOrderForm'].elements['recVatAmt'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['recVatPercent'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['payVatAmt'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['payVatPercent'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['recVatAmtTemp'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['payVatAmtTemp'+accArr[g]].value=0;
					
					</c:if>
					document.forms['serviceOrderForm'].elements['vatAmt'+accArr[g]].value=0;			
					document.forms['serviceOrderForm'].elements['estExpVatAmt'+accArr[g]].value=0;			
					<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
					document.forms['serviceOrderForm'].elements['revisionVatAmt'+accArr[g]].value=0;
					document.forms['serviceOrderForm'].elements['revisionExpVatAmt'+accArr[g]].value=0;			
				  </c:if>
			  }else{
				  document.forms['serviceOrderForm'].elements['payVatDescr'+accArr[g]].disabled=false;
				  document.forms['serviceOrderForm'].elements['recVatDescr'+accArr[g]].disabled=false;
			  }
			  }catch(e){}
		}
	}
function autoCompleterAjaxCallChargeCode(id,divid,idCondition){
	<configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
	 	document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value="AutoPricingChargeCode";
	 	document.getElementById("chargeCodeValidationMessage").value="";
	 	var ChargeName = document.getElementById('chargeCode'+id).value;
		var contract =document.forms['serviceOrderForm'].elements['billing.contract'].value;
		var idval='chargeCode'+id;
		contract=contract.trim();
		var category =  document.getElementById('category'+id).value;
		var accountCompanyDivision = document.forms['serviceOrderForm'].elements['companyDivision'+id].value;
		var originCountry=document.forms['serviceOrderForm'].elements['serviceOrder.originCountryCode'].value;
		var destinationCountry=document.forms['serviceOrderForm'].elements['serviceOrder.destinationCountryCode'].value;
		var mode;
		var pageCondition="";
	 	if(category=='Internal Cost'){
	 		pageCondition='AccountInternalCategory';
	 	}
	 	if(category!='Internal Cost'){
	 		pageCondition='AccountNonInternalCategory';
	 	}
		if(document.forms['serviceOrderForm'].elements['serviceOrder.mode']!=undefined){
			 mode=document.forms['serviceOrderForm'].elements['serviceOrder.mode'].value; 
			 }else{
				 mode="";
				 }
		
		if(contract!=""){
			if(ChargeName.length>=3){
	            $.get("ChargeCodeAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
	            	searchName:ChargeName,
	            	contract: contract,
	            	chargeCodeId:idval,
	            	autocompleteDivId: divid,
	            	id:id,
	            	idCondition:idCondition,
	            	searchcompanyDivision:accountCompanyDivision,
	            	originCountry:originCountry,
	            	destinationCountry:destinationCountry,
	            	serviceMode:mode,
	            	categoryCode:category,
	            	pageCondition:pageCondition,
	            	costElementFlag:${costElementFlag}
	            }, function(idval) {
	                document.getElementById(divid).style.display = "block";
	                $("#" + divid).html(idval)
	            })
	        } else {
	            document.getElementById(divid).style.display = "none"
	        }
		}
		else{
			alert("There is No Billing Contract: Please select.");
			document.getElementById("contract"+id).focus();
		}
		</configByCorp:fieldVisibility>
 }
 function copyChargeDetails(chargecode,chargecodeId,autocompleteDivId,id,idCondition){
			document.getElementById(chargecodeId).value=chargecode;
			document.getElementById(autocompleteDivId).style.display = "none";
			document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
			fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
			checkChargeCode('chargeCode'+id,'category'+id,id);
			updateContractChangeCharge('chargeCode'+id,'accChargeCodeTemp'+id,id,idCondition);
			findInternalCostVendorCode(id);
		}
 function closeMyChargeDiv(autocompleteDivId,chargecode,chargecodeId,id,idCondition){
			document.getElementById(autocompleteDivId).style.display = "none";
			 if(document.getElementById(chargecodeId).value==''){
				document.getElementById("quoteDescription"+id).value="";	
			}
			document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
			fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
			checkChargeCode('chargeCode'+id,'category'+id,id);
			updateContractChangeCharge('chargeCode'+id,'accChargeCodeTemp'+id,id,idCondition);
			}
 function closeMyChargeDiv2(chargecodeId,id){
		 if(document.getElementById(chargecodeId).value==''){
			document.getElementById("quoteDescription"+id).value="";	
		}
		 	document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
		 	document.forms['serviceOrderForm'].elements['chargeCodeValidationMessage'].value='NothingData';
		 	
		}
     function enablePreviousFunction(chargeCode,category,id,idCondition){
    	 <configByCorp:fieldVisibility componentId="component.charges.Autofill.chargeCode">
    	 if(document.getElementById("chargeCodeValidationMessage").value!='NothingData'){
    	 document.forms['serviceOrderForm'].elements['chargeCodeValidationVal'].value='';
			document.getElementById('ChargeNameDivId'+id).style.display = "none";
			checkChargeCode('chargeCode'+id,'category'+id,id);
			fillDiscription('quoteDescription'+id,'chargeCode'+id,'category'+id,'recGl'+id,'payGl'+id,'contractCurrency'+id,'contractExchangeRate'+id,'contractValueDate'+id,'payableContractCurrency'+id,'payableContractExchangeRate'+id,'payableContractValueDate'+id,'accChargeCodeTemp'+id,id);
			updateContractChangeCharge('chargeCode'+id,'accChargeCodeTemp'+id,id,idCondition);
			findInternalCostVendorCode(id);
    	 }
    	 </configByCorp:fieldVisibility>
	}
    
     function fillDefaultInvoiceNumber(aid){
    		var aVendorCode=document.forms['serviceOrderForm'].elements['vendorCode'+aid].value;
    		var aChargeCode=document.forms['serviceOrderForm'].elements['chargeCode'+aid].value;
    		var fillDefaultInvoiceNumberFlag=false;
    		if(aVendorCode!=null && aVendorCode!=undefined && aVendorCode!='' && aChargeCode!=null && aChargeCode!=undefined && aChargeCode!=''){
    			fillDefaultInvoiceNumberFlag=true;
    		}
    		<configByCorp:fieldVisibility componentId="component.field.Alternative.SetRegistrationNumber">
    		var invoiceNumber=document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value;
    		var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
    		var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;
    		var soRegistrationNumber='${serviceOrder.registrationNumber}';
    		payGl=payGl.trim();	invoiceNumber=invoiceNumber.trim();	actgCode=actgCode.trim();
    		if(fillDefaultInvoiceNumberFlag && (invoiceNumber=='')&&(actgCode!='')&&(payGl!='')){
    		if(soRegistrationNumber!=''){
    			document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value ='${serviceOrder.registrationNumber}';
    		}else{	
    			document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value ='${serviceOrder.shipNumber}';
    		}
    		document.forms['serviceOrderForm'].elements['payingStatus'+aid].value ='A'; 
    		document.forms['serviceOrderForm'].elements['accountLine.receivedDate'+aid].value=currentDateGlobal();
    		document.forms['serviceOrderForm'].elements['invoiceDate'+aid].value=currentDateGlobal(); 
    		}
    		</configByCorp:fieldVisibility>
    		<configByCorp:fieldVisibility componentId="component.field.PayableInvoiceNumber.SetServiceOrderId">
    		var vendorCodeOOCheck=document.forms['serviceOrderForm'].elements['vendorCodeOwneroperator'+aid].value;
    		if(fillDefaultInvoiceNumberFlag && vendorCodeOOCheck!=undefined && vendorCodeOOCheck!=null && vendorCodeOOCheck!='' && vendorCodeOOCheck=='Y'){
    			var invoiceNumber=document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value;
    			var actgCode=document.forms['serviceOrderForm'].elements['actgCode'+aid].value;
    			var payGl=document.forms['serviceOrderForm'].elements['payGl'+aid].value;
    			var soRegistrationNumber='${serviceOrder.registrationNumber}';
    			payGl=payGl.trim();	invoiceNumber=invoiceNumber.trim();	actgCode=actgCode.trim();
    				if((actgCode!='')&&(payGl!='')){ 
    					document.forms['serviceOrderForm'].elements['invoiceNumber'+aid].value = "${serviceOrder.id}"+document.forms['serviceOrderForm'].elements['vendorCode'+aid].value; 
    					document.forms['serviceOrderForm'].elements['payingStatus'+aid].value ='A'; 
    					document.forms['serviceOrderForm'].elements['receivedDate'+aid].value=currentDateGlobal();
    					document.forms['serviceOrderForm'].elements['invoiceDate'+aid].value=currentDateGlobal(); 
    				}
    		}
    		</configByCorp:fieldVisibility>
    	}
     
     function checkZeroAmountForInvoice(targetBilltoCode)
     {
    	 <c:set var="checkfromGenerateInvoiceValidation" value="N"/>
     	<configByCorp:fieldVisibility componentId="component.field.generateInvoice.ZeroAmountValidation">
     		<c:set var="checkfromGenerateInvoiceValidation" value="Y"/>
     	</configByCorp:fieldVisibility>
    	 <c:choose>
   	  		<c:when test="${checkfromGenerateInvoiceValidation=='Y'}">
   				var billToCode=targetBilltoCode;
	 			var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
				var url="checkZeroforInvoice.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&invoiceBillToCode="+encodeURI(billToCode);
 					httpCheckZeroforInvoice.open("GET", url, true);
 					httpCheckZeroforInvoice.onreadystatechange = handleHttpResponseforCheckZeroInvoice;
 					httpCheckZeroforInvoice.send(null);
   			</c:when>
			<c:otherwise>
				findBillToAuthority();
 			</c:otherwise>
		</c:choose>
     		
        
     }  

     function handleHttpResponseforCheckZeroInvoice()
     {
      		if(httpCheckZeroforInvoice.readyState == 4)
     		{
                    var results = httpCheckZeroforInvoice.responseText
                    results = results.trim(); 
                     if(parseFloat(results)!=0)
                    {
                     	findBillToAuthority();
                    }
                    else
                    {
                 	   alert("You cannot generate Invoice as Invoice amount is 0");
                   		progressBarAutoSave('0');
                       	return false;
                 	   
                    }
              }
     }

     function getBilltoCodeforGenerateInvoice(){ 
    	 var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
          var url="distAmtInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
          http3.open("GET", url, true);
          http3.onreadystatechange = handleHttpResponseGetBilltoCode;
          http3.send(null);
     }

     function handleHttpResponseGetBilltoCode()
             { 
              if (http3.readyState == 4)
                  {
                     var results = http3.responseText 
                     results = results.trim();   	
                     if(results.length>2) {
                     	
                       var res = results.split("@");
                       if(res.length == 2)  {  
                         
                         checkZeroAmountForInvoice(res[1]);
                       }  else  {
                     	  
                       }
                        
      				}  else  { 
                        
                      }
                  }
             }   
     
</script>
<script type="text/JavaScript">     
function invoicePreview(name,position){
var url='viewReceipt.html?jasperName=InvoicePreview.jrxml&docsxfer=Yes&list=main&decorator=popup&popup=true&reportParameter_Service Order Number='+encodeURI(name)+'&reportParameter_Corporate ID=${sessionCorpID}&fileType=PDF';

location.href=url;
return true;
}
function checkChargeCodeMandatory(sid,aid,chargeCode){
if((${checkContractChargesMandatory=='1'} || ${contractType}) && (chargeCode==undefined || chargeCode==null || chargeCode=='' || chargeCode.trim()=='')){
	alert('Charge code needs to be selected');
}else{
	window.open('editAccountLine.html?sid='+sid+'&id='+aid+'',"width=250,height=70,scrollbars=1");
}
}

function getSurveyFieldId(qid,target,type){
	var ansid;
	if(target.value==undefined){
		ansid = target;
	}else{
		ansid = target.value;
	}
	var idVal = qid+'~'+ansid;
	
	if(type!='dropDown' && type!='radio' && type!='text'){
		var surveyVal = document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value;
			     
	     if(surveyVal == ''){
	    	 document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = idVal;
	     }else{
	    	var check = surveyVal.indexOf(idVal);
	 		if(check > -1){
	 			var values = surveyVal.split("@");
	 		   for(var i = 0 ; i < values.length ; i++) {
	 		      if(values[i]== idVal) {
	 		    	values.splice(i, 1);
	 		    	surveyVal = values.join("@");
	 		    	document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = surveyVal;
	 		      }
	 		   }
	 		}else{
	 			surveyVal = surveyVal + "@" +idVal;
	 			document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = surveyVal.replace('@ @',"@");
	 		}
	     }
	}else{
		var surveyVal = document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value;
	     
	     if(surveyVal == ''){
	    	 document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = idVal;
	     }else{
	    	var check = surveyVal.indexOf(qid+'~');
	 		if(check > -1){
	 			var values = surveyVal.split("@");
	 			var sanitizedArray = [];
	 		   for(var i = 0 ; i < values.length ; i++) {
	 			  if (qid+'~' !== values[i] && values[i].indexOf(qid+'~') == -1) {
	 				 sanitizedArray.push(values[i]);
	 		        }
	 		    }
	 		 surveyVal = sanitizedArray.join("@");
	 		 if(surveyVal!="" && ansid!=""){
	 		 	document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = surveyVal+ "@" +idVal;
	 		 }else if(surveyVal=="" && ansid!=""){
	 			document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = idVal;
	 		 }else{
	 			document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = surveyVal;
	 		 }
	 		}else{
	 			surveyVal = surveyVal + "@" +idVal;
	 			document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value = surveyVal.replace('@ @',"@");
	 		}
	     }
	}
}

function saveSurveyByUserDetails(){
	var surveyByUserVal = document.forms['serviceOrderForm'].elements['surveyAnsByUserVal'].value;
	if(surveyByUserVal!=''){
		var sid = document.forms['serviceOrderForm'].elements['serviceOrder.id'].value;
			new Ajax.Request('saveSurveyByUserOnCfOrSo.html?ajax=1&decorator=simple&popup=true&sid='+sid+'&surveyByUserVal='+surveyByUserVal,
					{
				method:'get',
				onSuccess: function(transport){
					var response = transport.responseText || "";
					response = response.trim();
					//var accLineDetailsDiv = document.getElementById("customerSurveyDiv");
					//accLineDetailsDiv.innerHTML = response;
					
				},
				onFailure: function(){ 
				}
			});
	}else{
		alert('Nothing found for save.');
	}
}

</SCRIPT>