package com.trilasoft.app.service;
 
import org.appfuse.service.GenericManager;   
import com.trilasoft.app.model.RemovalRelocationService;   

public interface RemovalRelocationServiceManager extends GenericManager<RemovalRelocationService, Long> {   

}
