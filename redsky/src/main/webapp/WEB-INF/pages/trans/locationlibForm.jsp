<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html:html>

<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>

</head>
<body style="color:#222222;background-color:#dddddd">

<s:form id="locationlibForm" action="saveLocationlib" method="post"
	validate="true">

	<s:hidden name="location.id" value="%{location.id}" />
	<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>
			<tr>
				<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td align="right"></td>

							<td align="right" class="listwhite"><b>Location</b></td>
							<td class="listwhite"><s:textfield name="location.location"
								required="true" cssClass="text medium" /></td>
							<td></td>
						</tr>

						<tr>
							<td></td>
							<td align="right" class="listwhite"><b>Capacity</b></td>
							<td class="listwhite"><s:select name="location.capacity"
								list="{'1','2','3'}" headerKey="-1" /></td>

							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite"><b>Type</b></td>
							<td class="listwhite"><s:select name="location.type"
								list="{'R','S','K'}" headerKey="-1" /></td>

							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite"><b>Occupied</b></td>
							<td class="listwhite"><s:select name="location.occupied"
								list="{'X'}" headerKey="-1" /></td>

							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite"><b>CubicFeet</b></td>
							<td class="listwhite"><s:textfield name="location.cubicFeet"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>



	<div align="center">

	<li class="buttonBar bottom"><s:submit cssClass="button"
		method="save" key="button.save" /> <c:if
		test="${not empty location.id}">
		<s:submit cssClass="button" method="delete" key="button.delete"
			onclick="return confirmDelete('location')" />
	</c:if> <s:submit cssClass="button" method="cancel" key="button.cancel" /></li>
	</div>

</s:form>
</body>
</html:html>
<script type="text/javascript"> 
    Form.focusFirstElement($("locationlibForm")); 
</script>

