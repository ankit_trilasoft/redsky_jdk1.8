package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.service.GLCommissionTypeManager;
import com.trilasoft.app.dao.GLCommissionTypeDao;
import com.trilasoft.app.model.GLCommissionType;
import com.trilasoft.app.model.VanLineGLType;

public class GLCommissionTypeManagerImpl  extends GenericManagerImpl<GLCommissionType, Long> implements GLCommissionTypeManager {

	GLCommissionTypeDao glCommissionTypeDao; 

    public GLCommissionTypeManagerImpl(GLCommissionTypeDao glCommissionTypeDao) { 
        super(glCommissionTypeDao); 
        this.glCommissionTypeDao = glCommissionTypeDao; 
    } 
    
    public List<GLCommissionType> gLCommissionTypeList(String contract, String charge){
    	return glCommissionTypeDao.gLCommissionTypeList(contract, charge); 
    }
    public List<GLCommissionType> glList(String gl){
    	return glCommissionTypeDao.glList(gl);
   }
   
}
