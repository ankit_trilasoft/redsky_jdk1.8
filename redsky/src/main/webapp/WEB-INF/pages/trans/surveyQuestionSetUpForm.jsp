<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<head>   
    <title>Survey Question</title>   
    <style type="text/css">
</style>
<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
<script type="text/javascript">
function onlyNumberValue(evt){
	  var keyCode = evt.keyCode || evt.which;
	  if(keyCode==16){
	  return true;
	  }	  
	  if((keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ||  (keyCode==35) || (keyCode==36)){
	    return true;
	  }
	  else if(((keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105))){
	  return true;
	  }
	  else{
	   return false;
	  }
	}
function checkForColon(evt) {
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==186 || keyCode== 58){
		return false;
	}else{
		return true;
	}
}
function addRowSurveyQuestion(tableID) {
	var flag=checkMandatoryAddLineField(); 
	if(flag==true){
		    var table = document.getElementById(tableID);
		    var rowCount = table.rows.length;
		    
			var row = table.insertRow(rowCount); 
		    var newlineid=document.getElementById('newlineid').value;
		    if(newlineid==''){
		        newlineid=rowCount;
		      }else{
		      newlineid=newlineid+"~"+rowCount;
		      }
		    document.getElementById('newlineid').value=newlineid;
		    
		 	var cell1 = row.insertCell(0);
		 	var element1 = document.createElement("input");
			element1.type = "text";
			element1.style.width="20px";
			element1.setAttribute("class", "input-text" );
			element1.setAttribute("maxlength", "2" );
			element1.setAttribute("value", rowCount );
			element1.id='seq'+rowCount;
			element1.setAttribute("onkeydown","return onlyNumberValue(event)");
			cell1.appendChild(element1);
			
			var cell2 = row.insertCell(1);
			var element2 = document.createElement("select");
			element2.setAttribute("class", "list-menu" );
			element2.id="cat"+rowCount;
			element2.style.width="130px"
			var categoryList='${categoryList}';
			categoryList=categoryList.replace('{','').replace('}','');
			var categoryListArray =categoryList.split(",");
			var categoryOption= document.createElement("option");
			categoryOption.value="";
			categoryOption.text=""; 
			element2.options.add(categoryOption);
			for(var i=0;i<categoryListArray.length;i++){
				var value=categoryListArray[i].split("=");
		        var optionele= document.createElement("option");
		        optionele.value=value[0];
		        optionele.text=value[1];
		        element2.options.add(optionele);
			}
			cell2.appendChild(element2);
		
			var cell3 = row.insertCell(2);
			var element3 = document.createElement("input");
			element3.type = "text";
			element3.style.width="350px";
			element3.setAttribute("class", "input-text" );
			element3.setAttribute("maxlength", "300" );
			element3.id='que'+rowCount;
			element3.setAttribute("onkeydown","return checkForColon(event)");
			cell3.appendChild(element3);
			
			
			var cell4 = row.insertCell(3);
			var element4 = document.createElement("select");
			element4.setAttribute("class", "list-menu" );
			element4.id="ans"+rowCount;
			element4.style.width="150px"
			var answerList='${answerTypeList}';
			answerList=answerList.replace('{','').replace('}','');
			var answerListArray=answerList.split(",");
			for(var i=0;i<answerListArray.length;i++){
				var value=answerListArray[i].split("=");
		        var optionele= document.createElement("option");
		        optionele.value=value[0];
		        optionele.text=value[1];
		        element4.options.add(optionele);
			}
			cell4.appendChild(element4);
			
			
			var cell5 = row.insertCell(4);
			var element5 = document.createElement("select");
			element5.setAttribute("class", "list-menu" );
			element5.id="job"+rowCount;
			element5.style.width="100px"
			var jobList='${jobTypeList}';
			jobList=jobList.replace('{','').replace('}','');
			var jobListArray =jobList.split(",");
			var jobOption= document.createElement("option");
			jobOption.value="";
			jobOption.text=""; 
			element5.options.add(jobOption); 
			for(var i=0;i<jobListArray.length;i++){
				var value=jobListArray[i].split("=");
		        var optionele= document.createElement("option");
		        optionele.value=value[0];
		        optionele.text=value[1];
		        element5.options.add(optionele);
			}
			cell5.appendChild(element5);
			
			
			var cell6 = row.insertCell(5);
			var element6 = document.createElement("select");
			element6.setAttribute("class", "list-menu" );
			element6.id="rou"+rowCount;
			element6.style.width="100px"
			var routingList='${routingList}';
			routingList=routingList.replace('{','').replace('}','');
			var routingListArray =routingList.split(",");
			/* var routingOption= document.createElement("option");
			routingOption.value="";
			routingOption.text=""; 
			element6.options.add(routingOption);  */
			for(var i=0;i<routingListArray.length;i++){
				var value=routingListArray[i].split("=");
		        var optionele= document.createElement("option");
		        optionele.value=value[0];
		        optionele.text=value[1];
		        element6.options.add(optionele);
			}
			cell6.appendChild(element6);
			
			var cell7 = row.insertCell(6);
			var element7 = document.createElement("input");
			element7.type = "checkbox";
			element7.style.width="30px";
			element7.id='boo'+rowCount;
			element7.checked=true;
			cell7.appendChild(element7);
			
			var cell8 = row.insertCell(7);
			var element8 = document.createElement("input");
			element8.type = "checkbox";
			element8.style.width="30px";
			element8.id='ori'+rowCount;
			element8.checked=false;
			cell8.appendChild(element8);
			
			var cell9 = row.insertCell(8);
			var element9 = document.createElement("input");
			element9.type = "checkbox";
			element9.style.width="30px";
			element9.id='des'+rowCount;
			element9.checked=false;
			cell9.appendChild(element9);
			
			
			var cell10 = row.insertCell(9);
			var element10 = document.createElement("select");
			element10.setAttribute("class", "list-menu" );
			element10.id="lan"+rowCount;
			element10.style.width="80px"
			var languageList='${languageList}';
			languageList=languageList.replace('{','').replace('}','');
			var languageListArray =languageList.split(",");
			for(var i=0;i<languageListArray.length;i++){
				var value=languageListArray[i].split("=");
		        var optionele= document.createElement("option");
		        optionele.value=value[0];
		        optionele.text=value[1];
		        element10.options.add(optionele);
			}
			cell10.appendChild(element10);
			
			
			var cell11 = row.insertCell(10);
			var element11 = document.createElement("input");
			element11.type = "checkbox";
			element11.style.width="30px";
			element11.id='sta'+rowCount;
			element11.checked=true;
			cell11.appendChild(element11);
			
			var temp = " en";
			var mySelect = document.getElementById("lan"+rowCount);
			for(var i, j = 0; i = mySelect.options[j]; j++) {
				  if(i.value == temp) {
				    mySelect.selectedIndex = j;
				    break;
				  }
			}
			
			var cell12 = row.insertCell(11);
			var element12 = document.createElement("img");
			element12.setAttribute("src","<c:url value='/images/blankWhite.png'/>");
			cell12.appendChild(element12);
	}
}

function saveSurveyQuestion(){
	var seqNumListVal="";   	
   	var questionListVal="";
   	var answerTypeListVal="";
   	var jobTypeListVal="";
   	var routingListVal="";
   	var bookingAgentListVal="";
   	var originAgentListVal="";
   	var destinationAgentListVal="";
   	var languageListVal="";
   	var statusListVal="";
   	var id='';
   	var categoryListVal="";
   	var flag=checkMandatoryAddLineField(); 
	if(flag==true){	   
	    if(document.forms['surveyQuestionForm'].sequenceNumberList!=undefined){
	        if(document.forms['surveyQuestionForm'].sequenceNumberList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].sequenceNumberList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].sequenceNumberList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(seqNumListVal==''){
		            	 seqNumListVal=id+":"+document.forms['surveyQuestionForm'].sequenceNumberList[i].value;
		             }else{
		            	 seqNumListVal= seqNumListVal+"~"+id+":"+document.forms['surveyQuestionForm'].sequenceNumberList[i].value;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].sequenceNumberList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        seqNumListVal=id+":"+document.forms['surveyQuestionForm'].sequenceNumberList.value;
	 	      }	        
	       }
	    if(document.forms['surveyQuestionForm'].questionList!=undefined){
	        if(document.forms['surveyQuestionForm'].questionList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].questionList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].questionList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(questionListVal==''){
		            	 questionListVal=id+":"+document.forms['surveyQuestionForm'].questionList[i].value;
		             }else{
		            	 questionListVal= questionListVal+"~"+id+":"+document.forms['surveyQuestionForm'].questionList[i].value;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].questionList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        questionListVal=id+":"+document.forms['surveyQuestionForm'].questionList.value;
	 	      }	        
	       }
	    if(document.forms['surveyQuestionForm'].answerList!=undefined){
	        if(document.forms['surveyQuestionForm'].answerList.size!=0){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].answerList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].answerList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(answerTypeListVal==''){
		            	 answerTypeListVal=id+":"+document.forms['surveyQuestionForm'].answerList[i].value.trim();
		             }else{
		            	 answerTypeListVal= answerTypeListVal+"~"+id+":"+document.forms['surveyQuestionForm'].answerList[i].value.trim();
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].answerList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        answerTypeListVal=id+":"+document.forms['surveyQuestionForm'].answerList.value;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].jobList!=undefined){
	        if(document.forms['surveyQuestionForm'].jobList.size!=0){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].jobList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].jobList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(jobTypeListVal==''){
		            	 jobTypeListVal=id+":"+document.forms['surveyQuestionForm'].jobList[i].value.trim();
		             }else{
		            	 jobTypeListVal = jobTypeListVal+"~"+id+":"+document.forms['surveyQuestionForm'].jobList[i].value.trim();
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].jobList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        jobTypeListVal = id+":"+document.forms['surveyQuestionForm'].jobList.value;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].routingTypeList!=undefined){
	        if(document.forms['surveyQuestionForm'].routingTypeList.size!=0){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].routingTypeList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].routingTypeList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(routingListVal==''){
		            	 routingListVal=id+":"+document.forms['surveyQuestionForm'].routingTypeList[i].value.trim();
		             }else{
		            	 routingListVal = routingListVal+"~"+id+":"+document.forms['surveyQuestionForm'].routingTypeList[i].value.trim();
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].routingTypeList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        routingListVal = id+":"+document.forms['surveyQuestionForm'].routingTypeList.value;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].bookingAgentList!=undefined){
	        if(document.forms['surveyQuestionForm'].bookingAgentList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].bookingAgentList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].bookingAgentList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(bookingAgentListVal==''){
		            	 bookingAgentListVal = id+":"+document.forms['surveyQuestionForm'].bookingAgentList[i].checked;
		             }else{
		            	 bookingAgentListVal = bookingAgentListVal+"~"+id+":"+document.forms['surveyQuestionForm'].bookingAgentList[i].checked;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].bookingAgentList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        bookingAgentListVal = id+":"+document.forms['surveyQuestionForm'].bookingAgentList.checked;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].originAgentList!=undefined){
	        if(document.forms['surveyQuestionForm'].originAgentList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].originAgentList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].originAgentList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(originAgentListVal==''){
		            	 originAgentListVal = id+":"+document.forms['surveyQuestionForm'].originAgentList[i].checked;
		             }else{
		            	 originAgentListVal = originAgentListVal+"~"+id+":"+document.forms['surveyQuestionForm'].originAgentList[i].checked;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].originAgentList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        originAgentListVal = id+":"+document.forms['surveyQuestionForm'].originAgentList.checked;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].destinationAgentList!=undefined){
	        if(document.forms['surveyQuestionForm'].destinationAgentList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].destinationAgentList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].destinationAgentList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(destinationAgentListVal==''){
		            	 destinationAgentListVal = id+":"+document.forms['surveyQuestionForm'].destinationAgentList[i].checked;
		             }else{
		            	 destinationAgentListVal = destinationAgentListVal+"~"+id+":"+document.forms['surveyQuestionForm'].destinationAgentList[i].checked;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].destinationAgentList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        destinationAgentListVal = id+":"+document.forms['surveyQuestionForm'].destinationAgentList.checked;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].languageTypeList!=undefined){
	        if(document.forms['surveyQuestionForm'].languageTypeList.size!=0){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].languageTypeList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].languageTypeList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(languageListVal==''){
		            	 languageListVal = id+":"+document.forms['surveyQuestionForm'].languageTypeList[i].value.trim();
		             }else{
		            	 languageListVal = languageListVal+"~"+id+":"+document.forms['surveyQuestionForm'].languageTypeList[i].value.trim();
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].languageTypeList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        languageListVal = id+":"+document.forms['surveyQuestionForm'].languageTypeList.value;
	 	      }	        
	      }
	    if(document.forms['surveyQuestionForm'].statusList!=undefined){
	        if(document.forms['surveyQuestionForm'].statusList.length!=undefined){
	 	      for (i=0; i<document.forms['surveyQuestionForm'].statusList.length; i++){	
	 	    	 id=document.forms['surveyQuestionForm'].statusList[i].id;
	                id=id.replace(id.substring(0,3),'').trim();
		             if(statusListVal==''){
		            	 statusListVal = id+":"+document.forms['surveyQuestionForm'].statusList[i].checked;
		             }else{
		            	 statusListVal = statusListVal+"~"+id+":"+document.forms['surveyQuestionForm'].statusList[i].checked;
		             }
	 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].statusList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        statusListVal = id+":"+document.forms['surveyQuestionForm'].statusList.checked;
	 	      }	        
	      }
	      if(document.forms['surveyQuestionForm'].categoryTypeList!=undefined){
		        if(document.forms['surveyQuestionForm'].categoryTypeList.length!=undefined){
		 	      for (i=0; i<document.forms['surveyQuestionForm'].categoryTypeList.length; i++){
		 	    	 id=document.forms['surveyQuestionForm'].categoryTypeList[i].id;
		                id=id.replace(id.substring(0,3),'').trim();
		                if(categoryListVal==''){
		                	categoryListVal = id+":"+document.forms['surveyQuestionForm'].categoryTypeList[i].value.trim();
			             }else{
			            	 categoryListVal = categoryListVal+"~"+id+":"+document.forms['surveyQuestionForm'].categoryTypeList[i].value.trim();
			             }
		 	        }	
	 	      }else{	
	 	    	 id=document.forms['surveyQuestionForm'].categoryTypeList.id;
	 	         id=id.replace(id.substring(0,3),'').trim();   
	 	        categoryListVal = id+":"+document.forms['surveyQuestionForm'].categoryTypeList.value;
	 	      }	        
		  }
	    document.getElementById('seqNumListVal').value=seqNumListVal;			 
		document.getElementById('questionListVal').value=questionListVal;  
		document.getElementById('answerTypeListVal').value=answerTypeListVal;
		document.getElementById('jobTypeListVal').value=jobTypeListVal;			 
		document.getElementById('routingListVal').value=routingListVal;  
		document.getElementById('bookingAgentListVal').value=bookingAgentListVal;
		document.getElementById('originAgentListVal').value=originAgentListVal;			 
		document.getElementById('destinationAgentListVal').value=destinationAgentListVal;  
		document.getElementById('languageListVal').value=languageListVal;
		document.getElementById('statusListVal').value=statusListVal;
		document.getElementById('categoryListVal').value=categoryListVal;
		
		var surveyQuestionRowlist="";
	    var newlineid=document.getElementById('newlineid').value;
	    if(newlineid!=''){
	           var arrayLine=newlineid.split("~");
	           for(var i=0;i<arrayLine.length;i++){
	               if(surveyQuestionRowlist==''){
	            	   surveyQuestionRowlist=document.getElementById('seq'+arrayLine[i]).value.trim()+":"+document.getElementById('cat'+arrayLine[i]).value.trim()+":"+document.getElementById('que'+arrayLine[i]).value.trim()+":"+document.getElementById('ans'+arrayLine[i]).value.trim()+":"+document.getElementById('job'+arrayLine[i]).value.trim()+":"+document.getElementById('rou'+arrayLine[i]).value.trim()+":"+document.getElementById('boo'+arrayLine[i]).checked+":"+document.getElementById('ori'+arrayLine[i]).checked+":"+document.getElementById('des'+arrayLine[i]).checked+":"+document.getElementById('lan'+arrayLine[i]).value.trim()+":"+document.getElementById('sta'+arrayLine[i]).checked;
	               }else{
	            	   surveyQuestionRowlist=surveyQuestionRowlist+"~"+document.getElementById('seq'+arrayLine[i]).value.trim()+":"+document.getElementById('cat'+arrayLine[i]).value.trim()+":"+document.getElementById('que'+arrayLine[i]).value.trim()+":"+document.getElementById('ans'+arrayLine[i]).value.trim()+":"+document.getElementById('job'+arrayLine[i]).value.trim()+":"+document.getElementById('rou'+arrayLine[i]).value.trim()+":"+document.getElementById('boo'+arrayLine[i]).checked+":"+document.getElementById('ori'+arrayLine[i]).checked+":"+document.getElementById('des'+arrayLine[i]).checked+":"+document.getElementById('lan'+arrayLine[i]).value.trim()+":"+document.getElementById('sta'+arrayLine[i]).checked;
	               }
	           }
	     }
	    document.getElementById('surveyQuestionRowlist').value =surveyQuestionRowlist;
	    document.forms['surveyQuestionForm'].action="saveSurveyQuestionSetUp.html";
	    document.forms['surveyQuestionForm'].submit();
	}
}
function checkMandatoryAddLineField(){
	  var newlineid=document.getElementById('newlineid').value; 	
	  if(newlineid!=''){
	     var arrayLine=newlineid.split("~");        
	        for(var i=0;i<arrayLine.length;i++){
	   		 var questionFieldVal=document.getElementById('que'+arrayLine[i]).value;
		       if(questionFieldVal==""){
			    	 alert("Please fill question for line # "+arrayLine[i]);
			    	 return false;
		        }
          }
		 return true;
     }else{
	     return true;
     }
}
function openSurveyAnswer(parentId){
		  var $j = jQuery.noConflict();
			$j("#surveyAnswerDetails").modal({show:true,backdrop: 'static'});
			  	$j.ajax({
		            url: 'findSurveyAnswerListAjax.html?parentId='+parentId+'&decorator=modal&popup=true',
		            success: function(data){
							$j("#surveyAnswerDetails").html(data);
		            },
		            error: function () {
		                alert('Some error occurs');
		                $j("#surveyAnswerDetails").modal("hide");
		             }   
	        	});  
}
function surveyAnswerModalClose(){
	var $j = jQuery.noConflict();
	$j("#surveyAnswerDetails").modal("hide");
}

function surveyAnsByUser(){
	//unip - 854276
	var sid='854452'; //voer
	//var sid = '859609'; //cwms
	var cid='716927';
	//javascript:openWindow('openSueryUserForm.html?sid='+sid+'&decorator=popup&popup=true',650,170) ;
	 var $j = jQuery.noConflict();
		$j("#surveyByUserDetails").modal({show:true,backdrop: 'static'});
		  	$j.ajax({
	            url: 'openSurveyUserForm.html?sid='+sid+'&decorator=modal&popup=true',
	            success: function(data){
					$j("#surveyByUserDetails").html(data);
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#surveyByUserDetails").modal("hide");
	             }   
     	});
}
function surveyByUserModalClose(){
	var $j = jQuery.noConflict();
	$j("#surveyByUserDetails").modal("hide");
}

</script>
</head>

<s:form id="surveyQuestionForm" name="surveyQuestionForm" method="post" validate="true">

<s:hidden name="newlineid" id="newlineid" value=""/>
<s:hidden name="seqNumListVal" id="seqNumListVal" />
<s:hidden name="questionListVal" id="questionListVal" />
<s:hidden name="answerTypeListVal" id="answerTypeListVal" />
<s:hidden name="jobTypeListVal" id="jobTypeListVal" />
<s:hidden name="routingListVal" id="routingListVal" />
<s:hidden name="bookingAgentListVal" id="bookingAgentListVal" />
<s:hidden name="originAgentListVal" id="originAgentListVal" />
<s:hidden name="destinationAgentListVal" id="destinationAgentListVal" />
<s:hidden name="languageListVal" id="languageListVal" />
<s:hidden name="statusListVal" id="statusListVal" />
<s:hidden name="surveyQuestionRowlist" id="surveyQuestionRowlist" />
<s:hidden name="categoryListVal" id="categoryListVal" />

 <div id="surveySetUp">
   <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%;margin:0px;">
	  <tbody>	 	
		  <tr>
		  <td>		  	
		  	<div id="otabs" style="margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Survey Question</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
			<div id="para1" style="clear:both;">	
					 <table class="table" id="dataTable" style="width:99%;margin-bottom:0px;">
					 <thead>
					 <tr>
					 <th style="text-align:center;">Seq#</th>
					 <th style="text-align:center;">Category</th>
					 <th style="text-align:center;">Question</th>
					 <th style="text-align:center;">Answer Type</th>
					 <th style="text-align:center;">Job Type</th>
					 <th style="text-align:center;">Routing</th>
					 <th style="text-align:center;">Role&nbsp;BA</th>
					 <th style="text-align:center;">Role&nbsp;OA</th>
					 <th style="text-align:center;">Role&nbsp;DA</th>
					 <th style="text-align:center;">Language</th>
					 <th style="text-align:center;">Status</th>
					 <th></th>
					 
					 </tr>
					 </thead>
					 <tbody>
					 
						 <c:forEach var="individualItem" items="${surveyQuestionList}" >
							<tr>
							<td><s:textfield name="sequenceNumberList" id="seq${individualItem.id}" value="${individualItem.sequenceNumber}" size="20" maxlength="2" cssClass="input-text" cssStyle="width:20px;"/></td>
							<td><s:select cssClass="list-menu" list="%{categoryList}" name="categoryTypeList" id="cat${individualItem.id}" value="'${individualItem.category}'" headerKey="" headerValue="" cssStyle="width:130px;"/></td>
							<td><s:textfield name="questionList" id="que${individualItem.id}" value="${individualItem.question}" cssClass="input-text" maxlength="300" size="10" cssStyle="width:350px;" onkeydown="return checkForColon(event)"/></td>
					  		<td><s:select cssClass="list-menu" list="%{answerTypeList}" name="answerList" id="ans${individualItem.id}" value="'${individualItem.answerType}'" cssStyle="width:150px;"/></td>
							<td><s:select cssClass="list-menu" list="%{jobTypeList}" name="jobList" id="job${individualItem.id}" value="'${individualItem.jobType}'" headerKey="" headerValue="" cssStyle="width:100px;"/></td>
							<td><s:select cssClass="list-menu" list="%{routingList}" name="routingTypeList" id="rou${individualItem.id}" value="'${individualItem.routing}'" headerKey="" headerValue="" cssStyle="width:100px;"/></td>
							<td><s:checkbox name="bookingAgentList" id="boo${individualItem.id}" value="${individualItem.bookingAgent}" cssStyle="width:30px;"/></td>
							<td><s:checkbox name="originAgentList" id="ori${individualItem.id}" value="${individualItem.originAgent}" cssStyle="width:30px;"/></td>
							<td><s:checkbox name="destinationAgentList" id="des${individualItem.id}" value="${individualItem.destinationAgent}" cssStyle="width:30px;"/></td>
							<td><s:select cssClass="list-menu" list="%{languageList}" name="languageTypeList" id="lan${individualItem.id}" value="'${individualItem.language}'" cssStyle="width:80px;"/></td>
							<td><s:checkbox name="statusList" id="sta${individualItem.id}" value="${individualItem.status}" cssStyle="width:30px;"/></td>
							<c:if test="${individualItem.answerType=='Text'}">
								<td>&nbsp;</td>
							</c:if>
							<c:if test="${individualItem.answerType!='Text'}">
								<div id="surveyAnswerDetails" class="modal fade"></div>
								<td><a onclick="openSurveyAnswer(${individualItem.id})" /><span>Ans</span></a> </td>
							</c:if>
						    </tr>
						 </c:forEach>
					 
					 </tbody>
					 </table>
			</div>
		</td>
		</tr>
		     <tr>
				<td align="left" height="13px"></td>
			</tr>		  	
		 </tbody>
	</table>
   </div>

<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:95px; height:25px;" value="Add Question" onclick="addRowSurveyQuestion('dataTable');" />
<input type="button" class="cssbutton1" id="save" name="save" style="width:70px; height:25px;" value="Save" onclick="saveSurveyQuestion();" />
<!-- <div id="surveyByUserDetails" class="modal fade"></div>
<input type="button" class="cssbutton1" id="surveyByUser" name="surveyByUser" style="width:70px; height:25px;" value="Survey By User" onclick="surveyAnsByUser();" />
 -->
</s:form>