<%@ include file="/common/taglibs.jsp" %> 

<head>   
    <title><fmt:message key="contractPolicyDetail.title"/></title>
<style>
.textareaUpper {
    background-color: #E3F1FE;
    border: 1px solid #ABADB3;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;   
    text-decoration: none;
    
}
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#prev{ width: 328px !important }
</style>   
    <meta name="heading" content="<fmt:message key='contractPolicyDetail.heading'/>"/> 
  <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tiny_mce.js"></script>
   
    <script type="text/javascript">
    function showOrHide(value) {
        if (value==0) {
           if (document.layers)
               document.layers["overlay11"].visibility='hide';
            else
               document.getElementById("overlay11").style.visibility='hidden';
       }
       else if (value==1) {
          if (document.layers)
              document.layers["overlay11"].visibility='show';
           else
              document.getElementById("overlay11").style.visibility='visible';
       }
    } 
</script>
    <script type="text/javascript">
			// Use it to attach the editor to all textareas with full featured setup
			//WYSIWYG.attach('all', full);
			
			// Use it to attach the editor directly to a defined textarea
			 <c:if test="${usertype!='ACCOUNT'}">
			 tinyMCE.init({
			
			 mode : "textareas",
			 theme : "advanced",
			 relative_urls : false,
			 remove_script_host : false,
			 plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			 
			 theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,insertdate,inserttime|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
				 theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,
					// Example content CSS (should be your site CSS)
					content_css : "css/content.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js"
			 }) 
			</c:if>
		
			
		</script> 
<script>
function checkdate(clickType){
	
	if(!(clickType == 'save')){
	var id = document.forms['contractPolicyForm'].elements['contractPolicy.id'].value;
	var id1 = document.forms['contractPolicyForm'].elements['partner.id'].value;
	var partnerType = document.forms['contractPolicyForm'].elements['partnerType'].value;
	
	if (document.forms['contractPolicyForm'].elements['formStatus'].value == '1'){
		
		var agree = confirm("Do you want to save the contact detail and continue? press OK to continue with save OR press CANCEL");
		if(agree){
			document.forms['contractPolicyForm'].action = 'saveContractPolicy!saveOnTabChange.html';
			document.forms['contractPolicyForm'].submit();
		}else{
			if(id != ''){
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
				location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
				location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
				location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'partnerPublics.html?partnerType=AC';
					}
				}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
				location.href = 'partnerReloSvcs.html?id='+id1+'&partnerType=AC';
				}
				
		}
		}
	}else{
		
	if(id != ''){
		
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
				location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
				location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
				location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
				}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
				if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'partnerPublics.html?partnerType=AC';
					}
				}
				if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
				location.href = 'partnerReloSvcs.html?id='+id1+'&partnerType=AC';
				}
	}else{
		if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			location.href = 'editPartnerPublic.html?id='+id1+'&partnerType=AC';
			}
		if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountprofile'){
			location.href = 'editNewAccountProfile.html?id='+id1+'&partnerType=AC';
			}
		if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
			location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
			}
		if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
			location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
			}
		if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
			if('<%=session.getAttribute("paramView")%>' == 'View'){
					location.href = 'searchPartnerView.html';
				}else{
					location.href = 'partnerPublics.html?partnerType=AC';
				}
			}
			if(document.forms['contractPolicyForm'].elements['gotoPageString'].value == 'gototab.partnerReloSvcs'){
			location.href = 'partnerReloSvcs.html?id='+id1+'&partnerType=AC';
			}
	
	}
	}
}
}
function changeStatus(){
	document.forms['contractPolicyForm'].elements['formStatus'].value = '1';
}
function openNotesPopupTab(targetElement){
	openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',755,500);
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this Document?");
	var did = targetElement;
	var policyId=document.forms['contractPolicyForm'].elements['contractPolicy.id'].value;
	if (agree){
		location.href='policyAdminDocDelete.html?&btnType=del&id='+policyId+'&id2='+did;
	}else{
		return false;
	}
}

function checkValue(){
	    var sectionName=document.forms['contractPolicyForm'].elements['contractPolicy.sectionName'].value;
		var language=document.forms['contractPolicyForm'].elements['contractPolicy.language'].value;
		var oldSectionName='${contractPolicy.sectionName}';
		var oldLanguage='${contractPolicy.language}';
		if((sectionName ==oldSectionName && language ==oldLanguage)){
		document.getElementById("xx").disabled = false;	
		}else if(sectionName=='' && language==''){
		document.getElementById("xx").disabled =true;	
		}else{
		document.getElementById("xx").disabled =true;
        }
}
     function uploadData(){ 
  	  		var sectionName=document.forms['contractPolicyForm'].elements['contractPolicy.sectionName'].value;
  	  		var language=document.forms['contractPolicyForm'].elements['contractPolicy.language'].value;
    	    openWindow('uploadAdminDocument.html?sectionName='+sectionName+'&language='+language+'&decorator=popup&popup=true',600,450);
    
    	   return true;
  		}
     function show(){
    		var redirect = document.forms['contractPolicyForm'].elements['btnType'].value;
    		if(redirect=='del'){
    			location.href = "/addContractPolicyFile.html?id=${partner.id}&partnerType=AC"
    		}
    	}
 	function checkField(){
 		if(document.forms['contractPolicyForm'].elements['contractPolicy.sectionName'].value==""){
 			alert("Please Select  Section Name");
 			return false;
 		}else if(document.forms['contractPolicyForm'].elements['contractPolicy.language'].value==""){
 			alert("Please Select Language");
 			return false;
 		}
 		return true;
 	}

 	function disabledAll(){
 		var elementsLen=document.forms['contractPolicyForm'].elements.length;
 		for(i=0;i<=elementsLen-1;i++){
 			if(document.forms['contractPolicyForm'].elements[i].type=='text'){
 				document.forms['contractPolicyForm'].elements[i].readOnly =true;
 				document.forms['contractPolicyForm'].elements[i].className = 'input-textUpper';
 			}else{
 				document.forms['contractPolicyForm'].elements[i].disabled=true;
 			} 
 		}
 		document.forms['contractPolicyForm'].elements['goToPolicyList'].disabled=false;
 	}

</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>
<c:set var="partnerId1" value="${partner.id}"/>
<s:hidden name="id1" value="<%= request.getParameter("id1")%>"/>
<s:form id="contractPolicyForm" action="saveAdminPolicy" method="post"  validate="true"> 
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="partnerType" value="${partnerType}" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
 <s:hidden name="btnType" value="<%= request.getParameter("btnType") %>" />
  <c:set var="btnType" value="<%= request.getParameter("btnType") %>" />

<s:hidden  name="contractPolicy.id" />
<s:hidden name="partner.id" />
<s:hidden name="dummsectionName" />
<s:hidden name="dummlanguage" />

<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.accountlist' }">
	<c:if test='${paramValue == "View"}'>
		<c:redirect url="/searchPartnerView.html"/>
	</c:if>
	<c:if test='${paramValue != "View"}'>
		<c:redirect url="/partnerPublics.html?partnerType=AC"/>
	</c:if>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountdetail' }">
	<c:redirect url="/editPartnerPublic.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountprofile' }">
	<c:redirect url="/editNewAccountProfile.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.accountcontact' }">
	<c:redirect url="/accountContactList.html?id=${partner.id}&partnerType=AC"/>
</c:when>
<c:when test="${gotoPageString == 'gototab.partnerReloSvcs' }">
				<c:redirect url="/partnerReloSvcs.html?id=${partner.id}&partnerType=AC" />
			</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer4" style="width:100%;">
<div id="newmnav">
				<ul>
					<c:url value="frequentlyAskedList.html" var="url">
					
					</c:url>			  	  
			  	  <c:if test="${empty param.popup}" >
			  	  <li><a href="cPortalResourceMgmts.html"><span>Cportal Docs</span></a></li>
   			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Policy<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    <li><a href="${url}"><span>FAQ</span></a></li>
									
					</c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div> 
<div id="Layer1" onkeydown="changeStatus();" style="width:98%;">
<div id="content" align="center">
<!--<div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom: 3px">
         <tr>
	  		<td align="right" class="listwhitetext" width="60px">Name</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.lastName" required="true" cssClass="input-textUpper" size="50" maxlength="80" readonly="true" /></td>
			<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode' /></td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="8" readonly="true" /></td>
			<td align="right" class="listwhitetext">Status</td>
			<td align="left" class="listwhitetext" colspan=""><s:textfield key="partner.status" required="true" cssClass="input-textUpper" size="15" readonly="true" /></td>
		</tr>
    </table>
  
 </div>
<div class="bottom-header" style="margin-top:30px;"><span></span></div>

</div>

--><div id="liquid-round-top">
<div class="top"><span></span></div>
  <div class="center-content">
    <table style="margin-bottom:3px;width:100%;">
    <tr>						
				<td colspan="20" width="" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('infoPackage')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					    <tr>
					       <td class="headtab_left">
					       </td>
					       <td NOWRAP class="headtab_center">&nbsp;&nbsp;Info Package
					       </td>
					       <td width="28" valign="top" class="headtab_bg"></td>
					       <td class="headtab_bg_special">&nbsp;
					       </td>
					       <td class="headtab_right">
					      </td>
					  </tr>
				   </table>
			  </div>
					  <div id="infoPackage">
					    <table width="100%" style="margin:0px;padding:0px;">
					    <tr>
					    <td align="right" class="listwhitetext">Section&nbsp;Name<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
					    <td colspan="10">
					    <table style="margin:0px;padding:0px;">
					       <tr>
					       <s:hidden name="contractPolicy.partnerCode" />					       
					           <td class="listwhitetext"><s:select cssClass="list-menu" name="contractPolicy.sectionName" list="%{sectionNameList}" onchange="checkValue();" cssStyle="width:155px" headerKey="" headerValue=""  /></td>
					           <td></td>
					           <td align="right" class="listwhitetext">Language<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
					           <td class="listwhitetext"><s:select cssClass="list-menu" name="contractPolicy.language" list="%{languageList}" onchange="checkValue();" cssStyle="width:155px" headerKey="" headerValue=""  /></td>
					            <td align="right" class="listwhitetext" width="50px">Job&nbsp;Type</td>
                               <td align="left"><s:textfield cssClass="input-text" name="contractPolicy.jobType" size="29" maxlength="65"/></td>
					           <td align="right" class="listwhitetext">Sequence&nbsp;#<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
					           <td class="listwhitetext"><s:textfield name="contractPolicy.docSequenceNumber" cssClass="input-text" onkeypress="return isNumberKey(event)" maxlength="10" size="8"></s:textfield></td>
					       </tr>
					       </table>
					       </td>
					       </tr>					       
					       <tr><td></td></tr>
					      
					       <tr>						      
						      <td align="right" class="listwhitetext">Description<c:if test="${usertype!='ACCOUNT'}"><font color="red" size="2">*</font></c:if></td>
							  <td align="left" colspan="8"><!--
							  <textarea  id="contractPolicypolicy" name="contractPolicy.policy" cols="83" rows="5" cssClass="textarea" >
							
							  </textarea>
							  -->
							 
				<c:if test="${usertype!='ACCOUNT'}">
				<s:textarea id="contractPolicypolicy" name="contractPolicy.policy" cols="83" rows="5" cssClass="textarea">
							
							</s:textarea> 
				</c:if>
				<c:if test="${usertype=='ACCOUNT'}">
				<div style="overflow-x:scroll; border:1px solid #74B3DC; overflow-y:scroll;width:550px;height:180px;">
							 <table border="0" style="width:100%;height:180px;margin:0px;padding:0px;" >
							
							 <tr>
							 <td style="vertical-align:top;">
							 ${contractPolicy.policy }
							 </td></tr>
							 
							 </table>
							 </div>		
				</c:if>
							  </td></tr>
					     <tr>
					     <td></td>
					     <td colspan="10" align="left">
					     <c:if test="${not empty policyDocumentList}">
					        <display:table name="policyDocumentList" id="policyDocumentList" class="table" pagesize="10" requestURI="" style="width:100%;">
					        <%-- <display:column style="width:5%" title="#"><a onclick=""><c:out value="${policyDocumentList_rowNum}"/></a></display:column> --%>
					        <display:column style="width:8%" property="docSequenceNumber" title="Sequence #"></display:column>
					        <display:column  title="Document Name" style="width:15%" >
								<a
									onclick="javascript:openWindow('PolicyDocumentImageServletAction.html?id=${policyDocumentList.id}&decorator=popup&popup=true',900,600);">
								<c:out value="${policyDocumentList.documentName}"
									escapeXml="false" /></a>
							</display:column>
					        <display:column property="fileName" title="File Name" maxLength="10" style="width:15%"/>
					        <display:column property="fileSize" title="Size"/>
					        <display:column property="language" title="Language"/>
					        <display:column property="sectionName" title="Section Name"/>
					        <display:column property="updatedOn"  title="Updated On"  format="{0,date,dd-MMM-yyyy}" />
						    <display:column property="updatedBy"  title="Updated By"/>
						    <c:if test="${usertype!='ACCOUNT'}">
						    	<c:choose>
									<c:when test="${partner.partnerType == 'DMM' or partner.partnerType == 'CMM'}">
										<configByCorp:fieldVisibility componentId="component.field.CmmDmm.visibility">
										    		<display:column title="Remove" style="width: 15px;" >
														<a><img align="middle" title="Remove" onclick="confirmSubmit(${policyDocumentList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a> 
													</display:column>
										</configByCorp:fieldVisibility>
									</c:when>
									<c:otherwise>
						   					 <display:column title="Remove" style="width: 15px;" >
												<a><img align="middle" title="Remove" onclick="confirmSubmit(${policyDocumentList.id});"  style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
											</display:column> 
									</c:otherwise>
								</c:choose>
								</c:if>
							 </display:table>
					       </c:if>
					       </td>
					     </tr>
					       <tr>
					      <td>
					      <c:if test="${not empty contractPolicy.id}">
					      <td><input type="button" id="xx" class="cssbutton" style="width:82px; height:25px" value="Upload" onclick="return uploadData();"/></td>
					      </c:if>
					      </td>
					      </tr>
					   </table>								
					
					</div>
		</td>
  </tr>
  </table>
 
  </div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>


<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please Wait</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>

</div>
</div>
<table width="760px" >
			<tbody>
				<tr>
				
				      <td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${contractPolicy.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="contractPolicy.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${contractPolicy.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
					
					
					<td align="right" class="listwhitetext" width="100"><b><fmt:message key='contractPolicy.createdBy' /></b></td>
					
					<c:if test="${not empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy"/>
						<td ><s:label name="createdBy" value="%{contractPolicy.createdBy}"/></td>
					</c:if>
					<c:if test="${empty contractPolicy.id}">
						<s:hidden name="contractPolicy.createdBy" value="${pageContext.request.remoteUser}"/>
						<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
					</c:if>
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedOn'/></b></td>
					<s:hidden name="contractPolicy.updatedOn"/>
					<td width="130"><fmt:formatDate value="${contractPolicy.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
					<td align="right" class="listwhitetext" width="90"><b><fmt:message key='contractPolicy.updatedBy' /></b></td>
					<c:if test="${not empty contractPolicy.id}">
								<s:hidden name="contractPolicy.updatedBy"/>
								<td><s:label name="updatedBy" value="%{contractPolicy.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty contractPolicy.id}">
								<s:hidden name="contractPolicy.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							
							
				</tr>
			</tbody>
			</table>  
			
<c:if test="${empty param.popup}" >
 <c:if test="${usertype!='ACCOUNT'}">
	<s:submit cssClass="cssbutton" method="savePolicy" key="button.save"   cssStyle="width:60px; height:25px "/>
	<s:reset cssClass="cssbutton" key="Reset" cssStyle="width:60px; height:25px "/>
</c:if> 
	<input id="gotoPolicy" class="cssbutton" type="button" name="goToPolicyList" style="margin-right: 5px;height: 25px;width:120px; font-size:15" value="Go To Policy List"
			onclick="location.href='contractFilePolicy.html'" />  

</c:if>
	<c:if test=" ${not empty partner.id}">
	<c:if test="${hitFlag=='1'}">
	 <c:redirect url="/addContractPolicyFile.html?id1=${partner.id}&id=${contractPolicy.id}"/>
	</c:if>
	</c:if>
</s:form>

<script type="text/javascript"> 
try{
<c:if test="${param.popup}" >
	for(i=0;i<100;i++){
		document.forms['contractPolicyForm'].elements[i].disabled = true;
	}
	Form.focusFirstElement($("contractPolicyForm"));   
</c:if>  
}
catch(e){}
    Form.focusFirstElement($("contractPolicyForm")); 
    try{
    	//show();
    }catch(e){} 
    try{
        <c:if test="${hitFlag=='9'}">
         <c:redirect url="/addContractPolicy.html?id=${contractPolicy.id}"/>
        </c:if>
    }catch(e){
    } 
</script>
<script type="text/javascript">
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
    function showOrHide(value) {
        if (value==0) {
           if (document.layers)
               document.layers["overlay11"].visibility='hide';
            else
               document.getElementById("overlay11").style.visibility='hidden';
       }
       else if (value==1) {
          if (document.layers)
              document.layers["overlay11"].visibility='show';
           else
              document.getElementById("overlay11").style.visibility='visible';
       }
    } 
</script>
<script type="text/javascript">
try{	
	 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">
	 disabledAll();
	 	</sec-auth:authComponent>
}
catch(e){}

</script>
<script>
setTimeout("showOrHide(0)",2000);
</script>
<script type="text/javascript">
try{
	var sectionName=document.forms['contractPolicyForm'].elements['contractPolicy.sectionName'].value;
	var language=document.forms['contractPolicyForm'].elements['contractPolicy.language'].value;
	if(sectionName=='' || language==''){
	document.getElementById("xx").disabled =true;
	}	
}catch(e){
	
}

</script>
 