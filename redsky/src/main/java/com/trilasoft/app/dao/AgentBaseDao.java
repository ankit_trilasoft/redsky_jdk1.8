package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.AgentBase;

public interface AgentBaseDao extends GenericDao<AgentBase, Long>  {
	
	public List getbaseListByPartnerCode(String partnerCode);
	
	public List isExisted(String baseCode, String sessionCorpID, String partnerCode);
	
	public void deleteBaseSCAC(String partnerCode, String baseCode, String sessionCorpID);
	
}
