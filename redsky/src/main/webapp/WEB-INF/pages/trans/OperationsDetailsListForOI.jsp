<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
.hidden { display: none;}
.inputLink {width:40px;height:19px;cursor:pointer;text-decoration:underline;}
.textarea-comment {border:1px solid #219dd1;color:#000;height:17px; width:180px}
.textarea-comment:focus{height:45px; width: 200px;}
.avoid-clicks { pointer-events: none;}
</style>
<script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
<script type="text/javascript" src="scripts/ajax-dynamic-content.js"></script>
 <script type="text/javascript" src="scripts/ajax.js"></script>
 <script type="text/javascript" src="scripts/ajax-tooltip.js"></script>
 <link rel="stylesheet" href="styles/ajax-tooltip.css" media="screen" type="text/css">
 <link rel="stylesheet" href="styles/ajax-tooltip-demo.css" media="screen" type="text/css">
<script>
function showScopeForWO(id,workorder,position){
    var shipNumber = '${shipNumber}';
    var url = "showScopeOfWOForOI.html?decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&shipNumber="+shipNumber;
    ajax_showTooltip(url,position);
    var crewDiv = $('#ajax_tooltipObj').remove();
    crewDiv.appendTo('#distinctOperationResourceList');
    var divscroll = document.getElementById("distinctOperationResourceList").scrollLeft;
      var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;   
    document.getElementById("ajax_tooltipObj").style.left = myposition+'px';
   
    var divscroll2 = document.getElementById("distinctOperationResourceList").scrollTop;
      var myposition2 = document.getElementById("ajax_tooltipObj").offsetTop-divscroll2 ;  
    document.getElementById("ajax_tooltipObj").style.top = ((parseInt(myposition2))-160)+'px';//((parseInt(myposition2))-25)+'px';
}
</script>


<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
 <c:set value="00" var="workOrder"/> 
 <c:set var="myStyle" value="background:#CBEBFF none repeat scroll 0 0;" />
<c:set var="myStylesec" value="background:#FBFBFB none repeat scroll 0 0;" />
<s:set name="distinctOperationResourceList" value="distinctOperationResourceList" scope="request"/>
 <c:if test="${distinctOperationResourceList!='[]'}"> 
 <display:table name="distinctOperationResourceList" class="table" requestURI="" id="distinctOperationResourceList" style="margin-bottom:5px;width:95%;"> 
    
    <display:column title="Scope" style="width:5px;" >
 <a><img align="middle" id="scope${distinctOperationResourceList.id}" title="<c:out value="${distinctOperationResourceList.scopeOfWorkOrder}"></c:out>" style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/externalScope.png"  /></a>
  </display:column>
    <display:column  style="width:5px;">    
        <img id="show-${distinctOperationResourceList.id}" onclick ="showChild('${distinctOperationResourceList.id}','child${distinctOperationResourceList.id}',this,this.id,'${distinctOperationResourceList.ticket}','${distinctOperationResourceList.workTicketId}','${distinctOperationResourceList.shipNumber}','${distinctOperationResourceList.workorder}');" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
        <img id="hide-${distinctOperationResourceList.id}" onclick ="hideChild('${distinctOperationResourceList.id}','${distinctOperationResourceList_rowNum}',this.id);" src="${pageContext.request.contextPath}/images/minus1-small.png" style="display: none;" HEIGHT=14 WIDTH=14 ALIGN=TOP />
    </display:column>
 
    <display:column  title="WO#"  style="width:100px;">
   <input type="text" class="input-textUpper pr-f11" name="workOrderOI${distinctOperationResourceList.id}" id="workOrderOI${distinctOperationResourceList.id}"  value="${distinctOperationResourceList.workorder}" style="width:45px" disabled="true"/> 
    </display:column>
     <display:column  title="Ticket"  style="width:100px;">
   <input type="text" class="input-textUpper pr-f11" name="workOrderOI${distinctOperationResourceList.id}" id="ticket${distinctOperationResourceList.id}"  value="${distinctOperationResourceList.ticket}" style="width:40px" disabled="true"/> 
    </display:column>
    <display:column  title="Status"  style="width:100px;">
   <input type="text" class="input-textUpper pr-f11" name="workOrderOI${distinctOperationResourceList.id}" id="ticket${distinctOperationResourceList.id}"  value="${distinctOperationResourceList.targetActual}" style="width:70px" disabled="true"/> 
    
    </display:column>
    
    <display:column  title='Date'  style="width:100px;">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${distinctOperationResourceList.date}" var="parsedAtDepartDate" />
    <fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" /> 
    <s:textfield cssClass="input-textUpper pr-f11" id="date${distinctOperationResourceList.id}" name="date${distinctOperationResourceList.id}" value="${formattedAtDepartDate}" onkeydown="" required="true" cssStyle="width:60px" maxlength="11" readonly="true" tabindex="72"/>
    </display:column>
    
    <display:column  title="Service" style="width:100px;">
     <select id="type${distinctOperationResourceList.id}" name ="type${distinctOperationResourceList.id}" disabled="true" style="width:200px" class="list-menuUpper pr-f11" > 
    <c:forEach var="chrms" items="${tcktservc}" varStatus="loopStatus">
    <c:choose>
    <c:when test="${chrms.key == distinctOperationResourceList.service}">
    <c:set var="selectedInd" value=" selected"></c:set>
    </c:when>
    <c:otherwise>
    <c:set var="selectedInd" value=""></c:set>
    </c:otherwise>
    </c:choose>
    <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach> 
    </select>
    </display:column>
    <display:column title="&nbsp;Crew&nbsp;Details&nbsp;" style="width:10%;color:red;" >
             <c:choose>
              <c:when test="${flag }">
                <c:if test="${distinctOperationResourceList.ticket !='' && distinctOperationResourceList.ticket !=null }" >
                    <img id="a-${distinctOperationResourceList.id }" align="middle" title="Crew Details" onmouseover="findToolTipResource123('${distinctOperationResourceList.ticket}','C',this,'${distinctOperationResourceList.corpId}');" onmouseout="ajax_hideTooltip();"  src="${pageContext.request.contextPath}/images/client_account.png"/>        
                </c:if>
                </c:when>
               <c:otherwise>
                <c:if test="${distinctOperationResourceList.ticket !='' && distinctOperationResourceList.ticket !=null }" >
                    <img id="a-${distinctOperationResourceList.id }" align="middle" title="Crew Details" onclick="findToolTipResource('${distinctOperationResourceList.ticket}','C',this,'${distinctOperationResourceList.corpId}');" onmouseover="findToolTipResource('${distinctOperationResourceList.ticket}','C',this,'${distinctOperationResourceList.corpId}');" onmouseout="ajax_hideTooltip();"  src="${pageContext.request.contextPath}/images/client_account.png"/>        
                </c:if>
               </c:otherwise>
               </c:choose>
    </display:column> 
 </display:table> 
 </c:if>
 <c:if test="${distinctOperationResourceList=='[]'}"> 
 <display:table name="resourceListForOI" class="table" requestURI="" id="resourceListForOI" style="margin-bottom:5px;"> 
    <display:column title="Scope" style="width:5px;" />
    <display:column  style="width:5px;" />    
    <display:column  title="WO#"  style="width:100px;" />
    <display:column  title="Ticket"  style="width:100px;" />
    <display:column  title="Status"  style="width:100px;" />
    <display:column  title='Date'  style="width:100px;" />
    <display:column  title="Service" style="width:100px;" />
    <display:column title="&nbsp;Crew&nbsp;Details&nbsp;" style="width:10%;color:red;" />
 </display:table> 
 </c:if>
 <table width="100%" cellpadding="2" class="detailTabLabel">
 <tr>
 
 <td></td>
 </tr>
 </table>
 <script type="text/javascript"> 
function showChild(id,divId,position,rowId,ticket,workTicketId,shipNumber,workorder){
	var flagForDetails = ${flag};
	document.getElementById('show-'+id).style.display='none';
    document.getElementById('hide-'+id).style.display='block';
    var table=document.getElementById("distinctOperationResourceList");
    var rownum = document.getElementById(rowId);
    var myrow = rownum.parentNode;
    var newrow = myrow.parentNode.rowIndex;
    val = newrow+1;
    var row=table.insertRow(val);
    row.setAttribute("id","ch"+id);
    row.setAttribute("name",id);
    row.innerHTML = "<td colspan=\"10\"><div id="+id+"></div></td>";
    if(ticket!=null && !ticket==''){
    var url='showTaskDetails.html?ajax=1&ticket='+ticket+'&workTicketId='+workTicketId+'&shipNumber='+shipNumber+'&workorder='+workorder+'&flag='+flagForDetails+'&decorator=simple&popup=true&currentId='+id;
    new Ajax.Request(url,
              {
                method:'get',
                onSuccess: function(transport){
                  var response = transport.responseText || "no response text";
                   var mydiv = document.getElementById(id);
                  mydiv.innerHTML = response;
                  mydiv.style.display='block';
                  setOnSelectBasedMethods(["saveDateInTaskDetails()"]);
                  setCalendarFunctionality();
                  
                },
                onFailure: function(){
                    alert('Something went wrong...') 
                    }
              });
    }else{
          var mydiv = document.getElementById(id);
          mydiv.innerHTML = 'Nothing Found To Display.';
          mydiv.style.display='block';
    }
         return 0;
}

function hideChild(id,rowNum,rowId){
    val = "ch"+id;
    var table = document.getElementById('distinctOperationResourceList');
    var rowCount = table.rows.length;
    for(var i=0; i<rowCount; i++) {
        var row = table.rows[i];
        if(row != undefined && row != null && val == row.id){
            table.deleteRow(i);
            document.getElementById('show-'+id).style.display='block';
            document.getElementById('hide-'+id).style.display='none';
        }
    }
}

function addTaskDetails(ticket,workTicketId,workorder,shipNumber,currentId){
	$.get("addTaskDetailsByAjax.html?ajax=1&decorator=simple&popup=true", 
            {ticket:ticket,workTicketId:workTicketId,workorder:workorder,shipNumber:shipNumber},
            function(data){             
            	 findAllOperationsDetails(currentId);
            	// document.getElementById('show-'+id).style.display='block';
            	 
        });	
}

function findToolTipResource(ticket,type,position,sessionCorpID){
	var url="findToolTipResourceDetail.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket)+"&type="+encodeURI(type)+"&sessionCorpID="+encodeURI(sessionCorpID);
	ajax_showTooltip(url,position);

	 var crewDiv = $('#ajax_tooltipObj').remove();
	    crewDiv.appendTo('#distinctOperationResourceList');
	    
	    /* var crewDiv2 = $('#ajax_tooltipObj').detach();
	    crewDiv3.appendTo('#distinctOperationResourceList'); */
	    
	    var divscroll = document.getElementById("distinctOperationResourceList").scrollLeft;
	      var myposition = document.getElementById("ajax_tooltipObj").offsetLeft-divscroll ;   
	    document.getElementById("ajax_tooltipObj").style.left = myposition+'px';
	   
	    var divscroll2 = document.getElementById("distinctOperationResourceList").scrollTop;
	      var myposition2 = document.getElementById("ajax_tooltipObj").offsetTop-divscroll2 ;  
	    document.getElementById("ajax_tooltipObj").style.top = ((parseInt(myposition2))-160)+'px';//((parseInt(myposition2))-25)+'px';
	}
	
function findToolTipResource123(ticket,type,position,sessionCorpID){
    var url="findToolTipResourceDetail.html?ajax=1&decorator=simple&popup=true&ticket="+encodeURI(ticket)+"&type="+encodeURI(type)+"&sessionCorpID="+encodeURI(sessionCorpID);
    ajax_showTooltip(url,position);

    var containerdiv =  document.getElementById("distinctOperationResourceList").scrollLeft;
    var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;  
    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';   
}

</script> 