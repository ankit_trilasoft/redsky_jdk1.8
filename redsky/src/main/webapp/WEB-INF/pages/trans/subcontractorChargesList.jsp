<!-- *File Name:subcontractorCharges.jsp 
 * Created By:Ashish Mishra 
 * Created Date:08-Aug-2008
 * Summary: This jsp is created for showing List Subcontractor Charges.
 -->
<%@ include file="/common/taglibs.jsp"%> 
<head>   
    <title><fmt:message key="subcontractorChargesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='subcontractorChargesList.heading'/>"/> 
    <style type="text/css">


/* collapse */

.table tfoot td {
border:1px solid #E0E0E0;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

#mydiv{margin-top:-45px; }
</style> 
 <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

<!-- Modification closed here -->
<script language="javascript" type="text/javascript">

function clear_fields(){
	
			
	                document.forms['subcontractorChargesList'].elements['subcontractorCharges.id'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.personId'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.personType'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.serviceOrder'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.description'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.regNumber'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.approved'].value = "";
					document.forms['subcontractorChargesList'].elements['subcontractorCharges.advanceDate'].value = "";
					document.forms['subcontractorChargesList'].elements['settled'].checked = false;
			
}


</script>

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>

<style>
span.pagelinks {
display:block;
font-size:0.9em;
margin-top:-12px;
text-align:right;
}
</style>


  </head>  
  <s:form id="subcontractorChargesList" name="subcontractorChargesForm" action="searchSubcontractorCharges" validate="true" onsubmit="return checkNumber('subcontractorChargesList','subcontractorCharges.id','Please Enter Valid Id');" method="post"> 
   <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="YES"/>
</configByCorp:fieldVisibility>
  <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
  <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
  <s:hidden name="formStatus" />
 
  <c:if test="${!param.popup}">
  
  
  <div id="Layer1" style="width: 100%">

 	<div id="otabs" class="form_magn">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width: 100%;"><!-- sandeep -->
<thead>
<tr>
<th>TransactionID</th>
<th><fmt:message key="subcontractorCharges.personIdList"/></th>
<th><fmt:message key="subcontractorCharges.personTypeList"/></th>
<th>S/O#</th>
<th>Description</th>
<th><fmt:message key="subcontractorCharges.regNumber"/></th>
<th>Settled</th>
<th>Advance Date</th>
<th><fmt:message key="subcontractorCharges.approved"/></th>

</tr></thead> 

<tbody>
  <tr>
    <td>
       <s:textfield name="subcontractorCharges.id"  cssClass="input-text" onkeyup="validCheck(this,'special1')"  size="9" cssStyle="width:120px;"/>
   </td>
   <td>
       <s:textfield name="subcontractorCharges.personId" required="true" cssClass="input-text" size="12" cssStyle="width:120px;"/>
   </td>
    <td>
       <s:select cssClass="list-menu"   name="subcontractorCharges.personType" list="%{personTypeList}" headerKey="" headerValue="" cssStyle="width:120px"/>
    </td>
   <td>
       <s:textfield name="subcontractorCharges.serviceOrder" required="true" cssClass="input-text" size="15" cssStyle="width:120px;"/>
   </td>
   <td>
    	<s:textfield name="subcontractorCharges.description" required="true" cssClass="input-text" size="18" cssStyle="width:120px;"/>
       
   </td>
   <td>
    	<s:textfield name="subcontractorCharges.regNumber" required="true" cssClass="input-text" size="12" cssStyle="width:120px;"/>
       
   </td>
						
	<td align="right">
  	   <s:checkbox key="settled" />
	</td>
   	         <c:if test="${not empty subcontractorCharges.advanceDate}">
				<s:text id="subcontractorChargesDateValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.advanceDate" /></s:text>
				<td align="left" width="110px"><s:textfield cssClass="input-text" id="advanceDate" name="subcontractorCharges.advanceDate" value="%{subcontractorChargesDateValue}" required="true" cssStyle="width:60px" maxlength="11" readonly="true" /><img id="advanceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty subcontractorCharges.advanceDate}">
				<td align="left" width="110px"><s:textfield cssClass="input-text" id="advanceDate" name="subcontractorCharges.advanceDate" required="true" cssStyle="width:60px" maxlength="11" readonly="true" /><img id="advanceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
    	<!--<s:textfield name="subcontractorCharges.approved" required="true" cssClass="input-text" size="20"/>
    	--><c:if test="${not empty subcontractorCharges.approved}">
				<s:text id="subcontractorChargesValue" name="${FormDateValue}"><s:param name="value" value="subcontractorCharges.approved" /></s:text>
				<td align="left" width="110px"><s:textfield cssClass="input-text" id="approved" name="subcontractorCharges.approved" value="%{subcontractorChargesValue}" required="true" cssStyle="width:60px" maxlength="11" readonly="true" /><img id="approved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty subcontractorCharges.approved}">
				<td align="left" width="110px"><s:textfield cssClass="input-text" id="approved" name="subcontractorCharges.approved" required="true" cssStyle="width:60px" maxlength="11" readonly="true" /><img id="approved_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			
     </tr>
     <tr>
     <td colspan="6"></td>
   <td colspan="3" style="border-left:hidden;text-align:right;">
       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;" method="" key="button.search" />  
       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
       
   </td>
   
  </tr>
  </tbody>
 </table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />  
</c:if>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Subcontractor Charges List</span></a></li>
		  </ul>
		</div>
	<div class="spnblk">&nbsp;</div>
	  <display:table name="subcontractorChargesExt" class="table" id="subcontractorChargesList" requestURI=""  defaultsort="1" pagesize="10" export="true">
      <%-- <display:column property="personId" sortable="true" titleKey="subcontractorCharges.personIdList" href="editsubcontractorCharges.html" paramId="id" paramProperty="id" /> --%>
       <display:column sortable="true" titleKey="subcontractorCharges.personIdList" >
	   <a onclick="setUrl(this,${subcontractorChargesList.id})" href="#"> 
	   <c:if test="${subcontractorChargesList.personId=='' || subcontractorChargesList.personId==null}">
				<img class="openpopup" height="20" width="100%" align="top" src="images/blank.png">
	 </c:if>
	 <c:if test="${subcontractorChargesList.personId!='' && subcontractorChargesList.personId!=null}">
				<c:out value="${subcontractorChargesList.personId}" />
	 </c:if> 
	   </a>
	</display:column>
      <display:column property="personType" sortable="true" titleKey="subcontractorCharges.personTypeList"/>
      <display:column headerClass="containeralign" sortable="true" titleKey="subcontractorCharges.amount" sortProperty="amount">
      <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" groupingUsed="true" value="${subcontractorChargesList.amount}" /></div>
      </display:column>
      <display:column property="branch" sortable="true" titleKey="subcontractorCharges.branch"/>
      <display:column  sortable="true" title="S/O#" sortProperty="serviceOrder">
      <div ><c:out value="${subcontractorChargesList.serviceOrder}" /></div>
      </display:column>
      <display:column property="description" sortable="true" titleKey="subcontractorCharges.description"/>
      <display:column headerClass="containeralign" style="text-align: right;" property="regNumber" sortable="true" titleKey="subcontractorCharges.regNumber"/>
      <display:column property="approved" title="Approved" sortable="true" format="{0,date,dd-MMM-yyyy}"/>
       <display:column property="advanceDate" sortable="true" title="Advance Date" format="{0,date,dd-MMM-yyyy}"/>
     <display:column property="accountSent" sortable="true" title="Sent Ac" format="{0,date,dd-MMM-yyyy}"/> 
 

     
    </display:table>
    <table class="" cellspacing="0" cellpadding="1" border="0" style="width:100%;margin:0px;">
 <tbody>
   <tr>
	 <td>  
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="setUrlForAddBtn(this);" value="<fmt:message key="button.add"/>"/>  
  </td>
  </tr>
  </tbody>
  </table>
  <div id="mydiv" style="position:absolute;" ></div>
  
  </s:form>
  <script type="text/javascript">
function setUrlForAddBtn(addbtnref){
	//alert("111");location.href='<c:url value="/editsubcontractorCharges.html"/>'
	var chkComp1="";
	<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	 chkComp1=document.forms['subcontractorChargesForm'].elements['glCodeFlag'].value;
	 </configByCorp:fieldVisibility>
	// alert(chkComp);
	 location.href='editsubcontractorCharges.html?&glCodeFlag='+chkComp1;	 
	  
}
</script>
   <script type="text/javascript">      
      function setUrl(aref,id11){
    	 var chkComp="";
    	  <configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
    	  chkComp=document.forms['subcontractorChargesForm'].elements['glCodeFlag'].value;
    	  </configByCorp:fieldVisibility>
    	 // var id11='${subcontractorChargesList1.id}';editsubcontractorCharges.html
    	 // alert(chkComp);
    	 // alert(id11);
    	  aref.href='editsubcontractorCharges.html?id='+id11+'&glCodeFlag='+chkComp;
    	  
      }
     
      
      </script>  
  <script type="text/javascript">
var r1={
		 'special1':/['\ '&'\#'&'\$'&'\~'&'\!'&'\@'&'\+'&'\\'&'\/'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'('&'\)']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		
function validCheck(targetElement,w){
	var numbers = /^[0-9]+$/;  
	targetElement.value = targetElement.value.replace(r1[w],'');
    if(targetElement.value.match(numbers)) {
    	return true;
    }else{
    	alert('Enter Valid Number');
    	document.forms['subcontractorChargesList'].elements['subcontractorCharges.id'].value='';
    	return false;
    }
}
</script>
<script type="text/javascript">
    highlightTableRows("subcontractorChargesList");
	   	try{
	   	if('${settled}'=='true')
   	{
   		document.forms['subcontractorChargesList'].elements['settled'].checked = true;
   	}
   	}
   	catch(e){}
  try{
   if('${settled}'=='false')
   	{
   		document.forms['subcontractorChargesList'].elements['settled'].checked = false;
   	}
   	}
   	catch(e){}
    
</script>
  
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>     