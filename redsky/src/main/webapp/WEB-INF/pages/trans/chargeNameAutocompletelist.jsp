<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style>
.pr-f11{	font-family:arial,verdana;	font-size:11px;	}
</style>
</head>   
<s:form id="chargeDetailsForm" method="post" > 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow:auto;height:auto;min-width:450px;">
 <c:if test="${SearchforAutocompleteListChargeCode!='[]'}">
  <display:table  name="SearchforAutocompleteListChargeCode" class="table" id="SearchforAutocompleteListChargeCode" style="width:100%;font-size:1em;">
  
   	<display:column class="pr-f11"  title="Charge" >
   	<a onclick="copyChargeDetails('${SearchforAutocompleteListChargeCode.charge}','${chargeCodeId}','${autocompleteDivId}','${id}','${idCondition}');">${SearchforAutocompleteListChargeCode.charge}</a>
   	</display:column>
  	<display:column property="description" class="pr-f11" title="Description" ></display:column>
</display:table>
 </c:if>
 <c:if test="${SearchforAutocompleteListChargeCode=='[]'}">
  <display:table name="SearchforAutocompleteListChargeCode" class="table pr-f11" id="SearchforAutocompleteListChargeCode" style="width:100%;font-size:1em;">
	<display:column  title="Charge" >
   	</display:column>
  	<display:column  title="Description" ></display:column>
	<td  class="listwhitetextWhite pr-f11" colspan="2">Nothing Found To Display.</td>  
</display:table>
<script type="text/javascript">
closeMyChargeDiv2('${chargeCodeId}','${id}');
</script>
 </c:if>

</div>
</td>
 <c:if test="${SearchforAutocompleteListChargeCode!='[]'}">
 <td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyChargeDiv('${autocompleteDivId}','${SearchforAutocompleteListChargeCode.charge}','${chargeCodeId}','${id}','${idCondition}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
 </c:if>
  <c:if test="${SearchforAutocompleteListChargeCode=='[]'}">
   <td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMyChargeDiv('${autocompleteDivId}','','${chargeCodeId}','${id}','${idCondition}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
   
  </c:if>
</tr>
</table>  
</s:form>
