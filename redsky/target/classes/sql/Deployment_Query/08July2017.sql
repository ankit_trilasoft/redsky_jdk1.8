// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

 ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `storageBilling` VARCHAR(100) DEFAULT '',
 ADD COLUMN `SITBilling` VARCHAR(100) DEFAULT '' AFTER `storageBilling`,
 ADD COLUMN `TPSBilling` VARCHAR(100) DEFAULT '' AFTER `SITBilling`;
 
 
 ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `SCSBilling` VARCHAR(100) DEFAULT '';
 
 update systemdefault set storageBilling="('STO','STF','STL','OIS')",SITBilling="('UVL','MVL')",TPSBilling="('TPS')",SCSBilling="('SCS')" where vatCalculation is false;
update systemdefault set storageBilling="('STO','STF','STL')",SITBilling="('UVL','MVL')",TPSBilling="('TPS')",SCSBilling="" where vatCalculation is true;
update systemdefault set storageBilling="('STO','STF','STL')",SITBilling="('UVL','MVL','INT')",TPSBilling="('TPS')",SCSBilling="" where corpid='UTSI';

ALTER TABLE `redsky`.`defaultaccountline` CHANGE COLUMN `orderNumber` `orderNumber` INT(5) NOT NULL  ;

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `GSTRegnNo` VARCHAR(25) DEFAULT '';

ALTER TABLE `redsky`.`charges` ADD COLUMN `GSTHSNCode` VARCHAR(25) DEFAULT '' ;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `surveyReferenceNumber` VARCHAR(30) NULL  AFTER `internalBillingPerson` ;

ALTER TABLE `redsky`.`defaultaccountline` ADD INDEX `Index_2`(`corpID`);

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `globallyUniqueIdentifier` VARCHAR(100) NULL  AFTER `childParameterValues` ;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `orgLocId` VARCHAR(100) NULL;
ALTER TABLE `redsky`.`customerfile` ADD COLUMN `destLocId` VARCHAR(100) NULL;
ALTER TABLE `redsky`.`crew` ADD COLUMN `crewEmail` VARCHAR(82) NULL  AFTER `contractor` ;
ALTER TABLE `redsky`.`timesheet` ADD COLUMN `crewEmail` VARCHAR(82) NULL;
ALTER TABLE `redsky`.`timesheet` ADD COLUMN `integrationTool` VARCHAR(5) NULL;
ALTER TABLE `redsky`.`timesheet` ADD COLUMN `integrationStatus` VARCHAR(100) NULL;
ALTER TABLE `redsky`.`company` ADD COLUMN `integrationUser` VARCHAR(82) NULL  AFTER `acctRefSameParent` , ADD COLUMN `integrationPassword` VARCHAR(45) NULL  AFTER `integrationUser` ;
ALTER TABLE `redsky`.`crew` ADD COLUMN `integrationPassword` VARCHAR(45) NULL  AFTER `crewEmail` ;
-- 
