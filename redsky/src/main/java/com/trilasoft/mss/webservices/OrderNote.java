
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for OrderNote complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrderNote">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NoteTimestampUTC" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="SideOorD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrderNote", propOrder = {
    "noteTimestampUTC",
    "sideOorD",
    "note"
})
public class OrderNote {

    @XmlElement(name = "NoteTimestampUTC", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar noteTimestampUTC;
    @XmlElement(name = "SideOorD")
    protected String sideOorD;
    @XmlElement(name = "Note")
    protected String note;

    /**
     * Gets the value of the noteTimestampUTC property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNoteTimestampUTC() {
        return noteTimestampUTC;
    }

    /**
     * Sets the value of the noteTimestampUTC property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNoteTimestampUTC(XMLGregorianCalendar value) {
        this.noteTimestampUTC = value;
    }

    /**
     * Gets the value of the sideOorD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSideOorD() {
        return sideOorD;
    }

    /**
     * Sets the value of the sideOorD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSideOorD(String value) {
        this.sideOorD = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

}
