/**
 * Implementation of <strong>ModelSupport</strong> that contains convenience methods.
 * This class represents the basic model on "Container" object in Redsky that allows for Job management.
 * @Class Name	Container
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */



package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="container")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Container extends BaseObject {
	
	private Long id; 
	private Long serviceOrderId;
    private String corpID;
    private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String cntnrNumber;
	private String numberOfContainer;
	private String containerNumber;
	private String sealNumber;
	private String customSeal;
	private String size;
	private BigDecimal grossWeight= new BigDecimal(0);
	private BigDecimal emptyContWeight= new BigDecimal(0);
	private BigDecimal netWeight;
	private BigDecimal volume = new BigDecimal(0);
	private BigDecimal grossWeightKilo= new BigDecimal(0);
	private BigDecimal emptyContWeightKilo= new BigDecimal(0);
	private BigDecimal netWeightKilo;
	private BigDecimal volumeCbm = new BigDecimal(0);
	private Integer pieces=0;
	private BigDecimal useDsp;
	private BigDecimal density;
	private BigDecimal densityMetric;
	private String totalLine;
	private String idNumber;
	private Date topier;
	private Date pickup;
	private Date deliver;
	private String tripNumber;
	private ServiceOrder serviceOrder;
	private String unit1;
	private String unit2;
	private BigDecimal grossWeightTotal;
	private BigDecimal totalNetWeight;
	private BigDecimal totalVolume; 
	private Integer totalPieces;
	private Date sealDate;
	private Long containerId;
	private String ugwIntId;
	private String pickupPier;
	private String redeliveryPier;
	private boolean status = true;
	private Date vgmCutoff;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("serviceOrderId", serviceOrderId)
				.append("corpID", corpID)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("cntnrNumber", cntnrNumber)
				.append("numberOfContainer", numberOfContainer)
				.append("containerNumber", containerNumber)
				.append("sealNumber", sealNumber)
				.append("customSeal", customSeal).append("size", size)
				.append("grossWeight", grossWeight)
				.append("emptyContWeight", emptyContWeight)
				.append("netWeight", netWeight).append("volume", volume)
				.append("grossWeightKilo", grossWeightKilo)
				.append("emptyContWeightKilo", emptyContWeightKilo)
				.append("netWeightKilo", netWeightKilo)
				.append("volumeCbm", volumeCbm).append("pieces", pieces)
				.append("useDsp", useDsp).append("density", density)
				.append("densityMetric", densityMetric)
				.append("totalLine", totalLine).append("idNumber", idNumber)
				.append("topier", topier).append("pickup", pickup)
				.append("deliver", deliver).append("tripNumber", tripNumber)
				.append("unit1", unit1)
				.append("unit2", unit2)
				.append("grossWeightTotal", grossWeightTotal)
				.append("totalNetWeight", totalNetWeight)
				.append("totalVolume", totalVolume)
				.append("totalPieces", totalPieces)
				.append("sealDate", sealDate)
				.append("containerId", containerId)
				.append("ugwIntId", ugwIntId)
				.append("pickupPier",pickupPier)
				.append("redeliveryPier",redeliveryPier)
				.append("status",status)
				.append("vgmCutoff",vgmCutoff).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Container))
			return false;
		Container castOther = (Container) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(corpID, castOther.corpID)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(cntnrNumber, castOther.cntnrNumber)
				.append(numberOfContainer, castOther.numberOfContainer)
				.append(containerNumber, castOther.containerNumber)
				.append(sealNumber, castOther.sealNumber)
				.append(customSeal, castOther.customSeal)
				.append(size, castOther.size)
				.append(grossWeight, castOther.grossWeight)
				.append(emptyContWeight, castOther.emptyContWeight)
				.append(netWeight, castOther.netWeight)
				.append(volume, castOther.volume)
				.append(grossWeightKilo, castOther.grossWeightKilo)
				.append(emptyContWeightKilo, castOther.emptyContWeightKilo)
				.append(netWeightKilo, castOther.netWeightKilo)
				.append(volumeCbm, castOther.volumeCbm)
				.append(pieces, castOther.pieces)
				.append(useDsp, castOther.useDsp)
				.append(density, castOther.density)
				.append(densityMetric, castOther.densityMetric)
				.append(totalLine, castOther.totalLine)
				.append(idNumber, castOther.idNumber)
				.append(topier, castOther.topier)
				.append(pickup, castOther.pickup)
				.append(deliver, castOther.deliver)
				.append(tripNumber, castOther.tripNumber) 
				.append(unit1, castOther.unit1).append(unit2, castOther.unit2)
				.append(grossWeightTotal, castOther.grossWeightTotal)
				.append(totalNetWeight, castOther.totalNetWeight)
				.append(totalVolume, castOther.totalVolume)
				.append(totalPieces, castOther.totalPieces)
				.append(sealDate, castOther.sealDate)
				.append(containerId, castOther.containerId)
				.append(ugwIntId, castOther.ugwIntId)
				.append(pickupPier, castOther.pickupPier)
				.append(redeliveryPier, castOther.redeliveryPier)
				.append(status, castOther.status)
				.append(vgmCutoff, castOther.vgmCutoff).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(serviceOrderId)
				.append(corpID).append(sequenceNumber).append(ship)
				.append(shipNumber).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(cntnrNumber)
				.append(numberOfContainer).append(containerNumber)
				.append(sealNumber).append(customSeal).append(size)
				.append(grossWeight).append(emptyContWeight).append(netWeight)
				.append(volume).append(grossWeightKilo)
				.append(emptyContWeightKilo).append(netWeightKilo)
				.append(volumeCbm).append(pieces).append(useDsp)
				.append(density).append(densityMetric).append(totalLine)
				.append(idNumber).append(topier).append(pickup).append(deliver)
				.append(tripNumber).append(unit1)
				.append(unit2).append(grossWeightTotal).append(totalNetWeight)
				.append(totalVolume).append(totalPieces).append(sealDate)
				.append(containerId).append(ugwIntId)
				.append(pickupPier).append(redeliveryPier)
				.append(status)
				.append(vgmCutoff).toHashCode();
	}
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}  
    
	
	
	@Column(length=10)
	public String getCntnrNumber() {
		return cntnrNumber;
	}
	public void setCntnrNumber(String cntnrNumber) {
		this.cntnrNumber = cntnrNumber;
	}
	@Column(length=50)
	public String getContainerNumber() {
		return containerNumber;
	}
	public void setContainerNumber(String containerNumber) {
		this.containerNumber = containerNumber;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getDeliver() {
		return deliver;
	}
	public void setDeliver(Date deliver) {
		this.deliver = deliver;
	}
	@Column(length=20)
	public BigDecimal getDensity() {
		return density;
	}
	public void setDensity(BigDecimal density) {
		this.density = density;
	}
	@Column(length=20)
	public BigDecimal getEmptyContWeight() {
		return emptyContWeight;
	}
	public void setEmptyContWeight(BigDecimal emptyContWeight) {
		this.emptyContWeight = emptyContWeight;
	}
	@Column(length=20)
	public BigDecimal getGrossWeight() {
		return grossWeight;
	}
	public void setGrossWeight(BigDecimal grossWeight) {
		this.grossWeight = grossWeight;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=10)
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}
	@Column(length=20)
	public BigDecimal getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(BigDecimal netWeight) {
		this.netWeight = netWeight;
	}
	@Column(length=1)
	public String getNumberOfContainer() {
		return numberOfContainer;
	}
	public void setNumberOfContainer(String numberOfContainer) {
		this.numberOfContainer = numberOfContainer;
	}
	@Column
	public Date getPickup() {
		return pickup;
	}
	public void setPickup(Date pickup) {
		this.pickup = pickup;
	}
	@Column(length=5)
	public Integer getPieces() {
		return pieces;
	}
	public void setPieces(Integer pieces) {
		this.pieces = pieces;
	}
	@Column(length=25)
	public String getSealNumber() {
		return sealNumber;
	}
	public void setSealNumber(String sealNumber) {
		this.sealNumber = sealNumber;
	}
	@Column(length=15)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length=2)
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column(length=15)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=20)
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	@Column
	public Date getTopier() {
		return topier;
	}
	public void setTopier(Date topier) {
		this.topier = topier;
	}
	@Column(length=1)
	public String getTotalLine() {
		return totalLine;
	}
	public void setTotalLine(String totalLine) {
		this.totalLine = totalLine;
	}
	@Column(length=10)
	public String getTripNumber() {
		return tripNumber;
	}
	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=20)
	public BigDecimal getUseDsp() {
		return useDsp;
	}
	public void setUseDsp(BigDecimal useDsp) {
		this.useDsp = useDsp;
	}
	@Column(length=20)
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	@Column(length=10)
	public String getUnit1() {
		return unit1;
	}
	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length=10)
	public String getUnit2() {
		return unit2;
	}
	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Column
	public BigDecimal getGrossWeightTotal() {
		return grossWeightTotal;
	}
	public void setGrossWeightTotal(BigDecimal grossWeightTotal) {
		this.grossWeightTotal = grossWeightTotal;
	}
	@Column
	public BigDecimal getTotalNetWeight() {
		return totalNetWeight;
	}
	public void setTotalNetWeight(BigDecimal totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}
	@Column
	public Integer getTotalPieces() {
		return totalPieces;
	}
	public void setTotalPieces(Integer totalPieces) {
		this.totalPieces = totalPieces;
	}
	@Column
	public BigDecimal getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(BigDecimal totalVolume) {
		this.totalVolume = totalVolume;
	}
	@Column(length=14)
	public String getCustomSeal() {
		return customSeal;
	}
	public void setCustomSeal(String customSeal) {
		this.customSeal = customSeal;
	}
	/**
	 * @return the serviceOrderId
	 */
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	/**
	 * @param serviceOrderId the serviceOrderId to set
	 */
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	/**
	 * @return the deleteStatus
	 */
	/**
	 * @return the status
	 */
	/**
	 * @return the emptyContWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getEmptyContWeightKilo() {
		return emptyContWeightKilo;
	}
	/**
	 * @param emptyContWeightKilo the emptyContWeightKilo to set
	 */
	public void setEmptyContWeightKilo(BigDecimal emptyContWeightKilo) {
		this.emptyContWeightKilo = emptyContWeightKilo;
	}
	/**
	 * @return the grossWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getGrossWeightKilo() {
		return grossWeightKilo;
	}
	/**
	 * @param grossWeightKilo the grossWeightKilo to set
	 */
	public void setGrossWeightKilo(BigDecimal grossWeightKilo) {
		this.grossWeightKilo = grossWeightKilo;
	}
	/**
	 * @return the netWeightKilo
	 */
	@Column(length=20)
	public BigDecimal getNetWeightKilo() {
		return netWeightKilo;
	}
	/**
	 * @param netWeightKilo the netWeightKilo to set
	 */
	public void setNetWeightKilo(BigDecimal netWeightKilo) {
		this.netWeightKilo = netWeightKilo;
	}
	/**
	 * @return the volumeCbm
	 */
	@Column(length=20)
	public BigDecimal getVolumeCbm() {
		return volumeCbm;
	}
	/**
	 * @param volumeCbm the volumeCbm to set
	 */
	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}
	/**
	 * @return the densityMetric
	 */
	@Column(length=20)
	public BigDecimal getDensityMetric() {
		return densityMetric;
	}
	/**
	 * @param densityMetric the densityMetric to set
	 */
	public void setDensityMetric(BigDecimal densityMetric) {
		this.densityMetric = densityMetric;
	}
	@Column
	public Date getSealDate() {
		return sealDate;
	}
	public void setSealDate(Date sealDate) {
		this.sealDate = sealDate;
	}
	@Column
	public Long getContainerId() {
		return containerId;
	}
	public void setContainerId(Long containerId) {
		this.containerId = containerId;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	@Column
	public String getPickupPier() {
		return pickupPier;
	}
	public void setPickupPier(String pickupPier) {
		this.pickupPier = pickupPier;
	}
	@Column
	public String getRedeliveryPier() {
		return redeliveryPier;
	}
	public void setRedeliveryPier(String redeliveryPier) {
		this.redeliveryPier = redeliveryPier;
	}	
	@Column
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	@Column
	public Date getVgmCutoff() {
		return vgmCutoff;
	}
	public void setVgmCutoff(Date vgmCutoff) {
		this.vgmCutoff = vgmCutoff;
	}
	
	
	
	
}
