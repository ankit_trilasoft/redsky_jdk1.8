package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CompanySystemDefault;

public interface CompanySystemDefaultDao extends GenericDao<CompanySystemDefault, Long>  {
	
	/*public List<CompanySystemDefault> findByCorpID(String sessionCorpID);*/

}
