package com.trilasoft.app.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DataSecurityFilterDao;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.service.DataSecurityFilterService;

public class DataSecurityFilterServiceImpl implements DataSecurityFilterService{ 

	DataSecurityFilterDao dataSecurityFilterDao;
		
	public List findById(Long id) {
		return dataSecurityFilterDao.findById(id);
	}
	public DataSecurityFilterServiceImpl() {
		
	}
	public void setDataSecurityFilterDao(DataSecurityFilterDao dataSecurityFilterDao) {
		this.dataSecurityFilterDao = dataSecurityFilterDao;
	}

	public List getAll() {
		
		return dataSecurityFilterDao.getAll();
	}
	public List getFilterData(Long userId, String sessionCorpID) {
		return dataSecurityFilterDao.getFilterData(userId, sessionCorpID);
	}
	
	public DataSecurityFilter save(DataSecurityFilter dataSecurityFilter) {
		return dataSecurityFilterDao.save(dataSecurityFilter);
	}
	public List getUsingId(Long id) {
		return dataSecurityFilterDao.getUsingId(id);
	}
	public String getParentCorpId(String sessionCorpID) {
		return dataSecurityFilterDao.getParentCorpId(sessionCorpID);
	}
	public List PartnerAccessCorpIdList() {
		return dataSecurityFilterDao.PartnerAccessCorpIdList();
	}
	
	public List getChildAgentCode(String filterValues, String sessionCorpID,String parentCorpID){
		return dataSecurityFilterDao.getChildAgentCode(filterValues,sessionCorpID,parentCorpID);
	}
	public Map getChildAgentCodeMap() { 
		return dataSecurityFilterDao.getChildAgentCodeMap();
	}

	

}
