<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<!-- load the dojo toolkit base -->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"
    djConfig="parseOnLoad:true, isDebug:false"></script>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/redskydojo.js'/>"></script>


<style type="text/css">
  @import "<c:url value='/scripts/dojo/dijit/themes/nihilo/nihilo.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/Grid.css'/>";
  @import "<c:url value='/scripts/dojo/dojox/grid/_grid/nihiloGrid.css'/>";
  @import "<c:url value='/styles/${appConfig["csstheme"]}/dojo-layout.css'/>";

 


</style>

 <script type="text/javascript">
  dojo.require("dojox.grid.Grid");
  dojo.require("dojox.grid._data.model");
  dojo.require("dojox.grid._data.dijitEditors");
  dojo.require("dojo.parser");
  dojo.require("dojox.grid._grid.publicEvents");

  function getColumnListAry(){
	  var columnListAsStr = '';
	  <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
	  	if (columnListAsStr != '') columnListAsStr += ',';
	  	columnListAsStr += '<c:out value="${fieldName}"/>';	
	  </c:forEach> 	
	  return columnListAsStr.split(',');	
  }
  	  
  function getColumnName(inputColumnIndex){
	  return getColumnListAry()[inputColumnIndex];
  }

  function getColumnNameFromObj(inputObj){
	  return getColumnName(getParentTd(inputObj).getAttribute('idx'));
  }

  function getColumnValue(columnName){
	    var selectedRow = grid2.selection.getSelected()[0];
	    var rowData = grid2.model.data[selectedRow];
	    return rowData[getColumnIndex(columnName)];
  }

  function setColumnValue(columnName, newValue){
	    var selectedRow = grid2.selection.getSelected()[0];
	    var rowData = grid2.model.data[selectedRow];
	    rowData[getColumnIndex(columnName)] = newValue;
  }
  
  function getColumnIndex(inputColumnName){
	  var i=0;
	  var columnListAry = getColumnListAry();
	  for (i=0;i<=columnListAry.length;i++) {
	  	if (columnListAry[i] == inputColumnName) {
	  		return i;
	  	}
	  }
  }

  //coupled to html used for rendering the grid 
  function getParentTd(inputObj){
	  var loopfor = 5;
	  var loopCnt = 0;
	  var parentNodeObj = inputObj.parentNode; 
	  while (true) {
		  if (parentNodeObj == null || parentNodeObj.tagName == 'TD' || loopCnt >= loopfor) break;
		  parentNodeObj = parentNodeObj.parentNode;
		  loopCnt++;
		  
  		}
		return parentNodeObj;
  }
  

	
	button = dojo.byId('dojoxGrid-input');
	dojo.connect(button, 'onchange', 'processOnChange');
	
	  
  
</script>

<script>
function elmLoop(){

	var theForm = document.forms[0]

	   for(i=0; i<theForm.elements.length; i++){
	   var alertText = ""
	   alertText += "Element Type: " + theForm.elements[i].type + "\n"

	      if(theForm.elements[i].type == "text" || theForm.elements[i].type == "textarea" || theForm.elements[i].type == "button"){
	      alertText += "Element Value: " + theForm.elements[i].value + "\n"
	      }
	      else if(theForm.elements[i].type == "checkbox"){
	      alertText += "Element Checked? " + theForm.elements[i].checked + "\n"
	      }
	      else if(theForm.elements[i].type == "select-one"){
	      alertText += "Selected Option's Text: " + theForm.elements[i].options[theForm.elements[i].selectedIndex].text + "\n"
	      }
	   alert(alertText)
	   }

	} 

</script>

<%@page import="com.trilasoft.app.webapp.util.GridDataProcessor"%>


<!-- ***********************move code from below once ready******* -->

<script type="text/javascript">

    // ==========================================================================
    // Create a data model
    // ==========================================================================
    <%
	String htmlCode = ((String[][])request.getAttribute("tableMetadata"))[0][2];				
	String pattern = "listRow\\[[0-9a-zA-Z_]+\\]"; 
    %>
    data = [
			<c:forEach var="listRow" items="${request[tableMetadata[0][0]]}" varStatus="status">
			[
			<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
				<c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
					"<%=GridDataProcessor.processHtml(htmlCode, pageContext.getAttribute("listRow"), pattern)%>"
				 </c:if>			
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int' || tableMetadata[2][fieldNameIteratorStatus.index] == 'long' || tableMetadata[2][fieldNameIteratorStatus.index] == 'float'|| tableMetadata[2][fieldNameIteratorStatus.index] == 'double'|| tableMetadata[2][fieldNameIteratorStatus.index] == 'BigDecimal')}">

	 				<c:out value="${listRow[fieldName]}"/>
	 			</c:if>
				<c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'text'}">
				 	'<c:out value="${listRow[fieldName]}"/>'		
				 </c:if>
				  //   For Accountline.......//
				 <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'string'}">
				 	'<c:out value="${listRow[fieldName]}"/>'		
				 </c:if>
				//// 
				 
				 <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
			</c:forEach> 
			] <c:if test="${!status.last}">,</c:if>			
			</c:forEach>
		];
		 material=[<c:forEach var="listRow" items="${material}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ];
      basis=[<c:forEach var="listRow" items="${bilingbasis}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ];
      checkNew=[<c:forEach var="listRow" items="${billingcheckNew}" varStatus="status">
           <c:set var="list" value="${listRow}"/>
	     '<c:out value="${listRow}"/>',
     </c:forEach>
      ]; 
      
      
      job=[<c:forEach var="listRow" items="${job}" varStatus="status">
           <c:set var="list" value="${listRow}"/>'<c:out value="${fn:substring(list,0,fn:indexOf(list, '='))}"/>',
     </c:forEach>
      ];
        model = new dojox.grid.data.Table(null, data);
        
        
      var formatQuantity1 = 0;
      var formatRate1 = 0;
      var formatAmount1 = 0;
      var formatGrossQuantity1 =0;
      var formatReturnQuantity1 = 0;
       
    formatRate =   function calcRate(inDatum){
    	if(isNaN(inDatum)){
			inDatum = 0;
		}
    	formatRate1 = inDatum;
    	return  (inDatum);
    }
      
    formatGrossQuantity =   function calcGross(inDatum){
    	if(isNaN(inDatum)){
			inDatum = 0;
		} 
    	formatGrossQuantity1 = inDatum;
		return (inDatum);
	}
	
	formatReturnQuantity =   function calcReturn(inDatum){
		if(isNaN(inDatum)){
			inDatum = 0;
		}
    	formatReturnQuantity1 = inDatum;
    	return  (inDatum);
	}
   
   	formatQuantity =   function calcQuan(inDatum){
   		if(isNaN(inDatum)){
			inDatum = 0;
		}
   		if(inDatum==null){
			inDatum = 0;
		}
		if(inDatum == 0 || inDatum == ''){
			if((formatGrossQuantity1 !='Null' || formatReturnQuantity1 !='Null')){
	        	formatQuantity1 =((Math.round(formatGrossQuantity1*100)/100).toFixed(0)-(Math.round(formatReturnQuantity1*100)/100).toFixed(0))
	    		////formatQuantity1 = (formatGrossQuantity1-formatReturnQuantity1);
	    		return  (formatQuantity1);
	    		/// } else {
	    		/// return (inDatum);
     		}
		}else{
			formatQuantity1 = inDatum;
     		return (formatQuantity1);
     	}
	}
	
  	formatAmount =   function calcAmount(inDatum){
  		if(isNaN(inDatum)){
			inDatum = 0;
		}
  		if(inDatum == 0 || inDatum == ''){
    	if(formatRate1 !='Null' || formatQuantity1 !='Null'){
        		formatAmount1=(formatRate1*formatQuantity1);
       	return  ((Math.round(formatAmount1*100)/100).toFixed(2));  
    	}
  	    }else{
  	    	formatAmount1 = inDatum;
 		return (formatAmount1);
 	    }	
    }
      
    
    // ==========================================================================
    // Tie UI to the data model
    // ==========================================================================
    model.observer(this);
    modelChange = function(){
      dojo.byId("rowCount").innerHTML = 'Row count: ' + model.count;
    }
    
    // ==========================================================================
    // Custom formatters 
    // ==========================================================================
    formatCurrency = function(inDatum){
      return isNaN(inDatum) ? '...' : dojo.currency.format(inDatum, this.constraint);
    }
    formatNumber = function(inDatum){
      return isNaN(inDatum) ? '' : inDatum;
    }    
    formatDate = function(inDatum){
      return dojo.date.locale.format(new Date(inDatum), this.constraint);
    }
    
    // ==========================================================================
    // Grid structure
    // ==========================================================================
    
    
    
    
    
    
    
    gridLayout = [{
     type: 'dojox.GridRowView', width: '0px'
   },{
     defaultCell: { width: 4, styles: 'text-align: right;'  },
     rows: [[
           <c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatNumber, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'long') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'id'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatNumber, width: 5}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'descript'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: material, width: 20}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') &&
                   tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'descript'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: material, width: 20}
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'string') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>',  width: '70px'}
				</c:if>
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'basis'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: basis, width: '200px'}
				</c:if>
				<c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'text') && 	tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'checkNew'}"> 
							{ name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.ComboBox, options: checkNew, width: '200px'}
				</c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'cost'}">
                         { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999 }, formatter: formatRate, width: 5}
                     /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit,editorClass: dijit.form.NumberTextBox, editorProps: {min:5.00, max:999.99, places:2}, formatter: formatRate, width: 5}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'cost'}">
                  		 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999 }, formatter: formatRate, width: 5}
              		////  { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit,editorClass: dijit.form.NumberTextBox, editorProps: {min:5.00, max:999.99, places:2}, formatter: formatRate, width: 5}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'qty'}">
                  /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
                     { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10 }
              
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'qty'}">
                  /// { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: left;',editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: { places: 0 }, formatter: formatGrossQuantity, width: 10, maxlength: 12, id="qty" }
               	 { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatGrossQuantity, width: 10 }
               </c:if>
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'returned'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatReturnQuantity, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'returned'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Dijit, editorClass: "dijit.form.NumberTextBox", constraint: {min:0,max:999999, places: 0 }, formatter: formatReturnQuantity, width: 10}
               </c:if>
                <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actualQty'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatQuantity, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'int') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actualQty'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatQuantity,  width: 10}
               </c:if>
               
               <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'readonly' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actual'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;',editor: dojox.grid.editors.Input, formatter: formatAmount, width: 10}
                 </c:if>
                 <c:if test="${(tableMetadata[2][fieldNameIteratorStatus.index] == 'float') && tableMetadata[3][fieldNameIteratorStatus.index] == 'editable' && tableMetadata[1][fieldNameIteratorStatus.index] == 'actual'}">
                   { name: '<fmt:message key="${tableMetadata[0][1]}.${fieldName}"/>', styles: 'text-align: right;', editor: dojox.grid.editors.Input, formatter: formatAmount, width: 10}
               </c:if>
                <c:if test="${tableMetadata[2][fieldNameIteratorStatus.index] == 'html'}">
                           { name: '<fmt:message key="${fieldName}"/>'}
                </c:if> 
               <c:if test="${!fieldNameIteratorStatus.last}">,</c:if>
                
      </c:forEach>
     ]]
   }];
    
    
    
    
    
    
    
    
    
    

    // ==========================================================================
    // UI Action
    // ==========================================================================
    addRow = function(){
       grid2.addRow([
          0, 0, 0, 0, 0, 0, 0
         ]);
       }
 
 

    
    saveListData = function(){
    	grid2.edit.apply();
    	var fields = "";
    	var fieldTypes = "";
    	var editability = "";
		<c:forEach var="fieldName" items="${tableMetadata[1]}" varStatus="fieldNameIteratorStatus">
			if (fields != "") fields += ',';
			fields += '<c:out value="${fieldName}"/>';
			if (editability != "") editability += ',';
			editability += '<c:out value="${tableMetadata[3][fieldNameIteratorStatus.index]}"/>';
			if (fieldTypes != "") fieldTypes += ',';
			fieldTypes += '<c:out value="${tableMetadata[2][fieldNameIteratorStatus.index]}"/>';
			
		</c:forEach> 
      document.forms['gridForm'].elements['listData'].value = dojo.toJson(grid2.model.data);
      document.forms['gridForm'].elements['listFieldNames'].value = fields;
      document.forms['gridForm'].elements['listFieldTypes'].value = fieldTypes;      
      document.forms['gridForm'].elements['listFieldEditability'].value = editability;
      document.forms['gridForm'].submit();
    } 
</script>
  
	<div id="otabs">
         <ul>
          <c:if test="${itemType=='M'}"> <li><a class="current"><span>Material List</span></a></li></c:if>
          <c:if test="${itemType=='E'}"> <li><a class="current"><span>Equipment List</span></a></li></c:if>
         </ul>
       </div>
       <div class="spnblk">&nbsp;</div>
   
    
 <c:choose>
  <c:when test='${itemType == "M"|| itemType=="E"}'>
 <table style="width:100%; border:1px solid #c1c1c1; border-top:none;padding-top:0px;" cellpading="0" cellspacing="0"">
 <tr>
<td>	      
<div id="grid2" jsId="grid2" dojoType="dojox.Grid" model="model" structure="gridLayout"
        style="width:100%;height:350px;border-width:1px 1px 1px 1px; ">
 </div>
 </td>
 </tr>
 </table></c:when>
 <c:otherwise>
 <table style="width:100%; border:1px solid #c1c1c1; border-top:none;padding-top:0px;" cellpading="0" cellspacing="0"">
 <tr>
<td>	      
<div id="grid2" jsId="grid2" dojoType="dojox.Grid" model="model" structure="gridLayout"
        style="width:100%;height:250px;border-width:1px 1px 1px 1px;">
 </div>
 </td>
 </tr>
 </table>
    </c:otherwise></c:choose>

<div id="rowCount" style="font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;margin-bottom:4px; "></div>
<table cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;width:100%;">
<tr>
<td align="left" width="649px"  style=" font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;  " class="">Total</td>
<td align="right" width="241px" class="" style="font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px; "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${actualcost}" /></div></td>

<td align="left" width=""  style=" font-size:12px;font-family:arial;font-weight: bold;background-color: #d3d2d2;padding:5px;  " class=""></td>
</tr>
</table>
<div id="controls">
<table cellpadding="4" cellspacing="2" style="margin:0px;padding:0px;"><tr><td>
 <s:submit cssClass="cssbutton1" key="button.save" method="delete" onclick="saveListData()" /></td>
 <td>
 <div id="adddiv">
 <s:submit cssClass="cssbutton1" key="button.add" method="delete" onclick="addRow()" />
 </div></td>
 <td>
 <s:submit cssClass="cssbutton1" key="button.reset" method="delete" onclick="grid2.refresh()" />
 </td></tr></table>
</div>


<br/>
<script type="text/javascript">
if(document.forms['gridForm'].elements['showAdd'].value=='Yes')
{
document.getElementById("adddiv").style.display="block";
}
 if(document.forms['gridForm'].elements['showAdd'].value=='No')
{
document.getElementById("adddiv").style.display="none";
}
</script>



