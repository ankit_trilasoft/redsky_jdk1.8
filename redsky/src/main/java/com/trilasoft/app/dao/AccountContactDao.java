package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AccountContact;

public interface AccountContactDao extends GenericDao<AccountContact, Long> {

		public List getAccountContacts(String partnerCode,String corpID);
		public List getNotesForContract(String partnerCode,String corpID);
		public List getRelatedNotesForContract(String corpID, String partnerCode);
		public List getRelatedNotesForProfile(String corpID, String partnerCode);
}
