/**
 * This class represents the basic "Billing" object in Redsky that allows for Billing of Shipment.
 * @Class Name	Billing
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="billing")

@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class Billing extends BaseObject implements ListLinkData{
	private String corpID;
	private String auditor;
	private Long id;
	private String creditTerms;
	private String sequenceNumber;
	private String shipNumber;
	private String authorityType;
	private String billCompleteA;
    private Date billComplete;
    private String billingId;
    private String billingIdOut;
    private String billingInstruction;
    private String billingInstructionCode;
    private Date billThrough;
    private String billTo1Point;
    private String billTo2Authority;
    private String billTo2Code;
    private String billTo2Name;
    private String billTo2Point;
    private String billTo2Reference;
    private String billToAuthority;
    private String authorizationNo2;
    private String billToCode;
    private String billToName;
    private String billToReference;
    private String certnum;
    private String charge;
    private String insuranceCharge;
    private String storageBillingParty;
    private String contract;
    private int cycle;
    private BigDecimal deposit;
    private BigDecimal discount;
    private BigDecimal insurancePerMonth;
    private String insuranceOptionCode;
    private BigDecimal insuranceValueActual = new BigDecimal(0.00);
    private BigDecimal insuranceValueEntitle;
    private String insuranceHas;
    private String insuranceOption;
    private String insurancePolicy;
    private BigDecimal insuranceRate;
    private String insuranceThrough;
    private String invoiceRemarks1;
    private String invoiceRemarks2;
    private String invoiceRemarks3;
    private String invoiceRemarks4;
    private String personPricing;
	private String personBilling;
	private String personPayable;
	private String personReceivable;
    private String matchShip;
    private Date noMoreWork;
    private String OrderForService;
    private BigDecimal onHand;
    private BigDecimal otherDiscount;
    private BigDecimal packDiscount;
    private boolean invoiceByEmail;
    private BigDecimal postGrate;
    private String securityAccount;
    private String serial;
    private BigDecimal sharedDiscount;
    private BigDecimal sitDiscount;
    private String storageMeasurement;
    private Date storageOut;
    private BigDecimal storagePerMonth;
    private BigDecimal transportationDiscount;
    private String warehouse;
	private Date taExpires;
	private String scheduleCode;
	private String specialInstruction;
	private Date systemDate;
    private String approvedBy;
    private boolean noCharge;
    private Date dateApproved;
    private String insuranceOptionCodeWithDesc;
    private String billingInstructionCodeWithDesc;
    private String imfJob;
    private String isCreditCard;
    private Date gstVATrefund;
    private String privatePartyBillingCode;
    private String privatePartyBillingName;
    private Boolean signedDisclaimer;
    private String vendorCode;
    private String vendorName;
    private String totalInsrVal;
    private String currency;
	private Date revenueRecognition;
	private Date issueDate;
	private String actgCode;
	private String payMethod;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date auditComplete;
	private Date taxableOn;
	private String insuranceValueEntitleCurrency;
	private String vendorCode1;
	private String vendorName1;
	private String insuranceCharge1;
	//Gst Authorization functionality
	
	
	private Date gstThirdPartyOrigin;
	private Date gstThirdPartyRequestOrigin;
	private Date gstThirdPartyAuthReceivedOrigin;
	private String gstThirdPartyAuthNoOrigin;
	
	private Date gstShuttleOriginService;
	private Date gstShuttleOriginRequest;
	private Date gstShuttleOriginAuthReceived;
	private String gstShuttleOriginAuthNo;
	
	private Date gstShuttleDestService;
	private Date gstShuttleDestRequest;
	private Date gstShuttleDestAuthReceived;
	private String gstShuttleDestAuthNo;
	
	private Date gstAccess1Service;
	private Date gstAccess1Request;
	private Date gstAccess1AuthReceived;
	private String gstAccess1AuthNo;
	
	private Date gstAccess2Service;
	private Date gstAccess2Request;
	private Date gstAccess2AuthReceived;
	private String gstAccess2AuthNo;
	
	private Date gstAccess3Service;
	private Date gstAccess3Request;
	private Date gstAccess3AuthReceived;
	private String gstAccess3AuthNo;
	
	private Date gstExtraStopService;
	private Date gstExtraStopRequest;
	private Date gstExtraStopAuthReceived;
	private String gstExtraStopAuthNo;
	
	
	private Date gstThirdPartyDestination;
	private Date gstThirdPartyRequestDestination;
	private Date gstThirdPartyAuthReceivedDestination;
	private String gstThirdPartyAuthNoDestination;
	
	private Date gstPartialDelivery;
	private Date gstPartialDeliveryRequest;
	private Date gstPartialDeliveryAuthReceived;
	private String gstPartialDeliveryAuthNo;
	private String attnBilling;
	//Billing -- add Auth Updated functionality
	private Date authUpdated;
	private Boolean isAutomatedStorageBillingProcessing;
	private Boolean historicalContracts;	
	private String storageVatPercent;
    private String storageVatDescr;
    private String insuranceVatPercent;
    private String insuranceVatDescr;
    private String	billingFrequency ;
	private Set<CreditCard> creditCards;
	private boolean cod;
	private BigDecimal codAmount;
//	 Mapping Variables
	private ServiceOrder serviceOrder; //One To One RelationShip With ServiceOrder
	private CustomerFile customerFile;
	private String billingEmail;
	
// new enhancement by Voerman
	private BigDecimal baseInsuranceValue = new BigDecimal(0.00);
	private BigDecimal insuranceBuyRate;
	private String baseInsuranceTotal;
	private BigDecimal exchangeRate= new BigDecimal(1);
	private Date valueDate;
	private BigDecimal estMoveCost = new BigDecimal(0.00);
	private String deductible;
	private String additionalNo;
	private String billingCurrency;
	private String vendorStoragePerMonth;
	private String vendorStorageVatDescr;
	private BigDecimal vendorStorageVatPercent = new BigDecimal(0.00);
	private String vendorBillingCurency;
	private String ugwIntId;
	private Boolean readyForInvoicing = new Boolean(false);
	private String contractCurrency;
	private String payableContractCurrency;
	//Added For #7150 
	private String claimHandler;
	private String primaryVatCode;
	private String secondaryVatCode;
	private String privatePartyVatCode;
	
	private String originAgentVatCode;
	private String destinationAgentVatCode;
	private String originSubAgentVatCode;
	private String destinationSubAgentVatCode;
	private String forwarderVatCode;
	private String brokerVatCode;
	private String networkPartnerVatCode;
	private String bookingAgentVatCode;
	private String vendorCodeVatCode;
	private String vendorCodeVatCode1;
	private BigDecimal payableRate;
	private String noInsurance;
	private Date insSubmitDate;
	private String storageEmail;
	private Date wareHousereceiptDate;
	private String personForwarder;
	private String billingInsurancePayableCurrency;
	private String billName;
	private String locationAgentCode;
	private String locationAgentName;
	private Date recSignedDisclaimerDate;
	private Boolean fXRateOnActualizationDate;
	private boolean storageVatExclude;
	private boolean insuranceVatExclude;
	private String networkBillToCode;
    private String networkBillToName;
    private Date contractReceivedDate;
    private String internalBillingPerson;
    private String contractSystem;
    private String totalLossCoverageIndicator;
    private String mechanicalMalfunctionIndicator;
    private String pairsOrSetsIndicator;
    private String moldOrMildewIndicator;
    private BigDecimal autoBilledRate = new BigDecimal(0.00);
    private BigDecimal autoInsuredValue = new BigDecimal(0.00);
    private BigDecimal autoInternalRate = new BigDecimal(0.00);
    private Boolean paymentReceived;
    private String storageEmailType;
    private Date FIDIISOAudit;
    private String valuation;
    private Date readyForInvoiceCheck;
	private Date financialsComplete;
	private String billingCycle;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID).append("billingCurrency",billingCurrency).append("vendorStoragePerMonth",vendorStoragePerMonth)
				.append("vendorStorageVatDescr",vendorStorageVatDescr).append("vendorStorageVatPercent",vendorStorageVatPercent)
				.append("vendorBillingCurency",vendorBillingCurency)
				.append("auditor", auditor)
				.append("id", id)
				.append("creditTerms", creditTerms)
				.append("contractCurrency",contractCurrency).append("payableContractCurrency",payableContractCurrency)
				.append("sequenceNumber", sequenceNumber)
				.append("shipNumber", shipNumber)
				.append("authorityType", authorityType)
				.append("billCompleteA", billCompleteA)
				.append("billComplete", billComplete)
				.append("billingId", billingId)
				.append("billingIdOut", billingIdOut)
				.append("billingInstruction", billingInstruction)
				.append("billingInstructionCode", billingInstructionCode)
				.append("billThrough", billThrough)
				.append("billTo1Point", billTo1Point)
				.append("billTo2Authority", billTo2Authority)
				.append("billTo2Code", billTo2Code)
				.append("billTo2Name", billTo2Name)
				.append("billTo2Point", billTo2Point)
				.append("billTo2Reference", billTo2Reference)
				.append("billToAuthority", billToAuthority)
				.append("authorizationNo2", authorizationNo2)
				.append("billToCode", billToCode)
				.append("billToName", billToName)
				.append("billToReference", billToReference)
				.append("certnum", certnum)
				.append("charge", charge)
				.append("insuranceCharge", insuranceCharge)
				.append("storageBillingParty", storageBillingParty)
				.append("contract", contract)
				.append("cycle", cycle)
				.append("deposit", deposit)
				.append("discount", discount)
				.append("insurancePerMonth", insurancePerMonth)
				.append("insuranceOptionCode", insuranceOptionCode)
				.append("insuranceValueActual", insuranceValueActual)
				.append("insuranceValueEntitle", insuranceValueEntitle)
				.append("insuranceHas", insuranceHas)
				.append("insuranceOption", insuranceOption)
				.append("insurancePolicy", insurancePolicy)
				.append("insuranceRate", insuranceRate)
				.append("insuranceThrough", insuranceThrough)
				.append("invoiceRemarks1", invoiceRemarks1)
				.append("invoiceRemarks2", invoiceRemarks2)
				.append("invoiceRemarks3", invoiceRemarks3)
				.append("invoiceRemarks4", invoiceRemarks4)
				.append("personPricing", personPricing)
				.append("personBilling", personBilling)
				.append("personPayable", personPayable)
				.append("personReceivable", personReceivable)
				.append("matchShip", matchShip)
				.append("noMoreWork", noMoreWork)
				.append("OrderForService", OrderForService)
				.append("onHand", onHand)
				.append("otherDiscount", otherDiscount)
				.append("packDiscount", packDiscount)
				.append("invoiceByEmail", invoiceByEmail)
				.append("postGrate", postGrate)
				.append("securityAccount", securityAccount)
				.append("serial", serial)
				.append("sharedDiscount", sharedDiscount)
				.append("sitDiscount", sitDiscount)
				.append("storageMeasurement", storageMeasurement)
				.append("storageOut", storageOut)
				.append("storagePerMonth", storagePerMonth)
				.append("transportationDiscount", transportationDiscount)
				.append("warehouse", warehouse)
				.append("taExpires", taExpires)
				.append("scheduleCode", scheduleCode)
				.append("specialInstruction", specialInstruction)
				.append("systemDate", systemDate)
				.append("approvedBy", approvedBy)
				.append("noCharge", noCharge)
				.append("dateApproved", dateApproved)
				.append("insuranceOptionCodeWithDesc",
						insuranceOptionCodeWithDesc)
				.append("billingInstructionCodeWithDesc",
						billingInstructionCodeWithDesc)
				.append("imfJob", imfJob)
				.append("isCreditCard", isCreditCard)
				.append("gstVATrefund", gstVATrefund)
				.append("privatePartyBillingCode", privatePartyBillingCode)
				.append("privatePartyBillingName", privatePartyBillingName)
				.append("signedDisclaimer", signedDisclaimer)
				.append("vendorCode", vendorCode)
				.append("vendorName", vendorName)
				.append("totalInsrVal", totalInsrVal)
				.append("currency", currency)
				.append("revenueRecognition", revenueRecognition)
				.append("issueDate", issueDate)
				.append("actgCode", actgCode)
				.append("payMethod", payMethod)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("auditComplete", auditComplete)
				.append("taxableOn", taxableOn)
				.append("insuranceValueEntitleCurrency",
						insuranceValueEntitleCurrency)
				.append("vendorCode1", vendorCode1)
				.append("vendorName1", vendorName1)
				.append("insuranceCharge1", insuranceCharge1)
				.append("gstThirdPartyOrigin", gstThirdPartyOrigin)
				.append("gstThirdPartyRequestOrigin",
						gstThirdPartyRequestOrigin)
				.append("gstThirdPartyAuthReceivedOrigin",
						gstThirdPartyAuthReceivedOrigin)
				.append("gstThirdPartyAuthNoOrigin", gstThirdPartyAuthNoOrigin)
				.append("gstShuttleOriginService", gstShuttleOriginService)
				.append("gstShuttleOriginRequest", gstShuttleOriginRequest)
				.append("gstShuttleOriginAuthReceived",
						gstShuttleOriginAuthReceived)
				.append("gstShuttleOriginAuthNo", gstShuttleOriginAuthNo)
				.append("gstShuttleDestService", gstShuttleDestService)
				.append("gstShuttleDestRequest", gstShuttleDestRequest)
				.append("gstShuttleDestAuthReceived",
						gstShuttleDestAuthReceived)
				.append("gstShuttleDestAuthNo", gstShuttleDestAuthNo)
				.append("gstAccess1Service", gstAccess1Service)
				.append("gstAccess1Request", gstAccess1Request)
				.append("gstAccess1AuthReceived", gstAccess1AuthReceived)
				.append("gstAccess1AuthNo", gstAccess1AuthNo)
				.append("gstAccess2Service", gstAccess2Service)
				.append("gstAccess2Request", gstAccess2Request)
				.append("gstAccess2AuthReceived", gstAccess2AuthReceived)
				.append("gstAccess2AuthNo", gstAccess2AuthNo)
				.append("gstAccess3Service", gstAccess3Service)
				.append("gstAccess3Request", gstAccess3Request)
				.append("gstAccess3AuthReceived", gstAccess3AuthReceived)
				.append("gstAccess3AuthNo", gstAccess3AuthNo)
				.append("gstExtraStopService", gstExtraStopService)
				.append("gstExtraStopRequest", gstExtraStopRequest)
				.append("gstExtraStopAuthReceived", gstExtraStopAuthReceived)
				.append("gstExtraStopAuthNo", gstExtraStopAuthNo)
				.append("gstThirdPartyDestination", gstThirdPartyDestination)
				.append("gstThirdPartyRequestDestination",
						gstThirdPartyRequestDestination)
				.append("gstThirdPartyAuthReceivedDestination",
						gstThirdPartyAuthReceivedDestination)
				.append("gstThirdPartyAuthNoDestination",
						gstThirdPartyAuthNoDestination)
				.append("gstPartialDelivery", gstPartialDelivery)
				.append("gstPartialDeliveryRequest", gstPartialDeliveryRequest)
				.append("gstPartialDeliveryAuthReceived",
						gstPartialDeliveryAuthReceived)
				.append("gstPartialDeliveryAuthNo", gstPartialDeliveryAuthNo)
				.append("attnBilling", attnBilling)
				.append("authUpdated", authUpdated)
				.append("isAutomatedStorageBillingProcessing",
						isAutomatedStorageBillingProcessing)
				.append("historicalContracts", historicalContracts)
				.append("storageVatPercent", storageVatPercent)
				.append("storageVatDescr", storageVatDescr)
				.append("insuranceVatPercent", insuranceVatPercent)
				.append("insuranceVatDescr", insuranceVatDescr)
				.append("billingFrequency", billingFrequency)
				.append("cod", cod)
				.append("codAmount", codAmount) 
				.append("billingEmail", billingEmail)
				.append("baseInsuranceValue", baseInsuranceValue)
				.append("insuranceBuyRate", insuranceBuyRate)
				.append("baseInsuranceTotal", baseInsuranceTotal)
				.append("exchangeRate", exchangeRate)
				.append("valueDate", valueDate)
				.append("estMoveCost", estMoveCost)
				.append("deductible", deductible)
				.append("additionalNo", additionalNo)
				.append("billingCurrency", billingCurrency)
				.append("vendorStoragePerMonth", vendorStoragePerMonth)
				.append("vendorStorageVatDescr", vendorStorageVatDescr)
				.append("vendorStorageVatPercent", vendorStorageVatPercent)
				.append("vendorBillingCurency", vendorBillingCurency)
				.append("ugwIntId", ugwIntId)
				.append("vendorCode1", vendorCode1)
				.append("vendorName1", vendorName1)
				.append("insuranceCharge1", insuranceCharge1)
				.append("deductible",deductible)
				.append("additionalNo",additionalNo)
				.append("readyForInvoicing",readyForInvoicing)
				.append("primaryVatCode",primaryVatCode)
				.append("secondaryVatCode",secondaryVatCode)
				.append("privatePartyVatCode",privatePartyVatCode)
				.append("originAgentVatCode",originAgentVatCode) 
				.append("destinationAgentVatCode",destinationAgentVatCode)
				.append("originSubAgentVatCode",originSubAgentVatCode)
				.append("destinationSubAgentVatCode",destinationSubAgentVatCode)
				.append("forwarderVatCode",forwarderVatCode)
				.append("brokerVatCode",brokerVatCode)
				.append("networkPartnerVatCode",networkPartnerVatCode)
				.append("bookingAgentVatCode",bookingAgentVatCode)
				.append("vendorCodeVatCode",vendorCodeVatCode)
				.append("payableRate",payableRate)
				.append("vendorCodeVatCode1",vendorCodeVatCode1)
				.append("noInsurance",noInsurance)
				.append("insSubmitDate",insSubmitDate)				
				.append("storageEmail",storageEmail)
				.append("wareHousereceiptDate",wareHousereceiptDate)
				.append("personForwarder",personForwarder)
				.append("billingInsurancePayableCurrency",billingInsurancePayableCurrency)
				.append("billName",billName)
				.append("locationAgentCode",locationAgentCode).append("locationAgentName",locationAgentName)
				.append("recSignedDisclaimerDate",recSignedDisclaimerDate)
				.append("storageVatExclude",storageVatExclude)
				.append("insuranceVatExclude",insuranceVatExclude)
				.append("fXRateOnActualizationDate",fXRateOnActualizationDate)
				.append("networkBillToCode",networkBillToCode)
				.append("networkBillToName",networkBillToName)
				.append("contractReceivedDate",contractReceivedDate)
				.append("internalBillingPerson",internalBillingPerson).append("contractSystem", contractSystem)
				.append("totalLossCoverageIndicator", totalLossCoverageIndicator).append("mechanicalMalfunctionIndicator", mechanicalMalfunctionIndicator)
				.append("pairsOrSetsIndicator", pairsOrSetsIndicator).append("moldOrMildewIndicator", moldOrMildewIndicator)
				.append("autoBilledRate", autoBilledRate).append("autoInsuredValue",autoInsuredValue)
				.append("autoInternalRate", autoInternalRate)
				.append("paymentReceived", paymentReceived)
				.append("storageEmailType", storageEmailType)
				.append("FIDIISOAudit", FIDIISOAudit)
				.append("valuation", valuation) 
				.append("readyForInvoiceCheck", readyForInvoiceCheck) 
				.append("financialsComplete", financialsComplete) 
				.append("billingCycle", billingCycle) 
				.toString();
		        
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Billing))
			return false;
		Billing castOther = (Billing) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(contractCurrency, castOther.contractCurrency)
				.append(payableContractCurrency, castOther.payableContractCurrency)
				.append(billingCurrency,castOther.billingCurrency).append(vendorStoragePerMonth,castOther.vendorStoragePerMonth)
				.append(vendorStorageVatDescr,castOther.vendorStorageVatDescr).append(vendorStorageVatPercent,castOther.vendorStorageVatPercent)
				.append(vendorBillingCurency,castOther.vendorBillingCurency)
				.append(auditor, castOther.auditor)
				.append(id, castOther.id)
				.append(creditTerms, castOther.creditTerms)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber)
				.append(authorityType, castOther.authorityType)
				.append(billCompleteA, castOther.billCompleteA)
				.append(billComplete, castOther.billComplete)
				.append(billingId, castOther.billingId)
				.append(billingIdOut, castOther.billingIdOut)
				.append(billingInstruction, castOther.billingInstruction)
				.append(billingInstructionCode,
						castOther.billingInstructionCode)
				.append(billThrough, castOther.billThrough)
				.append(billTo1Point, castOther.billTo1Point)
				.append(billTo2Authority, castOther.billTo2Authority)
				.append(billTo2Code, castOther.billTo2Code)
				.append(billTo2Name, castOther.billTo2Name)
				.append(billTo2Point, castOther.billTo2Point)
				.append(billTo2Reference, castOther.billTo2Reference)
				.append(billToAuthority, castOther.billToAuthority)
				.append(authorizationNo2, castOther.authorizationNo2)
				.append(billToCode, castOther.billToCode)
				.append(billToName, castOther.billToName)
				.append(billToReference, castOther.billToReference)
				.append(certnum, castOther.certnum)
				.append(charge, castOther.charge)
				.append(insuranceCharge, castOther.insuranceCharge)
				.append(storageBillingParty, castOther.storageBillingParty)
				.append(contract, castOther.contract)
				.append(cycle, castOther.cycle)
				.append(deposit, castOther.deposit)
				.append(discount, castOther.discount)
				.append(insurancePerMonth, castOther.insurancePerMonth)
				.append(insuranceOptionCode, castOther.insuranceOptionCode)
				.append(insuranceValueActual, castOther.insuranceValueActual)
				.append(insuranceValueEntitle, castOther.insuranceValueEntitle)
				.append(insuranceHas, castOther.insuranceHas)
				.append(insuranceOption, castOther.insuranceOption)
				.append(insurancePolicy, castOther.insurancePolicy)
				.append(insuranceRate, castOther.insuranceRate)
				.append(insuranceThrough, castOther.insuranceThrough)
				.append(invoiceRemarks1, castOther.invoiceRemarks1)
				.append(invoiceRemarks2, castOther.invoiceRemarks2)
				.append(invoiceRemarks3, castOther.invoiceRemarks3)
				.append(invoiceRemarks4, castOther.invoiceRemarks4)
				.append(personPricing, castOther.personPricing)
				.append(personBilling, castOther.personBilling)
				.append(personPayable, castOther.personPayable)
				.append(personReceivable, castOther.personReceivable)
				.append(matchShip, castOther.matchShip)
				.append(noMoreWork, castOther.noMoreWork)
				.append(OrderForService, castOther.OrderForService)
				.append(onHand, castOther.onHand)
				.append(otherDiscount, castOther.otherDiscount)
				.append(packDiscount, castOther.packDiscount)
				.append(invoiceByEmail, castOther.invoiceByEmail)
				.append(postGrate, castOther.postGrate)
				.append(securityAccount, castOther.securityAccount)
				.append(serial, castOther.serial)
				.append(sharedDiscount, castOther.sharedDiscount)
				.append(sitDiscount, castOther.sitDiscount)
				.append(storageMeasurement, castOther.storageMeasurement)
				.append(storageOut, castOther.storageOut)
				.append(storagePerMonth, castOther.storagePerMonth)
				.append(transportationDiscount,
						castOther.transportationDiscount)
				.append(warehouse, castOther.warehouse)
				.append(taExpires, castOther.taExpires)
				.append(scheduleCode, castOther.scheduleCode)
				.append(specialInstruction, castOther.specialInstruction)
				.append(systemDate, castOther.systemDate)
				.append(approvedBy, castOther.approvedBy)
				.append(noCharge, castOther.noCharge)
				.append(dateApproved, castOther.dateApproved)
				.append(insuranceOptionCodeWithDesc,
						castOther.insuranceOptionCodeWithDesc)
				.append(billingInstructionCodeWithDesc,
						castOther.billingInstructionCodeWithDesc)
				.append(imfJob, castOther.imfJob)
				.append(isCreditCard, castOther.isCreditCard)
				.append(gstVATrefund, castOther.gstVATrefund)
				.append(privatePartyBillingCode,
						castOther.privatePartyBillingCode)
				.append(privatePartyBillingName,
						castOther.privatePartyBillingName)
				.append(signedDisclaimer, castOther.signedDisclaimer)
				.append(vendorCode, castOther.vendorCode)
				.append(vendorName, castOther.vendorName)
				.append(totalInsrVal, castOther.totalInsrVal)
				.append(currency, castOther.currency)
				.append(revenueRecognition, castOther.revenueRecognition)
				.append(issueDate, castOther.issueDate)
				.append(actgCode, castOther.actgCode)
				.append(payMethod, castOther.payMethod)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(auditComplete, castOther.auditComplete)
				.append(taxableOn, castOther.taxableOn)
				.append(insuranceValueEntitleCurrency,
						castOther.insuranceValueEntitleCurrency)
				.append(vendorCode1, castOther.vendorCode1)
				.append(vendorName1, castOther.vendorName1)
				.append(insuranceCharge1, castOther.insuranceCharge1)
				.append(gstThirdPartyOrigin, castOther.gstThirdPartyOrigin)
				.append(gstThirdPartyRequestOrigin,
						castOther.gstThirdPartyRequestOrigin)
				.append(gstThirdPartyAuthReceivedOrigin,
						castOther.gstThirdPartyAuthReceivedOrigin)
				.append(gstThirdPartyAuthNoOrigin,
						castOther.gstThirdPartyAuthNoOrigin)
				.append(gstShuttleOriginService,
						castOther.gstShuttleOriginService)
				.append(gstShuttleOriginRequest,
						castOther.gstShuttleOriginRequest)
				.append(gstShuttleOriginAuthReceived,
						castOther.gstShuttleOriginAuthReceived)
				.append(gstShuttleOriginAuthNo,
						castOther.gstShuttleOriginAuthNo)
				.append(gstShuttleDestService, castOther.gstShuttleDestService)
				.append(gstShuttleDestRequest, castOther.gstShuttleDestRequest)
				.append(gstShuttleDestAuthReceived,
						castOther.gstShuttleDestAuthReceived)
				.append(gstShuttleDestAuthNo, castOther.gstShuttleDestAuthNo)
				.append(gstAccess1Service, castOther.gstAccess1Service)
				.append(gstAccess1Request, castOther.gstAccess1Request)
				.append(gstAccess1AuthReceived,
						castOther.gstAccess1AuthReceived)
				.append(gstAccess1AuthNo, castOther.gstAccess1AuthNo)
				.append(gstAccess2Service, castOther.gstAccess2Service)
				.append(gstAccess2Request, castOther.gstAccess2Request)
				.append(gstAccess2AuthReceived,
						castOther.gstAccess2AuthReceived)
				.append(gstAccess2AuthNo, castOther.gstAccess2AuthNo)
				.append(gstAccess3Service, castOther.gstAccess3Service)
				.append(gstAccess3Request, castOther.gstAccess3Request)
				.append(gstAccess3AuthReceived,
						castOther.gstAccess3AuthReceived)
				.append(gstAccess3AuthNo, castOther.gstAccess3AuthNo)
				.append(gstExtraStopService, castOther.gstExtraStopService)
				.append(gstExtraStopRequest, castOther.gstExtraStopRequest)
				.append(gstExtraStopAuthReceived,
						castOther.gstExtraStopAuthReceived)
				.append(gstExtraStopAuthNo, castOther.gstExtraStopAuthNo)
				.append(gstThirdPartyDestination,
						castOther.gstThirdPartyDestination)
				.append(gstThirdPartyRequestDestination,
						castOther.gstThirdPartyRequestDestination)
				.append(gstThirdPartyAuthReceivedDestination,
						castOther.gstThirdPartyAuthReceivedDestination)
				.append(gstThirdPartyAuthNoDestination,
						castOther.gstThirdPartyAuthNoDestination)
				.append(gstPartialDelivery, castOther.gstPartialDelivery)
				.append(gstPartialDeliveryRequest,
						castOther.gstPartialDeliveryRequest)
				.append(gstPartialDeliveryAuthReceived,
						castOther.gstPartialDeliveryAuthReceived)
				.append(gstPartialDeliveryAuthNo,
						castOther.gstPartialDeliveryAuthNo)
				.append(attnBilling, castOther.attnBilling)
				.append(authUpdated, castOther.authUpdated)
				.append(isAutomatedStorageBillingProcessing,
						castOther.isAutomatedStorageBillingProcessing)
				.append(historicalContracts, castOther.historicalContracts)
				.append(storageVatPercent, castOther.storageVatPercent)
				.append(storageVatDescr, castOther.storageVatDescr)
				.append(insuranceVatPercent, castOther.insuranceVatPercent)
				.append(insuranceVatDescr, castOther.insuranceVatDescr)
				.append(billingFrequency, castOther.billingFrequency) 
				.append(cod, castOther.cod)
				.append(codAmount, castOther.codAmount) 
				.append(billingEmail, castOther.billingEmail)
				.append(baseInsuranceValue, castOther.baseInsuranceValue)
				.append(insuranceBuyRate, castOther.insuranceBuyRate)
				.append(baseInsuranceTotal, castOther.baseInsuranceTotal)
				.append(exchangeRate, castOther.exchangeRate)
				.append(valueDate, castOther.valueDate)
				.append(estMoveCost, castOther.estMoveCost)
				.append("vendorCode1", castOther.vendorCode1)
				.append("vendorName1", castOther.vendorName1)
				.append("insuranceCharge1", castOther.insuranceCharge1)
				.append(deductible, castOther.deductible)
				.append(additionalNo, castOther.additionalNo)
				.append(billingCurrency, castOther.billingCurrency)
				.append(vendorStoragePerMonth, castOther.vendorStoragePerMonth)
				.append(vendorStorageVatDescr, castOther.vendorStorageVatDescr)
				.append(vendorStorageVatPercent,castOther.vendorStorageVatPercent)
				.append(vendorBillingCurency, castOther.vendorBillingCurency)
				.append(ugwIntId, castOther.ugwIntId)
				.append("additionalNo", castOther.additionalNo)
				.append("readyForInvoicing", castOther.readyForInvoicing)
				.append("primaryVatCode", castOther.primaryVatCode)
				.append("secondaryVatCode", castOther.secondaryVatCode)
				.append("privatePartyVatCode", castOther.privatePartyVatCode)
				.append("originAgentVatCode",castOther.originAgentVatCode) 
				.append("destinationAgentVatCode",castOther.destinationAgentVatCode)
				.append("originSubAgentVatCode",castOther.originSubAgentVatCode)
				.append("destinationSubAgentVatCode",castOther.destinationSubAgentVatCode)
				.append("forwarderVatCode",castOther.forwarderVatCode)
				.append("brokerVatCode",castOther.brokerVatCode)
				.append("networkPartnerVatCode",castOther.networkPartnerVatCode)
				.append("bookingAgentVatCode",castOther.bookingAgentVatCode)
				.append("vendorCodeVatCode",castOther.vendorCodeVatCode) 
				.append("payableRate",castOther.payableRate)  
				.append("vendorCodeVatCode1",castOther.vendorCodeVatCode1) 
				.append("noInsurance",castOther.noInsurance) 
				.append("insSubmitDate",castOther.insSubmitDate) 
				.append("storageEmail",castOther.storageEmail)
				.append("wareHousereceiptDate",castOther.wareHousereceiptDate) 
				.append("personForwarder",castOther.personForwarder)
				.append("billingInsurancePayableCurrency",castOther.billingInsurancePayableCurrency)
				.append("billName",castOther.billName)
				.append("locationAgentCode",castOther.locationAgentCode).append("locationAgentName",castOther.locationAgentName)
				.append("recSignedDisclaimerDate", castOther.recSignedDisclaimerDate)
				.append("storageVatExclude", castOther.storageVatExclude)
				.append("insuranceVatExclude", castOther.insuranceVatExclude)
				.append("fXRateOnActualizationDate", castOther.fXRateOnActualizationDate)
				.append("networkBillToCode", castOther.networkBillToCode)
				.append("networkBillToName", castOther.networkBillToName)
				.append("contractReceivedDate", castOther.contractReceivedDate)
				.append("internalBillingPerson", castOther.internalBillingPerson)
				.append(contractSystem, castOther.contractSystem)
				.append(totalLossCoverageIndicator, castOther.totalLossCoverageIndicator)
				.append(mechanicalMalfunctionIndicator, castOther.mechanicalMalfunctionIndicator)
				.append(pairsOrSetsIndicator, castOther.pairsOrSetsIndicator)
				.append(moldOrMildewIndicator, castOther.moldOrMildewIndicator)
				.append(autoBilledRate, castOther.autoBilledRate).append(autoInsuredValue,castOther.autoInsuredValue)
				.append(autoInternalRate, castOther.autoInternalRate)
				.append(paymentReceived, castOther.paymentReceived)
				.append(storageEmailType, castOther.storageEmailType)
				.append(FIDIISOAudit, castOther.FIDIISOAudit)
				.append(valuation, castOther.valuation)
				.append(readyForInvoiceCheck, castOther.readyForInvoiceCheck)
				.append(financialsComplete, castOther.	financialsComplete)
				.append(billingCycle, castOther.	billingCycle)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(auditor).append(id)
		.append(payableContractCurrency).append(contractCurrency)
				.append(billingCurrency).append(vendorStoragePerMonth)
				.append(vendorStorageVatDescr).append(vendorStorageVatPercent).append(vendorBillingCurency)		
				.append(creditTerms).append(sequenceNumber).append(shipNumber)
				.append(authorityType).append(billCompleteA)
				.append(billComplete).append(billingId).append(billingIdOut)
				.append(billingInstruction).append(billingInstructionCode)
				.append(billThrough).append(billTo1Point)
				.append(billTo2Authority).append(billTo2Code)
				.append(billTo2Name).append(billTo2Point)
				.append(billTo2Reference).append(billToAuthority)
				.append(authorizationNo2).append(billToCode).append(billToName)
				.append(billToReference).append(certnum).append(charge)
				.append(insuranceCharge).append(storageBillingParty)
				.append(contract).append(cycle).append(deposit)
				.append(discount).append(insurancePerMonth)
				.append(insuranceOptionCode).append(insuranceValueActual)
				.append(insuranceValueEntitle).append(insuranceHas)
				.append(insuranceOption).append(insurancePolicy)
				.append(insuranceRate).append(insuranceThrough)
				.append(invoiceRemarks1).append(invoiceRemarks2)
				.append(invoiceRemarks3).append(invoiceRemarks4)
				.append(personPricing).append(personBilling)
				.append(personPayable).append(personReceivable)
				.append(matchShip).append(noMoreWork).append(OrderForService)
				.append(onHand).append(otherDiscount).append(packDiscount)
				.append(invoiceByEmail).append(postGrate)
				.append(securityAccount).append(serial).append(sharedDiscount)
				.append(sitDiscount).append(storageMeasurement)
				.append(storageOut).append(storagePerMonth)
				.append(transportationDiscount).append(warehouse)
				.append(taExpires).append(scheduleCode)
				.append(specialInstruction).append(systemDate)
				.append(approvedBy).append(noCharge).append(dateApproved)
				.append(insuranceOptionCodeWithDesc)
				.append(billingInstructionCodeWithDesc).append(imfJob)
				.append(isCreditCard).append(gstVATrefund)
				.append(privatePartyBillingCode)
				.append(privatePartyBillingName).append(signedDisclaimer)
				.append(vendorCode).append(vendorName).append(totalInsrVal)
				.append(currency).append(revenueRecognition).append(issueDate)
				.append(actgCode).append(payMethod).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(auditComplete).append(taxableOn)
				.append(insuranceValueEntitleCurrency).append(vendorCode1)
				.append(vendorName1).append(insuranceCharge1)
				.append(gstThirdPartyOrigin).append(gstThirdPartyRequestOrigin)
				.append(gstThirdPartyAuthReceivedOrigin)
				.append(gstThirdPartyAuthNoOrigin)
				.append(gstShuttleOriginService)
				.append(gstShuttleOriginRequest)
				.append(gstShuttleOriginAuthReceived)
				.append(gstShuttleOriginAuthNo).append(gstShuttleDestService)
				.append(gstShuttleDestRequest)
				.append(gstShuttleDestAuthReceived)
				.append(gstShuttleDestAuthNo).append(gstAccess1Service)
				.append(gstAccess1Request).append(gstAccess1AuthReceived)
				.append(gstAccess1AuthNo).append(gstAccess2Service)
				.append(gstAccess2Request).append(gstAccess2AuthReceived)
				.append(gstAccess2AuthNo).append(gstAccess3Service)
				.append(gstAccess3Request).append(gstAccess3AuthReceived)
				.append(gstAccess3AuthNo).append(gstExtraStopService)
				.append(gstExtraStopRequest).append(gstExtraStopAuthReceived)
				.append(gstExtraStopAuthNo).append(gstThirdPartyDestination)
				.append(gstThirdPartyRequestDestination)
				.append(gstThirdPartyAuthReceivedDestination)
				.append(gstThirdPartyAuthNoDestination)
				.append(gstPartialDelivery).append(gstPartialDeliveryRequest)
				.append(gstPartialDeliveryAuthReceived)
				.append(gstPartialDeliveryAuthNo).append(attnBilling)
				.append(authUpdated)
				.append(isAutomatedStorageBillingProcessing)
				.append(historicalContracts).append(storageVatPercent)
				.append(storageVatDescr).append(insuranceVatPercent)
				.append(insuranceVatDescr).append(billingFrequency)
				.append(cod).append(codAmount)
				.append(billingEmail)
				.append(baseInsuranceValue).append(insuranceBuyRate)
				.append(baseInsuranceTotal).append(exchangeRate)
				.append(valueDate).append(estMoveCost).append(deductible)
				.append(additionalNo).append(billingCurrency)
				.append(vendorStoragePerMonth).append(vendorStorageVatDescr)
				.append(vendorStorageVatPercent).append(vendorBillingCurency)
				.append(ugwIntId)
				.append(valueDate).append(estMoveCost)
				.append(vendorCode1).append(vendorName1).append(insuranceCharge1)
				.append(deductible)
				.append(additionalNo).append(readyForInvoicing)
				.append(primaryVatCode).append(secondaryVatCode).append(privatePartyVatCode)
				.append(originAgentVatCode).append(destinationAgentVatCode).append(originSubAgentVatCode)
				.append(destinationSubAgentVatCode).append(forwarderVatCode).append(brokerVatCode)
				.append(networkPartnerVatCode).append(bookingAgentVatCode).append(vendorCodeVatCode)
				.append(payableRate)
				.append(vendorCodeVatCode1)
				.append(noInsurance)
				.append(insSubmitDate)
				.append(storageEmail)
				.append(wareHousereceiptDate)
				.append(personForwarder).append(billingInsurancePayableCurrency).append(billName)
				.append(locationAgentCode).append(locationAgentName)
				.append(storageVatExclude).append(storageVatExclude)
				.append(insuranceVatExclude).append(insuranceVatExclude)
				.append(recSignedDisclaimerDate).append(fXRateOnActualizationDate)
				.append(networkBillToCode).append(networkBillToName)
				.append(contractReceivedDate).append(internalBillingPerson).append(contractSystem)
				.append(totalLossCoverageIndicator).append(mechanicalMalfunctionIndicator)
				.append(pairsOrSetsIndicator).append(moldOrMildewIndicator)
				.append(autoBilledRate).append(autoInsuredValue)
				.append(autoInternalRate).append(paymentReceived)
				.append(storageEmailType)
				.append(FIDIISOAudit)
				.append(valuation) 
				.append(readyForInvoiceCheck) 
					.append(financialsComplete) 
					.append(billingCycle)
				.toHashCode();
	}
	public String getIsCreditCard() {
		return isCreditCard;
	}

	public void setIsCreditCard(String isCreditCard) {
		this.isCreditCard = isCreditCard;
	}

	

	@OneToOne
    @JoinColumn(name="shipNumber",
    referencedColumnName="shipNumber", insertable=false, updatable=false)
    public ServiceOrder getServiceOrder() {
        return serviceOrder;
    }
    
	public void setServiceOrder(ServiceOrder serviceOrder) {
			this.serviceOrder = serviceOrder;
	 } 
	
	@ManyToOne
	@JoinColumn(name = "sequenceNumber", insertable = false, updatable = false, referencedColumnName = "sequenceNumber")
	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	
	/*@OneToMany(mappedBy = "billing", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<AuthorizationNo> getAuthorizationNos() {
		return authorizationNos;
	}

	public void setAuthorizationNos(Set<AuthorizationNo> authorizationNos) {
		this.authorizationNos = authorizationNos;
	}*/
	
	@OneToMany(mappedBy="billing", cascade = CascadeType.ALL, fetch = FetchType.EAGER )


	public Set<CreditCard> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(Set<CreditCard> creditCards) {
		this.creditCards = creditCards;
	}

	@Column
	public Date getTaExpires() {
		return taExpires;
	}

	public void setTaExpires(Date taExpires) {
		this.taExpires = taExpires;
	}
	
	@Column(length=4)
	public String getAuthorityType() {
		return authorityType;
	}
	public void setAuthorityType(String authorityType) {
		this.authorityType = authorityType;
	}
		
	@Column
	public Date getBillComplete() {
		return billComplete;
	}
	public void setBillComplete(Date billComplete) {
		this.billComplete = billComplete;
	}
	
	@Column(length=1)
	public String getBillCompleteA() {
		return billCompleteA;
	}
	public void setBillCompleteA(String billCompleteA) {
		this.billCompleteA = billCompleteA;
	}
	
	@Column(length=25)
	public String getBillingId() {
		return billingId;
	}
	public void setBillingId(String billingId) {
		this.billingId = billingId;
	}
	
	@Column(length=65)
	public String getBillingInstruction() {
		return billingInstruction;
	}
	public void setBillingInstruction(String billingInstruction) {
		this.billingInstruction = billingInstruction;
	}
	
	@Column
	public Date getBillThrough() {
		return billThrough;
	}
	public void setBillThrough(Date billThrough) {
		this.billThrough = billThrough;
	}
	
	@Column(length=4)
	public String getBillTo1Point() {
		return billTo1Point;
	}
	public void setBillTo1Point(String billTo1Point) {
		this.billTo1Point = billTo1Point;
	}
	
	@Column(length=35)
	public String getBillTo2Authority() {
		return billTo2Authority;
	}
	public void setBillTo2Authority(String billTo2Authority) {
		this.billTo2Authority = billTo2Authority;
	}
	
	@Column(length=8)
	public String getBillTo2Code() {
		return billTo2Code;
	}
	public void setBillTo2Code(String billTo2Code) {
		this.billTo2Code = billTo2Code;
	}
	
	@Column(length=255)
	public String getBillTo2Name() {
		return billTo2Name;
	}
	public void setBillTo2Name(String billTo2Name) {
		this.billTo2Name = billTo2Name;
	}
	
	@Column(length=5)
	public String getBillTo2Point() {
		return billTo2Point;
	}
	public void setBillTo2Point(String billTo2Point) {
		this.billTo2Point = billTo2Point;
	}
	
	@Column(length=50)
	public String getBillTo2Reference() {
		return billTo2Reference;
	}
	public void setBillTo2Reference(String billTo2Reference) {
		this.billTo2Reference = billTo2Reference;
	}
	@Column(length=35)
	public String getBillToAuthority() {
		return billToAuthority;
	}
	public void setBillToAuthority(String billToAuthority) {
		this.billToAuthority = billToAuthority;
	}
	
	@Column(length=8)
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	
	@Column(length=255)
	public String getBillToName() {
		return billToName;
	}
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	
	@Column(length=50)
	public String getBillToReference() {
		return billToReference;
	}
	public void setBillToReference(String billToReference) {
		this.billToReference = billToReference;
	}
	
	/*
	@Column
	public Date getCcApproved() {
		return ccApproved;
	}
	public void setCcApproved(Date ccApproved) {
		this.ccApproved = ccApproved;
	}
	
	@Column(length=11)
	public BigDecimal getCcApprovedBillAmount() {
		return ccApprovedBillAmount;
	}
	public void setCcApprovedBillAmount(BigDecimal ccApprovedBillAmount) {
		this.ccApprovedBillAmount = ccApprovedBillAmount;
	}
	
	@Column(length=20)
	public String getCcAuthorizationBillNumber() {
		return ccAuthorizationBillNumber;
	}
	public void setCcAuthorizationBillNumber(String ccAuthorizationBillNumber) {
		this.ccAuthorizationBillNumber = ccAuthorizationBillNumber;
	}
	
	
	@Column
	public Date getCcExpires() {
		return ccExpires;
	}
	public void setCcExpires(Date ccExpires) {
		this.ccExpires = ccExpires;
	}
	
	@Column(length=45)
	public String getCcName() {
		return ccName;
	}
	public void setCcName(String ccName) {
		this.ccName = ccName;
	}
	
	@Column(length=20)
	public String getCcNumber() {
		return ccNumber;
	}
	public void setCcNumber(String ccNumber) {
		this.ccNumber = ccNumber;
	}
	
	@Column(length=10)
	public String getCcType() {
		return ccType;
	}
	public void setCcType(String ccType) {
		this.ccType = ccType;
	}
	
	@Column
	public Date getCcVlTransDate() {
		return ccVlTransDate;
	}
	public void setCcVlTransDate(Date ccVlTransDate) {
		this.ccVlTransDate = ccVlTransDate;
	}
	*/
	
	@Column(length=15)
	public String getCertnum() {
		return certnum;
	}
	public void setCertnum(String certnum) {
		this.certnum = certnum;
	}
	
	@Column(length=25)
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	
	@Column(length=100)
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=5)
	public int getCycle() {
		return cycle;
	}
	public void setCycle(int cycle) {
		this.cycle = cycle;
	}
	@Column(length=15)
	public BigDecimal getDeposit() {
		return deposit;
	}
	public void setDeposit(BigDecimal deposit) {
		this.deposit = deposit;
	}
	
	@Column(length=15)
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=1)
	public String getInsuranceHas() {
		return insuranceHas;
	}
	public void setInsuranceHas(String insuranceHas) {
		this.insuranceHas = insuranceHas;
	}
	
	@Column(length=150)
	public String getInsuranceOption() {
		return insuranceOption;
	}
	public void setInsuranceOption(String insuranceOption) {
		this.insuranceOption = insuranceOption;
	}
	
	@Column(length=7)
	public String getInsuranceOptionCode() {
		return insuranceOptionCode;
	}
	public void setInsuranceOptionCode(String insuranceOptionCode) {
		this.insuranceOptionCode = insuranceOptionCode;
	}
	
	@Column(length=11)
	public BigDecimal getInsurancePerMonth() {
		return insurancePerMonth;
	}
	public void setInsurancePerMonth(BigDecimal insurancePerMonth) {
		this.insurancePerMonth = insurancePerMonth;
	}
	
	@Column(length=20)
	public String getInsurancePolicy() {
		return insurancePolicy;
	}
	public void setInsurancePolicy(String insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}
	
	@Column(length=15)
	public BigDecimal getInsuranceRate() {
		return insuranceRate;
	}
	public void setInsuranceRate(BigDecimal insuranceRate) {
		this.insuranceRate = insuranceRate;
	}
	
	@Column(length=20)
	public String getInsuranceThrough() {
		return insuranceThrough;
	}
	public void setInsuranceThrough(String insuranceThrough) {
		this.insuranceThrough = insuranceThrough;
	}
	
	@Column(length=15)
	public BigDecimal getInsuranceValueActual() {
		return insuranceValueActual;
	}
	public void setInsuranceValueActual(BigDecimal insuranceValueActual) {
		this.insuranceValueActual = insuranceValueActual;
	}
	
	@Column(length=15)
	public BigDecimal getInsuranceValueEntitle() {
		return insuranceValueEntitle;
	}
	public void setInsuranceValueEntitle(BigDecimal insuranceValueEntitle) {
		this.insuranceValueEntitle = insuranceValueEntitle;
	}
	@Column(length=40)
	public String getInvoiceRemarks1() {
		return invoiceRemarks1;
	}
	public void setInvoiceRemarks1(String invoiceRemarks1) {
		this.invoiceRemarks1 = invoiceRemarks1;
	}
	
	@Column(length=40)
	public String getInvoiceRemarks2() {
		return invoiceRemarks2;
	}
	public void setInvoiceRemarks2(String invoiceRemarks2) {
		this.invoiceRemarks2 = invoiceRemarks2;
	}
	
	@Column(length=40)
	public String getInvoiceRemarks3() {
		return invoiceRemarks3;
	}
	public void setInvoiceRemarks3(String invoiceRemarks3) {
		this.invoiceRemarks3 = invoiceRemarks3;
	}
	
	@Column(length=40)
	public String getInvoiceRemarks4() {
		return invoiceRemarks4;
	}
	public void setInvoiceRemarks4(String invoiceRemarks4) {
		this.invoiceRemarks4 = invoiceRemarks4;
	}
	@Column(length=9)
	public String getMatchShip() {
		return matchShip;
	}
	public void setMatchShip(String matchShip) {
		this.matchShip = matchShip;
	}
	@Column
	public Date getNoMoreWork() {
		return noMoreWork;
	}
	public void setNoMoreWork(Date noMoreWork) {
		this.noMoreWork = noMoreWork;
	}
	
	@Column(length=15)
	public BigDecimal getOnHand() {
		return onHand;
	}
	public void setOnHand(BigDecimal onHand) {
		this.onHand = onHand;
	}
	
	@Column(length=2)
	public String getOrderForService() {
		return OrderForService;
	}
	public void setOrderForService(String orderForService) {
		OrderForService = orderForService;
	}
	
	@Column(length=6)
	public BigDecimal getOtherDiscount() {
		return otherDiscount;
	}
	public void setOtherDiscount(BigDecimal otherDiscount) {
		this.otherDiscount = otherDiscount;
	}
	
	@Column(length=6)
	public BigDecimal getPackDiscount() {
		return packDiscount;
	}
	public void setPackDiscount(BigDecimal packDiscount) {
		this.packDiscount = packDiscount;
	}
	
	@Column(length=15)
	public BigDecimal getPostGrate() {
		return postGrate;
	}
	public void setPostGrate(BigDecimal postGrate) {
		this.postGrate = postGrate;
	}
	
	@Column(length=12)
	public String getScheduleCode() {
		return scheduleCode;
	}

	public void setScheduleCode(String scheduleCode) {
		this.scheduleCode = scheduleCode;
	}
	@Column(length=4)
	public String getSecurityAccount() {
		return securityAccount;
	}
	public void setSecurityAccount(String securityAccount) {
		this.securityAccount = securityAccount;
	}
	
	@Column(length=20)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	@Column(length=30)
	public String getSerial() {
		return serial;
	}
	public void setSerial(String serial) {
		this.serial = serial;
	}
	
	@Column(length=9)
	public BigDecimal getSharedDiscount() {
		return sharedDiscount;
	}
	public void setSharedDiscount(BigDecimal sharedDiscount) {
		this.sharedDiscount = sharedDiscount;
	}
	@Column(length=20) 
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column(length=15)
	public BigDecimal getSitDiscount() {
		return sitDiscount;
	}
	public void setSitDiscount(BigDecimal sitDiscount) {
		this.sitDiscount = sitDiscount;
	}
	@Column(length=70)
	public String getStorageMeasurement() {
		return storageMeasurement;
	}
	public void setStorageMeasurement(String storageMeasurement) {
		this.storageMeasurement = storageMeasurement;
	}
	
	@Column
	public Date getStorageOut() {
		return storageOut;
	}
	public void setStorageOut(Date storageOut) {
		this.storageOut = storageOut;
	}
	
	@Column(length=11)
	public BigDecimal getStoragePerMonth() {
		return storagePerMonth;
	}
	public void setStoragePerMonth(BigDecimal storagePerMonth) {
		this.storagePerMonth = storagePerMonth;
	}
	@Column(length=6)
	public BigDecimal getTransportationDiscount() {
		return transportationDiscount;
	}
	public void setTransportationDiscount(BigDecimal transportationDiscount) {
		this.transportationDiscount = transportationDiscount;
	}
	
	@Column(length=20)
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	
	@Column(length=130)
	public String getSpecialInstruction() {
		return specialInstruction;
	}

	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}
	@Column(length=10)
	public String getBillingInstructionCode() {
		return billingInstructionCode;
	}

	public void setBillingInstructionCode(String billingInstructionCode) {
		this.billingInstructionCode = billingInstructionCode;
	}
    //New Field 
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=82)
	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	
	@Column
	public Date getDateApproved() {
		return dateApproved;
	}

	public void setDateApproved(Date dateApproved) {
		this.dateApproved = dateApproved;
	}
	@Column(length=1)
	public boolean isNoCharge() {
		return noCharge;
	}

	public void setNoCharge(boolean noCharge) {
		this.noCharge = noCharge;
	}

	public Date getSystemDate() {
		return systemDate;
	}

	public void setSystemDate(Date systemDate) {
		this.systemDate = systemDate;
	}
	// for drop down
	@Column(length=77)
	public String getBillingInstructionCodeWithDesc() {
		return billingInstructionCodeWithDesc;
	}

	public void setBillingInstructionCodeWithDesc(
			String billingInstructionCodeWithDesc) {
		this.billingInstructionCodeWithDesc = billingInstructionCodeWithDesc;
	}

	@Column(length=150)
	public String getInsuranceOptionCodeWithDesc() {
		return insuranceOptionCodeWithDesc;
	}

	public void setInsuranceOptionCodeWithDesc(String insuranceOptionCodeWithDesc) {
		this.insuranceOptionCodeWithDesc = insuranceOptionCodeWithDesc;
	}
	
	@Column(length=1)
	public String getImfJob() {
		return imfJob;
	}

	public void setImfJob(String imfJob) {
		this.imfJob = imfJob;
	}
	
	@Column(length=15)
	public String getPrivatePartyBillingCode() {
		return privatePartyBillingCode;
	}

	public void setPrivatePartyBillingCode(String privatePartyBillingCode) {
		this.privatePartyBillingCode = privatePartyBillingCode;
	}
	@Column(length=255)
	public String getPrivatePartyBillingName() {
		return privatePartyBillingName;
	}

	public void setPrivatePartyBillingName(String privatePartyBillingName) {
		this.privatePartyBillingName = privatePartyBillingName;
	}
	@Column
	public Boolean getSignedDisclaimer() {
		return signedDisclaimer;
	}

	public void setSignedDisclaimer(Boolean signedDisclaimer) {
		this.signedDisclaimer = signedDisclaimer;
	}

	@Column
	public String getTotalInsrVal() {
		return totalInsrVal;
	}

	public void setTotalInsrVal(String totalInsrVal) {
		this.totalInsrVal = totalInsrVal;
	}
	@Column(length = 8)
	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	@Column
	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column(length = 15)
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
	@Column
	public Date getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}
	
	//@@@@@@@@@@@@@@@@@@@@@@@@
	@Transient
	public String getListCode() {		
		return billToCode;
	}

	@Transient
	public String getListDescription() {
		return this.billToName;
	}

	@Transient
	public String getListSecondDescription() {
		return null;
	}

	@Transient
	public String getListThirdDescription() {
		return null;
	}
	
	@Transient
	public String getListFourthDescription() {
		return null;
	}
	
	@Transient
	public String getListFifthDescription() {
		return null;
	}
	
	
	@Transient
	public String getListSixthDescription() {
		return null;
	}

	public void setGstVATrefund(Date gstVATrefund) {
		this.gstVATrefund = gstVATrefund;
	}

	public Date getGstVATrefund() {
		return gstVATrefund;
	}
	@Column(length=25)
	public String getInsuranceCharge() {
		return insuranceCharge;
	}

	public void setInsuranceCharge(String insuranceCharge) {
		this.insuranceCharge = insuranceCharge;
	}
	@Column(length=10)
	public String getStorageBillingParty() {
		return storageBillingParty;
	}

	public void setStorageBillingParty(String storageBillingParty) {
		this.storageBillingParty = storageBillingParty;
	}
	@Transient
	public String getListSeventhDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Column
	public Date getRevenueRecognition() {
		return revenueRecognition;
	}

	public void setRevenueRecognition(Date revenueRecognition) {
		this.revenueRecognition = revenueRecognition;
	}
	@Column( length=20 )
	public String getActgCode() {
		return actgCode;
	}

	public void setActgCode(String actgCode) {
		this.actgCode = actgCode;
	}
	@Column(length=5)
	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	@Column
	public Boolean getIsAutomatedStorageBillingProcessing() {
		return isAutomatedStorageBillingProcessing;
	}

	public void setIsAutomatedStorageBillingProcessing(Boolean isAutomatedStorageBillingProcessing) {
		this.isAutomatedStorageBillingProcessing = isAutomatedStorageBillingProcessing;
	}

	/**
	 * @return the personBilling
	 */
	@Column( length=82 )
	public String getPersonBilling() {
		return personBilling;
	}

	/**
	 * @param personBilling the personBilling to set
	 */
	public void setPersonBilling(String personBilling) {
		this.personBilling = personBilling;
	}

	/**
	 * @return the personPayable
	 */
	@Column( length=82 )
	public String getPersonPayable() {
		return personPayable;
	}

	/**
	 * @param personPayable the personPayable to set
	 */
	public void setPersonPayable(String personPayable) {
		this.personPayable = personPayable;
	}

	/**
	 * @return the personPricing
	 */
	@Column( length=82 )
	public String getPersonPricing() {
		return personPricing;
	}

	/**
	 * @param personPricing the personPricing to set
	 */
	public void setPersonPricing(String personPricing) {
		this.personPricing = personPricing;
	}

	/**
	 * @return the personReceivable
	 */
	@Column( length=82 )
	public String getPersonReceivable() {
		return personReceivable;
	}

	/**
	 * @param personReceivable the personReceivable to set
	 */
	public void setPersonReceivable(String personReceivable) {
		this.personReceivable = personReceivable;
	}

	/**
	 * @return the invoiceByEmail
	 */
	@Column(length=1)
	public boolean isInvoiceByEmail() {
		return invoiceByEmail;
	}

	/**
	 * @param invoiceByEmail the invoiceByEmail to set
	 */
	public void setInvoiceByEmail(boolean invoiceByEmail) {
		this.invoiceByEmail = invoiceByEmail;
	}

	/**
	 * @return the auditComplete
	 */
	@Column
	public Date getAuditComplete() {
		return auditComplete;
	}

	/**
	 * @param auditComplete the auditComplete to set
	 */
	public void setAuditComplete(Date auditComplete) {
		this.auditComplete = auditComplete;
	}

	/**
	 * @return the billingIdOut
	 */
	@Column(length=25)
	public String getBillingIdOut() {
		return billingIdOut;
	}

	/**
	 * @param billingIdOut the billingIdOut to set
	 */
	public void setBillingIdOut(String billingIdOut) {
		this.billingIdOut = billingIdOut;
	}

	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the authorizationNo2
	 */
	@Column(length=25)
	public String getAuthorizationNo2() {
		return authorizationNo2;
	}

	/**
	 * @param authorizationNo2 the authorizationNo2 to set
	 */
	public void setAuthorizationNo2(String authorizationNo2) {
		this.authorizationNo2 = authorizationNo2;
	}

	/**
	 * @return the authUpdated
	 */
	@Column
	public Date getAuthUpdated() {
		return authUpdated;
	}

	/**
	 * @param authUpdated the authUpdated to set
	 */
	public void setAuthUpdated(Date authUpdated) {
		this.authUpdated = authUpdated;
	}

	/**
	 * @return the gstAccess1AuthNo
	 */
	@Column(length=20)
	public String getGstAccess1AuthNo() {
		return gstAccess1AuthNo;
	}

	/**
	 * @param gstAccess1AuthNo the gstAccess1AuthNo to set
	 */
	public void setGstAccess1AuthNo(String gstAccess1AuthNo) {
		this.gstAccess1AuthNo = gstAccess1AuthNo;
	}

	/**
	 * @return the gstAccess1AuthReceived
	 */
	@Column
	public Date getGstAccess1AuthReceived() {
		return gstAccess1AuthReceived;
	}

	/**
	 * @param gstAccess1AuthReceived the gstAccess1AuthReceived to set
	 */
	public void setGstAccess1AuthReceived(Date gstAccess1AuthReceived) {
		this.gstAccess1AuthReceived = gstAccess1AuthReceived;
	}

	/**
	 * @return the gstAccess1Request
	 */
	@Column
	public Date getGstAccess1Request() {
		return gstAccess1Request;
	}

	/**
	 * @param gstAccess1Request the gstAccess1Request to set
	 */
	public void setGstAccess1Request(Date gstAccess1Request) {
		this.gstAccess1Request = gstAccess1Request;
	}

	/**
	 * @return the gstAccess1Service
	 */
	@Column
	public Date getGstAccess1Service() {
		return gstAccess1Service;
	}

	/**
	 * @param gstAccess1Service the gstAccess1Service to set
	 */
	public void setGstAccess1Service(Date gstAccess1Service) {
		this.gstAccess1Service = gstAccess1Service;
	}

	/**
	 * @return the gstAccess2AuthNo
	 */
	@Column(length=20)
	public String getGstAccess2AuthNo() {
		return gstAccess2AuthNo;
	}

	/**
	 * @param gstAccess2AuthNo the gstAccess2AuthNo to set
	 */
	public void setGstAccess2AuthNo(String gstAccess2AuthNo) {
		this.gstAccess2AuthNo = gstAccess2AuthNo;
	}

	/**
	 * @return the gstAccess2AuthReceived
	 */
	@Column
	public Date getGstAccess2AuthReceived() {
		return gstAccess2AuthReceived;
	}

	/**
	 * @param gstAccess2AuthReceived the gstAccess2AuthReceived to set
	 */
	public void setGstAccess2AuthReceived(Date gstAccess2AuthReceived) {
		this.gstAccess2AuthReceived = gstAccess2AuthReceived;
	}

	/**
	 * @return the gstAccess2Request
	 */
	@Column
	public Date getGstAccess2Request() {
		return gstAccess2Request;
	}

	/**
	 * @param gstAccess2Request the gstAccess2Request to set
	 */
	public void setGstAccess2Request(Date gstAccess2Request) {
		this.gstAccess2Request = gstAccess2Request;
	}

	/**
	 * @return the gstAccess2Service
	 */
	@Column
	public Date getGstAccess2Service() {
		return gstAccess2Service;
	}

	/**
	 * @param gstAccess2Service the gstAccess2Service to set
	 */
	public void setGstAccess2Service(Date gstAccess2Service) {
		this.gstAccess2Service = gstAccess2Service;
	}

	/**
	 * @return the gstAccess3AuthNo
	 */
	@Column(length=20)
	public String getGstAccess3AuthNo() {
		return gstAccess3AuthNo;
	}

	/**
	 * @param gstAccess3AuthNo the gstAccess3AuthNo to set
	 */
	public void setGstAccess3AuthNo(String gstAccess3AuthNo) {
		this.gstAccess3AuthNo = gstAccess3AuthNo;
	}

	/**
	 * @return the gstAccess3AuthReceived
	 */
	@Column
	public Date getGstAccess3AuthReceived() {
		return gstAccess3AuthReceived;
	}

	/**
	 * @param gstAccess3AuthReceived the gstAccess3AuthReceived to set
	 */
	public void setGstAccess3AuthReceived(Date gstAccess3AuthReceived) {
		this.gstAccess3AuthReceived = gstAccess3AuthReceived;
	}

	/**
	 * @return the gstAccess3Request
	 */
	@Column
	public Date getGstAccess3Request() {
		return gstAccess3Request;
	}

	/**
	 * @param gstAccess3Request the gstAccess3Request to set
	 */
	public void setGstAccess3Request(Date gstAccess3Request) {
		this.gstAccess3Request = gstAccess3Request;
	}

	/**
	 * @return the gstAccess3Service
	 */
	@Column
	public Date getGstAccess3Service() {
		return gstAccess3Service;
	}

	/**
	 * @param gstAccess3Service the gstAccess3Service to set
	 */
	public void setGstAccess3Service(Date gstAccess3Service) {
		this.gstAccess3Service = gstAccess3Service;
	}

	/**
	 * @return the gstExtraStopAuthNo
	 */
	@Column(length=20)
	public String getGstExtraStopAuthNo() {
		return gstExtraStopAuthNo;
	}

	/**
	 * @param gstExtraStopAuthNo the gstExtraStopAuthNo to set
	 */
	public void setGstExtraStopAuthNo(String gstExtraStopAuthNo) {
		this.gstExtraStopAuthNo = gstExtraStopAuthNo;
	}

	/**
	 * @return the gstExtraStopAuthReceived
	 */
	@Column
	public Date getGstExtraStopAuthReceived() {
		return gstExtraStopAuthReceived;
	}

	/**
	 * @param gstExtraStopAuthReceived the gstExtraStopAuthReceived to set
	 */
	public void setGstExtraStopAuthReceived(Date gstExtraStopAuthReceived) {
		this.gstExtraStopAuthReceived = gstExtraStopAuthReceived;
	}

	/**
	 * @return the gstExtraStopRequest
	 */
	@Column
	public Date getGstExtraStopRequest() {
		return gstExtraStopRequest;
	}

	/**
	 * @param gstExtraStopRequest the gstExtraStopRequest to set
	 */
	public void setGstExtraStopRequest(Date gstExtraStopRequest) {
		this.gstExtraStopRequest = gstExtraStopRequest;
	}

	/**
	 * @return the gstExtraStopService
	 */
	@Column
	public Date getGstExtraStopService() {
		return gstExtraStopService;
	}

	/**
	 * @param gstExtraStopService the gstExtraStopService to set
	 */
	public void setGstExtraStopService(Date gstExtraStopService) {
		this.gstExtraStopService = gstExtraStopService;
	}

	/**
	 * @return the gstShuttleDestAuthNo
	 */
	@Column(length=20)
	public String getGstShuttleDestAuthNo() {
		return gstShuttleDestAuthNo;
	}

	/**
	 * @param gstShuttleDestAuthNo the gstShuttleDestAuthNo to set
	 */
	public void setGstShuttleDestAuthNo(String gstShuttleDestAuthNo) {
		this.gstShuttleDestAuthNo = gstShuttleDestAuthNo;
	}

	/**
	 * @return the gstShuttleDestAuthReceived
	 */
	@Column
	public Date getGstShuttleDestAuthReceived() {
		return gstShuttleDestAuthReceived;
	}

	/**
	 * @param gstShuttleDestAuthReceived the gstShuttleDestAuthReceived to set
	 */
	public void setGstShuttleDestAuthReceived(Date gstShuttleDestAuthReceived) {
		this.gstShuttleDestAuthReceived = gstShuttleDestAuthReceived;
	}

	/**
	 * @return the gstShuttleDestRequest
	 */
	@Column
	public Date getGstShuttleDestRequest() {
		return gstShuttleDestRequest;
	}

	/**
	 * @param gstShuttleDestRequest the gstShuttleDestRequest to set
	 */
	public void setGstShuttleDestRequest(Date gstShuttleDestRequest) {
		this.gstShuttleDestRequest = gstShuttleDestRequest;
	}

	/**
	 * @return the gstShuttleDestService
	 */
	@Column
	public Date getGstShuttleDestService() {
		return gstShuttleDestService;
	}

	/**
	 * @param gstShuttleDestService the gstShuttleDestService to set
	 */
	public void setGstShuttleDestService(Date gstShuttleDestService) {
		this.gstShuttleDestService = gstShuttleDestService;
	}

	/**
	 * @return the gstShuttleOriginAuthNo
	 */
	@Column(length=20)
	public String getGstShuttleOriginAuthNo() {
		return gstShuttleOriginAuthNo;
	}

	/**
	 * @param gstShuttleOriginAuthNo the gstShuttleOriginAuthNo to set
	 */
	public void setGstShuttleOriginAuthNo(String gstShuttleOriginAuthNo) {
		this.gstShuttleOriginAuthNo = gstShuttleOriginAuthNo;
	}

	/**
	 * @return the gstShuttleOriginAuthReceived
	 */
	@Column
	public Date getGstShuttleOriginAuthReceived() {
		return gstShuttleOriginAuthReceived;
	}

	/**
	 * @param gstShuttleOriginAuthReceived the gstShuttleOriginAuthReceived to set
	 */
	public void setGstShuttleOriginAuthReceived(Date gstShuttleOriginAuthReceived) {
		this.gstShuttleOriginAuthReceived = gstShuttleOriginAuthReceived;
	}

	/**
	 * @return the gstShuttleOriginRequest
	 */
	@Column
	public Date getGstShuttleOriginRequest() {
		return gstShuttleOriginRequest;
	}

	/**
	 * @param gstShuttleOriginRequest the gstShuttleOriginRequest to set
	 */
	public void setGstShuttleOriginRequest(Date gstShuttleOriginRequest) {
		this.gstShuttleOriginRequest = gstShuttleOriginRequest;
	}

	/**
	 * @return the gstShuttleOriginService
	 */
	@Column
	public Date getGstShuttleOriginService() {
		return gstShuttleOriginService;
	}

	/**
	 * @param gstShuttleOriginService the gstShuttleOriginService to set
	 */
	public void setGstShuttleOriginService(Date gstShuttleOriginService) {
		this.gstShuttleOriginService = gstShuttleOriginService;
	}
	@Column(length=20)
	public String getGstThirdPartyAuthNoOrigin() {
		return gstThirdPartyAuthNoOrigin;
	}

	public void setGstThirdPartyAuthNoOrigin(String gstThirdPartyAuthNoOrigin) {
		this.gstThirdPartyAuthNoOrigin = gstThirdPartyAuthNoOrigin;
	}
	@Column
	public Date getGstThirdPartyAuthReceivedOrigin() {
		return gstThirdPartyAuthReceivedOrigin;
	}

	public void setGstThirdPartyAuthReceivedOrigin(
			Date gstThirdPartyAuthReceivedOrigin) {
		this.gstThirdPartyAuthReceivedOrigin = gstThirdPartyAuthReceivedOrigin;
	}
	@Column
	public Date getGstThirdPartyRequestOrigin() {
		return gstThirdPartyRequestOrigin;
	}

	public void setGstThirdPartyRequestOrigin(Date gstThirdPartyRequestOrigin) {
		this.gstThirdPartyRequestOrigin = gstThirdPartyRequestOrigin;
	}

	@Column
	public Date getGstPartialDelivery() {
		return gstPartialDelivery;
	}

	public void setGstPartialDelivery(Date gstPartialDelivery) {
		this.gstPartialDelivery = gstPartialDelivery;
	}
	@Column(length=20)
	public String getGstPartialDeliveryAuthNo() {
		return gstPartialDeliveryAuthNo;
	}

	public void setGstPartialDeliveryAuthNo(String gstPartialDeliveryAuthNo) {
		this.gstPartialDeliveryAuthNo = gstPartialDeliveryAuthNo;
	}
	@Column
	public Date getGstPartialDeliveryAuthReceived() {
		return gstPartialDeliveryAuthReceived;
	}

	public void setGstPartialDeliveryAuthReceived(
			Date gstPartialDeliveryAuthReceived) {
		this.gstPartialDeliveryAuthReceived = gstPartialDeliveryAuthReceived;
	}
	@Column
	public Date getGstPartialDeliveryRequest() {
		return gstPartialDeliveryRequest;
	}

	public void setGstPartialDeliveryRequest(Date gstPartialDeliveryRequest) {
		this.gstPartialDeliveryRequest = gstPartialDeliveryRequest;
	}
	@Column(length=20)
	public String getGstThirdPartyAuthNoDestination() {
		return gstThirdPartyAuthNoDestination;
	}

	public void setGstThirdPartyAuthNoDestination(
			String gstThirdPartyAuthNoDestination) {
		this.gstThirdPartyAuthNoDestination = gstThirdPartyAuthNoDestination;
	}
	@Column
	public Date getGstThirdPartyAuthReceivedDestination() {
		return gstThirdPartyAuthReceivedDestination;
	}
	
	public void setGstThirdPartyAuthReceivedDestination(
			Date gstThirdPartyAuthReceivedDestination) {
		this.gstThirdPartyAuthReceivedDestination = gstThirdPartyAuthReceivedDestination;
	}
	@Column
	public Date getGstThirdPartyDestination() {
		return gstThirdPartyDestination;
	}

	public void setGstThirdPartyDestination(Date gstThirdPartyDestination) {
		this.gstThirdPartyDestination = gstThirdPartyDestination;
	}
	@Column
	public Date getGstThirdPartyOrigin() {
		return gstThirdPartyOrigin;
	}

	public void setGstThirdPartyOrigin(Date gstThirdPartyOrigin) {
		this.gstThirdPartyOrigin = gstThirdPartyOrigin;
	}
	@Column
	public Date getGstThirdPartyRequestDestination() {
		return gstThirdPartyRequestDestination;
	}

	public void setGstThirdPartyRequestDestination(
			Date gstThirdPartyRequestDestination) {
		this.gstThirdPartyRequestDestination = gstThirdPartyRequestDestination;
	}

	/**
	 * @return the attnBilling
	 */
	@Column(length=100)
	public String getAttnBilling() {
		return attnBilling;
	}

	/**
	 * @param attnBilling the attnBilling to set
	 */
	public void setAttnBilling(String attnBilling) {
		this.attnBilling = attnBilling;
	}

	@Column
	public String getAuditor() {
		return auditor;
	}

	public void setAuditor(String auditor) {
		this.auditor = auditor;
	}

	@Column(length=30)
	public String getInsuranceVatDescr() {
		return insuranceVatDescr;
	}

	public void setInsuranceVatDescr(String insuranceVatDescr) {
		this.insuranceVatDescr = insuranceVatDescr;
	}

	@Column(length=6)
	public String getInsuranceVatPercent() {
		return insuranceVatPercent;
	}

	public void setInsuranceVatPercent(String insuranceVatPercent) {
		this.insuranceVatPercent = insuranceVatPercent;
	}

	@Column(length=30)
	public String getStorageVatDescr() {
		return storageVatDescr;
	}

	public void setStorageVatDescr(String storageVatDescr) {
		this.storageVatDescr = storageVatDescr;
	}
	@Column(length=6)
	public String getStorageVatPercent() {
		return storageVatPercent;
	}

	public void setStorageVatPercent(String storageVatPercent) {
		this.storageVatPercent = storageVatPercent;
	}

	public String getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(String creditTerms) {
		this.creditTerms = creditTerms;
	}
	@Column
	public Boolean getHistoricalContracts() {
		return historicalContracts;
	}

	public void setHistoricalContracts(Boolean historicalContracts) {
		this.historicalContracts = historicalContracts;
	}
	
	@Column(length=20)
	public String getBillingFrequency() {
		return billingFrequency;
	}

	public void setBillingFrequency(String billingFrequency) {
		this.billingFrequency = billingFrequency;
	}
	
	@Column
	public Date getTaxableOn() {
		return taxableOn;
	}

	public void setTaxableOn(Date taxableOn) {
		this.taxableOn = taxableOn;
	}
	@Column(length=1)
	public boolean getCod() {
		return cod;
	}

	public void setCod(boolean cod) {
		this.cod = cod;
	}
	@Column(length=15)
	public BigDecimal getCodAmount() {
		return codAmount;
	}

	public void setCodAmount(BigDecimal codAmount) {
		this.codAmount = codAmount;
	}
	@Column
	public String getBillingEmail() {
		return billingEmail;
	}

	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	@Column
	public String getInsuranceValueEntitleCurrency() {
		return insuranceValueEntitleCurrency;
	}

	public void setInsuranceValueEntitleCurrency(
			String insuranceValueEntitleCurrency) {
		this.insuranceValueEntitleCurrency = insuranceValueEntitleCurrency;
	}
	/**
	 * @return the gstThirdPartyService
	 */
	/*@Column
	public Date getGstThirdPartyService() {
		return gstThirdPartyService;
	}

	*//**
	 * @param gstThirdPartyService the gstThirdPartyService to set
	 *//*
	public void setGstThirdPartyService(Date gstThirdPartyService) {
		this.gstThirdPartyService = gstThirdPartyService;
	}*/

	

	 

	@Column
	public Date getValueDate() {
		return valueDate;
	}

	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}

 

	 

	@Column
	public String getBaseInsuranceTotal() {
		return baseInsuranceTotal;
	}

	public void setBaseInsuranceTotal(String baseInsuranceTotal) {
		this.baseInsuranceTotal = baseInsuranceTotal;
	}
	@Column
	public BigDecimal getBaseInsuranceValue() {
		return baseInsuranceValue;
	}

	public void setBaseInsuranceValue(BigDecimal baseInsuranceValue) {
		this.baseInsuranceValue = baseInsuranceValue;
	}
	@Column
	public BigDecimal getInsuranceBuyRate() {
		return insuranceBuyRate;
	}

	public void setInsuranceBuyRate(BigDecimal insuranceBuyRate) {
		this.insuranceBuyRate = insuranceBuyRate;
	}
	@Column
	public BigDecimal getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public BigDecimal getEstMoveCost() {
		return estMoveCost;
	}

	public void setEstMoveCost(BigDecimal estMoveCost) {
		this.estMoveCost = estMoveCost;
	}

	public String getVendorCode1() {
		return vendorCode1;
	}

	public void setVendorCode1(String vendorCode1) {
		this.vendorCode1 = vendorCode1;
	}

	public String getVendorName1() {
		return vendorName1;
	}

	public void setVendorName1(String vendorName1) {
		this.vendorName1 = vendorName1;
	}

	public String getInsuranceCharge1() {
		return insuranceCharge1;
	}

	public void setInsuranceCharge1(String insuranceCharge1) {
		this.insuranceCharge1 = insuranceCharge1;
	}

	public String getDeductible() {
		return deductible;
	}

	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}

	public String getAdditionalNo() {
		return additionalNo;
	}

	public void setAdditionalNo(String additionalNo) {
		this.additionalNo = additionalNo;
	}
	@Column
	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
	@Column
	public String getVendorStoragePerMonth() {
		return vendorStoragePerMonth;
	}

	public void setVendorStoragePerMonth(String vendorStoragePerMonth) {
		this.vendorStoragePerMonth = vendorStoragePerMonth;
	}
	@Column
	public String getVendorStorageVatDescr() {
		return vendorStorageVatDescr;
	}

	public void setVendorStorageVatDescr(String vendorStorageVatDescr) {
		this.vendorStorageVatDescr = vendorStorageVatDescr;
	}
	@Column
	public BigDecimal getVendorStorageVatPercent() {
		return vendorStorageVatPercent;
	}

	public void setVendorStorageVatPercent(BigDecimal vendorStorageVatPercent) {
		this.vendorStorageVatPercent = vendorStorageVatPercent;
	}
	@Column
	public String getVendorBillingCurency() {
		return vendorBillingCurency;
	}
	
	public void setVendorBillingCurency(String vendorBillingCurency) {
		this.vendorBillingCurency = vendorBillingCurency;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}

	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}

	@Column
	public Boolean getReadyForInvoicing() {
		return readyForInvoicing;
	}

	public void setReadyForInvoicing(Boolean readyForInvoicing) {
		this.readyForInvoicing = readyForInvoicing;
	}
	@Column
	public String getContractCurrency() {
		return contractCurrency;
	}

	public void setContractCurrency(String contractCurrency) {
		this.contractCurrency = contractCurrency;
	}
	@Column
	public String getPayableContractCurrency() {
		return payableContractCurrency;
	}
	@Column
	public String getClaimHandler() {
		return claimHandler;
	}

	public void setClaimHandler(String claimHandler) {
		this.claimHandler = claimHandler;
	}

	public void setPayableContractCurrency(String payableContractCurrency) {
		this.payableContractCurrency = payableContractCurrency;
	}

	@Column(length=30)
	public String getPrimaryVatCode() {
		return primaryVatCode;
	}

	public void setPrimaryVatCode(String primaryVatCode) {
		this.primaryVatCode = primaryVatCode;
	}

	@Column(length=30)
	public String getSecondaryVatCode() {
		return secondaryVatCode;
	}

	public void setSecondaryVatCode(String secondaryVatCode) {
		this.secondaryVatCode = secondaryVatCode;
	}

	@Column(length=30)
	public String getPrivatePartyVatCode() {
		return privatePartyVatCode;
	}

	public void setPrivatePartyVatCode(String privatePartyVatCode) {
		this.privatePartyVatCode = privatePartyVatCode;
	}

	@Column(length=30)
	public String getOriginAgentVatCode() {
		return originAgentVatCode;
	}

	public void setOriginAgentVatCode(String originAgentVatCode) {
		this.originAgentVatCode = originAgentVatCode;
	}

	@Column(length=30)
	public String getDestinationAgentVatCode() {
		return destinationAgentVatCode;
	}

	public void setDestinationAgentVatCode(String destinationAgentVatCode) {
		this.destinationAgentVatCode = destinationAgentVatCode;
	}

	@Column(length=30)
	public String getOriginSubAgentVatCode() {
		return originSubAgentVatCode;
	}

	public void setOriginSubAgentVatCode(String originSubAgentVatCode) {
		this.originSubAgentVatCode = originSubAgentVatCode;
	}

	@Column(length=30)
	public String getDestinationSubAgentVatCode() {
		return destinationSubAgentVatCode;
	}

	public void setDestinationSubAgentVatCode(String destinationSubAgentVatCode) {
		this.destinationSubAgentVatCode = destinationSubAgentVatCode;
	}

	@Column(length=30)
	public String getForwarderVatCode() {
		return forwarderVatCode;
	}

	public void setForwarderVatCode(String forwarderVatCode) {
		this.forwarderVatCode = forwarderVatCode;
	}

	@Column(length=30)
	public String getBrokerVatCode() {
		return brokerVatCode;
	}

	public void setBrokerVatCode(String brokerVatCode) {
		this.brokerVatCode = brokerVatCode;
	}

	@Column(length=30)
	public String getNetworkPartnerVatCode() {
		return networkPartnerVatCode;
	}

	public void setNetworkPartnerVatCode(String networkPartnerVatCode) {
		this.networkPartnerVatCode = networkPartnerVatCode;
	}

	@Column(length=30)
	public String getBookingAgentVatCode() {
		return bookingAgentVatCode;
	}

	public void setBookingAgentVatCode(String bookingAgentVatCode) {
		this.bookingAgentVatCode = bookingAgentVatCode;
	}

	@Column(length=30)
	public String getVendorCodeVatCode() {
		return vendorCodeVatCode;
	}

	public void setVendorCodeVatCode(String vendorCodeVatCode) {
		this.vendorCodeVatCode = vendorCodeVatCode;
	}
	
	@Column(length=15)
	public BigDecimal getPayableRate() {
		return payableRate;
	}

	public void setPayableRate(BigDecimal payableRate) {
		this.payableRate = payableRate;
	}
	@Column(length=30)
	public String getVendorCodeVatCode1() {
		return vendorCodeVatCode1;
	}

	public void setVendorCodeVatCode1(String vendorCodeVatCode1) {
		this.vendorCodeVatCode1 = vendorCodeVatCode1;
	}
	@Column
	public String getNoInsurance() {
		return noInsurance;
	}

	public void setNoInsurance(String noInsurance) {
		this.noInsurance = noInsurance;
	}
	@Column
	public Date getInsSubmitDate() {
		return insSubmitDate;
	}

	public void setInsSubmitDate(Date insSubmitDate) {
		this.insSubmitDate = insSubmitDate;
	}
	
	
	public String getStorageEmail() {
		return storageEmail;
	}

	public void setStorageEmail(String storageEmail) {
		this.storageEmail = storageEmail;
	}
	@Column
	public Date getWareHousereceiptDate() {
		return wareHousereceiptDate;
	}

	public void setWareHousereceiptDate(Date wareHousereceiptDate) {
		this.wareHousereceiptDate = wareHousereceiptDate;
	}

	public String getPersonForwarder() {
		return personForwarder;
	}

	public void setPersonForwarder(String personForwarder) {
		this.personForwarder = personForwarder;
	}
	@Column
	public String getBillingInsurancePayableCurrency() {
		return billingInsurancePayableCurrency;
	}

	public void setBillingInsurancePayableCurrency(
			String billingInsurancePayableCurrency) {
		this.billingInsurancePayableCurrency = billingInsurancePayableCurrency;
	}
	@Column
	public String getBillName() {
		return billName;
	}

	public void setBillName(String billName) {
		this.billName = billName;
	}

	public String getLocationAgentCode() {
		return locationAgentCode;
	}

	public void setLocationAgentCode(String locationAgentCode) {
		this.locationAgentCode = locationAgentCode;
	}

	public String getLocationAgentName() {
		return locationAgentName;
	}

	public void setLocationAgentName(String locationAgentName) {
		this.locationAgentName = locationAgentName;
	}
	@Column
	public Date getRecSignedDisclaimerDate() {
		return recSignedDisclaimerDate;
	}

	public void setRecSignedDisclaimerDate(Date recSignedDisclaimerDate) {
		this.recSignedDisclaimerDate = recSignedDisclaimerDate;
	}
	@Column
	public Boolean getfXRateOnActualizationDate() {
		return fXRateOnActualizationDate;
	}

	public void setfXRateOnActualizationDate(Boolean fXRateOnActualizationDate) {
		this.fXRateOnActualizationDate = fXRateOnActualizationDate;
	}
	@Column
	public boolean isStorageVatExclude() {
		return storageVatExclude;
	}

	public void setStorageVatExclude(boolean storageVatExclude) {
		this.storageVatExclude = storageVatExclude;
	}
	@Column
	public boolean isInsuranceVatExclude() {
		return insuranceVatExclude;
	}

	public void setInsuranceVatExclude(boolean insuranceVatExclude) {
		this.insuranceVatExclude = insuranceVatExclude;
	}

	@Column(length=8)
	public String getNetworkBillToCode() {
		return networkBillToCode;
	}

	public void setNetworkBillToCode(String networkBillToCode) {
		this.networkBillToCode = networkBillToCode;
	}

	@Column(length=225)
	public String getNetworkBillToName() {
		return networkBillToName;
	}

	public void setNetworkBillToName(String networkBillToName) {
		this.networkBillToName = networkBillToName;
	}

	public Date getContractReceivedDate() {
		return contractReceivedDate;
	}

	public void setContractReceivedDate(Date contractReceivedDate) {
		this.contractReceivedDate = contractReceivedDate;
	}

	public String getInternalBillingPerson() {
		return internalBillingPerson;
	}

	public void setInternalBillingPerson(String internalBillingPerson) {
		this.internalBillingPerson = internalBillingPerson;
	}
	@Column(length=30)
	public String getContractSystem() {
		return contractSystem;
	}

	public void setContractSystem(String contractSystem) {
		this.contractSystem = contractSystem;
	}

	public String getTotalLossCoverageIndicator() {
		return totalLossCoverageIndicator;
	}

	public void setTotalLossCoverageIndicator(String totalLossCoverageIndicator) {
		this.totalLossCoverageIndicator = totalLossCoverageIndicator;
	}

	public String getMechanicalMalfunctionIndicator() {
		return mechanicalMalfunctionIndicator;
	}

	public void setMechanicalMalfunctionIndicator(
			String mechanicalMalfunctionIndicator) {
		this.mechanicalMalfunctionIndicator = mechanicalMalfunctionIndicator;
	}

	public String getPairsOrSetsIndicator() {
		return pairsOrSetsIndicator;
	}

	public void setPairsOrSetsIndicator(String pairsOrSetsIndicator) {
		this.pairsOrSetsIndicator = pairsOrSetsIndicator;
	}

	public String getMoldOrMildewIndicator() {
		return moldOrMildewIndicator;
	}

	public void setMoldOrMildewIndicator(String moldOrMildewIndicator) {
		this.moldOrMildewIndicator = moldOrMildewIndicator;
	}
	public BigDecimal getAutoInsuredValue() {
		return autoInsuredValue;
	}
	
	public void setAutoInsuredValue(BigDecimal autoInsuredValue) {
		this.autoInsuredValue = autoInsuredValue;
	}

	public BigDecimal getAutoBilledRate() {
		return autoBilledRate;
	}

	public void setAutoBilledRate(BigDecimal autoBilledRate) {
		this.autoBilledRate = autoBilledRate;
	}

	public BigDecimal getAutoInternalRate() {
		return autoInternalRate;
	}

	public void setAutoInternalRate(BigDecimal autoInternalRate) {
		this.autoInternalRate = autoInternalRate;
	}
	@Column
	public Boolean getPaymentReceived() {
		return paymentReceived;
	}

	public void setPaymentReceived(Boolean paymentReceived) {
		this.paymentReceived = paymentReceived;
	}

	public String getStorageEmailType() {
		return storageEmailType;
	}

	public void setStorageEmailType(String storageEmailType) {
		this.storageEmailType = storageEmailType;
	}
	@Column
	public Date getFIDIISOAudit() {
		return FIDIISOAudit;
	}

	public void setFIDIISOAudit(Date fIDIISOAudit) {
		FIDIISOAudit = fIDIISOAudit;
	}

	@Column
	public String getValuation() {
		return valuation;
	}

	public void setValuation(String valuation) {
		this.valuation = valuation;
	}
	public Date getReadyForInvoiceCheck() {
		return readyForInvoiceCheck;
	}

	public void setReadyForInvoiceCheck(Date readyForInvoiceCheck) {
		this.readyForInvoiceCheck = readyForInvoiceCheck;
	}
  @Column
	public Date getFinancialsComplete() {
		return financialsComplete;
	}

	public void setFinancialsComplete(Date financialsComplete) {
		this.financialsComplete = financialsComplete;
	}
	public String getBillingCycle() {
		return billingCycle;
	}

	public void setBillingCycle(String billingCycle) {
		this.billingCycle = billingCycle;
	}
	
	}