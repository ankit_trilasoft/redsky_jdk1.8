package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CPortalResourceMgmtDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.CPortalResourceMgmt;

public class CPortalResourceMgmtDaoHibernate extends GenericDaoHibernate<CPortalResourceMgmt, Long> implements CPortalResourceMgmtDao{
	private HibernateUtil hibernateUtil;
	public CPortalResourceMgmtDaoHibernate(){
		super(CPortalResourceMgmt.class);
	}
	public List cPortalResourceMgmtList(String corpId){
		return getHibernateTemplate().find("from CPortalResourceMgmt where  corpId= ?",corpId);
	}
	public List getMaxId(){
		return getHibernateTemplate().find("select max(id) from CPortalResourceMgmt");
	}
	public List getListByDesc(Long id){
		return getHibernateTemplate().find("select documentName from CPortalResourceMgmt where id=?",id);
	}
	public List vendorName(String partnerCode,String corpId){
		return this.getSession().createSQLQuery("select lastName from partner where binary partnerCode='"+partnerCode.trim()+"' and isAccount=true and status='Approved' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')").list();
	}
	public List getBysearchCriteria(String documentType, String documentName, String fileFileName, String originCountry, String destinationCountry, String billToCode, String billToName,String corpID) {
		// TODO Auto-generated method stub
		return getHibernateTemplate().find("from CPortalResourceMgmt where documentType like '%"+documentType.replaceAll("'", "''").replaceAll(":", "''")+"%'  and documentName like '%"+documentName.replaceAll("'", "''").replaceAll(":", "''")+"%' and  fileFileName like '%"+fileFileName.replaceAll("'", "''").replaceAll(":", "''")+"%'  and originCountry like '%"+originCountry.replaceAll("'", "''").replaceAll(":", "''")+"%'  and destinationCountry like '%"+destinationCountry.replaceAll("'", "''").replaceAll(":", "''")+"%'  and billToCode like '%"+billToCode.trim().replaceAll("'", "''").replaceAll(":", "''")+"%' and billToName like '%"+billToName.trim().replaceAll("'", "''").replaceAll(":", "''")+"%' and corpid='"+corpID+"'");
	}
	
	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public List findCPortalDocument(String partnerCode,String documentType ,String documentName,String fileFileName,String language,String infoPackage,String corpId )
	{
		return getHibernateTemplate().find("from CPortalResourceMgmt where documentType ='"+documentType+"' and (documentName='"+documentName+"' OR documentName='' ) and (fileFileName='"+fileFileName+"' or fileFileName='' )  and (language='"+language+"' OR language='' ) and (infoPackage='"+infoPackage+"' OR infoPackage='') and (billToCode='"+partnerCode+"' OR billToCode='') and corpid='"+corpId+"'");	
	}
	public List accountportalResourceMgmtList(String partnerCode,String corpId)
	{
		 return getHibernateTemplate().find("from CPortalResourceMgmt where  corpid='"+corpId+"'and documentType!='Internal' and (billToCode='"+partnerCode.trim()+"' or billToCode is null or billToCode='') and billToExcludes<>'"+partnerCode.trim()+"'");	 
	 }
	 public List FindsearchCriteria(String documentType,String documentName,String fileFileName,String originCountry,String destinationCountry,String billToCode,String corpID){
		 
		return getHibernateTemplate().find("from CPortalResourceMgmt where documentType like '%"+documentType.replaceAll("'", "''").replaceAll(":", "''")+"%'  and documentType!='Internal'  and documentName like '%"+documentName.replaceAll("'", "''").replaceAll(":", "''")+"%' and  fileFileName like '%"+fileFileName.replaceAll("'", "''").replaceAll(":", "''")+"%'  and originCountry like '%"+originCountry.replaceAll("'", "''").replaceAll(":", "''")+"%'  and destinationCountry like '%"+destinationCountry.replaceAll("'", "''").replaceAll(":", "''")+"%' and  (billToCode='"+billToCode.trim()+"' or billToCode is null or billToCode='') and billToExcludes<>'"+billToCode+"' and corpid='"+corpID+"'");	 
	 }
	public List<CPortalResourceMgmt> resourceExtracts(String documentType, String sessionCorpID) {
		return getHibernateTemplate().find("from CPortalResourceMgmt where documentType like '%"+documentType+"' and  corpId='"+sessionCorpID+"' order by documentType ");
	}
	public List getChildList(String partnerCode,String corpId){
		 List list1= getSession().createSQLQuery("select partnerCode from partnerpublic where agentParent='"+partnerCode+"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')").list();
		  return list1;
	}
	
	public List getDocumentLocation(String corpID,String language,String jobType){
		String query="select concat(SUBSTRING_INDEX(documentLocation,'/',7),'/',fileFileName) FROM cportalresourcemgmt  where corpid= '"+corpID+"' and language = '"+language+"' and documentType='SURVEYEMAIL' and jobtype like '%"+jobType+"%' ";
		List list =  this.getSession().createSQLQuery(query).list();
		return list;
	}
	
}