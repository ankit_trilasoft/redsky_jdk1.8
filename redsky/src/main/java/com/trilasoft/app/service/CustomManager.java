/**
 * 
 */
package com.trilasoft.app.service;

import java.util.Collection;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Custom;


public interface CustomManager extends GenericManager<Custom, Long> {   
	public List findBySO(Long shipNumber, String corpId);

	public List countBondedGoods(Long soId, String sessionCorpID);

	public List countForTicket(Long ticket, Long serviceOrderId, String movement, String sessionCorpID);

	public List customsInList(Long sid, String movement);
	
	public int checkCustomsId(Long transactionId, String movement, String sessionCorpID);

	public Long findRemoteCustom(Long networkId, Long serviceOrderId);

	public void deleteNetworkCustom(Long serviceOrderId, Long networkId);
	public List getTransactionIdCount(String sessionCorpID,Long customId);
	} 

