package com.trilasoft.app.dao.hibernate;

import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.appfuse.dao.hibernate.GenericDaoHibernate; 
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.trilasoft.app.dao.RefFreightRatesDao;  
import com.trilasoft.app.model.RefFreightRates;
import com.trilasoft.app.model.RefMaster;

public class RefFreightRatesDaoHibernate extends GenericDaoHibernate<RefFreightRates, Long> implements RefFreightRatesDao{
	public RefFreightRatesDaoHibernate() {   
        super(RefFreightRates.class);   
    }
	private RefFreightRates refFreightRates;
	private Map<String,String> country=new  HashMap<String, String>();
	
	public String saveData(String sessionCorpID, String file, String userName) {
		List<RefMaster> refMasterlist;
		refMasterlist = getHibernateTemplate().find("from RefMaster where  parameter= 'COUNTRY' ORDER BY description");
		for (RefMaster refMaster : refMasterlist) {
			country.put(refMaster.getBucket2(),refMaster.getCode()); 
		}
		String country3Digit=new String("");
		Session session = null;
	    //Transaction tx = null;
	    SessionFactory sessionFactory = null; 
	    String message ="";
	    String serviceContractNo=""; 
	    try
		{
	    InputStream myxls1 = new FileInputStream(file);  
		HSSFWorkbook      workBookValidation = new HSSFWorkbook (myxls1);
		HSSFSheet         sheetValidation    = workBookValidation.getSheetAt (0); 
	    Iterator  rowsValidation     = sheetValidation.rowIterator (); 
        while(rowsValidation.hasNext()){ 
			 HSSFRow rowValidation = (HSSFRow)rowsValidation.next ();   
			 if(rowValidation.getRowNum()<4){
				Iterator  cells = rowValidation.cellIterator (); 
				while (cells.hasNext ())
				{ 	
				  HSSFCell cellValidation = (HSSFCell)cells.next (); 
				  if(cellValidation.getCellNum ()<2){ 
	               if(rowValidation.getRowNum()==3 ){
     	            if(cellValidation.getCellNum ()==1){ 
     		            serviceContractNo=cellValidation.getStringCellValue(); 
     		            int count=Integer.parseInt(getHibernateTemplate().find("select count(*)from RefFreightRates where serviceContractNo ='"+serviceContractNo+"' and corpID='"+sessionCorpID+"'").get(0).toString());
     	                if(count>0){
     	                message =serviceContractNo+" already has been uploaded, so no action will be performed.";
     	                return message;
     	                }
     	             }
		           }
		        }
				}
			 }
        }
		}catch (Exception e)
		{
		e.printStackTrace ();
		}
		try
		{  
		 int batchSize = 100; 
		 int processFromRow = 19;
		 int processToRow = 119; 
		 InputStream myxls = new FileInputStream(file);  
		 HSSFWorkbook      workBook = new HSSFWorkbook (myxls);
		 HSSFSheet         sheet    = workBook.getSheetAt (0); 
		 int sheetSize=sheet.getLastRowNum()+100;
		while(sheetSize>processToRow){ 
		Map freightData = new HashMap(); 
	    ArrayList originRegionList = new ArrayList<String>();
	    ArrayList originCityList = new ArrayList<String>();
	    ArrayList originCountryList = new ArrayList<String>();
	    ArrayList originPortCityList = new ArrayList<String>();
	    ArrayList originPortCountryList = new ArrayList<String>();
	    ArrayList originRoutingList = new ArrayList<String>();
	    ArrayList destinationRegionList = new ArrayList<String>();
	    ArrayList destinationCityList = new ArrayList<String>();
	    ArrayList destinationCountryList = new ArrayList<String>();
	    ArrayList destinationPortCityList = new ArrayList<String>();
	    ArrayList destinationPortCountryList = new ArrayList<String>();
	    ArrayList equipmentTypeList = new ArrayList<String>();
	    ArrayList transitDaysList = new ArrayList<String>();
	    ArrayList numberofSailingsList = new ArrayList<String>();
	    ArrayList sailingFrequencyList = new ArrayList<String>();
	    ArrayList transshipmentList = new ArrayList<String>();
	    ArrayList lineItemEffDateList = new ArrayList<String>();
	    ArrayList lineItemExpDateList = new ArrayList<String>();
	    ArrayList totalPriceList = new ArrayList<String>();
	    ArrayList laneCommentsList = new ArrayList<String>();
	    ArrayList destArbitrariesChargeValueList = new ArrayList<String>();
	    ArrayList destArbitrariesChargeDetailsList = new ArrayList<String>();
	    ArrayList orgArbitrariesChargeValueList = new ArrayList<String>();
	    ArrayList orgArbitrariesChargeDetailsList = new ArrayList<String>();
	    ArrayList BUCValueList = new ArrayList<String>();
	    ArrayList BUCDetailsList = new ArrayList<String>();
	    ArrayList destTHCValueList = new ArrayList<String>();
	    ArrayList destTHCDetailsList = new ArrayList<String>();
	    ArrayList baseCurrencyList = new ArrayList<String>();
		String label="";
		String carrier="";
		Iterator  rows     = sheet.rowIterator (); 
           while(rows.hasNext()){ 
			HSSFRow row = (HSSFRow)rows.next ();  
			 //once get a row its time to iterate through cells.
				Iterator  cells = row.cellIterator (); 
				while (cells.hasNext ())
				{ 	
				HSSFCell cell = (HSSFCell)cells.next (); 
				if(cell.getCellNum()<65){
				    if(row.getRowNum()==7 ){
	            	   if(cell.getCellNum ()==1){
	            		  carrier=cell.getStringCellValue();
	            	   }
				    }
				if(row.getRowNum()==18 ) {
					label	 = cell.getStringCellValue();
					if(label.trim().equalsIgnoreCase("Origin Region"))
						freightData.put(cell.getCellNum ()+"String", originRegionList);
					else if(label.trim().equalsIgnoreCase("Origin City"))
					    freightData.put(cell.getCellNum ()+"String", originCityList);
					else if(label.trim().equalsIgnoreCase("Origin Country"))
					    freightData.put(cell.getCellNum ()+"String", originCountryList);
					else if(label.trim().equalsIgnoreCase("Origin Port City"))
					    freightData.put(cell.getCellNum ()+"String", originPortCityList);
					else if(label.trim().equalsIgnoreCase("Origin Port Country"))
					    freightData.put(cell.getCellNum ()+"String", originPortCountryList);
					else if(label.trim().equalsIgnoreCase("Origin Routing"))
					    freightData.put(cell.getCellNum ()+"String", originRoutingList);
					else if(label.trim().equalsIgnoreCase("Destination Region"))
					    freightData.put(cell.getCellNum ()+"String", destinationRegionList);
					else if(label.trim().equalsIgnoreCase("Destination City"))
					    freightData.put(cell.getCellNum ()+"String", destinationCityList);
					else if(label.trim().equalsIgnoreCase("Destination Country"))
					    freightData.put(cell.getCellNum ()+"String", destinationCountryList);
					else if(label.trim().equalsIgnoreCase("Destination Port City"))
					    freightData.put(cell.getCellNum ()+"String", destinationPortCityList);
					else if(label.trim().equalsIgnoreCase("Destination Port Country"))
					    freightData.put(cell.getCellNum ()+"String", destinationPortCountryList);
					else if(label.trim().equalsIgnoreCase("Equipment Type"))
					    freightData.put(cell.getCellNum ()+"String", equipmentTypeList);
					else if(label.trim().equalsIgnoreCase("Transit Time (Days)"))
					    freightData.put(cell.getCellNum ()+"Num", transitDaysList);
					else if(label.trim().equalsIgnoreCase("Number of Sailings"))
					    freightData.put(cell.getCellNum ()+"String", numberofSailingsList);
					else if(label.trim().equalsIgnoreCase("Sailing Frequency"))
					    freightData.put(cell.getCellNum ()+"String", sailingFrequencyList);
					else if(label.trim().equalsIgnoreCase("Transshipment"))
					    freightData.put(cell.getCellNum ()+"String", transshipmentList);
					else if(label.trim().equalsIgnoreCase("Line Item Eff Date"))
					    freightData.put(cell.getCellNum ()+"Date", lineItemEffDateList);
					else if(label.trim().equalsIgnoreCase("Line Item Exp Date"))
					    freightData.put(cell.getCellNum ()+"Date", lineItemExpDateList);
					else if(label.trim().equalsIgnoreCase("Total Price"))
					    freightData.put(cell.getCellNum ()+"Num", totalPriceList);
					else if(label.trim().equalsIgnoreCase("Lane Comments"))
					    freightData.put(cell.getCellNum ()+"String", laneCommentsList);
					else if(label.trim().equalsIgnoreCase("Arbitraries Charge (Destination) Value"))
					    freightData.put(cell.getCellNum ()+"Num", destArbitrariesChargeValueList);
					else if(label.trim().equalsIgnoreCase("Arbitraries Charge (Destination) Details"))
					    freightData.put(cell.getCellNum ()+"String", destArbitrariesChargeDetailsList);
					else if(label.trim().equalsIgnoreCase("Arbitraries Charge (Origin) Value"))
					    freightData.put(cell.getCellNum ()+"Num", orgArbitrariesChargeValueList);
					else if(label.trim().equalsIgnoreCase("Arbitraries Charge (Origin) Details"))
					    freightData.put(cell.getCellNum ()+"String", orgArbitrariesChargeDetailsList);
					else if(label.trim().equalsIgnoreCase("BUC Value"))
					    freightData.put(cell.getCellNum ()+"Num", BUCValueList);
					else if(label.trim().equalsIgnoreCase("BUC Details"))
					    freightData.put(cell.getCellNum ()+"String", BUCDetailsList);
					else if(label.trim().equalsIgnoreCase("Dest. THC Value"))
					    freightData.put(cell.getCellNum ()+"Num", destTHCValueList);
					else if(label.trim().equalsIgnoreCase("Dest. THC Details"))
					    freightData.put(cell.getCellNum ()+"String", destTHCDetailsList);
					else if(label.trim().equalsIgnoreCase("Base Rate Currency"))
					    freightData.put(cell.getCellNum ()+"String", baseCurrencyList);
				} 
				if(row.getRowNum()>= processFromRow && row.getRowNum() < processToRow ) {
					ArrayList list =  new ArrayList<String>(); 
					list= (ArrayList)freightData.get(cell.getCellNum()+"String"); 
				    if (list != null) {
				    	try{
				    	list.add(cell.getStringCellValue());
				    	freightData.put(cell.getCellNum()+"String", list);
				    	}catch(Exception e){
				    		
				    	}
				    }else if(list == null){
				    	list = (ArrayList)freightData.get(cell.getCellNum()+"Num");
				    	 if (list != null) {
				    		 try{
						    	list.add(cell.getNumericCellValue());
						    	freightData.put(cell.getCellNum()+"Num", list);
				    		 }catch(Exception e){
						    		
						    	}
				    } else if(list == null){
				    	list = (ArrayList)freightData.get(cell.getCellNum()+"Date");
				    	if(list != null){
				    		try{
				    		Date LineItemEffDate =cell.getDateCellValue(); 
					  		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
					        Date du = new Date(); 
					        df = new SimpleDateFormat("yyyy-MM-dd"); 
					        df.format(LineItemEffDate);
					        list.add(df.format(LineItemEffDate));
					        freightData.put(cell.getCellNum()+"Date", list);
				    		}catch(Exception e){
					    		
					    	}
				    	}
				    }
				}
				
		}
				}
		}  
           }
		
//		inser the data
		for (int i =0;i<originRegionList.size();i++){
			refFreightRates = new  RefFreightRates();
			refFreightRates.setCorpID(sessionCorpID);
			refFreightRates.setCreatedBy(userName);
			refFreightRates.setCreatedOn(new Date());
			refFreightRates.setUpdatedBy(userName);
			refFreightRates.setUpdatedOn(new Date());
			refFreightRates.setMode("Sea");
			refFreightRates.setContractType("OSA Rates");
			refFreightRates.setCarrier(carrier);
			refFreightRates.setServiceContractNo(serviceContractNo);
			try{
			refFreightRates.setOriginRegion((String)originRegionList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setOriginCity((String)originCityList.get(i));
			}catch(Exception e){}
			try{
				country3Digit=country.get((String)originCountryList.get(i));
			refFreightRates.setOriginCountry(country3Digit);
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setOriginPortCity((String)originPortCityList.get(i));
			}catch(Exception e){
				
			}
			try{
				country3Digit=country.get((String)originPortCountryList.get(i));
			refFreightRates.setOriginPortCountry(country3Digit);
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setOriginRouting((String)originRoutingList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setDestinationRegion((String)destinationRegionList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setDestinationCity((String)destinationCityList.get(i));
			}catch(Exception e){
				
			}
			try{
				country3Digit=country.get((String)destinationCountryList.get(i));
			refFreightRates.setDestinationCountry(country3Digit);
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setDestinationPortCity((String)destinationPortCityList.get(i));
			}catch(Exception e){
				
			}
			try{
				country3Digit=country.get((String)destinationPortCountryList.get(i));
			refFreightRates.setDestinationPortCountry(country3Digit);
			}catch(Exception e){
				
			}
			try{
				String equipmentType=(String)equipmentTypeList.get(i);	
				if(equipmentType.equalsIgnoreCase("20' Standard Dry"))
					 equipmentType="20";
				if(equipmentType.equalsIgnoreCase("40' Standard Dry"))
					 equipmentType="40";
				if(equipmentType.equalsIgnoreCase("45' High Cube Dry"))
					 equipmentType="45";
				if(equipmentType.equalsIgnoreCase("40' High Cube Dry"))
					 equipmentType="40H";
				else
					 equipmentType=	equipmentType;
			refFreightRates.setEquipmentType(equipmentType);
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setTransitDays((String)(transitDaysList.get(i)));
			}catch(Exception e){ 
			}try{
			refFreightRates.setNumberofSailings((String)numberofSailingsList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setSailingFrequency((String)sailingFrequencyList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setTransshipment((String)transshipmentList.get(i));
			}catch(Exception e){
				
			}
			
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
			try{
			refFreightRates.setLineItemEffDate(df.parse((String)lineItemEffDateList.get(i)));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setLineItemExpDate(df.parse((String)lineItemExpDateList.get(i)));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setTotalPrice(new BigDecimal(totalPriceList.get(i).toString()));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setLaneComments((String)laneCommentsList.get(i));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setDestArbitrariesChargeValue(new BigDecimal(destArbitrariesChargeValueList.get(i).toString()));
            }catch(Exception e){
				
			}try{
			refFreightRates.setDestArbitrariesChargeDetails((String)destArbitrariesChargeDetailsList.get(i));
           }catch(Exception e){
				
			}try{
			refFreightRates.setOrgArbitrariesChargeValue(new BigDecimal(orgArbitrariesChargeValueList.get(i).toString()));
            }catch(Exception e){
				
			}try{
			refFreightRates.setOrgArbitrariesChargeDetails((String)orgArbitrariesChargeDetailsList.get(i));
			
             }catch(Exception e){
				
			}try{
				refFreightRates.setBUCValue(new BigDecimal(BUCValueList.get(i).toString()));
			}catch(Exception e){
				
			}try{
			refFreightRates.setBUCDetails((String)BUCDetailsList.get(i));
            }catch(Exception e){
				
			}try{
			refFreightRates.setDestTHCValue(new BigDecimal(destTHCValueList.get(i).toString()));
			}catch(Exception e){
				
			}
			try{
			refFreightRates.setDestTHCDetails((String)destTHCDetailsList.get(i));
			}catch(Exception e){
				
			}
			try{
				if(baseCurrencyList.isEmpty()){
				refFreightRates.setBaseCurrency("USD");	
				}else{
				refFreightRates.setBaseCurrency((String)baseCurrencyList.get(i));
				}
				}catch(Exception e){
					
				}
			//session = getHibernateTemplate().getSessionFactory().openSession();
	        //session.setFlushMode(FlushMode.COMMIT);
	        //tx = session.beginTransaction();
			try{
			this.getSession().save(refFreightRates);
			message ="File data processed successfully.";
			}catch(Exception e){
				
			}
			if ( i % 20 == 0 ) { 
			        
			        session.flush();
			        session.clear();
			    }

	    }
		processFromRow += batchSize;
		processToRow += batchSize;
		
		}
		if(message.equalsIgnoreCase("File data processed successfully.")){}
		else{message="Invalid File";}
		return message;
		}catch (Exception e)
			{
			e.printStackTrace ();
			}
		
		return message;
	
		
	}
	public  class DTOExistingContracts
	 {
		private Object carrier;
	  	private Object serviceContractNo;
		private Object count;
		private Object port;
		public Object getCarrier() {
			return carrier;
		}
		public void setCarrier(Object carrier) {
			this.carrier = carrier;
		}
		public Object getCount() {
			return count;
		}
		public void setCount(Object count) {
			this.count = count;
		}
		public Object getServiceContractNo() {
			return serviceContractNo;
		}
		public void setServiceContractNo(Object serviceContractNo) {
			this.serviceContractNo = serviceContractNo;
		}
		public Object getPort() {
			return port;
		}
		public void setPort(Object port) {
			this.port = port;
		} 
		
	 }
	public List getExistingContracts(String sessionCorpID) {
		List list1=new ArrayList();
		String query="Select distinct carrier, serviceContractNo, count(*) from reffreightrates where corpid = 	'"+sessionCorpID+"' group by carrier, servicecontractno order by 1,2;";
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
			while(it.hasNext())
			{
				Object []row= (Object [])it.next();
				DTOExistingContracts dtoExistingContracts = new DTOExistingContracts();
				dtoExistingContracts.setCarrier(row[0].toString());
				dtoExistingContracts.setServiceContractNo((row[1])); 
				dtoExistingContracts.setCount((row[2])); 
				list1.add(dtoExistingContracts);
				
			}
			
			return list1;
	}
	public List getMissingPort(String sessionCorpID) {
		List list1=new ArrayList();
		String query="select distinct originportcity as 'Port' from reffreightrates where originportcity is not null and originportcity not in (select distinct refabbreviation from port where mode = 'Sea') union select distinct destinationportcity as 'Port' from reffreightrates where destinationportcity is not null and destinationportcity not in (select distinct refabbreviation from port where mode = 'Sea') order by 1; ";
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
			while(it.hasNext())
			{
				Object row= it.next();
				DTOExistingContracts dtoExistingContracts = new DTOExistingContracts();
				dtoExistingContracts.setPort(row.toString()); 
				list1.add(dtoExistingContracts);
				
			}
			
			return list1;
	}
}
