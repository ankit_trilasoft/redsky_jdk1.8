<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerPublicList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerPublicList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
	document.forms['partnerListForm'].elements['partnerPublic.lastName'].value = '';
	document.forms['partnerListForm'].elements['partnerPublic.firstName'].value = '';
	document.forms['partnerListForm'].elements['partnerPublic.partnerCode'].value = '';
	document.forms['partnerListForm'].elements['partnerPublic.aliasName'].value = '';
	document.forms['partnerListForm'].elements['countryCodeSearch'].value = '';
	document.forms['partnerListForm'].elements['stateSearch'].value = '';
	document.forms['partnerListForm'].elements['countrySearch'].value = '';
	document.forms['partnerListForm'].elements['citySearch'].value = '';
	document.forms['partnerListForm'].elements['partnerPublic.status'].value = '';
	document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked = false;
	document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked = false;
	document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked = false;
	document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked = false;
	document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked = false;
	document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked = false;
}

function seachValidate(){
	var pp =  document.forms['partnerListForm'].elements['partnerPublic.isPrivateParty'].checked;
	var ac =  document.forms['partnerListForm'].elements['partnerPublic.isAccount'].checked;
	var ag =  document.forms['partnerListForm'].elements['partnerPublic.isAgent'].checked;
	var vd =  document.forms['partnerListForm'].elements['partnerPublic.isVendor'].checked;
	var cr =  document.forms['partnerListForm'].elements['partnerPublic.isCarrier'].checked;
	var oo =  document.forms['partnerListForm'].elements['partnerPublic.isOwnerOp'].checked;
	if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
		alert('Please select atleast one partnerPublic type.');
		return false;
	} 
}

function userStatusCheck(target,corpid){

	var targetElement = target;
	var targetElementcheck = target;
	var ids = targetElement.value; 
	var corpidValue=corpid
	var targetElementflage="";
	var userCheckCount = document.forms['addPartner'].elements['partnerCorpid'].value;
	var sameCorpidCount=0; 
	sameCorpidCount= eval(document.forms['addPartner'].elements['sameCorpidCount'].value); 
	if(targetElement.checked){

	if(userCheckCount.length<7){
   		var userCheckStatus = document.forms['addPartner'].elements['userCheck'].value;
   		var userCheckStatusCode = document.forms['addPartner'].elements['userCheckCode'].value;
   		var corpidStatus = document.forms['addPartner'].elements['partnerCorpid'].value;
   		if(userCheckStatus == ''){
			document.forms['addPartner'].elements['userCheck'].value = ids;
			document.forms['addPartner'].elements['userCheckCode'].value =ids.substring(0,ids.indexOf("*")) ;
			document.forms['addPartner'].elements['partnerCorpid'].value=corpidValue;
			document.forms['addPartner'].elements['sameCorpidCount'].value=0;
   		}else{
   		    if(sameCorpidCount>0 && userCheckCount.indexOf(corpid) == -1) {
   		    alert("Can not select more than two partner codes with different corpIds.");
   	        targetElement.checked=false;
   	        targetElementflage="true"
   		    }else{
   			var userCheckStatus =	document.forms['addPartner'].elements['userCheck'].value = userCheckStatus + ',' + ids;
   			document.forms['addPartner'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   			var userCheckStatusCode = document.forms['addPartner'].elements['userCheckCode'].value = userCheckStatusCode + ',' +ids.substring(0,ids.indexOf("*")) ;
   			document.forms['addPartner'].elements['userCheckCode'].value = userCheckStatusCode.replace( ',,' , ',' );
   			var corpidStatus=	document.forms['addPartner'].elements['partnerCorpid'].value;
   			if(corpidStatus==corpidValue){
   			corpidStatus=	document.forms['addPartner'].elements['partnerCorpid'].value = corpidStatus ;
   			sameCorpidCount =sameCorpidCount+1;
   			}else{
   			corpidStatus=	document.forms['addPartner'].elements['partnerCorpid'].value = corpidStatus + ',' + corpidValue;
   			}
   			document.forms['addPartner'].elements['partnerCorpid'].value = corpidStatus.replace( ',,' , ',' );
   		    document.forms['addPartner'].elements['sameCorpidCount'].value=sameCorpidCount; 
   		}
   		}
   	}
   	else{
   	alert("Can not select more than two partner codes with different corpIds.");
   	targetElement.checked=false;
   	targetElementflage="true"
   	}
   	}
 	if(targetElement.checked==false && targetElementflage==""){
 	
  		var userCheckStatus = document.forms['addPartner'].elements['userCheck'].value;
  		var userCheckStatusCode = document.forms['addPartner'].elements['userCheckCode'].value;
  		var userCheckStatus=document.forms['addPartner'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
  		document.forms['addPartner'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
  		var idsCode=ids.substring(0,ids.indexOf("*"));
  		var userCheckStatusCode = document.forms['addPartner'].elements['userCheckCode'].value = userCheckStatusCode.replace(idsCode,'') ;
  		document.forms['addPartner'].elements['userCheckCode'].value = userCheckStatusCode.replace( ',,' , ',' );
  		var corpidStatus = document.forms['addPartner'].elements['partnerCorpid'].value; 
  		if(sameCorpidCount <= 0){
  		var corpidStatus=document.forms['addPartner'].elements['partnerCorpid'].value = corpidStatus.replace( corpidValue , '' );
  		}
  		sameCorpidCount=sameCorpidCount-1;
  		document.forms['addPartner'].elements['sameCorpidCount'].value=sameCorpidCount; 
  		document.forms['addPartner'].elements['partnerCorpid'].value = corpidStatus.replace( ',,' , ',' );
  	}
  	var convertBtnCount = document.forms['addPartner'].elements['userCheck'].value; 
	if(convertBtnCount.length>15 ){ 
	document.forms['addPartner'].elements['convertBtn'].disabled = true;
	}else{
	document.forms['addPartner'].elements['convertBtn'].disabled = false;
	}
	var partnerCorpidTSFT=document.forms['addPartner'].elements['partnerCorpid'].value;
	if(partnerCorpidTSFT.indexOf("TSFT")  >= 0){
	document.forms['addPartner'].elements['convertBtn'].disabled = true;
	}
}

function activBtn(){
	document.forms['addPartner'].elements['userCheck'].value=document.forms['addPartner'].elements['userCheckCode'].value
	var checkBoxId = document.forms['addPartner'].elements['userCheck'].value;
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select atleast one or more duplicate partner to merge.');
		return false;
	}
	var userCheckMaster = document.forms['addPartner'].elements['userCheckMaster'].value;
	var userCheck = document.forms['addPartner'].elements['userCheck'].value;
	var partnerCorpid = document.forms['addPartner'].elements['partnerCorpid'].value;
	openWindow('mergePartnerConfirm.html?userCheckMaster='+userCheckMaster+'&userCheck='+userCheck+'&partnerCorpid='+partnerCorpid+'&decorator=popup&popup=true',1000,550);	
}

function convertPartner(){
	document.forms['addPartner'].elements['userCheck'].value=document.forms['addPartner'].elements['userCheckCode'].value
	var checkBoxId = document.forms['addPartner'].elements['userCheck'].value;
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select atleast one or more duplicate partner to merge.');
		return false;
	}
	var userCheckMaster = document.forms['addPartner'].elements['userCheckMaster'].value;
	var userCheck = document.forms['addPartner'].elements['userCheck'].value;
	var partnerCorpid = document.forms['addPartner'].elements['partnerCorpid'].value; 
	var url="checkPartnerCode.html?ajax=1&decorator=simple&popup=true&userCheck=" + encodeURI(userCheck);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
		
}

function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim(); 
                if(results>1){
                var userCheck = document.forms['addPartner'].elements['userCheck'].value;
                alert("Cannot convert to TSFT,as partner Code ("+userCheck+") exists with different corpIds ")
                }else{ 
                var userCheckMaster = document.forms['addPartner'].elements['userCheckMaster'].value;
	            var userCheck = document.forms['addPartner'].elements['userCheck'].value;
	            var partnerCorpid = document.forms['addPartner'].elements['partnerCorpid'].value; 
                document.forms['partnerListForm'].action ='convertPartner.html?convertButton=yes&userCheckMaster='+userCheckMaster+'&userCheck='+userCheck+'&partnerCorpid='+partnerCorpid;
                document.forms['partnerListForm'].submit(); 
                }
                
     }
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();

function userStatusCheckR(target,corpid){
	var targetElement = target;
	var status = targetElement.value;
	status = status.substring(status.indexOf("~")+1, status.length);
	if(status != 'Approved'){
		alert('Please select the approved partner as a master.');
		document.forms['addPartner'].reset();
		document.forms['addPartner'].elements['userCheckMaster'].checked = false;
		var elementsLen=document.forms['partnerListForm'].elements['DD'].length;
		for(i=0;i<=elementsLen-1;i++){
			document.forms['partnerListForm'].elements['DD'][i].checked = false ;
			document.forms['partnerListForm'].elements['DD'][i].disabled = true;
		}
		document.forms['addPartner'].elements['mergeBtn'].disabled = true;
		document.forms['addPartner'].elements['convertBtn'].disabled = true;
		document.forms['addPartner'].elements['userCheck'].value = '';
		document.forms['addPartner'].elements['userCheckCode'].value = '';
		document.forms['addPartner'].elements['partnerCorpid'].value = '';
		document.forms['addPartner'].elements['userCheckMaster'].value = '';	
		return false;
	}
	document.forms['addPartner'].elements['userCheck'].value="";
	document.forms['addPartner'].elements['userCheckCode'].value = "";
	document.forms['addPartner'].elements['partnerCorpid'].value = ""; 
	var len = document.forms['partnerListForm'].elements['DD'].length;
	if(len >1){
	for (i = 0; i < len; i++){
		document.forms['partnerListForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['partnerListForm'].elements['DD'][i],corpid);
	}
	}else{
	    document.forms['partnerListForm'].elements['DD'].checked = false ;
		userStatusCheck(document.forms['partnerListForm'].elements['DD'],corpid);
	}
	var id = targetElement.value;
	id = id.substring(0, id.indexOf("`"));
	document.forms['addPartner'].elements['userCheckMaster'].value = id;
	var elementsLen=document.forms['partnerListForm'].elements['DD'].length;
	if(elementsLen >1){
	for(i=0;i<=elementsLen-1;i++){
		document.forms['partnerListForm'].elements['DD'][i].disabled=false;
	}
	}else{
	    document.forms['partnerListForm'].elements['DD'].disabled=false;
	}
	document.forms['addPartner'].elements['mergeBtn'].disabled = false;	
	if(corpid !='TSFT'){
	document.forms['addPartner'].elements['convertBtn'].disabled = false;
	} else{
	document.forms['addPartner'].elements['convertBtn'].disabled = true;
	}
	var partnerCode = targetElement.value;
	partnerCode = partnerCode.substring(partnerCode.indexOf("`")+1, partnerCode.indexOf("~")); 
	if(len >1){
	for (j = 0; j < len; j++){
		var check = document.forms['partnerListForm'].elements['DD'][j].value;
		if(check == partnerCode){
			document.forms['partnerListForm'].elements['DD'][j].checked = true;
			document.forms['partnerListForm'].elements['DD'][j].disabled=true;
			document.forms['addPartner'].elements['userCheck'].value = partnerCode;
			document.forms['addPartner'].elements['userCheckCode'].value = partnerCode.substring(0, partnerCode.indexOf("*"));
			document.forms['addPartner'].elements['partnerCorpid'].value = corpid;
			document.forms['addPartner'].elements['sameCorpidCount'].value=0;
		}
	}
	}else{
	var check = document.forms['partnerListForm'].elements['DD'].value;
		if(check == partnerCode){
			document.forms['partnerListForm'].elements['DD'].checked = true;
			document.forms['partnerListForm'].elements['DD'].disabled=true;
			document.forms['addPartner'].elements['userCheck'].value = partnerCode;
			document.forms['addPartner'].elements['userCheckCode'].value = partnerCode.substring(0, partnerCode.indexOf("*"));
			document.forms['addPartner'].elements['partnerCorpid'].value = corpid;
			document.forms['addPartner'].elements['sameCorpidCount'].value=0;
	}
	}
}
</script>
<style>
span.pagelinks {
	display:block;
	margin-bottom:0px;
	!margin-bottom:2px;
	margin-top:-10px;
	!margin-top:-17px;
	padding:2px 0px;
	text-align:right;
	width:99%;
	!width:98%;
	font-size:0.85em;
}

.radiobtn {
	margin:0px 0px 2px 0px;
	!padding-bottom:5px;
	!margin-bottom:5px;
}

form {
	margin-top:-40px;
	!margin-top:0px;
}

div#main {
	margin:-5px 0 0;
}
</style>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action="searchPartnerMerge" method="post" >  
<div id="layer5" style="width:100%">

<div id="otabs">
		<ul>
			<li><a class="current"><span>Search</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;  "><span></span></div>
<div class="center-content">
<table class="table" border="0" style="width:99%;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Alias Name</th>
			<th>Last/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingCity"/></th>
			<th><fmt:message key="partner.billingState"/></th>
			<th><fmt:message key="partner.status"/></th>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partnerPublic.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partnerPublic.firstName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.aliasName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="partnerPublic.lastName" size="15" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="15" cssClass="input-text"/></td>
			<td><s:textfield name="citySearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partnerPublic.status" list="%{partnerStatus}" cssStyle="width:75px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isPrivateParty}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px">Private party<s:checkbox key="partnerPublic.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isAccount}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border-left:hidden;text-align:center;">Accounts<s:checkbox key="partnerPublic.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isAgent}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border-left:hidden;text-align:center;">Agents<s:checkbox key="partnerPublic.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
										
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isVendor}">
			<c:set var="ischecked" value="true"/>
			</c:if>					
			<td align="center" class="listwhitetext" width="100px" style="padding-left:20px ;border-left:hidden;">Vendors<s:checkbox key="partnerPublic.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
											
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isCarrier}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border-left:hidden;text-align:center;">Carriers<s:checkbox key="partnerPublic.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
					
			<c:set var="ischecked" value="false"/>
			<c:if test="${partnerPublic.isOwnerOp}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border-left:hidden;">Owner Ops<s:checkbox key="partnerPublic.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
		
			<td width="130px" style="border-left:hidden;text-align:right;" colspan="3"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
</div>

<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
	<ul>
		<li><a class="current"><span>Partner List</span></a></li>
	</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partnerPublicList" value="partnerPublicList" scope="request"/>  
<display:table name="partnerPublicList" class="table" requestURI="" id="partnerPublicLists" export="false" defaultsort="2" pagesize="500" style="width:99%;margin-left:5px;">  		
	<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerPublicLists.firstName} ${partnerPublicLists.lastName}" /></display:column>
	<display:column title="Alias Name" sortable="true" style="width:390px"><c:out value="${partnerPublicLists.aliasName} " /></display:column>
	<display:column property="mergedCode" sortable="true" title="Merged Code" style="width:65px"/>
	
	<display:column title="Master">
		<c:if test="${partnerPublicLists.doNotMerge != true}">
			<input type="radio" style="margin-left:5px;" id="checkId" name="R" value="${partnerPublicLists.id}`${partnerPublicLists.partnerCode}*${partnerPublicLists.corpID}~${partnerPublicLists.status}" onclick="userStatusCheckR(this,'${partnerPublicLists.corpID}')"/>
		</c:if>
		<c:if test="${partnerPublicLists.doNotMerge == true}">
			<input type="radio" style="margin-left:5px;" id="checkId" DISABLED name="R" value="${partnerPublicLists.id}`${partnerPublicLists.partnerCode}*${partnerPublicLists.corpID}~${partnerPublicLists.status}" onclick="userStatusCheckR(this,'${partnerPublicLists.corpID}')"/>
		</c:if>
	</display:column>
	
	<display:column title="Dupl.">
		<c:if test="${partnerPublicLists.doNotMerge != true}">
			<input type="checkbox" style="margin-left:5px;" id="checkboxId" DISABLED name="DD" value="${partnerPublicLists.partnerCode}*${partnerPublicLists.corpID}" onclick="userStatusCheck(this,'${partnerPublicLists.corpID}')"/>
		</c:if>
		<c:if test="${partnerPublicLists.doNotMerge == true}">
			<input type="checkbox" style="margin-left:5px;" id="checkboxId" DISABLED name="SS" value="${partnerPublicLists.partnerCode}*${partnerPublicLists.corpID}" onclick="userStatusCheck(this,'${partnerPublicLists.corpID}')"/>
		</c:if>
	</display:column>

	<display:column property="corpID" sortable="true" title="Corp ID" style="width:80px"/>
	
	<display:column title="PParty" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isPrivateParty == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAccount == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isAgent == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isCarrier == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isVendor == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>
	<display:column title="Owner Ops" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerPublicLists.isOwnerOp == true}">  
    		<img style="cursor: default;" src="${pageContext.request.contextPath}/images/tick01.gif" />
    	</c:if>
	</display:column>			     
	
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:100px"/>
    
    <display:setProperty name="paging.banner.item_name" value="Partner Public"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Public"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table> 

</div>
</s:form>
<table height="30px" cellpadding="0" cellspacing="0" style="width: 100%;">
	<tbody>
		<s:form id="addPartner" action="mergePartner.html" method="post" >  
			<s:hidden name="convertButton" value="<%=request.getParameter("convertButton") %>"/>
            <c:set var="convertButton"  value="<%=request.getParameter("convertButton") %>"/>
			<s:hidden name="userCheck"/>
			<s:hidden name="userCheckCode"/>
			<s:hidden name="userCheckMaster"/>
			<s:hidden name="partnerCorpid"/>
			<s:hidden name="sameCorpidCount"/> 
			<tr>
				<td>
					<input type="button" name="mergeBtn" class="cssbutton" style="width:105px; height:25px; margin-left:30px;" align="top" value="Merge Data" onclick="return activBtn();"/>   
					<input type="button" name="convertBtn" class="cssbutton" style="width:145px; height:25px; margin-left:30px;" align="top" value="Convert Master to TSFT" onclick="return convertPartner();"/>   
				</td>
			</tr>	
		</s:form>
	</tbody>
</table>
<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
<c:if test="${convertButton=='yes'}"> 
document.forms['partnerListForm'].action ='searchPartnerMerge.html';
document.forms['partnerListForm'].submit();
</c:if>	
try{
	document.forms['addPartner'].elements['mergeBtn'].disabled = true;
	document.forms['addPartner'].elements['convertBtn'].disabled = true;
}
catch(e){}
</script>
<script type="text/javascript">   
    <c:if test="${hitflag == 1}" >
    	<c:redirect url="/partnerMergeWizard.html"></c:redirect>
	</c:if>  
</script>
