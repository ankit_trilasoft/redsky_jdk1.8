<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
 
 <%--
 <script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&visible=100&amp;key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghRtKhpDtLX6R5MagO3ZOu0_GHeYthTZmolg4KuvVTJhWVj_M3YuGSUoeA" type="text/javascript"></script>  	


<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>

<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
</script>


<script>
function changeStatus()
{
    document.forms['refZipGeoCodeMapForm'].elements['formStatus'].value = '1';
}
function initLoader() { 
	 var script = document.createElement("script");
    script.type = "text/javascript";
 	 script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadGoogleGeoCode";
    document.body.appendChild(script);
    setTimeout('',6000);
    }
var geocoder;
var map;
function loadGoogleGeoCode()
   {
   	var portName=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.zipcode'].value;
	var portCode=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.city'].value;
	var country=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].value;
	
	var address = portCode+","+portName+","+country;
	
	
      // Create new map object
      map = new GMap2(document.getElementById("map"));
		
      // Create new geocoding object
      geocoder = new GClientGeocoder();

      // Retrieve location information, pass it to addToMap()
      geocoder.getLocations(address, addToMap);
   }
   function addToMap(response)
   {
      // Retrieve the object
     

      // Retrieve the latitude and longitude
      if('${refZipGeoCodeMap.latitude}'=='' || document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.latitude'].value=='' || document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.longitude'].value=='')
      {
       place = response.Placemark[0];
      point = new GLatLng(place.Point.coordinates[1],
                          place.Point.coordinates[0]);
        document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.latitude'].value = place.Point.coordinates[1];
		document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.longitude'].value = place.Point.coordinates[0];
		}
		else
		{
			point = new GLatLng('${refZipGeoCodeMap.latitude}',
                          '${refZipGeoCodeMap.longitude}');
		}
      // Center the map on this point
      map.setCenter(point, 13);
		map.setUIToDefault();
      // Create a marker
      marker = new GMarker(point);

      // Add the marker to map
      map.addOverlay(marker);

      // Add address information to marker
      //marker.openInfoWindowHtml(place.address);
   }
   


function findGeoCode(){
	window.open('http://www.gpsvisualizer.com/geocode','','width=750,height=570') ;
}

function clickPrimaryRecord(){
var zipCode=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.zipcode'].value;
if(zipCode!=''){
var primaryRec=document.forms['refZipGeoCodeMapForm'].elements['primaryRecord'].checked;
if(primaryRec== true){
document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.primaryRecord'].value='P'
}
else{
document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.primaryRecord'].value=''
}
var validateRecord=document.forms['refZipGeoCodeMapForm'].elements['primaryRecordValueValidate'].value;
if(validateRecord=='vadidate' && primaryRec== true)
{
var agree=confirm("Primary record already exist for this postal code. Do you want to change?");
var url="validatePrimaryRecord.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
if (agree){
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);
        setTimeout('validateSubmitt()',1000);
        }
        else{
        document.forms['refZipGeoCodeMapForm'].elements['primaryRecord'].checked=false;        
        document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.primaryRecord'].value='';
        }
}
}
else{
alert("Please select the mandatory (*) field.");
document.forms['refZipGeoCodeMapForm'].elements['primaryRecord'].checked=false;
}
}

function validateSubmitt()
{
 var url= "saveRefZipGeoCodeMap.html";
	    document.forms['refZipGeoCodeMapForm'].action='saveRefZipGeoCodeMap.html';
	    document.forms['refZipGeoCodeMapForm'].submit();
}
function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}

function calidatePrRe(){
var zipCode=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.zipcode'].value;
var url="validatePrimaryRecordThroughEdit.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
		http33.open("GET", url, true);
        http33.onreadystatechange = handleHttpResponse11;
        http33.send(null);	
}

function handleHttpResponse11(){
      if (http33.readyState == 4){
           var result= http33.responseText 
           result = result.trim();
           document.forms['refZipGeoCodeMapForm'].elements['primaryRecordValueValidate'].value=result;
      }
}

var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http33 = getHTTPObject33();

function getHTTPObject33(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getState(targetElement) {
	var country = document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.country'].value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
  	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(country);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
               res = results.split("@");
                targetElement = document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'];
				targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
						if(res[i] == ''){
						document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].text = '';
						document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].value = '';
						}else{
						stateVal = res[i].split("#");
						document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].text = stateVal[1];
						document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].value = stateVal[0];
						}
					}
				document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].selectedIndex=0;
		   }
        }  
 var httpState = getHTTPObject();       
 function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

 function enableStateListOrigin(){ 
		var oriCon=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.country'].value;
		  var enbState = '${enbState}';
		  var index = (enbState.indexOf(oriCon)> -1);
		  if(index != ''){
		  		document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].disabled = false;
		  }else{
			  document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].disabled = true;
		  }	        
	}

 function newFunctionForCountryState(){	
	 var originAbc ='${refZipGeoCodeMap.country}';
	 var enbState = '${enbState}';
	 var index = (enbState.indexOf(originAbc)> -1);	
	 if(index != ''){
		 document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].disabled =false;
	 getOriginStateReset(originAbc);
	 }
	 else{
		 document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].disabled =true;
	 }	 
	 }

	function getOriginStateReset(target) {
		 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(target);
	     httpState1.open("GET", url, true);
	     httpState1.onreadystatechange = handleHttpResponse555555;
	     httpState1.send(null);
	}
	          
	 function handleHttpResponse555555(){
        if (httpState1.readyState == 4){
           var results = httpState1.responseText
           results = results.trim();
           res = results.split("@");
           targetElement = document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++) {
				if(res[i] == ''){
				document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].text = '';
				document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].value = '';
				}else{
				stateVal = res[i].split("#");
				document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].text = stateVal[1];
				document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.stateabbreviation'].options[i].value = stateVal[0];
				
				if (document.getElementById("state").options[i].value == '${refZipGeoCodeMap.stateabbreviation}'){
				   document.getElementById("state").options[i].defaultSelected = true;
				} } }
				document.getElementById("state").value = '${refZipGeoCodeMap.stateabbreviation}';
        }
   }  
	  var httpState1 = getHTTPObjectState1();	  
 	function getHTTPObjectState1()	 {
	     var xmlhttp;
	     if(window.XMLHttpRequest)
	     {
	         xmlhttp = new XMLHttpRequest();
	     }
	     else if (window.ActiveXObject)
	     {
	         xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	         if (!xmlhttp)
	         {
	             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	         }
	     }
	     return xmlhttp;
	 } 
	 	 
</script>
<title><fmt:message key="refZipGeoCodeMapDetail.title" /></title>
  <meta name="heading" content="<fmt:message key='refZipGeoCodeMapDetail.heading'/>" />
	
</head>
<s:form name="refZipGeoCodeMapForm" id="refZipGeoCodeMapForm" action="saveRefZipGeoCodeMap" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>

<s:hidden name="primaryRecordValue"/>
<s:hidden name="primaryRecordValueValidate"/>
	<s:hidden name="refZipGeoCodeMap.id" value="%{refZipGeoCodeMap.id}"/>
	<s:hidden name="refZipGeoCodeMap.countryCode"/>
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Ref Zip Geo Code Map&nbsp;Detail<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
             <li><a href="refZipGeoCodeMaps.html"><span>Ref Zip Geo Code Map&nbsp;List</span></a></li>
            <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${refZipGeoCodeMap.id}&tableName=refzipgeocodemap&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
         </ul>
        </div><div class="spn">&nbsp;</div>
        
        </div>
        <div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
     <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">   
   
   <table cellspacing="0" cellpadding="0" border="0" style="width:100%;margin: 0px;padding: 0px;" >
   <tr>
   <td valign="top" align="left" style="margin: 0px;padding: 0px;">
	<table class="" cellspacing="1" cellpadding="2" border="0" style="width:100%">
	
	
	  <tr> 
	  
	  	                    <td class="listwhitetext" align="right" width=""><fmt:message key='refZipGeoCodeMap.zipcode'/><font color="red">*</font></td>
							<td align="left" width="40px"><s:textfield name="refZipGeoCodeMap.zipcode" size="10" maxlength="10"  cssClass="input-text" onchange="calidatePrRe();" onblur="valid(this,'special')"/></td>
							</tr>
							
							<tr>
                        	<td class="listwhitetext" align="right" width=""><fmt:message key='refZipGeoCodeMap.country'/></td>
							<td align="left" width="40px" colspan="3"><s:select cssClass="list-menu" name="refZipGeoCodeMap.country" id="country" list="%{ocountry}" cssStyle="width:175px;" headerKey="" headerValue="" onchange="getState(this);enableStateListOrigin();" /></td>
						  	</tr>
						  	
						  	<tr>
                        	<td class="listwhitetext" align="right" width=""><fmt:message key='refZipGeoCodeMap.state'/></td>
							<td align="left" width="40px" colspan="3"><s:select cssClass="list-menu" key="refZipGeoCodeMap.stateabbreviation" id="state" list="%{ostates}" cssStyle="width:175px;" headerKey="" headerValue=""/></td>
						  	</tr>
						  	
						  	
							<tr>
							<td class="listwhitetext" align="right" width=""><fmt:message key='refZipGeoCodeMap.city'/></td>
							<td align="left" width="40px" colspan="3"><s:textfield name="refZipGeoCodeMap.city" size="29" maxlength="50"  cssClass="input-text"  /></td>
                        	</tr>
							
						  	
						  	
				      	
						<tr>
						<td></td>
							<td align="left" class="listwhitetext" colspan="3">
								<input type="button" class="cssbutton" onclick="findGeoCode();" name="Find Geo Code" value="Manually Find Geo Code" style="width:175px; height:25px"/>
								</td>
						</tr>
						<tr>
						<tr>
						<td align="right" class="listwhitetext" width=""><fmt:message key='refZipGeoCodeMap.latitude'/></td>
						    <td width="20px"><s:textfield cssClass="input-text" name="refZipGeoCodeMap.latitude"  size="12"/></td>
						    <td align="right" class="listwhitetext" width="50px"><fmt:message key='refZipGeoCodeMap.longitude'/></td>
						    <td><s:textfield cssClass="input-text" name="refZipGeoCodeMap.longitude"  size="12"/></td>
						   
							</tr>
						<tr>
						
						<c:set var="ischeckedddd" value="false"/>
				        <c:if test="${refZipGeoCodeMap.primaryRecord =='P'}">
					    <c:set var="ischeckedddd" value="true"/>					    
				        </c:if>
				       <s:hidden name="refZipGeoCodeMap.primaryRecord"/> 
				       <td align="right" colspan="1"  class="listwhitetext">Primary</td>							
							<td align="left" colspan="1"><s:checkbox key="primaryRecord" value="${ischeckedddd}" fieldValue="true" onclick="clickPrimaryRecord()"/></td>
					   		<td align="right" class="listwhitetext" width="50px">Area&nbsp;Code</td>
						<td><s:textfield cssClass="input-text" name="refZipGeoCodeMap.phoneAreaCode"  size="12" maxlength="55"/></td>
						</tr>  
						</table>
						
						</td>
						<td valign="top" style="margin: 0px;padding: 0px;">
						<div id="map" style="width: 590px; height: 200px"></div>
						</td>
						</tr>
						</table>
						</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
						

		   <s:hidden name="refZipGeoCodeMap.corpID"/>				
		<table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:880px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:120px">
							<fmt:formatDate var="refZipFormattedValue" value="${refZipGeoCodeMap.createdOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="refZipGeoCodeMap.createdOn" value="${refZipFormattedValue}"/>
							<fmt:formatDate value="${refZipGeoCodeMap.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						<td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td>
						<c:if test="${not empty refZipGeoCodeMap.id}">
							<s:hidden name="refZipGeoCodeMap.createdBy" />
							<td ><s:label name="createdBy" value="%{refZipGeoCodeMap.createdBy}" /></td>
						</c:if>
						<c:if test="${empty refZipGeoCodeMap.id}">
							<s:hidden name="refZipGeoCodeMap.createdBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<fmt:formatDate var="refZipGeoCodeMapUpdatedOnFormattedValue" value="${refZipGeoCodeMap.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="refZipGeoCodeMap.updatedOn"  value="${refZipGeoCodeMapUpdatedOnFormattedValue}"/>
						<td style="width:130px"><fmt:formatDate value="${refZipGeoCodeMap.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty refZipGeoCodeMap.id}">
							<s:hidden name="refZipGeoCodeMap.updatedBy" />
							<td ><s:label name="updatedBy" value="%{refZipGeoCodeMap.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty refZipGeoCodeMap.id}">
							<s:hidden name="refZipGeoCodeMap.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>	
	</div>
	<div style="padding-left:15px; padding-top:10px;">
	<s:submit cssClass="cssbuttonA" method="save" key="button.save"
		cssStyle="width:55px;" />
		<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="newFunctionForCountryState();"/>
		</div>
</s:form>

<script type="text/javascript">
Form.focusFirstElement($("refZipGeoCodeMapForm")); 
try{
initLoader();
}catch(e){}
try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['refZipGeoCodeMapForm'].elements['refZipGeoCodeMap.country'].value;
	 if(oriCon=="")	 {
			document.getElementById('state').disabled = true;
			document.getElementById('state').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('state').disabled = false; 
				document.getElementById('state').value='${refZipGeoCodeMap.stateabbreviation}';
			}else{
				document.getElementById('state').disabled = true;
				document.getElementById('state').value ="";
			} 
		}
	 }catch(e){}

</script>


