<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="JobsInvoiced" pageWidth="742" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="682" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="4c973f55-e105-4f40-9a3f-3c3cb3fc5bd3">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select "JobsInvoiced-R-PDF-RSKY.jrxml",
	serviceorder.companyDivision AS serviceorder_companyDivision,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.status AS serviceorder_status,
	if((serviceorder.firstName is null || trim(serviceorder.firstName)=""),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))) AS Shipper,
	billing.billToCode AS billing_billToCode,
	billing.billToName AS billing_billToName,
	serviceorder.job AS serviceorder_job,
	trackingstatus.loadA AS trackingstatus_loadA,
	trackingstatus.deliveryA AS trackingstatus_deliveryA,
	sum(if(accountline.actualRevenue is null || accountline.actualRevenue='',0.0,accountline.actualRevenue)) AS invAmount,
	accountline.recRateCurrency as accountline_recRateCurrency,
	notes.subject AS notes_subject
From serviceorder serviceorder
	INNER JOIN accountline accountline ON serviceorder.id = accountline.serviceOrderId AND accountline.corpId = $P{Corporate ID}
	INNER JOIN billing billing ON serviceorder.id = billing.id AND billing.corpId = $P{Corporate ID}
	INNER JOIN trackingstatus trackingstatus ON serviceorder.id = trackingstatus.id AND trackingstatus.corpId = $P{Corporate ID}
	LEFT OUTER JOIN notes notes ON serviceorder.id = notes.notesKeyId AND notes.corpId = $P{Corporate ID}
WHERE serviceorder.corpId = $P{Corporate ID}
	AND (trackingstatus.loadA <= sysdate() or trackingstatus.deliveryA <= sysdate())
	AND accountline.status is true
	AND (notes.noteSubType is null or notes.noteSubType = 'Accounting Issue' ) AND (notes.notestatus<>'CMP' or notes.notestatus is null)
	AND serviceorder.status not in ('CNCL','CANCEL','DWNLD','DWN','HOLD','CLSD')
	AND serviceorder.job!=('DWN')
	AND billing.billComplete is null
        AND (((accountline.actualRevenue <> 0) && accountline.recinvoicenumber="") || ((accountline.actualRevenue = 0) && accountline.recinvoicenumber=""))
GROUP BY accountline.shipNumber,accountline.recRateCurrency
ORDER BY serviceorder.companyDivision,accountline.shipNumber,accountline.recRateCurrency,trackingstatus.loadA desc]]>
	</queryString>
	<field name="JobsInvoiced-R-PDF-RSKY.jrxml" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_status" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="billing_billToCode" class="java.lang.String"/>
	<field name="billing_billToName" class="java.lang.String"/>
	<field name="serviceorder_job" class="java.lang.String"/>
	<field name="trackingstatus_loadA" class="java.sql.Timestamp"/>
	<field name="trackingstatus_deliveryA" class="java.sql.Timestamp"/>
	<field name="invAmount" class="java.math.BigDecimal"/>
	<field name="accountline_recRateCurrency" class="java.lang.String"/>
	<field name="notes_subject" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[$F{serviceorder_companyDivision}]]></groupExpression>
		<groupHeader>
			<band height="20" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="152" height="20" uuid="559204fd-0103-4a81-a6b3-24731fee5238"/>
					<textElement verticalAlignment="Top">
						<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-13" x="0" y="0" width="102" height="20" uuid="f744f76c-3e68-4f11-a2cc-e0c273bb8048"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="Shipnumber">
		<groupExpression><![CDATA[$F{serviceorder_shipNumber}]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="18" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="82" y="0" width="600" height="18" printWhenGroupChanges="Shipnumber" uuid="237e2c08-e922-41ba-ab29-3ba7c9a2555c"/>
					<textElement>
						<font fontName="Arial" size="8" isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{notes_subject}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-11" x="0" y="0" width="82" height="18" printWhenGroupChanges="Shipnumber" uuid="79c4646a-7fe7-48d1-9a38-e31efba85a9c"/>
					<textElement>
						<font fontName="Arial" size="8" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Accounting Notes :]]></text>
				</staticText>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="37" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="5" width="682" height="32" uuid="1fa67c14-8370-496b-bfe0-9e7c59d40a44"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="14" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Jobs Not Invoiced But Loaded Or Delivered]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="29" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="1" y="9" width="87" height="20" uuid="23702973-f8da-4802-9e6a-d8bc98fa12e9"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SO #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="88" y="9" width="58" height="20" uuid="9b982e77-c8d9-4616-9b8e-87fdd8bc720c"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Status]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="146" y="9" width="117" height="20" uuid="fa4d5268-d974-471d-a25e-4b7e85e20313"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shipper]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="263" y="9" width="69" height="20" uuid="4c9984a6-8256-4781-88a0-9d78baa1ca9a"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="332" y="9" width="122" height="20" uuid="4d23aa21-6b32-4bb5-8916-d51cdee8f58b"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="454" y="9" width="40" height="20" uuid="7d3ec6c5-cf53-40b3-a963-c08c40b5586d"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Job]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="494" y="9" width="59" height="20" uuid="87762c28-7877-4154-8555-da0b77816462"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Load Act.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="553" y="9" width="67" height="20" uuid="4d58f695-d4f1-4687-8233-2e1ce9812afe"/>
				<textElement>
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Delivery Act.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="609" y="9" width="73" height="20" uuid="1a06a9b2-00a6-4423-b4f0-818b50f79d7e"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Inv Amt]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="1" y="0" width="87" height="18" uuid="a4b95369-548b-4628-812d-827c88b8b8d6"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="88" y="0" width="58" height="18" uuid="51a4555e-72a7-40dc-a708-080cbd24d2ea"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_status}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="146" y="0" width="117" height="18" uuid="e1938200-a635-4de9-a85a-4a5faa5d0341"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="263" y="0" width="69" height="18" uuid="c8e70feb-af2c-48b5-9be8-8b46db21ca00"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="332" y="0" width="122" height="18" uuid="600e8788-a7d2-40b0-9029-ca8a2eec561a"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToName}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="454" y="0" width="40" height="18" uuid="f5b4bffc-d8a9-4021-bc97-43addcabedee"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_job}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="494" y="0" width="59" height="18" uuid="30bc72da-214a-41b1-8f4c-2145655ea4dd"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_loadA}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField" x="553" y="0" width="56" height="18" uuid="d9511ec2-3f7f-4fa3-9d11-5043f362d888"/>
				<textElement>
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_deliveryA}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true">
				<reportElement key="textField" x="609" y="0" width="73" height="18" uuid="93c74158-3197-45eb-8d39-2bdf39112735"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="9"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{invAmount}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="17" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField-1" x="552" y="3" width="57" height="14" uuid="5e6a6235-f45b-4840-a5fe-1c7f13b0009c"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField-2" x="609" y="3" width="73" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="9fc6de72-b4aa-4cc3-a00c-630b44ba9f4b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Arial" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="dd-MMM-yy" isBlankWhenNull="true">
				<reportElement key="textField-3" x="1" y="3" width="145" height="14" uuid="a14d9177-544f-4d97-94f9-561801c92b68"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-12" x="246" y="3" width="190" height="14" uuid="636fe823-9486-4c6f-a107-33a74b445417"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[JobsInvoiced.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="1" width="678" height="1" uuid="429a1bad-7ac3-4071-ab13-77ad3590f48d"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
