<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title><fmt:message key="accountLineDetail.title" /></title>
<meta name="heading"
	content="<fmt:message key='accountLineDetail.heading'/>" />


<SCRIPT LANGUAGE="JavaScript">

function fillDescr()
{
	var estimateQuantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
	var estimateRate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
	var quoteDescription = document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value;
	if("${accountLine.id}">0)
	{
		if(quoteDescription =="")
		{
			document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value = estimateQuantity+"@"+estimateRate;
		}
	}
	else
	{
		document.forms['accountLineForms'].elements['accountLine.quoteDescription'].value = estimateQuantity+"@"+estimateRate;
	}
}

    function winOpenForActgCode()
  {
 	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
 	//alert(val); 
 	var finalVal = "OK";
 	var indexCheck = val.indexOf('Origin');
 	if(val == 'Origin')
 	{
 		finalVal='Origin';
 		openWindow('originForActgCode.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription');
 	}
 	else if(val == 'Destin')
 	{
 		finalVal='Destin';
 		openWindow('destinationForActgCode.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription');
 	} 
 	else if(val == 'Other')
 	{
 		finalVal='Other';
 		openWindow('originForActgCode.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription');
 	}
 	else
 	{
 	     finalVal='Origin';
         openWindow('originForActgCode.html?origin=${serviceOrder.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode&fld_seventhDescription=seventhDescription');
    }
 	document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
 }


function checkVendorName(){
    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
	if(vendorId == ''){
		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
	}
	if(vendorId != ''){
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    } 
}


function vendorCodeForActgCode(){ 
   <c:if test="${accountInterface=='Y'}">
    var companyDivision = document.forms['accountLineForms'].elements['serviceOrder.companyDivision'].value;
    var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
    if(vendorId==''){
    	document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
		document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
    }
    if(vendorId!=''){
	    var url="vendorNameForActgCode.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(vendorId)+"&companyDivision="+encodeURI(companyDivision);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse222;
	    http2.send(null);
    }
    </c:if>
} 

function handleHttpResponse222(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");  
                if(res.length >= 2){ 
		                if(res[3]=='Approved'){
		                	if(res[2] != "null" && res[2] !=''){
		                		document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value = res[1];
				                document.forms['accountLineForms'].elements['accountLine.actgCode'].value = res[2];
				            }else { 
				                document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value=res[1]; 
								document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
								document.forms['accountLineForms'].elements['accountLine.vendorCode'].select(); 
				            }	
				        }else{
				            alert("Vendor code is not approved" ); 
							document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
							document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
							document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
							document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
				        }     
		            }else{
		                alert("Vendor code not valid" ); 
						document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.actgCode'].value="";
						document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
					} 
       }
}

function disableButton(){
 	var vendorId = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
 	if(vendorId == ''){
 		document.forms['accountLineForms'].elements['commissionBtn'].disabled = true;
 	}else{
 		document.forms['accountLineForms'].elements['commissionBtn'].disabled = false;
 	}
 }
 
function findRadioValue(){
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var quotationContract = document.forms['accountLineForms'].elements['quotationContract'].value;
     var url="invoiceExtracts2.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(quotationContract);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse5;
     http2.send(null);
}

function findRevisedEntitleQuantitys(){
     document.forms['accountLineForms'].elements['accountLine.basis'].value=""; 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var quotationContract = document.forms['accountLineForms'].elements['quotationContract'].value;
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var url="invoiceExtracts3.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(quotationContract)+ "&shipNum=" + encodeURI(shipNumber);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse6;
     http2.send(null);

}
function findRevisedEstimateQuantitys(){
     document.forms['accountLineForms'].elements['accountLine.basis'].value=""; 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var quotationContract = document.forms['accountLineForms'].elements['quotationContract'].value;
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var url="invoiceExtracts4.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(quotationContract)+ "&shipNum=" + encodeURI(shipNumber);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse7;
     http2.send(null);

}

function httpResponseCheckPostDate()
        {
			//alert("a");
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                //alert("Result: "+results);
                if(results == "less"){
                    document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].focus();
                	alert('Posting date cannot be lesser than the Accounting default date');
                	
                }
             }
        }
        
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function handleHttpResponse2(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
		           if(res.size() >= 2){ 
		           		if(res[2] == 'Approved'){
		           			document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value = res[1];
		           			document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
		           		}else{
		           			alert("Vendor code is not approved" ); 
						    document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
						    document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
						    document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
		           		}
                }else{
                     alert("Vendor code not valid" );
                     document.forms['accountLineForms'].elements['accountLine.estimateVendorName'].value="";
				 	 document.forms['accountLineForms'].elements['accountLine.vendorCode'].value="";
				 	 document.forms['accountLineForms'].elements['accountLine.vendorCode'].select();
			   }
     }
}


function handleHttpResponse5()
     {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 results = results.trim();   
                 var res = results.split("#"); 
                 var chargeCode = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
                 var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
                 
                if((rev == "Revenue") && (chargeCode !=""))
                {  
                	if((res[2] != "")&&(res[2] != undefined)&&(res[1] !=undefined) && (res[1] !=""))
                	{  
                  		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=false;
                	}
                	else
                	{
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                	}
                	
                	if((res[1] != "" )&&(res[1] != undefined)&&(res[3] !=undefined) && (res[3] != ""))
                	{
                  		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=false;
                	}
                	else
                	{
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true;
                	}
                }   
                else
                	{ 
                		document.forms['accountLineForms'].elements['accountLine.compute1'].disabled=true;
                		document.forms['accountLineForms'].elements['accountLine.compute2'].disabled=true; 
                	}         
    }
}

function handleHttpResponse7()
     {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 results = results.trim(); 
                 var res = results.split("#");
              if(res[3]=="AskUser1"){
              var reply = prompt("Please Enter Quantity", "");
              if((reply>0 || reply<9)|| reply=='.')
              {
              	document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = reply;
              	}
              	else
              	{
              		alert("Please Enter legitimate Quantity");
              	}
              
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value = res[1];
              }
              if(res[5]=="AskUser"){
              var reply = prompt("Please Enter the value of Rate", "");
              if((reply>0 || reply<9)|| reply=='.')
              {
              	document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value = reply;
              	}
              	else
              	{
              		alert("Please Enter legitimate Rate");
              	}
              
              }else{
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value = res[4];
              }
              if(res[5]=="BuildFormula")
              {
               document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value = res[6];
              }
             var Q1 = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
             var Q2 = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
             var E1=Q1*Q2;
             var E2="";
             E2=Math.round(E1*100)/100;
             document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
             var type = res[8];
               var typeConversion=res[9];
               if(type == 'Divison')
               {
               	 if(typeConversion==0)
               	 {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E1;
               	 }
               	 else
               	 {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               	 }
               }
               if(type == ' ' || type == '')
               {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               }
               if(type == 'Multiply')
               {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = E2;
               }
       }
    }


function handleHttpResponse6()
     {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 results = results.trim(); 
              var res = results.split("#"); 
              if(res[3]=="AskUser0"){
              var reply = prompt("Please Enter Entitlement Amount", "");
              if((reply>0 || reply<9)|| reply=='.')
              {
              	document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = reply;
              	}
              	else
              	{
              		alert("Please Enter legitimate Quantity");
              	}
              
              }
              if(res[3]=="Preset0"){
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = res[0];
              }
             if(res[3]=="FromField0"){
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = res[1];
              }
              if(res[5]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if((reply>0 || reply<9)|| reply=='.')
              {
              	document.forms['accountLineForms'].elements['rateEstimate'].value = reply;
              	}
              	else
              	{
              		alert("Please Enter legitimate Rate");
              	}
                
                }
                else{
                document.forms['accountLineForms'].elements['rateEstimate'].value = res[7];
                }
                if(res[5]=="RateGrid"){
                document.forms['accountLineForms'].elements['rateEstimate'].value = res[8];
                }
               var Q1 = document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
               var Q2 = document.forms['accountLineForms'].elements['rateEstimate'].value;
               var E1=Q1*Q2;
               var E2="";
               E2=Math.round(E1*100)/100;
               document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value=E2;
               var type = res[4];
               var typeConversion=res[6];
               if(type == 'Divison')
               {
               	 if(typeConversion==0)
               	 {
               	   E1=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E1;
               	 }
               	 else
               	 {	
               		E1=(Q1*Q2)/typeConversion;
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               	 }
               }
               if(type == ' ' || type == '')
               {
               		E1=(Q1*Q2);
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               }
               if(type == 'Multiply')
               {
               		E1=(Q1*Q2*typeConversion);
               		E2=Math.round(E1*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value = E2;
               }
               
       }
    }
    
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
 function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    
  function getHTTPObject2()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http4 = getHTTPObject2();
    
</script>


<script type="text/javascript">
function expense()
     {
       var estimate=0;
       var estimateOther=0;
       var estimateOtherRev=0;
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
 	   var estimatePassPercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
 	   	
  if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt")
  {
      //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCwt'].value;
        var Q1=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
        var Q2=0;
        Q2=Math.round(Q1*1000)/1000;
        document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value= Q2;
     
     }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="kg")
  {
     //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseKg'].value;
     var Q5=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q6=0;
     Q6=Math.round(Q5*1000)/1000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=Q6;
      
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cft")
  {
     //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCft'].value;
     var Q9=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q10=0;
     Q10=Math.round(Q9*1000)/1000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=Q10;
      
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cbm")
  {
     //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseCbm'].value;
     var Q13=document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
     var Q14=0;
     Q14=Math.round(Q13*1000)/1000;
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value= Q14;
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="each")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
    
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="value")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=1;
     
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="flat")
  {
     //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
     
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="hour")
  {
     //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseHour'].value;
     
  } 
   else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")
   {
    //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
   
   }
   else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")
   {
    //document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=document.forms['accountLineForms'].elements['totalEstimateExpenseFlat'].value;
   
   }
  else
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value=0*1;
   
  }
 	expenseOld(); 
  	return estimate;
}        
   
   
   function mockupForRevenue() {
   	   var estimate=0;
       var estimateround=0;
       var sellRateround=0;
       var estimatepasspercentageRound=0;
       var	estimatepasspercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	   var estimateRevenue = document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value; 
   	   estimate = (quantity*rate);
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		  estimate = estimate/100;
       		  sellRate=(estimateRevenue/quantity)*100;
       		  sellRateround=Math.round(sellRate);
       		  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       		  estimate = estimate/1000;
       		  sellRate=(estimateRevenue/quantity)*1000;
       		  sellRateround=Math.round(sellRate);
       		  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
       	else if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age"|| document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000")
  	    {
  	        sellRate=estimateRevenue/quantity;
       	    sellRateround=Math.round(sellRate);
  	        document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;
  	    }
  	   estimatepasspercentage = (estimateRevenue/estimate)*100;
  	   estimatepasspercentageRound=Math.round(estimatepasspercentage);
  	   if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	   }else{
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=estimatepasspercentageRound;
  	   }  
  	 }   
	
	 function calculateRevenue() {
   	    var estimate=0;
   	    var sellRateround=0;
        var estimateround=0;
        var estimateRevenue=0;
        var estimateRevenueRound=0;
       var	estimatepasspercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
   	   estimate = (quantity*rate); 
   	   	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       		estimate = estimate/100; 	  	
       	}
       	if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       		estimate = estimate/1000; 	  	
       	}
       	estimateRevenue=((estimate)*estimatepasspercentage/100)
       	estimateRevenueRound=Math.round(estimateRevenue*100)/100;
  	    document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value=estimateRevenueRound;
  	     
	    if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age"){
       	  sellRate=(estimateRevenue/quantity)*100;
       	  sellRateround=Math.round(sellRate);
       	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
       	 if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000"){
       	  sellRate=(estimateRevenue/quantity)*1000;
       	  sellRateround=Math.round(sellRate);
       	  document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
       	}
	    else if(document.forms['accountLineForms'].elements['accountLine.basis'].value!="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value!="%age"|| document.forms['accountLineForms'].elements['accountLine.basis'].value!="per 1000")
        {
          sellRate=(estimateRevenue/quantity);
       	  sellRateround=Math.round(sellRate);
          document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value=sellRateround;	  	
	    }   
 }
    function expenseOld()
     {
       var estimate=0;
       var estimateSell=0;
       var estimateround=0;
       var estimateSellRound=0;
       var estimateOther=0;
       var passPercentage=0;
       var passPercentageround=0;
       var quantity = document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value; 
       var rate = document.forms['accountLineForms'].elements['accountLine.estimateRate'].value;
       var sellRate = document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value;
   	   var estimatePassPercentage = document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value;
   	  
   	   estimate = (quantity*rate);
  	   estimateSell=(quantity*sellRate)
  	 	
       if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt" || document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")
       {
      	   estimate = estimate/100;
      	   estimateSell=estimateSell/100; 	  	
       } 
       if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")
       {
      	   estimate = estimate/1000;
      	   estimateSell=estimateSell/1000; 	  	
       }
     estimateOther = ((estimate)*estimatePassPercentage/100);
     estimateround=Math.round(estimate*100)/100;
     estimateOtherRev=Math.round(estimateOther*100)/100;
     estimateSellRound=Math.round(estimateSell*100)/100;
     passPercentage=((estimateSell/estimate)*100);
     passPercentageround=Math.round(passPercentage);
  	 document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value = estimateround;
  	 document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value = estimateSellRound;
  	 document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value=passPercentageround; 	 
  if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cwt")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
    
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="kg")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
      }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cft")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
          
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="cbm")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
         
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="each")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
           
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="flat")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
        }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="per 1000")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
        
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="value")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
         
  }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="%age")
  {
     document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
       }
  else if( document.forms['accountLineForms'].elements['accountLine.basis'].value=="hour")
  {
    document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value;
          
  }
    
 if(document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == '' || document.forms['accountLineForms'].elements['accountLine.estimateExpense'].value == 0){
  	   	document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].value='';
  	   }
  	   
  	return estimate;		
}        
   
   


function validNumberEntitle()
{
   var  temp=document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
   if(temp<9999999999)
   {
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value;
   }
   else
   { 
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value=0*1;
     document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value.focus();
   }
}
</script>
<script type="text/javascript">

function chk()
{
	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
	var quotationContract = document.forms['accountLineForms'].elements['quotationContract'].value;
	if(document.forms['accountLineForms'].elements['accountLine.recGl'] == undefined && document.forms['accountLineForms'].elements['accountLine.payGl'] == undefined){
		recGlVal = 'secondDescription';
		payGlVal = 'thirdDescription';
	}else{
		recGlVal = 'accountLine.recGl';
		payGlVal = 'accountLine.payGl';
	}
	if(val=='Internal Cost'){
		quotationContract="Internal Cost Management";
		javascript:openWindow('chargess.html?contract='+quotationContract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code=accountLine.chargeCode');
	    document.forms['accountLineForms'].elements['accountLine.chargeCode'].select();
	}
	else {
	if(quotationContract=='' || quotationContract==' ')
	{
		alert("There is no pricing contract in Quotation File  Billing Details section: Please select.");
	}
	else{
		javascript:openWindow('chargess.html?contract='+quotationContract+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription='+recGlVal+'&fld_description=firstDescription&fld_thirdDescription='+payGlVal+'&fld_code=accountLine.chargeCode');
	}
	
}
document.forms['accountLineForms'].elements['accountLine.chargeCode'].focus();
}
</script>


<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==67) || (keyCode==86) ; 
	}
	function onlyNumberAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39); 
	} 
	
	function onlyFloatNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110)|| (keyCode==222); 
	  
	}	
	function onlyRateAllowed(evt)
	{
	
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
	}
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
</script>


<script type="text/javascript">
function notExists(){
	alert("The Account Line information has not been saved yet.");
}
</script>
 
<script type="text/javascript">
function setCategoryType()
 {
 	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
 	var finalVal = "OK";
 	// var indexCheck = val.indexOf('Agent');
 	  // if(indexCheck>0)
 	// {
 		finalVal='Isagent';
 		//alert(val);
 	// }
 	//alert(finalVal);
 }
 
 function winOpen()
 {
 	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
 	var finalVal = "OK";
 	var indexCheck = val.indexOf('Origin Agent');
 	if(val == 'Origin')
 	{
 		finalVal='Origin';
 		openWindow('originPartners.html?origin=${serviceOrder.originCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	else if(val == 'Destin')
 	{
 		finalVal='Destin';
 		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	/*else if(val == 'Broker')
 	{
 		finalVal='Broker';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	 	
 	else if(val == 'Setoff Agent')
 	{
 		finalVal='Setoff Agent';
 		openWindow('partnersPopup.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	 else if(val == 'Pickup Agent')
 	{
 		finalVal='Pickup Agent';
 		openWindow('originPartners.html?origin=${serviceOrder.originCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	
 	else if(val == 'Freight')
 	{
 		finalVal='Freight';
 		openWindow('carrierPartners.html?&partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	else if(val == 'Delivery Agent')
 	{
 		finalVal='Delivery Agent';
 		openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	
 	else if(val == 'Freight Forwarder')
 	{
 		finalVal='Freight Forwarder';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	else if(val == 'Insurance')
 	{
 		finalVal='Insurance';
 		openWindow('brokerPartners.html?&partnerType=VN&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}*/
 	else if(val == 'Other')
 	{
 		finalVal='Other';
 		openWindow('originPartners.html?origin=${serviceOrder.originCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
 	}
 	else
 	{
 	     finalVal='Origin';
         openWindow('originPartners.html?origin=${serviceOrder.originCountry}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=accountLine.estimateVendorName&fld_code=accountLine.vendorCode');
    }
 	
 }
 
 function activeStatusCheck(target)
 {
 
	 //alert(document.forms['accountLineForms'].elements['accountLine.status'].checked);
	 var receivableAmount = eval(document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value);
	 var payableAmount = eval(document.forms['accountLineForms'].elements['accountLine.actualExpense'].value);
	 if(receivableAmount!='0'||payableAmount!='0')
	    {
	    if(document.forms['accountLineForms'].elements['accountLine.status'].checked != true)
	    	{
	    		alert("Receivable Detail Revenue or Payable Detail Expense is not zero");
	    		return false;
	    	}
	    }
 }
 
 
 	
</script>
<script type="text/javascript">
function saveAuto(clickType){
	progressBarAutoSave('1'); 

	if ('${autoSavePrompt}' == 'No'){
	
		var noSaveAction;
		var id1 = document.forms['accountLineForms'].elements['serviceOrder.id'].value;
	    var jobNumber = document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value; 
		
		if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			 noSaveAction = 'editQuotationServiceOrderUpdate.html?id='+id1;
		}
		if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.accounting'){
			 noSaveAction = 'quotationAccountLineList.html?sid='+id1;
		}
		if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.quotation'){
			 noSaveAction = 'editRemoteQuotationFile.html?id='+id1;
		}
		processAutoSave(document.forms['accountLineForms'],'saveQuotationAccountLine!saveOnTabChange.html', noSaveAction );;
	
	}
	else{
     if(!(clickType == 'save')){
	   var id1 = document.forms['accountLineForms'].elements['serviceOrder.id'].value;
	   var jobNumber = document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value; 
	   if (document.forms['accountLineForms'].elements['formStatus'].value == '1'){
	       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the 'Quotation Account Line'");
		       if(agree){
		           document.forms['accountLineForms'].action = 'saveQuotationAccountLine!saveOnTabChange.html';
		           document.forms['accountLineForms'].submit();
		       }else{
		           if(id1 != ''){
		           	   if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			               location.href = 'editQuotationServiceOrderUpdate.html?id='+id1;
			               }
			           if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.accounting'){
			               location.href = 'quotationAccountLineList.html?sid='+id1;
			               }
		           	   if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.quotation'){
			               location.href = 'editRemoteQuotationFile.html?id='+id1;
			               }
      				 }
       			}
   			}else{
			   if(id1 != ''){
			   	   	   if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			               location.href = 'editQuotationServiceOrderUpdate.html?id='+id1;
			               }
			           if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.accounting'){
			               location.href = 'quotationAccountLineList.html?sid='+id1;
			               }
		           	   if(document.forms['accountLineForms'].elements['gotoPageString'].value == 'gototab.quotation'){
			               location.href = 'editRemoteQuotationFile.html?id='+id1;
			               }
   				}
   			}
		}
		
	}
  }
function changeStatus()
{
   document.forms['accountLineForms'].elements['formStatus'].value = '1';
}



</script>

<script type="text/javascript">
  function chkSelect(a)
     {
     		if(a == '2')
     		{
     			document.forms['accountLineForms'].elements['accountLine.estimatePassPercentage'].select(); 	
     		}
     		
        	if (checkFloatWithArth('accountLineForms','accountLine.estimateQuantity','Invalid data in Quantity Estimate Detail') == false)
           {
              document.forms['accountLineForms'].elements['accountLine.estimateQuantity'].value='0';
              
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estimateRate','Invalid data in Buy Rate Estimate Detail') == false)
           {
              document.forms['accountLineForms'].elements['accountLine.estimateRate'].value='0'; 
             
              return false
           }
           if (checkFloatWithArth('accountLineForms','accountLine.estimateSellRate','Invalid data in Sell Rate Estimate Detail') == false)
           {  
              document.forms['accountLineForms'].elements['accountLine.estimateSellRate'].value='0';
            
              return false
           }
          
           if (checkFloatWithArth('accountLineForms','accountLine.estimateRevenueAmount','Invalid data in Revenue Estimate Detail') == false)
           {
              document.forms['accountLineForms'].elements['accountLine.estimateRevenueAmount'].value='0';
              
              return false
           }
         
           if (checkFloatWithArth('accountLineForms','accountLine.entitlementAmount','Invalid data in Expense Entitlement Detail ') == false)
           {
              document.forms['accountLineForms'].elements['accountLine.entitlementAmount'].value='0';
              
              return false
           }
           if (checkNumber('accountLineForms','accountLine.accountLineNumber','Invalid data in Line # ') == false)
           {
               document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='0'; 
               return false 
           }
           checkAccountLineNumber();
 }
 
 
 
 function checkChargeCode()
{
	var val = document.forms['accountLineForms'].elements['accountLine.category'].value;
	var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
	var billingContract = document.forms['accountLineForms'].elements['billingContract'].value; 
	if(val=='Internal Cost'){
	 billingContract="Internal Cost Management";
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+billingContract+"&chargeCode="+chargeCode; 
	 http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse112;
     http5.send(null);
	}
	else{
	if(billingContract=='' || billingContract==' ')
	{
	alert("There is no pricing contract in billing: Please select.");
	document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
	}
	else{
	 var url="checkChargeCode.html?ajax=1&decorator=simple&popup=true&contract="+billingContract+"&chargeCode="+chargeCode; 
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse112;
     http5.send(null);
	}
	}
}
function handleHttpResponse112()
        {

             if (http5.readyState == 4)
             {
                var results = http5.responseText
                results = results.trim();
                var res = results.split("#");  
                if(results.length>4)
                {
                document.forms['accountLineForms'].elements['accountLine.recGl'].value = res[1];
                document.forms['accountLineForms'].elements['accountLine.payGl'].value = res[2];
                	//alert("good charge code" );
                 }
                 else
                 {
                    alert("Charge code does not exist according the billing contract, Please select another" );
                    document.forms['accountLineForms'].elements['accountLine.chargeCode'].value = "";
                    document.forms['accountLineForms'].elements['accountLine.recGl'].value = "";
                    document.forms['accountLineForms'].elements['accountLine.payGl'].value = "";
				 
			   }
             }
        }
        
        function fillRevQtyLocalAmounts(){ 
     var charge = document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
     var billingContract = document.forms['accountLineForms'].elements['billingContract'].value;
     var shipNumber=document.forms['accountLineForms'].elements['serviceOrder.shipNumber'].value;
     var rev = document.forms['accountLineForms'].elements['accountLine.category'].value;
     var comptetive = document.forms['accountLineForms'].elements['inComptetive'].value;
     charge=charge.trim(); 
     if(charge!="")
     {
     if(rev == "Internal Cost"&&charge!="SALESCM"){  
     billingContract="Internal Cost Management";
     var url="invoiceExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber) ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse301;
	     http2.send(null);
	     }
	}
 }
 
 
  function handleHttpResponse301()
     {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                 results = results.trim();
                if(results !=''){
                var res = results.split("#");
                if(res[9]=="AskUser2")
                {
                var reply = prompt("Please Enter Quantity", "");
                if((reply>0 || reply<9)|| reply=='.')
               {
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = reply;
               }
              	else
              	{
              		alert("Please Enter legitimate Quantity");
              	}
                
                }
                else{                                            
                document.forms['accountLineForms'].elements['localAmountPaybleHid'].value = res[7];
                    }
               if(res[10]=="AskUser"){
                var reply = prompt("Please Enter Rate", "");
                if((reply>0 || reply<9)|| reply=='.')
                {
              	document.forms['accountLineForms'].elements['ratePaybleHid'].value = reply;
                
              	}
              	else
              	{
              		alert("Please Enter legitimate Rate");
              	}
                
                }
                else{                                            
                document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[8];
                
               }
               if(res[10]=="BuildFormula"){
               document.forms['accountLineForms'].elements['ratePaybleHid'].value = res[11];
               
               }
               
	           var Q2 = document.forms['accountLineForms'].elements['localAmountPaybleHid'].value;
               var Q1 = document.forms['accountLineForms'].elements['ratePaybleHid'].value;
               var E1="";
               if(Q1==0 && Q2==0)
               {
               E1=0*1;
               }
               else if(Q2==0)
               {
               E1=0*1;
               }
               else
               {
               E1=Q1*Q2;               
               document.forms['accountLineForms'].elements['accountLine.localAmount'].value = E1;
               document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E1;
               }
               var Q5 = res[5];
                                           
               var type = res[3];
               var E2= "";
               var E3= ""; 
               if(type == 'Division')
               {
               	 if(Q5==0)
               	 {
               	   E2=0*1;
               	   document.forms['accountLineForms'].elements['accountLine.localAmount'].value = E2;
               	   document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E2;
               	   
               	 }
               	 else
               	 {	
               		E2=(E1)/Q5;
               		E3=Math.round(E2*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.localAmount'].value = E3;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               		
               	 }
               }
               if(type == ' ' || type == '')
               {
               		E2=(E1);
               		E3=Math.round(E2*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.localAmount'].value = E3;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               		
               }
               if(type == 'Multiply')
               {
               		E2=(E1)*Q5;
               		E3=Math.round(E2*100)/100;
               		document.forms['accountLineForms'].elements['accountLine.localAmount'].value = E3;
               		document.forms['accountLineForms'].elements['accountLine.actualExpense'].value = E3;
               		
               }
              document.forms['accountLineForms'].elements['accountLine.exchangeRate'].value =1;
              document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value ="INT";
              document.forms['accountLineForms'].elements['accountLine.payingStatus'].options[2].selected=true;
              //document.forms['accountLineForms'].elements['accountLine.note'].value = res[13];
        var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(document.forms['accountLineForms'].elements['accountLine.invoiceNumber'].value!='')
		{
		   document.forms['accountLineForms'].elements['accountLine.receivedDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.valueDate'].value=datam;
		   document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value=datam;
	     }
	    else
	    {
	     document.forms['accountLineForms'].elements['accountLine.valueDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.receivedDate'].value='';
	     document.forms['accountLineForms'].elements['accountLine.invoiceDate'].value='';
	    } 
		
      }
      }
    }
    
 function checkAccountLineNumber()
     {
    
       var accountLineNumber=eval(document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value); 
       var accountLineNumber1=document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       if(accountLineNumber>100)
       {
         alert("Manually you can not Enter Line # greater than 100");
         document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value=0;
       }
       else if(accountLineNumber1.length<2)
       {
       	//alert("10");
       	document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='00'+document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       }
       else if(accountLineNumber1.length==2)
       {
       	//alert("Asish"+accountLineNumber);
       	var aa = document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value;
       	document.forms['accountLineForms'].elements['accountLine.accountLineNumber'].value='0'+aa;
       }
     }  
 </script> 
</head>
<body>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="accountLineForms" name="accountLineForms" action="saveQuotationAccountLine" method="post" validate="true" onsubmit="return submit_form()">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	
	<s:hidden name="accountLine.id" value="%{accountLine.id}" />
	<s:hidden name="id" value="<%=request.getParameter("id")%>" />
	<s:hidden name="sid" value="<%=request.getParameter("sid")%>" />
    
	<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}" />
	<s:hidden name="accountLine.serviceOrderId" value="%{serviceOrder.id}"/>
	<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" />
	<s:hidden name="serviceOrder.sequenceNumber" />
	<s:hidden name="serviceOrder.ship" /> 
	<s:hidden name="serviceOrder.companyDivision" /> 
	<s:hidden name="accountLine.ignoreForBilling" />
	<s:hidden name="rateEstimate" /> 
	<c:set var="billingContract" value="${accountLine.contract}"/>
	<s:hidden name="billingContract" value="${accountLine.contract}"/> 
	<s:hidden name="quotationContract" /> 
	
	    <s:hidden id="countCategoryTypeDetailNotes" name="countCategoryTypeDetailNotes" value="<%=request.getParameter("countCategoryTypeDetailNotes") %>"/>
		<s:hidden id="countEstimateDetailNotes" name="countEstimateDetailNotes" value="<%=request.getParameter("countEstimateDetailNotes") %>"/>
		<s:hidden id="countPayableDetailNotes" name="countPayableDetailNotes" value="<%=request.getParameter("countPayableDetailNotes") %>"/>
		<s:hidden id="countRevesionDetailNotes" name="countRevesionDetailNotes" value="<%=request.getParameter("countRevesionDetailNotes") %>"/>
		<s:hidden id="countEntitleDetailOrderNotes" name="countEntitleDetailOrderNotes" value="<%=request.getParameter("countEntitleDetailOrderNotes") %>"/>
		<s:hidden id="countReceivableDetailNotes" name="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>"/>
		
		<c:set var="countCategoryTypeDetailNotes" value="<%=request.getParameter("countCategoryTypeDetailNotes") %>" />
		<c:set var="countEstimateDetailNotes" value="<%=request.getParameter("countEstimateDetailNotes") %>" />
		<c:set var="countPayableDetailNotes" value="<%=request.getParameter("countPayableDetailNotes") %>" />
		<c:set var="countRevesionDetailNotes" value="<%=request.getParameter("countRevesionDetailNotes") %>" />
	    <c:set var="countEntitleDetailOrderNotes" value="<%=request.getParameter("countEntitleDetailOrderNotes") %>" />
		<c:set var="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>" />
	
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
	<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	   <c:redirect url="/editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accounting' }">
	   <c:redirect url="/quotationAccountLineList.html?sid=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.quotation' }">
	   <c:redirect url="/editRemoteQuotationFile.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:otherwise>
	</c:otherwise>
</c:choose> 
</c:if>	
	<div id="newmnav">
		<ul>
			<li><a onclick="setReturnString('gototab.serviceorder');return saveAuto('none');"onmouseover="return chkSelect('2');"  /><span>S/O Details</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.accounting');return saveAuto('none');" onmouseover="return chkSelect('2');" class="current"><span>Accounting<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a onclick="setReturnString('gototab.quotation');return saveAuto('none');" onmouseover="return chkSelect('2');"><span>Quotation File</span></a></li>
			<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quote&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li> 
			
			<li><a onclick="openWindow('auditList.html?id=${accountLine.id}&tableName=accountline&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>	
		</ul>	
	</div>
	<div class="spn" >&nbsp;</div>

	
<div id="Layer1" style="width:100%" onkeydown="changeStatus();">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
	<table class="" style="width:90%" cellspacing="0" cellpadding="0" border="0">
		<tbody>
			<tr>
				<td align="left" class="listwhitebox" style="width:100%">
				<table class="detailTabLabel" border="0" style="width:95%">
					<tbody>
						<tr>
							<td align="left" height="5px"></td>
						</tr>
						<tr>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.shipper" /></td>
							<td align="left" colspan="2"><s:textfield
								name="serviceOrder.firstName"  size="21"
								cssClass="input-textUpper" readonly="true" /></td>
							<td align="left"><s:textfield name="serviceOrder.lastName"
								cssClass="input-textUpper" size="11" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.originCountry" /></td>
							<td align="left"><s:textfield
								name="serviceOrder.originCityCode" cssClass="input-textUpper"
								size="15" readonly="true" /></td>
							<td align="left"><s:textfield
								name="serviceOrder.originCountryCode" cssClass="input-textUpper"
								size="4" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.Type" /></td>
							<td align="left"><s:textfield name="serviceOrder.Job"
								cssClass="input-textUpper" size="6" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.commodity" /></td>
							<td align="left"><s:textfield name="serviceOrder.commodity"
								cssClass="input-textUpper" size="6" readonly="true" /></td>
							<td align="left" ><fmt:message
								key="billing.routing" /></td>
							<td align="left"><s:textfield name="serviceOrder.routing"
								cssClass="input-textUpper" size="6" readonly="true" /></td>
						</tr>
						<tr>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.jobNo" /></td>
							<td align="left"><s:textfield name="customerFileNumber"
								cssClass="input-textUpper"
								value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"
								size="15" readonly="true" /></td>
							<td align="right"><fmt:message key="billing.registrationNo" /></td>
							<td align="left"><s:textfield
								name="serviceOrder.registrationNumber"
								cssClass="input-textUpper" size="11" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.destination" /></td>
							<td align="left"><s:textfield
								name="serviceOrder.destinationCityCode"
								cssClass="input-textUpper" size="15" readonly="true" /></td>
							<td align="left"><s:textfield
								name="serviceOrder.destinationCountryCode"
								cssClass="input-textUpper" size="4" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.mode" /></td>
							<td align="left"><s:textfield name="serviceOrder.mode"
								cssClass="input-textUpper" size="6" readonly="true" /></td>
							<td align="right">&nbsp;&nbsp;<fmt:message
								key="billing.AccName" /></td>
							<td align="left" colspan="3"><s:textfield
								name="serviceOrder.billToName" cssClass="input-textUpper"
								size="27" readonly="true" /></td>
						</tr>
						<tr>
							<td align="left" height="5px"></td>
						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Line Details</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:0px;"><span></span></div>
   <div class="center-content">  
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%" >
		<tbody>
			<tr>
				<td >
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr>
								<tbody>
								<tr> <td height="10px"></td></tr>
									<tr>
										<td align="right" class="listwhitetext" width="100px"><fmt:message
											key="accountLine.categoryType" /><font color="red" size="2">*</font></td>
										<td align="left"  style="padding-left:2px"><s:select
											list="%{category}" cssClass="list-menu" cssStyle="width:135px" headerKey=" " headerValue=" "
											name="accountLine.category" onchange="changeStatus(),setCategoryType(),findRadioValue();"/></td>
											<td width="15px"></td>
											<td align="right" class="listwhitebox" valign="middle"><fmt:message key='accountLine.status'/></td>
										<td align="left"><s:checkbox name="accountLine.status" value="${accountLine.status}" fieldValue="true" disabled="disabled" onclick="return activeStatusCheck(this)"onchange="changeStatus()"/></td>
										<td width="115px"></td>
										<td align="right" class="listwhitebox" valign="middle"><fmt:message key='accountLine.accountLineNumberDetail'/></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.accountLineNumber" readonly="false" size="6" maxlength="3" onkeydown="return onlyNumberAllowed(event);" onblur="chkSelect('1');" onchange="checkAccountLineNumber();"/></td>
										</tr>
										</tbody>
										</table>
										<table class="detailTabLabel" border="0" >
										</tr>
										</tbody>
										<tbody>
										<tr>
										<tbody>
										<tr>
										<c:if test="${accountInterface!='Y'}">
										<td align="right" class="listwhitetext" width="95"><fmt:message key="accountLine.vendorCode" /></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorName()"/></td>
									    <td align="left"width="50"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                        </c:if>
                                        <c:if test="${accountInterface=='Y'}">
                                        <td align="right" class="listwhitetext" width="95"><fmt:message key="accountLine.vendorCode" /></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.vendorCode" readonly="false" size="5" maxlength="10" onchange="vendorCodeForActgCode();disableButton();"/></td>
									    <td align="left"width="50"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenForActgCode();" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                        </c:if>
                                       
                                        <td align="right" class="listwhitetext" width="70"><fmt:message key="accountLine.estimateVendorName" /></td>
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="accountLine.estimateVendorName" readonly="true" size="38" /></td>
										<c:if test="${accountInterface=='Y'}">
										<td align="right" class="listwhitetext"><fmt:message key="accountLine.actgCode" /></td>
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="accountLine.actgCode" readonly="true" size="8" /></td>
										</c:if>
										<!--<td align="right" class="listwhitetext" width="105"><fmt:message key="accountLine.vendorCode" /></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.vendorCode" readonly="false" size="5" maxlength="10" onchange="checkVendorName()"/></td>
									    <td align="left"width="70"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                        <td width="17px"></td>
                                        <td align="right" class="listwhitetext"><fmt:message key="accountLine.estimateVendorName" /></td>
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="accountLine.estimateVendorName" readonly="true" size="38" /></td>
										<td width="12px"></td>
										<td align="right" class="listwhitetext"><fmt:message key="accountLine.actgCode" /></td>
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" key="accountLine.actgCode"  size="8" /></td>
										--><td align="right" class="listwhitetext"  width="77px"><fmt:message key="accountLine.chargeCode"  /></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.chargeCode" readonly="true" size="5" maxlength="10" onblur="findRadioValue();" onchange="checkChargeCode();" onselect="fillRevQtyLocalAmounts();"/></td>
										<td align="left"><img class="openpopup" width="17" height="20" onclick="document.forms['accountLineForms'].elements['accountLine.chargeCode'].focus();chk();" onselect="fillRevQtyLocalAmounts()" src="<c:url value='/images/open-popup.gif'/>" /></td>
									
									</tr>
								</tbody>
							</table>
						<tr>
	                     <td height="10" align="left" class="listwhitetext">
	                     
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Costing Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	
		               
						<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
  <tbody>
	 								<tr>
												<td colspan="10">
												<table class="detailTabLabel" cellpadding ="0" cellspacing="0" border="0">
												<tbody>
												<tr>
													<td class="subcontenttabChild" width="300px" height="30px" align="left" colspan="15"><fmt:message key="accountLine.EntitlementDetail" /></td>
													<td class="subcontenttabChild" width="510px"><input type="button" value="Compute" style="width:65px" name="accountLine.compute1" onclick="findRevisedEntitleQuantitys();"></td>
													
												<td class="subcontenttabChild"width="70px"align="center"><font color="black" size="2">Expense</font></td>
												<td class="subcontenttabChild"width="45px"align="center"><font color="black" size="2">Pass%</font></td>
												<td class="subcontenttabChild"width="75px"align="center"><font color="black" size="2">Revenue</font></td>
												</tr></tbody></table>
												<table class="detailTabLabel" border="0">
												<tbody> 
												<tr>
													<td align="right" class="listwhitetext"></td>
													<td align="left" class="listwhitetext"></td>
													<td align="left"></td>
													<td align="right" class="listwhitetext"></td>
													<td align="left" class="listwhitetext"></td>
													<td width="75px"></td>
													<td width="75px"></td>
													<td width="75px"></td>
													<td width="100px"></td>
													<td valign="middle" rowspan="4" class="vertlinedata_vert"></td>
													<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.entitlementAmount" size="7" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"onmouseover="validNumberEntitle();"/></td>
													<td valign="middle" rowspan="4"  class="vertlinedata_vert"></td>
													<td></td>
													<td valign="middle" rowspan="4" class="vertlinedata_vert"></td>
													<c:if test="${empty accountLine.id}">
													<td width="80px" align="right" ></td>
													</c:if>
													<c:if test="${not empty accountLine.id}">
														<c:choose>
															<c:when test="${countEntitleDetailOrderNotes == '0' || countEntitleDetailOrderNotes == '' || countEntitleDetailOrderNotes == null}">
															<td width="80px" align="right"><img id="countEntitleDetailOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',740,400);" ></a></td>
															</c:when>
															<c:otherwise>
															<td width="80px" align="right"><img id="countEntitleDetailOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',740,400);" ></a></td>
															</c:otherwise>
														</c:choose> 
														</c:if>
													</tr>
													
												<tr>
												<td class="subcontenttabChild" width="700px" height="30px" align="left" colspan="15"><fmt:message key="accountLine.EstimateDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Compute" style="width:65px" name="accountLine.compute2"  onclick="findRevisedEstimateQuantitys();"></td>
												</tr>
												<tr>
												
																<!--<td align="right" width="150" class="listwhitetext"><fmt:message
																	key="accountLine.reference" /></td>
																<td align="left" class="listwhitetext"><s:textfield
																	cssClass="input-text" key="accountLine.reference"
																	size="20" maxlength="20"/></td>
																	
																-->
																<td align="left"></td>
																<td align="left"></td>
																<td align="left"></td>
																<td align="right" class="listwhitetext"></td>
																<td align="left" style="width:75px"class="listwhitetext"></td>

																<td align="right" class="listwhitetext"></td>
																<td align="left" class="listwhitetext"></td>
																<td></td>
																<td></td>	
																<td></td>
																<td></td>
																<td align="right">
																    <c:if test="${empty accountLine.id}">
																	  <img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/>
																    </c:if>
																	<c:if test="${not empty accountLine.id}">
																	<c:choose>
																	<c:when test="${countEstimateDetailNotes == '0' || countEstimateDetailNotes == '' || countEstimateDetailNotes == null}">
																	<img id="countEstimateDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EstimateDetail&imageId=countEstimateDetailNotesImage&fieldId=countEstimateDetailNotesNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',740,400);" ></a>
																	</c:when>
																	<c:otherwise>
																	<img id="countEstimateDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EstimateDetail&imageId=countEstimateDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',740,400);" ></a>
																	</c:otherwise>
																</c:choose> 
																</c:if>
																</td>
															</tr>
															<tr>

																<td align="right" class="listwhitetext"><fmt:message
																	key="accountLine.basis" /></td>
																<td align="left" class="listwhitetext"><s:select
																	list="%{basis}" name="accountLine.basis"
																	headerKey="" headerValue="" onchange="changeStatus(),expense();" tabindex="1" cssClass="list-menu"/></td>
																<td align="right" width="200" class="listwhitetext" colspan="2"><fmt:message
																	key="accountLine.estimateQuantity" /></td>
																<td align="left" class="listwhitetext"><s:textfield
																	cssClass="input-text" cssStyle="text-align:right"
																	key="accountLine.estimateQuantity" size="8"
																	maxlength="12"
																	onkeydown="return onlyFloatNumsAllowed(event)" tabindex="2"
																	 onchange="expenseOld();fillDescr();" onblur="chkSelect('1');"/></td>
																<td align="right" class="listwhitetext"><fmt:message
																	key="accountLine.estimateRate" /></td>
																<td align="left" class="listwhitetext"><s:textfield
																	cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateRate"
																	size="8" maxlength="10"
																	onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');"
																	onchange="expenseOld();fillDescr();" tabindex="3"/></td>
																 <td align="right" class="listwhitetext"><fmt:message
																	key="accountLine.estimateSellRate" /></td>
																<td align="left" class="listwhitetext"><s:textfield
																	cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateSellRate"
																	size="8" maxlength="10"
																	onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');"
																	onchange="expenseOld();" tabindex="3"/></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateExpense" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="expenseOld();" tabindex="4"/></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePassPercentage" size="1" maxlength="4" onkeydown="return onlyNumsAllowed(event)" onchange="calculateRevenue();" tabindex="5"/></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateRevenueAmount" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="mockupForRevenue();" onblur="chkSelect('1');" tabindex="6"/></td>
															</tr>
															<tr>
															<td align="right" class="listwhitetext" >Description</td>
																<td align="left" class="listwhitetext" colspan="10"><s:textarea	cssClass="textarea" name="accountLine.quoteDescription" cols="120" rows="5" ></s:textarea>
																</td>
															
															</tr> 
														</tbody></table>
														</tbody>
										</table>
										</div>
										</td>
										</tr>
										</tbody>
										</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		<table>
		<tbody>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr>
				<td align="left" class="listwhitetext" width="30px"></td>
				<td colspan="5"></td>
			</tr>
			<tr><fmt:formatDate var="serviceCreatedOnFormattedValue" value="${accountLine.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='serviceOrder.createdOn' /></b></td>
				<s:hidden name="accountLine.createdOn" value="${serviceCreatedOnFormattedValue}"/>
				<td style="font-size:.90em ;width:130px"><fmt:formatDate value="${accountLine.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message 	key='serviceOrder.createdBy' /></b></td>
				<c:if test="${not empty accountLine.id}">
					<s:hidden name="accountLine.createdBy" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="%{accountLine.createdBy}" /></td>
				</c:if>
				<c:if test="${empty accountLine.id}">
					<s:hidden name="accountLine.createdBy"
						value="${pageContext.request.remoteUser}" />
					<td style="font-size:.90em"><s:label name="createdBy"
						value="${pageContext.request.remoteUser}" /></td>
				</c:if>

				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message
					key='serviceOrder.updatedOn' /></b></td>
					<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${accountLine.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="accountLine.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
				<td style="font-size:.90em; width:130px"><fmt:formatDate value="${accountLine.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
				<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message	key='serviceOrder.updatedBy' /></b></td>
				<c:if test="${not empty accountLine.id}"> 
                <s:hidden name="accountLine.updatedBy"/>

				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="%{accountLine.updatedBy}"/></td>
				
				</c:if>
				
				<c:if test="${empty accountLine.id}">
				
				<s:hidden name="accountLine.updatedBy" value="${pageContext.request.remoteUser}"/>
				
				<td style="width:85px ; font-size:.90em"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
				
				</c:if>
				<!--<s:hidden name="accountLine.updatedBy"	value="${pageContext.request.remoteUser}" />
				<td style="width:85px ; font-size:.90em"><s:label name="accountLine.updatedBy"
					value="${pageContext.request.remoteUser}" /></td>
			-->
			</tr>
		</tbody>
	</table>

</div>
	<s:submit cssClass="cssbuttonA" key="button.save" cssStyle="width:55px; height:25px" tabindex="23" onmouseover="return chkSelect('2');"/>
	<input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/quotationAccountLineList.html?sid=${serviceOrder.id}"/>'" />

	<s:hidden name="accountLine.corpID" />
    <s:hidden name="accountLine.sequenceNumber" /> 
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" />
	 
   <%--  <s:hidden name="totalEstimateExpenseCwt" value="<%=request.getParameter("totalEstimateExpenseCwt")%>"/>
	<s:hidden name="totalRevisionExpenseCwt" value="<%=request.getParameter("totalRevisionExpenseCwt")%>"/>
	<s:hidden name="totalEstimateExpenseKg" value="<%=request.getParameter("totalEstimateExpenseKg")%>"/>
	<s:hidden name="totalRevisionExpenseKg" value="<%=request.getParameter("totalRevisionExpenseKg")%>"/>
	<s:hidden name="totalEstimateExpenseCft" value="<%=request.getParameter("totalEstimateExpenseCft")%>"/>
	<s:hidden name="totalRevisionExpenseCft" value="<%=request.getParameter("totalRevisionExpenseCft")%>"/>
	<s:hidden name="totalEstimateExpenseCbm" value="<%=request.getParameter("totalEstimateExpenseCbm")%>"/>
	<s:hidden name="totalRevisionExpenseCbm" value="<%=request.getParameter("totalRevisionExpenseCbm")%>"/>
	<s:hidden name="totalEstimateExpenseEach" value="<%=request.getParameter("totalEstimateExpenseEach")%>"/>
	<s:hidden name="totalRevisionExpenseEach" value="<%=request.getParameter("totalRevisionExpenseEach")%>"/>
	<s:hidden name="totalEstimateExpenseFlat" value="<%=request.getParameter("totalEstimateExpenseFlat")%>"/>
	<s:hidden name="totalRevisionExpenseFlat" value="<%=request.getParameter("totalRevisionExpenseFlat")%>"/>
	<s:hidden name="totalEstimateExpenseHour" value="<%=request.getParameter("totalEstimateExpenseHour")%>"/>
	<s:hidden name="totalRevisionExpenseHour" value="<%=request.getParameter("totalRevisionExpenseHour")%>"/>--%>
</s:form>
<script type="text/javascript"> 
try{
findRadioValue();
}
catch(e){}
Form.focusFirstElement($("accountLineForms"));
try{
var f = document.getElementById('accountLineForms');
f.setAttribute("autocomplete", "off"); 
}
catch(e){}
</script> 
</body>
</html>
