DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`UploadFunctionalityAccountingTemplate` $$
CREATE  PROCEDURE `UploadFunctionalityAccountingTemplate`(NcorpId varchar(15),
CostElementflag bit(1),NupdatedBy varchar(82))
begin



if CostElementflag = false then 

update defaultaccountline d
inner join charges c  on c.charge=d.chargecode and c.contract=d.contract
and c.corpid=d.corpid and d.uploaddataflag=true
set d.recGl=c.gl,
d.payGl=c.expGl,
d.description=c.description,
d.updatedBy=NupdatedBy,
d.updatedon=now()
where c.corpid=NcorpId
and c.status =true;
end if;

if CostElementflag = true then 

update defaultaccountline d
inner join charges c1 on c1.charge=d.chargecode and c1.contract=d.contract
and c1.corpid=d.corpid and d.uploaddataflag=true
inner join contract c2 on c1.contract=c2.contract and c1.corpid=c2.corpid
inner join costelement c3 on  c1.costElement=c3.costElement and c1.corpid=c3.corpid
left join glcoderategrid c4 on c3.id=c4.costElementid
set d.recGl=if((c4.gridRecGl is null or c4.gridRecGl='') ,c3.recGl,c4.gridRecGl),
d.payGl=if((c4.gridPayGl is null or c4.gridPayGl=''),c3.payGl,c4.gridPayGl),
d.description=c1.description,
d.updatedBy=NupdatedBy,
d.updatedon=now()
where  c1.corpid=NcorpId and  c1.status =true
and c1.charge not in ('DMMFXFEE','DMMFEE')
 and (d.jobtype=c4.gridJob or c4.gridJob is null or c4.gridJob ='')
 and (d.route=c4.gridRouting or c4.gridRouting is null or c4.gridRouting =''
or d.route is null or d.route='')
and (d.companyDivision=c4.companyDivision or c4.companyDivision is null or c4.companyDivision =''
or d.companyDivision is null or d.companyDivision='');
end if;

update defaultaccountline d
inner join  partnerprivate p
on p.partnerCode=d.billToCode
and p.corpid=d.corpid
set d.billToName=p.lastname,
d.updatedBy=NupdatedBy,
d.updatedon=now()
where d.uploaddataflag=true
and d.corpid=NcorpId;

update defaultaccountline d
inner join  partnerprivate p
on p.partnerCode=d.vendorcode
and p.corpid=d.corpid
set d.vendorName=p.lastname,
d.updatedBy=NupdatedBy,
d.updatedon=now()
where d.uploaddataflag=true
and d.corpid=NcorpId;


update defaultaccountline
set estimatedRevenue=if((basis='%age' or basis='cwt'),((quantity*sellRate)/100),(quantity*sellRate)),
-- set estimatedRevenue=if((basis='%age' or basis='cwt'),((quantity*sellRate)/100),(if(basis='per 1000'),((quantity*sellRate)/1000),(quantity*sellRate)))),
amount=if((basis='%age' or basis='cwt'),((quantity*rate)/100),(quantity*rate)),
markUp=if(amount!=0,((estimatedRevenue/amount)*100),0),
estVatDescr=replace(estVatDescr,'.0',''),
estExpVatDescr=replace(estExpVatDescr,'.0',''),
equipment=replace(equipment,'.0',''),
estVatPercent = (select flex1 from refmaster where corpid=NcorpId and code=estVatDescr and parameter='EUVAT'),
estExpVatPercent = (select flex1 from refmaster where corpid=NcorpId and code=estExpVatDescr and parameter='PAYVATDESC'),
uploaddataflag=false
where uploaddataflag=true
and corpid=NcorpId;

end $$

DELIMITER ;