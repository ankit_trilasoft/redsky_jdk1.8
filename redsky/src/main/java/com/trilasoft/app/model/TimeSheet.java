package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table ( name="timesheet")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class TimeSheet  extends BaseObject{
	
	private Long id;
	private String corpID;
	private Date workDate;
	private String userName;
	private String action;
	private Long ticket;
	private String crewName;
	private String crewType;
	private String shipper;
	private String crewBillAs;
	private String beginHours;
	private String endHours;
	private String contract;
	private String distributionCode;
	private String calculationCode;
    private BigDecimal regularHours= new BigDecimal(0);	
    private BigDecimal overTime= new BigDecimal(0);	
    private BigDecimal doubleOverTime= new BigDecimal(0);	
    private BigDecimal adjustmentToRevenue= new BigDecimal(0);
    private BigDecimal paidRevenueShare= new BigDecimal(0);    

	private String regularPayPerHour;
	private String overTimePayPerHour;
	private String doubleOverTimePayPerHour;

	private String differentPayBasis;

	private String reasonForAdjustment;
	private String adjustedBy;
	private String notes;
	private String lunch; 
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	
	private String warehouse;
	
	private String doneForTheDay;
	private Boolean isCrewUsed;
	private String dailySheetNotes;
    private BigDecimal previewRegularHours= new BigDecimal(0);	
	private BigDecimal previewOverTime= new BigDecimal(0);	
	private String crewEmail;
	private String integrationTool;
	private String integrationStatus;
	@Override
	public String toString() {
		return "TimeSheet [id=" + id + ", corpID=" + corpID + ", workDate=" + workDate + ", userName=" + userName
				+ ", action=" + action + ", ticket=" + ticket + ", crewName=" + crewName + ", crewType=" + crewType
				+ ", shipper=" + shipper + ", crewBillAs=" + crewBillAs + ", beginHours=" + beginHours + ", endHours="
				+ endHours + ", contract=" + contract + ", distributionCode=" + distributionCode + ", calculationCode="
				+ calculationCode + ", regularHours=" + regularHours + ", overTime=" + overTime + ", doubleOverTime="
				+ doubleOverTime + ", adjustmentToRevenue=" + adjustmentToRevenue + ", paidRevenueShare="
				+ paidRevenueShare + ", regularPayPerHour=" + regularPayPerHour + ", overTimePayPerHour="
				+ overTimePayPerHour + ", doubleOverTimePayPerHour=" + doubleOverTimePayPerHour + ", differentPayBasis="
				+ differentPayBasis + ", reasonForAdjustment=" + reasonForAdjustment + ", adjustedBy=" + adjustedBy
				+ ", notes=" + notes + ", lunch=" + lunch + ", createdBy=" + createdBy + ", createdOn=" + createdOn
				+ ", updatedBy=" + updatedBy + ", updatedOn=" + updatedOn + ", warehouse=" + warehouse
				+ ", doneForTheDay=" + doneForTheDay + ", isCrewUsed=" + isCrewUsed + ", dailySheetNotes="
				+ dailySheetNotes + ", previewRegularHours=" + previewRegularHours + ", previewOverTime="
				+ previewOverTime + ", crewEmail=" + crewEmail + ", integrationTool=" + integrationTool
				+ ", integrationStatus=" + integrationStatus + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeSheet other = (TimeSheet) obj;
		if (action == null) {
			if (other.action != null)
				return false;
		} else if (!action.equals(other.action))
			return false;
		if (adjustedBy == null) {
			if (other.adjustedBy != null)
				return false;
		} else if (!adjustedBy.equals(other.adjustedBy))
			return false;
		if (adjustmentToRevenue == null) {
			if (other.adjustmentToRevenue != null)
				return false;
		} else if (!adjustmentToRevenue.equals(other.adjustmentToRevenue))
			return false;
		if (beginHours == null) {
			if (other.beginHours != null)
				return false;
		} else if (!beginHours.equals(other.beginHours))
			return false;
		if (calculationCode == null) {
			if (other.calculationCode != null)
				return false;
		} else if (!calculationCode.equals(other.calculationCode))
			return false;
		if (contract == null) {
			if (other.contract != null)
				return false;
		} else if (!contract.equals(other.contract))
			return false;
		if (corpID == null) {
			if (other.corpID != null)
				return false;
		} else if (!corpID.equals(other.corpID))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (crewBillAs == null) {
			if (other.crewBillAs != null)
				return false;
		} else if (!crewBillAs.equals(other.crewBillAs))
			return false;
		if (crewEmail == null) {
			if (other.crewEmail != null)
				return false;
		} else if (!crewEmail.equals(other.crewEmail))
			return false;
		if (crewName == null) {
			if (other.crewName != null)
				return false;
		} else if (!crewName.equals(other.crewName))
			return false;
		if (crewType == null) {
			if (other.crewType != null)
				return false;
		} else if (!crewType.equals(other.crewType))
			return false;
		if (dailySheetNotes == null) {
			if (other.dailySheetNotes != null)
				return false;
		} else if (!dailySheetNotes.equals(other.dailySheetNotes))
			return false;
		if (differentPayBasis == null) {
			if (other.differentPayBasis != null)
				return false;
		} else if (!differentPayBasis.equals(other.differentPayBasis))
			return false;
		if (distributionCode == null) {
			if (other.distributionCode != null)
				return false;
		} else if (!distributionCode.equals(other.distributionCode))
			return false;
		if (doneForTheDay == null) {
			if (other.doneForTheDay != null)
				return false;
		} else if (!doneForTheDay.equals(other.doneForTheDay))
			return false;
		if (doubleOverTime == null) {
			if (other.doubleOverTime != null)
				return false;
		} else if (!doubleOverTime.equals(other.doubleOverTime))
			return false;
		if (doubleOverTimePayPerHour == null) {
			if (other.doubleOverTimePayPerHour != null)
				return false;
		} else if (!doubleOverTimePayPerHour.equals(other.doubleOverTimePayPerHour))
			return false;
		if (endHours == null) {
			if (other.endHours != null)
				return false;
		} else if (!endHours.equals(other.endHours))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (integrationStatus == null) {
			if (other.integrationStatus != null)
				return false;
		} else if (!integrationStatus.equals(other.integrationStatus))
			return false;
		if (integrationTool == null) {
			if (other.integrationTool != null)
				return false;
		} else if (!integrationTool.equals(other.integrationTool))
			return false;
		if (isCrewUsed == null) {
			if (other.isCrewUsed != null)
				return false;
		} else if (!isCrewUsed.equals(other.isCrewUsed))
			return false;
		if (lunch == null) {
			if (other.lunch != null)
				return false;
		} else if (!lunch.equals(other.lunch))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (overTime == null) {
			if (other.overTime != null)
				return false;
		} else if (!overTime.equals(other.overTime))
			return false;
		if (overTimePayPerHour == null) {
			if (other.overTimePayPerHour != null)
				return false;
		} else if (!overTimePayPerHour.equals(other.overTimePayPerHour))
			return false;
		if (paidRevenueShare == null) {
			if (other.paidRevenueShare != null)
				return false;
		} else if (!paidRevenueShare.equals(other.paidRevenueShare))
			return false;
		if (previewOverTime == null) {
			if (other.previewOverTime != null)
				return false;
		} else if (!previewOverTime.equals(other.previewOverTime))
			return false;
		if (previewRegularHours == null) {
			if (other.previewRegularHours != null)
				return false;
		} else if (!previewRegularHours.equals(other.previewRegularHours))
			return false;
		if (reasonForAdjustment == null) {
			if (other.reasonForAdjustment != null)
				return false;
		} else if (!reasonForAdjustment.equals(other.reasonForAdjustment))
			return false;
		if (regularHours == null) {
			if (other.regularHours != null)
				return false;
		} else if (!regularHours.equals(other.regularHours))
			return false;
		if (regularPayPerHour == null) {
			if (other.regularPayPerHour != null)
				return false;
		} else if (!regularPayPerHour.equals(other.regularPayPerHour))
			return false;
		if (shipper == null) {
			if (other.shipper != null)
				return false;
		} else if (!shipper.equals(other.shipper))
			return false;
		if (ticket == null) {
			if (other.ticket != null)
				return false;
		} else if (!ticket.equals(other.ticket))
			return false;
		if (updatedBy == null) {
			if (other.updatedBy != null)
				return false;
		} else if (!updatedBy.equals(other.updatedBy))
			return false;
		if (updatedOn == null) {
			if (other.updatedOn != null)
				return false;
		} else if (!updatedOn.equals(other.updatedOn))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (warehouse == null) {
			if (other.warehouse != null)
				return false;
		} else if (!warehouse.equals(other.warehouse))
			return false;
		if (workDate == null) {
			if (other.workDate != null)
				return false;
		} else if (!workDate.equals(other.workDate))
			return false;
		return true;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((action == null) ? 0 : action.hashCode());
		result = prime * result + ((adjustedBy == null) ? 0 : adjustedBy.hashCode());
		result = prime * result + ((adjustmentToRevenue == null) ? 0 : adjustmentToRevenue.hashCode());
		result = prime * result + ((beginHours == null) ? 0 : beginHours.hashCode());
		result = prime * result + ((calculationCode == null) ? 0 : calculationCode.hashCode());
		result = prime * result + ((contract == null) ? 0 : contract.hashCode());
		result = prime * result + ((corpID == null) ? 0 : corpID.hashCode());
		result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result + ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result + ((crewBillAs == null) ? 0 : crewBillAs.hashCode());
		result = prime * result + ((crewEmail == null) ? 0 : crewEmail.hashCode());
		result = prime * result + ((crewName == null) ? 0 : crewName.hashCode());
		result = prime * result + ((crewType == null) ? 0 : crewType.hashCode());
		result = prime * result + ((dailySheetNotes == null) ? 0 : dailySheetNotes.hashCode());
		result = prime * result + ((differentPayBasis == null) ? 0 : differentPayBasis.hashCode());
		result = prime * result + ((distributionCode == null) ? 0 : distributionCode.hashCode());
		result = prime * result + ((doneForTheDay == null) ? 0 : doneForTheDay.hashCode());
		result = prime * result + ((doubleOverTime == null) ? 0 : doubleOverTime.hashCode());
		result = prime * result + ((doubleOverTimePayPerHour == null) ? 0 : doubleOverTimePayPerHour.hashCode());
		result = prime * result + ((endHours == null) ? 0 : endHours.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((integrationStatus == null) ? 0 : integrationStatus.hashCode());
		result = prime * result + ((integrationTool == null) ? 0 : integrationTool.hashCode());
		result = prime * result + ((isCrewUsed == null) ? 0 : isCrewUsed.hashCode());
		result = prime * result + ((lunch == null) ? 0 : lunch.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((overTime == null) ? 0 : overTime.hashCode());
		result = prime * result + ((overTimePayPerHour == null) ? 0 : overTimePayPerHour.hashCode());
		result = prime * result + ((paidRevenueShare == null) ? 0 : paidRevenueShare.hashCode());
		result = prime * result + ((previewOverTime == null) ? 0 : previewOverTime.hashCode());
		result = prime * result + ((previewRegularHours == null) ? 0 : previewRegularHours.hashCode());
		result = prime * result + ((reasonForAdjustment == null) ? 0 : reasonForAdjustment.hashCode());
		result = prime * result + ((regularHours == null) ? 0 : regularHours.hashCode());
		result = prime * result + ((regularPayPerHour == null) ? 0 : regularPayPerHour.hashCode());
		result = prime * result + ((shipper == null) ? 0 : shipper.hashCode());
		result = prime * result + ((ticket == null) ? 0 : ticket.hashCode());
		result = prime * result + ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = prime * result + ((updatedOn == null) ? 0 : updatedOn.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((warehouse == null) ? 0 : warehouse.hashCode());
		result = prime * result + ((workDate == null) ? 0 : workDate.hashCode());
		return result;
	}
	@Column( length = 15 )
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column( length = 15 )
	public String getAdjustedBy() {
		return adjustedBy;
	}
	public void setAdjustedBy(String adjustedBy) {
		this.adjustedBy = adjustedBy;
	}
	@Column( length = 25 )
	public BigDecimal getAdjustmentToRevenue() {
		return adjustmentToRevenue;
	}
	public void setAdjustmentToRevenue(BigDecimal adjustmentToRevenue) {
		this.adjustmentToRevenue = adjustmentToRevenue;
	}
	@Column( length = 10 )
	public String getBeginHours() {
		return beginHours;
	}
	public void setBeginHours(String beginHours) {
		this.beginHours = beginHours;
	}
	@Column( length = 10 )
	public String getCalculationCode() {
		return calculationCode;
	}
	public void setCalculationCode(String calculationCode) {
		this.calculationCode = calculationCode;
	}
	@Column( length = 100 )
	public String getContract() {
		return contract;
	}
	public void setContract(String contract) {
		this.contract = contract;
	}
	@Column( length = 15 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length = 82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length = 10 )
	public String getCrewBillAs() {
		return crewBillAs;
	}
	public void setCrewBillAs(String crewBillAs) {
		this.crewBillAs = crewBillAs;
	}
	@Column( length = 30 )
	public String getCrewName() {
		return crewName;
	}
	public void setCrewName(String crewName) {
		this.crewName = crewName;
	}
	@Column( length = 2 )
	public String getCrewType() {
		return crewType;
	}
	public void setCrewType(String crewType) {
		this.crewType = crewType;
	}
	@Column( length = 1 )
	public String getDifferentPayBasis() {
		return differentPayBasis;
	}
	public void setDifferentPayBasis(String differentPayBasis) {
		this.differentPayBasis = differentPayBasis;
	}
	@Column( length = 10 )
	public String getDistributionCode() {
		return distributionCode;
	}
	public void setDistributionCode(String distributionCode) {
		this.distributionCode = distributionCode;
	}
	@Column( length = 10 )
	public BigDecimal getDoubleOverTime() {
		return doubleOverTime;
	}
	public void setDoubleOverTime(BigDecimal doubleOverTime) {
		this.doubleOverTime = doubleOverTime;
	}
	@Column( length = 10 )
	public String getDoubleOverTimePayPerHour() {
		return doubleOverTimePayPerHour;
	}
	public void setDoubleOverTimePayPerHour(String doubleOverTimePayPerHour) {
		this.doubleOverTimePayPerHour = doubleOverTimePayPerHour;
	}
	@Column( length = 10 )
	public String getEndHours() {
		return endHours;
	}
	public void setEndHours(String endHours) {
		this.endHours = endHours;
	}
	@Id  @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length = 100 )
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Column( length = 30 )
	public BigDecimal getOverTime() {
		return overTime;
	}
	public void setOverTime(BigDecimal overTime) {
		this.overTime = overTime;
	}
	@Column( length = 10 )
	public String getOverTimePayPerHour() {
		return overTimePayPerHour;
	}
	public void setOverTimePayPerHour(String overTimePayPerHour) {
		this.overTimePayPerHour = overTimePayPerHour;
	}
	@Column( length = 20 )
	public BigDecimal getPaidRevenueShare() {
		return paidRevenueShare;
	}
	public void setPaidRevenueShare(BigDecimal paidRevenueShare) {
		this.paidRevenueShare = paidRevenueShare;
	}
	@Column( length = 100 )
	public String getReasonForAdjustment() {
		return reasonForAdjustment;
	}
	public void setReasonForAdjustment(String reasonForAdjustment) {
		this.reasonForAdjustment = reasonForAdjustment;
	}
	@Column( length = 10 )
	public BigDecimal getRegularHours() {
		return regularHours;
	}
	public void setRegularHours(BigDecimal regularHours) {
		this.regularHours = regularHours;
	}
	@Column( length = 10 )
	public String getRegularPayPerHour() {
		return regularPayPerHour;
	}
	public void setRegularPayPerHour(String regularPayPerHour) {
		this.regularPayPerHour = regularPayPerHour;
	}
	@Column( length = 30 )
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	@Column( length = 10 )
	public Long getTicket() {
		return ticket;
	}
	public void setTicket(Long ticket) {
		this.ticket = ticket;
	}
	@Column( length = 82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column( length=25 )
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column
	public Date getWorkDate() {
		return workDate;
	}
	public void setWorkDate(Date workDate) {
		this.workDate = workDate;
	}
	@Column( length = 3 )
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column
	public String getLunch() {
		return lunch;
	}
	public void setLunch(String lunch) {
		this.lunch = lunch;
	}
	@Column( length = 10 )
	public String getDoneForTheDay() {
		return doneForTheDay;
	}
	public void setDoneForTheDay(String doneForTheDay) {
		this.doneForTheDay = doneForTheDay;
	}
	public Boolean getIsCrewUsed() {
		return isCrewUsed;
	}
	public void setIsCrewUsed(Boolean isCrewUsed) {
		this.isCrewUsed = isCrewUsed;
	}
	public String getDailySheetNotes() {
		return dailySheetNotes;
	}
	public void setDailySheetNotes(String dailySheetNotes) {
		this.dailySheetNotes = dailySheetNotes;
	}
	@Column
	public BigDecimal getPreviewRegularHours() {
		return previewRegularHours;
	}
	public void setPreviewRegularHours(BigDecimal previewRegularHours) {
		this.previewRegularHours = previewRegularHours;
	}
	@Column
	public BigDecimal getPreviewOverTime() {
		return previewOverTime;
	}
	public void setPreviewOverTime(BigDecimal previewOverTime) {
		this.previewOverTime = previewOverTime;
	}
	@Column
	public String getCrewEmail() {
		return crewEmail;
	}
	public void setCrewEmail(String crewEmail) {
		this.crewEmail = crewEmail;
	}
	@Column
	public String getIntegrationTool() {
		return integrationTool;
	}
	public void setIntegrationTool(String integrationTool) {
		this.integrationTool = integrationTool;
	}
	@Column
	public String getIntegrationStatus() {
		return integrationStatus;
	}
	public void setIntegrationStatus(String integrationStatus) {
		this.integrationStatus = integrationStatus;
	}
		
	
}
