ALTER TABLE `redsky`.`customerfile` ADD COLUMN `emailSentDate` VARCHAR(50) AFTER `ugwIntId`;

ALTER TABLE `redsky`.`redskybilling` ADD COLUMN `packingA` DATETIME AFTER `redskyBillDate`,ADD COLUMN `loadA` DATETIME AFTER `packingA`,ADD COLUMN `deliveryA` DATETIME AFTER `loadA`;

ALTER TABLE `redsky`.`networkcontrol` ADD COLUMN `childAgentCode` VARCHAR(20) DEFAULT NULL AFTER `createdBy`,  ADD COLUMN `childAgentType` VARCHAR(5) DEFAULT NULL AFTER `childAgentCode`,  ADD COLUMN `companyDiv` VARCHAR(10) DEFAULT NULL AFTER `childAgentType`;
