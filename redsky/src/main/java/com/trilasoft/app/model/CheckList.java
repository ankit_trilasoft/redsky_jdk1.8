package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="checklist")

@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class CheckList extends BaseObject{
	
	private Long id;
	private String corpID;
	private String jobType;
	private String partnerCode;
	private String documentType;
	private String controlDate;
	private Long duration;
	private Boolean required;
	private String owner;
	private String messageDisplayed;
	private Boolean enable;
	private Boolean status;
	private String expression;
	private String tableRequired;
	private String url;
	
	private String routing;
	private String service;
	private String mode;
	private String importCountry;
	private String exportCountry;
	private Date customerFileDate;
	private Date serviceOrderDate;
	private String billToExcludes;
	
	
	
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("jobType", jobType).append("partnerCode",
				partnerCode).append("documentType", documentType).append(
				"controlDate", controlDate).append("duration", duration)
				.append("required", required).append("owner", owner).append(
						"messageDisplayed", messageDisplayed).append("enable",
						enable).append("status", status).append("expression",
						expression).append("tableRequired", tableRequired)
				.append("url", url).append("routing", routing).append(
						"service", service).append("mode", mode).append(
						"importCountry", importCountry).append("exportCountry",
						exportCountry).append("customerFileDate",
						customerFileDate).append("serviceOrderDate",
						serviceOrderDate).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
		         .append("billToExcludes", billToExcludes).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CheckList))
			return false;
		CheckList castOther = (CheckList) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(jobType, castOther.jobType).append(
				partnerCode, castOther.partnerCode).append(documentType,
				castOther.documentType).append(controlDate,
				castOther.controlDate).append(duration, castOther.duration)
				.append(required, castOther.required).append(owner,
						castOther.owner).append(messageDisplayed,
						castOther.messageDisplayed).append(enable,
						castOther.enable).append(status, castOther.status)
				.append(expression, castOther.expression).append(tableRequired,
						castOther.tableRequired).append(url, castOther.url)
				.append(routing, castOther.routing).append(service,
						castOther.service).append(mode, castOther.mode).append(
						importCountry, castOther.importCountry).append(
						exportCountry, castOther.exportCountry).append(
						customerFileDate, castOther.customerFileDate).append(
						serviceOrderDate, castOther.serviceOrderDate).append(
						createdBy, castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn)
						.append(billToExcludes,castOther.billToExcludes)
						.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(jobType)
				.append(partnerCode).append(documentType).append(controlDate)
				.append(duration).append(required).append(owner).append(
						messageDisplayed).append(enable).append(status).append(
						expression).append(tableRequired).append(url).append(
						routing).append(service).append(mode).append(
						importCountry).append(exportCountry).append(
						customerFileDate).append(serviceOrderDate).append(
						createdBy).append(updatedBy).append(createdOn).append(
						updatedOn)
						.append(billToExcludes)
						.toHashCode();
	}
	@Column
	public String getControlDate() {
		return controlDate;
	}
	public void setControlDate(String controlDate) {
		this.controlDate = controlDate;
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getDocumentType() {
		return documentType;
	}
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	@Column
	public Long getDuration() {
		return duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}
	@Column
	public Boolean getEnable() {
		return enable;
	}
	public void setEnable(Boolean enable) {
		this.enable = enable;
	}
	@Id  @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	@Column
	public String getMessageDisplayed() {
		return messageDisplayed;
	}
	public void setMessageDisplayed(String messageDisplayed) {
		this.messageDisplayed = messageDisplayed;
	}
	@Column
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	@Column
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public Boolean getRequired() {
		return required;
	}
	public void setRequired(Boolean required) {
		this.required = required;
	}
	
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getExpression() {
		return expression;
	}
	public void setExpression(String expression) {
		this.expression = expression;
	}
	@Column
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Column
	public String getTableRequired() {
		return tableRequired;
	}
	public void setTableRequired(String tableRequired) {
		this.tableRequired = tableRequired;
	}
	@Column
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Column
	public String getExportCountry() {
		return exportCountry;
	}
	public void setExportCountry(String exportCountry) {
		this.exportCountry = exportCountry;
	}
	@Column
	public String getImportCountry() {
		return importCountry;
	}
	public void setImportCountry(String importCountry) {
		this.importCountry = importCountry;
	}
	@Column
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	@Column
	public String getRouting() {
		return routing;
	}
	public void setRouting(String routing) {
		this.routing = routing;
	}
	@Column
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	@Column
	public Date getCustomerFileDate() {
		return customerFileDate;
	}
	public void setCustomerFileDate(Date customerFileDate) {
		this.customerFileDate = customerFileDate;
	}
	@Column
	public Date getServiceOrderDate() {
		return serviceOrderDate;
	}
	public void setServiceOrderDate(Date serviceOrderDate) {
		this.serviceOrderDate = serviceOrderDate;
	}
	@Column
	public String getBillToExcludes() {
		return billToExcludes;
	}
	public void setBillToExcludes(String billToExcludes) {
		this.billToExcludes = billToExcludes;
	}
	
	
}
