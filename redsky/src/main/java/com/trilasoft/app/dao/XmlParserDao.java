package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.XmlParser;

public interface XmlParserDao extends GenericDao<XmlParser, Long>{
	public String saveData(String sessionCorpID, String file, String userName);
	public String saveNokRate(String sessionCorpID, String file, String userName);
	//public String saveUsdRate(String sessionCorpID, String file, String userName);
	//public String saveGBPRate(String sessionCorpID, String file, String userName);
	//public String saveCNYRate(String sessionCorpID, String file, String userName);
	//public String saveINRRate(String sessionCorpID, String file, String userName);
	//public String saveBRLRate(String sessionCorpID, String file, String userName);
	//public String saveKRWRate(String sessionCorpID, String file, String userName);
	public String saveCADRate(String sessionCorpID, String file, String userName);
	//public String saveSGDRate(String sessionCorpID, String file, String userName);
	//public String saveJODRate(String sessionCorpID, String file, String userName);
	//public String saveIDRRate(String sessionCorpID, String file, String userName);
	public String saveDynamicRate(String sessionCorpID, String file, String userName,String baseCurrency);
}
