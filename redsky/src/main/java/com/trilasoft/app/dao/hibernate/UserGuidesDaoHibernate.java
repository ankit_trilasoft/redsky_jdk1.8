package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;

import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import com.trilasoft.app.dao.UserGuidesDao;
import com.trilasoft.app.model.UserGuides;

public class UserGuidesDaoHibernate extends GenericDaoHibernate<UserGuides, Long> implements UserGuidesDao{
	
		public UserGuidesDaoHibernate() {
		super(UserGuides.class);
		// TODO Auto-generated constructor stub
	}
		
	public List findListByModuleDocStatus(String module, String documentName,Boolean activeStatus) {
		if (activeStatus){
			return getHibernateTemplate().find("from UserGuides where module like '" + module + "%' AND documentName like '" + documentName + "%' and visible is true  ");
		}else{
			return getHibernateTemplate().find("from UserGuides where module like '" + module + "%' AND documentName like '" + documentName + "%' and visible is false ");	
		}

	}
	
	public List<UserGuides> findListByModule(String module,String corpId) {
			return getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and module='"+module+"' and visible is true order by displayOrder");
			}
	
	public List findDistinct() {
		return getHibernateTemplate().find(" SELECT distinct corpID FROM Company order by corpID");
	}
	
	 public List<UserGuides> findListByModuleAndDisplayOrder(String module,int displayOrder,Integer prevDisplayOrder){
		 String condition = "";
		 if(prevDisplayOrder != null  && prevDisplayOrder > 0 && prevDisplayOrder > displayOrder){
			 condition = " and displayOrder <= "+prevDisplayOrder;
		 }
		 return getHibernateTemplate().find("from UserGuides where module='"+module+"' and displayOrder >= "+displayOrder+condition+" and visible is true");
	 }
	 
	 public List<String> findListByDistinctModule(String corpId) {
		 List<String> list = new ArrayList<String>();
		 for (UserGuides userGuides : (List<UserGuides>)getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and visible is true group by module")) {
			 list.add(userGuides.getModule());
		 }
		 return list;
	}
	
	public List findListByDocumentName(String fileName,String UserType,Boolean cmmdmmAgent,Boolean vanlineEnabled,String corpId) {
		List al=null;
		if(UserType.equalsIgnoreCase("AGENT")){
			al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module ='Portals' and corpidChecks='Agent Portal' order by displayOrder");
		}else if(UserType.equalsIgnoreCase("ACCOUNT")){
			al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module ='Portals' and corpidChecks='Account Portal' order by displayOrder");			
		}else {
			if((cmmdmmAgent)&&(!vanlineEnabled)){
				al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module !='Portals' and corpidChecks!='Vanline' order by displayOrder");
			}else if((!cmmdmmAgent)&&(vanlineEnabled)){
				al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module !='Portals' and corpidChecks!='CMM/DMM agent' order by displayOrder");
			}else if((!cmmdmmAgent)&&(!vanlineEnabled)){
				al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module !='Portals' and corpidChecks!='Vanline' and corpidChecks!='CMM/DMM agent' order by displayOrder");
			}else{
				al=getHibernateTemplate().find("from UserGuides where corpId in('TSFT','"+corpId+"') and documentName like replace('%"+fileName+"%',' ','') and visible is true and module !='Portals' and order by displayOrder");
			}
		}
		  return al;
		}
	 public List findModuleCount(String corpId) {
	    	return getHibernateTemplate().find("from UserGuides where module='Portals' and visible is true");
	    }

	
}
