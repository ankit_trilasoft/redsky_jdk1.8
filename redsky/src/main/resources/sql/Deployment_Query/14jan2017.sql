// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`claim` CHANGE COLUMN `responsibleAgent` `responsibleAgentCode` VARCHAR(8),
ADD COLUMN `responsibleAgentName` VARCHAR(255) DEFAULT '' AFTER `remediationComment`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `isDefaultContactPerson` bit(1) DEFAULT b'0' ;

ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `uploaddataflag` bit(1) DEFAULT '' AFTER `estExpVatPercent`;

ALTER TABLE `redsky`.`company` ADD COLUMN `acctRefSameParent` bit(1) DEFAULT b'0' AFTER `activityMgmtVersion2`;
/* Move to below give trigger and procedure on prod . 
file path is 
--trigger
Project Explorer=>sql=>copy_charges.sql
Project Explorer=>sql=>copy_countrydeviation.sql
Project Explorer=>sql=>copy_rategrid.sql

--procedure

Project Explorer=>sql=>UploadFunctionalityAccountingTemplate_procedure.sql


*/


--For  ref ticket -12254

-- follow  below steps and run to prod 

-- step 1 
delete from extractcolumnmgmt
where reportname='RelocationAnalysis';

 -- step 2 
take backup  given table  -temp_extractcolumnmgmt2 and move to prod and restore in prod

-- step 3
-- run below query


insert into extractcolumnmgmt
 (corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,tableName, fieldName, displayName, columnSequenceNumber)
select corpID,'Backend_UPD' as createdBy, now() as createdOn, 'Backend_UPD' as updatedBy , now() as updatedOn ,reportname,tableName,fieldname ,displayName,
(@row_number:=@row_number +  1) AS columnSequenceNumber from temp_extractcolumnmgmt2
,(SELECT @row_number:= (select max(columnSequenceNumber) from extractcolumnmgmt where corpID='TSFT')) AS t
where corpID='TSFT'
order by rn;


insert into extractcolumnmgmt
 (corpID, createdBy, createdOn, updatedBy, updatedOn, reportName,tableName, fieldName, displayName, columnSequenceNumber)
select corpID,'Backend_UPD' as createdBy, now() as createdOn, 'Backend_UPD' as updatedBy , now() as updatedOn ,reportname,tableName,fieldname ,displayName,
(@row_number:=@row_number +  1) AS columnSequenceNumber from temp_extractcolumnmgmt2
,(SELECT @row_number:= (select max(columnSequenceNumber) from extractcolumnmgmt where corpID='VOER')) AS t
where corpID='VOER'
order by rn;

--For  ref ticket -12286

--run below query

insert into partneraccountref
 (Corpid,partnercode,accountCrossReference,RefType,createdby,createdon,updatedby,updatedon)
select  'VORU' as Corpid,a.partnercode,a.accountCrossReference,a.RefType,'Backend_Upd' as createdby,now() as createdon,'Backend_Upd' as updatedby,now() as updatedon
from partneraccountref a
 left join partneraccountref b on a.partnercode=b.partnercode and b.corpid='voru'
where a.corpid = 'VOER'
and a.reftype in ('P','R')
  and not exists (select * from partneraccountref c where b.partnercode=c.partnercode and a.reftype=c.reftype );
