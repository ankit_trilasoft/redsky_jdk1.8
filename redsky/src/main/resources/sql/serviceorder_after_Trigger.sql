
delimiter $$

CREATE trigger redsky.trigger_add_history_serviceorder_after AFTER INSERT on redsky.serviceorder

for each row BEGIN

DECLARE fieldNameList LONGTEXT;
DECLARE oldValueList LONGTEXT;
DECLARE newValueList LONGTEXT;
DECLARE BAcode LONGTEXT;
DECLARE HAcode LONGTEXT;
DECLARE OAcode LONGTEXT;
DECLARE DAcode LONGTEXT;
DECLARE OSAcode LONGTEXT;
DECLARE DSAcode LONGTEXT;
DECLARE BRcode LONGTEXT;
DECLARE FRcode LONGTEXT;
DECLARE NPcode LONGTEXT;
DECLARE countso LONGTEXT;
DECLARE pactualsurveydate LONGTEXT;
DECLARE psurvey LONGTEXT;


SET fieldNameList = " ";
SET oldValueList = " ";
SET newValueList = " ";
SET BAcode = " ";
SET HAcode = " ";
SET OAcode = " ";
SET DAcode = " ";
SET OSAcode = " ";
SET DSAcode = " ";
SET BRcode = " ";
SET FRcode = " ";
SET NPcode = " ";
SET countso = " ";
SET pactualsurveydate = " ";
SET psurvey = " ";


select CONCAT(fieldNameList,'serviceorder.bookingAgentShipNumber~') into fieldNameList;

select CONCAT(newValueList,NEW.bookingAgentShipNumber,'~') into newValueList;

select count(shipnumber) into countso from sodashboard where serviceorderid=NEW.id;

select actualsurveydate,survey into pactualsurveydate,psurvey from customerfile where id=NEW.customerfileid;

if(countso=0) then
insert into sodashboard(shipnumber,serviceorderid,corpid,customerfileid,actualSurvey,estimateSurvey)
select NEW.shipnumber,NEW.id,NEW.corpid,NEW.customerfileid,pactualsurveydate,psurvey from serviceorder where id=NEW.id;


select bookingagentcode into BAcode from serviceorder where id=NEW.id;

select haulingAgentCode into HAcode from miscellaneous where id=NEW.id;

select originagent into OAcode from trackingstatus where id=NEW.id;

select destinationagent into DAcode from trackingstatus where id=NEW.id;

select originsubagent into OSAcode from trackingstatus where id=NEW.id;

select destinationsubagent into DSAcode from trackingstatus where id=NEW.id;

select brokercode into BRcode from trackingstatus where id=NEW.id;

select forwardercode into FRcode from trackingstatus where id=NEW.id;

select networkPartnerCode into NPcode from trackingstatus where id=NEW.id;

update sodashboard set role=(trim( both '/' from concat(if(OAcode is not null and OAcode<>'','OA/',''),
if(DAcode is not null and DAcode <>'','DA/',''),
if(OSAcode is not null and OSAcode<>'','OSA/',''),
if(DSAcode is not null and DSAcode<>'','DSA/',''),
if(BRcode is not null and BRcode<>'','BR/',''),
if(FRcode is not null and FRcode<>'','FR/',''),
if(BAcode is not null and BAcode<>'','BA/',''),
if(NPcode is not null and NPcode<>'','NA/',''),
if(HAcode is not null and HAcode<>'','HA/','')))) where serviceorderid=NEW.id;

end if;

if(length(newValueList)>5) then

CALL add_tblHistory (NEW.id,"serviceorder", fieldNameList, oldValueList, newValueList, NEW.updatedby, NEW.corpID, now());

end if;



END $$

delimiter;
