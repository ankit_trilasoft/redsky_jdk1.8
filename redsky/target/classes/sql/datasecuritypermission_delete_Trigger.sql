delimiter $$
CREATE trigger redsky.trigger_delete_history_datasecuritypermission
before delete on redsky.datasecuritypermission
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE oldCorpid LONGTEXT;
  DECLARE newUser LONGTEXT;
  DECLARE newRoleName LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET oldCorpid = " ";
  SET newUser = " ";
  SET newRoleName = " ";

select corpid into oldCorpid from datasecurityset where id=OLD.datasecurityset_id;

IF (OLD.datasecurityfilter_id <> '' or OLD.datasecurityfilter_id is not null) THEN
    select name,updatedby into newRoleName,newUser from datasecurityfilter where id=OLD.datasecurityfilter_id;
       select CONCAT(fieldNameList,'datasecuritypermission.datasecurityfilter_id~') into fieldNameList;
       select CONCAT(oldValueList,newRoleName,'~') into oldValueList;
   END IF;


CALL add_tblHistory (OLD.datasecurityset_id,"datasecuritypermission", fieldNameList, oldValueList, newValueList, newUser, oldCorpid, now());

END $$
delimiter;