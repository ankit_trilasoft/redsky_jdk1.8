// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_exceptionAddedValue` TEXT NULL  AFTER `TEN_Utility_Miscellaneous` ;

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `mergeInto` VARCHAR(8) NULL  AFTER `sentToAccounting` ;

ALTER TABLE `redsky`.`serviceorder` ADD COLUMN `updaterUpdatedOn` DATETIME NULL  AFTER `surveyCoordinator` ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_Gas_Electric` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_exceptionAddedValue` ,
ADD COLUMN `TEN_Utility_Gas_Electric` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Gas_Electric` ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_housingRentVatDesc` VARCHAR(30) NULL  AFTER `TEN_Utility_Gas_Electric` ;

ALTER TABLE `redsky`.`tenancyutilityservice` DROP COLUMN `TEN_utilityVatPercent` , ADD COLUMN `TEN_utilityEndDate` DATETIME NULL  AFTER `TEN_utilityBillThrough` ;

ALTER TABLE `redsky`.`accountcontact` ADD COLUMN `status` BIT(1) DEFAULT b'1'  AFTER `basedAtName` ;

ALTER TABLE `redsky`.`operationsintelligence` MODIFY COLUMN `quantitiy` DECIMAL(19,2),
 MODIFY COLUMN `estimatedquantity` DECIMAL(19,2),
 MODIFY COLUMN `revisionquantity` DECIMAL(19,2),
 MODIFY COLUMN `revisedQuantity` DECIMAL(19,2) DEFAULT 0;
 
 ALTER TABLE `redsky`.`accountcontact` ADD COLUMN `status` BIT(1) DEFAULT b'1'  AFTER `basedAtName` ;

 
 