DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`CustomerFile_UpdateStatus` $$
CREATE PROCEDURE `CustomerFile_UpdateStatus`(l_customerfileid bigint,l_updatedby varchar(82))
BEGIN

declare l_distinctStatus bigint;

select count(*) into l_distinctStatus from serviceorder where customerfileid=l_customerfileid and status!='CLSD';
if (l_distinctStatus=0) then
  update customerfile set status='CLOSED',statusnumber='500',updatedby=l_updatedby,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='CLOSED';
end if;

END $$

DELIMITER ;