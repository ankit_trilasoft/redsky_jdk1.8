<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="carrierList.title"/></title>   
    <meta name="heading" content="<fmt:message key='carrierList.heading'/>"/>   
</head>   
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editAgent.html?id=${ServiceOrderID}" >Agent</a>
			</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Carrier</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
				
				<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editServiceOrderUpdate.html?id=${ServiceOrderID}">Service Orders</a></td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
   </tr>
   </tbody>
   </table>
   </div>
   
<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCarrier.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
       
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>  
  
<s:set name="carriers" value="carriers" scope="request"/>   
<display:table name="carriers" class="table" requestURI="" id="carrierList"  export="true"  defaultsort="1" pagesize="10">   
    
    <display:column property="carrierNumber" sortable="true" titleKey="carrier.carrierNumber" href="editCarrier.html"    
        paramId="id" paramProperty="id"/>   
    <display:column property="carrierCode" sortable="true" titleKey="carrier.carrierCode"/>
    <display:column property="carrierName" sortable="true" titleKey="carrier.carrierName"/>
    <display:column property="miles" sortable="true" titleKey="carrier.miles"/>
    <display:column property="carrierDeparture" sortable="true" titleKey="carrier.carrierDeparture"/>
    <display:column property="carrierTD" sortable="true" titleKey="carrier.carrierTD" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="carrierArrival" sortable="true" titleKey="carrier.carrierArrival"/>
    <display:column property="carrierTA" sortable="true" titleKey="carrier.carrierTA" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="shipNumber" sortable="true" titleKey="serviceOrder.shipNumber"/>
</display:table>   
  <c:out value="${buttons}" escapeXml="false" />
<script type="text/javascript">   
    highlightTableRows("carrierList");   
</script>  