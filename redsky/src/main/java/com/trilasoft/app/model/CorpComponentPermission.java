package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "corp_comp_permission")

//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class CorpComponentPermission extends BaseObject {
	private Long id;

	private String corpID;

	public String componentId;

	public String description;

	public Integer mask;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("componentId", componentId).append(
				"description", description).append("mask", mask).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CorpComponentPermission))
			return false;
		CorpComponentPermission castOther = (CorpComponentPermission) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(componentId, castOther.componentId)
				.append(description, castOther.description).append(mask,
						castOther.mask).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				componentId).append(description).append(mask).toHashCode();
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(length = 25)
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 50)
	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String fieldName) {
		this.componentId = fieldName;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(length = 25)
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 256)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "corp_id", length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public Integer getMask() {
		return mask;
	}

	public void setMask(Integer mask) {
		this.mask = mask;
	}

}
