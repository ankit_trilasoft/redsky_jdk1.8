<%@ include file="/common/taglibs.jsp"%> 
<head>
<meta name="heading" content="<fmt:message key='systemConfigurationForm.title'/>"/> 
<title><fmt:message key="systemConfigurationForm.heading"/></title> 

</head>

<script language="javascript" type="text/javascript">

function imposeMaxLength(Object, MaxLen)
{
return (Object.value.length <= MaxLen);
}

function openCatalogList(){
	document.forms['systemConfigurationMainForm'].action='searchDataCatalog.html';
	document.forms['systemConfigurationMainForm'].submit();
}
</script>

<script language="JavaScript">
function check(clickType){
	if(document.forms['systemConfigurationMainForm'].elements['dataCatalog.tableName'].value=="")
	{
			alert("Table Name is the required field");
			return false;
	}
	else if(document.forms['systemConfigurationMainForm'].elements['dataCatalog.fieldName'].value=="")
	{
		alert("Field Name is the required field");
		return false;
	}else{
		autoSave(clickType);
	}
	}

function changeStatus(){
	document.forms['systemConfigurationMainForm'].elements['formStatus'].value = '1';
}



function autoSave(clickType){	
	
		if(!(clickType == 'save')){
		if ('${autoSavePrompt}' == 'No'){

			var noSaveAction = '<c:out value="${dataCatalog.id}"/>';

			var id1 = document.forms['systemConfigurationMainForm'].elements['dataCatalog.id'].value;

			if(document.forms['systemConfigurationMainForm'].elements['gotoPageString'].value == 'gototab.systemlist'){

			noSaveAction = 'systemlist.html';

			}
			
			processAutoSave(document.forms['systemConfigurationMainForm'], 'saveSystemConfigurationMainForm!saveOnTabChange.html', noSaveAction);

		}else{	
			var id1 = document.forms['systemConfigurationMainForm'].elements['dataCatalog.id'].value;
			if (document.forms['systemConfigurationMainForm'].elements['formStatus'].value == '1'){
				var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='systemConfigurationForm.title'/>");
				if(agree){
					document.forms['systemConfigurationMainForm'].action = 'saveSystemConfigurationMainForm!saveOnTabChange.html';
					document.forms['systemConfigurationMainForm'].submit();
							}
					else{
						if(id1 != ''){
							if(document.forms['systemConfigurationMainForm'].elements['gotoPageString'].value == 'gototab.systemlist'){
								location.href = 'systemlist.html';
										}	
									}
							}
			}else{
				if(id1 != ''){
					if(document.forms['systemConfigurationMainForm'].elements['gotoPageString'].value == 'gototab.systemlist'){
							location.href = 'systemlist.html';
							}
						
				}
			}
			}
	}

}
</script>
<s:form id="systemConfigurationMainForm" name="systemConfigurationMainForm" action="saveSystemConfigurationMainForm" method="post" validate="true">
       <s:hidden name="tableName" value="<%=request.getParameter("tableName") %>"/>
       <s:hidden name="fieldName" value="<%=request.getParameter("fieldName") %>"/>
       <s:hidden name="description" value="<%=request.getParameter("description") %>"/>
       <s:hidden name="auditable" value="<%=request.getParameter("auditable") %>"/>
       <s:hidden name="defineByToDoRule" value="<%=request.getParameter("defineByToDoRule") %>"/>
       <s:hidden name="isdateField" value="<%=request.getParameter("isdateField") %>"/>
       <s:hidden name="visible" value="<%=request.getParameter("visible") %>"/>
       <s:hidden name="charge" value="<%=request.getParameter("charge") %>"/>
       
       
       <s:hidden name="gotoPageString" id="gotoPageString" value="" />
       <s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >      
<c:choose>
       <c:when test="${gotoPageString == 'gototab.systemlist' }">
       <c:redirect url="/systemlist.html"/>
       </c:when>
       <c:otherwise>
       </c:otherwise>
</c:choose>
</c:if>
<div id="Layer1" style="width:100%" onkeydown="changeStatus();">

		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>System Configuration<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a onclick="setReturnString('gototab.systemlist');return check('none');"><span>System Configuration List</span></a></li>
		    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${dataCatalog.id}&tableName=datacatalog&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		    <!--<li><a href="systemlist.html"><span>System Configuration List</span></a></li>
		  --></ul>
		</div><div class="spn">&nbsp;</div>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:2px;" ><span></span></div>
   <div class="center-content">	
 <table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  	<td align="left" height="10px" colspan="2"></td>
		  	<td width="20px"></td>
		  	<td></td>		  	
		  	</tr>
		  	<tr>
		  		<td rowspan="3" class="listwhitetext" valign="top">
		  		<table border="0" cellspacing="6" cellpadding="2">
		  		<tr>		  		
		  		<td align="right"><fmt:message key="dataCatalog.tableName"/><font color="red" size="2">*</font></td>
		  		<td align="left"><s:textfield name="dataCatalog.tableName"  maxlength="50" size="29" cssClass="input-text" readonly="false"/></td>
		  		</tr>
		  		<tr>
		  		<td align="right"><fmt:message key="dataCatalog.fieldName"/><font color="red" size="2">*</font></td>
		  		<td align="left" ><s:textfield name="dataCatalog.fieldName"  maxlength="50" size="29" cssClass="input-text" readonly="false"/></td>
		  		</tr>
		  		<tr>
		  		<td align="right"><fmt:message key="dataCatalog.charge"/></td>		  		
		  		<td align="left"><s:select cssClass="list-menu"   name="dataCatalog.charge" list="{'Quantity','Price'}" headerKey=" " headerValue=" " cssStyle="width:168px" onchange="changeStatus();"/></td> 
		  		</tr>
		  		</table>
		  		</td>
		  		<td rowspan="3" class="listwhitetext" valign="top">
				<fieldset style="width:200px;">
				<legend>Configuration</legend>
				<table class="detailTabLabel" border="0">
				<tr>
				<td align="right">
		  		<s:checkbox key="dataCatalog.auditable" fieldValue="true" onchange="changeStatus();"/>
		  		</td>
		  		<td class="listwhitetext"><fmt:message key="dataCatalog.auditable"/></td>
				</tr>
				<tr>
		  		<td align="right" >
		  		<s:checkbox key="dataCatalog.defineByToDoRule" fieldValue="true" onchange="changeStatus();"/>
		  		</td>
		  		<td class="listwhitetext"><fmt:message key="dataCatalog.defineByToDoRule"/></td>
		  		</tr>
		  		<tr>
		  		<td align="right" ><s:checkbox key="dataCatalog.isdateField" fieldValue="true" onchange="changeStatus();"/></td>
		  		<td align="left" class="listwhitetext"><fmt:message key="dataCatalog.isdateField"/></td>
		  		</tr>
		  		<tr>
		  		<td align="right" >
		  		<s:checkbox key="dataCatalog.visible" fieldValue="true" onchange="changeStatus();"/>
		  		</td>
		  		<td class="listwhitetext"><fmt:message key="dataCatalog.configerable"/></td>
		  		</tr>
				</table>
				</fieldset>
				</td>
				<td rowspan="3"></td>		  		
		  		</tr>		  			  	
			</tbody>
			</table>		  
				<table class="detailTabLabel" border="0">
				  <tbody>
				  <tr>
				  <td align="left" style="padding-left:7px;"><b><fmt:message key="dataCatalog.description"/></b></td>
				  </tr>	
				  	<tr>
		  			<td align="left" style="padding-left:7px;"><s:textarea name="dataCatalog.description"  onkeypress="return imposeMaxLength(this,1000);"  cssClass="textarea" rows="4" cols="89" readonly="false"/></td>
		  			</tr>
		  			<tr>
		  	<td  height="10px" ></td>
		  	  	
		  	</tr>
	            </tbody>
			 </table>		  		
		      	  
		     </td>
       	  	</tr>		  	
		  </tbody>
		  </table>		  
		  </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		 
		 	  
		  <table class="detailTabLabel" border="0" >
		  <tbody> 	
		  	<tr>
		  	<td align="left" height="3px"></td>
		  	</tr>	
						<tr>
											<fmt:formatDate var="serviceCreatedOnFormattedValue" value="${dataCatalog.createdOn}"	pattern="${displayDateTimeEditFormat}" />
											<td align="right" class="listwhitetext" style="width:50px"><b>
											<fmt:message key='dataCatalog.createdOn' /></b></td>
											<s:hidden name="dataCatalog.createdOn" value="${serviceCreatedOnFormattedValue}" />
											<td>
											<fmt:formatDate	value="${dataCatalog.createdOn}" pattern="${displayDateTimeFormat}" /> 
											<td align="right" class="listwhitetext" style="width:70px"><b>
											<fmt:message key='dataCatalog.createdBy' /></b></td>
											<c:if test="${not empty dataCatalog.id}">
												<s:hidden name="dataCatalog.createdBy" />
												<td><s:label name="createdBy" value="%{dataCatalog.createdBy}" /></td>
											</c:if>
											<c:if test="${empty dataCatalog.id}">
												<s:hidden name="dataCatalog.createdBy" value="${pageContext.request.remoteUser}" />
												<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
											</c:if>
											<td align="right" class="listwhitetext" style="width:75px">
											<b><fmt:message	key='dataCatalog.updatedOn' /></b></td>
											<fmt:formatDate var="serviceUpdatedOnFormattedValue" value="${dataCatalog.updatedOn}" pattern="${displayDateTimeEditFormat}" />
											<s:hidden name="dataCatalog.updatedOn" value="${serviceUpdatedOnFormattedValue}" />
											<td>
											<fmt:formatDate	value="${dataCatalog.updatedOn}" pattern="${displayDateTimeFormat}" />
											<td align="right" class="listwhitetext" style="width:75px"><b>
											<fmt:message key='dataCatalog.updatedBy' /></b></td>
											<c:if test="${not empty dataCatalog.id}">
												<s:hidden name="dataCatalog.updatedBy" />
												<td style="width:85px;"><s:label name="updatedBy" value="%{dataCatalog.updatedBy}" /></td>
											</c:if>
											<c:if test="${empty dataCatalog.id}">
												<s:hidden name="dataCatalog.updatedBy" value="${pageContext.request.remoteUser}" />
												<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
											</c:if>
										</tr><tr><td></td></tr>
		   <tr>
		  		<td align="left">
        		<s:submit cssClass="cssbutton1" type="button"  key="button.save" onclick= "return check('save');"/>  
        		</td>       
        		<td align="left">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td> 	
       	  	</tr>		  	
		  </tbody>
		  </table>
</div>
<s:hidden name="dataCatalog.corpID"/>
<s:hidden name="dataCatalog.id"/>
<s:hidden name="dataCatalog.rulesFiledName"/>
</s:form>
