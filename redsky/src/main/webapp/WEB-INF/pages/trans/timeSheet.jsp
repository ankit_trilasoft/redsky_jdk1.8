<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="<fmt:message key='timeSheet.heading'/>"/> 
<title><fmt:message key="timeSheet.title"/></title> 
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->

<script language="javascript" type="text/javascript"> 
function requestedWorkCrews(targetElement){
	// alert(targetElement.checked);
	var requestedCW = document.forms['timeSheetList'].elements['requestedCrews'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' ){
			document.forms['timeSheetList'].elements['requestedCrews'].value = targetElement.value;
		}else{
			if(requestedCW.indexOf(targetElement.value)>=0){}
			else{
				requestedCW = requestedCW + ',' +targetElement.value;
			}
			document.forms['timeSheetList'].elements['requestedCrews'].value = requestedCW.replace(',,',",");
		}
	}else{
		if(requestedCW.indexOf(targetElement.value)>=0){
			requestedCW = requestedCW.replace(targetElement.value,"");
		}
		document.forms['timeSheetList'].elements['requestedCrews'].value = requestedCW.replace(',,',",");
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedCrews'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedCrews'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedCrews'].value = (document.forms['timeSheetList'].elements['requestedCrews'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedCrews'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedCrews'].value.length -1){
			document.forms['timeSheetList'].elements['requestedCrews'].value = (document.forms['timeSheetList'].elements['requestedCrews'].value).substring(0,document.forms['timeSheetList'].elements['requestedCrews'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
		if(document.forms['timeSheetList'].elements['requestedCrews'].value == ''){
			document.forms['timeSheetList'].elements['absent'].disabled = true;
		}else{
			document.forms['timeSheetList'].elements['absent'].disabled = false;
		}
	}else{
		document.forms['timeSheetList'].elements['assign'].disabled = false;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = false;
		document.forms['timeSheetList'].elements['absent'].disabled = true;
	}
}
 </script>

<script language="javascript" type="text/javascript">  
function requestedWorkTickets(targetElement){
	// alert(targetElement.checked);
	var requestedWT = document.forms['timeSheetList'].elements['requestedWorkTkt'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = targetElement.value;
		}else{
			if(requestedWT.indexOf(targetElement.value)>=0){}
			else{
				requestedWT = requestedWT + ',' +targetElement.value;
			}
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = requestedWT.replace(',,',",");
		}
	}else{
		if(requestedWT.indexOf(targetElement.value)>=0){
			requestedWT = requestedWT.replace(targetElement.value,"");
		}
		document.forms['timeSheetList'].elements['requestedWorkTkt'].value = requestedWT.replace(',,',",");
	}
	
	if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedWorkTkt'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = (document.forms['timeSheetList'].elements['requestedWorkTkt'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedWorkTkt'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedWorkTkt'].value.length -1){
			document.forms['timeSheetList'].elements['requestedWorkTkt'].value = (document.forms['timeSheetList'].elements['requestedWorkTkt'].value).substring(0,document.forms['timeSheetList'].elements['requestedWorkTkt'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
		if(document.forms['timeSheetList'].elements['requestedCrews'].value == ''){
			document.forms['timeSheetList'].elements['absent'].disabled = true;
		}else{
			document.forms['timeSheetList'].elements['absent'].disabled = false;
		}
	}else{
		document.forms['timeSheetList'].elements['assign'].disabled = false;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = false;
		document.forms['timeSheetList'].elements['absent'].disabled = true;
	}
	
}
 </script>
<script language="javascript" type="text/javascript">  
function removeTimeSheets(targetElement){
	var requestedTS = document.forms['timeSheetList'].elements['requestedTimeSt'].value;
	if(targetElement.checked){
		if(document.forms['timeSheetList'].elements['requestedTimeSt'].value == '' ){
			document.forms['timeSheetList'].elements['requestedTimeSt'].value = targetElement.value;
		}else{
			if(requestedTS.indexOf(targetElement.value)>=0){}
			else{
				requestedTS = requestedTS + ',' +targetElement.value;
			}
			document.forms['timeSheetList'].elements['requestedTimeSt'].value = requestedTS.replace(',,',",");
		}
	}else{
		if(requestedTS.indexOf(targetElement.value)>=0){
			requestedTS = requestedTS.replace(targetElement.value,"");
		}
		document.forms['timeSheetList'].elements['requestedTimeSt'].value = requestedTS.replace(',,',",");
	}

	if(document.forms['timeSheetList'].elements['requestedTimeSt'].value == ',' ){
			document.forms['timeSheetList'].elements['requestedTimeSt'].value = '';
	}
	if((document.forms['timeSheetList'].elements['requestedTimeSt'].value).indexOf(",") == 0){
			document.forms['timeSheetList'].elements['requestedTimeSt'].value = (document.forms['timeSheetList'].elements['requestedTimeSt'].value).substring(1);
	}
	if(document.forms['timeSheetList'].elements['requestedTimeSt'].value.lastIndexOf(",") == document.forms['timeSheetList'].elements['requestedTimeSt'].value.length -1){
			document.forms['timeSheetList'].elements['requestedTimeSt'].value = (document.forms['timeSheetList'].elements['requestedTimeSt'].value).substring(0,document.forms['timeSheetList'].elements['requestedTimeSt'].value.length-1)
	}
	
	if(document.forms['timeSheetList'].elements['requestedTimeSt'].value == ''){
		document.forms['timeSheetList'].elements['remove'].disabled = true;
		//document.forms['timeSheetList'].elements['assigntogether'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['remove'].disabled = false;
		//document.forms['timeSheetList'].elements['assigntogether'].disabled = false;
	}
	requestedTS = document.forms['timeSheetList'].elements['requestedTimeSt'].value;
	
	if(requestedTS.indexOf(',') > 0){
		document.forms['timeSheetList'].elements['assigntogether'].disabled = false;
	}else{
		document.forms['timeSheetList'].elements['assigntogether'].disabled = true;
	}

}

 </script>
 
 <script>
 function buildTimeSheet(){
 	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}else{
 		submit_form();
 		document.forms['timeSheetList'].elements['typeSubmit'].value='B';
 		document.forms['timeSheetList'].action = 'buildTimeSheet.html';
 		document.forms['timeSheetList'].submit();
 	}
 }
 
 function groupAssignTimeSheet(){
 if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}else{
 		submit_form();
 		document.forms['timeSheetList'].elements['typeSubmit'].value='G';
 		document.forms['timeSheetList'].action = 'groupAssignTimeSheet.html';
 		document.forms['timeSheetList'].submit();
 	}
 
 }
 function removeTimeSheet(){
 	document.forms['timeSheetList'].action = 'removeTimeSheet.html';
 	document.forms['timeSheetList'].submit();
 	//self.document.location.reload(true);
 	//document.location.reload();
 }

 function AssignTimeSheetTogether(){
	 document.forms['timeSheetList'].action = 'assignTogether.html';
	 document.forms['timeSheetList'].submit();	
 }
 
 function absentsTimeSheet(){
 	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetList'].elements['workDt'].select();
 		return false;
 	}else{
 		document.forms['timeSheetList'].elements['typeSubmit'].value='A';
 		document.forms['timeSheetList'].action = 'absentsTimeSheet.html?filter=ShowAll';
 		document.forms['timeSheetList'].submit();
 	}
 }
 </script>
 
 <script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['timeSheetList'].elements['workDt'].value = "";
			document.forms['timeSheetList'].elements['wareHse'].value = "";
			document.forms['timeSheetList'].elements['tktWareHse'].value = "";
			document.forms['timeSheetList'].elements['crewNm'].value = "";
			document.forms['timeSheetList'].elements['tkt'].value = "";
}

function editTimeSheets(targetElement){
	var id = targetElement;
	document.forms['timeSheetList'].action = 'editTimeSheet.html?from=list&id='+id;
	document.forms['timeSheetList'].submit();
}

function isInteger()
{   var i;
	var s = document.forms['timeSheetList'].elements['tkt'].value;
	
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Only numbers are allowed in ticket field");
        return false;
        }
    }
    return true;
}

function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}

function checkCondition(){
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	
 	if(document.forms['timeSheetList'].elements['tktWareHse'].value == ""){
 		alert("Select Ticket WareHouse  to continue.....");
 		return false;
 	}
 	
}

function ShowAll()
{
	if(document.forms['timeSheetList'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value == ""){
 		alert("Select WareHouse date to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetList'].elements['wareHse'].value != "" && document.forms['timeSheetList'].elements['wareHse'].value != ""){
 		
 		document.forms['timeSheetList'].action = 'searchCrewTickets.html?filter=ShowAll';
 		document.forms['timeSheetList'].submit();
 	}
}

function excludeEearningOppertunity(){
 
 		document.forms['timeSheetList'].action = 'excludeEearningOppertunity.html?filter=excludeEearningOppertunity';
 		document.forms['timeSheetList'].submit();
 }
 
 function doneForDay(){
 
 		document.forms['timeSheetList'].action = 'doneForDay.html?filter=doneForDay';
 		document.forms['timeSheetList'].submit();
 }
 
 function filterLunch(){
 
 		document.forms['timeSheetList'].action = 'filterLunch.html?filter=filterLunch';
 		document.forms['timeSheetList'].submit();
 }
 
 function openTimeSheets(){
 
 	document.forms['timeSheetList'].action = 'openTimeSheets.html?filter=openTimeSheets';
 	document.forms['timeSheetList'].submit();
}

function setFlagValue(){
	document.forms['timeSheetList'].elements['hitFlag'].value = '1';
}

function selectTicketWarehouse()
{
	document.forms['timeSheetList'].elements['tktWareHse'].value=document.forms['timeSheetList'].elements['wareHse'].value;
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}

table {
font-size:1em;
margin:0pt 0pt 0em;
padding:0pt;
}
.table td, .table th, .tableHeaderTable td {
border:1px solid #E0E0E0;
padding:0.3em;
}

.listwhitetext-crew {
color:#000000;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
background-color:none;

}

.listwhitetext-hcrew {
color:#003366;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
text-decoration:none;
background-color:none;
padding:0px 0px 2px 0px;
!padding-bottom:3px;
}

.radiobtn {
margin:0px 0px 2px 0px;
!padding-bottom:5px;
!margin-bottom:5px;
}
</style>
</head>

<s:form cssClass="form_magn" name="timeSheetList" action="searchCrewTickets.html?filter=ShowAll" onsubmit="return submit_form()" >

<s:hidden name="requestedWorkTkt" value=""/>
<s:hidden name="requestedCrews" value=""/>
<s:hidden name="requestedTimeSt" value=""/>
<s:hidden name="editable" value="${editable}"/>
<c:set var="filter" value="<%=request.getParameter("filter") %>"/>
<s:hidden name="filter" value="<%=request.getParameter("filter") %>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="typeSubmit" value=""/>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<table class="table" style="width:100%;clear:both;">
		<thead>
			<tr>
				<th>Work Date</th>
				<!--<th><fmt:message key="workTicket.service" /></th>-->
				<th>Crew&nbsp;WareHouse</th>
				<th>Crew&nbsp;Name</th>
				<th>Ticket&nbsp;WareHouse</th>
				<!--<th>Shipper</th>
				--><th><fmt:message key="workTicket.ticket" /></th>
			<th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				
				<td align="left"><s:textfield cssClass="input-text" id="date1" name="workDt" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="calendern_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				<!--<td><s:textfield name="serv" required="true" cssClass="input-text" size="7"/></td>
				--><td><s:select name="wareHse" list="%{wareHouse}" cssStyle="width:160px" cssClass="list-menu" headerKey="" headerValue="" onchange="selectTicketWarehouse();"/></td>
				<td><s:textfield name="crewNm" required="true" cssClass="input-text" size="22"/></td>
				<!--<td><s:textfield name="shipper" required="true" cssClass="input-text" size="7"/></td>
				-->
				<td><s:select name="tktWareHse" list="%{wareHouse}" cssStyle="width:160px" cssClass="list-menu" headerKey="" headerValue=""/></td>
				<td><s:textfield name="tkt" required="true" cssClass="input-text" size="22" onkeydown="return onlyNumsAllowed(event);"/></td>
				<td><s:submit cssClass="cssbutton" method="search" cssStyle="width:55px; height:25px" key="button.search" onclick="return checkCondition();"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/></td> 
			</tr>
			
		</tbody>
	</table>

<table style="width:100%; height:250px; " >
<tbody>
<tr>
<td align="left" >
<div style="!margin-top:0px; width:100%; height:250px;overflow-x:auto; overflow-y:auto;">
	<div id="otabs" style=" margin-bottom:50px; margin-top:20px;">
		  <ul>
		    <li><a class="current"><span>Crew List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="availableCrews" class="table" requestURI="searchCrewTickets.html" id="PayrollssList" defaultsort="1" style="margin-top: -19px;!margin-top:0px" >   
   <display:column sortable="true" title="Name" ><c:out value="${PayrollssList.lastName}" />,&nbsp;<c:out value="${PayrollssList.firstName}" /></display:column>
   <display:column title="Whse" maxLength="20"><c:out value="${PayrollssList.warehouse}" /></display:column>
   <display:column title="Crew" maxLength="20" ><c:out value="${PayrollssList.typeofWork}" /></display:column>
   <display:column title="Hours" headerClass="containeralign"><div align="right"><fmt:formatNumber type="number"  
            value="${PayrollssList.availableHours}"  maxFractionDigits="2" groupingUsed="true"/></div></display:column>
   <display:column title="Member"><c:out value="${PayrollssList.isUnioun}" /></display:column>
   	<%--<c:if test="${PayrollssList.doneForTheDay==true }">
	<display:column  title="Done"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
	</c:if>
	<c:if test="${PayrollssList.doneForTheDay!=true }">
	<display:column  title="Done"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
	</c:if>
	--%>
	 <c:if test="${editable=='Yes'}"> 
   	<display:column title="Select" style="text-align: center" ><s:checkbox name="check1" fieldValue="${PayrollssList.id}" onclick="requestedWorkCrews(this);"/></display:column>
	</c:if>
	
</display:table>  
</div>
</td>

<td align="left">
<div style="width:100%;height:250px;overflow-x:auto; !margin-top:0px;">

	<div id="otabs" style="width:570px; margin-bottom:50px; margin-top:20px;">
		  <ul>
		    <li><a class="current"><span>Work Ticket List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	<s:set name="workTickets" value="workTickets" scope="request" />
	<display:table name="workTickets" class="table" requestURI="searchCrewTickets.html" id="workTicketList" defaultsort="2" style="width:100%;margin-top: -19px; !margin-top:0px">
		 <c:if test="${editable=='Yes'}"> 
		<display:column title="Select"><s:checkbox name="check2" fieldValue="${workTicketList.id}" onclick="requestedWorkTickets(this);"/></display:column>
		</c:if>
		<display:column property="ticket" sortable="true" style="text-align:right" titleKey="workTicket.ticket" url="/editWorkTicketUpdate.html" paramId="id" paramProperty="id"/>
		<display:column title="Shipper"><c:out value="${workTicketList.lastName}" />,&nbsp;<c:out value="${workTicketList.firstName}" /></display:column>
		<display:column property="date1" titleKey="workTicket.date1" style="width:100px;" format="{0,date,dd-MMM-yy}" />
		<display:column property="date2" titleKey="workTicket.date2" style="width:100px;" format="{0,date,dd-MMM-yy}" />
		<display:column headerClass="containeralign" property="estimatedWeight" style="text-align:right;" title="Est&nbsp;Wght" />
		<display:column property="service" titleKey="workTicket.service" />
		<display:column property="warehouse" title="Whse" />
		<%--  this was done to make the page links customized.
		<display:setProperty name="paging.banner.page.link" value='<a href="searchCrewTickets.html{1}" title="Go to page {0}">{0}</a>'/>   
	    <display:setProperty name="paging.banner.first" value='<span class="pagelinks">[First/Prev] {0} [ <a href="searchCrewTickets.html{3}">Next</a>/ <a href="searchCrewTickets.html{4}">Last</a>]</span>'/>   
    	--%>
	</display:table>
</div>
</td>
</tr>
<tr>
<td valign="middle" colspan="2">
<table>
<tr>
<td></td>
<td colspan="4" style="padding-left:250px;" ><input type="button" name="absent" value="Absent" style="width:70px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="return absentsTimeSheet();"/>
<input type="button" name="assign" value="Assign to tickets" style="width:120px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="buildTimeSheet();"/>
<input type="button" name="groupAssign" value="Group Assign" style="width:120px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="groupAssignTimeSheet();"/></td>
</tr>
</table>

</td>
</tr>
</tbody>
</table>
<table style="width:100%;">
<tbody>
<tr>
<td align="left">
<div style="width:100%;overflow-x:auto; !margin-top:-8px">
	<div id="otabs" style="width:970px; !margin-top:0px;">
		  <ul>
		    <li><a class="current"><span>Time Sheet</span></a></li>
		     <li class="listwhitetext"  style="background: transparent; font-size:12px;"><b># Time Sheets</b><b>{&nbsp;<font color="red">${countTimeSheet}</font>&nbsp;}</b></li>
		 		<li style="background: transparent;">
		 		<table border="0"  width="713px" cellpadding="0" cellspacing="2" class="timesheet_band" style="padding-left:1px;padding-top:1px; padding-bottom:1px;padding-right:8px;!width:720px; !margin-top:0px">
<tr>
<td class="listwhitetext" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;"><b>Display Filter:</b> </td>
<c:choose>
	<c:when test="${filter == 'excludeEearningOppertunity'}" >
<td align="right"><input name="button1" type="radio" value="true" checked="checked" onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Earning Opps</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Done Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Open Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Lunch</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Show All</b></h6></td> 
</c:when>
	<c:when test="${filter == 'doneForDay'}" >
<td align="right"><input name="button1" type="radio" value="true"  onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Earning Opps</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" checked="checked"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Done Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Open Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Lunch</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Show All</b></h6></td> 
</c:when>
	<c:when test="${filter == 'openTimeSheets'}" >
<td align="right"><input name="button1" type="radio" value="true"  onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Earning Opps</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Done Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" checked="checked" onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Open Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Lunch</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Show All</b></h6></td> 
</c:when>
	<c:when test="${filter == 'filterLunch'}" >
<td align="right"><input name="button1" type="radio" value="true"  onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Earning Opps</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Done Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Only Open Sheets</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" checked="checked" onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Hide Lunch</b></h6></td> 
<td align="right"><input name="button1" type="radio" value="true" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><h6><b>Show All</b></h6></td> 
</c:when>
	<c:when test="${filter == 'ShowAll'}" >
<td align="right" valign="top" ><input name="button1" class="radiobtn" type="radio" value="true" checked="checked" onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="top" ><h6><b>Hide Earning Opps</b></h6></td> 
<td align="right" valign="top"><input name="button1" class="radiobtn" type="radio" value="true"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="top" ><h6><b>Only Done Sheets</b></h6></td> 
<td align="right" valign="top"><input name="button1" class="radiobtn" type="radio" value="true"  onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="top" ><h6><b>Only Open Sheets</b></h6></td> 
<td align="right" valign="top"><input name="button1" class="radiobtn" type="radio" value="true"  onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="top"><h6><b>Hide Lunch</b></h6></td> 
<td align="right" valign="top"><input name="button1" class="radiobtn" type="radio" value="true" checked="checked" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="top" ><h6><b>Show All</b></h6></td> 
</c:when>
<c:otherwise>
<td align="right" valign="top"><input name="button1" class="radiobtn" type="radio" value="true"  onclick="excludeEearningOppertunity();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom" ><b>Hide Earning Opps</b></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="doneForDay();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom" ><b>Only Done Sheets</b></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="openTimeSheets();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom" ><b>Only Open Sheets</b></td> 
<td align="right"><input name="button1" type="radio" value="true"  onclick="filterLunch();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom" ><b>Hide Lunch</b></td> 
<td align="right"><input name="button1" type="radio" value="true" onclick="ShowAll();" /></td> 
<td align="left" class="listwhitetext-crew" valign="bottom"><b>Show All</b></td> 
</c:otherwise>
</c:choose>
<!--<input type="button" name="EEO" value="Hide Earning Opps" style="width:140px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="excludeEearningOppertunity();"/>
<input type="button" name="DFD" value="Only Done Sheets" style="width:140px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="doneForDay();"/>
<input type="button" name="OTS" value="Only Open Sheets" style="width:140px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="openTimeSheets();"/>
<input type="button" name="FL" value="Hide Lunch" style="width:100px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="filterLunch();"/>
<input type="button" name="Show All" value="Show All" style="width:70px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="ShowAll();"/>

--></tr>

</table>
		 		
		 		
		 		</li>
		  </ul>
		  
		</div>
		<div class="spnblk" style="margin-top:15px">&nbsp;</div>
		<display:table name="timeSheets" class="table" requestURI="searchCrewTickets.html" id="timeSheetList" defaultsort="2" style="width:100%;margin-top: -20px;">
		
		
		<display:column titleKey="timeSheet.workDate" style="width:80px;">
		<a style="cursor:pointer" onclick="editTimeSheets(${timeSheetList.id})">
		<fmt:formatDate pattern="dd-MMM-yy" value="${timeSheetList.workDate}"/>
		</a>
		</display:column>
		
		
		
		
		<display:column titleKey="timeSheet.crewName" group="1" style="width: 130px"><c:out value="${timeSheetList.crewName}" /></a></display:column>
		<display:column  title="Done"><div style="text-align:center">
		<c:if test="${timeSheetList.doneForTheDay==true }">
		<img align="middle" src="${pageContext.request.contextPath}/images/tick01.gif" /> 
		</c:if>
		<c:if test="${timeSheetList.doneForTheDay==false }">
		<img align="middle" src="${pageContext.request.contextPath}/images/cancel001.gif" />
		</c:if>
		</div>
		</display:column>
		
		<display:column titleKey="timeSheet.action" ><c:out value="${timeSheetList.action}" /></display:column>
		<display:column title="Calc Code" style="width: 60px;"><c:out value="${timeSheetList.calculationCode}" /></display:column>
		
		<display:column title="Dist Code" headerClass="containeralign" style="width: 60px;" ><div style="text-align:right"><c:out value="${timeSheetList.distributionCode}" /></div></display:column>
		
		<display:column headerClass="containeralign" titleKey="workTicket.ticket" ><div style="text-align:right"><c:out value="${timeSheetList.ticket}" /></div></display:column>
		<display:column property="shipper" titleKey="timeSheet.shipper" />
		<display:column headerClass="containeralign" title="Begin&nbsp;Hrs" ><div style="text-align:right"><c:out value="${timeSheetList.beginHours}" /></div></display:column>
		<display:column headerClass="containeralign" title="End&nbsp;Hrs" ><div style="text-align:right"><c:out value="${timeSheetList.endHours}" /></div></display:column>
		<display:column headerClass="containeralign" title="Reg&nbsp;Hrs" ><div style="text-align:right"><c:out value="${timeSheetList.regularHours}" /></div></display:column>
        <display:column headerClass="containeralign" titleKey="timeSheet.overTime" ><div style="text-align:right">
       		<c:out value="${timeSheetList.overTime}" />
        </div></display:column>
		<display:column headerClass="containeralign" title="Rev&nbsp;Share" ><div style="text-align:right"><c:out value="${timeSheetList.paidRevenueShare}" /></div></display:column>
        <c:if test="${editable=='Yes'}">    
        <display:column title="Select" ><div style="text-align:center"><s:checkbox name="check3" fieldValue="${timeSheetList.id}" onclick="removeTimeSheets(this)"/></div></display:column>
		</c:if>
	</display:table>
	</div>
</td>
</tr>
<tr>
<td align="left" valign="middle">
<table>
<tr>
<td><input type="button" name="remove" value="Remove" style="width:70px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="removeTimeSheet();"/></td>
<td><input type="button" name="assigntogether" value="Assign Together" style="width:120px; height:25px; padding:3px; height:28px; font-variant:normal;" onclick="AssignTimeSheetTogether();"/></td></td>
</tr>

</table>
</td>
</tr>
</tbody>
</table>
</s:form>


<script>
try{
if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' || document.forms['timeSheetList'].elements['requestedWorkTkt'].value == '' ){
		document.forms['timeSheetList'].elements['assign'].disabled = true;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['assign'].disabled = false;
		document.forms['timeSheetList'].elements['groupAssign'].disabled = false;
	}
}
catch(e){}	
try{
if(document.forms['timeSheetList'].elements['requestedCrews'].value == '' ){
		document.forms['timeSheetList'].elements['absent'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['absent'].disabled = false;
	}
	}
	catch(e){}
try{
if(document.forms['timeSheetList'].elements['requestedTimeSt'].value == ''){
		document.forms['timeSheetList'].elements['remove'].disabled = true;
		document.forms['timeSheetList'].elements['assigntogether'].disabled = true;
	}else{
		document.forms['timeSheetList'].elements['remove'].disabled = false;
		document.forms['timeSheetList'].elements['assigntogether'].disabled = false;
	}
	}
	catch(e){}
try{
<c:if test="${editable=='No'}">

	document.forms['timeSheetList'].elements['remove'].disabled = true;
	document.forms['timeSheetList'].elements['assign'].disabled = true;
	document.forms['timeSheetList'].elements['groupAssign'].disabled = true;
	document.forms['timeSheetList'].elements['absent'].disabled = true;
	document.forms['timeSheetList'].elements['assigntogether'].disabled = true;
	
</c:if>
}
catch(e){}
</script>

<script type="text/javascript">
	RANGE_CAL_1 = new Calendar({
	        inputField: "date1",
	        dateFormat: "%d-%b-%y",
	        trigger: "calendern_trigger",
	        bottomBar: true,
	        animation:true,
	        onSelect: function() {                             
	            this.hide();
   	 	}
	});
</script>