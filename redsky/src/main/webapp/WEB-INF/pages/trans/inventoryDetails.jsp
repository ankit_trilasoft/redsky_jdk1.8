<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head>
<title>Inventory Search</title>
<meta name="heading" content="Inventory Search" />
<style>
	<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<script type="text/javascript">
function search(){
var atricle = document.getElementById('atricle');
var room = document.getElementById('room');
window.location="searchInventoryData.html?";
}

function readxml(){
window.location="readXmlInventory.html?sid=${sid}";
}
<c:if test="${hitflag=='1'}">
 <c:redirect url="/inventoryDetails.html?sid=${sid}"/>
</c:if>

function clear_fields(){
document.forms['searchInventoryData'].elements['article'].value="";
document.forms['searchInventoryData'].elements['room'].value="";
}
function showItemPerPiece(){
	try{
		<c:forEach items="${inventoryPackingList}" var="listItems"> 
			var tempItem = '${listItem.item}';
			//var itemArray[] =
		</c:forEach>
	}catch(e){}
}

function imageList(sid,id,position){
	var url="inventoryPackingImageList.html?sid="+sid+"&id="+id+"&decorator=simple&popup=true";
	ajax_SoTooltip(url,position);	
}


var flagValue = 0;
var val = 0;
var lastId ='';
function displayResult(regNum,pieceid,controlFlag,position,id)
{
	if(flagValue==0){
		var serviceOrderid=document.forms['searchInventoryData'].elements['sid'].value;
		var table=document.getElementById("inventoryPackingList");
		var rownum = document.getElementById(id);
		//alert(rownum+"--rownum")
		lastId = rownum.id;
		//alert(lastId+"--lastId")
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		new Ajax.Request('inventorylistId.html?pieceID='+pieceid+'&sid='+serviceOrderid+'&controlFlag='+controlFlag+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				     //alert(response);
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){
					    //alert('Something went wrong...') 
					    }
				  });
		
		     row.innerHTML = "<td colspan=\"3\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		     return 1;
	}
 	if(flagValue==1){
		document.getElementById(val).parentNode.removeChild(document.getElementById(val));
		flagValue = 0;
		var serviceOrderid=document.forms['searchInventoryData'].elements['sid'].value;
		var table=document.getElementById("inventoryPackingList");
		var rownum = document.getElementById(id);
		if(lastId!=rownum.id){
		lastId = rownum.id;
		var myrow = rownum.parentNode;
		var newrow = myrow.parentNode.rowIndex;
		val = newrow+1;
		var row=table.insertRow(val);
		row.setAttribute("id",val);
		
		new Ajax.Request('inventorylistId.html?pieceID='+regNum+'&sid='+serviceOrderid+'&controlFlag='+controlFlag+'&decorator=simple&popup=true',
				  {
			
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var container = $(regNum);
				      container.update(response);
				      container.show();
				    },
				    onFailure: function(){ alert('Something went wrong...') }
				  });
		
		     row.innerHTML = "<td colspan=\"3\"><div id="+regNum+"></div></td>";
		     flagValue = 1;
		}
	     return 1;
	} 
			
}


function applyImage(){
	//alert("images")
	 var pieceFlag="";
	<c:forEach var="entry" items="${inventoryPackingList}">
	 var id="scope${entry.id}";
	 //alert("id--"+id)
	 if(pieceFlag=="" || pieceFlag != '${entry.pieceID}' )
		 {
		// alert("pieceFlag--"+pieceFlag)
		 pieceFlag="${entry.pieceID}";
		// alert("pieceFlag--"+pieceFlag)
		 //alert("entry.pieceID--"+${entry.pieceID})
	  if(${entry.pieceID != null && entry.pieceID != ''})
		 {
		  //alert("entry.pieceID--"+${entry.pieceID})
	     document.getElementById(id).src= '${pageContext.request.contextPath}/images/file_edit.png'; 
		 }else{
			 document.getElementById(id).src = '${pageContext.request.contextPath}/images/file_edit.png';
		 } }
</c:forEach> 
}

</script>
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:95%;
!width:95%;
}
</style>
</head>
<div id="otabs" style="margin-bottom:25px;">
		  <ul>
		    <li ><a class="current"><span>Search</span></a></li>
		    <li><a href="editServiceOrderUpdate.html?id=${sid}" /><span>S/O Details</span></a></li>
		  </ul>
		</div>
<div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>
    <div class="center-content" style="padding-left:25px;">
    <s:form method="post" action="searchInventoryData" name="searchInventoryData">
     <s:hidden name="sid" value="${sid}"/>
    <table class="table">
  <thead>
  <tr>
    <th>Item</th>
   <th>Room</th>
   </tr>
    </thead>	
		<tbody>
		<tr>
			<td width="20" align="left">
			    <s:textfield cssClass="input-text" name="article" value="${article}" size="50" maxlength="25"/>
			</td>
			<td width="20" align="left">
			    <s:select cssClass="list-menu" cssStyle="width:200px;" value="${room}"  name="room" list="%{roomType}"    headerKey="" headerValue=""/>
			</td>
			</tr>
		</tbody>
</table>
<table cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px; width: 99%;">
 <tr>
	<td align="right"><s:submit cssClass="cssbuttonA" cssStyle="width:58px;"  key="button.search" />
	<input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/></td>
	</tr>
	</table>
   </s:form>
   <div style="height:15px;">&nbsp;</div>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 
      <div id="otabs" style="margin-top:10px; margin-bottom:0px;" >
		  <ul>
		    <li><a class="current"><span>Inventory List</span></a></li>		   
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
		  <c:set var="rowCount"  value="1" />
		  <c:set var="quantitysum" value="0" />
		  <c:set var="volumesum" value="0" />
		 <c:if test="${inventoryPackingList!='[]'}"> 
	<display:table name="inventoryPackingList" class="table" requestURI="" id="inventoryPackingList"   defaultsort="1" style="width:100%;" >
	<display:column title="#" style="width: 10px; margin: 0px; padding: 0px 2px 0px 2px;">
	<a href="inventoryDetailsFrompieceId.html?sid=${sid }&pieceID=${inventoryPackingList.pieceID}" ><img id="scope${inventoryPackingList.id}" width="16" height="18"></img></a>
	</display:column> 
	<display:column  sortable="true"  title="SrNo." paramId="id" paramProperty="id" sortProperty="id"   >
  		${inventoryPackingList_rowNum}
  	</display:column> 
	<display:column title="Item"  paramId="id" paramProperty="id">
	   	<a href="editInventoryDetails.html?id=${inventoryPackingList.id}&sid=${sid}" >${inventoryPackingList.item}</a>
	</display:column>
	<display:column  property="mode" title="Mode"/>
	<display:column property="location" title="Room" sortable="true"  />
	<display:column property="itemQuantity" title="Quantity" sortable="true" headerClass="containeralign" style="text-align:right;"  />
	<c:set var="quantitysum" value="${inventoryPackingList.itemQuantity+quantitysum }" />
	<display:column property="volume" title="Volume" headerClass="containeralign" style="text-align:right;"/>
	<c:set var="volumesum" value="${inventoryPackingList.volume+volumesum}" />
	<display:column property="weight" title="Weight" headerClass="containeralign" style="text-align:right;"/>
	<display:column  property="value" title="Valuation" headerClass="containeralign" style="text-align:right;" />
	<display:column  property="conditon" title="Condition" />
	<display:column  title="Image" sortable="true"  >
	<c:if test="${inventoryPackingList.imagesAvailablityFlag!=0 && inventoryPackingList.imagesAvailablityFlag==1000 && inventoryPackingList.imagesAvailablityFlag!=2000  }">
 		<img id="userImage" src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="imageList('${sid}','${inventoryPackingList.id}',this)""/>
	</c:if>
	</display:column>
	 <display:footer >
            <tr> 
           
 	  	          <td align="right"  colspan="5" ><b><div align="right">Total</div></b></td>
		          <td align="right" style="height:22px;" class="tdheight-footer"> <fmt:formatNumber type="number"  maxFractionDigits="0" minFractionDigits="0"
                  groupingUsed="true" value="${quantitysum}"  />
		          </td>
		          <td align="right" ><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum}"  />
		          </td>
		          <td align="left" colspan="5"></td>
           </tr>
	</display:footer>
	</display:table>   
        </c:if>
        <c:if test="${inventoryPackingList=='[]'}"> 
         <display:table name="inventoryPackingList" class="table" requestURI="" id="inventoryPackingList"   defaultsort="1" style="width:100%;" >
	<display:column title="#" style="width: 10px; margin: 0px; padding: 0px 2px 0px 2px;">
	<a href="inventoryDetailsFrompieceId.html?sid=${sid }&pieceID=${inventoryPackingList.pieceID}" ><img id="scope${inventoryPackingList.id}" width="16" height="18"></img></a>
	</display:column> 
	<display:column  sortable="true"  title="SrNo." paramId="id" paramProperty="id" sortProperty="id"   >
  		
  	</display:column> 
	<display:column title="Item"  paramId="id" paramProperty="id">
	   	<a href="editInventoryDetails.html?id=${inventoryPackingList.id}&sid=${sid}" ></a>
	</display:column>
	<display:column   title="Mode"/>
	<display:column  title="Room" sortable="true"  />
	<display:column  title="Quantity" sortable="true" headerClass="containeralign" style="text-align:right;"  />
	
	<display:column property="volume" title="Volume" headerClass="containeralign" style="text-align:right;"/>
	
	<display:column  title="Weight" headerClass="containeralign" style="text-align:right;"/>
	<display:column   title="Valuation" headerClass="containeralign" style="text-align:right;" />
	<display:column   title="Condition" />
	<display:column  title="Image" sortable="true"  >
	<c:if test="${inventoryPackingList.imagesAvailablityFlag!=0 && inventoryPackingList.imagesAvailablityFlag==1000 && inventoryPackingList.imagesAvailablityFlag!=2000  }">
 		<img id="userImage" src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="imageList('${sid}','${inventoryPackingList.id}',this)""/>
	</c:if>
	</display:column>
	 
	</display:table>   
         
         </c:if>
       <script>
       
       applyImage();
       </script>  
           
<!--<input type="button" class="cssbutton1" value="Read Xml's" style="width:120px; height:25px;" onclick="readxml();"/>

-->