package com.trilasoft.app.dao;

import java.util.List;

import com.trilasoft.app.model.UrlPattern;

public interface UrlPatternDao {

	public List<UrlPattern> listUrlPatterns() ;
	

} 
