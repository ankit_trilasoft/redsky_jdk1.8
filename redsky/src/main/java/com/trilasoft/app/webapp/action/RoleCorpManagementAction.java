package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RoleCorpManagementManager;

public class RoleCorpManagementAction extends BaseAction {
	private String gotoPageString;
	private Long id;
	private String sessionCorpID;
	private String corpID;
	private String name;
	private String category;
	private List<Role> roleList;
	private List companyCorpId;
	private List searchList;
	private List<String> usertype;
	private Role role;
	private RoleCorpManagementManager roleCorpManagementManager;
	private RefMasterManager refMasterManager;
	private CompanyManager companyManager;
	static final Logger logger = Logger.getLogger(ServiceOrderAction.class);
	Date currentdate = new Date();
	
	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public RoleCorpManagementAction(){
		System.out.println("Initialized RoleCorpManagementAction!!!");
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	String corpIds ="";
	public String getComboList(){
		companyCorpId = companyManager.getcorpID();
		Collections.sort(companyCorpId);
		int i = 0;
		for(Object str : companyCorpId){
			i++;
			String temp =  "'" + str.toString() +"',";
			if(i == companyCorpId.size()){
				temp = temp.replace(",", "");
			}
			corpIds += temp;
		}
		usertype = refMasterManager.findUserType(sessionCorpID, "USER_TYPE");
		if(!sessionCorpID.equalsIgnoreCase("TSFT"))
		{
		usertype.remove("AGENT");
		}
		Collections.sort(usertype);
		searchList = roleCorpManagementManager.onlyRoles(corpIds);
		Set set = new HashSet(searchList);
		searchList = new ArrayList(set);
		Collections.sort(searchList);
		return SUCCESS;
	}
	
	@SkipValidation
	public String corpRoleManagementList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList();
		roleList = roleCorpManagementManager.corpRoles(corpIds);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
		return SUCCESS;
	}
	private List selectedUsertype;
	@SkipValidation
	public String editCorpRoleManagement(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList();
		role = roleCorpManagementManager.fetchRoleDetail(id,sessionCorpID);
		String batchJob="";
		if(role.getCategory()==null){batchJob="";}else{batchJob=role.getCategory();}	
    	if((batchJob!=null)&&(!batchJob.equalsIgnoreCase(""))){
    		selectedUsertype = new ArrayList(); 
			String[] ac = batchJob.split(",");
			int arrayLength2 = ac.length;
			for (int k = 0; k < arrayLength2; k++) {
				String accConJobType = ac[k];
				selectedUsertype.add(accConJobType.trim());	    		
			}
    	 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String save(){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			getComboList();
			String batchJob="";
			if(role.getCategory()==null){batchJob="";}else{batchJob=role.getCategory();}	
	    	if((batchJob!=null)&&(!batchJob.equalsIgnoreCase(""))){
	    		selectedUsertype = new ArrayList(); 
				String[] ac = batchJob.split(",");
				int arrayLength2 = ac.length;
				for (int k = 0; k < arrayLength2; k++) {
					String accConJobType = ac[k];
					selectedUsertype.add(accConJobType.trim());	    		
				}
	    	 }
			if(role.getCorpID() == null || role.getCorpID().equals("")){
				String key = "Corp ID is a required field .";
				errorMessage(getText(key));
	            return INPUT;	
			}
			else if(role.getName() == null || role.getName().trim().equals("")){
				String key = "Role Name is a required field .";
				errorMessage(getText(key));
	            return INPUT;	
			}
			else{
				try{
					boolean isExists = roleCorpManagementManager.checkRoleForExistance(role.getName(),role.getCorpID(),role.getCategory());
					if(!isExists){
						roleCorpManagementManager.save(role);
						if(role.getId() != null){
							String key = "Role Information Updated Successfully.";
							saveMessage(getText(key));
						}else{
							String key = "Role Information Saved Successfully.";
							saveMessage(getText(key));
						}
					}else{
						String key = "Role Information Already Exists.";
						errorMessage(getText(key));
					}
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
					return SUCCESS;
				}
				catch(Exception e){
					e.printStackTrace();
					logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
					return ERROR;
				}
			}
	}
	
	@SkipValidation
	public String search(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList();
		roleList = roleCorpManagementManager.searchRole(name,corpID,category);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	
	@SkipValidation
	public String removeRole(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList();
		try{
			roleCorpManagementManager.removeForOtherCorpid(id);
		}catch(Exception e){
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<Role> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<Role> roleList) {
		this.roleList = roleList;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void setRoleCorpManagementManager(
			RoleCorpManagementManager roleCorpManagementManager) {
		this.roleCorpManagementManager = roleCorpManagementManager;
	}

	public List<String> getUsertype() {
		return usertype;
	}

	public void setUsertype(List<String> usertype) {
		this.usertype = usertype;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public List getCompanyCorpId() {
		return companyCorpId;
	}

	public void setCompanyCorpId(List companyCorpId) {
		this.companyCorpId = companyCorpId;
	}

	public List getSearchList() {
		return searchList;
	}

	public void setSearchList(List searchList) {
		this.searchList = searchList;
	}

	public List getSelectedUsertype() {
		return selectedUsertype;
	}

	public void setSelectedUsertype(List selectedUsertype) {
		this.selectedUsertype = selectedUsertype;
	}

}
