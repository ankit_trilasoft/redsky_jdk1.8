<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Cost Element Form</title>   
    <meta name="heading" content="Cost Element Form"/>   

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<style>
 span.pagelinks {
display:block;font-size:0.85em;margin-bottom:3px;!margin-bottom:1px;margin-top:-21px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
</style>

</head>
<s:form name="costElementForm" action="saveCostElement" method="post" onsubmit="return checkCostElement();">
<s:hidden name="costElement.id"/>
<s:hidden name="oldCostElement"/>
<div id="newmnav">
	<ul>		  	
	<li><a href="costElementList.html"><span>Cost Element List</span></a></li>
	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Cost Element<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${costElement.id}&tableName=costelement&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</ul>
	</div>
	<div class="spn">&nbsp;</div>
	<div id="Layer1" style="width:95%"> 
 <div id="content" align="center">
<div id="liquid-round">
   <div class="top" ><span></span></div>
   <div class="center-content"> 	
<table class="detailTabLabel">
<tr>
<td class="listwhitetext" align="right">Cost Element<font color="red" size="2">*</font></td>
<td><s:textfield cssClass="input-text" key="costElement.costElement" size="20" onkeydown="return onlyAlphaNumericAllowed(event);" maxlength="45"/></td>
<td class="listwhitetext" align="right">Description</td>
<td><s:textfield cssClass="input-text" key="costElement.description" size="37" maxlength="30"/></td>
<td class="listwhitetext" align="right" width="120">Reporting Alias<font color="red" size="2">*</font></td>
<td>
	<s:select list="%{category}" cssClass="list-menu" cssStyle="width:135px" headerKey="" headerValue="" name="costElement.reportingAlias" />
<!-- Repalce as enhancement of Bug# 5630
<s:textfield cssClass="input-text" key="costElement.reportingAlias" size="40" maxlength="50"/> -->
</td>
</tr>
<tr>
<td class="listwhitetext" align="right" height="4"></td>
</tr>
<tr>
<td class="listwhitetext" align="right">Receivable GL Code</td>
<td><s:select cssClass="list-menu" id="costElement.recGl" name="costElement.recGl" list="%{glcodes}" headerKey=" " headerValue=" " cssStyle="width:100px" /></td>
<td class="listwhitetext" align="right" width="110">Payable GL Code</td>
<td><s:select cssClass="list-menu" id="costElement.recGl" name="costElement.payGl" list="%{glcodes}" headerKey=" " headerValue=" " cssStyle="width:100px" /></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td class="listwhitetext" align="right" height="10"></td>
</tr>
</table>

<table  width="100%" cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>				
<display:table name="glCodeRateGridList" class="table" id="glCodeRateGridList" requestURI="" pagesize="25" style="width:100%;margin:0px;"> 
            <display:column title="Company Division" style="width:150px;"  >
			<c:if test="${glCodeRateGridList.companyDivision==''}">
			<a style="color:#fff; width:100%"  onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.companyDivision!=''}">
			<a onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><c:out value="${glCodeRateGridList.companyDivision}"/></a>
			</c:if>
			</display:column>

			<display:column title="Job Type">
			<c:if test="${glCodeRateGridList.gridJob==''}">
			<a style="color:#fff; width:100%"  onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.gridJob!=''}">
			<a onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><c:out value="${glCodeRateGridList.gridJob}"/></a>
			</c:if>
			</display:column>
			<display:column title="Routing">
			<c:if test="${glCodeRateGridList.gridRouting==''}">
			<a style="color:#fff; width:100%"  onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><div style="color:#fff; width:100% ; height: 100%" /></a>
			</c:if>
			<c:if test="${glCodeRateGridList.gridRouting!=''}">
			<a onclick="popUpGLRateGridForm('${glCodeRateGridList.id}')"><c:out value="${glCodeRateGridList.gridRouting}"/></a>
			</c:if>
			</display:column>			
				
	<display:column property="gridRecGl" title="Receivable GL"/>
		<display:column property="gridPayGl" title="Payable GL"/>
		<display:column title="Remove" style="text-align:center; width:20px;">
		<a><img align="middle" onclick="confirmSubmit(${glCodeRateGridList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
		</display:table>
 

			</td>
		</tr>
	</tbody>
</table>
<c:set var="buttons">     
    <input class="cssbutton" style="width:65px;" type="button" value="Add Grid" onclick="popUpGLRateGridForm('NEW');"/> 
</c:set> 
<c:if test="${not empty costElement.id}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<table><tr>
<td height="10"></td>
</tr>
</table>
</div>

<div class="bottom-header" style="margin-top: 28px;"><span>
</span></div>
</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:780px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:120px">
								<fmt:formatDate var="portCreatedOnFormattedValue" value="${costElement.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="costElement.createdOn" value="${portCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${costElement.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						
						<td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td>
						
						<c:if test="${not empty costElement.id}">
							<s:hidden name="costElement.createdBy" />
							<td ><s:label name="createdBy" value="%{costElement.createdBy}" /></td>
						</c:if>
						<c:if test="${empty costElement.id}">
							<s:hidden name="costElement.createdBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<fmt:formatDate var="portUpdatedOnFormattedValue" value="${costElement.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<s:hidden name="costElement.updatedOn"  value="${portUpdatedOnFormattedValue}"/>
						<td style="width:130px"><fmt:formatDate value="${costElement.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty costElement.id}">
							<s:hidden name="costElement.updatedBy" />
							<td ><s:label name="updatedBy" value="%{costElement.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty costElement.id}">
							<s:hidden name="costElement.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>
<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;margin-left:10px;margin-top:5px;" onclick=""  method="" key="button.save"/>   
<input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();pickOldDate();"/>
</div>
</s:form>

<script language="javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	var costId="${costId}";
	if (agree){
		location.href="deleteGlRateGrid.html?costId="+costId+"&id="+id+"&decorator=popup&popup=true";		
	}else{
		return false;
	}
} 
function checkCostElement(){
	var costVal=document.forms['costElementForm'].elements['costElement.costElement'].value;
	var reportingAlias=document.forms['costElementForm'].elements['costElement.reportingAlias'].value;
	costVal=encodeURIComponent(costVal);
	reportingAlias=encodeURIComponent(reportingAlias);
 	if(costVal.trim()==''){
 	alert('Please Enter Cost Element');
 	document.forms['costElementForm'].elements['costElement.costElement'].focus();
 	return false;
 	}else if(reportingAlias.trim()==''){
 	alert('Please fill in the Reporting Alias');
 	document.forms['costElementForm'].elements['costElement.reportingAlias'].focus();
 	return false;
 	}else{
 	var costEleId= '${costElement.id}'
 	if(costEleId!=''){
 		var chk = confirm('Do you want to update associated charge codes ?');
 		if(chk){
 	 		var url = "updateChargesByCostElement.html";
 			document.forms['costElementForm'].action = url;
 			document.forms['costElementForm'].submit();			
 		}else{
 	 		return false;
 		}
 	}}
}
function pickOldDate()
{
	document.forms['costElementForm'].elements['oldCostElement'].value='${costElement.costElement}';
}
function onlyAlphaNumericAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(keyCode==222)
	{return false;
	} 
  	 
}
function popUpGLRateGrid(){
	var costId=document.forms['costElementForm'].elements['costElement.id'].value;
	if((costId=='')||(costId=='null')||(costId=='undefined')){
		costId="0";
	}
	window.open("glRateGridListPopUp.html?costId="+costId+"&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=750,height=450");
}
function popUpGLRateGridForm(target){
	var costId=document.forms['costElementForm'].elements['costElement.id'].value;
	if((costId=='')||(costId=='null')||(costId=='undefined')){
		costId="0";
	}
	if(target=='NEW'){
		window.open("editGlRateGrid.html?costId="+costId+"&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,width=750,height=450");		
	}else{
		window.open("editGlRateGrid.html?costId="+costId+"&decorator=popup&popup=true&id="+target,"mywindow","menubar=0,resizable=1,width=750,height=450");				
	}
}

</script>

<script type="text/javascript">
pickOldDate();							
</script>