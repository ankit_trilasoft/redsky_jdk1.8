package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.MyMessage;

	public interface MessageDao extends GenericDao<MyMessage, Long> {
		
		public List<MyMessage> findByToUser(String toUser);
		public List<MyMessage> findByFromUser(String fromUser);
		public List<MyMessage> findByUnreadMessages(String fromUser);
		public List<MyMessage> findByOutbox(String status, String fromUser);
		public int updateFwdStatus(Long noteId);
	}
