<%@ include file="/common/taglibs.jsp"%>
 
<head>
    <title><fmt:message key="companyDivisionDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='companyDivisionDetail.heading'/>"/>
    <style type="text/css">
.upper-case{
    text-transform:capitalize;
}
</style>
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />

</head>
<s:form id="companyDivisionForm" action="saveCompanyDivision" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<c:set var="FormDateValue1" value="{0,date,dd-MMM-yy hh:mm:ss}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer3" >
<s:hidden name="companyDivision.billingCountryCode"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<input type="hidden" name="encryptPass"/>
<c:if test="${validateFormNav == 'OK'}">
<c:choose>
<c:when test="${gotoPageString == 'gototab.companyDivisions' }">
<c:redirect url="/companyDivisions.html" />	
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>
<div id="layer4" style="width:100%;">
<div id="newmnav">
<ul>
 <li><a onclick="setReturnString('gototab.companyDivisions');return autoSaveFunc('none');"><span>Company Division List</span></a></li>
 <li id="newmnav1" style="background:#FFF"><a class="current"><span>Company Division Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
 <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${companyDivision.id}&tableName=companydivision&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
 
 </ul>
</div>
  <div class="spn">&nbsp;</div>
</div>
	<div id="Layer1" style="width:100%" onkeydown="changeStatus();">
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
	<tbody>
		<tr>
		<td> 	
 	<table class="detailTabLabel" width="100%" cellspacing="0" cellpadding="0" border="0" style="padding-bottom:4px;">	     
	<tbody>
	<tr>
	<td>
	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px; cursor:default; " >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Basic Info</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</td>
	</tr>	
	<tr>
	<td>
	<table class="detailTabLabel" border="0" cellspacing="3" cellpadding="2" width="">	
<s:hidden name="companyDivision.id" value="%{companyDivision.id}"/>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="companyDivision.corpID"/></td>
<td align="left" width="200px"><s:textfield name="companyDivision.corpID" maxlength="18"  required="true" cssClass="input-textUpper" size="32" readonly="true"/></td>
<td align="right" class="listwhitetext" width="127">Division<font color="red" size="2">*</font></td>
<td align="left"  width=""><s:textfield name="companyDivision.companyCode" onchange="checkCompanyCodeAvailability();"maxlength="9"  required="true" cssClass="input-text" cssStyle="width:125px;" /></td>
<td align="right" class="listwhitetext" width="70px"><fmt:message key='companyDivision.logoName'/></td>
<td><s:textfield cssClass="input-text" name="companyDivision.logoName" cssStyle="width:138px"  maxlength="50" /></td>

</tr>
<tr>
<td align="right" class="listwhitetext"><fmt:message key="companyDivision.description"/></td>
<td align="left"><s:textfield name="companyDivision.description" maxlength="100"  required="true" cssClass="input-text" size="32" /></td>
<td align="right" class="listwhitetext"><fmt:message key="companyDivision.accountingCode"/></td>
<td align="left" ><s:textfield name="companyDivision.accountingCode" maxlength="18"  required="true" cssClass="input-text" cssStyle="width:125px;" /></td>

<td class="listwhitetext" align="right" width="60px">Ops Hub</td>
<td><s:select cssClass="list-menu" name ="companyDivision.operationsHub" list="%{opshub}" headerKey="" headerValue="" cssStyle="width:143px"/></td>

</tr>
<tr>
<td width="105px" align="right" class="listwhitetext"><fmt:message key="companyDivision.bookingAgentCode"/><font color="red" size="2">*</font></td>
<td ><s:textfield name="companyDivision.bookingAgentCode"  onchange="checkBookAgentAvailability();" maxlength="18"  required="true" cssClass="input-text" size="32"/></td>
<td align="right" class="listwhitetext"><fmt:message key="companyDivision.vanlinedefaultJobtype"/></td>
<td align="left"><s:select cssClass="list-menu" name="companyDivision.vanlinedefaultJobtype" list="%{job}" cssStyle="width:130px" headerKey="" headerValue="" onchange="changeStatus();"/></td>
<td align="right" class="listwhitetext"><fmt:message key='companyDivision.vanlineCoordinator'/></td>
<td align="left"><s:select cssClass="list-menu" name="companyDivision.vanlineCoordinator" list="%{coord}" cssStyle="width:143px" headerKey="" headerValue=""  onchange="changeStatus();"/></td>
</tr>
<tr>
<td width="105px" align="right" class="listwhitetext">Child Agent Code</td>
<td ><s:textfield name="companyDivision.childAgentCode"  onchange="checkChildAgentCodeValidation();" maxlength="1000"  required="true" cssClass="input-text" size="32"/></td>

<td align="right" class="listwhitetext">Last&nbsp;Leave&nbsp;Calculator&nbsp;Run</td>
   <c:if test="${not empty companyDivision.lastUpdatedLeave}">
		<s:text id="lastUpdatedLeaveDateValue" name="${FormDateValue}"><s:param name="value" value="companyDivision.lastUpdatedLeave"/></s:text>
		<td><s:textfield id="lastUpdatedLeaveDate" cssClass="input-text" name="companyDivision.lastUpdatedLeave" value="%{lastUpdatedLeaveDateValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
		<img id="lastUpdatedLeaveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
   </c:if>
   <c:if test="${empty companyDivision.lastUpdatedLeave}">
		<td><s:textfield id="lastUpdatedLeaveDate" cssClass="input-text" name="companyDivision.lastUpdatedLeave" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/><img id="lastUpdatedLeaveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<td width="105px" align="right" class="listwhitetext">Website</td>
<td ><s:textfield name="companyDivision.companyWebsite"   maxlength="90"  required="true" cssClass="input-text" size="45"cssStyle="width: 138px;" /></td>
</tr>
<c:if test="${cportalAccessCompanyDivisionLevel}">
<tr>
<td colspan="4"></td>
<td valign="middle" align="right" class="listwhitetext">Cportal&nbsp;Branding&nbsp;URL</td>
<td valign="top"><s:textfield name="companyDivision.cportalBrandingURL"  maxlength="200"  readonly="true" cssClass="input-textUpper" size="45"/></td>
</tr>
</c:if>
<tr>
<td class="listwhitetext" align="right" valign="top">Mail Message</td>
<td colspan="3"><s:textarea name="companyDivision.textMessage" cols="86" rows="3" cssClass="textarea" onkeydown="limitText()" onkeyup="limitText()"/></td>
<td rowspan="2" colspan="2" style="vertical-align:top">
<table style="margin: 0px 0px 0px 22px;vertical-align:top;">
<configByCorp:fieldVisibility componentId="component.field.companyDivision.EORIUCRNo">
 <tr>
 <td valign="middle" align="right" class="listwhitetext">EORI&nbsp;No/UCR&nbsp;No</td>
<td valign="top"><s:textfield cssClass="input-text" name="companyDivision.eoriNo" cssStyle="width:138px"  maxlength="20" /></td>
</tr>
</configByCorp:fieldVisibility>
<tr>
<td align="right" class="listwhitetext">Inactive&nbsp;Effective</td>
   <c:if test="${not empty companyDivision.inactiveEffectiveDate}">
        <s:text id="inactiveEffectiveValue" name="${FormDateValue}"><s:param name="value" value="companyDivision.inactiveEffectiveDate"/></s:text>
        <td><s:textfield id="inactiveEffectiveDate" cssClass="input-text" name="companyDivision.inactiveEffectiveDate" value="%{inactiveEffectiveValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
        <img id="inactiveEffectiveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
   </c:if>
   <c:if test="${empty companyDivision.inactiveEffectiveDate}">
        <td><s:textfield id="inactiveEffectiveDate" cssClass="input-text" name="companyDivision.inactiveEffectiveDate" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/>
        <img id="inactiveEffectiveDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
    </c:if>
  </tr>
 <configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
 <tr>
     <c:set var="isClosedDivision" value="false"/>
		<c:if test="${companyDivision.closedDivision}">
		 <c:set var="isClosedDivision" value="true"/>
     	</c:if>
 	<td align="right" class="listwhitetext">Closed&nbsp;Division</td>
 	<td><s:checkbox name="companyDivision.closedDivision"  value="${isClosedDivision}" tabindex="10" cssStyle="margin:0px;padding:0px;" /> </td>
 </tr>
 </configByCorp:fieldVisibility> 
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
	<tbody>
	<tr><td colspan="10">
			<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin:0px;padding:0px;" >
				<tr>
					<td class="headtab_left"></td>
						<td class="headtab_center" >&nbsp;Billing Address</td>
						<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special" >&nbsp;</td>
						<td class="headtab_right"></td>
						</tr>
				</table>
				</td>
			</tr>	
	</tbody>
	</table>
	</td>
	</tr>
	<tr>
	<td>
	<table cellspacing="3" cellpadding="2" border="0" class="detailTabLabel">
		<tbody>			
			<tr>
			    <td width="105px"></td>
				<td colspan="2"><s:textfield cssClass="input-text upper-case" name="companyDivision.billingAddress1"  size="50" maxlength="30" tabindex="" onblur="titleCase(this)"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingCity'/></td>
				<td><s:textfield cssClass="input-text upper-case" name="companyDivision.billingCity" cssStyle="width:155px" maxlength="20" onkeydown="return onlyCharsAllowed(event)" onblur="titleCase(this)" tabindex=""/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingFax'/></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingFax" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex=""/></td>
			</tr>
			<tr>
				<td width="10px"></td>
				<td colspan="2"><s:textfield cssClass="input-text upper-case" name="companyDivision.billingAddress2" size="50" maxlength="30" tabindex="" onblur="titleCase(this)" /></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingState'/></td>
				<td><s:select name="companyDivision.billingState" id="state" cssClass="list-menu" list="%{bstates}" cssStyle="width:160px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingPhone'/></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingPhone" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex=""/></td>
			</tr>
			<tr>
				<td width="10px"></td>
				<td colspan="2"><s:textfield cssClass="input-text upper-case" name="companyDivision.billingAddress3" size="50" maxlength="30" tabindex="" onblur="titleCase(this)" /></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingZip'/></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingZip" cssStyle="width:155px"  maxlength="10" onkeydown="return onlyNumsAllowed(event)" tabindex=""/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingTelex'/></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingTelex" maxlength="20" size="22" onkeydown="return onlyPhoneNumsAllowed(event)" tabindex=""/></td>
			</tr>
			<tr>
				<td width="10px"></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingAddress4" size="40" maxlength="250" tabindex=""/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingCountry'/></td>
				<td colspan="2"><s:select cssClass="list-menu" name="companyDivision.billingCountry" list="%{countryDesc}" cssStyle="width:191px"  headerKey="" headerValue="" onchange="changeStatus();getBillingCountryCode(this);billingCountry(this);getBillingState(this);enableStateListBilling();"/></td>
				<td align="right" class="listwhitetext"><fmt:message key='companyDivision.billingEmail'/></td>
				<td><s:textfield cssClass="input-text" name="companyDivision.billingEmail"  size="51" maxlength="65" tabindex="" onchange="checkBillingEmails(this);"/></td>
			</tr>
			
		</tbody>
	</table>
	</td>
	</tr>
	<!--add new section -->
	<tr>
	<td>
	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px; cursor:default; " >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Vanline Controls</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</td>
	</tr>
	
	<tr>
<td>
<table cellspacing="3" cellpadding="2"  class="detailTabLabel" border="0">
	<tbody>
			<tr>
			<td align="right" width="105" class="listwhitetext"><fmt:message key="companyDivision.vanLineCodeGen"/></td>
			<td align="left"><s:textfield name="companyDivision.vanLineCodeGen" maxlength="5" required="true" onchange="checkCodeGen();" onkeyup="valid(this,'special')" onblur="valid(this,'special')" cssClass="input-text" size="18" /></td>
			<td align="right" class="listwhitetext">Job&nbsp;Types</td>
			<td align="left"><s:textfield name="companyDivision.vanlineJob" maxlength="30" required="true" id="jobType" onchange="" cssClass="input-text" size="24" /></td>
			
			<td align="right" class="listwhitetext">Seq&nbsp;#</td>
			<td align="left"><s:textfield name="companyDivision.vanlineseq" maxlength="30" required="true" id="seqNo" onchange="validateJobSeq(this.value);" cssClass="input-text" size="18" /></td>
			<td align="right" class="listwhitetext"><fmt:message key="companyDivision.SCAC"/></td>
			<td align="left"><s:textfield name="companyDivision.SCAC" maxlength="20"  cssClass="input-text" size="20"/></td>
			
		</tr>
		<tr>
			<td align="right" class="listwhitetext"><fmt:message key="companyDivision.vanLineCode"/></td>
			<td align="left" width="" ><s:select cssClass="list-menu" name="companyDivision.vanLineCode" list="%{driverAgencyList}" cssStyle="width:113px;" headerKey="" headerValue=""  /></td>
			<td align="right" class="listwhitetext" width="120">Unigroup&nbsp;Password</td>
			<td align="left"><input type="password" name="companyDivision.resourcePassword" showPassword="true" value="${companyDivision.resourcePassword}" class="input-text" size="24" onchange="passwordIncrypt(this)" oncopy="return false;" oncut="return false;" onpaste="return false;" />
			<td align="right" class="listwhitetext">Unigroup&nbsp;Resource&nbsp;ID</td>
			<td align="left"><s:textfield name="companyDivision.resourceID" maxlength="30"  required="true" cssClass="input-text" size="18" /></td>
		   
		</tr>
		<tr>
		<td align="right" class="listwhitetext">MSS&nbsp;Username</td>
		<td align="left"><s:textfield name="companyDivision.mssCustomerID" maxlength="30"  required="true" cssClass="input-text" size="18" /></td>
		<td align="right" class="listwhitetext">MSS&nbsp;Password</td>
		<td align="left"><input type="password" name="companyDivision.mssPassword" showPassword="true" value="${companyDivision.mssPassword}" class="input-text" size="24" onchange="passwordIncrypt(this)" oncopy="return false;" oncut="return false;" onpaste="return false;" />
		<td align="right" class="listwhitetext">Unigroup&nbsp;Agent&nbsp;Code</td>
		<td align="left" colspan="3"><s:textfield name="companyDivision.ugwwAgentCodes" maxlength="500"  required="true" cssClass="input-text" size="52" /></td>
		</tr>
	</tbody>
</table>
</td>
</tr>
	
	
	<tr>
	<td>
	<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px; cursor:default; " >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Accounting Controls</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</td>
	</tr>
	<tr>
<td>
<table cellspacing="3" cellpadding="2" class="detailTabLabel" border="0">
	<tbody>
			<tr>
			<td align="right" class="listwhitetext" width="105px">Beneficiary&nbsp;Name</td>
			<td colspan="2"><s:textfield cssClass="input-text" name="companyDivision.beneficiaryName" maxlength="250" size="30" cssStyle="width:122px;"/>
			<div class="listwhitetext" style="float:right;line-height:18px;padding-left:5px;"><fmt:message key='companyDivision.bankName'/></div></td>			
			<td ><s:textfield cssClass="input-text" name="companyDivision.bankName" maxlength="100" size="48" /></td>
			<td align="right" class="listwhitetext" width="82px"><fmt:message key='companyDivision.swiftcode'/></td>
			<td ><s:textfield cssClass="input-text" name="companyDivision.swiftcode" maxlength="50" size="31" /></td>
		</tr>
		<tr>
			<td align="right" class="listwhitetext" width="105px" ><fmt:message key='companyDivision.bankAccountNumber'/></td>
			<td><s:textfield cssClass="input-text" name="companyDivision.bankAccountNumber" cssStyle="width:120px"  maxlength="50" /></td>
			<td align="right" class="listwhitetext"><fmt:message key='companyDivision.wireTransferAccountNumber'/></td>
			<td ><s:textfield cssClass="input-text" name="companyDivision.wireTransferAccountNumber" maxlength="50" size="48" /></td>
			<td align="right" class="listwhitetext">Bank&nbsp;Address</td>
			<td ><s:textfield cssClass="input-text" name="companyDivision.bankAddress" maxlength="150" size="31" /></td>
		</tr>
		<tr>
		<td align="right" class="listwhitetext"><fmt:message key="companyDivision.EIN"/></td>
        <td align="left"><s:textfield name="companyDivision.EIN" maxlength="20"  cssClass="input-text" cssStyle="width:120px"/></td>
        <td align="right" class="listwhitetext">Internal&nbsp;Cost&nbsp;Vendor&nbsp;Code</td>
        <td align="left"><s:textfield name="companyDivision.internalCostVendorCode" maxlength="20"  cssClass="input-text" cssStyle="width:260px"/></td>
		
		<td align="right" class="listwhitetext" width=""><fmt:message key="systemDefault.baseCurrency"/></td>
		<td align="left" colspan="3"><s:select cssClass="list-menu"   name="companyDivision.baseCurrency" list="%{baseCurrency}" headerKey="" headerValue="" cssStyle="width:180px"/></td> 
		
		</tr>
		<tr>
		<configByCorp:fieldVisibility componentId="component.field.Tax.GST">
        
         <td width="105px" align="right" class="listwhitetext">GST&nbsp;Regn&nbsp;No</td>
         <td colspan=""><s:textfield name="companyDivision.GSTRegnNo"   maxlength="25"  required="true" cssClass="input-text" size="30"/></td>
       
      </configByCorp:fieldVisibility>
      <c:if test="${generateInvoiceByCompanyDivision}"> 
      <td align="right" class="listwhitetext">Seed&nbsp;Invoice&nbsp;Number</td>
      <td align="left"><s:textfield name="companyDivision.lastInvoiceNumber" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true" maxlength="10" size="16" tabindex=""/></td>
      <td align="right" class="listwhitetext">Seed&nbsp;Credit&nbsp;Invoice&nbsp;Number</td>
      <td align="left"><s:textfield name="companyDivision.lastCreditInvoice" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true" maxlength="10" size="16" tabindex=""/></td>
      </c:if>
      </tr>
	</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

</tbody>
</table>					
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table border="0">
<tbody>
	<tr>
		<td align="left" class="listwhitetext" width="30px"></td>
	<td colspan="5"></td>
	</tr>
	<tr>
		<td align="left" class="listwhitetext" width="30px"></td>
	<td colspan="5"></td>
	</tr>
	<tr>
		<td align="left" class="listwhitetext" style="width:60px"><b><fmt:message key='companyDivision.createdOn'/></b></td>
		
		<td valign="top">
		
		</td>
		<td style="width:120px">
		<fmt:formatDate var="companyDivisionCreatedOnFormattedValue" value="${companyDivision.createdOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="companyDivision.createdOn" value="${companyDivisionCreatedOnFormattedValue}"/>
		<fmt:formatDate value="${companyDivision.createdOn}" pattern="${displayDateTimeFormat}"/>
		</td>		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.createdBy' /></b></td>
		<c:if test="${not empty companyDivision.id}">
			<s:hidden name="companyDivision.createdBy"/>
			<td style="width:85px"><s:label name="createdBy" value="%{companyDivision.createdBy}"/></td>
		</c:if>
		<c:if test="${empty companyDivision.id}">
			<s:hidden name="companyDivision.createdBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.updatedOn'/></b></td>
		<fmt:formatDate var="companyDivisionupdatedOnFormattedValue" value="${companyDivision.updatedOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="companyDivision.updatedOn" value="${companyDivisionupdatedOnFormattedValue}"/>
		<td style="width:120px"><fmt:formatDate value="${companyDivision.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.updatedBy' /></b></td>
		<c:if test="${not empty companyDivision.id}">
			<s:hidden name="companyDivision.updatedBy"/>
			<td style="width:85px"><s:label name="updatedBy" value="%{companyDivision.updatedBy}"/></td>
		</c:if>
		<c:if test="${empty companyDivision.id}">
			<s:hidden name="companyDivision.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
	</tr>
</tbody>
</table>
    <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:50px;"/>
     <c:if test="${not empty companyDivision.id}">    
    <input type="button" class="cssbutton" onclick="location.href='<c:url value="/editCompanyDivision.html"/>'"  
        value="<fmt:message key="button.add"/>" style="width:50px;"/>  
	</c:if>
       <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:50px;" onclick="newFunctionForCountryState();"/>
 
</div>
</div>
</s:form>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
	animatedcollapse.addDiv('valuation', 'fade=1,hide=1')
	animatedcollapse.init()
</script>

<script type="text/javascript">

function newFunctionForCountryState(){
	try{
     var billingCountry ='${companyDivision.billingCountry}';
     var enbState = '${enbState}';
     var index = (enbState.indexOf(billingCountry)> -1);
     if(index != ''){
         document.forms['companyDivisionForm'].elements['companyDivision.billingState'].disabled =false;
         getStateOnReset(billingCountry);
     }
     else{
         document.forms['companyDivisionForm'].elements['companyDivision.billingState'].disabled =true;
     }
   
     }
catch(err){
	
} } 
     
     
function getStateOnReset(target) {
	try{
    var country = target;
    var countryCode = getKeyByValue(country);
    document.forms['companyDivisionForm'].elements['companyDivision.billingCountryCode'].value=countryCode;
    var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
    httpState1.open("GET", url, true);
    httpState1.onreadystatechange = handleHttpResponse555555;
    httpState1.send(null);  
    }catch(err ){}
    }
var httpState1 = getHTTPObject();
    
     function handleHttpResponse555555(){
    	 try{
     if (httpState1.readyState == 4){
        var results = httpState1.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['companyDivisionForm'].elements['companyDivision.billingState'];
        targetElement.length = res.length;
            for(i=0;i<res.length;i++) {
            if(res[i] == ''){
            document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].text = '';
            document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].value = '';
            }else{
            stateVal = res[i].split("#");
            document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].text = stateVal[1];
            document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].value = stateVal[0];
            
            if (document.getElementById("state").options[i].value == '${companyDivision.billingState}'){
               document.getElementById("state").options[i].defaultSelected = true;
            } } }
            document.getElementById("state").value = '${companyDivision.billingState}';
     }
}
  catch(err){}
  }   
     function getKeyByValue(value){
    	 try{
    	    var countryList = JSON.parse('${jsonCountryCodeText}');
    	    for (key in countryList) {
    	        if (countryList.hasOwnProperty(key)) {
    	            if(value==countryList[key]){
    	                return key;
    	            }
    	        }
    	    }
    	}catch(err){
    		} 
    	}
    
     
</script>
<script type="text/javascript">
function checkChildAgentCodeValidation(){ 	
	var cmpDiv=document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].value;
	cmpDiv=cmpDiv.trim();
	if(cmpDiv!=''){
			var cmpDivLength = cmpDiv.length;
			var reminder = ((cmpDivLength-1)%(cmpDivLength-1));
			var startPoint = cmpDiv.substring(0,2)
			var endPoint = cmpDiv.substring(cmpDivLength-2,cmpDivLength)
			if(startPoint!="('"){
				alert("Incorrect format for entering the Child Agent Code.");
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].value='';
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].select();
				return false;
			}else if(endPoint!="')"){
				
				alert("Incorrect format for entering the Child Agent Code.");
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].value='';
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].select();
				return false;
			}else if(reminder > 0){
				alert("Incorrect format for entering the Child Agent Code.");
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].value='';
				document.forms['companyDivisionForm'].elements['companyDivision.childAgentCode'].select();
				return false;
			}	
	}
	return true;
}
function autoSaveFunc(clickType) 
{
	if ('${autoSavePrompt}' == 'No'){
	
	var noSaveAction = '<c:out value="${companyDivision.id}"/>';
      		var id1 =document.forms['companyDivisionForm'].elements['companyDivision.id'].value;
      		
if(document.forms['companyDivisionForm'].elements['gotoPageString'].value == 'gototab.companyDivisions'){
               noSaveAction = 'companyDivisions.html';
               }
          processAutoSave(document.forms['companyDivisionForm'], 'saveCompanyDivision!saveOnTabChange.html', noSaveAction);
	
	
	}
	else{
	
   if(!(clickType == 'save'))
   {
     var id1 =document.forms['companyDivisionForm'].elements['companyDivision.id'].value;
     if (document.forms['companyDivisionForm'].elements['formStatus'].value == '1')
     {
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='userProfile.heading'/>");
       if(agree)
        {
           document.forms['companyDivisionForm'].action ='saveCompanyDivision!saveOnTabChange.html';
           document.forms['companyDivisionForm'].submit();
        }
       else
        {
         if(id1 != '')
          {
           if(document.forms['companyDivisionForm'].elements['gotoPageString'].value == 'gototab.companyDivisions'){
               location.href = 'companyDivisions.html';
               }
         }
       }
    }
   else
   {
     if(id1 != '')
      {
       if(document.forms['companyDivisionForm'].elements['gotoPageString'].value == 'gototab.companyDivisions'){
               location.href = 'companyDivisions.html';
               }
      }
   }
  }
  }
 }

function changeStatus()
{
   document.forms['companyDivisionForm'].elements['formStatus'].value = '1';
}

function billingCountry(targetElement) {
		 	var billingCountryCode=targetElement.options[targetElement.selectedIndex].value;
			// document.forms['companyDivisionForm'].elements['companyDivision.billingCountryCode'].value=billingCountryCode.substring(0,billingCountryCode.indexOf(":")-1);
		    //  targetElement.form.elements['companyDivision.billingCountry'].value=billingCountryCode.substring(billingCountryCode.indexOf(":")+2,billingCountryCode.length);
		}
		
function getBillingCountryCode(targetElement){
	var countryName=document.forms['companyDivisionForm'].elements['companyDivision.billingCountry'].value;
	var url="countryCode.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseBillingCountry;
    http4.send(null);
}

function handleHttpResponseBillingCountry(){
             if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                if(results.length>=1)
                {
 				document.forms['companyDivisionForm'].elements['companyDivision.billingCountryCode'].value = results;
				 }
                 else
                 {
                     
                 }
             }
        }		
		
function getBillingState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
	
	}	
		
function handleHttpResponse2() {
            if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['companyDivisionForm'].elements['companyDivision.billingState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].text = '';
					document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].text = stateVal[1];
					document.forms['companyDivisionForm'].elements['companyDivision.billingState'].options[i].value = stateVal[0];

					if (document.getElementById("state").options[i].value == '${companyDivision.billingState}'){
						   document.getElementById("state").options[i].defaultSelected = true;
						}
					}
					}
             }
            document.getElementById("state").value = '${companyDivision.billingState}';
        }
function enableStateListBilling(){ 
	var billCon=document.forms['companyDivisionForm'].elements['companyDivision.billingCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(billCon)> -1);
	  if(index != ''){
	  		document.forms['companyDivisionForm'].elements['companyDivision.billingState'].disabled = false;
	  }else{
		  document.forms['companyDivisionForm'].elements['companyDivision.billingState'].disabled = true;
	  }	        
}
        
function checkBookAgentAvailability() {
	var bookCode= document.forms['companyDivisionForm'].elements['companyDivision.bookingAgentCode'].value;
	var url="checkBookAgentExists.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse4;
    http4.send(null);
}
	
function handleHttpResponse4()
        {
		  if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                var res = results.split("@");
                if(res.length-1 >= 1){
                alert("Booking agent is already in use");
                document.forms['companyDivisionForm'].elements['companyDivision.bookingAgentCode'].value ='';
                } else {
                
                }
			}	
        }
        
function checkCompanyCodeAvailability() {
	var companyCode= document.forms['companyDivisionForm'].elements['companyDivision.companyCode'].value;
	var url="checkCompanyCodeExists.html?ajax=1&decorator=simple&popup=true&companyCode="+encodeURI(companyCode);
	http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponse5;
    http4.send(null);
}
	
function handleHttpResponse5()
        {
		  if (http4.readyState == 4)
             {
                var results = http4.responseText
                results = results.trim();
                var res = results.split("@");
                if(res.length-1 >= 1){
                alert("Company Code is already in use");
                document.forms['companyDivisionForm'].elements['companyDivision.companyCode'].value ='';
                } else {
                
                }
			}	
        }
        
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();  	
    var http4 = getHTTPObject(); 

   function passwordIncrypt(passValue){
	   var origPassword = "<s:property value="companyDivision.resourcePassword"/>";
	     if(passValue.value !=origPassword && passValue.value!=''){
		      document.forms['companyDivisionForm'].elements['encryptPass'].value =true;
	   }
   }
   function DisableRightClick(event)
   {
	  // alert('hi');
   //For mouse right click 
   if (event.button==2)
   {
   //alert("Right Clicking not allowed!");
	   return false;
   }
   }
   function DisableCtrlKey(e)
   {
	   //alert('hi2');
   var code = (document.all) ? event.keyCode:e.which;
   //var message = "Ctrl key functionality is disabled!";
   // look for CTRL key press
   if (parseInt(code)==17)
   {
  return false;
   }
  }
   
function limitText() {
	   limitField= document.forms['companyDivisionForm'].elements['companyDivision.textMessage'].value;
	   limitNum=6000;
	   	if (limitField.length > limitNum) {
		   	alert("Please enter less than 6000 characters!");
		   	document.forms['companyDivisionForm'].elements['companyDivision.textMessage'].value=limitField.substring(0,6000);
	    	document.forms['companyDivisionForm'].elements['companyDivision.textMessage'].focus();
	   	}
 } 

function checkSeq() {
	var limitField= document.getElementsByName('companyDivision.vanlineseq')[0].value.length;
   	if (limitField!='' && limitField < 5) {	   		
	   	alert("Please Enter 5 Digits for Seq#.");
	   	document.forms['companyDivisionForm'].elements['companyDivision.vanlineseq'].value='';
	   	document.forms['companyDivisionForm'].elements['companyDivision.vanlineseq'].focus();
	   	return false;	
	}
	return true; 
} 

function checkCodeGen() {
	var limitField= document.getElementsByName('companyDivision.vanLineCodeGen')[0].value.length;
   	if (limitField!='' && limitField < 5) {	   		
	   	alert("Please Enter 5 Characters for Van Line Code Gen.");
	   	document.forms['companyDivisionForm'].elements['companyDivision.vanLineCodeGen'].value=' ';
		document.forms['companyDivisionForm'].elements['companyDivision.vanLineCodeGen'].focus();
	} 
} 
function isNumeric(targetElement)
{   var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Please Enter Only Numbers for Seq#.");
        targetElement.value="";
        return false;
        }
    }
    return true;
}

function valid(targetElement,w){
	 targetElement.value = targetElement.value.replace(r[w],'');
	 targetElement.focus();
	}
var r={
		 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\=']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
</script>

<script type="text/javascript">
    Form.focusFirstElement($("companyDivisionForm"));

    try{
   	 var enbState = '${enbState}';
   	 var oriCon=document.forms['companyDivisionForm'].elements['companyDivision.billingCountry'].value;
   	 if(oriCon=="")	 {
   			document.getElementById('state').disabled = true;
   			document.getElementById('state').value ="";
   	 }else{
   	  		if(enbState.indexOf(oriCon)> -1){
   				document.getElementById('state').disabled = false; 
   				document.getElementById('state').value='${companyDivision.billingState}';
   			}else{
   				document.getElementById('state').disabled = true;
   				document.getElementById('state').value ="";
   			} 
   		}
   	 }
   		catch(e){} 

   		function validateJobSeq(str){
   			return true;
   		}
</script>


<script type="text/javascript">
	setOnSelectBasedMethods([""]);
	setCalendarFunctionality();
	
	function titleCase(element){ 
	    var txt=element.value; var spl=txt.split(" "); 
	    var upstring=""; for(var i=0;i<spl.length;i++){ 
	    try{ 
	    upstring+=spl[i].charAt(0).toUpperCase(); 
	    }catch(err){} 
	    upstring+=spl[i].substring(1, spl[i].length); 
	    upstring+=" ";   
	    } 
	    element.value=upstring.substring(0,upstring.length-1); 
	    }
	
	
	function checkBillingEmails(temp){
		 var reg =/^(([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+([,](([a-zA-Z0-9_\-\.\']+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5}){1,25})+)*$/;

		 if (reg.test(temp.value)){
			
		 return true; 
		}
		 else{
			 alert("Invalid Email")
			 document.forms['agentRequestForm'].elements['companyDivision.billingEmail'].value="";
		        
		 return false;
		 } 
	}
	
</script>