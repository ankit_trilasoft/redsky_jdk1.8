<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title><fmt:message key="invoiceCompanyCodeForm.title" /></title>
<meta name="heading" content="<fmt:message key='invoiceCompanyCodeForm.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>


<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript"> 
function showOrHideAutoSave(value) {
		    if (value==0) {
		        if (document.layers)
		           document.layers["overlay"].visibility='hide';
		        else
		           document.getElementById("overlay").style.visibility='hidden';
		   }
		   else if (value==1) {
		       if (document.layers)
		          document.layers["overlay"].visibility='show';
		       else
		          document.getElementById("overlay").style.visibility='visible';
		   }
		}
function delayer(){ 
    window.location = "invoiceCompanyCodeListTally.html" 
}
		
</script>
</head>

<s:form id="invoiceCompanyCodeForm" name="invoiceCompanyCodeForm" action="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:hidden cssClass="input-textUpper" id="recPostingDate" name="recPostingDate"  />  

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>
	<table  border="0" align="left"  class="tablegrey">
<tr><td height="10"></td></tr>
<tr><td width="5px"></td><td class="bgblue" colspan="2">Tally&nbsp;Invoice&nbsp;Data&nbsp;Extract</td>
</tr>
 <tr><td height="5"></td></tr>
	<tr><td colspan="3">
	
		</td>
	</tr>
	</table>
	</td>
  </tr> 
</table>
<div id="overlay">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div> 
</s:form>

<script type="text/javascript">  
showOrHideAutoSave(0);
delayer();
</script>