/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve the basic "WorkTicket" objects in Redsky that allows for WorkTicket of Shipment .
 * @Class Name	WorkTicketDaoHibernate
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 * Extended by GenericDaoHibernate to  implement WorkTicketDao
 */

package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;

import com.trilasoft.app.dao.WorkTicketDao;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.RevenueTrackerDTO;
import com.trilasoft.app.dao.hibernate.TimeSheetDaoHibernate.SeceduleTrucks;
import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.DriverDashboardDTO;
import com.trilasoft.app.dao.hibernate.dto.WorkPlanDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.AvailableCrew;
import com.trilasoft.app.model.CrewCapacity;
import com.trilasoft.app.model.OperationsDailyLimits;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.WorkTicket;

public class WorkTicketDaoHibernate extends GenericDaoHibernate<WorkTicket, Long> implements WorkTicketDao {
	private List workTickets;

	private HibernateUtil hibernateUtil;

	public WorkTicketDaoHibernate() {
		super(WorkTicket.class);
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public Long findMaximum(String corpId) {
		Long newTicketNumber = null;
		try {

			Long lastTicketNumber = (Long) this.getSession().createSQLQuery("Select lastTicketNumber From company where corpID = '" + corpId + "' FOR UPDATE;").addScalar(
					"lastTicketNumber", Hibernate.LONG).list().get(0);

			getHibernateTemplate().bulkUpdate("Update Company set lastTicketNumber=" + (lastTicketNumber + 1) + " where corpID = '" + corpId + "'");
			if (lastTicketNumber == null || lastTicketNumber == 0 || lastTicketNumber.equals("")) {
				newTicketNumber = Long.parseLong("1");
			} else {
				newTicketNumber = lastTicketNumber + 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return newTicketNumber;
	}

	public List findMaximumship(String shipNumber) {
		return getHibernateTemplate().find("select carrierName from ServicePartner where shipNumber='" + shipNumber + "' and id=(select min(id) from ServicePartner where  status=true and  shipNumber='" + shipNumber+ "')");
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from WorkTicket");
	}

	public List<WorkTicket> findByLastName(String lastName) {
		return getHibernateTemplate().find("from WorkTicket where lastName=?", lastName);
	}

	String toDate="";

	String fromDate="";

	public List<WorkTicket> findByTicket(Long ticket, String lastName, String firstName, String shipNumber, String warehouse, String service, Date date1, Date date2) {
		List workTickets = null;
		String columns = "id, ticket, shipNumber,registrationNumber,lastName,firstName,jobType,jobMode,city,destinationCity,date1,date2,service,warehouse,targetActual,estimatedWeight,reviewStatus";

		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			toDate = nowYYYYMMDD1.toString();
		}

		if (!(date2 == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
			fromDate = nowYYYYMMDD2.toString();
		}

		if ((date1 == null && date2 == null)) {
			if (ticket == null) {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service + "%' ");
			} else {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service + "%' AND  ticket=?", ticket);
			}
		}
		if ((date1 != null && date2 != null)) {
			if (ticket == null) {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service
						+ "%' AND (date1 BETWEEN '" + toDate + "' AND '" + fromDate + "') AND (date2 BETWEEN '" + toDate + "' AND '" + fromDate + "') ");
			} else {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''")
						+ "%' AND firstName like '" + firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '"
						+ service + "%' AND (date1 BETWEEN '" + toDate + "' AND '" + fromDate + "') AND (date2 BETWEEN '" + toDate + "' AND '" + fromDate + "') AND ticket=?", ticket);
			}
		}
		if ((date1 == null && date2 != null)) {
			if (ticket == null) {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service
						+ "%' AND (date1 <= '" + fromDate + "') AND (date2 <= '" + fromDate + "') ");
			} else {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service
						+ "%' AND (date1 <= '" + fromDate + "') AND (date2 <= '" + fromDate + "') AND ticket=?", ticket);
			}
		}
		if ((date1 != null && date2 == null)) {
			if (ticket == null) {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service
						+ "%' AND (date1 >= '" + toDate + "' ) AND (date2 >= '" + toDate + "' ) ");
			} else {
				workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where lastName like '" + lastName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND firstName like '"
						+ firstName.replaceAll("'", "''").replaceAll(":", "''") + "%' AND shipNumber like '"
						+ shipNumber.replaceAll("'", "''").replaceAll(":", "''") + "%'  AND  warehouse like '" + warehouse + "%' AND  service like '" + service
						+ "%' AND (date1 >= '" + toDate + "' ) AND (date2 >= '" + toDate + "' ) AND ticket=?", ticket);
			}
		}

		try {
			workTickets = hibernateUtil.convertScalarToComponent(columns, workTickets, WorkTicket.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workTickets;
	}

	public Set<WorkTicket> findByCustomerTicket(Long ticket, String sequenceNumber) {
		List wt = getHibernateTemplate().find("from WorkTicket where sequenceNumber=?", sequenceNumber);
		Set<WorkTicket> wts = new HashSet(wt);
		return wts;
	}

	public List findContainer(String containerNumber, String shipNumber) {
		return getHibernateTemplate().find("select containerNumber FROM Container where status = true and  shipNumber='" + shipNumber + "' and containerNumber != ''");
	}

	public List getWarehouse(String shipNumber) {
		return getHibernateTemplate().find("select warehouse from WorkTicket where shipNumber='" + shipNumber + "'");
	}

	public List findAllTickets() {
		List workTickets = null;
		String columns = "id, ticket, shipNumber,registrationNumber,lastName,firstName,jobType,jobMode,city,destinationCity,date1,date2,service,warehouse,targetActual,estimatedWeight,reviewStatus";
		workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket");
		try {
			workTickets = hibernateUtil.convertScalarToComponent(columns, workTickets, WorkTicket.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return workTickets;
	}

	String cartonCount;

	public String findTotalEstimatedCartons(Long ticket) {
		List totCartons = getHibernateTemplate().find("select sum(qty) from ItemsJbkEquip where ticket=" + ticket + " and type='M'");
		if (totCartons == null || totCartons.isEmpty() || totCartons.get(0) == null || totCartons.get(0).toString().equalsIgnoreCase("0")
				|| totCartons.get(0).toString().equalsIgnoreCase("")) {
			cartonCount = "0";
		} else {
			cartonCount = totCartons.get(0).toString();
		}
		return cartonCount;
	}

	public List findBillComlete(String shipNumber) {
		return this.getSession().createSQLQuery("select concat(if(billCompleteA is null,'1',billCompleteA) ,'#', if(billComplete is null ,'1',billComplete) ,'#', if(noMoreWork is null ,'1',if(noMoreWork>CURDATE(),'1',noMoreWork)),'#', if(auditComplete is null ,'1', auditComplete)) from billing where shipNumber='" + shipNumber + "'").list();
	}

	public List getStorageList(String shipNumber) {
		return getHibernateTemplate().find("from Storage where shipNumber='" + shipNumber + "' order by idNum");
	}

	public List findBillingPayMethod(String shipNumber) {
		List payMethod = new ArrayList();
		List billToPointCode = getHibernateTemplate().find("select billTo1Point from Billing where shipNumber='" + shipNumber + "'");
		if (!billToPointCode.isEmpty() && billToPointCode.get(0) != null) {
			payMethod = getHibernateTemplate().find("select description from RefMaster where parameter='PAYTYPE' and code='" + billToPointCode.get(0).toString() + "'");
		}

		return payMethod;

	}

	public List findBillingPayMethods(String payMethod) {
		return getHibernateTemplate().find("select description from RefMaster where parameter = 'PAYTYPE' and code='" + payMethod + "'");
	}

	public List getDailyOperationalLimit(Date date1, Date date2, String corpId) {
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			toDate = nowYYYYMMDD1.toString();
		}

		if (!(date2 == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
			fromDate = nowYYYYMMDD2.toString();
		}
		return getHibernateTemplate().find("Select count(*) from WorkTicket where targetActual <> 'cancelled' and service in ('PK', 'LD', 'DU', 'DL') and corpID = '"+corpId+"' and date1 >='"+ toDate +"' and date2 <='"+fromDate+"'");
	}
	
	public Boolean checkDate1(String warehouse, String service, Date date1, Long id, String corpId, Double estimatedVolume, Double estimatedWeight, Date date2){
		Boolean isDiff = false;
		String wHouse="";
		String serV="";
		Double estWeight=0.00;
		Double estVolume=0.00;
		Object dateOne=new Object();
		Object dateTwo=new Object();
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			fromDate = nowYYYYMMDD1.toString();
			
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(date2));
			toDate = nowYYYYMMDD2.toString();
			
			List date1List = getHibernateTemplate().find("select date1, warehouse, service, estimatedCubicFeet, estimatedWeight, date2 from WorkTicket where id='"+id+"' and corpID = '"+corpId+"'");
			
			Iterator it = date1List.iterator();
			while(it.hasNext()){
				  Object [] row=(Object[])it.next();
				  wHouse = row[1].toString();
				  serV = row[2].toString();
				  dateOne = row[0];
				  estVolume =  Double.parseDouble(row[3].toString());
				  estWeight =  Double.parseDouble(row[4].toString());
				  dateTwo = row[5];
			  }
			
			
			List getDiff1 = this.getSession().createSQLQuery("SELECT DATEDIFF('"+fromDate+"', '"+dateOne+"')").list();	
			List getDiff2 = this.getSession().createSQLQuery("SELECT DATEDIFF('"+toDate+"', '"+dateTwo+"')").list();
		
			if((Long.parseLong(getDiff1.get(0).toString()) == 0) && (Long.parseLong(getDiff2.get(0).toString()) == 0)){
				isDiff = false;
			}else{
				isDiff = true;
				return isDiff;
			}
			
			if(warehouse.equalsIgnoreCase(wHouse)){
				isDiff = false;
			}else{
				isDiff = true;
				return isDiff;
			}
			
			if(service.equalsIgnoreCase(serV)){
				isDiff = false;
			}else{
				isDiff = true;
				return isDiff;
			}
			if(estimatedVolume.doubleValue()== estVolume.doubleValue()){
				isDiff = false;
			}else{
				isDiff = true;
				return isDiff;
			}
			if(estimatedWeight.doubleValue()== estWeight.doubleValue()){
				isDiff = false;
			}else{
				isDiff = true;
				return isDiff;
			}
		}
		return isDiff;
	}

	public Boolean getDateDiff(Date date1, Long minServiceDay) {
		List weekDaysList = new ArrayList();
		List weekends=new ArrayList();
		List addDays = new ArrayList();
		Long weekDay=Long.parseLong("0");
		Boolean check = false;
		
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd hh:MM:ss");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			toDate = nowYYYYMMDD1.toString();
			
			weekDaysList = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+toDate+"', INTERVAL '"+minServiceDay+"' DAY) AS CHAR)").list();	
			weekends= this.getSession().createSQLQuery("SELECT WEEKDAY('"+weekDaysList.get(0)+"')").list();	
			
			weekDay = weekDay + minServiceDay;
			addDays = this.getSession().createSQLQuery("SELECT DATE_ADD(now(), INTERVAL '"+weekDay+"' DAY)").list();	
			
			List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+toDate+"', '"+addDays.get(0)+"')").list();	
			if(Long.parseLong(getDiff.get(0).toString())<0){
				check = true;
			}else{
				check = false;
			}
		}
	return check;
	}
	public List serviceTypeList(String corpId, String parameter )
	{
			List typeServiceList;
			typeServiceList = getHibernateTemplate().find(
					  "select code from RefMaster where  parameter= '" + parameter
						+ "'and corpID in ('TSFT','"
									+ hibernateUtil.getParentCorpID(corpId) + "','"
									+ corpId + "')  and bucket='Y' order by code ");
			return typeServiceList;
	}
    public String wtServiceList(String corpId){
    	String wttypeService="";
		List wtServiceTypeDyna=serviceTypeList(corpId, "TCKTSERVC");
		Iterator iter1 = wtServiceTypeDyna.iterator();
		while(iter1.hasNext())
		{
			if(wttypeService.equals(""))
			{
				wttypeService="'"+iter1.next().toString()+"'";
			}else{
				wttypeService=wttypeService+",'"+iter1.next().toString()+"'";
			}
			
		}
		return wttypeService;
    }
	public String getDailyOpsLimit(Date date1, Date date2, Long dailyCount, Long dailyCountPC, String sessionCorpID, String hubID, Double estimatedWeight, Double estimatedCubicFeet, Double dailyMaxEstWt, Double dailyMaxEstVol, Long id) {
		List warehouseHubList=new ArrayList();
		String countCheck="";
		List addDays = new ArrayList();
		List ticketCount = new ArrayList();
		Long tableDalilyLimit =0L;
		String sysdatetemp="";
		SimpleDateFormat dateformatnewDateBuilder = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder newDateBuilder = new StringBuilder(dateformatnewDateBuilder.format(new Date()));
		sysdatetemp = newDateBuilder.toString();
		String wttypeService=wtServiceList(sessionCorpID);
		Date tempDate=new Date();
		try {
			tempDate = dateformatnewDateBuilder.parse(sysdatetemp);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		boolean counterCheck=false;
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			toDate = nowYYYYMMDD1.toString();
		}

		if (!(date2 == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
			fromDate = nowYYYYMMDD2.toString();
		}
		
		List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+fromDate+"', '"+toDate+"')").list();	
		Long diff = Long.parseLong(getDiff.get(0).toString());
		if(diff>0){
			if(date2.before(tempDate)){}else{counterCheck=true;}
		}else{
			if(date1.before(tempDate)){}else{counterCheck=true;}
		}
		if(counterCheck){
		if(diff >= 0){
			String sendDate="";
			int intValueDiff = diff.intValue();
			for(int i=0;i<=intValueDiff;i++ ){
				
				addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+toDate+"', INTERVAL '"+i+"' DAY) AS CHAR)").list();
				sendDate = addDays.get(0).toString();
				List operationMgmtByValue  = this.getSession().createSQLQuery("select operationMgmtBy from systemdefault where corpID = '"+sessionCorpID+"' ").list();
				if(operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse")){
					warehouseHubList  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' and bucket = 'Y' ").list();
				}
				if(warehouseHubList!=null && !warehouseHubList.isEmpty() && warehouseHubList.get(0)!=null){
					ticketCount = this.getSession().createSQLQuery("Select count(*) from workticket t where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 ").list();
				}else {
					ticketCount = this.getSession().createSQLQuery("Select count(*) from workticket t, refmaster r where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 and r.parameter = 'HOUSE'").list();
				}
				
				Long totalTicket = Long.parseLong(ticketCount.get(0).toString());			
				List dailyCountFromTable = getHibernateTemplate().find("Select dailyLimits from OperationsDailyLimits where corpID = '"+sessionCorpID+"' and workDate ='"+ addDays.get(0) +"' and hubID = '"+hubID+"'");				
				List dailyWeightLimitList = this.getSession().createSQLQuery("Select if(dailyMaxEstWt is null or dailyMaxEstWt='',0.00,dailyMaxEstWt) from operationsdailylimits where corpID = '"+sessionCorpID+"' and workDate ='"+ addDays.get(0) +"' and hubID = '"+hubID+"'").list();
				List dailyVolLimitList = this.getSession().createSQLQuery("Select if(dailyMaxEstVol is null or dailyMaxEstVol='',0.00,dailyMaxEstVol) from operationsdailylimits where corpID = '"+sessionCorpID+"' and workDate ='"+ addDays.get(0) +"' and hubID = '"+hubID+"'").list();
				Double dailyWeightLimit = 0.00;
				Double dailyVolLimit = 0.00;
				if(dailyWeightLimitList!=null && !dailyWeightLimitList.isEmpty()){
					dailyWeightLimit = Double.parseDouble(dailyWeightLimitList.get(0).toString());
				}
				if(dailyVolLimitList!=null && !dailyVolLimitList.isEmpty()){
					dailyVolLimit = Double.parseDouble(dailyVolLimitList.get(0).toString());
				}
				if(dailyCountFromTable!=null && !dailyCountFromTable.isEmpty()){
		         	tableDalilyLimit = Long.parseLong(dailyCountFromTable.get(0).toString());
		         }
				List totalWeightVolume = null;
				List totalExistVolume = null;
				Double totalWeight = 0.00;
				Double totalVolume = 0.00;
				if(warehouseHubList!=null && !warehouseHubList.isEmpty() && operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse")){
					String query="Select SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t where (t.targetActual not in ('C','R','P') or t.id= '"+id+"' ) and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 ";
					totalWeightVolume = this.getSession().createSQLQuery(query).list();	
				}else{
					String query="Select SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t, refmaster r where (t.targetActual not in ('C','R','P') or t.id= '"+id+"' ) and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 and r.parameter = 'HOUSE'";
					totalWeightVolume = this.getSession().createSQLQuery(query).list();	
				}
				Double wtktTotalWeight =0.00;
				Double wtktTotalVolume =0.00;
				Iterator it = totalWeightVolume.iterator();
		         while (it.hasNext()) {
		        	 Object [] row=(Object[])it.next();
		        	 if(row[0]!=null){
		        		 totalWeight = Double.parseDouble(row[0].toString());
		        		 wtktTotalWeight = totalWeight;
		        		 if(estimatedWeight!=null && (!(estimatedWeight.toString().equals("")))){		        	 
				             wtktTotalWeight = totalWeight + estimatedWeight;
				         }
		        	 }else{
		        		 wtktTotalWeight = estimatedWeight;
		        	 }
		        	 if(row[1]!=null){
		        		 totalVolume = Double.parseDouble(row[1].toString());
		        		 wtktTotalVolume = totalVolume;
		        		 if(estimatedCubicFeet!=null && (!(estimatedCubicFeet.toString().equals("")))){
				        	 wtktTotalVolume = totalVolume + estimatedCubicFeet;
				         }
		        	 }else{
		        		 wtktTotalVolume = estimatedCubicFeet;
		        	 }
		         }
	
			    if((id != null) && (!(id.toString().equalsIgnoreCase("")))){
			    	if(warehouseHubList!=null && !warehouseHubList.isEmpty() && operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse")){
			    		String query="Select SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t where t.targetActual not in ('C') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 and t.id= '"+id+"'";
			    		totalExistVolume = this.getSession().createSQLQuery(query).list();				
			    	}else{
			    		String query="Select SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t, refmaster r where t.targetActual not in ('C') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+hubID+"' and '"+ addDays.get(0) +"' between t.date1 and  t.date2 and r.parameter = 'HOUSE' and t.id= '"+id+"'";
			    		totalExistVolume = this.getSession().createSQLQuery(query).list();
			    	}
			    	Double totalExistWt = 0.00;
					Double totalExistVol = 0.00;
					Iterator iter = totalExistVolume.iterator();
			         while (iter.hasNext()) {
			        	 Object [] row=(Object[])iter.next();
			        	 if(row[0]!=null){
			        		 totalExistWt = Double.parseDouble(row[0].toString());
			        		 if(totalWeight.doubleValue()>0){
			        			 if(totalWeight.doubleValue()>totalExistWt.doubleValue()){
			        				 totalWeight = totalWeight - totalExistWt;
			        			 }else{
			        				 totalWeight = totalExistWt - totalWeight;
			        			 }			        		 
			        		 }
			        		 if(estimatedWeight!=null && (!(estimatedWeight.toString().equals("")))){		        	 
					             wtktTotalWeight = totalWeight + estimatedWeight;
					         }else{
					        	 wtktTotalWeight = totalWeight; 
					         }
			        	 }
			        	 if(row[1]!=null){
			        		 totalExistVol = Double.parseDouble(row[1].toString());
			        		 if(totalVolume.doubleValue()>0){
			        			  if(totalVolume.doubleValue()>totalExistVol.doubleValue()){
			        				  totalVolume = totalVolume - totalExistVol;
				        			 }else{
				        				 totalVolume = totalExistVol - totalVolume;
				        			 }	    		 
			        		 }
			        		 if(estimatedCubicFeet!=null && (!(estimatedCubicFeet.toString().equals("")))){
					        	 wtktTotalVolume = totalVolume + estimatedCubicFeet;
					         }else{
					        	 wtktTotalVolume = totalVolume;
					         }
			        	 }	        	 	            
			         }
			    }		         
		         
		         if(hubID.equalsIgnoreCase("0") && (sessionCorpID.equalsIgnoreCase("SSCW"))){
		        	 String tempParentName="";
		        	 String tempParentTicket="";
		        	 String tempParentDailyLimitTicket="";
		        	 List operationsHubLimitValue=new ArrayList() ;
		        	 List operationsDailyLimitValue=new ArrayList() ;
		        	 Long totalOpHubLimit =0L;
		        	 Long totalParentTicket =0L;
		        	 Long totalParentDailyLimitTicket =0L;
		        	 Long totalTableDalilyLimit =0L;
		        	 Double totalMaxEstWtLimit = 0.00;
		        	 Double totalMaxEstVolLimit = 0.00;
		        	 Double parentDailyWeightLimit = 0.00;
		        	 Double parentDailyLimitWeightLimit = 0.00;
		        	 Double totalDailyWeightLimit = 0.00;
		        	 Double parentDailyVolLimit = 0.00;
		        	 Double parentDailyLimitVolLimit = 0.00;
		        	 Double totalDailyVolLimit = 0.00;
		        	 Long totalTicket1= 0L;
		        	 List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		        	 
		        	 if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
		    			 tempParentName = parentName.get(0).toString().trim();
		    			 operationsHubLimitValue  = this.getSession().createSQLQuery("Select count(ticket),SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t , refmaster r ,operationshublimits op " +
																	    			 "where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") " +
																	    			 "and r.corpid=t.corpid and t.corpID = '"+sessionCorpID+"'  " + 
																	    			 "and r.flex1='"+tempParentName+"' and r.parameter='HOUSE'  " +
																	    			 "and t.warehouse=r.code and op.hubid=t.warehouse " +
																	    			 "and  '"+ addDays.get(0) +"' between t.date1 and  t.date2").list();
		    			 
		    			 List parentDailyLimitTicketCount = this.getSession().createSQLQuery("select dailyLimits from operationsdailylimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"' and workDate ='"+ addDays.get(0) +"' ").list();
			    			 if(parentDailyLimitTicketCount!=null && !parentDailyLimitTicketCount.isEmpty() && parentDailyLimitTicketCount.get(0)!=null){
			    				 tempParentDailyLimitTicket = parentDailyLimitTicketCount.get(0).toString().trim();
			    				 totalParentDailyLimitTicket= Long.parseLong(tempParentDailyLimitTicket);
			    			 	}
			    		List parentDailyLimitMaxEstWt = this.getSession().createSQLQuery("select dailyMaxEstWt from operationsdailylimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"' and workDate ='"+ addDays.get(0) +"' ").list();
				    		if(parentDailyLimitMaxEstWt!=null && !parentDailyLimitMaxEstWt.isEmpty()){
				    			parentDailyLimitWeightLimit = Double.parseDouble(parentDailyLimitMaxEstWt.get(0).toString());
			 					}
				    	List parentDailyLimitMaxEstVol = this.getSession().createSQLQuery("select dailyMaxEstVol from operationsdailylimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"' and workDate ='"+ addDays.get(0) +"' ").list();
			    		 	if(parentDailyLimitMaxEstVol!=null && !parentDailyLimitMaxEstVol.isEmpty()){
			 					parentDailyLimitVolLimit = Double.parseDouble(parentDailyLimitMaxEstVol.get(0).toString());
			 					}
		    			 Iterator iter = operationsHubLimitValue.iterator();
				         while (iter.hasNext()) {
				        	 Object [] row=(Object[])iter.next();
				        	 if(row[0]!=null){
				        		 totalTableDalilyLimit = Long.parseLong(row[0].toString());
				        		 tableDalilyLimit = totalParentDailyLimitTicket;
				        		 totalTicket = totalTableDalilyLimit;
				        	 }
				        	 if(row[1]!=null){
				        		 totalDailyWeightLimit = Double.parseDouble(row[1].toString());
				        		 dailyWeightLimit = parentDailyLimitWeightLimit;
				        		 wtktTotalWeight = totalDailyWeightLimit;
					        		 if(estimatedWeight!=null && (!(estimatedWeight.toString().equals("")))){		        	 
							             wtktTotalWeight = totalDailyWeightLimit + estimatedWeight;
							         }
						        	 }else{
						        		 wtktTotalWeight = estimatedWeight;						        	 
						        	 }
				        	 if(row[2]!=null){
				        		 totalDailyVolLimit = Double.parseDouble(row[2].toString());
				        		 dailyVolLimit = parentDailyLimitVolLimit;
				        		 wtktTotalVolume = totalDailyVolLimit;
				        		 if(estimatedCubicFeet!=null && (!(estimatedCubicFeet.toString().equals("")))){
						        	 wtktTotalVolume = totalDailyVolLimit + estimatedCubicFeet;
						         }
				        	 }else{
				        		 wtktTotalVolume = estimatedCubicFeet;				        	 
				        	 }
				         }
				         if(tableDalilyLimit>0L || dailyWeightLimit.doubleValue()!=0.00 || dailyVolLimit.doubleValue()!=0.00){
								if(dailyWeightLimit.doubleValue()!=0.00){							
									if(wtktTotalWeight.doubleValue() > dailyWeightLimit.doubleValue()){
										return "GreaterWeightVolume:"+sendDate;
									}
								}
								if(dailyVolLimit.doubleValue()!=0.00){
									if(wtktTotalVolume.doubleValue() >dailyVolLimit.doubleValue()){
										return "GreaterWeightVolume:"+sendDate;
									}
								}
								if(tableDalilyLimit>=0L){
									if(totalTicket >= tableDalilyLimit){
										return "Greater:"+sendDate;
							 		}
								}
								if(tableDalilyLimit>0L){
									int	ticketPC=0;
									ticketPC = (dailyCountPC.intValue()*tableDalilyLimit.intValue())/100;
									if(ticketPC <= totalTicket){
										return "Percent:"+sendDate; 
									}
								}
				         }
				         operationsDailyLimitValue= this.getSession().createSQLQuery("Select count(ticket),SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t , refmaster r ,operationshublimits op " +
																	    			 "where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") " +
																	    			 "and r.corpid=t.corpid and t.corpID = '"+sessionCorpID+"'  " + 
																	    			 "and r.flex1='"+tempParentName+"' and r.parameter='HOUSE'  " +
																	    			 "and t.warehouse=r.code and op.hubid=t.warehouse " +
																	    			 "and  '"+ addDays.get(0) +"' between t.date1 and  t.date2").list();
				        
				         List parentDailyTicketCount = this.getSession().createSQLQuery("select dailyOpHubLimit from operationshublimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"'").list();
			    			 if(parentDailyTicketCount!=null && !parentDailyTicketCount.isEmpty() && parentDailyTicketCount.get(0)!=null){
			    				 tempParentTicket = parentDailyTicketCount.get(0).toString().trim();
			    				 totalParentTicket= Long.parseLong(tempParentTicket);
			    			 	}
		    			 List parentDailyMaxEstWt = this.getSession().createSQLQuery("select dailyMaxEstWt from operationshublimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"'").list();
			    			 if(parentDailyMaxEstWt!=null && !parentDailyMaxEstWt.isEmpty()){
			 					parentDailyWeightLimit = Double.parseDouble(parentDailyMaxEstWt.get(0).toString());
			 					}
			    		 List parentDailyMaxEstVol = this.getSession().createSQLQuery("select dailyMaxEstVol from operationshublimits where corpid='"+sessionCorpID+"' and hubid='"+tempParentName+"'").list();
			    		 	if(parentDailyMaxEstVol!=null && !parentDailyMaxEstVol.isEmpty()){
			 					parentDailyVolLimit = Double.parseDouble(parentDailyMaxEstVol.get(0).toString());
			 					} 
				         Iterator itr = operationsDailyLimitValue.iterator();
				         while (itr.hasNext()) {
				        	 Object [] row=(Object[])itr.next();
				        	 if(row[0]!=null){
				        		 totalOpHubLimit = Long.parseLong(row[0].toString());
				        		 dailyCount = totalParentTicket;
				        		 totalTicket1 = totalOpHubLimit;
				        	 }
				        	 if(row[1]!=null){
				        		 totalMaxEstWtLimit = Double.parseDouble(row[1].toString());
				        		 dailyMaxEstWt = parentDailyWeightLimit;
				        		 wtktTotalWeight = totalMaxEstWtLimit;
				        		 if(estimatedWeight!=null && (!(estimatedWeight.toString().equals("")))){		        	 
						             wtktTotalWeight = totalMaxEstWtLimit + estimatedWeight;
						         }
					        	 }else{
					        		 wtktTotalWeight = estimatedWeight;
				        	 }
				        	 if(row[2]!=null){
				        		 totalMaxEstVolLimit = Double.parseDouble(row[2].toString());
				        		 dailyMaxEstVol = parentDailyVolLimit ;
				        		 wtktTotalVolume = totalMaxEstVolLimit;
				        		 if(estimatedCubicFeet!=null && (!(estimatedCubicFeet.toString().equals("")))){
						        	 wtktTotalVolume = totalMaxEstVolLimit + estimatedCubicFeet;
						         }
				        	 }else{
				        		 wtktTotalVolume = estimatedCubicFeet;
				        	 }
				         }
				         
				         if((id != null) && (!(id.toString().equalsIgnoreCase("")))){						    		
						    		String query="Select SUM(estimatedWeight),SUM(estimatedCubicFeet) from workticket t where t.targetActual not in ('C') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"'  and '"+ addDays.get(0) +"' between t.date1 and  t.date2 and t.id= '"+id+"'";
						    		totalExistVolume = this.getSession().createSQLQuery(query).list();				
						    	
						    	Double totalExistWt = 0.00;
								Double totalExistVol = 0.00;
								Iterator itrt = totalExistVolume.iterator();
						         while (itrt.hasNext()) {
						        	 Object [] row=(Object[])itrt.next();
						        	 if(row[0]!=null){
						        		 totalExistWt = Double.parseDouble(row[0].toString());
						        		 if(totalMaxEstWtLimit.doubleValue()>0){
						        			 if(totalMaxEstWtLimit.doubleValue()>totalExistWt.doubleValue()){
						        				 totalMaxEstWtLimit = totalMaxEstWtLimit - totalExistWt;
						        			 }else{
						        				 totalMaxEstWtLimit = totalExistWt - totalMaxEstWtLimit;
						        			 }			        		 
						        		 }
						        		 if(estimatedWeight!=null && (!(estimatedWeight.toString().equals("")))){		        	 
								             wtktTotalWeight = totalMaxEstWtLimit + estimatedWeight;
								         }else{
								        	 wtktTotalWeight = totalMaxEstWtLimit; 
								         }
						        	 }
						        	 if(row[1]!=null){
						        		 totalExistVol = Double.parseDouble(row[1].toString());
						        		 if(totalMaxEstVolLimit.doubleValue()>0){
						        			  if(totalMaxEstVolLimit.doubleValue()>totalExistVol.doubleValue()){
						        				  totalMaxEstVolLimit = totalMaxEstVolLimit - totalExistVol;
							        			 }else{
							        				 totalMaxEstVolLimit = totalExistVol - totalMaxEstVolLimit;
							        			 }	    		 
						        		 }
						        		 if(estimatedCubicFeet!=null && (!(estimatedCubicFeet.toString().equals("")))){
								        	 wtktTotalVolume = totalMaxEstVolLimit + estimatedCubicFeet;
								         }else{
								        	 wtktTotalVolume = totalMaxEstVolLimit;
								         }
						        	 }	        	 	            
						         }
						    }
				         
				         if(wtktTotalWeight.doubleValue() > dailyMaxEstWt.doubleValue() || wtktTotalVolume.doubleValue() > dailyMaxEstVol.doubleValue()){
						        return "GreaterWeightVolume:"+sendDate;
						    }	
							if(totalTicket1 >= dailyCount){
								return "Greater:"+sendDate;
				 			}					
							int	ticketPC=0;
							ticketPC = (dailyCountPC.intValue()*dailyCount.intValue())/100;
							if(ticketPC <= totalTicket1){
								return "Percent:"+sendDate; 
							}
		        	 }
		        	 return countCheck;
		         }else{
				if(tableDalilyLimit>0L || dailyWeightLimit.doubleValue()!=0.00 || dailyVolLimit.doubleValue()!=0.00){
					if(dailyWeightLimit.doubleValue()!=0.00){							
						if(wtktTotalWeight.doubleValue() > dailyWeightLimit.doubleValue()){
							return "GreaterWeightVolume:"+sendDate;
						}
					}
					if(dailyVolLimit.doubleValue()!=0.00){
						if(wtktTotalVolume.doubleValue() >dailyVolLimit.doubleValue()){
							return "GreaterWeightVolume:"+sendDate;
						}
					}
					if(tableDalilyLimit>=0L){
						if(totalTicket >= tableDalilyLimit){
							return "Greater:"+sendDate;
				 		}
					}
					if(tableDalilyLimit>0L){
						int	ticketPC=0;
						ticketPC = (dailyCountPC.intValue()*tableDalilyLimit.intValue())/100;
						if(ticketPC <= totalTicket){
							return "Percent:"+sendDate; 
						}
					}
				  }else{
					if(wtktTotalWeight.doubleValue() > dailyMaxEstWt.doubleValue() || wtktTotalVolume.doubleValue() > dailyMaxEstVol.doubleValue()){
				        return "GreaterWeightVolume:"+sendDate;
				    }	
					if(totalTicket >= dailyCount){
						return "Greater:"+sendDate;
		 			}					
					int	ticketPC=0;
					ticketPC = (dailyCountPC.intValue()*dailyCount.intValue())/100;
					if(ticketPC <= totalTicket){
						return "Percent:"+sendDate; 
					}
				}	
		      }	
			}
		  }
		}
		return countCheck;
	}
	
	public Boolean getCheckedWareHouse(String wareHouse, String corpId){
		Boolean check = false;
		
		List bucketCheck = getHibernateTemplate().find("select bucket from RefMaster where code='"+wareHouse+"' and parameter = 'HOUSE' and (flex1 is not null and length(trim(flex1))>0) ");
		
		if(!bucketCheck.isEmpty() && bucketCheck.get(0).toString().equalsIgnoreCase("Y")){
			check = true;
		}else{
			check = false;
		}
		
		return check;
	}
	
	public List getWorkPlanList(Date fromDate, Date toDate, String hubID,String wHouse,String wareHouseUtilization, String corpId){		
		List workPlanList=new ArrayList();
		List workList =new ArrayList();
		String dateFrom = "";
		String dateTo = "";
		String query="";
		String wttypeService=wtServiceList(corpId);
		if (!(fromDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
			dateFrom = nowYYYYMMDD1.toString();
		}

		if (!(toDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(toDate));
			dateTo = nowYYYYMMDD2.toString();
		}
		if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
			query="select ticket, date1, date2, warehouse, ifnull(estimatedweight,0), service, datediff(date2,date1)+1 'duration', round(ifnull(estimatedweight,0)/(datediff(date2,date1)+1),0) 'avgWt',"
					+ "originCountry, destinationCountry, op.dailyOpHubLimit, op.dailyMaxEstWt,if(requiredcrew is null or requiredcrew='','0',requiredcrew) from workticket w"
					+ ", refmaster r, operationshublimits op where r.parameter = 'HOUSE' and r.flex1 = '"+hubID+"' and w.warehouse = r.code and w.corpid=r.corpid and op.hubID=w.warehouse "
					+ "and w.corpid = '"+ corpId +"' and w.service in ("+wttypeService+") and w.warehouse not in ('4','A','L','T','M','D','8','P') and targetactual not in ('C') "
					+ "and ((date1  between '"+ dateFrom +"' and  '"+ dateTo +"') or(date2 between '"+ dateFrom +"' and  '"+ dateTo +"') or  (date1 <  '"+ dateFrom +"' and  date2 > '"+ dateTo +"')) order by 2,3";
			workList=this.getSession().createSQLQuery(query).list();
		}else{
			query="select ticket, date1, date2, warehouse, ifnull(estimatedweight,0), service, datediff(date2,date1)+1 'duration', round(ifnull(estimatedweight,0)/(datediff(date2,date1)+1),0) 'avgWt',"
					+ "originCountry, destinationCountry from workticket "
					+ "where corpid = '"+ corpId +"' and warehouse like '%"+wHouse+"%' and service in ("+wttypeService+") and targetactual not in ('C') "
					+ "and ((date1  between '"+ dateFrom +"' and  '"+ dateTo +"') or(date2 between '"+ dateFrom +"' and  '"+ dateTo +"') or  (date1 <  '"+ dateFrom +"' and  date2 > '"+ dateTo +"')) order by 2,3";
			workList=this.getSession().createSQLQuery(query).list();
		}
		Iterator it = workList.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			WorkPlanDTO dto= new WorkPlanDTO();
			if(row[0] == null){dto.setTicket("");}else{dto.setTicket(row[0]);}
			if(row[1] == null){dto.setDate1("");}else{dto.setDate1(row[1]);}
			if(row[2] == null){dto.setDate2("");}else{dto.setDate2(row[2]);}
			if(row[3] == null){dto.setWarehouse("");}else{dto.setWarehouse(row[3]);}
			if(row[4] == null){dto.setEstimatedweight("");}else{dto.setEstimatedweight(row[4]);}
			if(row[5] == null){dto.setService("");}else{dto.setService(row[5]);}
			if(row[6] == null){dto.setDuration("");}else{dto.setDuration(row[6]);}
			if(row[7] == null){dto.setAvgWt("");}else{dto.setAvgWt(row[7]);}
			if(row[8] == null){dto.setOriginCountry("");}else{dto.setOriginCountry(row[8]);}
			if(row[9] == null){dto.setDestinationCountry("");}else{dto.setDestinationCountry(row[9]);}
			if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
				if(row[10] == null){dto.setDailyOpHubLimit("");}else{dto.setDailyOpHubLimit(row[10]);}
				if(row[11]== null){dto.setDailyMaxEstWt("");}else{dto.setDailyMaxEstWt(row[11]);}
				if(row[12]== null){dto.setRequiredcrew("");}else{dto.setRequiredcrew(row[12]);}
			}
			String dateUF1 = row[1].toString();
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			Date displayDate = null;
			try {
				displayDate = dateformatYYYYMMDD1.parse(dateUF1);
				dto.setDateDisplay(displayDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(displayDate.after(toDate) || displayDate.before(fromDate)){
				
			}else{
				workPlanList.add(dto);
			}
			if(row[6] != null){
			for(int i=1; i<Integer.parseInt(row[6].toString()); i++){
				WorkPlanDTO dto1= new WorkPlanDTO();
				if(row[0] == null){dto1.setTicket("");}else{dto1.setTicket(row[0]);}
				if(row[1] == null){dto1.setDate1("");}else{dto1.setDate1(row[1]);}
				if(row[2] == null){dto1.setDate2("");}else{dto1.setDate2(row[2]);}
				if(row[3] == null){dto1.setWarehouse("");}else{dto1.setWarehouse(row[3]);}
				if(row[4] == null){dto1.setEstimatedweight("");}else{dto1.setEstimatedweight(row[4]);}
				if(row[5] == null){dto1.setService("");}else{dto1.setService(row[5]);}
				if(row[6] == null){dto1.setDuration("");}else{dto1.setDuration(row[6]);}
				if(row[7] == null){dto1.setAvgWt("");}else{dto1.setAvgWt(row[7]);}
				if(row[8] == null){dto1.setOriginCountry("");}else{dto1.setOriginCountry(row[8]);}
				if(row[9] == null){dto1.setDestinationCountry("");}else{dto1.setDestinationCountry(row[9]);}
				if((wareHouseUtilization!=null && !wareHouseUtilization.equalsIgnoreCase("") && wareHouseUtilization.equalsIgnoreCase("Yes"))){
					if(row[10] == null){dto1.setDailyOpHubLimit("");}else{dto1.setDailyOpHubLimit(row[10]);}
					if(row[11]== null){dto1.setDailyMaxEstWt("");}else{dto1.setDailyMaxEstWt(row[11]);}
					if(row[12]== null){dto1.setRequiredcrew("");}else{dto1.setRequiredcrew(row[12]);}
				}
				List addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+row[1]+"', INTERVAL '"+i+"' DAY) AS CHAR)").list();
				
				String dateUF = addDays.get(0).toString();
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				Date displayDate1 = null;
				try {
					displayDate1 = dateformatYYYYMMDD2.parse(dateUF);
					dto1.setDateDisplay(displayDate1);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if(displayDate1.after(toDate) || displayDate1.before(fromDate)){
					
				}else{
					workPlanList.add(dto1);
				}
				
			  }
			}
		}
		return workPlanList;
	}
	
	public String getHubValueWorkPlanList(String hubID, String corpId){
		String hubWorkPlanList="";
		String query="select concat(dailyOpHubLimit,'~',dailyMaxEstWt) from operationshublimits where corpid='"+corpId+"' and hubid='"+hubID+"'";
		List al = this.getSession().createSQLQuery(query).list();
		if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)) {
			hubWorkPlanList = al.get(0).toString();
		}
		return hubWorkPlanList;
	}	
	
	public List goSOChild(Long serviceOrderId, String corpID, Long id) {
		return getHibernateTemplate().find("from  WorkTicket where serviceOrderId='"+serviceOrderId+"' and ticket <> '"+id+"' and corpID='"+corpID+"'");
	}
	
	public List goNextSOChild(Long serviceOrderId, String corpID, Long ticket) {
		return getHibernateTemplate().find("select id from  WorkTicket where serviceOrderId='"+serviceOrderId+"' and ticket >'"+ticket+"' and corpID='"+corpID+"' order by 1");
	}
	
	public List goPrevSOChild(Long serviceOrderId, String corpID, Long ticket) {
		return getHibernateTemplate().find("select id from  WorkTicket where serviceOrderId='"+serviceOrderId+"' and ticket <'"+ticket+"' and corpID='"+corpID+"' order by 1 desc");
	}
	
	public List findMaximumTicket(String shipNm) {
		return getHibernateTemplate().find("select max(ticket) from WorkTicket where shipNumber=?", shipNm);
	}
	
	public List findMinimumTicket(String shipNm) {
		return getHibernateTemplate().find("select min(ticket) from WorkTicket where shipNumber=?", shipNm);
	}
	
	public List findCountTicket(String shipNm) {
		return getHibernateTemplate().find("select count(*) from WorkTicket where shipNumber=?", shipNm);
	}

	public List<WorkTicket> getWorkTicketByTicekt(Long ticket) {
		return getHibernateTemplate().find("from WorkTicket where ticket='"+ticket+"'");
	}

	public void updateServiceOrderInland(String vendorCode, Long id, String sessionCorpID) {
		 getHibernateTemplate().bulkUpdate("update ServiceOrder set inlandCode='"+vendorCode+"' where id="+id+" and corpID='"+sessionCorpID+"'");
	}
	public void updateTrackingStatusBeginLoad(Date date1, String shipNumber, String sessionCorpID)
	{    
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update TrackingStatus set beginload='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	 protected HttpServletRequest getRequest() {
	        return ServletActionContext.getRequest();  
	    }
	public void updateTicketStatusPackADetails(Date Date2, Long soId,String sessionCorpID){
		if(Date2!=null){
			 Date date1Str = Date2;
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
				getHibernateTemplate().bulkUpdate("update TrackingStatus set packA='"+formatedDate1+"',deliveryA='"+formatedDate1+"',updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() where id='"+soId+"'  and corpID='"+sessionCorpID+"'");
		}
	}
	
	public void updateBillingGSTAccess1Service(Date date1, String shipNumber, String sessionCorpID)
	{    
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update Billing set gstAccess1Service='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	
	public void updateBillingGSTAccess2Service(Date date1, String shipNumber, String sessionCorpID)
	{    
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update Billing set gstAccess2Service='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	
	public void updateBillingGSTAccess3Service(Date date1, String shipNumber, String sessionCorpID)
	{    
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update Billing set gstAccess3Service='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	
	public void updateTrackingStatusLoadA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus, String loginUser){
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update TrackingStatus set loadA='"+formatedDate1+"' ,updatedOn=now(), updatedBy='"+loginUser+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
			getHibernateTemplate().bulkUpdate("update Billing set revenueRecognition='"+formatedDate1+"' ,updatedOn=now(), updatedBy='"+loginUser+"' where shipNumber='"+shipNumber+"' and revenueRecognition is null  and corpID='"+sessionCorpID+"'");
		if(updateSoStatus){
		}
	}
	}
	public void updateBillingStorageOut(Date date1, String shipNumber, String sessionCorpID){
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update Billing set storageOut='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	public List countReleaseStorageDate(String shipNumber){
		return getHibernateTemplate().find("Select count(*) from Storage where shipNumber = '"+shipNumber+"' and releaseDate is null");
	}
	public List locationReleaseStorageDate(String shipNumber, String sessionCorpID){
		return getHibernateTemplate().find("Select id from Storage where shipNumber = '"+shipNumber+"' and releaseDate is null and corpID='"+sessionCorpID+"'");
	}
	public void updateTrackingStatusDeliveryA(Date date1, String shipNumber, String sessionCorpID, Boolean updateSoStatus){		
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );			
			getHibernateTemplate().bulkUpdate("update TrackingStatus set deliveryA='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");			
		if(updateSoStatus){
		}	
		}
	}
	public void updateTrackingStatusPackA(Date date1, String shipNumber, String sessionCorpID,Boolean updateSoStatus){		
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );			
			getHibernateTemplate().bulkUpdate("update TrackingStatus set packA='"+formatedDate1+"' where shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'");		
		if(updateSoStatus){
		}
	}
	}
	public void updateTrackingStatusLeftWhon(Date date1, String shipNumber, String sessionCorpID){
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update TrackingStatus set leftWHOn='"+formatedDate1+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	}
	public void updateStorageReleasedDate(String shipNumber, String sessionCorpID, String updatedBy){
		getHibernateTemplate().bulkUpdate("update Storage set releaseDate = now(),updatedOn = now(),updatedBy='"+updatedBy+"' where shipNumber='"+shipNumber+"' and releaseDate is null and corpID='"+sessionCorpID+"'");
	}
	public void updateTrackingStatusDeliveryShipper(Date date1, String shipNumber, String sessionCorpID){
		if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update TrackingStatus set deliveryShipper='"+formatedDate1+"' where shipNumber='"+shipNumber+"' and deliveryShipper is null  and corpID='"+sessionCorpID+"'");
		}
	}
	public List checkCompanyDivisionAgentCode(String corpID){
		return getHibernateTemplate().find("select bookingAgentCode from CompanyDivision where corpID='"+corpID+"'" );
	}

	public String getStateDesc(String check) {
		String state=new String();
		List stateList=new ArrayList();
		stateList= getHibernateTemplate().find("select description from RefMaster where parameter='STATE' and code='"+check+"'");
		if(stateList!=null &&(!stateList.isEmpty())){
			state=stateList.get(0).toString();
		}
		return state;
	}

	public List getStateDescList(String originCountry) {
		return getHibernateTemplate().find("select description from RefMaster where parameter='STATE' and bucket2='"+originCountry+"' order by description");
	}

	public List getDispathMapList(Date date1, Date date2, String sessionCorpID, Boolean originMap,Boolean destinMap,String hubId) {
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			fromDate = nowYYYYMMDD1.toString();
		}

		if (!(date2 == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
			toDate = nowYYYYMMDD2.toString();
		}
		if(originMap==true && destinMap==true){
		String tempQuery = "select " +
				"concat((if(w.address1 is null,'',w.address1)),',',(if(w.city is null,'',w.city)),',',(if(w.state is null,'',w.state)),',',(if(w.originCountry is null,'',w.originCountry)),'-',(if(w.zip is null,'',w.zip))) as address, " +
				"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
				"w.state, w.city, w.zip " +
				"from workticket w , refmaster r " +
				"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
				"and r.bucket = 'Y' " +
				"and w.service in ('PK','LD','PL') " +
				"and w.targetActual <> 'C' " +
				"and w.corpID = '"+sessionCorpID+"' " +
				"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
				if(!hubId.equalsIgnoreCase("")){
					tempQuery += " and flex1 = '"+hubId+"' " ;
				}
		 tempQuery += "union select " +
				"concat((if(w.destinationAddress1 is null,'',w.destinationAddress1)),',',(if(w.destinationCity is null,'',w.destinationCity)),',',(if(w.destinationState is null,'',w.destinationState)),',',(if(w.destinationCountry is null,'',w.destinationCountry)),'-',(if(w.destinationZip is null,'',w.destinationZip))) as address, " +
				"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
				"w.destinationState, w.destinationCity, w.destinationZip " +
				"from workticket w , refmaster r  " +
				"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
				"and r.bucket = 'Y' " +
				"and w.service in ('DL','DU') " +
				"and w.targetActual <> 'C' " +
				"and w.corpID = '"+sessionCorpID+"' " +
				"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
				if(!hubId.equalsIgnoreCase("")){
					tempQuery += " and flex1 = '"+hubId+"' " ;
				}
		
				
		tempQuery += "order by 8, 9, 10";
		
		
		return getSession().createSQLQuery(tempQuery).list();
		}
		else if(originMap==true && destinMap==false){
			String tempQuery = "select " +
			"concat((if(w.address1 is null,'',w.address1)),',',(if(w.city is null,'',w.city)),',',(if(w.state is null,'',w.state)),',',(if(w.originCountry is null,'',w.originCountry)),'-',(if(w.zip is null,'',w.zip))) as address, " +
			"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
			"w.state, w.city, w.zip " +
			"from workticket w , refmaster r " +
			"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
			"and r.bucket = 'Y' " +
			"and w.service in ('PK','LD','PL') " +
			"and w.targetActual <> 'C' " +
			"and w.corpID = '"+sessionCorpID+"' " +
			"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
			if(!hubId.equalsIgnoreCase("")){
				tempQuery += " and flex1 = '"+hubId+"' " ;
			}
			tempQuery += "order by 8, 9, 10";
			return getSession().createSQLQuery(tempQuery).list();	
		}
		else if(originMap==false && destinMap==true){
			String tempQuery = "select " +
			"concat((if(w.destinationAddress1 is null,'',w.destinationAddress1)),',',(if(w.destinationCity is null,'',w.destinationCity)),',',(if(w.destinationState is null,'',w.destinationState)),',',(if(w.destinationCountry is null,'',w.destinationCountry)),'-',(if(w.destinationZip is null,'',w.destinationZip))) as address, " +
			"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
			"w.destinationState, w.destinationCity, w.destinationZip " +
			"from workticket w , refmaster r  " +
			"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
			"and r.bucket = 'Y' " +
			"and w.service in ('DL','DU') " +
			"and w.targetActual <> 'C' " +
			"and w.corpID = '"+sessionCorpID+"' " +
			"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
			if(!hubId.equalsIgnoreCase("")){
				tempQuery += " and flex1 = '"+hubId+"' " ;
			}
			tempQuery += "order by 8, 9, 10";
			return getSession().createSQLQuery(tempQuery).list();
		}else{
			String tempQuery = "select " +
			"concat((if(w.address1 is null,'',w.address1)),',',(if(w.city is null,'',w.city)),',',(if(w.state is null,'',w.state)),',',(if(w.originCountry is null,'',w.originCountry)),'-',(if(w.zip is null,'',w.zip))) as address, " +
			"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
			"w.state, w.city, w.zip " +
			"from workticket w , refmaster r " +
			"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
			"and r.bucket = 'Y' " +
			"and w.service in ('PK','LD','PL') " +
			"and w.targetActual <> 'C' " +
			"and w.corpID = '"+sessionCorpID+"' " +
			"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
			if(!hubId.equalsIgnoreCase("")){
				tempQuery += " and flex1 = '"+hubId+"' " ;
			}
	 tempQuery += "union select " +
			"concat((if(w.destinationAddress1 is null,'',w.destinationAddress1)),',',(if(w.destinationCity is null,'',w.destinationCity)),',',(if(w.destinationState is null,'',w.destinationState)),',',(if(w.destinationCountry is null,'',w.destinationCountry)),'-',(if(w.destinationZip is null,'',w.destinationZip))) as address, " +
			"DATE_FORMAT(w.date1,'%b %d'), DATE_FORMAT(w.date2,'%b %d'), w.estimatedWeight, w.service, concat(w.firstName,' ', w.lastName), w.ticket, " +
			"w.destinationState, w.destinationCity, w.destinationZip " +
			"from workticket w , refmaster r  " +
			"where (w.date1 between '"+fromDate+"' and '"+toDate+"' or w.date2 between '"+fromDate+"' and  '"+toDate+"' or w.date1 < '"+fromDate+"' and w.date2 > '"+toDate+"') " +
			"and r.bucket = 'Y' " +
			"and w.service in ('DL','DU') " +
			"and w.targetActual <> 'C' " +
			"and w.corpID = '"+sessionCorpID+"' " +
			"and w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID " ;
			if(!hubId.equalsIgnoreCase("")){
				tempQuery += " and flex1 = '"+hubId+"' " ;
			}
	
			
	tempQuery += "order by 8, 9, 10";
	
	
	return getSession().createSQLQuery(tempQuery).list();
		}
	}
	
	public OperationsHubLimits getOperationsHub(String warehouse, String sessionCorpID){
		String hub = "";
		List opsHubList = new ArrayList();
		OperationsHubLimits operationsHubLimits = new OperationsHubLimits();
		List operationMgmtByValue  = this.getSession().createSQLQuery("select operationMgmtBy from systemdefault where corpID = '"+sessionCorpID+"' ").list();
		if(operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse")){
		 opsHubList = getHibernateTemplate().find("from OperationsHubLimits where hubID = '"+warehouse+"'");
		 if(!opsHubList.isEmpty()){
				operationsHubLimits = (OperationsHubLimits)opsHubList.get(0);				
		 }else{	
			 List hublist =  getSession().createSQLQuery("select flex1 from refmaster where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'HOUSE' and code = '"+warehouse+"'").list();
			 if(!hublist.isEmpty()){
				 hub = hublist.get(0).toString();
				 opsHubList = getHibernateTemplate().find("from OperationsHubLimits where hubID = '"+hub+"'");
				 if(!opsHubList.isEmpty()){
					 operationsHubLimits = (OperationsHubLimits)opsHubList.get(0);
				}
			}
		}
		}else{	
			 List hublist =  getSession().createSQLQuery("select flex1 from refmaster where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'HOUSE' and code = '"+warehouse+"'").list();
			 if(!hublist.isEmpty()){
				 hub = hublist.get(0).toString();
				 opsHubList = getHibernateTemplate().find("from OperationsHubLimits where hubID = '"+hub+"'");
				 if(!opsHubList.isEmpty()){
					 operationsHubLimits = (OperationsHubLimits)opsHubList.get(0);
				}
			}
		}
		return operationsHubLimits;
	}

	public List getHubIdList(String sessionCorpID, String hubId) {
		return this.getSession().createSQLQuery("select distinct concat(\"'\",warehouse,\"'\") from workticket w, refmaster r where w.warehouse=r.code and r.parameter='HOUSE' and w.corpID=r.corpID and w.corpID='"+sessionCorpID+"' and flex1 = '"+hubId+"' ").list();
	}

	public List getOpsList(Date fromDate, String hubID, String corpID) {
		String queryString="";
		String warehouseHub="";
		String totalWtVolQuery="";
		String comment="";
		String status = "";
		String day = "";
		String hubWarehouseOperationValue="";
		List warehouseHubList=new ArrayList();
		List operationsList= new ArrayList();
		String today = "";
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
		today = nowYYYYMMDD1.toString();
		for(int i=1; i<=14; i++){
			Date tempDate = new Date();
			try {
				tempDate = dateformatYYYYMMDD1.parse(today);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Calendar calc = Calendar.getInstance();
			calc.setTime(tempDate);
			calc.add(Calendar.DATE, i);
			Date tommDateTemp = calc.getTime();
			
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(tommDateTemp));
			String processDate = nowYYYYMMDD2.toString();
			
			int dailyOpHubLimit  = 0;
			int dailyOpHubLimitPC = 0;
			double dailyOpHubWeight = 0.0;
			double dailyOpHubVolume = 0.0;
			BigDecimal crewHubLimit = new BigDecimal(0.00);
			BigDecimal crewThreshHoldLimit = new BigDecimal(0.00);			
			List<OperationsHubLimits> operationsHubList = getHibernateTemplate().find("from OperationsHubLimits where hubID = '"+hubID+"'" );
			if(!operationsHubList.isEmpty()){
				OperationsHubLimits operationsHubLimits = operationsHubList.get(0);
				dailyOpHubLimit  = Integer.parseInt(operationsHubLimits.getDailyOpHubLimit());
				dailyOpHubLimitPC = Integer.parseInt(operationsHubLimits.getDailyOpHubLimitPC());
				dailyOpHubWeight = operationsHubLimits.getDailyMaxEstWt().doubleValue();
				dailyOpHubVolume = operationsHubLimits.getDailyMaxEstVol().doubleValue();
				if(operationsHubLimits.getDailyCrewCapacityLimit()== null || operationsHubLimits.getDailyCrewCapacityLimit().equalsIgnoreCase("")){
					crewHubLimit = new BigDecimal(0.00);
				}else{					
					crewHubLimit = new BigDecimal(operationsHubLimits.getDailyCrewCapacityLimit()+".00");
				}
				if(operationsHubLimits.getDailyCrewCapacityLimit()== null || operationsHubLimits.getDailyCrewCapacityLimit().equalsIgnoreCase("")){
					crewThreshHoldLimit = new BigDecimal(0.00);
				}else{	
					crewThreshHoldLimit = new BigDecimal(operationsHubLimits.getDailyCrewThreshHoldLimit());
				}
			}
			
			int dailyLimit = 0;
			
			double dailyWeight = 0.0;
			double dailyVolume = 0.0;
			BigDecimal crewDailyLimit = new BigDecimal(0.00);
			List operationMgmtByValue  = this.getSession().createSQLQuery("select operationMgmtBy from systemdefault where corpID = '"+corpID+"' ").list();
			if(operationMgmtByValue!=null && !operationMgmtByValue.isEmpty() && operationMgmtByValue.get(0)!=null && operationMgmtByValue.get(0).toString().equals("By Warehouse")){
				warehouseHubList  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'HOUSE' and corpID = '"+corpID+"' and code = '"+hubID+"' ").list();
			}
			if(warehouseHubList !=null && !warehouseHubList.isEmpty()&& warehouseHubList.get(0)!=null ){
				warehouseHub= warehouseHubList.get(0).toString();
				List<OperationsDailyLimits> operationsDailyList = getHibernateTemplate().find("from OperationsDailyLimits where workDate = '"+processDate+"' AND hubID = '"+warehouseHub+"'");
				if(!operationsDailyList.isEmpty()){
					OperationsDailyLimits operationsDailyLimits = operationsDailyList.get(0);
					dailyLimit = Integer.parseInt(operationsDailyLimits.getDailyLimits());
					comment = operationsDailyLimits.getComment();
					dailyWeight = operationsDailyLimits.getDailyMaxEstWt().doubleValue();
					dailyVolume = operationsDailyLimits.getDailyMaxEstVol().doubleValue();
					if(operationsDailyLimits.getDailyCrewCapacityLimit() == null || operationsDailyLimits.getDailyCrewCapacityLimit().equalsIgnoreCase("")){
						crewDailyLimit = new BigDecimal(0.00);
					}else{					
						crewDailyLimit = new BigDecimal(operationsDailyLimits.getDailyCrewCapacityLimit()+".00");
					}
					if(dailyLimit == 0){
						dailyLimit = dailyOpHubLimit;
						if((dailyWeight == 0.0) || (dailyVolume == 0.0)){
							dailyWeight = dailyOpHubWeight;
							dailyVolume = dailyOpHubVolume;
						}
					}else{
						if((dailyWeight == 0.0) || (dailyVolume == 0.0)){
							dailyWeight = dailyOpHubWeight;
							dailyVolume = dailyOpHubVolume;
						}
					}
					if(crewDailyLimit.equals(BigDecimal.ZERO)){
						crewDailyLimit = crewHubLimit;
					}
				}else{
					dailyLimit = dailyOpHubLimit;
					dailyWeight = dailyOpHubWeight;
					dailyVolume = dailyOpHubVolume;
					comment = "";
					crewDailyLimit = crewHubLimit;
				}
			}else{
			List<OperationsDailyLimits> operationsDailyList = getHibernateTemplate().find("from OperationsDailyLimits where workDate = '"+processDate+"' AND hubID = '"+hubID+"'");
			if(!operationsDailyList.isEmpty()){
				OperationsDailyLimits operationsDailyLimits = operationsDailyList.get(0);
				dailyLimit = Integer.parseInt(operationsDailyLimits.getDailyLimits());
				comment = operationsDailyLimits.getComment();
				dailyWeight = operationsDailyLimits.getDailyMaxEstWt().doubleValue();
				dailyVolume = operationsDailyLimits.getDailyMaxEstVol().doubleValue();
				if(operationsDailyLimits.getDailyCrewCapacityLimit()==null || operationsDailyLimits.getDailyCrewCapacityLimit().equalsIgnoreCase("")){
					crewDailyLimit = new BigDecimal(0.00);
				}else{				
					crewDailyLimit = new BigDecimal(operationsDailyLimits.getDailyCrewCapacityLimit()+".00");
				}
				if(dailyLimit == 0){
					dailyLimit = dailyOpHubLimit;
					if((dailyWeight == 0.0) || (dailyVolume == 0.0)){
						dailyWeight = dailyOpHubWeight;
						dailyVolume = dailyOpHubVolume;
					}
				}else{
					if((dailyWeight == 0.0) || (dailyVolume == 0.0)){
						dailyWeight = dailyOpHubWeight;
						dailyVolume = dailyOpHubVolume;
					}
				}
				if(crewDailyLimit.equals(BigDecimal.ZERO)){
					crewDailyLimit = crewHubLimit;
				}
			}else{
				dailyLimit = dailyOpHubLimit;
				dailyWeight = dailyOpHubWeight;
				dailyVolume = dailyOpHubVolume;
				comment = "";
				crewDailyLimit = crewHubLimit;
			}
			}
			
			String queryStringOff = "select count(*) from calendarfile where username = '"+ corpID +"' and surveydate = '"+ processDate +"'";
			List tempOffList = this.getSession().createSQLQuery(queryStringOff).list();
			int tempOff = Integer.parseInt(tempOffList.get(0).toString());
			
			if(calc.get(Calendar.DAY_OF_WEEK) == 1){
				day="Sunday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 2){
				day="Monday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 3){
				day="Tuesday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 4){
				day="Wednesday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 5){
				day="Thursday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 6){
				day="Friday";
			}else if(calc.get(Calendar.DAY_OF_WEEK) == 7){
				day="Saturday";
			}
			
			List hubWarehouseOprValue  = this.getSession().createSQLQuery("select hubWarehouseOperationalLimit from systemdefault where corpID = '"+corpID+"' ").list();
			if(hubWarehouseOprValue!=null && !hubWarehouseOprValue.isEmpty() && hubWarehouseOprValue.get(0)!=null && hubWarehouseOprValue.get(0).toString().equals("Ticket")){
					
				if(warehouseHubList !=null && !warehouseHubList.isEmpty()&& warehouseHubList.get(0)!=null ){
					warehouseHub= warehouseHubList.get(0).toString();
					totalWtVolQuery="select SUM(estimatedWeight),SUM(estimatedCubicFeet),count(*) as 'TicketCount' from workticket t where t.targetActual not in ('C','R','P') and t.service in ('PK', 'LD', 'DU', 'DL','UP','PL') and t.corpID = '"+corpID+"' and t.warehouse = '"+warehouseHub+"' and '"+ processDate +"' between date1 and  date2 "; 
				}else{
					totalWtVolQuery="select SUM(estimatedWeight),SUM(estimatedCubicFeet),count(*) as 'TicketCount' from workticket t, refmaster r where t.targetActual not in ('C','R','P') and t.service in ('PK', 'LD', 'DU', 'DL','UP','PL') and t.corpID = '"+corpID+"' and  r.corpID = t.corpID and t.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+hubID+"' and '"+ processDate +"' between date1 and  date2 and r.parameter = 'HOUSE' ";
				}
			
			List totalWeightVolume = null;
			totalWeightVolume = this.getSession().createSQLQuery(totalWtVolQuery).list();
			Double totalWeightTemp = 0.0;
			Double totalVolumeTemp = 0.0;
			int tecketCountemp = 0;
			Iterator it = totalWeightVolume.iterator();
	         while (it.hasNext()) {
	        	 Object [] row=(Object[])it.next();
	        	 if(row[0]!=null){
	        		 totalWeightTemp = Double.parseDouble(row[0].toString());
	        	 }
	        	 if(row[1]!=null){
	        		 totalVolumeTemp = Double.parseDouble(row[1].toString());
	        	 }
	        	 if(row[2]!=null){
	        		 tecketCountemp = Integer.parseInt(row[2].toString());
	        	 }
	         }
			
			int threshold  =  (int) (Math.round(((double)dailyLimit) * dailyOpHubLimitPC/100));
			double thresholdWeight = dailyWeight * dailyOpHubLimitPC/100;
			double thresholdVolume = dailyVolume * dailyOpHubLimitPC/100;			
			
			
			if((tecketCountemp < threshold) && (totalWeightTemp.doubleValue() < thresholdWeight) && (totalVolumeTemp.doubleValue() < thresholdVolume)){
				status = "Open";
			}else if((tecketCountemp >= dailyLimit) || (totalWeightTemp.doubleValue() >= dailyWeight) || (totalVolumeTemp.doubleValue() >= dailyVolume)){
				status = "Closed";
			}else if((tecketCountemp >= threshold && tecketCountemp < dailyLimit) || (totalWeightTemp.doubleValue() >= thresholdWeight && totalWeightTemp.doubleValue() < dailyWeight) || (totalVolumeTemp.doubleValue() >= thresholdVolume && totalVolumeTemp.doubleValue() < dailyVolume)){
				status = "Call Operations";
			}
			
			OpertaionsDTO opertaionsDTO = new OpertaionsDTO();
			try {
				Date processDateFmt = dateformatYYYYMMDD1.parse(processDate);
				opertaionsDTO.setDate(processDateFmt);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			opertaionsDTO.setDay(day);
			opertaionsDTO.setStatus(status);
			opertaionsDTO.setDate(processDate);
			opertaionsDTO.setHoliday(tempOff);
			opertaionsDTO.setComment(comment);
			operationsList.add(opertaionsDTO);
			
			}else{				
					BigDecimal  crewThHoldLimit = new BigDecimal(0.00);
					BigDecimal  divideVal = new BigDecimal(100);
					BigDecimal totalCrewUsed = new BigDecimal(0.00);
					BigDecimal totalAvailableCrew = new BigDecimal(0.00);
					crewThHoldLimit = crewDailyLimit;
					BigDecimal  absentCrewNo=new BigDecimal(0.00);
					BigDecimal  requiredCrewNo=new BigDecimal(0.00);
					int tecketCountemp = 0;
					String tempAbsentCrew="";
					String tempRequiredCrew="";
					String query="";
					String query1="";
					query1="select sum(requiredcrew) from workticket t, refmaster r where t.targetActual not in ('C','R','P') and t.service in ('PK', 'LD', 'DU', 'DL','UP','PL') and t.corpID = '"+corpID+"' and  r.corpID = t.corpID and t.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+hubID+"' and '"+ processDate +"' between date1 and  date2 and r.parameter = 'HOUSE' ";
					List requiredCrewAndTicket = this.getSession().createSQLQuery(query1).list();				
					if(requiredCrewAndTicket!=null && !requiredCrewAndTicket.isEmpty() && requiredCrewAndTicket.get(0)!=null){
						requiredCrewNo = new BigDecimal(requiredCrewAndTicket.get(0).toString());
					}else{				
						requiredCrewNo = new BigDecimal("0.00");
					}
					totalCrewUsed = requiredCrewNo;
					totalAvailableCrew = crewThHoldLimit.subtract(totalCrewUsed);
					int res;
					int res1;
					res = totalCrewUsed.compareTo(totalAvailableCrew);
					res1 = totalCrewUsed.compareTo(crewDailyLimit);
					if(res == -1 ){
						status = "Open";
					}else if(res1 == 1 || res1 == 0){
						status = "Closed";
					}else if((res == 1 || res == 0) && (res1 == -1)){
						status = "Call Operations";
					}
					
					OpertaionsDTO opertaionsDTO = new OpertaionsDTO();
					try {
						Date processDateFmt = dateformatYYYYMMDD1.parse(processDate);
						opertaionsDTO.setDate(processDateFmt);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					opertaionsDTO.setDay(day);
					opertaionsDTO.setStatus(status);
					opertaionsDTO.setDate(processDate);
					opertaionsDTO.setHoliday(tempOff);
					opertaionsDTO.setComment(comment);
					opertaionsDTO.setAvailableCrew(totalAvailableCrew);
					opertaionsDTO.setCrewUsed(totalCrewUsed);
					opertaionsDTO.setCrewHubLimit(crewDailyLimit);
					operationsList.add(opertaionsDTO);
				
				}
			}			
			
		return operationsList;
	}
	
	public class OpertaionsDTO{
		private Object date;
		private Object day;
		private Object status;
		private Object holiday;
		private Object comment;
		private Object availableCrew;
		private Object crewUsed;
		private Object crewHubLimit;
		
		public Object getDate() {
			return date;
		}
		public void setDate(Object date) {
			this.date = date;
		}
		public Object getDay() {
			return day;
		}
		public void setDay(Object day) {
			this.day = day;
		}
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getHoliday() {
			return holiday;
		}
		public void setHoliday(Object holiday) {
			this.holiday = holiday;
		}
		public Object getComment() {
			return comment;
		}
		public void setComment(Object comment) {
			this.comment = comment;
		}
		public Object getAvailableCrew() {
			return availableCrew;
		}
		public void setAvailableCrew(Object availableCrew) {
			this.availableCrew = availableCrew;
		}
		public Object getCrewUsed() {
			return crewUsed;
		}
		public void setCrewUsed(Object crewUsed) {
			this.crewUsed = crewUsed;
		}		
		public Object getCrewHubLimit() {
			return crewHubLimit;
		}
		public void setCrewHubLimit(Object crewHubLimit) {
			this.crewHubLimit = crewHubLimit;
		}
		
	}
	public List actualizedTicketWareHouse(String wareHouse, String shipNumber, String sessionCorpID){
		List actualizedTicketWareHouse = this.getSession().createSQLQuery("select w.warehouse FROM workticket w, billing b, serviceorder so, refmaster r  where so.id=b.id and so.id=w.serviceorderid and so.corpID = '"+sessionCorpID+"' and r.corpID in ('TSFT', '"+sessionCorpID+"') and r.parameter='house' and r.code=w.warehouse and r.bucket='Y' and w.targetActual='A' and (b.warehouse is null or b.warehouse='') and w.shipNumber ='"+shipNumber+"' order by w.id").list();
		return actualizedTicketWareHouse;
		}
	public void billingWareHouse(String wareHouse, String shipNumber, String sessionCorpID){
	   getHibernateTemplate().bulkUpdate("update Billing set warehouse='"+wareHouse+"' where shipNumber='"+shipNumber+"'  and corpID='"+sessionCorpID+"'");
	}
	public List findWorkTicketID(String ticket,String corpId){
		return getHibernateTemplate().find("select id from WorkTicket where ticket='"+ticket+"' and corpID='"+corpId+"' ");
	}

	public class WorkTicketStorageDTO{
		 private Object shipNumber;
		 private Object ticket; 
		 private Object description;
		 private Object locationId;
		 private Object pieces;
		 private Object releaseDate;
		 private Object containerId;
		 private Object itemNumber;
		 
		 private Object id; 
		 private Object service;
		 private Object warehouse;
		 private Object date1;
		 private Object date2;
		 private Object day;
		 private Object ticketTransferStatus;
		 private Object workTicketID;
		 private Object targetActual;
		
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getTicket() {
			return ticket;
		}
		public void setTicket(Object ticket) {
			this.ticket = ticket;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getLocationId() {
			return locationId;
		}
		public void setLocationId(Object locationId) {
			this.locationId = locationId;
		}
		public Object getPieces() {
			return pieces;
		}
		public void setPieces(Object pieces) {
			this.pieces = pieces;
		}
		public Object getReleaseDate() {
			return releaseDate;
		}
		public void setReleaseDate(Object releaseDate) {
			this.releaseDate = releaseDate;
		}
		public Object getContainerId() {
			return containerId;
		}
		public void setContainerId(Object containerId) {
			this.containerId = containerId;
		}
		public Object getItemNumber() {
			return itemNumber;
		}
		public void setItemNumber(Object itemNumber) {
			this.itemNumber = itemNumber;
		}
		public Object getService() {
			return service;
		}
		public void setService(Object service) {
			this.service = service;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(Object warehouse) {
			this.warehouse = warehouse;
		}
		public Object getDate1() {
			return date1;
		}
		public void setDate1(Object date1) {
			this.date1 = date1;
		}
		public Object getDate2() {
			return date2;
		}
		public void setDate2(Object date2) {
			this.date2 = date2;
		}
		public Object getDay() {
			return day;
		}
		public void setDay(Object day) {
			this.day = day;
		}
		public Object getTicketTransferStatus() {
			return ticketTransferStatus;
		}
		public void setTicketTransferStatus(Object ticketTransferStatus) {
			this.ticketTransferStatus = ticketTransferStatus;
		}
		public Object getWorkTicketID() {
			return workTicketID;
		}
		public void setWorkTicketID(Object workTicketID) {
			this.workTicketID = workTicketID;
		}
		public Object getTargetActual() {
			return targetActual;
		}
		public void setTargetActual(Object targetActual) {
			this.targetActual = targetActual;
		}
		
	}
	
	
	public List findWorkTicketStorageList(Date date1, Date date2,String service, String warehouse, String sessionCorpID) {
		if (!(date1 == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
			fromDate = nowYYYYMMDD1.toString();
		}

		if (!(date2 == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
			toDate = nowYYYYMMDD2.toString();
		}
		String query="";
		if((warehouse!=null)&& (!(warehouse.trim().equals("")))){
			query="select st.shipNumber,st.ticket,st.description,st.locationId,st.pieces,st.releaseDate,st.containerId,st.itemNumber from workticket wt ,storage st where wt.warehouse like '"+warehouse+"' and (wt.date1 between '"+fromDate+"' and '"+toDate+"' or wt.date2 between '"+fromDate+"' and  '"+toDate+"' or wt.date1 < '"+fromDate+"' and wt.date2 > '"+toDate+"')  and service in ('AC','RL','DL','DU') and st.releaseDate is null and wt.serviceOrderId=st.serviceOrderId and wt.corpId='"+sessionCorpID+"' and st.corpId='"+sessionCorpID+"'";
		}else{
			query="select st.shipNumber,st.ticket,st.description,st.locationId,st.pieces,st.releaseDate,st.containerId,st.itemNumber from workticket wt ,storage st where (wt.date1 between '"+fromDate+"' and '"+toDate+"' or wt.date2 between '"+fromDate+"' and  '"+toDate+"' or wt.date1 < '"+fromDate+"' and wt.date2 > '"+toDate+"')  and service in ('AC','RL','DL','DU') and st.releaseDate is null and wt.serviceOrderId=st.serviceOrderId and wt.corpId='"+sessionCorpID+"' and st.corpId='"+sessionCorpID+"'";
		} 
		List list= getSession().createSQLQuery(query).list();
		List result= new ArrayList();
		Iterator it=list.iterator();
		while(it.hasNext())
		{		 
			Object []obj=(Object[]) it.next();
			WorkTicketStorageDTO WtsDTO = new WorkTicketStorageDTO();
			WtsDTO.setShipNumber(obj[0]);
			WtsDTO.setTicket(obj[1]);
			WtsDTO.setDescription(obj[2]);
			WtsDTO.setLocationId(obj[3]);
			WtsDTO.setPieces(obj[4]);
			WtsDTO.setReleaseDate(obj[5]);
			WtsDTO.setContainerId(obj[6]);
			WtsDTO.setItemNumber(obj[7]);
			result.add(WtsDTO);
		}
		
		return result;
	}

	public String findAccountingHold(String billToCode) {
		String AccountingHold="0" ;
		List AccountingHoldList=new ArrayList();
		AccountingHoldList=getHibernateTemplate().find("select count(accountingHold) from PartnerPrivate where partnerCode='"+billToCode+"' and accountingHold is true");
		AccountingHold=AccountingHoldList.get(0).toString();
		return AccountingHold;
	}
	
	public List findStorageUnit(Long ticket, String shipNumber){
		return getHibernateTemplate().find("from Storage where shipNumber='" + shipNumber + "' and (hoTicket is null or hoTicket='' or hoTicket='"+ticket+"') ");
	}
	
	public void updateStoForHandout(String handout, String ids, Long hoTicket){
		String query="";
		if(handout.equalsIgnoreCase("A")){
			query="update Storage set handoutStatus='"+handout+"', hoTicket='"+hoTicket+"' where id in ("+ids+")";
		}else{
		query="update Storage set handoutStatus="+handout+", hoTicket=null where id in ("+ids+")";
		}
		getHibernateTemplate().bulkUpdate(query);
	}
	
	public List findWorkTicketService(Long ticket, String sessionCorpID){
		String query="select concat(id,'~',service,'~',warehouse) from workticket where ticket='"+ticket+"' and targetActual!='C' and corpID='"+sessionCorpID+"' and service in('RC','HO') ";
		List list= getSession().createSQLQuery(query).list();
		return list;
	}
	public void updateTicketStatus(Long ticket, String sessionCorpID){
		String query="update WorkTicket set targetActual='A', statusDate=now() where ticket='"+ticket+"' and corpID='"+sessionCorpID+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	
	public void updateTicketStatusPending(String tickets, String sessionCorpID,String targetActual){
		if ("P".equals(targetActual)) {
			String query="update WorkTicket set targetActual='"+targetActual+"' where ticket in ("+tickets+") and corpID='"+sessionCorpID+"' ";
			getHibernateTemplate().bulkUpdate(query);
		} else {
			String query="update WorkTicket set targetActual='"+targetActual+"' where ticket in ("+tickets+") and corpID='"+sessionCorpID+"' and targetActual='P'";
			getHibernateTemplate().bulkUpdate(query);
		}
	}

	public String getWorkTicketIdByStorageTicket(Long ticket, String sessionCorpID) {
		String query="select id from workticket where ticket='"+ticket+"' and corpID='"+sessionCorpID+"' ";
		return getSession().createSQLQuery(query).list().get(0).toString();
	}
	
	public void changeHandoutAccess(Long ticket, String sessionCorpID){
		
		String query="update Storage set handoutStatus='', hoTicket=null where hoTicket='"+ticket+"' and corpID='"+sessionCorpID+"' ";
		getHibernateTemplate().bulkUpdate(query);
		}

	public List totalHandOutVolumePieces(Long ticket, String ids, String sessionCorpID) {
		String query="select concat(if(pieces is null or pieces ='' ,0,pieces),'~',if(volume is null or volume ='',0,volume)) from storage where corpID='"+sessionCorpID+"' and id in ("+ids+") and releaseDate is null ";
		List list= getSession().createSQLQuery(query).list();
		return list;
	}
	public List handoutWeightandVolume(Long ticket, String shipNumber,String sessionCorpID) {
		String query="select sum(pieces),sum(volume) from storage where corpID='"+sessionCorpID+"' and handoutStatus='A' and shipNumber='"+shipNumber+"' and releaseDate is null and (hoTicket is null or hoTicket='' or hoTicket='"+ticket+"') ";
		List list= getSession().createSQLQuery(query).list();
		return list;
	}
	public String getAddress(Long id, String sessionCorpID,String addType){
		List list= new ArrayList();
		String query="";
		if(addType.equalsIgnoreCase("Origin"))
		{
		query="SELECT concat(if(originAddress1 is not null,originAddress1,''),'~',if(originAddress2 is not null,originAddress2,''),'~',if(originAddress3 is not null,originAddress3,''),'~',if(originCompany is not null,originCompany,''),'~',if(originCountry is not null,originCountry,''),'~',if(originState is not null,originState,''),'~',if(originCity is not null,originCity,''),'~',if(originZip is not null,originZip,''),'~',if(originDayPhone is not null,originDayPhone,''),'~',if(originDayPhoneExt is not null,originDayPhoneExt,''),'~',if(originHomePhone is not null,originHomePhone,''),'~',if(originMobile is not null,originMobile,''),'~',if(originFax is not null,originFax,'')) as address from customerfile where corpID='"+sessionCorpID+"' and id='"+id+"'";
		list= getSession().createSQLQuery(query).list();		      
		}else{
		query="SELECT concat(if(destinationAddress1 is not null,destinationAddress1,''),'~',if(destinationAddress2 is not null,destinationAddress2,''),'~',if(destinationAddress3 is not null,destinationAddress3,''),'~',if(destinationCompany is not null,destinationCompany,''),'~',if(destinationCountry is not null,destinationCountry,''),'~',if(destinationState is not null,destinationState,''),'~',if(destinationCity is not null,destinationCity,''),'~',if(destinationZip is not null,destinationZip,''),'~',if(destinationDayPhone is not null,destinationDayPhone,''),'~',if(destinationDayPhoneExt is not null,destinationDayPhoneExt,''),'~',if(destinationHomePhone is not null,destinationHomePhone,''),'~',if(destinationMobile is not null,destinationMobile,''),'~',if(destinationFax is not null,destinationFax,''))as address from customerfile where corpID='"+sessionCorpID+"' and id='"+id+"'";
		list= getSession().createSQLQuery(query).list();
		}
		String address="";
		if((!list.isEmpty())&&(list!=null)&&(list.get(0)!=null))
		{
			address=list.get(0).toString();
		}
		return address;
	}
	public String getAddressDescription(Long cid, String sessionCorpID,String descType){
		Long addressId= new Long(Long.parseLong(descType));
		List list= new ArrayList();
		String query="";
		query="SELECT concat(if(a.address1 is not null,a.address1,''),'~',if(a.address2 is not null,a.address2,''),'~',if(a.address3 is not null,a.address3,''),'~',if(b.description is not null,b.description,''),'~',if(a.state is not null,a.state,''),'~',if(a.city is not null,a.city,''),'~',if(a.zipCode is not null,a.zipCode,''),'~',if(a.phone is not null,a.phone,'')) as address from address a, refmaster b where a.country=b.code and b.parameter='COUNTRY' and a.corpID='"+sessionCorpID+"' and a.id='"+descType+"' and customerFileId='"+cid+"'";
		list= getSession().createSQLQuery(query).list();		       
		String address="";
		if((!list.isEmpty())&&(list!=null)&&(list.get(0)!=null))
		{
			address=list.get(0).toString();
		}
		return address;
	}	
	public String getStandardAddressDescription(Long StdID,String sessionCorpID){
		List list= new ArrayList();
		String query="";
		query="SELECT concat(if(addressLine1 is not null,addressLine1,''),'~',if(addressLine2 is not null,addressLine2,''),'~',if(addressLine3 is not null,addressLine3,''),'~',if(countryCode is not null,countryCode,''),'~',if(state is not null,state,''),'~',if(city is not null,city,''),'~',if(zip is not null,zip,''),'~',if(dayPhone is not null,dayPhone,''),'~',if(homePhone is not null,homePhone,''),'~',if(mobPhone is not null,mobPhone,''),'~',if(faxPhone is not null,faxPhone,'')) as address from standardaddresses where corpID='"+sessionCorpID+"' and id='"+StdID+"'";
		list= getSession().createSQLQuery(query).list();		       
		String address="";
		if((!list.isEmpty())&&(list!=null)&&(list.get(0)!=null))
		{
			address=list.get(0).toString();
		}
		return address;
	}
	public List getCrewNameByContractor(Long ticket, String sessionCorpID){
		return getHibernateTemplate().find("from TimeSheet where ticket='"+ticket+"' and corpID='"+sessionCorpID+"' ");
	}

	public List getWorkTicketToTransfer(String shipNumber, String sessionCorpID){
		String query = "select distinct w.id,w.ticket,w.service,w.warehouse,w.date1,w.date2,if((w.date1=i.beginDate and w.date2=i.endDate),concat('Day ',i.day),'No Match') day,i.beginDate,i.endDate,i.ticketTransferStatus,i.workTicketID,w.targetActual from workticket w,itemsjbkequip i where i.shipNum=w.shipNumber and "+
					   "w.shipNumber='"+shipNumber+"' and w.corpId='"+sessionCorpID+"' and i.ticket is null order by i.ticketTransferStatus desc";
		List list= getSession().createSQLQuery(query)
		.addScalar("id", Hibernate.LONG)
		.addScalar("ticket", Hibernate.LONG)
		.addScalar("service", Hibernate.STRING)
		.addScalar("warehouse", Hibernate.STRING)
		.addScalar("date1", Hibernate.DATE)
		.addScalar("date2", Hibernate.DATE)
		.addScalar("day", Hibernate.STRING)
		.addScalar("beginDate", Hibernate.DATE)
		.addScalar("endDate", Hibernate.DATE)
		.addScalar("ticketTransferStatus", Hibernate.BOOLEAN)
		.addScalar("workTicketID", Hibernate.LONG)
		.addScalar("targetActual", Hibernate.STRING)
		.list();
		
		Map<Long,List> map = new HashMap<Long, List>();
		Iterator it1=list.iterator();
		while(it1.hasNext()){
			Object []obj=(Object[]) it1.next();
			Long id = Long.valueOf(obj[0].toString());
			List li = new ArrayList();
			for (int i = 0; i < obj.length; i++) {
				li.add(obj[i]);
			}
			if(map.containsKey(id)){
				Boolean blnObj = new Boolean(obj[9].toString().trim());
				 boolean transferStatus = blnObj.booleanValue();
				if(obj[6] !=null && obj[6].toString().startsWith("Day")){
						map.put(id, li);
				}
				if(obj[6] !=null && obj[6].toString().startsWith("Day") && transferStatus ==false){
					map.put(id, li);
				}
			}else{
				map.put(id, li);
			}
		}
		list.clear();
		Iterator entries = map.entrySet().iterator();
		 while (entries.hasNext()) {
		     Map.Entry entry = (Map.Entry) entries.next();
		     Long key = (Long)entry.getKey();
		     List value = (List)entry.getValue();
		     list.add(value);
		 }
		List result= new ArrayList();
		Iterator it=list.iterator();
		while(it.hasNext()){
			List obj=(List)it.next();
			WorkTicketStorageDTO WtsDTO = new WorkTicketStorageDTO();
			WtsDTO.setId(obj.get(0));
			WtsDTO.setTicket(obj.get(1));
			List serviceList=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+obj.get(2)+"' and parameter='TCKTSERVC' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') ").addScalar("r.description", Hibernate.STRING).list();
			WtsDTO.setService(serviceList.get(0).toString());
			List whseList=this.getSession().createSQLQuery("select r.description from refmaster r where r.code='"+obj.get(3)+"' and parameter='HOUSE' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') ").addScalar("r.description", Hibernate.STRING).list();
			WtsDTO.setWarehouse(whseList.get(0).toString());
			WtsDTO.setDate1(obj.get(4));
			WtsDTO.setDate2(obj.get(5));
			
			WtsDTO.setDay(obj.get(6));
			WtsDTO.setTicketTransferStatus(obj.get(9));
			WtsDTO.setWorkTicketID(obj.get(10));
			WtsDTO.setTargetActual(obj.get(11));
			result.add(WtsDTO);
		}
		return result;
	}

	public List ticketTransfer(String shipNumber, String sessionCorpID,Long ticket, Long id) {
		String SOquery = "update itemsjbkequip "+
		"set ticket="+ticket+ 
		" where shipNum='"+shipNumber+"' and corpId='"+sessionCorpID+"'";
		getSession().createSQLQuery(SOquery).executeUpdate();
		return null;
	}
	public List findWorkTicketResourceList(Long id){
		List list = this.getSession().createSQLQuery("select id from itemsjbkequip where workticketId='"+id+"'").list();
		return list;
	}
//	<-- created this method as per Bug 6535 -->
	public List findServiceDesc(String code,String sessionCorpID){
		List list = this.getSession().createSQLQuery("select description from  refmaster where parameter='TCKTSERVC' and code='"+code+"' and language='en' and corpId='"+sessionCorpID+"'").list();
		return list;
	}
	public String findWorkTicketReview(String shipnum,String sessionCorpID){
		String query = "select reviewStatus from workticket where shipNumber='"+shipnum+"' and reviewStatus='UnBilled' and targetActual not in ('C','F')";
		List list = this.getSession().createSQLQuery(query).list();
		String review="";
		if (list != null && list.size() > 0 && list.get(0) != null && !"".equals(list.get(0).toString().trim())) {
			review=list.get(0).toString().trim();
		}
		return review;
	}
	public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID){
		String query="select city,zip,state,originCountry from workticket where id='"+shipNumberForCity+"'and city='"+ocityForCity+"' and corpId='"+sessionCorpID+"'";
		 List list= this.getSession().createSQLQuery(query).list();
		 Iterator it= list.iterator();
		 List list1=new ArrayList();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           OriginCityDTO availableCrew = new OriginCityDTO();
	           
	           if(row[0] == null){availableCrew.setOriginCity("");}else{availableCrew.setOriginCity(row[0].toString());}
	           if(row[3] == null){availableCrew.setOriginCountry("");}else{availableCrew.setOriginCountry(row[3].toString());}
	           if(row[2] == null){availableCrew.setOriginState("");}else{availableCrew.setOriginState(row[2].toString());}
	           if(row[1] == null){availableCrew.setOriginZip("");}else{availableCrew.setOriginZip(row[1].toString());}      
	           
	           list1.add(availableCrew);
	}
		 return list1;
	}
	public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID){
		String query="select destinationCity,destinationZip,destinationState,destinationCountry from workticket where id='"+shipNumberForCity+"'and destinationCity='"+dcityForCity+"' and corpId='"+sessionCorpID+"'";
		 List list= this.getSession().createSQLQuery(query).list();
		 Iterator it= list.iterator();
		 List list1=new ArrayList();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           DestinationCityDTO availableCrew = new DestinationCityDTO();
	           
	           if(row[0] == null){availableCrew.setDestinationCity("");}else{availableCrew.setDestinationCity(row[0].toString());}
	           if(row[3] == null){availableCrew.setDestinationCountry("");}else{availableCrew.setDestinationCountry(row[3].toString());}
	           if(row[2] == null){availableCrew.setDestinationState("");}else{availableCrew.setDestinationState(row[2].toString());}
	           if(row[1] == null){availableCrew.setDestinationZip("");}else{availableCrew.setDestinationZip(row[1].toString());}      
	           
	           list1.add(availableCrew);
	}
		 return list1;
	}
	
public class OriginCityDTO {
		
		private Object originCity;
		private Object originZip;
		private Object originState;
		private Object originCountry;
		public Object getOriginCity() {
			return originCity;
		}
		public Object getOriginZip() {
			return originZip;
		}
		public Object getOriginState() {
			return originState;
		}
		public Object getOriginCountry() {
			return originCountry;
		}
		public void setOriginCity(Object originCity) {
			this.originCity = originCity;
		}
		public void setOriginZip(Object originZip) {
			this.originZip = originZip;
		}
		public void setOriginState(Object originState) {
			this.originState = originState;
		}
		public void setOriginCountry(Object originCountry) {
			this.originCountry = originCountry;
		}
		
		
	}
public class DestinationCityDTO {
	
	private Object destinationCity;
	private Object destinationZip;
	private Object destinationState;
	private Object destinationCountry;
	public Object getDestinationCity() {
		return destinationCity;
	}
	public Object getDestinationZip() {
		return destinationZip;
	}
	public Object getDestinationState() {
		return destinationState;
	}
	public Object getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCity(Object destinationCity) {
		this.destinationCity = destinationCity;
	}
	public void setDestinationZip(Object destinationZip) {
		this.destinationZip = destinationZip;
	}
	public void setDestinationState(Object destinationState) {
		this.destinationState = destinationState;
	}
	public void setDestinationCountry(Object destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	
	
}

	public List getLabel4Value(String ticketWarehouse,String sessionCorpID){
		
		return getHibernateTemplate().find("select label4 from RefMaster where  parameter='HOUSE' and code='"+ticketWarehouse+"' and corpid='"+sessionCorpID+"'");
	}
	public void updateResourceMiscellaneous(String tktService,String userName,String sequenceNumber,String sessionCorpID){
		if(tktService.equalsIgnoreCase("PK")|| tktService.equalsIgnoreCase("PL")){
			getHibernateTemplate().bulkUpdate("update  Miscellaneous set packingDriverId='"+userName+"' where sequenceNumber='"+sequenceNumber+"' and corpid='"+sessionCorpID+"'");
		}else if(tktService.equalsIgnoreCase("LD")){
			getHibernateTemplate().bulkUpdate("update  Miscellaneous set loadingDriverId='"+userName+"' where sequenceNumber='"+sequenceNumber+"' and corpid='"+sessionCorpID+"'");
		}else if(tktService.equalsIgnoreCase("DL")||tktService.equalsIgnoreCase("DU")){
			getHibernateTemplate().bulkUpdate("update  Miscellaneous set deliveryDriverId='"+userName+"' where sequenceNumber='"+sequenceNumber+"' and corpid='"+sessionCorpID+"'");
			
		}
	}
	public String findTargetActualByTicketNum(Long ticket,String sessionCorpID){
		Set set = new HashSet<String>();
		set.add("A");set.add("C");
		
		String query="select targetActual from workticket where ticket="+ticket+" and corpId='"+sessionCorpID+"'";
		List list = getSession().createSQLQuery(query).list();
		if (list != null && list.size() >0 && list.get(0) != null && !"".equals(list.get(0))) {
			if(!set.contains(list.get(0).toString())){
				return "OK";
			}else{
				return "";
			}
		}else{
			return "";
		}
	}
	
	public String statusOfWorkTicket(Long ticket,String sessionCorpID){
		String statusOfWorkTic = "";
		String query="select targetActual from workticket where ticket="+ticket+" and corpId='"+sessionCorpID+"'";
		List list = getSession().createSQLQuery(query).list();
		if (list != null && list.size() >0 && list.get(0) != null && !"".equals(list.get(0))) {
			statusOfWorkTic = list.get(0).toString();
		}
		return statusOfWorkTic;
	}
	public List getAllRecord(Long id, String sessionCorpID){
		return getHibernateTemplate().find("from ItemsJbkEquip where workTicketID='"+id+"' and corpID='"+sessionCorpID+"' ");
	}
	public void updateBillToAuthority( String billToAuthority, Date authUpdated,Long id){
		try {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(authUpdated));
			toDate = nowYYYYMMDD2.toString();
	    }catch (Exception e) {
			e.printStackTrace();
		}	
	    getHibernateTemplate().bulkUpdate("update Billing set billToAuthority=?,authUpdated='"+toDate+"' where  id='"+id+"'",billToAuthority);
	}
	public Long findRemoteWorkTicket(String ticket) {
		Long wid=0L;
			List custList =this.getSession().createSQLQuery("select id from workticket where ticket='"+ticket+"'").list();
			if(!custList.isEmpty()){
				wid=Long.parseLong(custList.get(0).toString());
			}
			return wid;
		}
	
	public List getWorkTicketList(String serviceType,String wareHouse,Date date1, String sessionCorpID,String mode,String military,String job,String billToCode){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	    String date = new String(format.format(date1));
	    if("All".equals(serviceType) || "".equals(serviceType)){
	    	serviceType = "";
	    }else{
	    	serviceType = " and service in ('"+serviceType.replaceAll(" ", "").replaceAll(",", "','")+"')";
	    }
	    if(wareHouse != null && !"All".equals(wareHouse.trim()) && !"".equals(wareHouse.trim())){
	    	wareHouse = " and warehouse = '"+wareHouse+"'";
	    }else{
	    	wareHouse = "";
	    }
	    if(mode != null && !"".equals(mode.trim())){
	    	mode = " and jobMode ='"+mode+"' ";
	    }else{
	    	mode = "";
	    }
	    if(job !=null && !job.equalsIgnoreCase("")){
	    	job = " and jobType in ('"+job.replaceAll(" ", "").replaceAll(",", "','")+"')";
	    }else{
	    	job = "";
	    }
	    if(billToCode !=null && !billToCode.equalsIgnoreCase("")){
	    	billToCode = " and account in ('"+billToCode.replaceAll(" ", "").replaceAll(",", "','")+"')";
	    }else{
	    	billToCode = "";
	    }
	    
		String query = "from WorkTicket where corpId='"+sessionCorpID+"' and targetActual not in ('C') "+serviceType+wareHouse+mode+job+billToCode+
					   " and '"+date+"' between date1 and date2 order by shipNumber,ticket";
		return getHibernateTemplate().find(query);
	}
	public List findCrewFirstLastName(Long ticket,String sessionCorpID){
		List al=null;
		List driverCrewTypeList  = this.getSession().createSQLQuery("select if(driverCrewType is null OR driverCrewType='' OR driverCrewType='()','',driverCrewType) from systemdefault where corpid='"+sessionCorpID+"' limit 1").list();
		String crewTypeList="";
		if((driverCrewTypeList!=null)&&(!driverCrewTypeList.isEmpty())&&(driverCrewTypeList.get(0)!=null)&&(!driverCrewTypeList.get(0).toString().equalsIgnoreCase(""))){
			crewTypeList=driverCrewTypeList.get(0).toString();
		}
		if(!crewTypeList.equalsIgnoreCase("")){
		String query="select distinct concat(c.firstName,' ',c.lastName) from crew c,workticket w,timesheet t where w.corpId='"+sessionCorpID+"' " +
						"and t.corpId='"+sessionCorpID+"' and c.corpId='"+sessionCorpID+"' and w.ticket=t.ticket " +
						"and t.username =c.username  and  w.ticket='"+ticket+"' and t.crewType in "+crewTypeList+" group by c.firstName;";
		al = getSession().createSQLQuery(query).list();
		}
		return al;
	}
	public String findVendorCodeWithName(String code, String sessionCorpID){
		String value="";
		String query=("select concat(flex2 ,'~',flex3) from refmaster where parameter='TCKTSERVC' and code='"+code+"' and corpId='"+sessionCorpID+"'");
		List al =  getSession().createSQLQuery(query).list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null))
		{
			value=al.get(0).toString();
		}
		return value;
	}
	public String findCoordinatorByShipNum(String shipNumber,String sessionCorpID){
		String value="";
		String query="select coordinator from  serviceorder where shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"'";
		List list = this.getSession().createSQLQuery(query).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			value=list.get(0).toString();
		}
		return value;
		
	}
	public List getdriverTicket(Date fromDate, Date toDate,String userParentAgent,String corpId){
		List workPlanList=new ArrayList();
		String dateFrom = "";
		String dateTo = "";
		if (!(fromDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
			dateFrom = nowYYYYMMDD1.toString();
		}

		if (!(toDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(toDate));
			dateTo = nowYYYYMMDD2.toString();
		}
		String tempPartnerCode="";
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+userParentAgent+"' and corpid='"+corpId+"'").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+userParentAgent+"'";
		}else{
			partnerCodes = "'"+userParentAgent+"',"+tempPartnerCode;
		}
		List workList=this.getSession().createSQLQuery("select w.ticket,w.date1,w.date2,w.warehouse,w.estimatedWeight,w.service,w.city,w.destinationCity,w.trucks,concat(w.firstname,',',w.lastname),w.id ,w.scheduledArrive as aTime,w.timeFrom as dTime,w.estimatedCartoons,DATEDIFF(w.date2, w.date1) from timesheet t inner join workticket w  on w.ticket = t.ticket and w.corpid = '"+corpId+"' inner join crew as c on c.username = t.username and c.corpid = '"+corpId+"' and c.partnerCode in ("+partnerCodes+") and c.partnerCode!='' and c.partnerCode is not null where ((w.date1  between '"+dateFrom+"' and  '"+dateTo+"') or(w.date2 between '"+dateFrom+"' and  '"+dateTo+"') or  (w.date1 <  '"+dateFrom+"' and w.date2 > '"+dateTo+"')) group by  w.ticket").list();
		Iterator it = workList.iterator();
		while (it.hasNext()){
			Object []row= (Object [])it.next();
			WorkPlanDTO dto= new WorkPlanDTO();
			if(row[0] == null){dto.setTicket("");}else{dto.setTicket(row[0]);}
			if(row[1] == null){dto.setDate1("");}else{dto.setDate1(row[1]);}
			if(row[2] == null){dto.setDate2("");}else{dto.setDate2(row[2]);}
			if(row[3] == null){dto.setWarehouse("");}else{dto.setWarehouse(row[3]);}
			if(row[4] == null){dto.setEstimatedweight("");}else{dto.setEstimatedweight(row[4]);}
			if(row[5] == null){dto.setService("");}else{dto.setService(row[5]);}
			if(row[6] == null){dto.setOriginCity("");}else{dto.setOriginCity(row[6]);}
			if(row[7] == null){dto.setDestinationCity("");}else{dto.setDestinationCity(row[7]);}
			if(row[8] == null){dto.setTrucks("");}else{dto.setTrucks(row[8]);}
			if(row[9]!=null && !row[9].toString().equals("")){			
			String[] ars=row[9].toString().split(",");
			if(!ars[0].trim().equals("") && !ars[1].trim().equals("")){
				if(row[9] == null){dto.setShipper("");}else{dto.setShipper(row[9]);}
			}else if(ars[0].trim().equals("") && !ars[1].trim().equals("")  ){
				if(row[9] == null){dto.setShipper("");}else{dto.setShipper(ars[1]);}
			}else if(!ars[0].trim().equals("") && ars[1].trim().equals("")){
				if(row[9] == null){dto.setShipper("");}else{dto.setShipper(ars[0]);}
			}
			}else{
				dto.setShipper("");
			}
			if(row[10] == null){dto.setId("");}else{dto.setId(row[10]);}
			if(row[11] == null){dto.setaTime("");}else{dto.setaTime(row[11].toString());}
			if(row[12] == null){dto.setdTime("");}else{dto.setdTime(row[12].toString());}
			if(row[13]==null){dto.setEstimatedCartoons("");}else{dto.setEstimatedCartoons(row[13].toString());}
			String dateUF1 = row[1].toString();
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			Date displayDate = null;
			try {
				displayDate = dateformatYYYYMMDD1.parse(dateUF1);
				dto.setDateDisplay(displayDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if(displayDate.after(toDate) || displayDate.before(fromDate)){
				
			}else{
				
			}
			workPlanList.add(dto);
			if(row[14] !=null && (!(row[14].toString().trim().equals(""))) && (!(row[14].toString().trim().equals("0")))){
			int dateDiff=Integer.parseInt(row[14].toString());
			for(int i=1;i<=dateDiff;i++)
			 {
				dto= new WorkPlanDTO();
				if(row[0] == null){dto.setTicket("");}else{dto.setTicket(row[0]);}
				if(row[1] == null){dto.setDate1("");}else{dto.setDate1(row[1]);}
				if(row[2] == null){dto.setDate2("");}else{dto.setDate2(row[2]);}
				if(row[3] == null){dto.setWarehouse("");}else{dto.setWarehouse(row[3]);}
				if(row[4] == null){dto.setEstimatedweight("");}else{dto.setEstimatedweight(row[4]);}
				if(row[5] == null){dto.setService("");}else{dto.setService(row[5]);}
				if(row[6] == null){dto.setOriginCity("");}else{dto.setOriginCity(row[6]);}
				if(row[7] == null){dto.setDestinationCity("");}else{dto.setDestinationCity(row[7]);}
				if(row[8] == null){dto.setTrucks("");}else{dto.setTrucks(row[8]);}
				if(row[9]!=null && !row[9].toString().equals("")){			
				String[] ars=row[9].toString().split(",");
				if(!ars[0].trim().equals("") && !ars[1].trim().equals("")){
					if(row[9] == null){dto.setShipper("");}else{dto.setShipper(row[9]);}
				}else if(ars[0].trim().equals("") && !ars[1].trim().equals("")  ){
					if(row[9] == null){dto.setShipper("");}else{dto.setShipper(ars[1]);}
				}else if(!ars[0].trim().equals("") && ars[1].trim().equals("")){
					if(row[9] == null){dto.setShipper("");}else{dto.setShipper(ars[0]);}
				}
				}else{
					dto.setShipper("");
				}
				if(row[10] == null){dto.setId("");}else{dto.setId(row[10]);}
				if(row[11] == null){dto.setaTime("");}else{dto.setaTime(row[11].toString());}
				if(row[12] == null){dto.setdTime("");}else{dto.setdTime(row[12].toString());}
				if(row[13]==null){dto.setEstimatedCartoons("");}else{dto.setEstimatedCartoons(row[13].toString());}
				 dateUF1 = row[1].toString();
				 dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				 displayDate = null;
				try {
					displayDate = dateformatYYYYMMDD1.parse(dateUF1);
					Calendar c = Calendar.getInstance();
					c.setTime(displayDate); // Now use today date.
					c.add(Calendar.DATE, i);
					dto.setDateDisplay(c.getTime());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if(displayDate.after(toDate) || displayDate.before(fromDate)){
					
				}else{
					
				}
				workPlanList.add(dto);	
			 }
			}
		}
		return workPlanList;
	}

	public String findWorkTicketIdList(String shipNumber, String sessionCorpID) {
		String value="";
		String query="select max(id) from  workticket where shipNumber='"+shipNumber+"' and corpId='"+sessionCorpID+"'";
		List list = this.getSession().createSQLQuery(query).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			value=list.get(0).toString();
		}
		return value;
		
	}
	public String findDefaultWeightUnit(String sessionCorpID) {
		String value="";
		String query="select weightUnit from  systemdefault where corpId='"+sessionCorpID+"'";
		List list = this.getSession().createSQLQuery(query).list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			value=list.get(0).toString();
		}
		return value;
	}
	public String getCustomsTicketList(Long StdID, String corpId){
		String customsTicket="";
		String query="select m.ticket,m.id from custom m inner join workticket w on  w.ticket=m.ticket and w.bondedGoods='Y'  where m.movement = 'In' and m.corpid='"+corpId+"' and m.serviceOrderId='"+StdID+"'";
		List al = this.getSession().createSQLQuery(query).list();
		String query2="select transactionId from custom where corpid='"+corpId+"' and serviceOrderId='"+StdID+"' and transactionId is not null;";
		List a2 = this.getSession().createSQLQuery(query2).list();
		if ((al != null) && (!al.isEmpty())) {
			Iterator it = al.iterator();
			while(it.hasNext()){
			Object row [] = (Object []) it.next();
			if(a2.contains(row[1])){
				
			}else{
				if(customsTicket.equals("")){
					customsTicket=row[0].toString();
				}else{
					customsTicket=customsTicket+","+row[0].toString()+"";
				}
			}
			
			}
		}
		return customsTicket;
	}
    public String getUserCompanyDivisionFilter() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
    	List companyDivisionList=new ArrayList();
    	String companyDivisionSet="";
    	try {
			
    		if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
	    		List partnerCorpIDList = new ArrayList(); 
				HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
				if(valueMap!=null){
					Iterator mapIterator = valueMap.entrySet().iterator();
					while (mapIterator.hasNext()) {
						Map.Entry entry = (Map.Entry) mapIterator.next();
						String key = (String) entry.getKey(); 
					    partnerCorpIDList= (List)entry.getValue(); 
					}
				}
				Iterator listIterator= partnerCorpIDList.iterator();
				while (listIterator.hasNext()) {
					companyDivisionList.add("'" + listIterator.next().toString() + "'");	
				}
				companyDivisionSet= companyDivisionList.toString().replace("[","").replace("]", "");
	    	}
				
    	}catch(Exception e){}
    	return companyDivisionSet;
    }			

	public List planningCalendarList(String sessionCorpID,String wareHouses){
		String unit=findDefaultWeightUnit(sessionCorpID);
		String userFilter=getUserCompanyDivisionFilter();
		Date startDate=null;
		Date endDate=null;
		Calendar calc = Calendar.getInstance();
		calc.setTime(new Date());
		calc.add(Calendar.DATE, -60);
		startDate=calc.getTime();
		calc.setTime(new Date());
		calc.add(Calendar.DATE, 360);
		endDate=calc.getTime();
		
		String startDate1="";
		String endDate1="";
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		StringBuilder sb = new StringBuilder(df.format(startDate));
		startDate1 = sb.toString();
		
		sb = new StringBuilder(df.format(endDate));
		endDate1 = sb.toString();
		
		String query="select "+
					"concat('#&nbsp;<font color=#2779aa><strong><a target=_blank href=editWorkTicketUpdate.html?id=',w.id,'>',w.ticket,'</a></strong></font>(<font color=red>',w.targetactual,'</font>)') ticketNumber, "+
					"date_format(w.date1,'%Y-%m-%d') 'start', "+
					"date_format(w.date2,'%Y-%m-%d') 'end', "+
					"if(w.service is null or w.service='','',concat('<font color=#2779aa><strong>',w.service,'</strong></font>')) 'service', "+
					"if(w.warehouse is null or w.warehouse='','',w.warehouse) 'warehouse', "+
					"datediff(w.date2,w.date1)+1 'duration', "+
					"if(w.timeFrom='00:00','',concat('<font color=#2779aa><strong>',w.timeFrom,'</strong></font>')) 'startTime', "+					
					"concat(if(w.actualVolume is null OR w.actualVolume=0,concat(if(w.estimatedCubicFeet is null OR w.estimatedCubicFeet=0,'',concat(w.estimatedCubicFeet,' ',w.unit2))),concat(w.actualVolume,' ',w.unit2))) 'volume', "+
					"if(w.city is null or w.city='','',w.city) 'city', "+
					"if(w.originCountryCode is null or w.originCountryCode='','',w.originCountryCode) 'originCountryCode', "+
					"if(w.destinationCity is null or w.destinationCity='','',w.destinationCity) 'destinationCity', "+
					"if(w.destinationCountryCode is null or w.destinationCountryCode='','',w.destinationCountryCode) 'destinationCountryCode', "+
					"if(w.firstName is null or w.firstName='','',w.firstName) 'firstName', "+
					"if(w.lastName is null or w.lastName='','',w.lastName) 'lastName', "+
					"if(s.mode is null or s.mode='','',s.mode) 'jobMode', "+
					"if(s.status is null or s.status='','',s.status) 'status', "+
					"concat(if(w.actualWeight is null OR w.actualWeight=0,concat(if(w.estimatedWeight is null OR w.estimatedWeight=0,'',concat(w.estimatedWeight,' ',w.unit1))),concat(w.actualWeight,' ',w.unit1))) 'volume1',w.shipNumber as shipNum "+
					"from workticket w,serviceorder s "+
					"where s.shipNumber=w.shipNumber and s.status not in ('CNCL','CLSD') AND w.targetActual not in ('C','R') AND s.corpId='"+sessionCorpID+"' AND w.corpid = '"+sessionCorpID+"' "+((wareHouses!=null && !wareHouses.equalsIgnoreCase(""))?" and w.warehouse in ("+wareHouses+")":"")+((userFilter!=null && !userFilter.equalsIgnoreCase(""))?" and w.companyDivision in ("+userFilter+")":"")+((userFilter!=null && !userFilter.equalsIgnoreCase(""))?" and s.companyDivision in ("+userFilter+")":"")+
					" and ( (w.date1 between '"+startDate1+"' and  '"+endDate1+"') or (w.date2 between '"+startDate1+"' and  '"+endDate1+"') )  "+
					"order by 2,3,4";
		
					List DailyList=this.getSession().createSQLQuery(query).list();
					PlanningCalendarDTO dto = null;
					List dailySheetList=new ArrayList();
		Iterator it = DailyList.iterator();
		String str="";
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new PlanningCalendarDTO();
			dto.setTicketNumber(row[0]);
			dto.setStart(row[1]);
			dto.setEnd(row[2]);
			dto.setService(row[3]);
			dto.setWarehouse(row[4]);
			dto.setDuration(row[5]);
			dto.setStartTime(row[6]);
			dto.setVolume(row[7]);
			dto.setCity(row[8]);
			dto.setOriginCountryCode(row[9]);
			dto.setDestinationCity(row[10]);
			dto.setDestinationCountryCode(row[11]);
			dto.setFirstName(row[12]);
			dto.setLastName(row[13]);
			dto.setMode(row[14]);
			dto.setStatus(row[15]);
			dto.setVolume1(row[16]);
			if(dto.getFirstName()!=null && !dto.getFirstName().toString().trim().equalsIgnoreCase("")){
				dto.setFirstName(dto.getFirstName().toString().trim().replaceAll("\"", ""));
			}
			if(dto.getLastName()!=null && !dto.getLastName().toString().trim().equalsIgnoreCase("")){
				dto.setLastName(dto.getLastName().toString().trim().replaceAll("\"", ""));
			}			
			str=(dto.getStartTime()!=null && !dto.getStartTime().toString().equalsIgnoreCase("")?dto.getStartTime()+" ":"")+""
					+ ""+(dto.getService()!=null && !dto.getService().toString().equalsIgnoreCase("")?dto.getService()+" for ":"")
					+ ""+dto.getFirstName()+" "+dto.getLastName()+(((dto.getFirstName()!=null && !dto.getFirstName().toString().equalsIgnoreCase(""))||(dto.getLastName()!=null && !dto.getLastName().toString().equalsIgnoreCase("")))?", ":"")+""
					+ ""+((unit!=null && unit.equalsIgnoreCase("Lbs"))?(dto.getVolume1()!=null && !dto.getVolume1().toString().equalsIgnoreCase("")?dto.getVolume1()+", ":""):(dto.getVolume()!=null && !dto.getVolume().toString().equalsIgnoreCase("")?dto.getVolume()+", ":""))+""
					+ ""+(dto.getMode()!=null && !dto.getMode().toString().equalsIgnoreCase("")?dto.getMode()+", ":"")+""
					+ ""+(dto.getStatus()!=null && !dto.getStatus().toString().equalsIgnoreCase("")?dto.getStatus()+", ":"")+""
					+ ""+(dto.getCity()!=null && !dto.getCity().toString().equalsIgnoreCase("")?dto.getCity()+" ":"")+""
					+ ""+(dto.getOriginCountryCode()!=null && !dto.getOriginCountryCode().toString().equalsIgnoreCase("")?"("+dto.getOriginCountryCode()+") &rarr; ":"")+""
					+ ""+(dto.getDestinationCity()!=null && !dto.getDestinationCity().toString().equalsIgnoreCase("")?dto.getDestinationCity()+" ":"")+""
				    + ""+(dto.getDestinationCountryCode()!=null && !dto.getDestinationCountryCode().toString().equalsIgnoreCase("")?"("+dto.getDestinationCountryCode()+"), ":"")+""
				    + ""+(dto.getTicketNumber()!=null && !dto.getTicketNumber().toString().equalsIgnoreCase("")?dto.getTicketNumber()+" ":"");
			
			
			dto.setTitle(str);
			dailySheetList.add(dto);
		}
		return dailySheetList;
	}
	public class PlanningCalendarDTO {
		private Object ticketNumber;
		private Object start;
		private Object end;
		private Object service;
		private Object warehouse;
		private Object duration;
		private Object startTime;
		private Object volume;
		private Object city;
		private Object originCountryCode;
		private Object destinationCity;
		private Object destinationCountryCode;
		private Object firstName;
		private Object lastName;
		private Object mode;
		private Object title;
		private Object status;
		private Object volume1;
		private Object crewList;
		private Object truckList;
		private Object coordinatorName;
		private Object salesPerson;
		private Object routing;
		private Object addressList;
		private Object shipNum;
		
		public Object getTicketNumber() {
			return ticketNumber;
		}
		public void setTicketNumber(Object ticketNumber) {
			this.ticketNumber = ticketNumber;
		}
		public Object getStart() {
			return start;
		}
		public void setStart(Object start) {
			this.start = start;
		}
		public Object getEnd() {
			return end;
		}
		public void setEnd(Object end) {
			this.end = end;
		}
		public Object getService() {
			return service;
		}
		public void setService(Object service) {
			this.service = service;
		}
		public Object getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(Object warehouse) {
			this.warehouse = warehouse;
		}
		public Object getDuration() {
			return duration;
		}
		public void setDuration(Object duration) {
			this.duration = duration;
		}
		public Object getStartTime() {
			return startTime;
		}
		public void setStartTime(Object startTime) {
			this.startTime = startTime;
		}
		public Object getVolume() {
			return volume;
		}
		public void setVolume(Object volume) {
			this.volume = volume;
		}
		
		public Object getOriginCountryCode() {
			return originCountryCode;
		}
		public void setOriginCountryCode(Object originCountryCode) {
			this.originCountryCode = originCountryCode;
		}

		public Object getDestinationCountryCode() {
			return destinationCountryCode;
		}
		public void setDestinationCountryCode(Object destinationCountryCode) {
			this.destinationCountryCode = destinationCountryCode;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}

		public Object getMode() {
			return mode;
		}
		public void setMode(Object mode) {
			this.mode = mode;
		}
		public Object getCity() {
			return city;
		}
		public void setCity(Object city) {
			this.city = city;
		}
		public Object getDestinationCity() {
			return destinationCity;
		}
		public void setDestinationCity(Object destinationCity) {
			this.destinationCity = destinationCity;
		}
		public Object getTitle() {
			return title;
		}
		public void setTitle(Object title) {
			this.title = title;
		}
		public Object getStatus() {
			return status;
		}
		public void setStatus(Object status) {
			this.status = status;
		}
		public Object getVolume1() {
			return volume1;
		}
		public void setVolume1(Object volume1) {
			this.volume1 = volume1;
		}
		public Object getCrewList() {
			return crewList;
		}
		public void setCrewList(Object crewList) {
			this.crewList = crewList;
		}
		public Object getTruckList() {
			return truckList;
		}
		public void setTruckList(Object truckList) {
			this.truckList = truckList;
		}
		public Object getCoordinatorName() {
			return coordinatorName;
		}
		public void setCoordinatorName(Object coordinatorName) {
			this.coordinatorName = coordinatorName;
		}
		public Object getRouting() {
			return routing;
		}
		public void setRouting(Object routing) {
			this.routing = routing;
		}
		public Object getAddressList() {
			return addressList;
		}
		public void setAddressList(Object addressList) {
			this.addressList = addressList;
		}
		public Object getSalesPerson() {
			return salesPerson;
		}
		public void setSalesPerson(Object salesPerson) {
			this.salesPerson = salesPerson;
		}
		public Object getShipNum() {
			return shipNum;
		}
		public void setShipNum(Object shipNum) {
			this.shipNum = shipNum;
		}
		

	}
	public List findDailySheetList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		List truckList=new ArrayList();
		List crewNameList=new ArrayList();
		List noOfCrewList=new ArrayList();
		List dailySheetList=new ArrayList();
		String query = "select w.id,w.ticket,w.jobType, w.shipNumber,concat(ifNULL(w.lastName,''),',<BR>',ifNULL(w.firstName,'')) As Customer, " +
						" concat(ifNULL(w.address1,''),',',ifNULL(w.address2,''),'<BR>',ifNULL(w.address3,''),',',ifNULL(w.originCountry,''),'<BR>',ifNULL(w.state,''),',',ifNULL(w.city,''),'<BR>',ifNULL(w.zip,'')) As FromAddress," +
						" concat(ifNULL(w.destinationAddress1,''),',',ifNULL(w.destinationAddress2,''),'<BR>',ifNULL(w.destinationAddress3,''),',' ,ifNULL(w.destinationCountry,''),'<BR>'," +
						" ifNULL(w.destinationState,''),',',ifNULL(w.destinationCity,''),'<BR>',ifNULL(w.destinationZip,'')) As ToAddress,w.accountName,w.actualWeight,w.unit1," +
						" concat(date_format(w.date1,'%Y-%m-%d'),'<BR>',date_format(w.date2,'%Y-%m-%d')) As Time, " +
						" n.id as notesID,s.registrationNumber,w.warehouse,w.companyDivision,w.service " +
						
						" from workticket w " +
						" left outer join notes n on w.id=n.notesKeyId  and n.corpid='"+sessionCorpID+"' " +
						" left outer join serviceorder s on s.id = w.serviceorderid and s.corpid='"+sessionCorpID+"'" +
						" where (w.date1<='"+dailySheetDate1+"' AND w.date2>='"+dailySheetDate1+"') " +
						" and w.targetActual not in ('C','R') " +
						" and w.warehouse='"+wRHouse+"' and w.corpID='"+sessionCorpID+"' " +
						" group by w.ticket";
		List DailyList=this.getSession().createSQLQuery(query).list();
		
		DailySheetListDTO dto = null;
		Iterator it = DailyList.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new DailySheetListDTO();
			dto.setId(row[0]);
			dto.setTicket(row[1]);
			dto.setJobType(row[2]);
			dto.setShipNumber(row[3]);
			dto.setCustomerName(row[4]);
			dto.setFromAddress(row[5]);
			dto.setToAddress(row[6]);
			dto.setAccountName(row[7]);
			dto.setActualWeight(row[8]);
			dto.setUnit(row[9]);			
			dto.setDates(row[10]);
			dto.setNotes(row[11]);
			dto.setRegNumber(row[12]);
			dto.setWareHouse(row[13]);
			dto.setCompDivision(row[14]);
			dto.setService(row[15]);
			String query1= "select distinct localtrucknumber from truckingoperations where corpid='"+sessionCorpID+"' and ticket='"+row[1]+"' and workdate like '"+dailySheetDate1+"%' and isTruckUsed is true";
			truckList = this.getSession().createSQLQuery(query1).list();
			if(truckList!=null && !truckList.isEmpty() && truckList.get(0)!=null){
				dto.setTruckNo(truckList.toString().replace("[", "").replace("]", ""));
        	}else{        		
        		dto.setTruckNo("");
        	}
			String query2= "select concat(c.lastname,' ',c.firstname) from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username " +
					" where t.corpid='"+sessionCorpID+"' and t.ticket='"+row[1]+"' and t.workdate like '"+dailySheetDate1+"%' and t.isCrewUsed is true " +
					" and c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse='"+wRHouse+"' and c.corpid='"+sessionCorpID+"' group by c.username";
			crewNameList = this.getSession().createSQLQuery(query2).list();
			if(crewNameList!=null && !crewNameList.isEmpty() && crewNameList.get(0)!=null){			
				dto.setCrewName(crewNameList.toString().replace("[", "").replace("]", ""));
        	}else{
        		dto.setCrewName("");	
        	}
			String query3="select count(username) from timesheet where corpid='"+sessionCorpID+"'  and ticket='"+row[1]+"' and workdate='"+dailySheetDate1+"'";
			noOfCrewList = this.getSession().createSQLQuery(query3).list();
			if(noOfCrewList!=null && !noOfCrewList.isEmpty() && noOfCrewList.get(0)!=null){			
				dto.setNoOfCrews(noOfCrewList.toString().replace("[", "").replace("]", ""));
        	}else{
        		dto.setNoOfCrews("");	
        	}
			dailySheetList.add(dto);
		}
		return dailySheetList;		
	}
	public class DailySheetListDTO {
		private Object id;
		private Object ticket;
		private Object jobType;
		private Object shipNumber;
		private Object customerName;
		private Object fromAddress;
		private Object toAddress;
		private Object accountName;
		private Object actualWeight;
		private Object crewName;
		private Object unit;
		private Object truckNo;
		private Object dates;
		private Object noOfCrews;
		private Object notes;
		private Object regNumber;
		private Object wareHouse;
		private Object compDivision;
		private Object service;
		
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getTicket() {
			return ticket;
		}
		public void setTicket(Object ticket) {
			this.ticket = ticket;
		}
		public Object getJobType() {
			return jobType;
		}
		public void setJobType(Object jobType) {
			this.jobType = jobType;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getCustomerName() {
			return customerName;
		}
		public void setCustomerName(Object customerName) {
			this.customerName = customerName;
		}
		public Object getFromAddress() {
			return fromAddress;
		}
		public void setFromAddress(Object fromAddress) {
			this.fromAddress = fromAddress;
		}
		public Object getToAddress() {
			return toAddress;
		}
		public void setToAddress(Object toAddress) {
			this.toAddress = toAddress;
		}
		public Object getAccountName() {
			return accountName;
		}
		public void setAccountName(Object accountName) {
			this.accountName = accountName;
		}
		public Object getActualWeight() {
			return actualWeight;
		}
		public void setActualWeight(Object actualWeight) {
			this.actualWeight = actualWeight;
		}
		public Object getCrewName() {
			return crewName;
		}
		public void setCrewName(Object crewName) {
			this.crewName = crewName;
		}
		public Object getUnit() {
			return unit;
		}
		public void setUnit(Object unit) {
			this.unit = unit;
		}
		public Object getTruckNo() {
			return truckNo;
		}
		public void setTruckNo(Object truckNo) {
			this.truckNo = truckNo;
		}
		public Object getDates() {
			return dates;
		}
		public void setDates(Object dates) {
			this.dates = dates;
		}
		public Object getNoOfCrews() {
			return noOfCrews;
		}
		public void setNoOfCrews(Object noOfCrews) {
			this.noOfCrews = noOfCrews;
		}
		public Object getNotes() {
			return notes;
		}
		public void setNotes(Object notes) {
			this.notes = notes;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		public Object getWareHouse() {
			return wareHouse;
		}
		public void setWareHouse(Object wareHouse) {
			this.wareHouse = wareHouse;
		}
		public Object getCompDivision() {
			return compDivision;
		}
		public void setCompDivision(Object compDivision) {
			this.compDivision = compDivision;
		}
		public Object getService() {
			return service;
		}
		public void setService(Object service) {
			this.service = service;
		}
				
	}
	
	public String findCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		String value="";
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		 List crewNoList=new ArrayList();
		 String query = "select count(username) from timesheet where corpid='"+sessionCorpID+"' and ticket!=0 and workdate='"+dailySheetDate1+"' and warehouse='" +wRHouse+"' ";
		 crewNoList=this.getSession().createSQLQuery(query).list();
		 if((crewNoList!=null)&&(!crewNoList.isEmpty())&&(crewNoList.get(0)!=null))
			{
				value=crewNoList.get(0).toString();
			}
			return value;		
	}
	
	public List findAvailableCrews(String wRHouse, Date dailySheetDate,String sessionCorpID){
		List list1 = new ArrayList();
		 List listAvailCrew;
		 String availCrewQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		
		 availCrewQuery = "(select c.id , c.firstName, c.lastName , c.licenceNumber, c.typeofWork,sum(distinct t.regularHours), t.isCrewUsed  from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +wRHouse+"'  and t.workDate= '"+dailySheetDate1+"' and c.corpID='"+sessionCorpID+"' and t.isCrewUsed is not null group By c.username) UNION "+
				 "(select c.id , c.firstName, c.lastName , c.licenceNumber, c.typeofWork,'',''  from crew as c where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +wRHouse+"'  and c.username not in (select username from timesheet where workDate= '"+dailySheetDate1+"' and warehouse = c.warehouse) and c.corpID='"+sessionCorpID+"' group By c.username) ";
		listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
		Iterator it=listAvailCrew.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew = new AvailableCrew();
	           availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[1] == null){availableCrew.setFirstName("");}else{availableCrew.setFirstName(row[1].toString());}
	           if(row[2] == null){availableCrew.setLastName("");}else{availableCrew.setLastName(row[2].toString());}
	           if(row[3] == null){availableCrew.setLicenceNumber("");}else{availableCrew.setLicenceNumber(row[3].toString());}
	           if(row[4] == null){availableCrew.setTypeofWork("");}else{availableCrew.setTypeofWork(row[4].toString());}
	           if(row[5] == null){availableCrew.setAvailableHours("");}else{availableCrew.setAvailableHours(row[5].toString());}
	           if(row[6] == null || row[6].equals("")){availableCrew.setIsCrewUsed(false);}else{availableCrew.setIsCrewUsed(true);}
	           list1.add(availableCrew);
	       }
	    	return list1;
		
	}
	public class AvailableCrew {
		private Long id;
		private String lastName;
		private String firstName;
		private String licenceNumber;
		private String typeofWork;
		private String availableHours;
		private Boolean isCrewUsed; 
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}		
		public String getTypeofWork() {
			return typeofWork;
		}
		public void setTypeofWork(String typeofWork) {
			this.typeofWork = typeofWork;
		}
		public String getAvailableHours() {
			return availableHours;
		}
		public void setAvailableHours(String availableHours) {
			this.availableHours = availableHours;
		}
		public Boolean getIsCrewUsed() {
			return isCrewUsed;
		}
		public void setIsCrewUsed(Boolean isCrewUsed) {
			this.isCrewUsed = isCrewUsed;
		}
		public String getLicenceNumber() {
			return licenceNumber;
		}
		public void setLicenceNumber(String licenceNumber) {
			this.licenceNumber = licenceNumber;
		}
	}
	
	public List searchAvailableTrucks(String wRHouse, Date dailySheetDate,String sessionCorpID) {
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		 	List availableTrucks = new ArrayList();
		 	String query="select distinct t.localnumber,t.description, tr.isTruckUsed, t.id as truckId " +
		 			"from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
		 			"and tr.corpid='"+sessionCorpID+"' and tr.workDate='"+dailySheetDate1+"' where t.corpid='"+sessionCorpID+"' " +
		 			"and t.warehouse='"+wRHouse+"' and t.truckstatus = 'A' ";
		 	List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext()){
				 Object []row= (Object [])it.next();
				 AvailableTrucks availTrucks= new AvailableTrucks();
				 availTrucks.setTruckNumber(row[0].toString());
				 availTrucks.setDescription(row[1].toString());
				 if(row[2] == null || row[2].equals("")){availTrucks.setIsTruckUsed(false);}else{availTrucks.setIsTruckUsed(true);}
				 availTrucks.setId(row[3].toString());
				 availableTrucks.add(availTrucks);
			}
			return availableTrucks;
	}
	public class AvailableTrucks{
		private String id;
		private String description;
		private String truckNumber;
		private Boolean isTruckUsed;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getTruckNumber() {
			return truckNumber;
		}
		public void setTruckNumber(String truckNumber) {
			this.truckNumber = truckNumber;
		}
		public Boolean getIsTruckUsed() {
			return isTruckUsed;
		}
		public void setIsTruckUsed(Boolean isTruckUsed) {
			this.isTruckUsed = isTruckUsed;
		}
	}
	
	public List findOriginAddWorkTicket(Long oId,String sessionCorpID) {
		String query="select concat(ifNULL(address1,''),'`',ifNULL(address2,''),'`',ifNULL(address3,''),'`',ifNULL(originCountry,''),'`',ifNULL(state,'')," +
				"'`',ifNULL(city,''),'`',ifNULL(zip,''),'`',ifNULL(destinationAddress1,''),'`',ifNULL(destinationAddress2,''),'`',ifNULL(destinationAddress3,'')," +
				"'`',ifNULL(destinationCountry,''),'`',ifNULL(destinationState,''),'`',ifNULL(destinationCity,''),'`',ifNULL(destinationZip,''),'`',actualWeight) from  workticket where id='"+oId+"' and corpId='"+sessionCorpID+"'";
		List list = this.getSession().createSQLQuery(query).list();		
		return list;		
	}	
	
	public String findOMDCrewNoList(String wRHouse, Date dailySheetDate,String sessionCorpID){
		String value="";
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		 List crewNoList=new ArrayList();
		 String query = "select sum(crews) from workticket where (date1<='"+dailySheetDate1+"' AND date2>='"+dailySheetDate1+"') and targetActual not in ('C','R') and warehouse='"+wRHouse+"' and companydivision ='OMD' and corpID='"+sessionCorpID+"' ";
		 crewNoList=this.getSession().createSQLQuery(query).list();
		 if((crewNoList!=null)&&(!crewNoList.isEmpty())&&(crewNoList.get(0)!=null))
			{
				value=crewNoList.get(0).toString();
			}
			return value;		
	}
	public void updateDailySheetNote(String wRHouse, Date dailySheetDate,String sessionCorpID,String wDailySheetNotes){
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		String query="update TimeSheet set dailySheetNotes='"+wDailySheetNotes+"' where warehouse ='"+wRHouse+"' and corpId='"+sessionCorpID+"' and workdate = '"+dailySheetDate1+"' ";
		getHibernateTemplate().bulkUpdate(query);
	}
	public String findDailySheetNotes(String wRHouse, Date dailySheetDate,String sessionCorpID){
		String value="";
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		 List dSNotesList=new ArrayList();
		 String query = "select dailySheetNotes from TimeSheet where workdate ='"+dailySheetDate1+"' and warehouse ='"+wRHouse+"' and corpID='"+sessionCorpID+"' ";
		 dSNotesList=getHibernateTemplate().find(query);
		 if((dSNotesList!=null)&&(!dSNotesList.isEmpty())&&(dSNotesList.get(0)!=null))
			{
				value=dSNotesList.get(0).toString();
			}
		 value = value.replaceAll("\r\n", "<br/>");
		return value;
	}
	public List findAvailableCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		List list1 = new ArrayList();
		 List listAvailCrew;
		 String availCrewQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		availCrewQuery	= "select c.username from crew c " +						
						" where c.active='true' and c.ignoreForTimeSheet is false " +
						" AND c.warehouse = '" +wRHouse+"' and c.corpID='"+sessionCorpID+"'" +
						" and c.username not in(select t.username from timesheet t where t.workDate= '"+dailySheetDate1+"' and t.corpid='"+sessionCorpID+"'"+
						" and (t.ticket = '"+wTicket+"' or t.ticket=0))"+
						" group By c.username order by c.username ";
		listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
		Iterator it=listAvailCrew.iterator();
		 while(it.hasNext())
	       {
	           Object row= it.next();
	           list1.add((row==null?"":row.toString()));
	       }
	    return list1;	
	}
	public List findAssignedCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		List list1 = new ArrayList();
		 List listUsedCrew;
		 String assignCrewQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		 assignCrewQuery = "select c.username from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' " +
		 		" and c.ignoreForTimeSheet is false AND c.warehouse = '" +wRHouse+"'  and t.workDate= '"+dailySheetDate1+"' and t.ticket='"+wTicket+"' " +
		 		" and c.corpID='"+sessionCorpID+"' and t.isCrewUsed is not null group By c.username order by c.username ";
				 
		listUsedCrew= this.getSession().createSQLQuery(assignCrewQuery).list();
		Iterator it=listUsedCrew.iterator();
		
		 while(it.hasNext())
	       {
	           Object row= it.next();
	           list1.add(row==null?"":row.toString());
	       }
		 return list1;		
	}
	public List findAbsentCrewsForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID){
		List list1 = new ArrayList();
		 List listAbsentCrew;
		 String absentCrewQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		absentCrewQuery = "select c.userName from crew c, timesheet t where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +wRHouse+"' "
				+ "and c.username=t.username and t.workDate = '"+dailySheetDate1+"' "
						+ "and t.warehouse = c.warehouse and c.corpID='"+sessionCorpID+"' and (t.isCrewUsed is false or t.isCrewUsed is null) group By c.username";
		listAbsentCrew= this.getSession().createSQLQuery(absentCrewQuery).list();
		Iterator it=listAbsentCrew.iterator();
		 while(it.hasNext())
	       {
	           Object row= it.next();
	           list1.add((row==null?"":row.toString()));
	       }
	    return list1;	
	}
	public Long findUserDetailsFromCrew(String asgCrew,String sessionCorpID){
		Long sid=0L;
		String query = "select id from crew where username = '" +asgCrew+"' and corpID='"+sessionCorpID+"'";
		List list1= this.getSession().createSQLQuery(query).list();
		if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
			sid=Long.parseLong(list1.get(0).toString());
		}
		return sid;
	}
	public Long findUserDetailsFromTS(String crewValue,Date dailySheetDate,String sessionCorpID,String dSTicket){
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		Long sid=0L;
		String query = "select id from timesheet where username = '" +crewValue+"' and corpID='"+sessionCorpID+"' and ticket='"+dSTicket+"' and workDate='"+dailySheetDate1+"' ";
		List list1= this.getSession().createSQLQuery(query).list();
		if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
			sid=Long.parseLong(list1.get(0).toString());
		}
		return sid;
	}
	public List findAvailableTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String dSTicket){
		List list1 = new ArrayList();
		 String availTruckQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		availTruckQuery = "select distinct t.localnumber from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
		 				" and tr.corpid='"+sessionCorpID+"' and workDate='"+dailySheetDate1+"' and tr.ticket='"+dSTicket+"' where t.corpid='"+sessionCorpID+"' " +
		 				" and t.warehouse='"+wRHouse+"' and truckstatus = 'A' and tr.isTruckUsed is null order by t.localnumber";
		List listAvailTruck= this.getSession().createSQLQuery(availTruckQuery).list();
		Iterator it=listAvailTruck.iterator();
		 while(it.hasNext())
	       {
	           Object row= it.next();
	           list1.add((row==null?"":row.toString()));
	       }
	    return list1;	
	}
	public List findAssignedTruckForEditing(String wRHouse, Date dailySheetDate,String sessionCorpID,String wTicket){
		List list1 = new ArrayList();
		 String assignTruckQuery;
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		assignTruckQuery = "select distinct t.localnumber from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
							" and tr.corpid='"+sessionCorpID+"' and workDate='"+dailySheetDate1+"' where t.corpid='"+sessionCorpID+"' and t.warehouse='"+wRHouse+"' and tr.ticket='"+wTicket+"' and truckstatus = 'A' and tr.isTruckUsed is true";
				 
		List listUsedTruck= this.getSession().createSQLQuery(assignTruckQuery).list();
		Iterator it=listUsedTruck.iterator();
		
		 while(it.hasNext())
	       {
	           Object row= it.next();
	           list1.add(row==null?"":row.toString());
	       }
		 return list1;		
	}
	public List findTruckDetailsFromTO(String truckNo,Date dailySheetDate,String sessionCorpID,String dSTicket){
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		String query = "select id from TruckingOperations where localTruckNumber = '" +truckNo+"' and corpID='"+sessionCorpID+"' and ticket='"+dSTicket+"' and workDate='"+dailySheetDate1+"' ";
		return getHibernateTemplate().find(query);
	}
	public Long findTruckDetailsFromTruck(String truckNo,String sessionCorpID){
		Long tid=0L;
		String query = "select id from truck where localNumber = '" +truckNo+"' and corpID='"+sessionCorpID+"' ";
		List list1= this.getSession().createSQLQuery(query).list();
		if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
			tid=Long.parseLong(list1.get(0).toString());
		}
		return tid;
	}	
	public String getCellDataForParticulardate(String dailySheetwRHouse,Date dailySheetCalenderDate,String sessionCorpID,String truckNo){
		String str="";
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetCalenderDate));
		String processDate = nowYYYYMMDD2.toString();
		String query2="select group_concat(distinct(concat(w.shipnumber,' * ',w.jobtype,'<BR>',w.ticket,' * ',w.service,' * ',w.actualweight))) "+  
		" from workticket w  left outer join truckingoperations t on  w.ticket=t.ticket "+ 
		" where t.corpid='"+sessionCorpID+"' and t.workDate='"+processDate+"' "+ 
		" and w.warehouse='"+dailySheetwRHouse+"'  and w.corpid='"+sessionCorpID+"' "+
		" and '"+processDate+"' between w.date1 and w.date2  and isTruckUsed is true and t.localTrucknumber='"+truckNo+"'";
		List list1 = this.getSession().createSQLQuery(query2).list();
		if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
			str=list1.get(0).toString();
		}
		return str;
	}	
	public Map<String, List> getDSDispatch(String dailySheetwRHouse,Date dailySheetCalenderDate,String sessionCorpID){
		List truckList = new ArrayList();
		List tempList=null;
		Map<String, List> finalMap= new TreeMap<String, List>();
			String query="select localnumber from truck where corpid='"+sessionCorpID+"' and warehouse='"+dailySheetwRHouse+"' and localnumber is not null and localnumber!=''";
			truckList = this.getSession().createSQLQuery(query).list();
			Iterator it=truckList.iterator();
			while (it.hasNext()) {
				Object type =  it.next();
				tempList = new ArrayList();
				
				for (int i=0; i<10; i++) {					
					Calendar calc = Calendar.getInstance();
					calc.setTime(dailySheetCalenderDate);
					calc.add(Calendar.DATE, i);
					Date totalTempDate = calc.getTime();
					tempList.add(getCellDataForParticulardate(dailySheetwRHouse,totalTempDate,sessionCorpID,type.toString()).replaceAll(",", "<HR>"));
				}				
				finalMap.put(type.toString(),tempList);
			}
		return finalMap;
	}


	public void updateTrackingStatusSitDestinationIn(Date date1, Long id, String sessionCorpID) {
	     if(date1 !=null){
		    Date date1Str = date1;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );
			getHibernateTemplate().bulkUpdate("update TrackingStatus set sitDestinationIn='"+formatedDate1+"' where id='"+id+"'  and corpID='"+sessionCorpID+"'");
	     }		
	}
	public Long findWorkTicketIdByTicket(String dSTicket,String sessionCorpID){
		Long wid=0L;
		String query = "select id from workticket where ticket = '" +dSTicket+"' and corpID='"+sessionCorpID+"' ";
		List list1= this.getSession().createSQLQuery(query).list();
		if((list1!=null)&&(!list1.isEmpty())&&(list1.get(0)!=null)){
			wid=Long.parseLong(list1.get(0).toString());
		}
		return wid;
	}
	public List findScheduleResourceId(String dSTicket,String wRHouse, Date dailySheetDate,String sessionCorpID){
		String dailySheetDate1 = "";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}		
		String query = "select id from ScheduleResourceOperation where ticket = '" +dSTicket+"' and warehouse='" +wRHouse+"' and workdate='"+dailySheetDate1+"' and corpID='"+sessionCorpID+"' ";
		return getHibernateTemplate().find(query);	
	}	
	public String findAbsentCrew(String asgCrew, Date dailySheetDate,String sessionCorpID){
		String dailySheetDate1 = "";
		String str="";
		if (!(dailySheetDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
			dailySheetDate1 = nowYYYYMMDD1.toString();
		}
		String query="select username from timesheet where corpid='"+sessionCorpID+"' and workdate='"+dailySheetDate1+"' and username='"+asgCrew+"' and isCrewUsed is true";
		List list1 = this.getSession().createSQLQuery(query).list();
		if(list1!=null && !list1.isEmpty() && list1.get(0)!=null){
			str=list1.get(0).toString();
		}
		return str;		
	}
	 public AccessInfo getAccessinfo(String sessionCorpID,Long sid){
		 AccessInfo accInfo = null;
		 try {
			List l1 =  getHibernateTemplate().find("from AccessInfo where serviceOrderId='"+sid+"'");
			 if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
				 accInfo = (AccessInfo) l1.get(0);
			 }
		} catch (Exception e) {
			e.printStackTrace();
		}
		 return accInfo;
	 }
	 public String findcrewNoAndCrewThreshHold(Date date1, Date date2,String wareHouse,String sessionCorpID,BigDecimal noOfCrewFromPeople,String oldTicketNumber){
		 String countCheck="";
		 String countCheckInput="";
		 String countCheckCall="";
		 List dailyCrewLimitOpsHub = new ArrayList();
		 List dailyCrewThOpsHub = new ArrayList();
		 List dailyCrewLimitOpsDaily = new ArrayList();
		 String  crewNo="";
		 BigDecimal  crewNoMapValue=new BigDecimal(0.00);
		 BigDecimal  thVal=new BigDecimal(0.00);
		 BigDecimal  availableCrewThreshHold=new BigDecimal(0.00);
		 BigDecimal  availableCrewThreshHoldMapValue=new BigDecimal(0.00);
		 BigDecimal  divideVal=new BigDecimal(100);
		 Map<String,BigDecimal> finalMap= new HashMap<String,BigDecimal>();
		 List addDays = new ArrayList();
		 BigDecimal tempCrewUsedNew = new BigDecimal(0.00);
		 String tempParentName="";
		 	if (!(date1 == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
				toDate = nowYYYYMMDD1.toString();
			}
			if (!(date2 == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
				fromDate = nowYYYYMMDD2.toString();
			}
			List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+fromDate+"', '"+toDate+"')").list();	
			Long diff = Long.parseLong(getDiff.get(0).toString());
			int intValueDiff = diff.intValue();
			for(int i=0;i<=intValueDiff;i++ ){					
				addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+toDate+"', INTERVAL '"+i+"' DAY) AS CHAR)").list();
			
		 List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+wareHouse+"' ").list();
			if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
				 tempParentName = parentName.get(0).toString().trim();
				 dailyCrewLimitOpsDaily = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationsdailylimits where corpID = '"+sessionCorpID+"' and workDate = '"+addDays.get(0)+"' and hubID = '"+tempParentName+"'").list();
				 dailyCrewLimitOpsHub = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationshublimits where corpID = '"+sessionCorpID+"' and hubID = '"+tempParentName+"'").list();
				 dailyCrewThOpsHub = this.getSession().createSQLQuery("Select if(dailyCrewThreshHoldLimit is null or dailyCrewThreshHoldLimit='','0',dailyCrewThreshHoldLimit) from operationshublimits where corpID = '"+sessionCorpID+"' and hubID = '"+tempParentName+"'").list();
			}	
			if(dailyCrewLimitOpsDaily!=null && !dailyCrewLimitOpsDaily.isEmpty() && dailyCrewLimitOpsDaily.get(0)!=null){
				crewNo = dailyCrewLimitOpsDaily.get(0).toString();
			}else{
				if(dailyCrewLimitOpsHub!=null && !dailyCrewLimitOpsHub.isEmpty() && dailyCrewLimitOpsHub.get(0)!=null){
					crewNo = dailyCrewLimitOpsHub.get(0).toString();
				}
			}
			if(dailyCrewThOpsHub!=null && !dailyCrewThOpsHub.isEmpty() && dailyCrewThOpsHub.get(0)!=null){
				thVal = new BigDecimal(dailyCrewThOpsHub.get(0).toString());
			}			
			availableCrewThreshHold = (new BigDecimal(crewNo).multiply(thVal).divide(divideVal,2, RoundingMode.HALF_UP));	
			
			BigDecimal tempCrewUsed = findSumOfrequiredCrew(oldTicketNumber,wareHouse,date1, date2,sessionCorpID);	
			if(tempCrewUsed==null){
				tempCrewUsed = new BigDecimal(0.00);
			}
			tempCrewUsedNew = tempCrewUsed.add(noOfCrewFromPeople);
			finalMap.put(addDays.get(0).toString()+"~"+crewNo, availableCrewThreshHold);			
			}
		for (Map.Entry<String, BigDecimal> entry : finalMap.entrySet()) {
			String dateAndCrewNo = (String)entry.getKey();
			crewNoMapValue = new BigDecimal(dateAndCrewNo.split("~")[1]);			 
			availableCrewThreshHoldMapValue = entry.getValue();					
			int res;
			int res1;
			res = availableCrewThreshHoldMapValue.compareTo(tempCrewUsedNew);
			res1 = crewNoMapValue.compareTo(tempCrewUsedNew);
				if(res1 == -1){
					countCheckInput = "INPUT";
				}else if(res == -1){											
					countCheckCall ="~"+"Call";
				}
				countCheck = countCheckInput.concat(countCheckCall);
			}		
			
		return  countCheck;		 
	 }

	 public BigDecimal findSumOfrequiredCrew(String oldTicketNumber,String wareHouse,Date date1, Date date2,String sessionCorpID){
		 BigDecimal  requiredCrewNo=new BigDecimal(0.00);
		 String tempRequiredCrew="";
		 String tempParentName="";
		 String query="";
		 	if (!(date1 == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
				toDate = nowYYYYMMDD1.toString();
			}
			if (!(date2 == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
				fromDate = nowYYYYMMDD2.toString();
			}
			
			 List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+wareHouse+"' ").list();
				if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
					 tempParentName = parentName.get(0).toString().trim();
					 query ="select sum(w.requiredcrew) from workticket w, refmaster r where w.targetActual not in ('C','R','P') and w.service in ('PK', 'LD', 'DU', 'DL','UP','PL') AND (('"+toDate+"' BETWEEN w.date1 AND w.date2) OR ('"+fromDate+"' BETWEEN w.date1 AND w.date2)) "
					 		+ "and w.ticket!='"+oldTicketNumber+"'  and w.requiredcrew is not null and w.corpID = '"+sessionCorpID+"' and  r.corpID = w.corpID and w.warehouse = r.code and r.bucket = 'Y' and r.flex1 = '"+tempParentName+"'";
				}
			List requiredCrewValue = this.getSession().createSQLQuery(query).list();
			if(requiredCrewValue!=null && !requiredCrewValue.isEmpty() && requiredCrewValue.get(0)!=null){
				tempRequiredCrew=requiredCrewValue.get(0).toString();
				requiredCrewNo = new BigDecimal(tempRequiredCrew);
			}
			return requiredCrewNo;
	 }
	 public BigDecimal findAbsentCrewFromtimeSheet(Date date1, Date date2,String sessionCorpID){
		 BigDecimal  absentCrewNo=new BigDecimal(0.00);
		 String tempAbsentCrew="";
		 	if (!(date1 == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
				toDate = nowYYYYMMDD1.toString();
			}
			if (!(date2 == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
				fromDate = nowYYYYMMDD2.toString();
			}
			String query="";
			query ="select count(*) from timesheet where corpid='"+sessionCorpID+"' and action in('H', 'V', 'P', 'R', 'S', 'BE', 'U') and (workdate BETWEEN '"+toDate+"' AND '"+fromDate+"') and iscrewused is false ";
			List absentCrewValue = this.getSession().createSQLQuery(query).list();
			BigDecimal divideVal = new BigDecimal(3);
			if((absentCrewValue!=null && !absentCrewValue.isEmpty()) && absentCrewValue.get(0)!=null){
				tempAbsentCrew= absentCrewValue.get(0).toString();
				absentCrewNo = new BigDecimal(tempAbsentCrew);
				absentCrewNo = absentCrewNo.divide(divideVal,2, RoundingMode.HALF_UP);
			}
			return absentCrewNo;
	 }
	 public Map<String, String> findCrewCalendarList(String crewCalendarwRHouse,Date crewCalendarDate,String sessionCorpID){
			String crewCalendarDate1 = "";
			if (!(crewCalendarDate == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(crewCalendarDate));
				crewCalendarDate1 = nowYYYYMMDD1.toString();
			}			 
			 List crewList = new ArrayList();			
			 Map<String, String> finalMap= new TreeMap<String, String>();
			 String query="select distinct(username),crewname from timesheet where corpid='"+sessionCorpID+"' and workdate='"+crewCalendarDate1+"' and warehouse='"+crewCalendarwRHouse+"'and ticket!='0'";
			 crewList = this.getSession().createSQLQuery(query).list();
			 crewNameDetail dto = new crewNameDetail();
			 Iterator it=crewList.iterator();
				while (it.hasNext()) {	
				List tempList = new ArrayList();
				Object []row= (Object [])it.next();				
					dto.setUsername(row[0].toString());				
					dto.setCrewname(row[1].toString());
					String crewListByuserName="";
					List l=(findCrewListByuserName(crewCalendarwRHouse,crewCalendarDate1,sessionCorpID,dto.getUsername().toString()));
					if(l!=null && (!(l.isEmpty()) && l.get(0)!=null)){
					Iterator its=l.iterator();
					while(its.hasNext()){
						crewListByuserName = its.next().toString();
					if(crewListByuserName!=null && (!(crewListByuserName.equals("")))){
						 String[] crewListByuserNameValue = crewListByuserName.split(",");
						 String beginhours=crewListByuserNameValue[5]; 
						 String endhours=crewListByuserNameValue[6]; 
						 crewListByuserName="";
						 crewListByuserName=crewListByuserName+crewListByuserNameValue[0]+","+crewListByuserNameValue[1]+","+crewListByuserNameValue[2]+","+crewListByuserNameValue[3]+","+crewListByuserNameValue[4]+",ticketId"+crewListByuserNameValue[7];
						finalMap.put(dto.getCrewname().toString()+"~"+beginhours+"~"+endhours, crewListByuserName);
					}
					}
				}
				}
			return finalMap;
	 }
	 public List findCrewListByuserName(String crewCalendarwRHouse,String crewCalendarDate1,String sessionCorpID,String username){
		String query2="select concat(w.ticket,', ',w.jobtype,', ',w.actualweight,', ',w.jobmode,', ',w.service,', ',t.beginhours,', ',t.endhours,', ',w.id) "+  
					" from workticket w  left outer join timesheet t on  w.ticket=t.ticket "+ 
					" where t.corpid='"+sessionCorpID+"' and t.workDate='"+crewCalendarDate1+"' "+ 
					" and w.warehouse='"+crewCalendarwRHouse+"'  and w.corpid='"+sessionCorpID+"' and w.warehouse=t.warehouse "+
					" and '"+crewCalendarDate1+"' between w.date1 and w.date2  and t.isCrewUsed is true and t.username='"+username+"'";
					List list1 = this.getSession().createSQLQuery(query2).list();
		 return list1;
	 }
	 public class crewNameDetail{
		 private Object username;
		 private Object crewname;
		public Object getUsername() {
			return username;
		}
		public void setUsername(Object username) {
			this.username = username;
		}
		public Object getCrewname() {
			return crewname;
		}
		public void setCrewname(Object crewname) {
			this.crewname = crewname;
		}
	 }
	 
	 public Map<Map,Map> findRequiredCrewValue(Date date1, Date date2,String hub,String sessionCorpID){
		 List dailyCrewLimitOpsHub = new ArrayList();
		 List dailyCrewLimitOpsDaily = new ArrayList();
		 List dailyCrewHubOpsHub = new ArrayList();
		 List dailyCrewHubOpsDaily = new ArrayList();
		 String  wareHouseCrewNo="";	
		 String  hubCrewNo="";
		 Map<Map,Map> finalMap= new HashMap<Map,Map>();
		 Map<String,String> wareHouseCrewMap= new HashMap<String,String>();
		 Map<String,String> hubCrewMap= new HashMap<String,String>();
		 List addDays = new ArrayList();		
		 List tempWRName=new ArrayList();
		 	if (!(date1 == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
				toDate = nowYYYYMMDD1.toString();
			}
			if (!(date2 == null)) {
				SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD2.format(date2));
				fromDate = nowYYYYMMDD2.toString();
			}
			List getDiff = this.getSession().createSQLQuery("SELECT DATEDIFF('"+fromDate+"', '"+toDate+"')").list();	
			Long diff = Long.parseLong(getDiff.get(0).toString());			
			int intValueDiff = diff.intValue();		
			 List wareHouseName  = this.getSession().createSQLQuery("select code from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and flex1 = '"+hub+"' ").list();
			 
			 for(int i=0;i<=intValueDiff;i++ ){
					addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+toDate+"', INTERVAL '"+i+"' DAY) AS CHAR)").list(); 
				Iterator it=wareHouseName.iterator();
			 while (it.hasNext()) {	
				  Object code =  it.next();			  
				dailyCrewLimitOpsDaily = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationsdailylimits where corpID = '"+sessionCorpID+"' and workDate = '"+addDays.get(0)+"' and hubID = '"+code.toString()+"'").list();
				dailyCrewLimitOpsHub = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationshublimits where corpID = '"+sessionCorpID+"' and hubID = '"+code.toString()+"'").list();
			
				if(dailyCrewLimitOpsDaily!=null && !dailyCrewLimitOpsDaily.isEmpty() && dailyCrewLimitOpsDaily.get(0)!=null){
					wareHouseCrewNo = dailyCrewLimitOpsDaily.get(0).toString();
				}else{
					if(dailyCrewLimitOpsHub!=null && !dailyCrewLimitOpsHub.isEmpty() && dailyCrewLimitOpsHub.get(0)!=null){
						wareHouseCrewNo = dailyCrewLimitOpsHub.get(0).toString();
					}
				}
			wareHouseCrewMap.put(addDays.get(0).toString()+"~"+code.toString(),wareHouseCrewNo);
			}
          }
			for(int i=0;i<=intValueDiff;i++ ){						
				addDays = this.getSession().createSQLQuery("SELECT CAST(DATE_ADD('"+toDate+"', INTERVAL '"+i+"' DAY) AS CHAR)").list();
				dailyCrewHubOpsDaily = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationsdailylimits where corpID = '"+sessionCorpID+"' and workDate = '"+addDays.get(0)+"' and hubID = '"+hub+"'").list();
				dailyCrewHubOpsHub = this.getSession().createSQLQuery("Select if(dailyCrewCapacityLimit is null or dailyCrewCapacityLimit='','0',dailyCrewCapacityLimit) from operationshublimits where corpID = '"+sessionCorpID+"' and hubID = '"+hub+"'").list();
				
				if(dailyCrewHubOpsDaily!=null && !dailyCrewHubOpsDaily.isEmpty() && dailyCrewHubOpsDaily.get(0)!=null){
					hubCrewNo = dailyCrewHubOpsDaily.get(0).toString();
				}else{
					if(dailyCrewHubOpsHub!=null && !dailyCrewHubOpsHub.isEmpty() && dailyCrewHubOpsHub.get(0)!=null){
						hubCrewNo = dailyCrewHubOpsHub.get(0).toString();
					}
				}
			hubCrewMap.put(addDays.get(0).toString(), hubCrewNo);
			}
			finalMap.put(wareHouseCrewMap, hubCrewMap);
		return  finalMap;		 
	 }
	 
	 public Long getDailyOpsLimitForOi(Long dailyCount, String typeService,Date dateFormat, String hubID, String sessionCorpID)
	 {
		
		 SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateFormat));
			String dateOI = nowYYYYMMDD1.toString();
		 
		 String wttypeService=wtServiceList(sessionCorpID);
		 Long diff = null;
		 List ticketCount = new ArrayList();
		 ticketCount = this.getSession().createSQLQuery("Select count(*) from workticket t where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and t.date2 = '"+dateOI+"' ").list();
		 if(ticketCount!=null && !ticketCount.isEmpty() && ticketCount.get(0)!=null){
		String totalTicketCount = ticketCount.toString().replaceAll("[\\[\\](){}]","").trim();
		diff = dailyCount -  Long.parseLong(totalTicketCount);
		 }
		 return diff;
	 	}
	 public List getAllTicketByDate(String typeService,Date dateFormat, String hubID, String sessionCorpID, String itemsjbkequipId)
	 {
		 SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		String ticket ="";	
		 StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateFormat));
			String dateOI = nowYYYYMMDD1.toString();
			 String wttypeService=wtServiceList(sessionCorpID);
			 List totalQuantity = new ArrayList();
			 List ticketByDate = new ArrayList();
			 ticketByDate = this.getSession().createSQLQuery("select ticket from workticket t where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and t.date2 = '"+dateOI+"' ").list();
			 if(ticketByDate!=null && !ticketByDate.isEmpty() && ticketByDate.get(0)!=null){
				 Iterator it = ticketByDate.iterator();
				 while(it.hasNext())
				 {
					 Object obj = (Object)it.next();
					 if(ticket.equalsIgnoreCase(""))
					 {
						 ticket="'"+obj.toString().trim()+"'";
					 }else{
						 ticket = ticket +",'"+obj.toString().trim()+"'";
					 }
				 }
			 totalQuantity= this.getSession().createSQLQuery("select sum(qty),type,descript from itemsjbkequip where corpid='"+sessionCorpID+"' and ticket in ("+ticket+") and id not in ("+itemsjbkequipId+") group by type,descript").list();
			 return  totalQuantity;
			 }else{
				 return null;
			 }
	 }
	 
	 public List checkQuantityInOperation(String resorceTypeOI,String resorceDescriptionOI, String hubID, String sessionCorpID)
	 {
		 List checkQuantity = new ArrayList();
		 checkQuantity = this.getSession().createSQLQuery("select resourceLimit,resource,category from operationsresourcelimits where corpid='"+sessionCorpID+"' and hubId = '"+hubID+"' and resource='"+resorceDescriptionOI+"' and category= '"+resorceTypeOI+"'").list();	 
		 if(checkQuantity!=null && !checkQuantity.isEmpty() && checkQuantity.get(0)!=null){
			 return checkQuantity;
		 }else{
			 return null;
		 }
	 	}
	 
		public Long msgForThreshold(Long dailyCount,Long dailyCountPC,Date dateFormat, String hubID, String sessionCorpID){
		 SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			String ticket ="";	
			Long totalTicket = 0L;
			 StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dateFormat));
				String dateOI = nowYYYYMMDD1.toString();
				 String wttypeService=wtServiceList(sessionCorpID);
				 List ticketByDate = new ArrayList();
				 ticketByDate = this.getSession().createSQLQuery("select count(*) from workticket t where t.targetActual not in ('C','R','P') and t.service in ("+wttypeService+") and t.corpID = '"+sessionCorpID+"' and t.warehouse = '"+hubID+"' and t.date2 = '"+dateOI+"' ").list();
		Long count = dailyCount *(dailyCountPC/100);
		if(ticketByDate!=null && !ticketByDate.isEmpty() && ticketByDate.get(0)!=null){
		String totalTicketCount = ticketByDate.toString().replaceAll("[\\[\\](){}]","").trim();
		totalTicket = Long.parseLong(totalTicketCount);
		}
		return totalTicket;
		}
		
		public List getServiceForOI( String ticketId,String sessionCorpID ,String shipNumber)
		 {
			 return getHibernateTemplate().find("from WorkTicket where shipNumber='" + shipNumber + "' and corpID='"+sessionCorpID +"' and id='"+ticketId+"'");	 
		 }
		public void updateStatusOfWorkTicket(String returnAjaxStringValue,String shipNumber, String corpID)
		{
			 getHibernateTemplate().bulkUpdate("update WorkTicket set targetActual='P' where shipNumber='"+ shipNumber +"' and ticket in ("+returnAjaxStringValue+") and corpID='"+corpID +"'"); 
		}
		public List workTicketList(Long serviceOrderId) {
			return getHibernateTemplate().find("from WorkTicket where serviceOrderId='" + serviceOrderId + "'");
		}
		
		public List getAllDataItemList(Long workTicketId, String shipNumber)
		{
			return getHibernateTemplate().find("from ItemData where workTicketId='" + workTicketId + "' order by (itemDesc * 1) , itemDesc");
		}
		public String getTruckNumber(Long workTicket, String sessionCorpID){
			String query1= "select distinct localtrucknumber from truckingoperations where corpid='"+sessionCorpID+"' and ticket='"+workTicket+"' and isTruckUsed is true limit 1";
			String truckNumber="";
			List l = this.getSession().createSQLQuery(query1).list();
			if(l!=null && !l.isEmpty()){
				truckNumber = l.get(0).toString();
			}
			return truckNumber;
		}		
		public String getServiceAndWareHouseByTicket(String workTicket,String sessionCorpID){
			String returnVal="";
			String query="select concat(service,'~',warehouse,'~',targetActual) from workticket where corpid='"+sessionCorpID+"' and ticket='"+workTicket+"' ";
			List al = this.getSession().createSQLQuery(query).list();
			if ((al != null) && (!al.isEmpty()) && (al.get(0)!=null)) {
				returnVal = al.get(0).toString();
			}	
			return returnVal;
		}
		
		public void updateDateByTicket(Long ticket,Date date1,String sessionCorpID,String updatedBy,Boolean updateValue){
			if (!(date1 == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(date1));
				toDate = nowYYYYMMDD1.toString();
			}
			getHibernateTemplate().bulkUpdate("update OperationsIntelligence set date='"+toDate+"',updatedBy='"+updatedBy+"', updatedOn=now() where ticket ='"+ticket+"' and corpID='"+sessionCorpID +"' ");
			if(updateValue!=null && updateValue){
				getHibernateTemplate().bulkUpdate("update WorkTicket set date1='"+toDate+"',date2='"+toDate+"',targetActual='P',updatedBy='System :"+updatedBy+"', updatedOn=now() where ticket ='"+ticket+"' and corpID='"+sessionCorpID +"'");
			}else{
				getHibernateTemplate().bulkUpdate("update WorkTicket set date1='"+toDate+"',date2='"+toDate+"',updatedBy='System :"+updatedBy+"', updatedOn=now() where ticket ='"+ticket+"' and corpID='"+sessionCorpID +"'");
			}
		}
		public List planningCalendarListForUnip(String sessionCorpID,String wareHouses,String printStart,String printEnd){
			String unit=findDefaultWeightUnit(sessionCorpID);
			String userFilter=getUserCompanyDivisionFilter();
			Date startDate=null;
			Date endDate=null;
			String startDate1="";
			String endDate1="";
			if(printStart==null || printStart.equals("")){
			Calendar calc = Calendar.getInstance();
			calc.setTime(new Date());
			calc.add(Calendar.DATE, -60);
			startDate=calc.getTime();
			calc.setTime(new Date());
			calc.add(Calendar.DATE, 360);
			endDate=calc.getTime();
								
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			StringBuilder sb = new StringBuilder(df.format(startDate));
			startDate1 = sb.toString();
			
			sb = new StringBuilder(df.format(endDate));
			endDate1 = sb.toString();
			}else{
				
				startDate1 = printStart;
				
				endDate1 = printEnd;
			}
			String query="select "+
						"concat('#&nbsp;<font color=#2779aa><strong><a target=_blank href=editWorkTicketUpdate.html?id=',w.id,'>',w.ticket,'</a></strong></font>(<font color=red>',w.targetactual,'</font>)') ticketNumber, "+
						"date_format(w.date1,'%Y-%m-%d') 'start', "+
						"date_format(w.date2,'%Y-%m-%d') 'end', "+
						"if(w.service is null or w.service='','',concat('<font color=#2779aa><strong>',w.service,'</strong></font>')) 'service', "+
						"if(w.warehouse is null or w.warehouse='','',w.warehouse) 'warehouse', "+
						"datediff(w.date2,w.date1)+1 'duration', "+
						"if(w.timeFrom='00:00','',concat('<font color=#2779aa><strong>',w.timeFrom,'</strong></font>')) 'startTime', "+					
						"concat(if(w.actualVolume is null OR w.actualVolume=0,concat(if(w.estimatedCubicFeet is null OR w.estimatedCubicFeet=0,'',concat(w.estimatedCubicFeet,' ',w.unit2))),concat(w.actualVolume,' ',w.unit2))) 'volume', "+
						"if(s.originCity is null or s.originCity='','',s.originCity) 'originCity', "+
						"if(s.originCountry is null or s.originCountry='','',s.originCountry) 'originCountry', "+
						"if(s.destinationCity is null or s.destinationCity='','',s.destinationCity) 'destinationCity', "+
						"if(s.destinationCountry is null or s.destinationCountry='','',s.destinationCountry) 'destinationCountry', "+
						"if(w.firstName is null or w.firstName='','',w.firstName) 'firstName', "+
						"if(w.lastName is null or w.lastName='','',w.lastName) 'lastName', "+
						"if(s.mode is null or s.mode='','',s.mode) 'jobMode', "+
						"if(s.status is null or s.status='','',s.status) 'status', "+
						"concat(if(w.actualWeight is null OR w.actualWeight=0,concat(if(w.estimatedWeight is null OR w.estimatedWeight=0,'',concat(w.estimatedWeight,' ',w.unit1))),concat(w.actualWeight,' ',w.unit1))) 'volume1', "+
						"if(s.routing is null or s.routing='','',s.routing) 'routing', "+
						"if(s.coordinator is null or s.coordinator='','',s.coordinator) 'coordinator', "+
						"if(s.salesMan is null or s.salesMan='','',s.salesMan) 'salesPerson', "+
						"if(s.originState is null or s.originState='','',s.originState) 'originState', "+ 
						"if(s.originZip is null or s.originZip='','',s.originZip) 'originZip', "+
						"if(s.destinationState is null or s.destinationState='','',s.destinationState) 'destinationState', "+
						"if(s.destinationZip is null or s.destinationZip='','',s.destinationZip) 'destinationZip', "+
						"if(s.originAddressLine1 is null or s.originAddressLine1='','',s.originAddressLine1) 'originAddressLine1', "+
						"if(s.originAddressLine2 is null or s.originAddressLine2='','',s.originAddressLine2) 'originAddressLine2', "+
						"if(s.destinationAddressLine1 is null or s.destinationAddressLine1='','',s.destinationAddressLine1) 'destinationAddressLine1', "+
						"if(s.destinationAddressLine2 is null or s.destinationAddressLine2='','',s.destinationAddressLine2) 'destinationAddressLine2', "+
						" w.ticket,w.shipNumber as shiNum  "+
						"from workticket w,serviceorder s "+
						"where s.shipNumber=w.shipNumber and s.status not in ('CNCL','CLSD') AND w.targetActual not in ('C','R') AND s.corpId='"+sessionCorpID+"' AND w.corpid = '"+sessionCorpID+"' "+((wareHouses!=null && !wareHouses.equalsIgnoreCase(""))?" and w.warehouse in ("+wareHouses+")":"")+((userFilter!=null && !userFilter.equalsIgnoreCase(""))?" and w.companyDivision in ("+userFilter+")":"")+((userFilter!=null && !userFilter.equalsIgnoreCase(""))?" and s.companyDivision in ("+userFilter+")":"")+
						" and ( (w.date1 between '"+startDate1+"' and  '"+endDate1+"') or (w.date2 between '"+startDate1+"' and  '"+endDate1+"') )  "+
						"order by 2,3,4";
			
						List DailyList=this.getSession().createSQLQuery(query).list();
						PlanningCalendarDTO dto = null;
						List dailySheetList=new ArrayList();
			Iterator it = DailyList.iterator();
			String str="";
			while(it.hasNext()){
				Object []row= (Object [])it.next();
				dto = new PlanningCalendarDTO();
				dto.setTicketNumber(row[0]);
				dto.setStart(row[1]);
				dto.setEnd(row[2]);
				dto.setService(row[3]);
				dto.setWarehouse(row[4]);
				dto.setDuration(row[5]);
				dto.setStartTime(row[6]);
				dto.setVolume(row[7]);
				dto.setFirstName(row[12]);
				dto.setLastName(row[13]);
				dto.setMode(row[14]);
				dto.setStatus(row[15]);
				dto.setVolume1(row[16]);
				dto.setRouting(row[17]);
				if(row[18]!=null && !row[18].equals("")){
					String coordName = row[18].toString();
					dto.setCoordinatorName(coordName.substring(0, 2));
				}else{
					dto.setCoordinatorName("");
				}
				if(row[19]!=null && !row[19].equals("")){
					String salesManName = row[19].toString();
					dto.setSalesPerson(salesManName.substring(0, 2));
				}else{
					dto.setSalesPerson("");
				}
				if(dto.getFirstName()!=null && !dto.getFirstName().toString().trim().equalsIgnoreCase("")){
					dto.setFirstName(dto.getFirstName().toString().trim().replaceAll("\"", ""));
				}
				if(dto.getLastName()!=null && !dto.getLastName().toString().trim().equalsIgnoreCase("")){
					dto.setLastName(dto.getLastName().toString().trim().replaceAll("\"", ""));
				}
				String query1="select c.lastname from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where t.ticket='"+row[28]+"' and t.corpID='"+sessionCorpID+"'";
				List crewNameList=this.getSession().createSQLQuery(query1).list();
				if(crewNameList!=null && !crewNameList.isEmpty() && crewNameList.get(0)!=null){			
					dto.setCrewList(crewNameList.toString().replace("[", "").replace("]", ""));
	        	}else{
	        		dto.setCrewList("");	
	        	}
				
				String query2="select localTruckNumber from truckingoperations where ticket='"+row[28]+"' and corpID='"+sessionCorpID+"' group by localTruckNumber";
				List truckNameList=this.getSession().createSQLQuery(query2).list();
				if(truckNameList!=null && !truckNameList.isEmpty() && truckNameList.get(0)!=null){			
					dto.setTruckList(truckNameList.toString().replace("[", "").replace("]", ""));
	        	}else{
	        		dto.setTruckList("");	
	        	}
				
				String fullAddress="";
				if((dto.getRouting()!=null && !dto.getRouting().toString().equalsIgnoreCase("")) && dto.getRouting().toString().equalsIgnoreCase("EXP")){
					if(row[24]!=null && !row[24].equals("")){
						fullAddress = row[24].toString()+",";
					}
					if(row[25]!=null && !row[25].equals("")){
						fullAddress = fullAddress+row[25].toString()+",";
					}
					if(row[8]!=null && !row[8].equals("")){
						fullAddress = fullAddress+row[8].toString()+",";
					}
					if(row[20]!=null && !row[20].equals("")){
						fullAddress = fullAddress+row[20].toString()+",";
					}
					if(row[9]!=null && !row[9].equals("")){
						fullAddress = fullAddress+row[9].toString()+",";
					}
					if(row[21]!=null && !row[21].equals("")){
						fullAddress = fullAddress+row[21].toString();
					}
				}else if((dto.getRouting()!=null && !dto.getRouting().toString().equalsIgnoreCase("")) && dto.getRouting().toString().equalsIgnoreCase("IMP")){
					if(row[26]!=null && !row[26].equals("")){
						fullAddress = row[26].toString()+",";
					}
					if(row[27]!=null && !row[27].equals("")){
						fullAddress = fullAddress+row[27].toString()+",";
					}
					if(row[10]!=null && !row[10].equals("")){
						fullAddress = fullAddress+row[10].toString()+",";
					}
					if(row[22]!=null && !row[22].equals("")){
						fullAddress = fullAddress+row[22].toString()+",";
					}
					if(row[11]!=null && !row[11].equals("")){
						fullAddress = fullAddress+row[11].toString()+",";
					}
					if(row[23]!=null && !row[23].equals("")){
						fullAddress = fullAddress+row[23].toString();
					}
				}else if((dto.getRouting()!=null && !dto.getRouting().toString().equalsIgnoreCase("")) && dto.getRouting().toString().equalsIgnoreCase("NAT")){
					String originAddress="";
					String destinationAddress="";
					if(row[24]!=null && !row[24].equals("")){
						originAddress = row[24].toString();
					}
					if(row[25]!=null && !row[25].equals("")){
						originAddress = originAddress+","+row[25].toString();
					}
					if(row[8]!=null && !row[8].equals("")){
						originAddress = originAddress+","+row[8].toString();
					}
					if(row[20]!=null && !row[20].equals("")){
						originAddress = originAddress+","+row[20].toString();
					}
					if(row[9]!=null && !row[9].equals("")){
						originAddress = originAddress+","+row[9].toString();
					}
					if(row[21]!=null && !row[21].equals("")){
						originAddress = originAddress+","+row[21].toString();
					}
					if(row[26]!=null && !row[26].equals("")){
						destinationAddress = destinationAddress+row[26].toString();
					}
					if(row[27]!=null && !row[27].equals("")){
						destinationAddress = destinationAddress+","+row[27].toString();
					}
					if(row[10]!=null && !row[10].equals("")){
						destinationAddress = destinationAddress+","+row[10].toString();
					}
					if(row[22]!=null && !row[22].equals("")){
						destinationAddress = destinationAddress+","+row[22].toString();
					}
					if(row[11]!=null && !row[11].equals("")){
						destinationAddress = destinationAddress+","+row[11].toString();
					}
					if(row[23]!=null && !row[23].equals("")){
						destinationAddress = destinationAddress+","+row[23].toString();
					}
					fullAddress = originAddress+" &rarr; "+destinationAddress;
				}
				dto.setAddressList(fullAddress);
				dto.setShipNum(row[29]);
				str=(dto.getStartTime()!=null && !dto.getStartTime().toString().equalsIgnoreCase("")?dto.getStartTime()+" ":"")+""
						+ ""+(dto.getService()!=null && !dto.getService().toString().equalsIgnoreCase("")?dto.getService()+" for ":"")
						+ ""+dto.getFirstName()+" "+dto.getLastName()+(((dto.getFirstName()!=null && !dto.getFirstName().toString().equalsIgnoreCase(""))||(dto.getLastName()!=null && !dto.getLastName().toString().equalsIgnoreCase("")))?"/":"")+""
						+ ""+((unit!=null && unit.equalsIgnoreCase("Lbs"))?(dto.getVolume1()!=null && !dto.getVolume1().toString().equalsIgnoreCase("")?dto.getVolume1()+"/ ":""):(dto.getVolume()!=null && !dto.getVolume().toString().equalsIgnoreCase("")?dto.getVolume()+"/":""))+""
						+ ""+(dto.getMode()!=null && !dto.getMode().toString().equalsIgnoreCase("")?dto.getMode()+"/":"")+""
						+ ""+(dto.getStatus()!=null && !dto.getStatus().toString().equalsIgnoreCase("")?dto.getStatus()+"/":"")+""
						+ ""+(dto.getCrewList()!=null && !dto.getCrewList().toString().equalsIgnoreCase("")?dto.getCrewList()+"/":"")+""
						+ ""+(dto.getTruckList()!=null && !dto.getTruckList().toString().equalsIgnoreCase("")?dto.getTruckList()+"/":"")+""
						+ ""+(dto.getEnd()!=null && !dto.getEnd().toString().equalsIgnoreCase("")?dto.getEnd()+"/":"")+""
						+ ""+(dto.getCoordinatorName()!=null && !dto.getCoordinatorName().toString().equalsIgnoreCase("")?dto.getCoordinatorName()+"/":"")+""
						+ ""+(dto.getSalesPerson()!=null && !dto.getSalesPerson().toString().equalsIgnoreCase("")?dto.getSalesPerson()+"/":"")+""
						+ ""+(dto.getAddressList()!=null && !dto.getAddressList().toString().equalsIgnoreCase("")?dto.getAddressList()+"/":"")+""
						+ ""+(dto.getShipNum()!=null && !dto.getShipNum().toString().equalsIgnoreCase("")?dto.getShipNum()+" ":"")
					    + ""+(dto.getTicketNumber()!=null && !dto.getTicketNumber().toString().equalsIgnoreCase("")?dto.getTicketNumber()+" ":"");
					    
				dto.setTitle(str);
				dailySheetList.add(dto);
			}
			return dailySheetList;
		}
		public String findCrewEmail(String wRHouse, Date dailySheetDate,String sessionCorpID,Long ticket){
			String value="";
			String dailySheetDate1 = "";
			if (!(dailySheetDate == null)) {
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(dailySheetDate));
				dailySheetDate1 = nowYYYYMMDD1.toString();
			}
			 List crewNoList=new ArrayList();
			 String query = "select crewEmail from timesheet where corpid='"+sessionCorpID+"' and ticket="+ticket+" and workdate='"+dailySheetDate1+"' and warehouse='" +wRHouse+"' ";
			 crewNoList=this.getSession().createSQLQuery(query).list();
			 if((crewNoList!=null)&&(!crewNoList.isEmpty())&&(crewNoList.get(0)!=null))
				{
					value=crewNoList.get(0).toString();
				}
				return value;		
		}
}