<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="QualitySurveyReport"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="649"
		 pageHeight="842"
		 columnWidth="589"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Customer File Number" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT
	"QualitySurveyReport-UGMY.jrxml",
	customerfile.`firstName` AS customerfile_firstName,
	customerfile.`lastName` AS customerfile_lastName,
	customerfile.`originCountry` AS customerfile_originCountry,
	customerfile.`destinationCountry` AS customerfile_destinationCountry,
	customerfile.`billtoname` AS customerfile_billtoname,
	customerfile.`sequenceNumber` AS customerfile_sequenceNumber,
	customerfile.companyDivision AS customerfile_companyDivision,
	concat("Please Return via Fax: ",if(systemdefault.fax is null || systemdefault.fax="","",systemdefault.fax),"  or via email: ",if(systemdefault.email is null || systemdefault.email="","",systemdefault.email)) as systemdefault_fax_email,
	concat("Please rate your satisfaction with the ",systemdefault.company," service person that coordinated your move.(  ) ") as comment1,
	concat("Please rate your ",systemdefault.company," move overall. (   ) ") as comment2,
	app_user.alias as customerfile_coordinator,
	concat("Would you use or recommend ",systemdefault.company,"  again? Y (   )     N (   )  ") as comment3,
	concat("Thank you very much for your time and please do not hesitate to let us know if there is anything else ",systemdefault.company," can do to assist in making your move your best move yet! ") as comment4

FROM
     `customerfile` customerfile
	LEFT OUTER JOIN systemdefault systemdefault ON customerfile.corpid=systemdefault.corpid and systemdefault.corpid = $P{Corporate ID}
	left outer join `app_user` app_user on customerfile.`coordinator` =app_user.`username` and app_user.`corpID`=$P{Corporate ID}
WHERE
    customerfile.corpid = $P{Corporate ID}
    and customerfile.`sequenceNumber` = $P{Customer File Number}]]></queryString>

	<field name="QualitySurveyReport-UGMY.jrxml" class="java.lang.String"/>
	<field name="customerfile_firstName" class="java.lang.String"/>
	<field name="customerfile_lastName" class="java.lang.String"/>
	<field name="customerfile_originCountry" class="java.lang.String"/>
	<field name="customerfile_destinationCountry" class="java.lang.String"/>
	<field name="customerfile_billtoname" class="java.lang.String"/>
	<field name="customerfile_sequenceNumber" class="java.lang.String"/>
	<field name="customerfile_companyDivision" class="java.lang.String"/>
	<field name="systemdefault_fax_email" class="java.lang.String"/>
	<field name="comment1" class="java.lang.String"/>
	<field name="comment2" class="java.lang.String"/>
	<field name="customerfile_coordinator" class="java.lang.String"/>
	<field name="comment3" class="java.lang.String"/>
	<field name="comment4" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="114"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="5"
						y="67"
						width="274"
						height="45"
						key="staticText-1"/>
					<box topPadding="3"></box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font pdfFontName="Helvetica-Bold" size="20" isBold="true"/>
					</textElement>
				<text><![CDATA[Quality Survey Report
]]></text>
				</staticText>
				<image  onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="388"
						y="2"
						width="201"
						height="53"
						key="image-4"/>
					<box></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA[($F{customerfile_companyDivision}.equals("UNIMY") ? "../../images/Logo_UTS_RGB.jpg" :($F{customerfile_companyDivision}.equals("SMKL")? "../../images/subalipack_logo.jpg" :""))]]></imageExpression>
				</image>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="600"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Transparent"
						x="5"
						y="0"
						width="100"
						height="18"
						key="staticText-2"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Transferee Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="295"
						y="0"
						width="120"
						height="18"
						key="staticText-3"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Job Number]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="5"
						y="36"
						width="100"
						height="18"
						key="staticText-4"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Country of Origin]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="5"
						y="18"
						width="100"
						height="18"
						key="staticText-5"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Company]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="405"
						y="36"
						width="170"
						height="18"
						key="textField-1"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_destinationCountry}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="100"
						y="36"
						width="185"
						height="18"
						key="textField-2"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"
						isPrintWhenDetailOverflows="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_originCountry}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="100"
						y="0"
						width="185"
						height="18"
						key="textField-3"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[($F{customerfile_firstName}==null ? "" : $F{customerfile_firstName})+" "+($F{customerfile_lastName}==null ? "" : $F{customerfile_lastName})]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						mode="Transparent"
						x="295"
						y="36"
						width="120"
						height="18"
						key="staticText-15"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Destination]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="295"
						y="18"
						width="120"
						height="18"
						key="staticText-24"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box leftPadding="1">					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[Moving Coordinator]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="405"
						y="0"
						width="170"
						height="18"
						key="textField-21"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_sequenceNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="405"
						y="18"
						width="170"
						height="18"
						key="textField-22"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_coordinator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Transparent"
						x="100"
						y="18"
						width="185"
						height="18"
						key="textField-23"
						isPrintRepeatedValues="false"
						isRemoveLineWhenBlank="true"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement verticalAlignment="Middle">
						<font pdfFontName="Helvetica" isBold="false" isUnderline="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{customerfile_billtoname}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="5"
						y="111"
						width="584"
						height="15"
						key="staticText-52"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Please rate our services on a scale of 1 to 8, 8 being Excellence.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="69"
						width="584"
						height="42"
						key="staticText-55"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[The purpose of this survey is to evaluate the services performed by us so that we could better serve our customers in
future. Your feedback is valuable to us and your cooperation in completing this survey is greatly appreciated.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="126"
						width="584"
						height="15"
						key="staticText-56"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Please rate your satisfaction with the service provided to you by the surveyor/estimator who visited your]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="141"
						width="584"
						height="15"
						key="staticText-57"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[home. (   )]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="156"
						width="584"
						height="15"
						key="staticText-58"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Please rate your satisfaction with the services at time of origin. (   )]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="171"
						width="584"
						height="15"
						key="staticText-59"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Please rate your satisfaction with the services at time of destination. (   )]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="231"
						width="584"
						height="15"
						key="staticText-63"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Do you intend to file a claim? Y (   )      N (   )]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="246"
						width="584"
						height="15"
						key="staticText-64"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[What did you like the most about our service?]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="4"
						y="266"
						width="585"
						height="1"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="4"
						y="332"
						width="585"
						height="1"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="5"
						y="276"
						width="584"
						height="15"
						key="staticText-65"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[What could we have done better?]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="308"
						width="584"
						height="15"
						key="staticText-66"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[What additional services do you wish we provided at the time of your move?]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="4"
						y="299"
						width="585"
						height="1"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="5"
						y="338"
						width="584"
						height="15"
						key="staticText-67"/>
					<box leftPadding="30"></box>
					<textElement>
						<font pdfFontName="Helvetica" isBold="false"/>
					</textElement>
				<text><![CDATA[Do we have your permission to use your comments as a reference? Y (   )       N (   )]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="5"
						y="405"
						width="584"
						height="42"
						key="staticText-69"/>
					<box></box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" isBold="true" isItalic="false"/>
					</textElement>
				<text><![CDATA[I realize it is an extremely busy time for you but your feedback is extremely important to us and ultimately
determines the course of relationship as a preferred vendor.]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="358"
						y="531"
						width="230"
						height="18"
						key="staticText-71"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[DATE :_____________________]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Transparent"
						x="5"
						y="531"
						width="353"
						height="18"
						key="staticText-72"
						isPrintRepeatedValues="false"
						isPrintInFirstWholeBand="true"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" size="10" isBold="true"/>
					</textElement>
				<text><![CDATA[CUSTOMER :_____________________]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="121"
						width="13"
						height="15"
						key="staticText-73"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="151"
						width="13"
						height="15"
						key="staticText-74"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="166"
						width="13"
						height="15"
						key="staticText-75"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="181"
						width="13"
						height="15"
						key="staticText-76"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="196"
						width="13"
						height="15"
						key="staticText-77"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="212"
						width="13"
						height="15"
						key="staticText-78"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="241"
						width="13"
						height="15"
						key="staticText-79"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="10"
						y="226"
						width="13"
						height="15"
						key="staticText-80"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="9"
						y="271"
						width="13"
						height="15"
						key="staticText-81"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="9"
						y="302"
						width="13"
						height="15"
						key="staticText-82"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="9"
						y="333"
						width="13"
						height="15"
						key="staticText-83"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Top">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[.]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="8"
						y="449"
						width="579"
						height="36"
						key="textField-29"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{comment4}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="6"
						y="216"
						width="583"
						height="16"
						key="textField-30"/>
					<box leftPadding="30"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{comment3}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="199"
						width="584"
						height="15"
						key="textField-31"/>
					<box leftPadding="30"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{comment2}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="5"
						y="185"
						width="584"
						height="15"
						key="textField-32"/>
					<box leftPadding="30"></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{comment1}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="10"
						y="488"
						width="574"
						height="30"
						key="textField-33"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{systemdefault_fax_email}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="94"
						y="0"
						width="5"
						height="18"
						key="staticText-84"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="94"
						y="18"
						width="5"
						height="18"
						key="staticText-85"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="94"
						y="36"
						width="5"
						height="18"
						key="staticText-86"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="399"
						y="0"
						width="5"
						height="18"
						key="staticText-87"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="399"
						y="18"
						width="5"
						height="18"
						key="staticText-88"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="399"
						y="36"
						width="5"
						height="18"
						key="staticText-89"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font pdfFontName="Helvetica-Bold" isBold="true"/>
					</textElement>
				<text><![CDATA[:]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
