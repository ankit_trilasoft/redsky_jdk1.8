package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.Agent;

public interface AgentManager extends GenericManager<Agent, Long> { 
    public List<Agent> findByLastName(String lastName); 
    public List checkById(Long id);
    
    public List findMaximumCarrierNumber(String shipNum);

} 


