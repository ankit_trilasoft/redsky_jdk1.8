<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<title><fmt:message key="accountLineDetail.title" /></title>
<meta name="heading" content="<fmt:message key='accountLineDetail.heading'/>" />
<style type="text/css"> 
.true{display:none;}
.false{display:show;} 
.subcontenttabChild { background: url("images/greylinebg.gif"); }
</style>

<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
<%-- Shift1 --%>
</head>
<jsp:include flush="true" page="accountLineFormsJS.jsp"></jsp:include>  
<jsp:include flush="true" page="accountLineFormsJSSecond.jsp"></jsp:include> 
<body onload="expense('none');"> 
<div id="Layer5" style="width:100%;" >
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/> 
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="shipNumber" id ="shipNumber" value="%{serviceOrder.shipNumber}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" value="ServiceOrder" />
<s:form id="accountLineForms" name="accountLineForms" action="saveAccountLine" onsubmit="return checkPaymentStatusPIR();" method="post" validate="true">
<c:set var="grossMarginActualization" value="N" />
<configByCorp:fieldVisibility componentId="component.field.accountLine.grossMarginActualization">
<c:if test="${((serviceOrder.estimatedGrossMarginPercentage<10 || serviceOrder.revisedGrossMarginPercentage<10) && (billing.approvedBy ==null || billing.approvedBy =='' || empty billing.dateApproved))}">
<c:set var="grossMarginActualization" value="Y"/>
</c:if>
<c:if test="${((serviceOrder.estimatedGrossMarginPercentage=='0' || serviceOrder.estimatedGrossMarginPercentage=='0.00') && (serviceOrder.revisedGrossMarginPercentage=='0' || serviceOrder.revisedGrossMarginPercentage=='0.00' ))}">
<c:set var="grossMarginActualization" value="N"/>
</c:if>
</configByCorp:fieldVisibility>
<s:hidden name="grossMarginActualization" value="${grossMarginActualization}"/>
<s:hidden name="accountLine.invoicereportparameter"/>
<s:hidden name="vendorCodeOwneroperator" />
 <s:hidden name="chargeCodeValidationVal" />
 <s:hidden name="checkComputeDescription" />
 <s:hidden name="chargeCodeValidationMessage"  id= "chargeCodeValidationMessage" />
<s:hidden name="oldAccountLineBillToCode" value="${accountLine.billToCode}" />
<s:hidden name="vanLineAccountView" value="${vanLineAccountView}" />
<s:hidden name="accountLine.dynamicNavisionFlag"  />
<s:hidden name="accountLine.deleteDynamicNavisionFlag"  />
<s:hidden name="accountLine.oldCompanyDivision"  />
<s:hidden name="accountLine.dynamicNavXfer"  />
<s:hidden name="isSOExtract"/>
<s:hidden name="accRecVatDescr"  value="${accountLine.recVatDescr}"/>
<s:hidden name="accPayVatDescr"  value="${accountLine.payVatDescr}"/>
<s:hidden name="accountLine.invoiceAutoUpload" /> 
<s:hidden name="accountLine.invoiceType"  />
<s:hidden name="accountLine.activateAccPortal" />
<s:hidden name="accountLine.categoryResourceName" />
<s:hidden name="accountLine.driverCompanyDivision" />
<s:hidden name="accountLine.download" />
<s:hidden name="accountLine.selectiveInvoice" /> 
<s:hidden name="accountLine.accountLineCostElement" />
<s:hidden name="accountLine.accountLineScostElementDescription" /> 
<s:hidden name="contractTypeValue" />
<s:hidden name="contractType" />
<s:hidden name="accountLine.additionalService" />
<s:hidden name="masterId"  value="${serviceOrder.id}"/>
<s:hidden name="chargeCostElement" value="" />
<s:hidden name="serviceOrder.grpStatus"  />
<s:hidden name="serviceOrder.controlFlag" />
<s:hidden name="accountLine.groupAccount" /> 
<s:hidden name="contractTypeAcc" value="${contractType}" />
<s:hidden name="accountLine.VATExclude"  value="${accountLine.VATExclude}" />
  <s:hidden name="accountLine.varianceRevenueAmount" />
  <s:hidden name="accountLine.varianceExpenseAmount" />
	<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
	</configByCorp:fieldVisibility>
	<s:hidden name="soRegistrationNumber" value="${serviceOrder.registrationNumber}"/>
	<configByCorp:fieldVisibility componentId="component.field.Alternative.SetRegistrationNumber">
	<s:hidden name="setRegistrationNumber" value="YES"/>
	</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.Alternative.autoDriverReversal">
	<s:hidden name="automaticDriverReversal " value="YES" />
</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
		<s:hidden name="actgCodeFlag" value="YES" />
	</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<c:if test="${not empty accountLine.sendDynamicNavisionDate}"><s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> 
		<s:param name="value" value="accountLine.sendDynamicNavisionDate" /></s:text>
		<s:hidden  name="accountLine.sendDynamicNavisionDate" value="%{customerFileSurveyFormattedValue}" /> 
	</c:if>
	<c:if test="${not empty systemDefault.postDate1}"><s:text id="systemDefaultPostDate1Value" name="${FormDateValue}"> 
		<s:param name="value" value="systemDefault.postDate1" /></s:text>
		<s:hidden  name="systemDefaultPostDate1" value="%{systemDefaultPostDate1Value}" /> 
	</c:if>
	
	<c:if test="${empty accountLine.sendDynamicNavisionDate}">
			 <s:hidden   name="accountLine.sendDynamicNavisionDate"/> 
	</c:if>
	
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="accountLine.id" value="%{accountLine.id}" />
	<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
    <c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
    <c:set var="noteFor" value="ServiceOrder" scope="session"/>
    <c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
    <c:set var="tableName" value="accountline" scope="session"/>
    <c:if test="${empty serviceOrder.id}">
	      <c:set var="isTrue" value="false" scope="request"/>
    </c:if>
    <c:if test="${not empty serviceOrder.id}">
	      <c:set var="isTrue" value="true" scope="request"/>
     </c:if>
     <c:if test="${not empty billing.billComplete}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
			 <s:hidden  name="billing.billComplete" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty billing.billComplete}">
		 <s:hidden   name="billing.billComplete"/> 
	 </c:if>
     <c:if test="${not empty accountLine.accrueRevenueVariance}">
		 <s:text id="accrueRevenueVarianceFormattedDate" name="${FormDateValue}">
				<s:param name="value" value="accountLine.accrueRevenueVariance" />
		 </s:text>
		 <s:hidden  name="accountLine.accrueRevenueVariance" value="%{accrueRevenueVarianceFormattedDate}" /> 
	 </c:if>
	 <c:if test="${empty accountLine.accrueRevenueVariance}">
		 <s:hidden   name="accountLine.accrueRevenueVariance"/> 
	 </c:if>	
	 
	 <c:if test="${not empty accountLine.accrueExpenseVariance}">
		 <s:text id="accrueExpenseVarianceFormattedDate" name="${FormDateValue}">
				<s:param name="value" value="accountLine.accrueExpenseVariance" />
		 </s:text>
		 <s:hidden  name="accountLine.accrueExpenseVariance" value="%{accrueExpenseVarianceFormattedDate}" /> 
	 </c:if>
	 <c:if test="${empty accountLine.accrueExpenseVariance}">
		 <s:hidden   name="accountLine.accrueExpenseVariance"/> 
	 </c:if>	 
	 
	 <s:hidden name="partnerCommisionCode" value=""/>
	<s:hidden name="accountLine.serviceOrderId" value="%{serviceOrder.id}"/>
	<s:hidden name="accountLine.networkSynchedId" />
	<s:hidden name="bookingAgentCodeDMM" value="${bookingAgentCodeDMM}" />
	
	<s:hidden name="accountLine.agentFeesNo" />
	<s:hidden name="accNote" value="${accountLine.note}"/>
	<s:hidden name="accDescription" value="${accountLine.description}"/>
	<s:hidden name="accQuoteDescription" value="${accountLine.quoteDescription}"/>
	
	<s:hidden name="id" value="<%=request.getParameter("id")%>" />
	<s:hidden name="sid" value="<%=request.getParameter("sid")%>" />
    <s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}" />
	<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" />
	<s:hidden name="serviceOrder.sequenceNumber" />
	<s:hidden name="serviceOrder.ship" />
	<s:hidden name="rateEstimate" />
	<s:hidden name="accountLine.buyDependSell" value="${accountLine.buyDependSell}" />
	<s:hidden name="accountLine.deviation" value="${accountLine.deviation}" />
	<s:hidden name="ratePaybleHid" />
	<s:hidden name="localAmountPaybleHid" />
	<s:hidden name="systemDefaultmiscVl" value="%{systemDefaultmiscVl}" />
	<s:hidden name="systemDefaultCommissionJob" value="%{systemDefaultCommissionJob}" />
	<s:hidden name="systemDefaultstorage" value="%{systemDefaultstorage}" /> 
	<s:hidden name="systemDefaultVatCalculation" value="%{systemDefaultVatCalculation}" />
	<s:hidden name="salesCommisionRate" value="${salesCommisionRate}"/>
	<s:hidden name="grossMarginThreshold" value="${grossMarginThreshold}"/>
	<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
	<s:hidden name="inEstimatedTotalRevenue" value="${serviceOrder.estimatedTotalRevenue}"/>
	<s:hidden name="inRevisedTotalExpense" value="${serviceOrder.revisedTotalExpense}"/>
	<s:hidden name="inRevisedTotalRevenue" value="${serviceOrder.revisedTotalRevenue}"/>
	<s:hidden name="inActualExpense" value="${serviceOrder.actualExpense}"/>
	<s:hidden name="inActualRevenue" value="${serviceOrder.actualRevenue}"/>
	<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
    <s:hidden name="formStatus"/> 
    <s:hidden id="checkActSenttoclient" name="checkActSenttoclient" />
    <s:hidden id="dateUpdate" name="dateUpdate" />
    <s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minAccountLine" />
    <s:hidden name="accountLine.displayOnQuote" value="${accountLine.displayOnQuote}"/>
    <s:hidden name="maxAccountLine" />
    <s:hidden name="countAccountLine" />
    <s:hidden name="customerFile.id"/>
    <s:hidden name="checkLHF"/>
    <s:hidden name="serviceOrder.serviceType"/>
    <s:hidden name="accountLine.estimateStatus" value="${accountLine.estimateStatus}" />
    <c:if test="${accountInterface!='Y'}">
    <c:if test="${not empty accountLine.recAccDate}">
    <s:text id="recAccDate" name="${FormDateValue}"><s:param name="value" value="accountLine.recAccDate" /></s:text>
    <s:hidden  name="accountLine.recAccDate" value="%{recAccDate}" /> 
    </c:if>
    <c:if test="${empty accountLine.recAccDate}">
    <s:hidden   name="accountLine.recAccDate"/> 
    </c:if>
    </c:if>
    <c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
   <c:if test="${validateFormNav =='Ok'}">	
   <c:choose>
     <c:when test="${gotoPageString == 'gototab.serviceorderDetails'}">
        <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
     </c:when>
     <c:when test="${gotoPageString == 'gototab.billingDetails'}">
        <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.accountLineList'}">
       <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.containerList'}">    
		<c:if test="${forwardingTabVal!='Y'}"> 
       		<c:redirect url="/containers.html?id=${serviceOrder.id}"/>
       	</c:if>
       	<c:if test="${forwardingTabVal=='Y'}">
       		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
       	</c:if>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.OI' }">
			<c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
	</c:when>
   <c:when test="${gotoPageString == 'gototab.domestic'}">
      <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
   </c:when>
    			<c:when test="${gotoPageString == 'gototab.trackingStatusDetails' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>
			</c:when>
    <c:when test="${gotoPageString == 'gototab.customerWorkTicketsList'}">
       <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
    </c:when>
   <c:when test="${gotoPageString == 'gototab.claimsList'}">
       <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
   </c:when>
   <c:when test="${gotoPageString == 'gototab.CustomerFileDetails'}">
      <c:redirect url="editCustomerFile.html?id=${customerFile.id}"/>
    </c:when>
    
    <c:when test="${gotoPageString == 'gototab.moveNextAccountLine'}">
      <c:redirect url="/editNextAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}"/>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.movePrevAccountLine'}">
      <c:redirect url="/editPrevAccountLine.html?sidNum=${serviceOrder.id}&soIdNum=${accountLine.id}"/>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.moveSelectAccount'}">
      <c:redirect url="/editAccountLine.html?sid=${serviceOrder.id}&id=${selectedAccountId}"/>
    </c:when>
    <c:when test="${gotoPageString == 'gototab.criticaldate' }">
	<sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
		<c:redirect url="/soAdditionalDateDetails.html?sid=${serviceOrder.id}" />
	</sec-auth:authComponent>
	</c:when>	
    <c:otherwise>
    </c:otherwise>
  </c:choose>
</c:if> 
<c:if test="${validateFormNav =='networkAgentInvoiced'}">
<c:redirect url="/editAccountLine.html?sid=${serviceOrder.id}&id=${accountLine.id}&checkLHF=false"/>
</c:if>
	<c:set var="accountLine.contract" value="${accountLine.contract}"/>
	<c:set var="charge" value="${accountLine.chargeCode}"/>
	<s:hidden name="accountLine.contract" value="${accountLine.contract}"/>
	<s:hidden name="charge" value="${accountLine.chargeCode}"/>
        <s:hidden id="countCategoryTypeDetailNotes" name="countCategoryTypeDetailNotes" value="<%=request.getParameter("countCategoryTypeDetailNotes") %>"/>
		<s:hidden id="countEstimateDetailNotes" name="countEstimateDetailNotes" value="<%=request.getParameter("countEstimateDetailNotes") %>"/>
		<s:hidden id="countPayableDetailNotes" name="countPayableDetailNotes" value="<%=request.getParameter("countPayableDetailNotes") %>"/>
		<s:hidden id="countRevesionDetailNotes" name="countRevesionDetailNotes" value="<%=request.getParameter("countRevesionDetailNotes") %>"/>
		<s:hidden id="countEntitleDetailOrderNotes" name="countEntitleDetailOrderNotes" value="<%=request.getParameter("countEntitleDetailOrderNotes") %>"/>
		<s:hidden id="countReceivableDetailNotes" name="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>"/>
		<c:set var="countCategoryTypeDetailNotes" value="<%=request.getParameter("countCategoryTypeDetailNotes") %>" />
		<c:set var="countEstimateDetailNotes" value="<%=request.getParameter("countEstimateDetailNotes") %>" />
		<c:set var="countPayableDetailNotes" value="<%=request.getParameter("countPayableDetailNotes") %>" />
		<c:set var="countRevesionDetailNotes" value="<%=request.getParameter("countRevesionDetailNotes") %>" />
	    <c:set var="countEntitleDetailOrderNotes" value="<%=request.getParameter("countEntitleDetailOrderNotes") %>" />
		<c:set var="countReceivableDetailNotes" value="<%=request.getParameter("countReceivableDetailNotes") %>" />
	<div id="newmnav" style="float: left;">
	<ul>
	<s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
            <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
            <c:set var="relocationServicesKey" value="" />
            <c:set var="relocationServicesValue" value="" /> 
            <c:forEach var="entry" items="${relocationServices}">
            <c:if test="${relocationServicesKey==''}">
            <c:if test="${entry.key==serviceOrder.serviceType}">
            <c:set var="relocationServicesKey" value="${entry.key}" />
            <c:set var="relocationServicesValue" value="${entry.value}" /> 
            </c:if>
           </c:if> 
           </c:forEach>
		<li><a onclick="setReturnString('gototab.serviceorderDetails');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"/><span>S/O Details</span></a></li>
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
			<li><a onclick="setReturnString('gototab.billingDetails');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		<li id="newmnav1" style="background:#FFF "><a onclick="setReturnString('gototab.accountLineList');return autoSaveFunc('none');" onmouseover="return chkSelect('2');"class="current"><span>Accounting<img
			src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		<%-- <c:if test="${serviceOrder.job =='OFF'}"> --%>	
		<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}"> 	
	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a onclick="setReturnString('gototab.OI');return autoSaveFunc('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>	
	   <c:if test="${serviceOrder.job !='RLO'}"> 

       <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
		<li><a onclick="setReturnString('gototab.containerList');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Forwarding</span></a></li>
		</c:if>
		</c:if>
		<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS' || billing.billToCode =='P4071'}">
			<c:if test="${serviceOrder.job !='RLO'}"> 
			<li><a onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Domestic</span></a></li>
		    </c:if>
		</c:if>
		<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	     <c:if test="${serviceOrder.job =='INT'}">
	     <li><a onclick="setReturnString('gototab.domestic');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Domestic</span></a></li>
	     </c:if>
	     </sec-auth:authComponent>
        <li><a onclick="setReturnString('gototab.trackingStatusDetails');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Status</span></a></li>
		<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a onclick="setReturnString('gototab.customerWorkTicketsList');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Ticket</span></a></li>
		<configByCorp:fieldVisibility componentId="component.standard.claimTab">
		<li><a onclick="setReturnString('gototab.claimsList');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Claims</span></a></li>
		</configByCorp:fieldVisibility>
		</c:if>
		 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</sec-auth:authComponent>
		<li><a onclick="setReturnString('gototab.CustomerFileDetails');return autoSaveFunc('none');"onmouseover="return chkSelect('2');"><span>Customer File</span></a></li>
         <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
	 			<li><a onclick="setReturnString('gototab.criticaldate');return autoSaveFunc('none');" onmouseover="return chkSelect('2');"><span>Critical Dates</span></a></li>
			</sec-auth:authComponent>
        <li><a onclick="window.open('auditList.html?id=${accountLine.id}&tableName=accountline&decorator=popup&popup=true','audit','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</div>
	<table cellpadding="0" cellspacing="0" height="22px" style="margin:0px; padding:0px;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" id="navigation1" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" id="navigation2" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" id="navigation3" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" id="navigation4" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" id="navigation5" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" id="navigation6" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>	
		</c:if></tr></table>
	<div class="spn">&nbsp;</div>
	</div> 
<div id="Layer1" style="width:100%" onkeydown="changeStatus();">  
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
 <%@ include file="/WEB-INF/pages/trans/accountLineFormsHeader.jsp"%>
	</div>
<div class="bottom-header" style="margin-top:26px;!margin-top:45px;"><span></span></div>
</div>
</div>
	<div id="Layer3" style="width:100%;margin-bottom:2px;!margin-bottom:0px; padding:0px;" >  
<div id="newmnav" style="float: left;">
		  <ul>
		    <li><a class=""><span>Line Details</span></a></li>		    
		  </ul>
		</div>		
		<table cellpadding="0" cellspacing="0" height="22px" style="margin:0px; padding:0px;margin-bottom:0px;float:none;"><tr>				
		<c:if test="${not empty accountLine.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${accountLine.id> minAccountLine}" >
  		<a><img align="middle" onclick="settSelectedAccountId(${accountLine.id});setReturnString('gototab.movePrevAccountLine');return autoSaveFunc('none');" onmouseover="return chkSelect('2');" alt="Previous" title="Previous"  id="navigation7" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${accountLine.id == minAccountLine}" >
  		<a><img align="middle" id="navigation8"  src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<td width="20px" align="left">
  		<c:if test="${accountLine.id < maxAccountLine}" >
  		<a><img align="middle" onclick="settSelectedAccountId(${accountLine.id});setReturnString('gototab.moveNextAccountLine');return autoSaveFunc('none');" onmouseover="return chkSelect('2');"  alt="Next" title="Next" id="navigation9"  src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${accountLine.id == maxAccountLine}" >
  		<a><img align="middle" id="navigation10" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>

		<c:if test="${countAccountLine != 1}" >
		<td width="20px" align="left" style="!padding-top:1px;">
		<a><img class="openpopup" onclick="findCustomerOtherSOChild(this);"  id="navigation11" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="SO AccountLine List" title="Customer SO List" /></a> 
		</td> 
		</c:if>
		<c:if test="${countAccountLine == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
		<a><img align="middle" id="navigation12" src="images/navdisable_05.png"/></a>
  		</td> 
  		</c:if>
		</c:if></tr></table>
		 <c:if test="${serviceOrder.vip}">
					<table cellpadding="0" cellspacing="0" style="margin-top:-40px;!margin-bottom:0px; padding:0px;float:right;"><tr>
					<td><img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
					</td>
					</tr>
					</table>
				</c:if> 
		  </div>
		  <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-top:0px;" >
		<tbody>
			<tr>
				<td >
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					<tbody>
						<tr><td height="5px"></td></tr>
									<tr>
										<td align="right" class="listwhitetext" width="90px"><fmt:message
											key="accountLine.categoryType" /><font color="red" size="2">*</font></td>
										<td align="left">
										
										<configByCorp:customDropDown 	listType="map" list="${category}" fieldValue="${accountLine.category}"
																     attribute="id=category class=list-menu name=accountLine.category  style=width:135px  headerKey='' headerValue='' onchange=changeStatus(),setCategoryType(),findRadioValue(),disableAmountRevenue('edit') onclick=checkCategoryType(),checkDriverId(this); tabindex='1' "/></td>
                       								<td width="15px"></td>
											<td width="3px"></td>
											<td align="right" class="listwhitebox" valign="middle"><fmt:message key='accountLine.status'/></td>
										<td align="left" ><s:checkbox name="accountLine.status" value="${accountLine.status}" fieldValue="true" disabled="disabled" onclick="return activeStatusCheck(this)"onchange="changeStatus()" tabindex="2"/></td>
									    <td width=""></td>
										<td align="right" class="listwhitebox" valign="middle"><fmt:message key='accountLine.accountLineNumberDetail'/></td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.accountLineNumber" readonly="false" cssStyle="width:49px;" maxlength="3" onkeydown="return onlyNumberAllowed(event);" onblur="chkSelect('1');" onchange="checkAccountLineNumber();" tabindex="3"/></td>
										<td style="min-width:30px;">
										<c:if test="${serviceOrder.job =='HVY'}">
										<table style="margin:0px;padding:0px;" cellpadding="0">
										<tr>
										<td align="right" class="listwhitebox" valign="middle"  width="40px" >Truck&nbsp;</td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.truckNumber" onkeydown="return onlyDel(event,this)" readonly="true" size="5" /></td>
										 <td align="left"><img class="openpopup" width="17" height="20" onclick="findTruckNumber();" id="openpopup1.img"  src="<c:url value='/images/open-popup.gif'/>" /></td> 
										</tr>
										</table>
										</c:if>
										</td>
										<c:if  test="${compDivFlag == 'Yes'}">
										<td>										
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
										<tr>
										<td class="listwhitebox" id="keyForInternalCost1">&nbsp;Company Division&nbsp;<font color="red" size="2">*</font> <s:select  name="accountLine.companyDivision"   cssClass="list-menu" cssStyle="width:60px;" headerKey="" headerValue="" onchange="setCurrencyCompanyDivision();setCompanyDivision();changeStatus();findPayVatWithVatGroup(); findrecVATDesc();" onclick="checkCompanyDivision();" list= "%{companyDivis}"  required="true" tabindex="4"/>
	                                    </td>
	                                    </tr>
	                                    </table>	                                    
	                                    </td>
	                                    </c:if>
	                                    <c:if  test="${compDivFlag != 'Yes'}">
										<s:hidden name="accountLine.companyDivision"/>							
										</c:if>	
	                                    <c:if test="${accountInterface!='Y'}">
										   <td align="right" class="listwhitetext"  width="79px"><fmt:message key="accountLine.chargeCode"  />&nbsp;<c:if test="${checkContractChargesMandatory=='1'}"><font color="red" size="2">*</font></c:if></td>
										   <td align="left" class="listwhitetext">
                                           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 || fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}">
										      <s:textfield cssClass="input-text" key="accountLine.chargeCode"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','A');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();myFunctionGL();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="exchangeContractAfterChargeChange();checkChargeCode();changeStatus(); driverCommission();" onkeydown="return noQuotes(event);" tabindex="9"/>
										   </c:if>
										   <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0 && fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)<0}">
										      <s:textfield cssClass="input-text" key="accountLine.chargeCode"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','B');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="exchangeContractAfterChargeChange();checkChargeCode();changeStatus(); driverCommission();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9" />
										   </c:if>
										   <div id="ChargeNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
										   </td>
										   <td align="left">&nbsp;<img class="openpopup" width="17" height="20" onclick="chk(),findInternalCostVendorCode('2');"  id="openpopup5.img" src="<c:url value='/images/open-popup.gif'/>" /></td> 
									   </c:if>
									   <c:if test="${accountInterface=='Y'}">
									   <c:if test="${!utsiRecAccDateFlag && !utsiPayAccDateFlag}"> 
									   <c:if test="${( (accNonEditFlag && (accountLine.recInvoiceNumber=='' || empty accountLine.recInvoiceNumber )) || (!accNonEditFlag && empty accountLine.recAccDate)) && empty accountLine.payAccDate}">
									       <td align="right" class="listwhitetext"  width="79px"><fmt:message key="accountLine.chargeCode"  />&nbsp;<c:if test="${checkContractChargesMandatory=='1'}"><font color="red" size="2">*</font></c:if></td>
										   <td align="left" class="listwhitetext">
                                           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 || fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','C');" onblur="exchangeContractAfterChargeChange();changeStatus();findRadioValue();fillRevQtyLocalAmounts();myFunctionGL();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode(); driverCommission();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9"/>
										   </c:if>
										   <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0 && fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)<0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','D');" onblur="exchangeContractAfterChargeChange();changeStatus();findRadioValue();fillRevQtyLocalAmounts();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode(); driverCommission();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9"/>
										   </c:if>
										   <div id="ChargeNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
										   </td>
										   <td align="left">&nbsp;<img class="openpopup" width="17" height="20" onclick="chk(),findInternalCostVendorCode('2');"  id="openpopup6.img" src="<c:url value='/images/open-popup.gif'/>" /></td> 
									   </c:if>
									   <c:if test="${(accNonEditFlag  && (accountLine.recInvoiceNumber!='' && not empty accountLine.recInvoiceNumber ))  || (!accNonEditFlag && not empty accountLine.recAccDate )|| not empty accountLine.payAccDate}">
									     <td align="right" class="listwhitetext"  width="79px"><fmt:message key="accountLine.chargeCode"  />&nbsp;</td>
										   <td align="left" class="listwhitetext">
                                           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 || fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode" readonly="true" size="17" maxlength="25" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','E');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();myFunctionGL();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode(); driverCommission();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9" />
										   </c:if>
										   <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0 && fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)<0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode" readonly="true"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','F');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode(); driverCommission();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9" />
										   </c:if>
										   <div id="ChargeNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
										   </td>
										   <c:if test="${(!accNonEditFlag)}">
										   <td align="left" width="26">&nbsp;<img align="top" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as Sent To Acc has been already filled')" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" />  </td> 
									      </c:if>
									      <c:if test="${(accNonEditFlag)}">
										   <td align="left" width="26">&nbsp;<img align="top" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as invoice has been generated. ')" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" />  </td> 
									      </c:if>
									   </c:if> 
									   </c:if>
									   <c:if test="${utsiRecAccDateFlag || utsiPayAccDateFlag}">
									   <td align="right" class="listwhitetext"  width="79px"><fmt:message key="accountLine.chargeCode"  />&nbsp;</td>
										   <td align="left" class="listwhitetext">
                                           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 || fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode" readonly="true" size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','G');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();myFunctionGL();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="9" />
										   </c:if>
										   <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0 && fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)<0}">
										     <s:textfield cssClass="input-text" key="accountLine.chargeCode" readonly="true"  size="17" maxlength="25" id="chargeCode" onkeyup="autoCompleterAjaxCallChargeCode('ChargeNameDivId','H');" onblur="changeStatus();findRadioValue();fillRevQtyLocalAmounts();setLHF();checkLHFValue();setVatExcludeNew();driverCommission();" onchange="changeStatus();checkChargeCode();findRollUpInvoiceValFromChargeAjax();" onkeydown="return noQuotes(event);" tabindex="17" />
										   </c:if>
										   </td>
										   <td align="left">&nbsp;<img align="top" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Charge Code as sent to Accounting and/or already Invoiced in UTSI Instance')" id="openpopup7.img" src="<c:url value='/images/open-popup.gif'/>" />  </td> 
									   
									   </c:if> 
									   </c:if>
									   <c:if test="${systemDefaultVatCalculation=='true'}">
									   <td class="listwhitetext" width="107" align="right">Revenue&nbsp;VAT&nbsp;Desc&nbsp;</td>
									   <td class="listwhitetext" width="88px" >
									   
									   <configByCorp:customDropDown 	 listType="map" list="${euVatList}"   fieldValue="${accountLine.recVatDescr}"
																     attribute="id=euVatList class=list-menu name=accountLine.recVatDescr style=width:80px  headerKey='' headerValue=''  tabindex='18' onchange=getVatPercent();changeStatus();checkVatForReadonlyFields('first') onclick='' onfocus='enablePreviousFunction() tabindex='10'; "/>
									   <td class="listwhitetext" align="right" >VAT&nbsp;%&nbsp;</td>
									   
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.recVatPercent" id="accountLine.recVatPercent" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloat(this);" maxLength="6" /></td>
										
									   </c:if>
									   <%-- <c:if test="${serviceOrder.vip}">
					<div style="position:absolute;top:0px;margin-left:882px;!margin-left:856px;">
						<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
					</div>
				</c:if> --%>
										</tr>
										</tbody>
										</table>
										<table class="detailTabLabel"border="0">
										</tr>
										</tbody>
										<tbody>
										<tr>
										<tbody>
										<tr>
										<c:if test="${accountInterface!='Y'}">
										<td align="right" class="listwhitetext" width="83">Vendor Code</td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="accountLinevendorCodeId" key="accountLine.vendorCode" readonly="false" cssStyle="width:80px;" maxlength="10" onchange="checkPurchaseInvoiceProcessing(this,'VEN');driverCommission();findPayVatWithVatGroup();" onblur="changeStatus();driverCommission();" tabindex="5"/></td>
									    <td align="left"width="36"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen()" id="openpopup2.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                        </c:if>
                                        <c:if test="${accountInterface=='Y'}">
                                        <c:if test="${empty accountLine.payAccDate}">
                                        <td align="right" class="listwhitetext" width="83">Vendor Code</td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="accountLinevendorCodeId" key="accountLine.vendorCode" readonly="false" cssStyle="width:80px;" maxlength="10"  onchange="checkPurchaseInvoiceProcessing(this,'VEN');driverCommission();findPayVatWithVatGroup();" onblur="changeStatus();driverCommission();" tabindex="5"/></td>
									    <td align="left"width="36"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpenForActgCode();" id="openpopup3.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
                                        </c:if>
                                        <c:if test="${not empty accountLine.payAccDate}">
                                        <td align="right" class="listwhitetext" width="83">Vendor Code</td>
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="accountLinevendorCodeId" key="accountLine.vendorCode" readonly="true" cssStyle="width:80px;" maxlength="10" tabindex="5"/></td>
									    <td align="left"width="36"><img align="top" class="openpopup" width="17" height="20" onclick="javascript:alert('You can not change the Partner Code as Sent To Acc has been already filled')" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td> 
									    </c:if>
                                        </c:if> 
                                        <td align="right" class="listwhitetext" width="51" ><fmt:message key="accountLine.estimateVendorName" /></td>
									    <c:if test="${accountInterface!='Y'}">
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" id="accountestimateVendorNameId" key="accountLine.estimateVendorName" readonly="false" size="31" onchange="checkPurchaseInvoiceProcessing(this,'VEN');" onkeyup="findPartnerDetails('accountestimateVendorNameId','accountLinevendorCodeId','accountestimateVendorDiv','and ((isAccount=true or isAgent=true or isVendor=true or isCarrier=true or isOwnerOp=true or isPrivateParty=true))','',event);" tabindex="7"/><img align="top" class="openpopup" width="17" height="20"   onclick="findAgent(this,'BA');" src="<c:url value='/images/address2.png'/>" />&nbsp;</td>
										</c:if>
										<c:if test="${accountInterface=='Y'}">
                                        <c:if test="${empty accountLine.payAccDate}">
                                        <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" id="accountestimateVendorNameId" key="accountLine.estimateVendorName" readonly="false" size="31" onchange="checkPurchaseInvoiceProcessing(this,'VEN');" onkeyup="findPartnerDetails('accountestimateVendorNameId','accountLinevendorCodeId','accountestimateVendorDiv','and ((isAccount=true or isAgent=true or isVendor=true or isCarrier=true or isOwnerOp=true or isPrivateParty=true))','',event);" tabindex="7"/><img align="top" class="openpopup" width="17" height="20"   onclick="findAgent(this,'BA');" src="<c:url value='/images/address2.png'/>" />&nbsp;</td>
                                        </c:if>
                                        <c:if test="${not empty accountLine.payAccDate}">
                                        <td align="left" class="listwhitetext"><s:textfield	cssClass="input-text" id="accountestimateVendorNameId" key="accountLine.estimateVendorName" readonly="true" size="31" tabindex="7"/><img align="top" class="openpopup" width="17" height="20"   onclick="findAgent(this,'BA');" src="<c:url value='/images/address2.png'/>" />&nbsp;</td>
                                        </c:if>
                                        </c:if>
										<div id="accountestimateVendorDiv" class="autocomplete" style="z-index:9999;position:absolute;left:10%;margin-top:25px;"></div>
										<c:if test="${accountInterface=='Y'}">
										<td align="right" class="listwhitetext" width="66"><fmt:message key="accountLine.actgCode" /></td>
									    <td align="left" class="listwhitetext"><s:textfield	cssClass="input-textUpper" key="accountLine.actgCode" readonly="true" size="6" cssStyle="width:48px;" tabindex="8" /></td>
										</c:if>
										<c:if test="${accountInterface!='Y'}">
										<td width="198"></td><td></td>
										</c:if>
										
										<td align="right" class="listwhitetext" width="66"><fmt:message
																	key="accountLine.basis" /></td>
																	<c:if test="${accountInterface=='Y'}">
																	<c:if test="${empty accountLine.accrueRevenue &&  empty accountLine.accruePayable}">
																     <td align="left"><configByCorp:customDropDown 	listType="map" list="${basis}" fieldValue="${accountLine.basis}"
																     attribute="id=basis class=list-menu name=accountLine.basis style=width:81px  headerKey='' headerValue='' onchange=accrualQuantitycheck('2'),changeStatus() tabindex='13' "/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accrueRevenue || not empty accountLine.accruePayable}">
																	<td align="left"><configByCorp:customDropDown listType="map" 	list="${basis}" fieldValue="${accountLine.basis}" 
																	attribute="id=basis class=list-menu name=accountLine.basis style=width:75px
																	headerKey='' headerValue='' onchange=changeStatus(),accrualQuantitycheck('2'); tabindex='13'" 
																	 /></td>
																	</c:if>
																	</c:if>
																	<c:if test="${accountInterface!='Y'}">
																	<td align="left" width=""><configByCorp:customDropDown listType="map" 	list="${basis}" fieldValue="${accountLine.basis}" 
																	attribute="id=basis class=list-menu name=accountLine.basis style=width:81px 
																	headerKey='' headerValue='' onchange=changeStatus(),expense('accountLine.basis'); tabindex='13'" /></td>
																	</c:if>
										<td style="min-width:23px;">							
										<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
										<table style="margin:0px;padding:0px;" cellpadding="0">
										<tr>
										<td align="right" class="listwhitetext" width="40">Division<font color="red" size="2">*</font> </td>																	
									   <td align="left" width="">
										<c:if test="${empty accountLine.recAccDate || accountLine.recAccDate == null || accountLine.recAccDate =='null'}">
									   <s:select cssClass="list-menu" list="%{division}" name="accountLine.division" cssStyle="width:65px" />
									   </c:if>
										<c:if test="${not empty accountLine.recAccDate && accountLine.recAccDate != null && accountLine.recAccDate !='null'}">
									   <s:select cssClass="list-menu" list="%{division}" name="accountLine.division" cssStyle="width:65px" disabled="true"/>
									   </c:if>									   
									   </td>	
									   </tr>
									   </table>									
										</configByCorp:fieldVisibility>
										</td>
									   <c:if test="${systemDefaultVatCalculation=='true'}">
									   <td class="listwhitetext" width="" align="right">Expense&nbsp;VAT&nbsp;Desc</td>
									   <td class="listwhitetext" width="83px" >
									   
									   
									   
									   <configByCorp:customDropDown listType="map" list="${payVatList}"   fieldValue="${accountLine.payVatDescr}"
																     attribute="id=paystatus class=list-menu name=accountLine.payVatDescr style=width:80px  headerKey='' headerValue=''   onchange=getPayPercent(),checkVatForReadonlyFields('second'),changeStatus(); tabindex='47'"/>
									     <td class="listwhitetext" align="right" >VAT&nbsp;%</td>
									   <td width="100px" align="left" class="listwhitetext">
										<s:textfield cssClass="input-text"  cssStyle="text-align:right;" key="accountLine.payVatPercent" id="accountLine.payVatPercent" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="return checkFloatPayVat(this);" maxLength="6" tabindex="48" />
										</td>
									   </c:if>
										
									   </tr>
									   
									   <s:hidden name="accountLine.branchCode"/>
									    <s:hidden name="accountLine.branchName"/> 
							         </tbody>
							</table>
						<tr>
	                     <td height="10" align="left" class="listwhitetext">
<div  onClick="javascript:animatedcollapse.toggle('costing')" style="margin:0px;">
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Costing Details
</td> 
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td> 
<td class="headtab_right">
</td>
</tr>
</table> 
	</div>
	
	<div id="costing" class="switchgroup1">
						<table class="detailTabLabel" cellpadding ="0" cellspacing="0" border="0" width="100%">
                         <tbody>
	 								<tr>
												<td colspan="10">
												<table class="detailTabLabel" cellpadding ="0" cellspacing="0" border="0" width="100%" >
												<tbody>
												<tr>
												<td class="subcontenttabChild" style="height:26px;">
												<table style="margin:0px;padding:0px;" cellpadding ="0" cellspacing="0" width="100%">
												<tr>
												<td style="width:87px;!width:100px;" align="left" colspan=""><fmt:message key="accountLine.EntitlementDetail" /></td>
												<td width="280px">
												<div style="position: absolute; margin-left:0%;!margin-left:-40px; margin-top:-11px;!margin-top:-14px;">
												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Compute" style="width:65px" name="accountLine.compute1" onclick="findRevisedEntitleQuantitys();">
												</div>
												</td>
												<td width="61%" align="right"><font color="black" size="2">Expense</font></td>
												<td  width="46px" align="right"><font color="black" size="2">Pass%</font></td>
												<td width="55px" align="right"><font color="black" size="2">Revenue</font></td>
												</tr>												
												</table>
												</td>	
												</tr></tbody>
												</table>
												<table class="detailTabLabel" border="0" width="100%">
												<tbody> 
												<tr>
													<td align="right" class="listwhitetext"></td>
													<td align="left" class="listwhitetext"></td>
													<td align="left"></td>
													<td align="right" class="listwhitetext"></td>
													<td align="left" class="listwhitetext"></td>
													<td width="75px"></td>
													<td width="75px"></td>
													<td width="75px"></td>
													<td width="1000px"></td>
													<td valign="middle" rowspan="30" class="vertlinedata_vert"></td>
													<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.entitlementAmount" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"onchange="validNumberEntitle();" tabindex="10"/></td>
													<td valign="middle" rowspan="30" class="vertlinedata_vert"></td>
													<td></td>
													<td valign="middle" rowspan="30" class="vertlinedata_vert"></td>
													<c:if test="${empty accountLine.id}">
													<td width="80px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty accountLine.id}">
														<c:choose>
															<c:when test="${countEntitleDetailOrderNotes == '0' || countEntitleDetailOrderNotes == '' || countEntitleDetailOrderNotes == null}">
															<td width="80px" align="right"><img id="countEntitleDetailOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',790,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',790,580);" ></a></td>
															</c:when>
															<c:otherwise>
															<td width="80px" align="right"><img id="countEntitleDetailOrderNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',790,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EntitleDetail&imageId=countEntitleDetailOrderNotesImage&fieldId=countEntitleDetailOrderNotes&decorator=popup&popup=true',790,580);" ></a></td>
															</c:otherwise>
														</c:choose> 
														</c:if>
													</tr> 
													
												<tr>
                   							    <td colspan="15" align="left" class="listwhitetext">													        
												<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
												<tr>
												<td class="headtab_left">
												</td>
												<td class="headtab_center" >&nbsp;<fmt:message key="accountLine.EstimateDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="position:absolute;margin-top:-18px;!margin-top:-5px;margin-left:14%;"><input type="button" value="Compute" style="width:65px;vertical-align:middle;" name="accountLine.compute2"  onclick="findRevisedEstimateQuantitys();"></div>
												</td> 
												<td width="28" valign="top" class="headtab_bg"></td>
												<td class="headtab_bg_center" style="width:55%;">&nbsp;
												</td> 
												<td class="headtab_right">
												</td>
												</tr>
												</table> 
												</td>
												</tr>	
													
												<!--<tr>
												<td class="subcontenttabChild" width="700px" height="30px" align="left" colspan="15" background="images/greyband.gif"><fmt:message key="accountLine.EstimateDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" value="Compute" style="width:65px" name="accountLine.compute2"  onclick="findRevisedEstimateQuantitys();"></td>
												</tr>
												--><tr> 		
												<td width="2"></td>
												<td align="right" width="12%" class="listwhitetext">Vendor&nbsp;Reference</td>
												                 <c:if test="${accountLine.externalIntegrationReference!='PP'}">												       
																<td align="left" style="width:100px;"  class="listwhitetext"><s:textfield cssClass="input-text" key="accountLine.reference" size="20" maxlength="20" tabindex="11"/></td>
																</c:if>
																 <c:if test="${accountLine.externalIntegrationReference=='PP'}">															
																<td align="left" style="width:100px;" class="listwhitetext"><s:textfield cssClass="input-textUpper" key="accountLine.reference" size="20" maxlength="20" tabindex="11" readonly="true" /></td>
																</c:if>																 
																	 
																	<td colspan="5" style="padding-left:5px;">
																	<table style="margin:0px;padding:0px;width:100%;">
																	<tr>
																	<td align="right"  width="100" class="listwhitetext"><fmt:message key="accountLine.estimateDate" /></td>
																	<c:if test="${not empty accountLine.estimateDate}"> 
					                                                 <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.estimateDate"/></s:text>
				                                                       <td width="50" align="left"><s:textfield id="estimateDate" name="accountLine.estimateDate" value="%{accountLineFormattedValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6" /></td>
				                                                   </c:if>
				                                                   <c:if test="${empty accountLine.estimateDate}">
					                                               <td width="50" align="left"><s:textfield id="estimateDate" name="accountLine.estimateDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:57px;" size="6" tabindex="100"/></td>
				                                                   </c:if>
				                                                   <td align="left"><img id="estimateDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				                                                   
				                                                   <c:if test="${not empty accountLine.externalIntegrationReference}">
				                                                     <td align="left" style="padding-left:15px;">External&nbsp;Source:&nbsp;<c:if test="${accountLine.externalIntegrationReference=='PP'}">Price Point</c:if><c:if test="${accountLine.externalIntegrationReference!='PP'}"><c:out value="${accountLine.externalIntegrationReference}"></c:out></c:if></td>
				                                                   	</c:if>				                                                   	
				                                                   </tr>	
				                                                   </table>	
				                                                   </td>	
				                                                   <s:hidden name="accountLine.externalIntegrationReference" />
				                                                   	
																<c:set var="isIncludeFlag" value="false"/>
								                                <c:if test="${accountLine.includeLHF}">
									                            <c:set var="isIncludeFlag" value="true"/>
								                                </c:if> 
						                                        <td align="right" class="listwhitetext" colspan="1"><configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">Include in LHF</configByCorp:fieldVisibility>
				      	                                        <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman"><s:checkbox key="accountLine.includeLHF" value="${isIncludeFlag}"  fieldValue="true" cssStyle="vertical-align:middle;" onclick="changeStatus()" tabindex="12"/></configByCorp:fieldVisibility></td>	
                                                                <td></td>
																<td></td> 										
																<td align="right">
																    <c:if test="${empty accountLine.id}">
																	  <img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/>
																    </c:if>
																	<c:if test="${not empty accountLine.id}">
																	<c:choose>
																	<c:when test="${countEstimateDetailNotes == '0' || countEstimateDetailNotes == '' || countEstimateDetailNotes == null}">
																	<img id="countEstimateDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EstimateDetail&imageId=countEstimateDetailNotesImage&fieldId=countEstimateDetailNotesNotes&decorator=popup&popup=true',790,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',790,580);" ></a>
																	</c:when>
																	<c:otherwise>
																	<img id="countEstimateDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=EstimateDetail&imageId=countEstimateDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',790,580);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=ReceivableDetail&imageId=countReceivableDetailNotesImage&fieldId=countEstimateDetailNotes&decorator=popup&popup=true',790,580);" ></a>
																	</c:otherwise>
																</c:choose> 
																</c:if>
																</td>
															</tr>
															
															<tr>
															<td colspan="9">
															<table style="background-color:#E6F3F9;margin:0px;padding:0px;width:100%;" cellspacing="0" cellpadding="0">
															<tr>
												<td class="subcontenttabChild" width="700px" style="height:24px;" align="left" colspan="9" background="images/greyband.gif">Expense Currency Details &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												</tr>
															<c:if test="${contractType}"> 
															 <tr>
															 <td colspan="9">
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="1px"></td>
															 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															 <td align="left" colspan="2" >
															 <configByCorp:customDropDown listType="map" list="${country}"   fieldValue="${accountLine.estimatePayableContractCurrency}"
																     attribute="id=estimatePayableContractCurrency class=list-menu name=accountLine.estimatePayableContractCurrency style=width:60px  headerKey='' headerValue=''  tabindex=18 onchange=changeStatus();findEstimatePayableContractExchangeRate();"/>	</td>				 
															 
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.estimatePayableContractValueDate}"> 
					                                          <s:text id="accountLineFormattedEstimatePayableContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.estimatePayableContractValueDate"/></s:text>
				                                              <td ><s:textfield id="estimatePayableContractValueDate" name="accountLine.estimatePayableContractValueDate" value="%{accountLineFormattedEstimatePayableContractValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6"/>
				                                              <img id="estimatePayableContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.estimatePayableContractValueDate}">
					                                          <td><s:textfield id="estimatePayableContractValueDate" name="accountLine.estimatePayableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6"/>
					                                          <img id="estimatePayableContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePayableContractExchangeRate" size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateEstimateRateByContractRate('none');"  onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePayableContractRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateRateByContractRate('accountLine.estimatePayableContractRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.estimatePayableContractRateAmmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr>
															</c:if> 
															<tr>
															<td colspan="9">
															<table class="detailTabLabel" style="margin:0px;">
															<tr>															
															<td width="45px"></td>
														    <c:choose>
															<c:when test="${contractType}">
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">*</font></td>
															</c:when>
															<c:otherwise>
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">&nbsp;</font></td>
															</c:otherwise>
															</c:choose>
															<td align="left" colspan="2" > 
															<configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.estCurrency}"
																     attribute="id=estCurrency class=list-menu name=accountLine.estCurrency style=width:65px  headerKey='' headerValue=''  tabindex=12 onchange=changeStatus();findExchangeEstRate();"/>
															<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															<c:if test="${not empty accountLine.estValueDate}"> 
					                                          <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.estValueDate"/></s:text>
				                                              <td ><s:textfield id="estValueDate" name="accountLine.estValueDate" value="%{accountLineFormattedValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssStyle="width:65px;" cssClass="input-text" size="6"/>
				                                              <img id="estValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                           </c:if>
				                                           <c:if test="${empty accountLine.estValueDate}">
					                                          <td><s:textfield id="estValueDate" name="accountLine.estValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssStyle="width:65px;" cssClass="input-text" size="6"/>
					                                          <img id="estValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                            </c:if>
				                                            <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-text " cssStyle="text-align:right;width:57px;" key="accountLine.estExchangeRate"    size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateEstimateExpenseByExchangeRate();" onblur="chkSelect('1');"/></td>   			
				                                            <td align="right" class="listwhitetext" width="81px">Buy&nbsp;Rate</td>
														    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.estLocalRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateLocalAmount('form','accountLine.estLocalRate');" onblur="chkSelect('1');"/></td>	 
				                                            <td align="right" class="listwhitetext" width="85px">Curr&nbsp;Amount</td>
														    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.estLocalAmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateExpense('accountLine.estLocalAmount');" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr>
															 </table>
															</td>
															</tr>
														
															  
															<c:if test="${multiCurrency=='Y'}">															
															<tr>
												<td class="subcontenttabChild" width="700px" style="height:24px;" align="left" colspan="9" background="images/greyband.gif">Revenue Currency Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
												</tr>
															
															<c:if test="${contractType}">																											 
															 <tr>
															 <td colspan="9">															 											
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="1px"></td>
															 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															 <td align="left" colspan="2" >
															 <configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.estimateContractCurrency}"
																     attribute="id=estimateContractCurrency tabindex=12 class=list-menu name=accountLine.estimateContractCurrency style=width:65px  headerKey='' headerValue='' onchange=changeStatus();findEstimateContractCurrencExchangeyRate('accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');"/>	
																								 
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.estimateContractValueDate}"> 
					                                          <s:text id="accountLineFormattedEstimateContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.estimateContractValueDate"/></s:text>
				                                              <td ><s:textfield id="estimateContractValueDate" name="accountLine.estimateContractValueDate" value="%{accountLineFormattedEstimateContractValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:62px;" size="6"/>
				                                              <img id="estimateContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.estimateContractValueDate}">
					                                          <td><s:textfield id="estimateContractValueDate" name="accountLine.estimateContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:62px;" size="6"/>
					                                          <img id="estimateContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.estimateContractExchangeRate"  size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateEstimateSellRateByContractRate('none','','','');" onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.estimateContractRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateSellRateByContractRate('accountLine.estimateContractRate','accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:57px;" key="accountLine.estimateContractRateAmmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true"  onblur="chkSelect('1');"/></td>	 
															 </tr></table>															
															 </td>															
															 </tr>
															  
															</c:if> 
															
															 <tr>
															 <td colspan="9">															 
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="15px"></td>
															 <c:choose>
															 <c:when test="${contractType}">
															 <td align="right" class="listwhitetext" style="width: 79px;">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
															 </c:when>
															 <c:otherwise>
															 <td align="right" class="listwhitetext" style="width: 79px;">Billing&nbsp;Currency</td>
															 </c:otherwise>
															 </c:choose>
															 <td align="left" colspan="2" >
															 <configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.estSellCurrency}"
																     attribute="id=estSellCurrency tabindex=12 class=list-menu name=accountLine.estSellCurrency style=width:65px  headerKey='' headerValue='' onchange=changeStatus();findEstSellExchangeRate();"/>	
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.estSellValueDate}"> 
					                                          <s:text id="accountLineFormattedEstSellValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.estSellValueDate"/></s:text>
				                                              <td ><s:textfield id="estSellValueDate" name="accountLine.estSellValueDate" value="%{accountLineFormattedEstSellValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px;" size="6"/>
				                                              <img id="estSellValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.estSellValueDate}">
					                                          <td><s:textfield id="estSellValueDate" name="accountLine.estSellValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px;" size="6"/>
					                                          <img id="estSellValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width: 57px;" key="accountLine.estSellExchangeRate"  size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateEstimateRevenueByExchangerate();" onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Sell&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width: 57px;" key="accountLine.estSellLocalRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateSellLocalAmount('form','accountLine.estSellLocalRate','accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Curr&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;    width: 57px;" key="accountLine.estSellLocalAmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateEstimateRevenue('accountLine.estSellLocalAmount','accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
															 </tr></table>															  
															 </td></tr>
															 
															  <tr><td colspan="9"  class="vertlinedata"></td></tr>
															</c:if> 
															 
															 <tr><!-- jitendra -->
															 <td colspan="9">
															 <table border="0" style="margin:0px;padding:0px;">
															<tr> 
																	<!--<td align="right" class="listwhitetext" width="89"></td>
															 <td width="4%">&nbsp;</td> -->
																<c:if test="${!contractType}">																
																<td align="left" class="listwhitetext" width="18%"></td>	
																<td align="left" class="listwhitetext" width="5"></td>															
																</c:if>
																<td align="right" style="width:110px;" class="listwhitetext" colspan="1"><fmt:message key="accountLine.estimateQuantity" /></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;margin-left: 1px;" key="accountLine.estimateQuantity" size="5" maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="14"  onchange="expenseOld('all','accountLine.estimateQuantity','','','');" onblur="chkSelect('1');"/></td>
																<c:if test="${!contractType}">																
																<td align="left" class="listwhitetext" style="width:14px;"></td>																														
																</c:if>
																<td align="right" style="width:45px;" class="listwhitetext"><fmt:message key="accountLine.estimateRate" /></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right; width:57px;" key="accountLine.estimateRate" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="expenseOld('exp','accountLine.estimateRate','','','');" tabindex="15"/></td>
																<c:if test="${contractType}">
																<td align="right"style="width:81px;" class="listwhitetext"><fmt:message key="accountLine.estimateQuantity" /></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57ssssspx;" key="accountLine.estimateSellQuantity" size="5" maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" tabindex="14"  onchange="expenseOld('all','accountLine.estimateSellQuantity','','','');" onblur="chkSelect('1');"/></td>
																</c:if>																
																 <td align="right" class="listwhitetext" style="width:82px;" ><fmt:message key="accountLine.estimateSellRate" /></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.estimateSellRate" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="expenseOld('rev','accountLine.estimateSellRate','accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');" tabindex="16"/></td>
															</tr>
															</table>
															</td>														
																
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateExpense" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="expenseOld('exp','accountLine.estimateExpense','','','');" tabindex="16"/></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimatePassPercentage" size="1" maxlength="4" onkeydown="return onlyNumsAllowed(event)" onchange="calculateRevenue();" tabindex="16"/></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateRevenueAmount"  size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="mockupForRevenue('accountLine.estimateRevenueAmount','accountLine.estSellCurrency~accountLine.estimateContractCurrency','accountLine.estSellValueDate~accountLine.estimateContractValueDate','accountLine.estSellExchangeRate~accountLine.estimateContractExchangeRate');" onblur="chkSelect('1');" tabindex="16" /></td>
															</tr>
															<tr >
															<td align="right" class="listwhitetext" colspan="4">&nbsp;</td>
															<td colspan="5" align="right">
															<div id="estimateDeviationShow" > 
															<table class="detailTabLabel"><tr> 
																<td align="right" class="listwhitetext" width="72">Buy&nbsp;Deviation</td>
																<td align="left" class="listwhitetext" width="90"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateDeviation" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="changeBuyDeviation();" tabindex="17" />%</td>
																 <s:hidden name="oldEstimateDeviation" value="${accountLine.estimateDeviation}"/>
																 <td align="right" class="listwhitetext">Sell&nbsp;Deviation</td>
																 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.estimateSellDeviation" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="changeSellDeviation();" tabindex="17"/>%</td>
																  <s:hidden name="oldEstimateSellDeviation"  value="${accountLine.estimateSellDeviation}" />
																</tr></table></div></td> 
															</tr> 
															<c:if test="${systemDefaultVatCalculation=='true'}">
															<tr>
															<td align="right" class="listwhitetext"></td>
														    <td align="left"  ></td>
															<td class="listwhitetext" width="48px" align="left"><!--<input type="button" class="cssbuttonA" style="width:65px; height:20px" id="estVATCalc" name="estVATCalc" value="VAT Calc" onclick="VATCalculateEst(),checkVatForReadonlyFieldsEst('first');"  disabled="disabled" />--></td>
															<td align="left" class="listwhitetext" colspan="6">
															<table class="detailTabLabel" cellpadding="0" cellspacing="0">
															<tr>
															<td class="listwhitetext" width="32px" >&nbsp;</td>
															<td class="listwhitetext">&nbsp;&nbsp;</td>
															<td class="listwhitetext">&nbsp;</td>
															<td>
															
															</td> 
															<td width="43px" align="right" class="listwhitetext" >&nbsp;</td>
                        									<td align="left" class="listwhitetext"></td>
															</tr>
															</table> 
															</td>
															
                        									
                        									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.estExpVatAmt" id="accountLine.estExpVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td>
                        									<td align="left" class="listwhitetext">VAT</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.estVatAmt" id="accountLine.estVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
															</tr></c:if> 
															<c:choose>
															<c:when test="${(serviceOrder.grpStatus=='Finalized' || serviceOrder.grpStatus=='finalized')  && (serviceOrder.controlFlag=='G')  && (accountLine.status)}"> 
															<tr>
															<td align="right" colspan="9" style="padding-right:60px;"><input type="button" class="cssbuttonA"   style="width:155px; height:25px" name="distributeUnderEstimate" value="Distribute groupage cost" onclick="distributeOrdersAmount('UnderEstimate');"></td>
															</tr>
															</c:when>
															<c:otherwise>
															<tr></tr>
															</c:otherwise>
															</c:choose>
															
															<tr>
															<td colspan="9">
															<table style="margin:0px;padding:0px;">
															<tr>
															<td align="right" class="listwhitetext" width="14%" style="width:98px;"><fmt:message key="accountLine.recNote" /></td>
															<td align="left" class="listwhitetext"><s:textarea  name="accountLine.quoteDescription" cssStyle="height:35px;width: 621px;" cols="111"  cssClass="textarea" tabindex="17"/></td>
															</tr>
															</table>
															</td><!--
															<tr> 
																<td class="subcontenttabChild" height="30px" align="left" colspan="15" background="images/greyband.gif"><fmt:message key="accountLine.RevisionDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input type="button" value="Compute" onclick="findRevisedQuantitys();" style="width:65px" name="accountLine.compute3"></td>
															 </tr>
															 --><tr>
				                   							    <td colspan="15" align="left" class="listwhitetext">													        
																<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
																<tr>
																<td class="headtab_left">
																</td>
																<td class="headtab_center" >&nbsp;<fmt:message key="accountLine.RevisionDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div style="position:absolute;margin-top:-18px;!margin-top:-5px;margin-left:14%;"><input type="button" value="Compute" onclick="findRevisedQuantitys();" style="width:65px;vertical-align:middle;" name="accountLine.compute3"></div>
																</td> 
																<td width="28" valign="top" class="headtab_bg"></td>
																<td class="headtab_bg_center" style="width:55%;">&nbsp;
																</td> 
																<td class="headtab_right">
																</td>
																</tr>
																</table> 
																</td>
															</tr>
															
																<tr>
															<td colspan="9">															
															<table style="background-color:#E6F3F9;margin:0px;padding:0px;width:100%;" cellspacing="0" cellpadding="0">
																  <tr>
															<td class="subcontenttabChild" width="700px" style="height:24px;" align="left" colspan="9" background="images/greyband.gif">Expense Currency Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															</tr>
																 
															<c:if test="${contractType}"> 
															
															 <tr>
															 <td colspan="9">
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="1px"></td>
															 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															 <td align="left" colspan="2" >
															 
															  <configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.revisionPayableContractCurrency}"
																     attribute="id=revisionPayableContractCurrency tabindex=12 class=list-menu name=accountLine.revisionPayableContractCurrency style=width:60px  headerKey='' headerValue='' onchange=changeStatus();findRevisionPayableContractCurrencyExchangeRate();"/>	
															
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.revisionPayableContractValueDate}"> 
					                                          <s:text id="accountLineFormattedRevisionPayableContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionPayableContractValueDate"/></s:text>
				                                              <td ><s:textfield id="revisionPayableContractValueDate" name="accountLine.revisionPayableContractValueDate" value="%{accountLineFormattedRevisionPayableContractValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6"/>
				                                              <img id="revisionPayableContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.revisionPayableContractValueDate}">
					                                          <td><s:textfield id="revisionPayableContractValueDate" name="accountLine.revisionPayableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6"/>
					                                          <img id="revisionPayableContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPayableContractExchangeRate" size="5" maxlength="10" tabindex="12"  onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRevisionRateByContractRate('none');" onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPayableContractRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionRateByContractRate('accountLine.revisionPayableContractRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionPayableContractRateAmmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr>
															 
															</c:if>
															
															 <tr>
															 <td colspan="9">
															<table class="detailTabLabel" border="0" style="margin:0px;">
															<tr>															
															<td width="45px"></td>
															<c:choose>
															<c:when test="${contractType}">
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">*</font></td>
															</c:when>
															<c:otherwise>
															<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">&nbsp;</font></td>
															</c:otherwise></c:choose>
															<td align="left" colspan="2" >
															<configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.revisionCurrency}"
																     attribute="id=revisionCurrency  tabindex='18' class=list-menu name=accountLine.revisionCurrency style=width:65px  headerKey='' headerValue=''  tabindex='18' onchange=changeStatus();findExchangeRevisionRate();"/>
															<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															<c:if test="${not empty accountLine.revisionValueDate}"> 
					                                          <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionValueDate"/></s:text>
				                                              <td ><s:textfield id="revisionValueDate" name="accountLine.revisionValueDate" value="%{accountLineFormattedValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" cssStyle="width:65px;" size="6"/>
				                                             <img id="revisionValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                             </td>
				                                           </c:if>
				                                           <c:if test="${empty accountLine.revisionValueDate}">
					                                          <td><s:textfield id="revisionValueDate" name="accountLine.revisionValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6" cssStyle="width:65px;" />
					                                         <img id="revisionValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                         </td>
				                                            </c:if>
				                                            <td align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
														    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionExchangeRate"  size="5" maxlength="10" tabindex="18" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRevisionExpenseByExchangerate('accountLine.revisionExchangeRate');" onblur="chkSelect('1');"/></td>   			
															<td align="right" class="listwhitetext" width="81px">Buy&nbsp;Rate</td>
														    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:56px;" key="accountLine.revisionLocalRate" size="5" maxlength="12" tabindex="18" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionLocalAmount('form','accountLine.revisionLocalRate');" onblur="chkSelect('1');"/></td>	 
															<td align="right" class="listwhitetext" width="85px">Curr&nbsp;Amount</td>
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionLocalAmount" size="5" maxlength="12"  tabindex="18" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionExpense('accountLine.revisionLocalAmount');" onblur="chkSelect('1');"/></td>	 
															 </tr>
															 </table></td></tr> 														
															 </table>
															 </td>
															 </tr>
															
															<c:if test="${multiCurrency=='Y'}">
															<tr>
															<td class="subcontenttabChild" width="700px" style="height:24px;" align="left" colspan="9" background="images/greyband.gif">Revenue Currency Details&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
															</tr>															
															<c:if test="${contractType}"> 
															 <tr>
															 <td colspan="9">
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="1px"></td>
															 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															 <td align="left" colspan="2" >
															 	<configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.revisionContractCurrency}"
																     attribute="id=revisionContractCurrency  tabindex=18 class=list-menu name=accountLine.revisionContractCurrency style=width:65px  headerKey='' headerValue=''  onchange=changeStatus();findRevisionContractCurrencyExchangeRate('accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');"/>
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.revisionContractValueDate}"> 
					                                          <s:text id="accountLineFormattedRevisionContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionContractValueDate"/></s:text>
				                                              <td ><s:textfield id="revisionContractValueDate" name="accountLine.revisionContractValueDate" value="%{accountLineFormattedRevisionContractValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6"  cssStyle="width: 62px;" />
				                                              <img id="revisionContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.revisionContractValueDate}">
					                                          <td><s:textfield id="revisionContractValueDate" name="accountLine.revisionContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6" cssStyle="width: 62px;"/>
					                                          <img id="revisionContractValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionContractExchangeRate" size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRevisionSellRateByContractRate('none','','','');" onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Contract&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:56px;" key="accountLine.revisionContractRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionSellRateByContractRate('accountLine.revisionContractRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right;width:56px;" key="accountLine.revisionContractRateAmmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" readonly="true" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr> 															  
															</c:if>
															
															 <tr>
															 <td colspan="9">
															 <table class="detailTabLabel" style="margin:0px;">
															 <tr>															
															 <td width="15px"></td>
															 <c:choose>
															 <c:when test="${contractType}">
															 <td align="right" class="listwhitetext" style="width:79px;">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
															 </c:when>
															 <c:otherwise>
															 <td align="right" style="width:79px;" lass="listwhitetext">Billing&nbsp;Currency</td>
															 </c:otherwise></c:choose>
															 <td align="left" colspan="2" >
															<configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.revisionSellCurrency}"
																     attribute="id=country class=list-menu name=accountLine.revisionSellCurrency style=width:65px  headerKey='' headerValue=''  tabindex='12' onchange=changeStatus();findRevisionSellExchangeRate();"/>													 
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.revisionSellValueDate}"> 
					                                          <s:text id="accountLineFormattedRevisionSellValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.revisionSellValueDate"/></s:text>
				                                              <td ><s:textfield id="revisionSellValueDate" name="accountLine.revisionSellValueDate" value="%{accountLineFormattedRevisionSellValueDate}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6" cssStyle="width:65px;" />
				                                              <img id="revisionSellValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.revisionSellValueDate}">
					                                          <td><s:textfield id="revisionSellValueDate" name="accountLine.revisionSellValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="6" cssStyle="width:65px;"/>
					                                          <img id="revisionSellValueDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </td>
				                                             </c:if>
				                                             <td  align="right" class="listwhitetext" width="39px">Ex.&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionSellExchangeRate"   size="5" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateRevisionRevenueContractByExchangerate();" onblur="chkSelect('1');"/></td>   			
				                                             <td align="right" class="listwhitetext" width="81px">Sell&nbsp;Rate</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionSellLocalRate" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionSellLocalAmount('form','accountLine.revisionSellLocalRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
				                                             <td align="right" class="listwhitetext" width="85px">Curr&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionSellLocalAmount" size="5" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)" onchange="calculateRevisionRevenueContract('accountLine.revisionSellLocalAmount','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" onblur="chkSelect('1');"/></td>	 
															 </tr></table></td></tr>
															  <tr><td colspan="9"  class="vertlinedata"></td></tr>
															</c:if>  
															 
															 
															<tr>
																
																<td colspan="9">
																<table style="margin:0px;padding:0px;">
																<tr>
																<td width="18"></td>
																<c:if test="${!contractType}">
																<td width="2"></td>
																<td align="right" class="listwhitetext" width="18%"></td>
																</c:if>
																<td align="right" class="listwhitetext" style="width:92px"><fmt:message key="accountLine.revisionQuantity" /></td>
																	<c:if test="${empty accountLine.accrueRevenue &&  empty accountLine.accruePayable}">
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;" name="accountLine.revisionQuantity"  maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" onchange="sameQuantityChange(),revisionExpanseOld('all','accountLine.revisionQuantity','','','');" onselect="accrualQuantitycheck('1');" tabindex="18" value="${accountLine.revisionQuantity}"/></td>
                                                                 </c:if>
                                                                 <c:if test="${not empty accountLine.accrueRevenue || not empty accountLine.accruePayable}">
                                                                 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="accountLine.revisionQuantity" size="5" maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" onchange="sameQuantityChange(),revisionExpanseOld('all','accountLine.revisionQuantity','','','');" tabindex="18" value="${accountLine.revisionQuantity}" readonly="true"/></td>
																	</c:if>
																	<c:if test="${!contractType}">
																<td align="right" class="listwhitetext" width="2%"></td>
																</c:if>
																<td align="right" class="listwhitetext" style="width:51px"><fmt:message key="accountLine.revisionRate" /></td>
																<c:if test="${empty accountLine.accruePayable}">	
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionRate"  maxlength="10" onselect="accrualQuantitycheck('1');" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="revisionExpanseOld('exp','accountLine.revisionRate','','','');" tabindex="19"/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accruePayable}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionRate" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="revisionExpanseOld('exp','accountLine.revisionRate','','','');" tabindex="19" readonly="true"/></td>
																	</c:if>
																	<c:if test="${contractType}">
																	<td align="right" class="listwhitetext" style="width:62px"><fmt:message key="accountLine.revisionQuantity" /></td>
																	<c:if test="${empty accountLine.accrueRevenue &&  empty accountLine.accruePayable}">
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:58px;" name="accountLine.revisionSellQuantity"  maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" onchange="sameQuantityChange(),revisionExpanseOld('all','accountLine.revisionSellQuantity','','','');" onselect="accrualQuantitycheck('1');" tabindex="18" value="${accountLine.revisionSellQuantity}"/></td>
                                                                 </c:if>
                                                                 <c:if test="${not empty accountLine.accrueRevenue || not empty accountLine.accruePayable}">
                                                                 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" name="accountLine.revisionSellQuantity" size="5" maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onblur="chkSelect('1');" onchange="sameQuantityChange(),revisionExpanseOld('all','accountLine.revisionSellQuantity','','','');" tabindex="18" value="${accountLine.revisionSellQuantity}" readonly="true"/></td>
																	</c:if>
																	</c:if>
																	 <td align="right" class="listwhitetext" style="width:80px"><fmt:message key="accountLine.revisionSellRate" /></td>
																	<c:if test="${empty accountLine.accrueRevenue}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:57px;" key="accountLine.revisionSellRate"  maxlength="10" onselect="accrualQuantitycheck('1');" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="revisionExpanseOld('rev','accountLine.revisionSellRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" tabindex="19"/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accrueRevenue}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right; width:57px;" key="accountLine.revisionSellRate"  maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="revisionExpanseOld('rev','accountLine.revisionSellRate','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" tabindex="19" readonly="true" /></td>
																  </c:if>																	
																	</tr>
																	</table>
																	</td>
																	
																	<c:if test="${empty accountLine.accruePayable}">
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionExpense" size="8" maxlength="15" onselect="accrualQuantitycheck('1');" onkeydown="return onlyFloatNumsAllowed(event)" onchange="revisionExpanseOld('exp','none','','','');" tabindex="20"/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accruePayable}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionExpense" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)" onchange="revisionExpanseOld('exp','none','','','');" readonly="true" tabindex="20"/></td>
																	</c:if>
																	<c:if test="${empty accountLine.accrueRevenue &&  empty accountLine.accruePayable}">
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPassPercentage" size="1" maxlength="4" onselect="accrualQuantitycheck('1');" onkeydown="return onlyNumsAllowed(event)" onchange="calculateRevisionRevenue();" tabindex="21"/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accrueRevenue || not empty accountLine.accruePayable}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionPassPercentage" size="1" maxlength="4" onkeydown="return onlyNumsAllowed(event)" onchange="calculateRevisionRevenue();" readonly="true" tabindex="21"/></td>
																	</c:if>
																	<c:if test="${empty accountLine.accrueRevenue}">
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionRevenueAmount" size="8" maxlength="15" onselect="accrualQuantitycheck('1');" onkeydown="return onlyFloatNumsAllowed(event)"onblur="chkSelect('1');" onchange="mockupForRevisionRevenue('accountLine.revisionRevenueAmount','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" tabindex="22"/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.accrueRevenue}">
																	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionRevenueAmount" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"onblur="chkSelect('1');" onchange="mockupForRevisionRevenue('accountLine.revisionRevenueAmount','accountLine.revisionSellCurrency~accountLine.revisionContractCurrency','accountLine.revisionSellValueDate~accountLine.revisionContractValueDate','accountLine.revisionSellExchangeRate~accountLine.revisionContractExchangeRate');" readonly="true" tabindex="22"/></td>
																	</c:if> 
															</tr>
															<tr > 
															<td align="right" class="listwhitetext" colspan="4">&nbsp;</td>
															<td colspan="5" align="right">
															<div id="revisionDeviationShow" > 
															<table class="detailTabLabel"><tr> 
																<td align="right" class="listwhitetext" width="72">Buy&nbsp;Deviation</td> 
																 <td align="left" class="listwhitetext" width="90"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionDeviation" size="5" maxlength="10" tabindex="22" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="changeRevisionBuyDeviation();" />%</td>
																 <s:hidden name="oldRevisionDeviation" value="${accountLine.revisionDeviation}"/>
																 <td align="right" class="listwhitetext">Sell&nbsp;Deviation</td>
																  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.revisionSellDeviation" size="5" maxlength="10" tabindex="22" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="changeRevisionSellDeviation();" />%</td>
																   <s:hidden name="oldRevisionSellDeviation"  value="${accountLine.revisionSellDeviation}" />
															</tr></table></div></td></tr> 
															<c:if test="${systemDefaultVatCalculation=='true'}">
															<tr>
															<td align="right" class="listwhitetext"></td>
														    <td align="left"  ></td>
															<td class="listwhitetext" width="48px" align="left"><!--<input type="button" class="cssbuttonA" style="width:65px; height:20px" id="revisionVATCalc" name="revisionVATCalc" value="VAT Calc" onclick="VATCalculateRevision(),checkVatForReadonlyFieldsEst('second');"  disabled="disabled" />--></td>
															<td align="left" class="listwhitetext" colspan="6">
															<table class="detailTabLabel" cellpadding="0" cellspacing="0">
															<tr>
															<td class="listwhitetext" width="32px" >&nbsp;</td>
															<td class="listwhitetext">&nbsp;&nbsp;</td>
															<td class="listwhitetext">&nbsp;</td>
															<td>
															
															</td> 
															<td width="43px" align="right" class="listwhitetext" >&nbsp;</td>
                        									<td align="left" class="listwhitetext"></td>
															</tr>
															</table> 
															</td> 
															
                        									       
                        									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionExpVatAmt" id="accountLine.revisionExpVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td>
                        									<td align="left" class="listwhitetext">VAT</td>  
															<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.revisionVatAmt" id="accountLine.revisionVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></td> 
															</tr></c:if> 
															<c:choose>
															<c:when test="${(serviceOrder.grpStatus=='Finalized' || serviceOrder.grpStatus=='finalized') && (serviceOrder.controlFlag=='G') && (accountLine.status)}"> 
															<tr>
															<td align="right" colspan="9" style="padding-right:60px;"><input type="button" class="cssbuttonA"  name="distributeUnderRevision" style="width:155px; height:25px" value="Distribute groupage cost"  onclick="distributeOrdersAmount('UnderRevision');" ></td>
															</tr>
															</c:when>
															<c:otherwise>
															<tr>
															</tr>
															</c:otherwise>
															</c:choose>
															</tbody></table>
														</tbody>
										</table>
										</div>
										</div>
										</td>
										</tr>
										<tr>
	                     <td height="10" align="left" class="listwhitetext">
	                      <c:if test="${accountInterface!='Y'}">  
	                      <div  onClick="checkchargeStarForRac('rec');" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Receivable&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>	                   
		                  </c:if>
		                  <c:if test="${accountInterface=='Y'}"> 
		                  <div  onClick="checkchargeForRac('rec')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Receivable&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td> 
<td class="headtab_right">
</td>
</tr>
</table>
</div>	                 
 </c:if>  
<div  id="rec" >  
<jsp:include flush="true" page="accountLineFormsSec.jsp"></jsp:include>
<sec-auth:authComponent componentId="module.accountLine.section.receivableDetailDate.edit">
	<%;
	String receivableDetailDate="true";
	int permissionDate  = (Integer)request.getAttribute("module.accountLine.section.receivableDetailDate.edit" + "Permission");
 	if (permissionDate > 2 ){
 		receivableDetailDate = "false";
 		
 	}
 	
  %>
													<table width="100%" class="colored_bg" style="margin-bottom:0;border-top-left-radius:3px;border-top-right-radius:3px;" border="0">
													<tr style="">
													
													<configByCorp:fieldVisibility componentId="component.accountLine.section.paymentSent">
													<td align="right" height="20" width=""  style="width:211px;" class="listwhitetextAcct">
													Payment Sent
													</td>
													</configByCorp:fieldVisibility>
													<sec-auth:authComponent componentId="module.accountLine.section.adminfinance.edit">
								<%;
								String adminFinanceEdit="true";
								int permissionadminfinance  = (Integer)request.getAttribute("module.accountLine.section.adminfinance.edit" + "Permission");
							 	if (permissionadminfinance > 2 ){
							 		adminFinanceEdit = "false";
							 		
							 	}
							 	
							  %> 
													<configByCorp:fieldVisibility componentId="component.accountLine.section.paymentSent">
															 <c:if test="${not empty accountLine.paymentSent}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.paymentSent" /></s:text>
																	<td width="100px" class="listwhitetextAcct" >
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" value="%{accountLineFormattedValue}" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																	<img id="paymentSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>	
																	<% if (receivableDetailDate.equalsIgnoreCase("true")){%>
																	<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" value="%{accountLineFormattedValue}" size="8" maxlength="12" readonly="true" />
																	<%} %>	
																	</td>
																</c:if>
																<c:if test="${empty  accountLine.paymentSent}">
																	<td width="100px">
																	<s:textfield cssClass="input-text" id="paymentSent" name="accountLine.paymentSent" required="true" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="paymentSent-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																		<%} %>
																</td>
																</c:if>  
													</configByCorp:fieldVisibility>
													
								</sec-auth:authComponent>	
													
													
															<td align="right" height="20" width=""  style="width:211px;" class="listwhitetextAcct">
															<configByCorp:fieldVisibility componentId="accountLine.sendEstToClient">
															<fmt:message key="accountLine.sendEstToClient" /></configByCorp:fieldVisibility></td>
																<c:if test="${not empty accountLine.sendEstToClient}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendEstToClient" /></s:text>
																	<td width="100px" class="listwhitetextAcct"colspan="1"><configByCorp:fieldVisibility componentId="accountLine.sendEstToClient"><s:textfield cssClass="input-text" cssStyle="width:70px;margin-left:2px;" id="sendEstToClient" name="accountLine.sendEstToClient" value="%{accountLineFormattedValue}" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																		<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																		<img id="sendEstToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																		<%} %>
																		</configByCorp:fieldVisibility>
																	</td>
																</c:if>
																<c:if test="${empty accountLine.sendEstToClient}">
																	<td width="100px" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.sendEstToClient"><s:textfield cssClass="input-text" id="sendEstToClient" cssStyle="width:70px;margin-left:2px;" name="accountLine.sendEstToClient" required="true" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="sendEstToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																		<%} %>
																		</configByCorp:fieldVisibility></td>
																</c:if> 
															<td align="right" width=""  class="listwhitetextAcct" colspan="2" style="width: 99px;"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><fmt:message key="accountLine.sendActualToClient" /></configByCorp:fieldVisibility></td>
																<c:if test="${not empty accountLine.sendActualToClient}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendActualToClient" /></s:text>
																	<td width="99px"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" value="%{accountLineFormattedValue}" size="8" cssStyle="width:70px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true" />
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="sendActualToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id),copyActSenttoclient(); return false;" />
																	<%} %>		
																		</configByCorp:fieldVisibility></td>
																  </c:if>
																<c:if test="${empty accountLine.sendActualToClient}">
																	<td width="99px"><configByCorp:fieldVisibility componentId="accountLine.sendActualToClient"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" required="true" size="8" cssStyle="width:70px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true"/>
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="sendActualToClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="triggerCalendarId(this.id),copyActSenttoclient(); return false;" />
																		<%} %>	
																		</configByCorp:fieldVisibility></td>
																</c:if> 
															<td align="right" width=""  class="listwhitetextAcct" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><fmt:message key="accountLine.doNotSendtoClient" /></configByCorp:fieldVisibility></td>
																<c:if test="${not empty accountLine.doNotSendtoClient}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.doNotSendtoClient" /></s:text>
																	<td width="113px"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" value="%{accountLineFormattedValue}" size="8" cssStyle="width:63px;" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="doNotSendtoClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>	
																		</configByCorp:fieldVisibility></td>
																  </c:if>
																<c:if test="${empty accountLine.doNotSendtoClient}">
																	<td width="113px"><configByCorp:fieldVisibility componentId="accountLine.doNotSendtoClient"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" required="true" cssStyle="width:63px;" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																	<% if (receivableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="doNotSendtoClient-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																		<%} %>	
																		</configByCorp:fieldVisibility></td>
																</c:if> 
																<c:if test="${systemDefaultVatCalculation=='true'}">
																<td align="right" width=""  class="listwhitetextAcct" colspan="1"><configByCorp:fieldVisibility componentId="accountLine.recVatGL">VAT&nbsp;GL&nbsp;Code</configByCorp:fieldVisibility></td>
																<td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.recVatGL"><s:textfield cssClass="input-text" key="accountLine.recVatGl" size="7"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td> 
																<td align="right" width=""  class="listwhitetextAcct"><div id="qstRecVatGlLabel">Qst GL Code</div></td>
																<td align="left"><div id="qstRecVatGlId"><s:textfield cssClass="input-text" key="accountLine.qstRecVatGl" size="7"  maxlength="7" readonly="true" tabindex="35" /></div></td>
																</c:if>
																</tr>
																
	<sec-auth:authComponent componentId="module.accountLine.section.adminfinance.edit">
	<%;
	String adminFinanceEdit="true";
	int permissionadminfinance  = (Integer)request.getAttribute("module.accountLine.section.adminfinance.edit" + "Permission");
 	if (permissionadminfinance > 2 ){
 		adminFinanceEdit = "false";
 		
 	}
 	
  %>                                                          
                                                            <tr> 
                                                            <td height="20" align="right" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.recGl"><fmt:message key="charges.glCode"/></configByCorp:fieldVisibility></td>  
                                                                <td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.recGl"><s:textfield cssClass="input-text" key="accountLine.recGl" size="8"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td> 
															<td align="right" class="listwhitetextAcct" width="70px" style="width:130px;" ><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><fmt:message key="accountLine.accrueRevenue" /></configByCorp:fieldVisibility></td> 
																	<c:if test="${not empty accountLine.accrueRevenue}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenue" /></s:text>
																	<td class="listwhitetextAcct" colspan="4"><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><s:textfield cssClass="input-text" cssStyle="margin-left:2px;width:70px;" id="accrueRevenue" name="accountLine.accrueRevenue" value="%{accountLineFormattedValue}" size="8"
																		maxlength="12"  onkeydown="" readonly="true"/>
																		<%--<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<img id="accrueRevenue-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																	<%} %>--%>  
																	</configByCorp:fieldVisibility></td>
																 </c:if>
																 <c:if test="${empty accountLine.accrueRevenue}">
																	<td class="listwhitetextAcct" colspan="4"><configByCorp:fieldVisibility componentId="accountLine.accrueRevenue"><s:textfield cssClass="input-text" cssStyle="margin-left:2px;width:70px;" id="accrueRevenue" name="accountLine.accrueRevenue" required="true" size="8" maxlength="12"  onkeydown=""readonly="true"/> 
																		 <%--<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<img id="accrueRevenue-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																	<%} %>  
																	--%>
																	</configByCorp:fieldVisibility></td>
																</c:if>  
																<configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">
																<td colspan="2"></td>
																</configByCorp:fieldVisibility>
                                                             <c:if test="${accountInterface=='Y'}"> 
    <sec-auth:authComponent componentId="module.accountLine.section.receivableDetailAccrual.edit">
	<%;
	String receivableDetailAccrual="true";
	int permissionAccural  = (Integer)request.getAttribute("module.accountLine.section.receivableDetailAccrual.edit" + "Permission");
 	if (permissionAccural > 2 ){
 		receivableDetailAccrual = "false";
 		
 	}
 	
  %>                                                       <td align="right"  class="listwhitetextAcct"><fmt:message key="accountLine.accrueRevenueManual" />
                                                            <c:choose>
                                                            <c:when test="${empty accountLine.accrueRevenue }">
                                                            <td align="left"  class="listwhitetextAcct"><s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange="changeStatus()"/></td>
                                                            </c:when>
                                                            <c:when test="${not empty accountLine.accrueRevenue && empty accountLine.accrueRevenueReverse}">
                                                            <td align="left"  class="listwhitetextAcct"> <s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="<%=receivableDetailAccrual%>" onchange="changeStatus()"/></td>
                                                            </c:when>
                                                            <c:otherwise>
                                                           <td align="left"  class="listwhitetextAcct"> <s:checkbox name="accountLine.accrueRevenueManual" value="${accountLine.accrueRevenueManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange="changeStatus()"/></td>
                                                            </c:otherwise>
                                                            </c:choose> 
  </sec-auth:authComponent>
  
                                                            <td align="right"  class="listwhitetextAcct" colspan="1"><fmt:message key="accountLine.accrueRevenueReverse" /></td>
	                                                           <c:if test="${not empty accountLine.accrueRevenue }">
	                                                           <c:if test="${not empty accountLine.accrueRevenueReverse}"> 
		                                                          <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenueReverse"/></s:text>
		                                                       <td align="left" class="listwhitetextAcct" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" value="%{accountLineFormattedValue}" onkeydown="" readonly="true"  cssClass="input-text" size="7"/>
	                                                          <%--<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<img id="accrueRevenueReverse-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>--%></td>
	                                                          </c:if>
	                                                         <c:if test="${empty accountLine.accrueRevenueReverse}">
	                                                        <td align="left" class="listwhitetextAcct" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" onkeydown="" readonly="true" cssClass="input-text" size="7"/>
	                                                        <%-- <% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<img id="accrueRevenueReverse-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>--%></td>
	                                                         </c:if>
	                                                      </c:if>
	                                                         <c:if test="${empty accountLine.accrueRevenue }">
	                                                         <c:if test="${not empty accountLine.accrueRevenueReverse}"> 
		                                                          <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenueReverse"/></s:text>
		                                                       <td align="left" class="listwhitetextAcct" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" value="%{accountLineFormattedValue}"  readonly="true" cssClass="input-text" size="7"/>
	                                                          </td>
	                                                          </c:if>
	                                                         <c:if test="${empty accountLine.accrueRevenueReverse}">
	                                                        <td align="left" class="listwhitetextAcct" ><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse"  readonly="true" cssClass="input-text" size="7"/>
	                                                         </td>
	                                                         </c:if>
	                                                         </c:if>
	                                                         </c:if>
	                                                            </tr> 													
                                                         <tr><td align="right"  class="listwhitetextAcct" ><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><fmt:message key="accountLine.recPostDate" /></configByCorp:fieldVisibility></td> 
																	<c:if test="${not empty accountLine.recPostDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.recPostDate" /></s:text>
																	<td class="listwhitetextAcct"width="120px"><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" value="%{accountLineFormattedValue}" size="8"
																		maxlength="12"  onkeydown="" readonly="true"/></configByCorp:fieldVisibility><configByCorp:fieldVisibility componentId="accountLine.recPostDate">
																		<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<c:if test="${empty accountLine.recAccDate}">
																		   <img id="recPostDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['accountLineForms'].recPostDate.focus(); return false;" />
																		</c:if>
																		<c:if test="${not empty accountLine.recAccDate}">
																		   <img id="recPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																		</c:if>
																		<%} %>
																		</configByCorp:fieldVisibility>
																		</td>
																</c:if>
																<c:if test="${empty accountLine.recPostDate}">
																	<td class="listwhitetextAcct"width="120px"><configByCorp:fieldVisibility componentId="accountLine.recPostDate"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" required="true" size="8" maxlength="11"
																		 onkeydown=""readonly="true"/></configByCorp:fieldVisibility><configByCorp:fieldVisibility componentId="accountLine.recPostDate">
																	<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																	<c:if test="${empty accountLine.recAccDate}">
																		 <img id="recPostDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['accountLineForms'].recPostDate.focus(); return false;" />
																	</c:if>	
																	<c:if test="${not empty accountLine.recAccDate}">
																		 <img id="recPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	</c:if>
																	<%} %>
																		</configByCorp:fieldVisibility>
																	</td>
																</c:if>
																<c:if test="${accountInterface=='Y'}">
																<td height="20" align="right" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><fmt:message key="accountLine.recAccDate"/></configByCorp:fieldVisibility></td>  
                                                                <c:if test="${not empty accountLine.recAccDate}">
																<s:text id="recAccDate" name="${FormDateValue}"><s:param name="value" value="accountLine.recAccDate" /></s:text>
																<% if (!(adminFinanceEdit.equalsIgnoreCase("false"))){%>
																<td class="listwhitetextAcct" colspan="2"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate"  name="accountLine.recAccDate" value="%{recAccDate}" cssStyle="width:68px" maxlength="11" readonly="true" />
																</configByCorp:fieldVisibility></td> 
																<%} %> 
																<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																        <td class="listwhitetextAcct" colspan="2"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate"  name="accountLine.recAccDate" value="%{recAccDate}" cssStyle="width:70px;margin-left:2px;" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
																		<img id="recAccDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
																	    </configByCorp:fieldVisibility></td> 
																	<%} %> 
																
																</c:if>
																<c:if test="${empty accountLine.recAccDate}">
																<td class="listwhitetextAcct" colspan="2"><configByCorp:fieldVisibility componentId="accountLine.recAccDate"><s:textfield cssClass="input-text" id="recAccDate"  name="accountLine.recAccDate" cssStyle="width:70px;margin-left:2px;" maxlength="11" readonly="true" />
																<% if (adminFinanceEdit.equalsIgnoreCase("false")){%>
																		<img id="recAccDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
																	 <%} %></configByCorp:fieldVisibility></td>
																</c:if> 
																</c:if>
<configByCorp:fieldVisibility componentId="component.field.auditCompleteDateForUTSI">
<c:if test="${accountInterface=='Y'}">
<sec-auth:authComponent componentId="module.accountLine.section.auditCompleteDate.edit">
	<%;
	String auditCompleteDateEdit="true";
	int permissionAuditCompleteDate  = (Integer)request.getAttribute("module.accountLine.section.auditCompleteDate.edit" + "Permission");
 	if (permissionAuditCompleteDate > 2 ){
 		auditCompleteDateEdit = "false";
 		
 	}
 	
  %>																													
<td align="right" class="listwhitetextAcct"><fmt:message key="accountLine.auditCompleteDate"/></td>
<c:choose>
<c:when test="${empty accountLine.recInvoiceNumber && (accountLine.actualRevenue!='0.00' && accountLine.actualRevenue!='0' && accountLine.actualRevenue!='0.0') && empty accountLine.recPostDate}" >
	<c:if test="${not empty accountLine.auditCompleteDate}">
	<s:text id="auditCompleteDate" name="${FormDateValue}"><s:param name="value" value="accountLine.auditCompleteDate" /></s:text>
	<% if (!(auditCompleteDateEdit.equalsIgnoreCase("false"))){%>
	<td class="listwhitetextAcct" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:63px" maxlength="11" readonly="true" />
	</td> 
	<%} %> 
	<% if (auditCompleteDateEdit.equalsIgnoreCase("false")){%>
	<td class="listwhitetextAcct" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:63px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" />
	<img id="auditCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
	</td> 
	<%} %> 
	</c:if>
	<c:if test="${empty accountLine.auditCompleteDate}">
	<td class="listwhitetextAcct" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" cssStyle="width:63px" maxlength="11" readonly="true" />
	<% if((auditCompleteDateEdit.equalsIgnoreCase("false"))){%>
	<img id="auditCompleteDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
	<%} %>
	</td>  
	</c:if>
 </c:when>
<c:otherwise>
	<c:if test="${not empty accountLine.auditCompleteDate}">
	<s:text id="auditCompleteDate" name="${FormDateValue}"><s:param name="value" value="accountLine.auditCompleteDate" /></s:text>
	<td class="listwhitetextAcct" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" value="%{auditCompleteDate}" cssStyle="width:63px" maxlength="11" readonly="true" />
	</c:if>
	<c:if test="${empty accountLine.auditCompleteDate}">
	<td class="listwhitetextAcct" colspan=""><s:textfield cssClass="input-text" id="auditCompleteDate" name="accountLine.auditCompleteDate" cssStyle="width:63px" maxlength="11" readonly="true" />
	</td>  
	</c:if>
</c:otherwise>
</c:choose>
</sec-auth:authComponent>
</c:if>
</configByCorp:fieldVisibility>
																<td align="right" class="listwhitetextAcct" colspan="3"><configByCorp:fieldVisibility componentId="accountLine.recXfer"><fmt:message key="accountLine.recXfer"/></configByCorp:fieldVisibility></td>  
                                                                <td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.recXfer"><s:textfield cssClass="input-text" key="accountLine.recXfer" size="15"  maxlength="20"   readonly="<%= adminFinanceEdit%>" tabindex="36"/></configByCorp:fieldVisibility></td> 
										                       
                                                                <c:if test="${accountInterface=='Y'}">
                                                                <td height="20" align="right" class="listwhitetextAcct" colspan="1"><fmt:message key="accountLine.recXferUser"/></td>  
                                                                <td align="left" class="listwhitetextAcct" colspan="2"><s:textfield cssClass="input-text" key="accountLine.recXferUser" size="7"  maxlength="7" readonly="<%= adminFinanceEdit%>" tabindex="37"/></td> 	 
                                                                </c:if>
                                                                 <script type="text/javascript">
                                                                   disableBYRecAcc();
                                                                   checkMultiAuthorization();
                                                                </script>
                                                           </tr> 
  </sec-auth:authComponent> 						
													</table> 
	</sec-auth:authComponent> 				
												</div>
											</td>
										</tr>
															
	<tr>
	        <td height="" align="left" class="listwhitetext">
	       <c:if test="${accountInterface!='Y'}">  
	       <div  onClick="javascript:checkVendorCodeForPay('pay')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Payable&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>    </c:if>
		   <c:if test="${accountInterface=='Y'}">  
		     <div  onClick="checkActgCode('pay')" style="margin: 0px"  >
<table cellpadding="0" cellspacing="0" width="100%" border="0"  style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center" >&nbsp;Payable&nbsp;Details
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</div>		
		 </c:if>
			<div  id="pay" >

<jsp:include flush="true" page="accountLineFormsPayableSec.jsp"></jsp:include>
<script type="text/javascript">
try{
disablePayableSec()
disableBYUnlinkedCMM()
disablePayableSectionAfterInvoice()
}catch(e){}
</script>	
														</div>
														</td>
														</tr> 
						<tr> 
						</tr> 
						<tr height="15px"></tr>
					</tbody>
				</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
				<jsp:include flush="true" page="accountLineFormsSection2.jsp"></jsp:include> 
</div> 
	<s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" tabindex="55" onmouseover="return chkSelect('2','save');" onclick="return activeAccrueStatus(),checkLHFValue();"/>
	<input type="button" class="cssbutton1" name="Cancel" value="Cancel" size="20" style="width:55px; height:25px" tabindex="56" onclick="cancelBtn();" />
	<c:choose>
	<c:when test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.createdBy == 'Networking')}">
	<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" tabindex="57" onclick="" />
	</c:when>
	<c:when test="${accountLine.t20Process}">
		<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" tabindex="57" onclick="" />
	</c:when>
	<c:otherwise>
	<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" tabindex="57" onclick="findRadioValue();resetDisableCategory();setTimeout('checkMultiAuthorization()',700);" />
	</c:otherwise>
	</c:choose>
	<c:if test="${multiCurrency !='Y'}">
	<input type="button" class="cssbuttonA" style="width:100px; height:25px"	 
		 onclick="saveAndCopyAccountLine();" value="Save With Copy" />
	</c:if>
	    
    <s:hidden name="accountLine.corpID" />
    <s:hidden name="accountLine.uvlControlNumber" />
    <s:hidden name="accountLine.ddrControlNumber" />
    <s:hidden name="accountLine.sequenceNumber" /> 
	<s:hidden name="secondDescription" />
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription"/>
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="seventhDescription" />
	<s:hidden name="eigthDescription" />
	<s:hidden name="ninthDescription" />
	<s:hidden name="tenthDescription" />
	 
    <s:hidden name="totalEstimateExpenseCwt" value="<%=request.getParameter("totalEstimateExpenseCwt")%>"/>
	<s:hidden name="totalRevisionExpenseCwt" value="<%=request.getParameter("totalRevisionExpenseCwt")%>"/>
	<s:hidden name="totalEstimateExpenseKg" value="<%=request.getParameter("totalEstimateExpenseKg")%>"/>
	<s:hidden name="totalRevisionExpenseKg" value="<%=request.getParameter("totalRevisionExpenseKg")%>"/>
	<s:hidden name="totalEstimateExpenseCft" value="<%=request.getParameter("totalEstimateExpenseCft")%>"/>
	<s:hidden name="totalRevisionExpenseCft" value="<%=request.getParameter("totalRevisionExpenseCft")%>"/>
	<s:hidden name="totalEstimateExpenseCbm" value="<%=request.getParameter("totalEstimateExpenseCbm")%>"/>
	<s:hidden name="totalRevisionExpenseCbm" value="<%=request.getParameter("totalRevisionExpenseCbm")%>"/>
	<s:hidden name="totalEstimateExpenseEach" value="<%=request.getParameter("totalEstimateExpenseEach")%>"/>
	<s:hidden name="totalRevisionExpenseEach" value="<%=request.getParameter("totalRevisionExpenseEach")%>"/>
	<s:hidden name="totalEstimateExpenseFlat" value="<%=request.getParameter("totalEstimateExpenseFlat")%>"/>
	<s:hidden name="totalRevisionExpenseFlat" value="<%=request.getParameter("totalRevisionExpenseFlat")%>"/>
	<s:hidden name="totalEstimateExpenseHour" value="<%=request.getParameter("totalEstimateExpenseHour")%>"/>
	<s:hidden name="totalRevisionExpenseHour" value="<%=request.getParameter("totalRevisionExpenseHour")%>"/> 
	<s:hidden name="checkChargeCodePopup" value=""/>
	<s:hidden name="selectedAccountId" value="" id="setSelectedAccountId"/>
</s:form> 
<%-- Script Shifted from Top to Botton on 07-Sep-2012 By Kunal --%>

<%-- Shift1 Starts --%>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
	

<link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
  <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>   
    
	
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<%--  <script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>  --%>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script> 
<%-- Shift1 Ends --%>


<!-- Shifting Closed Here -->
<script type="text/javascript">

function saveAndCopyAccountLine(){
	progressBarAutoSave('1');
	try{
	editTrap();
	}catch(e){}	
	 document.forms['accountLineForms'].action = 'addWithCopyAccountLine.html?btntype=yes&save=Yes';
	 document.forms['accountLineForms'].submit();
}

var elemtId = '';
function triggerCalendarId(elementId){
	return elemtId = elementId;
 }
function actSentToclient(){
	if(elemtId ==  'sendActualToClient-trigger'){
		elemtId='';
		updateActSenttoclient();
	}
}
<%--
<c:if test="${(trackingStatus.soNetworkGroup  && billingCMMContractType && (accountLine.createdBy == 'Networking' || (fn1:indexOf(accountLine.createdBy,'Stg Bill')>=0 && !(trackingStatus.accNetworkGroup) ))) ||(trackingStatus.soNetworkGroup  && billingCMMContractType && accountLine.chargeCode == 'MGMTFEE') || (trackingStatus.soNetworkGroup  && billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE' ))||(accountLine.chargeCode == 'MGMTFEE') || (billingDMMContractType && (accountLine.chargeCode == 'DMMFEE' || accountLine.chargeCode == 'DMMFXFEE') )}">
iehack();
trap();
	
</c:if> 
--%>

<c:if test="${disableALL == 'YES'}">
iehack();
trap();
</c:if>

<c:if test="${(trackingStatus.soNetworkGroup  && billingDMMContractType && accountLine.createdBy == 'Networking')}">
//if(document.forms['accountLineForms'].elements['accountLine.billToCode'].value.trim()==document.forms['accountLineForms'].elements['bookingAgentCodeDMM'].value.trim()){
     iehack();
	 trap();
 
 //} 
</c:if>
<c:if test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
try{
iehack();
trap();
}catch(e){}
</c:if>
<c:if test='${serviceOrder.status == "CLSD"}'>
<configByCorp:fieldVisibility componentId="component.field.companyDivision.closedDivision">
	findClosedCompanyDivision('${serviceOrder.companyDivision}','${serviceOrder.bookingAgentCode}');
</configByCorp:fieldVisibility>

function findClosedCompanyDivision(companyCode,bookingAgentCode,oldStatus,status){ 
	 new Ajax.Request('/redsky/findClosedCompanyDivisionAjax.html?ajax=1&companyCode='+companyCode+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      if(response.trim()=="false"){
			    	  
			      }else{
			    	  iehack();
			    	  trap();
			      }
			    },
			    onFailure: function(){ 
				    }
			  });
}
</c:if>

<c:if test="${serviceOrder.corpID=='VOER'}">
setOnSelectBasedMethods(['hitAcct(),actSentToclient(),changeStatus(),calcInvoiceDate(),calcDays()']);
</c:if>
<c:if test="${serviceOrder.corpID!='VOER'}">
	setOnSelectBasedMethods(['hitAcct(),actSentToclient(),changeStatus(),calcDays()']);
</c:if>
	setCalendarFunctionality(); 

	function calcInvoiceDate()
	{ 
		
		var t='${accountLine.receivedInvoiceDate}';
		
		var strArray=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
		var olddate = new Date('${accountLine.receivedInvoiceDate}')
		 var day = olddate.getDate(); 
        if (day < 10) { 
            day = "0" + day; 
        } 
        var m = strArray[olddate.getMonth()];
        var year = olddate.getFullYear().toString().substr(-2); 
       
	     var invoiceDate=document.forms['accountLineForms'].elements['accountLine.receivedInvoiceDate'].value;

		 var date1SplitResult = invoiceDate.split("-");
		 var day1 = date1SplitResult[0];
		 var month1 = date1SplitResult[1];
		 var year1 = date1SplitResult[2];
		   	 year1 = '20'+year1;
		   	 if(month1 == 'Jan'){ month1 = "01";  }
			    else if(month1 == 'Feb'){ month1 = "02";  }
				else if(month1 == 'Mar'){ month1 = "03";  }
				else if(month1 == 'Apr'){ month1 = "04";  }
				else if(month1 == 'May'){ month1 = "05";  }
				else if(month1 == 'Jun'){ month1 = "06";  }
				else if(month1 == 'Jul'){ month1 = "07";  }
				else if(month1 == 'Aug'){ month1 = "08";  }
				else if(month1 == 'Sep'){ month1 = "09";  }
				else if(month1 == 'Oct'){ month1 = "10";  }																					
				else if(month1 == 'Nov'){ month1 = "11";  }
				else if(month1 == 'Dec'){ month1 = "12";  }
		var  current_datetime = new Date()
	    var formatted_date = current_datetime.getDate() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getFullYear()
	   

	   var currentD ='<%= (new java.text.SimpleDateFormat("yyyy-MM-dd")).format(new java.util.Date()) %>';
	 
	   var date3SplitResult = currentD.split("-");
			var day3 = date3SplitResult[2];
			var month3 = date3SplitResult[1];
			var year3 = date3SplitResult[0];
			var selectedDate = new Date(month1+"/"+day1+"/"+year1);
			var currentDate = new Date(month3+"/"+day3+"/"+year3);
			
			  var daysApart = Math.round((selectedDate-currentDate)/86400000);
			if(daysApart>0){
				
				 alert("Invoice date can't be in future")
				 if(t!='')
			{
				 document.forms['accountLineForms'].elements['accountLine.receivedInvoiceDate'].value=day + "-" + m + "-" + year;
			}
				 else
					 {
					 document.forms['accountLineForms'].elements['accountLine.receivedInvoiceDate'].value='';
					 }
			    return false;
				}
		

	}
</script>


<c:if test="${empty accountLine.id}">
<script type="text/javascript">  
try{
//findExchangeRate();
fillBranchCode();  
}
catch(e){}

</script> 
</c:if>
<script type="text/javascript">

try{
<c:if test="${accountLine.deviation!='' && accountLine.deviation!='NCT' }">
document.getElementById("estimateDeviationShow").style.display="block"; 
document.getElementById("revisionDeviationShow").style.display="block"; 
document.getElementById("receivableDeviationShow").style.display="block";  
document.getElementById("payableDeviationShow").style.display="block";    
</c:if>
<c:if test="${accountLine.deviation=='' || accountLine.deviation=='NCT' || empty accountLine.deviation || accountLine.deviation =='null' }">
document.getElementById("estimateDeviationShow").style.display="none"; 
document.getElementById("revisionDeviationShow").style.display="none";
document.getElementById("receivableDeviationShow").style.display="none"; 
document.getElementById("payableDeviationShow").style.display="none"; 
</c:if>
}
catch(e){}
try{
chechCreditInvoice();
}
catch(e){} 
try{
<c:if test="${accountInterface=='Y'}">
 var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value; 
 var glCode= document.forms['accountLineForms'].elements['accountLine.recGl'].value;
 var billToCode= document.forms['accountLineForms'].elements['accountLine.billToCode'].value;
 var vendorCode= document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
 var actgCode= document.forms['accountLineForms'].elements['accountLine.actgCode'].value;
 var payGl= document.forms['accountLineForms'].elements['accountLine.payGl'].value; 
 var grossMarginActualization= document.forms['accountLineForms'].elements['grossMarginActualization'].value; 
 chargeCode =chargeCode.trim();
 glCode =glCode.trim();
 billToCode =billToCode.trim(); 
 vendorCode =vendorCode.trim(); 
 actgCode =actgCode.trim(); 
 payGl=payGl.trim(); 
 if(chargeCode!='' && glCode!='' && billToCode!='' && grossMarginActualization =='N' ) { 
  animatedcollapse.addDiv('rec', 'fade=0,persist=0,show=1')
 } else{ 
 animatedcollapse.addDiv('rec', 'fade=0,persist=0,hide=1')
 }
 if(vendorCode!='' && actgCode!='' && chargeCode!='' && payGl!='' && grossMarginActualization =='N'){ 
 animatedcollapse.addDiv('pay', 'fade=0,persist=0,show=1')
 }else{  
 animatedcollapse.addDiv('pay', 'fade=0,persist=0,hide=1')
 }
</c:if>
<c:if test="${accountInterface!='Y'}">
 var chargeCode= document.forms['accountLineForms'].elements['accountLine.chargeCode'].value;
 var vendorCode= document.forms['accountLineForms'].elements['accountLine.vendorCode'].value; 
 chargeCode =chargeCode.trim();
 vendorCode =vendorCode.trim();
 if(chargeCode!='') {
   animatedcollapse.addDiv('rec', 'fade=0,persist=0,show=1') 
 }else {
   animatedcollapse.addDiv('rec', 'fade=0,persist=0,hide=1')
 } 
 if(vendorCode!='') {
   animatedcollapse.addDiv('pay', 'fade=0,persist=0,show=1')
 } else {
   animatedcollapse.addDiv('pay', 'fade=0,persist=0,hide=1')
 } 
</c:if> 
<c:if test="${costingDetailShowFlag=='Y'}">
animatedcollapse.addDiv('costing', 'fade=0,persist=0,show=1')
</c:if>
<c:if test="${costingDetailShowFlag!='Y'}">
animatedcollapse.addDiv('costing', 'fade=0,persist=0,hide=1')
</c:if>
animatedcollapse.init() 
//roundRecRate(); 
if(document.forms['accountLineForms'].elements['calOpener'].value=='open')
{
	receivedAmountActive(); 									    
}
checkInvoice(); 
findRadioValue();
disableAmountRevenue('load'); 
fillDescription();
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0 ||  fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
myFunctionGL(); 
</c:if>
var f = document.getElementById('accountLineForms');
f.setAttribute("autocomplete", "off"); 
<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">  
disableButton();
</c:if>
}
catch(e){}
</script>
<SCRIPT type="text/javascript">
window.onload=function(){
checkVatForReadonlyFields('first');
checkVatForReadonlyFields('second');
checkVatForReadonlyFieldsEst('first');
checkVatForReadonlyFieldsEst('second');
}

</SCRIPT> 
<script type="text/javascript">
window.onload = function() { 
	
var fieldName = document.forms['accountLineForms'].elements['field'].value;

var fieldName1 = document.forms['accountLineForms'].elements['field1'].value;
if(fieldName!=''){
document.forms['accountLineForms'].elements[fieldName].className = 'rules-textUpper';
}
if(fieldName1!=''){
document.forms['accountLineForms'].elements[fieldName1].className = 'rules-textUpper';
} 
}
</script>
<script type="text/javascript">
try{
disablePayableSec()
}catch(e){}
try{
fillDefaultInvoiceNumber();
}catch(e){}
try{
	disableBYUnlinkedCMM();
}catch(e){}

function cancelBtn(){
	var url = 'accountLineList.html?sid=${serviceOrder.id}';
	
	try{
		<c:if test="${vanLineAccountView =='category'}">
	    	url = 'accountCategoryViewList.html?sid=${serviceOrder.id}&vanLineAccountView=${vanLineAccountView}';
		</c:if>
	}catch(e){}
	
	location.href = url;
}
try{
disableDriverCompanyDivision();
driverValueCompany();
}catch(e){}

function isSoExtFlag(){
	//alert("1");
    document.forms['accountLineForms'].elements['isSOExtract'].value="yes";
    //alert("2");
  }
try{
<c:if test="${systemDefaultVatCalculation=='true'}"> 
payQstHideBlock();
recQstHideBlock();
</c:if>
}catch(e){}

function checkActualRevenue(){
	<configByCorp:fieldVisibility componentId="component.field.AccountLine.AuthorizedCreditNote">
		var actualRevenueVal = document.forms['accountLineForms'].elements['accountLine.actualRevenue'].value;
		var authorizedCreditNoteVal = document.getElementById('authorizedCreditNote').checked;
		if(authorizedCreditNoteVal == true && actualRevenueVal >=0){
			alert('Authorized Credit Note can not be checked, As Actual Revenue is not negative.')
			document.getElementById('authorizedCreditNote').checked = false;
		}
	</configByCorp:fieldVisibility>
}
var httppayVatBilling = gethttppayVatBilling();
function gethttppayVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}

var httprecVatBilling = gethttprecVatBilling();
function gethttprecVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}
function findrecVATDesc()
{
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	     var partnerCode = document.forms['accountLineForms'].elements['accountLine.billToCode'].value;
	     var accountLineid = document.forms['accountLineForms'].elements['accountLine.id'].value;
	     var recVatDescr='${accountLine.recVatDescr}';
	 	 var companyDivision=document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
		 var url="recVatAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&companyDivision="+encodeURI(companyDivision)+"&id="+encodeURI(accountLineid)+"&recVatDescr="+encodeURI(recVatDescr);
		 httprecVatBilling.open("GET", url, true);
		 httprecVatBilling.onreadystatechange = handleHttpResponserecVatBilling;
		 httprecVatBilling.send(null);
		 </configByCorp:fieldVisibility>
}

function handleHttpResponserecVatBilling() { 
	 if (httprecVatBilling.readyState == 4)
    {
      var results = httprecVatBilling.responseText
        results = results.trim();
      results = results.replace('[','');
      results=results.replace(']',''); 
      res = results.split("@");
      var targetElement = document.forms['accountLineForms'].elements['accountLine.recVatDescr'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['accountLineForms'].elements['accountLine.recVatDescr'].options[i].text = '';
					document.forms['accountLineForms'].elements['accountLine.recVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['accountLineForms'].elements['accountLine.recVatDescr'].options[i].value = stateVal[0];
					document.forms['accountLineForms'].elements['accountLine.recVatDescr'].options[i].text = stateVal[1];
					}
					}
				document.forms['accountLineForms'].elements['accountLine.recVatDescr'].value = '${accountLine.recVatDescr}';
				findDefaultVatFromPartnerForBill();
           
    }
}
//function to findoutPayVatDec
function findPayVatWithVatGroup()
{
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var partnerCode = document.forms['accountLineForms'].elements['accountLine.vendorCode'].value;
	    accountLineid = document.forms['accountLineForms'].elements['accountLine.id'].value;
	    var payVatDescr='${accountLine.payVatDescr}';
	 	var companyDivision=document.forms['accountLineForms'].elements['accountLine.companyDivision'].value;
		var url="payVatAccountLine.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(partnerCode)+"&companyDivision="+encodeURI(companyDivision)+"&id="+encodeURI(accountLineid)+"&payVatDescr="+encodeURI(payVatDescr);
		httppayVatBilling.open("GET", url, true);
		httppayVatBilling.onreadystatechange = handleHttpResponsepayVatBilling;
		httppayVatBilling.send(null);
		</configByCorp:fieldVisibility >
}

function handleHttpResponsepayVatBilling() { 
	 if (httppayVatBilling.readyState == 4)
    {   
   var results = httppayVatBilling.responseText
      results = results.trim();
      results = results.replace('[','');
      results=results.replace(']',''); 
      res = results.split("@");
      var targetElement = document.forms['accountLineForms'].elements['accountLine.payVatDescr'];
			targetElement.length = res.length;
				for(i=0;i<res.length;i++)
					{
					console.log(res);
					if(res[i] == ''){
					document.forms['accountLineForms'].elements['accountLine.payVatDescr'].options[i].text = '';
					document.forms['accountLineForms'].elements['accountLine.payVatDescr'].options[i].value = '';
					}else{
					var stateVal = res[i].split("#");
					document.forms['accountLineForms'].elements['accountLine.payVatDescr'].options[i].value = stateVal[0];
					document.forms['accountLineForms'].elements['accountLine.payVatDescr'].options[i].text = stateVal[1];
				
			
					}
					}
				document.forms['accountLineForms'].elements['accountLine.payVatDescr'].value = '${accountLine.payVatDescr}';
				findDefaultVatFromPartnerForVendor();
           
    }
}
</script>
<script language="javascript" type="text/javascript">
function findUserPermission(name,position) { 
  var url="findAcctRefNumList.html?ajax=1&decorator=simple&popup=true&code=" + encodeURI(name);
  ajax_showTooltip(url,position);	
  }

</script>
</body>
</html>

