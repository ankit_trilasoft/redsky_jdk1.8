package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import sun.misc.BASE64Decoder;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;

import javax.servlet.jsp.PageContext;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.AgentBaseSCAC;
import com.trilasoft.app.model.AgentRequest;
import com.trilasoft.app.model.AgentRequestReason;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ContractPolicy;
import com.trilasoft.app.model.ControlExpirations;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.AgentRequestManager;
import com.trilasoft.app.service.AgentRequestReasonManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.PartnerAccountRefManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RefZipGeoCodeMapManager;

public class AgentRequestAction extends BaseAction implements Preparable{

	private Long id;

	private PartnerPrivate partnerPrivate;
   private RefMasterManager refMasterManager;

	private String sessionCorpID;

	private String gotoPageString;


	private String hitflag;
	
	private String imageDataFileLogo;
   private String countryCodeSearch;

	private String stateSearch;

	private String countrySearch;

	private String citySearch;

	private String partnerType;
	
	private Map<String, String> countryDesc;

	private List multiplequalityCertifications;

	private List multiplVanlineAffiliations;

	private List multiplServiceLines;
	private List ownerOperatorList = new ArrayList();
	private Map<String, String> bstates;

	private Map<String, String> tstates;

	private Map<String, String> mstates;

	private CustomerFileManager customerFileManager;

	private PartnerManager partnerManager;
	private Company company;
	private CompanyManager companyManager;
	private Map<String, String> typeOfVendor;
	
	private Map<String, String> corp;

	private Map<String, String> vanlineAffiliations;

	private Map<String, String> serviceLines;

	private Map<String, String> qualityCertifications;
	
	private Map<String, String> fleet; 
	private Set partners;
	private File file;
	private File file6;

	private File file1;
	private File  fileLogo;
	private String fileLogoUploadFlag;
	private File file2;

	private File file3;

	private String fileFileName;

	private String file1FileName;
	private String fileLogoFileName;

	private String file2FileName;

	private String file3FileName;
	private String file4FileName;

	private String popupval;

	private String exList;

	private List partnerListNew;

	private String flag;

	private String formClose;
	private String parentId;//Enhancement for ControlExpirations
	private String paramView = "";

	 private String agentCountryCode;
	private UserManager userManager;
	
	private String destination;

	private String origin;

	private List companyDivis = new ArrayList();
	
	private String partnerCodeCount;
	
	private String compDiv = "";

	private String userCheck;
	private String partnerCorpid;
	private String userCheckMaster;
	private String jobRelo;
	private String logQuery = "";
	private Set<Role> userRole;
	private String userType ;
	private Map<String, String> creditTerms;
	private Boolean isIgnoreInactive = false; 
	private Map<String,String> driverAgencyList;
	private Map<String, String> paytype;
    private Map<String, String> serviceRelos1= new HashMap<String, String>();
    private Map<String, String>  serviceRelos;
	private Map<String, String> serviceRelos2= new HashMap<String, String>(); 
	private Map<String, String> driverType;
    private List donotMergelist;
	private String donoFlag;
	private String partnerNotes;
	private DataSecuritySet dataSecuritySet;
	private DataSecurityFilter dataSecurityFilter;
	private Boolean checkTransfereeInfopackage;
    private String tempTransPort;
    private String usertype; 
    private String validVatCode;
    private String billingCountry;
    private Map<String,String> bankCode;
    private String enbState;
    private Map<String, String> countryCod;
    private String chkAgentTrue;
    private Date partnerPrivateHire;
    private Date partnerPrivateTermination;
    private String vanlineCodeSearch;
    private Map<String, String> countryWithBranch;
    private String flagHit;
    private  Map<String, String> planCommissionList;
    Date currentdate = new Date();
    private boolean cmmdmmflag=false;
    private String searchListOfVendorCode;
    private Map<String, String> listOfVendorCode;
    private  Map<String, String> vatBillingGroups = new LinkedHashMap<String, String>();
    private String compDivFlag;
    private String bankNoList;
    private String currencyList;
    private String checkList;
    private String newlineid;
    private List partnerBankInfoList;
    private String bankPartnerCode;
    private String partnerbanklist;
    private String checkCodefromDefaultAccountLine;
    private Integer compensationYear;
    private boolean checkFieldVisibility=false;
    private String checkCompensationYear="";
    private int parentCompensationYear=0;
	static final Logger logger = Logger.getLogger(AgentRequestAction.class);
	private AgentRequestManager agentRequestManager;
	private AgentRequest agentRequest;
	private AgentRequest agentRequestNew;
  
	private EmailSetupManager emailSetupManager;
	private String validateFormNav;
    private PartnerPublic partnerPublic;
	
    private PartnerPublic partner;

	private PartnerPublicManager partnerPublicManager;

	private PartnerPrivateManager partnerPrivateManager;
private String status;
private Map<String, String> reason;
    public void prepare() throws Exception {
		enbState = customerFileManager.enableStateList(sessionCorpID);
		//countryWithBranch=agentRequestManager.getCountryWithBranch("COUNTRY",sessionCorpID);
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		
	}
    
    public String saveOnTabChange() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");    	
		String s = save();
		validateFormNav = "OK";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	public AgentRequestAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
		userRole = user.getRoles();
		userType=user.getUserType();
	}


	private List selectedJobList;
	private Long pId;
	

	public Long getpId() {
		return pId;
	}

	public void setpId(Long pId) {
		this.pId = pId;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
		if (paramView.equalsIgnoreCase("View")) {
			getRequest().getSession().setAttribute("paramView", paramView);
		}
		if (id != null) { 
			agentRequest = agentRequestManager.get(id); 
			getComboList(agentRequest.getCorpID());
			serviceLines = refMasterManager.findByParameterWithoutParent(agentRequest.getCorpID(), "SERVICELINES");
	    	try{
	    		selectedJobList = new ArrayList(); 
	    		String tv1 = new String();
	    		tv1 = agentRequest.getTypeOfVendor().trim();
	    		if (tv1.indexOf(",") == 0) {
	    			tv1 = tv1.substring(1);
	    		}
	    		String[] ac = tv1.split(",");
	    		int arrayLength2 = ac.length;	
	    		for (int j = 0; j < arrayLength2; j++) {
	    			String qacCerti = ac[j];
	    			if (qacCerti.indexOf("(") == 0) {
	    				qacCerti = qacCerti.substring(1);
	    			}
	    			if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf(")") == qacCerti.length() - 1)) {
	    				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
	    			}
	    			if (qacCerti.indexOf("'") == 0) {
	    				qacCerti = qacCerti.substring(1);
	    			}
	    			if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf("'") == qacCerti.length() - 1)) {
	    				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
	    			}

	    			selectedJobList.add(qacCerti.trim());
	    		}		
	    		agentRequest.setTypeOfVendor(selectedJobList.toString().replace("[", "").replace("]", ""));
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}  
			try {
				multiplequalityCertifications = new ArrayList();
				multiplVanlineAffiliations = new ArrayList();
				multiplServiceLines = new ArrayList();
				if(agentRequest.getQualityCertifications()!=null && !agentRequest.getQualityCertifications().trim().equals(""))
				{
				String[] qc = agentRequest.getQualityCertifications().split(",");
				int arrayLength1 = qc.length;
				for (int j = 0; j < arrayLength1; j++) {
					String qacCerti = qc[j];
					if (qacCerti.indexOf("(") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf(")") == qacCerti.length() - 1)) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}
					if (qacCerti.indexOf("'") == 0) {
						qacCerti = qacCerti.substring(1);
					}
					if (!qacCerti.equalsIgnoreCase("") && (qacCerti.lastIndexOf("'") == qacCerti.length() - 1)) {
						qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
					}

					multiplequalityCertifications.add(qacCerti.trim());
				}
				}
				
			   } catch (Exception ex) {
				ex.printStackTrace();
			  }try{
				  if(agentRequest.getVanLineAffiliation()!=null && !agentRequest.getVanLineAffiliation().trim().equals(""))
				  {
				String[] va = agentRequest.getVanLineAffiliation().split(",");
				int arrayLength2 = va.length;
				for (int k = 0; k < arrayLength2; k++) {
					String vanCer = va[k];
					if (vanCer.indexOf("(") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (!vanCer.equalsIgnoreCase("") && (vanCer.lastIndexOf(")") == vanCer.length() - 1)) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}
					if (vanCer.indexOf("'") == 0) {
						vanCer = vanCer.substring(1);
					}
					if (!vanCer.equalsIgnoreCase("") && (vanCer.lastIndexOf("'") == vanCer.length() - 1)) {
						vanCer = vanCer.substring(0, vanCer.length() - 1);
					}

					multiplVanlineAffiliations.add(vanCer.trim());
				}
				  }
			    } catch (Exception ex) {
					ex.printStackTrace();
				}
			    try{
			    	if(agentRequest.getServiceLines()!=null && !agentRequest.getServiceLines().trim().equals(""))
			    	{
				String[] servLi = agentRequest.getServiceLines().split(",");
				int arrayLength3 = servLi.length;
				for (int i = 0; i < arrayLength3; i++) {
					String serLines = servLi[i];
					if (serLines.indexOf("(") == 0) {
						serLines = serLines.substring(1);
					}
					if (!serLines.equalsIgnoreCase("") && (serLines.lastIndexOf(")") == serLines.length() - 1)) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}
					if (serLines.indexOf("'") == 0) {
						serLines = serLines.substring(1);
					}
					if (!serLines.equalsIgnoreCase("") && (serLines.lastIndexOf("'") == serLines.length() - 1)) {
						serLines = serLines.substring(0, serLines.length() - 1);
					}

					multiplServiceLines.add(serLines.trim());
				}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			bstates = customerFileManager.findDefaultStateList(agentRequest.getBillingCountryCode(), sessionCorpID); 
			List terminalCountryCode = partnerManager.getCountryCode(agentRequest.getTerminalCountry());
			List mailIngCountryCode = partnerManager.getCountryCode(agentRequest.getMailingCountry());
			try {
				if (!terminalCountryCode.isEmpty()) {
					tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
				} else {
					tstates = customerFileManager.findDefaultStateList(agentRequest.getTerminalCountryCode(), sessionCorpID);
				}
				if (!mailIngCountryCode.isEmpty()) {
					mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
				} else {
					mstates = customerFileManager.findDefaultStateList(agentRequest.getMailingCountryCode(), sessionCorpID);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			agentCountryCode=partnerPublicManager.getCountryCode(agentRequest.getBillingCountry(),"COUNTRY",sessionCorpID); 
			if(agentRequest.getId()!=null && agentRequest.getPartnerId()!=null){
			getChangesRequested(agentRequest.getPartnerId(),agentRequest.getId());
			}
		} else { 
			getComboList(sessionCorpID);
			agentRequest = new AgentRequest();
			agentRequest.setStatus("New");
			agentRequest.setCounter("0");
			agentRequest.setPartnerId(null);
			bstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			tstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			mstates = customerFileManager.findDefaultStateList("", sessionCorpID);
			agentRequest.setCorpID(sessionCorpID);
			agentRequest.setCreatedOn(new Date());
			agentRequest.setUpdatedOn(new Date());
			if((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))){
					agentRequest.setIsAgent(true); 
			}
			if(partnerType!=null && partnerType.equals("AG")){
				agentRequest.setIsAgent(true);
			}
			if(chkAgentTrue!=null && chkAgentTrue.equalsIgnoreCase("Y")){
				agentRequest.setIsAgent(true);
			} 
		} 
		try {
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
			if (file != null) {

				if (file.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				

				//String rightUploadDir = uploadDir.replace("redsky", "userData");

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file.exists()) {
					InputStream stream = new FileInputStream(file);

					OutputStream bos = new FileOutputStream(uploadDir + fileFileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
					agentRequest.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
				}
			}
			if (file1 != null) {
				if (file1.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file1.exists()) {
					InputStream stream = new FileInputStream(file1);

					OutputStream bos = new FileOutputStream(uploadDir + file1FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
					agentRequest.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
				}
			}

			if (file2 != null) {
				if (file2.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file2.exists()) {
					InputStream stream = new FileInputStream(file2);

					OutputStream bos = new FileOutputStream(uploadDir + file2FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
					agentRequest.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
				}
			}

			if (file3 != null) {
				if (file3.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}

				/*String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";

				String rightUploadDir = uploadDir.replace("redsky", "userData");*/

				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file3.exists()) {
					InputStream stream = new FileInputStream(file3);

					OutputStream bos = new FileOutputStream(uploadDir + file3FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
					agentRequest.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
				}
			}

		} catch (Exception ex) {
			
			ex.printStackTrace();
		}
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		ownerOperatorList=partnerPublicManager.findOwnerOperatorList(parentId, sessionCorpID);		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	

	private String corpid;
	

	
private PartnerAccountRefManager partnerAccountRefManager;
	
	private PartnerAccountRef partnerAccountRef;
	@SkipValidation
	public void createPartnerAccountRef(AgentRequest partnerPublic,PartnerPrivate partnerPrivate , String refType){
		try{
			String permKey = sessionCorpID +"-"+"component.AcctRef.autoGenerateAccRefWithPartner";
			boolean visibilityForPUTT=false;
			 visibilityForPUTT=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
			 if(visibilityForPUTT){
			 List tempCount = partnerAccountRefManager.countChkAccountRef(partnerPrivate.getPartnerCode(),refType,sessionCorpID);
			 if(tempCount.get(0).toString().trim().equalsIgnoreCase("0")){
				    partnerAccountRef = new PartnerAccountRef();
		        	partnerAccountRef.setCorpID(sessionCorpID);    
		        	partnerAccountRef.setRefType(refType);
		        	String actg="";
		        	String PartnerCode=(partnerPrivate.getPartnerCode());
		        	if(PartnerCode.substring(0, 1).matches("[0-9]")){
						actg=PartnerCode;	
					}else{
					actg=PartnerCode.substring(1, PartnerCode.length());
					}
		        	String val = "000000";
					if(actg.toString().length()<=6){
					val = val.substring(actg.toString().length()); 
					actg=val+actg;
					}
					if(partnerAccountRef.getRefType()!=null && partnerAccountRef.getRefType().toString().trim().equalsIgnoreCase("R")){
					    actg="D"+actg;
					}else{
						actg="C"+actg;	
					}
					partnerAccountRef.setAccountCrossReference(actg);
		        	partnerAccountRef.setPartnerCode(partnerPrivate.getPartnerCode());
		        	partnerAccountRef.setUpdatedOn(new Date());
		        	partnerAccountRef.setCreatedOn(new Date());
		        	partnerAccountRef.setCreatedBy(getRequest().getRemoteUser());
			        partnerAccountRef.setUpdatedBy(getRequest().getRemoteUser());
			        //partnerAccountRef.setPartner(partner);  	
			        partnerAccountRef = partnerAccountRefManager.save(partnerAccountRef);
			 }
			 }

			}catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
		    	 e.printStackTrace(); 
			}
	}
	
	public String writeByteArraysToFile(String fileName, byte[] content,String userName) throws IOException {
		/*String uploadDir = "/usr/local/redskydoc/VOER/"+ userName+ "/";
		String sessionCorpID = "VOER";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + userName + "/");*/
		
		String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
		uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
		
        File file = new File(uploadDir);
        if (!file.exists()) {
        	file.mkdirs();
		}
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(uploadDir+fileName));
        writer.write(content);
        writer.flush();
        writer.close();
        
        return file.getAbsolutePath() + Constants.FILE_SEP + fileName;
 
    }
	
	@SkipValidation
	public String save() throws Exception { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	 
	    agentRequest.setIsAgent(true);  
		boolean isNew = (agentRequest.getId() == null); 
		String name=agentRequestManager.getUserCoprid(agentRequest.getCreatedBy());
		if (isNew)
		{
		agentRequest.setCorpID(sessionCorpID); 
		agentRequest.setCreatedBy(getRequest().getRemoteUser());
		agentRequest.setCreatedOn(new Date()); 
		agentRequest.setUpdatedBy(getRequest().getRemoteUser());
		agentRequest.setUpdatedOn(new Date()); 
		
		}
		else
		{
			
			agentRequest.setUpdatedOn(new Date()); 
			agentRequest.setUpdatedBy(getRequest().getRemoteUser()); 
		} 
		if (name.equals("TSFT"))
		{
		agentRequest.setCorpID(sessionCorpID); 
		}
		else
		{
			agentRequest.setCorpID(name);
			
		} 
		agentRequest.setCounter("0");
		getComboList(agentRequest.getCorpID()); 
		serviceLines = refMasterManager.findByParameterWithoutParent(agentRequest.getCorpID(), "SERVICELINES");
		
		List terminalCountryCode = partnerManager.getCountryCode(agentRequest.getTerminalCountry());
		if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
			agentRequest.setTerminalCountryCode(terminalCountryCode.get(0).toString());
			tstates = customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);
		} 
		List mailIngCountryCode = partnerManager.getCountryCode(agentRequest.getMailingCountry());

		if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
			agentRequest.setMailingCountryCode(mailIngCountryCode.get(0).toString());
			mstates = customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);
		}
		if(agentRequest.getBillingCountryCode()==null|| agentRequest.getBillingCountryCode().trim().equals(""))
		{
		List billingCountryCode = partnerManager.getCountryCode(agentRequest.getBillingCountryCode());
		if (!billingCountryCode.isEmpty() && (billingCountryCode.get(0)!=null)) {
			agentRequest.setBillingCountryCode(billingCountryCode.get(0).toString());
		}
		} 
		if (agentRequest.getBillingCountry() == null || agentRequest.getBillingCountry().trim().equals("")) {
			agentRequest.setBillingCountry("");
			agentRequest.setBillingCountryCode("");
		}
		if (agentRequest.getBillingCountryCode() == null || agentRequest.getBillingCountryCode().trim().equals("")) {
			
			agentRequest.setBillingCountryCode("");
		}
		if (agentRequest.getTerminalCountry() == null || agentRequest.getTerminalCountry().trim().equals("")) {
			agentRequest.setTerminalCountry("");
			agentRequest.setTerminalCountryCode("");
		}
		if (agentRequest.getMailingCountry() == null || agentRequest.getMailingCountry().trim().equals("")) {
			agentRequest.setMailingCountry("");
			agentRequest.setMailingCountryCode("");
		}
		if (agentRequest.getBillingState() == null) {
			agentRequest.setBillingState("");
		}
		 
		if (agentRequest.getTerminalCity() == null) {
			agentRequest.setTerminalCity("");
		}
		if (agentRequest.getTerminalCountryCode() == null) {
			agentRequest.setTerminalCountryCode("");
		}
		if (agentRequest.getTerminalState() == null) {
			agentRequest.setTerminalState("");
		}
		
		String changeFields="";
		Map<String,String> partnerPublicData = new HashMap<String,String>();
		Map<String,String> agentRequestData = new HashMap<String,String>();
		PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
		if(agentRequest.getId()!=null)
		{Long oldId = agentRequest.getId(); 
		String craetedBy=agentRequest.getCreatedBy();
		Date agentCreatedOn=agentRequest.getCreatedOn();
		 partnerPublic=partnerPublicManager.checkById(agentRequest.getPartnerId());
		//Long partnerId=agentRequest.getPartnerId();
		//String partnerCode=agentRequest.getPartnerCode();
		String comapnylogo=agentRequest.getCompanyLogo();
			agentRequestData = BeanUtils.describe(agentRequest);
			partnerPublicData = BeanUtils.describe(partnerPublic);
			
			 Iterator keysIt=partnerPublicData.entrySet().iterator();
			
			List<String> list = new ArrayList();
				while (keysIt.hasNext()) {
					Map.Entry entry = (Map.Entry) keysIt.next();
					String key = (String) entry.getKey(); 
					
							
				if(partnerPublicData.containsKey(key) && agentRequestData.containsKey(key)){
					if((partnerPublicData.get(key) !=null && agentRequestData.get(key)!=null && (!(partnerPublicData.get(key).toString().equalsIgnoreCase(agentRequestData.get(key).toString())))) 
							|| ((partnerPublicData.get(key) == null || partnerPublicData.get(key).trim().equals("")) && ((agentRequestData.get(key)!=null) && (!(agentRequestData.get(key).trim().equals("")))))){
						if(!(key.trim().equalsIgnoreCase("corpID")) && !(key.trim().equalsIgnoreCase("updatedOn")) && !(key.trim().equalsIgnoreCase("updatedby")) && !(key.trim().equalsIgnoreCase("createdBy")) && !(key.trim().equalsIgnoreCase("createdOn")) && !(key.trim().equalsIgnoreCase("id"))  && !(key.trim().equalsIgnoreCase("status")) )
							if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
								  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(agentRequest, key.trim()));
								}
							changeFields=changeFields+""+key+",";
						
					}
				}
				}
		   
			if (changeFields.endsWith(",")) {
				changeFields = changeFields.substring(0, changeFields.length() - 1);
				System.out.println(changeFields);
				} }
	agentRequest.setChangeRequestColumn(changeFields);
		try {
			String uploadDir = ServletActionContext.getServletContext().getRealPath("/images") + "/";
			uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc"  + "/" + "userData" + "/" + "images" + "/" );
			if (file != null) {

				if (file.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}  
				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file.exists()) {
					InputStream stream = new FileInputStream(file);

					OutputStream bos = new FileOutputStream(uploadDir + fileFileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
					agentRequest.setLocation1(dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName);
					
				}
			}
			if (file6 != null) {

				if (file6.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				} 
				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file6.exists()) {
					InputStream stream = new FileInputStream(file6);

					OutputStream bos = new FileOutputStream(uploadDir + file4FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file4FileName);
					agentRequest.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file4FileName);
					
				}
			}
			
			if(imageDataFileLogo !=null && (!(imageDataFileLogo.trim().equals("")))) {
				
				imageDataFileLogo= imageDataFileLogo.substring(imageDataFileLogo.indexOf("base64,")+7);
				
				byte[] base64EncodedData = Base64.decodeBase64(imageDataFileLogo.getBytes());
				 
		        try {
		      String  filePath =writeByteArraysToFile(UUID.randomUUID().toString()+".jpg", base64EncodedData,"admin");
		      agentRequest.setCompanyLogo(filePath);
				} catch (IOException ex) {
					 
					ex.printStackTrace();
				} 
		        }
			
			if (file1 != null) {
				if (file1.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				}  
				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file1.exists()) {
					InputStream stream = new FileInputStream(file1);

					OutputStream bos = new FileOutputStream(uploadDir + file1FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
					agentRequest.setLocation2(dirPath.getAbsolutePath() + Constants.FILE_SEP + file1FileName);
				}
			}

			if (file2 != null) {
				if (file2.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				} 
				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file2.exists()) {
					InputStream stream = new FileInputStream(file2);

					OutputStream bos = new FileOutputStream(uploadDir + file2FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
					agentRequest.setLocation3(dirPath.getAbsolutePath() + Constants.FILE_SEP + file2FileName);
				}
			}

			if (file3 != null) {
				if (file3.length() > 41943040) {
					addActionError(getText("maxLengthExceeded"));
					return INPUT;
				} 
				File dirPath = new File(uploadDir);
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}

				if (file3.exists()) {
					InputStream stream = new FileInputStream(file3);

					OutputStream bos = new FileOutputStream(uploadDir + file3FileName);
					int bytesRead = 0;
					byte[] buffer = new byte[8192];

					while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
						bos.write(buffer, 0, bytesRead);
					}

					bos.close();
					stream.close();

					getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
					agentRequest.setLocation4(dirPath.getAbsolutePath() + Constants.FILE_SEP + file3FileName);
				}
			}

		} catch (Exception ex) {
			System.out.println("\n\n\n\n\n---- >>>> error while uploading photograph" + ex);
			ex.printStackTrace();
		} 
         //agentRequest.setPartnerCode("");
         agentRequest = agentRequestManager.save(agentRequest);
		
		try{
		selectedJobList = new ArrayList(); 
		String tv1 = new String();
		if(agentRequest.getTypeOfVendor()!=null && !agentRequest.getTypeOfVendor().trim().equals(""))
		{
		tv1 = agentRequest.getTypeOfVendor();
		if (tv1.indexOf(",") == 0) {
			tv1 = tv1.substring(1);
		}
		String[] ac = tv1.split(",");
		int arrayLength2 = ac.length;	
		for (int j = 0; j < arrayLength2; j++) {
			String qacCerti = ac[j];
			if (qacCerti.indexOf("(") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf(")") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}
			if (qacCerti.indexOf("'") == 0) {
				qacCerti = qacCerti.substring(1);
			}
			if (qacCerti.lastIndexOf("'") == qacCerti.length() - 1) {
				qacCerti = qacCerti.substring(0, qacCerti.length() - 1);
			}

			selectedJobList.add(qacCerti.trim());
		}
		}
		agentRequest.setTypeOfVendor(selectedJobList.toString().replace("[", "").replace("]", ""));
		}catch(Exception e){
			e.printStackTrace();
		}
			donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, null);
			if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
				donoFlag="1";
			} 
		bstates = customerFileManager.findDefaultStateList(agentRequest.getBillingCountryCode(), sessionCorpID);
		tstates = customerFileManager.findDefaultStateList(agentRequest.getTerminalCountryCode(), sessionCorpID);
		mstates = customerFileManager.findDefaultStateList(agentRequest.getMailingCountryCode(), sessionCorpID);
		try {
			multiplequalityCertifications = new ArrayList();
			multiplVanlineAffiliations = new ArrayList();
			multiplServiceLines = new ArrayList();
			
			
			String va1 = new String();
			if(agentRequest.getVanLineAffiliation()!=null && !agentRequest.getVanLineAffiliation().trim().equals(""))
			{
			va1 = agentRequest.getVanLineAffiliation().trim();
			if (va1.indexOf(",") == 0) {
				va1 = va1.substring(1);
			}
			String[] va = va1.split(",");
			int arrayLength2 = va.length;
			for (int k = 0; k < arrayLength2; k++) {
				String vanCer = va[k];
				if (vanCer.indexOf("(") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf(")") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}
				if (vanCer.indexOf("'") == 0) {
					vanCer = vanCer.substring(1);
				}
				if (vanCer.lastIndexOf("'") == vanCer.length() - 1) {
					vanCer = vanCer.substring(0, vanCer.length() - 1);
				}

				multiplVanlineAffiliations.add(vanCer.trim());
			}
			}
			agentRequest.setVanLineAffiliation(multiplVanlineAffiliations.toString().replace("[", "").replace("]", ""));
			String servLi1 = new String();
			if(agentRequest.getServiceLines()!=null && !agentRequest.getServiceLines().trim().equals(""))
			{
			servLi1 = agentRequest.getServiceLines().trim();
			if (servLi1.indexOf(",") == 0) {
				servLi1 = servLi1.substring(1);
			}
			String[] servLi = servLi1.split(",");
			int arrayLength3 = servLi.length;
			for (int l = 0; l < arrayLength3; l++) {
				String serLines = servLi[l];
				if (serLines.indexOf("(") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf(")") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}
				if (serLines.indexOf("'") == 0) {
					serLines = serLines.substring(1);
				}
				if (serLines.lastIndexOf("'") == serLines.length() - 1) {
					serLines = serLines.substring(0, serLines.length() - 1);
				}

				multiplServiceLines.add(serLines.trim());
			}
			}
			agentRequest.setServiceLines(multiplServiceLines.toString().replace("[", "").replace("]", ""));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		donotMergelist=partnerPublicManager.getFindDontNerge(sessionCorpID, null);
		if(donotMergelist!=null && !donotMergelist.isEmpty()&& !donotMergelist.contains("null")){
			donoFlag="1";
		} 
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		hitflag = "1"; 
		ownerOperatorList=partnerPublicManager.findOwnerOperatorList(parentId, sessionCorpID);
		if(checkCodefromDefaultAccountLine!=null && !checkCodefromDefaultAccountLine.equalsIgnoreCase("")){
			try{
			String[] templateDetail = checkCodefromDefaultAccountLine.split("~");
       	 	String checkFromTemplate= templateDetail[0];
       	 	String partnerType= templateDetail[1];
       	 	String userName=getRequest().getRemoteUser();
       	 	
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
	 
		    String url=getRequest().getRequestURL().toString();
			String  requestURL=url.substring(0,url.lastIndexOf("/"));
			String msgText1="";
			String subject="";
			 String link = "";
			 String website="";
			 website=requestURL;
			 website=website+"/pricingList.html?";
				link = "<a href=\""+website+"\"></a>"; 
				String key = "Agent update request has been received. We will verify the changes and get back to you. To track the changes, use the Agent Change Request tab.";
			    saveMessage(getText(key));	
		flagHit="2";		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String cartonEditURL;
	private Set agentRequestList; 
	private List agentRequestLists;
	private Set viewAgentList;
	private List viewAgentLists;
	 @SkipValidation
	    public String findAgentRequest() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start"); 
			String parameter = (String) getRequest().getSession().getAttribute("paramView");
			getRequest().setAttribute("soLastName","");
			if (parameter != null && parameter.equalsIgnoreCase("View")) {
				getRequest().getSession().removeAttribute("paramView");
			}

			getComboList(sessionCorpID);
			agentRequest = new AgentRequest();
			
			agentRequest.setIsAgent(true);
			
			isIgnoreInactive = true;
			agentRequestList = new HashSet(agentRequestManager.getPartnerPublicList(sessionCorpID, "", "","", "","", "",false,counter));
			String key = "Please enter your search criteria below";
			saveMessage(getText(key));		
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	   
	 @SuppressWarnings({ "rawtypes", "unchecked" })
	    @SkipValidation
	    public String searchAgentRequest() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
			getRequest().setAttribute("soLastName","");
			/*String parameter = (String) getRequest().getSession().getAttribute("paramView");
			if (parameter != null && parameter.equalsIgnoreCase("View")) {
				getRequest().getSession().removeAttribute("paramView");
			}*/ 
			getComboList(sessionCorpID);
			try {
				
				String lastName;
				String aliasName;
				String billingCountryCode;
				String billingStateCode;
				String billingCountry;
				String status; 
				if (agentRequest != null) { 
					lastName = agentRequest.getLastName() == null ? "" : agentRequest.getLastName();			
					aliasName=agentRequest.getAliasName()==null ? "" :agentRequest.getAliasName();
					billingCountryCode = countryCodeSearch == null ? "" : countryCodeSearch;
					billingStateCode = stateSearch == null ? "" : stateSearch;
					billingCountry = countrySearch == null ? "" : countrySearch;
					status = agentRequest.getStatus() == null ? "" : agentRequest.getStatus();
					if(status.equalsIgnoreCase("ReRequest"))
					{
						counter="1";
						status="Rejected";
					}
					else
					{
						counter="0";
					}
					
					corpid=corpid==null?"" : corpid;
					
					agentRequestList = new LinkedHashSet(agentRequestManager.getPartnerPublicList(corpid,lastName,aliasName, billingCountryCode, billingCountry, billingStateCode, status, true,counter));
				} else {
					agentRequest = new AgentRequest();
					
					agentRequest.setIsAgent(true);
					isIgnoreInactive = true;

					agentRequestList = new LinkedHashSet(agentRequestManager.getPartnerPublicList("", "", "","", "", "","New" ,true,""));
				}
			} catch (Exception e) {
				agentRequest = new AgentRequest();
				
				isIgnoreInactive = true;
				agentRequestList = new LinkedHashSet(agentRequestManager.getPartnerPublicList("", "", "","", "", "", "",false,""));
				String key = "Please enter your search criteria below";
				saveMessage(getText(key));
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	
	   
	   @SkipValidation
	    public String viewAgentDetail() { 
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"+id);

			String lastName = "";
			String aliasName = "";
			
			String billingCountry = "";
			String billingCity = "";
			String billingState="";
			String billingEmail="";
			 String billingZip="";
			 String billingAddress1="";
			  agentRequest = agentRequestManager.get(id);
			 if (!(agentRequest == null)) { 
				if ((agentRequest.getLastName() == null)) {
					lastName = "";
				} else {
					lastName = agentRequest.getLastName();
				}
				if ((agentRequest.getAliasName() == null)) {
					aliasName = "";
				} else {
					aliasName = agentRequest.getAliasName();
				}
				if ((agentRequest.getBillingCountry() == null)) {
					billingCountry = "";
				} else {
					billingCountry = agentRequest.getBillingCountry();
				}
				if ((agentRequest.getBillingCity() == null)) {
					billingCity = "";
				} else {
					billingCity = agentRequest.getBillingCity();
				}
				if ((agentRequest.getBillingState() == null)) {
					billingState = "";
				} else {
					billingState = agentRequest.getBillingState();
				}
				if ((agentRequest.getBillingEmail() == null)) {
					billingEmail = "";
				} else {
					billingEmail = agentRequest.getBillingEmail();
				}
				if ((agentRequest.getBillingZip() == null)) {
					billingZip = "";
				} else {
					billingZip = agentRequest.getBillingZip();
				}
				if ((agentRequest.getBillingAddress1() == null)) {
					billingAddress1 = "";
				} else {
					billingAddress1 = agentRequest.getBillingAddress1();
				}
				
				if ((agentRequest.getPartnerId() == null)) {
					partnerId =null;
				} else {
					partnerId = agentRequest.getPartnerId();
				}
				
		          viewAgentList=new HashSet(agentRequestManager.getAgentRequestDetailList(sessionCorpID,lastName,aliasName,billingCountry, billingCity,billingState,"APPROVED",billingEmail,true,billingZip,billingAddress1,partnerId));
		          viewAgentLists = new ArrayList();
		          viewAgentLists.addAll(viewAgentList);
		     
		     } 
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;   
	    }
	   
	   private Long partnerId;
	   private Long agentId;
	   private String agentStatus;
	   private String createdBy;
	   private String email;
	   private String signatureNote;
	   private String fullName;
	   private String lastName;
	   private String partnerCode;
	   private List partnerlist;
	   public List getPartnerlist() {
		return partnerlist;
	}

	public void setPartnerlist(List partnerlist) {
		this.partnerlist = partnerlist;
	}
	

	@SkipValidation
	    public String updatedAgentData() throws Exception  {
		   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"+id);
		   
		  agentRequest = agentRequestManager.get(agentId);
		  if(agentStatus.equals("Approved"))
		  {
			 
			  agentRequestManager.updateAgentRecord(sessionCorpID,agentRequest.getLastName(),agentRequest.getAliasName(),agentRequest.getBillingCountry(), agentRequest.getBillingCity(),agentRequest.getBillingState(),agentStatus,agentRequest.getBillingEmail(),true,agentRequest.getBillingZip(),agentRequest.getBillingAddress1(),getRequest().getRemoteUser(),agentRequest.getId(),agentRequest.getCreatedBy());
				User user1 = userManager.getUserByUsername(agentRequest.getUpdatedBy());
		//partnerPrivate
		 
	
		  boolean isNew = (agentRequest.getId() == null);
		  partnerPublic=partnerPublicManager.checkById(agentRequest.getPartnerId());
			 if(partnerPublic!=null && partnerPublic.getId()!=null)
			 {
				 String strPartner[] = agentRequest.getChangeRequestColumn().split(",");
			 /*Long oldId = agentRequest.getId();
				String oldUpdatedby = agentRequest.getUpdatedBy();
				String craetedBy=agentRequest.getCreatedBy();
				Long partnerId=agentRequest.getPartnerId();
				String partnerCode=agentRequest.getPartnerCode();
				String comapnylogo=agentRequest.getCompanyLogo();*/
			    PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
			    Map<String,String> partnerPublicData = new HashMap<String,String>();
				Map<String,String> agentRequestData = new HashMap<String,String>();
				//partnerPublicData = BeanUtils.describe(partnerPublic);
				Date ppCreatedOn = partnerPublic.getCreatedOn();
				String ppCreatedBy = partnerPublic.getCreatedBy();
				agentRequestData = BeanUtils.describe(agentRequest); 
				for(String h:strPartner){
							if(h !=null &&  (!(h.trim().equals(""))) && (!(h.trim().equalsIgnoreCase("id"))) && (!(h.trim().equalsIgnoreCase("partnerCode"))) && (!(h.trim().equalsIgnoreCase("CorpID"))) && (!(h.trim().equalsIgnoreCase("status"))) && (!(h.trim().equalsIgnoreCase("isAccount"))) && (!(h.trim().equalsIgnoreCase("isAgent"))) && (!(h.trim().equalsIgnoreCase("isCarrier"))) && (!(h.trim().equalsIgnoreCase("isVendor"))) && (!(h.trim().equalsIgnoreCase("isOwnerOp"))) && (!(h.trim().equalsIgnoreCase("isPrivateParty")))) {
						        if( agentRequestData.containsKey(h) && agentRequestData.get(h)!=null){
						        	
							try {
					        	if(beanUtilsBean.isWriteable(partnerPublic, h)) {
					             beanUtilsBean.setProperty(partnerPublic,h.toString().trim(),beanUtilsBean.getProperty(agentRequest, h.trim()));
					        	
					        	}}catch(Exception e) {
									e.printStackTrace();
								}
						    
	        	}}}
							
						 
				if((agentRequest.getTerminalAddress1() == null && agentRequest.getTerminalAddress2() == null && agentRequest.getTerminalAddress3() == null &&  agentRequest.getTerminalAddress4() == null && agentRequest.getTerminalCountry() == null && agentRequest.getTerminalCity() == null && agentRequest.getTerminalEmail() == null && agentRequest.getTerminalFax() == null && agentRequest.getTerminalPhone() == null && agentRequest.getTerminalZip() == null)||(agentRequest.getTerminalAddress1().trim().equals("") && agentRequest.getTerminalAddress2().trim().equals("") && agentRequest.getTerminalAddress3().trim().equals("") && agentRequest.getTerminalAddress4().trim().equals("") && agentRequest.getTerminalCountry().trim().equals("") && agentRequest.getTerminalCity().trim().equals("") && agentRequest.getTerminalEmail().trim().equals("") && agentRequest.getTerminalFax().trim().equals("") && agentRequest.getTerminalPhone().trim().equals("") && agentRequest.getTerminalZip().trim().equals("")))	
				{
					agentRequest.setTerminalAddress1(agentRequest.getBillingAddress1());
					agentRequest.setTerminalAddress2(agentRequest.getBillingAddress2());
					agentRequest.setTerminalAddress3(agentRequest.getBillingAddress3());
					agentRequest.setTerminalAddress4(agentRequest.getBillingAddress4());
					agentRequest.setTerminalCountry(agentRequest.getBillingCountry());
					agentRequest.setTerminalCity(agentRequest.getBillingCity());
					agentRequest.setTerminalPhone(agentRequest.getBillingPhone());
					agentRequest.setTerminalZip(agentRequest.getBillingZip());
					agentRequest.setTerminalFax(agentRequest.getBillingFax());
					agentRequest.setTerminalEmail(agentRequest.getBillingEmail());
					agentRequest.setTerminalState(agentRequest.getBillingState());
					partnerPublic.setTerminalAddress1(agentRequest.getBillingAddress1());
					partnerPublic.setTerminalAddress2(agentRequest.getBillingAddress2());
					partnerPublic.setTerminalAddress3(agentRequest.getBillingAddress3());
					partnerPublic.setTerminalAddress4(agentRequest.getBillingAddress4());
					partnerPublic.setTerminalCountry(agentRequest.getBillingCountry());
					partnerPublic.setTerminalCity(agentRequest.getBillingCity());
					partnerPublic.setTerminalPhone(agentRequest.getBillingPhone());
					partnerPublic.setTerminalZip(agentRequest.getBillingZip());
					partnerPublic.setTerminalFax(agentRequest.getBillingFax());
					partnerPublic.setTerminalEmail(agentRequest.getBillingEmail());
					partnerPublic.setTerminalState(agentRequest.getBillingState());
				}
				if((agentRequest.getMailingAddress1() == null && agentRequest.getMailingAddress2() == null && agentRequest.getMailingAddress3() == null &&  agentRequest.getMailingAddress4() == null && agentRequest.getMailingCountry() == null && agentRequest.getMailingCity() == null && agentRequest.getMailingEmail() == null && agentRequest.getMailingFax() == null && agentRequest.getMailingPhone() == null && agentRequest.getMailingZip() == null)||(agentRequest.getMailingAddress1().trim().equals("") && agentRequest.getMailingAddress2().trim().equals("") && agentRequest.getMailingAddress3().trim().equals("") && agentRequest.getMailingAddress4().trim().equals("") && agentRequest.getMailingCountry().trim().equals("") && agentRequest.getMailingCity().trim().equals("") && agentRequest.getMailingEmail().trim().equals("") && agentRequest.getMailingFax().trim().equals("") && agentRequest.getMailingPhone().trim().equals("") && agentRequest.getMailingZip().trim().equals("")))	
				{
				
					agentRequest.setMailingAddress1(agentRequest.getBillingAddress1());
					agentRequest.setMailingAddress2(agentRequest.getBillingAddress2());
					agentRequest.setMailingAddress3(agentRequest.getBillingAddress3());
					agentRequest.setMailingAddress4(agentRequest.getBillingAddress4());
					agentRequest.setMailingCountry(agentRequest.getBillingCountry());
					agentRequest.setMailingCity(agentRequest.getBillingCity());
					agentRequest.setMailingPhone(agentRequest.getBillingPhone());
					agentRequest.setMailingZip(agentRequest.getBillingZip());
					agentRequest.setMailingFax(agentRequest.getBillingFax());
					agentRequest.setMailingEmail(agentRequest.getBillingEmail());
					agentRequest.setMailingState(agentRequest.getBillingState());
					partnerPublic.setMailingAddress1(agentRequest.getBillingAddress1());
					partnerPublic.setMailingAddress2(agentRequest.getBillingAddress2());
					partnerPublic.setMailingAddress3(agentRequest.getBillingAddress3());
					partnerPublic.setMailingAddress4(agentRequest.getBillingAddress4());
					partnerPublic.setMailingCountry(agentRequest.getBillingCountry());
					partnerPublic.setMailingCity(agentRequest.getBillingCity());
					partnerPublic.setMailingPhone(agentRequest.getBillingPhone());
					partnerPublic.setMailingZip(agentRequest.getBillingZip());
					partnerPublic.setMailingFax(agentRequest.getBillingFax());
					partnerPublic.setMailingEmail(agentRequest.getBillingEmail());
					partnerPublic.setMailingState(agentRequest.getBillingState());
					
				}
				agentRequest.setChangeRequestColumn("");
			/*	partnerPublic.setLastName(agentRequest.getLastName());*/
				partnerPublic.setIsAgent(true);
				partnerPublic.setIsAccount(false);
				partnerPublic.setIsCarrier(false);
				partnerPublic.setIsVendor(false);
				partnerPublic.setIsOwnerOp(false);
				partnerPublic.setIsPrivateParty(false);
				partnerPublic.setCorpID("TSFT");
				partnerPublic.setCreatedOn(ppCreatedOn);
				partnerPublic.setCreatedBy(ppCreatedBy);
				
			    partnerPublic.setUpdatedOn(new Date());
			   
			    partnerPublic.setUpdatedBy(agentRequest.getUpdatedBy());
				if (agentRequest.getBillingCountry() == null || agentRequest.getBillingCountry().trim().equals("")) {
					partnerPublic.setBillingCountry("");
					partnerPublic.setBillingCountryCode("");
				}
				if (agentRequest.getTerminalCountry() == null || agentRequest.getTerminalCountry().trim().equals("")) {
					partnerPublic.setTerminalCountry("");
					partnerPublic.setTerminalCountryCode("");
				}
				if (agentRequest.getMailingCountry() == null || agentRequest.getMailingCountry().trim().equals("")) {
					partnerPublic.setMailingCountry("");
					partnerPublic.setMailingCountryCode("");
				}
				if (agentRequest.getBillingState() == null) {
					partnerPublic.setBillingState("");
				}
				 
				if (agentRequest.getTerminalCity() == null) {
					partnerPublic.setTerminalCity("");
				}
				if (agentRequest.getTerminalCountryCode() == null) {
					partnerPublic.setTerminalCountryCode("");
				}
				if (agentRequest.getTerminalState() == null) {
					partnerPublic.setTerminalState("");
				} 
				agentRequest.setPartnerId(partnerPublic.getId());
				agentRequest.setStatus(agentStatus);
				agentRequest.setCounter("0");
				agentRequest.setPartnerCode(partnerPublic.getPartnerCode());
				agentRequest.setUpdatedBy(getRequest().getRemoteUser());
				agentRequest.setUpdatedOn(new Date());
				partnerPublic.setLocation1(agentRequest.getLocation1());
				partnerPublic.setLocation2(agentRequest.getLocation2());
				partnerPublic.setLocation3(agentRequest.getLocation3());
				partnerPublic.setLocation4(agentRequest.getLocation4());
				
				agentRequest = agentRequestManager.save(agentRequest);
				if(partnerPublic.getAgentParent()==null || partnerPublic.getAgentParent().equals("") )
				{
					partnerPublic.setAgentParent(agentRequest.getPartnerCode());
					partnerPublic.setAgentParentName(agentRequest.getLastName());
					
				}
				/*String maxPartnerCode;
				try {
					if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
						maxPartnerCode = "T10001";
					} else {
						List maxCode = partnerPublicManager.findMaxByCode("T");

						maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
						if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
							maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
						}
					}
				} catch (Exception ex) {
					maxPartnerCode = "T10001";
					ex.printStackTrace();
				}
*/           if (agentRequest.getVanLineAffiliation() == null || agentRequest.getVanLineAffiliation().trim().equals("")) {
	              partnerPublic.setVanLineAffiliation("");
                 } 
				partnerPublic = partnerPublicManager.save(partnerPublic);
				partnerPublicManager.updateAgentParentName(partnerPublic.getLastName(),partnerPublic.getPartnerCode(),partnerPublic.getUpdatedBy());
				/*List partnerPrivateListForDiffCorpId =partnerPrivateManager.getPartnerPrivateId(partnerPublic.getPartnerCode());
			   
				Iterator itPartnerPrivate = partnerPrivateListForDiffCorpId.iterator();
				while(itPartnerPrivate.hasNext()) {
					String idValue = itPartnerPrivate.next().toString();
					Long idPartnerPrivate=Long.parseLong(idValue);
					PartnerPrivate partnerPrivateForDiffCorpId = partnerPrivateManager.checkById(idPartnerPrivate);
					createPartnerPrivate(partnerPrivateForDiffCorpId);
						}*/
											
			      
					
			  
				
}
			 else
			 {
			partnerPublic = new PartnerPublic();
			/*Long oldId = agentRequest.getId();
			String oldUpdatedby = agentRequest.getUpdatedBy();
			String craetedBy=agentRequest.getCreatedBy();
			Long partnerId=agentRequest.getPartnerId();
			String partnerCode=agentRequest.getPartnerCode();
			String comapnylogo=agentRequest.getCompanyLogo();*/
		    PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
		    Map<String,String> partnerPublicData = new HashMap<String,String>();
			Map<String,String> agentRequestData = new HashMap<String,String>();
			//partnerPublicData = BeanUtils.describe(partnerPublic);
			agentRequestData = BeanUtils.describe(agentRequest);
				
				 Iterator keysIt=agentRequestData.entrySet().iterator();
					while (keysIt.hasNext()) {
						try {
						Map.Entry entry = (Map.Entry) keysIt.next();
						String key = (String) entry.getKey(); 
				
						 if(agentRequestData.containsKey(key) && agentRequestData.get(key)!=null ){ 
				             //BeanUtils.copyProperties(agentRequest,partnerPublics);
							 if(key !=null &&  (!(key.trim().equals(""))) && (!(key.trim().equalsIgnoreCase("id"))) && (!(key.trim().equalsIgnoreCase("CorpID"))) && (!(key.trim().equalsIgnoreCase("status"))) && (!(key.trim().equalsIgnoreCase("isAccount"))) && (!(key.trim().equalsIgnoreCase("isAgent"))) && (!(key.trim().equalsIgnoreCase("isCarrier"))) && (!(key.trim().equalsIgnoreCase("isVendor"))) && (!(key.trim().equalsIgnoreCase("isOwnerOp"))) && (!(key.trim().equalsIgnoreCase("isPrivateParty")))) {
							 if(beanUtilsBean.isWriteable(partnerPublic, key)) {
							 beanUtilsBean.setProperty(partnerPublic,key.toString().trim(),beanUtilsBean.getProperty(agentRequest, key.trim())); 
							 }
							 }
					    }
						}catch(Exception e) {
							e.printStackTrace();
						}
					}
					if((agentRequest.getTerminalAddress1() == null && agentRequest.getTerminalAddress2() == null && agentRequest.getTerminalAddress3() == null &&  agentRequest.getTerminalAddress4() == null && agentRequest.getTerminalCountry() == null && agentRequest.getTerminalCity() == null && agentRequest.getTerminalEmail() == null && agentRequest.getTerminalFax() == null && agentRequest.getTerminalPhone() == null && agentRequest.getTerminalZip() == null)||(agentRequest.getTerminalAddress1().trim().equals("") && agentRequest.getTerminalAddress2().trim().equals("")  && agentRequest.getTerminalAddress3().trim().equals("") && agentRequest.getTerminalAddress4().trim().equals("")&& agentRequest.getTerminalCountry().trim().equals("") && agentRequest.getTerminalCity().trim().equals("") && agentRequest.getTerminalEmail().trim().equals("") && agentRequest.getTerminalFax().trim().equals("") && agentRequest.getTerminalPhone().trim().equals("") && agentRequest.getTerminalZip().trim().equals("")))	
					{
						agentRequest.setTerminalAddress1(agentRequest.getBillingAddress1());
						agentRequest.setTerminalAddress2(agentRequest.getBillingAddress2());
						agentRequest.setTerminalAddress3(agentRequest.getBillingAddress3());
						agentRequest.setTerminalAddress4(agentRequest.getBillingAddress4());
						agentRequest.setTerminalCountry(agentRequest.getBillingCountry());
						agentRequest.setTerminalCity(agentRequest.getBillingCity());
						agentRequest.setTerminalPhone(agentRequest.getBillingPhone());
						agentRequest.setTerminalZip(agentRequest.getBillingZip());
						agentRequest.setTerminalFax(agentRequest.getBillingFax());
						agentRequest.setTerminalEmail(agentRequest.getBillingEmail());
						agentRequest.setTerminalState(agentRequest.getBillingState());
						agentRequest.setTerminalCountryCode(agentRequest.getBillingCountryCode());
						partnerPublic.setTerminalAddress1(agentRequest.getBillingAddress1());
						partnerPublic.setTerminalAddress2(agentRequest.getBillingAddress2());
						partnerPublic.setTerminalAddress3(agentRequest.getBillingAddress3());
						partnerPublic.setTerminalAddress4(agentRequest.getBillingAddress4());
						partnerPublic.setTerminalCountry(agentRequest.getBillingCountry());
						partnerPublic.setTerminalCity(agentRequest.getBillingCity());
						partnerPublic.setTerminalPhone(agentRequest.getBillingPhone());
						partnerPublic.setTerminalZip(agentRequest.getBillingZip());
						partnerPublic.setTerminalFax(agentRequest.getBillingFax());
						partnerPublic.setTerminalEmail(agentRequest.getBillingEmail());
						partnerPublic.setTerminalState(agentRequest.getBillingState());
						partnerPublic.setTerminalCountryCode(agentRequest.getBillingCountryCode());
					}
					if((agentRequest.getMailingAddress1() == null && agentRequest.getMailingAddress2() == null && agentRequest.getMailingAddress3() == null &&  agentRequest.getMailingAddress4() == null && agentRequest.getMailingCountry() == null && agentRequest.getMailingCity() == null && agentRequest.getMailingEmail() == null && agentRequest.getMailingFax() == null && agentRequest.getMailingPhone() == null && agentRequest.getMailingZip() == null)||(agentRequest.getMailingAddress1().trim().equals("") && agentRequest.getMailingAddress2().trim().equals("") && agentRequest.getMailingAddress3().trim().equals("") && agentRequest.getMailingAddress4().trim().equals("") && agentRequest.getMailingCountry().trim().equals("") && agentRequest.getMailingCity().trim().equals("") && agentRequest.getMailingEmail().trim().equals("") && agentRequest.getMailingFax().trim().equals("") && agentRequest.getMailingPhone().trim().equals("") && agentRequest.getMailingZip().trim().equals("")))	
					{
					
						agentRequest.setMailingAddress1(agentRequest.getBillingAddress1());
						agentRequest.setMailingAddress2(agentRequest.getBillingAddress2());
						agentRequest.setMailingAddress3(agentRequest.getBillingAddress3());
						agentRequest.setMailingAddress4(agentRequest.getBillingAddress4());
						agentRequest.setMailingCountry(agentRequest.getBillingCountry());
						agentRequest.setMailingCity(agentRequest.getBillingCity());
						agentRequest.setMailingPhone(agentRequest.getBillingPhone());
						agentRequest.setMailingZip(agentRequest.getBillingZip());
						agentRequest.setMailingFax(agentRequest.getBillingFax());
						agentRequest.setMailingEmail(agentRequest.getBillingEmail());
						agentRequest.setMailingState(agentRequest.getBillingState());
						agentRequest.setMailingCountryCode(agentRequest.getBillingCountryCode());
						partnerPublic.setMailingAddress1(agentRequest.getBillingAddress1());
						partnerPublic.setMailingAddress2(agentRequest.getBillingAddress2());
						partnerPublic.setMailingAddress3(agentRequest.getBillingAddress3());
						partnerPublic.setMailingAddress4(agentRequest.getBillingAddress4());
						partnerPublic.setMailingCountry(agentRequest.getBillingCountry());
						partnerPublic.setMailingCity(agentRequest.getBillingCity());
						partnerPublic.setMailingPhone(agentRequest.getBillingPhone());
						partnerPublic.setMailingZip(agentRequest.getBillingZip());
						partnerPublic.setMailingFax(agentRequest.getBillingFax());
						partnerPublic.setMailingEmail(agentRequest.getBillingEmail());
						partnerPublic.setMailingState(agentRequest.getBillingState());
						partnerPublic.setMailingCountryCode(agentRequest.getBillingCountryCode());
						
					}
					agentRequest.setChangeRequestColumn("");
			partnerPublic.setReloContact("");
			partnerPublic.setIsAgent(true);
			
			partnerPublic.setIsAccount(false);
			partnerPublic.setIsCarrier(false);
			partnerPublic.setIsVendor(false);
			partnerPublic.setIsOwnerOp(false);
			partnerPublic.setIsPrivateParty(false);
     
			partnerPublic.setCorpID("TSFT");
			partnerPublic.setStatus(agentStatus);
			partnerPublic.setFirstName("");
			
			partnerPublic.setCreatedOn(new Date());
			partnerPublic.setUpdatedOn(new Date());
			partnerPublic.setCreatedBy(agentRequest.getUpdatedBy());
			partnerPublic.setUpdatedBy(agentRequest.getUpdatedBy());
			partnerPublic.setTypeOfVendor(partnerPublic.getTypeOfVendor());
			
			String maxPartnerCode;
			try {
				if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
					maxPartnerCode = "T10001";
				} else {
					List maxCode = partnerPublicManager.findMaxByCode("T");

					maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
					if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
						maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
					}
				}
			} catch (Exception ex) {
				maxPartnerCode = "T10001";
				ex.printStackTrace();
			}

			partnerPublic.setPartnerCode(maxPartnerCode);
			if(partnerPublic.getVanLineAffiliation()==null)
			{
				partnerPublic.setVanLineAffiliation("");
				
			}
			partnerPublic = partnerPublicManager.save(partnerPublic);
			agentRequest.setPartnerId(partnerPublic.getId());
			agentRequest.setStatus(agentStatus);
			agentRequest.setCounter("0");
			agentRequest.setPartnerCode(partnerPublic.getPartnerCode());
			agentRequest.setUpdatedBy(getRequest().getRemoteUser());
			agentRequest.setUpdatedOn(new Date());
			agentRequest = agentRequestManager.save(agentRequest);
			partnerPublic.setLocation1(agentRequest.getLocation1());
			partnerPublic.setLocation2(agentRequest.getLocation2());
			partnerPublic.setLocation3(agentRequest.getLocation3());
			partnerPublic.setLocation4(agentRequest.getLocation4());
			if(partnerPublic.getAgentParent()==null || partnerPublic.getAgentParent().equals("") )
			{
				partnerPublic.setAgentParent(agentRequest.getPartnerCode());
				partnerPublic.setAgentParentName(agentRequest.getLastName());
				
			}
			if (agentRequest.getVanLineAffiliation() == null || agentRequest.getVanLineAffiliation().trim().equals("")) {
	              partnerPublic.setVanLineAffiliation("");
               }
			partnerPublicManager.updateAgentParentName(partnerPublic.getLastName(),partnerPublic.getPartnerCode(),partnerPublic.getUpdatedBy());
			    /*partnerPrivate = new PartnerPrivate();
			    partnerPrivate.setLastName(partnerPublic.getLastName());
			    partnerPrivate.setPartnerPublicId(partnerPublic.getId());
			    partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
				partnerPrivate.setCreatedOn(new Date());
				partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
				partnerPrivate.setUpdatedOn(new Date());
			    partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
				partnerPrivate.setCreatedOn(new Date());
				partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
				partnerPrivate.setUpdatedOn(new Date());
			//partnerPrivate.setCorpID("TSFT");
			    partnerPrivate.setStatus(agentStatus);
				partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
				partnerPrivate.setExtReference("");
				
				partnerPrivate = partnerPrivateManager.save(partnerPrivate);*/
				 if(!(agentStatus.equalsIgnoreCase("New"))){
		        	 List corpIdList = companyManager.findCorpIdAgentList();
		        	 if(corpIdList!=null && (!(corpIdList.isEmpty()))){
		        		 Iterator iter = corpIdList.iterator();
				         while (iter.hasNext()) {
				        	 String partnerCodeCorpId = iter.next().toString();
				        	 String[] strPartner = partnerCodeCorpId.split("#");
				        	 String otherCorpId= strPartner[0];
				        	 String partnrCode= strPartner[1];
				        	 PartnerPrivate otherPartnerPrivate=null;
				        	 if(!(partnrCode.equalsIgnoreCase("NA"))){
				        	 //PartnerPublic oPartnerPublic = partnerPublicManager.getPartnerByCode(partnrCode);
				        	 //if(oPartnerPublic.getUtsNumber().equalsIgnoreCase("Y")){
				        	 List partnrPrivateList = partnerPrivateManager.getPartnerPrivateByPartnerCode(otherCorpId, partnerPublic.getPartnerCode());
					         if(partnrPrivateList!=null && (!(partnrPrivateList.isEmpty())) && (partnrPrivateList.get(0)!=null)){
						            Long partnrPrivateId = Long.parseLong(partnrPrivateList.get(0).toString());
						        	otherPartnerPrivate = partnerPrivateManager.getForOtherCorpid(partnrPrivateId);
						        	otherPartnerPrivate.setStatus(partnerPublic.getStatus());
						        	partnerPrivateManager.save(otherPartnerPrivate);
						        /*	try{
						        		if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
										 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
										 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
						        		}
										 
									    }catch (Exception e) {
											String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
									    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
									    	 e.printStackTrace(); 
										}*/
					          }else{
					        		otherPartnerPrivate = new PartnerPrivate(); 
					        		otherPartnerPrivate.setAccountHolder(getRequest().getRemoteUser().toString().toUpperCase());
					        		otherPartnerPrivate.setLastName(partnerPublic.getLastName());
					        		otherPartnerPrivate.setStatus(agentStatus);
					        		otherPartnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
					        		otherPartnerPrivate.setPartnerPublicId(partnerPublic.getId());
					        		otherPartnerPrivate.setCorpID(otherCorpId);	
					     			otherPartnerPrivate.setCompanyDivision("");
					     			
					     			//otherPartnerPrivate.setUTSCompanyType(partnerPrivate.getUTSCompanyType());			
					     			
					     			otherPartnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
					     			otherPartnerPrivate.setCreatedOn(new Date());
					     			otherPartnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
					     			otherPartnerPrivate.setUpdatedOn(new Date());
					     			//otherPartnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
					     			partnerPrivateManager.save(otherPartnerPrivate);
					     			/*try{
					     				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
										 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
										 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
					     				}
										 
									    }catch (Exception e) {
											String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
									    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
									    	 e.printStackTrace(); 
										}*/
					     		
					        	  }
				              }
		        	    }
		        	 }	 
		         } 
		  }
			   
			 
			    String msgText1="Dear "+user1.getFirstName()+  ", \n\nYour request to add/edit agent  "+agentRequest.getLastName()+" has been approved with agent code: "+partnerPublic.getPartnerCode()+"\n\nBest regards,\nRedsky Team";
			    String subject = "Request Approved for Agent "+agentRequest.getLastName();
				String smtpHost = "localhost"; 
				String from = "support@redskymobility.com";
			
				emailSetupManager.globalEmailSetupProcess(from,user1.getEmail(), "", "", "", msgText1, subject, agentRequest.getCorpID(),"","","");
			 
		  }
		  else
		  {
			 agentRequestManager.updateAgentRecord(sessionCorpID,agentRequest.getLastName(),agentRequest.getAliasName(),agentRequest.getBillingCountry(), agentRequest.getBillingCity(),agentRequest.getBillingState(),agentStatus,agentRequest.getBillingEmail(),true,agentRequest.getBillingZip(),agentRequest.getBillingAddress1(),getRequest().getRemoteUser(),agentRequest.getId(),agentRequest.getCreatedBy());
			 User user1 = userManager.getUserByUsername(agentRequest.getUpdatedBy());	
	           agentRequestReason = agentRequestReasonManager.getByID(agentRequest.getId());
	           System.out.println(agentRequestReason.getComment());
	           System.out.println(agentRequestReason.getReason());
			    String msgText1="Dear "+user1.getFirstName()+  ", \n\nYou request to add/edit agent  "+agentRequest.getLastName()+" has been Rejected with reason "+agentRequestReason.getComment()+". \n\nIf this is an error, you may use the resend button on the Agent Request List tab under Move Management->Partner to modify or resend the request.  \n\n Best regards,\nRedsky Team";
			    String subject = "Request Reject for Agent "+agentRequest.getLastName();
				String smtpHost = "localhost"; 
				String from = "support@redskymobility.com";
			if(sessionCorpID.equalsIgnoreCase("TSFT"))
			{
				  emailSetupManager.globalEmailSetupProcess(from,user1.getEmail(), "", "", "", msgText1, subject, agentRequest.getCorpID(),"","","");
			}
				  
				  /*boolean isNew = (agentRequest.getId() == null);
				  partnerPublic=partnerPublicManager.checkById(agentRequest.getPartnerId());
					 if(partnerPublic!=null && partnerPublic.getId()!=null)
					 {
						 
						partnerPublic.setLastName(agentRequest.getLastName());
						partnerPublic.setIsAgent(true);
					     
						partnerPublic.setCorpID("TSFT");
						partnerPublic.setStatus(agentStatus);
						partnerPublic.setFirstName("");
						partnerPublic.setBillingPhone(agentRequest.getBillingPhone());
						partnerPublic.setAliasName(agentRequest.getAliasName());
						partnerPublic.setBillingCountry(agentRequest.getBillingCountry());
						partnerPublic.setTerminalAddress1(agentRequest.getTerminalAddress1());
						partnerPublic.setTerminalCity(agentRequest.getTerminalCity());
						partnerPublic.setTerminalCountry(agentRequest.getTerminalCountry());
						partnerPublic.setTerminalState(agentRequest.getTerminalState());
						partnerPublic.setTerminalCountryCode(agentRequest.getTerminalCountryCode());
						partnerPublic.setBillingCountryCode(agentRequest.getBillingCountryCode());
						partnerPublic.setBillingCountry(agentRequest.getBillingCountry());
						partnerPublic.setBillingCity(agentRequest.getBillingCity());
						partnerPublic.setBillingState(agentRequest.getBillingState());
						partnerPublic.setBillingEmail(agentRequest.getBillingEmail());
						partnerPublic.setBillingAddress1(agentRequest.getBillingAddress1());
						partnerPublic.setBillingZip(agentRequest.getBillingZip());
						partnerPublic.setCreatedOn(new Date());
						partnerPublic.setUpdatedOn(new Date());
						partnerPublic.setUpdatedBy(agentRequest.getUpdatedBy());
						partnerPublic.setCreatedBy(agentRequest.getCreatedBy());
						partnerPublic.setBillingCountryCode(agentRequest.getBillingCountryCode());
						partnerPublic.setTerminalCountryCode(agentRequest.getTerminalCountryCode());
						partnerPublic.setTypeOfVendor("");
						if (agentRequest.getBillingCountry() == null || agentRequest.getBillingCountry().trim().equals("")) {
							partnerPublic.setBillingCountry("");
							partnerPublic.setBillingCountryCode("");
						}
						if (agentRequest.getTerminalCountry() == null || agentRequest.getTerminalCountry().trim().equals("")) {
							partnerPublic.setTerminalCountry("");
							partnerPublic.setTerminalCountryCode("");
						}
						if (agentRequest.getMailingCountry() == null || agentRequest.getMailingCountry().trim().equals("")) {
							partnerPublic.setMailingCountry("");
							partnerPublic.setMailingCountryCode("");
						}
						if (agentRequest.getBillingState() == null) {
							partnerPublic.setBillingState("");
						}
						 
						if (agentRequest.getTerminalCity() == null) {
							partnerPublic.setTerminalCity("");
						}
						if (agentRequest.getTerminalCountryCode() == null) {
							partnerPublic.setTerminalCountryCode("");
						}
						if (agentRequest.getTerminalState() == null) {
							partnerPublic.setTerminalState("");
						}
						String maxPartnerCode;
						try {
							if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
								maxPartnerCode = "T10001";
							} else {
								List maxCode = partnerPublicManager.findMaxByCode("T");

								maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
								if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
									maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
								}
							}
						} catch (Exception ex) {
							maxPartnerCode = "T10001";
							ex.printStackTrace();
						}
		
						partnerPublic.setPartnerCode(partnerPublic.getPartnerCode());
						//partnerPublic = partnerPublicManager.save(partnerPublic);
						List partnerPrivateListForDiffCorpId =partnerPrivateManager.getPartnerPrivateId(partnerPublic.getPartnerCode());
					   System.out.println("sdfxgvbhjm");
						Iterator itPartnerPrivate = partnerPrivateListForDiffCorpId.iterator();
						while(itPartnerPrivate.hasNext()) {
							String idValue = itPartnerPrivate.next().toString();
							Long idPartnerPrivate=Long.parseLong(idValue);
							PartnerPrivate partnerPrivateForDiffCorpId = partnerPrivateManager.checkById(idPartnerPrivate);
							createPartnerPrivate(partnerPrivateForDiffCorpId);
								}
													
					       System.out.println("aesrdtfghjkl");
							
					  
						
		}
					 else
					 {
					partnerPublic = new PartnerPublic();
					partnerPublic.setLastName(agentRequest.getLastName());
					partnerPublic.setIsAgent(true);
		 
					partnerPublic.setCorpID("TSFT");
					partnerPublic.setStatus(agentStatus);
					partnerPublic.setFirstName("");
					partnerPublic.setBillingPhone(agentRequest.getBillingPhone());
					partnerPublic.setAliasName(agentRequest.getAliasName());
					partnerPublic.setBillingCountry(agentRequest.getBillingCountry());
					partnerPublic.setTerminalAddress1(agentRequest.getTerminalAddress1());
					partnerPublic.setTerminalCity(agentRequest.getTerminalCity());
					partnerPublic.setTerminalCountry(agentRequest.getTerminalCountry());
					partnerPublic.setTerminalState(agentRequest.getTerminalState());
					partnerPublic.setTerminalCountryCode(agentRequest.getTerminalCountryCode());
					partnerPublic.setBillingCountryCode(agentRequest.getBillingCountryCode());
					partnerPublic.setBillingCountry(agentRequest.getBillingCountry());
					partnerPublic.setBillingCity(agentRequest.getBillingCity());
					partnerPublic.setBillingState(agentRequest.getBillingState());
					partnerPublic.setBillingEmail(agentRequest.getBillingEmail());
					partnerPublic.setBillingAddress1(agentRequest.getBillingAddress1());
					partnerPublic.setBillingZip(agentRequest.getBillingZip());
					partnerPublic.setCreatedOn(new Date());
					partnerPublic.setUpdatedOn(new Date());
					partnerPublic.setUpdatedBy(agentRequest.getUpdatedBy());
					partnerPublic.setCreatedBy(agentRequest.getCreatedBy());
					partnerPublic.setBillingCountryCode(agentRequest.getBillingCountryCode());
					partnerPublic.setTerminalCountryCode(agentRequest.getTerminalCountryCode());
					partnerPublic.setTypeOfVendor("");
					if (agentRequest.getBillingCountry() == null || agentRequest.getBillingCountry().trim().equals("")) {
						partnerPublic.setBillingCountry("");
						partnerPublic.setBillingCountryCode("");
					}
					if (agentRequest.getTerminalCountry() == null || agentRequest.getTerminalCountry().trim().equals("")) {
						partnerPublic.setTerminalCountry("");
						partnerPublic.setTerminalCountryCode("");
					}
					if (agentRequest.getMailingCountry() == null || agentRequest.getMailingCountry().trim().equals("")) {
						partnerPublic.setMailingCountry("");
						partnerPublic.setMailingCountryCode("");
					}
					if (agentRequest.getBillingState() == null) {
						partnerPublic.setBillingState("");
					}
					 
					if (agentRequest.getTerminalCity() == null) {
						partnerPublic.setTerminalCity("");
					}
					if (agentRequest.getTerminalCountryCode() == null) {
						partnerPublic.setTerminalCountryCode("");
					}
					if (agentRequest.getTerminalState() == null) {
						partnerPublic.setTerminalState("");
					}
					String maxPartnerCode;
					try {
						if ((partnerPublicManager.findPartnerCode("T10001")).isEmpty()) {
							maxPartnerCode = "T10001";
						} else {
							List maxCode = partnerPublicManager.findMaxByCode("T");

							maxPartnerCode = "T" + (String.valueOf((Long.parseLong((maxCode.get(0).toString()).substring(1, maxCode.get(0).toString().length())) + 1)));
							if (!(partnerPublicManager.findPartnerCode(maxPartnerCode) == null || (partnerPublicManager.findPartnerCode(maxPartnerCode)).isEmpty())) {
								maxPartnerCode = "T" + (String.valueOf((Long.parseLong(maxPartnerCode.substring(1, maxPartnerCode.length())) + 1)));
							}
						}
					} catch (Exception ex) {
						maxPartnerCode = "T10001";
						ex.printStackTrace();
					}

					partnerPublic.setPartnerCode(maxPartnerCode);
					partnerPublic = partnerPublicManager.save(partnerPublic);
				    agentRequest.setPartnerId(partnerPublic.getId());
					System.out.println("sdfxgvhbjklm----"+partnerPublic.getId());
					 partnerPrivate = new PartnerPrivate();
					    partnerPrivate.setLastName(partnerPublic.getLastName());
					    partnerPrivate.setPartnerPublicId(partnerPublic.getId());
					    partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
						partnerPrivate.setCreatedOn(new Date());
						partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
						partnerPrivate.setUpdatedOn(new Date());
					   partnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
						partnerPrivate.setCreatedOn(new Date());
						partnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
						partnerPrivate.setUpdatedOn(new Date());
					//partnerPrivate.setCorpID("TSFT");
					  partnerPrivate.setStatus(agentStatus);
						partnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
						partnerPrivate.setExtReference("");
						
						partnerPrivate = partnerPrivateManager.save(partnerPrivate);
						 if(!(agentStatus.equalsIgnoreCase("New"))){
				        	 List corpIdList = companyManager.findCorpIdAgentList();
				        	 if(corpIdList!=null && (!(corpIdList.isEmpty()))){
				        		 Iterator iter = corpIdList.iterator();
						         while (iter.hasNext()) {
						        	 String partnerCodeCorpId = iter.next().toString();
						        	 String[] strPartner = partnerCodeCorpId.split("#");
						        	 String otherCorpId= strPartner[0];
						        	 String partnrCode= strPartner[1];
						        	 PartnerPrivate otherPartnerPrivate=null;
						        	 if(!(partnrCode.equalsIgnoreCase("NA"))){
						        	 //PartnerPublic oPartnerPublic = partnerPublicManager.getPartnerByCode(partnrCode);
						        	 //if(oPartnerPublic.getUtsNumber().equalsIgnoreCase("Y")){
						        	 List partnrPrivateList = partnerPrivateManager.getPartnerPrivateByPartnerCode(otherCorpId, partnerPublic.getPartnerCode());
							         if(partnrPrivateList!=null && (!(partnrPrivateList.isEmpty())) && (partnrPrivateList.get(0)!=null)){
								            Long partnrPrivateId = Long.parseLong(partnrPrivateList.get(0).toString());
								        	otherPartnerPrivate = partnerPrivateManager.getForOtherCorpid(partnrPrivateId);
								        	otherPartnerPrivate.setStatus(partnerPublic.getStatus());
								        	partnerPrivateManager.save(otherPartnerPrivate);
								        	try{
								        		if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
												 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
												 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
								        		}
												 
											    }catch (Exception e) {
													String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
											    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
											    	 e.printStackTrace(); 
												}
							          }else{
							        		otherPartnerPrivate = new PartnerPrivate(); 
							        		otherPartnerPrivate.setAccountHolder(getRequest().getRemoteUser().toString().toUpperCase());
							        		otherPartnerPrivate.setLastName(partnerPublic.getLastName());
							        		otherPartnerPrivate.setStatus(agentStatus);
							        		otherPartnerPrivate.setPartnerCode(partnerPublic.getPartnerCode());
							        		otherPartnerPrivate.setPartnerPublicId(partnerPublic.getId());
							        		otherPartnerPrivate.setCorpID(otherCorpId);	
							     			otherPartnerPrivate.setCompanyDivision("");
							     			
							     			otherPartnerPrivate.setUTSCompanyType(partnerPrivate.getUTSCompanyType());			
							     			
							     			otherPartnerPrivate.setCreatedBy(partnerPublic.getCreatedBy());
							     			otherPartnerPrivate.setCreatedOn(new Date());
							     			otherPartnerPrivate.setUpdatedBy(partnerPublic.getUpdatedBy());
							     			otherPartnerPrivate.setUpdatedOn(new Date());
							     			otherPartnerPrivate.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
							     			partnerPrivateManager.save(otherPartnerPrivate);
							     			try{
							     				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
												 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
												 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
							     				}
												 
											    }catch (Exception e) {
													String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
											    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
											    	 e.printStackTrace(); 
												}
							     		
							        	  }
						              }
				        	    }
				        	 }	 
				         } 
				  }
*/
		  
			  
		  }
		  
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	return SUCCESS;  
	    	}
	  
	private PartnerPrivate createPartnerPrivate(PartnerPrivate partnerPrivate) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		PartnerPrivate partnerPrivateNew = new PartnerPrivate();
		try {
			//BeanUtils.copyProperties(partnerPrivateNew, partnerPrivate);
			BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
			beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
			beanUtilsBean.copyProperties(partnerPrivateNew, partnerPrivate); 
			
			List corpIdList = companyManager.findCorpIdAgentList();
       	 if(corpIdList!=null && (!(corpIdList.isEmpty()))){
       		 Iterator iter = corpIdList.iterator();
		         while (iter.hasNext()) {
		        	 String partnerCodeCorpId = iter.next().toString();
		        	 String[] strPartner = partnerCodeCorpId.split("#");
		        	 String otherCorpId= strPartner[0];
		        	 String partnrCode= strPartner[1];
		        	 PartnerPrivate otherPartnerPrivate=null;
		        	 if(!(partnrCode.equalsIgnoreCase("NA"))){
		        	 //PartnerPublic oPartnerPublic = partnerPublicManager.getPartnerByCode(partnrCode);
		        	 //if(oPartnerPublic.getUtsNumber().equalsIgnoreCase("Y")){
		        	 List partnrPrivateList = partnerPrivateManager.getPartnerPrivateByPartnerCode(otherCorpId, partnerPublic.getPartnerCode());
			         if(partnrPrivateList!=null && (!(partnrPrivateList.isEmpty())) && (partnrPrivateList.get(0)!=null)){
				            Long partnrPrivateId = Long.parseLong(partnrPrivateList.get(0).toString());
				        	otherPartnerPrivate = partnerPrivateManager.getForOtherCorpid(partnrPrivateId);
				        	otherPartnerPrivate.setStatus(agentStatus);
				        	otherPartnerPrivate.setLastName(partnerPublic.getLastName());
				        	
				        	//partnerPrivateManager.save(otherPartnerPrivate);
				        	/*try{
				        		if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
				        		}
								 
							    }catch (Exception e) {
									String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
							    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
							    	 e.printStackTrace(); 
								}*/
			          }else{
			        		
			        	  partnerPrivateNew.setAccountHolder(getRequest().getRemoteUser().toString().toUpperCase());
			        	  partnerPrivateNew.setLastName(partnerPublic.getLastName());
			        	  partnerPrivateNew.setStatus(agentStatus);
			        	  partnerPrivateNew.setPartnerCode(partnerPublic.getPartnerCode());
			        	  partnerPrivateNew.setPartnerPublicId(partnerPublic.getId());
			        	  partnerPrivateNew.setCompanyDivision("");
			     			
			        	  partnerPrivateNew.setUTSCompanyType(partnerPrivate.getUTSCompanyType());			
			     			
			        	  partnerPrivateNew.setCreatedBy(partnerPublic.getCreatedBy());
			        	  partnerPrivateNew.setCreatedOn(new Date());
			        	  partnerPrivateNew.setUpdatedBy(partnerPublic.getUpdatedBy());
			        	  partnerPrivateNew.setUpdatedOn(new Date());
			        	  partnerPrivateNew.setVatBillingGroup(partnerPrivate.getVatBillingGroup());
			
			     			/*try{
			     				if((partnerPublic.getIsPrivateParty() || partnerPublic.getIsAccount() || partnerPublic.getIsCarrier() || partnerPublic.getIsOwnerOp() || partnerPublic.getIsVendor() || (partnerPublic.getIsAgent() && partnerPrivate.getStatus() !=null && partnerPrivate.getStatus().equalsIgnoreCase("Approved") )) && (partnerPublic.getStatus() !=null && partnerPublic.getStatus().equalsIgnoreCase("Approved")) ){
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "R");
								 	createPartnerAccountRef(partnerPublic,partnerPrivate , "P");
			     				}
								 
							    }catch (Exception e) {
									String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
							    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString(); 
							    	 e.printStackTrace(); 
								}*/
			     		
			        	  }
		              }
/*
			partnerPrivateNew.setLastName(partnerPublic.getLastName());
			
			 
			partnerPrivateNew.setCorpID(otherCorpId);
		         
			partnerPrivate.setCreatedBy(partnerPripartnerPublicvate.getCreatedBy());
			partnerPrivate.setCreatedOn(new Date());
			partnerPrivateNew.setUpdatedBy(partnerPrivate.getUpdatedBy());
			partnerPrivateNew.setUpdatedOn(new Date());*/
			
		         }}
		} catch (Exception e) {
			e.printStackTrace();
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return partnerPrivateNew;
	}
	

	private Map<String, String>  prefixList = new HashMap<String, String>(); ;
	
	///#9684 - UTS_COMPANY_TYPE parameter is no longer used in RedSky/
	//private Map <String,String> companyUTSType;
	private Map <String,String> billingCurrency;
	private Map <String,String> movingCompanyType;
	private Map <String,String> UTSmovingCompanyType;
	private Map <String,String> pType;
	private Map <String,String> taxIdType;
	private Map<String, String> collectionList;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private Map<String, String> partnerStatus;
	private Map<String, String> actionType;
	private Map<String, String> agentRequestStatus;
	private List corpidList;
	
	
	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Map<String, String> getPartnerStatus() {
		return partnerStatus;
	}

	public void setPartnerStatus(Map<String, String> partnerStatus) {
		this.partnerStatus = partnerStatus;
	}

	public Map<String, String> getActionType() {
		return actionType;
	}

	public void setActionType(Map<String, String> actionType) {
		this.actionType = actionType;
	}

	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		pType= refMasterManager.findByParameter(sessionCorpID, "AgentType");
		//companyUTSType = refMasterManager.findByParameter(corpId, "UTS_Company_Type");
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		billingCurrency = refMasterManager.findCodeOnleByParameter(corpId, "DEFAULT_BILLING_CURRENCY");
		movingCompanyType = refMasterManager.findByParameter(corpId, "MovingCompanyType");
		UTSmovingCompanyType = refMasterManager.findByParameter(corpId, "UTSMovingCompanyType");
		driverAgencyList=refMasterManager.findByParameter(sessionCorpID,"DRIVERAGENCY"); 
		sale = refMasterManager.findUser(corpId, "ROLE_SALE_COORD");
		prefixList=refMasterManager.findByParameter(corpId, "PREFFIX");
		countryDesc = refMasterManager.findCountry(corpId, "COUNTRY");
		typeOfVendor = refMasterManager.findByParameter(corpId, "TYPEOFVENDOR");
		corp=refMasterManager.findByParameter(corpId, "CORP");
		vanlineAffiliations = refMasterManager.findByParameter(corpId, "VANLINEAFFILIATIONS");
		serviceLines = refMasterManager.findByParameter(corpId, "SERVICELINES");
		qualityCertifications = refMasterManager.findByParameter(corpId, "QUALITYCERTIFICATIONS");
		companyDivis = customerFileManager.findOpenCompanyDivision(sessionCorpID);
		partnerStatus = refMasterManager.findByParameter(corpId, "PARTNER_STATUS");
		reason=refMasterManager.findByParameter(corpId, "AGENTREQUESTREASON");
		fleet=refMasterManager.findByParameter(corpId, "FLEET");
		creditTerms = refMasterManager.findByParameter(corpId, "CrTerms");
		try{
		//creditTerms=CommonUtil.sortByKey(creditTerms);
		}catch(Exception e){e.printStackTrace();}
		paytype = refMasterManager.findByParameter(corpId, "PAYTYPE");
		driverType = refMasterManager.findByParameter(corpId, "DRIVER_TYPE");
		bankCode= refMasterManager.findByParameter(corpId, "bankCode");
		taxIdType= refMasterManager.findByParameter(corpId, "TAXID_TYPE");
		countryCod = refMasterManager.findByParameter(corpId, "COUNTRY");
		planCommissionList = refMasterManager.findByParameter(sessionCorpID, "COMMISSIONPLAN");
		collectionList = refMasterManager.findByParameter(sessionCorpID,"COLLECTION");
		vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
		actionType = new LinkedHashMap<String, String>();
		agentRequestStatus = refMasterManager.findByParameter(corpId, "AGENTREQUESTSTATUS");
	     if(!corpId.equalsIgnoreCase("TSFT")){
		agentRequestStatus.remove("ReRequest");	
	     }
			
		corpidList=companyManager.getcorpID();
		actionType.put("PP", "Private Party");
		actionType.put("AC", "Account");
		actionType.put("AG", "Agent");
		actionType.put("VN", "Vendor");
		actionType.put("CR", "Carrier");
		actionType.put("OO", "Owner Ops");	
		company= companyManager.findByCorpID(corpId).get(0);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String mergePartnerConfirm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;		
		
	}

	private String counter;
	private Long agentCountId;
	
	@SkipValidation
	 public String countResendRequest()
	   { 
		agentRequestManager.checkById(agentId);
		agentRequest.setCounter(counter);
		getComboList(sessionCorpID);
		 return SUCCESS;
	   }
	@SkipValidation
	 public String viewAgentRequestReason()
	   {
		 getComboList(sessionCorpID);
		 return SUCCESS;
	   }
	private Long agentReasonId;
	private AgentRequestReason agentRequestReason;
	private AgentRequestReasonManager agentRequestReasonManager;

	@SkipValidation
	 public String agentReason()
	   {
		 
         getComboList(sessionCorpID); 
		if (id != null) {
			agentRequestReason = agentRequestReasonManager.get(id);
		
			//states = customerFileManager.findDefaultStateList(truck.getCountry(), sessionCorpID);
		} else {
			agentRequestReason = new AgentRequestReason();
			//states = customerFileManager.findDefaultStateList("", sessionCorpID);
			agentRequestReason.setCorpID(sessionCorpID);
			agentRequestReason.setComment(agentRequestReason.getComment());
			
			
		}
		//driverList =truckManager.findDriverList(truck.getWarehouse(),sessionCorpID);
		//ownerOperatorList=truckManager.findOwnerOperatorList(parentId, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	   }
	private String comment;
	private String agentreason;
	@SkipValidation
	public String agentSaveRequestReason()
	{
		
		if(id!=null)
		{
		agentRequestReason=agentRequestReasonManager.get(id);
		}
		else
		{
			agentRequestReason =new AgentRequestReason();
		
		agentRequestReason.setComment(comment);
		agentRequestReason.setReason(agentreason);
		agentRequestReason.setCreatedBy(createdBy);
		
//		/agentRequestReason.setAgentRequestId(agentReasonId);
	
		agentRequestReason=agentRequestReasonManager.save(agentRequestReason);
		
		}
		return SUCCESS;
		
	}
	private Long agentInfoId;
    
    private List findAgentReasonList;
	@SkipValidation
	public String getAgentInfo()
	{
		
		if(agentInfoId!=null)
		{
		agentRequestReason=agentRequestReasonManager.get(agentInfoId);
      String reason=agentRequestReason.getReason();
      String comment=agentRequestReason.getComment();
     
     //findAgentReasonList= agentRequestReasonManager.findAddressList(serviceOrder.getShipNumber(),sessionCorpID);
		}

		return SUCCESS;
		
	}
	private String changeFields;
	@SkipValidation
	public String getChangesRequested(Long ppid,Long id) 
	{
		
		
		partnerPublic=partnerPublicManager.get(ppid);
		agentRequest=agentRequestManager.get(id);
		 changeFields="";
		 if(agentRequest.getChangeRequestColumn()!=null && !agentRequest.getChangeRequestColumn().equals(""))
		 {
		 String strPartner[] = agentRequest.getChangeRequestColumn().split(",");
		List<String> list = new ArrayList();
		try
		{
			Map<String,String> partnerPublicData = new HashMap<String,String>();
			Map<String,String> agentRequestData = new HashMap<String,String>();
			partnerPublicData = BeanUtils.describe(partnerPublic);
			agentRequestData = BeanUtils.describe(agentRequest);
			 
			 Iterator keysIt=partnerPublicData.entrySet().iterator();
			 for(String h:strPartner){
					if(h !=null &&  (!(h.trim().equals(""))) && (!(h.trim().equalsIgnoreCase("id"))) && (!(h.trim().equalsIgnoreCase("partnerCode"))) && (!(h.trim().equalsIgnoreCase("CorpID"))) && (!(h.trim().equalsIgnoreCase("status"))) && (!(h.trim().equalsIgnoreCase("isAccount"))) && (!(h.trim().equalsIgnoreCase("isAgent"))) && (!(h.trim().equalsIgnoreCase("isCarrier"))) && (!(h.trim().equalsIgnoreCase("isVendor"))) && (!(h.trim().equalsIgnoreCase("isOwnerOp"))) && (!(h.trim().equalsIgnoreCase("isPrivateParty")))) {
				        if( agentRequestData.containsKey(h) && agentRequestData.get(h)!=null){
				        	
					
								changeFields=changeFields+""+h+","	;
							
						
					}
					
			   }}
				if (changeFields.endsWith(",")) {
					changeFields = changeFields.substring(0, changeFields.length() - 1);
					System.out.println(changeFields);
					} 
			
		}
		catch(Exception e)
		{e.printStackTrace();
			
			
		}}
		 else
		 {
				try
				{
			 Map<String,String> partnerPublicData = new HashMap<String,String>();
				Map<String,String> agentRequestData = new HashMap<String,String>();
				partnerPublicData = BeanUtils.describe(partnerPublic);
				agentRequestData = BeanUtils.describe(agentRequest);
						 Iterator keysIt=partnerPublicData.entrySet().iterator();
						 PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
						List<String> list = new ArrayList();
							while (keysIt.hasNext()) {
								Map.Entry entry = (Map.Entry) keysIt.next();
								String key = (String) entry.getKey(); 
								
										
							if(partnerPublicData.containsKey(key) && agentRequestData.containsKey(key)){
								if((partnerPublicData.get(key) !=null && agentRequestData.get(key)!=null && (!(partnerPublicData.get(key).toString().equalsIgnoreCase(agentRequestData.get(key).toString())))) 
										|| ((partnerPublicData.get(key) == null || partnerPublicData.get(key).trim().equals("")) && ((agentRequestData.get(key)!=null) && (!(agentRequestData.get(key).trim().equals("")))))){
									if(!(key.trim().equalsIgnoreCase("corpID")) && !(key.trim().equalsIgnoreCase("updatedOn")) && !(key.trim().equalsIgnoreCase("updatedby")) && !(key.trim().equalsIgnoreCase("createdBy")) && !(key.trim().equalsIgnoreCase("createdOn")) && !(key.trim().equalsIgnoreCase("id"))  && !(key.trim().equalsIgnoreCase("status")) )
										if(beanUtilsBean.isWriteable(agentRequest, key)) {	  
											  beanUtilsBean.setProperty(agentRequest,key.toString().trim(),beanUtilsBean.getProperty(partnerPublic, key.trim()));
											}
										changeFields=changeFields+""+key+",";
									
								}
							}
							}
					   
						if (changeFields.endsWith(",")) {
							changeFields = changeFields.substring(0, changeFields.length() - 1);
							System.out.println(changeFields);
							} 
					
			
				}
				catch(Exception e)
				{
					
				}
		 }
		 
		 
		 
		 
		return changeFields;	
	}
	
	private String partnerVanLineCode;
	private List vanLineList;
	

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}


	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	
	

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getCountryCodeSearch() {
		return countryCodeSearch;
	}

	public void setCountryCodeSearch(String countryCodeSearch) {
		this.countryCodeSearch = countryCodeSearch;
	}

	public String getCountrySearch() {
		return countrySearch;
	}

	public void setCountrySearch(String countrySearch) {
		this.countrySearch = countrySearch;
	}

	public String getStateSearch() {
		return stateSearch;
	}

	public void setStateSearch(String stateSearch) {
		this.stateSearch = stateSearch;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public Map<String, String> getCountryDesc() {
		return countryDesc;
	}

	public void setCountryDesc(Map<String, String> countryDesc) {
		this.countryDesc = countryDesc;
	}

	public List getMultiplequalityCertifications() {
		return multiplequalityCertifications;
	}

	public void setMultiplequalityCertifications(List multiplequalityCertifications) {
		this.multiplequalityCertifications = multiplequalityCertifications;
	}

	public List getMultiplServiceLines() {
		return multiplServiceLines;
	}

	public void setMultiplServiceLines(List multiplServiceLines) {
		this.multiplServiceLines = multiplServiceLines;
	}

	public List getMultiplVanlineAffiliations() {
		return multiplVanlineAffiliations;
	}

	public void setMultiplVanlineAffiliations(List multiplVanlineAffiliations) {
		this.multiplVanlineAffiliations = multiplVanlineAffiliations;
	}

	public void setBstates(Map<String, String> bstates) {
		this.bstates = bstates;
	}

	public void setMstates(Map<String, String> mstates) {
		this.mstates = mstates;
	}

	public void setTstates(Map<String, String> tstates) {
		this.tstates = tstates;
	}

	public Map<String, String> getBstates() {
		if (agentRequest != null) {
			return (bstates != null && !bstates.isEmpty()) ? bstates : customerFileManager.findDefaultStateList(agentRequest.getBillingCountryCode(), sessionCorpID);
		} else {
			return (bstates != null && !bstates.isEmpty()) ? bstates : customerFileManager.findDefaultStateList(partner.getBillingCountryCode(), sessionCorpID);
		}

	}

	public Map<String, String> getTstates() {
		if (agentRequest != null) {
			List terminalCountryCode = partnerManager.getCountryCode(agentRequest.getTerminalCountry());
			try {
				if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(agentRequest.getTerminalCountryCode(), sessionCorpID);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			List terminalCountryCode = partnerManager.getCountryCode(partner.getTerminalCountry());
			try {
				if (!terminalCountryCode.isEmpty() && (terminalCountryCode.get(0)!=null)) {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(terminalCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (tstates != null && !tstates.isEmpty()) ? tstates : customerFileManager.findDefaultStateList(partner.getTerminalCountryCode(), sessionCorpID);
				}

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return tstates;
	}

	public Map<String, String> getMstates() {
		if (agentRequest != null) {
			List mailIngCountryCode = partnerManager.getCountryCode(agentRequest.getMailingCountry());
			try {
				if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(agentRequest.getMailingCountryCode(), sessionCorpID);

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			List mailIngCountryCode = partnerManager.getCountryCode(partner.getMailingCountry());
			try {
				if (!mailIngCountryCode.isEmpty() && (mailIngCountryCode.get(0)!=null)) {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(mailIngCountryCode.get(0).toString(), sessionCorpID);

				} else {
					return (mstates != null && !mstates.isEmpty()) ? mstates : customerFileManager.findDefaultStateList(partner.getMailingCountryCode(), sessionCorpID);

				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}

		return mstates;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getTypeOfVendor() {
		return typeOfVendor;
	}

	public void setTypeOfVendor(Map<String, String> typeOfVendor) {
		this.typeOfVendor = typeOfVendor;
	}

	public Map<String, String> getQualityCertifications() {
		return qualityCertifications;
	}

	public void setQualityCertifications(Map<String, String> qualityCertifications) {
		this.qualityCertifications = qualityCertifications;
	}

	public Map<String, String> getServiceLines() {
		return serviceLines;
	}

	public void setServiceLines(Map<String, String> serviceLines) {
		this.serviceLines = serviceLines;
	}

	public Map<String, String> getVanlineAffiliations() {
		return vanlineAffiliations;
	}

	public void setVanlineAffiliations(Map<String, String> vanlineAffiliations) {
		this.vanlineAffiliations = vanlineAffiliations;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile1() {
		return file1;
	}

	public void setFile1(File file1) {
		this.file1 = file1;
	}

	public String getFile1FileName() {
		return file1FileName;
	}

	public void setFile1FileName(String file1FileName) {
		this.file1FileName = file1FileName;
	}

	public File getFile2() {
		return file2;
	}

	public void setFile2(File file2) {
		this.file2 = file2;
	}

	public String getFile2FileName() {
		return file2FileName;
	}

	public void setFile2FileName(String file2FileName) {
		this.file2FileName = file2FileName;
	}

	public File getFile3() {
		return file3;
	}

	public void setFile3(File file3) {
		this.file3 = file3;
	}

	public String getFile3FileName() {
		return file3FileName;
	}

	public void setFile3FileName(String file3FileName) {
		this.file3FileName = file3FileName;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getExList() {
		return exList;
	}

	public void setExList(String exList) {
		this.exList = exList;
	}

	public List getPartnerListNew() {
		return partnerListNew;
	}

	public void setPartnerListNew(List partnerListNew) {
		this.partnerListNew = partnerListNew;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public PartnerPublic getPartner() {
		return partner;
	}

	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getFormClose() {
		return formClose;
	}

	public void setFormClose(String formClose) {
		this.formClose = formClose;
	}

	public String getParamView() {
		return paramView;
	}

	public void setParamView(String paramView) {
		this.paramView = paramView;
	}

	

	
	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public List getCompanyDivis() {
		return companyDivis;
	}

	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}

	public String getCompDiv() {
		return compDiv;
	}

	public void setCompDiv(String compDiv) {
		this.compDiv = compDiv;
	}

	public String getCitySearch() {
		return citySearch;
	}

	public void setCitySearch(String citySearch) {
		this.citySearch = citySearch;
	}

	public String getUserCheck() {
		return userCheck;
	}

	public void setUserCheck(String userCheck) {
		this.userCheck = userCheck;
	}

	public String getUserCheckMaster() {
		return userCheckMaster;
	}

	public void setUserCheckMaster(String userCheckMaster) {
		this.userCheckMaster = userCheckMaster;
	}

	


	public String getLogQuery() {
		return logQuery;
	}

	public void setLogQuery(String logQuery) {
		this.logQuery = logQuery;
	}

	public Boolean getIsIgnoreInactive() {
		return isIgnoreInactive;
	}

	public void setIsIgnoreInactive(Boolean isIgnoreInactive) {
		this.isIgnoreInactive = isIgnoreInactive;
	}

	/**
	 * @return the ownerOperatorList
	 */
	public List getOwnerOperatorList() {
		return ownerOperatorList;
	}

	/**
	 * @param ownerOperatorList the ownerOperatorList to set
	 */
	public void setOwnerOperatorList(List ownerOperatorList) {
		this.ownerOperatorList = ownerOperatorList;
	}

	/**
	 * @return the parentId
	 */
	public String getParentId() {
		return parentId;
	}

	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public Map<String, String> getPaytype() {
		return paytype;
	}

	public void setPaytype(Map<String, String> paytype) {
		this.paytype = paytype;
	}

	public Map<String, String> getServiceRelos1() {
		return serviceRelos1;
	}

	public void setServiceRelos1(Map<String, String> serviceRelos1) {
		this.serviceRelos1 = serviceRelos1;
	}

	public Map<String, String> getServiceRelos2() {
		return serviceRelos2;
	}

	public void setServiceRelos2(Map<String, String> serviceRelos2) {
		this.serviceRelos2 = serviceRelos2;
	}

	public Map<String, String> getServiceRelos() {
		return serviceRelos;
	}

	public void setServiceRelos(Map<String, String> serviceRelos) {
		this.serviceRelos = serviceRelos;
	}

    public Map<String, String> getFleet() {
		return fleet;
	}

	public void setFleet(Map<String, String> fleet) {
		this.fleet = fleet;
	}

	public Map<String, String> getCorp() {
		return corp;
	}

	public void setCorp(Map<String, String> corp) {
		this.corp = corp;
	}
	
	public String getJobRelo() {
		return jobRelo;
	}

	public void setJobRelo(String jobRelo) {
		this.jobRelo = jobRelo;
	}

	public Set getPartners() {
		return partners;
	}

	public void setPartners(Set partners) {
		this.partners = partners;
	}

	public String getPartnerCorpid() {
		return partnerCorpid;
	}

	public void setPartnerCorpid(String partnerCorpid) {
		this.partnerCorpid = partnerCorpid;
	} 

	public String getPartnerCodeCount() {
		return partnerCodeCount;
	}

	public void setPartnerCodeCount(String partnerCodeCount) {
		this.partnerCodeCount = partnerCodeCount;
	}

	

	public Map<String, String> getCreditTerms() {
		return creditTerms;
	}

	public void setCreditTerms(Map<String, String> creditTerms) {
		this.creditTerms = creditTerms;
	}

	
	public Map<String, String> getDriverAgencyList() {
		return driverAgencyList;
	}

	public void setDriverAgencyList(Map<String, String> driverAgencyList) {
		this.driverAgencyList = driverAgencyList;
	}


	public Map<String, String> getBillingCurrency() {
		return billingCurrency;
	}
	public void setBillingCurrency(Map<String, String> billingCurrency) {
		this.billingCurrency = billingCurrency;
	}
/*	public Map<String, String> getCompanyUTSType() {
		return companyUTSType;
	}
	public void setCompanyUTSType(Map<String, String> companyUTSType) {
		this.companyUTSType = companyUTSType;
	}*/
	public Map<String, String> getMovingCompanyType() {
		return movingCompanyType;
	}

	public void setMovingCompanyType(Map<String, String> movingCompanyType) {
		this.movingCompanyType = movingCompanyType;
	}

	public Map<String, String> getUTSmovingCompanyType() {
		return UTSmovingCompanyType;
	}

	public void setUTSmovingCompanyType(Map<String, String> uTSmovingCompanyType) {
		UTSmovingCompanyType = uTSmovingCompanyType;
	}

	public Map<String, String> getDriverType() {
		return driverType;
	}

	public void setDriverType(Map<String, String> driverType) {
		this.driverType = driverType;
	}
	public Set<Role> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<Role> userRole) {
		this.userRole = userRole;
	}



	public String getDonoFlag() {
		return donoFlag;
	}

	public void setDonoFlag(String donoFlag) {
		this.donoFlag = donoFlag;
	}

	public List getDonotMergelist() {
		return donotMergelist;
	}

	public void setDonotMergelist(List donotMergelist) {
		this.donotMergelist = donotMergelist;
	}

	

	public String getPartnerNotes() {
		return partnerNotes;
	}

	public void setPartnerNotes(String partnerNotes) {
		this.partnerNotes = partnerNotes;
	}

	public Map<String, String> getpType() {
		return pType;
	}

	public void setpType(Map<String, String> pType) {
		this.pType = pType;
	}

	

	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}

	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}

	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}

	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}

	

	public String getTempTransPort() {
		return tempTransPort;
	}

	public void setTempTransPort(String tempTransPort) {
		this.tempTransPort = tempTransPort;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Map<String, String> getBankCode() {
		return bankCode;
	}

	public void setBankCode(Map<String, String> bankCode) {
		this.bankCode = bankCode;
	}

	public String getAgentCountryCode() {
		return agentCountryCode;
	}

	public void setAgentCountryCode(String agentCountryCode) {
		this.agentCountryCode = agentCountryCode;
	}

	public String getValidVatCode() {
		return validVatCode;
	}

	public void setValidVatCode(String validVatCode) {
		this.validVatCode = validVatCode;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public Map<String, String> getTaxIdType() {
		return taxIdType;
	}

	public void setTaxIdType(Map<String, String> taxIdType) {
		this.taxIdType = taxIdType;
	}

	public String getEnbState() {
		return enbState;
	}

	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}

	public Map<String, String> getCountryCod() {
		return countryCod;
	}

	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}

	public String getChkAgentTrue() {
		return chkAgentTrue;
	}

	public void setChkAgentTrue(String chkAgentTrue) {
		this.chkAgentTrue = chkAgentTrue;
	}

	

	public void setPartnerPrivateHire(Date partnerPrivateHire){
		this.partnerPrivateHire = partnerPrivateHire;
	}
	public Date getPartnerPrivateHire(){
		return partnerPrivateHire;
	}
	public void setPartnerPrivateTermination(Date partnerPrivateTermination){
		this.partnerPrivateTermination = partnerPrivateTermination;
	}
	public Date getPartnerPrivateTermination(){
		return partnerPrivateTermination;
	}

	public String getVanlineCodeSearch() {
		return vanlineCodeSearch;
	}

	public void setVanlineCodeSearch(String vanlineCodeSearch) {
		this.vanlineCodeSearch = vanlineCodeSearch;
	}

	public List getVanLineList() {
		return vanLineList;
	}

	public void setVanLineList(List vanLineList) {
		this.vanLineList = vanLineList;
	}

	public String getPartnerVanLineCode() {
		return partnerVanLineCode;
	}

	public void setPartnerVanLineCode(String partnerVanLineCode) {
		this.partnerVanLineCode = partnerVanLineCode;
	}

	

	public Map<String, String> getCountryWithBranch() {
		return countryWithBranch;
	}

	public void setCountryWithBranch(Map<String, String> countryWithBranch) {
		this.countryWithBranch = countryWithBranch;
	}

	public List getSelectedJobList() {
		return selectedJobList;
	}

	public void setSelectedJobList(List selectedJobList) {
		this.selectedJobList = selectedJobList;
	}



	public String getFlagHit() {
		return flagHit;
	}

	public void setFlagHit(String flagHit) {
		this.flagHit = flagHit;
	}

	public Map<String, String> getPlanCommissionList() {
		return planCommissionList;
	}

	public void setPlanCommissionList(Map<String, String> planCommissionList) {
		this.planCommissionList = planCommissionList;
	}

	public Map<String, String> getCollectionList() {
		return collectionList;
	}

	public void setCollectionList(Map<String, String> collectionList) {
		this.collectionList = collectionList;
	}


	public String getSearchListOfVendorCode() {
		return searchListOfVendorCode;
	}

	public void setSearchListOfVendorCode(String searchListOfVendorCode) {
		this.searchListOfVendorCode = searchListOfVendorCode;
	}

	public Map<String, String> getListOfVendorCode() {
		return listOfVendorCode;
	}

	public void setListOfVendorCode(Map<String, String> listOfVendorCode) {
		this.listOfVendorCode = listOfVendorCode;
	}



	public Map<String, String> getVatBillingGroups() {
		return vatBillingGroups;
	}

	public void setVatBillingGroups(Map<String, String> vatBillingGroups) {
		this.vatBillingGroups = vatBillingGroups;
	}

	public String getCompDivFlag() {
		return compDivFlag;
	}

	public void setCompDivFlag(String compDivFlag) {
		this.compDivFlag = compDivFlag;
	}

	

	public List getPartnerBankInfoList() {
		return partnerBankInfoList;
	}

	public void setPartnerBankInfoList(List partnerBankInfoList) {
		this.partnerBankInfoList = partnerBankInfoList;
	}

	public String getBankPartnerCode() {
		return bankPartnerCode;
	}

	public void setBankPartnerCode(String bankPartnerCode) {
		this.bankPartnerCode = bankPartnerCode;
	}

	public String getPartnerbanklist() {
		return partnerbanklist;
	}

	public void setPartnerbanklist(String partnerbanklist) {
		this.partnerbanklist = partnerbanklist;
	}



	public String getCurrencyList() {
		return currencyList;
	}

	public void setCurrencyList(String currencyList) {
		this.currencyList = currencyList;
	}

	public String getCheckList() {
		return checkList;
	}

	public void setCheckList(String checkList) {
		this.checkList = checkList;
	}

	public String getNewlineid() {
		return newlineid;
	}

	public void setNewlineid(String newlineid) {
		this.newlineid = newlineid;
	}

	public String getBankNoList() {
		return bankNoList;
	}

	public void setBankNoList(String bankNoList) {
		this.bankNoList = bankNoList;
	}



	public String getCheckCodefromDefaultAccountLine() {
		return checkCodefromDefaultAccountLine;
	}

	public void setCheckCodefromDefaultAccountLine(
			String checkCodefromDefaultAccountLine) {
		this.checkCodefromDefaultAccountLine = checkCodefromDefaultAccountLine;
	}

	public Integer getCompensationYear() {
		return compensationYear;
	}

	public void setCompensationYear(Integer compensationYear) {
		this.compensationYear = compensationYear;
	}

	public boolean isCheckFieldVisibility() {
		return checkFieldVisibility;
	}

	public void setCheckFieldVisibility(boolean checkFieldVisibility) {
		this.checkFieldVisibility = checkFieldVisibility;
	}

	public String getCheckCompensationYear() {
		return checkCompensationYear;
	}

	public void setCheckCompensationYear(String checkCompensationYear) {
		this.checkCompensationYear = checkCompensationYear;
	}

	public int getParentCompensationYear() {
		return parentCompensationYear;
	}

	public void setParentCompensationYear(int parentCompensationYear) {
		this.parentCompensationYear = parentCompensationYear;
	}

	public Map<String, String> getPrefixList() {
		return prefixList;
	}

	public void setPrefixList(Map<String, String> prefixList) {
		this.prefixList = prefixList;
	}
	

	public void setAgentRequestManager(AgentRequestManager agentRequestManager) {
		this.agentRequestManager = agentRequestManager;
	}

	public AgentRequest getAgentRequest() {
		return agentRequest;
	}

	public void setAgentRequest(AgentRequest agentRequest) {
		this.agentRequest = agentRequest;
	}

	public Set getAgentRequestList() {
		return agentRequestList;
	}

	public void setAgentRequestList(Set agentRequestList) {
		this.agentRequestList = agentRequestList;
	}

	public PartnerAccountRefManager getPartnerAccountRefManager() {
		return partnerAccountRefManager;
	}

	public void setPartnerAccountRefManager(
			PartnerAccountRefManager partnerAccountRefManager) {
		this.partnerAccountRefManager = partnerAccountRefManager;
	}

	public PartnerAccountRef getPartnerAccountRef() {
		return partnerAccountRef;
	}

	public void setPartnerAccountRef(PartnerAccountRef partnerAccountRef) {
		this.partnerAccountRef = partnerAccountRef;
	}

	public Map<String, String> getAgentRequestStatus() {
		return agentRequestStatus;
	}

	public void setAgentRequestStatus(Map<String, String> agentRequestStatus) {
		this.agentRequestStatus = agentRequestStatus;
	}

	public List getAgentRequestLists() {
		return agentRequestLists;
	}

	public void setAgentRequestLists(List agentRequestLists) {
		this.agentRequestLists = agentRequestLists;
	}

	public Set getViewAgentList() {
		return viewAgentList;
	}

	public void setViewAgentList(Set viewAgentList) {
		this.viewAgentList = viewAgentList;
	}

	public List getViewAgentLists() {
		return viewAgentLists;
	}

	public void setViewAgentLists(List viewAgentLists) {
		this.viewAgentLists = viewAgentLists;
	}

	
	public String getAgentStatus() {
		return agentStatus;
	}

	public void setAgentStatus(String agentStatus) {
		this.agentStatus = agentStatus;
	}

	public Long getAgentId() {
		return agentId;
	}

	public void setAgentId(Long agentId) {
		this.agentId = agentId;
	}



	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSignatureNote() {
		return signatureNote;
	}

	public void setSignatureNote(String signatureNote) {
		this.signatureNote = signatureNote;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public EmailSetupManager getEmailSetupManager() {
		return emailSetupManager;
	}

	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getCorpid() {
		return corpid;
	}

	public void setCorpid(String corpid) {
		this.corpid = corpid;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public PartnerPrivateManager getPartnerPrivateManager() {
		return partnerPrivateManager;
	}

	public String getCounter() {
		return counter;
	}

	public void setCounter(String counter) {
		this.counter = counter;
	}

	public Long getAgentCountId() {
		return agentCountId;
	}

	public void setAgentCountId(Long agentCountId) {
		this.agentCountId = agentCountId;
	}

	public File getFile6() {
		return file6;
	}

	public void setFile6(File file6) {
		this.file6 = file6;
	}

	public String getFile4FileName() {
		return file4FileName;
	}

	public void setFile4FileName(String file4FileName) {
		this.file4FileName = file4FileName;
	}

	public String getCartonEditURL() {
		return cartonEditURL;
	}

	public void setCartonEditURL(String cartonEditURL) {
		this.cartonEditURL = cartonEditURL;
	}

	public File getFileLogo() {
		return fileLogo;
	}

	public void setFileLogo(File fileLogo) {
		this.fileLogo = fileLogo;
	}

	public String getFileLogoFileName() {
		return fileLogoFileName;
	}

	public void setFileLogoFileName(String fileLogoFileName) {
		this.fileLogoFileName = fileLogoFileName;
	}

	public Map<String, String> getReason() {
		return reason;
	}

	public void setReason(Map<String, String> reason) {
		this.reason = reason;
	}

	public AgentRequestReason getAgentRequestReason() {
		return agentRequestReason;
	}

	public void setAgentRequestReason(AgentRequestReason agentRequestReason) {
		this.agentRequestReason = agentRequestReason;
	}

	
	public void setAgentRequestReasonManager(
			AgentRequestReasonManager agentRequestReasonManager) {
		this.agentRequestReasonManager = agentRequestReasonManager;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAgentreason() {
		return agentreason;
	}

	public void setAgentreason(String agentreason) {
		this.agentreason = agentreason;
	}

	public Long getAgentInfoId() {
		return agentInfoId;
	}

	public void setAgentInfoId(Long agentInfoId) {
		this.agentInfoId = agentInfoId;
	}

	
	public List getFindAgentReasonList() {
		return findAgentReasonList;
	}

	public void setFindAgentReasonList(List findAgentReasonList) {
		this.findAgentReasonList = findAgentReasonList;
	}

	public String getChangeFields() {
		return changeFields;
	}

	public void setChangeFields(String changeFields) {
		this.changeFields = changeFields;
	}

	public String getFileLogoUploadFlag() {
		return fileLogoUploadFlag;
	}

	public void setFileLogoUploadFlag(String fileLogoUploadFlag) {
		this.fileLogoUploadFlag = fileLogoUploadFlag;
	}

	public String getImageDataFileLogo() {
		return imageDataFileLogo;
	}

	public void setImageDataFileLogo(String imageDataFileLogo) {
		this.imageDataFileLogo = imageDataFileLogo;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}


	public void setCorpidList(List corpidList) {
		this.corpidList = corpidList;
	}

	public List getCorpidList() {
		return corpidList;
	}

	public AgentRequest getAgentRequestNew() {
		return agentRequestNew;
	}

	public void setAgentRequestNew(AgentRequest agentRequestNew) {
		this.agentRequestNew = agentRequestNew;
	}
	

	
	 
}