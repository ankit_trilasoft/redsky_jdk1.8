<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.registrationNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.firstName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.lastName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.statusDate'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.job'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.coordinator'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.socialSecurityNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.gbl'].value = "";
	document.forms['serviceOrderListForm'].elements['originCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value = '';
    document.forms['serviceOrderListForm'].elements['serviceOrder.originCountry'].value='';
	document.forms['serviceOrderListForm'].elements['serviceOrder.destinationCountry'].value = '';
    document.forms['serviceOrderListForm'].elements['serviceOrder.billToName'].value='';
	document.forms['serviceOrderListForm'].elements['activeStatus'].checked = false;

	document.forms['serviceOrderListForm'].elements['serviceOrder.companyDivision'].value ="";
	document.forms['serviceOrderListForm'].elements['serviceOrder.bookingAgentShipNumber'].value="";
	<c:if test="${checkAccessQuotation}">
	document.forms['serviceOrderListForm'].elements['serviceOrder.moveType'].value="";
	</c:if>

}
   
function selectSearchField(){
        var originCityCountryOrZip="";
        var destinationCityCountryOrZip="";
        var shipNumber=document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].value; 
		var registrationNumber= document.forms['serviceOrderListForm'].elements['serviceOrder.registrationNumber'].value;
		var lastName=document.forms['serviceOrderListForm'].elements['serviceOrder.lastName'].value;
		var firstName=document.forms['serviceOrderListForm'].elements['serviceOrder.firstName'].value;
		var status=document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value;
		var statusDate=document.forms['serviceOrderListForm'].elements['serviceOrder.statusDate'].value;
		var job=document.forms['serviceOrderListForm'].elements['serviceOrder.job'].value;
		var coordinator=document.forms['serviceOrderListForm'].elements['serviceOrder.coordinator'].value;
		var socialSecurityNumber=document.forms['serviceOrderListForm'].elements['serviceOrder.socialSecurityNumber'].value;
		var activeStatus=document.forms['serviceOrderListForm'].elements['activeStatus'].checked;
		var gbl=document.forms['serviceOrderListForm'].elements['serviceOrder.gbl'].value; 
		var billToName=document.forms['serviceOrderListForm'].elements['serviceOrder.billToName'].value; 
		var cDivision=document.forms['serviceOrderListForm'].elements['serviceOrder.companyDivision'].value;
		var externalSO = document.forms['serviceOrderListForm'].elements['serviceOrder.bookingAgentShipNumber'].value;
	    var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	    if(cityCountryZipVal== 'City' || cityCountryZipVal == 'Zip'){
	             originCityCountryOrZip = document.forms['serviceOrderListForm'].elements['originCityOrZip'].value;
	             destinationCityCountryOrZip = document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value;
	            }
	    else{
	            originCityCountryOrZip = document.forms['serviceOrderListForm'].elements['serviceOrder.originCountry'].value;
	            destinationCityCountryOrZip = document.forms['serviceOrderListForm'].elements['serviceOrder.destinationCountry'].value;
	            }
	    if(activeStatus==false) {
	        if(shipNumber=='' && registrationNumber=='' &&  lastName=='' && firstName=='' && status=='' && statusDate=='' && job=='' && coordinator=='' && activeStatus=='' && socialSecurityNumber=='' && gbl=='' && billToName =='' && cDivision =='' && externalSO=='' && originCityCountryOrZip=='' && destinationCityCountryOrZip=='')
			{
				alert('Please select any one of the search criteria!');	
				return false;	
			}else{  
				return true	;			
			}
			}else{
				return true	;
			}
	}
function showHideCityCountry(){
	var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
	document.forms['serviceOrderListForm'].elements['originCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['destinationCityOrZip'].value='';
	document.forms['serviceOrderListForm'].elements['serviceOrder.originCountry'].value ="";
	document.forms['serviceOrderListForm'].elements['serviceOrder.destinationCountry'].value ="";
}
</script>
<style>
span.pagelinks { display:block;font-size:0.95em;margin-bottom:0px;!margin-bottom:2px;margin-top:-10px;padding:2px 0px;text-align:right;width:100%; }
div.error, span.error, li.error, div.message { width:450px;margin-top:0px; }
form { margin-top:-40px;!margin-top:-5px; }
div#main {margin:-5px 0 0;}
.table td, .table th, .tableHeaderTable td {padding: 0.4em;}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
<script>

function goToSearch(){
	var selectedSearch= selectSearchField();
		if(selectedSearch){
			var externalSO = document.forms['serviceOrderListForm'].elements['serviceOrder.bookingAgentShipNumber'].value.trim();
			var status = document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value.trim();
				if((externalSO=='') && (status!='Not Delivered' && status!='Not Loaded')){
			        document.forms['serviceOrderListForm'].action = 'searchServiceOrders.html';
			        document.forms['serviceOrderListForm'].submit();
				}else{
					 document.forms['serviceOrderListForm'].action = 'searchSOWithExternalSO.html';
				     document.forms['serviceOrderListForm'].submit();
				}
		}else{
			return false;
		}
}
</script>

</head>
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:20px" 
        onclick="location.href='<c:url value="/editServiceOrder.html?id=${customerFile.id}"/>'"
         value="<fmt:message key="button.add"/>"/>   
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton1" cssStyle="width:52px;" align="top" key="button.search" onclick="return goToSearch();"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" onclick="clear_fields();"/> 
</c:set>
<s:hidden name="fileID"  id= "fileID" value=""/>

<s:form id="serviceOrderListForm" action="" method="post" >
<s:hidden name="isSoListShow" value="yes"/>
<s:hidden name="soTab"   value="${soTab}"/>
<c:set var="soTab"  value="${soTab}"/>
<s:hidden name="usertype" value="${usertype}" />
<c:set var="usertype"  value="${usertype}"/>
<s:hidden name="userPortalCheckType"   value="${userPortalCheckType}"/>
<c:set var="userPortalCheckType"  value="${userPortalCheckType}"/>
<s:hidden name="soSortOrder"   value="${soSortOrder}"/>
<c:set var="soSortOrder"  value="${soSortOrder}"/>
<s:hidden name="orderForJob"   value="${orderForJob}"/>
<c:set var="orderForJob"  value="${orderForJob}"/>
<c:if test="${!checkAccessQuotation }">
<s:hidden name="serviceOrder.moveType" />
</c:if>
<configByCorp:fieldVisibility componentId="component.field.customerfile.socialSecurityNumber">
<s:hidden name="serviceOrder.socialSecurityNumber" />
<s:hidden name="serviceOrder.gbl" />
</configByCorp:fieldVisibility>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width:100%;margin-top:2px;" border="1" cellpadding="0" cellspacing="0">
<thead>
<tr>
<th><fmt:message key="serviceOrder.shipNumber"/></th>
<c:if test="${usertype=='USER'}"> 
	<th>External S/O#</th>
</c:if>
<th><fmt:message key="serviceOrder.registrationNumber"/></th>
<th><fmt:message key="serviceOrder.lastName"/></th>
<th><fmt:message key="serviceOrder.firstName"/></th>
<th><fmt:message key="serviceOrder.job"/></th>
<th><fmt:message key="serviceOrder.statusDate"/></th>
<th><fmt:message key="serviceOrder.status"/></th>
<c:if test="${checkAccessQuotation}">
<th>Move Type</th>
</c:if>
<th><fmt:message key="serviceOrder.coordinator"/></th>
<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
<th>Employee #</th>
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.customerfile.socialSecurityNumber">

</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
</tr></thead>	
		<tbody>
		<tr>			
			<td width="">
			    <s:textfield name="serviceOrder.shipNumber" size="9" required="true" cssClass="input-text"/>
			</td>
			<c:if test="${usertype=='USER'}"> 
				<td width="">
				    <s:textfield name="serviceOrder.bookingAgentShipNumber" size="9" required="true" cssClass="input-text" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
				</td>
			</c:if>
			<c:if test="${usertype!='USER'}"> 
				<s:hidden name="serviceOrder.bookingAgentShipNumber" />
			</c:if>
			<td width="">
			    <s:textfield name="serviceOrder.registrationNumber" size="7" required="true" cssClass="input-text" />
			</td>
			<td width="">
			    <s:textfield name="serviceOrder.lastName" size="15" required="true" cssClass="input-text" />
			</td>
			<td width="">
			    <s:textfield name="serviceOrder.firstName" size="7" required="true" cssClass="input-text"/>
			</td>
			<td width="">
			    <s:select cssClass="list-menu" name="serviceOrder.job" list="%{job}" cssStyle="width:100px" headerKey="" headerValue="" />
			</td>		
		
			<td  width="">
			<c:if test="${not empty serviceOrder.statusDate}">
				<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate" /></s:text>
			<s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate" value="%{customerFiledate1FormattedValue}" size="7" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/>
			<img id="statusDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			
			</c:if>
			<c:if test="${empty serviceOrder.statusDate}">
			<s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate" size="7" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/>
			<img id="statusDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			
			</c:if>
			</td>
						
			<td width="">
			<s:select cssClass="list-menu" name="serviceOrder.status" list="%{JOB_STATUS}" cssStyle="width:80px" headerKey="" headerValue="" />
			 </td>
			 <c:if test="${checkAccessQuotation}">
			<td>		
			<s:select cssClass="list-menu" name="serviceOrder.moveType" list="%{moveTypeList}" cssStyle="width:86px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
			</c:if>			
			<td width="" align="left">
				<c:if test="${coordinatr == null || coordinatr == ''}" >
			    	<s:select cssClass="list-menu" name="serviceOrder.coordinator" list="%{coord}" cssStyle="width:100px" headerKey="" headerValue=""/>
			    </c:if>
			    <c:if test="${coordinatr != '' && coordinatr != null}" >
			    	<s:select cssClass="list-menu" name="serviceOrder.coordinator" list="%{coord}" cssStyle="width:100px" headerKey="" headerValue="" value="${coordinatr}"/>
			    </c:if>
			</td>
			<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
			<td width="">
			    <s:textfield name="serviceOrder.socialSecurityNumber" size="9" required="true" cssClass="input-text"/>
			</td>
			</configByCorp:fieldVisibility>
		</tr>
		<tr>
			<td colspan="9" style="margin:0;padding:0;border:1px solid #FFFFFF;"  class="listwhitetext">
			<table style="margin:0px;padding:0px;border:none; float:left;" class="listwhitetext">
				<tr>
			<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
				<td style="border:0px solid #FFFFFF;" class="listwhitetext">
				<fmt:message key="serviceOrder.gbl"/><br>				
					<s:textfield name="serviceOrder.gbl" size="11" required="true" cssStyle="margin-top:2px;" cssClass="input-text"/>
				</td>
			</configByCorp:fieldVisibility>
			
			<td style="border:0px solid #FFFFFF;"  class="listwhitetext">Company Division<br>
			    <s:select cssClass="list-menu" name="serviceOrder.companyDivision" list="%{companyDivis}" cssStyle="width:86px;margin-top:2px;" headerKey="" headerValue="" />
			</td>
				<td style="border:0px solid #FFFFFF;"  class="listwhitetext">Bill To Name<br>
				
					<s:textfield name="serviceOrder.billToName" size="20" required="true" cssStyle="width:120px;margin-top:2px;!width:100px;" cssClass="input-text"/>
				</td>
			
			<configByCorp:fieldVisibility componentId="component.field.customerfile.socialSecurityNumber">
				
			</configByCorp:fieldVisibility>
			
			<td style="border:none;" class="listwhitetext">
			O/D Options&nbsp;<br>
			<s:select name="cityCountryZipOption" id="cityCountryZipOption" list="%{cityCountryZipSearchOption}" cssClass="list-menu" cssStyle="width:70px;margin-right:5px;margin-top:2px;" onchange="showHideCityCountry();"/>
 			</td>
			 <td style="border:none;">
			<table id="showHideCityOrZip" style="margin:0px;padding:0px;border:none; float:left;">
				<tr>
					<td style="border:none;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:textfield  name="originCityOrZip" cssClass="input-text" cssStyle="width:120px;margin-right:5px;margin-top:2px;"/>
					</td>
					<td style="border:none;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:textfield  name="destinationCityOrZip" cssClass="input-text" cssStyle="width:120px;margin-right:0px;margin-top:2px;"/>
					</td>
				</tr>
			</table>
			
			
			<table id="showHideCountry" style="margin:0px;padding:0px;border:none; float:left;">
				
					<td style="border:none;" class="listwhitetext">
					Origin&nbsp;<br>
					<s:select  name="serviceOrder.originCountry" list="%{ocountry}" cssClass="list-menu" cssStyle="width:150px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
					<td style="border:none;" class="listwhitetext">
					Destination&nbsp;<br>
					<s:select  name="serviceOrder.destinationCountry" list="%{dcountry}" cssClass="list-menu" cssStyle="width:150px;margin-right:5px;margin-top:2px;" headerKey="" headerValue="" /></td>
				
			</table>
			</td>
			<td style="border:none;vertical-align:bottom;" class="listwhitetext">
			Active Status<br><s:checkbox key="activeStatus" cssStyle="vertical-align:middle; margin:5px 20px 3px 25px;"/>
			</td>			
			<td style="border:none;" class="listwhitetext">		
			Search Options<br>
			<s:select name="serviceOrderSearchVal" list="%{serviceOrderSearchType}" cssClass="list-menu" cssStyle="margin-top:2px;"/>
			</td>
			<td style="border:none;vertical-align:bottom;">
			<c:out value="${searchbuttons}" escapeXml="false" /></td>
			</tr>
			</table>
			</td>		
			</tr>			
		</tbody>
	</table>
	<div style="!margin-top:7px;"></div>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<c:out value="${searchresults}" escapeXml="false" /> 
<s:set name="externalSOList" value="serviceOrders" scope="request"/>

<div style="float:left;margin-bottom:1px;" id="newmnav">
		  <ul>	
		  <c:if test="${usertype=='USER' && visibilityForSalesPortla=='false'}">	  
		  <li> <a href="soDashboard.html"><span>SO Dashboard</span></a></li>
		  </c:if>		 
		 <li style="background:#FFF" id="newmnav1"><a class="current"><span>Service Order List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>  

<display:table name="externalSOList" class="table" requestURI="" id="serviceOrderList" export="true"  pagesize="10" style="width:1660px;!margin-top:-2px;">   
<c:if test="${userPortalCheckType!='ACCOUNT'}">
	<c:if test="${empty serviceOrderList.moveType || serviceOrderList.moveType=='BookedMove'}">
		<c:if test="${soQuickView=='Y'}">
				 <display:column title="" style="width:5px">
					<div ALIGN="center">
						<img align="middle" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrderList.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View"/>
					</div>
				</display:column>
		</c:if>
	</c:if>
	<c:if test="${serviceOrderList.moveType=='Quote'}">
		<c:if test="${soQuickView=='Y'}">
			 <display:column title="" style="width:5px">
						<div ALIGN="center">
							<img align="middle" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrderList.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View"/>
						</div>
			 </display:column>
		 </c:if>
	</c:if>
<c:choose>
			<%-- <c:when test='${soTab =="accountLineList.html?sid=" && (serviceOrderList.status == "CNCL" || serviceOrderList.status == "DWND" || serviceOrderList.status == "DWNLD")}'>
			   <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.');" href="editServiceOrderUpdate.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column>
			</c:when> --%>
			<c:when test='${soTab =="editMiscellaneous.html?id=" && (serviceOrderList.job == "INT" || serviceOrderList.job =="JVS")}'>
			<s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrderList.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
           <c:if test="${serviceOrderList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="editDspDetails.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${serviceOrderList.job!='RLO'}">
           <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="editTrackingStatus.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column>  
			</c:if> 
             </c:when>
             
            <c:when test='${empty soTab}'>
			   <s:textfield id="relocationServicesKey" name="relocationServicesKey" value="" />
	        <s:textfield id="relocationServicesValue" name="relocationServicesValue"  />
	        <c:set var="relocationServicesKey" value="" />
	        <c:set var="relocationServicesValue" value="" /> 
	        <c:forEach var="entry" items="${relocationServices}">
                <c:if test="${relocationServicesKey==''}">
                <c:if test="${entry.key==serviceOrderList.serviceType}">
	               <c:set var="relocationServicesKey" value="${entry.key}" />
	               <c:set var="relocationServicesValue" value="${entry.value}" /> 
                </c:if>
                </c:if> 
            </c:forEach>
            <c:if test="${serviceOrderList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="editDspDetails.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
           <c:if test="${serviceOrderList.job!='RLO'}">
           <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="editTrackingStatus.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" /> 
                  </a> 
                </display:column>  
			</c:if>  
			</c:when>
			<c:when test='${soTab =="containers.html?id="}'>
            <c:if test="${serviceOrderList.job=='RLO'}">
				 <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
               <a href="editDspDetails.html?id=${serviceOrderList.id}">
                 <c:out value="${serviceOrderList.shipNumber}" />
                  </a> 
                </display:column> 
			</c:if>
			<c:if test="${serviceOrderList.job!='RLO'}">
				 <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
				  <c:if test="${forwardingTabVal!='Y'}">
						<a href="containers.html?id=${serviceOrderList.id}" >
						<c:out value="${serviceOrderList.shipNumber}" />
						</a>
				</c:if>
				<c:if test="${forwardingTabVal=='Y'}">
					<a href="containersAjaxList.html?id=${serviceOrderList.id}" >
					<c:out value="${serviceOrderList.shipNumber}" />
					</a>
				</c:if> 
            	</display:column> 
			</c:if>
             </c:when>
			<c:otherwise>
		      <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
		      <c:if test="${soTab=='editTrackingStatus.html?from=list&id='}">
			        <c:if test="${serviceOrderList.job!='RLO'}">
    	                 <a href="${soTab}${serviceOrderList.id}">                     
        	              <c:out value="${serviceOrderList.shipNumber}" />
            	          </a> 
              		</c:if>
		      		<c:if test="${serviceOrderList.job=='RLO'}">
                    	 <a href="editDspDetails.html?id=${serviceOrderList.id}">                     
                         <c:out value="${serviceOrderList.shipNumber}" />
                         </a> 
              		</c:if>
              </c:if>
		      <c:if test="${soTab!='editTrackingStatus.html?from=list&id='}">
                     <c:if test="${serviceOrderList.job!='RLO'}">
    	                 <a href="${soTab}${serviceOrderList.id}">                     
        	              <c:out value="${serviceOrderList.shipNumber}" />
            	          </a> 
              		</c:if>
              		<c:if test="${serviceOrderList.job=='RLO'}">
                    	 <a href="editDspDetails.html?id=${serviceOrderList.id}">                     
                         <c:out value="${serviceOrderList.shipNumber}" />
                         </a> 
              		</c:if>
              </c:if>
              </display:column>
		    </c:otherwise>
	  </c:choose>
</c:if>
<c:if test="${userPortalCheckType=='ACCOUNT'}">  
	  <display:column sortable="true" style="width:50px" titleKey="serviceOrder.shipNumber" sortProperty="shipNumber">
        <a href="findSummaryList.html?id=${serviceOrderList.id}">
         	<c:out value="${serviceOrderList.shipNumber}" />
        </a> 
      </display:column> 
</c:if>
<display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:100px"/>  
<display:column property="lastName" sortable="true" titleKey="serviceOrder.lastName" style="width:75px" />
<display:column property="firstName" sortable="true" titleKey="serviceOrder.firstName" style="width:75px" />
<display:column property="status" sortable="true" titleKey="serviceOrder.status" style="width:20px" />
<display:column property="statusDate" sortable="true" titleKey="serviceOrder.statusDate" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>  
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="serviceType" sortable="true" titleKey="serviceOrder.serviceType" style="width:20px" maxLength="11" />  
<display:column property="billToName" sortable="true" title="Bill To Name" style="width:95px" maxLength="19"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/>   
<display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>
<display:column  sortable="true" titleKey="serviceOrder.originCity1" style="width:60px"><c:choose><c:when test="${serviceOrderList.originState!='' && serviceOrderList.originState!=null}"><c:set var="res1" value="${serviceOrderList.originCity }, ${serviceOrderList.originState}"/></c:when><c:otherwise><c:set var="res1" value="${serviceOrderList.originCity }"/></c:otherwise></c:choose><c:out value="${res1}"></c:out> </display:column> 
<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"></display:column>
<display:column  sortable="true" titleKey="serviceOrder.destinationCity1" style="width:60px"><c:choose><c:when test="${serviceOrderList.destinationState!='' && serviceOrderList.destinationState!=null }"><c:set var="res" value="${serviceOrderList.destinationCity }, ${serviceOrderList.destinationState}"/></c:when><c:otherwise><c:set var="res" value="${serviceOrderList.destinationCity }"/></c:otherwise></c:choose><c:out value="${res}"></c:out> </display:column>
<display:column property="coordinator" sortable="true" titleKey="serviceOrder.coordinator" style="width:20px"/>  
<configByCorp:fieldVisibility componentId="customerfile.socialSecurityNumber">
<display:column property="gbl" sortable="true" title="GBL #" style="width:50px"/>
<display:column property="socialSecurityNumber" sortable="true" title="Emp #" style="width:50px"/>  
</configByCorp:fieldVisibility>
<display:column property="createdOn" sortable="true" titleKey="serviceOrder.createdOn" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  

    <display:setProperty name="paging.banner.items_name" value="serviceOrder"/>
    <display:setProperty name="paging.banner.item_name" value="serviceorder"/>  
    <display:setProperty name="paging.banner.items_name" value="orders"/>
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
<!--</div>
--><s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<c:if test="${not empty customerFile.sequenceNumber}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${empty customerFile.sequenceNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>
  </s:form>
<script type="text/javascript"> 
    document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].focus(); 
    try{
    <c:if test="${coordinatr != '' && coordinatr != null}" >
    document.forms['serviceOrderListForm'].elements['serviceOrder.coordinator'].value ='${coordinatr}';
   </c:if>
}
catch(e){}
try{
<c:if test="${detailPage == true && userPortalCheckType!='ACCOUNT'}" >
	
	<c:choose> 
			<c:when test='${soTab =="editMiscellaneous.html?id=" && (serviceOrderList.job == "INT" || serviceOrderList.job =="JVS")}'>

                <c:if test="${serviceOrderList.job == 'RLO'}">  
                 
                	location.href = 'editDspDetails.html?id='+${serviceOrderList.id};
              	</c:if> 
                <c:if test="${serviceOrderList.job != 'RLO'}">  
                    
                    location.href =  'editTrackingStatus.html?id='+${serviceOrderList.id};
              	</c:if> 
                
            </c:when>
            
			<c:when test='${empty soTab}'>
            <c:if test="${serviceOrderList.job == 'RLO'}">  
            
        	location.href = 'editDspDetails.html?id='+${serviceOrderList.id};
      	</c:if> 
        <c:if test="${serviceOrderList.job != 'RLO'}">  
            
            location.href =  'editTrackingStatus.html?id='+${serviceOrderList.id};
      	</c:if>
			</c:when>
			<c:otherwise> 
			 <c:if test="${soTab=='editTrackingStatus.html?from=list&id='}">
				<c:if test="${serviceOrderList.job != 'RLO'}">  
				 location.href ="${soTab}${serviceOrderList.id}";
            	</c:if>
				<c:if test="${serviceOrderList.job == 'RLO'}">  
					location.href = 'editDspDetails.html?id='+${serviceOrderList.id};
	        	</c:if>
	        </c:if>
	        <c:if test="${soTab!='editTrackingStatus.html?from=list&id='}">
	        <c:if test="${serviceOrderList.job != 'RLO'}">  
			 location.href ="${soTab}${serviceOrderList.id}";
       		</c:if>
			<c:if test="${serviceOrderList.job == 'RLO'}">  
				location.href = 'editDspDetails.html?id='+${serviceOrderList.id};
       		</c:if>
	        </c:if>	        
            </c:otherwise>
            
	</c:choose>
</c:if>
<c:if test="${detailPage == true && userPortalCheckType=='ACCOUNT'}" >
	location.href =  'findSummaryList.html?id='+${serviceOrderList.id};
</c:if>
}
catch(e){}
  
    highlightTableRows("serviceOrderList");  
    Form.focusFirstElement($("serviceOrderListForm"));
	   	try{
	   	if('${activeStatus}'=='true')
   	{
   		document.forms['serviceOrderListForm'].elements['activeStatus'].checked = true;
   	}
   	}
   	catch(e){}
  try{
   if('${activeStatus}'=='false')
   	{
   		document.forms['serviceOrderListForm'].elements['activeStatus'].checked = false;
   	}
   	}
   	catch(e){}

   	     
</script>
<script type="text/javascript">
try{
	var cityCountryZipVal = document.forms['serviceOrderListForm'].elements['cityCountryZipOption'].value;
	var el = document.getElementById('showHideCityOrZip');
	var el1 = document.getElementById('showHideCountry');
	if(cityCountryZipVal == 'City' || cityCountryZipVal == 'Zip'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
	}else{
		el.style.display = 'none';
		el1.style.display = 'block';	
	}
}catch(e){}
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script> 
<script type="text/javascript">  
try{
	var companyDivision='${defaultCompanyCode}';
	if(companyDivision!=''){
	 document.forms['serviceOrderListForm'].elements['serviceOrder.companyDivision'].value='${companyCode}';
	}
}catch(e){}
</script> 