<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
    <title> GL Type Details </title>   
    <meta name="heading" content="GL Type Details"/>  
 </head>
 <SCRIPT LANGUAGE="JavaScript">
function openGlTypePopWindow(){
var chargesGl=document.forms['glCommisionForm'].elements['glCommissionType.glCode'].value
javascript:openWindow("glTypePopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=glCommissionType.glType&chargesGl="+encodeURI(chargesGl));
}

function openGlCodePopWindow(){
var chargesGl=document.forms['glCommisionForm'].elements['glCommissionType.glCode'].value
javascript:openWindow("glCodePopup.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=glCommissionType.code&chargesGl="+encodeURI(chargesGl));
}
</script>

<s:form id="glCommisionForm" name="glCommisionForm" action="glCommisionFormSave" method="post" validate="true"  > 

<s:hidden name="chargesGl" value="${charges.gl}"/>
<s:hidden name="contractId" value="${contractId}"/>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="description" />
<s:hidden name="glCommissionType.id" value="%{glCommissionType.id}"/>
<s:hidden name="glId" value="<%=request.getParameter("glId")%>"/> 
<div id="Layer1" style="width:90%">
<div id="newmnav">
			  <ul>
			    <li id="newmnav1" style="background:#FFF"><a class="current"><span>GL Type Details</span></a></li>
			    <li><a href="glCommissionTypeList.html?contract=${charges.contract}&charge=${charges.charge}&glId=${charges.id}&contractId=${contractId}"><span>GL&nbsp;Type List</span></a></li>
			    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${glCommissionType.id}&tableName=glcommission&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
			    
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:11px;!margin-top:-14px;"><span></span></div>
   <div class="center-content">  
   
<table class=""  cellspacing="0" cellpadding="2" border="0" style="width:680px">  	
	  	   <tr>
	  			<td height="5px" colspan="4"></td>
	  		</tr>
	  	   <tr>
	  	   	    <td  class="listwhitetext" align="right"  >Contract</td>
	  	   		<td  width="2px"></td>
	  			<td ><s:textfield cssClass="input-text"  name="glCommissionType.contract" value="${charges.contract}" size="35" maxlength="11" readonly="true" tabindex="1" /></td>	  		
	  				  			
	  			<td width="120px" class="listwhitetext" align="right"  >Charge Code</td>
	  			<td  width="2px"></td>
	  			<td ><s:textfield cssClass="input-text" name="glCommissionType.charge" value="${charges.charge}" size="15" maxlength="11" readonly="true" tabindex="2" /></td>
	  			  				  		
				</tr>
	  		<tr>
	  			<td  class="listwhitetext" align="right"  >GL Type</td>
	  			<td  width="2px"></td>
	  			<td><s:textfield cssClass="input-text" name="glCommissionType.glType"  size="10" maxlength="11" readonly="true" tabindex="3"/><img class="openpopup" style="vertical-align:top;" onclick="openGlTypePopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		 			
	  		
	  			<td  class="listwhitetext" align="right"  >GL Code</td>
	  			<td  width="2px"></td>	  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" name="glCommissionType.glCode" size="15" maxlength="11"  tabindex="4" /> 
		  					
	  		</tr>
	  		<tr>
	  			<td  class="listwhitetext" align="right"  >Comms. Type</td>
	  			<td  width="2px"></td>	  			
	  			<td align="left" class="listwhitetext" style=""><s:select name="glCommissionType.type" list="%{glList}"  cssStyle="width:80px;" cssClass="list-menu" headerKey="" headerValue=""  tabindex="5" /></td>
								
	  			<td  class="listwhitetext" align="right"  >Comms. Code</td>
	  			<td  width="2px"></td>	  					
	  			<td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" name="glCommissionType.code" size="15" maxlength="11" readonly="true" tabindex="6" /> <img class="openpopup" style="vertical-align:top;" onclick="openGlCodePopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	 		</tr>
	  		<tr>
	  			<td height="15px" colspan="4"></td>
	  		</tr>
     </table> 
    </div>    
	<div class="bottom-header"><span></span></div> 
</div>

<table border="0">
<tbody>
	<tr>
		<td align="left" class="listwhitetext" width="30px"></td>
	<td colspan="5"></td>
	</tr>
	<tr>
		<td align="left" class="listwhitetext" width="30px"></td>
	<td colspan="5"></td>
	</tr>
	<tr>
		<td align="left" class="listwhitetext" style="width:60px"><b><fmt:message key='companyDivision.createdOn'/></b></td>
		
		<td valign="top">
		
		</td>
		<td style="width:120px">
		<fmt:formatDate var="glCommissionTypeCreatedOnFormattedValue" value="${glCommissionType.createdOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="glCommissionType.createdOn" value="${glCommissionTypeCreatedOnFormattedValue}"/>
		<fmt:formatDate value="${glCommissionType.createdOn}" pattern="${displayDateTimeFormat}"/>
		</td>		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.createdBy' /></b></td>
		<c:if test="${not empty glCommissionType.id}">
			<s:hidden name="glCommissionType.createdBy"/>
			<td style="width:85px"><s:label name="createdBy" value="%{glCommissionType.createdBy}"/></td>
		</c:if>
		<c:if test="${empty glCommissionType.id}">
			<s:hidden name="glCommissionType.createdBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.updatedOn'/></b></td>
		<fmt:formatDate var="glCommissionTypeupdatedOnFormattedValue" value="${glCommissionType.updatedOn}" 
		pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="glCommissionType.updatedOn" value="${glCommissionTypeupdatedOnFormattedValue}"/>
		<td style="width:120px"><fmt:formatDate value="${glCommissionType.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='companyDivision.updatedBy' /></b></td>
		<c:if test="${not empty glCommissionType.id}">
			<s:hidden name="glCommissionType.updatedBy"/>
			<td style="width:85px"><s:label name="updatedBy" value="%{glCommissionType.updatedBy}"/></td>
		</c:if>
		<c:if test="${empty glCommissionType.id}">
			<s:hidden name="glCommissionType.updatedBy" value="${pageContext.request.remoteUser}"/>
			<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
	</tr>
</tbody>
</table>

    <s:submit  cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save"  tabindex="7"  />
		                                          
</div>
</div>

</s:form>
   
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/glCommissionTypeList.html" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>