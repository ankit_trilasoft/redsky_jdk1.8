package com.trilasoft.app.dao;

import java.util.List;
import java.util.Set;

import org.appfuse.dao.GenericDao;
import org.appfuse.model.Role;

import com.trilasoft.app.model.ReportBookmark;



public interface ReportBookmarkDao extends GenericDao<ReportBookmark, Long> {

	public List isExist(Long reportId, String userName);
	
	public List getBookMarkedReportList( String userName, String module, String menu, String subModule, String description,String sessionCorpID, Set<Role> roles);
	public List getBookMarkedFormList(String userName ,String module, String menu, String subModule, String description, String sessionCorpID);
}

