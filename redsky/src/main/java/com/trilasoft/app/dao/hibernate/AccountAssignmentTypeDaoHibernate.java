package com.trilasoft.app.dao.hibernate;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AccountAssignmentTypeDao;
import com.trilasoft.app.dao.TableCatalogDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.Entitlement;
import com.trilasoft.app.model.TableCatalog;

public class AccountAssignmentTypeDaoHibernate extends GenericDaoHibernate<AccountAssignmentType, Long> implements AccountAssignmentTypeDao{
	
	private HibernateUtil hibernateUtil;
	
	public AccountAssignmentTypeDaoHibernate() {
		super(AccountAssignmentType.class);
		
	}
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	public List getaccountAssignmentTypeReference(String partnerCode, String corpID,Long parentId){
		List al=getSession().createSQLQuery("select id from accountassignmenttype where partnerCode='"+ partnerCode +"' and corpID = '"+ corpID +"' and parentId='"+parentId+"' limit 1").list();
		return al;
	}
	
	public List findAssignmentByParentAgent(String parentAgent,String corpId) {		
		List al=getSession().createSQLQuery("select distinct assignment from accountassignmenttype where partnerCode='"+parentAgent+"' and corpId ='"+corpId+"' order by assignment").list();
		return al;
	}
	
	public List getAssignmentTypeList(String partnerCode,Long partnerId, String corpId){
		return getHibernateTemplate().find("from AccountAssignmentType where partnerCode='"+ partnerCode +"' and partnerPrivateId='"+partnerId+"' and corpId ='"+corpId+"'");
	}
	public List checkAssignmentDuplicate(String partnerCode, String corpId,String assignment) {
		List al=getSession().createSQLQuery("select assignment from accountassignmenttype where partnerCode='"+partnerCode+"' and corpID='"+corpId+"'"+" and assignment='"+assignment+"' ").list();
		return al;
	}
}
