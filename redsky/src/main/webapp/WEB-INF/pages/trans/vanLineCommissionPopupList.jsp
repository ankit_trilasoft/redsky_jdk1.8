<%@ include file="/common/taglibs.jsp"%>  
<head>   
<title><fmt:message key="vanLineCommissionTypeList.title"/></title>   
<meta name="heading" content="<fmt:message key='vanLineCommissionTypeList.heading'/>"/>   
<script language="javascript" type="text/javascript">
function clear_fields(){
			document.forms['searchForm'].elements['vanLineCommissionType.code'].value = "";
			document.forms['searchForm'].elements['vanLineCommissionType.type'].value = "";
			document.forms['searchForm'].elements['vanLineCommissionType.description'].value = "";
			document.forms['searchForm'].elements['vanLineCommissionType.glCode'].value = "";
}
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:99%;
}
div.error, span.error, li.error, div.message {

width:250px;
margin: -10px auto;
font-size: 1.05em;

}
</style>

</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editVanLineCommissionType.html"/>'"  value="<fmt:message key="button.add"/>"/>   
</c:set>  
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;!margin-bottom:10px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px; " onclick="clear_fields();"/> 
</c:set>   

     
<s:form id="searchCommissionPopup" action="searchCommissionPopupList.html?decorator=popup&popup=true" method="post" validate="true" >
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>

<div id="layer1" style="width:100%">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<c:if test="${vanLineCommissionTypeList=='[]'}"> 
				    <div class="message" style="">Please enter your search criteria below</div>
				    </c:if>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
<div class="center-content">
<table class="table" style="width:100%"  >
	<thead>
		<tr>
			<th><fmt:message key="vanLineCommissionType.code"/></th> 
			<th><fmt:message key="vanLineCommissionType.description"/></th>
			<th><fmt:message key="vanLineCommissionType.glCode"/></th>
			
		</tr>
	</thead>	
		<tbody>
		<tr>
			<td width="20" align="left"> <s:textfield name="vanLineCommissionType.code" required="true" cssClass="input-text" size="25"/> </td>
			<s:hidden name="vanLineCommissionType.type" value="D" />
			
			<td width="20" align="left"> <s:textfield name="vanLineCommissionType.description" required="true" cssClass="input-text" size="25"/> </td>
			<td width="20" align="left"> <s:textfield name="vanLineCommissionType.glCode" required="true" cssClass="input-text" size="25"/> </td>
			
		</tr>
		<tr>
		<td colspan="2"></td>
		<td align="left" style="border-left:hidden;" > <c:out value="${searchbuttons}" escapeXml="false" /> </td>
		</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>		

<c:out value="${searchresults}" escapeXml="false" />  
		<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Commission Code List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="vanLineCommissionTypeList" value="vanLineCommissionTypeList" scope="request"/>   
<display:table name="vanLineCommissionTypeList" class="table" requestURI="" id="vanLineCommissionTypeList" export="${empty param.popup}" defaultsort="1" pagesize="10" style="width:100%" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >  
    <c:if test="${empty param.popup}">  
    <display:column property="code" sortable="true" titleKey="vanLineCommissionType.code" url="/editVanLineCommissionType.html?from=list" paramId="id" paramProperty="id"/>
    </c:if>
    <c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="vanLineCommissionType.code"/>   
    </c:if> 
    <display:column property="type" sortable="true" titleKey="vanLineCommissionType.type"/>
    <display:column property="description" sortable="true" titleKey="vanLineCommissionType.description"/>
    <display:column property="glCode" sortable="true" titleKey="vanLineCommissionType.glCode"/>
    <display:column property="pc" headerClass="containeralign" style="text-align: right;" sortable="true" titleKey="vanLineCommissionType.pc" />
      
    <display:setProperty name="paging.banner.item_name" value="vanLineCommissionType"/>   
    <display:setProperty name="paging.banner.items_name" value="vanLineCommissionTypes"/>   
  
    <display:setProperty name="export.excel.filename" value="Van Line Commission Type List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Van Line Commission Type List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Van Line Commission Type List.pdf"/>   
</display:table>   
<c:set var="isTrue" value="false" scope="session"/>
</div>
</s:form>  
 
<script type="text/javascript">   
    highlightTableRows("vanLineCommissionTypeList");  
    Form.focusFirstElement($("searchForm"));    
</script> 