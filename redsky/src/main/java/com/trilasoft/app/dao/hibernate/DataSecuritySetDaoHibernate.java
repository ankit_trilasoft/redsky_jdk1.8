package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DataSecuritySetDao;
import com.trilasoft.app.dao.hibernate.UserlogfileDaoHibernate.DtoPermission;
import com.trilasoft.app.model.DataSecuritySet;

public class DataSecuritySetDaoHibernate extends GenericDaoHibernate<DataSecuritySet, Long>implements DataSecuritySetDao{
	
	public DataSecuritySetDaoHibernate()
	{
		super(DataSecuritySet.class);
	}

	public List findById(Long id) {
		return null;
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from DataSecuritySet");
	}

	public List getDataSecurityFilterList() {
		return getHibernateTemplate().find("select name from DataSecurityFilter order by name");
	}

	public List getPermission(String permissionName) {
		return getHibernateTemplate().find("from DataSecuritySet where name='"+permissionName+"'");
	}

	public List getPermissionList() {
		return getHibernateTemplate().find("select name from DataSecuritySet");
	}

	public List getSetList() {
		return getHibernateTemplate().find("FROM DataSecuritySet group by id");
	}

	public void saveDataSecuritySet(DataSecuritySet dataSecuritySet) {
		getHibernateTemplate().saveOrUpdate(dataSecuritySet);
		
	}

	public void deleteFilters(String prefixDataSet, String sessionCorpID) {
		this.getSession().createSQLQuery("delete from datasecuritypermission where datasecurityset_id=(select id from datasecurityset where name ='"+prefixDataSet+"' and corpID='"+sessionCorpID+"')");
		getHibernateTemplate().bulkUpdate("delete from DataSecuritySet where name='"+prefixDataSet+"' and corpID='"+sessionCorpID+"'");
		
	}

	public List getPartnerUsersList(String prefixDataSet, String sessionCorpID) {
		
		List userlist=this.getSession().createSQLQuery("Select username, first_name, last_name, userType, ap.id, account_enabled, ap.email, ap.contact from app_user ap, userdatasecurity uds, datasecurityset  dss where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id and dss.name like '%"+prefixDataSet+"' and ap.corpID='"+sessionCorpID+"'").list();
		List result= new ArrayList(); 
		
		if(userlist.size()>0){
			Iterator it=userlist.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setFirst_name(obj[1]);
				userDTO.setLast_name(obj[2]);
				userDTO.setUserType(obj[3]);
				userDTO.setId(obj[4]);
				userDTO.setAccount_enabled(obj[5]);
				userDTO.setEmail(obj[6]);
				userDTO.setContact(obj[7]);
				result.add(userDTO);
			}
		}
		return  result;
	}
	
	public List getUnassignedPartnerUsersList(String prefixDataSet, String sessionCorpID) {
		List userlist=this.getSession().createSQLQuery("select userName, first_name, last_name, userType, ap.id, account_enabled, ap.email, ap.contact,ap.relocationContact from app_user ap , user_role  ur, role r where ap.id=ur.user_id and ur.role_id=r.id and r.name='ROLE_AGENT' and ap.corpID='"+sessionCorpID+"' and account_enabled=false and username not in (Select username from app_user ap, userdatasecurity uds, datasecurityset  dss where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id and dss.name='"+prefixDataSet+"' and ap.corpID='"+sessionCorpID+"') order by ap.userName").list();
		List result= new ArrayList(); 
		
		if(userlist.size()>0){
			Iterator it=userlist.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setFirst_name(obj[1]);
				userDTO.setLast_name(obj[2]);
				userDTO.setUserType(obj[3]);
				userDTO.setId(obj[4]);
				userDTO.setAccount_enabled(obj[5]);
				userDTO.setEmail(obj[6]);
				userDTO.setContact(obj[7]);
				userDTO.setRelocationContact(obj[8]);
				result.add(userDTO);
			}
		}
		return  result;
	}
	
	public  class UserDTO
	 {
		private Object username;
		private Object first_name;
		private Object last_name;
		private Object userType;
		private Object id;
		private Object account_enabled;
		private Object email;
		private Object contact;
		private Object relocationContact;
		private Object alias;
		
		public Object getFirst_name() {
			return first_name;
		}
		public void setFirst_name(Object first_name) {
			this.first_name = first_name;
		}
		public Object getLast_name() {
			return last_name;
		}
		public void setLast_name(Object last_name) {
			this.last_name = last_name;
		}
		public Object getUsername() {
			return username;
		}
		public void setUsername(Object username) {
			this.username = username;
		}
		public Object getUserType() {
			return userType;
		}
		public void setUserType(Object userType) {
			this.userType = userType;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getAccount_enabled() {
			return account_enabled;
		}
		public void setAccount_enabled(Object account_enabled) {
			this.account_enabled = account_enabled;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getContact() {
			return contact;
		}
		public void setContact(Object contact) {
			this.contact = contact;
		}
		public Object getRelocationContact() {
			return relocationContact;
		}
		public void setRelocationContact(Object relocationContact) {
			this.relocationContact = relocationContact;
		}
		public Object getAlias() {
			return alias;
		}
		public void setAlias(Object alias) {
			this.alias = alias;
		}
	 }

	public List getDataSecuritySet(String prefixDataSet, String sessionCorpID) {
		return getHibernateTemplate().find("FROM DataSecuritySet where name='"+prefixDataSet+"' and corpID='"+sessionCorpID+"'");
	}

	public List getAssociatedAgents(String parentAgentCode, String sessionCorpID) {
		return getHibernateTemplate().find("select partnerCode from PartnerPublic where agentParent='"+parentAgentCode.trim()+"'  and partnerCode<>'"+parentAgentCode.trim()+"' and status = 'Approved' and partnerPortalActive is true ");
	}

	public List search(String name, String description) {
		if(name==null){name="";} 
		if(description==null){description="";} 
		return getHibernateTemplate().find("FROM DataSecuritySet where name like '%"+name.replaceAll("'", "''")+"%' and description like '%"+description.replaceAll("'", "''")+"%'");
	}

	public List validateUserName(String userName, String sessionCorpID) {
		return this.getSession().createSQLQuery("select id from app_user where username='"+userName+"'").list();
	}

	public List getUsers(String roleName, String userName, String sessionCorpID) {
		List userlist=this.getSession().createSQLQuery("select a.username, a.first_name, a.last_name, a.account_enabled, a.id from app_user a , user_role  b, role r where a.id=b.user_id and b.role_id=r.id and r.name like '"+roleName+"%' and a.username like '%"+userName+"%'  and a.corpID='"+sessionCorpID+"' group by a.username;").list();
		List result= new ArrayList(); 
		
		if(userlist.size()>0){
			Iterator it=userlist.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setFirst_name(obj[1]);
				userDTO.setLast_name(obj[2]);
				userDTO.setAccount_enabled(obj[3]);
				userDTO.setId(obj[4]);
				result.add(userDTO);
			}
		}
		return  result;
	}

	public List getRoleList(String sessionCorpID) {
		return getHibernateTemplate().find("select name from Role where corpID='"+sessionCorpID+"' order by name");
	}

	public  class DtoUserList{
		 private Object userName;
		 private Object roles;

		public Object getUserName() {
			return userName;
		}

		public void setUserName(Object userName) {
			this.userName = userName;
		}

		public Object getRoles() {
			return roles;
		}

		public void setRoles(Object roles) {
			this.roles = roles;
		}

		
	}
	
	public List getUserList(String sessionCorpID, String name) {

	    List list1=new ArrayList();
		String query="select a.userName from app_user a , userdatasecurity uds, datasecurityset dst where dst.id= uds.userdatasecurity_id and a.id=uds.user_id and dst.name='"+name+"' and a.corpID='"+sessionCorpID+"';";
		List lista=getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
		while(it.hasNext())
		{
			Object row= (Object)it.next();
			DtoUserList userListDTO = new DtoUserList();
			userListDTO.setUserName(row.toString());
			list1.add(userListDTO);
		} 
		return list1;

	}

	public List getUserRoleSet(String userName, String sessionCorpID) {
		List list1=new ArrayList();
		String query="select r.name from app_user ap , user_role  ur, role r where ap.id=ur.user_id and ur.role_id=r.id and ap.corpID='"+sessionCorpID+"' and ap.username='"+userName+"' group by r.name";
		List lista=getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
		while(it.hasNext())
		{
			Object row= (Object)it.next();
			DtoUserList userListDTO = new DtoUserList();
			userListDTO.setRoles(row.toString());
			list1.add(userListDTO);
		} 
		return list1;
	}

	public List getRequestedPartner(String partnerCode, String sessionCorpID) {
		List userlist=this.getSession().createSQLQuery("select userName, first_name, last_name, userType, ap.id, account_enabled, ap.email, ap.contact from app_user ap where ap.branch='"+partnerCode+"' and ap.corpID='"+sessionCorpID+"' and ap.account_enabled is false order by ap.userName").list();
		List result= new ArrayList(); 
		
		if(userlist.size()>0){
			Iterator it=userlist.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setFirst_name(obj[1]);
				userDTO.setLast_name(obj[2]);
				userDTO.setUserType(obj[3]);
				userDTO.setId(obj[4]);
				userDTO.setAccount_enabled(obj[5]);
				userDTO.setEmail(obj[6]);
				userDTO.setContact(obj[7]);
				result.add(userDTO);
			}
		}
		return  result;
	}

	public List getAgentsUsersList(String partnerCode, String sessionCorpID,String checkOption) {
		String condition=""; 
		String condition1="";
			if(checkOption.equalsIgnoreCase("BOTH"))
			{
				condition="";
			}else if(checkOption.equalsIgnoreCase("CONTAC")){
				condition=" AND contact is true AND account_enabled is false";
			}else if(checkOption.equalsIgnoreCase("USER")){
				condition=" AND contact is false AND account_enabled is true";
			}else{
				condition1="NoRec";
			}
		List newTempParent=new ArrayList();	
		String tempPartnerCode="";
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"')").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+partnerCode+"'";
		}else{
			partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
		}
		List agentUsers= this.getSession().createSQLQuery("Select distinct username, first_name, last_name, userType, ap.id, " +
				"account_enabled, ap.email, ap.contact,ap.relocationContact ,alias from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
				"datasecurityfilter dsf, datasecuritypermission dsp " +
				"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
				""+condition+" "+ 
				"and dsf.id=dsp.datasecurityfilter_id " +
				"and dss.id=dsp.datasecurityset_id " +
				// "and ap.parentAgent =  dsf.filtervalues " +
				"and dsf.filtervalues in("+partnerCodes+")" +
				"and ap.corpID in ('TSFT','"+sessionCorpID+"')" +
				" union " +
				"Select distinct username, first_name, last_name, userType, id,account_enabled,email, contact , relocationContact,alias " +
				"from app_user " +
				"where basedAt in("+partnerCodes+") " +
				"and corpID in ('TSFT','"+sessionCorpID+"')" +
				""+condition+" " +
				"order by 1").list();
		
			List result= new ArrayList(); 
		
		if(agentUsers.size()>0){
			Iterator it=agentUsers.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setFirst_name(obj[1]);
				userDTO.setLast_name(obj[2]);
				userDTO.setUserType(obj[3]);
				userDTO.setId(obj[4]);
				userDTO.setAccount_enabled(obj[5]);
				userDTO.setEmail(obj[6]);
				userDTO.setContact(obj[7]);
				userDTO.setRelocationContact(obj[8]);
				userDTO.setAlias(obj[9]);
				result.add(userDTO);
			}
		}
		if(condition1.equalsIgnoreCase("NoRec"))
		{
			result=null;
		}
		return result;
	}

	public Map<String, List> getChildAgentCodeMap() { 
		Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
		String query = "select agentparent,corpid,group_concat(partnercode) from partnerpublic where agentparent != '' and  agentparent is not null  and agentparent !=partnercode and status ='Approved' group by agentparent order by agentparent";
		List childAgentCodeList =  this.getSession().createSQLQuery(query).list();
		Iterator its=childAgentCodeList.iterator();
		while (its.hasNext()) {
		List codeList =new ArrayList(); 
		Object[] row = (Object[]) its.next();
		if(row[2] !=null){
		String partnercode = row[2].toString();
		String[] str1 = partnercode.split(","); 
		for (int i=0; i < str1.length; i++) {
			codeList.add(str1[i]);
		}
		}else{
		
		}
		childAgentCodeMap.put(row[1].toString().trim()+"~"+row[0].toString().trim(), codeList);
			} 
		return childAgentCodeMap;
	}

	
}
