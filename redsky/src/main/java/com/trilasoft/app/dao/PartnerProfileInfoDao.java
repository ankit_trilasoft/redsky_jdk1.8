package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PartnerProfileInfo;

public interface PartnerProfileInfoDao extends GenericDao<PartnerProfileInfo, Long> { 
	
	public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode);

}
