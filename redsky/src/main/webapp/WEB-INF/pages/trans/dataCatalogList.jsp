<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataCatalogList.title"/></title> 
<meta name="heading" content="<fmt:message key='dataCatalogList.heading'/>"/> 

<style>
span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:5px;
	margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:100%;
}
</style>
</head> 
<c:set var="buttons">  
     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/dataCatalogForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set> 

<s:form id="dataCatalogList" name="dataCatalogList" action="searchDataCatalog" method="post" validate="true">  
<s:hidden name="id" /> 
<div id="Layer1" style="width: 100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:13px;!margin-top:-5px;"><span></span></div>
    <div class="center-content">
<table class="table" style="width: 98%;">
<thead>
<tr>
<th><fmt:message key="dataCatalog.tableName"/></th>
<th><fmt:message key="dataCatalog.fieldName"/></th>
<th><fmt:message key="dataCatalog.description"/></th>
<th>Field Type</th>
<th><fmt:message key="dataCatalog.defineByToDoRule"/></th>
<th><fmt:message key="dataCatalog.auditable"/></th>
<th><fmt:message key="dataCatalog.configerable"/></th>
<th><fmt:message key="dataCatalog.charge"/></th>
</tr></thead> 

<tbody>
  <tr>
   <td>
       <s:textfield name="tableName" required="true" cssClass="input-text" size="8"/>
   </td>
    <td>
       <s:textfield name="fieldName" required="true" cssClass="input-text" size="8"/>
   </td>
   <td>
       <s:textfield name="description" required="true" cssClass="input-text" size="8"/>
   </td>
   <td>
   <%-- Drop Down List Added by Kunal Sharma on Feb-6-2012. Ticket Number: 5953 --%>
   		 <s:select cssClass="list-menu" name="usDomestic" list="%{usDomesticList}"  headerKey="" headerValue="" cssStyle="width:80px"/>
   </td>
    <td>
   		<s:select cssClass="list-menu"   name="defineByToDoRule" list="{'true','false'}" headerKey="" headerValue="" cssStyle="width:80px"/>
   </td>
   
   <td>
   		<s:select cssClass="list-menu"   name="auditable" list="{'true','false'}" headerKey="" headerValue="" cssStyle="width:80px"/>
   </td>
   
   <td>
   		<s:select cssClass="list-menu"   name="visible" list="{'true','false'}" headerKey="" headerValue="" cssStyle="width:80px"/>
   </td>
   <td>
   		<s:select cssClass="list-menu"   name="charge" list="{'Quantity','Price'}" headerKey="" headerValue="" cssStyle="width:80px"/>
   </td>
   </tr>
   <tr>
    <td colspan="7"></td>
    <td align="center" style="border-left: hidden;">
         <input type="button" class="cssbutton1" value="Search" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="searchCatalogList()"/>  
         <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
     </td>
  </tr>
  </tbody>
 </table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" /> 
</s:form>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Field Configuration</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<display:table name="dataCatalogList" class="table" requestURI="" id="DataCatalogssList"   export="true" pagesize="100"> 
<display:column titleKey="dataCatalog.tableName"><a onclick="goToDetails('${DataCatalogssList.id}');" ><c:out value="${DataCatalogssList.tableName}" /></a> </display:column>
<display:column property="fieldName"  sortable="true" titleKey="dataCatalog.fieldName"/> 
<display:column property="description" sortable="true" titleKey="dataCatalog.description"/> 
<display:column property="usDomestic" sortable="true" title="Field Type"/> 
 
 <c:if test="${DataCatalogssList.defineByToDoRule=='true' }">
 <display:column titleKey="dataCatalog.defineByToDoRule" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
 <c:if test="${DataCatalogssList.defineByToDoRule=='false' }">
 <display:column titleKey="dataCatalog.defineByToDoRule"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
 </c:if>
<%-- <c:if test="${DataCatalogssList.isdateField=='true' }">
 <display:column titleKey="dataCatalog.isdateField" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
 <c:if test="${DataCatalogssList.isdateField=='false' }">
 <display:column titleKey="dataCatalog.isdateField"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
 </c:if> --%>
 <c:if test="${DataCatalogssList.auditable=='true' }">
 <display:column titleKey="dataCatalog.auditable" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
<c:if test="${DataCatalogssList.auditable=='false' }">
 <display:column titleKey="dataCatalog.auditable"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
</c:if>
<c:if test="${DataCatalogssList.visible=='true' }">
 <display:column titleKey="dataCatalog.configerable" style="width:5px"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
 </c:if>
<c:if test="${DataCatalogssList.visible=='false' }">
 <display:column titleKey="dataCatalog.configerable"><img src="${pageContext.request.contextPath}/images/cancel001.gif" /></display:column> 
</c:if>

<display:column property="charge" sortable="true" titleKey="dataCatalog.charge"/> 
	<display:column title="Remove" style="text-align:center; width:20px;">
				<a><img align="middle" onclick="confirmSubmit(${DataCatalogssList.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
		</display:column>
</display:table> 

 </div>
<c:out value="${buttons}" escapeXml="false" /> 

<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove it?");
	var id = targetElement;
	if (agree){
		location.href="deleteDataCatalog.html?id="+id;
	}else{
		return false;
	}
}

function clear_fields(){
	var i;
	for(i=0;i<=7;i++){
		document.forms['dataCatalogList'].elements[i].value = "";
	}
}
function searchCatalogList(){
//alert(targetElement);
	document.forms['dataCatalogList'].action='searchDataCatalog.html';
	document.forms['dataCatalogList'].submit();
}

function goToDetails(targetValue){
    document.forms['dataCatalogList'].elements['id'].value = targetValue;
    document.forms['dataCatalogList'].action='dataCatalogForm.html';
	document.forms['dataCatalogList'].submit();
}
  
    //highlightTableRows("DataCatalogssList");  
    //Form.focusFirstElement($("searchForm"));    
</script> 
