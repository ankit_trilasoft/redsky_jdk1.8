<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
 <%@page autoFlush="true" %>
<head>   
    <title>Demo Ajax Search</title>   
    <meta name="heading" content="Distribution Revenue"/>   
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script src="${pageContext.request.contextPath}/scripts/jquery.min.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
 
   
<style>
span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}
span.links{
display: block;
    font-size: 0.85em;
    margin-bottom: 0;
    margin-top: 0px;
    padding: 0px 0;
    text-align: right;
    width: 100%;
    clear:both;
}

div.error, span.error, li.error, div.message {

width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-5px;
}

div#main {
margin:-5px 0 0;

}
.table td, .table th, .tableHeaderTable td {   
    padding: 0.4em;
}

div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

div#otabs{ 
margin-bottom: 0px;
}
</style>
<meta http-equiv="cache-control" content="no-cache" />
</head>
<c:set var="searchbuttons">   
	<input type="button" class="cssbutton1"  align="top"  id="searchBtn" value="Search" />   
    <input type="button" class="cssbutton1" value="Clear" style="width:50px;" id="clearBtn" /> 
</c:set>

<s:hidden name="accID" />
<s:hidden name="fileNo" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spn" style="clear:both;">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top" style="!margin-top:5px;">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="table" border="1" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th><fmt:message key="serviceOrder.shipNumber"/></th>
			<th><fmt:message key="serviceOrder.registrationNumber"/></th>
			<th><fmt:message key="serviceOrder.lastName"/></th>
			<th><fmt:message key="serviceOrder.firstName"/></th>
		</tr>
	</thead>	
	<tbody>
		<tr>			
			<td width="">
			    <s:textfield name="serviceOrder.shipNumber" size="20" required="true" cssClass="input-text" id="shipno"/>
			</td>
			<td width="">
			    <s:textfield name="serviceOrder.registrationNumber" size="20" required="true" cssClass="input-text" id="regno" />
			</td>
			<td width="">
			    <s:textfield name="serviceOrder.lastName" size="20" required="true" cssClass="input-text" id="lname" />
			</td>
			<td width="">
			    <s:textfield name="serviceOrder.firstName" size="20" required="true" cssClass="input-text" id="fname" />
			</td>
		</tr>
		<tr>
		<td style="border:none;vertical-align:bottom;text-align:right;" colspan="5" >
			<c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
<c:out value="${searchresults}" escapeXml="false" />  
<div style="!margin-top:7px;"></div>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="otabs" >
	<ul>
		<li><a class="current"><span>Service Order List</span></a></li>
	</ul>
</div>
<div id="loading"><img src="${pageContext.request.contextPath}/images/newloader.gif"></div>
<span class="links" ></span>
<div id="result">
	<display:table name="SOListExt" class="table" id="soList" pagesize="100" >
		 <display:column sortable="true" style="width:50px" title="Ship Number" sortProperty="shipNumber" />
    	<display:column property="registrationNumber"  title="Registration #" sortable="true" />
		<display:column property="lastName"  title="Last Name" sortable="true"/>
		<display:column property="firstName"  title="First Name" sortable="true"/>
	</display:table>    
</div>
 <script language="javascript">
 

$(document).ready(function (){
	$("#loading").hide();
	
	$("#searchBtn").click(function(){
		
		
		var shipno = $("#shipno").val();		
		var regno = $("#regno").val();
		var lname =  $("#lname").val();
		var fname = $("#fname").val();
		var page = 1;
		var result = document.getElementById("result");
		
		$.get("ajaxDemoSearch.html?ajax=1&decorator=simple&popup=true&page="+page, 
			{ shipno: shipno, regno: regno,lname: lname, fname: fname, page:page},
			function(data){
				
				$("#result").html(data);
			});
	});
	
$("#clearBtn").click(function(){
	$("input[type='text']").val(""); 
	});
	$(document).ajaxStart(function() {
		$( "#loading" ).show();
		//$( "#result" ).hide();
		});
	$(document).ajaxStop(function() {
		$( "#loading" ).hide();
		$( "#result" ).show();
		});
	$.ajaxSetup({cache: false});
})
</script>



  