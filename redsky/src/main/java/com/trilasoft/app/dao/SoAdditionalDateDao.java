package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SoAdditionalDate;

public interface SoAdditionalDateDao extends GenericDao<SoAdditionalDate, Long>{
	public List findId(Long sid, String sessionCorpID);
}
