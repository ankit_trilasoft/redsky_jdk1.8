package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RefQuoteServices;

public interface RefQuoteServicesManager extends GenericManager<RefQuoteServices, Long> {
	public List searchList(String job, String mode,String routing, String langauge, String sessionCorpID ,String description,String checkIncludeExclude,String companyDivision,String commodity,String packingMode,String originCountry,String destinationCountry,String serviceType,String contract);
	public void autoSaveRefQuotesSevisesAjax(String sessionCorpID,String fieldName,String fieldVal,Long serviceId);
	public Map<String, String> findForContract(String corpID) ;

	/*public String findUniqueCode(String code, String sessionCorpID);*/

}
