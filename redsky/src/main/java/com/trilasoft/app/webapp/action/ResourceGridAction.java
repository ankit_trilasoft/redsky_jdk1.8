package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.ResourceGrid;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ResourceGridManager;

public class ResourceGridAction extends BaseAction implements Preparable{
	
	private Long id;
	private String sessionCorpID;
	private ResourceGrid resourceGrid;
	private ResourceGridManager resourceGridManager;
	private RefMasterManager refMasterManager;
	private Map<String, String> resourceCategory;
	private String hub;
	private Long oId;
	
	public void prepare() throws Exception {
		
	}
	
	public ResourceGridAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}   
	
	public String getComboList(String corpID){
		 resourceCategory = refMasterManager.findByParameter(corpID, "Resource_Category");
	 	 return SUCCESS;
	}
	
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			resourceGrid = resourceGridManager.get(id);
		} else {			
			resourceGrid = new ResourceGrid(); 
			resourceGrid.setCreatedOn(new Date());
			resourceGrid.setUpdatedOn(new Date());
		
		} 
		return SUCCESS;
	}
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (resourceGrid.getId() == null);  
				if (isNew) { 
					resourceGrid.setCreatedOn(new Date());
		        } 
				resourceGrid.setCorpID(sessionCorpID);
				resourceGrid.setUpdatedOn(new Date());
				resourceGrid.setUpdatedBy(getRequest().getRemoteUser()); 
				resourceGridManager.save(resourceGrid);  
			    String key = (isNew) ?"Resource Grid have been saved." :"Resource Grid have been saved." ;
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	public String delete(){		
		resourceGridManager.remove(id);    
	  return SUCCESS;		    
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public ResourceGrid getResourceGrid() {
		return resourceGrid;
	}

	public void setResourceGrid(ResourceGrid resourceGrid) {
		this.resourceGrid = resourceGrid;
	}

	public void setResourceGridManager(ResourceGridManager resourceGridManager) {
		this.resourceGridManager = resourceGridManager;
	}

	public Map<String, String> getResourceCategory() {
		return resourceCategory;
	}

	public void setResourceCategory(Map<String, String> resourceCategory) {
		this.resourceCategory = resourceCategory;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public Long getoId() {
		return oId;
	}

	public void setoId(Long oId) {
		this.oId = oId;
	}

}
