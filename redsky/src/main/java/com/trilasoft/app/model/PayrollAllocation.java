package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="payrollallocation")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class PayrollAllocation extends BaseObject{
	private Long id;  
	private String corpID;
	 public String code;
	 public String job;
	 public String service;
	 public String bucket;
	 public String calculation;
	 public String revenueRate1;
	 public String revenueRate2;
	 public String rateType;
	 private String companyDivision;
	 private String mode;
	 
	 
	  	private String createdBy;
		private String updatedBy;
		private Date createdOn;
		private Date updatedOn;
		private Date valueDate;
		
	@Override
		public String toString() {
			return new ToStringBuilder(this).append("id", id).append("corpID",
					corpID).append("code", code).append("job", job).append(
					"service", service).append("bucket", bucket).append(
					"calculation", calculation)
					.append("revenueRate1", revenueRate1).append("revenueRate2",
							revenueRate2).append("rateType", rateType).append(
							"companyDivision", companyDivision).append("createdBy",
							createdBy).append("updatedBy", updatedBy).append(
							"createdOn", createdOn).append("updatedOn", updatedOn)
					.append("valueDate", valueDate).append("mode", mode).toString();
		}
	@Override
		public boolean equals(final Object other) {
			if (!(other instanceof PayrollAllocation))
				return false;
			PayrollAllocation castOther = (PayrollAllocation) other;
			return new EqualsBuilder().append(id, castOther.id).append(corpID,
					castOther.corpID).append(code, castOther.code).append(job,
					castOther.job).append(service, castOther.service).append(
					bucket, castOther.bucket).append(calculation,
					castOther.calculation).append(revenueRate1,
					castOther.revenueRate1).append(revenueRate2,
					castOther.revenueRate2).append(rateType, castOther.rateType)
					.append(companyDivision, castOther.companyDivision).append(
							createdBy, castOther.createdBy).append(updatedBy,
							castOther.updatedBy).append(createdOn,
							castOther.createdOn).append(updatedOn,
							castOther.updatedOn).append(valueDate,
							castOther.valueDate).append(mode, castOther.mode).isEquals();
		}
	@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(corpID).append(code)
					.append(job).append(service).append(bucket).append(calculation)
					.append(revenueRate1).append(revenueRate2).append(rateType)
					.append(companyDivision).append(createdBy).append(updatedBy)
					.append(createdOn).append(updatedOn).append(valueDate).append(mode)
					.toHashCode();
		}
	@Column( length=25 )
	public String getBucket() {
		return bucket;
	}
	public void setBucket(String bucket) {
		this.bucket = bucket;
	}
	@Column( length=25 )
	public String getCalculation() {
		return calculation;
	}
	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}
	@Column( length=25 )
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	@Column( length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getRevenueRate1() {
		return revenueRate1;
	}
	public void setRevenueRate1(String revenueRate1) {
		this.revenueRate1 = revenueRate1;
	}
	public String getRevenueRate2() {
		return revenueRate2;
	}
	public void setRevenueRate2(String revenueRate2) {
		this.revenueRate2 = revenueRate2;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column( length=10 )
	public String getRateType() {
		return rateType;
	}
	public void setRateType(String rateType) {
		this.rateType = rateType;
	}
	@Column( length=25 )
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	 

}
