/**
 * @Class Name  Container Dao
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Container;

public interface ContainerDao extends GenericDao<Container ,Long>
{
	public List<Container> findByLastName(String lastName);
	public List findMaxId();
    public List checkById(Long id);
    public List getGross(String shipNumber);
    public List getTare(String shipNumber);
    public List getNet(String shipNumber);
    public List getPieces(String shipNumber);
    public List getVolume(String shipNumber);
    public List getWeights(String shipNumber);
    public List getWeightUnit(String shipNumber);
    public List getVolumeUnit(String shipNumber);
    public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID);
    public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy);
    public int updateDeleteStatus(Long ids);
    public List searchForwarding(String containerNo, String bookingNo, String corpid, String blNumber, Date sailDate,String lastName,String vesselFilight,String caed);
    public List refreshWeights(String shipNum, String packingMode, String sessionCorpID);
    public int refreshContainer(String shipNum, String containerNos, String sessionCorpID,String userName);
    public List containerList(Long serviceOrderId);
    public List containerAuditList(Long serviceOrderId);
    public List getGrossKilo(String shipNumber);
    public List getTareKilo(String shipNumber);
    public List getNetKilo(String shipNumber);
    public List getVolumeCbm(String shipNumber);
    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID);
    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy);
    public List findMaximumChild(String shipNm);
    public List findMinimumChild(String shipNm);
    public List findCountChild(String shipNm);
    public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public Long findRemoteContainer(String idNumber, Long serviceOrderID);
	public List findRemoteContainerList(String shipNumber);
	public void deleteNetworkContainer(Long serviceOrderID, String idNumber,String containerStatus);
	public List findByServiceOrderId(Long sofid, String sessionCorpID);
	public List fieldsInfoById(Long grpId, String sessionCorpID);
	public List containerListByGrpId(Long grpId, Long contId);
	public List containerListByIdAndgrpId(Long grpId);
	public List orderIdWithNoContainer(String newIdList, String sessionCorpId);
	public List containersForSizeUpdate(String draftOrdersId);
	 public List containerListOfMaster(Long containerId);
	 public List containerWithNoLine(String sessionCorpId,Long grpId,Long masterContainerId);
	 public void updateContainerId(Long mastercontainerid, String oldIdList);
	 public List getExistingContainerList(String sessionCorpId,String shipNumber,Long masterContainerId);
	 public List getGrossNetwork(String shipNumber, String corpID);
	public List getNetNetwork(String shipNumber, String corpID);
	public List getPiecesNetwork(String shipNumber, String corpID);
	public List getVolumeNetwork(String shipNumber, String corpID);
	public List containerListOtherCorpId(Long sid);
	public void updateContainerDetails(Long id,String fieldName,String fieldValue,String tableName,String sessionCorpID,String shipNumber, String userName);
	public void updateContainerDetailsAllFields(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateContainerDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateContainerDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateContainerDetailsNetWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateContainerTareWtKilo(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateContainerDetailsDensityMetric(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName, String sessionCorpID,String userName);
	public void updateStatus(Long id,String containerStatus, String updateRecords);
	public String findContainerNumber(String corpId,String shipNumber);
	public List findServicePartnerData(String corpId,String shipNumber);
}

//End of Class.   