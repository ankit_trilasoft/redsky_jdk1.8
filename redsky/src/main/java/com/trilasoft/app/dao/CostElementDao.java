package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CostElement;


public interface CostElementDao extends GenericDao<CostElement, Long> {
	public List getCostListDataSummary();
	public String costElDetailCode(String costEl);
	public String costDesDetailCode(String costDes);
	public List findCostElementList(String chargeCostElement, String sessionCorpID,String jobType, String routing, String soCompanyDivision);
	public List findAgentCostElement(String costElement, String agentCorpId);
	public List getCharges(String costElement, String sessionCorpID);
	public int updateAccountlineGLByCostElement(String charge, String contract, String sessionCorpID, String recGl, String payGl,Long costElementId);
	public int updateAccountlineGLByGLRateGrid(String charge, String contract, String sessionCorpID, String job, String routing, String recGl, String payGl,String companyDivision);
	public int checkForCostEleUseInCharge(String costElement,String description, String recGl, String payGl, String sessionCorpID);
	public List getCostElementValue(String tableN,String tableD);
	public List findCostElementListTemplate(String chargeCode,String sessionCorpID, String jobType, String routing,String contract, String soCompanyDivision);
	public Map<String, String> costElementDataMap(String agentCorpID);
	public List findAgentCostElementData(String sessionCorpID,String agentCorpId);
}
