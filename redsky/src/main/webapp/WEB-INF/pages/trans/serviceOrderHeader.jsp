<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:-3px;"><span></span></div>
   <div class="center-content">
<table  cellspacing="1" cellpadding="0" border="0" width="100%">
   <tbody>
    <tr><td align="left" class="listwhitebox">
       <table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%" style="margin-left:8px;">
         <tbody>                  
             <tr>
               <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">
             <tr>
             <td align="right" style="width: 39px;"><fmt:message key="billing.shipper"/></td>
             <td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" size="23"  cssStyle="width:139px;" cssClass="input-textUpper" readonly="true" tabindex="1"/>
             <td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true" tabindex="1" cssStyle="margin-left: 2px;"/></td>
            
             <td align="right" width="30"><fmt:message key="billing.originCountry"/></td>
             <td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" cssStyle="margin-left:2px;" size="18" readonly="true" tabindex="1" /></td>
             <td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="4" readonly="true" tabindex="1" /></td>
             <td align="right" width="29"><fmt:message key="billing.Type"/></td>
             <td align="left"><s:textfield name="serviceOrder.job" cssClass="input-textUpper" size="5" readonly="true" tabindex="1" /></td>
             <c:if test="${serviceOrder.job !='RLO'}"> 
             <td align="right" width="59"><fmt:message key="billing.commodity"/></td>
             <td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="4" readonly="true" tabindex="1" /></td>
             <td align="right" width="40"><fmt:message key="billing.routing"/></td>
             <td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="3" readonly="true" tabindex="1" /></td>
           	</c:if>
           <td align="right" width="30"><fmt:message key='customerFile.status'/></td>
           <td colspan="3" align="left" class="listwhitetext"> <s:textfield  cssClass="input-textUpper" key="serviceOrder.status" size="3" readonly="true" tabindex="1" /> </td>
             </tr>             
             </table>
             </td>
             </tr>
             <tr>
             <td align="left" colspan="16">
             <table style="margin:0px;padding:0px;">
             <tr> 
             <td align="right" width="39"><fmt:message key="billing.jobNo"/></td>
             <td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true" tabindex="1"/></td>
             <td align="right" width="32"><fmt:message key="billing.registrationNo"/></td>
             <td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true" tabindex="1"/></td>
                     
             <td align="right" width="30"><fmt:message key="billing.destination"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="18" readonly="true" tabindex="1" cssStyle="margin-left:2px;"/></td>
             <td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="4" readonly="true" tabindex="1"/></td>
             <c:if test="${serviceOrder.job!='RLO'}">
             <td align="right" width="28"><fmt:message key="billing.mode"/></td>
             <td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" cssStyle="margin-left:1px;" size="5" readonly="true" tabindex="1"/></td>
             </c:if>
             <td align="right" style="width:59px;"><fmt:message key="billing.AccName"/></td>
             <td align="left" colspan="5"><s:textfield name="serviceOrder.billToName" value="${billing.billToName}" cssClass="input-textUpper" size="34" cssStyle="width:205px;" readonly="true" tabindex="1"/></td>
          <c:if test="${usertype=='AGENT' || usertype=='ACCOUNT'}">
             <td align="right">Billing&nbsp;Status</td>
<c:choose>
<c:when test="${serviceOrder.actualRevenue =='0' || serviceOrder.actualRevenue=='0.0' || serviceOrder.actualRevenue=='0.00' || serviceOrder.actualRevenue==null}">
<td align="left" colspan="5"><s:textfield name="" cssClass="input-textUpper" size="18" value="Not Invoiced" readonly="true" tabindex="1"/></td>
</c:when>
<c:when test="${serviceOrder.actualRevenue !='0' && serviceOrder.actualRevenue !='0.0' && serviceOrder.actualRevenue !='0.00' && billing.billComplete == null}">
<td align="left" colspan="5"><s:textfield name="" cssClass="input-textUpper" size="18" value="Invoicing started" readonly="true" tabindex="1"/></td>
</c:when>
<c:otherwise>
<td align="left" colspan="5"><s:textfield name="" cssClass="input-textUpper" size="18" value="Invoicing completed" readonly="true" tabindex="1"/></td>	
</c:otherwise>
</c:choose> 
</c:if>
             </tr>
             </table>
             </td>
             </tr>
            </tbody>
     </table>
     </td></tr>
     <tr>
<td align="left" colspan="18" class="vertlinedata"></td>
</tr>
     <tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="">
<tbody>


                                          <tr>
                                          <c:if test="${serviceOrder.job!='RLO'}">
                                          
                                          <configByCorp:fieldVisibility componentId="component.field.Alternative.hideForVoerman">
                                          <td colspan="4" id="weightVolMain11" >
                                           <table style="margin:0px;padding:0px;">
                                           <tr>
                                           <td align="right" class="listwhitetext">Est&nbsp;Wght</td>                               
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                         </c:if>
                                         <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="right" class="listwhitetext">Act&nbsp;Wght</td>
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit1=="Lbs"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit1 !='Lbs'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 !='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeightKilo}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit1 =='Lbs'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
                                           </tr>
                                           </table>
                                           </td>
										
											
                                           </configByCorp:fieldVisibility>
                                           
                                           <configByCorp:fieldVisibility componentId="component.field.Alternative.showForCorpID">
                                              <td colspan="2" id="chargeableMain" style="display:none;">
                                           <table style="margin:0px;padding:0px;">
                                           <tr>
                                           	<td height="25" align="center" class="listwhitetext">Chargeable&nbsp;Weight</td>
											<c:if test="${serviceOrder.mode =='Air'&& miscellaneous.unit1=='Kgs'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="chargeableGrossWeight" value="%{miscellaneous.chargeableGrossWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'&& miscellaneous.unit1=='Kgs'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="chargeableGrossWeight" value="%{miscellaneous.chargeableNetWeightKilo}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<c:if test="${serviceOrder.mode =='Air'&& miscellaneous.unit1=='Lbs'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="chargeableGrossWeight" value="%{miscellaneous.chargeableGrossWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'&& miscellaneous.unit1=='Lbs'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="chargeableGrossWeight" value="%{miscellaneous.chargeableNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
											</c:if>
											<td align="left" ><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true"/></td>
										
											 <td align="right" class="listwhitetext">Chargeable&nbsp;Vol.</td>
                               				 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.chargeableCubicFeet}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.chargeableCubicMtr}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.chargeableNetCubicMtr}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.chargeableNetCubicFeet}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
											</tr>
											</table></td>
                                           <td colspan="4" id="weightVolMain" >
                                           <table style="margin:0px;padding:0px;">
                                           <tr>
                                           <td align="right" class="listwhitetext">Est&nbsp;Vol.</td>
                               				 <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicFeet}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.estimateCubicMtr}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicMtr}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateCubicFeet" value="%{miscellaneous.netEstimateCubicFeet}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="right" class="listwhitetext">Act&nbsp;Vol.</td>
                                           <c:if test='${serviceOrder.mode =="Air" && miscellaneous.unit2=="Cft"}'> 
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicFeet}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                          <c:if test="${serviceOrder.mode =='Air' && miscellaneous.unit2 !='Cft'}">
                                          <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.actualCubicMtr}" size="6" required="true" maxlength="10" readonly="true" tabindex="1"/></td>
                                          </c:if>
                                            <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 !='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicMtr}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${serviceOrder.mode !='Air' && miscellaneous.unit2 =='Cft'}">
                                           <td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualCubicFeet" value="%{miscellaneous.netActualCubicFeet}" size="6" maxlength="10" required="true" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <td align="left"><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;width:25px;" id="unit2" value="%{miscellaneous.unit2}" size="3"  maxlength="10" readonly="true" tabindex="1"/></td>
                                           </tr>
                                           </table>
                                           </td>
                                           </configByCorp:fieldVisibility>
                                           </c:if>
                                           <configByCorp:fieldVisibility componentId="customerFile.survey">
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
											<c:if test="${not empty customerFile.survey}">
											<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="Survey" value="${customerFileSurveyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
											</c:if>
											<c:if test="${empty customerFile.survey}">
											<td><s:textfield cssClass="input-textUpper" name="Survey" value="${customerFileSurveyFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td><%--<td><img id="calender2" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['customerFileForm'].survey,'calender2',document.forms['customerFileForm'].dateFormat.value); return false;"/></td>--%>
											</c:if>
											</configByCorp:fieldVisibility> 
											 <configByCorp:fieldVisibility componentId="trackingStatus.surveyDate">
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.survey'/></td>
											<c:if test="${not empty trackingStatus.surveyDate}">
											<s:text id="trackingStatusSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.surveyDate"/></s:text>
											<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
											</c:if>
											<c:if test="${empty trackingStatus.surveyDate}">
											<td><s:textfield cssClass="input-textUpper" name="SurveyDate" value="${trackingStatusSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
											</c:if>
											</configByCorp:fieldVisibility>
											 <c:if test="${serviceOrder.job!='RLO'}">   
                                            <td align="right" class="listwhitetext">Pack&nbsp;Date</td>                                            
                                           <c:if test="${empty trackingStatus.packA}">
                                           <c:if test="${empty trackingStatus.beginPacking}">
                                           <td><s:textfield cssClass="input-textUpper" name="packingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:56px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           <c:if test="${not empty trackingStatus.beginPacking}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginPacking"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:56px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>  
                                           </c:if>
                                           <c:if test="${not empty trackingStatus.packA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="packingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:56px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           
                                           <td align="right" class="listwhitetext">Load&nbsp;Date</td>
                                           <c:if test="${empty trackingStatus.loadA}">
                                           <c:if test="${empty trackingStatus.beginLoad}">
                                           <td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${not empty trackingStatus.beginLoad}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>                                             </c:if>
                                           <c:if test="${not empty trackingStatus.loadA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           
                                           
                                           <td align="right" class="listwhitetext">Delivery</td>
                                           <c:if test="${empty trackingStatus.deliveryA}">
                                           <c:if test="${empty trackingStatus.deliveryShipper}">
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                           <c:if test="${not empty trackingStatus.deliveryShipper}">
                                           <td width="10px"><font color="red" size="1">(T)</font></td>
                                           <s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>                                             </c:if>
                                           <c:if test="${not empty trackingStatus.deliveryA}">
                                           <td width="10px"><font color="red" size="1">(A)</font></td>
                                           <s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
                                           <td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="1"/></td>
                                           </c:if>
                                          
												</c:if>										
											
                                           
                                           <td align="right" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
                                           <td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="32"  maxlength="250" cssStyle="width:178px;" readonly="true" tabindex="1"/></td>
                                          <configByCorp:fieldVisibility componentId="component.field.serviceOrder.coordinator">
								           <td align="right" class="listwhitetext"><fmt:message key="serviceOrder.coordinator"/></td>
							               <td align="left"><s:textfield name="serviceOrder.coordinator" cssClass="input-textUpper" size="18" readonly="true" tabindex="1"/></td>
							              </configByCorp:fieldVisibility>
                                       </tr>
                                           </tbody>
                             </table>
                             </td>
                            </tr>
                            <tr>
                              <td align="left" colspan="18" class="vertlinedata"></td>
                             </tr>
   </tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:31px;!margin-top:41px;"><span style="margin-left:50px; padding-top:5px">
  <c:if test="${billing.cod}">
  <div style="position:absolute; width:95%; text-align:center; font-size:14px;padding-top:3px; font-family:Tahoma,Calibri,Verdana,Geneva,sans-serif; font-weight:bold; color:#fe0303 "> COD </div>
   <img id="cod1" src="${pageContext.request.contextPath}/images/cod-blue.png" width="95%" />
      </c:if>
                              
                           
</span></div>
</div>
</div>
<s:hidden name="customerFileId" value="%{customerFile.id}"/>
<script type="text/JavaScript">
function volumeDisplay(){

	var weightVolSectionId = document.getElementById('weightVolMain');
	if(weightVolSectionId != null && weightVolSectionId != undefined){
		document.getElementById('weightVolMain').style.display = 'none';
		document.getElementById('chargeableMain').style.display = 'block';
		try{
		document.getElementById('weightVolMain11').style.display = 'none';
		}catch(e){
		}
		
	}
}
try{
	<sec-auth:authComponent componentId="module.tab.serviceorder.weightvol">  
	 volumeDisplay();
  </sec-auth:authComponent>
   
}catch(e){
}
</script>

