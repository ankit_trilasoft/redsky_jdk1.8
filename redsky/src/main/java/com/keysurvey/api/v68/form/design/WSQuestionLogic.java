/**
 * WSQuestionLogic.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSQuestionLogic  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSAdvancedLogic[] advancedLogics;

    private com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic[] fuzzyLogicList;

    private com.keysurvey.api.v68.form.design.WSGoToLogic goToLogic;

    private long questionId;

    private com.keysurvey.api.v68.form.design.WSSkipLogic[] skipLogics;

    public WSQuestionLogic() {
    }

    public WSQuestionLogic(
           long versionId,
           com.keysurvey.api.v68.form.design.WSAdvancedLogic[] advancedLogics,
           com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic[] fuzzyLogicList,
           com.keysurvey.api.v68.form.design.WSGoToLogic goToLogic,
           long questionId,
           com.keysurvey.api.v68.form.design.WSSkipLogic[] skipLogics) {
        super(
            versionId);
        this.advancedLogics = advancedLogics;
        this.fuzzyLogicList = fuzzyLogicList;
        this.goToLogic = goToLogic;
        this.questionId = questionId;
        this.skipLogics = skipLogics;
    }


    /**
     * Gets the advancedLogics value for this WSQuestionLogic.
     * 
     * @return advancedLogics
     */
    public com.keysurvey.api.v68.form.design.WSAdvancedLogic[] getAdvancedLogics() {
        return advancedLogics;
    }


    /**
     * Sets the advancedLogics value for this WSQuestionLogic.
     * 
     * @param advancedLogics
     */
    public void setAdvancedLogics(com.keysurvey.api.v68.form.design.WSAdvancedLogic[] advancedLogics) {
        this.advancedLogics = advancedLogics;
    }

    public com.keysurvey.api.v68.form.design.WSAdvancedLogic getAdvancedLogics(int i) {
        return this.advancedLogics[i];
    }

    public void setAdvancedLogics(int i, com.keysurvey.api.v68.form.design.WSAdvancedLogic _value) {
        this.advancedLogics[i] = _value;
    }


    /**
     * Gets the fuzzyLogicList value for this WSQuestionLogic.
     * 
     * @return fuzzyLogicList
     */
    public com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic[] getFuzzyLogicList() {
        return fuzzyLogicList;
    }


    /**
     * Sets the fuzzyLogicList value for this WSQuestionLogic.
     * 
     * @param fuzzyLogicList
     */
    public void setFuzzyLogicList(com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic[] fuzzyLogicList) {
        this.fuzzyLogicList = fuzzyLogicList;
    }

    public com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic getFuzzyLogicList(int i) {
        return this.fuzzyLogicList[i];
    }

    public void setFuzzyLogicList(int i, com.keysurvey.api.v68.form.design.WSFuzzyJumpLogic _value) {
        this.fuzzyLogicList[i] = _value;
    }


    /**
     * Gets the goToLogic value for this WSQuestionLogic.
     * 
     * @return goToLogic
     */
    public com.keysurvey.api.v68.form.design.WSGoToLogic getGoToLogic() {
        return goToLogic;
    }


    /**
     * Sets the goToLogic value for this WSQuestionLogic.
     * 
     * @param goToLogic
     */
    public void setGoToLogic(com.keysurvey.api.v68.form.design.WSGoToLogic goToLogic) {
        this.goToLogic = goToLogic;
    }


    /**
     * Gets the questionId value for this WSQuestionLogic.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSQuestionLogic.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }


    /**
     * Gets the skipLogics value for this WSQuestionLogic.
     * 
     * @return skipLogics
     */
    public com.keysurvey.api.v68.form.design.WSSkipLogic[] getSkipLogics() {
        return skipLogics;
    }


    /**
     * Sets the skipLogics value for this WSQuestionLogic.
     * 
     * @param skipLogics
     */
    public void setSkipLogics(com.keysurvey.api.v68.form.design.WSSkipLogic[] skipLogics) {
        this.skipLogics = skipLogics;
    }

    public com.keysurvey.api.v68.form.design.WSSkipLogic getSkipLogics(int i) {
        return this.skipLogics[i];
    }

    public void setSkipLogics(int i, com.keysurvey.api.v68.form.design.WSSkipLogic _value) {
        this.skipLogics[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSQuestionLogic)) return false;
        WSQuestionLogic other = (WSQuestionLogic) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.advancedLogics==null && other.getAdvancedLogics()==null) || 
             (this.advancedLogics!=null &&
              java.util.Arrays.equals(this.advancedLogics, other.getAdvancedLogics()))) &&
            ((this.fuzzyLogicList==null && other.getFuzzyLogicList()==null) || 
             (this.fuzzyLogicList!=null &&
              java.util.Arrays.equals(this.fuzzyLogicList, other.getFuzzyLogicList()))) &&
            ((this.goToLogic==null && other.getGoToLogic()==null) || 
             (this.goToLogic!=null &&
              this.goToLogic.equals(other.getGoToLogic()))) &&
            this.questionId == other.getQuestionId() &&
            ((this.skipLogics==null && other.getSkipLogics()==null) || 
             (this.skipLogics!=null &&
              java.util.Arrays.equals(this.skipLogics, other.getSkipLogics())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAdvancedLogics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdvancedLogics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAdvancedLogics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getFuzzyLogicList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFuzzyLogicList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFuzzyLogicList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGoToLogic() != null) {
            _hashCode += getGoToLogic().hashCode();
        }
        _hashCode += new Long(getQuestionId()).hashCode();
        if (getSkipLogics() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getSkipLogics());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getSkipLogics(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSQuestionLogic.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionLogic"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("advancedLogics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "advancedLogics"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAdvancedLogic"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuzzyLogicList");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fuzzyLogicList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFuzzyJumpLogic"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("goToLogic");
        elemField.setXmlName(new javax.xml.namespace.QName("", "goToLogic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSGoToLogic"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("skipLogics");
        elemField.setXmlName(new javax.xml.namespace.QName("", "skipLogics"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSSkipLogic"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
