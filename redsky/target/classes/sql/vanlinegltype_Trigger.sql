delimiter $$

CREATE trigger redsky.trigger_add_history_vanlinegltype
BEFORE UPDATE on redsky.vanlinegltype
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



   IF (OLD.glType <> NEW.glType ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.glType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glType,'~') into newValueList;
  END IF;





   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;





   IF (OLD.glCode <> NEW.glCode ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.glCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glCode,'~') into newValueList;
  END IF;





   IF (OLD.calc <> NEW.calc ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.calc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.calc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.calc,'~') into newValueList;
  END IF;





   IF (OLD.bucket <> NEW.bucket ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.bucket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket,'~') into newValueList;
  END IF;





   IF (OLD.invItem <> NEW.invItem ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.invItem~') into fieldNameList;
      select CONCAT(oldValueList,OLD.invItem,'~') into oldValueList;
      select CONCAT(newValueList,NEW.invItem,'~') into newValueList;
  END IF;





   IF (OLD.realCalc <> NEW.realCalc ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.realCalc~') into fieldNameList;
      select CONCAT(oldValueList,OLD.realCalc,'~') into oldValueList;
      select CONCAT(newValueList,NEW.realCalc,'~') into newValueList;
  END IF;





   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'vanlinegltype.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;






    IF ((OLD.discrepancy <> NEW.discrepancy) or (OLD.discrepancy is null and NEW.discrepancy is not null)
      or (OLD.discrepancy is not null and NEW.discrepancy is null)) THEN
        select CONCAT(fieldNameList,'vanlinegltype.discrepancy~') into fieldNameList;
        IF(OLD.discrepancy is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.discrepancy is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.discrepancy is not null) then
            select CONCAT(oldValueList,OLD.discrepancy,'~') into oldValueList;
        END IF;
        IF(NEW.discrepancy is not null) then
            select CONCAT(newValueList,NEW.discrepancy,'~') into newValueList;
        END IF;
   END IF;     
    




    IF ((OLD.pc <> NEW.pc) or (OLD.pc is null and NEW.pc is not null)
      or (OLD.pc is not null and NEW.pc is null)) THEN
        select CONCAT(fieldNameList,'vanlinegltype.pc~') into fieldNameList;
        IF(OLD.pc is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pc is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pc is not null) then
            select CONCAT(oldValueList,OLD.pc,'~') into oldValueList;
        END IF;
        IF(NEW.pc is not null) then
            select CONCAT(newValueList,NEW.pc,'~') into newValueList;
        END IF;
   END IF;     
    




    IF ((OLD.shortHaul <> NEW.shortHaul) or (OLD.shortHaul is null and NEW.shortHaul is not null)
      or (OLD.shortHaul is not null and NEW.shortHaul is null)) THEN
        select CONCAT(fieldNameList,'vanlinegltype.shortHaul~') into fieldNameList;
        IF(OLD.shortHaul is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.shortHaul is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.shortHaul is not null) then
            select CONCAT(oldValueList,OLD.shortHaul,'~') into oldValueList;
        END IF;
        IF(NEW.shortHaul is not null) then
            select CONCAT(newValueList,NEW.shortHaul,'~') into newValueList;
        END IF;
   END IF;     
    



  IF (OLD.doNotAuto <> NEW.doNotAuto ) THEN
       select CONCAT(fieldNameList,'vanlinegltype.doNotAuto~') into fieldNameList;
       IF(OLD.doNotAuto = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.doNotAuto = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.doNotAuto = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.doNotAuto = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


     IF (OLD.useDrvrGL <> NEW.useDrvrGL ) THEN
       select CONCAT(fieldNameList,'vanlinegltype.useDrvrGL~') into fieldNameList;
       IF(OLD.useDrvrGL = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.useDrvrGL = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.useDrvrGL = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.useDrvrGL = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


     IF (OLD.calcByDrvr <> NEW.calcByDrvr ) THEN
       select CONCAT(fieldNameList,'vanlinegltype.calcByDrvr~') into fieldNameList;
       IF(OLD.calcByDrvr = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.calcByDrvr = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.calcByDrvr = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.calcByDrvr = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


     IF (OLD.putGL <> NEW.putGL ) THEN
       select CONCAT(fieldNameList,'vanlinegltype.putGL~') into fieldNameList;
       IF(OLD.putGL = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.putGL = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.putGL = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.putGL = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



 CALL add_tblHistory (OLD.id,"vanlinegltype", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;