<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Existing Contracts List</title>   
    <meta name="heading" content="Existing Contracts List"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}


 span.pagelinks {
display:block;
font-size:0.85em;
margin-top:-2px;
padding:2px 0;
text-align:right;
width:100%;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style> 
	
<script type="text/javascript"> 
  </script>  
</head>

<s:form id="existingContractsList" action="" method="post"> 
<s:set name="existingContractsList" value="existingContractsList" scope="request"/> 
<div id="Layer1" style="width:80%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Existing Contracts List</span></a></li>
		  </ul>
		</div>
<div class="spnblk">&nbsp;</div> 
</div>
<display:table name="existingContractsList" class="table" requestURI="" id="existingContractsList"  defaultsort="1" pagesize="20" >   
 		
		<display:column property="carrier" sortable="true" title="Carrier" style="width:25%" />
		<display:column property="serviceContractNo" sortable="true" title="Service Contract #"   style="width:42%"/> 
		<display:column  sortable="true" title="Count" headerClass="containeralign" style="text-align:right;width:5%;" > 
		<div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${existingContractsList.count}" /></div>
        </display:column> 
	     
</display:table>
</td></tr>
<tr>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td></tr>
</table> 
</div>
</s:form>

<script type="text/javascript">   
</script>  
		  	