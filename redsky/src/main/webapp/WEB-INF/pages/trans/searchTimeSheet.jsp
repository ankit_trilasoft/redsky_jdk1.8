<%@ include file="/common/taglibs.jsp"%> 

<head>
<meta name="heading" content="<fmt:message key='timeSheet.heading'/>"/> 
<title><fmt:message key="timeSheet.title"/></title> 

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<!-- Modification closed here -->

<script language="javascript" type="text/javascript">

function clear_fields(){
			document.forms['timeSheetSearchForm'].elements['workDt'].value = "";
			document.forms['timeSheetSearchForm'].elements['serv'].value = "";
			document.forms['timeSheetSearchForm'].elements['wareHse'].value = "";
			document.forms['timeSheetSearchForm'].elements['tkt'].value = "";
			document.forms['timeSheetSearchForm'].elements['shipper'].value = "";
			document.forms['timeSheetSearchForm'].elements['crewNm'].value = "";
		
}
function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) ; 
	}
function checkCondition(){
	if(document.forms['timeSheetSearchForm'].elements['workDt'].value == ""){
 		alert("Select work date to continue.....");
 		document.forms['timeSheetSearchForm'].elements['workDt'].select();
 		return false;
 	}
 	if(document.forms['timeSheetSearchForm'].elements['wareHse'].value == ""){
 		alert("Select Crew WareHouse to continue.....");
 		return false;
 	}
 	if(document.forms['timeSheetSearchForm'].elements['tktWareHse'].value == ""){
 		alert("Select Ticket WareHouse to continue.....");
 		return false;
 	}
 	
}

function selectTicketWarehouse()
{
	document.forms['timeSheetSearchForm'].elements['tktWareHse'].value=document.forms['timeSheetSearchForm'].elements['wareHse'].value;
}
</script>

<style>

.topHead { background:url transparent (images/topbandnew.jpg) no-repeat scroll 0%;}

</style>
</head>
<s:form cssClass="form_magn" name="timeSheetSearchForm" action="searchCrewTickets.html?filter=ShowAll">

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer1" style="width:100%; margin-top:20px;"><!-- sandeep --> 
<div id="otabs">
			  <ul>
			    <li><a class="current" style="padding:0px 0px 0px 8px;"><span>Search for available crews and timesheet records</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="4" border="0" style="width:564px" >
<thead>
<tr>
<td align="right" class="listwhitetext" >Work Date<font color="red" size="2">*</font></td>
<td align="left" style="width:450px"><s:textfield cssClass="input-text" id="date1" name="workDt" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" /><img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</tr>
	<tr><td align="right" class="listwhitetext" width="100px">Crew Warehouse<font color="red" size="2">*</font></td>
		<td style="width:250px"><s:select name="wareHse" list="%{wareHouse}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" tabindex="1" onchange="selectTicketWarehouse();"/></td>
	</tr>
	<tr><td align="right" class="listwhitetext" width="100px">Ticket Warehouse<font color="red" size="2">*</font></td>
		<td style="width:250px"><s:select name="tktWareHse" list="%{wareHouse}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" tabindex="1"/></td>
	</tr>
	<!--<tr><td align="right" class="listwhitetext">Service</td>
		<td style="width:100px"><s:textfield name="serv" required="true" cssClass="input-text" size="10" tabindex="2"/></td>
	</tr>
	--><tr><td align="right" class="listwhitetext">Crew&nbsp;Name</td>
		<td style="width:100px"><s:textfield name="crewNm" required="true" cssClass="input-text" size="10" tabindex="3" cssStyle="width:247px;"/></td>
	</tr>
	<tr><td align="right" class="listwhitetext">Ticket#</td>
	<td style="width:100px"><s:textfield id="tkt" name="tkt" required="true" cssClass="input-text" size="10" onblur="return isInteger(this);" tabindex="4" cssStyle="width:247px;"/></td>
	</tr>
	<!--<tr><td align="right" class="listwhitetext">Shipper</td>
	<td style="width:100px"><s:textfield name="shipper" required="true" cssClass="input-text" size="10" tabindex="5"/></td>
	</tr>
	--><tr><td align="left" style="padding-left:114px;" colspan="2">
		<s:submit cssClass="cssbutton" method="search" cssStyle="width:55px; height:25px" key="button.search" onclick="return checkCondition();" tabindex="6"/>
		<s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Clear" /></td>
			
	</tr>
	<tr><td height="13px"></td></tr>
		</thead>
	</table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>

<script type="text/javascript">
	setCalendarFunctionality();
</script>
