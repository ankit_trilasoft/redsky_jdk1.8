package com.trilasoft.app.webapp.action;

import java.util.Date;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.ResourceContractCharges;
import com.trilasoft.app.service.ResourceContractChargeManager;

public class ResourceContractChargeAction extends BaseAction implements Preparable{
	
	private Long id;
	private String sessionCorpID;
	private ResourceContractCharges resourceContractCharges;
	private ResourceContractChargeManager resourceContractChargeManager;
	private Long parentId;
	
	private String partnerPrivateURL;
	private String btnType;
	
	public void prepare() throws Exception { 
		
	}
	
	public ResourceContractChargeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
	}
	
	public String save() throws Exception {
			partnerPrivateURL = "?checkDup=NO";
			boolean isNew = (resourceContractCharges.getId() == null);  
				if (isNew) { 
					resourceContractCharges.setCreatedOn(new Date());
		        } 
				resourceContractCharges.setCorpID(sessionCorpID);
				resourceContractCharges.setUpdatedOn(new Date());
				resourceContractCharges.setUpdatedBy(getRequest().getRemoteUser()); 
				resourceContractChargeManager.save(resourceContractCharges);  
			    String key = (isNew) ?"Contract details have been saved." :"Contract details have been saved." ;
			    saveMessage(getText(key)); 
				return SUCCESS;
	}
	
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public ResourceContractCharges getResourceContractCharges() {
		return resourceContractCharges;
	}

	public void setResourceContractCharges(
			ResourceContractCharges resourceContractCharges) {
		this.resourceContractCharges = resourceContractCharges;
	}

	public ResourceContractChargeManager getResourceContractChargeManager() {
		return resourceContractChargeManager;
	}

	public void setResourceContractChargeManager(
			ResourceContractChargeManager resourceContractChargeManager) {
		this.resourceContractChargeManager = resourceContractChargeManager;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getPartnerPrivateURL() {
		return partnerPrivateURL;
	}

	public void setPartnerPrivateURL(String partnerPrivateURL) {
		this.partnerPrivateURL = partnerPrivateURL;
	}

	public String getBtnType() {
		return btnType;
	}

	public void setBtnType(String btnType) {
		this.btnType = btnType;
	}
	
	
}
