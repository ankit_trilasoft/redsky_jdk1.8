
// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `invoicereportparameter` VARCHAR(255) DEFAULT '' AFTER     `authorizedCreditNote`;

ALTER TABLE `redsky`.`qualitysurveysettings` MODIFY COLUMN `accountId` VARCHAR(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
 ADD INDEX `Index_Corpid`(`corpId`),
 ADD INDEX `Index_AccountId`(`accountId`);
 
 ALTER TABLE `redsky`.`crew` ADD COLUMN `contractor` bit(1) DEFAULT b'0' ;
 
 ALTER TABLE `redsky`.`customerfile` CHANGE COLUMN `partnerEntitle` `partnerEntitle` VARCHAR(1000) NULL DEFAULT NULL  ;