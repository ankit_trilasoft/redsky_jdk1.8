<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>Pricing Quote List</title> 
<meta name="heading" content="Pricing Quote List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-17px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>
</head>
<c:set var="buttons"> 
<c:set var="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" />
<s:hidden name="serviceOrderId" value="<%=request.getParameter("serviceOrderId") %>" /> 
<c:if test="${!param.popup}">
     <input type="button" class="cssbutton1" style="width:115px; height:25px"  
        onclick="location.href='<c:url value="/agentPricingBasicInfo.html"/>'"  
        value="New Pricing Quote"/> 
         <c:if test="${hitFlag!=2}">  
         <input type="button" class="cssbutton1" style="width:115px; height:25px"  
        onclick="location.href='<c:url value="/agentPricingAcceptedQuotes.html"/>'"  
        value="Accept Quotes"/> 
        </c:if>
       <c:if test="${hitFlag==2}">  
         <input type="button" id="submittedQuotes" class="cssbutton1" style="width:115px; height:25px"  
         onclick="return submitSelectedQuotes();" 
        value="Save Quotes"/> 
      </c:if>
</c:if>	
<c:if test="${param.popup}">
     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/agentPricingBasicInfo.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"/>'"  
        value="New Rate Quote"/>"/>   
</c:if>
</c:set>

	<s:form id="agentPricingList" name="agentPricingList" action='${empty param.popup?"agentPricingSearch.html":"agentPricingSearch.html?decorator=popup&popup=true"}' method="post" validate="true">   
	<s:hidden name="userCheck"/> 
	
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">		
	<table class="table" style="width:100%;">
	<thead>
	<tr>
	<th>Pricing ID#</th>
	<th>Customer Name</th>
	<th>Quote #</th>
	<th>Packing</th>
	<th>&nbsp;</th>	
	</tr>
	</thead>
	
	<tbody>
		<tr>
		    <td width="" align="left"><s:textfield name="pricingControl.pricingID" required="true" cssClass="input-text" size="20"/></td>
		    <td width="" align="left"><s:textfield name="pricingControl.customerName" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="pricingControl.sequenceNumber" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:select cssClass="list-menu" name="pricingControl.packing" list="%{pkmode}" headerKey="" headerValue="" cssStyle="width:165px;" /></td>
			<td style="text-align:right;">
			<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
			</td>
		</tr>
			 <tr>
			 <td colspan="5"></td>		
   			</td>
		</tr>		
		</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	<c:if test="${!param.popup && hitFlag!=2}">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Price Engine List</span></a></li>
		  </ul>
		</div>
	</c:if>	
	
		
	<c:if test="${!param.popup && hitFlag==2}">	
		<div id="newmnav">
		  <ul>
		  	
		  	<li><a href="agentPricingList.html"><span>Price Engine List</span></a></li>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Accept Quote List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div>
	</c:if>	
		<div class="spnblk">&nbsp;</div>
	<c:if test="${!param.popup && hitFlag!=2}">
	<display:table name="pricingControlList" class="table" requestURI="" id="pricingControlListId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="pricingID"  href="agentPricingBasicInfo.html" paramId="id" paramProperty="id" sortable="true" title="Pricing ID#"/>
   	<display:column property="customerName"  sortable="true" title="Customer Name"/>
   	<display:column property="sequenceNumber"  sortable="true" title="Quote #"/>
   	<display:column property="packing"  sortable="true" title="Packing"/>
   	<display:column   sortable="true" title="Origin">
   	<c:if test="${pricingControlListId.originCity!='' && pricingControlListId.originCountry!=''}">
   	<c:out value="${pricingControlListId.originCity}" />,<c:out value="${pricingControlListId.originCountry}" />
   	</c:if>
   	<c:if test="${pricingControlListId.originCity=='' || pricingControlListId.originCountry==''}">
   	<c:out value="${pricingControlListId.originCountry}" />
   	</c:if>
   	</display:column>
   	<display:column   sortable="true" title="Destination">
   	<c:if test="${pricingControlListId.destinationCity!='' && pricingControlListId.destinationCountry!=''}">
     	<c:out value="${pricingControlListId.destinationCity}" />,<c:out value="${pricingControlListId.destinationCountry}" />
   	</c:if>
   	<c:if test="${pricingControlListId.destinationCity=='' || pricingControlListId.destinationCountry==''}">
   	<c:out value="${pricingControlListId.destinationCountry}" />
   	</c:if> 
   	</display:column>
   	 <display:column title="Remove" style="width: 15px;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${pricingControlListId.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>	
   	<display:setProperty name="export.excel.filename" value="Price Engine List.xls"/>
	<display:setProperty name="export.csv.filename" value="Price Engine List.csv"/>
   	</display:table>
	</c:if>
	
	<c:if test="${!param.popup && hitFlag==2}">
	<display:table name="pricingControlList" class="table" requestURI="" id="pricingControlListId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="pricingID"  href="agentPricingBasicInfo.html" paramId="id" paramProperty="id" sortable="true" title="Pricing ID#"/>
   	<display:column property="customerName"  sortable="true" title="Customer Name"/>
   	<display:column property="sequenceNumber"  sortable="true" title="Quote #"/>
   	<display:column property="packing"  sortable="true" title="Packing"/>
   	<display:column   sortable="true" title="Origin">
   	<c:if test="${pricingControlListId.originCity!='' && pricingControlListId.originCountry!=''}">
   	<c:out value="${pricingControlListId.originCity}" />,<c:out value="${pricingControlListId.originCountry}" />
   	</c:if>
   	<c:if test="${pricingControlListId.originCity=='' || pricingControlListId.originCountry==''}">
   	<c:out value="${pricingControlListId.originCountry}" />
   	</c:if>
   	</display:column>
   	<display:column   sortable="true" title="Destination">
   	<c:if test="${pricingControlListId.destinationCity!='' && pricingControlListId.destinationCountry!=''}">
     	<c:out value="${pricingControlListId.destinationCity}" />,<c:out value="${pricingControlListId.destinationCountry}" />
   	</c:if>
   	<c:if test="${pricingControlListId.destinationCity=='' || pricingControlListId.destinationCountry==''}">
   	<c:out value="${pricingControlListId.destinationCountry}" />
   	</c:if> 
   	</display:column>
   	<display:column title="Select"><input type="checkbox" style="margin-left:10px;" id="${pricingControlListId.id}" name="DD" value="${pricingControlListId.id}" onclick="userStatusCheck(${pricingControlListId.id},this)"/></display:column>	
   	<display:setProperty name="export.excel.filename" value="Price Engine List.xls"/>
	<display:setProperty name="export.csv.filename" value="Price Engine List.csv"/>
   	</display:table>
	</c:if>
	
	
	
	<c:if test="${param.popup}">
	<display:table name="pricingControlList" class="table" requestURI="" id="pricingControlListId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="pricingID"  href="agentPricing.html?decorator=popup&popup=true&serviceOrderId=${serviceOrderId}" paramId="id" paramProperty="id" sortable="true" title="Pricing ID#"/>
   	<display:column property="sequenceNumber"  sortable="true" title="Quote #"/>
   	<display:column property="packing"  sortable="true" title="Packing"/>
   	<display:column property="originCountry"  sortable="true" title="Origin Country"/>
   	<display:column property="destinationCountry"  sortable="true" title="Destination Country"/>
   	 <display:column title="Remove" style="width: 15px;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${pricingControlListId.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>
	<display:setProperty name="export.excel.filename" value="Price Engine List.xls"/>
	<display:setProperty name="export.csv.filename" value="Price Engine List.csv"/>
   	</display:table>
	</c:if>
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 
<script language="javascript" type="text/javascript">
function clear_fields(){
	var i;
			for(i=0;i<=3;i++)
			{
					document.forms['agentPricingList'].elements[i].value = "";
			}
	document.forms['agentPricingList'].elements['pricingControl.packing'].value = "";
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to delete this row?");
	if (agree){
		if('${!param.popup}')
		{
			location.href = "deleteAgentPricingList.html?id="+encodeURI(targetElement);
		}
	    if('${param.popup}')
		{
			location.href = "deleteAgentPricingList.html?id="+encodeURI(targetElement)+"&serviceOrderId=${serviceOrderId}&decorator=popup&popup=true";
		}
     }else{
		return false;
	}
}

function userStatusCheck(rowId, targetElement) // 1 visible, 0 hidden
   { 
    rowId=targetElement.id;
    if(targetElement.checked)
     {
      var userCheckStatus = document.forms['agentPricingList'].elements['userCheck'].value;
      if(userCheckStatus == '')
      {
	  document.forms['agentPricingList'].elements['userCheck'].value = rowId;
	  document.forms['agentPricingList'].elements['submittedQuotes'].disabled = false;
      }
      else
      {
       var userCheckStatus=	document.forms['agentPricingList'].elements['userCheck'].value = userCheckStatus + ',' + rowId;
      document.forms['agentPricingList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      document.forms['agentPricingList'].elements['submittedQuotes'].disabled = false;
      }
      if(userCheckStatus == ','){
      	document.forms['agentPricingList'].elements['userCheck'].value = '';
      }
      if(document.forms['agentPricingList'].elements['userCheck'].value == ''){
		document.forms['agentPricingList'].elements['submittedQuotes'].disabled = true;
		}
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['agentPricingList'].elements['userCheck'].value;
     var userCheckStatus=document.forms['agentPricingList'].elements['userCheck'].value = userCheckStatus.replace( rowId , '' );
     document.forms['agentPricingList'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
     document.forms['agentPricingList'].elements['submittedQuotes'].disabled = false;
     }
     if(userCheckStatus == ','){
      	document.forms['agentPricingList'].elements['userCheck'].value = '';
      }
      if(document.forms['agentPricingList'].elements['userCheck'].value == ''){
		document.forms['agentPricingList'].elements['submittedQuotes'].disabled = true;
		}
    }
    
    function submitSelectedQuotes()
    {
		var url = "pricingInformation.html";
		document.forms['agentPricingList'].action = url;
		document.forms['agentPricingList'].submit();
    }
</script>
<script type="text/javascript"> 
try{
<c:if test="${hitFlag == 1}" >
	if('${!param.popup}'){
		<c:redirect url="/agentPricingList.html"  />
	}
	if('${param.popup}'){
		<c:redirect url="agentPricingList.html?serviceOrderId=${serviceOrderId}&decorator=popup&popup=true"  />
	}
</c:if>
document.forms['agentPricingList'].elements['submittedQuotes'].disabled = true;
}
catch(e){}
</script> 