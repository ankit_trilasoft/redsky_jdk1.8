<%@ include file="/common/taglibs.jsp"%>

<head>
    <title><fmt:message key="userList.title"/></title>
    <meta name="heading" content="<fmt:message key='userList.heading'/>"/>
    
  <script language="javascript" type="text/javascript">
  function clear_fields(){
	var i;
		for(i=0;i<=5;i++){
			document.forms['searchForm'].elements[i].value = "";
		}
	}
	
	function btntype(targerelement){
		document.forms['searchForm'].elements['buttonType'].value=targerelement.value;
	}
	
	function userStatusCheck(targetElement){
	
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-18px;
!margin-top:-12px;
padding:0px 0px;
text-align:right;
width:100%;

}
</style>
</head>

<c:set var="buttons">
    <input type="button" class="cssbuttonA"
        onclick="location.href='<c:url value="/partnerUserUnassignedList.html?method=Add&from=list"/>'"
        value="<fmt:message key="button.add"/>"/>
   
</c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="userListBySearch" key="button.search"/> 
   
</c:set>
<s:form id="searchForm" action="assignDataSet" method="post" validate="true">
<c:set var="id" value="<%= request.getParameter("id")%>" />
<s:hidden name="id" value="<%= request.getParameter("id")%>" />
<c:set var="partnerId" value="<%= request.getParameter("id")%>" />
<s:hidden name="partnerId" value="<%= request.getParameter("id")%>" />		
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="userCheck"/>
<s:hidden name="buttonType"/>	
<!-- <c:out value="${buttons}" escapeXml="false" /> -->

<div id="newmnav">
		  <ul>
		  	<c:if test="${partnerType == 'AG'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Detail</span></a></li>
	  		</c:if>
	  		<c:if test="${partnerType == 'AC'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Account Detail</span></a></li>
	  		</c:if>
		  	<c:if test="${partnerType == 'VN'}">
	  		<li><a href="editPartnerPublic.html?partnerType=${partnerType}&id=${partnerPublic.id}"><span>Vendor Detail</span></a></li>
	  		</c:if>
	  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
	  		<c:if test="${not empty partnerPublic.id}">
					<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
						<c:if test="${partnerType == 'AC'}">
							<li><a href="editNewAccountProfile.html?id=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'AG'}">
							<li><a href="findPartnerProfileList.html?code=${partnerPublic.partnerCode}&partnerType=${partnerType}&id=${partnerPublic.id}"><span>Agent Profile</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'CR' || partnerType == 'OO'}">
							<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						</c:if>
						<c:if test="${partnerType == 'OO'}">
							<li><a href="standardDeductionsList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Standard Deductions</span></a></li>
						</c:if>
					</configByCorp:fieldVisibility>
				</c:if>
			</sec-auth:authComponent>
			<c:if test="${sessionCorpID!='TSFT' }">
		  	<li><a href="editPartnerPrivate.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Additional Info</span></a></li>
		  	</c:if>
		  	<c:if test="${partnerType == 'AG'}">
				<c:if test="${not empty partnerPublic.id}">
					<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
					<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
					<li><a href="baseList.html?id=${partnerPublic.id}"><span>Base</span></a></li>
					<c:if test="${paramValue == 'View'}">
						<li><a href="partnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test="${paramValue != 'View'}">
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					</c:if>	
					<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
						<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
					</c:if>
					<c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
						<li><a href="partnerRateGrids.html?partnerId=${partnerPublic.id}"><span>Rate Matrix</span></a></li>
					</c:if>
				  	--><li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			   </c:if>
	  	    </c:if>
		  	<c:if test="${partnerType == 'AC'}">
					<c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
						<configByCorp:fieldVisibility componentId="component.standard.accountContactTab"><li><a href="accountContactList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Account Contact</span></a></li></configByCorp:fieldVisibility>
						<c:if test="${checkTransfereeInfopackage==true}">
							<li><a href="editContractPolicy.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Policy</span></a></li>
						</c:if>
					</c:if>
			</c:if>
		 	<c:if test="${partnerType == 'AG'}">
		 	<li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
		 	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=50, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
		 	<!--<li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Agent Users</span></a></li>
		  	--></c:if>
	  		<c:if test="${partnerType == 'AC'}">
	  		<li><a href="partnerUsersList.html?id=${id}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
	  		<sec-auth:authComponent componentId="module.tab.partner.accountDetailTab">
	  		<c:if test="${not empty partnerPublic.id}">
			  	<c:if test="${paramValue == 'View'}">
						<li><a href="partnerView.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
			</c:if>
			</sec-auth:authComponent>
			<c:if test="${partnerType == 'VN' || partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
						<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
				 </c:if>
			</c:if>
			<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
			</c:url>
			<c:if test="${partnerType == 'AC'}">
				<c:if test="${not empty partnerPublic.id}">
				 	<li><a href="${url}"><span>FAQ</span></a></li>
				</c:if>
			</c:if>
			<c:if test="${partnerType == 'AG'}">
		 	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=50, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
		    </c:if>
		    <c:if test="${partnerType == 'AC'}">
		 	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=50, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
		    </c:if>
	  		<li><a href="partnerUserUnassignedList.html?id=${id}&partnerType=${partnerType}"><span>Unassigned Portal Users</span></a></li>
	  		</c:if>
		  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Requested Partner Access<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
<display:table name="partnerUsersList" requestURI="" defaultsort="2" id="users" class="table" pagesize="10" style="width:100%;!margin-top:3px;">
    <display:column title=" "><input type="checkbox" style="margin-left:10px;" id="checkboxId" name="DD" value="${users.id}" onclick="userStatusCheck(this)"/></display:column>
	<display:column  sortable="true" titleKey="user.username" style="width: 25%">
	<a href="addUsersFromPartner.html?id=${id}&userId=${users.id}&partnerType=${partnerType}"><c:out value="${users.username}"></c:out></a>
	</display:column>
    <display:column property="first_name"  sortable="true" title="First Name" style="width: 25%"/>
    <display:column property="last_name"  sortable="true" title="Last Name" style="width: 25%"/>
    <display:column  sortable="true" title="Portal Access" style="width: 25%;">
    <c:if test="${users.account_enabled==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${users.account_enabled==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
    <display:setProperty name="export.excel.filename" value="User List.xls"/>
    <display:setProperty name="export.csv.filename" value="User List.csv"/>
    <display:setProperty name="export.pdf.filename" value="User List.pdf"/>
</display:table>

<s:submit cssClass="cssbutton" value="Assign as User" onclick="btntype(this)" cssStyle="width:100px; height:25px "/>
<s:submit cssClass="cssbutton" value="Assign as Contact" onclick="btntype(this)" cssStyle="width:120px; height:25px "/>
</s:form>

<script type="text/javascript">
	
</script>
