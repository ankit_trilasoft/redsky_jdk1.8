/**
 * @Class Name  Vehicle Manager Impl
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.VehicleDao;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.service.VehicleManager;

public class VehicleManagerImpl extends GenericManagerImpl<Vehicle, Long> implements VehicleManager { 
	VehicleDao vehicleDao; 
    public VehicleManagerImpl(VehicleDao vehicleDao) { 
        super(vehicleDao); 
        this.vehicleDao = vehicleDao; 
    } 
    public List<Vehicle> findByLastName(String lastName) { 
        return vehicleDao.findByLastName(lastName); 
    } 
    public List checkById(Long id) { 
        return vehicleDao.checkById(id); 
    }
    public List findMaxId() {
 		return vehicleDao.findMaxId();
	} 
    public List getContainerNumber(String shipNumber) {
		return vehicleDao.getContainerNumber(shipNumber);
	}
    public List findMaximumIdNumber(String shipNum){
    	return vehicleDao.findMaximumIdNumber(shipNum);
    }
    public int updateDeleteStatus(Long ids){
    	return vehicleDao.updateDeleteStatus(ids);
    }
    
    
	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return vehicleDao.goNextSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return vehicleDao.goPrevSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return vehicleDao.goSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List findCountChild(String shipNm) {
		return vehicleDao.findCountChild(shipNm);
	}

	public List findMaximumChild(String shipNm) {
		return vehicleDao.findMaximumChild(shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return vehicleDao.findMinimumChild(shipNm);
	}
	public List vehicleList(Long serviceOrderId) {
		return vehicleDao.vehicleList(serviceOrderId);
	}
	public Long findRemoteVehicle(String idNumber, Long serviceOrderId)  {
		return vehicleDao.findRemoteVehicle(idNumber, serviceOrderId);
	}
	public void deleteNetworkVehicle(Long serviceOrderId, String idNumber, String vehicleStatus) {
		vehicleDao.deleteNetworkVehicle(serviceOrderId, idNumber,  vehicleStatus);	
	}
	public List findVehicleBySid(Long sofid, String sessionCorpID){
		return vehicleDao.findVehicleBySid(sofid, sessionCorpID);
	}
	public List vehicleListOtherCorpId(Long sid){
		return vehicleDao.vehicleListOtherCorpId(sid);
	}
	public void updateStatus(Long id, String vehicleStatus,String updatedBy) {
		vehicleDao.updateStatus(id, vehicleStatus, updatedBy);
		
	}
	
	/*
     * Bug 15084 - Changes on the EDI Report for ONLY customers using EDI Reporting
     */
    public List getWeight(String shipNumber){
    	return vehicleDao.getWeight(shipNumber);
    }
	public List getVolume(String shipNumber){
		return vehicleDao.getVolume(shipNumber);
	}
	public List getVolumeUnit(String shipNumber){
		return vehicleDao.getVolumeUnit(shipNumber);
	}
	public List getWeightUnit(String shipNumber){
		return vehicleDao.getWeightUnit(shipNumber);
	}
	public int updateMisc(String shipNumber,String weight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
		return vehicleDao.updateMisc(shipNumber, weight, weightUnit, volumeUnit, updatedBy, sessionCorpID);
	}
	public int updateMiscVolume(String shipNumber,String volume,String weightUnit,String corpID,String updatedBy){
		return vehicleDao.updateMiscVolume(shipNumber, volume,weightUnit, corpID, updatedBy);
	}
	/*
     * End
     */
}

//End of Class.
