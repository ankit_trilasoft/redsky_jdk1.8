<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  
<head>   
    <title>Unpost Payable Invoice List</title>   
    <meta name="heading" content="Unpost Payable Invoice List"/> 
    <style type="text/css"> 

.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
.listwhitetextWhite {
    background-color: #FFFFFF;
    color: #003366;
    font-family: arial,verdana;
    font-size: 10.5px;
   }
</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style> 
	
<script type="text/javascript">

	function unpostPayableData(){ 
       var aidVal = document.forms['unpostingListForm'].elements['stringAid'].value;
	    if (aidVal == null || aidVal==''){
	          alert("Please check any check box to continue."); 
	       	  return false;
	       }else{
	    	   var agree = confirm("Press OK to proceed, or press Cancel.");
	            if(agree){
	               document.forms['unpostingListForm'].action ='updateUnPostPayable.html?decorator=popup&popup=true';
	               document.forms['unpostingListForm'].submit();
	               pick();
	             }else {
	             	return false; 
	             } 
           }  
      }
   
   
    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
   
   function setValues(){
	   var aidList='';
		<c:forEach items="${invoicesForUnpostingList}" var="list1">
		var aid="${list1.aid}";
			if(document.getElementById(aid).checked){
				if(aidList!=''){
					aidList = aidList+','+document.getElementById(aid).value;
				}else{
					aidList = document.getElementById(aid).value;
				} 	
			}
		</c:forEach>
		 document.forms['unpostingListForm'].elements['stringAid'].value = aidList;
  }
   
  </script>  
</head>
<s:form id="unpostingListForm" method="post"> 
<s:hidden name="stringAid" />
<s:hidden name="shipNumber" value="<%=request.getParameter("shipNumber") %>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yyyy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/> 

<c:if test="${invoicesForUnpostingList=='[]'}">
<table class="table" id="dataTable" style="width:550px;margin-top:0px;">
	<thead>					 
			<tr>
					<th width="">Vendor Invoice#</th>
					 <th width="">Vendor Name</th>
					 <th width="">Expense Amount</th>
					 <th width="">Posted Date</th>
			</tr>
	</thead>
			<tbody>
			<td  class="listwhitetextWhite" colspan="3">Nothing Found To Display.</td>
			</tbody>
</table>
</c:if>
 
<c:if test="${invoicesForUnpostingList!='[]'}"> 
<s:set name="invoicesForUnpostingList" value="invoicesForUnpostingList" scope="request"/>
<display:table name="invoicesForUnpostingList" class="table" requestURI="" id="invoicesForUnpostingList" style="width:500px" defaultsort="1" pagesize="100" >   
		
		 <display:column property="invoiceNumber" sortable="true" title="Vendor Invoice#" style="width:100px" />		 
		 <display:column property="estimateVendorName" sortable="true" title="Vendor Name" style="width:180px"/> 
		 <display:column property="actualExpense" sortable="true" title="Expense Amount" style="width:80px"/>
		 <display:column property="payPostDate" sortable="true" title="Posted Date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
		 <s:hidden name="${invoicesForUnpostingList.aid}" value="${invoicesForUnpostingList.aid}" /> 
	    <display:column   title="" style="width:10px" >
 			<input style="vertical-align:bottom;" type="checkbox" id=${invoicesForUnpostingList.aid} value=${invoicesForUnpostingList.aid} onclick="setValues()"/>
 		</display:column>
</display:table>
<table> 
<tr>
<td>
<input type="button" name="Submit" onclick="return unpostPayableData();" value="Unpost Selected Invoice" class="cssbuttonA" style="width:170px; height:25px" >
</td>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td>
</tr>
</table>
</c:if>
</s:form>
 		  	