<%@ include file="/common/taglibs.jsp"%> 

<head>

	<meta name="heading" content="<fmt:message key='vanLineForm.title'/>"/> 
	<title><fmt:message key="vanLineForm.title"/></title>
	
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>

<!-- Modification closed here -->
<script>
function newWindow(mypage,myname,w,h,features) {
  var winl = (screen.width-w)/2;
  var wint = (screen.height-h)/2;
  if (winl < 0) winl = 0;
  if (wint < 0) wint = 0;
  var settings = 'height=' + h + ',';
  settings += 'width=' + w + ',';
  settings += 'top=' + wint + ',';
  settings += 'left=' + winl + ',';
  settings += features;
  win = window.open(mypage,myname,settings);
  win.window.focus();
}
function vanLineExtract(){
	var agent = document.forms['vanLineSettForm'].elements['vanLine.agent'].value;
	if(agent== ''){
		alert('Please enter the Agency #.');
		document.forms['vanLineSettForm'].elements['vanLine.agent'].focus();
		return false
	}
	
	var weekEnding = document.forms['vanLineSettForm'].elements['vanLine.weekEnding'].value;
	if(weekEnding== ''){
		alert('Please enter the Week Ending date.');
		document.forms['vanLineSettForm'].elements['vanLine.weekEnding'].focus();
		return false
	}
	location.href='vanLineExtractMgt.html?agent='+agent+'&weekEnding='+weekEnding;	
}
</script> 	
<style type="text/css">


/* collapse */

</style>

</head>
<body style="background-color:#444444;">
<s:form id="vanLineSettForm" name="vanLineSettForm" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:68%">
<div id="otabs">
			  <ul>
			    <li><a href="vanLineFormSett.html"  ><span>Van Line Settlement</span></a></li>
			  </ul>
			    <ul>
			    <li><a class="current"><span>Van Line Account Line Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">

  	
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="5px"></td>
		  	</tr>

		  	<tr>
		  		<td align="right" class="listwhitebox"><fmt:message key="vanLine.agencya"/></td>
		  		<td align="left"><s:select cssClass="list-menu" name="vanLine.agent" headerKey="" headerValue="" list="%{vanLineCodeList}" cssStyle="width:110px" onchange="changeStatus();" tabindex="1"/></td>
		  		</tr>
		  	
		  	<tr>
		  		<td align="left" height="10px">
		  	</tr>
		  	
		  	<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
			<s:hidden name="hitFlag" />
			<c:if test="${hitFlag == 1}" >
			  	<tr>
			  		<td colspan="4">
						<table class="detailTabLabel" cellpadding="0" cellspacing="2">
							<tr>
								<td align="right" class="listwhitetext"><b>The following are the list of unapproved statement list by selected agency.</b></td> 
							</tr>
						</table> 
					</td>
				</tr>
				
				<tr>
					<td colspan="4">
						<table align="center">
							<tr>
								<td>
									<s:set name="weekendingDateList" value="weekendingDateList" scope="request"/> 
						        	<display:table name="weekendingDateList" class="table" requestURI="" id="weekendingDateList" style="width:400px" defaultsort="1"  defaultorder="descending"  pagesize="100" >   
						 				<display:column property="weekending" sortable="true" title="Week Ending Date" format="{0,date,dd-MMM-yyyy}" style="width:100px" />
										<display:column property="recordCount" headerClass="containeralign"  title="# of lines in Suspense/Dispute" maxLength="15"  style="width:100px;text-align: right;"/>
					     			</display:table> 
								</td>
							</tr>
						</table>
					</td>
				</tr> 
		  	</c:if>
		  	
		  	<tr>
		  		<td align="right" class="listwhitebox"><fmt:message key="vanLine.weekEnding"/></td>		  	   	
		  	   	<c:if test="${not empty vanLine.weekEnding}">
					<s:text id="batchDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="vanLine.weekEnding"/></s:text>
					<td><s:textfield id="batchDate" cssClass="input-text" name="vanLine.weekEnding" value="%{batchDateFormattedValue}" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty vanLine.weekEnding}">
					<td><s:textfield id="batchDate" cssClass="input-text" name="vanLine.weekEnding" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/><img id="batchDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>		  	   	
		  	</tr>
		  	<tr>
		  		<td align="left" height="10px">
		  	</tr>
		  	
		  	<tr>	
		  	  	<td align="right">	
       			<td align="left" >        		
	       			<input type="button" class="cssbutton1" style="width:180px;" name="DisplaywithReconcile" value="Vanline Settlement Extract" onClick="vanLineExtract();" /> 
				</td>  	
        	</tr>
        	
        	
        	<tr>
		  		<td align="left" height="18px"></td>
		  	</tr>
	
	</table> 
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 	
</div>
</s:form>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>

		