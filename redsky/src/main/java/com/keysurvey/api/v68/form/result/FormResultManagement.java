/**
 * FormResultManagement.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public interface FormResultManagement extends javax.xml.rpc.Service {
    public java.lang.String getFormResultManagementServicePortAddress();

    public com.keysurvey.api.v68.form.result.FormResultManagementService getFormResultManagementServicePort() throws javax.xml.rpc.ServiceException;

    public com.keysurvey.api.v68.form.result.FormResultManagementService getFormResultManagementServicePort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
