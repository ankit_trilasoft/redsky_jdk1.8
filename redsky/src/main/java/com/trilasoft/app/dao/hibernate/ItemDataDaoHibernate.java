package com.trilasoft.app.dao.hibernate;

import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ItemDataDao;
import com.trilasoft.app.model.ItemData;

public class ItemDataDaoHibernate extends GenericDaoHibernate<ItemData, Long> implements ItemDataDao{
	
public ItemDataDaoHibernate(){
	super(ItemData.class);
}
public String weightAndVolumeFromRfMaster(String itemDescription){
	String query ="select flex1 ,flex2,flex3 from refmaster where parameter='ITEMS' And code='" + itemDescription + "' ";
	 List al=getSession().createSQLQuery(query).list();
	 String valueOfWeightAndVolume ="";
	 if(al!=null && !al.isEmpty() && al.get(0)!=null){
		 Iterator itr=al.iterator();
		 while(itr.hasNext()){
			 Object obj[]=(Object[])itr.next();
			 if(obj[0] != null && obj[1] != null)
			 {
				 valueOfWeightAndVolume =obj[0]+"~"+obj[1]+"~"+obj[2];
			 }
		 
	 }
}
	 return valueOfWeightAndVolume;
}

public void deleteFromItemData(Long id , int workTicketId){
	getHibernateTemplate().bulkUpdate("delete from ItemData where id='"+ id +"' And  workTicketId='"+ workTicketId +"' ");
}

public void updateItemDataWeightAndVolume(String totalWeight,String totalVolume, Integer workTicketId,String totalEstimatedPieces,String  totalEstimatedVolume,String totalEstimatedWeight){
	getHibernateTemplate().bulkUpdate("update ItemData set totalVolume='"+ totalVolume +"' ,totalWeight='"+ totalWeight +"' where workTicketId='"+ workTicketId +"' ");
	
	if(!totalEstimatedPieces.equalsIgnoreCase("0")){
	getHibernateTemplate().bulkUpdate("update WorkTicket set estimatedPieces='"+ totalEstimatedPieces +"' , estimatedCubicFeet='"+ totalEstimatedVolume +"' ,estimatedWeight='"+ totalEstimatedWeight +"' where id='"+ workTicketId +"' ");

	}}

public List getAllHandOutList(String shipNumber){
	return getHibernateTemplate().find("from ItemData where shipNumber='" + shipNumber + "' order by (itemDesc * 1) , itemDesc ");
}

public List getAllHandOutList(String shipNumber, String itemDescription){
	return getHibernateTemplate().find("from ItemData where shipNumber='"+shipNumber+"'  and itemDesc like '"+itemDescription+"%' and  (hoTicket is  null or hoTicket ='') order by (itemDesc * 1) , itemDesc");
}

public void updateHandoutList(String shipNumber, long handoutItemId ,Integer ticket){
	getSession().createSQLQuery("update itemdata set hoTicket='"+ ticket +"' where shipNumber='"+shipNumber+"'  and id = '"+handoutItemId+"' ").executeUpdate();
}

public void updateAllHoTicketInHandOutList(String shipNumber, Integer ticket,Boolean releaseAllItemsValue){
	if(releaseAllItemsValue){
	getSession().createSQLQuery("update itemdata set hoTicket='"+ ticket +"' where shipNumber='"+shipNumber+"' and (hoTicket is null || hoTicket='') and released is null ").executeUpdate();
	}else{
		getSession().createSQLQuery("update itemdata set hoTicket='' where shipNumber='"+shipNumber+"' and released is null ").executeUpdate();	
	}
}

public String updateHoTicketByHo(String shipNumber, Long id,Boolean hoValue,Integer ticket)
{
	String query="";
	String hoValueFromTic="";
	if(hoValue){
	getSession().createSQLQuery("update itemdata set hoTicket='"+ticket+"' where shipNumber='"+shipNumber+"' and id = '"+id+"' ").executeUpdate();
	}else{
		getSession().createSQLQuery("update itemdata set hoTicket='' where shipNumber='"+shipNumber+"' and id = '"+id+"' ").executeUpdate();	
	}
	query = "select concat(if(SUM(quantityReceived) is null ,0,SUM(quantityReceived)),'~',if(SUM(weightReceived) is null ,0.00,SUM(weightReceived)),'~',SUM(volumeReceived)) from itemdata where shipnumber='"+shipNumber+"' and hoticket='"+ticket+"'";
	List al = this.getSession().createSQLQuery(query).list();
	if ((al != null) && (!al.isEmpty()) && (al.get(0)!=null)) {
		hoValueFromTic = al.get(0).toString();
		getHibernateTemplate().bulkUpdate("update WorkTicket set actualWeight='"+hoValueFromTic.split("~")[1]+"' , actualVolume='"+hoValueFromTic.split("~")[2]+"' ,estimatedPieces='"+hoValueFromTic.split("~")[0]+"' where shipNumber='"+shipNumber +"' and ticket='"+ticket+"' ");
	}else{
		getHibernateTemplate().bulkUpdate("update WorkTicket set actualWeight='0.00' , actualVolume='0.00' ,estimatedPieces='0' where shipNumber='"+shipNumber +"' and ticket='"+ticket+"' ");
		hoValueFromTic="0~0.00~0.00";
	}
	return hoValueFromTic;
	
}

public Object getItemDataByLine(Long ticket,Integer line, String serviceType){
	 List <ItemData> list =null;
	 if(serviceType.equals("RC")){
	 list= getHibernateTemplate().find("from ItemData where ticket ='"+ticket+"' and line = '"+line+"' ");
	 }else{
		 list= getHibernateTemplate().find("from ItemData where hoTicket ='"+ticket+"' and itemUomid = '"+line+"' "); 
	 }
	 if(list.isEmpty()){
	  return null;
	 }else{
	  return list.get(0);
	 }
	}

public Integer getMaxLine(String shipNumber){
	String query = "Select max(line) from itemdata where shipNumber='"+shipNumber+"'";
	Integer line = 0;
	List al=getSession().createSQLQuery(query).list();
	if(al!=null && !al.isEmpty() && al.get(0)!=null){
		 line =Integer.parseInt(al.toString().replaceAll("[\\[\\](){}]",""));
	}
	return line;
}

public void saveNotes(Long id, Integer workTicketId, String valueOfNotes){
	getSession().createSQLQuery("update itemdata set notes='"+valueOfNotes.replace("'", "\\'")+"' where workTicketId='"+workTicketId+"' and id = '"+id+"' ").executeUpdate();
	
}
public List getAllHOItemList(Long hoTicket, String shipNumber){
	return getHibernateTemplate().find("from ItemData where hoTicket='"+hoTicket+"' and shipNumber='"+shipNumber+"' ");	
}

public void updatetotalActQty(Integer workTicketId, String totalActualQty){
	getHibernateTemplate().bulkUpdate("update ItemData set totalActQuantity='"+ totalActualQty +"' where workTicketId='"+ workTicketId +"' ");
	//getHibernateTemplate().bulkUpdate("update WorkTicket set actualWeight='"+ totalWeight +"' , actualVolume='"+ totalVolume +"' ,estimatedPieces='"+ totalActualQty +"' where shipNumber='"+shipNumber +"' and service='HO' ");
}
}