package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.AccountProfileManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;

public class AccountProfileAction extends BaseAction{
	
	private Long id;
	private AccountProfileManager accountProfileManager;
	private AccountProfile accountProfile;
	private PartnerManager partnerManager;
	private PartnerPublicManager partnerPublicManager;
	private RefMasterManager refMasterManager;
	private PartnerPublic partner;
	private String sessionCorpID;
	private String countAccountProfileNotes;
	private NotesManager notesManager;
	private static List stageunits;
	private  Map<String, String> sale = new LinkedHashMap<String, String>();
	private Long maxId;
	private String hitflag;
	private String partnerCode;
	private String partnerId;
	Date currentdate = new Date();
	private String usertype;
    private Company company;
	private CompanyManager companyManager;
	static final Logger logger = Logger.getLogger(AccountProfileAction.class);

	 

	public AccountProfileAction(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        usertype = user.getUserType();
        this.sessionCorpID = user.getCorpID();
	}
	
	public AccountProfile getAccountProfile() {
		return accountProfile;
	}
	public void setAccountProfile(AccountProfile accountProfile) {
		this.accountProfile = accountProfile;
	}
	
	public void setAccountProfileManager(AccountProfileManager accountProfileManager) {
		this.accountProfileManager = accountProfileManager;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	private Long id1;
	private List notess;
	private List owner;
	public String relatedNotesForProfile(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(id);
		accountProfile=accountProfileManager.get(id1);
		//System.out.println("\n\n\n\npartnerCode-->"+accountProfile.getPartnerCode());
		notess=accountProfileManager.getRelatedNotesForProfile(sessionCorpID,accountProfile.getPartnerCode());
		//System.out.println("\n\n\n\nnotess-->"+notess);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
	}
	
	/*public String edit(){
		if(id != null ){
			accountProfile = accountProfileManager.get(id);
		}else{
			accountProfile = new AccountProfile();
		}
		return SUCCESS;
	}*/
	private Boolean checkTransfereeInfopackage;
	public String editNewAccountProfile(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partner = partnerPublicManager.get(id);
		List<AccountProfile> accountProfileList = accountProfileManager.findAccountProfileByPC(partner.getPartnerCode(), sessionCorpID);
		if(accountProfileList == null || accountProfileList.isEmpty()){
			accountProfile = new AccountProfile();
			accountProfile.setPartnerCode(partner.getPartnerCode());
			accountProfile.setCorpID(sessionCorpID);
			accountProfile.setCreatedOn(new Date());
			accountProfile.setUpdatedOn(new Date());
		}else{
			accountProfile = (AccountProfile) accountProfileList.get(0);
		}
		getComboList(sessionCorpID, "a");
		getNotesForIconChange();
		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private static Map<String, String> ocountry;
	private static Map<String, String> lead;
	private static Map<String,String>currency;
	private static Map<String,String>industry;
	private static Map<String,String>revenue;
	public String getComboList(String corpId, String jobType) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ocountry = refMasterManager.findByParameter(corpId, "COUNTRY");
		currency=refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
		lead = refMasterManager.findByParameter(corpId, "LEAD");
		industry = refMasterManager.findByParameter(corpId, "INDUSTRY");
		revenue = refMasterManager.findByParameter(corpId, "REVENUE");
		sale = refMasterManager.findUser(corpId, "ROLE_SALE");
		owner= accountProfileManager.getAccountOwner(sessionCorpID);
		stageunits = new ArrayList();
		stageunits.add("Prospect");
		stageunits.add("Quote");
		stageunits.add("Panel");
		stageunits.add("Contract");
		stageunits.add("Major");
		//stageunits.add("VIP");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public Map<String, String> getLead() {
		return lead;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public PartnerPublic getPartner() {
		return partner;
	}
	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public Map<String, String> getSale() {
		return sale;
	}
	public  List getStageunits() {
		return stageunits;
	}
	public String save(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (accountProfile.getId() == null);
		if(isNew){
			accountProfile.setCreatedOn(new Date());
		}
		accountProfile.setUpdatedOn(new Date());
		accountProfile.setUpdatedBy(getRequest().getRemoteUser());
		accountProfile = accountProfileManager.save(accountProfile);
		
		partner = partnerPublicManager.getPartnerByCode(accountProfile.getPartnerCode());
		
		if(partner.getStatus() != null && accountProfile.getStage() != null){
			if(partner.getStatus().equalsIgnoreCase("New") && accountProfile.getStage().equalsIgnoreCase("Prospect")){
				partnerPublicManager.updateStatus(partner.getPartnerCode());
			}
		}
		
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Account Profile has been added successfully" : "Account Profile has been updated successfully";
		saveMessage(getText(key));
		}
		getComboList(sessionCorpID, "a");
		getNotesForIconChange();
		hitflag ="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");

		return SUCCESS;
     }

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getCountAccountProfileNotes() {
		return countAccountProfileNotes;
	}

	public void setCountAccountProfileNotes(String countAccountProfileNotes) {
		this.countAccountProfileNotes = countAccountProfileNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getNotesForIconChange() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List m = notesManager.countAccountProfileNotes(partner.getPartnerCode(), sessionCorpID);
		
		if (m.isEmpty()) {
			countAccountProfileNotes = "0";
		} else {
			countAccountProfileNotes = ((m).get(0)).toString();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public static Map<String, String> getCurrency() {
		return currency;
	}

	public static void setCurrency(Map<String, String> currency) {
		AccountProfileAction.currency = currency;
	}

	public static Map<String, String> getIndustry() {
		return industry;
	}

	public static void setIndustry(Map<String, String> industry) {
		AccountProfileAction.industry = industry;
	}

	public static Map<String, String> getRevenue() {
		return revenue;
	}

	public static void setRevenue(Map<String, String> revenue) {
		AccountProfileAction.revenue = revenue;
	}

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}
	
	private String gotoPageString;
	private String validateFormNav;
	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	public String getGotoPageString() {

	return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	this.gotoPageString = gotoPageString;

	}
	 
	public String saveOnTabChange() throws Exception {
	    //if (option enabled for this company in the system parameters table then call save) {
			validateFormNav = "OK";
	        String s = save();    // else simply navigate to the requested page)
	        return gotoPageString;
	}

	public Long getId1() {
		return id1;
	}

	public void setId1(Long id1) {
		this.id1 = id1;
	}

	public List getNotess() {
		return notess;
	}

	public void setNotess(List notess) {
		this.notess = notess;
	}

	public List getOwner() {
		return owner;
	}

	public void setOwner(List owner) {
		this.owner = owner;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}

	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

}
