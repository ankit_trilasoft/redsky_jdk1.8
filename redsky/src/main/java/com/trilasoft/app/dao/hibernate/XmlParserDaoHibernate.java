package com.trilasoft.app.dao.hibernate;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.trilasoft.app.RateData;
import com.trilasoft.app.RateObject;
import com.trilasoft.app.dao.XmlParserDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ExchangeRate;
import com.trilasoft.app.model.XmlParser;
import com.trilasoft.app.service.ExchangeRateManager;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.w3c.dom.*;
import org.xml.sax.*;

import javax.xml.parsers.*;

public class XmlParserDaoHibernate extends GenericDaoHibernate<XmlParser,Long> implements XmlParserDao{
	private ExchangeRateManager exchangeRateManager;
	private HibernateUtil hibernateUtil;
	public XmlParserDaoHibernate() {
		super(XmlParser.class);
	}
	
	public String saveData(String sessionCorpID, String file, String userName) {
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		    URL url = null;
		    InputStream in=null;
		    URLConnection con=null;
		    
		    /**
		     * changes based on dynamic data currency
		     */
		    
		    String baseCurr="";
			List l1 = this.getSession().createSQLQuery("select x.baseCurrency from systemdefault x where x.corpID='"+ sessionCorpID +"'").list();
			if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
				baseCurr = l1.get(0).toString();
			}
		    
		    String refCurrency="";
		    
		    List refcode = this.getSession().createSQLQuery("select code from refmaster x where x.corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID +"') and parameter='CURRENCY' and status='Active' ").list();
			if(refcode!=null && !refcode.isEmpty() && refcode.get(0)!=null ){
				refCurrency = refcode.toString().replace("[", "").replace("]", "").replaceAll(" ", "").trim();
			}
		    
		    String getUrl = "https://xecdapi.xe.com/v1/convert_from.json/?from="+baseCurr+"&to="+refCurrency+"";
	        HttpGet getRequest = new HttpGet(getUrl);
			 getRequest.setHeader("Authorization","Basic cmVkc2t5OTYyNDM1MDY4IDozaXNwbTBjNmJ2dmk4b3BnY2djNHViMHNnag==");
			 getRequest.addHeader("Accept", "application/json");
	        StringBuilder sb = new StringBuilder();
	        System.out.println("Executing HTTP Post...\n"+getRequest);
	        DefaultHttpClient httpClient = new DefaultHttpClient();
	        try {
				HttpResponse response = httpClient.execute(getRequest);
				
				// Check the HTTP status of the post.
	            if (response.getStatusLine().getStatusCode() != 200 && response.getStatusLine().getStatusCode() != 201) {
	                try {
						throw new RuntimeException("Failed: HTTP error code: "
						    + response.getStatusLine().getStatusCode());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            }
	            // Create a reader to read in the HTTP post results.
	            BufferedReader br = new BufferedReader(
	                            new InputStreamReader((response.getEntity().getContent())));
				String output;
				String output1 = new String();
				System.out.println("Output from Server .... \n");
				while ((output = br.readLine()) != null) {
					System.out.println(output);
					output1=output;
					 sb.append(output + "\n");
				}
				
				System.out.println("Out-put from server outer loop >>"+output1);
				
				ObjectMapper mapper = new ObjectMapper();
				
				RateObject rateObj = new RateObject();
				
				try {
					rateObj = mapper.readValue(output1.toString(), RateObject.class);
					} catch (JsonParseException e1) {
						e1.printStackTrace();
					} catch (JsonMappingException e1) {
						e1.printStackTrace();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				if (response.getStatusLine().getStatusCode() == 200 
						&& response.getStatusLine().getStatusCode() != 201) {
				copyDataInExchangeRate(sessionCorpID,rateObj.getFrom(),rateObj,"Autoupdate");
				}
	        }catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
			 }
		    
	        /*try {
	           url = new URL("http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml");
	            }
	        catch (MalformedURLException e) { } 
	        try {
				con = url.openConnection();
				con.connect();
		        in = con.getInputStream();
		        Document doc = parseFile(sessionCorpID,in,userName);
				Node root = doc.getDocumentElement();
				writeDocumentToOutput(sessionCorpID,root,userName);
				in.close();
			} 
	        catch (IOException e1) {
	        	e1.printStackTrace();
	        }*/ 
	    
		   return null;
	}	
	
	public void copyDataInExchangeRate(String corpID,String baseCurrency,RateObject rateObj,String userName){
		String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", corpID).get(0).toString();
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", corpID);
    	List<RateData> rateData = rateObj.getTo(); 
    	for(RateData rateData1 : rateData){
    	if(rateData1.getQuotecurrency()!=null && !(currencyList.contains(rateData1.getQuotecurrency()))){
    	ExchangeRate exchangeRate = new ExchangeRate();
        exchangeRate.setCorpID(corpID);
        exchangeRate.setCurrency(rateData1.getQuotecurrency());
        exchangeRate.setBaseCurrency(baseCurrency);
        BigDecimal officialrate = (rateData1.getMid());
        exchangeRate.setOfficialRate(officialrate);
        BigDecimal divisor = new BigDecimal(100);
        exchangeRate.setMarginApplied(new BigDecimal(margin));
        BigDecimal marginPercent = (new BigDecimal(margin)).divide(divisor, 4, RoundingMode.HALF_UP);
        BigDecimal sustractedValue = (new BigDecimal(1)).subtract(marginPercent);
        BigDecimal baseCurrencyRate = (new BigDecimal(officialrate.toString())).multiply(sustractedValue);
        exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
        BigDecimal cBR = (new BigDecimal("1.0")).divide(baseCurrencyRate, 4, RoundingMode.HALF_UP);
        exchangeRate.setCurrencyBaseRate(cBR);
        exchangeRate.setManualUpdate("N");
        exchangeRate.setValueDate(new Date());
        exchangeRate.setCreatedBy(userName);
        exchangeRate.setCreatedOn(new Date());
        exchangeRate.setUpdatedBy(userName);
        exchangeRate.setUpdatedOn(new Date());
        exchangeRateManager.save(exchangeRate);
    		}
    	}
    }
	
	 /** Parses XML file and returns XML document */
	 public Document parseFile(String sessionCorpID,InputStream in, String userName) {
	        DocumentBuilder docBuilder;
	        Document doc = null;
	        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	        docBuilderFactory.setIgnoringElementContentWhitespace(true);
	         try {
	             docBuilder = docBuilderFactory.newDocumentBuilder();
	             }
	         catch (ParserConfigurationException e) {
	            System.out.println("Wrong parser configuration: " + e.getMessage());
	            return null;
	            }	             
	         //File sourceFile = new File(fileName);
	         try {
	            doc = docBuilder.parse(in);
	            }
	         catch (SAXException e) {
	            System.out.println("Wrong XML file structure: " + e.getMessage());
	            return null;
	            }
	        catch (IOException e) {
	             System.out.println("Could not read source file: " + e.getMessage());
	        }
	         return doc;
	   }
	
     public final static String getElementValue( Node elem ) {
     Node kid;
     if( elem != null){
         if (elem.hasChildNodes()){
             for( kid = elem.getFirstChild(); kid != null; kid = kid.getNextSibling() ){
                 if( kid.getNodeType() == Node.TEXT_NODE  ){
                    return kid.getNodeValue();
	                 }
	         }
	         }
	      }
	       return "";
	   }
     
	/** find node and all child nodes */
	@SuppressWarnings("unchecked")
	public void writeDocumentToOutput(String sessionCorpID,Node node,String userName) {
		 Map<String, String> currencyRate= new HashMap<String, String>();
		 String currencyValue=null;
		 String rateValue=null;
		 List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		 // get attributes of element
	       NamedNodeMap attributes = node.getAttributes();
	         for (int i = 0; i < attributes.getLength(); i++) {
	            Node attribute = attributes.item(i);
	            if(attribute.getNodeName()=="currency"){
	            	currencyValue=attribute.getNodeValue();
	            	//System.out.println("AttributeName: " + attribute.getNodeName() + ", attributeValue: " + attribute.getNodeValue());
	            }
	            if(attribute.getNodeName()=="rate"){
	            	rateValue=attribute.getNodeValue();
	            	//System.out.println("AttributeName: " + attribute.getNodeName() + ", attributeValue: " + attribute.getNodeValue());
	            }
	         }         
	        // write all child nodes recursively
	         NodeList children = node.getChildNodes();
             for (int i = 0; i < children.getLength(); i++) {
	             Node child = children.item(i);
	            if (child.getNodeType() == Node.ELEMENT_NODE) {
	               writeDocumentToOutput(sessionCorpID,child,userName);
	            }
	         }
             currencyRate.put(currencyValue, rateValue);    
	         Iterator it = currencyRate.entrySet().iterator();
	         while (it.hasNext()) {
	             Map.Entry crMap = (Map.Entry)it.next();             
	             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
	             ExchangeRate exchangeRate= new ExchangeRate();
	             exchangeRate.setCorpID(sessionCorpID);
	             exchangeRate.setCurrency((String) crMap.getKey());
	             exchangeRate.setBaseCurrency("EUR");
	             BigDecimal officialrate=new BigDecimal((String) crMap.getValue());
	             exchangeRate.setOfficialRate(officialrate);
	             BigDecimal divisor=new BigDecimal(100);
	             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
	             exchangeRate.setMarginApplied(new BigDecimal(margin));
	             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
	             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
	             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
	             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
	             BigDecimal cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
	             exchangeRate.setCurrencyBaseRate(cBR);
	             exchangeRate.setManualUpdate("N");
	             exchangeRate.setValueDate(new Date());
	             exchangeRate.setCreatedBy(userName);
	             exchangeRate.setCreatedOn(new Date());
	             exchangeRate.setUpdatedBy(userName);  
	             exchangeRate.setUpdatedOn(new Date());
	             exchangeRateManager.save(exchangeRate); 
	             }
	      }	
	 }

	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}

	public String saveNokRate(String sessionCorpID, String file, String userName) {
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("https://www.dnbnor.no/seg-markets/portalfront/datafiles/miscellaneous/csv/kursliste_ws.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("valutakurs");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("kode");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("salg");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		             exchangeRate.setCurrency((String) crMap.getKey());
		             exchangeRate.setBaseCurrency("NOK");
		             BigDecimal officialrate= new BigDecimal(0);
		             if((String)crMap.getValue()!="0"){
		             officialrate=new BigDecimal((String) crMap.getValue());
		             }
		             exchangeRate.setOfficialRate(officialrate);
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(baseCurrencyRate);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(cBR);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
		             exchangeRateManager.save(exchangeRate); 
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    
	   return null;
	}
	@SuppressWarnings("rawtypes")
	/*public String saveUsdRate(String sessionCorpID, String file, String userName){
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           //url = new URL("http://pipes.yahoo.com/pipes/pipe.run?_id=_kHVIdZ13hGXgt1VwmH_9A&_render=rss");
        	url = new URL("http://themoneyconverter.com/rss-feed/USD/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		            	 ExchangeRate exchangeRate= new ExchangeRate();
			             exchangeRate.setCorpID(sessionCorpID);
			             String res=crMap.getKey().toString();
			             String[] res1=res.split("/");
			             String res2=res1[0];
			             exchangeRate.setCurrency(res2);
			             exchangeRate.setBaseCurrency("USD");
			             String ras=crMap.getValue().toString();
			             String[] ras2=ras.split(" ");
			             String ras3=ras2[5].replaceAll(",","");
			             String ras4=ras3.substring(0, ras3.length()-1);
			             BigDecimal officialrate= new BigDecimal(0);
			             if((String)crMap.getValue()!="0"){
			             officialrate=new BigDecimal(ras3);
			             }
			             exchangeRate.setOfficialRate(new BigDecimal(ras4));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin="0.0000";
		             if(exchangeRate.getCurrency()!=null && exchangeRate.getBaseCurrency()!=null && exchangeRate.getCurrency().toString().equalsIgnoreCase(exchangeRate.getBaseCurrency().toString()) ){
		            	 
		             }else{
		             margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             }
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
		             exchangeRateManager.save(exchangeRate); 
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    
	   return null;	
	}*/
	/*public String saveGBPRate(String sessionCorpID, String file, String userName){
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           //url = new URL("http://themoneyconverter.com/GBP/rss.xml");
        	url = new URL("http://themoneyconverter.com/rss-feed/GBP/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		             String res=crMap.getKey().toString();
		             String[] res1=res.split("/");
		             String res2=res1[0];
		             exchangeRate.setCurrency(res2);
		             exchangeRate.setBaseCurrency("GBP");
		             String ras=crMap.getValue().toString();
		             String[] ras2=ras.split(" ");
		             String ras3=ras2[5].replaceAll(",","");
		             String ras4=ras3.substring(0, ras3.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if((String)crMap.getValue()!="0"){
		             officialrate=new BigDecimal(ras3);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras4));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
		             exchangeRateManager.save(exchangeRate); 
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
    
	   return null;	
	
}*/
	/*public String saveIDRRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/IDR/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("IDR");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
				      if(ras6!=null && ras6.indexOf("0.0000")>-1){
				    	  ras6="0.0001";
				      }				      		             
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin="0.0000";
		             if(exchangeRate.getCurrency()!=null && exchangeRate.getBaseCurrency()!=null && exchangeRate.getCurrency().toString().equalsIgnoreCase(exchangeRate.getBaseCurrency().toString()) ){
		            	 
		             }else{
		             margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             }		             
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             if(baseCurrencyRate!=null && baseCurrencyRate.toString().indexOf("0.0000")>-1){
		            	 exchangeRate.setBaseCurrencyRate(new BigDecimal("0.0001"));
		             }else{
		            	 exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             }
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
	
	public String saveDynamicRate(String sessionCorpID, String file, String userName,String baseCurrency){
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
        	 url = new URL("http://www.floatrates.com/daily/"+baseCurrency+".xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	String val="";
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("targetCurrency");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	NodeList rateList = fstElmnt.getElementsByTagName("exchangeRate");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        val=((Node) rateNm.item(0)).getNodeValue();
		        val=val.replaceAll(",", "");
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), val);
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		               exchangeRate.setCurrency(crMap.getKey().toString());
		              exchangeRate.setBaseCurrency(baseCurrency);
		             exchangeRate.setOfficialRate(new BigDecimal(crMap.getValue().toString()));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin="0.0000";
		             if(exchangeRate.getCurrency()!=null && exchangeRate.getBaseCurrency()!=null && exchangeRate.getCurrency().toString().equalsIgnoreCase(exchangeRate.getBaseCurrency().toString()) ){
		            	 
		             }else{
		             margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             }		             
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(exchangeRate.getOfficialRate().toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             if(baseCurrencyRate!=null && baseCurrencyRate.toString().indexOf("0.0000")>-1){
		            	 exchangeRate.setBaseCurrencyRate(new BigDecimal("0.0001"));
		             }else{
		            	 exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             }
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,crMap.getKey().toString());
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
			
	}
	
	/*public String saveJODRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/JOD/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("JOD");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin="0.0000";
		             if(exchangeRate.getCurrency()!=null && exchangeRate.getBaseCurrency()!=null && exchangeRate.getCurrency().toString().equalsIgnoreCase(exchangeRate.getBaseCurrency().toString()) ){
		            	 
		             }else{
		             margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             }		             
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
	/*public String saveSGDRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/SGD/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("SGD");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
	private synchronized static String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			if (el.getFirstChild() != null) {
				textVal = el.getFirstChild().getNodeValue();
			}
		}

		return textVal;
	}
	public String saveCADRate(String sessionCorpID, String file, String userName){
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		 Map<String, String> currencyRateMap= new HashMap<String, String>();
		try {
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
				URL url = new URL("http://www.bankofcanada.ca/stats/assets/rates_rss/closing/en_all.xml");
				InputStream stream = url.openStream();
				Document doc = docBuilder.parse(stream);
				doc.getDocumentElement ().normalize ();
				NodeList pref = doc.getElementsByTagName("cb:exchangeRate");
				String cur="";
				String rate="";
				Element elPref=null;
				currencyRateMap.put("CAD", "1");
				for (int s = 0; s < pref.getLength(); s++) {
					elPref = (Element)pref.item(s);
					cur = getTextValue(elPref, "cb:targetCurrency");
					cur = cur.replaceAll("_CLOSE", "");
					rate = getTextValue(elPref, "cb:value");
					currencyRateMap.put(cur, rate);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
	       for(Map.Entry<String, String> exchangeRateRecord:currencyRateMap.entrySet()){
	    	   if((exchangeRateRecord.getKey()!=null) && !(currencyList.contains(exchangeRateRecord.getKey()))){
		    	   ExchangeRate exchangeRate= new ExchangeRate();
		    	   exchangeRate.setCorpID(sessionCorpID);
	               exchangeRate.setCurrency(exchangeRateRecord.getKey());
		           exchangeRate.setBaseCurrency("CAD");
		           exchangeRate.setOfficialRate(new BigDecimal(exchangeRateRecord.getValue()));
		           BigDecimal divisor=new BigDecimal(100);
		             String margin="0.0000";
		             if(exchangeRate.getCurrency()!=null && exchangeRate.getBaseCurrency()!=null && exchangeRate.getCurrency().toString().equalsIgnoreCase(exchangeRate.getBaseCurrency().toString()) ){
		             }else{
		            	 margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             }
		           exchangeRate.setMarginApplied(new BigDecimal(margin));
		           BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		           BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(exchangeRateRecord.getValue().toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,exchangeRateRecord.getKey());
			         String checkCurrencyManualUpdate="NO";
					 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
						 checkCurrencyManualUpdate="YES";
					 }
					 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
						 if(!sessionCorpID.equalsIgnoreCase("UGCA")){
							 exchangeRateManager.save(exchangeRate); 
						 }else{
							 if("CAD,USD,GBP,EUR".indexOf(exchangeRateRecord.getKey())>-1){
								 exchangeRateManager.save(exchangeRate); 
							 }
						 }
					 }
		         }

	       }
	       return null;
	}	
	/*public String saveKRWRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/KRW/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("KRW");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
	/*public String saveBRLRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/BRL/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("BRL");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
	/*public String saveINRRate(String sessionCorpID, String file, String userName){

		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL("http://themoneyconverter.com/rss-feed/INR/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("INR");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
			         List estimatePayableContractRate=exchangeRateManager.findAccExchangeRate(sessionCorpID,ras3);
			         String checkCurrencyManualUpdate="NO";
						 if((estimatePayableContractRate!=null)&&(!estimatePayableContractRate.isEmpty())&& estimatePayableContractRate.get(0)!=null && (!(estimatePayableContractRate.get(0).toString().equals("")))){
							 checkCurrencyManualUpdate="YES";
						 }
						 if(checkCurrencyManualUpdate.equalsIgnoreCase("NO")){
							 exchangeRateManager.save(exchangeRate); 
						 }
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
	}*/
/*	public String saveCNYRate(String sessionCorpID, String file, String userName){
		getHibernateTemplate().bulkUpdate("delete from ExchangeRate where (manualUpdate='N' or manualUpdate is null or manualUpdate='') and corpID='"+sessionCorpID+"'");
		// List currencyList = getHibernateTemplate().find("select if(currency is null or currency='','1',currency) from ExchangeRate where corpID='"+sessionCorpID+"'");
		List<String> currencyList=getHibernateTemplate().find("select currency from ExchangeRate where corpID=?", sessionCorpID);
		URL url = null;
	    InputStream in=null;
	    URLConnection con=null;
        try {
           url = new URL(" http://themoneyconverter.com/rss-feed/CNY/rss.xml");
            }
        catch (MalformedURLException e) { } 
        try {
			con = url.openConnection();
			con.connect();
	        in = con.getInputStream();
	        Map<String, String> currencyRateMap= new HashMap<String, String>();
	        try {
	        	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        	DocumentBuilder db = dbf.newDocumentBuilder();
	        	Document doc = db.parse(in);
	        	doc.getDocumentElement().normalize();
	        	//System.out.println("Root element " + doc.getDocumentElement().getNodeName());
	        	NodeList nodeLst = doc.getElementsByTagName("item");
	        	for (int s = 0; s < nodeLst.getLength(); s++) {
	        	Node fstNode = nodeLst.item(s);
	        	if (fstNode.getNodeType() == Node.ELEMENT_NODE) {
	        	Element fstElmnt = (Element) fstNode;
	        	NodeList kodeList = fstElmnt.getElementsByTagName("title");
	        	Element fstNmElmnt = (Element) kodeList.item(0);
	        	NodeList fstNm = fstNmElmnt.getChildNodes();
	        	//System.out.println("Currency : " + ((Node) fstNm.item(0)).getNodeValue());	        	 
	        	NodeList rateList = fstElmnt.getElementsByTagName("description");
		        Element rateNmElmnt = (Element) rateList.item(0);
		        NodeList rateNm = rateNmElmnt.getChildNodes();
		        //System.out.println("Rate : " + ((Node) rateNm.item(0)).getNodeValue());	        	 
	        	currencyRateMap.put(((Node) fstNm.item(0)).getNodeValue(), ((Node) rateNm.item(0)).getNodeValue());
	        	    }
	        	  }
	        	Iterator it = currencyRateMap.entrySet().iterator();
		         while (it.hasNext()) {
		             Map.Entry crMap = (Map.Entry)it.next();             
		             if((crMap.getKey()!=null) && !(currencyList.contains(crMap.getKey()))){
		             ExchangeRate exchangeRate= new ExchangeRate();
		             exchangeRate.setCorpID(sessionCorpID);
		                String ras=crMap.getKey().toString();
			        	String[] ras1=ras.split("/");
			        	String ras3=ras1[0];
		               exchangeRate.setCurrency(ras3);
		              exchangeRate.setBaseCurrency("CNY");
		              String regs=crMap.getValue().toString();
		               String[] res1=regs.split("=");
				      String res2=res1[1];
				      String[] res3=res2.split(" ");
				      String res4=res3[1];
				      String res5=res4.replaceAll(",","").trim();
				      String ras6=res5.substring(0, res5.length()-1);
		             BigDecimal officialrate= new BigDecimal(0);
		             if(!res5.equals("0")){
		             officialrate=new BigDecimal(res5);
		             }
		             exchangeRate.setOfficialRate(new BigDecimal(ras6));
		             BigDecimal divisor=new BigDecimal(100);
		             String margin=getHibernateTemplate().find("select currencyMargin from SystemDefault where corpID=?", sessionCorpID).get(0).toString();
		             exchangeRate.setMarginApplied(new BigDecimal(margin));
		             BigDecimal marginPercent=new BigDecimal(margin).divide(divisor,4,RoundingMode.HALF_UP);
		             BigDecimal sustractedValue=new BigDecimal(1).subtract(marginPercent); 
		             BigDecimal baseCurrencyRate=new BigDecimal(officialrate.toString()).multiply(sustractedValue);
		             BigDecimal cBR=new BigDecimal("0.0");
		             if(!(baseCurrencyRate.toString().equalsIgnoreCase("0.0")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.0000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.00000")) && !(baseCurrencyRate.toString().equalsIgnoreCase("0.000000"))){
		            	 cBR=new BigDecimal("1.0").divide(baseCurrencyRate,4,RoundingMode.HALF_UP);
		             }
		             exchangeRate.setCurrencyBaseRate(cBR);
		             exchangeRate.setManualUpdate("N");
		             exchangeRate.setBaseCurrencyRate(baseCurrencyRate);
		             exchangeRate.setValueDate(new Date());
		             exchangeRate.setCreatedBy(userName);
		             exchangeRate.setCreatedOn(new Date());
		             exchangeRate.setUpdatedBy(userName);  
		             exchangeRate.setUpdatedOn(new Date());
		             exchangeRateManager.save(exchangeRate); 
		             }
		          }	
	        	} 
	        catch (Exception e) {
	        	e.printStackTrace();
	        	}
		} 
        catch (IOException e1) {
        	e1.printStackTrace();
        }
        finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
		return null;
}*/

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
}

