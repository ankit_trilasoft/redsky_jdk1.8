<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<head>

<title>Order Management Detail</title>
<meta name="heading"
	content="Order Management Detail" />

<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}

.subcontenttabChild {
	background:#DCDCDC url(images/greylinebg.gif) repeat scroll 0% 0%;
	border:1px solid #DCDCDC;	
	color:#15428B;
	font-family:Arial,Helvetica,sans-serif;
	font-size:12px;
	font-weight:bold;
	!width:100%;
	height:20px;
	padding:4px 3px 0 5px;
	text-decoration:none;
}
.OverFlowPr {
	overflow-x:auto;
	overflow-y:auto;
	!overflow-x:scroll;
	!overflow-y:visible;			
}
.upper-case{
	text-transform:capitalize;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

<style><%@ include file="/common/calenderStyle.css"%></style>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript">
function openadd()
{
animatedcollapse.ontoggle=function($, divobj, state){
	
	if (divobj.id=="address"){ //if "peter" DIV is being toggled
				  if (state=="block"){ //if div is expanded
		   animatedcollapse.show("additional");
		}
	 else{
	      animatedcollapse.hide("additional");
		 }
					  
	
	}
}
}
</script>
<!-- Modification closed here -->
<script type="text/javascript">
function openPopWindow(target){
	var targetName=target.name;
	if(targetName == 'P'){
		if(document.forms['customerFileForm'].elements['destinationcopyaddress'].checked){
			var destinationCountryVal = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
			if(destinationCountryVal == null || destinationCountryVal == '' || destinationCountryVal == undefined){
				alert('Please Select Destination Country.');
				document.forms['customerFileForm'].elements['customerFile.destinationCountry'].focus();
				return false;
			}
		}
		else{
			var originCountryVal = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
			if(originCountryVal == null || originCountryVal == '' || originCountryVal == undefined){
				alert('Please Select Origin Country.');
				document.forms['customerFileForm'].elements['customerFile.originCountry'].focus();
				return false;
			}
		}
	}
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var orgAdd1= document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
	var orgAdd2= document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
	var orgAdd3= document.forms['customerFileForm'].elements['customerFile.originAddress3'].value;
	var orgCountry= document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var orgState= document.forms['customerFileForm'].elements['customerFile.originState'].value;
	var orgCity= document.forms['customerFileForm'].elements['customerFile.originCity'].value;
	var orgZip= document.forms['customerFileForm'].elements['customerFile.originZip'].value;
	var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgphone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var orgemail = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var orgeFax= document.forms['customerFileForm'].elements['customerFile.originFax'].value;
	var prefix= document.forms['customerFileForm'].elements['customerFile.prefix'].value;
	var companyDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
	var checkAddress=document.forms['customerFileForm'].elements['destinationcopyaddress'].checked;
	var orgAddress=orgAdd1+'~'+orgAdd2+'~'+orgAdd3+'~'+orgCountry+'~'+orgState+'~'+orgCity+'~'+orgZip+'~'+orgphone+'~'+orgemail+'~'+orgeFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var destAdd1= document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value;
	var destAdd2= document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
	var destAdd3= document.forms['customerFileForm'].elements['customerFile.destinationAddress3'].value;
	var destCountry= document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var destState= document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
	var destCity= document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
	var destZip= document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
	var destphone = document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value;
	var destemail = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
	var destFax= document.forms['customerFileForm'].elements['customerFile.destinationFax'].value;
	var destAddress=destAdd1+'~'+destAdd2+'~'+destAdd3+'~'+destCountry+'~'+destState+'~'+destCity+'~'+destZip+'~'+destphone+'~'+destemail+'~'+destFax+'~'+first+'~'+last+'~'+prefix+'~'+companyDiv+'~'+status;
	var compDiv = '';
<c:if  test="${companies == 'Yes'}">
compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
</c:if>
if(targetName=='P' && checkAddress==true){
	createPrivateParty(destAddress);
	}else if(targetName=='P' && checkAddress==false){ 
	createPrivateParty(orgAddress);
	}else{
	javascript:openWindow('partnersPopup.html?decorator=popup&popup=true&partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&orgAddress='+encodeURIComponent(orgAddress)+'&destAddress='+encodeURIComponent(destAddress)+'&compDiv='+compDiv+'&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');
	}
}
</script>
<script type="text/javascript">
window.onload = function() { 
	<% int count=0; %>
	getFamilysize();
	setEntitleValue();
var fieldName = document.forms['customerFileForm'].elements['field'].value;
var fieldName1 = document.forms['customerFileForm'].elements['field1'].value;
	if(fieldName!=''){
		document.forms['customerFileForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
		document.forms['customerFileForm'].elements[fieldName1].className = 'rules-textUpper';
	}
}

function showPartnerAlert(display, code, position){
     if(code == ''){ 
     	document.getElementById(position).style.display="none";
     	ajax_hideTooltip();
     }else if(code != ''){
     	getBAPartner(display,code,position);
     }      
}
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     http3.open("GET", url, true);
     http3.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     http3.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (http3.readyState == 4){
		var results = http3.responseText
        results = results.trim();
        //alert(results.length);
        if(results.length > 565){
          	document.getElementById(position).style.display="block";
          	if(display != 'onload'){
          		getPartnerAlert(code,position);
          	}
      	}else{
      		document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	}
	}
}


function focusDate(target){
	document.forms['customerFileForm'].elements[target].focus();
}
function EmailPortDate(){
	var EmailPort = document.forms['customerFileForm'].elements['customerFile.email'].value;
	document.forms['customerFileForm'].elements['customerFileemail'].value = EmailPort;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value=EmailPort;
	}	
}

</script>
<script language="JavaScript" type="text/javascript">
function myDate() {
	var HH2;
	var HH1;
	var MM2;
	var MM1;
	var tim1=document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
	var tim2=document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
		tim1=tim1.replace(":","");
		tim2=tim2.replace(":","");
	if((tim1.substring(0,tim1.length-2)).length == 1){
		HH1 = '0'+tim1.substring(0,tim1.length-2);
	}else {
		HH1 = tim1.substring(0,tim1.length-2);
	}
	MM1 = tim1.substring(tim1.length-2,tim1.length);
	if((tim2.substring(0,tim1.length-2)).length == 1){
		HH2 = '0'+tim2.substring(0,tim2.length-2);
	}else {
		HH2 = tim2.substring(0,tim2.length-2);
	}
	MM2 = tim2.substring(tim2.length-2,tim2.length);
	document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = HH1 + ':' + MM1;
	document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = HH2 + ':' + MM2;

	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off"); 
}

function setCountry(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		if(job=='UVL' || job=='HVY' || job=='MVL' || job=='DOM' || job=='LOC' || job=='OFF' || job=='VAI' || job=='STO' || job=='MLL'){
		    document.forms['customerFileForm'].elements['customerFile.originCountry'].value = 'United States';
			document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value = 'United States';
			document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value='USA'
			document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value='USA'
			getState(document.forms['customerFileForm'].elements['customerFile.originCountry']);
			getDestinationState(document.forms['customerFileForm'].elements['customerFile.destinationCountry']);
		}
	}	
}

function setStatas(){
	if(document.forms['customerFileForm'].elements['customerFile.id'].value ==''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
		
		if(job=='UVL' || job=='MVL' || job=='DOM' || job=='LOC' || job=='OFF' || job=='VAI' || job=='STO' || job=='MLL' || job=='HVY'){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
			
		}
	}	
}

function copyCompanyToDestination(targetElement){
		document.forms['customerFileForm'].elements['customerFile.destinationCompany'].value = targetElement.value;
}
var digits = "0123456789";
var phoneNumberDelimiters = "()- ";
var validWorldPhoneChars = phoneNumberDelimiters + "+";
var minDigitsInIPhoneNumber = 10;

function autoPopulate_customerFile_originCountry(targetElement) {		
		var oriCountry = targetElement.value;
		var oriCountryCode = '';
		if(oriCountry == 'United States')
		{
			oriCountryCode = "USA";
		}
		if(oriCountry == 'India')
		{
			oriCountryCode = "IND";
		}
		if(oriCountry == 'Canada')
		{
			oriCountryCode = "CAN";
		}
		if(oriCountry == 'United States' || oriCountry == 'Canada' || oriCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
			if(oriCountry == 'United States'){
			document.forms['customerFileForm'].elements['customerFile.originZip'].focus();
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.originState'].value = '';
			autoPopulate_customerFile_originCityCode(oriCountryCode,'special');
		}
}
	function autoPopulate_customerFile_destinationCountry(targetElement) {   
	
	
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
			if(dCountry == 'United States'){
			document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus();
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.destinationState'].value = '';
			autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	}
	function autoPopulate_customerFile_destinationCityCode(targetElement, w) {
	var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
	}
	function autoPopulate_customerFile_originCityCode(targetElement, w) {
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};

		if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
				
			}
			else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			}
		}
	}
	function autoPopulate_customerFile_statusDate(targetElement) {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.statusDate'].value=datam;
	}
	function autoPopulate_customerFile_approvedDate(targetElement, w) {
		
		var r={
		 'special':/[\W]/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		targetElement.form.elements['customerFile.billApprovedDate'].value=datam;
	}	 
   function openOriginLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.originCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.originZip'].value;
        var address = "";
        if(country == 'United States')
        {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
        }
        address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	 function openDestinationLocation() {
        var city = document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
        var zip = document.forms['customerFileForm'].elements['customerFile.destinationZip'].value;
       	var address = "";
        if(country == 'United States')
        {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress1'].value+","+document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        }
        else
        {
        	address = document.forms['customerFileForm'].elements['customerFile.destinationAddress2'].value;
        }
       	address = address.replace("#","-");
        var state = document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
    	window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	function generatePortalId() {
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked = true ;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		var emailID = document.forms['customerFileForm'].elements['customerFile.email'].value;
		if(checkPortalBox==true && emailID !='')
		{
		var emailID2 = document.forms['customerFileForm'].elements['customerFile.email2'].value;
		emailID=emailID+','+emailID2;
		}
		var emailTypeFlag=false;
		var emailTypeVOERFlag=false;
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeflag">
			emailTypeFlag = true;
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.customerfile.field.emailtypeVOERflag">
		  emailTypeVOERFlag=true;
	    </configByCorp:fieldVisibility>
		var portalIdName = document.forms['customerFileForm'].elements['customerFile.customerPortalId'].value;
		
		var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		if(checkPortalStatus !='CNCL' && checkPortalStatus !='CLOSED'){
		if(emailID!='')
		{
		if(portalIdName!=''){
		window.open('sendMail.html?id=${customerFile.id}&emailTo='+emailID+'&userID='+portalIdName+'&emailTypeFlag='+emailTypeFlag+'&emailTypeVOERFlag='+emailTypeVOERFlag+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
		}
		else{
		alert("Please create ID for Customer Portal");
		}
		}else
		{
			alert("Please enter primary email");
			document.forms['customerFileForm'].elements['customerFile.email'].focus();
		}
		}
		else{
		alert("You can't Reset password for Customer Portal ID because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		}
	} 
	
	
	function updateOriginSoAdd() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
        javascript:openWindow('changeOriginSoAdd.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;	    
	    }
		} 
	function updateOriginAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateOriginAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		} 
		}
	function updateDestinAddSo() {
	    var agree = confirm("Do you want to update address in corresponding S/Os? click OK to proceed or Cancel.");
	    if(agree){
		javascript:openWindow('updateDestinAddSo.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		}
		}
	function updateDestinAddSoAndTkt() {
	var agree = confirm("Do you want to update address in corresponding S/Os and Tickets? click OK to proceed or Cancel.");
	if(agree){
		javascript:openWindow('updateDestinAddSoAndTkt.html?custid=${customerFile.id}&decorator=popup&popup=true&from=file',650,170) ;
		}
		}
		
		function disabledSOBtn()
		{
			//document.forms['customerFileForm'].originSoUpdate.disabled=true;
			//document.forms['customerFileForm'].originSoTicketUpdate.disabled=true;
		}
	function disabledSODestinBtn()
		{
			//document.forms['customerFileForm'].originSoDestinUpdate.disabled=true;
			//document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=true;
		}
		
function disabledSOBtnReset()
		{
			//document.forms['customerFileForm'].originSoUpdate.disabled=false;
			//document.forms['customerFileForm'].originSoTicketUpdate.disabled=false;
		}
function disabledSODestinBtnReset()
		{
			//document.forms['customerFileForm'].originSoDestinUpdate.disabled=false;
			//document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=false;
		}
		
	function checkPortalBoxId() {
	    var checkPortalStatus=document.forms['customerFileForm'].elements['customerFile.status'].value;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked;
		if(checkPortalBox == true){
		if(checkPortalStatus =='CNCL' || checkPortalStatus =='CLOSED')
		{
		alert("You can't Activate the ID for Customer Portal because job is Closed/Cancelled");
		document.forms['customerFileForm'].elements['customerFile.portalIdActive'].checked=false;
		}
		}
		
	} 
function checkPortalBoxSecondaryId() {
	    var secondaryEmail=document.forms['customerFileForm'].elements['customerFile.email2'].value;
		var checkPortalBox = document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked;
		if(checkPortalBox == true && secondaryEmail==''){
		alert("Please select secondary email in origin section.");
		document.forms['customerFileForm'].elements['customerFile.secondaryEmailFlag'].checked=false;
		}
		
	} 	
function scheduleSurvey()
           {
           var jobTypeSchedule =document.forms['customerFileForm'].elements['customerFile.job'].value;
           var addressLine1=document.forms['customerFileForm'].elements['customerFile.originAddress1'].value;
           var addressLine2=document.forms['customerFileForm'].elements['customerFile.originAddress2'].value;
          	addressLine1= addressLine1.replace('#','');
          	addressLine2= addressLine2.replace('#','');
          	if(addressLine2!=''){
          	addressLine1=addressLine1+', '+addressLine2;}
           var city=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
           var country=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
           var zipCode=document.forms['customerFileForm'].elements['customerFile.originZip'].value;
           var state = document.forms['customerFileForm'].elements['customerFile.originState'].value;
           var survey=document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var targetAddress=addressLine1+', '+city+', '+state+', '+zipCode+', '+country;
           var surveyTime="";
	       var surveyTime2="";
	       var estimated="";
           if(jobTypeSchedule=='' || addressLine1=='' || city =='' || country=='' || zipCode=='')
           { 
           alert("Please select job type, address line1, country, city, postal code in origin section.");
           }
           else if(survey !='')
           {
           var agree= confirm("WARNING: Survey already scheduled, do you want to reschedule? Hit OK to reschedule and Cancel to keep current date.");
           if(agree){
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }           
           }
           else{
           window.open("sendSurveySchedule.html?jobTypeSchedule="+jobTypeSchedule+"&targetAddress="+targetAddress+"&surveyTime="+surveyTime+"&surveyTime2="+surveyTime2+"&estimated="+estimated+"&decorator=popup&popup=true","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes");
           }
           }
        function comptetiveSurveyEmail(val) {
          var comp = document.forms['customerFileForm'].elements['customerFile.comptetive'].value;
		if(comp=="Y"){
		document.forms['customerFileForm'].Button1.disabled=false;
		} else {
		document.forms['customerFileForm'].Button1.disabled=true;
		}
        }
        
function validate_email(targetElement)
{
with (targetElement)
{
apos=value.indexOf("@")
dotpos=value.lastIndexOf(".")
if (apos<1||dotpos-apos<2) 
  {
   return false;
  }
else {return true}
}
}
        
    	function sendSurveyEmail(){
    	if(document.forms['customerFileForm'].elements['customerFile.email'].value !=''){
  		    if (validate_email(document.forms['customerFileForm'].elements['customerFile.email'])== true) {
           var estimator = document.forms['customerFileForm'].elements['customerFile.estimator'].value;
           var survey = document.forms['customerFileForm'].elements['customerFile.survey'].value;
           var surveyTime = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
           var surveyTime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
           var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
           var hour = surveyTime.substring(0, surveyTime.indexOf(":"));
           var hour2 = surveyTime2.substring(0, surveyTime.indexOf(":"));
           var min = surveyTime.substring(surveyTime.indexOf(":")+1, surveyTime.length);
           var min2 = surveyTime2.substring(surveyTime2.indexOf(":")+1, surveyTime2.length);
           if (hour<12){
           		surveyTime=hour+":"+min+"AM";
           } else if ((surveyTime.substring(0, surveyTime.indexOf(":")))>12) {
               surveyTime=(hour-12)+":"+min+"PM";
           }else{
               surveyTime=hour+":"+min+"PM";
           }
            if (hour2<12){
           		surveyTime2=hour2+":"+min2+"AM";
           } else if (hour2>12) {
               surveyTime2=(hour2-12)+":"+min2+"PM";
           }else{
               surveyTime2=hour2+":"+min2+"PM";
           }
           if(estimator=='' || survey=='' || surveyTime =='00:00AM' || surveyTime2=='00:00AM' || surveyTime =='' || surveyTime2=='') { 
           alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
           } else if (survey !='' && surveyTime!='00:00AM' && surveyTime2 !='00:00AM' && estimator !='' && surveyTime2 !='' && surveyTime2 !=''){
	        var systemDate = new Date();
			var mySplitResult = survey.split("-");
		   	var day = mySplitResult[0];
		   	var month = mySplitResult[1];
		   	var year = mySplitResult[2];
		 	if(month == 'Jan'){
		       month = "01";
		   	}else if(month == 'Feb'){
		       month = "02";
		   	}else if(month == 'Mar'){
		       month = "03"
		   	}else if(month == 'Apr'){
		       month = "04"
		   	}else if(month == 'May'){
		       month = "05"
		   	}else if(month == 'Jun'){
		       month = "06"
		   	}else if(month == 'Jul'){
		       month = "07"
		   	}else if(month == 'Aug'){
		       month = "08"
		   	}else if(month == 'Sep'){
		       month = "09"
		   	}else if(month == 'Oct'){
		       month = "10"
		   	}else if(month == 'Nov'){
		       month = "11"
		   	}else if(month == 'Dec'){
		       month = "12";
		   	}
		   	var weekday=new Array(7);
			weekday[0]="Sunday";
			weekday[1]="Monday";
			weekday[2]="Tuesday";
			weekday[3]="Wednesday";
			weekday[4]="Thursday";
			weekday[5]="Friday";
			weekday[6]="Saturday";
			var finalDate = month+"/"+day+"/"+year;
		   	survey = finalDate.split("/");
		   	var enterDate = new Date(survey[0]+"/"+survey[1]+"/20"+survey[2]);
		  	var sendDate = weekday[enterDate.getDay()]+" "+finalDate;
		  	var newsystemDate = new Date(systemDate.getMonth()+1+"/"+systemDate.getDate()+"/"+systemDate.getFullYear());
		  	var daysApart = Math.round((enterDate-newsystemDate)/86400000);
		  	if(daysApart < 0){
		    	alert("The Survey Schedule date is past the current date-The Survey is already over or Please Schedule a new survey.");
		    	return false;
		  	}
		  	if(survey != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			if (hour < 0  || hour > 23) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					return false;
				}
				if (min<0 || min > 59) {
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
					return false;
				}
				if(systemDate.getHours() > hour){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				} else if(systemDate.getHours() == hour && systemDate.getMinutes() > min){
					alert("The Survey Schedule time is past the current time-The Survey is already over or Please Schedule a new survey.");
					return false;
				   }				
			  }
	  	   }
	  	   var agree= confirm("Press OK to Send Survey Email, else press Cancel.");
            if(agree){
            var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
			var firstName = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
			var surveyDate = document.forms['customerFileForm'].elements['customerFile.survey'].value; 
			var updatedBy = document.forms['customerFileForm'].elements['customerFile.updatedBy'].value;
			var subject = "Confirmation for "+firstName+"'s survey on "+surveyDate;
			window.open('sendSurveyEmail.html?id=${customerFile.id}&emailTo='+email+'&estimator='+estimator+'&surveyDates='+sendDate+'&firstName='+firstName+'&surveyTime='+surveyTime+'&surveyTime2='+surveyTime2+'&decorator=popup&popup=true&from=file','','width=550,height=170') ;
			 } else {
            }
            } else {
            alert("Survey is not yet Scheduled - Cannot send Survey Email to the customer");
            } 
            
            } else {
             alert("Invalid Origin's Primary Email Id");
            } 
            } else {
            alert("Primary Email is not yet entered - Cannot send Survey Email to the customer");
            }
            }
	
	function completeTimeString() {
	
		stime1 = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
		stime2 = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value = "0" + stime2;
			}
		}
	}
function IsValidTime(clickType) {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['customerFileForm'].elements['customerFile.surveyTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.surveyTime'].focus();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['customerFileForm'].elements['customerFile.surveyTime2'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'Pre Move Survey' time must between 0 to 23 (Hrs)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'Pre Move Survey' time must between 0 to 59 (Min)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'Pre Move Survey' time must between 0 to 59 (Sec)");
document.forms['customerFileForm'].elements['customerFile.surveyTime2'].focus();
return false;
}

if(!(clickType == 'save')){
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
		var noSaveAction = '<c:out value="${customerFile.id}"/>';
      		var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
      		var shipNumber = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
      		var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
           if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				noSaveAction = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				noSaveAction = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				noSaveAction = 'surveysList.html?id1='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
				noSaveAction = 'showAccountPolicy.html?id='+id1+'&code='+billCode;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
				openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',900,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
				noSaveAction = 'editImfEntitlement.html?cid='+id1;
				}
             
          processAutoSave(document.forms['customerFileForm'], 'saveCustomerFile!saveOnTabChange.html', noSaveAction);
          
	}else{
	var id1 = document.forms['customerFileForm'].elements['customerFile.id'].value;
	var billCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var shipNumber = document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
	if (document.forms['customerFileForm'].elements['formStatus'].value == '1'){
		var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='customerFileDetail.heading'/>");
		if(agree){
			document.forms['customerFileForm'].action = 'saveCustomerFile!saveOnTabChange.html';
			document.forms['customerFileForm'].submit();
		}else{
			if(id1 != ''){
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+billCode;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
				openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',900,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
				location.href = 'editImfEntitlement.html?cid='+id1;
				}
			
		}
		}
	}else{
	if(id1 != ''){
		if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
				location.href = 'customerServiceOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.raterequest'){
				location.href = 'customerRateOrders.html?id='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.surveys'){
				location.href = 'surveysList.html?id1='+id1;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.accountpolicy'){
				location.href = 'showAccountPolicy.html?id='+id1+'&code='+billCode;
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.reports'){
				openWindow('subModuleReports.html?id='+id1+'&jobNumber='+shipNumber+'&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true',740,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.docs'){
				openWindow('myFiles.html?id='+id1+'&myFileFor=CF&decorator=popup&popup=true',740,400);
				}
			if(document.forms['customerFileForm'].elements['gotoPageString'].value == 'gototab.imfEntitlements'){
				location.href = 'editImfEntitlement.html?cid='+id1;
				}
			
	}
	}
}
}
}


function notExists(){
	alert("The customer information has not been saved yet, please save customer information to continue");
}


function openPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
var compDiv = '';
<c:if  test="${companies == 'Yes'}">
compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
</c:if>
javascript:openWindow('partnersPopup.html?partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&decorator=popup&popup=true&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.billToName&fld_code=customerFile.billToCode');

}

function openBookingAgentPopWindow(){
	var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
	var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
	var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
	var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
	var compDiv = '';
	<c:if  test="${companies == 'Yes'}">
	compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	</c:if>
	javascript:openWindow('partnersPopup.html?partnerType=AC&flag=3&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&decorator=popup&popup=true&fld_sixthDescription=customerFile.customerEmployer&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');

}

function openAccountPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
var phone = document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value;
var email = document.forms['customerFileForm'].elements['customerFile.email'].value;
var compDiv = '';
<c:if  test="${companies == 'Yes'}">
compDiv=document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
</c:if>
javascript:openWindow('partnersPopup.html?partnerType=AC&flag=4&firstName='+first+'&lastName='+last+'&phone='+phone+'&email='+email+'&compDiv='+compDiv+'&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.accountName&fld_code=customerFile.accountCode');
}

function openBookingAgentPopWindow(){
var first = document.forms['customerFileForm'].elements['customerFile.firstName'].value;
var last = document.forms['customerFileForm'].elements['customerFile.lastName'].value;
javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=customerFile.bookingAgentName&fld_code=customerFile.bookingAgentCode');
}
</script>

<SCRIPT LANGUAGE="JavaScript">

function findBillToNameOnload(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2OnLoad;
     http2.send(null);
     }
     //copyBillToCode();
}

function handleHttpResponse2OnLoad(){
             if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length >= 2){
	           		if(res[2] == 'Approved'){
	           				if(res[3]== '' || res[3] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[3] == 'null'){
	           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[1];
 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[1];
 							getPortalDefaultPartnerData();
		           		}else{
		           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
		           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
		           			}else{
		           				alert('Please select company division.');
		           			} 
		           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
						 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
						 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
		           		}	
	           		}else if(res[2] != 'Approved'){
	           			alert("Bill To code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					 	document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
	           		}
	           		var code=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
                   var name=document.forms['customerFileForm'].elements['customerFile.accountName'].value;
	               if(code=="" || name==""){  
	           	    document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
		            document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
              }
	           }else{
	        	   alert("Bill To code not valid");
				document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
				document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
				document.forms['customerFileForm'].elements['customerFile.billToCode'].select();
			   }
           }
}
function getPortalDefaultPartnerData(){
	var partnerCode=document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="partnerPrivatePortalDefaultData.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	httpPortalAccess.open("GET", url, true);
	httpPortalAccess.onreadystatechange = httpPortalDefaultPartnerData;
	httpPortalAccess.send(null);
}
function httpPortalDefaultPartnerData(){
     if (httpPortalAccess.readyState == 4){
                var results = httpPortalAccess.responseText
                results = results.trim();
                if(results!=""){
                var res = results.split("~"); 
                if(res[0]!="NODATA"){
                 document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0]; 
                }
                if(res[1]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value = res[1]; 
                }
                if(res[2]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPricing'].value = res[2]; 
                }
                if(res[3]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personBilling'].value = res[3]; 
                }
                if(res[4]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.personPayable'].value = res[4]; 
                }
                if(res[5]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.auditor'].value = res[5]; 
                }
                /* if(res[6]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.coordinator'].value = res[6]; 
                } */
                if(res[7]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.source'].value = res[7]; 
                }
                if(res[8]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.billToAuthorization'].value = res[8]; 
                }
                if(res[9]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.contract'].value = res[9]; 
                }
                if(res[10]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountCode'].value = res[10]; 
                }
                if(res[11]!="NODATA"){
                    document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[11]; 
                }
                }
     }
}

// Method For City And State On the Basis of ZipCode
function findCityStateOfZipCode(targetElement,targetField){
      var zipCode = targetElement.value;
     
      if(targetField=='OZ'){
       var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
        if(document.forms['customerFileForm'].elements['originCountryFlex3Value'].value=='Y'){
         var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
          http33.open("GET", url, true);
          http33.onreadystatechange = function(){ handleHttpResponseCityState('OZ');};
          http33.send(null);
        }else{
        
         }
       }else if(targetField=='DZ'){
        var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
          if( document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y'){
           var url="findCityState.html?ajax=1&decorator=simple&popup=true&countryForZipCode="+encodeURI(countryCode)+"&zipCode=" + encodeURI(zipCode);
              http33.open("GET", url, true);
              http33.onreadystatechange = function(){ handleHttpResponseCityState('DZ');};
              http33.send(null);
             }else{
        
             }
      
        }  
      }  


function findCityState(targetElement,targetField){ 
     var zipCode = targetElement.value;
     if(targetField=='OZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value;
     } else if (targetField=='DZ'){
     var countryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value;
     }
     if(zipCode!='' && countryCode=='USA'){
     var url="findCityState.html?ajax=1&decorator=simple&popup=true&zipCode=" + encodeURI(zipCode);
     http33.open("GET", url, true);
     http33.onreadystatechange = function(){ handleHttpResponseCityState(targetField);};
     http33.send(null);
     }  }
 function handleHttpResponseCityState(targetField){
             if (http33.readyState == 4){
                var results = http33.responseText
                results = results.trim();
                var resu = results.replace("[",'');
                resu = resu.replace("]",'');
                resu = resu.split(",");
				for(var i = 0; i < resu.length+1; i++) {
                var res = resu[i];
                res = res.split("#");
                if(targetField=='OZ'){
	           	if(res[3]=='P') {	            
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.originState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.originHomePhone'].value=(res[4].substring(0,19));
	             } 
	              //document.getElementById('zipCodeList').style.display = 'none';
	             } else if(res[3]!='P') {
	             //document.getElementById('zipCodeList').style.display = 'block';
	           	 } 
	           	 document.forms['customerFileForm'].elements['customerFile.originCity'].focus();
	           	 } 	else if (targetField=='DZ'){
	           	if(res[3]=='P') {
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].value=res[0];
	           	 document.forms['customerFileForm'].elements['customerFile.destinationState'].value=res[2];
	            if (document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=='') {
	           	 document.forms['customerFileForm'].elements['customerFile.destinationHomePhone'].value=(res[4].substring(0,19));
	           }
	            //document.getElementById('zipDestCodeList').style.display = 'none';
	           	 }	else if(res[3]!='P') {
	           	///document.getElementById('zipDestCodeList').style.display = 'block';
	           	}
	           	document.forms['customerFileForm'].elements['customerFile.destinationCity'].focus();
	           	} } } else { } }

// End Of Method
function findBillToName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     //var url="vendorName.html?decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     var url="findBillToCodeCheckCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     httpBilltoCode.open("GET", url, true);
     httpBilltoCode.onreadystatechange = function(){ handleHttpResponse2(billToCode);};
     httpBilltoCode.send(null);
     }
     //copyBillToCode();
}

function findPaymentMethod(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
	}
    if(billToCode!=''){
     	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     	http22.open("GET", url, true);
     	http22.onreadystatechange = handleHttpResponsePM;
     	http22.send(null);
    }
}
function findAccToName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
    if(billToCode==''){
    	document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
    }
    if(billToCode!=''){
     var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponseAcc;
     http2.send(null);
   }  
}

function findPricingBillingPaybaleBILLName(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var url="pricingBillingPaybaleName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode) + "&jobType=" + encodeURI(job);
    http8.open("GET", url, true);
    http8.onreadystatechange = handleHttpResponse2001;
    http8.send(null);
}


function copyBillToCode(){
    var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	var url="accountName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCopyBill;
    http4.send(null);
}

function getOriginCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = handleHttpResponseCountryName;
    http4.send(null);
}
function getDestinationCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http4.open("GET", url, true);
    http4.onreadystatechange = httpDestinationCountryName;
    http4.send(null);
}

function httpDestinationCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.destinationCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['DestinationCountryFlex3Value'].value='N';
 					}
				}else{
                     
                 }
             }
        }
        
function findCompanyDivisionByBookAg(){
	var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findCompanyDivisionByBookAg.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode);
    http6.open("GET", url, true);
    http6.onreadystatechange = handleHttpResponse4444;
    http6.send(null);
}
	
function handleHttpResponse4444()
        {
		    if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim();
                var res = results.split("@");
               
                var targetElement = document.forms['customerFileForm'].elements['customerFile.companyDivision'];
					targetElement.length = res.length;
 					for(i=0;i<res.length;i++)
 					{
 						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].text = res[i];
						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[i].value = res[i];
						 if(res.length-1 == 1){
						document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[1].selected=true;
						} else {
						/////document.forms['customerFileForm'].elements['customerFile.companyDivision'].options[1].selected=false;
						}
					}
                	getJobList();
                
			}	
        }
function findCompanyCodeAndJobFromCompanyDivision(){
	var bookCode= document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findCompanyCodeAndJobFromCompanyDivisionAjax.html?ajax=1&decorator=simple&popup=true&bookCode="+encodeURI(bookCode)
		httpCompanyDivisionAjax.open("GET", url, true);
    	httpCompanyDivisionAjax.onreadystatechange = handleHttpResponseCompanyDivisionAjax;
    	httpCompanyDivisionAjax.send(null);
}

function handleHttpResponseCompanyDivisionAjax(){
	if (httpCompanyDivisionAjax.readyState == 4){
       var results = httpCompanyDivisionAjax.responseText
       results = results.trim();
       if(results!=''){
    	   var res = results.split('~')
    	   if(res[0]!=null && res[0]!='' && res[0]!='NA'){
    		   document.forms['customerFileForm'].elements['customerFile.companyDivision'].value = res[0];
    	   }
    	   if(res[1]!=null && res[1]!='' && res[1]!='NA'){
    		   document.forms['customerFileForm'].elements['customerFile.job'].value = res[1];
    	   }
       }
	}
}
var httpCompanyDivisionAjax = getHTTPObject();
function getContract() {
	var custJobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
	document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
     http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse3;
     http5.send(null);
	
}

function getContractReset() {
	var custJobType = '${customerFile.job}';
    document.forms['customerFileForm'].elements['custJobType'].value= custJobType;
	var contractBillCode = '${customerFile.billToCode}';
	document.forms['customerFileForm'].elements['contractBillCode'].value= contractBillCode;
	var custCreatedOn = document.forms['customerFileForm'].elements['customerFile.createdOn'].value;
    document.forms['customerFileForm'].elements['custCreatedOn'].value= custCreatedOn;
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
	var url="findContractbyJob.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(custJobType)+"&contractBillCode="+encodeURI(contractBillCode)+"&custCreatedOn="+encodeURI(custCreatedOn)+"&bookingAgentCode="+encodeURI(bookingAgentCode);
     http555.open("GET", url, true);
     http555.onreadystatechange = handleHttpResponse5555555;
     http555.send(null);
	
}

function findBookingAgentName(){
    var bookingAgentCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
    if(bookingAgentCode==''){
    	document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
    }
    if(bookingAgentCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    } 
}

function getState(targetElement) {
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse5;
     httpState.send(null);
}
	
function getDestinationState(targetElement){
	var country = targetElement.value; 
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}
function getDestinationStateReset(destinationAbc){
	var countryCode = destinationAbc;
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6666666666;
     http3.send(null);
}
function getOriginStateReset(originAbc) {
	 var countryCode = originAbc;
	 var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     httpState.open("GET", url, true);
     httpState.onreadystatechange = handleHttpResponse555555;
     httpState.send(null);
}



String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(billToCode){
             if (httpBilltoCode.readyState == 4){
                var results = httpBilltoCode.responseText
                results = results.trim();
                //alert(results);
                var res = results.split("~");
                //alert(res);
                if(res.length >= 2){
               // alert('0-->'+res[0]+'---1-->'+res[1]+'---2-->'+res[2]+'---3-->'+res[3]+'---4-->'+res[4]+'---5-->'+res[5]);
	           		if(res[5]== 'Approved'){
	           			if(res[2] == '0'){
	           				if(res[1]== '' || res[1] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[1] == 'null' || res[1] == '#')
	           				{
		           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[0];
	 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0];
	 							var test = res[4];
	 							var x=document.getElementById("billPayMethod")
	  							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
	  							if(billPayMethod == 0){
	 								for(var a = 0; a < x.length; a++){
	 									if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
	 										document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
	 									}
	 								}
	 							}
	 							showOrHide(0);
	 							showPartnerAlert('onchange',billToCode,'hidBillTo');
	 						}else{
	 							showOrHide(0);
	 							
			           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
			           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
			           			}else{
			           				alert('Please select company division.');
			           			} 
			           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].select();
							 	showPartnerAlert('onchange','','hidBillTo');
			           		}	
		           		}else if(res[2] == '1'){
		           			if(res[3] <= 0){
		           				showOrHide(0);
		           				alert('The Vendor cannot be selected as credit limit exceeded.');
		           				document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
							 	document.forms['customerFileForm'].elements['customerFile.billToName'].select();
							 	showPartnerAlert('onchange','','hidBillTo');
		           			}
		           			if(res[3] > 0){
		           				if(res[1]== '' || res[1]== '#' || res[1] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[1] == 'null')
		           				{
			           				document.forms['customerFileForm'].elements['customerFile.billToName'].value = res[0];
		 							document.forms['customerFileForm'].elements['customerFile.customerEmployer'].value = res[0];
		 							var test = res[4];
		 							var x=document.getElementById("billPayMethod")
		  							var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
		  							if(billPayMethod == 0){
		 								for(var a = 0; a < x.length; a++){
		 									if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
		 										document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
		 									}
		 								}
		 							}
		 							showOrHide(0);
	 								alert("The Vendor currently has available credit of "+res[3]+", please ensure that this s/o will be for less than this amount before selecting this vendor.");
	 								showPartnerAlert('onchange',billToCode,'hidBillTo');
	 							}else{
	 								showOrHide(0);
	 								//alert('a');
				           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
				           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
				           			}else{
				           				alert('Please select company division.');
				           			} 
				           			
				           			document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
								 	document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
								 	document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
								 	//document.forms['customerFileForm'].elements['customerFile.billToName'].select();
								 	showPartnerAlert('onchange','','hidBillTo');
			           			}
		           			}
		           			
		           		}
		            }else{
		            	showOrHide(0);
		            	
		            	alert("Bill To code not approved");    
						document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
						document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
						document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
						document.forms['customerFileForm'].elements['customerFile.billToName'].select();
						showPartnerAlert('onchange','','hidBillTo');
	           		}
	           	}else{
		           	showOrHide(0);
		            alert("Bill To code not valid");  
		            
					document.forms['customerFileForm'].elements['customerFile.billToCode'].value="";
					document.forms['customerFileForm'].elements['customerFile.billToName'].value="";
					document.forms['customerFileForm'].elements['customerFile.billPayMethod'].value="";
					document.forms['customerFileForm'].elements['customerFile.billToName'].select();
					showPartnerAlert('onchange','','hidBillTo');  
			   }
           }
}

function handleHttpResponsePM(){
   	if (http22.readyState == 4){
       	var results = http22.responseText
       	results = results.trim();
       	var res = results.split("#");
       	if(res.length >= 2){
	       	var test = res[6];
			var x=document.getElementById("billPayMethod")
			var billPayMethod = document.getElementById('billPayMethod').selectedIndex;
			if(billPayMethod == 0){
				for(var a = 0; a < x.length; a++){
					if(test == document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].value){
						document.forms['customerFileForm'].elements['customerFile.billPayMethod'].options[a].selected="true";
					}
				}
			}
		}	
	}
}

function handleHttpResponse2001(){
             if (http8.readyState == 4){
                var results = http8.responseText
                results = results.trim();
                var res = results.split("#"); 
                	if(res.length >= 3){ 
                			var test = res[0];
  							var x=document.getElementById("personPricing")
  							var personPricing = document.getElementById('personPricing').selectedIndex;
  						
  							if(personPricing ==0)
  							{
 							for(var a = 0; a < x.length; a++)
 							{
 								if(test == document.forms['customerFileForm'].elements['customerFile.personPricing'].options[a].value)
 								{
 									document.forms['customerFileForm'].elements['customerFile.personPricing'].options[a].selected="true";
 								}
 							}
 							}
 							var testBilling = res[1];
  							var y=document.getElementById("personBilling")
  							var personBilling = document.getElementById('personBilling').selectedIndex;
  							if(personBilling ==0)
  							{
 							for(var a = 0; a < y.length; a++)
 							{
 								if(testBilling == document.forms['customerFileForm'].elements['customerFile.personBilling'].options[a].value)
 								{
 									document.forms['customerFileForm'].elements['customerFile.personBilling'].options[a].selected="true";
 								}
 							}
 							}
 							var testPayable = res[2];
  							var z=document.getElementById("personPayable")
  							var personPayable = document.getElementById('personPayable').selectedIndex;
  							if(personPayable ==0)
  							{
 							for(var a = 0; a < z.length; a++)
 							{
 								if(testPayable == document.forms['customerFileForm'].elements['customerFile.personPayable'].options[a].value)
 								{
 									document.forms['customerFileForm'].elements['customerFile.personPayable'].options[a].selected="true";
 								}
 							}
 							}
		           		    
		           		    var testAuditor = res[3];
  							var z=document.getElementById("auditor")
  							var personAuditor = document.getElementById('auditor').selectedIndex;
  							if(personAuditor ==0)
  							{
 							for(var a = 0; a < z.length; a++)
 							{
 								if(testAuditor == document.forms['customerFileForm'].elements['customerFile.auditor'].options[a].value)
 								{
 									document.forms['customerFileForm'].elements['customerFile.auditor'].options[a].selected="true";
 								}
 							}
 							}
		           		
		           		}
		           		
           }
}

function handleHttpResponseCountryName(){
             if (http4.readyState == 4){
                var results = http4.responseText
                results = results.trim();
                var res=results.split('#');
                if(res.length>=1){
                	document.forms['customerFileForm'].elements['customerFile.originCountryCode'].value = res[0]; 
                	if(res[1]=='Y'){
                	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='Y';				
				 	}else{
				 	document.forms['customerFileForm'].elements['originCountryFlex3Value'].value='N';
				 	}	
                     
                 }
             }
}
     
function handleHttpResponseCopyBill(){
   if (http4.readyState == 4){
          var results = http4.responseText
          results = results.trim();
          
          if(results.length >=1){
            var code=document.forms['customerFileForm'].elements['customerFile.accountCode'].value;
            var name=document.forms['customerFileForm'].elements['customerFile.accountName'].value;
             if(code=="" || name==""){  
           	   document.forms['customerFileForm'].elements['customerFile.accountCode'].value = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	           document.forms['customerFileForm'].elements['customerFile.accountName'].value = document.forms['customerFileForm'].elements['customerFile.billToName'].value;
             }
          }
   }
}

function newFunctionForCountryState(){
var destinationAbc ='${customerFile.destinationCountryCode}';
var originAbc ='${customerFile.originCountryCode}';
if(originAbc == "USA" || originAbc == "IND" || originAbc == "CAN"){
document.forms['customerFileForm'].elements['customerFile.originState'].disabled =false;
getOriginStateReset(originAbc);
}
else{
document.forms['customerFileForm'].elements['customerFile.originState'].disabled =true;
}
if(destinationAbc == "USA" || destinationAbc == "IND" || destinationAbc == "CAN"){
document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =false;
getDestinationStateReset(destinationAbc);
}
else{
document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled =true;
}
}

    
function handleHttpResponseAcc(){
       if (http2.readyState == 4){
          var results = http2.responseText
          results = results.trim();
          var res = results.split("#");         		
           		
          if(res.length>= 2){ 
           		if(res[2] == 'Approved'){
           			if(res[3]== '' || res[3] == document.forms['customerFileForm'].elements['customerFile.companyDivision'].value || res[3] == 'null'){
           				document.forms['customerFileForm'].elements['customerFile.accountName'].value = res[1];
           				document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		           	}else{
		           			if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != ''){
		           				alert("Company Division of selected bill to code is not "+ document.forms['customerFileForm'].elements['customerFile.companyDivision'].value); 
		           			}else{
		           				alert('Please select company division.');
		           			} 
		           			document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
							document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
							document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		           }
           		}else{
           			alert("Account code is not approved" ); 
				    document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
					document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
					document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
           		}
           					
          }else{
               alert("Account code not valid");    
			   document.forms['customerFileForm'].elements['customerFile.accountCode'].value="";
			   document.forms['customerFileForm'].elements['customerFile.accountName'].value="";
			   document.forms['customerFileForm'].elements['customerFile.accountCode'].select();
		   }
       }
  }
  function handleHttpResponse3(){
		 if (http5.readyState == 4){
                var results = http5.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =res[i];
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =res[i];
					}
				}
         }
}

function handleHttpResponse5555555(){
		 if (http555.readyState == 4){
                var results = http555.responseText
                results = results.trim();
                res = results.replace("[",'');
                res = res.replace("]",'');
                res = res.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.contract'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value = '';
					}else{
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].text =res[i];
						document.forms['customerFileForm'].elements['customerFile.contract'].options[i].value =res[i];
					}
				}
         }
}

            
function handleHttpResponse4(){
		if (http2.readyState == 4) {
              var results = http2.responseText
              results = results.trim();
              var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
	           			document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value = res[1];
                   		document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
                	}else{
	           			alert("Booking Agent code is not approved" ); 
					    document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
	           		}  
                }else{
                     alert("Booking Agent code not valid");
                     document.forms['customerFileForm'].elements['customerFile.bookingAgentName'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value="";
					 document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].select();
               }
      }
} 
        
 function handleHttpResponse5(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("originState").value = '';
					}
					else{ document.getElementById("originState").value == '${customerFile.originState}';
					}
             }
        }  
   function handleHttpResponse555555(){
             if (httpState.readyState == 4){
                var results = httpState.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = '${customerFile.originState}';
             }
        }       
function handleHttpResponse6(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("destinationState").value = '';
					}
					else{ document.getElementById("destinationState").value == '${customerFile.destinationState}';
					   }
        }
}  

function handleHttpResponse6666666666(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = '${customerFile.destinationState}';
        }
}          


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
                  
        
	var http2 = getHTTPObject();
	var http50 = getHTTPObject50();
	var http51 = getHTTPObject51();
    var http3 = getHTTPObject1();
	var http4 = getHTTPObject3();
	var http5 = getHTTPObject();
	var http6 = getHTTPObject2();
    var http8 = getHTTPObject8();
     var http9 = getHTTPObject9();
    var httpState = getHTTPObjectState()
    var http22 = getHTTPObject22();
    var http33 = getHTTPObject33();
    var http444 = getHTTPObject33();
    var httpBilltoCode = getHTTPObjectBilltoCode();
    var httpPortalAccess = getHTTPObjectPortalAccess();
    function getHTTPObjectPortalAccess()
    {
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp)
            {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
function getHTTPObjectBilltoCode()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject22()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject50()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject51()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject33()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    function getHTTPObject9()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

 var http555 = getHTTPObject555();   
function getHTTPObject555()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    
    

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
  function getHTTPObjectState()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject2(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function getHTTPObject3(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
 function getHTTPObject8(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}    
function changeStatus(){
	document.forms['customerFileForm'].elements['formStatus'].value = '1';
}
function checkStatusSO(){
     	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
     	var oldStatus=document.forms['customerFileForm'].elements['oldCustStatus'].value; 
     	var sequenceNumberForStatus=document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
     	if(status=='CLOSED'||status=='REOPEN' || status=='HOLD'|| status=='CNCL' || status=='NEW'){
     	if(status=='CLOSED' ){
     		var url="getServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
     		http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseServiceOrderStatus; 
     		http5.send(null); 
       	}else if( status=='CNCL'){
         	var url="checkServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
       	 	http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseStatus; 
     		http5.send(null);  
       	} 
       	 else if( status=='HOLD'){
       	  if(oldStatus=='NEW'){
       	      document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=100;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to Hold");
         	}
       	}
       	else if(status=='REOPEN') {
       	  if(oldStatus=='HOLD'){
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               alert("You can't change current status to Reopen");
       	   } 
      	}
      	else if(status=='NEW'){
      	   if(oldStatus=='DWNLD'){
      	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to New");
         	}
      	   
      	   }  
      } 
     if(status!='CLOSED' && status!='REOPEN' && status!='HOLD' && status !='CNCL' && status!='NEW'){
     document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
     alert("You can select only Closed, Reopen, Hold, cancelled status");
     }
     coordinatorRequiredStatusJob();
}

function checkStatus(){
     	var status=document.forms['customerFileForm'].elements['customerFile.status'].value; 
     	var oldStatus=document.forms['customerFileForm'].elements['oldCustStatus'].value; 
     	var sequenceNumberForStatus=document.forms['customerFileForm'].elements['customerFile.sequenceNumber'].value;
     	if(status=='CLOSED'||status=='REOPEN' || status=='HOLD'|| status=='CNCL' || status=='NEW'){
     	if(status=='CLOSED' ){
     		var url="getServiceOrderStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
     		http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseServiceOrderStatus; 
     		http5.send(null); 
       	}else if( status=='CNCL'){
         	var url="checkWorkTicketStatus.html?ajax=1&decorator=simple&popup=true&sequenceNumberForStatus="+encodeURI(sequenceNumberForStatus);
       	 	http5.open("GET", url, true); 
     		http5.onreadystatechange = handleHttpResponseStatusWT; 
     		http5.send(null);  
       	} 
       	 else if( status=='HOLD'){
       	  if(oldStatus=='NEW'){
       	      document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=100;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to Hold");
         	}
       	}
       	else if(status=='REOPEN') {
       	  if(oldStatus=='HOLD'){
       	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	       autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	       cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               alert("You can't change current status to Reopen");
       	   } 
      	}
      	else if(status=='NEW'){
      	   if(oldStatus=='DWNLD'){
      	       document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=1;
       	      autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
       	      cheackStatusReason();
       	  } else {
       	       document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
                alert("You can't change current status to New");
         	}
      	   
      	   }  
      } 
     if(status!='CLOSED' && status!='REOPEN' && status!='HOLD' && status !='CNCL' && status!='NEW'){
     document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
     alert("You can select only Closed, Reopen, Hold, cancelled status");
     }
     coordinatorRequiredStatusJob();
}
     
function handleHttpResponseServiceOrderStatus(){
             if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
              	res = results.replace("[",'');
               res = res.replace("]",'');
               if(res!=''){ 
	               alert('File cannot be closed as all corresponding service order/s are not closed');
	               document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               }  else {
               document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
               autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
               
               }
             }
             
}
 
 function handleHttpResponseStatusWT(){
 if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
              	res = results.replace("[",'');
               res = res.replace("]",'');
               if(res!=''){ 
  					alert('Work Ticket is Open for this Customer File , Customer File cannot be cancelled');
	                document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
               }  else {
               checkStatusSO();
               }
          }
		}
 
function handleHttpResponseStatus(){
 if (http5.readyState == 4){
               var results = http5.responseText
               results = results.trim(); 
               res = results.replace("[",'');
               res = res.replace("]",'');
               if(res!=0){
               if(res==1){ 
                var agree = confirm("There is "+res+" Service Order not canceled for this Customer File. Do you want to cancel?.");
                } else {
                var agree = confirm("There are "+res+" Service Order/s not cancelled for this Customer File. Do you want to cancel all?.");
                }
	           if(agree){
	           document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
	           } else {
	            	// alert('File is not cancelled as all corresponding service order/s are not cancelled');
	                 document.forms['customerFileForm'].elements['customerFile.status'].value='${customerFile.status}';
	              } 
               }  else {
               document.forms['customerFileForm'].elements['customerFile.statusNumber'].value=500;
               cheackStatusReason();
               autoPopulate_customerFile_statusDate(document.forms['customerFileForm'].elements['customerFile.statusDate']);
               
               }
             }
}
     
function sendEmail1(target){
     	var originEmail = target;
   		var subject = 'C/F# ${customerFile.sequenceNumber}';
   		subject = subject+" ${customerFile.firstName}";
   		subject = subject+" ${customerFile.lastName}";
   		
		var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
		win = window.open(mailto_link,'emailWindow'); 
		if (win && win.open &&!win.closed) win.close(); 
}
     
function copyDestination2Email(){
    var EmailPort = document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value;
	if(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value==''){
		document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value=EmailPort;
	}   
}

function cheackStatusReason()
{   
      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.status'].value; 
      if(customerFileStatus=='CLOSED' || customerFileStatus=='CNCL') { 
        document.getElementById("hidStatusReg").style.display="block";
	    document.getElementById("hidStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidStatusReg").style.display="none";
        document.getElementById("hidStatusReg1").style.display="none";
         
     } 
}

function openStandardAddressesPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.originMobile&fld_ninthDescription=customerFile.originCity&fld_eigthDescription=customerFile.originFax&fld_seventhDescription=customerFile.originHomePhone&fld_sixthDescription=customerFile.originDayPhone&fld_fifthDescription=customerFile.originZip&fld_fourthDescription=stdAddOriginState&fld_thirdDescription=customerFile.originCountry&fld_secondDescription=customerFile.originAddress3&fld_description=customerFile.originAddress2&fld_code=customerFile.originAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="originAddSection";

}
function openStandardAddressesDestinationPopWindow(){
var jobType = document.forms['customerFileForm'].elements['customerFile.job'].value;
javascript:openWindow('standardaddresses.html?decorator=popup&popup=true&jobType='+jobType+'&fld_tenthDescription=customerFile.destinationMobile&fld_ninthDescription=customerFile.destinationCity&fld_eigthDescription=customerFile.destinationFax&fld_seventhDescription=customerFile.destinationHomePhone&fld_sixthDescription=customerFile.destinationDayPhone&fld_fifthDescription=customerFile.destinationZip&fld_fourthDescription=stdAddDestinState&fld_thirdDescription=customerFile.destinationCountry&fld_secondDescription=customerFile.destinationAddress3&fld_description=customerFile.destinationAddress2&fld_code=customerFile.destinationAddress1');
document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value="destinAddSection";
}

function enableState(){
var id=document.forms['customerFileForm'].elements['customerFile.id'].value;

var AddSection = document.forms['customerFileForm'].elements['checkConditionForOriginDestin'].value;
if(AddSection=='originAddSection'){
	if(id!=''){
	document.forms['customerFileForm'].originSoUpdate.disabled=true;
	document.forms['customerFileForm'].originSoTicketUpdate.disabled=true;
	}
	var originCountry = document.forms['customerFileForm'].elements['customerFile.originCountry'].value
	getOriginCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(originCountry=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse12;
     http9.send(null);

}
if(AddSection=='destinAddSection'){
if(id!=''){
document.forms['customerFileForm'].originSoDestinUpdate.disabled=true;
document.forms['customerFileForm'].originSoTicketDestinUpdate.disabled=true;
}
	var country = document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value
getDestinationCountryCode();
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http9.open("GET", url, true);
     http9.onreadystatechange = handleHttpResponse11;
     http9.send(null);

}
}


function handleHttpResponse11(){
		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.destinationState'];
                var stdAddDestinState=document.forms['customerFileForm'].elements['stdAddDestinState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.destinationState'].options[i].value = stateVal[0];
					
					if (document.getElementById("destinationState").options[i].value == '${customerFile.destinationState}'){
					   document.getElementById("destinationState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("destinationState").value = stdAddDestinState;
					if(document.forms['customerFileForm'].elements['customerFile.destinationCity'].value != ''){
			if(document.forms['customerFileForm'].elements['customerFile.destinationState'].value == ''){
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value;
			}else{
				document.forms['customerFileForm'].elements['customerFile.destinationCityCode'].value=document.forms['customerFileForm'].elements['customerFile.destinationCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.destinationState'].value;
			}
		}
        
        }
}  
function handleHttpResponse12(){
		
		if (http9.readyState == 4){
                var results = http9.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.originState'];
                var stdAddOriginState=document.forms['customerFileForm'].elements['stdAddOriginState'].value;
                targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.originState'].options[i].value = stateVal[0];
					
					if (document.getElementById("originState").options[i].value == '${customerFile.originState}'){
					   document.getElementById("originState").options[i].defaultSelected = true;
					}
					}
					}
					document.getElementById("originState").value = stdAddOriginState;
					if(document.forms['customerFileForm'].elements['customerFile.originCity'].value != '')
		{
			if(document.forms['customerFileForm'].elements['customerFile.originState'].value != '')
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value+', '+document.forms['customerFileForm'].elements['customerFile.originState'].value;
				
			}
			else
			{
				document.forms['customerFileForm'].elements['customerFile.originCityCode'].value=document.forms['customerFileForm'].elements['customerFile.originCity'].value;
				
			}
		}
        
        }
}
var http52 = getHTTPObject52();
	function getHTTPObject52()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http53 = getHTTPObject53();
	function getHTTPObject53()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http54 = getHTTPObject54();
	function getHTTPObject54()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
 var http55 = getHTTPObject55();
	function getHTTPObject55()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRef = getHTTPObjectRef();
function getHTTPObjectRef(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpRefSale = getHTTPObjectRefSale();
function getHTTPObjectRefSale(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getRefCoord(){
	if(document.forms['customerFileForm'].elements['customerFile.coordinator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findCoordRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRef.open("GET", url, true);
	     	httpRef.onreadystatechange = handleHttpResponseRef;
	     	httpRef.send(null);
	    } 
	}
}
function handleHttpResponseRef(){	
	if (httpRef.readyState == 4){
		   var cordVal = "";
           var results = httpRef.responseText
           results = results.trim();
           res = results.split("~");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
			}else{
				cordVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.coordinator'].value = cordVal[0];
			}		
		}
    }
}

function getRefSale(){
	if(document.forms['customerFileForm'].elements['customerFile.estimator'].value == ''){
		var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
	   	if(job!=''){
	     	var url = "findSaleRef.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
	     	httpRefSale.open("GET", url, true);
	     	httpRefSale.onreadystatechange = handleHttpResponseRefSale;
	     	httpRefSale.send(null);
	    } 
	}
}
function handleHttpResponseRefSale(){	
	if (httpRefSale.readyState == 4){
		   var saleVal = "";
           var results = httpRefSale.responseText
           results = results.trim();
           res = results.split("@");
           for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.estimator'].options[i].value = '';
			}else{
				saleVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.estimator'].value = saleVal[0];
			}
		}
    }
}
 
 
function getNewCoordAndSale(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
    var cfContractType="";
	var checkUTSI="${company.UTSI}";
	var checkSoFlag = false;
	var bookAgCode = document.forms['customerFileForm'].elements['customerFile.bookingAgentCode'].value;
   	if(job!=''){
     	var url = "findCoord.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job)+"&cfContractType="+encodeURI(cfContractType)+"&checkUTSI="+encodeURI(checkUTSI)+"&bookAgCode="+encodeURI(bookAgCode)+"&checkSoFlag="+encodeURI(checkSoFlag);
     	http50.open("GET", url, true);
     	http50.onreadystatechange = handleHttpResponse50;
     	http50.send(null);
    }  
}
function handleHttpResponse50(){	
		if (http50.readyState == 4){
                var results = http50.responseText
                results = results.trim();
                res = results.split("~");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.coordinator'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = '';
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].text = stateVal[1];
						document.forms['customerFileForm'].elements['customerFile.coordinator'].options[i].value = stateVal[0];
					}
				}
				document.getElementById("coordinator").value = '${customerFile.coordinator}';

				if(document.getElementById("coordinator").value == ''){
					getRefCoord();
				}
				if(document.getElementById("estimator").value == ''){
					getRefSale();
				}
				
				getPricing();
        }
}

function getPricing(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPricing.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http52.open("GET", url, true);
     	http52.onreadystatechange = handleHttpResponse52;
     	http52.send(null);
    }  
}
function handleHttpResponse52(){
		if (http52.readyState == 4){
                var results = http52.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPricing'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPricing'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPricing").value = '${customerFile.personPricing}';
        getBilling();
        }
  }
 
  function getBilling(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findBilling.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http53.open("GET", url, true);
     	http53.onreadystatechange = handleHttpResponse53;
     	http53.send(null);
    }
    
}
function handleHttpResponse53(){
		
		if (http53.readyState == 4){
                var results = http53.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personBilling'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personBilling'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personBilling").value = '${customerFile.personBilling}';
        getApprovedBy();
        }
  }
  
  function getPayable(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findPayable.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http54.open("GET", url, true);
     	http54.onreadystatechange = handleHttpResponse54;
     	http54.send(null);
    }
    
}
function handleHttpResponse54(){
		
		if (http54.readyState == 4){
                var results = http54.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.personPayable'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.personPayable'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("personPayable").value = '${customerFile.personPayable}';
          }
  }
   function getApprovedBy(){
    var job = document.forms['customerFileForm'].elements['customerFile.job'].value;
   	if(job!=''){
     	var url = "findApprovedBy.html?ajax=1&decorator=simple&popup=true&custJobType="+encodeURI(job);
     	http55.open("GET", url, true);
     	http55.onreadystatechange = handleHttpResponse55;
     	http55.send(null);
    }
    
}
function handleHttpResponse55(){
		
		if (http55.readyState == 4){
                var results = http55.responseText
                results = results.trim();
                res = results.split("@");
              	targetElement = document.forms['customerFileForm'].elements['customerFile.approvedBy'];
               	targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.approvedBy'].options[i].value = stateVal[0];
					}
					}
					document.getElementById("approvedBy").value = '${customerFile.approvedBy}';
        getPayable();
        }
  }
  
var httpJob = getHTTPObjectJob();
function getHTTPObjectJob(){
    var xmlhttp;
    if(window.XMLHttpRequest)    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
  
function getJobList(){
	var comDiv = "";
	if(document.forms['customerFileForm'].elements['customerFile.companyDivision'].value != null){
		comDiv = document.forms['customerFileForm'].elements['customerFile.companyDivision'].value;
	}
  	var url = "findJobs.html?ajax=1&decorator=simple&popup=true&compDivision="+encodeURI(comDiv);
   	httpJob.open("GET", url, true);
   	httpJob.onreadystatechange = handleHttpResponseJob;
   	httpJob.send(null);
}
function handleHttpResponseJob(){
	if (httpJob.readyState == 4){
    	var results = httpJob.responseText
        results = results.trim();
        res = results.split("@");
        targetElement = document.forms['customerFileForm'].elements['customerFile.job'];
        targetElement.length = res.length;
		for(i=0;i<res.length;i++){
			if(res[i] == ''){
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = '';
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = '';
			}else{
				stateVal = res[i].split("#");
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].text = stateVal[1];
				document.forms['customerFileForm'].elements['customerFile.job'].options[i].value = stateVal[0];
			}
		}
		document.getElementById("job").value = '${customerFile.job}';
	}
}  


function coordinatorRequiredStatusJob()
{
      var jobNote=document.forms['customerFileForm'].elements['customerFile.job'].value;
      var statusNote=document.forms['customerFileForm'].elements['customerFile.status'].value;      
		var el = document.getElementById('coordRequiredTrue');
		var el1 = document.getElementById('coordRequiredFalse');
		if(jobNote == 'INT' || jobNote == 'GST' || statusNote == 'STORG' || statusNote == 'DELIV'|| statusNote == 'CLAIM'|| statusNote == 'ACTIVE'|| statusNote == 'DWNLD'){
		el.style.display = 'block';		
		el1.style.display = 'none';		
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		}
}
function zipCode(){
        //var originCountryCode=document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
		//var el = document.getElementById('zipCodeRequiredTrue');
		//var el1 = document.getElementById('zipCodeRequiredFalse');
		///if(originCountryCode == 'United States'){
		//el.style.display = 'block';		
		//el1.style.display = 'none';		
		//}else{
		//el.style.display = 'none';
		//el1.style.display = 'block';
}
function zipCodeDestin(){
       // var destinCountryCode=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;      
		//var el = document.getElementById('zipCodeDestinRequiredTrue');
		//var el1 = document.getElementById('zipCodeDestinRequiredFalse');
		//if(destinCountryCode == 'United States'){
		//el.style.display = 'block';		
		//el1.style.display = 'none';		
		//}else{
		//el.style.display = 'none';
		//el1.style.display = 'block';
}

function checkAllRelo(){ 
var len = document.forms['customerFileForm'].elements['checkV'].length;
var check=true;
for (i = 0; i < len; i++){ 
        if(document.forms['customerFileForm'].elements['checkV'][i].checked == false ){
        check=false;
        i=len+1
        }
    } 
    if(check){
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=true;
    }else{
    document.forms['customerFileForm'].elements['checkAllRelo'].checked=false;
    }
}

function checkAll(temp){
    document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = "";
    var len = document.forms['customerFileForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){

        document.forms['customerFileForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['customerFileForm'].elements['checkV'][i]);
    }
    }
}   


</script>
<script type="text/javascript">
animatedcollapse.addDiv('address', 'fade=1,hide=0,show=1' )
animatedcollapse.addDiv('transferee', 'fade=1,hide=0,show=1' )
animatedcollapse.addDiv('alternative', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('billing', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('entitlement', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('cportal', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('family', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('benefits', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('hr', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('removal', 'fade=1,hide=0,show=1')
animatedcollapse.addDiv('additional', 'fade=1,hide=1')

animatedcollapse.init()

function massageAddress(){
	 alert("Please Save Order Initiation Detail first before adding Additional address.");
}

function cheackVisaRequired()
{   

      var customerFileStatus = document.forms['customerFileForm'].elements['customerFile.visaRequired'].value; 
      if(customerFileStatus=='Yes' ) { 
        document.getElementById("hidVisaStatusReg").style.display="block";
	    document.getElementById("hidVisaStatusReg1").style.display="block"; 
	 }else{
	    document.getElementById("hidVisaStatusReg").style.display="none";
        document.getElementById("hidVisaStatusReg1").style.display="none";
         
     } 
}

function cheackQuotationStatus(temp)
{  
     
}

function changeQuotationStatus(){
quotationStatus = document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value;
quotationStatus=quotationStatus.trim();
if(quotationStatus=='Accepted'){
document.forms['customerFileForm'].elements['customerFile.controlFlag'].value='C';
document.forms['customerFileForm'].elements['customerFile.moveType'].value='BookedMove';
} else {
document.forms['customerFileForm'].elements['customerFile.controlFlag'].value='A';
}
if(quotationStatus=='Rejected'){
	document.forms['customerFileForm'].elements['customerFile.orderIntiationStatus'].value='Rejected';
}
}
function selectColumn(targetElement) 
   {  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' + rowId;
      document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;
     var userCheckStatus=document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( rowId , '' );
     document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus.replace( '##' , '#' );
     } 
    }


function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value; 
if(userCheckStatus!=""){ 
document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value="";
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value;  
     var userCheckStatus=	document.forms['customerFileForm'].elements['customerFile.serviceRelo'].value = userCheckStatus + '#' +column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}
function updateOrderManagementStatus(temp) { 
     var quotationStatus=temp;
     var orderAction= document.forms['customerFileForm'].elements['customerFile.orderAction'].value; 
     var id=document.forms['customerFileForm'].elements['customerFile.id'].value;
     window.location.href="updateOrderManagementStatus.html?id="+id+"&orderAction="+orderAction+"&quotationStatus="+quotationStatus;
      
 }   
function checkAssignmentType(){ 
 if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=="Other"){ 
 document.getElementById('otherContract').style.display = 'block'; 
 }else
 {
 document.getElementById('otherContract').style.display ='none'; 
 } 
 }
 
 function autoPopulate_customerFile_homeCountry(targetElement) {   
	
		var dCountry = targetElement.value;
		var dCountryCode = '';
		if(dCountry == 'United States')
		{
			dCountryCode = "USA";
		}
		if(dCountry == 'India')
		{
			dCountryCode = "IND";
		}
		if(dCountry == 'Canada')
		{
			dCountryCode = "CAN";
		}
		if(dCountry == 'United States' || dCountry == 'Canada' || dCountry == 'India' ){
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
			//document.getElementById('destinationStateRequired').style.display = 'block';
		    //document.getElementById('destinationStateNotRequired').style.display = 'none';
			if(dCountry == 'United States'){
			//document.forms['customerFileForm'].elements['customerFile.destinationZip'].focus(); 
			}
		}else{
			document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
			document.forms['customerFileForm'].elements['customerFile.homeState'].value = '';
			//document.getElementById('destinationStateRequired').style.display = 'none';
		    //document.getElementById('destinationStateNotRequired').style.display = 'block';
			//autoPopulate_customerFile_destinationCityCode(dCountryCode,'special');
		}
	} 

function gethomeState(targetElement){
	var country = targetElement.value;
	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponsehomeState;
     http3.send(null);
}	

function handleHttpResponsehomeState(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['customerFileForm'].elements['customerFile.homeState'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = '';
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = '';
					}else{
					stateVal = res[i].split("#");
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].text = stateVal[1];
					document.forms['customerFileForm'].elements['customerFile.homeState'].options[i].value = stateVal[0];
					
					if (document.getElementById("homeState").options[i].value == '${customerFile.homeState}'){
					   document.getElementById("homeState").options[i].defaultSelected = true;
					}
					}
					}
					if ('${stateshitFlag}'=="1"){
					document.getElementById("homeState").value = '';
					}
					else{ document.getElementById("homeState").value == '${customerFile.homeState}';
					   }
        }
}

function openHomeCountryLocation(){
	    var city = document.forms['customerFileForm'].elements['customerFile.homeCity'].value;
        var country = document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
        var zip = "";
        var address = "";
        address = address.replace("#","-");
        var state = "";
       
 		window.open('http://maps.google.com/maps?f=q&hl=en&geocode=&q='+address+','+city+','+zip+','+state+','+country);
	}
	
function setContractValidFlag(temp){
document.forms['customerFileForm'].elements['contractValidFlag'].value=temp;
}

function qryHowOld(varAsOfDate, varBirthDate)
   {
   var dtAsOfDate;
   var dtBirth;
   var dtAnniversary;
   var intSpan;
   var intYears;
   var intMonths;
   var intWeeks;
   var intDays;
   var intHours;
   var intMinutes;
   var intSeconds;
   var strHowOld;

   // get born date
   dtBirth = new Date(varBirthDate);
   
   // get as of date
   dtAsOfDate = new Date(varAsOfDate);

   // if as of date is on or after born date
   if ( dtAsOfDate >= dtBirth )
      {

      // get time span between as of time and birth time
      intSpan = ( dtAsOfDate.getUTCHours() * 3600000 +
                  dtAsOfDate.getUTCMinutes() * 60000 +
                  dtAsOfDate.getUTCSeconds() * 1000    ) -
                ( dtBirth.getUTCHours() * 3600000 +
                  dtBirth.getUTCMinutes() * 60000 +
                  dtBirth.getUTCSeconds() * 1000       )

      // start at as of date and look backwards for anniversary 

      // if as of day (date) is after birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is on or after birth time
      if ( dtAsOfDate.getUTCDate() > dtBirth.getUTCDate() ||
           ( dtAsOfDate.getUTCDate() == dtBirth.getUTCDate() && intSpan >= 0 ) )
         {

         // most recent day (date) anniversary is in as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth(),
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         }

      // if as of day (date) is before birth day (date) or
      //    as of day (date) is birth day (date) and
      //    as of time is before birth time
      else
         {

         // most recent day (date) anniversary is in month before as of month
         dtAnniversary = 
            new Date( Date.UTC( dtAsOfDate.getUTCFullYear(),
                                dtAsOfDate.getUTCMonth() - 1,
                                dtBirth.getUTCDate(),
                                dtBirth.getUTCHours(),
                                dtBirth.getUTCMinutes(),
                                dtBirth.getUTCSeconds() ) );

         // get previous month
         intMonths = dtAsOfDate.getUTCMonth() - 1;
         if ( intMonths == -1 )
            intMonths = 11;

         // while month is not what it is supposed to be (it will be higher)
         while ( dtAnniversary.getUTCMonth() != intMonths )

            // move back one day
            dtAnniversary.setUTCDate( dtAnniversary.getUTCDate() - 1 );

         }

      // if anniversary month is on or after birth month
      if ( dtAnniversary.getUTCMonth() >= dtBirth.getUTCMonth() )
         {

         // months elapsed is anniversary month - birth month
         intMonths = dtAnniversary.getUTCMonth() - dtBirth.getUTCMonth();

         // years elapsed is anniversary year - birth year
         intYears = dtAnniversary.getUTCFullYear() - dtBirth.getUTCFullYear();

         }

      // if birth month is after anniversary month
      else
         {

         // months elapsed is months left in birth year + anniversary month
         intMonths = (11 - dtBirth.getUTCMonth()) + dtAnniversary.getUTCMonth() + 1;

         // years elapsed is year before anniversary year - birth year
         intYears = (dtAnniversary.getUTCFullYear() - 1) - dtBirth.getUTCFullYear();

         }

      // to calculate weeks, days, hours, minutes and seconds
      // we can take the difference from anniversary date and as of date

      // get time span between two dates in milliseconds
      intSpan = dtAsOfDate - dtAnniversary;

      // get number of weeks
     // intWeeks = Math.floor(intSpan / 604800000);

      // subtract weeks from time span
    //  intSpan = intSpan - (intWeeks * 604800000);
      
      // get number of days
      intDays = Math.floor(intSpan / 86400000);

      // subtract days from time span
      intSpan = intSpan - (intDays * 86400000);

      // get number of hours
      intHours = Math.floor(intSpan / 3600000);
    
      // subtract hours from time span
      intSpan = intSpan - (intHours * 3600000);

      // get number of minutes
      intMinutes = Math.floor(intSpan / 60000);

      // subtract minutes from time span
      intSpan = intSpan - (intMinutes * 60000);

      // get number of seconds
      intSeconds = Math.floor(intSpan / 1000);

      // create output string     
      if ( intYears >= 0 )
         if ( intYears > 1 )
            strHowOld = intYears.toString() + 'Y';
         else
            strHowOld = intYears.toString() + 'Y';
      else
         strHowOld = '';

      if ( intMonths > 0 )
         if ( intMonths > 1 )
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
         else
            strHowOld = strHowOld + '-' + intMonths.toString() + 'M';
           
     // if ( intWeeks > 0 )
      //   if ( intWeeks > 1 )
       //     strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';
       //  else
        //    strHowOld = strHowOld + ' ' + intWeeks.toString() + ' W';

      if ( intDays > 0 )
         if ( intDays > 1 )
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
         else
            strHowOld = strHowOld + '-' + intDays.toString() + 'D';
      }
   else
      strHowOld = ''

   // return string representation
   return strHowOld
   }   		
function calcDays() {
 document.forms['customerFileForm'].elements['customerFile.duration'].value="";
 var date2 = document.forms['customerFileForm'].elements['customerFile.contractStart'].value;	 
 var date1 = document.forms['customerFileForm'].elements['customerFile.contractEnd'].value; 
  var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = document.forms['customerFileForm'].elements['contractEndYear'].value; 
   if(year==''){var yearbreak='${customerFile.contractEnd}';
                  yearbreak=yearbreak.split("-");year=yearbreak[0];       
        }
  if(month == 'Jan')
   {
       month = "01";
   } else if(month == 'Feb')
   {
       month = "02";
   } else if(month == 'Mar')
   {
       month = "03"
   } else if(month == 'Apr')
   {
       month = "04"
   }  else if(month == 'May')
   {
       month = "05"
   } else if(month == 'Jun')
   {
       month = "06"
   }  else if(month == 'Jul')
   {
       month = "07"
   } else if(month == 'Aug')
   {
       month = "08"
   }  else if(month == 'Sep')
   {
       month = "09"
   }  else if(month == 'Oct')
   {
       month = "10"
   }  else if(month == 'Nov')
   {
       month = "11"
   }  else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = document.forms['customerFileForm'].elements['contractStartYear'].value;
    if(year2==''){var yearbreak='${customerFile.contractStart}';
                  yearbreak=yearbreak.split("-");year2=yearbreak[0];       
        }
   if(month2 == 'Jan')
   {
       month2 = "01";
   }  else if(month2 == 'Feb')
   {
       month2 = "02";
   }  else if(month2 == 'Mar')
   {
       month2 = "03"
   }  else if(month2 == 'Apr')
   {
       month2 = "04"
   }  else if(month2 == 'May')
   {
       month2 = "05"
   }  else if(month2 == 'Jun')
   {
       month2 = "06"
   }   else if(month2 == 'Jul')
   {
       month2 = "07"
   }  else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }  else if(month2 == 'Oct')
   {
       month2 = "10"
   }  else if(month2 == 'Nov')
   {
       month2 = "11"
   }  else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var diff = Math.round((sDate-eDate)/86400000);
  var  daysApart=0;
  if(diff<0)
  {
    alert("Contract Start Date must be less than Contract End Date");
    document.forms['customerFileForm'].elements['customerFile.duration'].value='';  
    document.forms['customerFileForm'].elements['customerFile.contractEnd'].value=""; 
    }else{
  daysApart=qryHowOld(sDate, eDate);
  }
  //var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['customerFileForm'].elements['customerFile.duration'].value = daysApart; 

  if(document.forms['customerFileForm'].elements['customerFile.duration'].value=='NaN')
   {
     document.forms['customerFileForm'].elements['customerFile.duration'].value = '';
   } 
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value = '';
}

function forDays(){
 document.forms['customerFileForm'].elements['orderInitiationASMLForDaysClick'].value ='1';
} 
function saveValidation(){
	var newPage= document.forms['customerFileForm'].elements['newPage'].value;
	 if(newPage=='N')
		{
     submitRelo();
		}	
	else 
		{
     submitReloNew();
		} }


function submitRelo()
{ 
if(document.forms['customerFileForm'].elements['parentAgentForOrderinitiation'].value=='true'){
document.forms['customerFileForm'].elements['removalRelocationService.isIta60'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta60'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta2m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta2m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta1m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta1m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isIta3m3'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isIta3m3'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isTransportAllow'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isTransportAllow'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isStorage'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isStorage'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isUsdollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isUsdollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isDollar'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isDollar'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOtherCurrency'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOtherCurrency'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.usdollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.usdollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.dollarAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.dollarAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherCurrencyAmounts'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherCurrencyAmounts'].value;
document.forms['customerFileForm'].elements['removalRelocationService.otherServicesRequired'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.otherServicesRequired'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.transportAllowExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.orientationTrip'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.orientationTrip'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.authorizedDays'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.authorizedDays'].value;
document.forms['customerFileForm'].elements['removalRelocationService.additionalNotes'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.additionalNotes'].value;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;

}
}
function submitReloNew()
{ 
if(document.forms['customerFileForm'].elements['parentAgentForOrderinitiation'].value=='true'){
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtExpat40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtExpat40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isKgExpat30'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isKgExpat30'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtSingle20'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtSingle20'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isFtFamily40'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isFtFamily40'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isOrientation24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isOrientation24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeFurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isHomeUnfurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isHomeUnfurnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSchoolSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSchoolSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch8'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch8'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch16'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch16'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.isSelfInSearch24'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.isSelfInSearch24'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.id'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.id'].value;
document.forms['customerFileForm'].elements['removalRelocationService.customerFileId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.customerFileId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.corpId'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.corpId'].value;
document.forms['customerFileForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.ftExpat40ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.kgExpat30ExceptionalItems'].value;
document.forms['customerFileForm'].elements['removalRelocationService.homeSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.homeSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.settlingInAssistance'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.settlingInAssistance'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.schoolSearch'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.schoolSearch'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.furnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.furnished'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.unFurnished'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.unFurnished'].checked;
<configByCorp:fieldVisibility componentId="component.field.removalRelocationSer.immigration">
document.forms['customerFileForm'].elements['removalRelocationService.entryVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.entryVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.longTermStayVisa'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.longTermStayVisa'].checked;
document.forms['customerFileForm'].elements['removalRelocationService.workPermit'].value =window.frames['relo'].document.forms['removalRelocationServiceForm'].elements['removalRelocationService.workPermit'].checked;
</configByCorp:fieldVisibility>
}}

function getHomeCountryCode(){
	var countryName=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	var url="countryCodeAndFlexValue.html?ajax=1&decorator=simple&popup=true&countryName=" + encodeURI(countryName);
    http444.open("GET", url, true);
    http444.onreadystatechange = httpHomeCountryName;
    http444.send(null);
}
function httpHomeCountryName(){
     if (http444.readyState == 4){
                var results = http444.responseText
                results = results.trim();
                var res = results.split("#");
                if(res.length>=1){
 					document.forms['customerFileForm'].elements['customerFile.homeCountryCode'].value = res[0];
 					if(res[1]=='Y'){
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='Y';
 					}else{
 					document.forms['customerFileForm'].elements['HomeCountryFlex3Value'].value='N';
 					}                     
                 }
             }
}
	
function emailValidate(form_id,email,message) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   var address = document.forms[form_id].elements[email].value;
    if(address!=''){
   if(reg.test(address) == false) {
      alert('Invalid '+message+' Email Address');
      document.forms[form_id].elements[email].value='';
      return false;
      }
    }  
}

function autoPopulateSecondryEmail(){
  if(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value == '')
     {
        document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value=document.forms['customerFileForm'].elements['customerFile.email2'].value;
     }
}

function checkPetInvolve(result){
if(result.checked==true){
document.getElementById('petTypeId').style.display ='block';
}if(result.checked!=true){document.getElementById('petTypeId').style.display ='none';
}
}
function savevalidation(){
	getValue3();  
	var contractType = document.forms['customerFileForm'].elements['customerFile.assignmentType'].value;
	var contractTypeFlag=false;
	<configByCorp:fieldVisibility componentId="component.customerfile.field.contractTypeFlag">
	contractTypeFlag = true;
	</configByCorp:fieldVisibility>
	if(contractType == " " && contractTypeFlag){
		alert("Type of Contract is Required Field.");
		return false;
	}
	if(document.forms['customerFileForm'].elements['customerFile.petsInvolved'].checked==true){
		if(document.forms['customerFileForm'].elements['customerFile.petType'].value==""){
			alert("Type Of Pet is a required field in Entitlement");
			return false;
		}
	}
}
function enableStateListOrigin(){ 
	var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(oriCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.originState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.originState'].disabled = true;
	  }	        
}
function enableStateListDestin(){ 
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(desCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.destinationState'].disabled = true;
	  }	        
}
function enableStateListHome(){ 
	var homCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
	  var enbState = '${enbState}';
	  var index = (enbState.indexOf(homCon)> -1);
	  if(index != ''){
	  		document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = false;
	  }else{
		  document.forms['customerFileForm'].elements['customerFile.homeState'].disabled = true;
	  }	        
}
function findAssignmentFromParentAgent(){	
	   var bCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value; 
	   if(bCode!='') { 
		   var url="findAssignment.html?ajax=1&decorator=simple&popup=true&bCode="+encodeURI(bCode);
		   http51.open("GET", url, true); 
		   http51.onreadystatechange = handleHttp90; 
		   http51.send(null); 
	   }
	 }
function handleHttp90(){
	 if (http51.readyState == 4){	 
	         var results = http51.responseText
	         results = results.trim();
	         if(results.length >= 1){
	         	var res = results.split("@"); 
		         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
						targetElement.length = res.length;
						for(i=0;i<res.length;i++){
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].text = res[i];
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[i].value = res[i];
							if(document.getElementById("assignmentType").options[i].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							}
						} 
		 		}else {
			 		var j=1;
			         var targetElement = document.forms['customerFileForm'].elements['customerFile.assignmentType'];
			         var len=null;

			         <c:if test="${parentAgentForOrderinitiation=='true'}">
			        	 len='${fn1:length(assignmentTypesASML)}';
							targetElement.length = parseInt(len)+1;
				 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
							<c:forEach var="assignment1" items="${assignmentTypesASML}" varStatus="loopStatus">
				 	       	var entKey = "<c:out value="${assignment1.key}"/>";
				 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
				 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text = entvalue;
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value = entKey;
							if(document.getElementById("assignmentTypesAS").options[j].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							}
							j++;
			 	 		</c:forEach>			        	 
			         </c:if>
			         <c:if test="${parentAgentForOrderinitiation!='true'}">
			       		  len='${fn1:length(assignmentTypes)}';
							targetElement.length = parseInt(len)+1;
				 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].text = ' ';
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[0].value = ' ';
							<c:forEach var="assignment1" items="${assignmentTypes}" varStatus="loopStatus">
				 	       	var entKey = "<c:out value="${assignment1.key}"/>";
				 	      	var entvalue = "<c:out value="${assignment1.value}"/>";
				 	     	document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].text =entvalue ;
							document.forms['customerFileForm'].elements['customerFile.assignmentType'].options[j].value = entKey ;
							if(document.getElementById("assignmentType").options[j].value=='${customerFile.assignmentType}'){
								document.forms['customerFileForm'].elements['customerFile.assignmentType'].value='${customerFile.assignmentType}';
							}
							j++;
			 	 		</c:forEach>
			         </c:if>
		 		}
	 		}
		}
var http51 = getHTTPObject();
function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
  xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  if (!xmlhttp)
  {
      xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
  }
}
return xmlhttp;
}

function stateOrigin(){
    var originCountry=document.forms['customerFileForm'].elements['customerFile.originCountry'].value; 
	var os1 = document.getElementById('originStateRequired');
	var os2 = document.getElementById('originStateNotRequired');
	if(originCountry == 'United States'){
	os1.style.display = 'block';		
	os2.style.display = 'none';	
	}else{
	os1.style.display = 'none';
	os2.style.display = 'block';
	}
}
function stateDestination(){
    var originCountry=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value; 
	var os1 = document.getElementById('destinationStateRequired');
	var os2 = document.getElementById('destinationStateNotRequired');
	if(originCountry == 'United States'){
	os1.style.display = 'block';		
	os2.style.display = 'none';	
	}else{
	os1.style.display = 'none';
	os2.style.display = 'block';
	}
}
</script>
<script type="text/javascript"> 
function titleCase(element){ 
	var txt=element.value; var spl=txt.split(" "); 
	var upstring=""; for(var i=0;i<spl.length;i++){ 
	try{ 
	upstring+=spl[i].charAt(0).toUpperCase(); 
	}catch(err){} 
	upstring+=spl[i].substring(1, spl[i].length); 
	upstring+=" ";   
	} 
	element.value=upstring.substring(0,upstring.length-1); 
}
</script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>
<script type="text/JavaScript">
function callPostalCode(){
	window.open('http://www.canadapost.ca/cpo/mc/personal/postalcode/fpc.jsf', '_blank');
}
</script> 

</head> 
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="customerFileForm" name="customerFileForm" action="saveOrderManagement" onsubmit="saveValidation();return submit_form();" method="post" validate="true" >
<s:hidden name="customerFile.partnerEntitle"/>
<c:set var="soCoordinator" value="${fn:trim(customerFile.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(userName)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="passportNumberEncrypt" value="false"/>
<c:if test="${appUserName==soCoordinator}">
<c:set var="passportNumberEncrypt" value="true"/>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.familydetails.passport.encrypted">
<c:set var="passportNumberEncryptShow" value="Y"/>
</configByCorp:fieldVisibility>
<c:set var="rolesAssignFlag" value="N" />
<c:forEach var="item" items="${roles}">
  <c:if test="${item eq 'ROLE_SUPERVISOR' || item eq 'ROLE_ADMIN'}">
    <c:set var="rolesAssignFlag" value="Y" />
  </c:if>
</c:forEach>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%;">
        <s:hidden  name="customerFile.homeCountryCode" />
        <s:hidden name="customerFile.isNetworkRecord" />
	    <s:hidden name="HomeCountryFlex3Value" id="HomeCountryFlex3Value" />
		<s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
		 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
		 <s:hidden name="contractValidFlag" id="contractValidFlag" value="Y"/>
		<s:hidden name="contractStartYear" id="contractStartYear"/>
		<s:hidden name="parentAgentForOrderinitiation" id="parentAgentForOrderinitiation"/>
		<s:hidden name="contractEndYear" id="contractEndYear"/>
		<s:hidden name="orderInitiationASMLForDaysClick" id="orderInitiationASMLForDaysClick"/>
		<s:hidden name="customerFile.id" value="%{customerFile.id}" /> 
		<s:hidden name="custJobType" value="<%=request.getParameter("custJobType") %>" />
		<s:hidden name="contractBillCode" value="<%=request.getParameter("contractBillCode") %>" />
		<s:hidden name="custCreatedOn" value="<%=request.getParameter("custCreatedOn") %>" />
		<s:hidden name="custStatus"/>
		<s:hidden name="oldCustStatus" value="${customerFile.status}"/>
		<s:hidden name="customerFile.corpID" />
	    <s:hidden name="stdAddDestinState" />
	    <s:hidden name="stdAddOriginState" />
	    <s:hidden name="checkConditionForOriginDestin" />
		<s:hidden name="id" value="<%=request.getParameter("id") %>" /> 
		<s:hidden name="gotoPageString" id="gotoPageString" value="" />
		<s:hidden name="idOfWhom" value="${customerFile.id}" /> 
		<s:hidden id="countNotes" name="countNotes" value="<%=request.getParameter("countNotes") %>"/>
		<s:hidden id="countOriginNotes" name="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>"/>
		<s:hidden id="countDestinationNotes" name="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>"/>
		<s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
		<s:hidden id="countVipNotes" name="countVipNotes" value="<%=request.getParameter("countVipNotes") %>"/>
		<s:hidden id="countBillingNotes" name="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<s:hidden id="countSpouseNotes" name="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<s:hidden id="countContactNotes" name="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<s:hidden id="countCportalNotes" name="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<s:hidden id="countEntitlementNotes" name="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<c:set var="countNotes" value="<%=request.getParameter("countNotes") %>" />
		<c:set var="countOriginNotes" value="<%=request.getParameter("countOriginNotes") %>" />
		<c:set var="countDestinationNotes" value="<%=request.getParameter("countDestinationNotes") %>" />
		<c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
		<c:set var="countVipNotes" value="<%=request.getParameter("countVipNotes") %>" />
		<c:set var="countBillingNotes" value="<%=request.getParameter("countBillingNotes") %>"/>
		<c:set var="countSpouseNotes" value="<%=request.getParameter("countSpouseNotes") %>"/>
		<c:set var="countContactNotes" value="<%=request.getParameter("countContactNotes") %>"/>
		<c:set var="countCportalNotes" value="<%=request.getParameter("countCportalNotes") %>"/>
		<c:set var="countEntitlementNotes" value="<%=request.getParameter("countEntitlementNotes") %>"/>
		<s:hidden name="dCountry" />
		<s:hidden name="oCountry" /> 
		<s:hidden id="countDSNotes" name="countDSNotes" value="<%=request.getParameter("countDSNotes") %>"/>
		<c:set var="countDSNotes" value="<%=request.getParameter("countDSNotes") %>" /> 
		<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	    <s:hidden name="formStatus" value=""/> 
	    <s:hidden  name="customerFile.orderIntiationStatus"  />   
	    <s:hidden  name="customerFile.quotationStatus"  />   
	    <s:hidden name="customerFile.parentAgent" />
	     <s:hidden name="emailTypeFlag" />	
	     <s:hidden name="emailTypeVOERFlag" />
	     <s:hidden name="customerFile.moveType" />
	<c:choose>
			<c:when test="${gotoPageString == 'gototab.serviceorder' }">
				<c:redirect url="/customerServiceOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.raterequest' }">
				<c:redirect url="/customerRateOrders.html?id=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.surveys' }">
				<c:redirect url="/surveysList.html?id1=${customerFile.id}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.accountpolicy' }">
				<c:redirect url="/showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" />
			</c:when>
			<c:when test="${gotoPageString == 'gototab.imfEntitlements' }">
				<c:redirect url="/editImfEntitlement.html?cid=${customerFile.id}" />
			</c:when> 
			<c:otherwise>
			</c:otherwise>
		</c:choose> 
		<c:set var="from" value="<%=request.getParameter("from") %>"/>
		<c:set var="field" value="<%=request.getParameter("field") %>"/>
		<s:hidden name="field" value="<%=request.getParameter("field") %>" />
		<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
		<c:set var="field1" value="<%=request.getParameter("field1") %>"/> 
	    <s:hidden name="jobNumber" value="%{customerFile.sequenceNumber}"/>
		<c:set var="jobNumber" value="<%=request.getParameter("sequenceNumber") %>" />
		<s:set name="reportss" value="reportss" scope="request"/>

	<c:if test="${not empty customerFile.id}">
		<div id="newmnav">
		  <ul> 
		    <li id="newmnav1" style="background:#FFF "><a href="editOrderManagement.html?id=${customerFile.id}" class="current"><span>Order Detail</span></a></li>
		     <li><a href="orderManagements.html" /><span>Order List</span></a></li>
		 </div><div class="spn">&nbsp;</div>
		<div style="padding-bottom:0px;"></div>
	</c:if>	
	<c:if test="${empty customerFile.id}">
		<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a href="" class="current"><span>Order Detail</span></a></li>
		    <li><a href="orderManagements.html" /><span>Order List</span></a></li>
		 </div><div class="spn">&nbsp;</div>
	</c:if> 
	

<div id="content" align="center">
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tbody>
		<tr>
         <td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('address')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Order Instructions
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
				 </div>
				 <div id="Initiation" class="switchgroup1">
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					 <tbody>
					 <tr><td height="10px"></td></tr>
					 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
					 <tr>
					  <td align="right" width="110px" class="listwhitetext" valign="bottom">Action Requested<font color="red" size="2">*&nbsp;</font></td>
					  <td width=""></td>
					  <td width="30px"></td>
					  <td width=""></td>
					  
					  </tr>
					   
					  <c:choose>
								<c:when test="${customerFile.orderAction == 'IO'}" >
						  		 <tr>
						  			<td></td>
						  			<td class="listwhitetext" width="80px">Order Initiation</td>
						  			<td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="IO"  tabindex="1"></td>
						  			<td></td>
						  			
						  		 </tr>
						  		 <tr>
						  		 <td></td>
						  		 <td class="listwhitetext">Quote Request</td>
						  		 <td><INPUT type="radio" name="customerFile.orderAction"  value="QR"  tabindex="2"></td> 
						  		 <td id="hidIntiationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Order" name="acceptOrder" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderManagementStatus('Accepted')"/> 
						  		 </td>
						  		 </tr>
						  		</c:when>
						  		<c:when test="${customerFile.orderAction == 'QR'}" >
						  		 <tr>
						  		   <td></td>
						  		   <td class="listwhitetext" width="80px">Order Initiation</td>
						  		   <td><INPUT type="radio" name="customerFile.orderAction"  value="IO"   tabindex="1"></td>
						  		   <td></td>
						  		   
						  		  </tr>
						  		  <tr>
						  		  <td></td>
						  		  <td class="listwhitetext">Quote Request</td>
						  		  <td><INPUT type="radio" name="customerFile.orderAction" checked="checked" value="QR"  tabindex="2"></td> 
						  		  <td id="hidIntiationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Order" name="acceptOrder" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderManagementStatus('Accepted')"/> 
						  		 </td>
						  		  </tr>
						  		</c:when>
						  		<c:otherwise>
						  		 <tr>
						  			<td></td>
						  			<td class="listwhitetext" width="80px">Order Initiation</td>
						  			<td><INPUT type="radio" name="customerFile.orderAction"  value="IO"  tabindex="1"></td>
						  		    <td></td>
						  		    
						  		 </tr>
						  		 <tr>
						  		  <td></td>
						  		  <td class="listwhitetext">Quote Request</td>
						  		  <td><INPUT type="radio" name="customerFile.orderAction"  value="QR"  tabindex="2"></td> 
						  		 <td id="hidIntiationStatus">
						  		  <input type="button" class="cssbutton1" value="Accept Order" name="acceptOrder" style="width:100px; height:20px;!margin-bottom: 10px;" onclick="updateOrderManagementStatus('Accepted')"/> 
						  		 </td>
						  		 </tr>
						  		</c:otherwise>
					  		</c:choose> 
					 
    	              </table>
    	              <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" >
    	              <tr>
					  <td align="right" width="112"  class="listwhitetext" valign="bottom">Service Requested&nbsp;</td>
					  </tr>
					  <tr>
					  <td></td>
					  <td colspan="11">
					    <fieldset style="margin:2px; padding:5px; width:380px;">
					  <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
					  <tr>					
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Air</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAir" value="${customerFile.serviceAir}" fieldValue="true" disabled="disabled" tabindex="4" /></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Sea</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceSurface" value="${customerFile.serviceSurface}" fieldValue="true" disabled="disabled" tabindex="5"/></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Truck</td>
					  <td align="left" width="3%" ><s:checkbox name="customerFile.serviceAuto" value="${customerFile.serviceAuto}" fieldValue="true" disabled="disabled" tabindex="6"/></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Storage</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.serviceStorage" value="${customerFile.serviceStorage}" fieldValue="true" disabled="disabled" tabindex="7"/></td>
					  <td align="right" class="listwhitebox" width="2%" valign="middle">Domestic</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.serviceDomestic" value="${customerFile.serviceDomestic}" fieldValue="true" disabled="disabled" tabindex="8" /></td>
					  <configByCorp:fieldVisibility componentId="component.tab.customerFile.servicePovCheck">
					   <td align="right" class="listwhitebox" width="2%" valign="middle">POV</td>
					  <td align="left"  width="3%"><s:checkbox name="customerFile.servicePov" value="${customerFile.servicePov}" fieldValue="true" disabled="disabled" tabindex="8" /></td>
					  </configByCorp:fieldVisibility>
                     <td width="2%"></td>					 
					 </tr>
					 </table>
					 </fieldset>
					 </td>
                      </tr></table>
                      <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%">
                      <tr>
                      <td align="left" height="5"  class="listwhitetext" valign="bottom"></td>
                      <td></td>
                      </tr>
                      <tr>
                      <td align="right" width="114"  class="listwhitetext" valign="top">Relocation&nbsp;Services&nbsp;</td>
                      <s:hidden  name="customerFile.serviceRelo"   />
                     <td><td class="listwhitetext" valign="middle"><input type="checkbox" style="margin-left:0px;vertical-align:middle;" name="checkAllRelo"  onclick="checkAll(this)" tabindex="9" />Check&nbsp;All&nbsp;</td></td>
                     </tr>
                     <tr>
                     <td></td>
                     
                     <td colspan="2" valign="top">
                     <c:if test="${not empty serviceRelos1}">
                     <fieldset style="margin:3px 0 0 0; padding:2px 0 0 2px; width:55%;">                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                      <td width="" valign="top">                      
                        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">  
                          <c:forEach var="entry" items="${serviceRelos1}">
                           <tr>
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="10"/> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>                           
                          </tr>
                          </c:forEach>
                          </table>                           
                       </td>
                       <td valign="top"> 
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${serviceRelos2}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" tabindex="10" /> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:forEach>
                          </table>                          
                       </td>
                       </tr>
                       </table>
                       </fieldset>
                       </c:if>
                       </td>
                        
                       </tr>
                       <tr>
	                      <td align="left" height="10"  class="listwhitetext" valign="bottom"></td>
	                      <td></td>
                        </tr>
                      </table>       
                      	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin: 0px; paddding: 0px;">
						<tr> <td>&nbsp;</td> </tr>
						<tr>
							<td align="right" width="109" class="listwhitetext">Pets&nbsp;Involved<font color="red" size="2">*</font></td>
							<td width="5">&nbsp;</td>
							<td align="left" width="3%">
								<s:checkbox name="customerFile.petsInvolved" cssStyle="margin-left:0px;vertical-align:middle;" value="${customerFile.petsInvolved}" fieldValue="true"
								disabled="disabled" tabindex="3" />
							</td>
							<td width="10%">&nbsp;</td>
							<td width="5">&nbsp;</td>
							<td width="">&nbsp;</td>
						</tr>
						
						<tr><td>&nbsp;</td></tr>

						<tr>

							<td align="right" width="108" class="listwhitetext">Visa Required&nbsp;</td>
							<td width="5">&nbsp;</td>
							<td align="left" width="115">
							<s:select cssClass="list-menu" name="customerFile.visaRequired" list="%{visaRequiredList}"
								cssStyle="width:100px" headerKey="" headerValue="" onchange="cheackVisaRequired();" tabindex="10" /></td>
								<td width="15">&nbsp;</td>
							<td align="right" nowrap="nowrap" class="listwhitetext" id="hidVisaStatusReg" width="10%">Visa
							Status<font color="red" size="2">*&nbsp;</font></td>
							<td width="5">&nbsp;</td>
							<td align="left" id="hidVisaStatusReg1">
							 <s:select cssClass="list-menu" name="customerFile.visaStatus"
								list="%{visaStatusList}" cssStyle="width:100px" headerKey=""
								headerValue="" tabindex="11" /></td>
							<td width="">&nbsp;</td>
						</tr>
						<tr> <td>&nbsp;</td> </tr>
					</table>


<table class="detailTabLabel" cellspacing="0" cellpadding="0"
							border="0" style="margin: 0px; paddding: 0px;">
							<tr>
							<td width="108" class="listwhitetext" align="right" valign="top">Entitlement&nbsp;&nbsp;</td>
							<td>
							<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin: 0px; paddding: 0px;">
							<c:forEach var="entitlementMap" items="${entitlementMap}">
							<tr>								
								<td align="left" width="" class="listwhitetext">								
								<s:checkbox name="chk_${entitlementMap.key}" id="chk_${entitlementMap.key}" value="chk_${entitlementMap.key}" fieldValue="true" tabindex="3" onclick="getValue3();" cssStyle="vertical-align:middle;" />
	                           	<s:label title="${entitlementMap.value}" value="${entitlementMap.key}"></s:label>
								</td>
							</tr>
							</c:forEach>
							
						</table>
						</td>
						</tr>
						</table>



					
                      
                      <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                      <tr>
                      <td align="left"   class="listwhitetext" valign="bottom"></td>
                      </tr>
                      <tr>
                      <td class="listwhitetext" width="113" valign="top" align="right">&nbsp;</td>
                      <td  align="left" width="" ><s:textarea name="customerFile.orderComment" cssStyle="width:390px;height:90px" cssClass="textarea" readonly="false" tabindex="13" /></td>
                      </tr>
                      </table>
    	              <tr><td height="2px"></td></tr>
					</tbody></table>
			<tr>
				<td>
				<table cellpadding="0" cellspacing="0" border="0" width="100%" style="margin:0px;padding:0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;Employee Details
</td>
<td width="28" valign="top" class="headtab_bg" style="cursor:default; "></td>
<td class="headtab_bg_center" style="cursor:default; ">
<a href="javascript:animatedcollapse.hide(['address', 'alternative', 'billing', 'entitlement', 'cportal','family','benefits','hr','removal'])">Collapse All<img src="${pageContext.request.contextPath}/images/section_contract.png" HEIGHT=14 WIDTH=14 align="top"></a> | <a href="javascript:animatedcollapse.show(['address', 'alternative', 'billing', 'entitlement', 'cportal','family','benefits','hr','removal'])">Expand All<img src="${pageContext.request.contextPath}/images/section_expanded.png" HEIGHT=14 WIDTH=14 align="top"></a>
</td>
<td class="headtab_right">
</td>
</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="1" class="detailTabLabel" style="margin-left:93px;" >
  <tr>
    
    		<td align="left">
			<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tbody> 
				  <tr>
				  <td>&nbsp;</td>
				  <td align="left">
	              <table width="950" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
					<tbody>
										
						<tr>
							<td align="left" class="listwhitetext" valign="bottom" ><fmt:message key='customerFile.prefix'/></td>
							
							<td align="left" class="listwhitetext" valign="bottom">Title / Rank</td>
							
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.firstName' /></td>
							
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.middleInitial'/></td>
							
							<td align="left" class="listwhitetext" valign="bottom"><fmt:message key='customerFile.lastName'/><font color="red" size="2">*</font></td>
							
							
							<td align="left" class="listwhitetext" ><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.socialSecurityNumber',this);return false" ><fmt:message key='customerFile.socialSecurityNumber'/></a></td>
							<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.vip',this);return false" ><fmt:message key='serviceOrder.vip'/></a></td>
						    <td align="left" colspan="4" class="listwhitetext">Control&nbsp;Flag</td>
						    
						</tr>
						<tr>
						<c:set var="isVipFlag" value="false"/>
								<c:if test="${customerFile.vip}">
									<c:set var="isVipFlag" value="true"/>
								</c:if>
							<td align="left" class="listwhitetext" valign="top" style="width: 55px;" > <s:select cssClass="list-menu" name="customerFile.prefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" onfocus = "myDate();" tabindex="14"/></td>
						
							<td align="left" class="listwhitetext" valign="top" width="79"><s:textfield cssClass="input-text" key="customerFile.rank" size="9" maxlength="7" onkeyup="valid(this,'special')" onblur="valid(this,'special')" tabindex="15"/> </td>
							
							<td align="left" class="listwhitetext" valign="top" width="238" > <s:textfield cssClass="input-text upper-case" name="customerFile.firstName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							cssStyle="width:227px" maxlength="80" onfocus = "myDate();" tabindex="16"/> </td>
							
							<td align="left" class="listwhitetext" valign="top" > <s:textfield cssClass="input-text" key="customerFile.middleInitial"
							required="true" cssStyle="width:13px" maxlength="1" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="17"/> </td>
						
							<td align="left" class="listwhitetext" valign="top" width="205" > <s:textfield cssClass="input-text upper-case" key="customerFile.lastName" onblur="titleCase(this)" onkeypress="" onchange="noQuote(this);" required="true"
							size="32" maxlength="80" onfocus = "myDate();" tabindex="18"/> </td>
							
							
					        <td align="left" class="listwhitetext" valign="top">
					       
							<s:textfield cssClass="input-text" key="customerFile.socialSecurityNumber" size="7" maxlength="9" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="19"/>
					        
				           </td>
							<td width="50" align="left" valign="top" ><s:checkbox key="customerFile.vip" value="${isVipFlag}" fieldValue="true" onclick="changeStatus()" tabindex="20"/></td>
							<td align="left" valign="top" class="listwhitetext" > <s:select cssClass="list-menu" name="customerFile.controlFlag" cssStyle="width:47px" list="%{ControlFlagList}" headerKey="" headerValue="" tabindex="21" /></td>
							<c:if test="${empty customerFile.id}">
							<td align="right" style="width:137px;"><img style="margin-right:30px;" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countVipNotes == '0' || countVipNotes == '' || countVipNotes == null}">
								<td align="right" style="width:137px"><img style="margin-right:30px;" id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,580);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" style="width:137px"><img  style="margin-right:30px;" id="countVipNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=VipReason&imageId=countVipNotesImage&fieldId=countVipNotes&decorator=popup&popup=true',800,580);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>
					  </tr>
					</tbody>
				</table>
</td>
			    </tr>
				
				<tr>
				  <td>&nbsp;</td>
				  <td align="left"> 
				<table width="" border="0" cellpadding="0" cellspacing="0" class="detailTabLabel">
					<tbody> 
							<tr>
							<td width="134" align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.originCountry',this);return false" >Origin</a></td>
							
							<td width="15" align="left" class="listwhitetext" ></td>
							<td width="10" align="left" class="listwhitetext">&nbsp;</td>
							
							
						
							
							<td width=""  align="left" class="listwhitetext" >&nbsp;</td>
							<td width="134" align="left" class="listwhitetext" ><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.destinationCountry',this);return false" >Destination</a></td>
							<td width="" align="left" valign="bottom" class="listwhitetext">
								
							</td> 
                            <td></td>
                            <td></td>
                            <td></td>
                            <td class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.moveDate',this);return false" ><fmt:message key='customerFile.moveDate'/></a></td>
                            <td></td>
                            <td class="listwhitetext"><a href="#"  style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.status',this);return false"><fmt:message key='customerFile.status'/></a></td>
                            <td></td>
                            <td></td>
                            <td></td>
							</tr>
						<tr>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.originCityCode" size="20" maxlength="30" readonly="true" tabindex="22"/></td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper"  name="customerFile.originCountryCode" cssStyle="width:37px" maxlength="30" readonly="true" tabindex="23"/></td>
							<td align="left" class="listwhitetext" >&nbsp;</td>
							<td align="left" class="listwhitetext" >&nbsp;</td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCityCode" size="20" maxlength="30" readonly="true" tabindex="24"/></td>
							<td align="left" class="listwhitetext" >&nbsp;</td>
							<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="customerFile.destinationCountryCode" cssStyle="width:37px;" maxlength="30" readonly="true" tabindex="25"/></td>
							<td align="left" class="listwhitetext" >&nbsp;</td>
							<td align="left" class="listwhitetext" >&nbsp;</td>
							<c:if test="${not empty customerFile.moveDate}">
							<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.moveDate"/></s:text>
							<td width="65"><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/></td>
							<td width="35">&nbsp;<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<c:if test="${empty customerFile.moveDate}">
							<td width="65"><s:textfield cssClass="input-text" id="moveDate" name="customerFile.moveDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/></td>
							<td width="35">&nbsp;<img id="moveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							</c:if>
							<td align="left" class="listwhitetext" valign="top" > <s:select cssClass="list-menu" key="customerFile.status"
							list="%{custStatus}" headerKey="" headerValue="" cssStyle="width:108px" onchange="checkStatus();changeStatus();" onfocus = "myDate();" tabindex="26"/> </td>
							<s:hidden  key="customerFile.customerEmployer" cssStyle="width:180px;" />
						</tr>
					</tbody>
				</table></td>
		    </tr> </tbody></table></td></tr></table> 		
		    
							
							<c:if test="${not empty customerFile.bookingDate}">
								<s:text id="customerFileBookingFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.bookingDate"/></s:text>
								<s:hidden id="bookingDate" name="customerFile.bookingDate" value="%{customerFileBookingFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.bookingDate}">
								<s:hidden id="bookingDate"  name="customerFile.bookingDate" />
							</c:if> 
							<c:if test="${not empty customerFile.initialContactDate}">
							<s:text id="customerFileInitialFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.initialContactDate"/></s:text>
							<s:hidden id="initialContactDate"  name="customerFile.initialContactDate" value="%{customerFileInitialFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.initialContactDate}">
							<s:hidden id="initialContactDate"  name="customerFile.initialContactDate" />
							</c:if> 
		
				<table border="0" cellpadding="0" cellspacing="2" class="detailTabLabel">
					<tbody>
						
						<tr>
						<td></td>
						  <td align="left" class="listwhitetext" >Code</td>
						  <td width="20"></td>
						  <td align="left" class="listwhitetext" style="width:100px">Name</td> 
						  <td width="10"></td>
						  <td align="left" class="listwhitetext" width="100px">Company Division</td>
						  <td width="10"></td>
						  <td  align="left" id="coordRequiredFalse" class="listwhitetext" width="100px"><fmt:message key='customerFile.coordinator'/></td>
						  <td width="17"></td> 
						  <td align="left" class="listwhitetext" width="100px">Cost Center</td>
						  <td ></td> 
						  </tr>
						<tr>
						    <td align="right" valign="middle" class="listwhitetext" width="95px" >Booking&nbsp;Agent:</td> 
						    <c:choose>
							    <c:when test="${not empty customerFile.bookingAgentCode }">
							    	<td width="80px"><s:textfield cssClass="input-textUpper" readonly="true" name="customerFile.bookingAgentCode" cssStyle="width:75px;" maxlength="8" tabindex="27"/></td>
							    	<td width="20"><img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>"/></td>
									<td align="left" width="200"><s:textfield cssClass="input-textUpper" readonly="true" name="customerFile.bookingAgentName" size="37" maxlength="250" tabindex="28" /></td>
							<td></td>
							    </c:when>
							    <c:otherwise>
							    	<td width="80px"><s:textfield cssClass="input-textUpper" readonly="true" name="customerFile.bookingAgentCode" value="${defaultBookingAgentCode}" cssStyle="width:75px;" maxlength="8" tabindex="27"/></td>
							    	<td width="20"><img class="openpopup" style="vertical-align:top;" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>"/></td>
									<td align="left" width="200"><s:textfield cssClass="input-textUpper" readonly="true" name="customerFile.bookingAgentName" value="${defaultBookingAgentName}" size="37" maxlength="250" tabindex="28" /></td>
									<td></td>
							    </c:otherwise>
							</c:choose>
							<td align="left" width="110"><s:textfield cssClass="input-text"  name="customerFile.companyDivision" cssStyle="width:110px" tabindex="29" /></td>
							<s:hidden  id="job" name="customerFile.job"  />
							<td></td>
							<td align="left" width="110"><s:select cssClass="list-menu" id="coordinator" name="customerFile.coordinator" list="%{coordinatorList}"  cssStyle="width:130px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="30"/></td>
							<td width="13"></td>
	             			<td align="left" width="110" colspan="5"><s:textfield cssClass="input-text" name="customerFile.costCenter" id="orderInitiationCostCenter" size="32" maxlength="50"  tabindex="82" value="${customerFile.costCenter}"/></td> 
							<s:hidden name="customerFile.salesStatus" />
							<td align="left" class="listwhitetext" ></td>
							<td align="left"><s:hidden  name="customerFile.salesMan" /></td>
							
						</tr>
						 <tr><td height="5px"></td></tr>
					</tbody>
				</table>				
			  
							<s:hidden   name="customerFile.comptetive"  /> 
			                <s:hidden  name="customerFile.source"  /> 
							<s:hidden  name="customerFile.estimator" id="estimator" /> 
							<c:if test="${not empty customerFile.survey}">
							<s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.survey"/></s:text>
							<s:hidden  id="survey" name="customerFile.survey" value="%{customerFileSurveyFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.survey}">
							<s:hidden  id="survey" name="customerFile.survey" />
							</c:if> 
							<s:hidden  name="customerFile.surveyTime" id="surveyTime" /> 
							
							<s:hidden name="customerFile.surveyTime2" id="surveyTime2" /> 
							<s:hidden name="timeZone" value="${sessionTimeZone}" />  
							<c:if test="${not empty customerFile.priceSubmissionToAccDate}">
							<s:text id="customerFileSubmissionToAccFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToAccDate"/></s:text>
							<s:hidden id="priceSubmissionToAccDate" name="customerFile.priceSubmissionToAccDate" value="%{customerFileSubmissionToAccFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToAccDate}">
							<s:hidden id="priceSubmissionToAccDate"  name="customerFile.priceSubmissionToAccDate" />
							</c:if>
							
							<c:if test="${not empty customerFile.priceSubmissionToTranfDate}">
							<s:text id="customerFileSubmissionToTranfFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.priceSubmissionToTranfDate"/></s:text>
							<s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" value="%{customerFileSubmissionToTranfFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.priceSubmissionToTranfDate}">
							<s:hidden id="priceSubmissionToTranfDate" name="customerFile.priceSubmissionToTranfDate" />
							</c:if>
							
							<c:if test="${not empty customerFile.quoteAcceptenceDate}">
							<s:text id="customerFileAcceptenceFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.quoteAcceptenceDate"/></s:text>
							<s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" value="%{customerFileAcceptenceFormattedValue}" />
							</c:if>
							<c:if test="${empty customerFile.quoteAcceptenceDate}">
							<s:hidden id="quoteAcceptenceDate"  name="customerFile.quoteAcceptenceDate" />
							</c:if>  
				
				<tr>
					<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('address'); openadd()" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Address Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="address" class="switchgroup1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
											<tbody>
										
											<tr>
												<td>
												<div class="subcontenttabChild"><b>&nbsp;Origin</b></div>
												</td>
												</tr>
											
											
												<tr>
													<td valign="top" align="left" class="listwhitetext">
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originAddress1'/></td>
																<td width="65px">&nbsp;</td>
																
															
																</tr>
																<tr>
																<td align="left" class="listwhitetext" width="80"></td>
															    <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress1" cssStyle="width:230px;" maxlength="100" tabindex="31" onblur="titleCase(this);" onkeydown="disabledSOBtn();"/></td>
																
																 <td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																<s:hidden  name="customerFile.originCompany"  />
																<s:hidden  name="customerFile.originCompanyCode"  />
															    </tr>
																</table>
																
																</td>
															</tr>
														
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
															
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress2" cssStyle="width:230px;" maxlength="100" tabindex="32" onblur="titleCase(this);" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																
																</tr>
																<tr>
																<td align="left" class="listwhitetext" style=""></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.originAddress3" cssStyle="width:230px;" maxlength="100" tabindex="33" onblur="titleCase(this);" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																
																</tr>
																</table>
																</td>
																
																<td style="margin:0px; padding: 0px;" >
																
													               </td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
														
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td width="57px"></td>
																<td align="left" class="listwhitetext" id="originStateNotRequired"><fmt:message key='customerFile.originState'/></td>
																<td align="left" class="listwhitetext" id="originStateRequired"><fmt:message key='customerFile.originState'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.originCountry" list="%{ocountry}" id="ocountry" cssStyle="width:235px" onkeydown="disabledSOBtn();" onchange="stateOrigin();zipCode();changeStatus();getOriginCountryCode(this);autoPopulate_customerFile_originCountry(this);getState(this);disabledSOBtn();enableStateListOrigin();"  headerKey="" headerValue="" tabindex="34"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openOriginLocation();"/></td>
																<td width="57px"></td>
																<td><s:select cssClass="list-menu" id="originState" name="customerFile.originState" list="%{ostates}" cssStyle="width:228px" onkeydown="disabledSOBtn();" onchange="changeStatus();autoPopulate_customerFile_originCityCode(this); disabledSOBtn();" headerKey="" headerValue="" tabindex="35"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
															<td align="left" class="listwhitetext" onmouseover ="gettooltip('customerFile.originCity');" onmouseout="hideddrivetip();"><fmt:message key='customerFile.originCity'/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" id="zipCodeRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.originZip' /></td> 
																</tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
													<td><s:textfield cssClass="input-text upper-case" name="customerFile.originCity" cssStyle="width:150px;" maxlength="30" onkeypress="" onkeydown="disabledSOBtn();" onblur="titleCase(this);autoPopulate_customerFile_originCityCode(this,'special');" tabindex="36"/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originZip" cssStyle="width:64px;" maxlength="10" onchange="findCityState(this,'OZ');disabledSOBtn();" onkeydown="return onlyAlphaNumericAllowed(event,this,'special');" onblur="valid(this,'special')" tabindex="37"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																 <configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
						                        <td>
                                                <li><a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="" style="cursor: pointer;">&nbsp;&nbsp;&nbsp;&nbsp;<u><strong><font size="2"><spam >Look-Up Canadian Postal Code</spam></font></strong></u></a>
                                                </li></td>
                                                </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originDayPhone'/></td>
																
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.originDayPhoneExt'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originHomePhone'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originMobile'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.originFax'/></td>
															</tr>
															<tr>	
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhone" cssStyle="width:150px;" maxlength="20"  tabindex="38" onkeydown="disabledSOBtn();"/></td>
																<td style="width:10px"> </td>
																<td><s:textfield cssClass="input-text" name="customerFile.originDayPhoneExt" cssStyle="width:64px;" maxlength="10"  onkeydown="disabledSOBtn();" tabindex="39"/></td>
																<td align="left" class="listwhitetext" style="width:26px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originHomePhone" cssStyle="width:130px;" maxlength="20"  tabindex="40" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originMobile" cssStyle="width:130px;" maxlength="25"  tabindex="41" onkeydown="disabledSOBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originFax" cssStyle="width:130px;" maxlength="20"  tabindex="42" onkeydown="disabledSOBtn();"/></td>
															</tr>
														</tbody>
													</table>
													
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.email2'/></td>
																<td align="left" class="listwhitetext" style="width:45px"></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email" cssStyle="width:230px;" maxlength="65" tabindex="43" onchange="emailValidate('customerFileForm','customerFile.email','Origin Primary');EmailPortDate();" onkeydown="disabledSOBtn();"/></td>
																<td align="center" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email}"/></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.email2" cssStyle="width:240px;" maxlength="65" tabindex="44" onkeydown="disabledSOBtn();" onchange="emailValidate('customerFileForm','customerFile.email2','Origin Secondary');autoPopulateSecondryEmail();"/></td>
																<td align="left" class="listwhitetext" style="width:49px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.email2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.email2}"/></td>
																<td><s:textfield cssClass="input-text" name="customerFile.originPreferredContactTime" cssStyle="width:130px;" maxlength="30" tabindex="48" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:110px;" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countOriginNotes == '0' || countOriginNotes == '' || countOriginNotes == null}">
																		<td style="width:110px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:110px;" align="right"><img id="countOriginNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Origin&imageId=countOriginNotesImage&fieldId=countOriginNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
														</tbody>
													</table>
													</td>
													
												</tr>
												<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
												<tr><td align="center" colspan="15"  class="vertlinedata"></td></tr>
												
											<tr>
												<td>
												<div class="subcontenttabChild"><b>&nbsp;Destination</b></div>
												</td>
												</tr>
												<tr>
													<td valign="top" align="left" class="listwhitetext" >
													<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px; padding: 0px;">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
														<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;" colspan="2">
																<table style="margin:0px; padding: 0px;">
																<tr>
																<td align="left" class="listwhitetext" style="width:80px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationAddress1'/></td>
																<td width="65px">&nbsp;</td> 
																</tr>
																<tr>
																<td align="left" class="listwhitetext" width="95px"></td>
															   <td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress1" cssStyle="width:230px;" maxlength="100" tabindex="45" onblur="titleCase(this);" onkeydown="disabledSODestinBtn();"/></td>
																<td width="22"><img class="openpopup" width="17" height="20" onclick="openStandardAddressesDestinationPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
																
																<s:hidden name="customerFile.destinationCompany" />
																<s:hidden name="customerFile.destinationCompanyCode" />
																
															    </tr>
																</table>
																
																</td>
															</tr>
														
															<tr>
																
																<td valign="top" style="margin:0px; padding: 0px;padding-right:12px;">
																<table style="margin:0px; padding: 0px;" >
															
																<tr>
																<td align="left" class="listwhitetext" style="width:95px"></td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress2" cssStyle="width:230px;" maxlength="100" tabindex="46" onblur="titleCase(this);" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address2</font></td>
																
																</tr>
																<tr>
																<td align="left" class="listwhitetext" style=""></td>
																<td ><s:textfield cssClass="input-text upper-case" name="customerFile.destinationAddress3" cssStyle="width:230px;" maxlength="100" tabindex="47" onblur="titleCase(this);" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="padding-left:3px"><font color="gray">Address3</font></td>
																
																</tr>
																</table>
																</td>
																
																<td style="margin:0px; padding: 0px;" >
																
													               </td>
															</tr>
														
														
														
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCountry'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext"></td>
																<td width="57px"></td>
																<td align="left" class="listwhitetext" id="destinationStateNotRequired"><fmt:message key='customerFile.destinationState'/></td>
																<td align="left" class="listwhitetext" id="destinationStateRequired"><fmt:message key='customerFile.destinationState'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																
															<tr>
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:select cssClass="list-menu" name="customerFile.destinationCountry" list="%{dcountry}" id="dcountry" cssStyle="width:235px" onkeydown="disabledSODestinBtn();" onchange="stateDestination();zipCodeDestin();changeStatus();getDestinationCountryCode(this);autoPopulate_customerFile_destinationCountry(this);getDestinationState(this);disabledSODestinBtn();enableStateListDestin();"  headerKey="" headerValue="" tabindex="48"/></td><td><img src="${pageContext.request.contextPath}/images/globe.png"  onclick="openDestinationLocation();"/></td>
																<td width="57px"></td>
																<td><s:select cssClass="list-menu" id="destinationState" name="customerFile.destinationState" list="%{dstates}" cssStyle="width:228px" value="%{customerFile.destinationState}" onkeydown="disabledSODestinBtn();" onchange="changeStatus();autoPopulate_customerFile_destinationCityCode(this);disabledSODestinBtn();" headerKey="" headerValue="" tabindex="49"/></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
															
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:100px; height:5px;" colspan="7"></td></tr>
															<tr>
															<td class="listwhitetext" style="width:100px"></td>
													        <td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationCity'/></td>
														    <td align="left" class="listwhitetext" style="width:5px"></td>
														    <td align="left" id="zipCodeDestinRequiredFalse" class="listwhitetext"><fmt:message key='customerFile.destinationZip' /></td>
															
															</tr>
													        <tr>
													        <td class="listwhitetext" style="width:100px"></td>
													        <td><s:textfield cssClass="input-text upper-case" name="customerFile.destinationCity" cssStyle="width:150px" maxlength="30" onkeypress="" onkeydown="disabledSODestinBtn();" onblur="titleCase(this);autoPopulate_customerFile_destinationCityCode(this,'special');" tabindex="50"/></td>
															<td align="left" class="listwhitetext" style="width:10px"></td>
															<td><s:textfield cssClass="input-text" name="customerFile.destinationZip" cssStyle="width:64px" maxlength="10"  onchange="findCityState(this,'DZ');disabledSODestinBtn();" onkeydown="return onlyAlphaNumericAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="51"/></td>
															<td align="left" class="listwhitetext" style="width:10px"></td>
															<configByCorp:fieldVisibility componentId="component.field.postalCodeHSRG">
						                        <td>
                                                <li><a id="myUniqueLinkId" onclick="callPostalCode();" onmouseover="" style="cursor: pointer;">&nbsp;&nbsp;&nbsp;&nbsp;<u><strong><font size="2"><spam >Look-Up Canadian Postal Code</spam></font></strong></u></a>
                                                </li></td>
                                                </configByCorp:fieldVisibility>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationDayPhone'/></td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																<td align="left" class="listwhitetext" style="width:25px"><fmt:message key='customerFile.destinationDayPhoneExt'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationHomePhone'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.destinationMobile'/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationFax'/></td>
															</tr>
															<tr>	
																<td align="left" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhone" cssStyle="width:150px" maxlength="20"  tabindex="52" onkeydown="disabledSODestinBtn();"/></td>
																<td style="width:10px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationDayPhoneExt" cssStyle="width:64px" maxlength="10"  tabindex="53" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:26px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationHomePhone" cssStyle="width:130px" maxlength="20"  tabindex="54" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationMobile" cssStyle="width:130px" maxlength="25"  tabindex="55" onkeydown="disabledSODestinBtn();"/></td>
																<td align="left" class="listwhitetext" style="width:13px"></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destinationFax" cssStyle="width:130px" maxlength="20"  tabindex="56" onkeydown="disabledSODestinBtn();"/></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.destinationEmail2'/></td>
																<td align="left" class="listwhitetext" style="width:45px"></td>
																<td align="left" class="listwhitetext">Preferred&nbsp;Time&nbsp;To&nbsp;Contact</td>
																<td align="left" class="listwhitetext" style="width:10px"></td>
																</tr>
															<tr>
																<td align="left" class="listwhitetext" style="width:100px"></td>
																<td align="left"><s:textfield cssClass="input-text" name="customerFile.destinationEmail" cssStyle="width:230px" maxlength="65" tabindex="57" onkeydown="disabledSODestinBtn();" onchange="emailValidate('customerFileForm','customerFile.destinationEmail','Destination Primary');"/></td>
																<td align="center" class="listwhitetext" style="width:25px"><img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.destinationEmail'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail}"/></td>
																<td ><s:textfield cssClass="input-text" name="customerFile.destinationEmail2" cssStyle="width:240px" maxlength="65" tabindex="58" onkeydown="disabledSODestinBtn();" onchange="emailValidate('customerFileForm','customerFile.destinationEmail2','Destination Secondary');"/></td>
																<td align="left" class="listwhitetext" style="width:49px">&nbsp;&nbsp;<img class="openpopup" onclick="sendEmail1(document.forms['customerFileForm'].elements['customerFile.destinationEmail2'].value)" src="<c:url value='/images/email_small.gif'/>" title="Send Email to: ${customerFile.destinationEmail2}"/></td>
																<td><s:textfield cssClass="input-text" name="customerFile.destPreferredContactTime" cssStyle="width:130px" maxlength="30" tabindex="48" /></td>
																<c:if test="${empty customerFile.id}">
																<td style="width:118px" align="right"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countDestinationNotes == '0' || countDestinationNotes == '' || countDestinationNotes == null}">
																		<td style="width:118px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:when>
																	<c:otherwise>
																		<td style="width:118px" align="right"><img id="countDestinationNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Destination&imageId=countDestinationNotesImage&fieldId=countDestinationNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:otherwise>
																</c:choose>
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:100px; height:10px;" colspan="7"></td></tr>
																<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px; padding: 0px;">
											<tr>
												<td class="subcontenttabChild" style="height:25px;!height:20px;padding-bottom:3px;!padding-bottom:0px;"><b>&nbsp;Spouse Contact</b></td>
												
													               
												</tr>
												</table>
																
													
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr>
															<c:set var="privilegeFlag" value="false"/>
															<c:if test="${customerFile.privileges}">
												 				<c:set var="privilegeFlag" value="true"/>
															</c:if>
															<td class="listwhitetext" style="width:50px; height:10px;"></td></tr>
															<tr></tr>
															<tr></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerPrefix'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerFirstName'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerLastName'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"> <s:select cssClass="list-menu" name="customerFile.custPartnerPrefix" cssStyle="width:47px" list="%{preffix}" headerKey="" headerValue="" onchange="changeStatus();" tabindex="59"/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" name="customerFile.custPartnerFirstName" required="true"
																cssStyle="width:169px" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="60"/> </td>
																<td align="left" class="listwhitetext" style="width:25px"></td>
																<td align="left" class="listwhitetext"> <s:textfield cssClass="input-text" key="customerFile.custPartnerLastName" required="true"
																cssStyle="width:278px" maxlength="80" onkeydown="return onlyCharsAllowed(event,this,'special')" onblur="valid(this,'special')" tabindex="61"/> </td>
																<td align="left">&nbsp;&nbsp;<s:checkbox key="customerFile.privileges" value="${privilegeFlag}" fieldValue="true" onclick="changeStatus()" tabindex="62"/></td>
																<td align="left" class="listwhitetext" style="width:65px"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.privileges',this);return false" >Joint&nbsp;Account</a></td>
															</tr>
														</tbody>
													</table>
													<table class="detailTabLabel" cellspacing="0" cellpadding="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:50px; height:5px;" colspan="7"></td></tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerEmail'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.custPartnerContact'/></td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" ><fmt:message key='customerFile.custPartnerMobile'/></td>
															</tr>
															<tr>
																<td align="left" valign="top" class="listwhitetext" style="width:100px">&nbsp;</td>
																<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="customerFile.custPartnerEmail"  cssStyle="width:230px" maxlength="65" tabindex="63" onchange="emailValidate('customerFileForm','customerFile.custPartnerEmail','Spouse');"/></td>
																<td align="left" class="listwhitetext" style="width:25px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerContact" cssStyle="width:130px" maxlength="20"  tabindex="64"/> </td>
																<td align="left" class="listwhitetext" style="width:15px"></td>
																<td align="left" class="listwhitetext" > <s:textfield cssClass="input-text" name="customerFile.custPartnerMobile" cssStyle="width:130px" maxlength="25"  tabindex="65"/> </td>
																<c:if test="${empty customerFile.id}">
																<td align="right" style="width:258px" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																</c:if>
																<c:if test="${not empty customerFile.id}">
																<c:choose>
																	<c:when test="${countSpouseNotes == '0' || countSpouseNotes == '' || countSpouseNotes == null}">
																	<td align="right" style="width:258px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:when>
																	<c:otherwise>
																	<td align="right" style="width:258px" valign="bottom"><img id="countSpouseNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Spouse&imageId=countSpouseNotesImage&fieldId=countSpouseNotes&decorator=popup&popup=true',800,580);" ></a></td>
																	</c:otherwise>
																</c:choose> 
																</c:if>
															</tr>
															<tr><td class="listwhitetext" style="width:50px; height:10px;" colspan="7"></td></tr>
														</tbody>
													</table>
										</div>
										</td>
									</tr>
														</tbody>
													</table>
													</td>
												</tr>
											</tbody>
										</table>
										</div>
										</td>
									</tr> 
									
									
									<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('additional')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;Additional&nbsp;Addresses
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="additional" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">
<s:set name="adAddressesDetailsList" value="adAddressesDetailsList" scope="request"/>
<display:table name="adAddressesDetailsList" class="table" requestURI="" id="adAddressesDetailsList"  defaultsort="1" pagesize="10" style="margin:0px 0px 10px 0px; ">
    <display:column title="Description">
    <a href="javascript:openWindow('editAdAddressesDetails.html?isOrderInitiation=true&id=${adAddressesDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',800,400)">
    <c:out value="${adAddressesDetailsList.description}"></c:out></a>
    </display:column>
    <display:column property="addressType"  title="Address Type" />
    <display:column title="Address" maxLength="15">${adAddressesDetailsList.address1}&nbsp;${adAddressesDetailsList.address2}&nbsp;${adAddressesDetailsList.address3}</display:column>     
    <display:column property="country" title="Country" />
    <display:column property="state" title="State"  /> 
    <display:column property="city"  title="City"  /> 
	<display:column property="phone" title="Phone" />	
	</display:table>
	<c:if test="${not empty customerFile.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editAdAddressesDetails.html?isOrderInitiation=true&decorator=popup&popup=true&customerFileId=${customerFile.id}','customerFileForm','height=500,width=725,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add Addresses" style="width:120px; height:25px" tabindex=""/> 
     </c:if>
     <c:if test="${empty customerFile.id}">
     <input type="button" class="cssbutton1" onclick="massageAddress();"
	       value="Add Addresses" style="width:120px; height:25px" tabindex="83"/> 
     </c:if>
</div>
</td></tr> 
</tbody>
</table></div></td></tr>								
																
<s:hidden name="customerFile.contactName" /> 
																<s:hidden  name="customerFile.organization"  />  
																<s:hidden name="customerFile.contactEmail"  /> 
																<s:hidden  name="customerFile.contactPhone"   /> 
																<s:hidden  name="customerFile.contactNameExt"  /> 
																<s:hidden  name="customerFile.destinationContactName"   />
																<s:hidden  name="customerFile.destinationOrganization" />  
																<s:hidden  name="customerFile.destinationContactEmail"   /> 
																<s:hidden name="customerFile.destinationContactPhone" /> 
																<s:hidden name="customerFile.destinationContactNameExt"  /> 
  							
      							<div  onClick="javascript:animatedcollapse.toggle('billing')" style="margin:0px;">
										<table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;">
											<tr>
											<td class="headtab_left">
											</td>
											<td NOWRAP class="headtab_center">&nbsp;&nbsp;Billing Details
											</td>
											<td width="28" valign="top" class="headtab_bg"></td>
											<td class="headtab_bg_center">&nbsp;
											</td>
											<td class="headtab_right">
											</td>
											</tr>
											</table> 
										</div>
										<div id="billing" class="switchgroup1">
										<table class="detailTabLabel" cellspacing="0" cellpadding="2" width="" border="0">
														<tbody>
															<tr><td class="listwhitetext" style="width:75px; height:10px;"></td></tr> 
															<tr>
																<td align="left" class="listwhitetext">&nbsp;</td> 
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billToCode'/><font color="red" size="2">*</font></td>
																<td align="left" class="listwhitetext" ></td>
																<td align="left" class="listwhitetext">Bill To Name</td>
																<td align="left" class="listwhitetext" ></td>
																<td align="left" colspan="2" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.billToAuthorization',this);return false" >Account PO# / SAP Number</a></td>
																<td align="left" class="listwhitetext" >&nbsp;</td>
															    <td  colspan="2" align="left" class="listwhitetext">Account&nbsp;Reference&nbsp;# / Cost&nbsp;Centre&nbsp;# </td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" width="95px">Billing:&nbsp;</td>
																		<td colspan="2">
																		<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;">
																		<tr>
																		<td width="60px"><s:textfield cssClass="input-text" name="customerFile.billToCode" cssStyle="width:54px;" onchange="findBillToNameOnload()" maxlength="8" tabindex="66"/></td>
																	    <td><img class="openpopup" width="17" height="20" onclick="openPopWindow(this);document.forms['customerFileForm'].elements['customerFile.billToCode'].focus();" src="<c:url value='/images/open-popup.gif'/>"/></td>
																	   	</tr>
																	    </table>
																		</td>
																		<td align="left"><s:textfield cssClass="input-text" name="customerFile.billToName" cssStyle="width:220px;"  maxlength="250" readonly="true" tabindex="67"/></td>
																<td align="left" class="listwhitetext" ></td>
																<td width="130"><s:textfield cssClass="input-text" name="customerFile.billToAuthorization"  cssStyle="width:190px;" maxlength="50" onblur="valid(this,'special')" tabindex="68"/></td>
																<s:hidden  id="contract" name="customerFile.contract"  />
																<td width=""></td>
																<td align="left" class="listwhitetext" >&nbsp;</td>
															    <td><s:textfield cssClass="input-text" name="customerFile.billToReference"  cssStyle="width:160px;" maxlength="15"  onblur="valid(this,'special')" tabindex="68"/></td> 
															</tr>
														</tbody>
													</table>
													
																
                                                                <s:hidden id="billPayMethod"  name="customerFile.billPayMethod" />  
															    <s:hidden name="customerFile.accountCode"  /> 
																<s:hidden  name="customerFile.accountName" /> 
													
													<table class="detailTabLabel" cellspacing="1" cellpadding="1" width="" border="0">
														<tbody> 
															<tr>
																<td align="left" class="listwhitetext" style="width:75px;">&nbsp;</td>
																<td align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.orderBy',this);return false" ><fmt:message key='customerFile.orderBy'/></a></td>
																<td align="left" class="listwhitetext" style="width:5px"></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.billApprovedDate'/></td>
																<td align="left" class="listwhitetext" ></td>
																<td align="left" class="listwhitetext" ></td>
																<td align="left" class="listwhitetext"><fmt:message key='customerFile.orderPhone'/></td>												
															    <td align="left" class="listwhitetext" >&nbsp;</td>
															    <td align="left" class="listwhitetext">Email</td>
															</tr>
															<tr>
																<td align="right" class="listwhitetext" style="width:95px;">Authorization:&nbsp;</td>
																<td><s:textfield cssClass="input-text" name="customerFile.orderBy" cssStyle="width:200px;" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" onblur="autoPopulate_customerFile_approvedDate(this,'special');" tabindex="69"/></td>
																<td align="left" class="listwhitetext" ></td>
																<c:if test="${not empty customerFile.billApprovedDate}">
																<s:text id="customerFileApprovedDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.billApprovedDate"/></s:text>
																<td><s:textfield cssClass="input-text" id="billApprovedDate" name="customerFile.billApprovedDate" value="%{customerFileApprovedDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="billApprovedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																</c:if>
																<c:if test="${empty customerFile.billApprovedDate}">
																<td><s:textfield cssClass="input-text" id="billApprovedDate" name="customerFile.billApprovedDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" onfocus="changeStatus();"/></td><td><img id="billApprovedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																</c:if>
																<td align="left" class="listwhitetext" style="width:1px;" ></td>
																<td><s:textfield cssClass="input-text" name="customerFile.orderPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="70"/></td>
															    <td align="left" class="listwhitetext" style="width:10px;">&nbsp;</td>
															    <td><s:textfield cssClass="input-text" name="customerFile.orderEmail"  cssStyle="width:220px;" maxlength="65"   tabindex="71" onchange="emailValidate('customerFileForm','customerFile.orderEmail','');"/></td>
															    <td width=""></td>
															</tr>
															<tr><td height="10px"></td></tr>
														</tbody>
													</table>  	
                                                                <s:hidden name="customerFile.personPricing"   /> 
																<s:hidden name="customerFile.personBilling" /> 
																<s:hidden name="customerFile.personPayable"  /> 
																<s:hidden name="customerFile.auditor" /> 
																<s:hidden name="customerFile.noCharge" /> 
																<s:hidden name="customerFile.approvedBy" /> 
														    	<c:if test="${not empty customerFile.approvedOn}">
																	<s:text id="FormatedInvoiceDate" name="${FormDateValue}"><s:param name="value" value="customerFile.approvedOn"/></s:text>
																    <s:hidden id="dateApprovedOn" name="customerFile.approvedOn" value="%{FormatedInvoiceDate}" />
																</c:if>
																<c:if test="${empty customerFile.approvedOn}">
																	<s:hidden id="dateApprovedOn" name="customerFile.approvedOn" /> 
																</c:if> 
										</div>
										</td>
									</tr>
									
									
		 <tr> 
             <td height="10" width="100%" colspan="6" align="left" style="margin:0px">
               <div onClick="javascript:animatedcollapse.toggle('family')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Family&nbsp;Details </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>           
       
                <div id="family" class="OverFlowPr" style="margin:0px;padding:0px;margin-top:5px;margin-bottom:5px;;width:95%;">
                <s:set name="dsFamilyDetailsList" value="dsFamilyDetailsList" scope="request"/>
                  <display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1" pagesize="10" style="width:100%;margin-bottom:20px;">
                     <c:if test="${(customerFile.quotationStatus=='Accepted' )||(customerFile.quotationStatus=='Rejected')|| (customerFile.quotationStatus=='Submitted' ) || (customerFile.orderIntiationStatus=='Submitted')}">
                     <display:column title="First Name"> 
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out>
                     </display:column> 
                     </c:if>
                     <c:if test="${(customerFile.quotationStatus!='Accepted' )&&(customerFile.quotationStatus!='Rejected')&& (customerFile.quotationStatus!='Submitted')&& (customerFile.orderIntiationStatus!='Submitted') }">
                    <display:column title="First Name">
                     <a href="javascript:openWindow('editDsFamilyDetails.html?id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,400)">
                     <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
                     </display:column>
                     </c:if>
                     
                  <display:column property="lastName"  title="Last Name" > <% count++; %>  </display:column>
				   
			      <display:column property="relationship"  title="Relationship" />   
			      <display:column property="cellNumber"  title="Mobile Phone"  />
			      <display:column property="email" title="Email"  />v
			      <display:column property="dateOfBirth" title="Date Of Birth" format="{0,date,dd-MMM-yyyy}" />
			      <display:column property="age" title="Age"  />
			      <display:column property="countryOfBirth" title="Country Of Birth"  />
			      <c:choose>
			      <c:when test="${passportNumberEncryptShow=='Y'}">
			      <display:column  title="Passport #" >
			      <c:if test="${(passportNumberEncrypt =='true') || (rolesAssignFlag=='Y')}">
			    		<c:out value="${dsFamilyDetailsList.passportNumber}"></c:out>
			      </c:if>
			      <c:if test="${(passportNumberEncrypt =='false') && (rolesAssignFlag!='Y')}">
			    	<c:if test="${fn:length(dsFamilyDetailsList.passportNumber) > 0}" >
			    		<c:set var="encryptedpassportNumber" value="${fn:substring(dsFamilyDetailsList.passportNumber, fn:length(dsFamilyDetailsList.passportNumber)-4, fn:length(dsFamilyDetailsList.passportNumber))}" />
			    		<c:set var="encryptedValue" value="XXXXXXXXXXXX-" />
			    		<c:set var="actualencryptedpassportNumber" value="${encryptedValue}${encryptedpassportNumber}" /> 
			    		<c:out value="${actualencryptedpassportNumber}"></c:out>
			     	</c:if>
			      </c:if>
			      </display:column>
			      </c:when> 
			      <c:otherwise>
			      <display:column property="passportNumber" title="Passport #"  />
			      </c:otherwise> 
			      </c:choose> 
				  <display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" />
				  <display:column property="countryOfIssue"  title="Country Of Issue" /> 
				  </display:table>
				  
	              <c:if test="${not empty customerFile.id}">
	               <input type="button" class="cssbutton1" onclick="window.open('editDsFamilyDetails.html?decorator=popup&popup=true&customerFileId=${customerFile.id}','sqlExtractInputForm','height=500,width=1350,top=0, scrollbars=yes,resizable=yes').focus();" 
	                value="Add Family Details" style="width:120px; height:25px;margin-bottom:10px;" tabindex="72"/> 
                  </c:if>
                  <c:if test="${empty customerFile.id}">
	                <input type="button" class="cssbutton1" onclick="massageFamily();"  value="Add Family Details" style="width:120px; height:25px;margin-bottom:10px;" tabindex="73"/> 
                   </c:if>
                <span style="padding-left:20px;" class="listwhitetext">Family Size <s:textfield cssClass="input-text" name="customerFile.familysize" onchange="getFamilysize('FS');" size="2" maxlength="2"  tabindex="6"/></span>
     
                </div>
	         </td> 
	         </tr>
	         
	         <tr>							
									
									
							<tr>
							<td height="10" width="100%" align="left" style="margin: 0px">
							<div  onClick="javascript:animatedcollapse.toggle('hr')" style="margin: 0px">
 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
 <tr>
 <td class="headtab_left"></td>
 <td NOWRAP  class="headtab_center" style="cursor:default; ">&nbsp;&nbsp;HR Details </td>
 <td width="28" valign="top" class="headtab_bg"></td>
 <td class="headtab_bg_center">&nbsp; </td>
 <td class="headtab_right"> </td>
 </tr>
 </table>
 </div>
  <div id="hr" class="switchgroup1">
     <table border="0" class="detailTabLabel" style="margin:0px;">
	 <tbody>
	 <tr>
	 <td colspan="5" >
		 <table width="100%"  style="margin:0px;">
		 <tr> 
		  <td align="center"  class="listwhitetext" width="50%"><b><u>Local Country</u></b></td> 
		 </tr>
		 </table>
	 </td>
	 </tr>
	 <tr>		
		<td align="right" class="listwhitetext" width="250px" >Name Of Local ${partnerAbbreviation} responsible HR Person</td>
		<td><s:textfield cssClass="input-text" name="customerFile.localHr" cssStyle="width:250px;" maxlength="30" onkeydown="return onlyCharsAllowed1(event,this)" tabindex="74" /></td>
		</tr>
		
		<tr>		
		<td align="right" class="listwhitetext">Phone</td>   
		<td><s:textfield cssClass="input-text" name="customerFile.localHrPhone"  cssStyle="width:130px;" maxlength="20"  tabindex="75"/></td>
	    <td width="10px"></td>
	    
	    </tr>
	    
	    <tr>		
		<td align="right" class="listwhitetext">Email Address</td>
		<td ><s:textfield cssClass="input-text" name="customerFile.localHrEmail" cssStyle="width:250px;" maxlength="65" tabindex="76"  onkeydown="" onchange="emailValidate('customerFileForm','customerFile.localHrEmail','Local Country');"/></td>
		<td width="10px"></td>

	 </tr>
	 <tr>
        <td align="right" class="listwhitetext"></td>
     </tr>
	 </tbody>
	 </table> 
 </div>
							
							
							</td>
							</tr>		
 						<tr>							
							<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('entitlement')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>

					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Assignment
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
					<div id="entitlement" class="switchgroup1">
									
    <table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">    
    
    <tr>
    <td colspan="15">
     <table class="detailTabLabel" border="0" cellpadding="1" cellspacing="2">
               <tbody>  
			     <tr>
			     <td colspan="10">
			     <table class="detailTabLabel" border="0" cellpadding="1" cellspacing="1" style="margin:0px;">
			     <tr>
			     <td align="right" class="listwhitetext">Start Date Of Contract/Stay </td>
			     <c:if test="${not empty customerFile.contractStart}">
		           <s:text id="customerFileContractStartFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractStart"/></s:text>
		           <td width="95"> <s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" value="%{customerFileContractStartFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           	<img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('S');focusDate('customerFile.contractStart');forDays(); return false;"/>
                   </td>
                  </c:if>
                  <c:if test="${empty customerFile.contractStart}">
	               <td width="95"><s:textfield cssClass="input-text" id="contractStart" name="customerFile.contractStart" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           	<img id="contractStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('S');focusDate('customerFile.contractStart');forDays(); return false;"/>
	              </td>
	              </c:if>      
	              <td align="right"  width="45" >&nbsp;</td>            
	            <td align="right" class="listwhitetext" width="" >End Date Of Contract/Stay </td> 
                <c:if test="${not empty customerFile.contractEnd}">
		           <s:text id="customerFileContractEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.contractEnd"/></s:text>
		           <td> <s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" value="%{customerFileContractEndFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		           	<img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('E');focusDate('customerFile.contractEnd');forDays(); return false;"/>
                   </td>
                </c:if>
                <c:if test="${empty customerFile.contractEnd}">
		           <td><s:textfield cssClass="input-text" id="contractEnd" name="customerFile.contractEnd" required="true" cssStyle="width:65px;" maxlength="11" readonly="true" onfocus="changeStatus();" onkeydown="return onlyDel(event,this)"/>
		          	 <img id="contractEnd_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="setContractValidFlag('E');focusDate('customerFile.contractEnd');forDays(); return false;"/>
	               </td>
	            </c:if>
	           </tr>
			     </table>
			     </td>
			     </tr>
	            <tr>
	            <td colspan="10">
	            <table  class="detailTabLabel" border="0" cellpadding="1" cellspacing="2" style="margin:0px;">
	            <tr>
	          <td align="right" class="listwhitetext"  width="130px">Duration Of Assignment</td>
             <td colspan="3"><s:textfield cssClass="input-text" name="customerFile.duration" id="orderInitiationASMLDuration" cssStyle="width:146px;" maxlength="12"  tabindex="77" onselect="" onfocus="calcDays()"/></td>
             </tr> 
             <tr> 
             <td class="listwhitetext" align="right" >Type Of Contract<configByCorp:fieldVisibility componentId="component.customerfile.field.contractTypeFlag"> <font color="red">*</configByCorp:fieldVisibility></font></td></td>
             <td class="listwhitetext"colspan="3" >
             <c:if test="${parentAgentForOrderinitiation=='true'}">
             <%-- <s:select  cssClass="list-menu" name="customerFile.assignmentType" id="assignmentType" list="%{assignmentTypesASML}" headerKey=" " headerValue=" "  cssStyle="width:150px;" onclick="changeStatus();checkAssignmentType();" tabindex="78"/> --%>
             <configByCorp:customDropDown listType="map" list="${assignmentTypesASML_isactive}" fieldValue="${customerFile.assignmentType}" 
                        attribute="class=list-menu style=width:150px; id=assignmentType name=customerFile.assignmentType headerKey='' headerValue='' onclick=changeStatus();checkAssignmentType();"/></td>
             </c:if>
             <c:if test="${parentAgentForOrderinitiation!='true'}">
            <%--  <s:select  cssClass="list-menu" name="customerFile.assignmentType" id="assignmentType" list="%{assignmentTypes}" headerKey=" " headerValue=" "  cssStyle="width:150px;"  onclick="changeStatus();checkAssignmentType();" tabindex="78"/> --%>
            <configByCorp:customDropDown listType="map" list="${assignmentTypes_isactive}" fieldValue="${customerFile.assignmentType}" 
                        attribute="class=list-menu style=width:150px; id=assignmentType name=customerFile.assignmentType headerKey='' headerValue='' onclick=changeStatus();checkAssignmentType();"/></td>
            
             </c:if>
             </td>
             <td align="left"  colspan="5"><s:textfield  id="otherContract" cssClass="input-text" name="customerFile.otherContract" cssStyle="width:160px;"  maxlength="30" tabindex="79"/></td>
             </tr>
             <tr>
             <td align="right"  class="listwhitetext" >Home Country&nbsp;</td> 
             <td  colspan="2"><s:select cssClass="list-menu" name="customerFile.homeCountry" list="%{ocountry}" cssStyle="width:150px"  onchange="changeStatus();getHomeCountryCode(this);autoPopulate_customerFile_homeCountry(this);gethomeState(this);enableStateListHome();"  headerKey="" headerValue="" tabindex="80"/><img src="${pageContext.request.contextPath}/images/globe.png"  style="vertical-align:top" onclick="openHomeCountryLocation();"/></td>
             <td  align="right" class="listwhitetext" >State</td> 
             <td width="60"><s:select cssClass="list-menu" id="homeState" name="customerFile.homeState" list="%{homeStates}" cssStyle="width:164px"  onchange="changeStatus(); " headerKey="" headerValue="" tabindex="81"/></td>
             <td  align="right" class="listwhitetext" >City</td> 
             <td width="60"><s:textfield cssClass="input-text" name="customerFile.homeCity" cssStyle="width:140px;" maxlength="30"  onblur="" tabindex="82"/></td>
             </tr>
              <tr>
             <td  align="right"  class="listwhitetext" >Family Situation</td>  
             <td><s:select cssClass="list-menu" name="customerFile.familySitiation" list="%{familySitiationList}" cssStyle="width:150px" onchange="" headerKey="" headerValue="" tabindex="83"/></td>
             </tr>
            <tr> 
			 <td align="right" class="listwhitetext">Pets Involved</td>
			 <td colspan="2"><s:checkbox cssStyle="margin:0px;" name="customerFile.petsInvolved" value="${customerFile.petsInvolved}" fieldValue="true" onclick="changeStatus();checkPetInvolve(this)" tabindex="84" /></td>	
			 <td colspan="3">
			 <table width="" style="margin:0px;">
			<tr id="petTypeId">
			 <td class="listwhitetext">Type Of Pet </td>
			 <td style="width: 8px; color: red;" >*</td>
			 <td><s:select cssClass="list-menu"  name="customerFile.petType"  list="%{petTypeList}" cssStyle="width:100px"   tabindex="85"/></td>
			</tr>
			</table>
			</td>
			</tr>
             </table>
             </td>
             </tr>
	         </table>
    <div  id="entitlementDiv">
    <table class="detailTabLabel" cellspacing="0" cellpadding="0">
       <tr>
      <td width="140" align="right" valign="top" class="listwhitetext" >Comments:&nbsp;</td>
      <td width="520" align="left"  ><textarea name="customerFile.entitled" style="width:535px;height:90px;" class="textarea" tabindex="86" maxlength="250"><c:out value='${customerFile.entitled}'/></textarea></td>
      <td width="180" class="listwhitetext" ></td>
      <c:if test="${empty customerFile.id}">
        <td  align="center"  valign="bottom"  ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" height=17 width=50 onclick="notExists();"/></td>
      </c:if>
      <c:if test="${not empty customerFile.id}">
        <c:choose>
          <c:when test="${countEntitlementNotes == '0' || countEntitlementNotes == '' || countEntitlementNotes == null}">
            <td align="center"  valign="bottom"><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" height=17 width=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,580);" ></a></td>
          </c:when>
          <c:otherwise>
            <td  align="center" valign="bottom"  ><img id="countEntitlementNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" height=17 width=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,580);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Entitlement&imageId=countEntitlementNotesImage&fieldId=countEntitlementNotes&decorator=popup&popup=true',800,580);" ></a></td>
          </c:otherwise>
        </c:choose>
      </c:if>
    </tr>
    <tr>
      <td height="10" colspan="20"></td>
    </tr>
    </table>
    </div>
    </td>
    </tr>
	 <tr>
      <td height="10" colspan="6">
      <div id="parentAgentForOrderinitiationDiv">       
 <div onClick="javascript:animatedcollapse.toggle('removal')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">&nbsp;&nbsp;Removal/ Relocation&nbsp;Services </td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
               </div>
		<s:hidden name="newPage"/>
 <div id="removal" class="switchgroup1">
     <table border="0" class="detailTabLabel" cellpadding="0" width="100%">
	 <tbody>
	 <tr>
	<td >
	 <div id="activity" style="margin:0px;padding:0px;border:1px solid 	#C0C0C0; margin-left:0px; margin-bottom:0.3em;">
	 <c:choose>  
	 <c:when test="${newPage=='Y'}">
	 <c:if test="${empty customerFile.id}">
     <iframe src ="editRemovalRelocationNewService.html?decorator=asml&popup=true" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 <c:if test="${not empty customerFile.id}">
	 <iframe src ="editRemovalRelocationNewService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}" height="250px" style="margin-top:0px;padding-top:0px;" WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 </c:when>
	  <c:when test="${newPage=='N'}">
	  	 <c:if test="${empty customerFile.id}">
     <iframe src ="editRemovalRelocationService.html?decorator=asml&popup=true" height="250px" style="margin-top:0px;padding-top:0px;"  WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 <c:if test="${not empty customerFile.id}">
	 <iframe src ="editRemovalRelocationService.html?decorator=asml&popup=true&customerFileId=${customerFile.id}" height="250px" style="margin-top:0px;padding-top:0px;" WIDTH="100%" FRAMEBORDER=0 name="relo">
	   <p>Your browser does not support iframes.</p>
	 </iframe>
	 </c:if>
	 </c:when>
	 </c:choose>
	 </div>
	 </td>
	</tr>
    </table>
 </div>
						
 </td>  
  <%--  <s:hidden name="customerFile.portalIdActive"  /> 
   <s:hidden name="customerFileemail"   />
   <s:hidden name="customerFile.secondaryEmailFlag" /> 
   <s:hidden name="customerFile.customerPortalId" />
    
    --%>
   <s:hidden  name="customerFile.sequenceNumber"/> 
 <c:if test="${not empty customerFile.statusDate}">
 <s:text id="customerFileStatusDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.statusDate" /></s:text>
 <s:hidden  name="customerFile.statusDate" value="%{customerFileStatusDateFormattedValue}" />
 </c:if>
 <c:if test="${empty customerFile.statusDate}">
 <s:hidden   name="customerFile.statusDate"  />
 </c:if>
<s:hidden cssClass="list-menu" name="customerFile.statusReason" />
																																			
</tr>
 <configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
              <tr>						
						<td height="10" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('transferee')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Portal Detail
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="transferee" class="switchgroup1">							
						<table cellpadding="1" cellspacing="0" class="detailTabLabel">
<tr>													<c:set var="isActiveFlag" value="false"/>
													<c:if test="${customerFile.portalIdActive}">
														<c:set var="isActiveFlag" value="true"/>
													</c:if>
													<td width="131" align="right" valign="absmiddle" class="listwhitetext" >Customer Portal:</td>
    <td width="16" ><s:checkbox key="customerFile.portalIdActive" value="${isActiveFlag}" fieldValue="true" onclick="changeStatus(),checkPortalBoxId()" tabindex="84"/></td>
													<td width="95"  align="left" class="listwhitetext"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=customerFile.portalIdActive',this);return false" >
    <fmt:message key='customerFile.portalIdActive'/></a></td>
													<td width="40" align="right"  class="listwhitetext" >Email:&nbsp;</td>
    <td width="50" ><s:textfield cssClass="input-textUpper" name="customerFileemail"  value="%{customerFile.email}" readonly = "true" cssStyle="width:236px;" maxlength="65" tabindex="84"/></td>
	
	<c:set var="isSecondaryEmailFlag" value="false"/>
													<c:if test="${customerFile.secondaryEmailFlag}">
														<c:set var="isSecondaryEmailFlag" value="true"/>
													</c:if>
													<td width="119" align="right" class="listwhitetext" >Secondary Email:</td>
    <td width="16" ><s:checkbox key="customerFile.secondaryEmailFlag" value="${isSecondaryEmailFlag}" fieldValue="true" onclick="changeStatus(),checkPortalBoxSecondaryId()" tabindex="85"/></td>
	<td width="100" align="right"  class="listwhitetext" >Email Sent Date:</td>
    <td width="16" ><s:textfield cssClass="input-textUpper" cssStyle="width:65px;"  name="customerFile.emailSentDate" readonly="true" /></td>
												   
  </tr></table>
  
 <table  border="0" cellpadding="1" cellspacing="0" class="detailTabLabel">																																									
												<tr>
												  <td width="135" align="right" valign="absmiddle" class="listwhitetext"><fmt:message key='customerFile.customerPortalId'/></td>
													
												  <td align="left" width="155"><s:textfield cssClass="input-textUpper" name="customerFile.customerPortalId" required="true" size="15" maxlength="80" readonly="true" tabindex="85"/></td>
																								
													<c:if test="${not empty customerFile.id}">
													<td ><input type="button" value="Reset password and email to customer" class="cssbuttonB" style="width:240px;" onclick="generatePortalId();" tabindex="86"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td  align="left" ><input type="button" disabled value="Reset password and email to customer" class="cssbuttonB" style="width:240px;" onclick="generatePortalId();" tabindex="86"/></td>
													</c:if>
													<c:if test="${empty customerFile.id}">
													<td  align="right"  width="375" valign="bottom"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
													</c:if>
													<c:if test="${not empty customerFile.id}">
													<c:choose>
														<c:when test="${countCportalNotes == '0' || countCportalNotes == '' || countCportalNotes == null}">
														<td  align="right"  width="375" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:when>
														<c:otherwise>
														<td  align="right"  width="375" valign="bottom"><img id="countCportalNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=cportal&imageId=countCportalNotesImage&fieldId=countCportalNotes&decorator=popup&popup=true',800,600);" ></a></td>
														</c:otherwise>
													</c:choose> 
													</c:if>
												</tr>
										</table>										
						</div>
						</td>
						</tr>
						</configByCorp:fieldVisibility>
</table>

</div>
	</div>
		
		
		<div class="bottom-header" style="margin-top:50px; z-index:0"><span></span></div>
			</div>
		</div>
				<table border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${customerFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${customerFile.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{customerFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${customerFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="customerFile.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${customerFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{customerFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty customerFile.id}">
								<s:hidden name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
			</div>
<s:hidden name="componentId" value="customerFile" /> 
<%--<sec-auth:authComponent componentId="module.customerFileList.edit" replacementHtml=""> 	
<s:submit cssClass="cssbuttonA" method="save" key="button.save" onmouseover="completeTimeString();" onclick="return IsValidTime('save');"/>
				<c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/editCustomerFile.html"/>'" 
	        value="<fmt:message  key="button.add"/>"/> 
	    </c:if>  
		<s:hidden name="hitFlag" />
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editCustomerFile.html?id=${customerFile.id}"  />
		</c:if> 
<s:hidden name="submitCnt" value="<%=request.getParameter("submitCnt") %>"/>
<s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
	<c:choose>
		<c:when test="${submitType == 'job'}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" />
		</c:when>
		<c:otherwise>
		<c:if test="${not empty customerFile.id}">
			<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findCompanyDivisionByBookAg();disabledSODestinBtnReset();disabledSOBtnReset(); newFunctionForCountryState(); getContractReset();" />
		</c:if>
		<c:if test="${empty customerFile.id}">
		<s:reset type="button" cssClass="cssbutton1" key="Reset" onclick="findCompanyDivisionByBookAg();" />
		</c:if>
		</c:otherwise>
	 </c:choose>
	 <c:if test="${not empty customerFile.id}">
		    <input type="button" class="cssbutton1" 
	        onclick="location.href='<c:url   value="/addAndCopyCustomerFile.html?cid=${customerFile.id}"/>'" 
	        value="<fmt:message  key="button.addAndCopy"/>" style="width:110px; height:25px"/> 
	    </c:if>  
</sec-auth:authComponent>--%>
<s:hidden name="hitFlag" />
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/editOrderManagement.html?id=${customerFile.id}"  />
		</c:if> 
<s:submit cssClass="cssbuttonA" method="saveOrderManagement" key="button.save" onclick="return savevalidation();"/>		
<s:hidden name="customerFile.statusNumber" />	
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />




<s:text id="customerFileSystemDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="customerFile.systemDate" /></s:text>
<td valign="top"><s:hidden name="customerFile.systemDate" value="%{customerFileSystemDateFormattedValue}" /></td>
						
<s:hidden name="sendEmail" />
<c:if test="${parentAgentForOrderinitiation=='true'}">
<s:hidden name="removalRelocationService.isIta60" />
<s:hidden name="removalRelocationService.isIta1m3" />
<s:hidden name="removalRelocationService.isIta2m3" />
<s:hidden name="removalRelocationService.isIta3m3" />
<s:hidden name="removalRelocationService.isFtExpat20" />
<s:hidden name="removalRelocationService.isFtExpat40" />
<s:hidden name="removalRelocationService.isKgExpat30" />
<s:hidden name="removalRelocationService.isTransportAllow" />
<s:hidden name="removalRelocationService.isFtSingle20" />
<s:hidden name="removalRelocationService.isFtFamily40" />
<s:hidden name="removalRelocationService.isStorage" />
<s:hidden name="removalRelocationService.isOrientation8" />
<s:hidden name="removalRelocationService.isOrientation16" />
<s:hidden name="removalRelocationService.isOrientation24" />
<s:hidden name="removalRelocationService.isHomeSearch8" />
<s:hidden name="removalRelocationService.isHomeSearch16" />
<s:hidden name="removalRelocationService.isHomeSearch24" />
<s:hidden name="removalRelocationService.isHomeFurnished" />
<s:hidden name="removalRelocationService.isHomeUnfurnished" />
<s:hidden name="removalRelocationService.isUsdollar" />
<s:hidden name="removalRelocationService.isDollar" />
<s:hidden name="removalRelocationService.isOtherCurrency" />
<s:hidden name="removalRelocationService.isSchoolSearch8" />
<s:hidden name="removalRelocationService.isSchoolSearch16" />
<s:hidden name="removalRelocationService.isSchoolSearch24" />
<s:hidden name="removalRelocationService.isSelfInSearch8" />
<s:hidden name="removalRelocationService.isSelfInSearch16" />
<s:hidden name="removalRelocationService.isSelfInSearch24" />
<s:hidden name="removalRelocationService.id" />
<s:hidden name="removalRelocationService.customerFileId" />
<s:hidden name="removalRelocationService.corpId" />
<s:hidden name="removalRelocationService.usdollarAmounts" />
<s:hidden name="removalRelocationService.dollarAmounts" />
<s:hidden name="removalRelocationService.otherCurrencyAmounts" />
<s:hidden name="removalRelocationService.otherServicesRequired" />
<s:hidden name="removalRelocationService.ftExpat40ExceptionalItems" />
<s:hidden name="removalRelocationService.kgExpat30ExceptionalItems" />
<s:hidden name="removalRelocationService.transportAllowExceptionalItems" />
<s:hidden name="removalRelocationService.orientationTrip" />
<s:hidden name="removalRelocationService.homeSearch" />
<s:hidden name="removalRelocationService.settlingInAssistance" />
<s:hidden name="removalRelocationService.schoolSearch" />
<s:hidden name="removalRelocationService.authorizedDays" />
<s:hidden name="removalRelocationService.additionalNotes" />

<s:hidden name="removalRelocationService.furnished" />
<s:hidden name="removalRelocationService.unFurnished" />
<s:hidden name="removalRelocationService.entryVisa" />
<s:hidden name="removalRelocationService.longTermStayVisa" />
<s:hidden name="removalRelocationService.workPermit" />

</c:if>
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>

<input type="hidden" name="encryptPass" value="true" />

<s:hidden key="user.id"/>
<s:hidden key="user.version"/> 
</s:form>
<script type="text/javascript">
checkPetInvolve(document.forms['customerFileForm'].elements['customerFile.petsInvolved']);
function checkParentAgentForOrderinitiation(){
if(document.getElementById('parentAgentForOrderinitiation').value=='true'){
	   document.getElementById('parentAgentForOrderinitiationDiv').style.display = 'block';
	   document.getElementById('entitlementDiv').style.display = 'none';
}if(document.getElementById('parentAgentForOrderinitiation').value=='false'){
	   document.getElementById('parentAgentForOrderinitiationDiv').style.display = 'none';
	   document.getElementById('entitlementDiv').style.display = 'block';}
}
checkParentAgentForOrderinitiation();
try{
checkColumn();
}
catch(e){}
 try  {
 if(document.forms['customerFileForm'].elements['customerFile.assignmentType'].value=="Other"){ 
 document.getElementById('otherContract').style.display = 'block'; 
 }else
 {
 document.getElementById('otherContract').style.display ='none'; 
 }
 
 } 
 catch(e){}


try{
checkAllRelo();
}
catch(e){}

try{
cheackVisaRequired();
}
catch(e){}
try{
	var f = document.getElementById('customerFileForm'); 
	f.setAttribute("autocomplete", "off");
	}
	catch(e){}
	try{
	if('${stateshitFlag}'!= 1){
	getNewCoordAndSale();
	}}
	catch(e){}

	try{
 var ocountry='${customerFile.originCountry}';
 }
 catch(e){}
 try{
 var dcountry='${customerFile.destinationCountry}';
 }
 catch(e){}
 try{
 var hcountry='${customerFile.homeCountry}';
 }
 catch(e){}
  try{
	  var enbState = '${enbState}';
		 var homCon=document.forms['customerFileForm'].elements['customerFile.homeCountry'].value;
		 if(homCon=="")	 {
				document.getElementById('homeState').disabled = true;
				document.getElementById('homeState').value ="";
		 }else{
		  		if(enbState.indexOf(homCon)> -1){
					document.getElementById('homeState').disabled = false; 
					document.getElementById('homeState').value='${customerFile.homeState}';
				}else{
					document.getElementById('homeState').disabled = true;
					document.getElementById('homeState').value ="";
				} 
			}
		 }
	catch(e){}
 try{
	 var enbState = '${enbState}';
	 var oriCon=document.forms['customerFileForm'].elements['customerFile.originCountry'].value;
	 if(oriCon=="")	 {
			document.getElementById('originState').disabled = true;
			document.getElementById('originState').value ="";
	 }else{
	  		if(enbState.indexOf(oriCon)> -1){
				document.getElementById('originState').disabled = false; 
				document.getElementById('originState').value='${customerFile.originState}';
				
			}else{
				document.getElementById('originState').disabled = true;
				document.getElementById('originState').value ="";
				
			} 
		}
	 }
	catch(e){}
try{
	var desCon=document.forms['customerFileForm'].elements['customerFile.destinationCountry'].value;
	 var enbState = '${enbState}';
	 if(desCon=="")	 {
			document.getElementById('destinationState').disabled = true;
			document.getElementById('destinationState').value ="";
	 }else{
	  		if(enbState.indexOf(desCon)> -1){
				document.getElementById('destinationState').disabled = false;		
				document.getElementById('destinationState').value='${customerFile.destinationState}';
				document.getElementById('destinationStateRequired').style.display = 'block';
			    document.getElementById('destinationStateNotRequired').style.display = 'none';
			}else{
				document.getElementById('destinationState').disabled = true;
				document.getElementById('destinationState').value ="";
				document.getElementById('destinationStateRequired').style.display = 'none';
			    document.getElementById('destinationStateNotRequired').style.display = 'block';
			}
	 	}
	}
	catch(e){}	
try{
changeQuotationStatus()
}	
catch(e){}		
function billtoCheck(){
	var billToCode = document.forms['customerFileForm'].elements['customerFile.billToCode'].value;
	if(billToCode != '${customerFile.billToCode}')
	{
    if(billToCode!=''){
    	showOrHide(1)
	}
	changeStatus();
	//showOrHide(1)
	findBillToName();
	//setTimeout("showOrHide(0)",6000);
	copyBillToCode();
	getContract();
	findPricingBillingPaybaleBILLName();
	}
}	

function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}    
showOrHide(0);
 </script>
 
 <script type="text/javascript">
	function setEntitleValue(){
		var entitle = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
		if(entitle.length > 0){
			entitle = entitle.split('_?@').join(' '); 
			entitle = entitle.split('#?@');

			 for ( var i = 0; i < entitle.length; i++ ) {
				 if(entitle[i].length >0){
					 if(document.getElementById(entitle[i]) != null){
				 		document.getElementById(entitle[i]).checked=true
					 }
				 }
			 }
		}
	}

	
 function getValue(str) {
	 str=str.split(' ').join('_?@'); 
	 var partnerEnti = document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value;
	 if(partnerEnti.length == 0){
		 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = str+'#?@'+partnerEnti;
	 }else {
		 var currentTagTokens = partnerEnti.split( "#?@" );
		 var existingTags='';
		 var isExist = false;
		  for ( var i = 0; i < currentTagTokens.length; i++ ) {
		    if(currentTagTokens[i] != str){
			    if(currentTagTokens[i].length >0){
		    		existingTags = currentTagTokens[i]+'#?@'+existingTags;
			    }
		    }else{
		    	isExist = true;
		    }
		  }
		  if(isExist == false){
		  	document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags+'#?@'+str;
		  }else{
			  document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value = existingTags;
		  }
	 }
 }

 function getValue3() {
		document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value='';
		var parentEnt1='';
		<c:forEach var="entitlementMap" items="${entitlementMap}">
	       var entKey = "chk_<c:out value="${entitlementMap.key}"/>";
	       if(document.getElementById(entKey) != null && document.getElementById(entKey).checked == true){
	        	   entKey = entKey.split(' ').join('_?@'); 
	    			parentEnt1 = entKey+'#?@'+parentEnt1;
	       }
	 	</c:forEach>
		 document.forms['customerFileForm'].elements['customerFile.partnerEntitle'].value=parentEnt1;
	}

 function getFamilysize(str){
		var fSize = document.forms['customerFileForm'].elements['customerFile.familysize'].value;
//		alert(fSize);
		var no='<%= count %>';
		if(no == 0 && str != 'FS'){
			document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
			if(fSize == ''){
				document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
			}
			return;
		}
		if(no == 0 && str == 'FS'){
			if(parseInt(fSize) != fSize){
				 alert('Family size is not valid');
				 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
				 return false;
			 }else {
				 for(var i = 0;i < fSize.length;i++){
					 if(fSize.charAt(i) == '.' || fSize.charAt(i) == ' '){
						 alert('Family size is not valid');
						 document.forms['customerFileForm'].elements['customerFile.familysize'].value = '';
						 return false;
					 }
				 }
			 }
			document.forms['customerFileForm'].elements['customerFile.familysize'].value = fSize;
			document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= false;
		}else {
			document.forms['customerFileForm'].elements['customerFile.familysize'].readOnly= true;
			document.forms['customerFileForm'].elements['customerFile.familysize'].value = no;
		}
	}
 
 
 </script>

<script type="text/javascript">

var cFControlFlag =  '${customerFile.controlFlag}';
if(cFControlFlag == 'Q' || cFControlFlag == 'C'){
    document.forms['customerFileForm'].elements['customerFile.quotationStatus'].value = 'Accepted' ;
    document.forms['customerFileForm'].elements['customerFile.controlFlag'].value = 'C'
}


	setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();
	function clickOnRequest(){
		document.getElementById("customerFileForm_button_save").click();
	}
	function clickOnRequest(){
		document.getElementById("customerFileForm_button_save").click();
	}
try{
		findAssignmentFromParentAgent();
	}catch(e){}
try{
	stateOrigin();
	stateDestination();
	}catch(e){}
</script>