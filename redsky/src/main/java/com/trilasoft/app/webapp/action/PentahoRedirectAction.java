package com.trilasoft.app.webapp.action;

import java.rmi.server.UID;

import javax.servlet.http.Cookie;

import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.SSOLoginTokenManager;

public class PentahoRedirectAction extends BaseAction implements Preparable{

	private static Logger logger = Logger.getLogger(PentahoRedirectAction.class);
	
	private SSOLoginTokenManager ssoLoginTokenManager;
	private ClaimManager claimManager;
	private CompanyManager companyManager;
	private String user="X";
	private String token="Y";
	private String curr;
	private String corpid;
	private String scaling;
	private String pantahooUrl=(String)AppInitServlet.redskyConfigMap.get("pentah.url");
	public SSOLoginTokenManager getSsoLoginTokenManager() {
		return ssoLoginTokenManager;
	}

	public void setSsoLoginTokenManager(SSOLoginTokenManager ssoLoginTokenManager) {
		this.ssoLoginTokenManager = ssoLoginTokenManager;
	}
	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void prepare() throws Exception {
		logger.debug("Preparing to redirect to the Dispatch Console !!");
	}

	@Override
	public String execute() throws Exception {
		logger.debug("Redirecting to the Dispatch Console !!");
		try {
			if(getRequest().getUserPrincipal()!=null && getRequest().getUserPrincipal().getName()!=null ){
				setSSOHeaders(createLoginToken());
				return SUCCESS;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
		return ERROR;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getCorpid() {
		return corpid;
	}
	public String getCurr() {
		return curr;
	}
	
	public String getToken() {
		return token;
	}
	public String getScaling() {
		return scaling;
	}

	
	private void setSSOHeaders(Object loginToken) {
		//set username
		getRequest().setAttribute("sso_user", getRequest().getUserPrincipal().getName());
		//set token
		getRequest().setAttribute("sso_login_token", loginToken);
		//get cookies
		Cookie[] cookies = getRequest().getCookies();
		//setting for currency and corpid
		User userdetails = (User) claimManager.findUserDetails(getRequest().getRemoteUser()).get(0);
		Company company= companyManager.findByCorpID(userdetails.getCorpID()).get(0);		
		for(Cookie cookie: cookies){
			//set cookies
			getResponse().addCookie(cookie);
		}		
		
		this.token =  loginToken+"";
		//this.user = getRequest().getUserPrincipal().getName();
		this.corpid=userdetails.getCorpID();
		this.curr=company.getCurrencySign();
		this.curr=(this.curr).replace("&", "%26");
		this.curr=(this.curr).replace("#", "%23");
		this.scaling=company.getScaling();
		System.out.println("curralinggggggggggggggggggggggggggggggg......");
		System.out.println(curr);
		System.out.println("currlinggggggggggggggggggggggggggggggg......end................");
		String str1=getRequest().getUserPrincipal().getName()+"~"+corpid;
		Cookie ssoUserCookie = new Cookie("sso_user",str1);
		Cookie ssoLoginTokenCookie = new Cookie("sso_login_token",loginToken+"");
		
		
		ssoUserCookie.setMaxAge(100000000);
		ssoLoginTokenCookie.setMaxAge(100000000);
		getResponse().addCookie(ssoUserCookie);
		getResponse().addCookie(ssoLoginTokenCookie);
		String str=getRequest().getUserPrincipal().getName()+"~"+corpid;
		this.user = str;
		getResponse().setHeader("sso_user", str);
		getResponse().setHeader("sso_login_token", loginToken+"");
		/*this.token =  loginToken+"";
		this.user = getRequest().getUserPrincipal().getName();
		this.corpid=userdetails.getCorpID();
		this.curr=company.getCurrencySign();*/
	}

	/**
	 *   Set the username, logintoken, cookies to the outgoing request
	 *   This request will act as the SSO login request to the dispatch console  
	 */
	private String createLoginToken() throws Exception {
		UID loginToken = new UID();
		logger.info("Created login token: "+loginToken);
		registerLoginToken(loginToken.toString());
		return loginToken.toString();
	}

	/**
	 * 
	 * @param loginToken
	 * @throws Exception 
	 */
	private void registerLoginToken(String loginToken) throws Exception {
		String sessionId = null;
		try {
			Cookie[] cookies = getRequest().getCookies();
			for(Cookie cookie: cookies){
				if(cookie.getName().equalsIgnoreCase("jsessionid")){
					sessionId = cookie.getValue();
				}
			}
			logger.info("Registering login token : "+loginToken+" for session id "+sessionId);
			ssoLoginTokenManager.registerLoginToken(sessionId, loginToken, getRequest().getUserPrincipal().getName());
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Error registering login token for "+getRequest().getUserPrincipal().getName()+" session id "+sessionId);
		}
	}

	public String getPantahooUrl() {
		return pantahooUrl;
	}

	public void setPantahooUrl(String pantahooUrl) {
		this.pantahooUrl = pantahooUrl;
	}



}
