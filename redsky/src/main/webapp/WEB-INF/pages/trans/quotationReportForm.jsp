<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
	<meta name="heading" content="<fmt:message key='quotationReportForm.title'/>"/> 
	<title><fmt:message key="quotationReportForm.title"/></title>
	<style type="text/css">h2 {background-color: #FBBFFF}</style>	
</head>

<s:form id="apezForm" name="quotationReportForm" action="" method="post" validate="true">
<div id="Layer1" style="width:650px">
<div id="newmnav">
		  <ul>   
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Quotation Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  </ul>
</div><div class="spn">&nbsp;</div><br>
	
 <table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0" style="width:650px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>
		  	
		  	<tr>
		  		
		        <td align="right">From</td>
		  		<td align="left"><s:textfield name="serviceOrder.estimator" size="25" cssClass="input-text" readonly="true"/></td>
		  		<td align="right">To</td>
		  		<td align="left"><s:textfield name="serviceOrder.firstName" value="${name}" size="25" cssClass="input-text" readonly="true"/></td>
		  	</tr>
		  	 <tr>
		  		<td align="right" >Company</td>
		  		<td align="left"><s:textfield name="serviceOrder.billToName" size="25"  cssClass="input-text" readonly="true"/></td>
		  		<td align="right">E-mail</td>
		  		<td align="left"><s:textfield name="serviceOrder.email"  size="25"   cssClass="input-text" readonly="true"/></td>
		  	</tr>
		  	<tr>
		  	    <td align="right">Phone</td>
		  		<td align="left"><s:textfield name="serviceOrder.contactPhone" size="25"  cssClass="input-text" readonly="true"/></td>
		  	</tr>
 			<tr><td height="10px"></td></tr>
 			<tr>
		  	    <td style="height: 30px; vertical-align: middle;" align="center" colspan="4"><font style="font-size: medium;"><strong> Quote for Destination Services</strong></font></td>
		  	</tr>
		  	<tr><td height="10px"></td></tr>
 			<tr>
		  		<td align="right">RE:</td>
		  		<td align="left"><s:textfield name=""  value="${re}"size="25"  cssClass="input-text" readonly="true"/></td>
		  		<td align="right">TO:</td>
		  		<td align="left"><s:textfield name="" value="${to}"size="25"  cssClass="input-text" readonly="true"/></td>
		  	</tr> 
		  	 <tr>
		  	    <td align="right">Our Quote Number</td>
		  		<td align="left"><s:textfield name="serviceOrder.SequenceNumber" size="25"  cssClass="input-text" readonly="true"/></td>
		  	</tr> 
		  	<tr>
		  		<td align="right" class="listwhitetext" valign="top">Service Type :</td>
				<td align="left"><s:textarea name="notes" cols="35" rows="2" /></td>
			  	<td align="right" valign="top">Amount</td>
		  		<td align="left" valign="top"><s:textfield name="accountLine.estimaterevenueamount" size="25"  cssClass="input-text" readonly="true"/></td>
	        </tr>     
			<tr>
	 		    <td align="right" class="listwhitetext" valign="top"><fmt:message key="systemDefault.serviceInclude"/></td>
				<td align="left" colspan="3"><s:textarea name="systemDefault.serviceInclude" cols="60" rows="4" readonly="true"/></td>
			</tr>
			<tr><td height="10px"></td></tr>
			<tr>
			   <td align="right" class="listwhitetext" valign="top"><fmt:message key="systemDefault.serviceExclude"/></td>
				<td align="left"  colspan="3"><s:textarea name="systemDefault.serviceExclude" cols="60" rows="4" readonly="true"/></td>
			</tr>
			<tr><td height="10px"></td></tr>
			<tr>
			    <td align="right" class="listwhitetext" valign="top">Service:</td>
				<td align="left" colspan="3"><s:textarea name="customerFile.entitled" cols="60" rows="4" readonly="true"/></td>
			</tr>
		  
	</tbody>
</table>
</div>
<s:hidden name="apez.id"/>
</s:form>


		