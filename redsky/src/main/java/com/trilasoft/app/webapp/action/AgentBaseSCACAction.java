package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.Date;
import java.util.List;

import com.trilasoft.app.model.AgentBase;
import com.trilasoft.app.model.AgentBaseSCAC;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.service.AgentBaseSCACManager;
import com.trilasoft.app.service.PartnerPublicManager;

public class AgentBaseSCACAction extends BaseAction {

	private List SCACList;
	
	private List baseAgentSCAC;

	private Long id;

	private Long baseId;

	private Long maxId;
	
	private PartnerPublic partner;
	
	//private PartnerManager partnerManager;
	
	private PartnerPublicManager partnerPublicManager; 

	private AgentBase agentBase;
	
	private AgentBaseManager agentBaseManager;
	
	private AgentBaseSCAC agentBaseSCAC;
	
	private AgentBaseSCACManager agentBaseSCACManager;

	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;

	private String hitflag;
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(AgentBaseSCACAction.class);

	
	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}

	public AgentBaseSCACAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		SCACList = agentBaseSCACManager.getSCACList(sessionCorpID);
		agentBase = agentBaseManager.get(baseId);
		partner = partnerPublicManager.get(Long.parseLong(partnerPublicManager.findPartnerCode(agentBase.getPartnerCode()).get(0).toString()));
		if (id != null) {
			agentBaseSCAC = agentBaseSCACManager.get(id);
		} else {
			agentBaseSCAC = new AgentBaseSCAC();
			agentBaseSCAC.setCorpID(sessionCorpID);
			agentBaseSCAC.setPartnerCode(agentBase.getPartnerCode());
			agentBaseSCAC.setBaseCode(agentBase.getBaseCode());

			agentBaseSCAC.setCreatedOn(new Date());
			agentBaseSCAC.setUpdatedOn(new Date());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		SCACList = agentBaseSCACManager.getSCACList(sessionCorpID);
		agentBase = agentBaseManager.get(baseId);
		partner = partnerPublicManager.get(Long.parseLong(partnerPublicManager.findPartnerCode(agentBase.getPartnerCode()).get(0).toString()));
		if (cancel != null) {
			return "cancel";
		}

		agentBaseSCAC.setCorpID(sessionCorpID);
		boolean isNew = (agentBaseSCAC.getId() == null);
		List isexisted=agentBaseSCACManager.isExisted(agentBase.getBaseCode(), agentBase.getPartnerCode(), agentBaseSCAC.getSCACCode(), sessionCorpID);
		if(!isexisted.isEmpty() && isNew){
			errorMessage("Base SCAC code already exist.");
			return INPUT; 
		}else{ 
			if (isNew) {
				agentBaseSCAC.setCreatedOn(new Date());
			}
			agentBaseSCAC.setUpdatedOn(new Date());
			agentBaseSCACManager.save(agentBaseSCAC);
			hitflag = "1";
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "agentBaseSCAC.added" : "agentBaseSCAC.updated";
				saveMessage(getText(key));
			}
			if (!isNew) {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return INPUT;
			} else {
				
				baseAgentSCAC = agentBaseSCACManager.getbaseSCACList(agentBase.getPartnerCode(), agentBase.getBaseCode(), sessionCorpID);
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		}
	}


	public String deleteBaseSCAC() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		agentBaseSCACManager.remove(id);
		saveMessage(getText("agentBaseSCAC.deleted"));
		id = baseId;
		list();
		hitflag = "1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		agentBase = agentBaseManager.get(id);
		partner = partnerPublicManager.get(Long.parseLong(partnerPublicManager.findPartnerCode(agentBase.getPartnerCode()).get(0).toString()));
		baseAgentSCAC = agentBaseSCACManager.getbaseSCACList(agentBase.getPartnerCode(), agentBase.getBaseCode(), sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	

	public void setId(Long id) {
		this.id = id;
	}

	public AgentBase getAgentBase() {
		return agentBase;
	}

	public void setAgentBase(AgentBase agentBase) {
		this.agentBase = agentBase;
	}

	public void setAgentBaseManager(AgentBaseManager agentBaseManager) {
		this.agentBaseManager = agentBaseManager;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public Long getBaseId() {
		return baseId;
	}

	public void setBaseId(Long baseId) {
		this.baseId = baseId;
	}

	public List getBaseAgentSCAC() {
		return baseAgentSCAC;
	}

	public void setBaseAgentSCAC(List baseAgentSCAC) {
		this.baseAgentSCAC = baseAgentSCAC;
	}

	public AgentBaseSCAC getAgentBaseSCAC() {
		return agentBaseSCAC;
	}

	public void setAgentBaseSCAC(AgentBaseSCAC agentBaseSCAC) {
		this.agentBaseSCAC = agentBaseSCAC;
	}

	public List getSCACList() {
		return SCACList;
	}

	public void setSCACList(List list) {
		SCACList = list;
	}

	public void setAgentBaseSCACManager(AgentBaseSCACManager agentBaseSCACManager) {
		this.agentBaseSCACManager = agentBaseSCACManager;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public PartnerPublic getPartner() {
		return partner;
	}

	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}

}