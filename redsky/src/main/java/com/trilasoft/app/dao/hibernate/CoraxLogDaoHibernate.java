package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.CoraxLogDao;
import com.trilasoft.app.model.CoraxLog;


public class CoraxLogDaoHibernate extends GenericDaoHibernate<CoraxLog, Long> implements CoraxLogDao{

	public CoraxLogDaoHibernate() {
		super(CoraxLog.class);
		// TODO Auto-generated constructor stub
	}
	
	public List getByCorpID(String sessionCorpID){
		return  getHibernateTemplate().find("from CoraxLog where corpID =?",sessionCorpID);
    }

}
