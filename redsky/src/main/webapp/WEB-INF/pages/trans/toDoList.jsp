<%@ include file="/common/taglibs.jsp"%> 


<head> 

<title><fmt:message key="toDoList.title"/></title> 

<meta name="heading" content="<fmt:message key='toDoListList.heading'/>"/> 

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=4;i++)
			{
					document.forms['toDoList'].elements[i].value = "";
			}
			document.forms['toDoList'].elements['isAgentTdr'].checked = false;
}

function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
        if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}

var r={
 'special':/['\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'\_'&'\-']/g,
 'quotes':/['\''&'\"']/g,
 'notnumbers':/[^\d]/g
};

function valid(targetElement,w){
 targetElement.value = targetElement.value.replace(r[w],'');
}

</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;
margin-top:-17px;
!margin-top:-19px;
padding:2px 0px;
text-align:right;
width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>
</head> 
<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/rules.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set> 

<s:form id="toDoLists" name="toDoList" action="searchToDoRules" method="post" validate="true">   
<div id="Layer1" style="width:100%">

<DIV ID="layerH" style="position:absolute;width:350px;height:10px;left:250px;top:320px; background-color:white;z-index:150;">
	
	<td align="justify"  class="listwhitetext" valign="middle"><font size="4" color="#1666C9"><b>Rules Execution is in progress....</b></font></td>
		
	<!--<img src="<c:url value='/images/prg_bar.gif'/>" />
	-->
</DIV>
	
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" >
<thead>
<tr>
<th>Rule Id</th>
<th>Field To Validate</th>
<th><fmt:message key="toDoList.testdate"/></th>
<th><fmt:message key="toDoList.entitytablerequired"/></th>
<th><fmt:message key="toDoList.messagedisplayed"/></th>
<th><fmt:message key="toDoRule.status"/></th>
<th><fmt:message key="toDoRule.checkEnable"/></th>
<th><fmt:message key="toDoRule.agentTdr"/></th>
</tr></thead> 
<tbody>
  <tr>
  <td>
       <s:textfield name="toDoRule.ruleNumber" required="true" cssClass="input-text" size="3" onkeydown="return onlyNumsAllowed(event,this,'special')" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
   </td>
   <td>
       <s:textfield name="toDoRule.fieldToValidate1" required="true" cssClass="input-text" size="18"/>
   </td>
   <td>
       <s:textfield name="toDoRule.testdate" required="true" cssClass="input-text" size="18"/>
   </td>
    <td>
       <s:textfield name="toDoRule.entitytablerequired" required="true" cssClass="input-text" size="18"/>
   </td>
   <td>
       <s:textfield name="toDoRule.messagedisplayed" required="true" cssClass="input-text" size="18"/>
   </td>
   
    <td>
    	<s:select cssClass="list-menu"   name="toDoRule.status" list="{'Tested','Error'}" headerKey="All" headerValue="All" cssStyle="width:120px"/>
       
   </td>
   
   <td>
    	<s:select cssClass="list-menu"   name="toDoRuleCheckEnable" list="{'true','false'}" headerKey="All" headerValue="All" cssStyle="width:120px"/>
       
   </td>
   	<td align="center" class="listwhitetext" width="" ><s:checkbox key="isAgentTdr"  value="${isAgentTdr}"  onclick="" /></td>
   </tr>
   <tr>
   <td colspan="7"></td>
   <td style="border-left: hidden;">
       <s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" method="" key="button.search"/>  
       <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
       
   </td>
   
  </tr>
  </tbody>
 </table>
 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" /> 





<div id="otabs" style="margin-top:-15px; ">
		  <ul>
		    <li><a class="current"><span>Rules</span></a></li>
		    <li><a href="docCheckLists.html" style="cursor: pointer;"><span>Doc Check List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>


<s:set name="toDoRules" value="toDoRules" scope="request"/>
<display:table name="toDoRules" class="table" requestURI="" id="toDoList"  defaultsort="1" export="true" pagesize="10" style="margin-top:1px;"> 
<display:column headerClass="containeralign" style="text-align: right" property="ruleNumber" sortable="true" href="rules.html" paramId="id" paramProperty="id" titleKey="toDoList.id"/> 
<display:column property="entitytablerequired" sortable="true" titleKey="toDoList.entitytablerequired"/> 
<display:column property="messagedisplayed" sortable="true" titleKey="toDoList.messagedisplayed"/> 
<display:column property="testdate" sortable="true" titleKey="toDoList.testdate"/> 
<display:column  sortable="true" title="Validate Field">
<c:choose> 
<c:when test="${toDoList.fieldToValidate1!='' && toDoList.fieldToValidate2!='' && toDoList.fieldToValidate3!=''}">
<c:out value="${toDoList.fieldToValidate1}" />, <c:out value="${toDoList.fieldToValidate2}" />, <c:out value="${toDoList.fieldToValidate3}" />
</c:when>
<c:when test="${toDoList.fieldToValidate1!='' && toDoList.fieldToValidate2!='' && toDoList.fieldToValidate3==''}">
<c:out value="${toDoList.fieldToValidate1}" />, <c:out value="${toDoList.fieldToValidate2}" />
</c:when> 
<c:when test="${toDoList.fieldToValidate1!='' && toDoList.fieldToValidate2=='' && toDoList.fieldToValidate3!=''}">
<c:out value="${toDoList.fieldToValidate1}" />, <c:out value="${toDoList.fieldToValidate3}" />
</c:when> 
<c:otherwise>
<c:out value="${toDoList.fieldToValidate1}" />
</c:otherwise>
</c:choose>

</display:column>
<display:column property="durationAddSub" sortable="true" title="Days&nbsp;+/-"/>  
<display:column property="rolelist" sortable="true" titleKey="toDoList.Role"/> 
<c:if test="${toDoList.status=='Tested' }">
<display:column  titleKey="toDoRule.status" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif"  /></display:column> 
</c:if>
<c:if test="${toDoList.status!='Tested' }">
<display:column  titleKey="toDoRule.status" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
</c:if>
<c:if test="${toDoList.checkEnable=='true' }">
<display:column  titleKey="toDoRule.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/tick01.gif" /></display:column> 
</c:if>
<c:if test="${toDoList.checkEnable!='true' }">
<display:column  titleKey="toDoRule.checkEnable" style="text-align: center"><img src="${pageContext.request.contextPath}/images/cancel001.gif"  /></display:column> 
</c:if>

<display:column  title="Agent&nbsp;TDR" style="text-align: center">
	<c:if test="${toDoList.publishRule=='true' }">
		<img src="${pageContext.request.contextPath}/images/tick01.gif" />
	</c:if>
	 <c:if test="${toDoList.publishRule!='true' }">
	 <img src="${pageContext.request.contextPath}/images/cancel001.gif"  />
	</c:if> 
</display:column> 

    <display:setProperty name="export.excel.filename" value="toDo List.xls"/>   
    <display:setProperty name="export.csv.filename" value="toDo List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="toDo List.pdf"/> 
</display:table> 



<c:out value="${buttons}" escapeXml="false" /> 
<td align="right">
<input type="button" class="cssbutton1" value="Execute Rules" style="width:100px; height:25px" onclick="progressBar('1');location.href='<c:url value="/buildAllRules.html"/>'" />
</td>
</div>

</s:form>

<script type="text/javascript"> 
try{
showOrHide(0);
}
catch(e){}
highlightTableRows("toDoList"); 
try{
   	if('${isAgentTdr}'=='true')
   	{
   		document.forms['toDoList'].elements['isAgentTdr'].checked = true;
   	}
   	}
   	catch(e){}
   try{
   if('${isAgentTdr}'=='false')
   	{
   		document.forms['toDoList'].elements['isAgentTdr'].checked = false;
   	}
   	}
   	catch(e){}

</script> 

