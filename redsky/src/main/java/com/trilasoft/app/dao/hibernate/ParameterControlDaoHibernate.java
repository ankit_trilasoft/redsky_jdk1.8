package com.trilasoft.app.dao.hibernate;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ParameterControlDao;
import com.trilasoft.app.model.ParameterControl;
public class ParameterControlDaoHibernate extends GenericDaoHibernate<ParameterControl,Long> implements ParameterControlDao{
	  ParameterControlDaoHibernate(){
		  super(ParameterControl.class);
		  		  
	  }
	  public List findUseCorpId(){
		  List list;
		  list=getHibernateTemplate().find("select corpID from Company");
		  //System.out.println("test"+list);
		  return list;
		  
	  }
	public Boolean checkTsftFlag(String parameter2, String sessionCorpID2) {
		Boolean tsftFlag=false;
		List tsftFlagList= getHibernateTemplate().find("select tsftFlag from ParameterControl where parameter='"+parameter2+"'");
		if(tsftFlagList.get(0).toString().equalsIgnoreCase("true") || tsftFlagList.get(0).toString().equalsIgnoreCase("1")){
			tsftFlag=true;
		}
		return tsftFlag;
		
	}
	public Boolean checkHybridFlag(String parameter2, String sessionCorpID2) {
		Boolean hybridFlag=false;
		List hybridFlagList= getHibernateTemplate().find("select hybridFlag from ParameterControl where parameter='"+parameter2+"'");
		if(hybridFlagList.get(0).toString().equalsIgnoreCase("true") || hybridFlagList.get(0).toString().equalsIgnoreCase("1")){
			hybridFlag=true;
		}
		return hybridFlag;
	}
	public List<ParameterControl> findByFields(String parameter,String customType,Boolean active,Boolean tsftFlag,Boolean hybridFlag){
		List<ParameterControl> list;
		list=getHibernateTemplate().find("from ParameterControl where parameter like '%"+parameter+"%' AND customType like '%"+customType+"%' AND active is  "+active+" AND tsftFlag is "+tsftFlag+" AND hybridFlag is "+hybridFlag+"");
		return list;
		
	}
	public List getAllParameter(String parameter){
		List list;
		list=getHibernateTemplate().find("select parameter from ParameterControl where parameter not in ('"+parameter+"')  AND active is  true AND tsftFlag is true AND hybridFlag is true order by parameter");
		return list;
		
	}
	
	public String getDescription(String parameter,String corpId){
		String str="";
		String query="select REPLACE(if(description=null,'',description),'  ','') from parametercontrol where parameter= '"+ parameter + "' and corpID in ('TSFT','"+corpId+"')";
		List al=this.getSession().createSQLQuery(query).list();
		if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
			str=al.get(0).toString().trim();
		}
		return str;
	}
	

}
