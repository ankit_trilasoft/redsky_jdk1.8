package com.trilasoft.app.dao.hibernate;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ExtractColumnMgmtDao;
import com.trilasoft.app.dao.UserlogfileDao;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.stoExtractDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ExtractColumnMgmt;
import com.trilasoft.app.model.Userlogfile;

public class ExtractColumnMgmtDaoHibernate extends GenericDaoHibernate<ExtractColumnMgmt, Long> implements ExtractColumnMgmtDao {
	private HibernateUtil hibernateUtil;
	   
	public ExtractColumnMgmtDaoHibernate() {   
        super(ExtractColumnMgmt.class);   
    }
	
	public List getExtractColum(String reportName, String sessionCorpID) {
		
		return getHibernateTemplate().find("from ExtractColumnMgmt where reportName='"+reportName+"' and corpID in ('" + sessionCorpID + "','XXXX','TSFT','NULL') order by displayName asc");
	}
	public  class DTOExtractColumnMgmt{ 
		private Object fieldName;

		public Object getFieldName() {
			return fieldName;
		}

		public void setFieldName(Object fieldName) {
			this.fieldName = fieldName;
		}
	}
	public List getSelectCondition(String selectCondition,String reportName,String sessionCorpID) {
		List  fieldNameList = new ArrayList();
		DTOExtractColumnMgmt dtoExtractColumnMgmt;
		String query="SELECT distinct fieldName FROM extractcolumnmgmt where reportName='"+reportName+"' and columnSequenceNumber in ("+selectCondition+") and corpID in ('" + sessionCorpID + "','TSFT') order by columnSequenceNumber";
		List <Object> fieldNameListNew = new ArrayList<Object>();
		fieldNameList= this.getSession().createSQLQuery(query).list();
		
		return fieldNameList;
}
	
	public List getReportName(String corpid){
		List reportNameList;
		if(corpid.equalsIgnoreCase("TSFT")){
			reportNameList = getHibernateTemplate().find("select distinct  reportName from ExtractColumnMgmt where reportName IS NOT NULL order by reportName");
		}else{
			String query = "select distinct  reportName from ExtractColumnMgmt where reportName IS NOT NULL and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') order by reportName";
			reportNameList = getHibernateTemplate().find(query);
		}
		return reportNameList;
	}
	
	public List getTableName(String corpid){
		List tableNameList;
		if(corpid.equalsIgnoreCase("TSFT")){
			tableNameList = getHibernateTemplate().find("select distinct  tableName from ExtractColumnMgmt where tableName IS NOT NULL order by tableName");
		}else{
			String query = "select distinct  tableName from ExtractColumnMgmt where tableName IS NOT NULL and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') order by tableName";
			tableNameList = getHibernateTemplate().find(query);
		}
		return tableNameList;
	}
	
	public List getFieldNameList(String corpid){
		List fildNameList;
		if(corpid.equalsIgnoreCase("TSFT")){
			fildNameList = getHibernateTemplate().find("select distinct  fieldName from ExtractColumnMgmt where fieldName IS NOT NULL order by fieldName");
		}else{
			String query = "select distinct  fieldName from ExtractColumnMgmt where fieldName IS NOT NULL and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') order by fieldName";
			fildNameList = getHibernateTemplate().find(query);
		}
		return fildNameList;
	}
	
	public List getDisplayNameList(String corpid){
		List displayNameList;
		if(corpid.equalsIgnoreCase("TSFT")){
			displayNameList = getHibernateTemplate().find("select distinct  displayName from ExtractColumnMgmt  where displayName IS NOT NULL order by displayName");
		}else{
			String query = "select distinct  displayName from ExtractColumnMgmt where displayName IS NOT NULL and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"') order by displayName";
			displayNameList = getHibernateTemplate().find(query);
		}
		return displayNameList;
	}
	
	public List getTableNameByReportName(String reportName, String corpid){
		List tableNameList;
		if(corpid.equalsIgnoreCase("TSFT")){
			tableNameList = getHibernateTemplate().find("select distinct  tableName from ExtractColumnMgmt where reportName='"+reportName +"' and corpID='" +corpid+"'");
		}else{
			String query = "select distinct  tableName from ExtractColumnMgmt where reportName='"+reportName +"' and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpid)+"','"+corpid+"')";
			tableNameList = getHibernateTemplate().find(query);
		}
		return tableNameList;
		
	}
	
	public boolean checkExtractColumnMgmtRecord(String reportName, String tableName, String fieldName, String corpid){
		boolean flag = false;
		List checkRecordList = new ArrayList();
		try{
			String query = null;
			if(corpid.equalsIgnoreCase("TSFT")){
				query = "select fieldName from ExtractColumnMgmt where reportName= ? and tableName= ? and fieldName=?";
				checkRecordList = this.getSession().createQuery(query).setParameter(0, reportName).setParameter(1, tableName).setParameter(2, fieldName).list();
				//checkRecordList = this.getSession().createSQLQuery("select fieldName from extractcolumnmgmt where reportName= "+reportName  +" and tableName= "+tableName +" and fieldName="+fieldName).list();
			}else{
				query = "select fieldName from ExtractColumnMgmt where reportName= ? and tableName= ? and fieldName=? and corpid in ('"+corpid+"','TSFT')";
				checkRecordList = this.getSession().createQuery(query).setParameter(0, reportName).setParameter(1, tableName).setParameter(2, fieldName).list();
				//checkRecordList = this.getSession().createSQLQuery("select fieldName from extractcolumnmgmt where reportName= '"+reportName  +"' and tableName= '"+tableName +"' and fieldName='"+fieldName+"' and corpid in ('"+corpid+"','TSFT')").list();
			}
			
			if(checkRecordList.size()<1){
				flag = true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return flag;
	}
	
	public String checkExtractColumnMgmtRecordId(String reportName, String tableName, String fieldName, String corpid){
		String id = "";
		List checkRecordList = new ArrayList();
		try{
			String query = null;
			if(corpid.equalsIgnoreCase("TSFT")){
				query = "select id from ExtractColumnMgmt where reportName= ? and tableName= ? and fieldName=?";
				checkRecordList = this.getSession().createQuery(query).setParameter(0, reportName).setParameter(1, tableName).setParameter(2, fieldName).list();
				//checkRecordList = this.getSession().createSQLQuery("select id from extractcolumnmgmt where reportName= '"+reportName  +"' and tableName= '"+tableName +"' and fieldName='"+fieldName+"'").list();
			}else{
				query = "select id from ExtractColumnMgmt where reportName= ? and tableName= ? and fieldName=? and corpid in ('"+corpid+"','TSFT')";
				checkRecordList = this.getSession().createQuery(query).setParameter(0, reportName).setParameter(1, tableName).setParameter(2, fieldName).list();
				//checkRecordList = this.getSession().createSQLQuery("select id from extractcolumnmgmt where reportName= '"+reportName  +"' and tableName= '"+tableName +"' and fieldName='"+fieldName+"' and corpid in ('"+corpid+"','TSFT')").list();
			}
			System.out.println("query is : "+query);
			if(checkRecordList.size()>=1){
				id=checkRecordList.get(0).toString();
			}
		}catch(Exception e){
			System.out.println("from catch block of checkExtractColumnMgmtRecordId ");
			id="000";
			e.printStackTrace();
		}
		return id;
	}
	
	public Long getExtractColMgmtSeqNo(String corpId){
		long columnSeqNo = 0;
		String columnSeqNo1=null;
		//try{
		List colSeqNoList = getHibernateTemplate().find("select max(columnSequenceNumber) from ExtractColumnMgmt where reportName=?",corpId);
		if( colSeqNoList.contains(null) || colSeqNoList.contains("")){
			return columnSeqNo = 1;
		}else{
		Iterator it = colSeqNoList.iterator();
		if(it.hasNext()){
		columnSeqNo1=it.next().toString();
		}
		columnSeqNo=Integer.parseInt(columnSeqNo1)+1;
		}
		/*}catch(Exception e){
			e.printStackTrace();
			return columnSeqNo = 1;
		}*/
		return columnSeqNo;
	}
	
	public List searchFilterList(String reportName, String tableName, String fieldNameSearch, String displayName, String corpId, boolean tsftflag){
		String createdOnStr = "";
		//String query = "from ExtractColumnMgmt where (reportName like '%" +reportName+"%' or reportName='' or reportName is null) and (tableName like '%" +tableName+"%' or tableName='' or tableName is null) and (displayName like '%" +displayName+"%' or displayName='' or displayName is null) and corpId ='" +corpId+"'";
		//String query = "from ExtractColumnMgmt where (reportName like '%" +reportName+"%' or reportName='' or reportName is null) and (tableName like '%" +tableName+"%' or tableName='' or tableName is null) and (fieldName like '%" +fieldNameSearch+"%' or fieldName='' or fieldName is null) and (displayName like '%" +displayName+"%' or displayName='' or displayName is null) and (corpId like '%" +corpId+"%' or corpId='' or corpId is null)";
		String query = null;
		if(tsftflag){
		if(corpId.equalsIgnoreCase("")){
			query = "from ExtractColumnMgmt where (reportName like '%" +reportName+"%' or reportName='' or reportName is null) and (tableName like '%" +tableName+"%' or tableName='' or tableName is null) and (fieldName like '%" +fieldNameSearch+"%' or fieldName='' or fieldName is null) and (displayName like '%" +displayName+"%' or displayName='' or displayName is null) and (corpId like '%" +corpId+"%' or corpId='' or corpId is null)";
		}else{
			query = "from ExtractColumnMgmt where (reportName like '%" +reportName+"%' or reportName='' or reportName is null) and (tableName like '%" +tableName+"%' or tableName='' or tableName is null) and (fieldName like '%" +fieldNameSearch+"%' or fieldName='' or fieldName is null) and (displayName like '%" +displayName+"%' or displayName='' or displayName is null) and corpId in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')";
		}
		}else{
			query = "from ExtractColumnMgmt where (reportName like '%" +reportName+"%' or reportName='' or reportName is null) and (tableName like '%" +tableName+"%' or tableName='' or tableName is null) and (fieldName like '%" +fieldNameSearch+"%' or fieldName='' or fieldName is null) and (displayName like '%" +displayName+"%' or displayName='' or displayName is null) and corpId in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')";
		}
		
		List searchFilterList = getHibernateTemplate().find(query);
		return searchFilterList;
	}
	
	public List searchDefaultList(String corpId){
		List searchDefaultList;
		if(corpId.equalsIgnoreCase("TSFT")){
			searchDefaultList = getHibernateTemplate().find("from ExtractColumnMgmt");
		}else{
			String query = "from ExtractColumnMgmt where corpId in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"')"; 
			searchDefaultList = getHibernateTemplate().find(query);
		}
		return searchDefaultList;
	}
	
	public List getCorpIdList(){
		List corpIdList = getHibernateTemplate().find("SELECT corpID FROM Company order by corpID");
		return corpIdList;
	}
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
}
