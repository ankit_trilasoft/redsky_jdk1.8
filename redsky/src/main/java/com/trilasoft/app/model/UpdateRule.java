package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="updaterule")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public  class UpdateRule  extends BaseObject {
	
	private String corpID; 
	private Long id;
	private String  tableName;
	private String  fieldToUpdate;
	private String  validationContion;
	private String  updateFieldValue;
	private String  ruleName;
	private boolean checkEnable;
	private String  ruleStatus;
	private String  remarks;
	private String  updatedBy;
	private Date updatedOn;
	private String  createdBy;
	private Date createdOn;
	private Long ruleNumber;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpID", corpID)
				.append("tableName", tableName)
				.append("fieldToUpdate", fieldToUpdate)
				.append("validationContion", validationContion)
				.append("updateFieldValue", updateFieldValue)
				.append("ruleName", ruleName)
				.append("checkEnable", checkEnable)
				.append("ruleStatus", ruleStatus)
				.append("remarks", remarks)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("ruleNumber", ruleNumber)
				.toString();
	}
		
		
		@Override
		public boolean equals(final Object other) {
			UpdateRule castOther = (UpdateRule) other;
			return new EqualsBuilder()
			.append(id, castOther.id)
			.append(corpID, castOther.corpID)
			.append(tableName, castOther.tableName)
			.append(fieldToUpdate, castOther.fieldToUpdate)
			.append(validationContion, castOther.validationContion)
			.append(updateFieldValue, castOther.updateFieldValue)
			.append(ruleName, castOther.ruleName)
			.append(checkEnable, castOther.checkEnable)
			.append(ruleStatus, castOther.ruleStatus)
			.append(remarks, castOther.remarks)
			.append(updatedBy, castOther.updatedBy)
			.append(updatedOn, castOther.updatedOn)
			.append(createdBy,castOther.createdBy)
			.append(createdOn,castOther.createdOn)
			.append(ruleNumber,castOther.ruleNumber)
			.isEquals();
		}
		
		
		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(corpID)
					.append(tableName).append(fieldToUpdate).append(validationContion)
					.append(updateFieldValue).append(ruleName).append(checkEnable)
					.append(ruleStatus).append(remarks).append(updatedBy).append(updatedOn)
					.append(createdBy).append(createdOn).append(ruleNumber)
					.toHashCode();
		}

	
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column
	public String getFieldToUpdate() {
		return fieldToUpdate;
	}
	public void setFieldToUpdate(String fieldToUpdate) {
		this.fieldToUpdate = fieldToUpdate;
	}
	@Column
	public String getValidationContion() {
		return validationContion;
	}
	public void setValidationContion(String validationContion) {
		this.validationContion = validationContion;
	}
	@Column
	public String getUpdateFieldValue() {
		return updateFieldValue;
	}
	public void setUpdateFieldValue(String updateFieldValue) {
		this.updateFieldValue = updateFieldValue;
	}
	@Column
	public String getRuleName() {
		return ruleName;
	}
	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}
	@Column
	public boolean isCheckEnable() {
		return checkEnable;
	}
	public void setCheckEnable(boolean checkEnable) {
		this.checkEnable = checkEnable;
	}
	@Column
	public String getRuleStatus() {
		return ruleStatus;
	}
	public void setRuleStatus(String ruleStatus) {
		this.ruleStatus = ruleStatus;
	}
	@Column
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column
	public Long getRuleNumber() {
		return ruleNumber;
	}


	public void setRuleNumber(Long ruleNumber) {
		this.ruleNumber = ruleNumber;
	}

	
	
	
	
	

}
