package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ExtractQueryFileDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AccountContact;
import com.trilasoft.app.model.ExtractQueryFile;

public class ExtractQueryFileDaoHibernate extends GenericDaoHibernate<ExtractQueryFile, Long> implements ExtractQueryFileDao {
	private HibernateUtil hibernateUtil; 


	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public ExtractQueryFileDaoHibernate() {
		super(ExtractQueryFile.class);
		
	}  
	public List oldQueries(String userId) {
		return getHibernateTemplate().find("from ExtractQueryFile where (publicPrivateFlag='public' or userId='"+userId+"')");
		
	}
	static String replaceWord(String original, String find, String replacement) {
		int i = original.indexOf(find);
		if (i < 0) {
			return original; // return original if 'find' is not in it.
		}
		String partBefore = original.substring(0, i);
		String partAfter = original.substring(i + find.length());
		return partBefore + replacement + partAfter;
	}
	public List searchExtractQueryFiles(String queryName,String publicPrivateFlag,String modifiedby,String userId){
		if(queryName==null)
		{
			queryName="";
		}
		if(publicPrivateFlag==null)
		{
			publicPrivateFlag="";
		}
		if(modifiedby==null)
		{
			modifiedby="";
		}
		if(userId==null)
		{
			userId="";
		}
		
		return getHibernateTemplate().find( "from ExtractQueryFile where (publicPrivateFlag='public' or userId='"+userId+"') and queryName like '" + replaceWord(queryName,"'", "''") + "%' AND publicPrivateFlag like '" + replaceWord(publicPrivateFlag,"'", "''") + "%'   AND modifiedby like '" + replaceWord(modifiedby,"'", "''") + "%'");
	}
	public List fingQueryName(String remoteUser, String sessionCorpID, String queryName, String publicPrivateFlag) {
		if(publicPrivateFlag.toString().trim().equals("public")){
		return getHibernateTemplate().find("select count(queryName) from ExtractQueryFile where queryName='"+queryName+"' and corpID='"+sessionCorpID+"'");
	}
		else {
			return this.getSession().createSQLQuery("(select count(queryName) from extractqueryfile where queryName='"+queryName+"' and publicPrivateFlag='private' and userId='"+remoteUser+"' and corpID='"+sessionCorpID+"') UNION (select queryName from extractqueryfile where queryName='"+queryName+"' and publicPrivateFlag='public' and corpID='"+sessionCorpID+"');").list();
			//return getHibernateTemplate().find("select queryName from ExtractQueryFile where queryName='"+queryName+"'and publicPrivateFlag='public' and userId='"+remoteUser+"' and corpID='"+sessionCorpID+"'");
		}
	}

	public List getSchedulerQueryList() {
		
		return this.getSession().createSQLQuery("select e.id  from extractqueryfile e inner join app_user a ON e.createdBy = a.username where e.autoGenerateReoprt is true and e.email <> '' and e.email is not null and a.account_enabled is true and e.corpID in (select corpID from company where status is true)").list();
	}

	public void updateSendMailStatus(Long id, String lastEmailSent, String emailStatus, String emailMessage) {
		getHibernateTemplate().bulkUpdate("update ExtractQueryFile set emailDateTime=now(),lastEmailSent='"+lastEmailSent+"',emailStatus='"+emailStatus+"',emailMessage='"+emailMessage+"' where id='"+id+"'");
		
	}

	public List findDataExtractMailInfo(Long id) {
		return getHibernateTemplate().find("from ExtractQueryFile where  id='"+id+"'");
	}

}
