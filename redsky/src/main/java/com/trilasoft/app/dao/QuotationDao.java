package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PartnerQuote;
import com.trilasoft.app.model.Quotation;

public interface QuotationDao extends GenericDao<Quotation, Long> {   
	   
	public List findMaximumId();

	public List checkById(Long id);

	public List maxId(String  requestedSO);
	
	public List minId(String requestedSO);

	public List <PartnerQuote>findByQuotation(String quote, String quoteStatus, String sessionCorpID, String vendorCodeSet);
	
	public List checkQuotation(Long quote);
	
	public List<Quotation> getQuotation(Long sID, Long quoteID);
	
	public List checkQuotation2(Long sID, Long quoteID);
	
	public void submitQuotation(Long quoteID);
	
	public void processQuotation(Long quoteID);
	
	public List nextServiceOrder(String requestedSO,Long sid );
	
	public List previousServiceOrder(String requestedSO,Long sid );
	public List  BookingAgentName(String partnerCode, String corpid, Boolean cmmDmmFlag);
	public List findBookCodeStatus(String corpId, String bookAg);
	public String findBookCodeStatusAjax(String corpId, String bookAg);
	public  Map<String, String> findBookCodeQuotes(String corpId, Long id);
}
