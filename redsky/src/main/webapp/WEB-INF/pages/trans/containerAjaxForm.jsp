<%--
/**
 * Implementation of View that contains add and edit details.
 * This file represents the basic view on "Containers" in Redsky.
 * @File Name	containerForm
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        1-Dec-2008
 * --%>



<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="containerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='containerDetail.heading'/>"/>   
 
 
<style type="text/css">

/* collapse */

div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>   
 <script language="JavaScript">
$('input,select,textfield :visible').each(function (i) {
	$(this).attr('tabindex', i + 1);

	});
var links = document.getElementsByTagName( 'a' );
for( var i = 0, j =  links.length; i < j; i++ ) {
    links[i].setAttribute( 'tabindex', '-1' );
}
</script> 
</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="containerForm" action="saveContainerAjax" onsubmit="return checkBlankField()" method="post" validate="true">
<s:hidden name="serviceOrder.controlFlag"  /> 
<s:hidden name="container.containerId"  /> 
<s:hidden name="container.totalNetWeight" value="${container.totalNetWeight}" />
<s:hidden name="container.grossWeightTotal" value="${container.grossWeightTotal}" />

<s:hidden name="updateRecords" value=""/> 
<s:hidden name="usedVolume"  />  
<s:hidden name="container.id" value="%{container.id}"/>   
<s:hidden  name="container.ugwIntId" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="container.serviceOrderId" value="%{serviceOrder.id}"/>
<s:hidden name="container.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="container.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="customerFile.id" />
<s:hidden  name="container.corpID" />
<s:hidden name="shipSize" />
<s:hidden name="minShip" />
<s:hidden name="countShip" />
<s:hidden name="minChild" />
<s:hidden name="maxChild" />
<s:hidden name="countChild" />
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<s:hidden name="container.unit1"/>
<s:hidden name="container.unit2"/>	
<s:hidden name="serviceOrder.mode" value="%{serviceOrder.mode}"/>	
<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
<s:hidden name="soId" value="%{serviceOrder.id}"/>
<s:hidden id="countContainerNotes" name="countContainerNotes" value="<%=request.getParameter("countContainerNotes") %>"/>
<c:set var="countContainerNotes" value="<%=request.getParameter("countContainerNotes") %>" />
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="calOpener" value="notOPen" />
	

 <div id="Layer4" style="width:750px;">
 <div id="newmnav" style="float:left;">   

 <ul>
  <li id="newmnav1" style="background:#FFF "><a class="current"><span>SS Container</span></a></li>
</ul>

</div>
		
<div class="spn">&nbsp;</div>

</div>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0" style="width:95%" >
				<tbody>
				<tr><td height="5px"></td></tr> 
				 
				<tr>
				<td align="right" width="50px" class="listwhitetext">Status</td>
                <c:set var="isStatusFlag" value="false"/>
                <c:if test="${container.status}">
                 <c:set var="isStatusFlag" value="true"/>
                </c:if>
                <td align="left" width="20px" valign="bottom"><s:checkbox key="container.status" value="${isStatusFlag}" onchange="statusValidation(this);"   fieldValue="true"  tabindex="3" /></td>
				</tr> 
                     <tr>
     <td width="113px" align="right" class="listwhitetext"><fmt:message key="container.idNumber"/></td>
  <td align="left" class="listwhitetext"><s:textfield id="idNum" cssClass="input-textUpper" key="container.idNumber" cssStyle="width:85px;" readonly="true" maxlength="2" tabindex="" onchange="onlyNumeric(this); isNumeric(this);" /></td>
  

     
  <td align="right" class="listwhitetext" id="containerNumberRequiredFalse"><fmt:message key="container.containerNumber"/></td>
  <td align="right" class="listwhitetext" id="containerNumberRequiredTrue"><fmt:message key="container.containerNumber"/><font color="red" size="2">*</font></td>
  <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="container.containerNumber" cssStyle="width:85px;" maxlength="20" tabindex="" /></td>
     
    <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
    <c:if test="${empty container.id}">
						
							<td  align="right" colspan="4"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty container.id}">
							<c:choose>
								<c:when test="${countContainerNotes == '0' || countContainerNotes == '' || countContainerNotes == null}">
								
								<td  align="right" colspan="4"><img id="countContainerNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${container.id }&notesId=${container.id }&noteFor=Container&subType=Container&imageId=countContainerNotesImage&fieldId=countContainerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${container.id }&notesId=${container.id }&noteFor=Container&subType=Container&imageId=countContainerNotesImage&fieldId=countContainerNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td  align="right" colspan="4"><img id="countContainerNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${container.id }&notesId=${container.id}&noteFor=Container&subType=Container&imageId=countContainerNotesImage&fieldId=countContainerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${container.id }&notesId=${container.id }&noteFor=Container&subType=Container&imageId=countContainerNotesImage&fieldId=countContainerNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>

 </sec-auth:authComponent> 
  </tr>
  
  <tr>
  <td align="right" class="listwhitetext"><fmt:message key="container.sealNumber"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="container.sealNumber" cssStyle="width:85px;" maxlength="25"  tabindex="" onblur="makeSealDateMandatory();" onchange="makeSealDateMandatory();"/></td>
 
  <td align="right" class="listwhitetext"><fmt:message key="container.customSeal"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" key="container.customSeal" cssStyle="width:85px;" maxlength="14"  tabindex="" /></td>
<td></td>
<td align="right" class="listwhitetext"  id="sealdatefalse" style="padding-top:5px;">Seal Date</td>
<td align="right" class="listwhitetext" id="sealdatetrue" style="padding-top:5px;">Seal Date<font color="red" size="2">*</font></td>
<c:if test="${not empty container.sealDate}">
<s:text id="containerSealDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="container.sealDate"/></s:text>
<td align="left" class="listwhitetext" width="60px"><s:textfield name="container.sealDate" cssClass="input-text" id="sealDate" value="%{containerSealDateFormattedValue}" cssStyle="width:65px;" maxlength="14"  tabindex="" onkeydown="onlyDel(event,this)" /></td>
<td align="left"><img id="sealDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>				
</c:if>
<c:if test="${ empty container.sealDate}">
<td align="left" class="listwhitetext" width="60px"><s:textfield name="container.sealDate" cssClass="input-text" id="sealDate" cssStyle="width:65px;" maxlength="14"   tabindex="" onkeydown="onlyDel(event,this)"/></td>
<td align="left"><img id="sealDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>				
</c:if>
</tr>

<tr>
<td align="right" class="listwhitetext" id="sizeRequiredFalse"><fmt:message key="container.size"/></td>
<td align="right" class="listwhitetext" id="sizeRequiredTrue"><fmt:message key="container.size"/><font color="red" size="2">*</font></td>
<td><configByCorp:customDropDown listType="map" list="${EQUIP_isactive}" fieldValue="${container.size}"
		       attribute="id=containerSize class=list-menu name=container.size  style=width:89px  headerKey='' headerValue='' onchange='changeStatus();checkStatus('${serviceOrder.grpStatus}',this);' tabindex='' "/></td>

<%-- <s:select cssClass="list-menu" id="containerSize" name="container.size" list="%{EQUIP}" cssStyle="width:89px" headerKey="" headerValue=" " onchange="changeStatus();checkStatus('${serviceOrder.grpStatus}',this);" tabindex="" /></td>
 --%> <td colspan="" align="right" class="listwhitetext"><fmt:message key="container.pieces"/></td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;" key="container.pieces"  maxlength="4" onchange="isNumeric(this);" tabindex="" /></td>

 <td></td>
 <td align="right" class="listwhitetext"  id="sealdatefalse" style="padding-top:5px;">VGM&nbsp;Cutoff</td>
<c:if test="${not empty container.vgmCutoff}">
<s:text id="containervgmCutoffFormattedValue" name="${FormDateValue}"><s:param name="value" value="container.vgmCutoff"/></s:text>
<td align="left" class="listwhitetext" width="60px"><s:textfield name="container.vgmCutoff" cssClass="input-text" id="vgmCutoff" value="%{containervgmCutoffFormattedValue}" cssStyle="width:65px;" maxlength="14"  tabindex="" onkeydown="onlyDel(event,this)" /></td>
<td align="left"><img id="vgmCutoff_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>				
</c:if>
<c:if test="${ empty container.vgmCutoff}">
<td align="left" class="listwhitetext" width="60px"><s:textfield name="container.vgmCutoff" cssClass="input-text" id="vgmCutoff" cssStyle="width:65px;" maxlength="14"   tabindex="" onkeydown="onlyDel(event,this)"/></td>
<td align="left"><img id="vgmCutoff_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>				
</c:if>

 </tr>
  <tr>
 <td align="right"  class="listwhitetext" >Pickup&nbsp;Pier</td>
 <td align="left" colspan="1" class="listwhitetext"><s:textarea name="container.pickupPier" id="pickupPier" rows="1" cols="50" onkeydown="limitText(this.form.pickupPier,this.form.countdown,100);" onkeyup="limitText(this.form.pickupPier,this.form.countdown,100);" cssClass="textarea" /></td>
 
<input readonly type="hidden" id="countdown" size="3" value="100"> 
<input readonly type="hidden" id="countdown1" size="3" value="100"> 

 <td align="right"  class="listwhitetext" style="width:115px;">Re-Delivery&nbsp;Pier</td>
 <td align="left" colspan="4" class="listwhitetext"><s:textarea name="container.redeliveryPier" id="redeliveryPier"  rows="1" cols="50"  onkeydown="limitText(this.form.redeliveryPier,this.form.countdown1,100);" onkeyup="limitText(this.form.redeliveryPier,this.form.countdown1,100);" cssClass="textarea"/></td>
 </tr>
 <tr><td height="3px"></td></tr>
 </table>
 <table width="100%" class="colored_bg" style="margin-bottom:1px;" border="0">
 <tr><td height="2px"></td></tr>
 <tr>
 <td width="100%">
  <fieldset >
   <legend>Pounds & Cft 
   <c:if test="${weightType == 'lbscft'}">			
						<INPUT type="radio" name="weightType" checked="checked" value="lbscft" onclick="check(this);changeStatus();">
						</div>
						</c:if>
						<c:if test="${weightType != 'lbscft'}">			
						<INPUT type="radio" name="weightType" value="lbscft" onclick="check(this);changeStatus();">
						</div>
						</c:if></legend>
 <table  style="margin-bottom:1px;" border="0" width="100%">

 <tr >
  <td align="right" class="listwhitetextAcct" width="100px"><fmt:message key="container.grossWeight"/></td>
  <td colspan="" align="left" class="listwhitetextAcct" width="163px"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.grossWeight" size="13" maxlength="10" onchange="calcNetWeight(),calculate('container.unit1','container.unit2');onlyFloat(this);" tabindex=""/></td>
 <td align="left" class="listwhitetextAcct"> 
  
  </td>
  
<td colspan=""  align="right" class="listwhitetextAcct"><fmt:message key="container.volume"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.volume" size="13" maxlength="10" onchange="calculate('container.unit1','container.unit2');onlyFloat(this);"  tabindex=""/></td>
<td  colspan="" align="right" class="listwhitetextAcct"></td>
                        
</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="container.emptyContWeight"/></td>
<td colspan="" align="left" class="listwhitetextAcct">
<s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;" key="container.emptyContWeight" maxlength="10"  onchange="calcNetWeight(),calculate('container.unit1','container.unit2'); onlyFloat(this);" tabindex="" /></td>
<td colspan="2" align="right" class="listwhitetextAcct"><fmt:message key="container.density"/></td>
<td align="left" colspan="2" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.density"  size="2" readonly="true"  maxlength="5" onchange="isNumeric(this);" tabindex="" />
<input type="button" class="cssbutton" style="width:60px; height:25px"  name="calc" value="Calculate" onclick="calculate('container.unit1','container.unit2')" onmouseover="return chkSelect('calc');" tabindex="" /> 
</td>
</tr>

<tr>
  <td align="right" class="listwhitetextAcct"><fmt:message key="container.netWeight"/></td>
  <td colspan="" align="left" class="listwhitetextAcct">
  <s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.netWeight" 
  size="13" maxlength="10" onchange="onlyFloat(this);calcTare();" onblur="calculate('container.unit1','container.unit2');" tabindex=""/></td>
 
</tr>
</table>

</fieldset>
</td>
</tr>
<tr>
<td width="100%">
 <fieldset >
   <legend>Metric Units
   <c:if test="${weightType == 'kgscbm'}">
<INPUT type="radio" name="weightType" value="kgscbm" checked="checked" onclick="check(this);changeStatus();"></div>
                        </c:if>
						<c:if test="${weightType != 'kgscbm'}">
<INPUT type="radio" name="weightType" value="kgscbm"  onclick="check(this);changeStatus();"></div>
                        </c:if>
   </font></legend>
<table style="margin-bottom:1px;" border="0" width="100%">
<tr >
  <td align="right" class="listwhitetextAcct" width="100px"><fmt:message key="container.grossWeight"/></td>
  <td colspan="" align="left" class="listwhitetextAcct" width="163px"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.grossWeightKilo" size="13" maxlength="10" onchange="onlyFloat(this);calcNetWeightKilo(),calculate('container.unit1','container.unit2');" tabindex=""/></td>
 
  <td  colspan="" align="left" class="listwhitetextAcct"></td>
 
<td colspan=""  align="right" class="listwhitetextAcct"><fmt:message key="container.volume"/></td>
<td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.volumeCbm" size="13" maxlength="10" onchange="onlyFloat(this);calculateVolume('container.unit1','container.unit2');"  tabindex=""/></td>

                        
</tr>
<tr>
<td align="right" class="listwhitetextAcct"><fmt:message key="container.emptyContWeight"/></td>
<td colspan="" align="left" class="listwhitetextAcct">
<s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;" key="container.emptyContWeightKilo"  maxlength="10"  onchange=" onlyFloat(this);calcNetWeightKilo(),calculate('container.unit1','container.unit2');" tabindex="" /></td>
<td colspan="2" align="right" class="listwhitetextAcct"><fmt:message key="container.density"/></td>
<td align="left" colspan="2" class="listwhitetextAcct"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.densityMetric"  size="2" readonly="true"  maxlength="5" onchange="isNumeric(this);" tabindex="" /><input type="button" class="cssbutton" style="width:60px; height:25px"  name="calc" value="Calculate" onclick="calculate('container.unit1','container.unit2')" onmouseover="return chkSelect('calc');" tabindex="" /></td>
 
</tr>

<tr>
  <td align="right" class="listwhitetextAcct"><fmt:message key="container.netWeight"/></td>
  <td colspan="" align="left" class="listwhitetextAcct">
  <s:textfield cssClass="input-text" cssStyle="text-align:right;width:85px;"  key="container.netWeightKilo" 
  size="13" maxlength="10" onchange=" onlyFloat(this);calcTareKilo();" onblur="calculate('container.unit1','container.unit2');" tabindex=""/></td>
   
  </tr>
  <tr>
  </tr>
</table>
</fieldset>

</td>
</tr>
</table>

</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
</td>
									<td background="<c:url value='/images/bg-right.jpg'/>"
										align="left" class="listwhitetext"></td>
								</tr>
								
</tbody>
</table>						
	
									<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${container.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="container.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${container.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty container.id}">
								<s:hidden name="container.createdBy"/>
								<td><s:label name="createdBy" value="%{container.createdBy}"/></td>
							</c:if>
							<c:if test="${empty container.id}">
								<s:hidden name="container.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${container.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="container.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${container.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty container.id}">
							<s:hidden name="container.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{container.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty container.id}">
							<s:hidden name="container.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			</div>
			<c:choose>
		<c:when test="${serviceOrder.controlFlag =='G'  &&  serviceOrder.grpStatus=='Finalized'}">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return confirmUpdate();calcNetWeight();" onmouseover="return chkSelect();"/>   
        </c:when>        
		<c:when test="${serviceOrder.controlFlag =='G'  && serviceOrder.grpStatus=='Draft' }">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return confirmUpdate1();calcNetWeight();" onmouseover="return chkSelect();"/>   
        </c:when>        
        <c:when test="${serviceOrder.grpID != null && (serviceOrder.grpStatus=='Draft'  || serviceOrder.grpStatus=='Finalized' ) && (not empty container.containerId)}">													              
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return confirmUpdateByChild();" onmouseover="return chkSelect();"/>   
        </c:when>
        <c:otherwise>
        <s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return checkSealNumber();calcNetWeight();" onmouseover="return chkSelect();"/>   
        </c:otherwise>
        </c:choose>
        <s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px"/>
     
        <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
 </s:form> 


<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<!-- Modification closed here -->  
 
    <script language="JavaScript">
    
    function statusValidation(targetElementStatus){
    	var massage="";
    	if(targetElementStatus.checked==false){
    		massage="Are you sure you wish to deactivate this row?"
    	}else{
    		massage="Are you sure you wish to activate this row?"
    	}  
    	var agree=confirm(massage);
    	if (agree){
    		
    	}else{
    		if(targetElementStatus.checked==false){
    			document.forms['containerForm'].elements['container.status'].checked=true;
    			}else{
    				document.forms['containerForm'].elements['container.status'].checked=false;
    			}
    	}
    }

    <sec-auth:authComponent componentId="module.script.form.agentScript">

	window.onload = function() { 
	 trap();
		
		
		}
 </sec-auth:authComponent>
 
 <sec-auth:authComponent componentId="module.script.form.corpAccountScript">

	window.onload = function() { 

		
		trap();
					
	}
</sec-auth:authComponent>
 
 
 function trap() 
		  {
		  
		  if(document.images)
		    {
		    
		    	for(i=0;i<document.images.length;i++)
		      {
		      	
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}
		        	 

		      }
		    }
		  }
		  
		  function right(e) {
		
		//var msg = "Sorry, you don't have permission.";
		if (navigator.appName == 'Netscape' && e.which == 1) {
		//alert(msg);
		return false;
		}
		
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) {
		//alert(msg);
		return false;
		}
		
		else return true;
		}
		  function limitText(limitField, limitCount, limitNum) {
				if (limitField.value.length > limitNum) {
					limitField.value = limitField.value.substring(0, limitNum);
				} else {
					limitCount.value = limitNum - limitField.value.length;
				}
			}
function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="0";
	        
	        return false;
	        }
	        var f = document.getElementById('containerForm'); 
		f.setAttribute("autocomplete", "off");
	    }
	 
	      return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	
	
// Soft validation for SS container #
function isNumericCheck(elem, helperMsg){ 
if(elem.trim().length !=6){
var  agree = confirm(helperMsg);
if(agree){
return true;
}else{
return false;
}
} else{
	var numericExpression = /^[0-9]+$/;
	if(elem.match(numericExpression)){
		return true;
	}else{
		var  agree = confirm(helperMsg);
		if(agree){
		return true;
		}else{
		return false;
		}
	}
}
}

function isNumeric1(elem, helperMsg){
if(elem.trim().length !=1){
var  agree = confirm(helperMsg);
if(agree){
return true;
}else{
return false;
}
} else{
	var numericExpression = /^[0-9]+$/;
	if(elem.match(numericExpression)){
		return true;
	}else{
		var  agree = confirm(helperMsg);
		if(agree){
		return true;
		}else{
		return false;
		}
	}
}
}

function isAlphabet(elem, helperMsg){
if(elem.trim().length !=4){
var  agree = confirm(helperMsg);
if(agree){
return true;
}else{
return false;
}
} else{
	var alphaExp = /^[a-zA-Z]+$/;
	if(elem.match(alphaExp)){
		return true;
	}else{
		var  agree = confirm(helperMsg);
		if(agree){
		return true;
		}else{
		return false;
		}
	}
}
}
function validateSSContainer()
{
var containerNumber=document.forms['containerForm'].elements['container.containerNumber'].value;
//alert(containerNumber);
if(containerNumber.trim().length !=13){
var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
if(agree){
return true;
}else{
return false;
}
} else {
var check="";
var checkdash="";
var first4 =containerNumber.substring(0,4)
if(check==""){
 var returnCheck = isAlphabet(first4,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
 if(!returnCheck){
check="Ok";
}
} 
var firstdash = containerNumber.substring(4,5)
if(firstdash!='-'){
var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
if(agree){
return true;
}else{
return false;
}
checkdash="Ok"
}

var next6 = containerNumber.substring(5,11) 
if(check == "" && checkdash==""){ 
var returnCheck =isNumericCheck(next6,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
if(!returnCheck){
check="Ok";
}
}
var seconddash = containerNumber.substring(11,12)
if(checkdash==""){
if(seconddash!='-'){
var  agree = confirm('Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
if(agree){
return true;
}else{
return false;
}
}
}
var last1 = containerNumber.substring(12,13)
if(check=="" && checkdash==""){
var returnCheck =isNumeric1(last1,'Please enter the SSContainer# value in the format as "4 letters-6 numerals-1 numeric \nTo save click OK, To Enter new value Click Cancel.')
if(!returnCheck){
check="Ok";
}
}
	//if(containerNumber)
}
return true;
}	
//End of function.
	
	function notExists(){
	alert("The Container information has not been saved yet.");
}
</script>
 <script>
 
// A function for auto save funtionality.

function ContainerAutoSave(clickType){
var saveType= validateSSContainer();
if(saveType){
	var checkSealNumber1=document.forms['containerForm'].elements['container.sealNumber'].value;
    var checkSealDate=document.forms['containerForm'].elements['container.sealDate'].value;
     if(checkSealNumber1!=''&& checkSealDate ==''){
            alert('Please Enter the Seal Date for the Seal Number Entered');
              return false; 
         }
	progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
	    var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	    var id1 = document.forms['containerForm'].elements['serviceOrder.id'].value;
		if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                noSaveAction = 'accountLineList.html?sid='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    noSaveAction = 'pricingList.html?sid='+id1;
    }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                noSaveAction = 'containers.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                noSaveAction = 'editBilling.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                noSaveAction = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.status'){


		<c:if test="${serviceOrder.job=='RLO'}">
    	noSaveAction = 'editDspDetails.html?id='+id1; 
       </c:if>
       <c:if test="${serviceOrder.job!='RLO'}">
    	noSaveAction =  'editTrackingStatus.html?id='+id1;
        </c:if>
          }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                noSaveAction = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                noSaveAction = 'claims.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                noSaveAction = 'cartons.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                noSaveAction = 'vehicles.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                noSaveAction = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                noSaveAction = 'containers.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                noSaveAction = 'editCustomerFile.html?id='+cidVal;
                }
                processAutoSave(document.forms['containerForm'], 'saveContainer!saveOnTabChange.html', noSaveAction);
        }
	
	else{

    if(!(clickType == 'save')){
    var id1 = document.forms['containerForm'].elements['serviceOrder.id'].value;
    var jobNumber = document.forms['containerForm'].elements['serviceOrder.shipNumber'].value;

    if (document.forms['containerForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='containerDetail.heading'/>");
        if(agree){
            document.forms['containerForm'].action = 'saveContainer!saveOnTabChange.html';
            document.forms['containerForm'].submit();
        }else{
            if(id1 != ''){

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
        }
        }
    }else{
    if(id1 != ''){

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                location.href = 'editServiceOrderUpdate.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                location.href = 'accountLineList.html?sid='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    location.href = 'pricingList.html?sid='+id1;
    }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.forwarding'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.billing'){
                location.href = 'editBilling.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                location.href = 'editMiscellaneous.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.status'){
	<c:if test="${serviceOrder.job=='RLO'}">
	location.href = 'editDspDetails.html?id='+id1; 
   </c:if>
   <c:if test="${serviceOrder.job!='RLO'}">
   location.href =  'editTrackingStatus.html?id='+id1;
    </c:if>
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                location.href = 'customerWorkTickets.html?id='+id1;
                }

if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.claims'){
                location.href = 'claims.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.servicepartners'){
                location.href = 'servicePartnerss.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.vehicles'){
                location.href = 'vehicles.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.cartons'){
                location.href = 'cartons.html?id='+id1;
                }
                if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.containers'){
                location.href = 'containers.html?id='+id1;
                }
if(document.forms['containerForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                location.href = 'editCustomerFile.html?id='+cidVal;
                }
    }
    }
}
}
}
}
// End of function.

function changeStatus(){
    document.forms['containerForm'].elements['formStatus'].value = '1';
}



</script>

<script type="text/javascript"> 
var form_submitted = false;

function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}
</script>
    
<script type="text/javascript">

function calcNetWeight(){

// function for calculating the net weight on changing the gross weight and tare weight.

if(document.forms['containerForm'].elements['container.emptyContWeight'].value=='')
{
document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
}
if(document.forms['containerForm'].elements['container.grossWeight'].value=='')
{
document.forms['containerForm'].elements['container.grossWeight'].value=0;
}
var Q1 = eval(document.forms['containerForm'].elements['container.grossWeight'].value);
var Q2 = eval(document.forms['containerForm'].elements['container.emptyContWeight'].value);

var E1='';
    
	if(Q1<Q2)
	{
		alert("Gross weight should be greater than tare weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E2;
		var E3 = Q1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
		document.forms['containerForm'].elements['container.grossWeightKilo'].value=E4;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E4;
	}
	else
	{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E2;
		var E3 = E1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E4;
		var Q3 = Q1*0.4536;
		var Q4=Math.round(Q3*10000)/10000;
		document.forms['containerForm'].elements['container.grossWeightKilo'].value=Q4;
		var Q5 = Q2*0.4536;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=Q6;
	}
	
}
function calcTare()
{

if(document.forms['containerForm'].elements['container.emptyContWeight'].value=='')
{
document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
}
if(document.forms['containerForm'].elements['container.grossWeight'].value=='')
{
document.forms['containerForm'].elements['container.grossWeight'].value=0;
}
var Q1 = eval(document.forms['containerForm'].elements['container.grossWeight'].value);
var Q2 = eval(document.forms['containerForm'].elements['container.emptyContWeight'].value);
var Q3 = eval(document.forms['containerForm'].elements['container.netWeight'].value);
var E1='';
	if(Q1<Q3)
	{
		alert("Gross weight should be greater than net weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E2;
		var E3 = Q1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E4;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
	}
	else if(Q1 != undefined && Q3 != undefined)
	{
		E1=Q1-Q3;
		var E2=Math.round(E1*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=E2;
		var E3 = E1*0.4536;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=E4;
		var E5 = Q3*0.4536;
		var E6=Math.round(E5*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E6;
	}
	else if(Q3 == undefined)
			{
			document.forms['containerForm'].elements['container.grossWeight'].value=0;
			document.forms['containerForm'].elements['container.netWeightKilo'].value=0;
			document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
			document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
			document.forms['containerForm'].elements['container.netWeight'].value=0;
			document.forms['containerForm'].elements['container.grossWeightKilo'].value=0;
			}
	
}
// End of function.

function calcNetWeightKilo(){

// function for calculating the net weight on changing the gross weight and tare weight.

if(document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=='')
{
document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
}
if(document.forms['containerForm'].elements['container.grossWeightKilo'].value=='')
{
document.forms['containerForm'].elements['container.grossWeightKilo'].value=0;
}
var Q1 = eval(document.forms['containerForm'].elements['container.grossWeightKilo'].value);
var Q2 = eval(document.forms['containerForm'].elements['container.emptyContWeightKilo'].value);

var E1='';
    
	if(Q1<Q2)
	{
		alert("Gross weight should be greater than tare weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E2;
		var E3 = Q1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
		document.forms['containerForm'].elements['container.grossWeight'].value=E4;
		document.forms['containerForm'].elements['container.netWeight'].value=E4;
	}
	else
	{
		E1=Q1-Q2;
		var E2=Math.round(E1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E2;
		var E3 = E1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E4;
		var Q3 = Q1*2.2046;
		var Q4=Math.round(Q3*10000)/10000;
		document.forms['containerForm'].elements['container.grossWeight'].value=Q4;
		var Q5 = Q2*2.2046;
		var Q6=Math.round(Q5*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=Q6;
	}
	
}
function calcTareKilo()
{

if(document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=='')
{
document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
}
if(document.forms['containerForm'].elements['container.grossWeightKilo'].value=='')
{
document.forms['containerForm'].elements['container.grossWeightKilo'].value=0;
}
var Q1 = eval(document.forms['containerForm'].elements['container.grossWeightKilo'].value);
var Q2 = eval(document.forms['containerForm'].elements['container.emptyContWeightKilo'].value);
var Q3 = eval(document.forms['containerForm'].elements['container.netWeightKilo'].value);
var E1='';
    
	if(Q1<Q3)
	{
		alert("Gross weight should be greater than net weight");
		var E2=Math.round(Q1*10000)/10000;
		document.forms['containerForm'].elements['container.netWeightKilo'].value=E2;
		var E3 = Q1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E4;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
	}
	else if(Q1 != undefined && Q3 != undefined)
	{
		E1=Q1-Q3;
		var E2=Math.round(E1*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=E2;
		var E3 = E1*2.2046;
		var E4=Math.round(E3*10000)/10000;
		document.forms['containerForm'].elements['container.emptyContWeight'].value=E4;
		var E5 = Q3*2.2046;
		var E6=Math.round(E5*10000)/10000;
		document.forms['containerForm'].elements['container.netWeight'].value=E6;
	}
	else if(Q3 == undefined)
			{
			document.forms['containerForm'].elements['container.grossWeight'].value=0;
			document.forms['containerForm'].elements['container.netWeightKilo'].value=0;
			document.forms['containerForm'].elements['container.emptyContWeightKilo'].value=0;
			document.forms['containerForm'].elements['container.emptyContWeight'].value=0;
			document.forms['containerForm'].elements['container.netWeight'].value=0;
			document.forms['containerForm'].elements['container.grossWeightKilo'].value=0;
			}
			
}
// End of function.




// function for calculating the net weight on changing the gross weight and tare weight on the basis of unit changes.

function calculate(weightUnit,volumeUnit) 
{
	var weightUnit = document.getElementsByName(weightUnit);
	var bt_count = weightUnit.length;
	for (var i = 0; i <bt_count; i++)
	{
	 if (weightUnit[i].checked == true)
	  {
		 var weightUnitKgs=weightUnit[i].value;  
	  } 
    } 
	var volumeUnit = document.getElementsByName(volumeUnit);
	var bt_count1 = volumeUnit.length;
	for (var i = 0; i <bt_count1; i++)
	 {
      if (volumeUnit[i].checked == true)
      {
		 var volumeUnitCbm=volumeUnit[i].value;
	  }   
	 }
       var mode=document.forms['containerForm'].elements['serviceOrder.mode'].value
       var netWeight= eval (document.forms['containerForm'].elements['container.netWeight'].value);
	   var grossWeight= eval (document.forms['containerForm'].elements['container.grossWeight'].value);
	   var netWeightKilo= eval (document.forms['containerForm'].elements['container.netWeightKilo'].value);
	   var grossWeightKilo= eval (document.forms['containerForm'].elements['container.grossWeightKilo'].value);
	   var volume= eval (document.forms['containerForm'].elements['container.volume'].value);
	   var density=0;
	   var densityMetric=0;
	   var E2=0;
	   if(document.forms['containerForm'].elements['container.volume'].value =='')
	   {
	     var volume = document.forms['containerForm'].elements['container.volume'].value=0;
	     var volumeKilo = document.forms['containerForm'].elements['container.volumeCbm'].value=0;
	     var density=document.forms['containerForm'].elements['container.density'].value=0;
	   }
	   else{
	   var E1=volume*0.0283;
	   var E2=Math.round(E1*10000)/10000;
	   document.forms['containerForm'].elements['container.volumeCbm'].value = E2;
	   var E3=document.forms['containerForm'].elements['container.volumeCbm'].value;
	   }
	   if(weightUnitKgs=='Kgs')
	   {
	     netWeight=netWeight*2.2046;
	     grossWeight=grossWeight*2.2046;
	   }
	   if(volumeUnitCbm=='Cbm')
	   {
	     volume=volume*35.3147;
	   }
	   if(netWeight==undefined||netWeight==0||grossWeight==undefined||grossWeight==0||volume==undefined||volume==0)
	    {
		  document.forms['containerForm'].elements['container.density'].value=0;
		}
	   else
		{
		 if(mode=='Air')
		  {
		       density=Math.round((grossWeight/volume)*1000)/1000;
		       document.forms['containerForm'].elements['container.density'].value=density;
		   }
		  else
		   {
		        density=Math.round((netWeight/volume)*1000)/1000;
		        document.forms['containerForm'].elements['container.density'].value=density;
		    }
	    }
	    if(netWeightKilo==undefined||netWeightKilo==0||grossWeightKilo==undefined||grossWeightKilo==0||E3==undefined||E3==0)
	    { 
		  document.forms['containerForm'].elements['container.densityMetric'].value=0;
		} 
		else{
		if(mode=='Air')
		  {
		       densityMetric=Math.round((grossWeightKilo/E3)*1000)/1000;
		       document.forms['containerForm'].elements['container.densityMetric'].value=densityMetric;
		   }
		  else
		   {
		        densityMetric=Math.round((netWeightKilo/E3)*1000)/1000;
		        document.forms['containerForm'].elements['container.densityMetric'].value=densityMetric;
		   }
		}
		
		
}

// End of function.

// function for calculating the net weight on changing the gross weight and tare weight on the basis of unit changes.

function calculateVolume(weightUnit,volumeUnit) 
{
	var weightUnit = document.getElementsByName(weightUnit);
	var bt_count = weightUnit.length;
	for (var i = 0; i <bt_count; i++)
	{
	 if (weightUnit[i].checked == true)
	  {
		 var weightUnitKgs=weightUnit[i].value;  
	  } 
    } 
	var volumeUnit = document.getElementsByName(volumeUnit);
	var bt_count1 = volumeUnit.length;
	for (var i = 0; i <bt_count1; i++)
	 {
      if (volumeUnit[i].checked == true)
      {
		 var volumeUnitCbm=volumeUnit[i].value;
	  }   
	 }
       var mode=document.forms['containerForm'].elements['serviceOrder.mode'].value
       var netWeight= eval (document.forms['containerForm'].elements['container.netWeight'].value);
	   var grossWeight= eval (document.forms['containerForm'].elements['container.grossWeight'].value);
	   var netWeightKilo= eval (document.forms['containerForm'].elements['container.netWeightKilo'].value);
	   var grossWeightKilo= eval (document.forms['containerForm'].elements['container.grossWeightKilo'].value);
	   var volume= eval (document.forms['containerForm'].elements['container.volumeCbm'].value);
	   var density=0;
	   var densityMetric=0;
	   var E2=0;
	   if(document.forms['containerForm'].elements['container.volumeCbm'].value =='')
	   {
	     var volume = document.forms['containerForm'].elements['container.volume'].value=0;
	     var volumeKilo = document.forms['containerForm'].elements['container.volumeCbm'].value=0;
	     var density=document.forms['containerForm'].elements['container.densityMetric'].value=0;
	   }
	   else{
	   var E1=volume*35.3147;
	   var E2=Math.round(E1*10000)/10000;
	   document.forms['containerForm'].elements['container.volume'].value = E2;
	   var E3=document.forms['containerForm'].elements['container.volume'].value;
	   }
	   
	   if(netWeight==undefined||netWeight==0||grossWeight==undefined||grossWeight==0||volume==undefined||volume==0)
	    {
		  document.forms['containerForm'].elements['container.density'].value=0;
		}
	   else
		{
		 if(mode=='Air')
		  {
		       density=Math.round((grossWeightKilo/volume)*1000)/1000;
		       document.forms['containerForm'].elements['container.densityMetric'].value=density;
		   }
		  else
		   {
		        density=Math.round((netWeightKilo/volume)*1000)/1000;
		        document.forms['containerForm'].elements['container.densityMetric'].value=density;
		    }
	    }
	    if(netWeightKilo==undefined||netWeightKilo==0||grossWeightKilo==undefined||grossWeightKilo==0||E3==undefined||E3==0)
	    { 
		  document.forms['containerForm'].elements['container.densityMetric'].value=0;
		} 
		else{
		if(mode=='Air')
		  {
		       densityMetric=Math.round((grossWeightKilo/E3)*1000)/1000;
		       document.forms['containerForm'].elements['container.density'].value=densityMetric;
		   }
		  else
		   {
		        densityMetric=Math.round((netWeightKilo/E3)*1000)/1000;
		        document.forms['containerForm'].elements['container.density'].value=densityMetric;
		   }
		}		
}

// End of function.


// function for net weight,gross weight and tare weight validation.

function chkSelect()
    {
        	if (checkFloat('containerForm','container.volume','Invalid data in Volume') == false)
           {
              document.forms['containerForm'].elements['container.volume'].value='0';
              document.forms['containerForm'].elements['container.density'].value='0';
              document.forms['containerForm'].elements['container.volume'].focus();
              return false
           }
           if (checkFloat('containerForm','container.grossWeight','Invalid data in Gross Weight') == false)
           {
              document.forms['containerForm'].elements['container.grossWeight'].value='0'; 
              document.forms['containerForm'].elements['container.grossWeight'].focus();
              return false
           }
           if (checkFloat('containerForm','container.emptyContWeight','Invalid data in Tare') == false)
           {  
              document.forms['containerForm'].elements['container.emptyContWeight'].value='0';
              document.forms['containerForm'].elements['container.emptyContWeight'].focus();
              return false
           }
          if (checkFloat('containerForm','container.netWeight','Invalid data in Net Weight') == false)
           {
              document.forms['containerForm'].elements['container.emptyContWeight'].value='0';
              document.forms['containerForm'].elements['container.grossWeight'].value='0'; 
              document.forms['containerForm'].elements['container.netWeight'].value='0';
              document.forms['containerForm'].elements['container.density'].value=0;
              document.forms['containerForm'].elements['container.netWeight'].focus();
              return false
           }
           if(document.forms['containerForm'].elements['container.grossWeight'].value=='.')
           {
             document.forms['containerForm'].elements['container.grossWeight'].value='0';
           }
           if(document.forms['containerForm'].elements['container.emptyContWeight'].value == '.')
           {
             document.forms['containerForm'].elements['container.emptyContWeight'].value ='0';
           }
 }
 
 function changeCalOpenarvalue()
		  {
		  	document.forms['containerForm'].elements['calOpener'].value='open';
		  }
	  

// End of function.	
	
	
function check(targetElement){
  var t = targetElement.value;
  if(t=='lbscft'){
  document.forms['containerForm'].elements['container.unit1'].value='Lbs';
  document.forms['containerForm'].elements['container.unit2'].value='Cft';
  }
  if(t=='kgscbm'){
  document.forms['containerForm'].elements['container.unit1'].value='Kgs';
  document.forms['containerForm'].elements['container.unit2'].value='Cbm';
  }
} 
	var http5 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

	  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['containerForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['containerForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['containerForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['containerForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'containers.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['containerForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['containerForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrl(id)
	{
		location.href = "containers.html?id="+id;
	}
	
	
	
function goPrevChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['containerForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['containerForm'].elements['container.id'].value;
	var url="containerPrev.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function goNextChild() {
	progressBarAutoSave('1');
	var sidNum =document.forms['containerForm'].elements['serviceOrder.id'].value;
	var soIdNum =document.forms['containerForm'].elements['container.id'].value;
	var url="containerNext.html?ajax=1&decorator=simple&popup=true&sidNum="+encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShipChild; 
     http5.send(null); 
   }
   
 function handleHttpResponseOtherShipChild(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'editContainer.html?id='+results;
             }
       }     
function findCustomerOtherSOChild(position) {
 var sidNum=document.forms['containerForm'].elements['serviceOrder.id'].value;
 var soIdNum=document.forms['containerForm'].elements['container.id'].value;
 var url="containerSO.html?ajax=1&decorator=simple&popup=true&sidNum=" + encodeURI(sidNum)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  }   
function goToUrlChild(id)
	{
		location.href = "editContainer.html?id="+id;
	}

function makeSealDateMandatory(){
var checkSealNumber=document.forms['containerForm'].elements['container.sealNumber'].value;
var key1=document.getElementById('sealdatefalse');
var key2=document.getElementById('sealdatetrue');
if(checkSealNumber!='')
   {
      key1.style.display ='none';
      key2.style.display ='block';
   }
 else{
       key1.style.display ='block';
      key2.style.display ='none';
     
     }  
}

function checkSealNumber()
   {
    var checkSealNumber1=document.forms['containerForm'].elements['container.sealNumber'].value;
    var checkSealDate=document.forms['containerForm'].elements['container.sealDate'].value;
    if(checkSealNumber1!=''&& checkSealDate ==''){
   alert('Please Enter the Seal Date for the Seal Number Entered');
   return false; 
     }
     return validateSSContainer();
    }	
</script> 

<script language="javascript" type="text/javascript">
		function confirmUpdate(){
		var checkSealNumber1=document.forms['containerForm'].elements['container.sealNumber'].value;
        var checkSealDate=document.forms['containerForm'].elements['container.sealDate'].value;
        var sealDate =checkSealNumber();
        if(sealDate){
        if(checkSealNumber1!=''&& checkSealDate ==''){
            alert('Please Enter the Seal Date for the Seal Number Entered');
              return false; 
         }
		var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
		if(agree){
		
		document.forms['containerForm'].elements['updateRecords'].value='updateAll';
		
		}
		else{
		document.forms['containerForm'].elements['updateRecords'].value='updateCurrent';
		}
		}
		}
		
		
		function confirmUpdateByChild(){
		alert("Update is not allowed as this service order is a part of a groupage order.");
         return false;
		
		}
		function confirmUpdate1(){
			var checkSealNumber1=document.forms['containerForm'].elements['container.sealNumber'].value;
	        var checkSealDate=document.forms['containerForm'].elements['container.sealDate'].value;
	        if(checkSealNumber1!=''&& checkSealDate ==''){
	            alert('Please Enter the Seal Date for the Seal Number Entered');
	              return false; 
	         }
/*			var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for this order only.");
			if(agree){
			
			document.forms['containerForm'].elements['updateRecords'].value='updateAll';
			
			}
			else{*/
			document.forms['containerForm'].elements['updateRecords'].value='updateCurrent';
			/*}*/
		}
</script> 
<SCRIPT type="text/javascript">
 var http21=getHTTPObject21();
 
 function getHTTPObject21()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function checkStatus(targetStatus,targetElement){

var oldSize='${container.size}';
var newSize=document.getElementById("containerSize").value;

 var url="findNewVolume.html?ajax=1&decorator=simple&popup=true&containerSize="+encodeURI(newSize);
   http21.open("GET",url,true);
   http21.onreadystatechange=httpResponseNewVolume;
   http21.send(null);


}

 function httpResponseNewVolume(){
   if(http21.readyState==4){
     var result=http21.responseText
     var usedVol=document.forms['containerForm'].elements['usedVolume'].value;
     
     result=result.trim();
    
     if(usedVol!=''){
     if(result!='' && result!='0'){

       var total=parseFloat(result)-parseFloat(usedVol);
       total=parseFloat(total).toFixed(2);
       if(total<0){
       alert("You cannot change the container size as used volume exceeds avaliable volume.");
       document.getElementById("containerSize").value='${container.size}';
       }
      }
      else{
       alert("You cannot change the container size as used volume exceeds avaliable volume.");
       document.getElementById("containerSize").value='${container.size}';
      }
     }
     
   }
   }
 
 function checkMandatoryFieldUGCA(){
		var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
		str="1";
		</configByCorp:fieldVisibility>
	  	var routingVal="${serviceOrder.routing}";
		var cnrt = document.getElementById('containerNumberRequiredTrue');
		var cnrf = document.getElementById('containerNumberRequiredFalse');
		var szrt = document.getElementById('sizeRequiredTrue');
		var szrf = document.getElementById('sizeRequiredFalse');
		
		if((str=="1") && (routingVal =='EXP' || routingVal =='IMP')){
			cnrt.style.display = 'block';		
			cnrf.style.display = 'none';
			szrt.style.display = 'block';		
			szrf.style.display = 'none';
			
		}else if((str=="1") && (routingVal !='EXP' && routingVal !='IMP')){
			cnrt.style.display = 'none';		
			cnrf.style.display = 'block';
			szrt.style.display = 'none';		
			szrf.style.display = 'block';
			
		}else if(str=="0"){
			cnrt.style.display = 'none';
			cnrf.style.display = 'block';
			szrt.style.display = 'none';
			szrf.style.display = 'block';
			
		}else{
		
		}
	}
	function checkBlankField(){
		var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
		str="1";
		</configByCorp:fieldVisibility>
		var conNumber= document.forms['containerForm'].elements['container.containerNumber'].value;
		var size= document.forms['containerForm'].elements['container.size'].value;
		
		var routingVal="${serviceOrder.routing}";

		if((str=="1") && (routingVal =='EXP' || routingVal =='IMP') && (conNumber=='')){
			alert("SS Container is a required field.");
			return false;
		}else if((str=="1") && (routingVal =='EXP' || routingVal =='IMP') && (size=='')){
			alert("Size is a required field.");
			return false;		
		}else{
			return submit_form();
		}	
	}
	
</SCRIPT>  
<script type="text/javascript"> 
  makeSealDateMandatory();
</script>  
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
	checkMandatoryFieldUGCA();
</script>
<script type="text/javascript">
	try{  	
  		document.getElementById('idNum').focus();
  	}catch(e){}
</script>
<script type="text/javascript">
	<c:if test="${hitFlag=='1'}">	 
	  window.close();
	  try {
			window.opener.getContainerDetails();
			window.opener.getCartonDetails();
			window.opener.getVehicleDetails();
			window.opener.getRoutingDetails();
			window.opener.getCustomDetails();
			window.opener.getInlandAgentDetails();
		}catch(e){
			window.opener.document.forms[0].submit();
		}
	</c:if>
</script>