package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.inject.util.Function;
import com.trilasoft.app.model.ItemData;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.ItemDataManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.WorkTicketManager;

public class ItemDataAction extends BaseAction implements Preparable{
	
	//sid=703276&workTicketId=1312035&shipNumber=VOER104177301
	private Long id;
	private Integer sid;
	private Integer workTicketId;
	private String shipNumber;
	private Map<String, String>  itemList = new TreeMap<String, String>();
	
	private Map<String, String>  house;
	private String corpID;
	private RefMasterManager refMasterManager;
	private String userName;
	private ItemData itemData;
	private ItemDataManager itemDataManager;
	private WorkTicketManager workTicketManager;
	private List dataItemList;
	private List handOutList;
	private boolean hoValue;
	private boolean releaseAllItemsValue;
	private WorkTicket workTicket;
	private Integer maxLineValue;
	private String valueOfNotes;
	private SystemDefault systemDefault;
	private SystemDefaultManager systemDefaultManager;
	private Long systemDefaultId;
	private String unitFromSystemDefault;
	private String ticketAssignedStatus;

	public ItemDataAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.corpID = user.getCorpID();
        this.userName = user.getUsername();
    }
	public String allItemDataList()
	{
		house =	refMasterManager.findByParameter(corpID, "HOUSE");
		dataItemList=workTicketManager.getAllDataItemList(workTicketId.longValue(),shipNumber);
		return SUCCESS;
	}

	private String workTicketURL;
	private Integer ticket;
	private String closeFlag;
	private String flagForHO;
	private Double weightExpe;
	private Double VolumeExpe;
	private Integer qtyExpe;
	public String save()  {
		  // boolean isNew = (id == null);
		if(itemData.getId()==null)
		{
			   itemData.setCreatedOn(new Date());
			   itemData.setCreatedBy(userName);
		   	   itemData.setCorpId(corpID);
		   	   itemData.setTicket(ticket);
		   	   itemData.setServiceOrderId(sid);
			   itemData.setWorkTicketId(workTicketId);
			   itemData.setShipNumber(shipNumber);
			   maxLineValue= itemDataManager.getMaxLine(shipNumber);
			   itemData.setLine(maxLineValue+1);
		}
		else if(flagForHO.equalsIgnoreCase("HO")){
			
		ItemData itemDataOld = itemDataManager.get(itemData.getId());
		 weightExpe = itemDataOld.getWeight();
		 VolumeExpe = itemDataOld.getVolume();
		 qtyExpe = Integer.parseInt(itemDataOld.getQtyExpected());
		 if((qtyExpe != itemData.getQuantityReceived().intValue()) || (VolumeExpe != itemData.getVolumeReceived().doubleValue()) || (weightExpe != itemData.getWeightReceived().doubleValue())){
		 itemDataOld = new ItemData();
		 itemDataOld.setQuantityReceived(qtyExpe - itemData.getQuantityReceived());
		 itemDataOld.setVolumeReceived(VolumeExpe - itemData.getVolumeReceived());
		 itemDataOld.setWeightReceived(weightExpe - itemData.getWeightReceived());
		 itemDataOld.setItemQtyReceived(String.valueOf(qtyExpe - itemData.getQuantityReceived()));
		 itemDataOld.setQtyExpected(String.valueOf(itemDataOld.getQuantityReceived()));
		 itemDataOld.setVolume(itemDataOld.getVolumeReceived());
		 itemDataOld.setWeight(itemDataOld.getWeightReceived());
		 itemDataOld.setCreatedOn(new Date());
		 itemDataOld.setCreatedBy(userName);
		 itemDataOld.setUpdatedOn(new Date());
		 itemDataOld.setUpdatedBy(userName);
		 itemDataOld.setCorpId(corpID);
		 itemDataOld.setServiceOrderId(itemData.getServiceOrderId());
		 itemDataOld.setWorkTicketId(itemData.getWorkTicketId());
		 itemDataOld.setShipNumber(itemData.getShipNumber());
		 maxLineValue= itemDataManager.getMaxLine(itemData.getShipNumber());
		 itemDataOld.setLine(maxLineValue+1);
		 itemDataOld.setItemDesc(itemData.getItemDesc());
		 itemDataOld.setPackId(itemData.getPackId());
		 itemDataOld.setItemUomid(itemData.getItemUomid());
		 itemDataOld.setWarehouse(itemData.getWarehouse());
		 itemDataOld.setUnitOfVolume(itemData.getUnitOfVolume());
		 itemDataOld.setUnitOfWeight(itemData.getUnitOfWeight());
		 itemDataOld.setPackId(itemData.getPackId());
		 itemDataOld.setTicket(itemData.getTicket());
		 itemDataOld.setItemUomid(itemData.getItemUomid());
		 itemDataOld.setNotes(itemData.getNotes());
		 
			itemDataManager.save(itemDataOld);
		 	itemData.setQtyExpected(String.valueOf(itemData.getQuantityReceived()));
			itemData.setVolume(itemData.getVolumeReceived());
			itemData.setWeight(itemData.getWeightReceived());
		 }
		}
			Long aa = Long.parseLong(itemData.getQtyExpected());
	        itemData.setQuantityOrdered(aa.intValue());
			itemData.setUpdatedOn(new Date());
			itemData.setUpdatedBy(userName);
		 itemData = itemDataManager.save(itemData);
		 if(flagForHO.equalsIgnoreCase("HO")){
			 closeFlag="CLSDForHO";	 
		 }else{
		 closeFlag="CLSD";
		 }
		return SUCCESS;
		
		/*
		
				Long aa = Long.parseLong(itemData.getQtyExpected());
		        itemData.setQuantityOrdered(aa.intValue());
				itemData.setUpdatedOn(new Date());
				itemData.setUpdatedBy(userName);
		 itemData = itemDataManager.save(itemData);
		 closeFlag="CLSD";
		return SUCCESS;*/
		 
	 }
	
	public String edit() {
		 if (id != null) {
			house =	refMasterManager.findByParameter(corpID, "HOUSE");
			itemList= refMasterManager.findByParameter(corpID, "ITEMS");
			List<String> list = new ArrayList<String>(itemList.keySet());
			Collections.sort(list,new sortedItemListComp());
			System.out.println(list);
			/*Map map = new LinkedHashMap();*/
			Iterator it = list.iterator();
				while(it.hasNext()){
					String aa = (String) it.next();
					sortedItemList.put(aa, aa);
					}
			
			
			 itemData = itemDataManager.get(id);
	        }else{
	        	itemData = new ItemData();
	        }
       return SUCCESS;
   }
	
	 public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Date createdOn1;
	public Date getCreatedOn1() {
		return createdOn1;
	}

	public void setCreatedOn1(Date createdOn1) {
		this.createdOn1 = createdOn1;
	}
	
	class MyComparator implements Comparator<String>
    {
        public int compare(String o1,String o2)
        {
			
        	
        	return sid;
        }
    }
	
	class sortedItemListComp implements Comparator
	{
		public int compare(Object o1, Object o2) {
			
			String num1 = ((String) o1).split(":")[0];
			Integer aa =Integer.parseInt(num1);
			  String num2 = ((String) o2).split(":")[0];
			  Integer bb =Integer.parseInt(num2);
			  return  aa.compareTo(bb);
		}
	}
	
	@SkipValidation
	public String dataListForm(){
		if ((systemDefaultManager.findByCorpID(corpID)).isEmpty()) {

		} else {
			systemDefaultId = Long.parseLong(systemDefaultManager.findIdByCorpID(corpID).get(0).toString());
		}
		if (systemDefaultId != null) {
			systemDefault = systemDefaultManager.get(systemDefaultId);	
		}
		unitFromSystemDefault = systemDefault.getWeightUnit();
		workTicket = workTicketManager.get(workTicketId.longValue());
		house =	refMasterManager.findByParameter(corpID, "HOUSE");
		itemList= refMasterManager.findByParameter(corpID, "ITEMS");
		List<String> list = new ArrayList<String>(itemList.keySet());
		Collections.sort(list,new sortedItemListComp());
		System.out.println(list);
		Iterator it = list.iterator();
				while(it.hasNext())
				{
				String aa = (String) it.next();
				sortedItemList.put(aa, aa);
				}
		
		itemData = new ItemData();
		itemData.setCreatedOn(new Date());
		itemData.setUpdatedOn(new Date());
		itemData.setWarehouse(workTicket.getWarehouse());
		maxLineValue= itemDataManager.getMaxLine(shipNumber);
		itemData.setLine(maxLineValue+1);
		if(unitFromSystemDefault.equalsIgnoreCase("kgs")){
		itemData.setUnitOfWeight("Kg");	
		itemData.setUnitOfVolume("Cbm");
		}else{
			itemData.setUnitOfWeight("Pound");	
			itemData.setUnitOfVolume("Cft");
		}
		 return SUCCESS;
	 }
	
	private String itemDescription;
	private String returnAjaxStringValue;
	
	public String weightAndVolume()
	
	{
		returnAjaxStringValue=itemDataManager.weightAndVolumeFromRfMaster(itemDescription);
		return SUCCESS;
	}
	
	
	private String totalWeight;
	private String totalVolume;
	private String totalEstimatedPieces;
	private String totalEstimatedVolume;
	private String totalEstimatedWeight;

	public void updateItemDataWeightVolume()
	{
		itemDataManager.updateItemDataWeightAndVolume(totalWeight,totalVolume,workTicketId,totalEstimatedPieces,totalEstimatedVolume,totalEstimatedWeight);
	}
	
	private String totalActualQty;
	public void totalActQty()
	{
		itemDataManager.updatetotalActQty(workTicketId,totalActualQty);
	}
	
	public void deleteFromItemData()
	{
		itemDataManager.deleteFromItemData(id,workTicketId);
	}
	
	public String AllHandOutList()
	{
		house =	refMasterManager.findByParameter(corpID, "HOUSE");
		handOutList=itemDataManager.getAllHandOutList(shipNumber);
		return SUCCESS;
	}
	
	private Boolean flagForHandOutList = false;
	private long handoutItemId;
	public String itemSearchOnHandOut()
	{
		flagForHandOutList = true;  
		house =	refMasterManager.findByParameter(corpID, "HOUSE");
		handOutList=itemDataManager.getAllHandOutList(shipNumber,itemDescription);
		return SUCCESS;
	}
	
	@SkipValidation
	public void updateHandoutItem()
	{
		itemDataManager.updateHandoutList(shipNumber,handoutItemId,ticket);
	}
	@SkipValidation
	public void updateAllHoTicket()
	{
		itemDataManager.updateAllHoTicketInHandOutList(shipNumber,ticket,releaseAllItemsValue);
	}
	
	//@SkipValidation
	public String updateHoTicketByHoClick()
	{
		returnAjaxStringValue = itemDataManager.updateHoTicketByHo(shipNumber,id,hoValue,ticket);
		return SUCCESS;
	}
	
	@SkipValidation
	public void autoSaveNotesInItem(){
		 itemDataManager.saveNotes(id,workTicketId,valueOfNotes);
	}
	
	public long getHandoutItemId() {
		return handoutItemId;
	}

	public void setHandoutItemId(long handoutItemId) {
		this.handoutItemId = handoutItemId;
	}

	public Boolean getFlagForHandOutList() {
		return flagForHandOutList;
	}

	public void setFlagForHandOutList(Boolean flagForHandOutList) {
		this.flagForHandOutList = flagForHandOutList;
	}

	public String getReturnAjaxStringValue() {
		return returnAjaxStringValue;
	}

	public void setReturnAjaxStringValue(String returnAjaxStringValue) {
		this.returnAjaxStringValue = returnAjaxStringValue;
	}

	public String getItemDescription() {
		return itemDescription;
	}

	public void setItemDescription(String itemDescription) {
		this.itemDescription = itemDescription;
	}

	 public Map<String, String> getHouse() {
			return house;
		}
		public void setHouse(Map<String, String> house) {
			this.house = house;
		}

	 
	 public Integer getSid() {
			return sid;
		}

		public void setSid(Integer sid) {
			this.sid = sid;
		}
	
	public Integer getWorkTicketId() {
		return workTicketId;
	}

	public void setWorkTicketId(Integer workTicketId) {
		this.workTicketId = workTicketId;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public void prepare() throws Exception {
		
	}

	public Map<String, String> getItemList() {
		return itemList;
	}

	public void setItemList(Map<String, String> itemList) {
		this.itemList = itemList;
	}

	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	public ItemData getItemData() {
		return itemData;
	}

	public void setItemData(ItemData itemData) {
		this.itemData = itemData;
	}

	public void setItemDataManager(ItemDataManager itemDataManager) {
		this.itemDataManager = itemDataManager;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public String getWorkTicketURL() {
		return workTicketURL;
	}
	public void setWorkTicketURL(String workTicketURL) {
		this.workTicketURL = workTicketURL;
	}
	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

	public String getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(String totalVolume) {
		this.totalVolume = totalVolume;
	}
	
	public List getHandOutList() {
		return handOutList;
	}

	public void setHandOutList(List handOutList) {
		this.handOutList = handOutList;
	}
	
	public Integer getTicket() {
		return ticket;
	}

	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	
	public boolean isHoValue() {
		return hoValue;
	}

	public void setHoValue(boolean hoValue) {
		this.hoValue = hoValue;
	}
	
	public boolean isReleaseAllItemsValue() {
		return releaseAllItemsValue;
	}

	public void setReleaseAllItemsValue(boolean releaseAllItemsValue) {
		this.releaseAllItemsValue = releaseAllItemsValue;
	}
	
	public Integer getMaxLineValue(){
		return maxLineValue;
	}

	public void setMaxLineValue(Integer maxLineValue) {
		this.maxLineValue = maxLineValue;
	}

	public WorkTicket getWorkTicket(){
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket){
		this.workTicket = workTicket;
	}
	

	public String getValueOfNotes(){
		return valueOfNotes;
	}

	public void setValueOfNotes(String valueOfNotes){
		this.valueOfNotes = valueOfNotes;
	}

	Map sortedItemList = new LinkedHashMap();

	public Map getSortedItemList() {
		return sortedItemList;
	}

	public void setSortedItemList(Map sortedItemList){
		this.sortedItemList = sortedItemList;
	}

	public List getDataItemList() {
		return dataItemList;
	}

	public void setDataItemList(List dataItemList){
		this.dataItemList = dataItemList;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager){
		this.workTicketManager = workTicketManager;
	}
	
	public SystemDefault getSystemDefault() {
		return systemDefault;
	}
	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}
	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}
	
	public String getUnitFromSystemDefault() {
		return unitFromSystemDefault;
	}
	public void setUnitFromSystemDefault(String unitFromSystemDefault) {
		this.unitFromSystemDefault = unitFromSystemDefault;
	}
	public Long getSystemDefaultId() {
		return systemDefaultId;
	}
	public void setSystemDefaultId(Long systemDefaultId) {
		this.systemDefaultId = systemDefaultId;
	}
	public String getCloseFlag() {
		return closeFlag;
	}
	public void setCloseFlag(String closeFlag) {
		this.closeFlag = closeFlag;
	}
	
	public String getTotalEstimatedPieces() {
		return totalEstimatedPieces;
	}
	public void setTotalEstimatedPieces(String totalEstimatedPieces) {
		this.totalEstimatedPieces = totalEstimatedPieces;
	}
	public String getTotalEstimatedVolume() {
		return totalEstimatedVolume;
	}
	public void setTotalEstimatedVolume(String totalEstimatedVolume) {
		this.totalEstimatedVolume = totalEstimatedVolume;
	}
	public String getTotalEstimatedWeight() {
		return totalEstimatedWeight;
	}
	public void setTotalEstimatedWeight(String totalEstimatedWeight) {
		this.totalEstimatedWeight = totalEstimatedWeight;
	}
	public String getTicketAssignedStatus() {
		return ticketAssignedStatus;
	}
	public void setTicketAssignedStatus(String ticketAssignedStatus) {
		this.ticketAssignedStatus = ticketAssignedStatus;
	}
	public String getTotalActualQty() {
		return totalActualQty;
	}
	public void setTotalActualQty(String totalActualQty) {
		this.totalActualQty = totalActualQty;
	}
	public String getFlagForHO() {
		return flagForHO;
	}
	public void setFlagForHO(String flagForHO) {
		this.flagForHO = flagForHO;
	}
}
