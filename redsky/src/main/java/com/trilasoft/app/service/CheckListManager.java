package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CheckList;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.ToDoRule;

public interface CheckListManager extends GenericManager<CheckList, Long> {
	public List getById();
	public List getControlDateList(String sessionCorpID);
	public List execute(String sessionCorpID); 
	public void deleteThisResult(Long checkListId, String corpID);
	public List <CheckListResult> getResultList(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed);
	public List executeThisRule(CheckList checkList, String sessionCorpID);
	public List findNumberOfResultEntered(String sessionCorpID, Long id);
	public String executeTestRule(CheckList checkList, String sessionCorpID);
	public Integer findNumberOfResult(String sessionCorpID); 
	public List executeTdrCheckList(String sessionCorpID,Long currentRuleNumber,Long ruleId);
	public void updateAndDel(String sessionCorpID,Long currentRuleNumber,Long ruleId);
	public List <ToDoRule> getAllRules(String sessionCorpID);
	public List <CheckListResult> getResultListExtCordinator(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userType);
	public List <CheckListResult> getResultListExtCordinatorForTeam(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userName,String allradio,String userType);
}
