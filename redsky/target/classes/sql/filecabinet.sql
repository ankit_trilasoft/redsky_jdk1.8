DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`filecabinet` $$
CREATE DEFINER=`root`@`%` PROCEDURE `filecabinet`(
IN pId BIGINT(20),
IN pTableName VARCHAR(100),
IN pFieldNameList LONGTEXT,
IN pOldValueList LONGTEXT,
IN pNewValueList LONGTEXT,
IN pUser VARCHAR(255),
IN pCorpID VARCHAR(25),
IN pDated TIMESTAMP,
IN pfileid VARCHAR(25)
)
BEGIN
DECLARE pFieldName LONGTEXT;
DECLARE remainingFields LONGTEXT;
DECLARE pOldValue LONGTEXT;
DECLARE remainingOldValues LONGTEXT;
DECLARE pNewValue LONGTEXT;
DECLARE remainingNewValues LONGTEXT;
DECLARE pUserId LONGTEXT;
DECLARE pAuditable int(2);
DECLARE pDesc VARCHAR(1500);
DECLARE palertValue VARCHAR(25);
DECLARE palertRole VARCHAR(90);
DECLARE palertUser VARCHAR(30);
DECLARE palertFileNumber VARCHAR(50);
DECLARE palertShipperName VARCHAR(160);
DECLARE pfiletype VARCHAR(160);
DECLARE ppid VARCHAR(160);
DECLARE pshiapnumber VARCHAR(160);
DECLARE pflex2 VARCHAR(160);
DECLARE pcode VARCHAR(160);
DECLARE pusertype VARCHAR(160);
DECLARE pcreatedby VARCHAR(160);
DECLARE lengthpfileid VARCHAR(160);

SET palertUser = "";
SET palertFileNumber = "";
SET palertShipperName = "";
SET palertValue = 'All Updates';
SET pAuditable = 0;
SET pDesc = "";

SELECT SUBSTRING_INDEX(pFieldNameList,'~',1) INTO pFieldName;
SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;
SELECT SUBSTRING_INDEX(pOldValueList,'~',1) into pOldValue;
SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;
SELECT SUBSTRING_INDEX(pNewValueList,'~',1) into pNewValue;
SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;


WHILE pFieldName <> '' DO
    begin
      DECLARE CONTINUE HANDLER FOR 1329
      SET pAuditable = 0;
      SET palertValue = '';

    SELECT id,description,alertValue into pAuditable,pDesc,palertValue FROM auditSetup where tablename = pTableName
    AND fieldname = pFieldName and auditable='true' and corpID = pCorpID  limit 0,1;


select length(pfileid) into lengthpfileid;

if(lengthpfileid = 11) then

select  s.id,filetype,m.createdby into ppid,pfiletype,pcreatedby from myfile m,customerfile s where customernumber=pfileid and s.sequencenumber=pfileid and m.id=PId;

end if;

if(lengthpfileid = 13) then

select  s.id,filetype,m.createdby into ppid,pfiletype,pcreatedby from myfile m, serviceorder s where fileid=pfileid and s.shipnumber=pfileid and m.id=PId;

end if;


select code,flex2 into pcode,pflex2 from refmaster where parameter='DOCUMENT' and corpid=pCorpID and code=pfiletype;

 select usertype into pusertype from app_user where username=pcreatedby;

    end;

      IF (pAuditable != 0) THEN




      IF(pflex2 = '') THEN


           IF(pTableName='myfile') THEN

                 SELECT coordinator,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;





           IF(pTableName='myfile') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid
 and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId  and m.updatedby like 'Networking%';




         END IF;


           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;



           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

     END IF ;



      IF(pflex2 = 'Coordinator') THEN


           IF(pTableName='myfile') THEN

                 SELECT coordinator,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;





           IF(pTableName='myfile') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid
 and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId  and m.updatedby like 'Networking%';




         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'Coordinator' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

     END IF ;

IF(pflex2 = 'personPricing') THEN


           IF(pTableName='myfile') THEN

                 SELECT personPricing,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'personPricing' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT b.personPricing,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personPricing' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'personPricing' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personPricing' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;





IF(pflex2 = 'personBilling') THEN


            IF(pTableName='myfile') THEN

                 SELECT personBilling,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'personBilling' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT b.personBilling,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personBilling' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'personBilling' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personBilling' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;








IF(pflex2 = 'estimator') THEN


           IF(pTableName='myfile') THEN

                 SELECT estimator,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'estimator' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT s.estimator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'estimator' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;



           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'estimator' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;


           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'estimator' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;






IF(pflex2 = 'personPayable') THEN


           IF(pTableName='myfile') THEN

                 SELECT personPayable,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'personPayable' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT b.personPayable,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personPayable' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'personPayable' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personPayable' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;





IF(pflex2 = 'auditor') THEN


            IF(pTableName='myfile') THEN

                 SELECT auditor,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'auditor' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT b.auditor,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'auditor' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'auditor' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'auditor' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;







IF(pflex2 = 'personForwarder') THEN


            IF(pTableName='myfile') THEN

                 SELECT personForwarder,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'personForwarder' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT b.personForwarder,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personForwarder' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;





           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'personForwarder' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'personForwarder' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;







IF(pflex2 = 'salesMan') THEN


            IF(pTableName='myfile') THEN

                 SELECT salesMan,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'salesMan' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT salesMan,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'salesMan' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;





           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'salesMan' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'salesMan' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;








IF(pflex2 = 'claimHandler') THEN


            IF(pTableName='myfile') THEN

                 SELECT coordinator,concat(sequencenumber,'#',c.id),concat(firstname,' ',lastname),'claimHandler' into palertUser,palertFileNumber,palertShipperName,palertRole
                 from customerfile c,myfile m where customernumber=pfileid and sequencenumber=pfileid  and m.id=pId and m.updatedby like 'Networking%';

           END IF;

           IF(pTableName='myfile') THEN

SELECT claimHandler,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'claimHandler' into palertUser,palertFileNumber,palertShipperName,palertRole
from billing b,serviceorder s ,myfile m where m.fileid=pfileid and s.shipnumber=pfileid and s.id=ppid and b.id=ppid and m.id=pId and m.updatedby like 'Networking%';



         END IF;




           IF(pusertype !='USER') THEN

SELECT c.coordinator,concat(c.sequencenumber,'#',c.id),concat(c.firstname,' ',c.lastname),'claimHandler' into palertUser,palertFileNumber,palertShipperName,palertRole
from customerfile c  where  c.sequencenumber=pfileid ;



      END IF ;

           IF(pusertype !='USER') THEN

SELECT s.coordinator,concat(s.shipnumber,'#',s.id),concat(s.firstname,' ',s.lastname),'claimHandler' into palertUser,palertFileNumber,palertShipperName,palertRole
from serviceorder s  where  s.shipnumber=pfileid and s.id=ppid ;



      END IF ;

      END IF ;



      INSERT INTO `history` (`xtranId`, `user`, `tableName`, `fieldName`, `oldValue`, `newValue`, corpID, description,dated,alertRole,alertUser,isViewed,alertFileNumber,alertShipperName)
      VALUES (pId, pUser, pTableName, pFieldName, pOldValue, pNewValue, pCorpID, pDesc,now(),palertRole,palertUser,false,palertFileNumber,palertShipperName);
        End IF ;
    SELECT SUBSTRING_INDEX(remainingFields,'~',1) INTO pFieldName;
    SELECT SUBSTRING_INDEX(pFieldName,'.',-1) INTO pFieldName;
    SET pFieldNameList = remainingFields;
    SELECT MID(pFieldNameList,LOCATE('~',pFieldNameList)+1,CHAR_LENGTH(pFieldNameList)) into remainingFields;
    SELECT SUBSTRING_INDEX(remainingOldValues,'~',1) into pOldValue;
    SET pOldValueList = remainingOldValues;
    SELECT MID(pOldValueList,LOCATE('~',pOldValueList)+1,CHAR_LENGTH(pOldValueList)) into remainingOldValues;
    SELECT SUBSTRING_INDEX(remainingNewValues,'~',1) into pNewValue;
    SET pNewValueList = remainingNewValues;
    SELECT MID(pNewValueList,LOCATE('~',pNewValueList)+1,CHAR_LENGTH(pNewValueList)) into remainingNewValues;
   SET pAuditable = 0;
   SET palertValue ='';
   set palertUser = '';
   set palertRole = '';
   SET palertFileNumber = '';
   SET palertShipperName = '';

END WHILE;
END $$

DELIMITER ;