package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.model.SubcontractorCharges;

public interface VanLineDao extends GenericDao<VanLine, Long> {
	
	public List<VanLine> vanLineList();
	
	public List<VanLine> searchVanLine(String agent,Date weekEnding, String glType, String reconcileStatus);
	
	public List<VanLine> showLine(String regNum);
	
	public List getVanLineListWithReconcile(String agent,Date weekending,String sessionCorpID);
	
	public List getVanLineListWithoutReconcile(String agent,Date weekEnding,String sessionCorpID);
	public int updateDownloadByFileName(String fileName, String sessionCorpID);
	public int vanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode);
	public int updateAccByID(String accID,String accVenderDetails,String venderDetailsList,String sessionCorpID);
	public List<SubcontractorCharges> splitCharge(Long id);
	public int updateAccByFileName(String fileName,String fileNoVenderDetails,String venderDetailsList, String sessionCorpID);
	public List chargeAllocation(Long id);
	public List getDownloadXMLList(String shipNum, String sessionCorpID,String fileName);
	public List<SubcontractorCharges> totalAmountSum(Long id);
	public List getProcessedXMLList(String shipNum, String sessionCorpID);
	public List getVanLineCodeCompDiv(String sessionCorpID);
	public List getSerachedSOList(String shipNum, String regNum,String lastName, String firstName, String sessionCorpID);
	public List getWeekendingList(String agent, String corpId);
	public List getMiscellaneousSettlement(String agent,Date weekending,String sessionCorpID);
	public List getCategoryWithSettlementList(String agent,Date weekending,String sessionCorpID);
	public List getRegNumList(String vanRegNum,String agent,Date weekending);
	public List vanLineCategoryRegNum(String vanRegNum,String agent,String weekending,String sessionCorpID);
	public List vanLineCategoryRegNumberListMethod(String category,String vanRegNum,String agent,String weekEnding );
	public List getRegNumVanline(String agent, Date weekEnding);
	public List getShipNumber(String vanRegNum);
	public List<SystemDefault> findsysDefault(String corpID);
	public int updateDownloadByID(String accID, String sessionCorpID);
	public String getCompanyCode(String accountingAgent, String sessionCorpID);
	public List getvanLineExtract(String agent,Date weekending,String sessionCorpID);
	public int updateAccountLineWithReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode);
	public List checkInvoiceDateForWithReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode);
	public int updateVanLineWithReconcile(Long id, String loginUser);
	public void updateAccRecInvoiceNumberWithReconcile(Long id, String loginUser, String recAccDateToFormat, String recInvoiceNumber1);
	public int ownBillingVanLineListForReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode);
	public int updateAccountLineWithOwnBillingReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode);

	public List checkInvoiceOwnBillingDateForWithReconcile(String agent, Date weekEnding,BigDecimal vanlineMinimumAmount,boolean automaticReconcile,String sessionCorpID, String uvlBillToCode, BigDecimal vanlineMaximumAmount, String vanlineExceptionChargeCode);
	public void updateAccRecInvoiceNumberWithOwnBillingReconcile(String idList, String loginUser, String recAccDateTo, String recInvoiceNumber1, String creditInvoiceNumber, Long soId, String sessionCorpID);
	public Map<String, String> getChargeCodeList(String billingContract,String corpId);
	public String findCompanyDivision(String agent, String sessionCorpID);
	public List findVanLineAgent(String agent, String sessionCorpID);
	public List getOwnBillingRegNumVanline(String agent, Date weekEnding);
	public List getDSTROwnBillingRegNumVanline(String agent, Date weekEnding, String sessionCorpID);
	public List getOwnBillingRegNumVanlineAmount(String agent, Date weekEnding, String regnum, String sessionCorpID);
	public List findByOwnerAndContact(String driverID, String sessionCorpID);
	public Map getvenderCodeDetails(String shipNum,String corpId);	
	
}	