package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.model.UserDevice;
import org.appfuse.service.GenericManager;

public interface UserDeviceManager extends GenericManager<UserDevice, Long>{
	public List findRecordByLoginUser(String loginUser, String deviceIdVal);
}
