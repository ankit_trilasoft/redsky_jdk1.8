package com.trilasoft.app.service.impl;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.time.DateUtils;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CheckListDao;
import com.trilasoft.app.model.CheckList;
import com.trilasoft.app.model.CheckListResult;
import com.trilasoft.app.model.CheckListResultDTO;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.service.CheckListManager;
import com.trilasoft.app.service.CheckListResultManager;

public class CheckListManagerImpl extends GenericManagerImpl<CheckList, Long>  implements CheckListManager{
	CheckListDao checkListDao;
	private CheckListResultManager checkListResultManager;
	public CheckListManagerImpl(CheckListDao checkListDao) {
		super(checkListDao);
		this.checkListDao=checkListDao;
	}

	public List getById(){
		return null;
	}

	public List getControlDateList(String sessionCorpID) {
		return checkListDao.getControlDateList(sessionCorpID);
	}

	public List execute(String sessionCorpID) {
		List<CheckList> checkLists = checkListDao.getAllCheckListRules(sessionCorpID);
		for(CheckList checkList:checkLists){
			checkListDao.deleteThisResult(checkList.getId(), checkList.getCorpID());
			List<Object[]> results = executeRule(checkList, sessionCorpID);
			for (Object[] checkListResults: results){
				Object id = checkListResults[2];
				Object duration = checkListResults[1];
				
				CheckListResult checkListResult = new CheckListResult();
				checkListResult.setCheckListId(checkList.getId());
				checkListResult.setResultId(Long.parseLong(id.toString()));
				checkListResult.setCorpID(checkList.getCorpID());
				//checkListResult.setDuration(Long.parseLong(duration.toString()));
				if(duration!=null){
					checkListResult.setDuration(Long.parseLong(duration.toString()));	
					}else{
						checkListResult.setDuration((long) 0);	
					}
				checkListResult.setMessageDisplayed(checkList.getMessageDisplayed());
				String shipper = getShipper(checkListResults);
				checkListResult.setShipper(shipper);
				
				if(checkList.getOwner().equalsIgnoreCase("Coordinator"))
				{
				  if(checkListResults[5]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[5].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("Billing"))
				{
					if(checkListResults[6]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[6].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("Consultant"))
				{
					if(checkListResults[7]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[7].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("Payable"))
				{
					if(checkListResults[8]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[8].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("Pricing"))
				{
					if(checkListResults[9]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[9].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("SalesMan"))
				{
					if(checkListResults[10]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[10].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("Auditor")){
					if(checkListResults[12]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[12].toString());}
				}
				if(checkList.getTableRequired().equalsIgnoreCase("ServiceOrder")){
					
					if(checkList.getOwner().equalsIgnoreCase("OriginAgent")){
						if(checkListResults[19]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[19].toString());}
					}	
					if(checkList.getOwner().equalsIgnoreCase("DestinationAgent")){
						if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
					}
					if(checkList.getOwner().equalsIgnoreCase("OriginSubAgent")){
						if(checkListResults[14]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[14].toString());}
					}
					if(checkList.getOwner().equalsIgnoreCase("DestinationSubAgent")){
						if(checkListResults[15]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[15].toString());}
					}

					if(checkList.getOwner().equalsIgnoreCase("ForwarderAgent")){
						if(checkListResults[16]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[16].toString());}
						
					}
					if(checkList.getOwner().equalsIgnoreCase("BrokerAgent")){
						if(checkListResults[17]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[17].toString());}
						
					}
					if(checkList.getOwner().equalsIgnoreCase("MM Counselor")){
						if(checkListResults[18]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[18].toString());}
					}
				}
				if(checkList.getTableRequired().equalsIgnoreCase("CustomerFile")){
					if(checkList.getOwner().equalsIgnoreCase("OriginAgent")){
						if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
					}
				}
				if(checkList.getTableRequired().equalsIgnoreCase("Billing")){
					if(checkList.getOwner().equalsIgnoreCase("Claims")){
						if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
					}	
					if(checkList.getOwner().equalsIgnoreCase("Forwarder")){
						if(checkListResults[14]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[14].toString());}
					}
					}
			
				if(checkList.getOwner()== null){
					checkListResult.setOwner("");
				}
				
				checkListResult.setResultNumber(checkListResults[11].toString());
				checkListResult.setUrl(checkList.getUrl());
				checkListResult.setCreatedOn(new Date());
				checkListResult.setUpdatedOn(new Date());
				checkListResult.setCreatedBy("List Execution");
				checkListResult.setUpdatedBy("List Execution");
				checkListResultManager.save(checkListResult);
			}
		}
		return checkLists;
	}
	public List executeTdrCheckList(String sessionCorpID,Long currentRuleNumber,Long ruleId) {
		//checkListDao.deleteThisResult(ruleId, sessionCorpID);
		ArrayList<CheckListResult> results = new ArrayList<CheckListResult>();
		HashMap<String, CheckListResultDTO> checkLists = checkListDao.getAllToDoResultListForCheckList(sessionCorpID,currentRuleNumber);
		
		for(Map.Entry<String, CheckListResultDTO> entry : checkLists.entrySet()){
			
			CheckListResultDTO checkListResultDto = entry.getValue();
			CheckListResult checkListResult = null;
			if(checkListResultDto.getId()!=null){
				checkListResult =  checkListResultManager.get(checkListResultDto.getId());
			}else{
				checkListResult = new CheckListResult(); 
				checkListResult.setCreatedOn(new Date());
				checkListResult.setCreatedBy("List Execution");
			}
				checkListResult.setCheckListId(checkListResultDto.getCheckListId());
				checkListResult.setResultId(checkListResultDto.getResultId());
				checkListResult.setCorpID(checkListResultDto.getCorpID());
				//checkListResult.setDuration(Long.parseLong(duration.toString()));
				if(checkListResultDto.getDuration()!=null){
					checkListResult.setDuration(checkListResultDto.getDuration());	
					}else{
						checkListResult.setDuration((long) 0);
						
					}
				checkListResult.setMessageDisplayed(checkListResultDto.getMessageDisplayed());
				checkListResult.setShipper(checkListResultDto.getShipper());
				if(checkListResultDto.getOwner()!= null){
					checkListResult.setOwner(checkListResultDto.getOwner());
				}else{
					checkListResult.setOwner("");	
				}
				
				checkListResult.setResultNumber(checkListResultDto.getResultNumber());
				checkListResult.setUrl("myFiles");
				
				checkListResult.setUpdatedOn(new Date());
				
				checkListResult.setUpdatedBy("List Execution");
				checkListResult.setRuleNumber(checkListResultDto.getRuleNumber());
				checkListResult.setDocType(checkListResultDto.getDocType());
				checkListResult.setToDoResultId(checkListResultDto.getToDoResultId());
				checkListResult.setCheckResultForDel("1");
				checkListResult = checkListResultManager.save(checkListResult);
				results.add(checkListResult);
			}
		return results;
	}
	private String getShipper(Object[] checkListResults) {
		return (String)checkListResults[3] + " " +  (String)checkListResults[4];
	}

	private List executeRule(CheckList checkList, String sessionCorpID) {
		String ruleExpression = buildExpression(checkList, sessionCorpID);
		return checkListDao.executeExpression(ruleExpression);
	}

	private String buildExpression(CheckList checkList, String sessionCorpID) {
		String table = checkList.getTableRequired();
		String controlDate = checkList.getControlDate();
		int duration= Integer.parseInt(checkList.getDuration().toString());
		//duration = -duration;
		Date compareWithDate = DateUtils.addDays(getCurrentDate(), duration);
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String compareWithDateStr = dfm.format(compareWithDate);
		String dateExpression ="";
		if(duration==0){
			dateExpression=controlDate + " <= '" + new Date()+"'";
		}else{
			dateExpression=controlDate + " <= '" + compareWithDateStr+"'";
		}
			
		String ruleExpression11="";
		ruleExpression11=CheckListExpressionBuilder.buildExpression(table, dateExpression, sessionCorpID,checkList);
		String ruleExpression="";
		ruleExpression="select " +checkList.getId()+ " , "+"DATEDIFF(DATE_ADD(now(), INTERVAL "+duration+" DAY),"+controlDate+") as duration "+  //0,1
		" , "+getJoinid(table)+" , " + // 2
		" "+CheckListExpressionBuilder.getShipper(table) +" , " + // 3,4
		" "+CheckListExpressionBuilder.getCoordinator(table)+", " + // 5 
		" "+CheckListExpressionBuilder.getBilling(table) +" , " + // 6
		" "+CheckListExpressionBuilder.getConsultant(table) +" , " + // 7
		" "+CheckListExpressionBuilder.getPayable(table) +" , " + // 8
		" "+CheckListExpressionBuilder.getPricing(table)+" , " + // 9
		" "+CheckListExpressionBuilder.getSalesMan(table)+" , " + // 10
		" "+CheckListExpressionBuilder.getFileNumber(table)+" , " + // 11
		" "+CheckListExpressionBuilder.getAuditor(table)+ "," ;//12
		if(table.equals("ServiceOrder")){
		ruleExpression = ruleExpression +" "+CheckListExpressionBuilder.getDestinationAgent(table)+ ","+ //13
		" "+CheckListExpressionBuilder.getOrginSubAgent(table)+ ","+ //14
		" "+CheckListExpressionBuilder.getDestinationSubAgent(table)+ ","+ //15
		" "+CheckListExpressionBuilder.getForwarderAgent(table)+ ","+ // 16
		" "+CheckListExpressionBuilder.getBrokerAgent(table)+ ","+ // 17
		" "+CheckListExpressionBuilder.getMmCounselor(table)+","+ //18
		" "+CheckListExpressionBuilder.getOrginAgent(table)+ ",";//19
		}
		if(table.equals("Billing")){
			ruleExpression = ruleExpression +" "+CheckListExpressionBuilder.getForwarder(table)+","+ //13
			" "+ CheckListExpressionBuilder.getClaimHandLer(table)+","; //14
		}
		if(table.equalsIgnoreCase("CustomerFile")){
			ruleExpression = ruleExpression +" "+ CheckListExpressionBuilder.getOrginAgent(table)+" ,"; //13
		}
		ruleExpression = ruleExpression +" group_concat(filetype) documenttypes " + // 12
		" " + ruleExpression11;
			
		System.out.println("\n\n\n Expression-->>"+ruleExpression);
		return ruleExpression;
	}

	private String getJoinid(String table) {
		String joinid = table;
		
		if (table.equals("ServiceOrder")){
			joinid = " serviceorder.id";
		}
		else if (table.equals("CustomerFile")){
			joinid = " customerfile.id";
		}
		else if (table.equals("ServicePartner")){
			joinid = " servicepartner.id";
		}
		else if (table.equals("Vehicle")){
			joinid = " vehicle.id";
		}
		else if (table.equals("AccountLine")){
			joinid = " accountline.id";
		}
		else if (table.equals("WorkTicket")){
			joinid = " workticket.id";
		}
		
		else if (table.equals("AccountLineList")){
			joinid = " serviceorder.id";
		}
		
		else if (table.equals("Billing")){
			joinid = " billing.id";
		}
		
		else if (table.equals("Miscellaneous")){
			joinid = " miscellaneous.id";
		}else if(table.equals("QuotationFile")){
			joinid = " customerfile.id";
		}else if(table.equals("Claim")){
			joinid = " claim.id";
		}else if(table.equals("TrackingStatus")){
			joinid = " trackingstatus.id";
		}else if(table.equals("DspDetails")){
			joinid = " dspdetails.id";
		}
		
		return joinid;
		
	}

	private Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);
			
		} catch (java.text.ParseException e) {
			
			e.printStackTrace();
		}
		return currentDate;
	}
	public List executeThisRule(CheckList checkList, String sessionCorpID) {
		checkListDao.deleteThisResult(checkList.getId(), checkList.getCorpID());
		List<Object[]> results = executeRule(checkList, sessionCorpID);
		for (Object[] checkListResults: results){
			Object id = checkListResults[2];
			Object duration = checkListResults[1];
			
			CheckListResult checkListResult = new CheckListResult();
			checkListResult.setCheckListId(checkList.getId());
			checkListResult.setResultId(Long.parseLong(id.toString()));
			checkListResult.setCorpID(checkList.getCorpID());
			if(duration!=null){
			checkListResult.setDuration(Long.parseLong(duration.toString()));	
			}else{
				checkListResult.setDuration((long) 0);	
			}
			checkListResult.setMessageDisplayed(checkList.getMessageDisplayed());
			String shipper = getShipper(checkListResults);
			checkListResult.setShipper(shipper);
			if(checkList.getOwner().equalsIgnoreCase("Coordinator"))
			{
			  if(checkListResults[5]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[5].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("Billing"))
			{
				if(checkListResults[6]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[6].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("Consultant"))
			{
				if(checkListResults[7]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[7].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("Payable"))
			{
				if(checkListResults[8]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[8].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("Pricing"))
			{
				if(checkListResults[9]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[9].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("SalesMan"))
			{
				if(checkListResults[10]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[10].toString());}
			}
			if(checkList.getOwner().equalsIgnoreCase("Auditor")){
				if(checkListResults[12]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[12].toString());}
			}
			if(checkList.getTableRequired().equalsIgnoreCase("ServiceOrder")){
				
				if(checkList.getOwner().equalsIgnoreCase("OriginAgent")){
					if(checkListResults[19]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[19].toString());}
				}	
				if(checkList.getOwner().equalsIgnoreCase("DestinationAgent")){
					if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("OriginSubAgent")){
					if(checkListResults[14]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[14].toString());}
				}
				if(checkList.getOwner().equalsIgnoreCase("DestinationSubAgent")){
					if(checkListResults[15]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[15].toString());}
				}

				if(checkList.getOwner().equalsIgnoreCase("ForwarderAgent")){
					if(checkListResults[16]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[16].toString());}
					
				}
				if(checkList.getOwner().equalsIgnoreCase("BrokerAgent")){
					if(checkListResults[17]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[17].toString());}
					
				}
				if(checkList.getOwner().equalsIgnoreCase("MM Counselor")){
					if(checkListResults[18]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[18].toString());}
				}
			}
			if(checkList.getTableRequired().equalsIgnoreCase("CustomerFile")){
				if(checkList.getOwner().equalsIgnoreCase("OriginAgent")){
					if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
				}
			}
			if(checkList.getTableRequired().equalsIgnoreCase("Billing")){
				if(checkList.getOwner().equalsIgnoreCase("Claims")){
					if(checkListResults[13]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[13].toString());}
				}	
				if(checkList.getOwner().equalsIgnoreCase("Forwarder")){
					if(checkListResults[14]==null){checkListResult.setOwner("");}else{checkListResult.setOwner(checkListResults[14].toString());}
				}
				}
		
			if(checkList.getOwner()== null){
				checkListResult.setOwner("");
			}
			checkListResult.setResultNumber(checkListResults[11].toString());
			checkListResult.setUrl(checkList.getUrl());
			checkListResult.setCreatedOn(new Date());
			checkListResult.setUpdatedOn(new Date());
			checkListResult.setCreatedBy("Page Execution");
			checkListResult.setUpdatedBy("Page Execution");
			checkListResultManager.save(checkListResult);
		}
		
		return results;
	}
	public void setCheckListResultManager(
			CheckListResultManager checkListResultManager) {
		this.checkListResultManager = checkListResultManager;
	}

	public void deleteThisResult(Long checkListId, String corpID) {
		checkListDao.deleteThisResult(checkListId, corpID);		
	}
	public void deleteCheckThisResult(Long checkListId, String corpID) {
		checkListDao.deleteThisResult(checkListId, corpID);		
	}
	public List <CheckListResult> getResultList(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed) {
		return checkListDao.getResultList(sessionCorpID, owner, duration,ruleId, messageDisplayed);
	}
	public void updateAndDel(String sessionCorpID,Long currentRuleNumber,Long ruleId){
		checkListDao.updateAndDel(sessionCorpID, currentRuleNumber, ruleId);
	}
	

	public List findNumberOfResultEntered(String sessionCorpID, Long id) {
		return checkListDao.findNumberOfResultEntered(sessionCorpID, id);
	}

	public String executeTestRule(CheckList checkList, String sessionCorpID) {
		String message = "";
		List<Object[]> checkListResults = null;
			try{
				checkListResults = executeRule(checkList, sessionCorpID);
			//System.out.println("\n\n ToDo Results-->>"+todoResults);
			}catch(Exception ex){
				
				message = ex.getCause().getMessage();
				//System.out.println("\n\n ToDo Results message-->>"+message);
				return message;
			}
			return null;
	}

	public Integer findNumberOfResult(String sessionCorpID) {
		return checkListDao.findNumberOfResult(sessionCorpID);
	}

	public List<ToDoRule> getAllRules(String sessionCorpID) {
		return checkListDao.getAllRules(sessionCorpID);
	}
	public List <CheckListResult> getResultListExtCordinator(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userType)
	{
		return checkListDao.getResultListExtCordinator(sessionCorpID,owner,duration,ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,userType);
	}
	public List <CheckListResult> getResultListExtCordinatorForTeam(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed,String bookingAgentPopup,String billToCodePopup,String userName,String allradio,String userType)
	{
		return checkListDao.getResultListExtCordinatorForTeam(sessionCorpID,owner,duration,ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,userName,allradio,userType);
	}

}
