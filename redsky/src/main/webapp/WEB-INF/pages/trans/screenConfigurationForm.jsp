<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<script type="text/javascript">
<!--

//-->
</script>

<script>
<!--
</script>
<head>   
    <title><fmt:message key="systemConfigurationForm.title"/></title>   
    <meta name="heading" content="<fmt:message key='systemConfigurationForm.heading'/>"/>   
</head>
<s:form id="screenConfigurationForm" name="screenConfigurationForm" action="screensave" method="post" validate="true">
<s:hidden name="screenConfiguration.id" value="%{screenConfiguration.id}"/>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:textfield name="tableName" value="%{systemConfiguration.tableName}"/>



<div id="newmnav">
  <ul>
  
</ul>
</div><div class="spn">&nbsp;</div><br>
		
	
  




<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr>
	<td height="10" align="left" class="listwhitetext">
		<table  class="detailTabLabel" border="0">
 	
 			<tr><td align="left" height="5px"></td></tr>
 			<tr>
				<td align="right" style="width:100px"><fmt:message key="screenConfiguration.corpID"/></td>
				<td align="left" colspan="2"><s:textfield name="screenConfiguration.corpID" onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" size="10" maxlength="15"/></td>
		    </tr>
			<tr>
				<td align="right" style="width:100px"><fmt:message key="systemConfiguration.tableName"/></td>
				<td align="left" colspan="2"><s:textfield name="systemConfiguration.tableName" onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" size="10" maxlength="15"/></td>
		    </tr>
		    <tr>	
		    	<td align="right"><fmt:message key="systemConfiguration.fieldName"/></td>
		    	<td align="left" colspan="5"><s:textfield name="screenConfiguration.fieldName"   required="true" cssClass="input-text" size="10" maxlength="15"/></td>
		   	</tr>
		    <tr>
				<td align="right" style="width:100px"><fmt:message key="screenConfiguration.displayStatus"/></td>
				<td align="left" colspan="2"><s:checkbox name="screenConfiguration.displayStatus" value="${displayStatus}" fieldValue="true" disabled="disabled"/></td>
		    </tr>
			<tr>
				<td></td>
				<td align="left">
				<s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save"/>
				<input type="button" class="cssbutton1" style="width:55px; height:25px" value="Add" style="width:87px; height:26px" onclick="location.href='<c:url value="/editscreen.html"/>'"/></td>
			</tr>
		</table>
    
       
       
       </tr></tbody></table>

</s:form>  

<script type="text/javascript">   
    highlightTableRows("systemConfigurationForm");   
</script>