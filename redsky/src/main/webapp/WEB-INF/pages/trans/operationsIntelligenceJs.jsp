<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/common/taglibs.jsp"%>

<script language="javascript" type="text/javascript">
function saveCmtForRevision(id,commentField){
	var cmtValue = document.getElementById('valueOfCommentForRevised').value;
	 var idCmt=commentField+id;
	$.ajax({
          type: "POST",
          url: "saveCommentForRevised.html?ajax=1&decorator=simple&popup=true",
          data: { id:id,shipNumber:'${serviceOrder.shipNumber}',cmtValue:encodeURIComponent(cmtValue),commentField,commentField },
          success: function (data, textStatus, jqXHR) {
        	  ajax_hideTooltip();
        	  cmtValue = cmtValue.trim();
        	  if(cmtValue != null && cmtValue !=""){
        	  document.getElementById(idCmt).src = 'images/comment-oi-red.png';
        	  }else{
        		  document.getElementById(idCmt).src = 'images/comment-oi.png';  
        	  }
          },
        error: function (data, textStatus, jqXHR) {
                }
        }
     ); 
    }
    
</script>

<script type="text/javascript">
 function closePopup() {
	 scopePopup.close();
 }
 /* function openScopePopup(id){
		var shipNumber = '${shipNumber}';
		var url = 'scopeOfWOForOI.html?decorator=popup&popup=true&id='+id+"&shipNumber="+shipNumber;
		scopePopup = window.open(url,'scopePopup','height=250,width=500,top=250,left=150, scrollbars=yes,resizable=yes'); 
	} */
</script>
<script type="text/javascript">
function removeZero(id)
{
	var quantityValue= document.getElementById("resourceQuantity"+id).value
	if(quantityValue=='0' || quantityValue=='0.0')
	{
		document.getElementById("resourceQuantity"+id).value='';
	}
}

</script>
<script type="text/javascript">
function openScopeForWO(id,workorder,ticket){
	/* window.open("scopeOfWOForOI.html?id="+id+"&decorator=popup&popup=true','hiiii','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes'); */
	var shipNumber = '${serviceOrder.shipNumber}';
	var url = 'scopeOfWOForOI.html?decorator=popup&popup=true&id='+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
	window.open(url,'ScopeForWO','height=350,width=800,top=250,left=150, scrollbars=yes,resizable=yes'); 
}
function openRevisionScopeForWO(id,workorder,ticket){
	var shipNumber = '${serviceOrder.shipNumber}';
	var url = 'revisionScopeOfWOForOI.html?decorator=popup&popup=true&id='+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
	window.open(url,'RevisionScopeForWO','height=350,width=800,top=250,left=90, scrollbars=yes,resizable=yes'); 
}

 /* function openCommentForRevised(id,workorder,ticket){
    var url = 'openCommentForRevised.html?decorator=popup&popup=true&id='+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
    window.open(url,'ScopeForWO','height=150,width=400,top=250,right=150, scrollbars=yes,resizable=yes'); 
} */ 

function reposition()
{
    var mycontentdiv = document.getElementById('ajax_tooltip_content'); 
    var myarrowdiv = document.getElementById('ajax_tooltip_arrow');    
    mycontentdiv.style.left='-230px';    
    myarrowdiv.style.left='-5px';    
    myarrowdiv.style.transform = 'rotate(180deg)';  
}

function repositionClose()
{
	try{
	var mycontentdiv = document.getElementById('ajax_tooltip_content'); 
    var myarrowdiv = document.getElementById('ajax_tooltip_arrow');    
    mycontentdiv.style.left='';    
    myarrowdiv.style.left='';    
    myarrowdiv.style.transform = ''; 
    /* delete mycontentdiv;
    delete myarrowdiv; */
	}catch(e){}
}

function openCommentForRevised(id,workorder,commentField,position){
	var shipNumber = '${serviceOrder.shipNumber}';
	 var url = 'openCommentForRevised.html?decorator=popup&popup=true&id='+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&commentField="+commentField;
	    ajax_showTooltip(url,position);
}

function showScopeForWO(id,workorder,position,ticket){
	/* window.open("scopeOfWOForOI.html?id="+id+"&decorator=popup&popup=true','hiiii','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes'); */
	var shipNumber = '${serviceOrder.shipNumber}';
	var url = "showScopeOfWOForOI.html?decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
	ajax_showTooltip(url,position);
	    var containerdiv =  document.getElementById("resourceListForOI").scrollLeft;
	  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	
}

function showRevisionScopeForWO(id,workorder,position,ticket){
	var shipNumber = '${serviceOrder.shipNumber}';
	var url = "showRevisionScopeOfWOForOI.html?decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&shipNumber="+shipNumber+"&ticket="+ticket;
	ajax_showTooltip(url,position);
	    var containerdiv =  document.getElementById("resourceListForOI").scrollLeft;
	  	var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
	    document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	
}

function onlyFloatAllowed(target,estmatedSalesTax){  
	 var letters = /[^0-9]/g;  
	var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
	if( (target.value.match(letters) != null) && (target.value.match(decimal) == null))
	 {
		if (target.value.split(".")[0].length != 0){
		 alert("Please Enter Valid Value")
		  document.getElementById(target.id).value = '';
		 return false;
		}
		 } 	
	
	var salesTaxForOI = document.getElementById(target.id).value;
	var salesTaxFieldName = document.getElementById(target.id).name;
	
	var shipNumber = '${serviceOrder.shipNumber}';
		 var url = "updateSalesTaxForOI.html?ajax=1&salesTaxForOI="+salesTaxForOI+"&shipNumber="+shipNumber+"&salesTaxFieldName="+salesTaxFieldName;
		 http21.open("GET", url, true); 
			  http21.onreadystatechange = handleHttpResponse21(salesTaxForOI);
			  http21.send(null);  
}

function handleHttpResponse21(value){
	if (http21.readyState == 4){		
            var results = http20.responseText
            results = results.trim();   
            document.getElementById(target.id).value = value;
            showOrHide(0);                      
	}
}
var http21 = getHTTPObject();

function validateWorkOrder(id,target){
	var workOrder=document.getElementById("workOrderOI"+id).value;
	if(workOrder!=null && workOrder!=''){
		workOrder = workOrder.substring(3);
		workOrder = workOrder.length;
	}
	if(workOrder=='1' || workOrder=='0'|| workOrder==''){
		alert("Please enter valid WO# ");
		document.getElementById("workOrderOI"+id).value = "WO_"
	}else{
		checkWorkOrderValidation(id,target)
	}
}

function checkAddWorkOrderValidation(){
	var checkVal='N';
	var found='F';
	var initialVal=0;
	var distinctWorkOrderList = document.getElementById('distinctWorkOrderListVal').value;
	var res = distinctWorkOrderList.split(",");
	for(i=0;i<res.length;i++){ 
	var workNo=res[i].trim();
	if(workNo.indexOf('WO_')>-1){
		var tempNo=parseInt(workNo.replace('WO_',''));
		if(tempNo>initialVal){
			initialVal=tempNo;
		}
	}
	if(workNo=="WO_"){
		checkVal = 'Y';
	}
	if((workNo=='' || workNo==null || workNo==undefined || checkVal == 'Y') && found!='T'){
		found='T';
	}
	}
	found=found+"~"+initialVal;
	return found;
}
function checkWorkOrderValidation(id,target){
	var value=target.value;
	var oneIncNumber=0;
	if(value.indexOf('WO_')>-1){
		oneIncNumber=parseInt(value.replace('WO_',''))
	}
	var found=false;
	var findMax=checkAddWorkOrderValidation();
	var distinctWorkOrderList = document.getElementById('distinctWorkOrderListVal').value;
	var res = distinctWorkOrderList.split(",");
	for(i=0;i<res.length;i++){ 
	var workNo=res[i].trim();	
		if(workNo==value && !found){
			found=true;
		}
	}
	if(found){
	}else{
		var maxN=parseInt(findMax.split("~")[1]);
		maxN++;
		if((value.indexOf('WO_')>-1) && findMax.split("~")[0]=='T' && maxN==oneIncNumber){
		}else{
			alert("Please enter valid WO#");
			document.getElementById("workOrderOI"+id).value='WO_';			
		}
	} 
}
function getId(oid){
	var oIdList = document.getElementById("oId").value;
	if(oIdList!=''){
		var check = oIdList.indexOf(oid);
		if(check > -1){
		}else{
			oIdList = oIdList+','+oid;
		}
	}else{
		oIdList = oid;
	}
	document.getElementById("oId").value = oIdList;
}
function saveOI(){
	var oIdList = document.getElementById("oId").value;
	
	var delimeter1 = "-@-";
	 var delimeter = "-,-";
	 var oiValue = '';
     var check = saveValidation();
     if(check){
     	var id = oIdList.split(",");
	     if(id!=''){
			for (i=0;i<id.length;i++){
				if(oiValue == ''){
					oiValue  =  getOIValue(id[i].trim());
		   		}else{
		   			oiValue  =  oiValue +delimeter1+ getOIValue(id[i].trim());
		   		}
			}
		}
    if(oiValue!=''){
    	showOrHide(1);
     $.ajax({
		  type: "POST",
		  url: "autoSaveOIAjax.html?ajax=1&decorator=simple&popup=true",
		  data: { sid: '${serviceOrder.id}', oiValue:oiValue, delimeter:delimeter, delimeter1:delimeter1},
		  success: function (data, textStatus, jqXHR) {
			  var oAndIDetailsDiv = document.getElementById("oAndIRows");
			  oAndIDetailsDiv.innerHTML = data.trim();
			  document.getElementById ('oId').value = "";
			  try{
				  var workTktNo = document.getElementById('workTicketNumberForValidation').value;
				  if(workTktNo!=null && workTktNo!=''){
					  var msg = workTktNo.trim().replace(/'/g, "");
					  alertMsg = "The hub limit for resource is exceeding the limit for ticket "+msg+". The Ticket will be sent in Dispatch Queue for Approval.";
				  	  alert(alertMsg)
				  	document.getElementById ('workTicketNumberForValidation').value = "";
				  }
			  }catch(e){}
			  showOrHide(0);
			  //calculateRevenueForOI();
			  document.getElementById('activeInactive').disabled=true; 
			  var field = 'allOid';
			  var url = window.location.href;
			  if(url.indexOf('&' + field + '=') != -1){
				  removeParam("allOid");
			  }
			     
			  location.reload(true);
		  },
	    error: function (data, textStatus, jqXHR) {
			    	showOrHide(0);
				}
		});
    
     }else{
    	 location.reload(true);
     }
	}
}
function saveValidation(){
	/* var checkValueForWo='N';
	var found=false;
	<c:forEach var="entry" items="${resourceListForOI}"> 
	var id="${entry.id}";
	var workNo=document.getElementById('workOrderOI'+id).value;
	if(workNo=="WO_"){
        checkValueForWo = 'Y';
    }
	if((workNo=='' || workNo==null || workNo==undefined || checkValueForWo=='Y') && !found){
		found=true;
	}
	</c:forEach> */
	var oIdList = document.getElementById("oId").value;
	var id = oIdList.split(",");
    if(id!=''){
		for (i=0;i<id.length;i++){
			var workNo=document.getElementById('workOrderOI'+id[i].trim()).value;
			if(workNo!=null && workNo!=''){
				workNo = workNo.substring(3);
				workNo = workNo.length;
			}
			if(workNo=='1' || workNo=='0'|| workNo=='' || workNo==null || workNo==undefined){
				alert("Please enter valid WO#");
				document.getElementById('workOrderOI'+id[i].trim()).value = "WO_"
				return false;
			}
		}
		return true;
	}else{
		return true;	
	}
}
function getOIValue(id){
	var delimeter = "-,-";
	  var values = id;
	  values = values + delimeter + validateFieldValueString('workOrderOI'+id); //01
	  values = values + delimeter + validateFieldValueForDate('date'+id);//02
	  values = values + delimeter + validateFieldValueString('type'+id);//03
	  values = values + delimeter + validateFieldValueString('resourcedescription'+id);//04
	  values = values + delimeter + validateFieldValueForDouble('resourceQuantity'+id);//05
	  values = values + delimeter + validateFieldValueForDouble('resourceEstHour'+id);//06
	  values = values + delimeter + validateFieldValueString('resourceComments'+id);//07
	  values = values + delimeter + validateFieldValueForDouble('estimateQuantity'+id);//08
	  values = values + delimeter + validateFieldValueForDouble('estimateBuyRate'+id);//09
	  values = values + delimeter + validateFieldValueForDouble('estimateExpense'+id);//10
	  values = values + delimeter + validateFieldValueForDouble('estimateSellRate'+id);//11
	  values = values + delimeter + validateFieldValueForDouble('estimateRevenue'+id);//12
	  values = values + delimeter + validateFieldValueForDouble('revisionQuantity'+id);//13
	  values = values + delimeter + validateFieldValueForDouble('revisionbuyrate'+id);//14
	  values = values + delimeter + validateFieldValueForDouble('revisionexpense'+id);//15
	  values = values + delimeter + validateFieldValueForDouble('revisionsellrate'+id);//16
	  values = values + delimeter + validateFieldValueForDouble('revisionrevenue'+id);//17
	  values = values + delimeter + validateFieldValueForDouble('ticket'+id);//18
	  values = values + delimeter + validateFieldValueForDouble('revisedQuantity'+id);//19
	  return values;
}
function validateFieldValueForDate(fieldId){
	  var result1 =null;
	  try {
		  var result = document.getElementById(fieldId);
		  if(result == null || result ==  undefined  || result.value ==  ''){
				 return result1;
			 }else{
				 return convertItProperDateFormate1(result.value);
			 }
	  }catch(e) {
		  return result1;
	  }
}
function validateFieldValueForDouble(fieldId){
	  var result1 ="0.00";
	  try {
		  var result = document.getElementById(fieldId);
		  if(result == null || result ==  undefined  || result.value ==  ''){
				 return result1;
			 }else{
				 return result.value;
			 }
	  }catch(e) {
		  return result1;
	  }
}
function validateFieldValueForNumber(fieldId){
 var result1 ="0";
	  try {
		  var result = document.getElementById(fieldId);
			 if(result == null || result ==  undefined  || result.value ==  ''){
				 return result1;
			 }else{		
				 return (result.value.indexOf(".")>-1?result.value.substring(0,result.value.indexOf(".")):result.value);
			 }
	  }catch(e) {
		  return result1;
	  }
}
function validateFieldValueString(fieldId){
	  var result1 =" ";
	
	  try {
		  var result = document.getElementById(fieldId);
		  if(result == null || result ==  undefined  || result.value ==  ''){
				 return result1;
			 }else{
				 return result.value;
			 }
	  }catch(e) {
		  return result1;
	  }
}
function convertItProperDateFormate1(target){ 
	var calArr=target.split("-");
	var year=calArr[2];
	year="20"+year;
	var month=calArr[1];
	  	   if(month == 'Jan') { month = "01"; }  
	  else if(month == 'Feb') { month = "02"; } 
	  else if(month == 'Mar') { month = "03"; } 
	  else if(month == 'Apr') { month = "04"; } 
	  else if(month == 'May') { month = "05"; } 
	  else if(month == 'Jun') { month = "06"; }  
	  else if(month == 'Jul') { month = "07"; } 
	  else if(month == 'Aug') { month = "08"; }  
	  else if(month == 'Sep') { month = "09"; }  
	  else if(month == 'Oct') { month = "10"; }  
	  else if(month == 'Nov') { month = "11"; }  
	  else if(month == 'Dec') { month = "12"; }	
	var day=calArr[0];
	var date=year+"-"+month+"-"+day;
	return date;
}
</script>
<script type="text/javascript">
//calculateRevenueForOI(); 
function calculateRevenueForOI()
	{
	var estGrossMarginPercentage=0.00;;
	var totalEstExpanse=0.00;
	var totalEstRevenue=0.00
	var totalRevExpanse=0.00;
	var totalRevRevenue=0.00;
	var revGrossMarginPercentage=0.00;;
	var estmatedSalesTax = 0.00;
	var checkWorkOrder="";
	var estRevenue = 0.00;
	var estExpanse = 0.00;
	var revExpanse = 0.00;
	var revRevenue = 0.00;
	//var estmatedConsumables = 0.00;
	<c:forEach var="entry" items="${resourceListForOI}"> 
	var id="${entry.id}";
	if('${entry.quantitiy}'!='0.00'){
		estRevenue = document.getElementById('estimateRevenue'+id).value;
		estExpanse = document.getElementById('estimateExpense'+id).value;
		totalEstExpanse=totalEstExpanse +parseFloat(estExpanse);
		totalEstRevenue=totalEstRevenue+parseFloat(estRevenue);
		revExpanse =  document.getElementById('revisionexpense'+id).value
		revRevenue = 	document.getElementById('revisionrevenue'+id).value
		totalRevExpanse=totalRevExpanse+parseFloat(revExpanse);
		totalRevRevenue=totalRevRevenue+parseFloat(revRevenue);
		if('${entry.estmatedSalesTax}'!='0.00'){
		document.getElementById('estmatedSalesTax').value="${entry.estmatedSalesTax}";
		}
		/* if(${entry.estmatedConsumables}!='0.00'){
		document.getElementById('estmatedConsumables').value="${entry.estmatedConsumables}";
		} */
		
		if(checkWorkOrder != "${entry.workorder}"){
			var entrySalesTax = "${entry.salesTax}";
			if(entrySalesTax != null && entrySalesTax !=''){
				   estmatedSalesTax = estmatedSalesTax+parseFloat("${entry.salesTax}");
			}
					//estmatedConsumables = estmatedConsumables + parseFloat("${entry.consumables}");
					checkWorkOrder = "${entry.workorder}";
		}
	}
	</c:forEach>
	//document.getElementById("estmatedSalesTax").value = (Math.round(estmatedSalesTax * 100) / 100).toLocaleString();
	//document.getElementById("estmatedConsumables").value = (Math.round(estmatedConsumables * 100) / 100).toLocaleString();
	
    var calEstimatedTotalExpense = (Math.round(totalEstExpanse * 100) / 100).toLocaleString();
    var calEstimatedTotalRevenue = (Math.round(totalEstRevenue * 100) / 100).toLocaleString();
 	document.getElementById("estimatedTotalExpense").value=calEstimatedTotalExpense;
	document.getElementById("estimatedTotalRevenue").value=calEstimatedTotalRevenue;
	estimatedGrossMargin=totalEstRevenue-totalEstExpanse;
	var calEstGrossMarginForRevenue = (Math.round(estimatedGrossMargin * 100) / 100).toLocaleString();
	document.getElementById("estGrossMarginForRevenue").value=calEstGrossMarginForRevenue;
	if((totalEstRevenue==0)||(totalEstRevenue==0.0)||(totalEstRevenue==0.00)){
		estGrossMarginPercentage=0.00;
	}else{
		estGrossMarginPercentage=(estimatedGrossMargin*100)/totalEstRevenue;
	}
	var calEstGrossMarginForRevenuePercentage  = Math.round((Math.round(estGrossMarginPercentage * 100) / 100)).toLocaleString();
	document.getElementById("estGrossMarginForRevenuePercentage").value=calEstGrossMarginForRevenuePercentage;
	
	if(isNaN(totalRevExpanse))
		{
		totalRevExpanse=0;
		}
	if(isNaN(totalRevRevenue))
	{
		totalRevRevenue=0;
	}
	var calRevTotalExpense = (Math.round(totalRevExpanse * 100) / 100).toLocaleString();
	var calRevTotalRevenue = (Math.round(totalRevRevenue * 100) / 100).toLocaleString();
document.getElementById("revTotalExpense").value=calRevTotalExpense;
document.getElementById("revTotalRevenue").value=calRevTotalRevenue;
revisionGrossMargin=totalRevRevenue-totalRevExpanse;

var calRevGrossMarginForRevenue = (Math.round(revisionGrossMargin * 100) / 100).toLocaleString();
document.getElementById("revGrossMarginForRevenue").value=calRevGrossMarginForRevenue;
if((totalRevRevenue==0)||(totalRevRevenue==0.0)||(totalRevRevenue==0.00)){
revGrossMarginPercentage=0.00;
}else{
revGrossMarginPercentage=(revisionGrossMargin*100)/totalRevRevenue;
}
var calRevGrossMarginForRevenuePercentage = Math.round((Math.round(revGrossMarginPercentage * 100) / 100)).toLocaleString();
document.getElementById("revGrossMarginForRevenuePercentage").value=calRevGrossMarginForRevenuePercentage;
}
</script>
<script type="text/javascript">
function sellReteAndBuyRateByDescription(id,shipNumber){
	var type=validateFieldValueString('type'+id)
	var description=validateFieldValueString('resourcedescription'+id)
	var resourceDescription;
	/* var shipNumber='${serviceOrder.shipNumber}'; */
    showOrHide(0);
	<c:forEach var="entry" items="${resourceListForOI}">
	 var checkId="${entry.id}";
	 if(checkId==id){
	 var resourceDescription = "${entry.description}";
	 }
	 </c:forEach>
	 if(resourceDescription != description)
	 {
	 var url = "sellReteAndBuyRateByDescriptionAjax.html?ajax=1&decorator=simple&popup=true&id="+id+"&type="+type+"&description="+encodeURIComponent(description)+"&shipNumber="+shipNumber;
	 http203.open("GET", url, true); 
	 http203.onreadystatechange = function() {handleHttpResponse203(id)};
	 http203.send(null);
	 }
}
function handleHttpResponse203(id){
	if (http203.readyState == 4){		
            var results = http203.responseText
            results = results.trim(); 
            if(results.length>0)
            	{
            document.getElementById('resourceQuantity'+id).value=results.split("~")[0];            
            document.getElementById('estimateBuyRate'+id).value=results.split("~")[2];
            document.getElementById('estimateSellRate'+id).value=results.split("~")[1];
            document.getElementById('resourceEstHour'+id).value=1.0;
            document.getElementById('revisionbuyrate'+id).value=results.split("~")[2];
            document.getElementById('revisionsellrate'+id).value=results.split("~")[1];
            fillEstimateQuantity(results.split("~")[0],id);
          
            document.getElementById('revisionexpense'+id).value= (results.split("~")[2]) *  (document.getElementById('revisionQuantity'+id).value);
            document.getElementById('revisionrevenue'+id).value = (results.split("~")[1]) *  (document.getElementById('revisionQuantity'+id).value)
            //fillEstimateQuantityByHours('1.0',id)
           // fillExpenseByRate(results.split("~")[1]);
           // fillRevnueBySellRate(results.split("~")[2])
            showOrHide(0);                      
	}}
}
var http203 = getHTTPObject3();
function getHTTPObject3() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
function applyImage(){
	 var workorderFlag="";
	<c:forEach var="entry" items="${resourceListForOI}">
	 var id="scope${entry.id}";
	 var id1="revisionScope${entry.id}";
	 if(workorderFlag=="" || workorderFlag != '${entry.workorder}') {
		 workorderFlag="${entry.workorder}";
		
	  if(${entry.scopeOfWorkOrder != null && entry.scopeOfWorkOrder != ''}){
	     	document.getElementById(id).src = 'images/externalScope.png';
	     	document.getElementById(id1).src = 'images/externalScope.png';
		}else{
			 document.getElementById(id).src = 'images/externalScope.png';
			 document.getElementById(id1).src = 'images/externalScope.png';
		 } }
</c:forEach> 
}
</script>


<script type="text/javascript">
var http205 = getHTTPObject();
function validateWorkOrderByTicket(id,workorder1,shipNumber,sessionCorpID)
{
	showOrHide(0); 
	var workorder = document.getElementById('workOrderOI'+id).value;
	 var url = "validateWorkOrderByTicket.html?ajax=1&decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&sessionCorpID="+sessionCorpID+"&shipNumber="+shipNumber;
	 http205.open("GET", url, true); 
	 http205.onreadystatechange = function() {handleHttpResponse205()};
	 http205.send(null);	
}
function handleHttpResponse205(){
	if (http205.readyState == 4){		
            var results = http205.responseText
           results = results.trim(); 
            if(results.value=='' || results.value==null || results.value==undefined)
            	{
            	/* document.getElementById('workOrderOI'+id).value = workorder; */
            	}else{
            		alert("Please Enter Valid WO#");
                	document.getElementById('workOrderOI'+id).value = "";
            	}
            	}
            	}
</script>
<script type="text/javascript">
var http204 = getHTTPObject();
var http201 = getHTTPObject();
var http207 = getHTTPObject();

function preventWorkOrder(target,id,sessionCorpID,shipNumber,fieldName,ticket,workorder)
{
	if(ticket != null && ticket != '')
		{
		alert(" Work ticket has already been created, We cannot edit the WorkOrder.")
		document.getElementById('workOrderOI'+id).value = workorder;	
		return false;
		}
	var workorder =  document.getElementById('workOrderOI'+id).value;
	var resource =  document.getElementById('type'+id).value;
	if(fieldName == 'type')
		{
	if(((workorder !=null && workorder != '')) && resource !='T')
		{
	var url = "statusOfWorkTicketByWO.html?ajax=1&decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&sessionCorpID="+sessionCorpID+"&shipNumber="+shipNumber;
	http201.open("GET", url, true); 
	 http201.onreadystatechange = function() {handleHttpResponse201(target.id,id,target.value,shipNumber)};
	 http201.send(null);
		}}
	if(fieldName == 'workorder')
		{
		if(((workorder !=null && workorder != '')) && (resource =='V' ||resource == 'C' || resource == 'M' ||resource == 'E' ))
		{
	var url = "statusOfWorkTicketByWO.html?ajax=1&decorator=simple&popup=true&id="+id+"&workorder="+workorder+"&sessionCorpID="+sessionCorpID+"&shipNumber="+shipNumber;
	http201.open("GET", url, true); 
	 http201.onreadystatechange = function() {handleHttpResponse201(target.id,id,target.value,shipNumber)};
	 http201.send(null);
		}
		}
}

function handleHttpResponse201(id,id1,value,shipNumber){
	if (http201.readyState == 4){		
            var results = http201.responseText
            results = results.trim(); 
          if(results == 'T' || results == 'P' || results=="")
        	  {
        	  }else{
        var msgForWO = document.getElementById('workOrderOI'+id1).value
        		  if(results == 'A'){results="Actual";}
      			if(results == 'C'){results="Cancelled";}
      			if(results == 'F'){results="Forecasting";}
      			if(results == 'R'){results="Rejected";}
      			if(results == 'D'){results="Dispatching";}
      			alert("For "+msgForWO+", Work ticket in " +results+" Status, We cannot add/edit the resources, Please contact Operations. ")
      			document.getElementById(id).value="";
        	  }
          
          
          
 }
}

function chaekValidationForQty(target,value){
	try{
        estQty=target.value;
        var letters = /[^0-9]/g;  
        var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
         if( (target.value.match(letters) != null) && (target.value.match(decimal) == null))
         {
        	 if (target.value.split(".")[0].length != 0){
            alert("Please Enter Valid Value")
            if(value != undefined){
            document.getElementById(target.id).value=value;
           }else{
        	   document.getElementById(target.id).value=0;  
           }
             return false;
             }  
         }       
    }catch(e){
    }
}

function validateWorkTicketStatus(id,value,target,ticket,sessionCorpID,shipNumber,checkFlag)
{
	var estQty = 0;
 	try{
		estQty=target.value;
		var letters = /[^0-9]/g;  
		var decimal=  /^[+]?[0-9]+\.[0-9]+$/;   
		 if( (target.value.match(letters) != null) && (target.value.match(decimal) == null) && checkFlag)
		 {
			if (target.value.split(".")[0].length != 0){
			 alert("Please Enter Valid Value")
			 target.value = value;
			 return false;
			 }		}
	}catch(e){
		estQty=parseInt(target);
	} 
	
	if(ticket != null && ticket != "")
		{
	showOrHide(0); 
	 var url = "statusOfWorkTicket.html?ajax=1&decorator=simple&popup=true&id="+id+"&ticket="+ticket+"&sessionCorpID="+sessionCorpID;
	 http204.open("GET", url, true); 
	 http204.onreadystatechange = function() {handleHttpResponse204(target.id,id,value,shipNumber,ticket,target,checkFlag)};
	 http204.send(null);
		}else{
			if(checkFlag){
			var estQty =document.getElementById('resourceQuantity'+id).value ;
			var estHour1 = "resourceEstHour";
    		var estimateQty1 = "estimateQuantity"; 
    		var estimateQtyId = estimateQty1.concat(id);
    		var estHourId = estHour1.concat(id);
    		var estHour = document.getElementById(estHourId).value;
    		var estimateQuantityNew = "0.00";
    		estimateQuantityNew = estHour * estQty;
    		estimateQuantityNew=Math.round(estimateQuantityNew*100)/100;
    		estimateQuantityNew = Number(estimateQuantityNew);
    		if (String(estimateQuantityNew).split(".").length < 2 || String(estimateQuantityNew).split(".")[1].length<=2 ){
    			estimateQuantityNew = estimateQuantityNew.toFixed(2);
    		}
    		
    		document.getElementById(estimateQtyId).value = estimateQuantityNew;
    		
    		var estimateExpenseId = "estimateExpense";
    		var estimateRevenueId = "estimateRevenue";
    		var estimateExpenseId1 = estimateExpenseId.concat(id);
    		var estimateRevenueId1 = estimateRevenueId.concat(id);
    		
    		var estimateBuyRateId = 'estimateBuyRate';
    		var estimateBuyRateId1 = estimateBuyRateId.concat(id);
    		var estimateSellRateId = 'estimateSellRate';
    		var estimateSellRateId1 = estimateSellRateId.concat(id);
    		
    		var estimateExpense = "0.00";
    		estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
    		estimateExpense=Math.round(estimateExpense*100)/100;
   			estimateExpense = Number(estimateExpense);
    		if (String(estimateExpense).split(".").length < 2 || String(estimateExpense).split(".")[1].length<=2 ){
    			estimateExpense = estimateExpense.toFixed(2);
    		}
    		document.getElementById(estimateExpenseId1).value =  estimateExpense;
    		
    		var estimateRevenue = "0.00"; 
    		estimateRevenue = (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
    		estimateRevenue=Math.round(estimateRevenue*100)/100;
   			estimateRevenue = Number(estimateRevenue);
    		if (String(estimateRevenue).split(".").length < 2 || String(estimateRevenue).split(".")[1].length<=2 ){
    			estimateRevenue = estimateRevenue.toFixed(2);
    		}
    		document.getElementById(estimateRevenueId1).value =  estimateRevenue;
    		
    		if(estQty==''){
    			estQty = "0.00";
    		}else{
    			estQty = Number(estQty);
	    		if (String(estQty).split(".").length < 2 || String(estQty).split(".")[1].length<=2 ){
	    			estQty = estQty.toFixed(2);
	    		}
    		}
    		document.getElementById('resourceQuantity'+id).value=estQty;
    		if(estHour == null || estHour == '' ){
    			estHour=0.0;
    		}
			}
		}
}
function handleHttpResponse204(id,id1,value,shipNumber,ticket,target,checkFlag){
	if (http204.readyState == 4){		
            var results = http204.responseText
            results = results.trim(); 
            if(results.length>0)
            	{
            	if(results == 'T' || results == 'P' || results=="")
            		{
            		if(checkFlag){
            		var estQty =document.getElementById('resourceQuantity'+id1).value ;
            		var estHour1 = "resourceEstHour";
            		var estimateQty1 = "estimateQuantity"; 
            		var estimateQtyId = estimateQty1.concat(id1);
            		var estHourId = estHour1.concat(id1);
            		var estHour = document.getElementById(estHourId).value;
            		var estimateQuantityNew = "0.00";
            		estimateQuantityNew = estHour * estQty;
            		estimateQuantityNew=Math.round(estimateQuantityNew*100)/100;
            		estimateQuantityNew = Number(estimateQuantityNew);
            		if (String(estimateQuantityNew).split(".").length < 2 || String(estimateQuantityNew).split(".")[1].length<=2 ){
            			estimateQuantityNew = estimateQuantityNew.toFixed(2);
            		}
            		document.getElementById(estimateQtyId).value = estimateQuantityNew;
            		var estimateExpenseId = "estimateExpense";
            		var estimateRevenueId = "estimateRevenue";
            		var estimateExpenseId1 = estimateExpenseId.concat(id1);
            		var estimateRevenueId1 = estimateRevenueId.concat(id1);
            		
            		var estimateBuyRateId = 'estimateBuyRate';
            		var estimateBuyRateId1 = estimateBuyRateId.concat(id1);
            		var estimateSellRateId = 'estimateSellRate';
            		var estimateSellRateId1 = estimateSellRateId.concat(id1);
            		
            		//var estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
            		//var estimateRevenue =  (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
            		
            		var estimateExpense = "0.00";
            		estimateExpense = (estHour * estQty) * (document.getElementById(estimateBuyRateId1).value);
            		estimateExpense=Math.round(estimateExpense*100)/100;
           			estimateExpense = Number(estimateExpense);
            		if (String(estimateExpense).split(".").length < 2 || String(estimateExpense).split(".")[1].length<=2 ){
            			estimateExpense = estimateExpense.toFixed(2);
            		}
            		document.getElementById(estimateExpenseId1).value =  estimateExpense;
            		
            		var estimateRevenue = "0.00"; 
            		estimateRevenue = (estHour * estQty) * (document.getElementById(estimateSellRateId1).value);
            		estimateRevenue=Math.round(estimateRevenue*100)/100;
           			estimateRevenue = Number(estimateRevenue);
            		if (String(estimateRevenue).split(".").length < 2 || String(estimateRevenue).split(".")[1].length<=2 ){
            			estimateRevenue = estimateRevenue.toFixed(2);
            		}
            		document.getElementById(estimateRevenueId1).value =  estimateRevenue;
            		if(estHour == null || estHour == '' )
            			{
            			estHour=0.0;
            			}
            		}
            		/* if(target == "")
        			{
            		deleteDataFromId(id1,shipNumber,value,ticket,sessionCorpID);
        			} */
            		if(document.getElementById(id).value == 'T' || value == 'T')
            			{
            			//document.getElementById(id).value=value;
            			alert("Third Party resource can't  be Changed to other resource.Vice Varsa is also not Allowed ");
            			reloadDivById(id1);
            			return false;
            			}
            		}else{
            			validateWorkTicketStatusForResources(id1,ticket,sessionCorpID,shipNumber);
            			document.getElementById('resourcedescriptionTemp'+id1).value=1;
            			if(results == 'A'){results="Actual";}
            			if(results == 'C'){results="Cancelled";}
            			if(results == 'F'){results="Forecasting";}
            			if(results == 'R'){results="Rejected";}
            			if(results == 'D'){results="Dispatching";}
            			if(target == "")
            			{
            				alert("Work ticket in " +results+" Status, cannot delete the resources. Please contact Operations");	
            			}else{
            				
            			alert("Work ticket in " +results+" Status, cannot edit the resources. Please contact Operations");
            			document.getElementById(id).readOnly = true;
            			document.getElementById(id).value=value;
            			if(id =='type'+id1)
            				{
            				reloadDivById(id1);
            				}
            			return false;
            		}
            	}
            	}
}}

 function reloadDivById(id)
{
	<c:forEach var="entry" items="${resourceListForOI}">
	 var checkId="${entry.id}";
	 if(checkId==id)
		 {
		 document.getElementById('workOrderOI'+id).value="${entry.workorder}";
		 document.getElementById('type'+id).value="${entry.type}";
		 document.getElementById('resourcedescription'+id).value="${entry.description}";
		 document.getElementById('resourceQuantity'+id).value="${entry.quantitiy}";
		 document.getElementById('resourceEstHour'+id).value="${entry.esthours}";
		 /* document.getElementById('resourceComments'+id).value="${entry.comments}"; */
		 document.getElementById('estimateQuantity'+id).value="${entry.estimatedquantity}";
		 document.getElementById('estimateBuyRate'+id).value="${entry.estimatedbuyrate}";
		 document.getElementById('estimateExpense'+id).value="${entry.estimatedexpense}";
		 document.getElementById('estimateSellRate'+id).value="${entry.estimatedsellrate}";
		 document.getElementById('estimateRevenue'+id).value="${entry.estimatedrevenue}";
		 document.getElementById('revisionQuantity'+id).value="${entry.revisionquantity}";
		 document.getElementById('revisionbuyrate'+id).value="${entry.revisionbuyrate}";
		 document.getElementById('revisionexpense'+id).value="${entry.revisionexpense}";
		 document.getElementById('revisionsellrate'+id).value="${entry.revisionsellrate}";
		 document.getElementById('revisionrevenue'+id).value="${entry.revisionrevenue}";
		 }
	 </c:forEach>
}
function validateWorkTicketStatusForResources(id,ticket,sessionCorpID,shipNumber)
{
	showOrHide(0); 
	 var url = "statusOfWorkTicket.html?ajax=1&decorator=simple&popup=true&id="+id+"&ticket="+ticket+"&sessionCorpID="+sessionCorpID;
	 http207.open("GET", url, true); 
	 http207.onreadystatechange = function() {handleHttpResponse207(id,ticket)};
	 http207.send(null);
}
function handleHttpResponse207(id,ticket){
	if (http207.readyState == 4){		
            var results = http204.responseText
            results = results.trim(); 
            if(results.length>0)
            	{
            	if(results == 'T' || results == 'P' || results=="")
            		{
            		}else{
            			<c:forEach var="entry" items="${resourceListForOI}">
            			 var id="${entry.id}";
            			 var CheckTicket="${entry.ticket}";
            			if(CheckTicket == ticket)
            				{
            				document.getElementById('resourcedescriptionTemp'+id).value=1;
            				}
            			 </c:forEach>
            	}
            	}
}}
var currentClickId;
function setFieldId(id){
	currentClickId = id;
}
function checkLimitFoTicket(){
	if(currentClickId!=undefined && currentClickId!=null && currentClickId!=''){
		var id=currentClickId;
		 var workTicket = document.getElementById ('ticket'+id).value;
		 var date = document.getElementById ('date'+id).value;
		 var wo = document.getElementById ('workOrderOI'+id).value;
		 checkLimitOfWorkTicket(id,workTicket,date,wo);
	}	
}

function checkLimitOfWorkTicket(id,workTicket,date,wo){
	var updateValue = false;
 	var dataForCheckLimit = ""+','+""+","+date+","+wo+","+workTicket;
 	$.ajax({
	  type: "POST",
	  url: "checkLimitForOI.html?ajax=1&decorator=simple&popup=true",
	  data: { id: id,shipNumber:'${serviceOrder.shipNumber}',dataForCheckLimit:dataForCheckLimit},
	  success: function (data, textStatus, jqXHR) {
		     data = data.trim();
		     if(data=='NoDateChange'){
		    	alert('Work Ticket is not in Target Status,cannot edit the date.')
		    	  showOrHide(1);
		    	window.location.reload();
		     }else{
		     if(data=='thresholdMsg'){
		    	 alert("Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit.")
		    	 updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
		    	 }else if(data.length>0){
            		var checkForMsg =data.split(",")[0];
		            	 if(data.trim() == "checkMinimumServiceDays" ){
		            		 updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
		            		 }else if(checkForMsg.trim() == "exceedLimit" ){
				            	if(parseInt(data.split(",")[1].trim()) < 0){
				            		alert("The hub limit for resource is exceeding the limit for "+workTicket+". Ticket for "+workTicket+" will be sent in Dispatch Queue for Approval.")
				            		updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
				    			 }else if(parseInt(data.split(",")[1].trim()) == 0){
				    				 updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
				    			 }else{
				    			 }	
		            		}else if(parseInt(data) <= 0){
		            			alert("Ticket for "+workTicket+" is exceeding the limit. Ticket will be sent in Dispatch Queue for Approval.")
		            			updateDateByTicket(workTicket,document.getElementById ('date'+id).value,true);
					 	}else{
					 }
          		}else if(data.length==0){
          			updateDateByTicket(workTicket,document.getElementById ('date'+id).value,false);
          		}
		     }
	  	},
  		error: function (data, textStatus, jqXHR) {
		}
	}); 
}

function updateDateByTicket(workTicket,date,updateValue){
	 new Ajax.Request('/redsky/updateDateByTicketAjax.html?ajax=1&ticket='+workTicket+'&date1='+date+'&updateValue='+updateValue+'&decorator=simple&popup=true',
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      window.location.reload();
			    },
			    onFailure: function(){ 
				    }
			  });
}
</script>
<script>
function checkMsg(target)
{
	document.getElementById(target.id).style.height = "17px";

}
function checkMsg1(target)
{
	document.getElementById(target.id).style.height = "45px";
	document.getElementById(target.id).style.width = "200px;";

}

function setValuesForActiveId(rowId,targetElement,itemsJbkEquipId,workTicket,workOrder){
	var oiIdVal = document.getElementById("oiActiveId").value;
	if(itemsJbkEquipId==null || itemsJbkEquipId==''){
		itemsJbkEquipId = 'NA';
	}
	if(workTicket==null || workTicket==''){
		workTicket = 'NA';
	}
	var allValues = rowId+"#"+itemsJbkEquipId+"#"+workTicket+"#"+workOrder;
	if(targetElement.checked){
		if(oiIdVal == '' ){
			  document.getElementById("oiActiveId").value = allValues;
		}else{
			if(oiIdVal.indexOf(allValues)>=0){
			}else{
				oiIdVal = oiIdVal + "~" +allValues;
			}
			document.getElementById("oiActiveId").value = oiIdVal.replace('~ ~',"~");
		}
   	}else{
		if(oiIdVal == ''){
			document.getElementById("oiActiveId").value = allValues;
	     }else{
	    	 var check = oiIdVal.indexOf(allValues);
	 		if(check > -1){
	 			var values = oiIdVal.split("~");
	 		   for(var i = 0 ; i < values.length ; i++) {
	 		      if(values[i]== allValues) {
	 		    	values.splice(i, 1);
	 		    	oiIdVal = values.join("~");
	 		    	document.getElementById("oiActiveId").value = oiIdVal;
	 		      }
	 		   }
	 		}else{
	 			oiIdVal = oiIdVal + "~" +allValues;
	 			document.getElementById("oiActiveId").value = oiIdVal.replace('~ ~',"~");
	 		}
	     }
   	}
	if(document.getElementById("oiActiveId").value=='' && document.getElementById("oiInactiveId").value==''){
		document.getElementById('activeInactive').disabled=true;
	}else{
		document.getElementById('activeInactive').disabled=false;
	}
}

function setValuesForInactiveId(rowId,targetElement,itemsJbkEquipId,workTicket,workOrder){
	var oiIdVal = document.getElementById("oiInactiveId").value;
	if(itemsJbkEquipId==null || itemsJbkEquipId==''){
		itemsJbkEquipId = 'NA';
	}
	if(workTicket==null || workTicket==''){
		workTicket = 'NA';
	}
	var allValues = rowId+"#"+itemsJbkEquipId+"#"+workTicket+"#"+workOrder;
	if(targetElement.checked){
		if(oiIdVal == ''){
			document.getElementById("oiInactiveId").value = allValues;
	     }else{
	    	 var check = oiIdVal.indexOf(allValues);
	 		if(check > -1){
	 			var values = oiIdVal.split("~");
	 		   for(var i = 0 ; i < values.length ; i++) {
	 		      if(values[i]== allValues) {
	 		    	values.splice(i, 1);
	 		    	oiIdVal = values.join("~");
	 		    	document.getElementById("oiInactiveId").value = oiIdVal;
	 		      }
	 		   }
	 		}else{
	 			oiIdVal = oiIdVal + "~" +allValues;
	 			document.getElementById("oiInactiveId").value = oiIdVal.replace('~ ~',"~");
	 		}
	     }
   }else{
		if(oiIdVal == '' ){
			  document.getElementById("oiInactiveId").value = allValues;
		}else{
			if(oiIdVal.indexOf(allValues)>=0){
			}else{
				oiIdVal = oiIdVal + "~" +allValues;
			}
			document.getElementById("oiInactiveId").value = oiIdVal.replace('~ ~',"~");
		}
   }
	if(document.getElementById("oiActiveId").value=='' && document.getElementById("oiInactiveId").value==''){
		document.getElementById('activeInactive').disabled=true;
	}else{
		document.getElementById('activeInactive').disabled=false;
	}
}
if(document.getElementById("oiActiveId").value=='' && document.getElementById("oiInactiveId").value==''){
	document.getElementById('activeInactive').disabled=true;
}

function checkActiveInactive(){
	progressBarAutoSave('1');
	var oiActiveId = document.getElementById("oiActiveId").value;
	var oiInactiveId = document.getElementById("oiInactiveId").value;
	var shipNumber = '${serviceOrder.shipNumber}';
   	new Ajax.Request('/redsky/updateStatusResourceListForOI.html?ajax=1&decorator=simple&popup=true&oiActiveId='+encodeURIComponent(oiActiveId)+'&oiInactiveId='+encodeURIComponent(oiInactiveId)+'&shipNumber='+shipNumber,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			      window.location.reload(true);
			      progressBarAutoSave('0');
			    },
			    onFailure: function(){ 
				    }
			  });
}

var allOiDVal = "<%= (String)request.getAttribute("allOid") %>";
if(allOiDVal!=null && allOiDVal!='null' && allOiDVal!=''){
	document.getElementById ('oId').value = allOiDVal;
}

function removeParam(parameter){
  var url=document.location.href;
  var urlparts= url.split('?');

 if (urlparts.length>=2)
 {
  var urlBase=urlparts.shift(); 
  var queryString=urlparts.join("?"); 

  var prefix = encodeURIComponent(parameter)+'=';
  var pars = queryString.split(/[&;]/g);
  for (var i= pars.length; i-->0;)               
      if (pars[i].lastIndexOf(prefix, 0)!==-1)   
          pars.splice(i, 1);
  url = urlBase+'?'+pars.join('&');
  window.history.pushState('',document.title,url);

}
//return url;
}
</script>