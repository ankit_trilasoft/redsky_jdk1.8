package com.trilasoft.app.service;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager; 
import com.trilasoft.app.model.RefZipGeoCodeMap;
public interface RefZipGeoCodeMapManager extends GenericManager<RefZipGeoCodeMap,Long>
{
	  
    public List zipCodeExisted(String zipCode, String corpId);
    public List refZipGeoCodeMapByCorpId(String corpId);
    public List findMaximumId();
    public Map<String, String> findByParameter(String bucket2, String corpId);
    public List refZipGeoCodeMapState(String stateCode);
    public List searchByRefZipGeoCodeMap(String zipCode,String city,String state,String country);
    public int updatePrimaryRecord(String zipCode, String corpId);
	public String getStateName(String stateabbreviation, String sessionCorpID, String country); 
	
}
