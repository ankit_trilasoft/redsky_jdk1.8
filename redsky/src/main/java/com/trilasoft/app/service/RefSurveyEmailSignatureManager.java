package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RefSurveyEmailSignature;

public interface RefSurveyEmailSignatureManager extends GenericManager<RefSurveyEmailSignature, Long>{

}
