package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.PartnerRefCharges;
import com.trilasoft.app.service.PartnerRefChargesManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerRefChargesAction extends BaseAction {

	private Long id;
	
	private String sessionCorpID;

	private String gotoPageString;

	private String validateFormNav;
	
	private PartnerRefCharges partnerRefCharges;
	
	private PartnerRefChargesManager partnerRefChargesManager;
	
	private List partnerRefChargesList;
	
	private RefMasterManager refMasterManager;
	
	private Map<String, String> country;
	
	private Map<String, String> grid;

	private String hitflag;

	private Map<String, String> basisList;

	public String saveOnTabChange() throws Exception {
		String s = save();
		validateFormNav = "OK";
		return gotoPageString;
	}
	
	@SkipValidation
	public String getComboList(String corpId) {
		country = refMasterManager.findByParameter(corpId, "COUNTRY");
		grid = refMasterManager.findByParameter(corpId, "GRID");
		basisList=refMasterManager.findByParameter(corpId, "RATEGRID_BASIS");
		return SUCCESS;
	}
	
	@SkipValidation
	public String list() {
		getComboList(sessionCorpID);
		partnerRefChargesList = partnerRefChargesManager.getAll();
		return SUCCESS;
	}

	public PartnerRefChargesAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			partnerRefCharges = partnerRefChargesManager.get(id);
		} else {
			partnerRefCharges = new PartnerRefCharges();
			partnerRefCharges.setCorpID(sessionCorpID);
			
			partnerRefCharges.setCreatedOn(new Date());
			partnerRefCharges.setUpdatedOn(new Date());
		}
		return SUCCESS;
	}

	public String save() throws Exception {
		getComboList(sessionCorpID);
		if (cancel != null) {
			return "cancel";
		}

		partnerRefCharges.setCorpID(sessionCorpID);
		boolean isNew = (partnerRefCharges.getId() == null);
		
		if (isNew) {
			partnerRefCharges.setCreatedOn(new Date());
			partnerRefCharges.setCreatedBy(getRequest().getRemoteUser());
		}
		
		partnerRefCharges.setUpdatedOn(new Date());
		partnerRefCharges.setUpdatedBy(getRequest().getRemoteUser());
		
		partnerRefChargesManager.save(partnerRefCharges);
		hitflag = "1";
		if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
			String key = (isNew) ? "partnerRefCharges.added" : "partnerRefCharges.updated";
			saveMessage(getText(key));
		}
		return SUCCESS;
	}
	public String delete() {
		partnerRefChargesManager.remove(id);
		saveMessage(getText("Partner RefCharges Item is deleted sucessfully"));
		
		return SUCCESS;
	}
	
	public String searchRefChargesList(){
		getComboList(sessionCorpID);
		String value;
		
			if(partnerRefCharges.getValue()==null){
				value="";
			}
			else
			{
				value=partnerRefCharges.getValue().toString();
			}
			
				
			
			partnerRefChargesList =partnerRefChargesManager.getRefCharesList(value,partnerRefCharges.getTariffApplicability(),partnerRefCharges.getParameter(),partnerRefCharges.getCountryCode(),partnerRefCharges.getGrid(),partnerRefCharges.getBasis(),partnerRefCharges.getLabel(), sessionCorpID);
		return SUCCESS;
}
	public void setId(Long id) {
		this.id = id;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getCountry() {
		return country;
	}

	public void setCountry(Map<String, String> country) {
		this.country = country;
	}

	public PartnerRefCharges getPartnerRefCharges() {
		return partnerRefCharges;
	}

	public void setPartnerRefCharges(PartnerRefCharges partnerRefCharges) {
		this.partnerRefCharges = partnerRefCharges;
	}

	public List getPartnerRefChargesList() {
		return partnerRefChargesList;
	}

	public void setPartnerRefChargesList(List partnerRefChargesList) {
		this.partnerRefChargesList = partnerRefChargesList;
	}

	public void setPartnerRefChargesManager(PartnerRefChargesManager partnerRefChargesManager) {
		this.partnerRefChargesManager = partnerRefChargesManager;
	}
	
	public Map<String, String> getGrid() {
		return grid;
	}

	public void setGrid(Map<String, String> grid) {
		this.grid = grid;
	}

	public Map<String, String> getBasisList() {
		return basisList;
	}

	public void setBasisList(Map<String, String> basisList) {
		this.basisList = basisList;
	}

}