<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="DeclarationOfInsurance" pageWidth="1735" pageHeight="842" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="1675" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="1ebb4dcb-a835-4f7e-824a-78de92fe3c83">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Begin_Date" class="java.util.Date"/>
	<parameter name="End_Date" class="java.util.Date"/>
	<queryString>
		<![CDATA[SELECT   "DeclarationOfInsurance-R-PDF-VOAO.jrxml",
	serviceorder.companydivision AS serviceorder_companydivision,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	serviceorder.firstName AS serviceorder_firstName,
	serviceorder.lastName AS serviceorder_lastName,
	serviceorder.coordinator AS serviceorder_coordinator,
	DATE_FORMAT(trackingstatus.beginPacking,'%d %b %Y') AS trackingstatus_beginPacking,
	serviceorder.mode AS serviceorder_mode,
	servicepartner.carrierVessels AS servicepartner_carrierVessels,
	CONCAT(if(serviceorder.originCity is null || trim(serviceorder.originCity)='','',trim(serviceorder.originCity)),if(serviceorder.originCountry is null || serviceorder.originCountry='','',if(serviceorder.originCity is null || trim(serviceorder.originCity)='',serviceorder.originCountry,concat(", ",serviceorder.originCountry)))) AS serviceorder_originCityCountry,
	CONCAT(if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='','',trim(serviceorder.destinationCity)),if(serviceorder.destinationCountry is null || serviceorder.destinationCountry='','',if(serviceorder.destinationCity is null || trim(serviceorder.destinationCity)='',serviceorder.destinationCountry,concat(", ",serviceorder.destinationCountry)))) AS serviceorder_destinationCityCountry,
	billing.vendorCode AS billing_vendorCode,
	billing.vendorName AS billing_vendorName,
       If(billing.baseInsuranceValue is null || trim(billing.baseInsuranceValue)='',0.00,convert(billing.baseInsuranceValue,DECIMAL(10,3))) AS billing_baseInsuranceValue,
	CONVERT(billing.insuranceBuyRate,DECIMAL(10,3)) AS billing_insuranceBuyRate,
	if(billing.baseInsuranceTotal is null || trim(billing.baseInsuranceTotal)='',0.00,CONVERT(billing.baseInsuranceTotal,DECIMAL(10,3))) AS billing_baseInsuranceTotal,
	billing.estMoveCost AS 'Est. Move Cost',
	refmaster.description AS serviceorder_commodity,
	customerfile.accountName AS customerfile_accountName,
	billing.billToName AS billing_billToName,
	if(miscellaneous.`unit2`='Cbm',If(serviceorder.`mode`='AIR',if(miscellaneous.actualCubicMtr is null || miscellaneous.actualCubicMtr='','',concat(miscellaneous.actualCubicMtr,' ','Cbm')),if(miscellaneous.netActualCubicMtr is null || miscellaneous.netActualCubicMtr='','',concat(miscellaneous.netActualCubicMtr,' ','Cbm'))),If(serviceorder.`mode`='AIR',if(miscellaneous.actualCubicFeet is null || miscellaneous.actualCubicFeet='','',concat(miscellaneous.actualCubicFeet,' ','Cft')),if(miscellaneous.netActualCubicFeet is null || miscellaneous.netActualCubicFeet='','',concat(miscellaneous.netActualCubicFeet,' ','Cft')))) AS Actual_Volume,
        billing.certnum AS billing_certnum,
        if(billing.insuranceHas='Y',billing.additionalNo,'') AS billing_additionalNo,
	billing.insuranceOption AS billing_insuranceOption
FROM 	serviceorder serviceorder
	INNER JOIN trackingstatus trackingstatus ON serviceorder.id = trackingstatus.id AND trackingstatus.corpID = $P{Corporate ID}
	INNER JOIN billing billing ON serviceorder.id = billing.id AND billing.corpID = $P{Corporate ID}
	INNER JOIN miscellaneous miscellaneous ON serviceorder.id = miscellaneous.id AND miscellaneous.corpID = $P{Corporate ID}
	INNER JOIN customerfile customerfile on customerfile.sequencenumber= serviceorder.sequencenumber and customerfile.corpid=$P{Corporate ID}
	LEFT OUTER JOIN servicepartner servicepartner ON servicepartner.serviceOrderId = serviceorder.id and servicepartner.status is true AND servicepartner.corpId=$P{Corporate ID}
	LEFT OUTER JOIN refmaster refmaster ON serviceorder.commodity= refmaster.code AND refmaster.parameter = 'COMMODIT' AND refmaster.corpID IN  ($P{Corporate ID},'TSFT','VOER')
WHERE  serviceorder.corpID =$P{Corporate ID}
AND date_format(billing.issueDate,'%Y-%m-%d') BETWEEN $P{Begin_Date} AND $P{End_Date}
and billing.insuranceHas in ('Y','V')
group by serviceorder.id
order by serviceorder.shipNumber]]>
	</queryString>
	<field name="DeclarationOfInsurance-R-PDF-VOAO.jrxml" class="java.lang.String"/>
	<field name="serviceorder_companydivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_firstName" class="java.lang.String"/>
	<field name="serviceorder_lastName" class="java.lang.String"/>
	<field name="serviceorder_coordinator" class="java.lang.String"/>
	<field name="trackingstatus_beginPacking" class="java.lang.String"/>
	<field name="serviceorder_mode" class="java.lang.String"/>
	<field name="servicepartner_carrierVessels" class="java.lang.String"/>
	<field name="serviceorder_originCityCountry" class="java.lang.String"/>
	<field name="serviceorder_destinationCityCountry" class="java.lang.String"/>
	<field name="billing_vendorCode" class="java.lang.String"/>
	<field name="billing_vendorName" class="java.lang.String"/>
	<field name="billing_baseInsuranceValue" class="java.math.BigDecimal"/>
	<field name="billing_insuranceBuyRate" class="java.math.BigDecimal"/>
	<field name="billing_baseInsuranceTotal" class="java.math.BigDecimal"/>
	<field name="Est. Move Cost" class="java.math.BigDecimal"/>
	<field name="serviceorder_commodity" class="java.lang.String"/>
	<field name="customerfile_accountName" class="java.lang.String"/>
	<field name="billing_billToName" class="java.lang.String"/>
	<field name="Actual_Volume" class="java.lang.String"/>
	<field name="billing_certnum" class="java.lang.String"/>
	<field name="billing_additionalNo" class="java.lang.String"/>
	<field name="billing_insuranceOption" class="java.lang.String"/>
	<variable name="totalHits" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[$F{serviceorder_shipNumber}]]></variableExpression>
	</variable>
	<variable name="totalInsured" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{billing_baseInsuranceValue}]]></variableExpression>
	</variable>
	<variable name="totalBuy" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{billing_baseInsuranceTotal}]]></variableExpression>
	</variable>
	<background>
		<band height="1" splitType="Stretch"/>
	</background>
	<title>
		<band height="24" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="2" width="1675" height="18" uuid="fc7b0f34-1a2b-487d-bb86-1921f5f792e1"/>
				<textElement textAlignment="Center">
					<font size="12" isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Declaration of Insurance]]></text>
			</staticText>
			<line>
				<reportElement key="line-1" x="0" y="21" width="1675" height="1" forecolor="#000099" uuid="805a590f-1056-43f6-bd98-73b034442caa"/>
			</line>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="36" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="75" y="5" width="88" height="15" backcolor="#CCCCCC" uuid="09ce8f9a-1307-4fe9-87ef-be592b0f5d3e"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[SO #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" mode="Opaque" x="163" y="5" width="80" height="14" backcolor="#CCCCCC" uuid="63eb88c2-c178-46d9-83de-182ac32ab027"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[First Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" mode="Opaque" x="242" y="5" width="83" height="15" backcolor="#CCCCCC" uuid="875d345f-3da7-407a-9076-f12fa8926de4"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Surname]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" mode="Opaque" x="410" y="5" width="68" height="14" backcolor="#CCCCCC" uuid="de1b0dec-7be3-455e-af10-d90f9fb3cdfc"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Packing
 Date(T)
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" mode="Opaque" x="478" y="5" width="38" height="15" backcolor="#CCCCCC" uuid="71e3bd21-d435-4862-8852-bf4dc1716a76"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Mode]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" mode="Opaque" x="510" y="5" width="96" height="14" backcolor="#CCCCCC" uuid="4098dbc1-c141-4e2b-b5a5-0836056dc6cb"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Vessel/ Flight #]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" mode="Opaque" x="589" y="5" width="101" height="14" backcolor="#CCCCCC" uuid="5a5fc601-62e5-43fa-8b7d-2244f4869df4"/>
				<box leftPadding="5"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Origin City
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" mode="Opaque" x="676" y="5" width="105" height="15" backcolor="#CCCCCC" uuid="7b3d4ddf-f6c0-4a54-8a58-d7cd096bc0b9"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Destination City
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" mode="Opaque" x="766" y="5" width="65" height="14" backcolor="#CCCCCC" uuid="814163bf-0670-4a37-abf9-65d0452c59f6"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Vendor]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" mode="Opaque" x="816" y="5" width="69" height="14" backcolor="#CCCCCC" uuid="52e65767-52a4-4437-8905-caeec9d0f2e6"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Vendor
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="75" y="19" width="88" height="16" backcolor="#CCCCCC" uuid="2ae247ed-06b5-4ccf-92c9-564276dfc002"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" mode="Opaque" x="163" y="19" width="80" height="16" backcolor="#CCCCCC" uuid="564b0775-51b6-4dfa-85b3-a40437e1f7c6"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-18" mode="Opaque" x="243" y="19" width="83" height="16" backcolor="#CCCCCC" uuid="5e7a21bc-ed70-4346-9a8a-9f60ab1aadaa"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-19" mode="Opaque" x="410" y="19" width="68" height="16" backcolor="#CCCCCC" uuid="2db6f3f6-4875-454e-986e-1d3d35bdde62"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Date(T)
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-20" mode="Opaque" x="478" y="18" width="33" height="17" backcolor="#CCCCCC" uuid="8dd6eec4-aa88-421f-9ef6-6b4d3714c263"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" mode="Opaque" x="510" y="19" width="91" height="16" backcolor="#CCCCCC" uuid="59d826f9-458f-4f69-83f0-aa5801d2308f"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-22" mode="Opaque" x="589" y="19" width="101" height="16" backcolor="#CCCCCC" uuid="37e94a91-f910-412b-8e5e-15c193950f30"/>
				<box leftPadding="5"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Country]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" mode="Opaque" x="676" y="19" width="105" height="16" backcolor="#CCCCCC" uuid="8ccced4f-c225-45e5-91e4-ffd84d6b97a9"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Country]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-27" mode="Opaque" x="766" y="19" width="65" height="16" backcolor="#CCCCCC" uuid="92f5c80c-4755-472c-9528-5a989f527dda"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Code]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-28" mode="Opaque" x="816" y="19" width="69" height="16" backcolor="#CCCCCC" uuid="18345eb8-2cd3-4a68-9d78-02b550b44072"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-33" mode="Opaque" x="981" y="5" width="64" height="14" backcolor="#CCCCCC" uuid="c6c456d0-b58b-45d5-8bcc-11e47ddad148"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Buying]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-34" mode="Opaque" x="974" y="19" width="71" height="16" backcolor="#CCCCCC" uuid="2ec362c9-94db-4d56-8f5b-a165f6f7258b"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Amount €]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-35" mode="Opaque" x="1045" y="5" width="73" height="14" backcolor="#CCCCCC" uuid="cbf1646e-2cf5-49c2-bd56-4a6d85aa3a7d"/>
				<box leftPadding="3"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Commodity]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-36" mode="Opaque" x="1045" y="19" width="73" height="16" backcolor="#CCCCCC" uuid="7aa8c8a8-fa72-438a-b1b7-0cb1d0f02c85"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-31" mode="Opaque" x="929" y="5" width="57" height="15" backcolor="#CCCCCC" uuid="172cd372-bc6f-48d4-a70c-b2ff431463e2"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Buying]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-32" mode="Opaque" x="929" y="20" width="57" height="15" backcolor="#CCCCCC" uuid="5ee013e2-b133-4d97-a162-d36b3bcc2591"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Rate in %]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-29" mode="Opaque" x="855" y="5" width="80" height="15" backcolor="#CCCCCC" uuid="d55b16b2-265c-4fa7-873d-d4ba625860da"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Insured
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-30" mode="Opaque" x="853" y="19" width="82" height="16" backcolor="#CCCCCC" uuid="21df40a5-27dd-4938-9a62-bd0ae26ee95e"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[ Value €]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-42" mode="Opaque" x="1210" y="5" width="92" height="30" backcolor="#CCCCCC" uuid="7039293b-5b7c-4174-be0b-b34bbd3b9765"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Bill To Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-44" mode="Opaque" x="1302" y="5" width="71" height="30" backcolor="#CCCCCC" uuid="06be0e40-79ce-4a07-bbe2-eb1d5bec95ed"/>
				<textElement textAlignment="Right">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Actual Volume]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-46" mode="Opaque" x="1118" y="5" width="92" height="30" backcolor="#CCCCCC" uuid="6c382d20-be56-4667-9d06-755919fb2946"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Account Name]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-47" mode="Opaque" x="1373" y="5" width="81" height="30" backcolor="#CCCCCC" uuid="53a3c7f3-7712-448c-8eb3-a6d0bdf065f0"/>
				<box leftPadding="8"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Certificate#]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-48" mode="Opaque" x="1454" y="5" width="80" height="30" backcolor="#CCCCCC" uuid="82ff69a4-435d-455f-9d4c-1211d0cbd36d"/>
				<box leftPadding="5"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Additional#]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-49" mode="Opaque" x="1532" y="5" width="143" height="30" backcolor="#CCCCCC" uuid="fd914431-f196-45a8-a573-8f3ba4dbaaf4"/>
				<box leftPadding="5"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Declaration of Insurance]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-2" mode="Opaque" x="1" y="5" width="74" height="15" backcolor="#CCCCCC" uuid="2828d62d-fbc2-48eb-8244-c5ec8bbbb485"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Company]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" mode="Opaque" x="1" y="19" width="74" height="16" backcolor="#CCCCCC" uuid="825b7557-4cf6-4bcd-ba93-42021a8ea09e"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Division]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-50" mode="Opaque" x="325" y="5" width="85" height="15" backcolor="#CCCCCC" uuid="09a891b2-0614-44bb-a2d4-b68de1459915"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Coordinator]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-51" mode="Opaque" x="326" y="19" width="85" height="16" backcolor="#CCCCCC" uuid="6c4ac0a5-cb58-4a21-ba79-6e69fad37652"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Name]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="40" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-1" x="75" y="4" width="87" height="36" uuid="65d82411-c7cf-4044-8925-77bc2c42f5c8"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-2" x="163" y="4" width="80" height="36" uuid="eac5d454-efa6-4fb9-9eec-5e016c743940"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_firstName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-3" x="243" y="4" width="82" height="36" uuid="8c17b43c-3813-4efa-b2de-a48855beb0f4"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_lastName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-4" x="410" y="4" width="67" height="36" uuid="454aa9e0-5df6-4eb8-aa44-91149a63dc89"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{trackingstatus_beginPacking}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-5" x="477" y="4" width="33" height="36" uuid="5edb4998-2df1-4bed-9b5b-0ec72a43a6c6"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_mode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-6" x="511" y="4" width="78" height="36" uuid="ae2cf40d-fd40-4c43-b1a3-aacf7083ed22"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_carrierVessels}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-7" x="589" y="4" width="87" height="36" uuid="1897fb2f-a4f1-430c-9d32-61e3f692a620"/>
				<box leftPadding="5"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_originCityCountry}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-9" x="677" y="4" width="89" height="36" uuid="2338a4e8-186f-4672-b62f-d6e59ee72cd4"/>
				<box rightPadding="2"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_destinationCityCountry}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-12" x="765" y="4" width="51" height="36" uuid="378bc352-d9fa-4017-8f37-423858975108"/>
				<box leftPadding="2"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_vendorCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-13" x="815" y="4" width="67" height="36" uuid="4c6ade30-e0ef-483c-9089-3530e0b10d74"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_vendorName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.000" isBlankWhenNull="true">
				<reportElement key="textField-18" x="882" y="4" width="59" height="36" uuid="f4757e1a-12c1-471e-aa71-790687e56b9f"/>
				<textElement textAlignment="Right">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_baseInsuranceValue}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.000" isBlankWhenNull="true">
				<reportElement key="textField-19" x="945" y="4" width="41" height="36" uuid="c8f5168d-9e91-48ec-a594-4c5838318832"/>
				<textElement textAlignment="Right" verticalAlignment="Top">
					<font fontName="SansSerif" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_insuranceBuyRate}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.000" isBlankWhenNull="true">
				<reportElement key="textField-20" x="988" y="4" width="56" height="35" uuid="da200cdd-0f48-4a48-917b-d73432e68393"/>
				<textElement textAlignment="Right">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_baseInsuranceTotal}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-21" x="1046" y="4" width="72" height="36" uuid="5aca74ce-1216-4d95-9175-207f09090b11"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Left">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_commodity}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-25" x="1118" y="4" width="92" height="36" uuid="458e7658-caf7-47eb-baa4-03f8d54ea096"/>
				<box rightPadding="2"/>
				<textElement textAlignment="Left" verticalAlignment="Top">
					<font fontName="SansSerif" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customerfile_accountName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-26" x="1210" y="4" width="92" height="36" uuid="56b59bfb-39bd-4932-9782-0b37779e9ce8"/>
				<box rightPadding="3"/>
				<textElement textAlignment="Left">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_billToName}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-27" x="1302" y="4" width="71" height="36" uuid="020f5b00-965c-4205-956c-8d4a80ef31f0"/>
				<box leftPadding="2"/>
				<textElement textAlignment="Right">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Actual_Volume}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-28" x="1373" y="4" width="81" height="36" uuid="fdbbb54b-550e-498e-979c-b34948dbf4f2"/>
				<box leftPadding="8"/>
				<textElement textAlignment="Left">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_certnum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-29" x="1454" y="4" width="80" height="36" uuid="8ccd61c9-d86e-469d-8beb-3e872e804373"/>
				<box leftPadding="5"/>
				<textElement textAlignment="Left">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_additionalNo}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-30" x="1534" y="4" width="141" height="36" uuid="9e23f0c3-00e1-423e-b7f1-34511aadf0ed"/>
				<box leftPadding="5"/>
				<textElement textAlignment="Left">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{billing_insuranceOption}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-1" x="2" y="4" width="73" height="36" uuid="bac1de12-ef0a-40e3-b1d4-7d9dfe6b3553"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_companydivision}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-31" x="325" y="4" width="85" height="36" uuid="befde688-9652-4e1b-b958-0b6ff1157fa9"/>
				<textElement>
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_coordinator}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="18" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-14" x="1567" y="3" width="50" height="15" uuid="9e0a94af-efb7-4323-b2d2-42e693ae974b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Helvetica" size="8" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="true">
				<reportElement key="textField-15" x="1618" y="3" width="56" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="379553ad-ca1c-4374-b663-66e73fed989b"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None">
					<font fontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Times-Roman" pdfEncoding="CP1252" isPdfEmbedded="false"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-15" x="482" y="3" width="711" height="14" uuid="b881ae17-43c4-49af-840c-a8628c750406"/>
				<textElement textAlignment="Center">
					<font pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[DeclarationOfInsurance.jrxml]]></text>
			</staticText>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField-17" x="2" y="2" width="137" height="15" forecolor="#000000" backcolor="#FFFFFF" uuid="af6f84ce-1083-4e89-a820-dcaf1d5c0695"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Helvetica" size="10" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[new Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band height="23" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-37" x="563" y="4" width="96" height="16" uuid="987855f0-9404-41cc-9831-4efc1653a6b6"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total Policies:]]></text>
			</staticText>
			<textField isBlankWhenNull="true">
				<reportElement key="textField-22" x="635" y="4" width="54" height="16" uuid="a2e0668c-c582-4518-8140-00a8f475823e"/>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalHits}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="###0.000" isBlankWhenNull="true">
				<reportElement key="textField-23" x="796" y="4" width="88" height="15" uuid="0bf197a0-2aa1-4351-81dd-85a70a96bf05"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalInsured}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-38" x="692" y="4" width="117" height="16" uuid="053a83b4-13be-4e70-9862-a360d6f2bd12"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total Insured Value:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-39" x="789" y="4" width="17" height="17" uuid="3cb97570-5440-47a7-aad2-ab575b04d9bb"/>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[€]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-40" x="884" y="4" width="119" height="14" uuid="67ba6615-3aec-4939-999c-1e0c17bd3ab0"/>
				<textElement>
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[Total Buying Amount:]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-41" x="990" y="4" width="26" height="13" uuid="5c8d1963-6a6e-487f-aa3e-78b057a2e738"/>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<text><![CDATA[€]]></text>
			</staticText>
			<textField pattern="###0.000" isBlankWhenNull="true">
				<reportElement key="textField-24" x="997" y="4" width="124" height="14" uuid="213d75ce-5a33-42eb-a982-b7cfdfd5a854"/>
				<textElement textAlignment="Left">
					<font isBold="true" pdfFontName="Times-Roman"/>
					<paragraph lineSpacing="Single"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{totalBuy}]]></textFieldExpression>
			</textField>
			<line>
				<reportElement key="line-4" x="882" y="3" width="1" height="19" forecolor="#3333FF" backcolor="#FFFFFF" uuid="40a91e81-a964-48a5-931d-06d479108235"/>
			</line>
			<line>
				<reportElement key="line-5" x="689" y="3" width="1" height="19" forecolor="#3333FF" backcolor="#FFFFFF" uuid="5bc57191-34d7-42f2-8eff-287d7c8ed9f4"/>
			</line>
			<line>
				<reportElement key="line-2" x="0" y="2" width="1675" height="1" forecolor="#000099" uuid="b78f7486-f38a-4aea-ae11-f56505917436"/>
			</line>
			<line>
				<reportElement key="line-3" x="0" y="22" width="1675" height="1" forecolor="#000099" uuid="fa30cce1-db1c-444e-a4f7-1cbbdaeed410"/>
			</line>
		</band>
	</summary>
</jasperReport>
