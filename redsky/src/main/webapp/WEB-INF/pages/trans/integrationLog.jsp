<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title><fmt:message key="integrationLogList.title"/></title>
    <meta name="heading" content="<fmt:message key='integrationLogList.heading'/>"/>
   
   <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
   </script>
   <!-- Modification closed here -->
    
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			var i;
					for(i=0;i<=4;i++)
					{
						document.forms['integrationLogForm'].elements['integrationLog.fileName'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.processedOn'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.recordID'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.message'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.transactionID'].value = "";
						document.forms['integrationLogForm'].elements[i].value = "";
					}
		}
</script>
   
   <style> 
   span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
   
   </style>
</head>


<s:form id="integrationLogForm" action="searchIntegrationLog" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<table class="table" width="100%" >
<thead>
<tr>

<th><fmt:message key="integrationLog.fileName"/></th>
<th><fmt:message key="integrationLog.processedOn"/></th>
<th><fmt:message key="integrationLog.recordID"/></th>
<th><fmt:message key="integrationLog.message"/></th>
<th><fmt:message key="integrationLog.transactionID"/></th>

</tr></thead>	
		<tbody>
		<tr>
			
			<td><s:textfield name="integrationLog.fileName" required="true" size="30" cssClass="text medium" onkeypress="return checkIt(event)" onfocus="onFormLoad();" /></td>
			<!--<td><s:textfield name="integrationLog.processedOn"  required="true" cssClass="text medium"/></td>-->
			<c:if test="${not empty integrationLog.processedOn}">
				<s:text id="integrationLogprocessedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="integrationLog.processedOn" /></s:text>
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="integrationLog.processedOn" value="%{integrationLogprocessedOnFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty integrationLog.processedOn}">
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="integrationLog.processedOn" required="true" cssStyle="width:60px" maxlength="11" readonly="true"   onkeydown="return onlyDel(event,this)" /><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			<td><s:textfield name="integrationLog.recordID" size="30" required="true" cssClass="text medium" /></td>
			<td><s:textfield name="integrationLog.message" size="30" required="true" cssClass="text medium" /></td>
			<td><s:textfield name="integrationLog.transactionID" size="30" required="true" cssClass="text medium" /></td>
			</tr>
			<tr>
			<td colspan="4"></td>
			<td width="" style="border-left: hidden;">
			    <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px" key="button.search"  />
			    <input type="button" class="cssbutton" value="Clear" style="width:70px; height:25px" onclick="clear_fields();"/>   
			</td>
		</tr>
		</tbody>
	</table>

		<div id="newmnav">
		  <ul>
		  	<li><a href="weeklyXML.html"><span>Weekly XML Summary</span></a></li>
		    <li  id="newmnav1" style="background:#FFF "><a class="current"><span>Integration Log List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="memoUpload.html"><span>Memo Upload List</span></a></li>
		  </ul>
		</div>
		<div class="spn" style="line-height:0px;!margin-bottom:3px;">&nbsp;</div>		
<s:set name="integrationLogs" value="integrationLogs" scope="request"/>
<display:table name="integrationLogs" class="table" requestURI="" id="integrationLogList" export="true" pagesize="50" defaultsort="5" defaultorder="descending">
    <display:column property="id" sortable="true" paramId="id" paramProperty="id" titleKey="integrationLog.id"/>
    <display:column property="message" sortable="true" titleKey="integrationLog.message"/>
    <display:column property="fileName" sortable="true" titleKey="integrationLog.fileName"/>
 	<display:column property="batchID" style="text-align: right" sortable="true" titleKey="integrationLog.batchID"/>
    <display:column property="processedOn" sortable="true" title="Processed On" style="width:150px; text-align:right;"/>
    <display:column property="recordID" sortable="true" titleKey="integrationLog.recordID"/>
     <display:column  sortable="true" titleKey="integrationLog.transactionID">    
    	<c:set var="sign" value="${integrationLogList.fileName}"/>    
	    <c:choose>
			<c:when test="${fn:startsWith(sign, 'st')}">
				<a href="editVanLine.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:when>
			<c:when test="${integrationLogList.controlFlag=='Q'}">
			<a href="editQuotationServiceOrderUpdate.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:when>
	    	<c:otherwise>
	    		<a href="editServiceOrderUpdate.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:otherwise>
		</c:choose>
    </display:column>
    <display:setProperty name="paging.banner.item_name" value="integrationLog"/>
    <display:setProperty name="paging.banner.items_name" value="people"/>

    <display:setProperty name="export.excel.filename" value="IntegrationLog List.xls"/>
    <display:setProperty name="export.csv.filename" value="IntegrationLog List.csv"/>
    <display:setProperty name="export.pdf.filename" value="IntegrationLog List.pdf"/>
</display:table>
</s:form>
<script type="text/javascript">
    //highlightTableRows("integrationLogList");
    //Form.focusFirstElement($("integrationLogForm")); 
   	
   	RANGE_CAL_1 = new Calendar({
           inputField: "processedOn",
           dateFormat: "%d-%b-%y",
           trigger: "processedOn_trigger",
           bottomBar: true,
           animation:true,
           onSelect: function() {                             
               this.hide();
       }
        
   });
  
</script>