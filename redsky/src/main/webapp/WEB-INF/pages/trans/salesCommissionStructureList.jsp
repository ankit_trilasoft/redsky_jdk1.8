<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Sales Commission %</title>
<STYLE type=text/css>
</style>
<!-- Modified By Dilip at 16-May-2013 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->


<script type="text/JavaScript">
function addRow(tableID) {

	  var table = document.getElementById(tableID);
	    var rowCount = table.rows.length;
		var row = table.insertRow(rowCount); 
	    var newlineid=document.getElementById('newlineid').value;
	    if(newlineid==''){
	        newlineid=rowCount;
	      }else{
	      	newlineid=newlineid+"~"+rowCount;
	      }
	    document.getElementById('newlineid').value=newlineid;

	 	
		var cell1 = row.insertCell(0);
		var element1 = document.createElement("input");
		element1.type = "text";
		element1.style.width="50px";
		element1.setAttribute("class", "input-text" );
		element1.id='sbp'+rowCount;
		element1.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		cell1.appendChild(element1);

		var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "text";
		element2.style.width="50px";
		element2.setAttribute("class", "input-text" );
		element2.id='sbc'+rowCount;
		element2.setAttribute("onkeydown","return onlyFloatNumsAllowed(event);");
		cell2.appendChild(element2);
            
        }

function save(){

    var salesBaListServer="";
   	var commissionPercentageListServer="";
   	var id='';	 
		    if(document.forms['commisionStructureForm'].salesBaList!=undefined){
		        if(document.forms['commisionStructureForm'].salesBaList.size!=0){
		 	      for (i=0; i<document.forms['commisionStructureForm'].salesBaList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].salesBaList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(salesBaListServer==''){
	 	            	salesBaListServer=id+": "+document.forms['commisionStructureForm'].salesBaList[i].value;
	 	             }else{
	 	            	salesBaListServer= salesBaListServer+"~"+id+": "+document.forms['commisionStructureForm'].salesBaList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].salesBaList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        salesBaListServer=id+": "+document.forms['commisionStructureForm'].salesBaList.value;
		 	      }	        
		       }		   

		    if(document.forms['commisionStructureForm'].commissionList!=undefined){
		        if(document.forms['commisionStructureForm'].commissionList.size!=0){
		 	      for (i=0; i<document.forms['commisionStructureForm'].commissionList.length; i++){	
		 	    	 id=document.forms['commisionStructureForm'].commissionList[i].id;
	                 id=id.replace(id.substring(0,3),'').trim();
	 	             if(commissionPercentageListServer==''){
	 	            	commissionPercentageListServer=id+": "+document.forms['commisionStructureForm'].commissionList[i].value;
	 	             }else{
	 	            	commissionPercentageListServer= commissionPercentageListServer+"~"+id+": "+document.forms['commisionStructureForm'].commissionList[i].value;
	 	             }
		 	        }	
		 	      }else{	
		 	    	 id=document.forms['commisionStructureForm'].commissionList.id;
		 	         id=id.replace(id.substring(0,3),'').trim();   
		 	        commissionPercentageListServer=id+": "+document.forms['commisionStructureForm'].commissionList.value;
		 	      }	        
		       }		   
		    
			 document.getElementById('salesBaListServer').value=salesBaListServer; 	
			 document.getElementById('commissionPercentageListServer').value=commissionPercentageListServer; 		  	     		       
			 
		     var salesPersonCommissionListServer="";
		     var newlineid=document.getElementById('newlineid').value;
		     if(newlineid!=''){
		            var arrayLine=newlineid.split("~");
		            for(var i=0;i<arrayLine.length;i++){		            	              
		                if(salesPersonCommissionListServer==''){
		                	salesPersonCommissionListServer=document.getElementById('sbp'+arrayLine[i]).value+":"+document.getElementById('sbc'+arrayLine[i]).value;
		                }else{
		                	salesPersonCommissionListServer=salesPersonCommissionListServer+"~"+document.getElementById('sbp'+arrayLine[i]).value+":"+document.getElementById('sbc'+arrayLine[i]).value;
		                }
		              }
		         }
		     
		    document.getElementById('salesPersonCommissionListServer').value =salesPersonCommissionListServer;
		    saveCommissionValue();			   
}
	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	} 

function saveCommissionValue(){
    var salesBA=document.getElementById('salesBaListServer').value;
    var salesCP=document.getElementById('commissionPercentageListServer').value;
   	var commiNL=document.getElementById('salesPersonCommissionListServer').value;
   	if((salesBA!='')||(salesCP!='')||(commiNL!='')){   
	    var url="saveSalesCommissionValueAjax.html?ajax=1&salesBA="+salesBA+"&salesCP="+salesCP+"&commiNL="+commiNL+"&decorator=simple&popup=true&sContarct=${sContarct}&sChargeCode=${sChargeCode}";
	    http1986.open("GET", url, true);
	    http1986.onreadystatechange = handleHttpResponseAccListData;
	    http1986.send(null); 
   	}
}
function handleHttpResponseAccListData(){
    if (http1986.readyState == 4){
        alert("Commission Persentage Successfully Updated.");
        document.getElementById('salesBaListServer').value="";
        document.getElementById('commissionPercentageListServer').value="";
       	document.getElementById('salesPersonCommissionListServer').value="";
       	document.getElementById('newlineid').value="";
    }
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http1986 = getHTTPObject();
</script>

<s:form name="commisionStructureForm" id="commisionStructureForm" action="" method="post">
<s:hidden name="newlineid" id="newlineid" value=""/>
<s:hidden name="salesPersonCommissionListServer" id="salesPersonCommissionListServer" value=""/>
<s:hidden name="salesBaListServer" id="salesBaListServer" value=""/>
<s:hidden name="commissionPercentageListServer" id="commissionPercentageListServer" value=""/>
		<div id="otabs" style="margin-top: 10px; margin-bottom:0px;">

		  <ul>
		    <li><a class="current"><span>Sales Commission %</span></a></li>		    
		  </ul>
		</div>
		<div class="spn" style="line-height:0px;">&nbsp;</div>
		<div id="para1" style="clear:both;">
		 <table class="table" id="dataTable" style="width:500px;!margin-top:-6px;">
			 <thead>
			 <tr>
				 <th width="50px">Sales - BA %</th>
				 <th width="50px">Commission %</th>
			 </tr>
			 </thead>
		 <tbody>
			 <c:forEach var="individualItem" items="${salesPersonCommissionList}" >
				 <tr>
				 <td  class="listwhitetext" align="right" >
				 <input type="text" value="${individualItem.salesBa}" maxlength="15" class="input-text" name="salesBaList" id="sbp${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;" />
				 </td>
				 <td  class="listwhitetext" align="right" >
				<input type="text" value="${individualItem.commissionPercentage}" maxlength="15" class="input-text" name="commissionList" id="sbc${individualItem.id}" onkeydown="return onlyFloatNumsAllowed(event);" style="width:50px;"/>				 
				 </td>
				 </tr>
			 </c:forEach>
		 </tbody>		 		  
		 </table>
		 <table>
		 <tr>
		      	<td>
		      	<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width:60px; height:25px;margin-left:38px;" value="Add Lines" onclick="addRow('dataTable');" />
		      	</td>
		      	<td>
				<input type="button" class="cssbutton1" id="saveLine" name="saveLine" style="width:35px; height:25px;margin-left:5px;" value="Save" onclick="save();" />
		      	</td>		 
	     </tr>			 
		 
		 </table>
		 </div>
</s:form>