package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.PartnerProfileInfo;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PartnerProfileInfoManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerProfileInfoAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	private Long id;
	private PartnerProfileInfo partnerProfileInfo;
	private RefMasterManager refMasterManager;
	private PartnerProfileInfoManager partnerProfileInfoManager;
	private String partnerType;
	private String partnerCode;
	private String partnerId;
	Date currentdate = new Date();
	private String popupval;
	private String gotoPageString;
	private Map<String, String> agent;
	private PartnerPublic partnerPublic;
	private PartnerPublicManager partnerPublicManager;
	static final Logger logger = Logger.getLogger(PartnerProfileInfoAction.class);
	
	public PartnerProfileInfoAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	@SkipValidation
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");		
		agent = refMasterManager.findByParameter(corpId, "AGENTPROFILEINFO");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		List profileInfoList=partnerProfileInfoManager.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode);
		if(profileInfoList != null && (!(profileInfoList.isEmpty())) && profileInfoList.get(0)!= null && (!(profileInfoList.get(0).toString().equals("")))){
			partnerProfileInfo = (PartnerProfileInfo)partnerProfileInfoManager.getProfileInfoListByPartnerCode(sessionCorpID, partnerCode).get(0);
		}else {
			partnerProfileInfo = new PartnerProfileInfo();
			partnerProfileInfo.setPartnerCode(partnerCode);
			partnerProfileInfo.setPartnerPrivateId(new Long(partnerId));
			partnerProfileInfo.setCorpId(sessionCorpID);
			partnerProfileInfo.setCreatedOn(new Date());
			partnerProfileInfo.setUpdatedOn(new Date());
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");				
		boolean isNew = (partnerProfileInfo.getId() == null);
		partnerProfileInfo.setUpdatedBy(getRequest().getRemoteUser());
		if (isNew) {
			partnerProfileInfo.setCreatedOn(new Date());
		}
		partnerProfileInfo.setUpdatedOn(new Date());
		if(partnerProfileInfo.getPassword()!=null && (!partnerProfileInfo.getPassword().equals(""))){
		Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);
		if (/*"true".equals(getRequest().getParameter("encryptPass")) && */(encrypt != null && encrypt)) {
			String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);
		if (algorithm == null) { // should only happen for test case
            log.debug("assuming testcase, setting algorithm to 'SHA'");
            algorithm = "SHA";
        }
		
		String pass=partnerProfileInfo.getPassword();
		partnerProfileInfo.setPassword(StringUtil.encodePassword(pass, algorithm));
		}
		}
		partnerProfileInfo=partnerProfileInfoManager.save(partnerProfileInfo); 
		if (!popupval.equalsIgnoreCase("true")) {
			if (gotoPageString == null || gotoPageString.equalsIgnoreCase("")) {
				String key = (isNew) ? "Profile Info added successfully." : "Profile Info updated successfully.";
				saveMessage(getText(key));
			}
		}
		partnerPublic = partnerPublicManager.get(new Long(partnerId));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public String getPartnerType() {
		return partnerType;
	}

	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public Date getCurrentdate() {
		return currentdate;
	}

	public void setCurrentdate(Date currentdate) {
		this.currentdate = currentdate;
	}

	public static Logger getLogger() {
		return logger;
	}

	public String getPopupval() {
		return popupval;
	}

	public void setPopupval(String popupval) {
		this.popupval = popupval;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public Map<String, String> getAgent() {
		return agent;
	}

	public void setAgent(Map<String, String> agent) {
		this.agent = agent;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}

	public PartnerProfileInfo getPartnerProfileInfo() {
		return partnerProfileInfo;
	}

	public void setPartnerProfileInfo(PartnerProfileInfo partnerProfileInfo) {
		this.partnerProfileInfo = partnerProfileInfo;
	}

	public PartnerProfileInfoManager getPartnerProfileInfoManager() {
		return partnerProfileInfoManager;
	}

	public void setPartnerProfileInfoManager(
			PartnerProfileInfoManager partnerProfileInfoManager) {
		this.partnerProfileInfoManager = partnerProfileInfoManager;
	}

}
