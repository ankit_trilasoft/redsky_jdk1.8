package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AvailableCrew;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.DayDistributionHoursDTO;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.WorkTicket;

public interface TimeSheetDao  extends GenericDao<TimeSheet, Long> { 
	
	 public List<WorkTicket> findTickets(String warehouse,String service,String date1, String shipper, String tkt , String sessionCorpID);
	 public List<WorkTicket> findTicket(Long tkt);
	 public List<Billing> getBilling(String shipNum);
	 public List<PayrollAllocation> findDistAndCalcCode(String job, String service, String sessionCorpID,String mode,String comDiv);
	 public List<AvailableCrew> searchAvailableCrews(String warehouse,String date1, String crewNm, String sessionCorpID);
	 public List<Payroll> findTimeSheets(String warehouse,String date1, String shipper, String tkt, String crewNm, String sessionCorpID);
	 public List<SystemDefault> findSystemDefault(String corpID);
	 public List getTimeHoursFormSysDefault(String crewCorpId);
	 public List<AvailableCrew> findCrewList();
	 public List existsTimeSheet(Date wDate, String uName, String wHouse);
	 public List findTimeSheetByTicket(Long ticket);
	 //public List<DayDistributionHoursDTO> findDayDistributionHoursList(Long ticket, String actWgt, String totRev);
	 public List findDayDistributionHoursList(Long ticket, String actWgt );
	 public String findExpression(String calc, String sessionCorpID,String compDivision);
	 public List findWorkDays(Long ticket);
	 public List findTotalHoursWorked(Long ticket);
	 public List findNormalDist(Long ticket, Date workDt, String job, String service, String compDivision);
	 public List findTimeSheetByTicketAndDate(Long ticket, Date wrkDt);
	 public Boolean totalRevenueShare(Long ticket);
	 public TimeSheet findTimeSheetDetail();
	 public void updateWorkTicketData(Long ticket, String totalRevenue, String calcCode);
	 public List findTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List checkRevenueSharing(String crewNm, String workDt, String wareHse,String sessionCorpID, String endDt);
	 public List findTimeSheetsForNonUnionOT(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List findSummerTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List findNonRevenueHrs(String crewNm, String workDt, String wareHse);
	 public List findOTHrs(String crewNm, String workDt, String wareHse);
	 public List findAllTimeSheets(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List checkExtraPaidTimeSheet(String workDt,String userName);
	 public List checkActionOTimeSheet(String workDt,String userName);
	 public List getTimeFromEndHours(Long ticket, String crewName, Date date1);
	 public List getBiginHoursTime(Long ticket, String crewName, Date workDate);
	 public List findRegularHours(Long ticket, String crewName, Date workDate);
	 public void updateDoneForTheDay(String crewName, Date workDate, String DFD);
	 public List findOverTimeList(String beginDt, String endDt, String wareHse);
	 public void updateCrewType(String ticket);
	 public void deleteFromTimeSheet(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List getNumberOfCrew(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List getMaxId(String userName);
	 public void updateOtherTimeSheet(String bhours,String eHours, String regHours, String doneForDay,String pRevShare,String id,String action, String lunch, String reasonForAdjustment, String overTime, String doubleOverTime, String differentPayBasis, String adjustmentToRevenue, String adjustedBy, String calcCode, String distCode);
	 
	 public List excludeEearningOppertunity(String wareHse, String newWrkDt, String tkt, String crewNm);
	 public List doneForDay(String wareHse, String newWrkDt, String tkt, String crewNm);
	 public List filterLunch(String wareHse, String newWrkDt, String tkt, String crewNm);
	 
	 public List getParentTimeSheet(String userName, Date workDate, String lunch);
	 public void updateRegHours(String regHours, String userName, Date workDate, String lunch);
	 public List openTimeSheets(String wareHse, String newWrkDt, String tkt, String crewNm);
	 
	 public List getWorkTicket(Long ticket, String sessionCorpID);
	 public void deleteLunchTicket(Date date, String name, String house);
	 
	 public List checkRegularHours(String beginDt, String endDt, String wareHse);
	 public List getBillingDiscount(String shipNumber, String sessionCorpID);
	 
	 public List getCompanyHolidays(String sessionCorpID, String workDate);
	 
	 public List findDistributionCode(String actionCode, String sessionCorpID);
	 
	 public void updateRefmasterStopDate(String beginDt, String sessionCorpID, String wareHse);
	 
	 public List checkStopDate(String workDt, String sessionCorpID, String wareHse);
	 
	 public List getLockPayrollList(String sessionCorpID);
	 public List getcompanyDivision(String calcCode, String sessionCorpID);
	 public List getWarehouse(String id);
	 public List checkStopDateForCrew(Date date1, String sessionCorpID, String warehouse);
	 
	 public List checkCH(Long ticket, Date workdtTmp, String sessionCorpID);
	 
	 public List findTimeSheetsForNonUnionOTPost(String beginDt, String endDt, String wareHse, String sessionCorpID, String userName);
	 public List findTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName);
	 public List findOTTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName);
	 public List findWorkdate(String beginDt, String endDt, String wareHse2, String userName, String sessionCorpID2);
	 public List searchAvailableTrucks(String wareHse, String newWrkDt, String sessionCorpID,Boolean visibilityForCorpId);
	 public List findSecheDuledResource(String wareHse, String newWrkDt,String string, String tkt, String string2, String sessionCorpID);
	 public List findResourceTickets(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus);
	 public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID);
	 public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID);
	 public List searchAvailableResourceCrews(String wareHse, String newWrkDt, String sessionCorpID, String showAbsent,String defaultSortValueFlag);
	 public List getCountFromTimeSheet(String ticket, String date11, String wareHse,	String sessionCorpID);
	 public List findSecheDuledResourceOperation(String wareHse,String newWrkDt, String string, String tkt, String string2,String sessionCorpID);
	 public List getCountForTruckOpsId(String ticket, String date11,String wareHse, String sessionCorpID, Long truckOpsId);
	 public List getCountForTimeSheetId(String ticket, String date11,String wareHse, String sessionCorpID, Long timeSheetId);
	 public void updateWorkTicketCrew(Long ticket, String updatedBy,String sessionCorpID);
	 public Integer selectedWorkTicketSize(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus);
	 public Integer selectedAvailableCrewSize(String wareHse, String newWrkDt,String sessionCorpID);
	 public Integer selectedAvailableTrucksSize(String truckWareHse,String newWrkDt, String sessionCorpID);
	 public List findNonSelectedResourceTickets(String tktWareHse,String newWrkDt, String sessionCorpID, String workticketService, String workTicketStatus);
	 public void updateDriver(String driverId, Boolean driverChecked);
	 public List findResourceItemsList(String tktWareHse, String newWrkDt, String sessionCorpID);
	 public List findToolTipResourceDetail(String sessionCorpID,String ticket, String type);
	 public List findPreviewOverTimeList(String beginDt, String endDt, String wareHse);
	 public List findTimeSheetsForOTPreview(String beginDt, String endDt, String wareHse, String sessionCorpID);
	 public List findScheduleResourceSeparateCrewList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber);
	 public List findScheduleResourceSeparateTruckList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber);
	 public void updateAssignedCrewForTruck(String updateQuery);
	 public Map<Map, Map> findSecheDuledResourceMap(String wareHse, String newWrkDt,String sessionCorpID,String whereClauseSecheDuledResourceMap,String whereClauseSecheDuledResourceMap1);
	 public List findDistinctFieldValue(String wareHse,String newWrkDt,String fieldName,String sessionCorpID);
	 public Boolean getPlannedMove(String code,String parameter,String corpId);
	 public List findCrewResourceDetail(String sessionCorpID,String ticket);
	 public List findVehicleResourceDetail(String sessionCorpID,String ticket);
	 public List findEquipmentResourceDetail(String sessionCorpID,String ticket);
	 public List findMaterialResourceDetail(String sessionCorpID,String ticket);
	 public String findIssuedDriverCheck(String sessionCorpID,Long ticket);
}
