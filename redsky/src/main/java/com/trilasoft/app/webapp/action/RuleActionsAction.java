package com.trilasoft.app.webapp.action;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ListCellRenderer;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;
import org.bouncycastle.asn1.ocsp.Request;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.ToDoRule;

import com.trilasoft.app.model.RuleActions;
import com.trilasoft.app.service.CheckListManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RuleActionsManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.ToDoRuleManager;




public class RuleActionsAction extends BaseAction implements Preparable  {
	
    private String sessionCorpID;
    private RuleActions rule;
    
	public RuleActions getRule() {
		return rule;
	}

	public void setRule(RuleActions rule) {
		this.rule = rule;
	}
	private List ls; 
    private List nls;    
    private Long tdid;
    private Long id1;
    private Long soId;
	private Set toDoRules;	
	private Long userId;
	private String userFirstName;
	private String userType;
	private String tempUserForPage;
	private Map<String,String> coord=new LinkedHashMap<String,String>();
	private RefMasterManager refMasterManager;
	private Map<String,String> docsList;
	
	private List filterCheck;
	private RuleActionsManager ruleActionsManager;
	
	private String ownerName;
	private CompanyManager companyManager;
	private Company company;
	private String chkAutomaticLink ;
	private String chkRulesRunning;
	private String oiJobList;
	private ToDoRuleManager  toDoRuleManager;
	private ToDoRule toDoRule;
	public Long getTdid() {
		return tdid;
	}

	public void setTdid(Long tdid) {
		this.tdid = tdid;
	}
	private boolean flagActivityMgmtVersion2=false;
	private SystemDefault systemDefault;
	private SystemDefaultManager systemDefaultManager;
	private String wUnit;
	private String vUnit;
	private static List<DataCatalog> selectedDateField;
	 
	public static List<DataCatalog> getSelectedDateField() {
		return selectedDateField;
	}

	public static void setSelectedDateField(List<DataCatalog> selectedDateField) {
		RuleActionsAction.selectedDateField = selectedDateField;
	}
	static final Logger logger = Logger.getLogger(ToDoRuleAction.class);
	
	
	
	public void prepare() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        this.userId=user.getId();
        this.userFirstName=user.getUsername();
        this.userType=user.getUserType();
        tempUserForPage = userFirstName.toUpperCase();
       
        coord = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
      //  sale = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
      //  billingList = refMasterManager.findUser(sessionCorpID,"ROLE_BILLING");
        docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
        if(userType.equalsIgnoreCase("USER")){
        filterCheck=ruleActionsManager.getFilterName(sessionCorpID,userId);
        if( filterCheck!=null && !filterCheck.isEmpty()){
            ownerName=user.getUsername();
   		}
        }
        company= companyManager.findByCorpID(sessionCorpID).get(0);
        if(company.getAutomaticLinkup()!= null && company.getAutomaticLinkup()== true){
             chkAutomaticLink= "Y";                                                   
        }else{
        	chkAutomaticLink= "N";
        }
        if(company.getRulesRunning()!= null && company.getRulesRunning()){
        	chkRulesRunning= "Y";
        }else{
        	chkRulesRunning= "N";
        }
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
		}
        flagActivityMgmtVersion2 = company.getActivityMgmtVersion2();
        
        systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
        if(systemDefault!=null){
        	wUnit= systemDefault.getWeightUnit();
        	vUnit = systemDefault.getVolumeUnit();
        }
        
   }
public String deleteRule()
{
	
	ruleActionsManager.remove(tdid);
	urlId = "?id="+ruleId;
	System.out.println(urlId);
return SUCCESS;

}
private String  activestatus;

private Long actionId;
public String  updateStatus()
{
	ruleActionsManager.updateStatus(activestatus,ruleId,actionId);
	urlId = "?id="+ruleId;
	System.out.println(urlId);

return SUCCESS;
}
	public RuleActionsAction(){
		
	}
	public List systemDefaults;
public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}

	
	
	
	
	
	@SkipValidation
	

	public String getComboList(String corpId){
		//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		pentity = refMasterManager.findByParameter(corpId, "PENTITY");
		prulerole = refMasterManager.findByParameter(corpId, "PRULEROLE");
		notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
		notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		return SUCCESS;
	 }

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTempUserForPage() {
		return tempUserForPage;
	}

	public void setTempUserForPage(String tempUserForPage) {
		this.tempUserForPage = tempUserForPage;
	}

	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public Map<String, String> getDocsList() {
		return docsList;
	}

	public void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	public List getFilterCheck() {
		return filterCheck;
	}

	public void setFilterCheck(List filterCheck) {
		this.filterCheck = filterCheck;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getChkAutomaticLink() {
		return chkAutomaticLink;
	}

	public void setChkAutomaticLink(String chkAutomaticLink) {
		this.chkAutomaticLink = chkAutomaticLink;
	}

	public String getChkRulesRunning() {
		return chkRulesRunning;
	}

	public void setChkRulesRunning(String chkRulesRunning) {
		this.chkRulesRunning = chkRulesRunning;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public boolean isFlagActivityMgmtVersion2() {
		return flagActivityMgmtVersion2;
	}

	public void setFlagActivityMgmtVersion2(boolean flagActivityMgmtVersion2) {
		this.flagActivityMgmtVersion2 = flagActivityMgmtVersion2;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public String getwUnit() {
		return wUnit;
	}

	public void setwUnit(String wUnit) {
		this.wUnit = wUnit;
	}

	public String getvUnit() {
		return vUnit;
	}

	public void setvUnit(String vUnit) {
		this.vUnit = vUnit;
	}

	public List getSystemDefaults() {
		return systemDefaults;
	}

	public void setSystemDefaults(List systemDefaults) {
		this.systemDefaults = systemDefaults;
	}

	public static Map<String, String> getNotetype() {
		return notetype;
	}

	public static void setNotetype(Map<String, String> notetype) {
		RuleActionsAction.notetype = notetype;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public List getToEmailList() {
		return toEmailList;
	}

	public void setToEmailList(List toEmailList) {
		this.toEmailList = toEmailList;
	}

	public String getBtn() {
		return btn;
	}

	public void setBtn(String btn) {
		this.btn = btn;
	}



	public Long getMaxId() {
		return maxId;
	}

	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}

	public void setToDoRules(Set toDoRules) {
		this.toDoRules = toDoRules;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public static void setNotesubtype(Map<String, String> notesubtype) {
		RuleActionsAction.notesubtype = notesubtype;
	}

	private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);			
		} catch (java.text.ParseException e) {			
			e.printStackTrace();
		}
		return currentDate;		
	} 
	private static Map<String, String> pentity;
	private static Map<String, String> notetype;
	private static Map<String, String> notesubtype;
	private String param;
	private List toEmailList;
	private static Map<String, String> prulerole;
	private static Map<String, String> taskUser;
	private String btn;
	private ToDoRule addCopyToDoRule;
	private String entity;
	private String urlId; 
	
	public String getUrlId() {
		return urlId;
	}

	public void setUrlId(String urlId) {
		this.urlId = urlId;
	}
	private List actionList;
	public List getActionList() {
		return actionList;
	}

	public void setActionList(List actionList) {
		this.actionList = actionList;
	}
private String messagedisplayed;
private String rolelist;
private String noteType;
private String noteSubType;
private Boolean status;
public Boolean getStatus() {
	return status;
}

public void setStatus(Boolean status) {
	this.status = status;
}
private String  manualEmail;
private String emailNotification;
	public String getMessagedisplayed() {
	return messagedisplayed;
}

public void setMessagedisplayed(String messagedisplayed) {
	this.messagedisplayed = messagedisplayed;
}

public String getRolelist() {
	return rolelist;
}

public void setRolelist(String rolelist) {
	this.rolelist = rolelist;
}

public String getNoteType() {
	return noteType;
}

public void setNoteType(String noteType) {
	this.noteType = noteType;
}

public String getNoteSubType() {
	return noteSubType;
}

public void setNoteSubType(String noteSubType) {
	this.noteSubType = noteSubType;
}


public String getManualEmail() {
	return manualEmail;
}

public void setManualEmail(String manualEmail) {
	this.manualEmail = manualEmail;
}

public String getEmailNotification() {
	return emailNotification;
}

public void setEmailNotification(String emailNotification) {
	this.emailNotification = emailNotification;
}
private ToDoRule addCopyToDoOLDRule;
private String hitFlag;
private static List selectedField;
private CheckListManager checkListManager;
private int countNumberOfResult;
	public String save() throws Exception {
		System.out.println("save methodddddd---------------");
//		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(btn.equalsIgnoreCase("Copy Rule")){
			System.out.println("fvbhfrbvkrjbvjk---------------");
			addCopyToDoRule=toDoRuleManager.getForOtherCorpid(toDoRule.getId());
			toDoRule=new ToDoRule();
			entity=addCopyToDoOLDRule.getEntitytablerequired();
			if(entity.equalsIgnoreCase("TrackingStatus")){		
				entity = "ServiceOrder";
			}
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);
			toDoRule.setCorpID(addCopyToDoOLDRule.getCorpID());
			toDoRule.setCorpID(addCopyToDoOLDRule.getCorpID());
			toDoRule.setRolelist(addCopyToDoOLDRule.getRolelist());
			toDoRule.setCreatedOn(new Date());
			toDoRule.setSystemDate(new Date());		
			toDoRule.setUpdatedOn(new Date());
			toDoRule.setCreatedBy(getRequest().getRemoteUser());
			toDoRule.setUpdatedBy(getRequest().getRemoteUser());
			toDoRule.setEntitytablerequired(addCopyToDoOLDRule.getEntitytablerequired());
			toDoRule.setNoteType(addCopyToDoOLDRule.getNoteType());
			toDoRule.setNoteSubType(addCopyToDoOLDRule.getNoteSubType());
			toDoRule.setFieldToValidate1(addCopyToDoOLDRule.getFieldToValidate1());
			toDoRule.setFieldDisplay(addCopyToDoOLDRule.getFieldDisplay());
			toDoRule.setTestdate(addCopyToDoOLDRule.getTestdate());
			toDoRule.setMessagedisplayed(addCopyToDoOLDRule.getMessagedisplayed());
			toDoRule.setDurationAddSub(addCopyToDoOLDRule.getDurationAddSub());
			toDoRule.setExpression(addCopyToDoOLDRule.getExpression());
			toDoRule.setStatus(addCopyToDoOLDRule.getStatus());
			toDoRule.setUrl(addCopyToDoOLDRule.getUrl());
			toDoRule.setFieldToValidate2(addCopyToDoOLDRule.getFieldToValidate2());
			toDoRule.setPublishRule(addCopyToDoOLDRule.getPublishRule());
			//toDoRule.setFieldToValidate3(addCopyToDoRule.getFieldToValidate3());			
			List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
			Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
			toDoRule.setRuleNumber(autoRuleNumber);
			toDoRule=toDoRuleManager.save(toDoRule);
			urlId = "?id="+ruleId;
			return SUCCESS;   
		}else{
			System.out.println("else part ------");
			boolean isNew = (toDoRule.getId() == null); 	
			if(isNew){
				toDoRule.setCreatedOn(new Date());
				List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
				if (maxRuleNumber.get(0) == null) {
					toDoRule.setRuleNumber(01L);
				} else {
					Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
					toDoRule.setRuleNumber(autoRuleNumber);
				}
		    } 
        Date todayDate = getCurrentDate();
		if((toDoRule.getEmailOn()==null) || ((todayDate.getTime() - toDoRule.getEmailOn().getTime())>(24 * 60 * 60 * 1000))){
			toDoRule.setEmailOn(new Date());
		}else{
			toDoRule.setEmailOn(toDoRule.getEmailOn());
		}
		toDoRule.setUpdatedOn(new Date());
		toDoRule.setUpdatedBy(getRequest().getRemoteUser());
		toDoRule.setSystemDate(new Date());	
		toDoRule.setCreatedBy(getRequest().getRemoteUser());
					
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		
		if((toDoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder")) && (toDoRule.getFieldToValidate1().substring(0,14).equalsIgnoreCase("trackingStatus"))){		
			toDoRule.setUrl("editTrackingStatus");
		}else if((toDoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder")) && ((toDoRule.getFieldToValidate1().substring(0,12).equalsIgnoreCase("serviceOrder")) || (toDoRule.getFieldToValidate1().substring(0,13).equalsIgnoreCase("miscellaneous")))){		
			toDoRule.setUrl("editServiceOrderUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile")){		
			toDoRule.setUrl("editCustomerFile");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("QuotationFile")){		
			toDoRule.setUrl("QuotationFileForm");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("ServicePartner")){		
			toDoRule.setUrl("editServicePartner");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Vehicle")){		
			toDoRule.setUrl("editVehicle");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLine")){		
			toDoRule.setUrl("editAccountLine");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){		
			toDoRule.setUrl("editWorkTicketUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLineList")){		
			toDoRule.setUrl("accountLineList");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Billing")){		
			toDoRule.setUrl("editBilling");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Miscellaneous")){		
			toDoRule.setUrl("editMiscellaneous");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("DspDetails")){			
			toDoRule.setUrl("editDspDetails");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Claim")){			
			toDoRule.setUrl("editClaim");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus")){		
			toDoRule.setUrl("editTrackingStatus");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){		
			toDoRule.setUrl("editLeadCapture");
		}
		else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("CreditCard")){		
			toDoRule.setUrl("editBilling");
		}
		else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicketList")){		
			toDoRule.setUrl("customerWorkTickets");
		}
		if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){
			if(!toDoRule.getExpression().contains("customerfile.controlFlag = 'L'")){							
				toDoRule.setExpression(toDoRule.getExpression()+" and customerfile.controlFlag = 'L'");
			}
		}
		System.out.println("dscgdjcjsd----"+toDoRule);
		toDoRule= toDoRuleManager.save(toDoRule);
		System.out.println("dscgdjcjsd----"+toDoRule);
		String executeRules=toDoRuleManager.executeTestRule(toDoRule, sessionCorpID);	
		
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		else
			toDoRule.setCorpID(sessionCorpID);
		
		entity=toDoRule.getEntitytablerequired();
		if(entity.equalsIgnoreCase("TrackingStatus")){		
			entity = "ServiceOrder";
		}
		selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
		selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
		//getComboList(sessionCorpID);
		if(btn.equalsIgnoreCase("Test Condition")){
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				saveMessage("Rule has been executed successfully.");			
			}else{
				String sqlError = "";
				try{
					sqlError = executeRules;
					StringBuffer buffer = new StringBuffer();
					buffer.append(executeRules);
					buffer.append(" ");
					sqlError = buffer.toString();
				}catch(Exception ex){ }
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Rule cannot be executed. "+ sqlError);
			}
		}else if (btn.equalsIgnoreCase("Execute This Rule")){
			List executeThisRule= toDoRuleManager.executeThisRule(toDoRule, sessionCorpID,getRequest().getRemoteUser());
			if(executeThisRule == null ){
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}else{
			toDoRule.setStatus("Tested");
			toDoRuleManager.changestatus("Tested",toDoRule.getId());
			if(toDoRule.getDocType()!=null && !toDoRule.getDocType().equals("")){
			//createCheckResultRules(toDoRule.getRuleNumber(),toDoRule.getId());
				checkListManager.updateAndDel(sessionCorpID,toDoRule.getRuleNumber(),toDoRule.getId());
			int checkListEnterd = toDoRuleManager.findCheckListEnterd(sessionCorpID, toDoRule.getId());
				saveMessage("Rule has been executed successfully.  "   + checkListEnterd + " Record Entered");
			}else{
				toDoRuleManager.deleteUpdateByRuleNumber(toDoRule.getRuleNumber(),toDoRule.getCorpID());
				List numberOfResult=toDoRuleManager.findNumberOfResultEntered(sessionCorpID, toDoRule.getId(),toDoRule.getRolelist());
				countNumberOfResult=Integer.parseInt(numberOfResult.get(0).toString());
				saveMessage("Rule has been executed successfully.  "   + countNumberOfResult + " Record Entered");
			}
			}
		}else{
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				
				String key = (isNew) ? "Rule.added" : "Rule.updated";
				saveMessage(getText(key));
				
			}else{
				toDoRule.setCreatedOn(new Date());
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}	
		}}			
		if(ruleId!=null)
		{
		urlId = "?id="+ruleId;
		}
		
		else
		{
			urlId = "?id="+toDoRule.getId();
		}
	    return SUCCESS;

		
	}
	
	
	public String saveruleAction() throws Exception {

	System.out.println("sae start");
	System.out.println("Process Date : "+processDateInput);
	//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
	notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
	notesubtype = refMasterManager.findByParameter(sessionCorpID, "NOTESUBTYPE");
	toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
	prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
	taskUser = refMasterManager.findUserNotes(sessionCorpID, "PRULEROLE");
	System.out.println("sae start");
	System.out.println("rule id 1111 start"+ruleId);
	System.out.println("rule iod 2start"+ruleId);
	toDoRule = toDoRuleManager.get(ruleId);
	if(tdid!=null)
	  {
	  rule.setRuleId(ruleId);
	  rule.setUpdatedOn(new Date());
	  }
	
	else
	{  
		rule.setCreatedOn(new Date());
		rule.setStatus(true);
		rule.setRuleId(ruleId);
		rule.setUpdatedOn(new Date());
	}
	 
	    if(toDoRule.getPublishRule())
	    	rule.setCorpID("TSFT");
		else
			rule.setCorpID(sessionCorpID);
	    rule.setUpdatedBy(getRequest().getRemoteUser());
	    rule.setCreatedBy(getRequest().getRemoteUser());
	    ruleActionsManager.save(rule);
	    ruleActionsManager.updateToDoRole(rule.getMessagedisplayed(), rule.getRolelist(),ruleId,sessionCorpID);
        urlId = "?id="+ruleId;
        return SUCCESS;
}
	
	private String emailNotifi;
	 private String processDateInput=null;
	
	public String getProcessDateInput() {
		return processDateInput;
	}

	public void setProcessDateInput(String processDateInput) {
		this.processDateInput = processDateInput;
	}
	private Long maxId;
	private Long ruleId;

	
	
	public Long getRuleId() {
		return ruleId;
	}

	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}
	private String label;
	
	@SkipValidation
	public String edit() { 
	
		pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
		notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
		notesubtype = refMasterManager.findByParameter(sessionCorpID, "NOTESUBTYPE");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
		taskUser = refMasterManager.findUserNotes(sessionCorpID, "PRULEROLE");
		 
    	try {
			getComboList(sessionCorpID);
			toDoRule=toDoRuleManager.getForOtherCorpid(ruleId);
			entity=toDoRule.getEntitytablerequired();
			
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
		
			//toDoRule.setUpdatedOn(new Date());
			entity=toDoRule.getEntitytablerequired();
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
		
			if(tdid!=null)
			  {
			  rule= ruleActionsManager.get(tdid);
			  }
			else
			{
				rule=new RuleActions();
				rule.setRuleId(ruleId);
					//getComboList(sessionCorpID);
				rule.setRolelist("Coordinator");
				rule.setCreatedOn(new Date());
				rule.setUpdatedOn(new Date());
				rule.setEmailNotification(rule.getEmailNotification());
				rule.setStatus(status);
				rule.setCreatedBy(getRequest().getRemoteUser());
				rule.setUpdatedBy(getRequest().getRemoteUser());
				rule.setCorpID(sessionCorpID);
			}
		
		 }catch (NumberFormatException e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		  	 // errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		  	 e.printStackTrace();
		  	 return CANCEL;
		}  
      
        return SUCCESS;   
	
	}
	
	

	@SkipValidation
	
	public Set getToDoRules() {
		return toDoRules;
	}	
	public Map<String, String> getPentity() {
		return pentity;
	}
	public void setPentity(Map<String, String> pentity) {
		this.pentity = pentity;
	}
	
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	
	public Map<String, String> getPrulerole() {
		return prulerole;
	}
	public void setPrulerole(Map<String, String> prulerole) {
		this.prulerole = prulerole;
	}
	public List getNls() {
		return nls;
	}
	public void setNls(List nls) {
		this.nls = nls;
	}
	
	public Long getId1() {
		return id1;
	}
	public void setId1(Long id1) {
		this.id1 = id1;
	}
	
	

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	
	public static Map<String, String> getNotesubtype() {
		return notesubtype;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public ToDoRule getToDoRule() {
		return toDoRule;
	}

	public void setToDoRule(ToDoRule toDoRule) {
		this.toDoRule = toDoRule;
	}

	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	public static List getSelectedField() {
		return selectedField;
	}

	public static void setSelectedField(List selectedField) {
		RuleActionsAction.selectedField = selectedField;
	}

	public void setCheckListManager(CheckListManager checkListManager) {
		this.checkListManager = checkListManager;
	}

	public int getCountNumberOfResult() {
		return countNumberOfResult;
	}

	public void setCountNumberOfResult(int countNumberOfResult) {
		this.countNumberOfResult = countNumberOfResult;
	}

	public ToDoRule getAddCopyToDoRule() {
		return addCopyToDoRule;
	}

	public void setAddCopyToDoRule(ToDoRule addCopyToDoRule) {
		this.addCopyToDoRule = addCopyToDoRule;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getEmailNotifi() {
		return emailNotifi;
	}

	public void setEmailNotifi(String emailNotifi) {
		this.emailNotifi = emailNotifi;
	}

	
	public void setRuleActionsManager(RuleActionsManager ruleActionsManager) {
		this.ruleActionsManager = ruleActionsManager;
	}
	public String getActivestatus() {
		return activestatus;
	}

	public void setActivestatus(String activestatus) {
		this.activestatus = activestatus;
	}

	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}
	
	/**
	 * @return the taskUser
	 */
	public Map<String, String> getTaskUser() {
		return taskUser;
	}

	/**
	 * @param taskUser the taskUser to set
	 */
	public void setTaskUser(Map<String, String> taskUser) {
		this.taskUser = taskUser;
	}
}