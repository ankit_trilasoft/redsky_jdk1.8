ALTER TABLE `redsky`.`billing` ADD COLUMN `originAgentVatCode` VARCHAR(30) AFTER `privatePartyVatCode`, ADD COLUMN `destinationAgentVatCode` VARCHAR(30) AFTER `originAgentVatCode`, ADD COLUMN `originSubAgentVatCode` VARCHAR(30) AFTER `destinationAgentVatCode`, ADD COLUMN `destinationSubAgentVatCode` VARCHAR(30) AFTER `originSubAgentVatCode`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `forwarderVatCode` VARCHAR(30) AFTER `destinationSubAgentVatCode`, ADD COLUMN `brokerVatCode` VARCHAR(30) AFTER `forwarderVatCode`, ADD COLUMN `networkPartnerVatCode` VARCHAR(30) AFTER `brokerVatCode`, ADD COLUMN `bookingAgentVatCode` VARCHAR(30) AFTER `networkPartnerVatCode`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `vendorCodeVatCode` VARCHAR(30) AFTER `bookingAgentVatCode`;

insert into refmaster (corpid, code, description, fieldlength,parameter,createdon,createdby,updatedon,updatedby,language) values ('SSCW', 'PLANA', 'PlanA', 10, 'COMMISSIONPLAN',now(),'subrat',now(),'subrat','en'),('SSCW', 'PLANB', 'PlanB', 10, 'COMMISSIONPLAN',now(),'subrat',now(),'subrat','en'),('SSCW', 'PLANC', 'PLANC', 10, 'COMMISSIONPLAN',now(),'subrat',now(),'subrat','en');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('SSCW','COMMISSIONPLAN',10,true,now(),'subrat',now(),'subrat',1,1,'');

ALTER TABLE `redsky`.`app_user` ADD COLUMN `companyCode` VARCHAR(10) AFTER `networkCoordinator`;

ALTER TABLE `redsky`.`app_user` CHANGE COLUMN `companyCode` `companyDivision` VARCHAR(10)  DEFAULT NULL;

ALTER TABLE `redsky`.`billing` ADD COLUMN `payableRate` DECIMAL(19,2) DEFAULT NULL AFTER `vendorCodeVatCode`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type) values('Billing','payableRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),('Billing','payableRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`reports` MODIFY COLUMN `targetCabinet` VARCHAR(20)  DEFAULT NULL;

ALTER TABLE `redsky`.`reports` ADD COLUMN `autoupload` BIT(1) DEFAULT b'0' AFTER `extractTheme`; 

update charges set expensePrice='' where expensePrice is null;

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `bankAddress` VARCHAR(150) DEFAULT '' ;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('Billing','vendorCode' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','vendorCode' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','vendorName' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','vendorName' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceCharge' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceCharge' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceOptionCodeWithDesc' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceValueEntitle' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insurancePerMonth' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insurancePerMonth' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatDescr' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatDescr' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatPercent' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceVatPercent' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','certnum' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','issueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','currency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceValueActual' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','totalInsrVal' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','baseInsuranceValue' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceBuyRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','insuranceBuyRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','baseInsuranceTotal' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','baseInsuranceTotal' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','exchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','exchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','valueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM') ;
