<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
   <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['serviceOrderListForm'].elements['serviceOrder.shipNumber'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.firstName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.lastName'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.status'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.statusDate'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.job'].value = "";
	document.forms['serviceOrderListForm'].elements['serviceOrder.coordinator'].value = "";
}
</script>
</head>   
  
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editServiceOrder.html?id=${customerFile.id}"/>'"
         value="<fmt:message key="button.add"/>"/>   
</c:set>   
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>  
     
<s:form id="serviceOrderListForm" action="searchServiceOrders" method="post" > 
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>  

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<table class="table" style="width:800px"  >
<thead>
<tr>

<th><fmt:message key="serviceOrder.shipNumber"/></th>
<th><fmt:message key="serviceOrder.firstName"/></th>
<th><fmt:message key="serviceOrder.lastName"/></th>
<th><fmt:message key="serviceOrder.status"/></th>
<th><fmt:message key="serviceOrder.statusDate"/></th>
<th><fmt:message key="serviceOrder.job"/></th>
<th><fmt:message key="serviceOrder.coordinator"/></th>

<th style="background:none;border:none;"></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="serviceOrder.shipNumber" size="9" required="true" cssClass="text medium" onfocus="onFormLoad();" />
			</td>
			<td>
			    <s:textfield name="serviceOrder.firstName" size="9" required="true" cssClass="text medium"/>
			</td>
			
			<td>
			    <s:textfield name="serviceOrder.lastName" size="9" required="true" cssClass="text medium" />
			</td>
			<td>
			    <s:textfield name="serviceOrder.status" size="9" required="true" cssClass="text medium"/>
			</td>
			<c:if test="${not empty serviceOrder.statusDate}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.statusDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate" value="%{customerFileMoveDateFormattedValue}" readonly="true" size="7" maxlength="11" />
				<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />

			</c:if>
			<c:if test="${empty serviceOrder.statusDate}">
			<td><s:textfield cssClass="input-text" id="statusDate" name="serviceOrder.statusDate"  size="7" maxlength="11" readonly="true"/>
				<img id="statusDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
			</td>
			</c:if>
			<td>
			    <s:textfield name="serviceOrder.job" size="2" required="true" cssClass="text medium"/>
			</td>
			<td width="20" align="left">
				<c:if test="${coordinatr == null || coordinatr == ''}" >
			    	<s:textfield name="serviceOrder.coordinator" required="true" cssClass="input-text" size="7"/>
			    </c:if>
			    <c:if test="${coordinatr != '' && coordinatr != null}" >
			    	<s:textfield name="serviceOrder.coordinator" required="true" cssClass="input-text" size="7" value="${coordinatr}"/>
			    </c:if>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
		</tr>
		</tbody>
	</table>


<c:out value="${searchresults}" escapeXml="false" /> 

<div style=" width:800px; height:250px;overflow-x:auto; overflow-y:auto;">
<s:set name="serviceOrders" value="serviceOrders" scope="request"/>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Service Order List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>


<display:table name="serviceOrders" class="table" requestURI="" id="serviceOrderList" export="true" defaultsort="1" pagesize="10" style="width:1150px">
<c:if test="${serviceOrder.job =='RLO'}">    
<display:column property="shipNumber" sortable="true" titleKey="serviceOrder.shipNumber" href="editDspDetails.html"    
        paramId="id" paramProperty="id" style="width:75px"/>
        </c:if>
<c:if test="${serviceOrder.job !='RLO'}">    
<display:column property="shipNumber" sortable="true" titleKey="serviceOrder.shipNumber" href="editTrackingStatus.html"    
        paramId="id" paramProperty="id" style="width:75px"/>
        </c:if>
 <display:column property="registrationNumber" sortable="true" titleKey="serviceOrder.registrationNumber" style="width:50px"/>  

<display:column property="firstName" sortable="true" titleKey="serviceOrder.firstName" style="width:75px" />
<display:column property="lastName" sortable="true" titleKey="serviceOrder.lastName" style="width:75px" />
<display:column property="status" sortable="true" titleKey="serviceOrder.status" style="width:20px" />
<display:column property="statusDate" sortable="true" titleKey="serviceOrder.statusDate" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
<display:column property="job" sortable="true" titleKey="serviceOrder.job" style="width:50px"/>  
<display:column property="routing" sortable="true" titleKey="serviceOrder.routing" style="width:20px"/>  
<display:column property="commodity" sortable="true" titleKey="serviceOrder.commodity" style="width:20px"/>  
<display:column property="billToName" sortable="true" titleKey="serviceOrder.billToCode" style="width:110px"/>
<display:column property="mode" sortable="true" titleKey="serviceOrder.mode" style="width:20px"/>   
<display:column property="originCountry" sortable="true" titleKey="serviceOrder.originCountry" style="width:20px"/>  
<display:column property="destinationCountry" sortable="true" titleKey="serviceOrder.destinationCountry" style="width:20px"/>  
<display:column property="coordinator" sortable="true" titleKey="serviceOrder.coordinator" style="width:20px"/>  
<display:column property="createdOn" sortable="true" titleKey="serviceOrder.createdOn" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  




<display:setProperty name="paging.banner.items_name" value="serviceOrder"/>


 <display:setProperty name="paging.banner.item_name" value="serviceorder"/>   
    <display:setProperty name="paging.banner.items_name" value="orders"/>   
  
    <display:setProperty name="export.excel.filename" value="ServiceOrder List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ServiceOrder List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ServiceOrder List.pdf"/>  
</display:table>
</div>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="customerFile.id" />
<c:if test="${not empty customerFile.sequenceNumber}">
<c:out value="${buttons}" escapeXml="false" />
</c:if>
<c:if test="${empty customerFile.sequenceNumber}">
<c:set var="isTrue" value="false" scope="application"/>
</c:if>
 
 </s:form>
<script type="text/javascript">   
highlightTableRows("serviceOrderList");
Form.focusFirstElement($("serviceOrderListForm"));    
</script>   
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
