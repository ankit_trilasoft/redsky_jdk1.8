package com.trilasoft.app.service;


import java.util.List; 
import org.appfuse.service.GenericManager;   
import com.trilasoft.app.model.RefJobDocumentType;   

public interface RefJobDocumentTypeManager extends GenericManager<RefJobDocumentType, Long> {   
	public List findRefJobDocumentList(String corpID); 
	public List searchRefJobDocument(String jobType,String corpID);
	 public String getAllDoument(String jobType,String corpID);
}  