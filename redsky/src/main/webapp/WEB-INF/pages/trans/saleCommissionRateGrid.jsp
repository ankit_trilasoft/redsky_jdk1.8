<%@ include file="/common/taglibs.jsp"%>

<head>
<meta name="heading" content="<fmt:message key='rateGrid.heading'/>" />

<title><fmt:message key="rateGrid.title" />
</title>
</head>
<style type="text/css">
</style>
<script language="javascript" type="text/javascript">
	
</script>

<body>
	<s:form id="rateGrid" name="rateGrid" method="post">
		<s:hidden name="newlineid" id="newlineid" value="" />
		<s:hidden name="revenueListServer" id="revenueListServer" value="" />
		<s:hidden name="commListServer" id="commListServer" value="" />
		<s:hidden name="saleCommisionList" id="saleCommisionList" value="" />
		<s:hidden name="contract"></s:hidden>
		<s:hidden name="jobType"></s:hidden>
		<s:hidden name="companyDiv"></s:hidden>

		<div id="otabs" style="float: left; margin-bottom: 0px;">
			<ul>
				<li><a class="current"><span>Rate Grid</span></a></li>
			</ul>
		</div>
		<div style="padding-top: 2px;">
			<s:textfield name="commissionCon" readonly="true" value="${bucketMatchContract}" cssClass="input-textUpper" size="20" />
			<s:textfield name="commissionChargeCode" readonly="true" value="${bucketMatchChargeCode}" cssClass="input-textUpper" size="20" />
		</div>
		<div class="spn">&nbsp;&nbsp;&nbsp;&nbsp;</div>
		<div id="para1" style="clear: both;">
			<table class="table" id="dataTable" style="width: 100%;">
				<thead>
					<tr>
						<th width="50px">Retained Revenue %</th>
						<th width="70px">Flat %</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="individualItem" items="${rateGridList}"
						varStatus="rowCounter">
						<tr>
							<td><s:textfield cssClass="input-text" name="revenueList"
									value="${individualItem.quantity1}"
									id="rev${individualItem.id}"
									onchange="checkNumber('rev${individualItem.id}')"
									cssStyle="width:100px;text-align:right;" /> <c:if
									test="${rowCounter.count==1}">
									<c:out value="Min.Qty" />
								</c:if></td>
							<td><s:textfield cssClass="input-text" name="commList"
									value="${individualItem.rate1}" id="com${individualItem.id}"
									onchange="checkNumber('com${individualItem.id}')"
									cssStyle="width:100px;text-align:right;" />
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<c:if test="${not empty bucketMatchContract}">  
			<input type="button" class="cssbutton1" id="addLine" name="addLine" style="width: 70px; height: 25px;" value="Add Line" onclick="addRow('dataTable');" />
			<input type="button" class="cssbutton1" id="" name="" style="width: 70px; height: 25px;" value="Save Line" onclick="save();" />
		</c:if>
		<c:if test="${empty bucketMatchContract}">  
			<h3>Match not Found for the bucket: <b><%=request.getParameter("jobType")%></b> </h3>
		</c:if>
	</s:form>
	<script language="javascript" type="text/javascript">
		function checkNumber(id) {
			var numVal = document.getElementById(id).value;
			if (parseInt(numVal) == numVal) {
				return true;
			} else if (/^\d+\.\d+$/.test(numVal)) {
				return true;
			} else {
				alert('Enter valid Number');
				document.getElementById(id).value = '0.0';
				return false;
			}
			return true;
		}
		function save() {
			var revenueListServer = "";
			var commListServer = "";

			var id = '';

			if (document.forms['rateGrid'].revenueList != undefined) {
				if (document.forms['rateGrid'].revenueList.length != undefined) {
					for (i = 0; i < document.forms['rateGrid'].revenueList.length; i++) {
						id = document.forms['rateGrid'].revenueList[i].id;
						id = id.replace(id.substring(0, 3), '');
						if (revenueListServer == '') {
							revenueListServer = id
									+ ": "
									+ document.forms['rateGrid'].revenueList[i].value;
						} else {
							revenueListServer = revenueListServer+ "~"+ id+ ": "+ document.forms['rateGrid'].revenueList[i].value;
						}
					}
				} else {
					id = document.forms['rateGrid'].revenueList.id;
					id = id.replace(id.substring(0, 3), '');
					revenueListServer = id + ": "+ document.forms['rateGrid'].revenueList.value;
				}
			}
			if (document.forms['rateGrid'].commList != undefined) {
				if (document.forms['rateGrid'].commList.length != undefined) {
					for (i = 0; i < document.forms['rateGrid'].commList.length; i++) {
						id = document.forms['rateGrid'].commList[i].id;
						id = id.replace(id.substring(0, 3), '');
						if (commListServer == '') {
							commListServer = id+ ": "+ document.forms['rateGrid'].commList[i].value;
						} else {
							commListServer = commListServer+ "~"+ id+ ": "+ document.forms['rateGrid'].commList[i].value;
						}
					}
				} else {
					id = document.forms['rateGrid'].commList.id;
					id = id.replace(id.substring(0, 3), '');
					commListServer = id + ": "+ document.forms['rateGrid'].commList.value;
				}
			}
			document.getElementById('revenueListServer').value = revenueListServer;
			document.getElementById('commListServer').value = commListServer;

			var saleCommisionListServer = "";
			var newlineid = document.getElementById('newlineid').value;
			if (newlineid != '') {
				var arrayLine = newlineid.split("~");
				for ( var i = 0; i < arrayLine.length; i++) {
					if (saleCommisionListServer == '') {
						saleCommisionListServer = document.getElementById('rev'
								+ arrayLine[i]).value
								+ ":"
								+ document.getElementById('com' + arrayLine[i]).value;
					} else {
						saleCommisionListServer = saleCommisionListServer
								+ "~"
								+ document.getElementById('rev' + arrayLine[i]).value
								+ " :"
								+ document.getElementById('com' + arrayLine[i]).value;
					}
				}
			}
			document.getElementById('saleCommisionList').value = saleCommisionListServer;
			document.forms['rateGrid'].action = "saveSaleCommisionRateGrid.html?decorator=popup&popup=true";
			document.forms['rateGrid'].submit();
		}

		function addRow(tableID) {
			var table = document.getElementById(tableID);
			var rowCount = table.rows.length;
			var row = table.insertRow(rowCount);
			var newlineid = document.getElementById('newlineid').value;
			if (newlineid == '') {
				newlineid = rowCount;
			} else {
				newlineid = newlineid + "~" + rowCount;
			}
			document.getElementById('newlineid').value = newlineid;

			var cell1 = row.insertCell(0);
			var element1 = document.createElement("input");
			element1.type = "text";
			element1.setAttribute("class", "input-text");
			element1.id = 'rev' + rowCount;
			element1.setAttribute("onchange", "checkNumber(\"rev" + rowCount
					+ "\")");
			element1.setAttribute("style", "width:100px;text-align:right");
			cell1.appendChild(element1);

			var cell2 = row.insertCell(1);
			var element2 = document.createElement("input");
			element2.type = "text";
			element2.setAttribute("class", "input-text");
			element2.id = 'com' + rowCount;
			element2.setAttribute("onchange", "checkNumber(\"com" + rowCount
					+ "\")");
			element2.setAttribute("style", "width:100px;text-align:right");
			cell2.appendChild(element2);
		}
	</script>
</body>
