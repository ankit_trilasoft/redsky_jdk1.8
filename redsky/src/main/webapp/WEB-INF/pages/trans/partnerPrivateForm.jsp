<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Additional Info</title>
<meta name="heading" content="Additional Info" />
<script>

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var pcd = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;;
		var prId = '<%=request.getParameter("partnerId")%>';
		var url = 'deleteEntitlement.html?id='+encodeURI(targetElement)+'&partnerPrivateId='+prId+'&partnerCode='+pcd;
		location.href = url;
	 }else{
		return false;
	}
}

function confirmSubmitForAssignment(targetElement){
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
		var pcd = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;;
		var prId = '<%=request.getParameter("partnerId")%>';
		var url = 'deleteAccountAssignmentType.html?id='+encodeURI(targetElement)+'&partnerPrivateId='+prId+'&partnerCode='+pcd;
		location.href = url;
	 }else{
		return false;
	}
}
function notExists(){
	alert("The Account information has not been saved yet, please save account information to continue");
	
}

function openNotesPopup(targetElement){
	var id = document.forms['accountProfileForm'].elements['accountProfile.id'].value;
	var notesId = document.forms['accountProfileForm'].elements['accountProfile.partnerCode'].value;
	var noteSubType = document.forms['accountProfileForm'].elements['accountProfile.type'].value;
	var imgID = targetElement.id;
	var pId = document.forms['accountProfileForm'].elements['partner.id'].value;
	openWindow('accountNotes.html?id='+id+'&notesId='+notesId+'&accountNotesFor=AP&noteFor=AccountProfile&subType='+noteSubType+'&imageId='+imgID+'&pId='+pId+'&for=List&fieldId=countAccountProfileNotes&decorator=popup&popup=true',800,600);
	document.forms['accountProfileForm'].elements['formStatus'].value = '';
}

function openNotesPopupTab(targetElement){
	openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);
}
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script>

function checkdate(clickType){
var j;
	var elementLength = document.forms['accountProfileForm'].elements.length;
	for(j=0;j<=(elementLength-1);j++){
		document.forms['accountProfileForm'].elements[j].disabled = false;
	}
progressBarAutoSave('1');
	if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${'accountProfile.id'}"/>';
      		var id =document.forms['accountProfileForm'].elements['accountProfile.id'].value;
      		var id1 = document.forms['accountProfileForm'].elements['partner.id'].value;
			var partnerType = document.forms['accountProfileForm'].elements['partnerType'].value;
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
				      noSaveAction = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
		          		}
		      	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
						noSaveAction = 'accountContactList.html?id='+id1+'&partnerType= AC';
						}
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
						noSaveAction = 'editContractPolicy.html?id='+id1+'&partnerType= AC';
						}
				 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
				 		if('<%=session.getAttribute("paramView")%>' == 'View'){
							noSaveAction = 'searchPartnerView.html';
						}else{
							noSaveAction = 'searchPartnerAdmin.html?partnerType=AC';
						}
					}           
    processAutoSave(document.forms['accountProfileForm'], 'saveAccountProfile!saveOnTabChange.html', noSaveAction);
	document.forms['accountProfileForm'].action = 'saveAccountProfile!saveOnTabChange.html';
	} else {
	
   if(!(clickType == 'save'))   {
     var id1 =document.forms['accountProfileForm'].elements['partner.id'].value;
     if (document.forms['accountProfileForm'].elements['formStatus'].value == '1')
     {
       var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='billingDetail.heading'/>");
       if(agree)
        {
           document.forms['accountProfileForm'].action ='saveAccountProfile!saveOnTabChange.html';
           document.forms['accountProfileForm'].submit();
        }  else {
         if(id1 != '')  {
             if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
		      			location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
           				}
		     if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
						location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
						}
			 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
						location.href = 'editContractPolicy.html?id='+id1+'&partnerType=AC';
						}
			 if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
			 			if('<%=session.getAttribute("paramView")%>' == 'View'){
							location.href = 'searchPartnerView.html';
						}else{
							location.href = 'searchPartnerAdmin.html?partnerType=AC';
						}
						
		     } 
         }
       }
    }   else   {
     if(id1 != '')  {
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountdetail'){
			      location.href = 'editPartnerAddForm.html?id='+id1+'&partnerType=AC';
	           		}
	        if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountcontact'){
					location.href = 'accountContactList.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.contractpolicy'){
					location.href= 'editContractPolicy.html?id='+id1+'&partnerType=AC';
					}
		 	if(document.forms['accountProfileForm'].elements['gotoPageString'].value == 'gototab.accountlist'){
					if('<%=session.getAttribute("paramView")%>' == 'View'){
						location.href = 'searchPartnerView.html';
					}else{
						location.href = 'searchPartnerAdmin.html?partnerType=AC';
					}
			} 
      }
  }
  }
  }
}



function changeStatus(){
	document.forms['accountProfileForm'].elements['formStatus'].value = '1';
}

function addDate(dateObject, numDays) {

dateObject.setDate(dateObject.getDate() + numDays);

return dateObject;

}

function enableDisable(targetElement)
{
var el = document.getElementById('coordRequiredTrue');
var el1 = document.getElementById('coordRequiredFalse');
var el11 = document.getElementById('coordRequiredTrue1');
var el111 = document.getElementById('coordRequiredFalse1');
var el22 = document.getElementById('coordRequiredTrue2');
var el222 = document.getElementById('coordRequiredFalse2');
var el333 = document.getElementById('coordRequiredTrue12');
var el444 = document.getElementById('coordRequiredFalse12');
if(targetElement.checked == true){
		el.style.display = 'block';		
		el1.style.display = 'none';	
		
		el11.style.display = 'block';		
		el111.style.display = 'none';
		
		el22.style.display = 'block';		
		el222.style.display = 'none';

		el333.style.display = 'block';		
		el444.style.display = 'none';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=false;
		}catch(e){}
		//document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value='${partnerPrivate.creditDateCheck}';
		var checkdate='${partnerPrivate.creditDateCheck}';
		var mySplitResult= checkdate.split(" ");
	   	var day = mySplitResult[2];
	   	var month = mySplitResult[1];
	   	var year = mySplitResult[5];
	   	var datam = day+"-"+month+"-"+year.substring(2,4);
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value=datam;
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditCurrency'].value='${partnerPrivate.creditCurrency}';
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value='${partnerPrivate.creditAmount}';
	   	document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value='${partnerPrivate.availableCredit}';
	  	}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		
		el11.style.display = 'none';
		el111.style.display = 'block';
		
		el22.style.display = 'none';
		el222.style.display = 'block';

		el333.style.display = 'none';		
		el444.style.display = 'block';
		
		document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.creditCurrency'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value='';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=true;
	  	}catch(e){}
		}
}
function regExMatch(element,evt){
	if(!element.readOnly){
		var key='';
		if (window.event)
			 key = window.event.keyCode;
		if (evt)
			  key = evt.which;
		var alphaExp = /^[a-zA-Z0-9-_\.'&'\\'&'\/'&'\"'&'\+'&'\,'&'\''\#'&'\$'&'\~'&'\!'&'\@'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\('&'\)'&'\|'&'\['&'\]'&'\`'&'\='\' ']+$/;
		if(element.value.match(alphaExp)){
			return true;
		}
		else if ((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27)||(key==20)||(key==16)||(key==17)||(key==32)||(key==18)||(key==27)||(key==35)||(key==36)||(key==45)||(key==39)||(key==37)||(key==38)||(key==40)||(key==116)){
			   return true;
		}
		else{
			return true;
		}
	}else{
		return false;
	}
}

function creditDate(targetElement) {
var mdate=new Date();
var creaditCheckDate = document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value;
var creaditCheckFlag = document.forms['accountProfileForm'].elements['partnerPrivate.creditCheck'].checked;
if(creaditCheckDate==''){
var mdate=new Date();	    
mydate = addDate(mdate, 180);	    
}
else{
var mdate11=creaditCheckDate;
           mdate11 = mdate11.split("-");
            var day = mdate11[0];
		   	var month = mdate11[1];
		   	var year = mdate11[2];		   	
		 	if(month == 'Jan'){
		       month = "01";
		   	}else if(month == 'Feb'){
		       month = "02";
		   	}else if(month == 'Mar'){
		       month = "03"
		   	}else if(month == 'Apr'){
		       month = "04"
		   	}else if(month == 'May'){
		       month = "05"
		   	}else if(month == 'Jun'){
		       month = "06"
		   	}else if(month == 'Jul'){
		       month = "07"
		   	}else if(month == 'Aug'){
		       month = "08"
		   	}else if(month == 'Sep'){
		       month = "09"
		   	}else if(month == 'Oct'){
		       month = "10"
		   	}else if(month == 'Nov'){
		       month = "11"
		   	}else if(month == 'Dec'){
		       month = "12";
		   	}	
		   	if(year =='10'){
		   	year="2010"
		   	}	 	
	    mdate11=month+"/"+day+"/"+year;		 
	    mdate11= new Date(mdate11);
	    mdate11 = addDate(mdate11, 180);
	    

}
if(creaditCheckDate==''){
var mydate=new Date(mdate);
}else{
var mydate=new Date(mdate11);
}

		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
	    var daym=mydate.getDate();
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		if(targetElement.checked == true || (targetElement == true && creaditCheckFlag == true)){		
		document.getElementById("creditExpiry").value=datam;
		document.forms['accountProfileForm'].elements['checkCreditExpiryClick'].value = '';
	}else{
		document.getElementById("creditExpiry").value='';
	}

}
function insuranceValue(){
	<c:if test="${partnerType == 'AC'}">
	var lumpSum=document.forms['accountProfileForm'].elements['partnerPrivate.lumpSum'];
	var lumpSumAmount=document.forms['accountProfileForm'].elements['partnerPrivate.lumpSumAmount'].value
	var lumpPerUnit=document.forms['accountProfileForm'].elements['partnerPrivate.lumpPerUnit'].value
	var minReplace=document.forms['accountProfileForm'].elements['partnerPrivate.minReplacementvalue'].value
	if(lumpSum.checked){
		if(lumpSumAmount=='' || lumpSumAmount=='0.00'){
			alert("Select Amount to continue.....");
			return false;
		}else if(lumpPerUnit==''){
			alert("Select Per Unit to continue.....");
			return false;
		}else if(minReplace==''){
			alert("Select Min Replacement value to continue.....");
			return false;
		}
	}
	</c:if>
	return true;	
}
function getAgentCountryCode(){
	var ins=insuranceValue();
	var chk=true;
	var chkVatBillingGroup=true;
	<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">	
 	 chk= checkValidation();
    </configByCorp:fieldVisibility >
    <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">	
 	chk=checkValidationForPutters();
 	chkVatBillingGroup = savePatnerVatCode();
    </configByCorp:fieldVisibility >
 	 if(chk && ins && chkVatBillingGroup){
 	  submitPartner();
	}else{	 
	 return false;
	}
}

function checkValidationForPutters()
{
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	try{ 
	if(document.forms['accountProfileForm'].elements['partnerPrivate.paymentMethod'].value==''){
		alert('Please select the Payment Method');
		document.forms['accountProfileForm'].elements['partnerPrivate.paymentMethod'].focus();
		return false;
		
	}
	}catch(e){}
	try{ 
	if(document.forms['accountProfileForm'].elements['partnerPrivate.creditTerms'].value==''){
			alert('Please select the Credit Terms');
			document.forms['accountProfileForm'].elements['partnerPrivate.creditTerms'].focus();
			
			return false;
			
		}
	}catch(e){}
	</configByCorp:fieldVisibility>	
	return true;
	}
function showVlidField(){
	var lumpSum=document.forms['accountProfileForm'].elements['partnerPrivate.lumpSum'];
	 var e1 = document.getElementById('Currency11');	
	 var e2= document.getElementById('Per11');
	 var e3 = document.getElementById('Min11');
	if(lumpSum.checked){
		e1.style.display = 'block';
		e2.style.display = 'block';
		e3.style.display = 'block';
	}else{
		e1.style.display = 'none';
		e2.style.display = 'none';
		e3.style.display = 'none';
	}
}

function showOrHide(value) { 
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	} } 
function submitPartner(){
	showOrHide(1);
	
	document.forms['accountProfileForm'].action= "saveAdditionalInfo.html?partnerId=${partnerId}";
	 document.forms['accountProfileForm'].submit();
}
function forDays(){
 document.forms['accountProfileForm'].elements['checkCreditExpiryClick'].value = '1';
}


function pickAvailableCredit()
{   
	var creditAmount = document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value;	
	creditAmount=creditAmount.trim();
	if(creditAmount=='')
	{
		alert('Please enter credit amount first.');
		document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value='';		
	}else{
		var currency = document.forms['accountProfileForm'].elements['partnerPrivate.creditCurrency'].value;
		var billToCode = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;
		if((currency==undefined)||(currency==null))
		{
			currency="";
		}
		var url="pickAvailableCredit.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(billToCode)+"&defaultCurrency="+encodeURIComponent(currency);
	     	http221.open("GET", url, true);
	     	http221.onreadystatechange = handleHttpResponse221;
	     	http221.send(null);
	}
}
function handleHttpResponse221()
{
     if (http221.readyState == 4)
     {
        var results = http221.responseText
        results = results.trim();                     
        if(results!='')
        { 
        	var creditAmt = 0.00;
        	     creditAmt = document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value;        	    
        	     var usedAmt=parseFloat(results);
        	     creditAmt=creditAmt-usedAmt;
        	     var roundedNumber = roundNumber(creditAmt,2)
        	document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value=roundedNumber;
        }else{
        	document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value=0;
        }
     }
}
var http221 = getHTTPObject();
var updateBankRecordHttp = getHTTPObject(); 

function roundNumber(num, dec) {
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}
function checkValidation(){
	var corpIdValue=document.forms['accountProfileForm'].elements['partnerPrivate.corpID'].value
	var partnerType=document.forms['accountProfileForm'].elements['partnerType'].value
	<configByCorp:fieldVisibility componentId="component.field.partner.partnerCreditTermsStar">
	 if(document.forms['accountProfileForm'].elements['partnerPrivate.creditTerms'].value==''&& (partnerType=='AG'||partnerType=='AC')){
		alert('Please enter the Credit Terms');	
		return false;
	}
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">	
	if(document.forms['accountProfileForm'].elements['partnerPrivate.creditTerms'].value==''&& (partnerType=='AG'||partnerType=='AC')){
		alert('Please enter the Credit Terms');	
		return false;
	}
   </configByCorp:fieldVisibility >
    
	return true;
}

function enableField(){
	var enDibField='${partnerPrivate.creditCheck}';	
	enableDisable1(enDibField);
}
function openWindowMethod()
{
	window.open("updatePartnerPrivateSection.html?id=${partnerPrivate.id}&decorator=popup&popup=true","mywindow","menubar=0,resizable=1,scrollbars=yes,top=100,left=380,width=500,height=480");
}
function updatePartnerPrivate(){
	 var excludeFPU = document.getElementById("excludeFPU");	
	 if(excludeFPU.checked==true)
	 {
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=AIS&excludeFPU=TRUE&partnerCode=${partnerPublic.partnerCode}";
		  http12.open("GET", url, true); 
		  http12.onreadystatechange = handleHttpResponse2; 
	      http12.send(null); 
	 }else{
		  var url="updatePartnerPrivate.html?ajax=1&decorator=simple&popup=true&excludeOption=AIS&excludeFPU=FALSE&partnerCode=${partnerPublic.partnerCode}";
		  http12.open("GET", url, true); 
	      http12.onreadystatechange = handleHttpResponse2; 
	      http12.send(null); 
	 }
}
function handleHttpResponse2(){
   if (http12.readyState == 4)
   {
   			  var results = http12.responseText
	               results = results.trim();					               
				if(results=="")	{
				
				}
				else{
				
				} 
   } 
}
var http12 = getHTTPObject();
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function enableDisable1(targetElement)
{
var el = document.getElementById('coordRequiredTrue');
var el1 = document.getElementById('coordRequiredFalse');
var el11 = document.getElementById('coordRequiredTrue1');
var el111 = document.getElementById('coordRequiredFalse1');
var el22 = document.getElementById('coordRequiredTrue2');
var el222 = document.getElementById('coordRequiredFalse2');
var el333 = document.getElementById('coordRequiredTrue12');
var el444 = document.getElementById('coordRequiredFalse12');

if(targetElement=='true'){
		el.style.display = 'block';		
		el1.style.display = 'none';	
		
		el11.style.display = 'block';		
		el111.style.display = 'none';
		
		el22.style.display = 'block';		
		el222.style.display = 'none';

		el333.style.display = 'block';		
		el444.style.display = 'none';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=false;
		}catch(e){}
		//document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value='${partnerPrivate.creditDateCheck}';
		var checkdate='${partnerPrivate.creditDateCheck}';
		var mySplitResult= checkdate.split(" ");
	   	var day = mySplitResult[2];
	   	var month = mySplitResult[1];
	   	var year = mySplitResult[5];
	   	var datam = day+"-"+month+"-"+year.substring(2,4);
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value=datam;
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditCurrency'].value='${partnerPrivate.creditCurrency}';
	   	document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value='${partnerPrivate.creditAmount}';
	   	document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value='${partnerPrivate.availableCredit}';
	  	}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		
		el11.style.display = 'none';
		el111.style.display = 'block';
		
		el22.style.display = 'none';
		el222.style.display = 'block';

		el333.style.display = 'none';		
		el444.style.display = 'block';
		
		document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.creditCurrency'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.creditAmount'].value='';
		document.forms['accountProfileForm'].elements['partnerPrivate.availableCredit'].value='';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=true;
		}catch(e){}
		}
}
function GetSelectedItem(target) {
	var customerType=target;
	if(customerType=='Just Say Yes'){
	  	document.forms['accountProfileForm'].elements['partnerPrivate.customerFeedback'].value='Just Say Yes';	  
	  	document.forms['accountProfileForm'].elements['partnerPrivate.noTransfereeEvaluation'].disabled=true;
	   	document.forms['accountProfileForm'].elements['partnerPrivate.oneRequestPerCF'].disabled=true;
	   	document.forms['accountProfileForm'].rateScale[0].disabled = true;
	   	document.forms['accountProfileForm'].rateScale[1].disabled = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.description1'].readOnly = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.description2'].readOnly = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.description3'].readOnly = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.link1'].readOnly = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.link2'].readOnly = true;
		document.forms['accountProfileForm'].elements['partnerPrivate.link3'].readOnly = true;	  
   }
	else if(customerType=='Quality Survey'){		
	  	document.forms['accountProfileForm'].elements['partnerPrivate.customerFeedback'].value='Quality Survey';	
	  	document.forms['accountProfileForm'].elements['partnerPrivate.noTransfereeEvaluation'].disabled=false;
	   	document.forms['accountProfileForm'].elements['partnerPrivate.oneRequestPerCF'].disabled=false;
	   	if(document.forms['accountProfileForm'].elements['partnerPrivate.ratingScale'].value.indexOf('1 is the best')>-1){
	   		document.forms['accountProfileForm'].rateScale[1].checked=true;
		}else{
			document.forms['accountProfileForm'].rateScale[0].checked=true;
		}
	   	document.forms['accountProfileForm'].rateScale[0].disabled = false;
	   	document.forms['accountProfileForm'].rateScale[1].disabled = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.description1'].readOnly = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.description2'].readOnly = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.description3'].readOnly = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.link1'].readOnly = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.link2'].readOnly = false;
		document.forms['accountProfileForm'].elements['partnerPrivate.link3'].readOnly = false;	 
   }
}
function checkAccountStatus(target){
	var privateStatus=target.value;
	var publicStatus='${partnerPublic.status}'; 
	var checkStatus='${containsData}'; 
	if((publicStatus=='Inactive'|| publicStatus=='Blacklisted'|| publicStatus=='New'||publicStatus=='Prospect') && privateStatus=='Approved'){
		  alert("You can not mark status as 'Approve', Because Account details status is not Approve.")
		document.forms['accountProfileForm'].elements['partnerPrivate.status'].value='${partnerPrivate.status}';
		 return false;
	    }
}
function checkStatus(target){
	var privateStatus=target.value;
	var publicStatus='${partnerPublic.status}'; 
	var checkStatus='${containsData}'; 
	//alert(checkStatus+"~~~~~~~~~"+privateStatus);
	if((publicStatus=='Inactive'|| publicStatus=='Blacklisted'|| publicStatus=='New'||publicStatus=='Prospect') && privateStatus=='Approved'){
		  alert("You can not mark status as 'Approve', Because agent details status is not Approve.")
		document.forms['accountProfileForm'].elements['partnerPrivate.status'].value='${partnerPrivate.status}';
		 return false;
	    }else{
	    	var CheckPartnerType=checkStatus[2];
	    	var checkPartnerName="";
	    	if(CheckPartnerType=='V'){
	    		checkPartnerName="Vendor Code";
	    	}
	    	if(CheckPartnerType=='B'){
	    		checkPartnerName="Bill To Code";
	    		
	    	}
	    	    	if(privateStatus=='Inactive' && checkStatus[0]=='Y'){
	    	    		var retVal = confirm("Partnercode has been added in Default Account Line. If you want to continue ? "+checkPartnerName +" would be removed from Default Account Line.");
	    	               if( retVal == true ){
	    	            	   document.forms['accountProfileForm'].elements['checkCodefromDefaultAccountLine'].value="Y"+"~"+CheckPartnerType;
	    	                  return true;
	    	               }
	    	               else{
	    	            	   document.forms['accountProfileForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	    	            	   document.forms['accountProfileForm'].elements['partnerPrivate.status'].value='${partnerPrivate.status}';
	    	                  return false;
	    	               }
	    	    		
	    	    		 return false;
	    	    	}else{
	    	    			document.forms['accountProfileForm'].elements['checkCodefromDefaultAccountLine'].value="N"+"~"+CheckPartnerType;
	    	   				return true;
	    	    	}
	   }
	  return true;
	  }

function isInteger(targetElement)
{   var i;
	var s = targetElement.value;
	for (i = 0; i < s.length; i++)
	{   
	    var c = s.charAt(i);
	    if (((c < "0") || (c > "9"))) {
	    alert("Enter valid number");
	    targetElement.value="";
	    
	    return false;
	    }
	}
	return true;
}
function checkVendorName(){
    var vendorId = document.forms['accountProfileForm'].elements['partnerPrivate.vendorCode'].value;
    if(vendorId!=''){
    	var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse555;
	    http2.send(null);
     } else {
	     document.forms['accountProfileForm'].elements['partnerPrivate.vendorName'].value="";			
    }
}
function handleHttpResponse555(){
    if (http2.readyState == 4){
       var results = http2.responseText
       results = results.trim();
       var res = results.split("#"); 
       if(res.length>2){
       	if(res[2] == 'Approved'){
  				document.forms['accountProfileForm'].elements['partnerPrivate.vendorName'].value = res[1];  		
       		}else{
      			alert("Vendor Code is not approved" ); 
			    document.forms['accountProfileForm'].elements['partnerPrivate.vendorName'].value="";
		 		document.forms['accountProfileForm'].elements['partnerPrivate.vendorCode'].value="";
      		}
      	}else{
            alert("Vendor Code not valid" );
        	 document.forms['accountProfileForm'].elements['partnerPrivate.vendorName'].value="";
		 	 document.forms['accountProfileForm'].elements['partnerPrivate.vendorCode'].value="";
	   }
	}
}
var http2 = getHTTPObject();
function  checkNetworkPartnerName(){
       var vendorId = document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerCode'].value;
	    	if(vendorId!=''){ 
			    var url="networkPartnerName.html?ajax=1&decorator=simple&popup=true&pType=AG&partnerCode=" + encodeURI(vendorId); 
			    http2.open("GET", url, true);
			    http2.onreadystatechange = handleHttpResponseNetworkPartnerName;
			    http2.send(null);
		    }else{
		    	document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerName'].value='';
		    }
		 }
 
	function handleHttpResponseNetworkPartnerName(){
			if (http2.readyState == 4){
	                var results = http2.responseText
	                results = results.trim();
	                var res = results.split("#"); 
	                if(res.length>2){
	                	if(res[2] == 'Approved'){
	           				document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerName'].value = res[1];
		           		}else{
		           			alert("Network code is not approved" ); 
						    document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerName'].value="";
						    document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerCode'].value="";
		           		}
	               	}else {
	               	     alert("Network code is not valid" ); 
	                 	 document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerName'].value="";
	                 	 document.forms['accountProfileForm'].elements['partnerPrivate.networkPartnerCode'].value = "";
				   }
	             }
			}
	
	function validateRate(e){
		var rate=e.value;
		if(rate!=''){
		numeric=isNumeric(rate);
		if(!numeric){
			alert('Please enter valid Value');
			document.getElementById(e.id).value="";
			 document.getElementById(e.id).select();
			return false;
		}else{
			var num=parseFloat(rate).toFixed(2);
			var len = num.length;
			if(len>5){
				alert('The number not greater than 99.99');
				document.getElementById(e.id).value="";
				 document.getElementById(e.id).select();
				return false;
			}else{
			document.getElementById(e.id).value=num;
		}
		}
		}
	}
	
	function isNumeric(val) {
		var numeric = true;
	    var chars = "0123456789.";
	    var len = val.length;
	    var char = "";
	    for (i=0; i<len; i++) { char = val.charAt(i); if (chars.indexOf(char)==-1) { numeric = false; } }
	    return numeric;
	}
	
</script>

<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>

.listwhitetext2 {
background-color:#FFFFFF;
color:#444444;
font-family:arial,verdana;
font-size:11px;
font-weight:normal;
text-decoration:none;
padding-bottom:5px;
}
.bluefieldset {border:1px solid #219DD1;}
</style>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
animatedcollapse.addDiv('accDefault', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('CreditDetails', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('default', 'fade=0,persist=0,show=1')
animatedcollapse.addDiv('ControlInfo', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('DefaultRoles', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('QualityMeasurement', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('entitlement', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('accountAssignmentType', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('InsuranceSetup', 'fade=0,persist=1,hide=1')
animatedcollapse.addDiv('DefaultRolesForRelo', 'fade=0,persist=1,hide=1')
<c:if test="${partnerType == 'AG'}">
animatedcollapse.addDiv('information', 'fade=0,persist=0,show=1')
</c:if>
<c:if test="${partnerType == 'VN'}">
<configByCorp:fieldVisibility componentId="component.field.partner.monthlySalesGoalView">
	animatedcollapse.addDiv('monthlySalesGoal', 'fade=0,hide=1')
</configByCorp:fieldVisibility>
</c:if>
animatedcollapse.init()
</script>

</head>
<c:if test="${partnerType == 'AG' || partnerType == 'AC'  || partnerType == 'PP' || partnerType == 'CR' || partnerType == 'VN' || partnerType == 'OO'}">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partnerPrivate.id}" />
<c:set var="fileID" value="%{partnerPrivate.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>
<s:hidden name="noteFor" id="noteFor" value="Partner"></s:hidden>
</c:if>
<s:form id="accountProfileForm"  name="accountProfileFormName" action="" method="post" validate="true" >
<c:set var="defaultRolesEnable" value="true"/>
<s:hidden name="partnerPrivate.billToExtractAccess" />
<s:hidden name="partnerPrivate.vendorExtractAccess" />
<configByCorp:fieldVisibility componentId="component.partnerPrivate.defaultRoles.disable">
<c:if test="${(company.parentCorpId!=company.corpID) && (sessionCorpID eq company.corpID)}">
<c:set var="defaultRolesEnable" value="false"/>
</c:if>
</configByCorp:fieldVisibility>

<s:hidden name="checkCodefromDefaultAccountLine" /> 
 <c:if test="${partnerType != 'AG'}">
 <s:hidden name="partnerPrivate.privateCurrency" />
  <s:hidden name="partnerPrivate.privateBankCode" />
   <s:hidden name="partnerPrivate.privateBankAccountNumber" />
    <s:hidden name="partnerPrivate.privateVatNumber" />
 </c:if>

		<c:set var="qualityAgent" value="false"/>
		<configByCorp:fieldVisibility componentId="component.field.partner.QualityAgent">
		<c:set var="qualityAgent" value="true"/>
		</configByCorp:fieldVisibility>
		<c:set var="agentClassification" value="false"/>
		<configByCorp:fieldVisibility componentId="component.field.partner.agentClassification">
		<c:set var="agentClassification" value="true"/>
		</configByCorp:fieldVisibility>
	<s:hidden name="popupval" value="${papam.popup}" />
	<s:hidden name="newlineid" id="newlineid" value=" "/>
	<s:hidden name="partnerPrivate.customerFeedback" value="${partnerPrivate.customerFeedback }"/>
	<s:hidden name="agentCountryCode"/>
	<s:hidden id="countAccountProfileNotes" name="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>" />
	<c:set var="countAccountProfileNotes" value="<%=request.getParameter("countAccountProfileNotes") %>" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
	<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
	<s:hidden name="accountProfile.id" />
	<s:hidden name="accountProfile.corpID" />
	<s:hidden name="accountProfile.partnerCode" />
	<s:hidden name="partnerPrivate.id" />
	<s:hidden name="partnerPrivate.corpID" />
	<s:hidden id="checkCreditExpiryClick" name="checkCreditExpiryClick" />
	<s:hidden name="partnerPrivate.companyDivision" />
	<s:hidden name="partnerPrivate.cardNumber" />
	<s:hidden name="partnerPrivate.cardStatus" />
	<s:hidden name="partnerPrivate.accountId" />
	<s:hidden name="partnerPrivate.customerId" />
	<s:hidden name="partnerPrivate.licenseNumber" />
	<s:hidden name="partnerPrivate.licenseState" />
	<s:hidden name="partnerPrivate.cardSettelment" />
	<s:hidden name="partnerPrivate.directDeposit" />
	<s:hidden name="partnerPrivate.fuelPurchase" />
	<s:hidden name="partnerPrivate.expressCash" />
	<s:hidden name="partnerPrivate.commission" />
	<s:hidden name="partnerPrivate.accountingHold" />
	<s:hidden name="partnerPrivate.ratingScale" />
	<s:hidden name="secondDescription" />
		<s:hidden name="thirdDescription" />
		<s:hidden name="firstDescription" />
		<s:hidden name="fourthDescription" />
		<s:hidden name="fifthDescription" />
		<s:hidden name="sixthDescription" />
		<s:hidden name="seventhDescription"/>
		<s:hidden name="partnerCode" />
	
 <c:set var="idOfWhom" value="${partnerPrivate.id}" scope="session"/>
 <c:set var="noteID" value="${partnerPrivate.partnerCode}" scope="session"/>
 <c:set var="noteFor" value="Partner" scope="session"/>
 <c:set var="idOfTasks" value="" scope="session"/>
 <c:set var="PPID" value="${partnerPublic.id}"/>
 <s:hidden name="PPID" id="PPID" value="${partnerPublic.id}" />
 <c:if test="${empty partnerPrivate.id }">
	<c:set var="isTrue" value="false" scope="request"/>
 </c:if>
<c:if test="${not empty partnerPrivate.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
	
	<c:if test="${partnerType != 'AC' && partnerType != 'AG'}">
		<s:hidden name="partnerPrivate.accountManager" />
	</c:if>
		<c:if test="${partnerType != 'AC'}">
		<s:hidden name="partnerPrivate.insuranceAuthorized" />
	</c:if>
	<c:if test="${partnerType == 'VN'}">
	<s:hidden name="partnerPrivate.storageBillingGroup"/>
	</c:if>	
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />
	<s:hidden id="partnerNotes" name="partnerNotes" value="<%=request.getParameter("partnerNotes") %>"/>
	<c:set var="partnerNotes" value="<%=request.getParameter("partnerNotes") %>" />

	<div id="newmnav" style="!margin-bottom:3px;">
	<ul>
		<c:if test="${partnerType == 'AC'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
			<li><a href="editNewAccountProfile.html?id=${partnerId}&partnerType=${partnerType}&partnerCode=${partnerPublic.partnerCode}"><span>Account Profile</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<configByCorp:fieldVisibility componentId="component.section.partner.SuddathInfo">
			<li><a href="editProfileInfo.html?partnerId=${partnerId}&partnerCode=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Profile Information</span></a></li>
			</configByCorp:fieldVisibility>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<c:if test="${not empty partnerPrivate.id}">
			<configByCorp:fieldVisibility componentId="component.standard.accountContactTab">
				<li><a href="accountContactList.html?id=${partnerId}&partnerType=${partnerType}"><span>Account Contact</span></a></li>
				</configByCorp:fieldVisibility>
				<c:if test="${checkTransfereeInfopackage==true}">
				<li><a href="editContractPolicy.html?id=${partnerId}&partnerType=${partnerType}"><span>Policy</span></a></li>
				</c:if>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			 <configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Portal Users</span></a></li>
			</configByCorp:fieldVisibility>
			</c:if>
			<c:if test="${paramValue == 'View'}">
					<li><a href="searchPartnerView.html"><span>Public List</span></a></li>
				</c:if>
				<c:if test="${paramValue != 'View'}">
					<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a href="partnerReloSvcs.html?id=${partnerId}&partnerType=${partnerType}"><span>Services</span></a></li>
				</c:if>
					<c:if test="${partnerType == 'AC'}">
				 <c:if test="${not empty partnerPublic.id}">
				<c:url value="frequentlyAskedQuestionsList.html" var="url">
				<c:param name="partnerCode" value="${partnerPublic.partnerCode}"/>
				<c:param name="partnerType" value="AC"/>
				<c:param name="partnerId" value="${partnerPublic.id}"/>
				<c:param name="lastName" value="${partnerPublic.lastName}"/>
				<c:param name="status" value="${partnerPublic.status}"/>
				</c:url>				 
				<li><a href="${url}"><span>FAQ</span></a></li>
				<c:if test="${partnerPublic.partnerPortalActive == true}">
					<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Security Sets</span></a></li>
				</c:if>
				  </c:if>
				</c:if>	
				<c:if test="${not empty partnerPrivate.id}">
					<li><a onclick="openNotesPopupTab(this);"><span>Notes</span></a></li>
				</c:if>
				<%-- <c:if test="${partnerType == 'AG' || partnerType == 'VN' || partnerType == 'AC'}">
					<li><a onclick="window.open('auditList.html?id=${partnerPrivate.id}&tableName=partnerprivate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
				</c:if> --%>
						
			<%--<li><a onclick="window.open('auditList.html?id=${partnerPrivate.id}&tableName=partnerprivate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		--%></c:if>

		<c:if test="${partnerType == 'AG'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Agent Detail</span></a></li>
			<li><a href="findPartnerProfileList.html?code=${partnerPrivate.partnerCode}&partnerType=${partnerType}&id=${partnerId}"><span>Agent Profile</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref</span></a></li>
			<li><a href="baseList.html?id=${partnerId}"><span>Base</span></a></li>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
			</c:if>
			<!--<c:if test='${partnerPublic.latitude == ""  || partnerPublic.longitude == "" || partnerPublic.latitude == null  || partnerPublic.longitude == null}'>
				<li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>
			</c:if>
			<c:if test='${partnerPublic.latitude != "" && partnerPublic.longitude != "" && partnerPublic.latitude != null && partnerPublic.longitude != null}'>
				<li><a href="partnerRateGrids.html?partnerId=${partnerId}"><span>Rate Matrix</span></a></li>
			</c:if>
			--><c:if test="${partnerType == 'AG'}">
				<li><a href="partnerReloSvcs.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Services</span></a></li>
			</c:if>
			<c:if test="${partnerPublic.partnerPortalActive == true}">
			   <configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
	       			<li><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
	       			<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partnerPublic.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
			        </configByCorp:fieldVisibility>
			</c:if>
			
			       <li id="AGR"><a href="partnerUsersList.html?id=${partnerPublic.id}&partnerType=${partnerType}"><span>Portal Users & Contacts</span></a></li>
			 
			 
			  	
		</c:if>
		<c:if test="${partnerType == 'VN'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Vendor Detail</span></a></li>
			<li id="newmnav1" style="background:#FFF"><a class="current"><span>Additional Info<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="partnerAccountRefs.html?partnerCodeForRef=${partnerPublic.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
			<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>		
		</c:if>
		<c:if test="${partnerType == 'CR'}">
			<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=${partnerType}"><span>Carrier Detail</span></a></li>
		</c:if>
		<c:if test="${partnerType == 'CR' || partnerType == 'VN'}">
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
		<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${partnerPrivate.id}&tableName=partnerprivate&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	
		<!--<c:if test="${partnerPrivate.partnerPortalActive == true}">
			<c:if test="${partnerType == 'AG'}">
				<li><a href="partnerUsersList.html?id=${partnerId}&partnerType=${partnerType}"><span>Users & contacts</span></a></li>
			</c:if>
		</c:if>
		-->
		<configByCorp:fieldVisibility componentId="component.tab.partner.preferredAgent.show">
				<c:if test="${partnerType == 'AC'}">
							<li><a href="searchPreferredAgentList.html?partnerId=${partnerPublic.id}&partnerCode=${partnerPrivate.partnerCode}&partnerType=${partnerType}"><span>Preferred Agent</span></a></li>
						</c:if>
		</configByCorp:fieldVisibility>
	</ul>
	</div>
	<div class="spn">&nbsp;</div>

	<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
	<div id="content" align="center">
	<div id="liquid-round-top">
	<div class="top"><span></span></div>
		
	<!-- Start AccoutType Section	-->
	
	<c:if test="${partnerType == 'AC'}">
		<div class="center-content">
		<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin-bottom:5px;">
			<tbody>
				<tr>
					<td>
					<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%">
						<tbody>						
							<tr>
							<td colspan="20">
							<table class="detailTabLabel" border="0">
							<tr>
									<td align="right" class="listwhitetext" style="width: 102px; !width:135px">Local&nbsp;Name</td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.localName" required="true" cssClass="input-text" maxlength="95" size="30" /></td>							
								<td align="right" class="listwhitetext" width="60px">Name<font color="red" size="2">*</font></td>
								<s:hidden key="partnerPrivate.firstName" />
								<td align="left" class="listwhitetext"><s:textfield key="partnerPrivate.lastName" required="true" cssClass="input-textUpper" size="30" maxlength="80" onkeydown="return onlyCharsAllowed(event)" readonly="true" tabindex="2"/></td>
								<td align="right" class="listwhitetext" width="60px"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
								<td align="left" class="listwhitetext"><s:textfield key="partnerPrivate.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="6" onkeydown="" readonly="true" tabindex="1" /></td>
								<td align="right" class="listwhitetext" width="60px">Status</td>
								<td align="left" class="listwhitetext" colspan=""><s:select cssClass="list-menu" name="partnerPrivate.status" list="%{partnerStatus}" cssStyle="width:80px" onchange="return checkAccountStatus(this);"tabindex="3"/></td>
								<td align="right" class="listwhitetext" width="75px">External&nbsp;Ref.</td>
								<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="8" tabindex="4"  /></td>
								   <c:if test="${empty partnerPrivate.id}">
								          <td align="right" style="width:100px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
						           </c:if>
								<c:if test="${not empty partnerPrivate.id}">
									<c:choose>
										<c:when test="${partnerNotes == '0' || partnerNotes == '' || partnerNotes == null}">
											<td align="right" style="width:100px"><img id="partnerNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
										</c:when>
										<c:otherwise>
											<td align="right" style="width:100px"><img id="partnerNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&subType=VipReason&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
										</c:otherwise>
									</c:choose> 
								</c:if>
							</tr>
							</table>
							
							</td>
							</tr>
							
	<tr>						
		<td colspan="20" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('ControlInfo')" style="margin: 0px">
			 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					  <td class="headtab_left">
					  </td>
					  <td NOWRAP class="headtab_center">&nbsp;&nbsp;Control Info
					  </td>
					  <td width="28" valign="top" class="headtab_bg"></td>
					  <td class="headtab_bg_special">&nbsp;
					  </td>
					  <td class="headtab_right">
					  </td>
				 </tr>
			</table>
					  </div>
				<div id="ControlInfo" class="ControlInfo1">
					<table class="detailTabLabel"  cellpadding="0">
						<tr>
						
							<c:if test="${partnerPrivate.portalDefaultAccess}">
								<c:set var="ischecked" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="100px">Transferee Fills Info</td>
							<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.portalDefaultAccess" fieldValue="true" value="${ischecked}" onchange="changeStatus();" tabindex="" /></td>
							<td></td>
							<c:set var="ischeckeddddd" value="false" />
							<c:if test="${partnerPrivate.grpAllowed}">
								<c:set var="ischeckeddddd" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="100">Allow Groupage</td>
							<td class="listwhitetext" width="50px" align="left"><s:checkbox key="partnerPrivate.grpAllowed" onclick="changeStatus();" value="${ischeckeddddd}"  /></td>
					        <c:set var="ischeckedSurveyEmailSent" value="false" />
							<c:if test="${partnerPrivate.customerSurveyEmail}">
							<c:set var="ischeckedSurveyEmailSent" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="">Customer Survey Email Alert</td>
							<td class="listwhitetext" width=""><s:checkbox key="partnerPrivate.customerSurveyEmail" onclick="changeStatus();" value="${ischeckedSurveyEmailSent}" fieldValue="true" tabindex="17" /></td>
						</tr>
					</table>
				</div>	
		   </td>
	</tr>
							
				<tr>						
					<td colspan="20" width="100%" align="left" style="margin: 0px">
     				    <div onClick="javascript:animatedcollapse.toggle('DefaultRoles')" style="margin: 0px">
					      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					         <tr>
					             <td class="headtab_left">
					             </td>
					             <td NOWRAP class="headtab_center">&nbsp;&nbsp;Default Roles
					             </td>
					             <td width="28" valign="top" class="headtab_bg"></td>
					             <td class="headtab_bg_special">&nbsp;
					             </td>
					              <td class="headtab_right">
					              </td>
					       </tr>
					    </table>
					  </div>
							<div id="DefaultRoles" class="DefaultRoles1">
							<table class="detailTabLabel">
							<tr>
								<td align="right" class="listwhitetext" width="100">
									<fmt:message key='accountInfo.accountHolder' />
								</td>
								<td class="listwhitetext">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.accountHolder" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select cssClass="list-menu" name="partnerPrivate.accountHolder" list="%{sale}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" tabindex=""  />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" width="100" class="listwhitetext">Move&nbsp;Manager</td>
								<td colspan="2">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.accountManager" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.accountManager" cssClass="list-menu" list="%{coordinator}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right"  width="103"  class="listwhitetext">Pricing</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.pricingUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.pricingUser" cssClass="list-menu" list="%{pricing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right"  width="103"  class="listwhitetext">Claims</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.claimsUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.claimsUser" cssClass="list-menu" list="%{claimsUser}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
						    	<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
						    	<td align="right"  width="103"  class="listwhitetext">USA&nbsp;Flag&nbsp;Required</td>
							    <td><s:checkbox key="partnerPrivate.usaFlagRequired" tabindex=""/></td>
							    </configByCorp:fieldVisibility>
						
							</tr>
							<tr>					
								<td align="right" class="listwhitetext">Billing</td>
								<td align="left">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.billingUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.billingUser" cssClass="list-menu" list="%{billing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Payable</td>
								<td colspan="2">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.payableUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select cssClass="list-menu" name="partnerPrivate.payableUser" list="%{payable}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Auditor</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.auditor" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.auditor" cssClass="list-menu" list="%{auditor}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Coordinator</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.coordinator" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.coordinator"  list="%{coordinatorList}" onchange="" cssStyle="width:135px" headerKey="" headerValue="" cssClass="list-menu" />
									</c:otherwise>
								</c:choose>
								</td>
								<configByCorp:fieldVisibility componentId="component.field.billing.internalBillingPerson">
									<td align="right" class="listwhitetext">Internal&nbsp;Billing</td>
									<td>
									<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.internalBillingPerson" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.internalBillingPerson" cssClass="list-menu" list="%{internalBillingPersonList}" cssStyle="width:135px" headerKey="" headerValue=""/>
									</c:otherwise>
									</c:choose>
									</td>
								</configByCorp:fieldVisibility>
							</tr>
								</table>
							</div>	
					 </td>
				</tr>
				<tr>						
					<td colspan="20" width="100%" align="left" style="margin: 0px">
     				    <div onClick="javascript:animatedcollapse.toggle('DefaultRolesForRelo')" style="margin: 0px">
					      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					         <tr>
					             <td class="headtab_left">
					             </td>
					             <td NOWRAP class="headtab_center">&nbsp;&nbsp;Default Roles for Relocation
					             </td>
					             <td width="28" valign="top" class="headtab_bg"></td>
					             <td class="headtab_bg_special">&nbsp;
					             </td>
					              <td class="headtab_right">
					              </td>
					       </tr>
					    </table>
					  </div>
							<div id="DefaultRolesForRelo" class="DefaultRoles1">
							<table class="detailTabLabel">
							<tr>
								<td align="right" class="listwhitetext" width="100"><fmt:message key='accountInfo.accountHolder' /></td>
								<td class="listwhitetext">
								<s:select cssClass="list-menu" name="partnerPrivate.accountHolderForRelo" list="%{sale}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" />
								</td>
								<td align="right" width="100" class="listwhitetext">Move&nbsp;Manager</td>
								<td colspan="2"><s:select name="partnerPrivate.accountManagerForRelo" cssClass="list-menu" list="%{coordinator}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								<td align="right"  width="103"  class="listwhitetext">Pricing</td>
								<td><s:select name="partnerPrivate.pricingUserForRelo" cssClass="list-menu" list="%{pricing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								<td align="right"  width="103"  class="listwhitetext">Claims</td>
								<td><s:select name="partnerPrivate.claimsUserForRelo" cssClass="list-menu" list="%{claimsUser}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
						    							
							</tr>
							<tr>					
								<td align="right" class="listwhitetext">Billing</td>
								<td align="left"><s:select name="partnerPrivate.billingUserForRelo" cssClass="list-menu" list="%{billing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								<td align="right" class="listwhitetext">Payable</td>
								<td colspan="2"><s:select cssClass="list-menu" name="partnerPrivate.payableUserForRelo" list="%{payable}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								<td align="right" class="listwhitetext">Auditor</td>
								<td><s:select name="partnerPrivate.auditorForRelo" cssClass="list-menu" list="%{auditor}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" /></td>
								<td align="right" class="listwhitetext">Coordinator</td>
								<td><s:select name="partnerPrivate.coordinatorForRelo"  list="%{coordinatorList}" onchange="" cssStyle="width:135px" headerKey="" headerValue="" cssClass="list-menu" /></td>
							</tr>
								</table>
							</div>	
					 </td>
				</tr>
						<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('accDefault')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Accounting Default
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="accDefault" class="switchgroup1">
							<table class="detailTabLabel">		
							<sec-auth:authComponent componentId="module.partner.section.storageBillingGroup.edit">
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='accountInfo.storage' /></td>
									<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.storageBillingGroup" list="%{billgrp}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
									<td align="right" class="listwhitetext" width="100px"><fmt:message key='accountInfo.nationalAccount' /></td>
									<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.validNationalCode" size="22" cssStyle="width:130px;" maxlength="15" tabindex="" /></td>
									<configByCorp:fieldVisibility componentId="component.tab.partnerPrivate.AllPartnerTRN">
									<c:set var="AllPartnerTRN" value="Y" />
									</configByCorp:fieldVisibility>
									<c:if test="${AllPartnerTRN =='Y'}"> 
									<td align="right" class="listwhitetext" width="">TRN No.</td>
									</c:if>
									<c:if test="${AllPartnerTRN !='Y'}">
									<td align="right" class="listwhitetext" width="">MC# / Purchase Order No.</td>
									</c:if>
									<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.mc" size="25" cssStyle="width:165px"  maxlength="25" /></td>
								</tr>
							</sec-auth:authComponent>
							<tr>
								<td align="right" class="listwhitetext"><fmt:message key='accountInfo.abbreviation' /></td>
								<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.abbreviation" cssStyle="width:151px;" size="25" maxlength="15" tabindex="9" /></td>
								<td align="right" class="listwhitetext">Class Code</td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.classcode" list="%{classcode}" cssStyle="width:133px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>	
								<!-- remove after enhancement of bug #5843
								<td align="right" class="listwhitetext">Bank Account #
								<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
								<font color="red" size="2">*</font>
								</configByCorp:fieldVisibility>
								</td>
						        <td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.bankAccountNumber" size="25" maxlength="20" tabindex="12" /></td>
								-->
								<td class="listwhitetext" width="135px" align="right">Storage Email Address</td>
						<td><s:textfield cssClass="input-text" name="partnerPrivate.storageEmail" maxlength="50" size="33" cssStyle="width:165px;" onchange="checkEmail()" /></td>	     
								<c:set var="ischecked" value="false" />
								<td></td>
							</tr>
							
							<tr>
								<td align="right" class="listwhitetext"><fmt:message key='accountInfo.paymentMethod' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.paymentMethod" list="%{paytype}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="7" /></td>
								<td align="right" class="listwhitetext">Option</td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.payOption" list="%{payopt}" cssStyle="width:133px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
							   <!-- remove after enhancement of bug #5843
							   <td align="right" class="listwhitetext">Bank Code
							   <configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
							   <font color="red" size="2">*</font>
							   </configByCorp:fieldVisibility>
							   </td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.bankCode" list="%{bankCode}" cssStyle="width:155px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>								
								-->
									
									<td align="right" class="listwhitetext">Billing Cycle</td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.billingCycle" list="%{billCycle}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>	
						
								<td align="right" class="listwhitetext">Storage Email Type</td>
								<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.storageEmailType" list="%{storageEmailType}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>	
								<configByCorp:fieldVisibility componentId="component.tab.claim.claimByNoDaySettlement">							
								<td align="right" class="listwhitetext">Claim By</td>
								<td align="left"><s:textfield cssClass="input-text" name="partnerPrivate.claimBy" cssStyle="width:135px" onchange="isInteger(this)"/></td>
								</configByCorp:fieldVisibility>
								<td></td>
							</tr>
							<tr>
								<td align="right" class="listwhitetext"><fmt:message key='accountInfo.billingInstruction' /></td>
								<td class="listwhitetext" ><s:select cssClass="list-menu" name="partnerPrivate.billingInstruction" list="%{billinst}" cssStyle="width:315px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
							<td align="right" class="listwhitetext">Collection</td>
							<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.collection" list="%{collectionList}" cssStyle="width:133px;" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							<td align="right" class="listwhitetext">Billing Moment</td>
							<td align="left">
							<s:select cssClass="list-menu" name="partnerPrivate.billingMoment" list="%{billingMomentList}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" />
							</td>
			
							<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
							<td align="right" class="listwhitetext">Default&nbsp;Stationary</td>	
							<td><s:select cssClass="list-menu" name="partnerPrivate.emailPrintOption" list="%{printOptions}" cssStyle="width:130px"  value=" "></s:select>	</td>		
						    </configByCorp:fieldVisibility>
						        <!-- remove after enhancement of bug #5843
						         <td align="right" class="listwhitetext">VAT #
							      <configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
							         <font color="red" size="2">*</font>
							      </configByCorp:fieldVisibility>
							    </td>
								<td class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" name="partnerPrivate.vatNumber"   size="25" maxlength="23" tabindex="12" /></td>
						-->
							    	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
			               <td valign="middle" colspan="9" align="left">
		  	              <table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			              <td align="right" class="listwhitetext" width="80px">VAT&nbsp;Billing&nbsp;Group<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"></configByCorp:fieldVisibility></td>
				        <td align="left"><s:select cssClass="list-menu" name="partnerPrivate.vatBillingGroup" id="vatBillingGroup" list="%{vatBillingGroups}" onchange="changeStatus();vatBilling(this);" headerKey="" headerValue="" cssStyle="width:170px" tabindex=""/></td>
			             </table></td>
			          </configByCorp:fieldVisibility>
							</tr>	
													
					</table>
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="detailTabLabel">
						<tr>
						<td colspan="10">
						<table border="0" class="detailTabLabel">
						<tr>
							<c:set var="ischecked" value="false" />
							<c:if test="${partnerPrivate.multiAuthorization}">
								<c:set var="ischecked" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="145px"><fmt:message key='accountInfo.multiAuthorization' /></td>
							<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.multiAuthorization" fieldValue="true" value="${ischecked}" onchange="changeStatus();" tabindex="" /></td>
							
							<c:set var="ischeckedddd" value="false" />
							<c:if test="${partnerPrivate.payableUploadCheck}">
								<c:set var="ischeckedddd" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="261">Don't send to payable upload</td>
							<td class="listwhitetext" width="10px"><s:checkbox key="partnerPrivate.payableUploadCheck" onclick="changeStatus();" value="${ischeckedddd}" fieldValue="true" tabindex="14" /></td>
							
							<c:set var="ischeckeddddd" value="false" />
							<c:if test="${partnerPrivate.invoiceUploadCheck}">
								<c:set var="ischeckeddddd" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="213">Don't send to invoice upload</td>
							<td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.invoiceUploadCheck" onclick="changeStatus();" value="${ischeckeddddd}" fieldValue="true" tabindex="13" /></td>
							<configByCorp:fieldVisibility componentId="component.tab.partnerPrivate.excludeFinancialExt">
							<s:hidden name="partnerPrivate.excludeFinancialExtract" />
							</configByCorp:fieldVisibility>						
							
						</tr>
						<tr>
							<c:set var="ischeckedStopNot" value="false" />
							<c:if test="${partnerPrivate.stopNotAuthorizedInvoices}">
								<c:set var="ischeckedStopNot" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="">Stop Not Authorized Invoices</td>
							<td class="listwhitetext"><s:checkbox key="partnerPrivate.stopNotAuthorizedInvoices" onclick="changeStatus();" value="${ischeckedStopNot}" fieldValue="true" tabindex="15" /></td>
							
							<c:set var="ischeckedDoNotCopy" value="false" />
							<c:if test="${partnerPrivate.doNotCopyAuthorizationSO}">
								<c:set var="ischeckedDoNotCopy" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="">Don't Copy Authorization SO</td>
							<td class="listwhitetext" align="left"><s:checkbox key="partnerPrivate.doNotCopyAuthorizationSO" onclick="changeStatus();" value="${ischeckedDoNotCopy}" fieldValue="true" tabindex="16" /></td>
							
							<c:set var="isInsuranceAuthorized" value="false" />
							<c:if test="${partnerPrivate.insuranceAuthorized}">
								<c:set var="isInsuranceAuthorized" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="">Insurance Authorized</td>
							<td class="listwhitetext" align="left"><s:checkbox key="partnerPrivate.insuranceAuthorized" onclick="changeStatus();" value="${isInsuranceAuthorized}" fieldValue="true" tabindex="" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="213">Don't invoice</td>
							<td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.doNotInvoice" fieldValue="true" tabindex="" /></td>
							<td align="right" class="listwhitetext" width="235">Just Say Yes Email</td>
                            <td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.doNotSayYesEmail"  tabindex="" /></td>
                            <configByCorp:fieldVisibility componentId="component.field.partnerPrivate.converted">
							<c:set var="isConverted" value="false" />
							<c:if test="${partnerPrivate.converted}">
								<c:set var="isConverted" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="">Converted</td>
							<td width="10px"><s:checkbox key="partnerPrivate.converted" fieldValue="true" value="${isConverted}" onchange="changeStatus();"  /></td>
							</configByCorp:fieldVisibility>
                      	</tr>
						</table>
						</td>
						</tr>
												
						<tr>
					        <td align="right" width="223" class="listwhitetext">Minimum&nbsp;Margin&nbsp;%&nbsp;</td>
						    <td width="140"><input type="text" class="input-text" oncopy="return false" onpaste="return false" name="partnerPrivate.minimumMargin" id="minimumMargin" value="${partnerPrivate.minimumMargin}" maxlength="8" style="width:60px;margin-bottom:5px" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter()"></td>
						    <configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
						    <td align="right" width="150" class="listwhitetext">Consumable&nbsp;%&nbsp;</td>
						    <td width="140"><input type="text" class="input-text" name="partnerPrivate.consumablePercentage" id="consumablePercentage" value="${partnerPrivate.consumablePercentage}" maxlength="3" style="width:60px;margin-bottom:5px;text-align:right" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter()"></td>
						    </configByCorp:fieldVisibility>
						    <configByCorp:fieldVisibility componentId="component.tab.commission.compensation">
						    <td align="left" width="100" class="listwhitetext">Compensation&nbsp;Year&nbsp;</td>
						    <td>
						    <s:textfield key="partnerPrivate.compensationYear" required="true" cssClass="input-textUpper" maxlength="8" size="5"  readonly="true" tabindex="1" />
						    </td>
						    </configByCorp:fieldVisibility>
						    <configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
							<td align="right" class="listwhitetext" width="40">RUT#&nbsp;</td>
							<td class="listwhitetext">					
							<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
							</td>
							</configByCorp:fieldVisibility>
						    <td></td>
						</tr>
					
					
						
						
						</table>
					</div>
						</td>
						</tr>
						
						<tr>						
					<td colspan="20" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('CreditDetails')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Credit Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="CreditDetails" class="switchgroup1">						
						
								<table class="detailTabLabel"  cellpadding="0">
									<tr>
										<c:set var="ischeckedCredit" value="false" />
										<c:if test="${partnerPrivate.creditCheck}">
											<c:set var="ischeckedCredit" value="true" />
										</c:if>
										
										<td align="right" class="listwhitetext" width="98px">Credit&nbsp;Check</td>
										<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.creditCheck" fieldValue="true" value="${ischeckedCredit}" onclick="creditDate(this);enableDisable(this);" tabindex="18" /></td>
										
										<td align="right" class="listwhitetext" width="75px">Credit&nbsp;Amount</td>
										<td class="listwhitetext" id="coordRequiredTrue" width="80px"><s:textfield cssClass="input-text"  name="partnerPrivate.creditAmount" size="10" maxlength="15" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>
										<td class="listwhitetext" id="coordRequiredFalse" width="80px"><s:textfield cssClass="input-textUpper"  name="partnerPrivate.creditAmount1" size="10" maxlength="15" readonly="true" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;" tabindex="19"/></td>
										
										
										
										<td align="right" class="listwhitetext" width="85px">Credit&nbsp;Currency</td>
										<td class="listwhitetext" id="coordRequiredTrue1" width="87px"><s:select cssClass="list-menu" name="partnerPrivate.creditCurrency" list="%{currency}" cssStyle="width:85px;margin-top:5px;" headerKey="" headerValue="" onchange="changeStatus();" tabindex="20" /></td>
										<td class="listwhitetext" id="coordRequiredFalse1" width="87px"><s:select cssClass="list-menu"  name="partnerPrivate.creditCurrency1" list="%{currency}" cssStyle="width:85px;margin-top:5px;" headerKey="" headerValue="" disabled="true" onchange="changeStatus();" tabindex="20" /></td>
										
										
										
										<td align="right" class="listwhitetext" width="90px">Credit&nbsp;Date&nbsp;Check</td>
										<td align="left" colspan="2" valign="top">
													<table class="detailTabLabel" border="0">
													<tr>
										<c:if test="${not empty partnerPrivate.creditDateCheck}">
											<s:text id="creditDateCheckFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPrivate.creditDateCheck" /></s:text>
											<td width="50px"><s:textfield cssClass="input-text" id="creditDateCheck" readonly="true" name="partnerPrivate.creditDateCheck" value="%{creditDateCheckFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="creditDate(true);" tabindex=""/></td>
											<td align="left" id="coordRequiredTrue2" width="20px" style="padding-top:2px;vertical-align:bottom;position:relative;">
											<img id="creditDateCheck_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" /></td>
											<td align="left" id="coordRequiredFalse2" width="10px" style="padding-top:2px; vertical-align:bottom;position:relative;"></td>
										</c:if>
										<c:if test="${empty partnerPrivate.creditDateCheck}">
											<td width="50px"><s:textfield cssClass="input-text" id="creditDateCheck" readonly="true" name="partnerPrivate.creditDateCheck" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="creditDate(true);"/></td>
											<td align="left" id="coordRequiredTrue2" width="20px" style="padding-top:2px;vertical-align:bottom;position:relative;">
											<img id="creditDateCheck_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" /></td>
											<td align="left" id="coordRequiredFalse2" width="10px" style="padding-top:2px;position:relative;"></td>
										</c:if>
										</tr>
										</table>
										</td>
										
										
										<td align="right" class="listwhitetext" width="75px">Credit&nbsp;Expiry</td>
										<c:if test="${not empty partnerPrivate.creditExpiry}">
											<s:text id="creditExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPrivate.creditExpiry" /></s:text>
											<td width="50px"><s:textfield cssClass="input-textUpper" id="creditExpiry" readonly="true" name="partnerPrivate.creditExpiry" value="%{creditExpiryFormattedValue}" cssStyle="width:75px;"  maxlength="11" onkeydown="return onlyDel(event,this)" onselect="creditDate(true);" onchange="creditDate(true);" tabindex=""/></td>
											
										</c:if>
										<c:if test="${empty partnerPrivate.creditExpiry}">
											<td width="50px"><s:textfield cssClass="input-textUpper" id="creditExpiry" readonly="true" name="partnerPrivate.creditExpiry" cssStyle="width:75px;" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="creditDate(true);" onchange="creditDate(true);" tabindex=""/></td>
											
										</c:if>
																				
									</tr>
									<tr>
									<td colspan="6">
									<table class="detailTabLabel" cellpadding="0">
									<tr>									
						<td align="right" class="listwhitetext" width="102px">Available&nbsp;Credit</td>																	
						<td class="listwhitetext" id="coordRequiredTrue12"><s:textfield cssClass="input-text"  name="partnerPrivate.availableCredit" size="10" maxlength="15" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>
						<td class="listwhitetext" id="coordRequiredFalse12"><s:textfield cssClass="input-textUpper"  name="partnerPrivate.availableCredit1" size="10" maxlength="15" readonly="true" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>																									
						<td align="left">
						<input type="button" value="Available Credit" style="width:100px;" name="availableCreditButton" class="cssbuttonB" onclick="pickAvailableCredit();"/></td>										
						<td colspan="5" class="listwhitetext" style="padding-left:18px;">
						<span style="!vertical-align:top;"><fmt:message key='accountInfo.creditTerms' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility>
						<configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
						<font color="red" size="2">*</font>
						</configByCorp:fieldVisibility>
						</span>
							&nbsp;<s:select cssClass="list-menu" name="partnerPrivate.creditTerms" list="%{creditTerms}" cssStyle="width:81px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
						</tr>
									</table>
									</td>
									</tr>
						
						<!--<tr>
							<td colspan="10" width="100%" height="2px"></td>
						</tr>
						<tr>
							<td colspan="5" class="listwhitetext" style="padding-left:42px;">							
							<span style="!vertical-align:top;"><fmt:message key='accountInfo.creditTerms' /></span>
							&nbsp;<s:select cssClass="list-menu" name="partnerPrivate.creditTerms" list="%{creditTerms}" cssStyle="width:100px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
						
						</tr>
					--></table>
					</div>
						</td>
						</tr>	
			
				<%--<tr>
					<td height="10" align="left" class="listwhitetext" colspan="10">
					<div onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Partner Portal Detail</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="partnerportal">
					<table  cellpadding="1" cellspacing="0" class="detailTabLabel">
						<tr>
							<c:set var="isActiveFlag1" value="false" />
							<c:if test="${partnerPrivate.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true" />
							</c:if>
							<c:set var="isActiveFlag" value="false" />
							<c:if test="${partnerPrivate.viewChild}">
								<c:set var="isActiveFlag" value="true" />
							</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext">Partner Portal:</td>
							<td width="1px"><s:checkbox key="partnerPrivate.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()" tabindex="21" /></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive' /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext">Associated Account</td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPrivate.associatedAgents" size="30" maxlength="65" tabindex="22"/></td>
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Account Records:</td>
							<td width="16"><s:checkbox key="partnerPrivate.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="23" /></td>
							<td><fmt:message key='customerFile.portalIdActive' /></td>
							
							<c:if test="${not empty partnerPrivate.id}">
								<td align="left">
								<input type="button" tabindex="24" value="Child Account" class="cssbuttonB" onclick="window.open('getChildAgents.html?id=${partnerPrivate.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
							</c:if>
							
						</tr>
						
					</table>
					</div>
					</td>
				</tr>	
			--%>
			<configByCorp:fieldVisibility componentId="component.field.partner.QualityMeasurementUTSI">			
			 <c:if test="${company.qualitySurvey==true}">  
			   <tr>						
				 <td colspan="20" width="100%" align="left" style="margin: 0px">
				
     				 <div onClick="javascript:animatedcollapse.toggle('QualityMeasurement')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					   <tr>
					      <td class="headtab_left">
					      </td>
					      <td NOWRAP class="headtab_center">&nbsp;&nbsp;Quality Measurement
					      </td>
					      <td width="28" valign="top" class="headtab_bg"></td>
					      <td class="headtab_bg_special">&nbsp;
					      </td>
					      <td class="headtab_right">
					      </td>
					 </tr>
				</table>
				</div> 
				<div id="QualityMeasurement">					
					<table class="detailTabLabel"  cellpadding="0">
					<tr><td height="5px"></td></tr>
							
					<c:if test="${company.justSayYes==true}">
					<tr>								
					<td align="left" class="listwhitetext" width="150"><b><u>Customer&nbsp;Feedback</u></b></td>														 	
					<td align="left" class="listwhitetext">
					<c:if test="${qualitySurvey=='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey!='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;	<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey=='checked' && justSayYes!='checked'}">
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />		
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" checked="checked" style="vertical-align:bottom" value="Quality Survey" onclick="GetSelectedItem(this.value)"/>
					</c:if>
					</td>
					</tr>
					</c:if>								
								<tr><td height="5px"></td></tr>
								<tr>
							  <td colspan="10">
							  <fieldset class="bluefieldset" style="width:820px;">
							  <table class="detailTabLabel">
							  <tr>
							    <c:if test="${partnerPrivate.noTransfereeEvaluation}">
								   <c:set var="ischeckedEvaluation" value="true" />
							    </c:if>
							    
							       <td align="right" class="listwhitetext" width="145px">No Transferee Evaluation</td>
							       <td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.noTransfereeEvaluation" fieldValue="true" value="${ischeckedEvaluation}" onchange="changeStatus();" tabindex="" /></td>
							      
							        <c:set var="ischeckeddddd" value="false" />
							        <c:if test="${partnerPrivate.oneRequestPerCF}">
								       <c:set var="ischeckedCF" value="true" />
							       </c:if>
							          <td align="right" class="listwhitetext" width="145">One Request Per CF</td>
							          <td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.oneRequestPerCF" onclick="changeStatus();" value="${ischeckedCF}"  /></td>
									<td width="45"></td>
									<td class="listwhitetext" >&nbsp;&nbsp;&nbsp;&nbsp;Rating Scale: </td><td><s:radio name="rateScale" list="%{ratingScale}"  /></td>
							 	</tr>
							  	</table>
							  	</fieldset>
							  	</td>
							  </tr>	
							  <tr><td colspan="10">
							<table style="margin-bottom:0px; width:100%">
								<tr><td height="5px"></td></tr>	  
								<tr>
								<td  class="bgblue" >Quality Survey results</td></tr>
							<tr><td height="5px">
							<table style="width:100%">
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;1</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description1" readonly="true" size="50" maxlength="50"  cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="110px">Link:&nbsp;1</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link1" onchange="checkURL(this,'1')" readonly="true" size="70" cssStyle="width:370px;" /></td>
							</tr>
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;2</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description2" readonly="true" size="50" maxlength="50" cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="60px">Link:&nbsp;2</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link2" onchange="checkURL(this,'2')" readonly="true" size="70" cssStyle="width:370px;" /></td>
							</tr>
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;3</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description3" readonly="true" size="50" maxlength="50" cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="60px">Link:&nbsp;3</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link3" onchange="checkURL(this,'3')" readonly="true" size="70" cssStyle="width:370px;" /></td>
							</tr>
							</table>
							
							</td></tr>
							</table>
							</td></tr>							  				  
							</table>
							</div>							
			</td>
			 </tr>	
		</c:if>		 
		</configByCorp:fieldVisibility>
		
<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('entitlement')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;Entitlement
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="entitlement" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">

<s:set name="entitlementList" value="entitlementList" scope="request"/>
<display:table name="entitlementList" class="table" requestURI="" id="entitlementList"  pagesize="10" style="margin:0px 0px 10px 0px; ">
    <display:column title="#" style="width:10px;"><c:out value="${entitlementList_rowNum}"/></display:column>
    <display:column title="Option" maxLength="70" style="width:100px;">
    <a href="javascript: void(0)" 
  	 onclick= "window.open('editEntitleDetails.html?id=${entitlementList.id}&decorator=popup&popup=true&partnerCode=${partnerPrivate.partnerCode}&partnerPrivateId=<%=request.getParameter("partnerId")%>','accountProfileForm','height=400,width=550,top=50,left=230, scrollbars=yes,resizable=yes').focus();">
    <c:out value="${entitlementList.eOption}"></c:out></a>
    </display:column>
    
    <display:column property="eDescription" title="Description" maxLength="120" style="width:160px;"></display:column>       
    <display:column property="updatedOn"  title="Updated On" format="{0,date,dd-MMM-yyyy}" style="width:70px;"/>
    <display:column property="updatedBy"  title="Updated By" style="width:70px;"/>
    <display:column title="Remove" style="width: 10px;" >
		<a><img align="middle" title="User List" onclick="confirmSubmit('${entitlementList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	</display:table>
	<c:if test="${not empty partnerPrivate.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editEntitleDetails.html?decorator=popup&popup=true&partnerCode=${partnerPrivate.partnerCode}&partnerPrivateId=<%=request.getParameter("partnerId")%>','accountProfileForm','height=400,width=550,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add" style="width:60px; height:25px" tabindex=""/> 
     </c:if>	
     <c:if test="${empty partnerPrivate.id}">
     <input type="button" class="cssbutton1" onclick="massageAddress();"
	    value="Add" style="width:60px; height:25px" tabindex="83"/> 
     </c:if>     
</div>
</td></tr> 
</tbody>
</table></div></td></tr>
<tr>
	<td height="10" width="100%" align="left" style="margin:0px">
	<div onClick="javascript:animatedcollapse.toggle('accountAssignmentType')" style="margin:0px">
     <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">&nbsp;&nbsp;Assignment&nbsp;Type
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
     </div>
		<div id="accountAssignmentType" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="100%">
 		  <tbody>
 		  <tr><td align="left" height="5px"></td></tr>
<tr><td align="left" colspan="10">
<div style=" width:100%; overflow-x:auto; overflow-y:auto;">
<s:set name="accountAssignmentTypeList" value="accountAssignmentTypeList" scope="request"/>
<display:table name="accountAssignmentTypeList" class="table" requestURI="" id="accountAssignmentTypeList"  pagesize="10" style="margin:0px 0px 10px 0px; ">
    <display:column title="#" style="width:10px;"><c:out value="${accountAssignmentTypeList_rowNum}"/></display:column>
    <display:column title="Assignment" maxLength="70" style="width:100px;">
    <a href="javascript: void(0)" 
  	 onclick= "window.open('editAccountAssignmentType.html?id=${accountAssignmentTypeList.id}&decorator=popup&popup=true&partnerCode=${partnerPrivate.partnerCode}&partnerPrivateId=<%=request.getParameter("partnerId")%>','accountProfileForm','height=400,width=550,top=50,left=230, scrollbars=yes,resizable=yes').focus();">
    <c:out value="${accountAssignmentTypeList.assignment}"></c:out></a>
    </display:column>    
    <display:column title="Description" maxLength="120" style="width:160px;">
    <a href="javascript: void(0)" 
  	 onclick= "window.open('editAccountAssignmentType.html?id=${accountAssignmentTypeList.id}&decorator=popup&popup=true&partnerCode=${partnerPrivate.partnerCode}&partnerPrivateId=<%=request.getParameter("partnerId")%>','accountProfileForm','height=400,width=550,top=50,left=230, scrollbars=yes,resizable=yes').focus();">
    <c:out value="${accountAssignmentTypeList.description}"></c:out></a>
    </display:column>       
    <display:column property="updatedOn"  title="Updated On" format="{0,date,dd-MMM-yyyy}" style="width:70px;"/>
    <display:column property="updatedBy"  title="Updated By" style="width:70px;"/>
    <display:column title="Remove" style="width: 10px;" >
		<a><img align="middle" title="User List" onclick="confirmSubmitForAssignment('${accountAssignmentTypeList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
	</display:column>
	</display:table>
	<c:if test="${not empty partnerPrivate.id}">
	<input type="button" class="cssbutton1" onclick="window.open('editAccountAssignmentType.html?decorator=popup&popup=true&partnerCode=${partnerPrivate.partnerCode}&partnerPrivateId=<%=request.getParameter("partnerId")%>','accountProfileForm','height=400,width=550,top=0, scrollbars=yes,resizable=yes').focus();" 
	        value="Add" style="width:60px; height:25px" /> 
     </c:if>	
     <c:if test="${empty partnerPrivate.id}">
     <input type="button" class="cssbutton1" onclick="massageAddress();"
	    value="Add" style="width:60px; height:25px" /> 
     </c:if>     
</div>
</td></tr> 
</tbody>
</table></div></td></tr>
     <tr>
     	<td height="10" width="100%" align="left" style="margin:0px">
	    <div onClick="javascript:animatedcollapse.toggle('InsuranceSetup')" style="margin:0px">
        <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
      <tr>
         <td class="headtab_left">
         </td>
         <td NOWRAP class="headtab_center">&nbsp;&nbsp;Insurance&nbsp;Setup
         </td>
         <td width="28" valign="top" class="headtab_bg"></td>
         <td class="headtab_bg_center">&nbsp;
         </td>
         <td class="headtab_right">
         </td>
      </tr>
      </table>
        </div>
        <div id="InsuranceSetup" class="switchgroup1">
		<table class="detailTabLabel" border="0" width="">
		 <tbody>
		 <tr>
          <td><s:checkbox name="partnerPrivate.lumpSum"  cssClass="input-text" onclick="showVlidField();" /></td>
          <td align="left" class="listwhitetext"><c:out value="${baseCurrency}"/><div id="Currency11" style="float:right;"><font color="red" size="2">*</font></div></td>
          <td width="90"><s:textfield name="partnerPrivate.lumpSumAmount" cssClass="input-text" size="10" maxlength="" /></td>
           <td align="left" class="listwhitetext">Per<div id="Per11" style="float:right;"><font color="red" size="2">*</font></div></td>
           <td width="80"><s:select cssClass="list-menu"   name="partnerPrivate.lumpPerUnit" list="{'Kilo', 'CBM', 'CFT', 'LB'}" headerKey="" headerValue="" cssStyle="width:60px"/></td>
            <td align="left" class="listwhitetext">Min Replacement value<div id="Min11" style="float:right;"><font color="red" size="2">*</font></div></td>
           <td><s:textfield name="partnerPrivate.minReplacementvalue" cssClass="input-text" size="15" maxlength="" /></td>
           
           <configByCorp:fieldVisibility componentId="component.field.partner.agentClassification">
           <td align="left" class="listwhitetext">Buy Rate %</td>
           <td><s:textfield name="partnerPrivate.buyRate" id="partnerPrivate.buyRate" cssClass="input-text" tabindex="0"  size="5" maxlength="5" onblur="validateRate(this)" /></td>
           <td align="left" class="listwhitetext">Sell Rate %</td>
           <td><s:textfield name="partnerPrivate.sellRate" cssClass="input-text" size="5" maxlength="5" onblur="validateRate(this)" /></td>
		  </configByCorp:fieldVisibility>
		 </tr>
		 <tr><td><s:checkbox name="partnerPrivate.detailedList"  cssClass="input-text"  /></td>
		 <td colspan="3" align="left" class="listwhitetext" >Detailed List</td>
		 </tr>
		 <tr><td><s:checkbox name="partnerPrivate.noInsurance"  cssClass="input-text"  /></td>
		 <td colspan="3" align="left" class="listwhitetext" >No Insurance</td>
		 </tr>
		 </tbody>
		</table>
		</div>
   </tr>
<tr>
				<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('default')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Default
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="default" >
							<table border="0" class="detailTabLabel">
							<tr>
							<td align="right" width="83px" class="listwhitetext"><fmt:message key="partnerPrivate.contract"/></td>
							<td  align="left" colspan="" ><s:select cssClass="list-menu" name="partnerPrivate.contract" list="%{partnerContractList}" cssStyle="width:130px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
							<td align="right" width="145px" class="listwhitetext" >Network&nbsp;</td>
			<td><s:textfield cssClass="input-text" id="networkPartnerCodeId" key="partnerPrivate.networkPartnerCode" size="7" cssStyle="width:55px;" maxlength="10" onchange="valid(this,'special');checkNetworkPartnerName();" tabindex="" onblur=" " onselect="" onkeypress=" " onfocus=" "/></td>
			<td width=""><img align="left" class="openpopup" width="" height="20" onclick="javascript:winOpenPartnerNetwork();" id="openpopupnetwork.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left" colspan=""><s:textfield name="partnerPrivate.networkPartnerName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" size="35" maxlength="250" tabindex=""/></td>
			<td align="right" width="101px" class="listwhitetext"><fmt:message key="partnerPrivate.source"/></td>
			<td align="left" class="listwhitetext" ></td>
			<td style="padding-right:10px;"><s:select cssClass="list-menu" name="partnerPrivate.source" list="%{lead}" cssStyle="width:260px" headerKey="" headerValue="" onchange="changeStatus();" /></td>				
			</tr>
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
		  	<td align="right" class="listwhitetext" width="80px"><fmt:message key="partnerPrivate.insuranceHas"/></td>
	  		<td align="left" colspan="1"><s:select cssClass="list-menu" name="partnerPrivate.insuranceHas" list="%{hasval}" cssStyle="width:130px" onchange="changeStatus();" tabindex=""/></td>
			<td align="right" class="listwhitetext" width="145px">Vendor&nbsp;</td>					
			<td align="left" class="listwhitetext"><s:textfield onchange="checkVendorName();" key="partnerPrivate.vendorCode" cssClass="input-text" readonly="false" cssStyle="width:55px;" size="7" maxlength="8" tabindex=""/></td>
			<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen1()" id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
			<td align="left" colspan=""><s:textfield name="partnerPrivate.vendorName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" size="35" maxlength="250" tabindex=""/></td>
			<td align="right" class="listwhitetext" width="105px"><fmt:message key="partnerPrivate.insuranceOptionCode"/></td>
			<td align="left" colspan=""><s:select cssClass="list-menu" name="partnerPrivate.insuranceOption" list="%{insopt}" cssStyle="width:260px" onchange="" tabindex=""/></td>
			</table>
			</td>													
			</tr>
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			<td align="right" class="listwhitetext" width="80px">VAT Desc</td>
				<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.defaultVat" id="euVat" list="%{euVatList}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:130px" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:13px"></td>	
			<td align="left" class="listwhitetext" width="" colspan="">Account PO#/SAP Number</td>
			<td width=""><s:textfield cssClass="input-text" name="partnerPrivate.billToAuthorization"  cssStyle="width:279px;" size="45" maxlength="50" onblur="valid(this,'special')" /></td>
			<td align="right" class="listwhitetext" width="105px" colspan="">NPS score</td>
			<td align="left"><s:textfield cssClass="input-textUpper" name="partnerPrivate.NPSscore" readonly="true"  size="15" maxlength="15" onblur="valid(this,'special')" /></td>
			</table></td>
			</tr>
			<configByCorp:fieldVisibility componentId="component.field.Alternative.rddDaysTS">
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			<td align="right" class="listwhitetext" width="80px">RDD Based</td>
			<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.rddBased" list="%{rddBasedList}" headerKey="" headerValue="" cssStyle="width:80px" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:1px"></td>	
			<td align="left" class="listwhitetext" width="" colspan=""># Of Days</td>
			<td width=""><s:textfield cssClass="input-text" name="partnerPrivate.rddDays" size="4" maxlength="10" onchange="isInteger(this)" /></td>
			</table></td>
			</tr>
			</configByCorp:fieldVisibility>
	
			
			</table>
		</td>
							</tr>			
			</tbody>
		</table>
		</td>
		</tr>
		</tbody>
		</table>							
		<table width="100%" border="0" style="margin:0px;padding:0px;">	     	
	<tr align="right">
	<td align="right" colspan="2"><input type="button" class="cssbutton1" value="Update Child Accounts" style="width:145px; height:25px;" onclick="openWindowMethod();"/></td><td></td>
	</tr>
	<tr align="right">
	<c:set var="isExcludeFromParent" value="false" />
	<c:if test="${partnerPrivate.excludeFromParentUpdate}">
	<c:set var="isExcludeFromParent" value="true" />
	</c:if>
	<td align="right" class="listwhitetext">Exclude from Parent updates</td>
	<td class="listwhitetext" width="8px" align="right"><s:checkbox key="partnerPrivate.excludeFromParentUpdate" id="excludeFPU" value="${isExcludeFromParent}" onclick="updatePartnerPrivate();" fieldValue="true"/></td>
	</tr>	
	<tr><td height="20"></td></tr>											
	</table>
		</div>				
	</c:if>
	<configByCorp:fieldVisibility componentId="component.tab.partnerPrivate.excludeFinancialExt">
	<c:if test="${partnerType != 'AG' && partnerType != 'VN' && partnerType != 'AC'}">
	<s:hidden name="partnerPrivate.excludeFinancialExtract" />
	</c:if>
	</configByCorp:fieldVisibility>
	<!-- End AccoutType Section	-->
	 <c:if test="${partnerType == 'AG' || partnerType == 'CR' || partnerType == 'VN'}">
	<s:hidden name="partnerPrivate.lumpSum"></s:hidden>
	<s:hidden name="partnerPrivate.lumpSumAmount" ></s:hidden>
	<s:hidden name="partnerPrivate.lumpPerUnit"></s:hidden>
	<s:hidden name="partnerPrivate.minReplacementvalue"></s:hidden>
	<s:hidden name="partnerPrivate.detailedList"></s:hidden>
	<s:hidden name="partnerPrivate.noInsurance"></s:hidden>
	</c:if>
	<c:if test="${partnerType == 'AG' || partnerType == 'CR' || partnerType == 'VN'}">
		<div class="center-content">
		<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
			<tbody>
				<tr>
					<td align="left" class="listwhitetext">				
					<table class="detailTabLabel" border="0" cellspacing="0" cellpadding="0" width="100%">						
						<s:hidden name="partner.isBroker" value="false" />
						<s:hidden name="partner.qc" value="false" />
						<s:hidden name="partnerPrivate.portalDefaultAccess"/>
		<c:choose>
		<c:when test="${qualityAgent==true || qualityAgent=='true'}">
						<c:if test="${partnerType != 'AG'}">
						<s:hidden name="partnerPrivate.noTransfereeEvaluation"/>
						<s:hidden name="partnerPrivate.oneRequestPerCF"/>
						<s:hidden name="partnerPrivate.ratingScale"/>
						</c:if>								
		</c:when>
		<c:otherwise>
						<s:hidden name="partnerPrivate.noTransfereeEvaluation"/>
						<s:hidden name="partnerPrivate.oneRequestPerCF"/>
						<s:hidden name="partnerPrivate.ratingScale"/>
		
		</c:otherwise>
		</c:choose>
						
						<tr>
							<td colspan="7">
							<table style="padding-bottom: 0px; margin-bottom:3px" width="" border="0">
								<tr>
									<td align="right" class="listwhitetext"><fmt:message key='partner.partnerCode' /><font color="red" size="2">*</font></td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.partnerCode" required="true" cssClass="input-textUpper" maxlength="8" size="4" cssStyle="width:60px;" readonly="true" tabindex="1"/></td>
									
									<td align="right" class="listwhitetext" style="width:70px; !width:135px">Local&nbsp;Name</td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.localName" required="true" cssClass="input-text" maxlength="95" size="30" /></td>

									
									<td align="right" class="listwhitetext" width="40px">Name<font color="red" size="2">*</font></td>
									<s:hidden key="partnerPrivate.firstName" />
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.lastName" required="true" cssClass="input-textUpper" size="30" maxlength="80" readonly="true" tabindex="2" /></td>									<td align="right" class="listwhitetext">Status</td>
									<c:if test="${partnerType == 'AG' }">
									<td><s:select cssClass="list-menu" name="partnerPrivate.status" list="%{partnerStatus}" cssStyle="width:80px" onchange="return checkStatus(this),changeStatus();" tabindex="3"/></td>
									</c:if>
									<c:if test="${partnerType != 'AG' }">
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.status" required="true" cssClass="input-textUpper" size="7" readonly="true" tabindex="3"/></td>
									</c:if>
									<td align="right" class="listwhitetext" width="77px">External&nbsp;Ref.</td>
									<td align="left" class="listwhitetext" colspan=""><s:textfield key="partnerPrivate.extReference" required="true" cssClass="input-text" maxlength="15" size="8" tabindex="4" /></td>
									<c:if test="${empty partnerPrivate.id}">
										<td align="right" style="width:100px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
									</c:if>
									<c:if test="${not empty partnerPrivate.id}">
										<c:choose>
											<c:when test="${partnerNotes == '0' || partnerNotes == '' || partnerNotes == null}">
										<td align="right" style="width:100px"><img id="partnerNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
											</c:when>
											<c:otherwise>
											<td align="right" style="width:100px"><img id="partnerNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${partnerPrivate.id }&notesId=${partnerPrivate.partnerCode}&noteFor=Partner&imageId=partnerNotesImage&fieldId=partnerNotes&decorator=popup&popup=true',800,600);" ></a></td>
											</c:otherwise>
										</c:choose> 
									</c:if>
								</tr>
							</table>
							</td>
						</tr>
				<c:if test="${partnerType == 'AG'}">
				<c:if test="${qualityAgent==true || qualityAgent=='true'}">
			 <c:if test="${company.qualitySurvey==true}">  
			   <tr>						
				 <td colspan="20" width="100%" align="left" style="margin: 0px">
				
     				 <div onClick="javascript:animatedcollapse.toggle('QualityMeasurement')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					   <tr>
					      <td class="headtab_left">
					      </td>
					      <td NOWRAP class="headtab_center">&nbsp;&nbsp;Quality Measurement
					      </td>
					      <td width="28" valign="top" class="headtab_bg"></td>
					      <td class="headtab_bg_special">&nbsp;
					      </td>
					      <td class="headtab_right">
					      </td>
					 </tr>
				</table>
				</div> 
				<div id="QualityMeasurement">					
					<table class="detailTabLabel"  cellpadding="0">
					<tr><td height="5px"></td></tr>
							
					<c:if test="${company.justSayYes==true}">
					<tr>								
					<td align="left" class="listwhitetext" width="150"><b><u>Customer&nbsp;Feedback</u></b></td>														 	
					<td align="left" class="listwhitetext">
					<c:if test="${qualitySurvey=='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey!='checked' && justSayYes=='checked'}">			
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" checked="checked" onclick="GetSelectedItem(this.value)" />
						&nbsp;&nbsp;Quality Survey &nbsp;	<input type="radio" name="partnerPrivateCustomerFeedback" value="Quality Survey" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />
					</c:if>
					<c:if test="${qualitySurvey=='checked' && justSayYes!='checked'}">
						&nbsp;&nbsp;Just Say Yes &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" value="Just Say Yes" style="vertical-align:bottom" onclick="GetSelectedItem(this.value)" />		
						&nbsp;&nbsp;Quality Survey &nbsp;<input type="radio" name="partnerPrivateCustomerFeedback" checked="checked" style="vertical-align:bottom" value="Quality Survey" onclick="GetSelectedItem(this.value)"/>
					</c:if>
					</td>
					</tr>
					</c:if>								
								<tr><td height="5px"></td></tr>
								<tr>
							  <td colspan="10">
							  <fieldset class="bluefieldset" style="width:820px;">
							  <table class="detailTabLabel">
							  <tr>
							    <c:if test="${partnerPrivate.noTransfereeEvaluation}">
								   <c:set var="ischeckedEvaluation" value="true" />
							    </c:if>
							    
							       <td align="right" class="listwhitetext" width="145px">No Transferee Evaluation</td>
							       <td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.noTransfereeEvaluation" fieldValue="true" value="${ischeckedEvaluation}" onchange="changeStatus();" tabindex="" /></td>
							      
							        <c:set var="ischeckeddddd" value="false" />
							        <c:if test="${partnerPrivate.oneRequestPerCF}">
								       <c:set var="ischeckedCF" value="true" />
							       </c:if>
							          <td align="right" class="listwhitetext" width="145">One Request Per CF</td>
							          <td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.oneRequestPerCF" onclick="changeStatus();" value="${ischeckedCF}"  /></td>
									<td width="45"></td>
									<td class="listwhitetext" >&nbsp;&nbsp;&nbsp;&nbsp;Rating Scale: </td><td><s:radio name="rateScale" list="%{ratingScale}"  /></td>
							 	</tr>
							  	</table>
							  	</fieldset>
							  	</td>
							  </tr>	
							  <tr><td colspan="10">
							<table style="margin-bottom:0px; width:100%">
								<tr><td height="5px"></td></tr>	  
								<tr>
								<td  class="bgblue" >Quality Survey results</td></tr>
							<tr><td height="5px">
							<table style="width:100%">
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;1</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description1" readonly="true" size="50" maxlength="50" cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="110px">Link:&nbsp;1</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link1" onchange="checkURL(this,'1')" readonly="true" size="70" cssStyle="width:370px;"/></td>
							</tr>
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;2</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description2" readonly="true" size="50" maxlength="50" cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="60px">Link:&nbsp;2</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link2" onchange="checkURL(this,'2')" readonly="true" size="70" cssStyle="width:370px;" /></td>
							</tr>
							<tr>
							<td align="left" class="listwhitetext" width="60px">Description:&nbsp;3</td>
							<td class="listwhitetext" width=""><s:textfield cssClass="input-text"  name="partnerPrivate.description3" readonly="true" size="50" maxlength="50" cssStyle="width:270px;" /></td>
							<td align="right" class="listwhitetext" width="60px">Link:&nbsp;3</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.link3" onchange="checkURL(this,'3')" readonly="true" size="70" cssStyle="width:370px;" /></td>
							</tr>
							</table>
							
							</td></tr>
							</table>
							</td></tr>							  				  
							</table>
							</div>							
			</td>
			 </tr>	
		</c:if>		 
		</c:if>
				<tr>						
					<td colspan="20" width="100%" align="left" style="margin: 0px">
     				    <div onClick="javascript:animatedcollapse.toggle('DefaultRoles')" style="margin: 0px">
					      <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					         <tr>
					             <td class="headtab_left">
					             </td>
					             <td NOWRAP class="headtab_center">&nbsp;&nbsp;Default Roles
					             </td>
					             <td width="28" valign="top" class="headtab_bg"></td>
					             <td class="headtab_bg_special">&nbsp;
					             </td>
					              <td class="headtab_right">
					              </td>
					       </tr>
					    </table>
					  </div>
							<div id="DefaultRoles" class="DefaultRoles1">
							<table class="detailTabLabel">
							<tr>
								<td align="right" class="listwhitetext" width="100"><fmt:message key='accountInfo.accountHolder' /></td>
								<td class="listwhitetext">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.accountHolder" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select cssClass="list-menu" name="partnerPrivate.accountHolder" list="%{sale}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" width="100" class="listwhitetext">Move&nbsp;Manager</td>
								<td colspan="2">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.accountManager" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.accountManager" cssClass="list-menu" list="%{coordinator}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right"  width="103"  class="listwhitetext">Pricing</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.pricingUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.pricingUser" cssClass="list-menu" list="%{pricing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right"  width="103"  class="listwhitetext">Claims</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.claimsUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.claimsUser" cssClass="list-menu" list="%{claimsUser}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
							<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
							<td align="right"  width="103"  class="listwhitetext">USA&nbsp;Flag&nbsp;Required</td>
							    <td><s:checkbox key="partnerPrivate.usaFlagRequired" tabindex=""/></td>
							</configByCorp:fieldVisibility>
							</tr>
							<tr>					
								<td align="right" class="listwhitetext">Billing</td>
								<td align="left">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.billingUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.billingUser" cssClass="list-menu" list="%{billing}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Payable</td>
								<td colspan="2">
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.payableUser" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select cssClass="list-menu" name="partnerPrivate.payableUser" list="%{payable}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Auditor</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.auditor" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.auditor" cssClass="list-menu" list="%{auditor}" cssStyle="width:135px" headerKey="" headerValue="" onchange="changeStatus();" />
									</c:otherwise>
								</c:choose>
								</td>
								<td align="right" class="listwhitetext">Coordinator</td>
								<td>
								<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.coordinator" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.coordinator"  list="%{coordinatorList}" onchange="" cssStyle="width:135px" headerKey="" headerValue="" cssClass="list-menu" />
									</c:otherwise>
								</c:choose>
								</td>
								<configByCorp:fieldVisibility componentId="component.field.billing.internalBillingPerson">
									<td align="right">Internal&nbsp;Billing</td>
									<td>
									<c:choose>
									<c:when test="${defaultRolesEnable ==false}">
										<s:textfield key="partnerPrivate.internalBillingPerson" required="true" cssClass="input-textUpper" cssStyle="width:135px" readonly="true" tabindex=""/>
									</c:when>
									<c:otherwise>
										<s:select name="partnerPrivate.internalBillingPerson" cssClass="list-menu" list="%{internalBillingPersonList}" cssStyle="width:135px" headerKey="" headerValue=""/>
									</c:otherwise>
									</c:choose>
									</td>
								</configByCorp:fieldVisibility>								
							    </tr>
								</table>								
								<table style="margin:0px;padding:0px;">
								<tr>
								<td colspan="12">
								<fieldset style="padding:5px 3px 7px 10px;">
								<legend>Additional Contact Info:</legend>
								<table style="margin:0px;padding:0px;">
								<tr>
								<td align="right" class="listwhitetext" style="width:86px;">&nbsp;Default&nbsp;Contacts</td>
								<td>
								<s:select name="partnerPrivate.defaultContact"  list="%{defaultContact}" onchange="autoPopulate_defaultContactInfo(this);" cssStyle="width:135px" headerKey="" headerValue="" cssClass="list-menu" />
								 </td>
							    <td width="25" ></td>
								<td align="right" class="listwhitetext">Contact Name:</td>
								<td><s:textfield key="partnerPrivate.contactName" required="true"  cssClass="input-text" cssStyle="width:132px"  tabindex=""/></td>
								<td width="70" ></td>
								<td align="right" class="listwhitetext">Phone</td>
								<td><s:textfield key="partnerPrivate.phone" required="true"  cssClass="input-text" cssStyle="width:130px"  tabindex="" /></td>
								 <td width="75" ></td>
								<td align="right" class="listwhitetext">Email</td>
								<td><s:textfield key="partnerPrivate.email" required="true"  cssClass="input-text" cssStyle="width:132px"  tabindex="" onchange="globalEmail()" /></td>
								</tr>
								</table>
								</fieldset>
								</td>
								</tr>
								</table>
							</div>	
					 </td>
				</tr>
			</c:if>	
						<tr>						
						<td colspan="20" width="100%" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('accDefault')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Accounting Default
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
							<div id="accDefault" class="switchgroup1">						
						<table border="0" class="detailTabLabel">						
						<tr>
						<td>
						<table border="0" class="detailTabLabel">						
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='partner.accountingDefault' /></td>
							<td class="listwhitetext" width="8px" colspan="2"><s:checkbox key="partnerPrivate.accountingDefault" onclick="changeStatus();" tabindex="5"/></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.acctDefaultJobType' /></td>
							<td><s:textfield cssClass="input-text" name="partnerPrivate.acctDefaultJobType" cssStyle="width:165px" size="33" maxlength="50" tabindex="6" /></td>
						</tr>
						<tr>
							<s:hidden name="partner.rank" />
							<td align="right" class="listwhitetext"><fmt:message key='accountInfo.paymentMethod' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility></td>
							<td colspan="2" class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.paymentMethod" list="%{paytype}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="7" /></td>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billPayOption' /></td>
							<td colspan="3"><s:select name="partnerPrivate.billPayOption" list="%{payopt}" cssClass="list-menu" cssStyle="width:169px" onchange="changeStatus();" tabindex="8" /></td>
						</tr>
							<c:if test="${partnerType == 'AG'}">
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='accountInfo.storage' /></td>
							<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.storageBillingGroup" list="%{billgrp}" cssStyle="width:170px" onchange="changeStatus();" tabindex="" /></td>
						    <td></td>
						    <configByCorp:fieldVisibility componentId="component.tab.partnerPrivate.AllPartnerTRN">
						     <td align="right" class="listwhitetext" width="">TRN No.</td>
						     <td class="listwhitetext"><s:textfield cssClass="input-text" name="partnerPrivate.mc" size="25" cssStyle="width:165px"  maxlength="15" /></td>
						    </configByCorp:fieldVisibility>
						       
						</tr>
							</c:if>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='partner.abbreviation' /></td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPrivate.abbreviation" cssStyle="width:166px;" size="26" maxlength="15" tabindex="9"/></td>
							<td align="right" class="listwhitetext">Class Code</td>
									<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.classcode" list="%{classcode}" cssStyle="width:169px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="10"/></td>
									<td></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext"><fmt:message key='partner.billingInstructionCode' /></td>
							<td align="left" colspan="2"><s:select cssClass="list-menu" id="billingInstructionCode" name="partnerPrivate.billingInstruction" list="%{billinst}" cssStyle="width:170px;" headerKey="" headerValue="" onchange="changeStatus();" tabindex="11" /></td>							
						<td class="listwhitetext" width="200px" align="right">Storage Email Address</td>
						<td><s:textfield cssClass="input-text" name="partnerPrivate.storageEmail" maxlength="50" size="33" cssStyle="width:165px;" onchange="checkEmail()" /></td>	     
						</tr>
						<tr>
							<td align="right" class="listwhitetext">Collection</td>
							<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.collection" list="%{collectionList}" cssStyle="width:170px;" headerKey="" headerValue="" onchange="changeStatus();"/></td>
							
							<td align="right" class="listwhitetext"></td>	
							<td align="right" class="listwhitetext">Storage Email Type</td>
							<td class="listwhitetext"><s:select cssClass="list-menu" name="partnerPrivate.storageEmailType" list="%{storageEmailType}" cssStyle="width:169px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
						</tr>
						<tr>
							<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
							<td align="right" class="listwhitetext">Default stationary</td>	
							<td colspan="2"><s:select cssClass="list-menu" name="partnerPrivate.emailPrintOption" list="%{printOptions}" cssStyle="width:170px"  value=" "></s:select>	</td>		
						    </configByCorp:fieldVisibility>
						    <td align="right" class="listwhitetext">Billing Moment</td>
							<td align="left">
							 	<s:select cssClass="list-menu" name="partnerPrivate.billingMoment" list="%{billingMomentList}" cssStyle="width:170px" headerKey="" headerValue="" onchange="changeStatus();" />
							</td>
				<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
			
		  	<td align="right" class="listwhitetext"></td>
			<td align="right" class="listwhitetext">VAT&nbsp;Billing&nbsp;Group <configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"></configByCorp:fieldVisibility></td>
				<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.vatBillingGroup" id="vatBillingGroup" list="%{vatBillingGroups}" onchange="changeStatus();vatBilling(this);" headerKey="" headerValue="" cssStyle="width:170px" tabindex=""/></td>
			
			</configByCorp:fieldVisibility>
						</tr>						
						</table>
						</td>
						</tr>
						
						<tr>
							<c:set var="ischeckeddddd" value="false" />
							<c:if test="${partnerPrivate.invoiceUploadCheck}">
								<c:set var="ischeckeddddd" value="true" />
							</c:if>
							<td colspan="7"  style="margin: 0px;padding: 0px;">
								<table border="0" align="left" class="detailTabLabel" style="margin: 0px 32px 0px 0px; padding: 0px;">
									<tr>
									<td colspan="7" align="left">
									<table class="detailTabLabel" border="0" style="margin: 0px;padding: 0px;">
									<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.converted">							
										<c:set var="isConverted" value="false" />
										<c:if test="${partnerPrivate.converted}">
											<c:set var="isConverted" value="true" />
										</c:if>
										<tr>										
										<td align="right" class="listwhitetext" width="45px">Converted</td>
										<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.converted" fieldValue="true" value="${isConverted}" onchange="changeStatus();"  /></td>
										</tr>
										</configByCorp:fieldVisibility>
									<tr>
										<td align="right" class="listwhitetext" width="140">Don't send to invoice upload</td>
										<td class="listwhitetext" width="8px" ><s:checkbox key="partnerPrivate.invoiceUploadCheck" onclick="changeStatus();" value="${ischeckeddddd}" fieldValue="true" tabindex="13" /></td>
										<c:set var="ischeckedddd" value="false" />
										<c:if test="${partnerPrivate.payableUploadCheck}">
											<c:set var="ischeckedddd" value="true" />
										</c:if>
										<td align="right" class="listwhitetext" width="420">Don't send to payable upload</td>
										<td class="listwhitetext" width="8px" align="right"><s:checkbox key="partnerPrivate.payableUploadCheck" onclick="changeStatus();" value="${ischeckedddd}" fieldValue="true" tabindex="14" /></td>
										<configByCorp:fieldVisibility componentId="component.tab.partnerPrivate.excludeFinancialExt">
										<c:set var="isexcludeFinancialExtract" value="false" />
										<c:if test="${partnerPrivate.excludeFinancialExtract}">
										<c:set var="isexcludeFinancialExtract" value="true" />
										</c:if>
										<c:if test="${partnerType == 'AG' || partnerType == 'VN'}">
										<td align="right" class="listwhitetext" width="275">Exclude&nbsp;Financial&nbsp;Extract</td>
										<td class="listwhitetext" width="10px"><s:checkbox key="partnerPrivate.excludeFinancialExtract" onclick="changeStatus();" value="${isexcludeFinancialExtract}" fieldValue="true" tabindex="14" /></td>
										</c:if>
										</configByCorp:fieldVisibility>
										
									</tr>
									<tr>
										<c:set var="ischeckedStopNot" value="false" />
										<c:if test="${partnerPrivate.stopNotAuthorizedInvoices}">
											<c:set var="ischeckedStopNot" value="true" />
										</c:if>
										<td align="right" class="listwhitetext" width="">Stop Not Authorized Invoices</td>
										<td class="listwhitetext" width=""><s:checkbox key="partnerPrivate.stopNotAuthorizedInvoices" onclick="changeStatus();" value="${ischeckedStopNot}" fieldValue="true" tabindex="15" /></td>
										
										<c:set var="ischeckedDoNotCopy" value="false" />
										<c:if test="${partnerPrivate.doNotCopyAuthorizationSO}">
											<c:set var="ischeckedDoNotCopy" value="true" />
										</c:if>
										<td align="right" class="listwhitetext" width="">Don't Copy Authorization SO</td>
										<td class="listwhitetext" width="8px" align="right"><s:checkbox key="partnerPrivate.doNotCopyAuthorizationSO" onclick="changeStatus();" value="${ischeckedDoNotCopy}" fieldValue="true" tabindex="16" /></td>
									</tr>
									<tr>
										<c:set var="ischeckedEmailAlertSent" value="false" />
										<c:if test="${partnerPrivate.emailSentAlert}">
											<c:set var="ischeckedEmailAlertSent" value="true" />
										</c:if>
										<td align="right" class="listwhitetext" width="">Send Email Alert</td>
										<td class="listwhitetext" width=""><s:checkbox key="partnerPrivate.emailSentAlert" onclick="changeStatus();" value="${ischeckedEmailAlertSent}" fieldValue="true" tabindex="17" /></td>
										<c:set var="ischeckedSurveyEmailSent" value="false" />
										<c:if test="${partnerPrivate.customerSurveyEmail}">
											<c:set var="ischeckedSurveyEmailSent" value="true" />
										</c:if>
										<td align="right" class="listwhitetext" width="">Customer Survey Email Alert</td>
										<td class="listwhitetext" width=""><s:checkbox key="partnerPrivate.customerSurveyEmail" onclick="changeStatus();" value="${ischeckedSurveyEmailSent}" fieldValue="true" tabindex="17" /></td>
									</tr>
									
									   <tr>
                            <td align="right" class="listwhitetext" width="235">Don't invoice</td>
                            <td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.doNotInvoice" fieldValue="true" tabindex="" /></td>
                            <c:if test="${partnerType == 'AG' || partnerType == 'AC'}">		
                            <td align="right" class="listwhitetext" width="235"> Just Say Yes Email</td>
                            <td class="listwhitetext" width="10px" align="left"><s:checkbox key="partnerPrivate.doNotSayYesEmail"  tabindex="" /></td>
                            </c:if>
                        </tr>
									
									<c:if test="${partnerType == 'AG' || partnerType == 'AC'}">						
									<tr>
					        			<td align="right" width="155" class="listwhitetext">Minimum&nbsp;Margin&nbsp;%&nbsp;</td>
						    			<td><input type="text" class="input-text" oncopy="return false" onpaste="return false" name="partnerPrivate.minimumMargin" id="minimumMargin" value="${partnerPrivate.minimumMargin}" maxlength="8" size="8" Style="width:78px;" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter()"></td>
						    			<td></td>
						    				
									</tr>
									</c:if>
									</table>
									</td>
									</tr>
									</table>
									</div>
									</td>
									</tr>										
								</table>
							</td>
							</tr>
							
							
							<tr>						
						<td colspan="20" width="" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('CreditDetails')" style="margin: 0px">
					   <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Credit Details
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_special">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					  </div>
									<div id="CreditDetails">								
											<table border="0" class="detailTabLabel">
												<tr>
													<c:set var="ischeckedCredit" value="false" />
													<c:if test="${partnerPrivate.creditCheck}">
														<c:set var="ischeckedCredit" value="true" />
													</c:if>
													
													<td align="right" class="listwhitetext" width="98px">Credit&nbsp;Check</td>
													<td class="listwhitetext" colspan="" width="10px"><s:checkbox key="partnerPrivate.creditCheck" fieldValue="true" value="${ischeckedCredit}" onclick="creditDate(this);enableDisable(this);" tabindex="18" /></td>
													
													<td align="right" class="listwhitetext" width="75px">Credit&nbsp;Amount</td>
													<td class="listwhitetext" id="coordRequiredTrue" width="80px"><s:textfield cssClass="input-text"  name="partnerPrivate.creditAmount" size="10" maxlength="15" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>
										            <td class="listwhitetext" id="coordRequiredFalse" width="80px"><s:textfield cssClass="input-textUpper"  name="partnerPrivate.creditAmount1" size="10" maxlength="15" readonly="true" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;" tabindex="19"/></td>
													<td align="right" class="listwhitetext" width="85px">Credit&nbsp;Currency</td>
													<td class="listwhitetext" id="coordRequiredTrue1" width="112px"><s:select cssClass="list-menu"  name="partnerPrivate.creditCurrency" list="%{currency}" cssStyle="width:110px;margin-top:5px;" headerKey="" headerValue="" onchange="changeStatus();" tabindex="20" /></td>
										            <td class="listwhitetext" id="coordRequiredFalse1" width="112px"><s:select cssClass="list-menu"  name="partnerPrivate.creditCurrency1" list="%{currency}" cssStyle="width:110px;margin-top:5px;" headerKey="" headerValue="" disabled="true" onchange="changeStatus();" tabindex="20" /></td>
										
													<td align="right" class="listwhitetext" width="90px">Credit&nbsp;Date&nbsp;Check</td>
													<td align="left" colspan="2" valign="top">
													<table class="detailTabLabel" border="0">
													<tr>
													
											<c:if test="${not empty partnerPrivate.creditDateCheck}">
												<s:text id="creditDateCheckFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPrivate.creditDateCheck" /></s:text>
												<td width="50px"><s:textfield cssClass="input-text" id="creditDateCheck" readonly="true" name="partnerPrivate.creditDateCheck" value="%{creditDateCheckFormattedValue}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="creditDate(true);" tabindex=""/></td>
												<td align="left" id="coordRequiredTrue2" width="20px" style="padding-top:2px;vertical-align:bottom;position:relative;"><img id="creditDateCheck_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" /></td>
												<td align="left" id="coordRequiredFalse2" width="10px" style="padding-top:1px;"></td>
											</c:if>
											<c:if test="${empty partnerPrivate.creditDateCheck}">
												<td width="50px"><s:textfield cssClass="input-text" id="creditDateCheck" readonly="true" name="partnerPrivate.creditDateCheck" size="7" maxlength="11" onkeydown="return onlyDel(event,this)" onblur="creditDate(true);" tabindex=""/></td>
												<td align="left" id="coordRequiredTrue2" width="20px" style="padding-top:2px;vertical-align:bottom;position:relative;"><img id="creditDateCheck_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;" /></td>
												<td align="left" id="coordRequiredFalse2" width="10px" style="padding-top:1px;"></td>
											</c:if>
													</tr>
													</table>
													</td>
													
													<td align="right" class="listwhitetext" width="75px">Credit&nbsp;Expiry</td>
														<c:if test="${not empty partnerPrivate.creditExpiry}">
														<s:text id="creditExpiryFormattedValue" name="${FormDateValue}"><s:param name="value" value="partnerPrivate.creditExpiry" /></s:text>
														<td width="50px"><s:textfield cssClass="input-textUpper" id="creditExpiry" readonly="true" name="partnerPrivate.creditExpiry" value="%{creditExpiryFormattedValue}" cssStyle="width:77px;" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="creditDate(true);" onchange="creditDate(true);" tabindex=""/></td>
														
													</c:if>
													<c:if test="${empty partnerPrivate.creditExpiry}">
														<td width="50px"><s:textfield cssClass="input-textUpper" id="creditExpiry" readonly="true" name="partnerPrivate.creditExpiry" cssStyle="width:77px;" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="creditDate(true);" onchange="creditDate(true);" tabindex="" /></td>
													
													</c:if>
																					
												</tr>
												<tr>
									<td colspan="4">
									<table class="detailTabLabel">
									<tr>												
									<td align="right" class="listwhitetext" width="98px">Available&nbsp;Credit</td>
									<td class="listwhitetext" id="coordRequiredTrue12"><s:textfield  cssClass="input-text" name="partnerPrivate.availableCredit" size="10" maxlength="15" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>
									<td class="listwhitetext" id="coordRequiredFalse12"><s:textfield  cssClass="input-textUpper" name="partnerPrivate.availableCredit1" size="10" maxlength="15" readonly="true" onchange="onlyFloat(this);" cssStyle="vertical-align:baseline;text-align:right;"/></td>																									
									<td align="left">
									<input type="button" value="Available Credit" style="width:100px; name="availableCreditButton" class="cssbuttonB" onclick="pickAvailableCredit();"/></td>	
									</tr>
									</table>
									</td>
									</tr>
											
							
							
							<configByCorp:fieldVisibility componentId="component.field.partner.partnerCreditTermsStar">
								<tr>
									     <td colspan="9"></td>
									     <td align="right" class="listwhitetext" style="padding-right: 2px;"><fmt:message key='accountInfo.creditTerms' /><configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit"><font color="red" size="2">*</font></configByCorp:fieldVisibility>
									     <configByCorp:fieldVisibility componentId="component.field.partner.bankDetails">
									     <c:if test="${partnerType == 'AG'}">
									     <font color="red" size="2">*</font>
									     </c:if>
									     </configByCorp:fieldVisibility>
									     </td>
									     <td class="listwhitetext" width=""><s:select cssClass="list-menu" name="partnerPrivate.creditTerms" list="%{creditTerms}" cssStyle="width:81px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" /></td>
								</tr>
							</configByCorp:fieldVisibility>
							<configByCorp:fieldVisibility componentId="component.field.partner.partnerCreditTermsSscw">
								<s:hidden name="partnerPrivate.creditTerms"></s:hidden>
							</configByCorp:fieldVisibility>
						</tr>
						
					</table>
					</div>
					</td>
				</tr>	
			<c:if test="${partnerType == 'VN'}">
			<configByCorp:fieldVisibility componentId="component.field.partner.monthlySalesGoalView">
			<tr>						
				<td colspan="20" width="" align="left" style="margin: 0px">
     				<div onClick="javascript:animatedcollapse.toggle('monthlySalesGoal');findMonthlySalesGoalDetails();" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Monthly Sales Goal
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
					</div>
						<div id="monthlySalesGoal">								
								<div id="monthlySalesGoalDetailsAjax">
								</div>
						</div>
				</td>
			</tr>
			</configByCorp:fieldVisibility>	
			</c:if>	
    <!--Information new page start -->
     <%
 String  bankInformationAccess="true";
 %>
 <sec-auth:authComponent componentId="module.partnerPrivate.edit.bankInformation">
 <% 
 bankInformationAccess="flase";
 %>
 </sec-auth:authComponent>
     <c:if test="${partnerType == 'AG'}">
   <tr>
   <td height="10" width="100%" align="left" style="margin: 0px">
  
   <div  onClick="javascript:animatedcollapse.toggle('information')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Information
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						
						
						<div id="information">
						
						<table border="0" class="detailTabLabel">
					<tr>
					<td align="right" class="listwhitetext" width="82">Currency </td>
		    	    <td align="left">
					<%if(bankInformationAccess.equalsIgnoreCase("flase")){ %>	    	    
		    	    	<s:select name="partnerPrivate.privateCurrency" list="%{currency}" cssClass="list-menu" cssStyle="width:59px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="" />
		    	    <%}else{%>
		    	    	<s:textfield cssClass="input-textUpper" name="partnerPrivate.privateCurrency" cssStyle="width:59px" readonly="true"/>
		    	    <%}%>
		    	    </td>
		    	    <td></td>
					<td align="right" class="listwhitetext">VAT #
					</td>
					<td class="listwhitetext" colspan="">
					<%if(bankInformationAccess.equalsIgnoreCase("flase")){ %>					
					<s:textfield cssClass="input-text" name="partnerPrivate.privateVatNumber" size="25" maxlength="20" tabindex="10" onkeyup="regExMatch(this,event);"/>
					<%}else{%>
					<s:textfield cssClass="input-textUpper" name="partnerPrivate.privateVatNumber" size="25" maxlength="20" tabindex="10" onkeyup="regExMatch(this,event);" readonly="true"/>
					<%}%>
					</td>					
					<td align="right" class="listwhitetext" width="72">Bank Code
					</td>
					<td class="listwhitetext">					
					<%if(bankInformationAccess.equalsIgnoreCase("flase")){ %>					
					<s:textfield cssClass="input-text" name="partnerPrivate.privateBankCode" size="20" maxlength="20" tabindex="10" onkeyup="regExMatch(this,event);" />
					<%}else{%>
					<s:textfield cssClass="input-textUpper" name="partnerPrivate.privateBankCode" size="20" maxlength="20" tabindex="10" onkeyup="regExMatch(this,event);" readonly="true"/>
					<%}%>
					</td>	
					<td align="right" class="listwhitetext">Bank Account #</td>
					<td class="listwhitetext">
					<%if(bankInformationAccess.equalsIgnoreCase("flase")){ %>					
					<s:textfield cssClass="input-text" name="partnerPrivate.privateBankAccountNumber" size="35" maxlength="40" tabindex="12" cssStyle="width:204px;" onkeyup="regExMatch(this,event);" />
					<%}else{%>
					<s:textfield cssClass="input-textUpper" name="partnerPrivate.privateBankAccountNumber" size="35" maxlength="40" tabindex="12" cssStyle="width:204px;" onkeyup="regExMatch(this,event);" readonly="true"/>
					<%}%>	
					<img align="top" width="17" height="20" onclick="bankInfoList('${partnerPrivate.partnerCode}',this); reposition();" src="/redsky/images/plus-small.png">				
					</td>
					<configByCorp:fieldVisibility componentId="component.partner.rutTaxNumber">
					<td align="right" class="listwhitetext" width="72">RUT#&nbsp;</td>
					<td class="listwhitetext">					
					<s:textfield cssClass="input-text" name="partnerPrivate.rutTaxNumber" id="rutTaxNumber" size="25" maxlength="30" />
					</td>
					</configByCorp:fieldVisibility>
					</tr>
						</table>
						
						</div>
						
   </td>
   </tr>
   </c:if>
   <!--Information new page end-->
				
				<tr>
				<td height="10" width="100%" align="left" style="margin: 0px">
  				<div  onClick="javascript:animatedcollapse.toggle('default')" style="margin: 0px">
      				<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
					<tr>
					<td class="headtab_left">
					</td>
					<td NOWRAP class="headtab_center">&nbsp;&nbsp;Default
					</td>
					<td width="28" valign="top" class="headtab_bg"></td>
					<td class="headtab_bg_center">&nbsp;
					</td>
					<td class="headtab_right">
					</td>
					</tr>
					</table>
						</div>
						<div id="default" >
							<table border="0" class="detailTabLabel">
							<tr>
							<td align="right" width="97px" class="listwhitetext"><fmt:message key="partnerPrivate.contract"/></td>
							<td  align="left" ><s:select cssClass="list-menu" name="partnerPrivate.contract" list="%{partnerContractList}" cssStyle="width:130px"  headerKey="" headerValue="" onchange="changeStatus();" tabindex=""/></td>
							<td align="right" width="145px" class="listwhitetext" >Network&nbsp;</td>
							<td><s:textfield cssClass="input-text" id="networkPartnerCodeId" key="partnerPrivate.networkPartnerCode" cssStyle="width:55px;" size="7" maxlength="10" onchange="valid(this,'special');checkNetworkPartnerName();" tabindex="" onblur=" " onselect="" onkeypress=" " onfocus=" "/></td>
							<td width=""><img align="left" class="openpopup" width="" height="20" onclick="javascript:winOpenPartnerNetwork();" id="openpopupnetwork.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
							<td align="left" colspan=""><s:textfield name="partnerPrivate.networkPartnerName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" size="35" maxlength="250" tabindex=""/></td>
							<td align="right" width="102px" class="listwhitetext"><fmt:message key="partnerPrivate.source"/></td>
							<td style="padding-right:10px;"><s:select cssClass="list-menu" name="partnerPrivate.source" list="%{lead}" cssStyle="width:260px" headerKey="" headerValue="" onchange="changeStatus();" /></td>				
							</tr>
						<tr>
						<td valign="middle" colspan="9" align="left">
					  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
					  	<td align="right" class="listwhitetext" width="95px"><fmt:message key="partnerPrivate.insuranceHas"/></td>
				  		<td align="left" colspan="1"><s:select cssClass="list-menu" name="partnerPrivate.insuranceHas" list="%{hasval}" cssStyle="width:130px" onchange="" tabindex=""/></td>
						<td align="right" class="listwhitetext" width="145px">Vendor&nbsp;</td>					
						<td align="left" class="listwhitetext"><s:textfield onchange="checkVendorName();" key="partnerPrivate.vendorCode" cssClass="input-text" readonly="false" cssStyle="width:55px;" size="7" maxlength="8" tabindex=""/></td>
						<td align="left"><img align="left" class="openpopup" width="17" height="20" onclick="javascript:winOpen1()" id="openpopup5.img"	src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="left" colspan=""><s:textfield name="partnerPrivate.vendorName"  onkeydown="return onlyCharsAllowed(event)" required="true" cssClass="input-text" cssStyle="width:195px;" size="35" maxlength="250" tabindex=""/></td>
						<td align="right" class="listwhitetext" width="101px"><fmt:message key="partnerPrivate.insuranceOptionCode"/></td>
						<td align="left" colspan=""><s:select cssClass="list-menu" name="partnerPrivate.insuranceOption" list="%{insopt}" cssStyle="width:260px" onchange="" tabindex=""/></td>
						</table>
						</td>													
						</tr>
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
			<td align="right" class="listwhitetext" width="95px">VAT Desc</td>
				<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.defaultVat" id="euVat" list="%{euVatList}" onchange="changeStatus();" headerKey="" headerValue="" cssStyle="width:130px" tabindex=""/></td>
			<td align="left" class="listwhitetext" style="width:13px"></td>	
			<td align="left" class="listwhitetext" width="" colspan="">Account PO#/SAP Number</td>
			<td width=""><s:textfield cssClass="input-text" name="partnerPrivate.billToAuthorization" cssStyle="width:279px;"  size="45" maxlength="50" onblur="valid(this,'special')" /></td>
			<td align="right" class="listwhitetext" width="101px" colspan="">NPS score</td>
			<td align="left"><s:textfield cssClass="input-textUpper" name="partnerPrivate.NPSscore" readonly="true" size="15" cssStyle="width:255px;" maxlength="15" onblur="valid(this,'special')" /></td>
			</table></td>
			</tr>
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;" >
		  	<configByCorp:fieldVisibility componentId="component.partner.reloPartnerPortal">
		  		<c:if test="${partnerType == 'VN'}">
	        <td align="right" class="listwhitetext" width="105px" colspan="">Relo partner portal</td>
			<td class="listwhitetext" width="10px" align="left"><s:checkbox name="partnerPrivate.reloPartnerPortal" onclick="changeStatus();"  /></td>
			   <td align="right" class="listwhitetext" >Active</td>
			   </c:if>
			   </configByCorp:fieldVisibility>
			</table></td>
			
			
			</tr>
			<c:if test="${partnerType == 'AG'}">
			<c:if test="${agentClassification== true}">
			<tr>
			<td valign="middle" colspan="9" align="left">
		  	<table class="detailTabLabel" border="0" style="vertical-align:middle;margin:0px;" >
			<td align="right" class="listwhitetext" width="80px">Agent&nbsp;Classification</td>
			<td align="left"><s:select cssClass="list-menu" name="partnerPrivate.agentClassification" list="%{agentClassification}"  headerKey="" headerValue="" cssStyle="width:130px" onchange="checkCompanyDivisionValue()"/></td>
			<td align="right" class="listwhitetext" width="80px">Company&nbsp;Division</td>
			<td align="left"><s:select	name="partnerPrivate.multipleCompanyDivision" list="%{companyDivisionList}" value="%{multipleCompanyDivisionList}" multiple="true" cssClass="list-menu" cssStyle="width:110px;height:60px" onchange="checkAgentClassificationValue()" headerKey="" headerValue=""/></td>
			</table></td></tr>
			</c:if>
			</c:if>
			</table>
		</td>
			</tr>						
				<%--<tr>
					<td height="10" align="left" class="listwhitetext" colspan="10">
					<div onClick="javascript:animatedcollapse.toggle('partnerportal')" style="margin: 0px">
					<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin: 0px;">
						<tr>
							<td class="headtab_left"></td>
							<td class="headtab_center">&nbsp;Partner Portal Detail</td>
							<td width="28" valign="top" class="headtab_bg"></td>
							<td class="headtab_bg_special">&nbsp;</td>
							<td class="headtab_right"></td>
						</tr>
					</table>
					</div>
					<div id="partnerportal">
					<table  cellpadding="1" cellspacing="0" class="detailTabLabel">
						<tr>
							<c:set var="isActiveFlag1" value="false" />
							<c:if test="${partnerPrivate.partnerPortalActive}">
								<c:set var="isActiveFlag1" value="true" />
							</c:if>
							<c:set var="isActiveFlag" value="false" />
							<c:if test="${partnerPrivate.viewChild}">
								<c:set var="isActiveFlag" value="true" />
							</c:if>
							<td width="129" align="right" valign="middle" class="listwhitetext">Partner Portal:</td>
							<td width="1px"><s:checkbox key="partnerPrivate.partnerPortalActive" value="${isActiveFlag1}" fieldValue="true" onclick="changeStatus()" tabindex="21" /></td>
							<td width="180" align="left"><fmt:message key='customerFile.portalIdActive' /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext">Associated Agents</td>
							<td colspan="2"><s:textfield cssClass="input-text" name="partnerPrivate.associatedAgents" size="30" maxlength="65" tabindex="22"/></td>
							<td align="right" valign="middle" class="listwhitetext" width="200px">Allow access to child Agent Records:</td>
							<td width="16"><s:checkbox key="partnerPrivate.viewChild" value="${isActiveFlag}" fieldValue="true" tabindex="23" /></td>
							<td><fmt:message key='customerFile.portalIdActive' /></td>
							
							<c:if test="${not empty partnerPrivate.id}">
								<td align="left">
								<input type="button" tabindex="24" value="Child Agent" class="cssbuttonB" onclick="window.open('getChildAgents.html?id=${partnerPrivate.id}&partnerType=${partnerType}&decorator=popup&popup=true','','width=750,height=400,scrollbars=yes');"/></td>
							</c:if>
							
						</tr>
						
					</table>
					</div>
					</td>
					</tr>
					--%>
					</table>
					</td>					
				</tr>				
			</tbody>
		</table>
		
	</div>
	</c:if>		
		
	<div class="bottom-header" ><span></span></div>	
	</div>
	</div>
	</div>
	<table border="0" style="width:850px;">
		<tr>
			<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn' /></b></td>
			<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partnerPrivate.createdOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="partnerPrivate.createdOn" value="${containerCreatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partnerPrivate.createdOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
			<c:if test="${not empty partnerPrivate.id}">
				<s:hidden name="partnerPrivate.createdBy" />
				<td><s:label name="createdBy" value="%{partnerPrivate.createdBy}" /></td>
			</c:if>
			<c:if test="${empty partnerPrivate.id}">
				<s:hidden name="partnerPrivate.createdBy" value="${pageContext.request.remoteUser}" />
				<td><s:label name="createdBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn' /></td>
			<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partnerPrivate.updatedOn}" pattern="${displayDateTimeEditFormat}" />
			<s:hidden name="partnerPrivate.updatedOn" value="${containerUpdatedOnFormattedValue}" />
			<td width="120px"><fmt:formatDate value="${partnerPrivate.updatedOn}" pattern="${displayDateTimeFormat}" /></td>
			<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
			<c:if test="${not empty partnerPrivate.id}">
				<s:hidden name="partnerPrivate.updatedBy" />
				<td style="width:85px"><s:label name="updatedBy" value="%{partnerPrivate.updatedBy}" /></td>
			</c:if>
			<c:if test="${empty partnerPrivate.id}">
				<s:hidden name="partnerPrivate.updatedBy" value="${pageContext.request.remoteUser}" />
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}" /></td>
			</c:if>
		</tr>
	</table>

	<c:if test="${empty param.popup}">
		<input id="button.save" type="button" class="cssbutton"  value="Save" name="button.save" style="width:60px;" onclick="getAgentCountryCode(); " tabindex="" />
		<s:reset cssClass="cssbutton" key="Reset" onclick="enableField();showVlidField();" cssStyle="width:60px; height:25px " tabindex="" />
	</c:if>
	

</s:form>

<script type="text/javascript">

var position1='';
function bankInfoList(bankPartnerCode,position){
	 var agentCheck=false;
	 <c:if test="${not empty partnerPrivate.id && partnerPrivate.status=='Approved' && partnerType=='AG'}">
	 <sec-auth:authComponent componentId="module.tab.rollexe.bankInfo">
	 	agentCheck=true;
	 	</sec-auth:authComponent>
	 </c:if>
	 var url="partnerBankInfoDetail.html?ajax=1&decorator=simple&popup=true&bankPartnerCode="+bankPartnerCode+"&agentCheck="+agentCheck;
	 position1=position;
	 ajax_showTooltip(url,position);	
}

function reposition()
{
	var mycontentdiv = document.getElementById('ajax_tooltip_content'); 
	var myarrowdiv = document.getElementById('ajax_tooltip_arrow');
	
	mycontentdiv.style.left='-278px';
	
	myarrowdiv.style.left='0px';
	
	myarrowdiv.style.transform = 'rotate(180deg)';
	
	
	
}
function saveValidationForBankInfoById(id){
	 var flag =true;
	if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
		flag=false;
	}
	return flag;
}


function saveValidationForBankInfo(){
	var bankNoList='';
	var currencyList='';
	var id='';
	var valCheck='';
	var flag=true;
	
    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
         	  valCheck=document.getElementById(id).value;
         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
         	   if(bankNoList==''){
         		  bankNoList=id+": "+valCheck;
               }else{
            	   bankNoList= bankNoList+"~"+id+": "+valCheck;
               }
         	  }
 	       }	
 	     }else{	   
 	         id=document.forms['partnerBankInfoDetail'].bankNoList.id;
 	         valCheck=document.forms['partnerBankInfoDetail'].bankNoList.value;
 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
 	       	 bankNoList=id+": "+valCheck;
 	        }
 	     }	        
 	 }
	 currencyList='';
	 id='';
	 valCheck='';
	
    if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
        if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
 	      for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
         	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
         	  id=id.replace('ban','cur');
         	  valCheck=document.getElementById(id).value;
         	  if(valCheck==null || valCheck ==undefined || valCheck==''){
         	   if(currencyList==''){
         		  currencyList=id+": "+valCheck;
               }else{
            	   currencyList= currencyList+"~"+id+": "+valCheck;
               }
         	  }
 	       }	
 	     }else{	   
 	         id=document.forms['partnerBankInfoDetail'].currencyList.id;
 	         valCheck=document.forms['partnerBankInfoDetail'].currencyList.value;
 	        if(valCheck==null || valCheck ==undefined || valCheck==''){
 	       	 currencyList=id+":"+valCheck;
 	        }
 	     }	        
 	 }
   
    if(bankNoList!=''){
    	var bankArray=bankNoList.split("~");
		for ( var i=0; i<bankArray.length && flag; i++){
			id=bankArray[i].split(":")[0].trim();
			id=id.replace("ban","");
			id=id.replace("cur","");
			try{
			if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
				flag=false;
			}
			}catch(e){
				flag=false;
			}
		}
    }
    if(currencyList!=''){
	    var currArray=currencyList.split("~");
		for ( var i=0; i<currArray.length && flag; i++){
			id=currArray[i].split(":")[0].trim();
			id=id.replace("ban","");
			id=id.replace("cur","");
			try{

			if(document.getElementById("ban"+id).value=='' || document.getElementById("cur"+id).value==''){
				flag=false;
			}
			}catch(e){
				flag=false;
			}
		}
    }
   
	return flag;

}
function addRow(tableID,onlysave) {
var flag=saveValidationForBankInfo();
if(!flag){
alert("Please fill the required details.");
document.getElementById('addLine').disabled=false;
document.getElementById('addLine').value="Save&AddLine";
}else{
	if(onlysave!=""){
document.getElementById('addLine').disabled=true;
document.getElementById('addLine').value="Processing....";	
	}
 setTimeout(function(){saveBankInfo(tableID,onlysave);}, 3000); 
}
}
function add(tableID){
var table = document.getElementById(tableID);
var rowCount = table.rows.length;
var row = table.insertRow(rowCount); 
var newlineid=document.getElementById('newlineid').value; 
if(newlineid.trim()==''){
newlineid=rowCount;
}else{
newlineid=newlineid+"~"+rowCount;

}

document.getElementById('newlineid').value=newlineid;

var cell1 = row.insertCell(0);
var element1 = document.createElement("select");
element1.setAttribute("class", "list-menu" );
element1.id="cur"+rowCount;
element1.name="currencyList";
element1.style.width="60px"
var catogeryList='${billingCurrency}';
catogeryList=catogeryList.replace('{','').replace('}','');
var catogeryarray=catogeryList.split(",");
var optioneleBlankCategory= document.createElement("option");
optioneleBlankCategory.value="";
optioneleBlankCategory.text=""; 
element1.options.add(optioneleBlankCategory);  
for(var i=0;i<catogeryarray.length;i++){
var value=catogeryarray[i].split("=");
var optionele= document.createElement("option");
optionele.value=value[0];
optionele.text=value[1]; 
element1.options.add(optionele);             
}
cell1.appendChild(element1);

var cell2 = row.insertCell(1);
var element2 = document.createElement("input");
element2.type = "text";
element2.style.width="100px";
element2.setAttribute("class", "input-text" );
element2.id='ban'+rowCount;
element2.name="bankNoList";
cell2.appendChild(element2);

var cell3 = row.insertCell(2);
var element3 = document.createElement("input");
element3.type ="checkbox";
element3.style.width="30px";
element3.id='chk'+rowCount;
element3.name="checkList";
element3.value=true;
element3.checked=true;
cell3.appendChild(element3);

}
function saveBankInfo(tableID,onlysave){
var bankNoList='';
var currencyList='';
var checkList='';
var id='';
var valCheck='';
var newlineid=document.getElementById('newlineid').value; 
if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){
if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
   for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
 	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
 	  valCheck=document.getElementById(id).value;
 	   if(bankNoList==''){
 		  bankNoList=id+": "+valCheck;
       }else{
    	   bankNoList= bankNoList+"~"+id+": "+valCheck;
       }
    }	
  }else{	   
      id=document.forms['partnerBankInfoDetail'].bankNoList.id;
      valCheck=document.forms['partnerBankInfoDetail'].bankNoList.value;
    	 bankNoList=id+": "+valCheck;
  }

}
currencyList='';
id='';
valCheck='';
if(document.forms['partnerBankInfoDetail'].bankNoList!=undefined && document.forms['partnerBankInfoDetail'].bankNoList.size!=0){

if(document.forms['partnerBankInfoDetail'].bankNoList.length!=undefined){
   for (i=0; i<document.forms['partnerBankInfoDetail'].bankNoList.length; i++){	        	           
 	   id=document.forms['partnerBankInfoDetail'].bankNoList[i].id;
 	  id=id.replace('ban','cur');
 	  valCheck=document.getElementById(id).value;
 	   if(currencyList==''){
 		  currencyList=id+": "+valCheck;
       }else{
    	   currencyList= currencyList+"~"+id+": "+valCheck;
       }
    }	
  }else{	 
      id=document.forms['partnerBankInfoDetail'].currencyList.id;
      valCheck=document.forms['partnerBankInfoDetail'].currencyList.value;
    	 currencyList=id+":"+valCheck;
  }	        
}

checkList='';
id='';
valCheck='';

if(document.forms['partnerBankInfoDetail'].checkList!=undefined && document.forms['partnerBankInfoDetail'].checkList.size!=0){
if(document.forms['partnerBankInfoDetail'].checkList.length!=undefined){
   for (i=0; i<document.forms['partnerBankInfoDetail'].checkList.length; i++){	        	           
 	   id=document.forms['partnerBankInfoDetail'].checkList[i].id;
 	  valCheck=document.getElementById(id).checked;
 	   if(checkList==''){
 		  checkList=id+": "+valCheck;
       }else{
    	   checkList= checkList+"~"+id+": "+valCheck;
       }
    }	
  }else{	  
      id=document.forms['partnerBankInfoDetail'].checkList.id;
      valCheck=document.forms['partnerBankInfoDetail'].checkList.checked;
    	 checkList=id+":"+valCheck;
  }	        
}
var url = "savePrivateBankInfolist.html";
var parameters = "ajax=1&decorator=simple&popup=true&newlineid="+newlineid+"&bankPartnerCode=${partnerPublic.partnerCode}&bankNoList="+ encodeURI(bankNoList)+"&currencyList="+ encodeURI(currencyList)+"&checkList="+ encodeURI(checkList);
httpPrivateBankInfo.open("POST", url, true); 
httpPrivateBankInfo.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
httpPrivateBankInfo.setRequestHeader("Content-length", parameters .length);
httpPrivateBankInfo.setRequestHeader("Connection", "close");
httpPrivateBankInfo.onreadystatechange = handleHttpResponseBankInfo;
httpPrivateBankInfo.onreadystatechange = function(){handleHttpResponseBankInfo(tableID,onlysave);};
httpPrivateBankInfo.send(parameters);    
}
    
function handleHttpResponseBankInfo(tableID,onlysave){
 if (httpPrivateBankInfo.readyState == 4){
   var results = httpPrivateBankInfo.responseText
   results = results.trim();
   document.getElementById('newlineid').value='';
   	var table = document.getElementById(tableID);
	var rowCount = table.rows.length;
   		ajax_hideTooltip();
   		bankInfoList('${partnerPrivate.partnerCode}',position1);
   		if(onlysave!="")
   			{
	setTimeout(function(){add(tableID);}, 1000); 
   			}
   if(results.length>0){
   } }  } 
var httpPrivateBankInfo = getXMLHttpRequestObject();


function updateBankRecord(id){
	var flag=saveValidationForBankInfoById(id);
	if(flag){
var bankAccountNumber=document.getElementById('ban'+id).value;
var currency=document.getElementById('cur'+id).value;
var status=document.getElementById('chk'+id).checked;;
var fieldId=id;
var url="updatePrivateBankRecordAjax.html?ajax=1&decorator=simple&popup=true&bankPartnerCode=${partnerPrivate.partnerCode}&bankAccountNumber="+bankAccountNumber+"&currency="+currency+"&status="+status+"&fieldId="+fieldId;
updateBankRecordHttp.open("GET", url, true);
updateBankRecordHttp.onreadystatechange = handleHttpResponseupdateBankRecord;
updateBankRecordHttp.send(null);
	} else{
		alert("Please fill the required details.");
	}
}
function handleHttpResponseupdateBankRecord(){
if (updateBankRecordHttp.readyState == 4){
  var results = updateBankRecordHttp.responseText
}
}


function getXMLHttpRequestObject()
{
var xmlhttp;
if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
try {
xmlhttp = new XMLHttpRequest();
} catch (e) {
xmlhttp = false;
}
}
return xmlhttp;
}


var enDib=document.forms['accountProfileForm'].elements['partnerPrivate.creditCheck'].checked;
var el = document.getElementById('coordRequiredTrue');
var el1 = document.getElementById('coordRequiredFalse');
var el11 = document.getElementById('coordRequiredTrue1');
var el111 = document.getElementById('coordRequiredFalse1');
var el22 = document.getElementById('coordRequiredTrue2');
var el222 = document.getElementById('coordRequiredFalse2');
var el333 = document.getElementById('coordRequiredTrue12');
var el444 = document.getElementById('coordRequiredFalse12');

if(enDib == true){
		el.style.display = 'block';		
		el1.style.display = 'none';	
		
		el11.style.display = 'block';		
		el111.style.display = 'none';
		
		el22.style.display = 'block';		
		el222.style.display = 'none';

		el333.style.display = 'block';		
		el444.style.display = 'none';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=false;
		}catch(e){}
		}else{
		el.style.display = 'none';
		el1.style.display = 'block';
		
		el11.style.display = 'none';
		el111.style.display = 'block';
		
		el22.style.display = 'none';
		el222.style.display = 'block';

		el333.style.display = 'none';		
		el444.style.display = 'block';
		
		document.forms['accountProfileForm'].elements['partnerPrivate.creditDateCheck'].value='';
		try{
		document.forms['accountProfileForm'].elements['availableCreditButton'].disabled=true;
		}catch(e){}
		
}

try{ 
removeTick();
<c:if test="${partnerType=='AG'}">
var checkRoleAG = 0;
//Modified By subrat BUG #6566 
<sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	checkRoleAG  = 14;
</sec-auth:authComponent>
if(checkRoleAG < 14){
	document.getElementById('AGR').style.visibility = 'visible';
}else{
	document.getElementById('AGR').style.visibility = 'hidden';
}
</c:if>
}
catch(e){}

try{
	<c:if test="${justSayYes=='checked'}">
		GetSelectedItem('Just Say Yes');
	</c:if>
	<c:if test="${qualitySurvey=='checked'}">
		GetSelectedItem('Quality Survey');
	</c:if>
   }
catch(e){}

setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">

	function checkURL(url,link){
		var str = url.value;
		var indexNum = str.indexOf("www")
		var substr = str.substring(0,indexNum);
			str = str.replace(substr,"");
			if(link == '1'){
				document.forms['accountProfileForm'].elements['partnerPrivate.link1'].value=str;
			}else if(link == '2'){
				document.forms['accountProfileForm'].elements['partnerPrivate.link2'].value=str;
			}else if(link == '3'){
				document.forms['accountProfileForm'].elements['partnerPrivate.link3'].value=str;
			}
	}

	function clickOnRequest(){
		document.getElementById("button.save").click();
	}
	try{
		showVlidField();
	}catch(e){
		
	}
</script>

<script type="text/javascript">
function checkEmail() {
		var status = false;     
		var emailRegEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
		     if (document.forms['accountProfileFormName'].elements['partnerPrivate.storageEmail'].value.search(emailRegEx) == -1) {
		    	 document.forms['accountProfileFormName'].elements['partnerPrivate.storageEmail'].value="";
		          alert("Please enter a valid email address.");
		     }
		     else {
		          status = true;
		     }
		     return status;
		}
function globalEmail()
{
	var emailRegEx = /^[A-Z0-9.'_%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    if (document.forms['accountProfileFormName'].elements['partnerPrivate.email'].value.search(emailRegEx) == -1 ) {
   	 document.forms['accountProfileFormName'].elements['partnerPrivate.email'].value="";
   	 
   	 
         alert("Please enter a valid email address.");
    }
    
    }
function winOpen1()
{
		finalVal='Other';
		openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerPrivate.vendorName&fld_code=partnerPrivate.vendorCode');
}
function winOpenPartnerNetwork (){
	finalVal='Other';
	openWindow('networkPartnersPopUp.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerPrivate.networkPartnerName&fld_code=partnerPrivate.networkPartnerCode');

}

function onlyFloatNumsAllowed(evt)
{
	
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
}

function onlyFloat2(e) {
    var t;
    var n = e.value;
    var r = 0;
    for (t = 0; t < n.length; t++) {
        var i = n.charAt(t);
        if (i == ".") {
            r = r + 1
        }
        if ((i < "0" || i > "9") && i != "." || r > "1") {
            document.getElementById(e.id).value ="";
            alert ("Special characters are not allowed.");
            document.getElementById(e.id).select();
            return false;
        }
    }
    return true;
}

function ValidationSpecialCharcter(){
	var iChars = "!`@#$%^&*()+=-[]\\\';,/{}|\":<>?~_";   

	var data = document.forms['accountProfileFormName'].elements['partnerPrivate.minimumMargin'].value;

	for (var i = 0; i < data.length; i++)

	{      
	    if (iChars.indexOf(data.charAt(i)) != -1)
	    {    
	    alert ("Special characters are not allowed.");
	    document.forms['accountProfileFormName'].elements['partnerPrivate.minimumMargin'].value = "";
	    return false; 
	    } 
	} 
	}
    </script>
    <script type="text/javascript">	
    <configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
	var tmp=document.forms['accountProfileFormName'].elements['partnerPrivate.emailPrintOption'];
	for(var i=0;i<tmp.options.length;i++){
		if('${partnerPrivate.emailPrintOption}'==tmp.options[i].value){
			//alert("found............................");
			document.forms['accountProfileFormName'].elements['partnerPrivate.emailPrintOption'].value='${partnerPrivate.emailPrintOption}';
		}
	}
	</configByCorp:fieldVisibility>
	
	
		function autoPopulate_defaultContactInfo(targetElement) {
			var defaultContact=targetElement.options[targetElement.selectedIndex].value;
			 var url="findDefaultContactInfo.html?ajax=1&decorator=simple&popup=true&partnerCode=${partnerPublic.partnerCode}&userName="+encodeURI(defaultContact);
			http7.open("GET", url, true);
		    http7.onreadystatechange = handleHttpResponseDefaultContact;
		    http7.send(null);
	}
		
		function handleHttpResponseDefaultContact() { 
		         if (http7.readyState == 4) {
		        	 
		        	 var results = http7.responseText ;  
		        	 
	                  results = results.trim();   
	                  var res = results.split("~");   
	                   
	                   document.forms['accountProfileForm'].elements['partnerPrivate.contactName'].value=res[0];
	                  
	                   document.forms['accountProfileForm'].elements['partnerPrivate.phone'].value=res[2];
	                   document.forms['accountProfileForm'].elements['partnerPrivate.email'].value=res[1];
	                   if((res[0]==undefined)||(res[0]==null) )
		           		{
	                	   document.forms['accountProfileForm'].elements['partnerPrivate.contactName'].value="";
		           		}
	                    if((res[1]==undefined)||(res[1]==null) )
	   		           		{
	                    	 document.forms['accountProfileForm'].elements['partnerPrivate.email'].value="";
	   		           		}
	                   if((res[2]==undefined)||(res[2]==null) )
	   		           		{
	                	   
	   		                	    document.forms['accountProfileForm'].elements['partnerPrivate.phone'].value="";
	   		           		}
		        	  }
	 	         				
			       
				 }
		var http7 = getHTTPObjectst();
		function getHTTPObjectst() {
	    var xmlhttp;
	    if(window.XMLHttpRequest) {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)  {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)  {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }   } 
	    return xmlhttp;
	}
function findMonthlySalesGoalDetails(){
	var prId = '<%=request.getParameter("partnerId")%>'
	if(prId == null || prId ==''){
		animatedcollapse.toggle('monthlySalesGoal');
		alert('Please save Partner first before access Monthly Sales Goal Details.');
	}else{
		var pcd = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;
		new Ajax.Request('findMonthlySalesGoalDetailsAjax.html?ajax=1&decorator=simple&popup=true&partnerCode='+pcd,
				  {
			   method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var container = document.getElementById("monthlySalesGoalDetailsAjax");
				      container.innerHTML = response;
				    },
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					}
			  });
		}
   	}
function findMonthlySalesGoalDetailsByYear(){
	var prId = '<%=request.getParameter("partnerId")%>'
		if(prId == null || prId ==''){
			animatedcollapse.toggle('monthlySalesGoal');
			alert('Please save Partner first before access Monthly Sales Goal Details.');
		}else{
			var pcd = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;
			var yearVal = document.getElementById("year").value;
			new Ajax.Request('findMonthlySalesGoalDetailsAjax.html?ajax=1&decorator=simple&popup=true&partnerCode='+pcd+'&yearVal='+yearVal,
					  {
				   method:'get',
					    onSuccess: function(transport){
					      var response = transport.responseText || "no response text";
					      var container = document.getElementById("monthlySalesGoalDetailsAjax");
					      container.innerHTML = response;
					      document.getElementById("year").value = yearVal;
					    },
					    onFailure: function(){ 
						    //alert('Something went wrong...')
						}
				  });
			}
}
function onlyRateAllowed(evt){
	var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 
      && (charCode < 48 || charCode > 57))
       return false;

    return true; 
}
function calculateMonthlyTotal(field1,field2,field3){
	var field1Val = "0.00";
	var field2Val = "0.00";
	field1Val = Math.round(document.getElementById(field1).value*100)/100;
	field2Val = Math.round(document.getElementById(field2).value*100)/100;
	document.getElementById(field1).value = field1Val.toFixed(2);
	document.getElementById(field2).value = field2Val.toFixed(2);
	document.getElementById(field3).value = (field1Val + field2Val).toFixed(2);
	
	var janTotal = "0.00";
	var febTotal = "0.00";
	var marTotal = "0.00";
	var aprTotal = "0.00";
	var mayTotal = "0.00";
	var junTotal = "0.00";
	var julTotal = "0.00";
	var augTotal = "0.00";
	var sepTotal = "0.00";
	var octTotal = "0.00";
	var novTotal = "0.00";
	var decTotal = "0.00";
	
	janTotal = Math.round(document.getElementById("januaryTotal").value*100)/100;
	febTotal = Math.round(document.getElementById("februaryTotal").value*100)/100;
	marTotal = Math.round(document.getElementById("marchTotal").value*100)/100;
	aprTotal = Math.round(document.getElementById("aprilTotal").value*100)/100;
	mayTotal = Math.round(document.getElementById("mayTotal").value*100)/100;
	junTotal = Math.round(document.getElementById("juneTotal").value*100)/100;
	julTotal = Math.round(document.getElementById("julyTotal").value*100)/100;
	augTotal = Math.round(document.getElementById("augustTotal").value*100)/100;
	sepTotal = Math.round(document.getElementById("septemberTotal").value*100)/100;
	octTotal = Math.round(document.getElementById("octoberTotal").value*100)/100;
	novTotal = Math.round(document.getElementById("novemberTotal").value*100)/100;
	decTotal = Math.round(document.getElementById("decemberTotal").value*100)/100;
	
	document.getElementById("allMonthTotal").value = (janTotal + febTotal +marTotal +aprTotal +mayTotal +junTotal +julTotal +augTotal +sepTotal +octTotal +novTotal+decTotal).toFixed(2);
	
	janTotal = Math.round(document.getElementById("januaryImport").value*100)/100;
	febTotal = Math.round(document.getElementById("februaryImport").value*100)/100;
	marTotal = Math.round(document.getElementById("marchImport").value*100)/100;
	aprTotal = Math.round(document.getElementById("aprilImport").value*100)/100;
	mayTotal = Math.round(document.getElementById("mayImport").value*100)/100;
	junTotal = Math.round(document.getElementById("juneImport").value*100)/100;
	julTotal = Math.round(document.getElementById("julyImport").value*100)/100;
	augTotal = Math.round(document.getElementById("augustImport").value*100)/100;
	sepTotal = Math.round(document.getElementById("septemberImport").value*100)/100;
	octTotal = Math.round(document.getElementById("octoberImport").value*100)/100;
	novTotal = Math.round(document.getElementById("novemberImport").value*100)/100;
	decTotal = Math.round(document.getElementById("decemberImport").value*100)/100;
	
	document.getElementById("allMonthImportTotal").value = (janTotal + febTotal +marTotal +aprTotal +mayTotal +junTotal +julTotal +augTotal +sepTotal +octTotal +novTotal+decTotal).toFixed(2);

	janTotal = Math.round(document.getElementById("januaryExport").value*100)/100;
	febTotal = Math.round(document.getElementById("februaryExport").value*100)/100;
	marTotal = Math.round(document.getElementById("marchExport").value*100)/100;
	aprTotal = Math.round(document.getElementById("aprilExport").value*100)/100;
	mayTotal = Math.round(document.getElementById("mayExport").value*100)/100;
	junTotal = Math.round(document.getElementById("juneExport").value*100)/100;
	julTotal = Math.round(document.getElementById("julyExport").value*100)/100;
	augTotal = Math.round(document.getElementById("augustExport").value*100)/100;
	sepTotal = Math.round(document.getElementById("septemberExport").value*100)/100;
	octTotal = Math.round(document.getElementById("octoberExport").value*100)/100;
	novTotal = Math.round(document.getElementById("novemberExport").value*100)/100;
	decTotal = Math.round(document.getElementById("decemberExport").value*100)/100;
	
	document.getElementById("allMonthExportTotal").value = (janTotal + febTotal +marTotal +aprTotal +mayTotal +junTotal +julTotal +augTotal +sepTotal +octTotal +novTotal+decTotal).toFixed(2);
}
function findMsgFieldValueChanged(){
	document.getElementById("msgFieldValueChanged").value='Yes';
}
function checkAgentClassificationValue(){
	if(document.forms['accountProfileForm'].elements['partnerPrivate.agentClassification'].value=='' || document.forms['accountProfileForm'].elements['partnerPrivate.agentClassification'].value==null){
		alert("Please select Agent Classification first.");
		var elements = document.forms['accountProfileForm'].elements['partnerPrivate.multipleCompanyDivision'].options;
	    for(var i = 0; i < elements.length; i++){
	      if(elements[i].selected)
	        elements[i].selected = false;
	    }
	}
}
function checkCompanyDivisionValue(){
	if(document.forms['accountProfileForm'].elements['partnerPrivate.agentClassification'].value=='' || document.forms['accountProfileForm'].elements['partnerPrivate.agentClassification'].value==null){
		var elements = document.forms['accountProfileForm'].elements['partnerPrivate.multipleCompanyDivision'].options;
	    for(var i = 0; i < elements.length; i++){
	      if(elements[i].selected)
	        elements[i].selected = false;
	    }
	}
}
var httpVatBilling = gethttpVatBilling();
function gethttpVatBilling() {
var xmlhttp;
if(window.XMLHttpRequest) {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }   } 
return xmlhttp;
}
function vatBilling(targetElement)
{ 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	     var pcd = document.forms['accountProfileForm'].elements['partnerPrivate.partnerCode'].value;
		 var defaultContact=targetElement.options[targetElement.selectedIndex].text;
		 var vatBillingCode=targetElement.options[targetElement.selectedIndex].value;
		 var prId = '<%=request.getParameter("partnerId")%>';
		  var defaultVat='${partnerPrivate.defaultVat}';
		 var url="vatBillingDesc.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(pcd)+"&partnerPrivateId="+encodeURI(prId)+"&description="+encodeURI(defaultContact)+"&defaultVat="+encodeURI(defaultVat)+"&vatBillingCode="+encodeURI(vatBillingCode);
		 httpVatBilling.open("GET", url, true);
		 httpVatBilling.onreadystatechange = handleHttpResponseVatBilling;
		 httpVatBilling.send(null);
		 </configByCorp:fieldVisibility >
}

function handleHttpResponseVatBilling() { 
	 if (httpVatBilling.readyState == 4)
     {
       var results = httpVatBilling.responseText
         results = results.trim();
         results = results.replace('[','');
         results=results.replace(']',''); 
         res = results.split("@");
         var targetElement = document.forms['accountProfileForm'].elements['partnerPrivate.defaultVat'];
		 targetElement.length = res.length;
 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['accountProfileForm'].elements['partnerPrivate.defaultVat'].options[i].text = '';
					document.forms['accountProfileForm'].elements['partnerPrivate.defaultVat'].options[i].value = '';
					}else{
				     var stateVal = res[i].split("#");
					 document.forms['accountProfileForm'].elements['partnerPrivate.defaultVat'].options[i].value = stateVal[0];
					document.forms['accountProfileForm'].elements['partnerPrivate.defaultVat'].options[i].text = stateVal[1];
					}
				}
 				         var defaultVat='${partnerPrivate.defaultVat}'
 				    	    defaultVat=defaultVat.replace('~Active','');
					document.getElementById("euVat").value =defaultVat ; 
					
            
     }
}
function savePatnerVatCode() { 
	<configByCorp:fieldVisibility componentId="component.partner.vatBillingGroup.edit">
	var e = document.getElementById("vatBillingGroup");
	 var defaultVatBillingGroup= e.options[e.selectedIndex].text ;
	var vatBillingGroupFlexValue="";
	var relo="" 
	<c:forEach var="entry" items="${flexCodeList}">
	  if(relo==""){ 
        if(defaultVatBillingGroup=='${entry.key}') {
        	vatBillingGroupFlexValue='${entry.value}';
           relo="yes";
        }} 
	</c:forEach>
	var currentDefaultVatValue =  document.getElementById("euVat").value; 
	var currentDefaultVatValueTest =  currentDefaultVatValue+","; 
	vatBillingGroupFlexValue = vatBillingGroupFlexValue+",";
	if(defaultVatBillingGroup !=""){
	if(currentDefaultVatValue !="" && (!(vatBillingGroupFlexValue.includes(currentDefaultVatValueTest)))){
		var agree = confirm("Selected VAT code not part of the VAT Billing Group. Do you wish to continue?");
		if(agree){
			return true;
		}
		else
			{
			return false;
			}
	}else
	{
		return true;
		}
	}
	else
	{
		return true;
		}
	</configByCorp:fieldVisibility >  
	  }
	


</script>