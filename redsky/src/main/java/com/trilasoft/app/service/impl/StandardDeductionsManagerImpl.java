package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.StandardDeductionsDao;
import com.trilasoft.app.model.StandardDeductions;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.service.StandardDeductionsManager;

public class StandardDeductionsManagerImpl extends GenericManagerImpl<StandardDeductions, Long> implements StandardDeductionsManager {
	StandardDeductionsDao standardDeductionsDao;

	public StandardDeductionsManagerImpl(StandardDeductionsDao standardDeductionsDao) {
		super(standardDeductionsDao);
		this.standardDeductionsDao = standardDeductionsDao;
	}

	public List getSDListByPartnerCode(String partnerCode) {
		return standardDeductionsDao.getSDListByPartnerCode(partnerCode);
	}

	public void deleteSubContChargesForStandardDeduction(String companyDivision, String corpID) {
		standardDeductionsDao.deleteSubContChargesForStandardDeduction(companyDivision, corpID);
	}

	public List deuctionPreviewList(Date fromDate, String companyDivision, String sessionCorpID, String process, String ownerPayTo) {
		return standardDeductionsDao.deuctionPreviewList(fromDate, companyDivision, sessionCorpID, process, ownerPayTo);
	}

	public List getStandardDeductionPreviewList(Date fromDate, String companyDivision, String corpID, String process,String ownerPayTo) {
		return standardDeductionsDao.getStandardDeductionPreviewList(fromDate, companyDivision, corpID, process,ownerPayTo);
	}

	public void updateSubContChargesForStandardDeduction(String personId,Long id,String userName) {
		standardDeductionsDao.updateSubContChargesForStandardDeduction(personId,id,userName);
	}

	public void updateStandardDeduction(Long id, BigDecimal dedAmount, BigDecimal balancedAmount, Date lastDeductionDate, String status){
		standardDeductionsDao.updateStandardDeduction(id, dedAmount,balancedAmount, lastDeductionDate, status);
	}

	public List getTripCount(Date fromDate, String partnerCode, String corpID) {
		return standardDeductionsDao.getTripCount(fromDate, partnerCode, corpID);
	}

	public void updateDomestic(String partnerCode) {
		standardDeductionsDao.updateDomestic(partnerCode);
	}

	public List<SubcontractorCharges> getSubContChargesForFinalize(String personId) {
		return standardDeductionsDao.getSubContChargesForFinalize(personId);
	}

	public void updateSubContractorCharges(Long id) {
		standardDeductionsDao.updateSubContractorCharges(id);
	}

	public List validatePayTo(String ownerPayTo, String corpId) {		
		return standardDeductionsDao.validatePayTo(ownerPayTo, corpId);
	}
	
	public void updatePartnerPrivate(String ownerPayTo, String corpId,BigDecimal personalEscrowAmount){
		standardDeductionsDao.updatePartnerPrivate(ownerPayTo, corpId, personalEscrowAmount);
	}
	
	public List partnerPrivateObject(String ownerPayTo, String corpId){
		return standardDeductionsDao.partnerPrivateObject(ownerPayTo, corpId);
	}
	
	public List<SubcontractorCharges> getSubContChargesForPersonalEscrow(String partnerCode){
		return standardDeductionsDao.getSubContChargesForPersonalEscrow(partnerCode);
	}
	public List getUpdatedRecord(Long id){
		return standardDeductionsDao.getUpdatedRecord(id);
	}
	
	public List partnerPrivatePersonalEscrow(String ownerPayTo, String corpId){
		return standardDeductionsDao.partnerPrivatePersonalEscrow(ownerPayTo, corpId);
	}
	
	public List subContractorChargesFinalList(String ownerPayTo, String corpId, String companyDivision){
		return standardDeductionsDao.subContractorChargesFinalList(ownerPayTo, corpId, companyDivision);
	}
	public List chargesObject(String chargeCode, String corpId){
		return standardDeductionsDao.chargesObject(chargeCode, corpId);
	}
}
