package com.trilasoft.app.dao.hibernate;

import java.math.BigInteger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.MenuItemDao;
import com.trilasoft.app.dao.hibernate.QualitySurveySettingsDaoHibernate.SurveyEmailDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.MenuItem;
import com.trilasoft.app.webapp.action.HibernateFilterInterceptor;


public class MenuItemDaoHibernate extends GenericDaoHibernate<MenuItem, Long>
		implements MenuItemDao {

	private Set permission;
	private static Map permissionMap = AppInitServlet.pageActionUrls;

	public MenuItemDaoHibernate() {
		super(MenuItem.class);
	}

	public List<MenuItem> findByCorpID(String corpId) {
		return getHibernateTemplate().find("from MenuItem where corpID=?",
				corpId);
	}

	public List countForMenuItem(Long id) {
		return getHibernateTemplate().find(
				"select count(*) from MenuItem where  id =?", id);
	}

	public List findPermissions(String recipient, String corpId,
			String parentN, String menuN, String titleName, String urlN) {

		return this
				.getSession()
				.createSQLQuery(
						"select m2.id,m1.permission from menu_item_permission m1, menu_item m2 where m1.menuItemId = m2.id and m2.parentname like '"
								+ parentN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and m2.name like '"
								+ menuN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and m2.title like '"
								+ titleName.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and m2.url like '"
								+ urlN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and m1.corpId='"
								+ corpId
								+ "' and m2.corpId='"
								+ corpId
								+ "' and m1.recipient='" + recipient + "'")
				.list();
	}

	
	/**
	 * 
	   select p.id,pp.permission 
		from pageaction p,pageactionmap pm,menu_item m, pageactionpermission pp
		where 
		p.id=pm.pageaction_id 
		and 
		pm.menuitem_id = m.id  
		and 
		pp.pageaction_id=pm.pageaction_id
		and
		m.url=''


	 * 
	 * 
 * 						"select p.id,pp.permission "
							+ " from pageaction p,pageactionpermission pp,menu_item m,menu_item_permission mp where p.id=pp.pageaction_id "
							+ " and pp.menuitem_id = mp.id  "
							+ " and mp.menuitemid = m.id  "
							+ " and m.url='"
							+ url
							+ "'"

	 * 
	 * 
	 * 
	 */
	
	public List findPagePermissions(String url, String menuName, String corpId,
			String pageMenu, String pageDesc, String pageUrl,String role) {
		return this
				.getSession()
				.createSQLQuery(
						   "select p.id,pp.permission "+ 
							"from pageaction p,pageactionmap pm,menu_item m, pageactionpermission pp "+
							"where  "+
							"p.id=pm.pageaction_id "+ 
							"and  "+
							"pm.menuitem_id = m.id  "+ 
							"and  "+
							"pp.pageaction_id=pm.pageaction_id "+
							"and pp.role='"+role+"' and "+
							"m.url='"+url+"' "
								+ " and m.name like '"
								+ pageMenu.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%'"
								+ " and p.description like '"
								+ pageDesc.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%'"
								+ " and p.actionuri like '"
								+ pageUrl.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''") + "%'"
								+ " and m.corpId='"+corpId+"'"
								+ " and pp.corpId ='"+corpId+"'"
								+ " and p.actionuri!='" + url + "'").list();
	}
//not in use
	public void updatePageActionListByUrl(Long id1, int permission,
			String corpId, String recipient) {
		try {
			if (id1 != null) {

				List<String> urlList = this
						.getSession()
						.createSQLQuery(
								"select concat(mp.id,'~',p.actionuri) as PageList "
										+ " from pageaction p,pageactionpermission pp,menu_item m,menu_item_permission mp   "
										+ " where p.id=pp.pageaction_id  "
										+ " and pp.menuitem_id = mp.id  "
										+ " and mp.menuitemid = m.id "
										+ " and pp.corpId ='"+corpId+"'"
										+ " and m.corpId='"
										+ corpId
										+ "' "
										+ " and pp.corpId ='"+corpId+"'"
										+ " and m.corpid=mp.corpid "
										+ " and p.actionuri=(select m.url from menu_item m, menu_item_permission mp "
										+ " where m.id=mp.menuitemid and mp.id="+ id1 + " and mp.recipient='"+recipient+"') and mp.recipient='"+recipient+"'").list();

				for (String pageL : urlList) {
					String arr[] = pageL.split("~");
					this.getSession()
							.createSQLQuery(
									"update pageactionpermission set permission="
											+ permission
											+ ",corpId='"+corpId+"' where menuitem_id="
											+ Long.parseLong(arr[0]) + " and recipient='"+recipient+"'")
							.executeUpdate();
					List permittedUrls = (List) permissionMap.get(corpId + "-"
							+ recipient);
					List urlList1 = null;
					if (permission != 2) {
						if (permittedUrls == null) {
						} else {
							if (permittedUrls.contains(arr[1])) {
								permittedUrls.remove(arr[1]);
								permissionMap.remove(corpId + "-" + recipient);
								permissionMap.put(corpId + "-" + recipient,
										permittedUrls);
							}
						}
					} else {
						if (permittedUrls == null) {
							urlList1 = new ArrayList();
							urlList1.add(arr[1]);
							permissionMap
									.put(corpId + "-" + recipient, urlList);
						} else {
							if (permittedUrls.contains(arr[1])) {
							} else {
								permittedUrls.add(arr[1]);
								permissionMap.remove(corpId + "-" + recipient);
								permissionMap.put(corpId + "-" + recipient,
										permittedUrls);
							}
						}
					}
				}
			}
		} catch (Exception e) {
		}
	}

	public String getPageActionIdByUrl(String url, String menuName,
			String corpId) {
		String idList = "";
		List menuItemList = this
				.getSession()
				.createSQLQuery(
						"select p.id as pageId,p.actionuri as pageUrl,p.description as pageDescription "+
								"from menu_item m, pageactionmap pm, pageaction p "+
								"where "+
								"m.id = pm.menuitem_id "+ 
								"and "+
								"pm.pageaction_id=p.id "+
								"and "+
								"m.url='"+url +"'"
								+ " and m.corpId='"+ corpId+ "' and p.actionuri!='" + url + "'").list();

		Iterator it = menuItemList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			if (idList.equalsIgnoreCase("")) {
				if (row[0] != null) {
					idList = row[0].toString();
				}
			} else {
				if (row[0] != null) {
					idList = idList + "," + row[0].toString();
				}
			}

		}
		return idList;
	}

	public Boolean isSecurityEnabled(String corpId) {
		Boolean exists;
		List permissionsList = this
				.getSession()
				.createSQLQuery(
						"Select if((securityChecked is false OR securityChecked is null),'NO','YES') as Security from company where corpID ='"
								+ corpId + "'").list();
		if ((!permissionsList.isEmpty()) && (permissionsList != null)
				&& (permissionsList.get(0) != null)
				&& (permissionsList.get(0).toString().equalsIgnoreCase("YES"))) {
			exists = true;
		} else {
			exists = false;
		}
		return exists;
	}
	public List getPageActionListByUrlNew(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl){
		List<Object> menuList = new ArrayList<Object>();
		List menuItemList = this
		.getSession()
		.createSQLQuery(
					  	"select p.id as pageId,p.actionuri as pageUrl,p.description as pageDescription "+
						"from menu_item m, pageactionmap pm, pageaction p "+
						"where "+
						"m.id = pm.menuitem_id "+ 
						"and "+
						"pm.pageaction_id=p.id "+
						"and "+
						"m.url='"+url +"'"
						+ " and m.name like '"
						+ pageMenu.replaceAll("'", "''")
								.replaceAll(":", "''")
								.replace("_", "''")
						+ "%'"
						+ " and p.description like '"
						+ pageDesc.replaceAll("'", "''")
								.replaceAll(":", "''")
								.replace("_", "''")
						+ "%'"
						+ " and p.actionuri like '"
						+ pageUrl.replaceAll("'", "''")
								.replaceAll(":", "''")
								.replace("_", "''")
						+ "%'"
						+ " and m.corpId in ('TSFT','"
						+ corpId
						+ "') and p.actionuri!='"
						+ url
						+ "'  group by p.id").list();

					Iterator it = menuItemList.iterator();
					
					DTOPayableExtractLevel dTOPayableExtractLevel = null;
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						dTOPayableExtractLevel = new DTOPayableExtractLevel();
						dTOPayableExtractLevel.setId(row[0]);
						if (row[1] != null) {
							dTOPayableExtractLevel.setUrl(row[1]);
						} else {
							dTOPayableExtractLevel.setUrl("");
						}
						if (menuName != null) {
							dTOPayableExtractLevel.setParentMenu(menuName);
						} else {
							dTOPayableExtractLevel.setParentMenu("");
						}
						if (row[2] != null) {
							dTOPayableExtractLevel.setDescription(row[2]);
						} else {
							dTOPayableExtractLevel.setDescription("");
						}
						String assignROLE="";
						if ((row[1]!= null)&&(!row[1].toString().equalsIgnoreCase(""))) {
							List al = this.getSession().createSQLQuery("select name from role where corpId='"+corpId+"'").list();
							Iterator itr=al.iterator();
							Set<String> roleSet = new TreeSet<String>();
							while(itr.hasNext()){
								String rowObj=(String)itr.next();
								List permittedUrls = (List) permissionMap.get(corpId + "-"+ rowObj);
								if(row[1].toString().indexOf("?")>-1){
									row[1]=row[1].toString().substring(0, row[1].toString().indexOf("?"));
								}
								if((permittedUrls!=null) && (permittedUrls.contains(row[1].toString()))){
									if(!roleSet.contains(rowObj)){
										if(assignROLE.equalsIgnoreCase("")){
											assignROLE=rowObj;
										}else{
											assignROLE=assignROLE+", "+rowObj;
										}
										roleSet.add(rowObj);
									}
								}
							}
						}
						assignROLE=assignROLE.trim();
						dTOPayableExtractLevel.setRoleList(assignROLE);						
						menuList.add(dTOPayableExtractLevel);
					}
					return menuList;
	}
	public List getPageActionListByUrl(String url, String menuName,
			String corpId, String pageMenu, String pageDesc, String pageUrl) {
		List<Object> menuList = new ArrayList<Object>();

		/**
		 * 
		  	select p.id as pageId,p.actionuri as pageUrl,p.description as pageDescription
			from menu_item m, pageactionmap pm, pageaction p
			where
			m.id = pm.menuitem_id 
			and
			pm.pageaction_id=p.id
			and
			m.url='/workTickets.html';

		 */
		
		
		List menuItemList = this
				.getSession()
				.createSQLQuery(
							  	"select p.id as pageId,p.actionuri as pageUrl,p.description as pageDescription "+
								"from menu_item m, pageactionmap pm, pageaction p "+
								"where "+
								"m.id = pm.menuitem_id "+ 
								"and "+
								"pm.pageaction_id=p.id "+
								"and "+
								"m.url='"+url +"'"
								+ " and m.name like '"
								+ pageMenu.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%'"
								+ " and p.description like '"
								+ pageDesc.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%'"
								+ " and p.actionuri like '"
								+ pageUrl.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%'"
								+ " and m.corpId='"
								+ corpId
								+ "' and p.actionuri!='"
								+ url
								+ "'  group by p.id").list();

		Iterator it = menuItemList.iterator();

		DTOPayableExtractLevel dTOPayableExtractLevel = null;
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			dTOPayableExtractLevel = new DTOPayableExtractLevel();
			dTOPayableExtractLevel.setId(row[0]);
			if (row[1] != null) {
				dTOPayableExtractLevel.setUrl(row[1]);
			} else {
				dTOPayableExtractLevel.setUrl("");
			}
			if (menuName != null) {
				dTOPayableExtractLevel.setParentMenu(menuName);
			} else {
				dTOPayableExtractLevel.setParentMenu("");
			}
			if (row[2] != null) {
				dTOPayableExtractLevel.setDescription(row[2]);
			} else {
				dTOPayableExtractLevel.setDescription("");
			}
			String assignROLE="";
			if (row[1]!= null) {
				List al = this.getSession().createSQLQuery("select name from role where corpId='"+corpId+"'").list();
				Iterator itr=al.iterator();
				Set<String> roleSet = new TreeSet<String>();
				while(itr.hasNext()){
					String rowObj=(String)itr.next();
					List permittedUrls = (List) permissionMap.get(corpId + "-"+ rowObj);
					if((permittedUrls!=null) && (permittedUrls.contains(row[1].toString()))){
						if(!roleSet.contains(rowObj)){
							if(assignROLE.equalsIgnoreCase("")){
								assignROLE=rowObj;
							}else{
								assignROLE=assignROLE+", "+rowObj;
							}
							roleSet.add(rowObj);
						}
					}
				}
			}
			assignROLE=assignROLE.trim();
			dTOPayableExtractLevel.setRoleList(assignROLE);
			menuList.add(dTOPayableExtractLevel);

		}
		return menuList;

	}

	public class DTOPayableExtractLevel {
		private Object id;
		private Object url;
		private Object parentMenu;
		private Object description;
		private Object permission;
		private Object roleList;

		public Object getRoleList() {
			return roleList;
		}

		public void setRoleList(Object roleList) {
			this.roleList = roleList;
		}

		public Object getId() {
			return id;
		}

		public void setId(Object id) {
			this.id = id;
		}

		public Object getUrl() {
			return url;
		}

		public void setUrl(Object url) {
			this.url = url;
		}

		public Object getDescription() {
			return description;
		}

		public void setDescription(Object description) {
			this.description = description;
		}

		public Object getPermission() {
			return permission;
		}

		public void setPermission(Object permission) {
			this.permission = permission;
		}

		public Object getParentMenu() {
			return parentMenu;
		}

		public void setParentMenu(Object parentMenu) {
			this.parentMenu = parentMenu;
		}
	}

	public Boolean checkPermissions(Long menuItemId, int permission,
			String recipient, String sessionCorpID) {
		Boolean doesNotExist;
		
		
		List permissionsList = this
				.getSession()
				.createSQLQuery(
						"select * from menu_item_permission mp, menu_item m "+ 
						"where m.id = mp.menuItemId  AND m.corpID='" + sessionCorpID+ "'  "+
						"AND m.corpid=mp.corpid and mp.recipient='" + recipient + "' "+
						"and m.url=(select url from menu_item where id='"+ menuItemId + "')  "+
						"and mp.permission=" + permission + "" 
						).list();
		
		if (permissionsList.isEmpty()) {
			doesNotExist = true;
		} else {
			doesNotExist = false;
		}
		return doesNotExist;
	}

	public void updateMenuActionPermissionByList(Long menuId, String role,
			String sessionCorpID, int permission, String url) {
		try {
			if (menuId != null) {
				this.getSession()
						.createSQLQuery(
								"insert into menu_item_permission(menuItemId,recipient,corpID,permission)values("
										+ menuId
										+ ",'"
										+ role
										+ "','"
										+ sessionCorpID
										+ "','"
										+ permission
										+ "')").executeUpdate();

			}
		} catch (Exception e) {
		}
	}

	public void updatePageActionPermissionByList(String idList,
			Long menuItemPermisionid, int permisson, String role,
			String sessionCorpID) {
		try {
			if (!idList.equalsIgnoreCase("")) {
				String pageIdArr[] = idList.split(",");
				for (int i = 0; i < pageIdArr.length; i++) {
					this.getSession()
							.createSQLQuery(
									"insert into pageactionpermission(pageaction_id,role,permission,corpId)values("
											+ Long.parseLong(pageIdArr[i])
											+ ",'" + role + "','"
											+ permisson + "','"+sessionCorpID+"')").executeUpdate();
					
								}
			}
		} catch (Exception e) {
		}
	}
	public Boolean pagePermissionExists(Long menuId,
			int permission, String recipient, String childFlag, String corpId) {
		
		Boolean updatePermission;
		
		List permissionsList = this
				.getSession()
				.createSQLQuery(
						
						"select * from pageactionpermission pp, menu_item m, pageactionmap pm "+
						"where  "+
						"m.id=pm.menuitem_id "+
						"and "+
						"pm.pageaction_id=pp.pageaction_id "+
						"and "+
						"m.corpid='"+corpId+"' "+
						"and "+
						"pp.corpId='"+corpId+"' "+						
						"and "+
						"pp.role='"+recipient+"' "+
						"and "+
						"m.id = "+ menuId + ""
								
						).list();
		
		if (permissionsList.isEmpty()) {
			
			updatePermission = true;
			
		} else {
			if (!childFlag.equalsIgnoreCase("NO")) {
			
				List<String> urlList = this
						.getSession()
						.createSQLQuery(
								"select concat(p.id,'~',p.actionuri) from pageaction p, pageactionmap pm, menu_item m "+
								"where m.id=pm.menuitem_id and pm.pageaction_id=p.id  "+
								"and m.corpid='"+corpId+"' and m.id="+menuId+" "
								).list();

				
				for (String pageL : urlList) {
					
					String arr[] = pageL.split("~");
					int noOfRowsUpdated = this.getSession()
							.createSQLQuery(
									"update pageactionpermission set permission="
											+ permission
											+ ",corpId='"+corpId+"' where pageaction_id="
											+ Long.parseLong(arr[0]) + " and role='"+recipient+"'")
							.executeUpdate();
					// the row was not updated in pageactionpermission
					// need to make an entry??
					if(noOfRowsUpdated==0){
						//insert
						this.getSession()
						.createSQLQuery(
								"insert into pageactionpermission(pageaction_id,role,permission,corpId)values("
										+ Long.parseLong(arr[0])
										+ ",'" + recipient + "','"
										+ permission + "','"+corpId+"')").executeUpdate();						
					}
				}
			}
			updatePermission = false;
		}
		return updatePermission;
	}

	public Boolean permissionExists(Long menuItemId, int permission,
			String recipient, String sessionCorpID, String url) {
		Boolean updatePermission;
		List permissionsList = this
				.getSession()
				.createSQLQuery(
								"select mp.id from menu_item_permission mp, menu_item m "+ 
								"where m.id = mp.menuItemId  AND m.corpID='" + sessionCorpID+ "'  "+
								"AND m.corpid=mp.corpid and mp.recipient='" + recipient + "' "+
								"and m.id="+ menuItemId + ""
								).list();
		
		if (permissionsList.isEmpty()) {
		
			updatePermission = true;
		
		} else {
			BigInteger id = (BigInteger) permissionsList.get(0);
			this.getSession()
					.createSQLQuery(
									"update menu_item_permission set permission = "
									+ permission + " where id = "+id+""
									).executeUpdate();

			updatePermission = false;
		}
		return updatePermission;
	}

	public List<MenuItem> getMenuList(String corpId) {

		/*
		 * List<MenuItem> menuItems = getHibernateTemplate().getSessionFactory()
		 * .getCurrentSession().createCriteria(MenuItem.class)
		 * .add(Restrictions.eq("corpID", corpId))
		 * .addOrder(Order.asc("sequenceNum")).list(); return menuItems;
		 */

		return getHibernateTemplate().find(
				"from MenuItem where corpID = '" + corpId
						+ "' order by sequenceNum");

	}

	public List listAllMenuItems(String corpId, String parentN, String menuN,
			String titleName, String urlN,String pageUrl) {
		List<Object> menuList = new ArrayList<Object>();
		List menuItemList = this
				.getSession()
				.createSQLQuery(
						"select id,corpid,parentname,name,title,url from menu_item m where parentname like '"
								+ parentN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and name like '"
								+ menuN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and title like '"
								+ titleName.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and url like '"
								+ urlN.replaceAll("'", "''")
										.replaceAll(":", "''")
										.replace("_", "''")
								+ "%' and corpId='"
								+ corpId + "'").list();
		Iterator it = menuItemList.iterator();
		String pageUrlFlag="";
		while (it.hasNext()) {
			Object[] obj = (Object[]) it.next();
			MenuItem menuItems = new MenuItem();
			menuItems.setId(Long.parseLong(obj[0].toString()));
			if (obj[1] == null) {
				menuItems.setCorpID("");
			} else {
				menuItems.setCorpID(obj[1].toString());
			}
			if (obj[2] == null) {
				menuItems.setParentName("");
			} else {
				menuItems.setParentName(obj[2].toString());
			}
			if (obj[3] == null) {
				menuItems.setName("");
			} else {
				menuItems.setName(obj[3].toString());
			}
			if (obj[5] == null) {
				menuItems.setUrl("");
			} else {
				menuItems.setUrl(obj[5].toString());
				pageUrlFlag=obj[5].toString();
			}
			try{
			List urlList1 = this
			.getSession()
			.createSQLQuery(
					" select p.actionuri as pageUrl "+ 
					" from menu_item m, pageactionmap pm, pageaction p "+ 
					" where m.id = pm.menuitem_id and  pm.pageaction_id=p.id and m.url='"+pageUrlFlag+"' and m.corpId='"+corpId+"' "+
					" and p.actionuri!='"+pageUrlFlag+"' and p.actionuri='"+pageUrl+"' group by p.id"
					).list();
			if(urlList1!=null && !urlList1.isEmpty() && urlList1.get(0)!=null){
				pageUrlFlag="Y";
			}else{
				pageUrlFlag="N";
			}}catch(Exception e){pageUrlFlag="N";}
			String assignROLE="";
			if ((obj[5]!= null)&&(!obj[5].toString().equalsIgnoreCase(""))) {
				List al = this.getSession().createSQLQuery("select name from role where corpId='"+corpId+"'").list();
				Iterator itr=al.iterator();
				Set<String> roleSet = new TreeSet<String>();
				while(itr.hasNext()){
					String rowObj=(String)itr.next();
					List permittedUrls = (List) permissionMap.get(corpId + "-"+ rowObj);
					if(obj[5].toString().indexOf("?")>-1){
						obj[5]=obj[5].toString().substring(0, obj[5].toString().indexOf("?"));
					}
					if((permittedUrls!=null) && (permittedUrls.contains(obj[5].toString()))){
						if(!roleSet.contains(rowObj)){
							if(assignROLE.equalsIgnoreCase("")){
								assignROLE=rowObj;
							}else{
								assignROLE=assignROLE+", "+rowObj;
							}
							roleSet.add(rowObj);
						}
					}
				}
			}			
			assignROLE=assignROLE.trim();
			if (obj[4] == null) {
				menuItems.setTitle("~"+pageUrlFlag+"~"+assignROLE);
			} else {
				menuItems.setTitle(obj[4].toString()+"~"+pageUrlFlag+"~"+assignROLE);
			}
			menuList.add(menuItems);
		}
		return menuList;
	}

	public List allMenuItems() {
		List<Object> menuList = new ArrayList<Object>();
		List menuItemList = this
				.getSession()
				.createSQLQuery(
						"select id,corpid,parentname,name,title,url from menu_item")
				.list();
		Iterator it = menuItemList.iterator();
		while (it.hasNext()) {
			Object[] obj = (Object[]) it.next();
			MenuItem menuItems = new MenuItem();
			menuItems.setId(Long.parseLong(obj[0].toString()));
			if (obj[1] == null) {
				menuItems.setCorpID("");
			} else {
				menuItems.setCorpID(obj[1].toString());
			}
			if (obj[2] == null) {
				menuItems.setParentName("");
			} else {
				menuItems.setParentName(obj[2].toString());
			}
			if (obj[3] == null) {
				menuItems.setName("");
			} else {
				menuItems.setName(obj[3].toString());
			}
			if (obj[4] == null) {
				menuItems.setTitle("");
			} else {
				menuItems.setTitle(obj[4].toString());
			}
			if (obj[5] == null) {
				menuItems.setUrl("");
			} else {
				menuItems.setUrl(obj[5].toString());
			}
			menuList.add(menuItems);
		}
		return menuList;
	}

	public List findCorpId() {
		return getHibernateTemplate().find("select corpID from Company");
	}

	public List findMenuItem(String corp, String parName, String nm, String tit) {
		List<Object> menuList = new ArrayList<Object>();

		List menuItemList = this
				.getSession()
				.createSQLQuery(
						"select id,corpid,parentname,name,title,url from menu_item where corpid like '"
								+ corp.replaceAll("'", "''").replaceAll(":",
										"''")
								+ "%' and parentname like '"
								+ parName.replaceAll("'", "''").replaceAll(":",
										"''")
								+ "%' and name like '"
								+ nm.replaceAll("'", "''")
										.replaceAll(":", "''")
								+ "%' and title like '"
								+ tit.replaceAll("'", "''").replaceAll(":",
										"''") + "%'").list();

		Iterator it = menuItemList.iterator();

		while (it.hasNext()) {
			Object[] obj = (Object[]) it.next();
			MenuItem menuItems1 = new MenuItem();
			menuItems1.setId(Long.parseLong(obj[0].toString()));
			if (obj[1] == null) {
				menuItems1.setCorpID("");
			} else {
				menuItems1.setCorpID(obj[1].toString());
			}
			if (obj[2] == null) {
				menuItems1.setParentName("");
			} else {
				menuItems1.setParentName(obj[2].toString());
			}
			if (obj[3] == null) {
				menuItems1.setName("");
			} else {
				menuItems1.setName(obj[3].toString());
			}
			if (obj[4] == null) {
				menuItems1.setTitle("");
			} else {
				menuItems1.setTitle(obj[4].toString());
			}
			if (obj[5] == null) {
				menuItems1.setUrl("");
			} else {
				menuItems1.setUrl(obj[5].toString());
			}
			menuList.add(menuItems1);
		}
		return menuList;
	}

	public List findParent() {
		return getHibernateTemplate()
				.find("select distinct parentName from MenuItem where parentName is not null and parentName !='' ");
	}

	public List findSeqNumList(String corp, String parent) {
		List menuItemList = this
				.getSession()
				.createSQLQuery(
						"select sequenceNum from menu_item where parentName='"
								+ parent
								+ "' and corpID='"
								+ corp
								+ "' and sequenceNum is not null and sequenceNum !='' order by id")
				.list();
		return menuItemList;
	}

	public List checkTitleAvailability(String corp, String parent, String titleN) {
		List checkTitleList = this
				.getSession()
				.createSQLQuery(
						"select title from menu_item where parentName='"
								+ parent + "' and corpID='" + corp
								+ "' and title='" + titleN + "' ").list();
		return checkTitleList;
	}

	public List findParByCorp(String corp) {
		List findParByCorpList = this
				.getSession()
				.createSQLQuery(
						"select distinct(name) from menu_item where corpID='"
								+ corp
								+ "' and parentName is null or parentName='' ")
				.list();
		return findParByCorpList;
	}

	public Collection findPageActions(String corp, String menuItemUrl) {
		String menuQuery = "select p.actionuri "
				+ "from pageaction p,menu_item m,pageactionpermission pp,menu_item_permission mp  "
				+ "where pp.menuitem_id = mp.id and mp.menuitemid = m.id and m.url='"
				+ menuItemUrl + "'  " + "and m.corpid='" + corp
				+ "' and pp.corpId='"+corp+"' and p.id=pp.pageaction_id and pp.permission=2 ";
		return this.getSession().createSQLQuery(menuQuery).list();
	}

	public void updateModuleActionPermission(String sidL, String corpId,
			String oldSidL) {
		if (!oldSidL.trim().equalsIgnoreCase("")) {
			String[] resultTemp = oldSidL.split(",");
			for (int k = 0; k < resultTemp.length; k++) {
				if (sidL.indexOf(resultTemp[k]) < 0) {
					String[] resultP1 = resultTemp[k].split("~");
					this.getSession()
							.createSQLQuery(
									"delete from modulepermissions where role='"
											+ resultP1[1]
											+ "' and pageaction_id="
											+ Long.parseLong(resultP1[0])
											+ " and corpId='" + corpId + "'")
							.executeUpdate();

				}
			}
		}
		if (!sidL.trim().equalsIgnoreCase("")) {
			String[] result = sidL.split(",");
			String i;
			for (int x = 0; x < result.length; x++) {
				String[] resultP = result[x].split("~");
				List urlList = null;
				List checkTitleList = this
						.getSession()
						.createSQLQuery(
								"select role from modulepermissions where pageaction_id="
										+ Long.parseLong(resultP[0])
										+ " and role='" + resultP[1]
										+ "' and corpId='" + corpId + "'")
						.list();
				if ((checkTitleList != null) && (!checkTitleList.isEmpty())) {

				} else {
					this.getSession()
							.createSQLQuery(
									"insert into modulepermissions(role,pageaction_id,permission,createdBy,createdOn,updatedBy,updatedOn,corpId)values('"
											+ resultP[1]
											+ "',"
											+ Long.parseLong(resultP[0])
											+ ",'2','admin',sysdate(),'admin',sysdate(),'"
											+ corpId + "')").executeUpdate();

				}

			}
		}
	}

	public List checkRolePermission(String url, String corpId) {

		List findParByCorpList = this
				.getSession()
				.createSQLQuery(
						"select distinct group_concat(distinct recipient) from menu_item_permission where "
								+ "menuItemId in (select id from menu_item where corpId='"
								+ corpId
								+ "' and url='"
								+ url
								+ "') and corpId='" + corpId + "'").list();

		return findParByCorpList;
	}

	public void updatePageActionPermission(Long pageId, Long menuId,
			int permission, String corpId, String role, String url) {
		
		if (menuId != null) {
			List urlList1 = this
			.getSession()
			.createSQLQuery(
					"select pageaction_id "+ 
					"from pageactionpermission "+ 
					"where   "+
					"pageaction_id="+pageId+" and corpId='"+corpId+"' and role='"+role+"'"
					).list();
			if(urlList1!=null && !urlList1.isEmpty()){
			this.getSession()
			.createSQLQuery(
					"update pageactionpermission set permission="
							+ permission + ",corpId='"+corpId+"' where pageaction_id="
							+ pageId + " and role='"+role+"'").executeUpdate();
			}else{
				this.getSession()
				.createSQLQuery(
						"insert into pageactionpermission (pageaction_id,role,permission,corpId)values("+pageId+",'"+role+"',"+permission+",'"+corpId+"')").executeUpdate();
			}
		}
	}

	public List getModuleActionListByURL(String module, String role,
			String corpId, String moduleDescr, String moduleUrl) {

		String query = "select distinct actionclass, actionuri, id,description,module from pageaction "
				+ "where actionclass not in"
				+ " ("
		        + " select distinct p.actionclass "
		        + " from menu_item m, pageactionmap pm, pageaction p "
		        + " where "
		        + " m.id = pm.menuitem_id "
		        + " and "
		        + " pm.pageaction_id=p.id "
				+ " and m.corpid='"
				+ corpId
				+ "'"
				+ " ) and module like '"
				+ module.replaceAll("'", "''").replaceAll(":", "''")
						.replace("_", "''")
				+ "%' and description like '"
				+ moduleDescr.replaceAll("'", "''").replaceAll(":", "''")
						.replace("_", "''")
				+ "%' and actionuri like '"
				+ moduleUrl.replaceAll("'", "''").replaceAll(":", "''")
						.replace("_", "''") + "%'";

		List list = this.getSession().createSQLQuery(query).list();
		List moduleList = new ArrayList();
		ActionModuleDTO actionModuleDTO = null;
		Iterator it = list.iterator();
		while (it.hasNext()) {
			actionModuleDTO = new ActionModuleDTO();
			Object[] obj = (Object[]) it.next();
			if (obj[0] != null) {
				actionModuleDTO.setActionclass(obj[0]);
			} else {
				actionModuleDTO.setActionclass("");
			}
			if (obj[1] != null) {
				actionModuleDTO.setActionuri(obj[1]);
			} else {
				actionModuleDTO.setActionuri("");
			}
			if (obj[2] != null) {
				actionModuleDTO.setId(obj[2]);
			} else {
				actionModuleDTO.setId("");
			}
			if (obj[3] != null) {
				actionModuleDTO.setDescription(obj[3]);
			} else {
				actionModuleDTO.setDescription("");
			}
			if (obj[4] != null) {
				actionModuleDTO.setModule(obj[4]);
			} else {
				actionModuleDTO.setModule("");
			}
			if (obj[2] != null) {
				List al = this
						.getSession()
						.createSQLQuery(
								"select permission from modulepermissions where pageaction_id="
										+ obj[2] + " and role='" + role
										+ "' and corpId='" + corpId + "'")
						.list();
				if ((al != null) && (!al.isEmpty())) {
					actionModuleDTO.setPermission("2");
				} else {
					actionModuleDTO.setPermission("1");
				}
			}
			moduleList.add(actionModuleDTO);
		}
		return moduleList;
	}

	public class ActionModuleDTO {
		private Object actionclass;
		private Object actionuri;
		private Object id;
		private Object description;
		private Object module;
		private Object permission;

		public Object getActionclass() {
			return actionclass;
		}

		public void setActionclass(Object actionclass) {
			this.actionclass = actionclass;
		}

		public Object getActionuri() {
			return actionuri;
		}

		public void setActionuri(Object actionuri) {
			this.actionuri = actionuri;
		}

		public Object getId() {
			return id;
		}

		public void setId(Object id) {
			this.id = id;
		}

		public Object getDescription() {
			return description;
		}

		public void setDescription(Object description) {
			this.description = description;
		}

		public Object getModule() {
			return module;
		}

		public void setModule(Object module) {
			this.module = module;
		}

		public Object getPermission() {
			return permission;
		}

		public void setPermission(Object permission) {
			this.permission = permission;
		}
	}

	
	public List getModuleList(String corpId) {
		String query = "SELECT distinct module FROM pageaction "+
					"		where actionclass not in ( "+
					"		select distinct actionclass from pageaction p, menu_item m, pageactionmap pm "+
					"		where m.id=pm.menuitem_id and pm.pageaction_id = p.id and m.corpid='"+corpId+"' "+
					"		)  "+
					"		order by module asc";
		return this.getSession().createSQLQuery(query).list();
	}

	public Long getMenuItemPermissionId(Long menuId, String role, String corpId) {
		Long cid = 0L;
		List al = this
				.getSession()
				.createSQLQuery(
						"Select id from menu_item_permission where menuItemId="
								+ menuId + " and recipient='" + role
								+ "' and corpID='" + corpId + "' limit 1")
				.list();
		if ((!al.isEmpty()) && (al != null)) {
			cid = Long.parseLong(al.get(0).toString());
		} else {
			cid = null;
		}
		return cid;
	}
	public void updatePageActionUrlList(String pageDescription,String sessionCorpId,String pageMethod,String pageClass,String pageModule,String pageUrl,String menuId,String pageId){
		if(pageId==null || pageId.equalsIgnoreCase("")){
			this.getSession()
			.createSQLQuery(
					"insert into pageaction(actionuri,actionclass,method,description,module)values('"+pageUrl+"','"+pageClass+"','"+pageMethod+"','"+pageDescription+"','"+pageModule+"')").executeUpdate();
			List urlList1 = this
			.getSession()
			.createSQLQuery(
					"select id "+ 
					"from pageaction "+ 
					"where   "+
					"actionuri='"+pageUrl+"' and actionclass='"+pageClass+"' and method='"+pageMethod+"' and module='"+pageModule+"' limit 1"
					).list();
			if(urlList1!=null && !urlList1.isEmpty() && urlList1.get(0)!=null){
				Long pageI=Long.parseLong(urlList1.get(0).toString());
				this.getSession()
				.createSQLQuery(
						"insert into pageactionmap (pageaction_id,menuitem_id) values ("+pageI+","+Long.parseLong(menuId)+")").executeUpdate();
			}
			
		}else{
			this.getSession()
			.createSQLQuery(
					"update pageaction set actionuri='"
							+ pageUrl + "',description='"+pageDescription+"' where id="
							+ pageId + "").executeUpdate();			
		}
	}
	public void removePageActionUrlList(String pageId){
		if(pageId!=null && !pageId.equalsIgnoreCase("")){
			try{
			Long id=Long.parseLong(pageId);
			this.getSession().createSQLQuery("delete from pageaction where id="+id+"").executeUpdate();
			this.getSession().createSQLQuery("delete from pageactionmap where pageaction_id="+id+"").executeUpdate();
			this.getSession().createSQLQuery("delete from pageactionpermission where pageaction_id="+id+"").executeUpdate();
			}catch(Exception e){}
		}
	}
	public String getPageRecById(String pageId){
		String pageRec="";
		List urlList1 = this
		.getSession()
		.createSQLQuery(
				"select concat(if(actionuri is null or actionuri='',' ',actionuri),'~',if(actionclass is null or actionclass='',' ',actionclass),'~',if(method is null or method='',' ',method),'~',if(description is null or description='',' ',description),'~',if(module is null or module='',' ',module))"+ 
				"from pageaction "+ 
				"where   "+
				" id="+Long.parseLong(pageId)+" limit 1"
				).list();
		if(urlList1!=null && !urlList1.isEmpty() && urlList1.get(0)!=null){
			pageRec=urlList1.get(0).toString();
		}
		return pageRec;
	}
	public String getSecurityEnabledCorpId(){
		String strCorpId="";
		List permissionsList = this.getSession().createSQLQuery("Select corpId from company where securityChecked is true").list();
		if ((!permissionsList.isEmpty()) && (permissionsList != null)&& (permissionsList.get(0) != null)) {
			Iterator itr = permissionsList.iterator();
			while (itr.hasNext()) {
				Object row = (Object) itr.next();
				if(strCorpId.equalsIgnoreCase("")){
					strCorpId="'"+row+"'";
				}else{
					strCorpId=strCorpId+",'"+row+"'";
				}
			}
		} 
		return strCorpId;
	}
	public void updateCurrentUrlPermission(String sessionCorpID){
		try {
			 Long StartTime = System.currentTimeMillis();
			 String securityEnabledCorpIdList=getSecurityEnabledCorpId();
			 if(!securityEnabledCorpIdList.equalsIgnoreCase("")){
					String pageActionQuerySql = " select pm.corpid, pm.role , p.actionuri from menu_item m, pageactionmap mp, "+
					"	pageaction p, pageactionpermission pm "+
					"	where m.id=mp.menuitem_id "+
					"	and pm.pageaction_id=p.id "+
					"   and mp.pageaction_id=pm.pageaction_id "+
					"   and pm.corpId in ("+securityEnabledCorpIdList+") "+
					"   and m.corpid in ("+securityEnabledCorpIdList+") "+
					"   and pm.permission=2"+
					"	union"+
					"	select mp.corpid, mp.recipient, m.url"+
					"	from menu_item m,menu_item_permission mp"+  
					"	where m.id=mp.menuitemid "+
					"	and mp.menuitemid = m.id"+
					"	and m.corpId in ("+securityEnabledCorpIdList+") "+
					"	and mp.permission<>0"+
					"	union"+
					"	select mp.corpid, mp.role, p.actionuri"+ 
					"	from modulepermissions mp,pageaction p "+
					"	where mp.pageaction_id = p.id  "+
					"	and mp.corpid in ("+securityEnabledCorpIdList+") and mp.permission='2'";
					Map pageActionUrls = new HashMap();
					List pageActionsByRole = getSession().createSQLQuery(pageActionQuerySql).list();
					Iterator itr = pageActionsByRole.iterator();
					while (itr.hasNext()) {
						
						Object[] row = (Object[]) itr.next();
						String corpId = (String) row[0];
						String role = (String) row[1];
						String url = (String) row[2];
						if(url.indexOf("?")>-1){
							url=url.substring(0, url.indexOf("?"));
						}
						List urlList = null;
						
						if ((urlList = (List) pageActionUrls.get(corpId + "-" + role)) == null) {
							urlList = new ArrayList();
							urlList.add(url);
							pageActionUrls.put(corpId + "-" + role, urlList);
						} else {
							urlList.add(url);
							pageActionUrls.put(corpId + "-" + role, urlList);
						}
						
					}
					permissionMap.clear();
					permissionMap.putAll(pageActionUrls);	
			 }
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to update url : "+timeTaken+"\n\n\n\n\n\n\n\n\n\\n\n\n\n\n\n");
		} catch (Exception e) {
			log.error("Error creating the role based permission cache",e);
		}
	}
}