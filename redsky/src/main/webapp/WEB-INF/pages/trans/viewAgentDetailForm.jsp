<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>

<head> 
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">  
    <title><fmt:message key="agentRequest.title"/></title>   
    <meta name="heading" content="<fmt:message key="agentRequest.title"/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
      <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
   
<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		
	</style>
 
</head>

<div class="modal-dialog"  id="myModal" style="width:1200px;height:1200px">
 <div class="modal-content">
 <div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Agent Template Details</h4>
      </div>
     
<div class="modal-body" id="myModal">
<div class="modal fade" id="showErrorModal" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg">
											</div>
									    </div>
									    <div class="modal-footer">
									     <button type="button" id="spanghclose" class="btn btn-default" data-dismiss="modal" onclick="refersh();" >Close</button>
									     </div>
								</div>
							</div>
						</div>
<div class="modal fade" id="caseModal" aria-hidden="true">
							
</div>
 <s:form id="agentDetailsForm" action="searchAgentRequest.html" method="post" > 
 <s:hidden name="agentRequestId" value="<%=request.getParameter("id") %>" />
<s:set name="viewAgentLists" value="viewAgentLists" scope="request"/> 
<s:hidden name="radioId" />

<s:hidden name="agentId" id="agentId" value="<%=request.getParameter("id") %>"/>
<s:hidden name="createdBy" value="<%=request.getParameter("createdBy") %>"/>
<s:hidden name="updatedBy" value="<%=request.getParameter("updatedBy") %>"/>
<s:hidden name="temp" value="<%=request.getParameter("temp") %>"/>
   <c:if test="${not empty viewAgentLists}">
<s:set name="viewAgentLists" value="viewAgentLists" scope="request"/>
<div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Duplicate Details Found</h4>
      </div>
<display:table name="viewAgentList" id="viewAgentLists" class="table" requestURI="" export="false" defaultsort="2" pagesize="10" style="width:99%;margin:0px 0px 10px 5px;">  		
	
	<display:column title="Partner ID" style="width:100px"><c:out value="${viewAgentLists.id}"/></display:column>
	<display:column property="lastName" sortable="true" title="Alias Name" style="width:100px"/>
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:100px"/>
	 <display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:100px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:100px"/> 
	<display:column property="add1" sortable="true" title="Adddress" style="width:150px"/> 
	<display:column property="email" sortable="true" title="Email" style="width:150px"/> 
	<display:column property="zip" sortable="true" title="Zip" style="width:50px"/> 
	<display:column property="status" sortable="true" title="status" style="width:50px"/> 
    <display:column  sortable="true" title="Created By" style="width:50px;"  value="<%=request.getParameter("createdBy") %>" ></display:column>

	<display:column  sortable="true" title="Updated By" style="width:50px;"  value="<%=request.getParameter("updatedBy") %>" ></display:column> 
</display:table>
</c:if>
 <c:if test="${empty viewAgentLists}">
<div class="modal-header bg-info header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Duplicate Details Not Found</h4>
      </div>
</c:if>
<table style="padding-top:10px !important;">
<tr>
<td align="center"  style="width:180px; font-size:14px; border:none;">Action Need To Be Taken</td>
<td style="border:none;"><s:select  cssClass="list-menu" id="status" name="status" list="{'Approved','Rejected'}" onchange="statusValue(this,'','','','');" headerKey="" headerValue="" cssStyle="width:75px; background:#e5e5e5; border-radius:3px; margin-left:6px;" />
</td>
</tr>
</table>
  <div class="modal-footer">
<button type="button" id="spanghclose" class="btn btn-default" data-dismiss="modal" onclick="refersh();" >Close</button>
</div>								    
</s:form>


</div> 
</div></div>


<script type="text/javascript">


	function statusValue(target,id,lastName,partnerCode,corpId){
		 
    if(id=='')
			{
		    var agentid = document.forms['agentDetailsForm'].elements['agentId'].value;
		   var agentRequestId= document.forms['agentDetailsForm'].elements['agentId'].value;
			}
		  var position='';
		if(target.value=="Approved")
			{
		 var url="updatedAgentData.html?ajax=1&decorator=simple&popup=true&agentStatus=" + encodeURI(target.value)+"&agentId="+agentid;
		 showOrHide(1);
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse9;
	     http2.send(null);
			}
		else
			{
			
			  var rejected='Rejected';
		      var createdBy='<%=request.getParameter("createdBy") %>';
		      var postDate = '';
		      handleHttpResponse10(agentid,agentRequestId);
			}
}
    
	  function  refersh()
	  {
		  var modal = document.getElementById('myModal');
      	  modal.style.display = "none";
      	  window.location.reload(true);
	  }
function handleHttpResponse9()
       {
	var $j = jQuery.noConflict();
            if (http2.readyState == 4)
            {
            	
               var results = http2.responseText
               results = results.trim();
               var res = results.split("#");
             	$j('#getErrorMsg').html("<p>The newly created Agent has been Approved. Hence Email has been sent to your email id  that your Request has been Approved</p>");
             	$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
             	 showOrHide(0);
             	 // window.location.reload(true);  
      	       
      	       }}

function handleHttpResponse10(agentid,agentRequestId)
{     
	var $j = jQuery.noConflict();
//   var agree = bootbox.confirm("This is the default confirm!", function(result){ console.log('This was logged in the callback: ' + result); });
             
	          /*   $j('#getErrorMsg').html("<p>The newly created Agent has been Rejected. Hence Email has been sent to your email id  that your Request has been Rejected</p>");
 	            $j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false}); */
 	          

 	        		var createdBy='<%=request.getParameter("createdBy") %>';
 	        		
 	        				var $j = jQuery.noConflict();
 	        			  	$j.ajax({
 	        			  		async: true,
 	        			  		 url: 'editAgentRequestReason.html?agentId='+agentid+'&agentRequestId='+agentRequestId+'&decorator=modal&popup=true',
 	        			         success: function(data){
 	        			        	    $j("#caseModal").modal({show:true,backdrop: 'static',keyboard: false});
 	        			        	    $j("#caseModal").html(data);
 	        							$j("#agentDetailDiv").modal("hide");
 	        							$j("#showErrorModal").modal("hide");
 	        							
 	        		            },
 	        		            error: function () {
 	        		                alert('Some error occurs');
 	        		                $j("#caseModal").modal("hide");
 	        		             }   
 	        			}); 
 	   }
     /*  
      alert("1111111111111")
    
    showConfirmation = function(close, ok) {
    
   
    	alert("2222222222222222")
	var $j = jQuery.noConflict();
	$j('#showErrorModal').modal({ backdrop: 'static', keyboard: false })
	       
	    if (close) {
	    	 alert(close)
	    	   
	  
	        $j('#showErrorModal').modal.one('click', '.modal-footer .btn-primary', close);
	    }
	    if (ok) {
	    	$j('#showErrorModal').modal.one('click', '.modal-header .close, .modal-footer .btn-default', ok);
	    }
}
    var close = function(){
        alert('window.location.href=url');
    }
    var ok = function(){
        alert('Cancel');
    }; 
    showConfirmation(close, ok);  */
   
      /*  $j('#getErrorMsg').html("<p>The newly created Agent has been Approved. Hence Email has been sent to your email id  that your Request has been Approved</p>");
             	$j('#showErrorModal').modal({show:true,backdrop: 'static',keyboard: false});
             	var success = function(){
                    alert('window.location.href=url');
                }
                var cancel = function(){
                    alert('Cancel');
                };
                showConfirmation(success, cancel); */
     
          
      /*  if(agree){
			
			var $j = jQuery.noConflict();
		  	$j.ajax({
		  		async: true,
		  		 url: 'editAgentRequestReason.html?agentRequestId='+agentid+'&createdBy='+createdBy+'&corpId='+corpid+'&decorator=modal&popup=true',
		            success: function(data){
		            	alert("entry")
		            
		            	$j("#caseModal").modal({show:true,backdrop: 'static',keyboard: false});
		            	
						$j("#caseModal").html(data);
						 $j("#agentDetailDiv").modal("hide");
						
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#caseModal").modal("hide");
	             }   
		});   */
			/* var url="editAgentRequestReason.html?ajax=1&decorator=simple&popup=true&createdBy=" + createdBy+"&corpId=" + corpid;
			
			var $j = jQuery.noConflict();

			
			
			$j('#caseModal').modal({show:true,backdrop: 'static',keyboard: false}); */
        	/*  var containerdiv =  document.getElementById("para1").scrollLeft;
    	  //	var newposition = document.getElementById("ajax_tooltipObj1").offsetLeft-containerdiv ;	
    	    document.getElementById("ajax_tooltipObj1").style.left = containerdiv+'px';	  */
        	
			/* alert("fghjbk")
			var $j = jQuery.noConflict();
			alert("enter")
		  	$j.ajax({
		  		
		  		 url: 'editAgentRequestReason.html?createdBy='+createdBy+'&corpId='+corpid+'&decorator=modal&popup=true',
		            success: function(data){
		            	alert("enter2")
		            	$j("#viewagent"+agentid).modal({show:true,backdrop: 'static',keyboard: false});
						$j("#viewagent"+agentid).html(data);
						var mydiv = document.getElementById(divId+id);
		                  document.getElementById(divId+id).style.display = "block";
		                  mydiv.innerHTML = response;
		                  mydiv.show(); 
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#viewagent"+agentid).modal("hide");
	             }   
		});   */
			/* var url="editAgentRequestReason.html?ajax=1&decorator=simple&popup=true&createdBy=" + createdBy+"&corpId=" + corpId;
			ajax_showTooltip(url,position);	 */
			/* var containerdiv =  document.getElementById("para1").scrollLeft;
			var newposition = document.getElementById("ajax_tooltipObj").offsetLeft-containerdiv ;	
			document.getElementById("ajax_tooltipObj").style.left = ((parseInt(newposition))+10)+'px';	
		   window.open('editAgentRequestReason.html?&createdBy='+createdBy+'&corpId='+corpid+'&decorator=popup&popup=true','forms','height=600,width=600,top=1, left=120, scrollbars=yes,resizable=yes');
		 
		   
			document.forms['agentDetailsForm'].action= url;	 
			document.forms['agentDetailsForm'].submit();
			return true; */
		    
		
     	/* var originEmail = res[0];
		
		subject = "Request Has been Rejected";
         var body_message = "The Selected agent "+name+" is already existing in our system RedSky. "+partnercode+" is the agent code for the same.'"; 
		
		var mailto_link = 'mailto:'+res[0]+'?subject='+subject+'&body='+body_message;
		
	alert(mailto_link)
	win = window.open(mailto_link,'emailWindow'); 
	if (win && win.open &&!win.closed) win.close(); 
	return true; */
         
var http2 = getHTTPObject();

    function showOrHide(value) {
        if (value==0) {
           if (document.layers)
               document.layers["overlay190"].visibility='hide';
            else
               document.getElementById("overlay190").style.visibility='hidden';
       }
       else if (value==1) {
          if (document.layers)
              document.layers["overlay190"].visibility='show';
           else
              document.getElementById("overlay190").style.visibility='visible';
       }
    }
    

/* function sendEmail1(target){
	The below agent “Star International Movers” is already existing in our system RedSky. T164581 is the agent code for the same.
 	var originEmail = target;
		var subject = 'C/F# ${customerFile.sequenceNumber}';
		subject = subject+" ${customerFile.firstName}";
		subject = subject+" ${customerFile.lastName}";
		
	var mailto_link = 'mailto:'+encodeURI(originEmail)+'?subject='+encodeURI(subject);		
	win = window.open(mailto_link,'emailWindow'); 
	if (win && win.open &&!win.closed) win.close(); 
}  */
//showOrHide(0);

</script>
