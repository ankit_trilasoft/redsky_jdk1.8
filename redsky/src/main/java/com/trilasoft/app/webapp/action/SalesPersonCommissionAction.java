package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CommissionStructure;
import com.trilasoft.app.model.SalesPersonCommission;
import com.trilasoft.app.service.SalesPersonCommissionManager;

public class SalesPersonCommissionAction extends BaseAction implements Preparable {
	private Long id;
	private SalesPersonCommission salesPersonCommission;
	private SalesPersonCommissionManager salesPersonCommissionManager;
	private String sessionCorpID;
	private List salesPersonCommissionList;
	static final Logger logger = Logger.getLogger(SalesPersonCommissionAction.class);
	public void prepare() throws Exception { 
		getComboList(sessionCorpID);
	}
    public SalesPersonCommissionAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	 @SkipValidation	 
	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: getComboList ## User: "+getRequest().getRemoteUser()+" Start");
	
		logger.warn("USER AUDIT: ID: getComboList ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	private String sContarct;
	private String sChargeCode;
	 @SkipValidation
	public String list()
	{ 
		logger.warn("USER AUDIT: ID: list() ## User: "+getRequest().getRemoteUser()+" Start");
		salesPersonCommissionList=salesPersonCommissionManager.getCommPersentAcRange(sessionCorpID,sContarct,sChargeCode);
		logger.warn("USER AUDIT: ID: list() ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}	
	 @SkipValidation
	public String save() throws Exception {
		 String value="";
		 SalesPersonCommission salesPersonCommission=null;
		 value=getRequest().getParameter("salesBA");
		  String str[]=value.split("~");	
		  if(!value.equalsIgnoreCase("")){
		  try {
			for(int i=0;i<str.length;i++){
				  String rec[]=str[i].split(":");			  
				  salesPersonCommission=salesPersonCommissionManager.get(Long.parseLong(rec[0]));
				  salesPersonCommission.setSalesBa(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
				  salesPersonCommission.setUpdatedOn(new Date());			
				  salesPersonCommission.setUpdatedBy(getRequest().getRemoteUser());
				  
				  salesPersonCommissionManager.save(salesPersonCommission);
			  }
		} catch (Exception e) {
			e.printStackTrace();
		}}
		  
		  
			 value=getRequest().getParameter("salesCP");
			   str=value.split("~");	
			  if(!value.equalsIgnoreCase("")){
			  try {
				for(int i=0;i<str.length;i++){
					  String rec[]=str[i].split(":");			  
					  salesPersonCommission=salesPersonCommissionManager.get(Long.parseLong(rec[0]));
					  salesPersonCommission.setCommissionPercentage(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
					  salesPersonCommission.setUpdatedOn(new Date());			
					  salesPersonCommission.setUpdatedBy(getRequest().getRemoteUser());
					  
					  salesPersonCommissionManager.save(salesPersonCommission);
				  }
			} catch (Exception e) {
				e.printStackTrace();
			}}	
			  
			  
			  value=getRequest().getParameter("commiNL");
			  if(!value.equalsIgnoreCase("")){
			  str=value.split("~");
			  try {
				for(int i=0;i<str.length;i++){
					  String rec[]=str[i].split(":");
					  salesPersonCommission = new SalesPersonCommission();
					  salesPersonCommission.setCorpID(sessionCorpID);
					  salesPersonCommission.setChargeCode(sChargeCode);
					  salesPersonCommission.setContractName(sContarct);
					  salesPersonCommission.setCreatedOn(new Date());
					  salesPersonCommission.setUpdatedOn(new Date());			
					  salesPersonCommission.setCreatedBy(getRequest().getRemoteUser());
					  salesPersonCommission.setUpdatedBy(getRequest().getRemoteUser());
					  salesPersonCommission.setSalesBa(BigDecimal.valueOf(Double.parseDouble(rec[0].toString())));
					  salesPersonCommission.setCommissionPercentage(BigDecimal.valueOf(Double.parseDouble(rec[1].toString())));
					  salesPersonCommissionManager.save(salesPersonCommission);
				  }
			} catch (Exception e) {
				e.printStackTrace();
			}}			  
		return SUCCESS;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public SalesPersonCommission getSalesPersonCommission() {
		return salesPersonCommission;
	}
	public void setSalesPersonCommission(SalesPersonCommission salesPersonCommission) {
		this.salesPersonCommission = salesPersonCommission;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getSalesPersonCommissionList() {
		return salesPersonCommissionList;
	}
	public void setSalesPersonCommissionList(List salesPersonCommissionList) {
		this.salesPersonCommissionList = salesPersonCommissionList;
	}
	public String getsContarct() {
		return sContarct;
	}
	public void setsContarct(String sContarct) {
		this.sContarct = sContarct;
	}
	public String getsChargeCode() {
		return sChargeCode;
	}
	public void setsChargeCode(String sChargeCode) {
		this.sChargeCode = sChargeCode;
	}
	public void setSalesPersonCommissionManager(
			SalesPersonCommissionManager salesPersonCommissionManager) {
		this.salesPersonCommissionManager = salesPersonCommissionManager;
	}
}
