package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsPreviewTripDao;
import com.trilasoft.app.model.DsPreviewTrip;
import com.trilasoft.app.service.DsPreviewTripManager;

public class DsPreviewTripManagerImpl extends GenericManagerImpl<DsPreviewTrip, Long> implements DsPreviewTripManager {
	
	DsPreviewTripDao dsPreviewTripDao;
	
	public DsPreviewTripManagerImpl(DsPreviewTripDao dsPreviewTripDao) {
		super(dsPreviewTripDao); 
        this.dsPreviewTripDao = dsPreviewTripDao; 
	}
	public List getListById(Long id) {
		return dsPreviewTripDao.getListById(id);
	}
}
