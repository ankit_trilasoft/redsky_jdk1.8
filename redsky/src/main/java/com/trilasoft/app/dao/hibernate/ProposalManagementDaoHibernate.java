package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;   

import javax.management.Query;

import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.model.ProposalManagement;   
import com.trilasoft.app.dao.ProposalManagementDao; 
import com.trilasoft.app.dao.hibernate.InventoryPackingDaoHibernate.InventoryPieceDTO;
import com.trilasoft.app.model.RefMaster;

public class ProposalManagementDaoHibernate extends GenericDaoHibernate<ProposalManagement, Long> implements ProposalManagementDao {   
	 public ProposalManagementDaoHibernate() {   
	        super(ProposalManagement.class);   
	    }

	 public List findDistinct() {
			return getHibernateTemplate().find(" SELECT distinct corpID FROM Company ");
		}
	 public List findListByModuleCorpStatus(String module,String corpid,String status , Date approvalDate ,Date initiationDate,String ticketNo,Boolean activeStatus)
		{
		
		 StringBuilder formatedDate1=new StringBuilder() ;
		 StringBuilder formatedDate2=new StringBuilder() ;
			try{
				if(approvalDate!=null){
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( approvalDate ) );
				}
				
				else{
					if(activeStatus){
					 return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%' AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");
					}else{
					 return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%' AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");
					}
				}
	
				
				if(initiationDate!=null){
					SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
					formatedDate2 = new StringBuilder( dateformatYYYYMMDD1.format( initiationDate ) );
				}
				else{
					if(activeStatus){
					 return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%' AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");
					}else{
					 return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%' AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");

					}
				}
				
			}catch(Exception ex){}
			if(activeStatus){
			  return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%'  AND (approvalDate BETWEEN '" + formatedDate2 + "' AND '" + formatedDate1 + "') AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");
			}else{
		      return getHibernateTemplate().find("from ProposalManagement where module like '"+module+"%' AND corpId like '"+corpid+"%' AND status like '"+status+"%'  AND (approvalDate BETWEEN '" + formatedDate2 + "' AND '" + formatedDate1 + "') AND ticketNo like '%"+ticketNo+"%' order by  modifyOn desc ");
			}
		}
	
	 
 
	 public String findProposalDescForToolTip(Long id, String sessionCorpID){
			String proposalList="";
			List list = this.getSession().createSQLQuery("SELECT description FROM proposalmanagement where id="+id+"").list();
			if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
			{
				proposalList=list.get(0).toString();
			}
			return proposalList;
		}
	 
	 public void autoSaveProposalManagementAjax(String fieldName,String fieldVal,Long serviceId){
			try{
				if(fieldVal==null){
					getSession().createSQLQuery("update proposalmanagement set "+fieldName+"='' where id= "+serviceId+" ").executeUpdate();
				}else{
					getSession().createSQLQuery("update proposalmanagement set "+fieldName+"='"+fieldVal+"' where id= "+serviceId+" ").executeUpdate();
				}
				}catch(Exception e){
			    	 e.printStackTrace();
				}
		}
	 public List listOnLoad() {
			return getHibernateTemplate().find("from ProposalManagement order by  modifyOn desc");
		}
	 
	}


