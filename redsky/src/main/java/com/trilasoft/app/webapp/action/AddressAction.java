package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import javax.servlet.http.*;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.VanLineGLTypeManager;
import com.trilasoft.app.service.VanLineManager;
import com.trilasoft.app.service.AccrualProcessManager;
import com.trilasoft.app.service.AddressManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class AddressAction extends BaseAction {
	private Long id;	 
	private VanLineGLTypeManager vanLineGLTypeManager;
	private AddressManager addressManager;
	private AccrualProcessManager accrualProcessManager;
	private VanLineManager vanLineManager;
	private String sessionCorpID;
	private static Map<String, String> glCode;	
	private static Map<String, String> glType;	
	private String gotoPageString;
	private Date beginDate;
	private Date endDate;
	private Date date;
	private String partnerCode;
	private String track;	
	private List accrualRecord;	
	private String shipNumber;
	private String registrationNumber;
	private String firstName;
	private String lastName;
	private String job;
	private String billToCode;	
	private BigDecimal revisionRevenueAmount = new BigDecimal(0);
	private BigDecimal revisionExpense = new BigDecimal(0);
	private String recGl;
    private String payGl;
    private Date revenueRecognition;
    public int i;
	
    private String billingAddress1;   
	private String billingAddress2;
	private String billingAddress3;
	private String billingAddress4;
	private String billingCity;
	private String billingState;
	private String billingZip;
	private String billingCountry;
	
	private String billingFax;
	private String billingPhone;
	private String billingTelex;
	private String billingEmail;
	private String accountLineBillingParam;
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(AddressAction.class);

	public void setAddressManager(AddressManager addressManager) {
		this.addressManager = addressManager;
	}
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	
	public String getGotoPageString() {
	   return gotoPageString;
	}
	@SkipValidation
	public String test(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");

		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		//System.out.println("change done ...................");
		return SUCCESS;
	}
	
	@SkipValidation
	public String listSearch(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
//System.out.println("\n\n\nafter change listSearch()::"+partnerCode);
			accrualRecord=addressManager.search(partnerCode,sessionCorpID,accountLineBillingParam);
			//System.out.println("\n\n444444444444444444444size::"+accrualRecord.size());
			try{
			if(accrualRecord.size()>0)
			{
				Iterator it=accrualRecord.iterator();			
				while(it.hasNext())
					{
					Object []row= (Object [])it.next();	
	  				billingAddress1=(String)row[0].toString();
					billingAddress2=(String)row[1].toString();
					billingAddress3=(String)row[2].toString();
					billingAddress4=(String)row[3].toString();
					billingCity=(String)row[4].toString();
					billingState=(String)row[5].toString();
					billingZip=(String)row[6].toString();
					billingCountry=(String)row[7].toString();					
					billingFax=(String)row[8].toString();
					billingPhone=(String)row[9].toString();
					billingTelex=(String)row[10].toString();
					billingEmail=(String)row[11].toString();
					
					//System.out.println("\n\nbillingAddress1::"+billingAddress1);
					
		       }
				
			}
			else{
				//System.out.println("\n\nNo Of Record:"+accrualRecord.size());
				
			}
			}
			
			catch(Exception e)
			{
			e.printStackTrace();	
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    return SUCCESS;
	}
	
	
	public void setGotoPageString(String gotoPageString) {
	   this.gotoPageString = gotoPageString;
	}
	private String validateFormNav;	
    public String getValidateFormNav() {
		return validateFormNav;
	}
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public AddressAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		glType = vanLineGLTypeManager.findByParameter(sessionCorpID);
		/*glCode = vanLineGLTypeManager.findGlCode(sessionCorpID);*/
		/*glCode = refMasterManager.findByParameter(corpId, "GLCODES");*/
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	

		
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	


	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public Map<String, String> getGlType() {
		return glType;
	}

	public void setGlType(Map<String, String> glType) {
		this.glType = glType;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public void setAccrualProcessManager(AccrualProcessManager accrualProcessManager) {
		this.accrualProcessManager = accrualProcessManager;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getTrack() {
		return track;
	}
	public void setTrack(String track) {
		this.track = track;
	}
	public List getAccrualRecord() {
		return accrualRecord;
	}
	public void setAccrualRecord(List accrualRecord) {
		this.accrualRecord = accrualRecord;
	}
	public String getBillToCode() {
		return billToCode;
	}
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getPayGl() {
		return payGl;
	}
	public void setPayGl(String payGl) {
		this.payGl = payGl;
	}
	public String getRecGl() {
		return recGl;
	}
	public void setRecGl(String recGl) {
		this.recGl = recGl;
	}
	public String getRegistrationNumber() {
		return registrationNumber;
	}
	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}
	public Date getRevenueRecognition() {
		return revenueRecognition;
	}
	public void setRevenueRecognition(Date revenueRecognition) {
		this.revenueRecognition = revenueRecognition;
	}
	public BigDecimal getRevisionExpense() {
		return revisionExpense;
	}
	public void setRevisionExpense(BigDecimal revisionExpense) {
		this.revisionExpense = revisionExpense;
	}
	public BigDecimal getRevisionRevenueAmount() {
		return revisionRevenueAmount;
	}
	public void setRevisionRevenueAmount(BigDecimal revisionRevenueAmount) {
		this.revisionRevenueAmount = revisionRevenueAmount;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public String getBillingAddress1() {
		return billingAddress1;
	}
	public void setBillingAddress1(String billingAddress1) {
		this.billingAddress1 = billingAddress1;
	}
	public String getBillingAddress2() {
		return billingAddress2;
	}
	public void setBillingAddress2(String billingAddress2) {
		this.billingAddress2 = billingAddress2;
	}
	public String getBillingAddress3() {
		return billingAddress3;
	}
	public void setBillingAddress3(String billingAddress3) {
		this.billingAddress3 = billingAddress3;
	}
	public String getBillingAddress4() {
		return billingAddress4;
	}
	public void setBillingAddress4(String billingAddress4) {
		this.billingAddress4 = billingAddress4;
	}
	public String getBillingCity() {
		return billingCity;
	}
	public void setBillingCity(String billingCity) {
		this.billingCity = billingCity;
	}
	public String getBillingCountry() {
		return billingCountry;
	}
	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}
	public String getBillingEmail() {
		return billingEmail;
	}
	public void setBillingEmail(String billingEmail) {
		this.billingEmail = billingEmail;
	}
	public String getBillingFax() {
		return billingFax;
	}
	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}
	public String getBillingPhone() {
		return billingPhone;
	}
	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}
	public String getBillingState() {
		return billingState;
	}
	public void setBillingState(String billingState) {
		this.billingState = billingState;
	}
	public String getBillingTelex() {
		return billingTelex;
	}
	public void setBillingTelex(String billingTelex) {
		this.billingTelex = billingTelex;
	}
	public String getBillingZip() {
		return billingZip;
	}
	public void setBillingZip(String billingZip) {
		this.billingZip = billingZip;
	}
	public String getAccountLineBillingParam() {
		return accountLineBillingParam;
	}
	public void setAccountLineBillingParam(String accountLineBillingParam) {
		this.accountLineBillingParam = accountLineBillingParam;
	}
	
	

	



}
