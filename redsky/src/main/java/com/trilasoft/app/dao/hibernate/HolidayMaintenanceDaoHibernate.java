package com.trilasoft.app.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.trilasoft.app.dao.HolidayMaintenanceDao;
import com.trilasoft.app.model.HolidayMaintenance;

public class HolidayMaintenanceDaoHibernate extends GenericDaoHibernate<HolidayMaintenance, Long> implements HolidayMaintenanceDao{
public HolidayMaintenanceDaoHibernate(){
	super(HolidayMaintenance.class);
}
public List checkHoliDayList(String type,Date hDayDate,String corpID){
	Criteria crit = this.getSession().createCriteria(HolidayMaintenance.class);
	return crit.add(Restrictions.eq("holiDayDate", hDayDate)).add(Restrictions.eq("corpID", corpID)).add(Restrictions.ilike("holiDayBranch", type)).list();
}
public List getHoliDayMaintenanceList(String corpID){
	Criteria crit = this.getSession().createCriteria(HolidayMaintenance.class);
	return crit.addOrder(Order.desc("holiDayDate")).list();
}
}
