package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.VanLineGLType;

public interface VanLineGLTypeDao extends GenericDao<VanLineGLType, Long> {
	
	public List<VanLineGLType> vanLineGLTypeList();
	
	public List<VanLineGLType> searchVanLineGLType(String glType, String description, String glCode);
	
	public Map<String, String> findByParameter(String corpId);
	
	public List isExisted(String code, String corpId);
	
	public Map<String, String> findGlCode(String corpId);
	public List<VanLineGLType> getGLPopupList(String chargesGl);
	
}