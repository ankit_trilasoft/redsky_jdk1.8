package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ControlExpirationsDao;
import com.trilasoft.app.model.ControlExpirations;

public class ControlExpirationsHibernate extends GenericDaoHibernate<ControlExpirations, Long> implements ControlExpirationsDao {
	public ControlExpirationsHibernate() {
		super(ControlExpirations.class);
	}
	public List getDriverValidation(String driverCode,String corpId,String parentId){
		return getHibernateTemplate().find(" select count(*) from ControlExpirations where fieldName='"+driverCode+"'  and corpID='"+corpId+"' and parentId=?",parentId);
	}
}
