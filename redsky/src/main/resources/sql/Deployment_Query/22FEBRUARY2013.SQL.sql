ALTER TABLE `redsky`.`accessinfo` MODIFY COLUMN `id` BIGINT(20) NOT NULL DEFAULT NULL AUTO_INCREMENT,ADD COLUMN `serviceOrderId` BIGINT(20) AFTER `destinationComment`,ADD COLUMN `customerFileId` BIGINT(20) AFTER `serviceOrderId`;

ALTER TABLE `redsky`.`accountcontact` CHANGE COLUMN `title` `title` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`proposalmanagement` ADD COLUMN `approvalFileName` VARCHAR(255) DEFAULT NULL AFTER `currency`;

ALTER TABLE `redsky`.`proposalmanagement` ADD COLUMN `approvalFileLocation` VARCHAR(450) DEFAULT NULL AFTER `approvalFileName`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `noInsurance` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `insurancePerUnit` VARCHAR(25) DEFAULT NULL AFTER `noInsurance`,  ADD COLUMN `unitForInsurance` VARCHAR(10) DEFAULT NULL AFTER `insurancePerUnit`,  ADD COLUMN `minReplacementvalue` VARCHAR(25) DEFAULT NULL AFTER `unitForInsurance`,   ADD COLUMN `minInsurancePerUnit` VARCHAR(25) NOT NULL AFTER `minReplacementvalue`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `lumpSum` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `detailedList` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `noInsurance` BIT(1) DEFAULT b'0';

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `lumpSumAmount` DECIMAL(19,2) DEFAULT '0.00' AFTER `lumpSum`,   ADD COLUMN `lumpPerUnit` VARCHAR(15) DEFAULT NULL AFTER `lumpSumAmount`,  ADD COLUMN `minReplacementvalue` VARCHAR(25) DEFAULT NULL AFTER `lumpPerUnit`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `noInsurance` VARCHAR(25) DEFAULT NULL AFTER `qfStatusReason`;

ALTER TABLE `redsky`.`billing` ADD COLUMN `noInsurance` VARCHAR(25) DEFAULT NULL AFTER `payableRate`,  ADD COLUMN `insSubmitDate` DATETIME DEFAULT NULL AFTER `noInsurance`;

ALTER TABLE `redsky`.`vehicle` ADD COLUMN `valuation` INTEGER UNSIGNED DEFAULT 0 AFTER `vehicleType`;
