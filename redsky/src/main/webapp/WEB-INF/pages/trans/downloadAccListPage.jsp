<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>Process XML List</title>   
    <meta name="heading" content="Process XML List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
<s:form id="accForm" method="post" validate="true">
 
<display:table name="downloadXMLList" class="table" requestURI="" id="downloadXMLList" pagesize="100" style="width:100%;margin-bottom:0px;"  size="100"> 
 	<display:column>
  		 <c:if test="${downloadXMLList.recPostDate == null && downloadXMLList.payPostDate == null  }"> 
  		 	<c:if test = "${ downloadXMLList.download == true}">
  		 		<input type="checkbox" name="${downloadXMLList.id }" id="${downloadXMLList.id }" value="${downloadXMLList.id }" onclick = "return updateAccId(this);" checked="checked" />
			 </c:if>
			 <c:if test = "${ downloadXMLList.download == false || downloadXMLList.download == null}">
  		 		<input type="checkbox" name="${downloadXMLList.id }" id="${downloadXMLList.id }" value="${downloadXMLList.id }" onclick = "return updateAccId(this);"  />
			 </c:if>
		 </c:if>
		<c:if test="${downloadXMLList.recPostDate != null || downloadXMLList.payPostDate != null  }">
 			<c:if test = "${ downloadXMLList.download == true}">
  		 		<input type="checkbox" name="${downloadXMLList.id }" id="${downloadXMLList.id }" value="${downloadXMLList.id }" onclick = "return updateAccId(this);" checked="checked" disabled="disabled" />
			 </c:if>
			 <c:if test = "${ downloadXMLList.download == false  ||  downloadXMLList.download == null}">
  		 		<input type="checkbox" name="${downloadXMLList.id }" id="${downloadXMLList.id }" value="${downloadXMLList.id }" onclick = "return updateAccId(this);" disabled="disabled"  />
			 </c:if>	</c:if> 
	</display:column>
	<display:column property="chargeCode" title="Charge" />
	<display:column property="estimateRevenueAmount" title ="Estimate&nbsp;Revenue" />
	<display:column property="revisionRevenueAmount" title ="Revision&nbsp;Revenue" />
	<display:column property="actualRevenue" title ="Actual&nbsp;Revenue" />
	<display:column property="distributionAmount" title ="Distribution"  />
	
	<display:column property="description" title ="Charge Code Description" />
	<display:column  title ="Driver" >
	<s:select  id="venderCode${downloadXMLList.id}" cssStyle="width:200px" onchange="venderList('${downloadXMLList.id}',this)"   value=""  cssClass="list-menu" list="%{venderCodeDetails}"  />
	</display:column>
	
</display:table>
	
</s:form>

	

