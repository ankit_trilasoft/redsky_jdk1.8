
delimiter $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_trackingstatus` $$

CREATE
DEFINER=`root`@`%`
TRIGGER `redsky`.`trigger_add_history_trackingstatus`
BEFORE UPDATE ON `redsky`.`trackingstatus`
FOR EACH ROW
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE BAcode LONGTEXT;
  DECLARE HAcode LONGTEXT;
  DECLARE ssn LONGTEXT;
  DECLARE companyDivisionCode LONGTEXT;
  DECLARE networksonumber LONGTEXT;
DECLARE updatedon DATETIME;
  set NEW.updatedon=now();
  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET BAcode = " ";
  SET HAcode = " ";
  SET ssn = " ";
  SET networksonumber = " ";
  SET companyDivisionCode = " ";
  

   IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sequenceNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;
   IF (OLD.originLocationType <> NEW.originLocationType ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originLocationType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originLocationType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originLocationType,'~') into newValueList;
  END IF;
  IF (OLD.destinationLocationType <> NEW.destinationLocationType ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationLocationType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationLocationType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationLocationType,'~') into newValueList;
  END IF;
  IF (OLD.subOriginAgentPhoneNumber <> NEW.subOriginAgentPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subOriginAgentPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subOriginAgentPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subOriginAgentPhoneNumber,'~') into newValueList;
  END IF;
  
  IF (OLD.destinationAgentPhoneNumber <> NEW.destinationAgentPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentPhoneNumber,'~') into newValueList;
  END IF;
  
  IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
      select CONCAT(fieldNameList,'trackingstatus.ugwIntId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
  END IF;
  IF (OLD.cutOffTimeTo <> NEW.cutOffTimeTo ) THEN
      select CONCAT(fieldNameList,'trackingstatus.cutOffTimeTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cutOffTimeTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cutOffTimeTo,'~') into newValueList;
  END IF;
  
  IF (OLD.shipNumber <> NEW.shipNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.shipNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.activate,'%Y-%m-%d') <> date_format(NEW.activate,'%Y-%m-%d')) or (date_format(OLD.activate,'%Y-%m-%d') is null and date_format(NEW.activate,'%Y-%m-%d') is not null)
      or (date_format(OLD.activate,'%Y-%m-%d') is not null and date_format(NEW.activate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.activate~') into fieldNameList;
    IF(OLD.activate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.activate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.activate is not null) THEN
select CONCAT(oldValueList,OLD.activate,'~') into oldValueList;
END IF;
IF(NEW.activate is not null) THEN
select CONCAT(newValueList,NEW.activate,'~') into newValueList;
END IF;
   END IF;
   
   IF ((date_format(OLD.packConfirmCallPriorOneDay,'%Y-%m-%d') <> date_format(NEW.packConfirmCallPriorOneDay,'%Y-%m-%d')) or (date_format(OLD.packConfirmCallPriorOneDay,'%Y-%m-%d') is null and date_format(NEW.packConfirmCallPriorOneDay,'%Y-%m-%d') is not null)
      or (date_format(OLD.packConfirmCallPriorOneDay,'%Y-%m-%d') is not null and date_format(NEW.packConfirmCallPriorOneDay,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packConfirmCallPriorOneDay~') into fieldNameList;
    IF(OLD.packConfirmCallPriorOneDay is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packConfirmCallPriorOneDay is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packConfirmCallPriorOneDay is not null) THEN
select CONCAT(oldValueList,OLD.packConfirmCallPriorOneDay,'~') into oldValueList;
END IF;
IF(NEW.packConfirmCallPriorOneDay is not null) THEN
select CONCAT(newValueList,NEW.packConfirmCallPriorOneDay,'~') into newValueList;
END IF;
   END IF;
   
   IF ((date_format(OLD.packConfirmCallPriorThreeDay,'%Y-%m-%d') <> date_format(NEW.packConfirmCallPriorThreeDay,'%Y-%m-%d')) or (date_format(OLD.packConfirmCallPriorThreeDay,'%Y-%m-%d') is null and date_format(NEW.packConfirmCallPriorThreeDay,'%Y-%m-%d') is not null)
      or (date_format(OLD.packConfirmCallPriorThreeDay,'%Y-%m-%d') is not null and date_format(NEW.packConfirmCallPriorThreeDay,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packConfirmCallPriorThreeDay~') into fieldNameList;
    IF(OLD.packConfirmCallPriorThreeDay is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packConfirmCallPriorThreeDay is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packConfirmCallPriorThreeDay is not null) THEN
select CONCAT(oldValueList,OLD.packConfirmCallPriorThreeDay,'~') into oldValueList;
END IF;
IF(NEW.packConfirmCallPriorThreeDay is not null) THEN
select CONCAT(newValueList,NEW.packConfirmCallPriorThreeDay,'~') into newValueList;
END IF;
   END IF;
   
   IF ((date_format(OLD.serviceCompleteDate,'%Y-%m-%d') <> date_format(NEW.serviceCompleteDate,'%Y-%m-%d')) or (date_format(OLD.serviceCompleteDate,'%Y-%m-%d') is null and date_format(NEW.serviceCompleteDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.serviceCompleteDate,'%Y-%m-%d') is not null and date_format(NEW.serviceCompleteDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.serviceCompleteDate~') into fieldNameList;
    IF(OLD.serviceCompleteDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.serviceCompleteDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.serviceCompleteDate is not null) THEN
select CONCAT(oldValueList,OLD.packConfirmCallPriorThreeDay,'~') into oldValueList;
END IF;
IF(NEW.serviceCompleteDate is not null) THEN
select CONCAT(newValueList,NEW.serviceCompleteDate,'~') into newValueList;
END IF;
   END IF;
IF ((date_format(OLD.quoteOn,'%Y-%m-%d') <> date_format(NEW.quoteOn,'%Y-%m-%d')) or (date_format(OLD.quoteOn,'%Y-%m-%d') is null and date_format(NEW.quoteOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.quoteOn,'%Y-%m-%d') is not null and date_format(NEW.quoteOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.quoteOn~') into fieldNameList;
    IF(OLD.quoteOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.quoteOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.quoteOn is not null) THEN
select CONCAT(oldValueList,OLD.quoteOn,'~') into oldValueList;
END IF;
IF(NEW.quoteOn is not null) THEN
select CONCAT(newValueList,NEW.quoteOn,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.beginPacking,'%Y-%m-%d') <> date_format(NEW.beginPacking,'%Y-%m-%d')) or (date_format(OLD.beginPacking,'%Y-%m-%d') is null and date_format(NEW.beginPacking,'%Y-%m-%d') is not null)
      or (date_format(OLD.beginPacking,'%Y-%m-%d') is not null and date_format(NEW.beginPacking,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.beginPacking~') into fieldNameList;
    IF(OLD.beginPacking is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;


IF(NEW.beginPacking is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.beginPacking is not null) THEN
select CONCAT(oldValueList,OLD.beginPacking,'~') into oldValueList;
END IF;
IF(NEW.beginPacking is not null) THEN
select CONCAT(newValueList,NEW.beginPacking,'~') into newValueList;
END IF;

update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
  IF ((date_format(OLD.endPacking,'%Y-%m-%d') <> date_format(NEW.endPacking,'%Y-%m-%d')) or (date_format(OLD.endPacking,'%Y-%m-%d') is null and date_format(NEW.endPacking,'%Y-%m-%d') is not null)
      or (date_format(OLD.endPacking,'%Y-%m-%d') is not null and date_format(NEW.endPacking,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.endPacking~') into fieldNameList;
    IF(OLD.endPacking is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.endPacking is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.endPacking is not null) THEN
select CONCAT(oldValueList,OLD.endPacking,'~') into oldValueList;
END IF;
IF(NEW.endPacking is not null) THEN
select CONCAT(newValueList,NEW.endPacking,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.beginLoad,'%Y-%m-%d') <> date_format(NEW.beginLoad,'%Y-%m-%d')) or (date_format(OLD.beginLoad,'%Y-%m-%d') is null and date_format(NEW.beginLoad,'%Y-%m-%d') is not null)
      or (date_format(OLD.beginLoad,'%Y-%m-%d') is not null and date_format(NEW.beginLoad,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.beginLoad~') into fieldNameList;
    IF(OLD.beginLoad is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.beginLoad is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.beginLoad is not null) THEN
select CONCAT(oldValueList,OLD.beginLoad,'~') into oldValueList;
END IF;
IF(NEW.beginLoad is not null) THEN
select CONCAT(newValueList,NEW.beginLoad,'~') into newValueList;
END IF;
  update sodashboard set estimateLoading=NEW.beginLoad where serviceorderid=NEW.id;
  update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
  IF ((date_format(OLD.endLoad,'%Y-%m-%d') <> date_format(NEW.endLoad,'%Y-%m-%d')) or (date_format(OLD.endLoad,'%Y-%m-%d') is null and date_format(NEW.endLoad,'%Y-%m-%d') is not null)
      or (date_format(OLD.endLoad,'%Y-%m-%d') is not null and date_format(NEW.endLoad,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.endLoad~') into fieldNameList;
    IF(OLD.endLoad is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.endLoad is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.endLoad is not null) THEN
select CONCAT(oldValueList,OLD.endLoad,'~') into oldValueList;
END IF;
IF(NEW.endLoad is not null) THEN
select CONCAT(newValueList,NEW.endLoad,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.sitOriginYN <> NEW.sitOriginYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitOriginYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitOriginYN,'~') into newValueList;
  END IF;
  IF ((OLD.sitOriginDays <> NEW.sitOriginDays) or (OLD.sitOriginDays is null and NEW.sitOriginDays is not null)
      or (OLD.sitOriginDays is not null and NEW.sitOriginDays is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.sitOriginDays~') into fieldNameList;
        IF(OLD.sitOriginDays is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sitOriginDays is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.sitOriginDays is not null) then
            select CONCAT(oldValueList,OLD.sitOriginDays,'~') into oldValueList;
        END IF;
        IF(NEW.sitOriginDays is not null) then
            select CONCAT(newValueList,NEW.sitOriginDays,'~') into newValueList;
        END IF;
    END IF;
      IF ((date_format(OLD.sitOriginOn,'%Y-%m-%d') <> date_format(NEW.sitOriginOn,'%Y-%m-%d')) or (date_format(OLD.sitOriginOn,'%Y-%m-%d') is null and date_format(NEW.sitOriginOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitOriginOn,'%Y-%m-%d') is not null and date_format(NEW.sitOriginOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginOn~') into fieldNameList;
    IF(OLD.sitOriginOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitOriginOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitOriginOn is not null) THEN
select CONCAT(oldValueList,OLD.sitOriginOn,'~') into oldValueList;
END IF;
IF(NEW.sitOriginOn is not null) THEN
select CONCAT(newValueList,NEW.sitOriginOn,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.sitOriginTA <> NEW.sitOriginTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitOriginTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitOriginTA,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.packDone,'%Y-%m-%d') <> date_format(NEW.packDone,'%Y-%m-%d')) or (date_format(OLD.packDone,'%Y-%m-%d') is null and date_format(NEW.packDone,'%Y-%m-%d') is not null)
      or (date_format(OLD.packDone,'%Y-%m-%d') is not null and date_format(NEW.packDone,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packDone~') into fieldNameList;
    IF(OLD.packDone is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packDone is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packDone is not null) THEN
select CONCAT(oldValueList,OLD.packDone,'~') into oldValueList;
END IF;
IF(NEW.packDone is not null) THEN
select CONCAT(newValueList,NEW.packDone,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.paymentRecived,'%Y-%m-%d') <> date_format(NEW.paymentRecived,'%Y-%m-%d')) or (date_format(OLD.paymentRecived,'%Y-%m-%d') is null and date_format(NEW.paymentRecived,'%Y-%m-%d') is not null)
      or (date_format(OLD.paymentRecived,'%Y-%m-%d') is not null and date_format(NEW.paymentRecived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.paymentRecived~') into fieldNameList;
    IF(OLD.paymentRecived is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.paymentRecived is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.paymentRecived is not null) THEN
select CONCAT(oldValueList,OLD.paymentRecived,'~') into oldValueList;
END IF;
IF(NEW.paymentRecived is not null) THEN
select CONCAT(newValueList,NEW.paymentRecived,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.managerOkInital <> NEW.managerOkInital ) THEN
      select CONCAT(fieldNameList,'trackingstatus.managerOkInital~') into fieldNameList;
      select CONCAT(oldValueList,OLD.managerOkInital,'~') into oldValueList;
      select CONCAT(newValueList,NEW.managerOkInital,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.managerOkOn,'%Y-%m-%d') <> date_format(NEW.managerOkOn,'%Y-%m-%d')) or (date_format(OLD.managerOkOn,'%Y-%m-%d') is null and date_format(NEW.managerOkOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.managerOkOn,'%Y-%m-%d') is not null and date_format(NEW.managerOkOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.managerOkOn~') into fieldNameList;
    IF(OLD.managerOkOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.managerOkOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.managerOkOn is not null) THEN
select CONCAT(oldValueList,OLD.managerOkOn,'~') into oldValueList;
END IF;
IF(NEW.managerOkOn is not null) THEN
select CONCAT(newValueList,NEW.managerOkOn,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.instruction2FF,'%Y-%m-%d') <> date_format(NEW.instruction2FF,'%Y-%m-%d')) or (date_format(OLD.instruction2FF,'%Y-%m-%d') is null and date_format(NEW.instruction2FF,'%Y-%m-%d') is not null)
      or (date_format(OLD.instruction2FF,'%Y-%m-%d') is not null and date_format(NEW.instruction2FF,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.instruction2FF~') into fieldNameList;
    IF(OLD.instruction2FF is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.instruction2FF is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.instruction2FF is not null) THEN
select CONCAT(oldValueList,OLD.instruction2FF,'~') into oldValueList;
END IF;
IF(NEW.instruction2FF is not null) THEN
select CONCAT(newValueList,NEW.instruction2FF,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.leftWHOn,'%Y-%m-%d') <> date_format(NEW.leftWHOn,'%Y-%m-%d')) or (date_format(OLD.leftWHOn,'%Y-%m-%d') is null and date_format(NEW.leftWHOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.leftWHOn,'%Y-%m-%d') is not null and date_format(NEW.leftWHOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.leftWHOn~') into fieldNameList;
    IF(OLD.leftWHOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.leftWHOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.leftWHOn is not null) THEN
select CONCAT(oldValueList,OLD.leftWHOn,'~') into oldValueList;
END IF;
IF(NEW.leftWHOn is not null) THEN
select CONCAT(newValueList,NEW.leftWHOn,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.sitDestinationYN <> NEW.sitDestinationYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationYN,'~') into newValueList;
  END IF;
 IF ((OLD.sitDestinationDays <> NEW.sitDestinationDays) or (OLD.sitDestinationDays is null and NEW.sitDestinationDays is not null)
      or (OLD.sitDestinationDays is not null and NEW.sitDestinationDays is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.sitDestinationDays~') into fieldNameList;
        IF(OLD.sitDestinationDays is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.sitDestinationDays is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.sitDestinationDays is not null) then
            select CONCAT(oldValueList,OLD.sitDestinationDays,'~') into oldValueList;
        END IF;
        IF(NEW.sitDestinationDays is not null) then
            select CONCAT(newValueList,NEW.sitDestinationDays,'~') into newValueList;
        END IF;
    END IF;
  IF ((date_format(OLD.sitDestinationIn,'%Y-%m-%d') <> date_format(NEW.sitDestinationIn,'%Y-%m-%d')) or (date_format(OLD.sitDestinationIn,'%Y-%m-%d') is null and date_format(NEW.sitDestinationIn,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitDestinationIn,'%Y-%m-%d') is not null and date_format(NEW.sitDestinationIn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationIn~') into fieldNameList;
    IF(OLD.sitDestinationIn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitDestinationIn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitDestinationIn is not null) THEN
select CONCAT(oldValueList,OLD.sitDestinationIn,'~') into oldValueList;
END IF;
IF(NEW.sitDestinationIn is not null) THEN
select CONCAT(newValueList,NEW.sitDestinationIn,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.sitDestinationTA <> NEW.sitDestinationTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationTA,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.deliveryShipper,'%Y-%m-%d') <> date_format(NEW.deliveryShipper,'%Y-%m-%d')) or (date_format(OLD.deliveryShipper,'%Y-%m-%d') is null and date_format(NEW.deliveryShipper,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryShipper,'%Y-%m-%d') is not null and date_format(NEW.deliveryShipper,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryShipper~') into fieldNameList;
    IF(OLD.deliveryShipper is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryShipper is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryShipper is not null) THEN
select CONCAT(oldValueList,OLD.deliveryShipper,'~') into oldValueList;
END IF;
IF(NEW.deliveryShipper is not null) THEN
select CONCAT(newValueList,NEW.deliveryShipper,'~') into newValueList;
END IF;
  update sodashboard set estimateDelivery=NEW.deliveryShipper where serviceorderid=NEW.id;
  update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
 IF ((date_format(OLD.omniReport,'%Y-%m-%d') <> date_format(NEW.omniReport,'%Y-%m-%d')) or (date_format(OLD.omniReport,'%Y-%m-%d') is null and date_format(NEW.omniReport,'%Y-%m-%d') is not null)
      or (date_format(OLD.omniReport,'%Y-%m-%d') is not null and date_format(NEW.omniReport,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.omniReport~') into fieldNameList;
    IF(OLD.omniReport is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.omniReport is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.omniReport is not null) THEN
select CONCAT(oldValueList,OLD.omniReport,'~') into oldValueList;
END IF;
IF(NEW.omniReport is not null) THEN
select CONCAT(newValueList,NEW.omniReport,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.orderDate,'%Y-%m-%d') <> date_format(NEW.orderDate,'%Y-%m-%d')) or (date_format(OLD.orderDate,'%Y-%m-%d') is null and date_format(NEW.orderDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.orderDate,'%Y-%m-%d') is not null and date_format(NEW.orderDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.orderDate~') into fieldNameList;
    IF(OLD.orderDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.orderDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.orderDate is not null) THEN
select CONCAT(oldValueList,OLD.orderDate,'~') into oldValueList;
END IF;
IF(NEW.orderDate is not null) THEN
select CONCAT(newValueList,NEW.orderDate,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.vehicleRecived,'%Y-%m-%d') <> date_format(NEW.vehicleRecived,'%Y-%m-%d')) or (date_format(OLD.vehicleRecived,'%Y-%m-%d') is null and date_format(NEW.vehicleRecived,'%Y-%m-%d') is not null)
      or (date_format(OLD.vehicleRecived,'%Y-%m-%d') is not null and date_format(NEW.vehicleRecived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.vehicleRecived~') into fieldNameList;
    IF(OLD.vehicleRecived is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.vehicleRecived is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.vehicleRecived is not null) THEN
select CONCAT(oldValueList,OLD.vehicleRecived,'~') into oldValueList;
END IF;
IF(NEW.vehicleRecived is not null) THEN
select CONCAT(newValueList,NEW.vehicleRecived,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.freeEntry3299 <> NEW.freeEntry3299 ) THEN
      select CONCAT(fieldNameList,'trackingstatus.freeEntry3299~') into fieldNameList;
      select CONCAT(oldValueList,OLD.freeEntry3299,'~') into oldValueList;
      select CONCAT(newValueList,NEW.freeEntry3299,'~') into newValueList;
  END IF;
IF ((date_format(OLD.mail3299,'%Y-%m-%d') <> date_format(NEW.mail3299,'%Y-%m-%d')) or (date_format(OLD.mail3299,'%Y-%m-%d') is null and date_format(NEW.mail3299,'%Y-%m-%d') is not null)
      or (date_format(OLD.mail3299,'%Y-%m-%d') is not null and date_format(NEW.mail3299,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.mail3299~') into fieldNameList;
    IF(OLD.mail3299 is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.mail3299 is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.mail3299 is not null) THEN
select CONCAT(oldValueList,OLD.mail3299,'~') into oldValueList;
END IF;
IF(NEW.mail3299 is not null) THEN
select CONCAT(newValueList,NEW.mail3299,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.recived3299,'%Y-%m-%d') <> date_format(NEW.recived3299,'%Y-%m-%d')) or (date_format(OLD.recived3299,'%Y-%m-%d') is null and date_format(NEW.recived3299,'%Y-%m-%d') is not null)
      or (date_format(OLD.recived3299,'%Y-%m-%d') is not null and date_format(NEW.recived3299,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.recived3299~') into fieldNameList;
    IF(OLD.recived3299 is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.recived3299 is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.recived3299 is not null) THEN
select CONCAT(oldValueList,OLD.recived3299,'~') into oldValueList;
END IF;
IF(NEW.recived3299 is not null) THEN
select CONCAT(newValueList,NEW.recived3299,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.sent3299ToBroker,'%Y-%m-%d') <> date_format(NEW.sent3299ToBroker,'%Y-%m-%d')) or (date_format(OLD.sent3299ToBroker,'%Y-%m-%d') is null and date_format(NEW.sent3299ToBroker,'%Y-%m-%d') is not null)
      or (date_format(OLD.sent3299ToBroker,'%Y-%m-%d') is not null and date_format(NEW.sent3299ToBroker,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sent3299ToBroker~') into fieldNameList;
    IF(OLD.sent3299ToBroker is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sent3299ToBroker is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sent3299ToBroker is not null) THEN
select CONCAT(oldValueList,OLD.sent3299ToBroker,'~') into oldValueList;
END IF;
IF(NEW.sent3299ToBroker is not null) THEN
select CONCAT(newValueList,NEW.sent3299ToBroker,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.originBL,'%Y-%m-%d') <> date_format(NEW.originBL,'%Y-%m-%d')) or (date_format(OLD.originBL,'%Y-%m-%d') is null and date_format(NEW.originBL,'%Y-%m-%d') is not null)
      or (date_format(OLD.originBL,'%Y-%m-%d') is not null and date_format(NEW.originBL,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.originBL~') into fieldNameList;
    IF(OLD.originBL is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.originBL is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.originBL is not null) THEN
select CONCAT(oldValueList,OLD.originBL,'~') into oldValueList;
END IF;
IF(NEW.originBL is not null) THEN
select CONCAT(newValueList,NEW.originBL,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.clearCustom,'%Y-%m-%d') <> date_format(NEW.clearCustom,'%Y-%m-%d')) or (date_format(OLD.clearCustom,'%Y-%m-%d') is null and date_format(NEW.clearCustom,'%Y-%m-%d') is not null)
      or (date_format(OLD.clearCustom,'%Y-%m-%d') is not null and date_format(NEW.clearCustom,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.clearCustom~') into fieldNameList;
    IF(OLD.clearCustom is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.clearCustom is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.clearCustom is not null) THEN
select CONCAT(oldValueList,OLD.clearCustom,'~') into oldValueList;
END IF;
IF(NEW.clearCustom is not null) THEN
select CONCAT(newValueList,NEW.clearCustom,'~') into newValueList;
END IF;
  update sodashboard set customDate=NEW.clearCustom where serviceorderid=NEW.id;
   END IF;
  IF ((date_format(OLD.brokerDeliveryOrder,'%Y-%m-%d') <> date_format(NEW.brokerDeliveryOrder,'%Y-%m-%d')) or (date_format(OLD.brokerDeliveryOrder,'%Y-%m-%d') is null and date_format(NEW.brokerDeliveryOrder,'%Y-%m-%d') is not null)
      or (date_format(OLD.brokerDeliveryOrder,'%Y-%m-%d') is not null and date_format(NEW.brokerDeliveryOrder,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.brokerDeliveryOrder~') into fieldNameList;
    IF(OLD.brokerDeliveryOrder is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.brokerDeliveryOrder is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.brokerDeliveryOrder is not null) THEN
select CONCAT(oldValueList,OLD.brokerDeliveryOrder,'~') into oldValueList;
END IF;
IF(NEW.brokerDeliveryOrder is not null) THEN
select CONCAT(newValueList,NEW.brokerDeliveryOrder,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.pickUp2PierWH,'%Y-%m-%d') <> date_format(NEW.pickUp2PierWH,'%Y-%m-%d')) or (date_format(OLD.pickUp2PierWH,'%Y-%m-%d') is null and date_format(NEW.pickUp2PierWH,'%Y-%m-%d') is not null)
      or (date_format(OLD.pickUp2PierWH,'%Y-%m-%d') is not null and date_format(NEW.pickUp2PierWH,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.pickUp2PierWH~') into fieldNameList;
    IF(OLD.pickUp2PierWH is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.pickUp2PierWH is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.pickUp2PierWH is not null) THEN
select CONCAT(oldValueList,OLD.pickUp2PierWH,'~') into oldValueList;
END IF;
IF(NEW.pickUp2PierWH is not null) THEN
select CONCAT(newValueList,NEW.pickUp2PierWH,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.pickUp2PierWHDestination,'%Y-%m-%d') <> date_format(NEW.pickUp2PierWHDestination,'%Y-%m-%d')) or (date_format(OLD.pickUp2PierWHDestination,'%Y-%m-%d') is null and date_format(NEW.pickUp2PierWHDestination,'%Y-%m-%d') is not null)
      or (date_format(OLD.pickUp2PierWHDestination,'%Y-%m-%d') is not null and date_format(NEW.pickUp2PierWHDestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.pickUp2PierWHDestination~') into fieldNameList;
    IF(OLD.pickUp2PierWHDestination is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.pickUp2PierWHDestination is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.pickUp2PierWHDestination is not null) THEN
select CONCAT(oldValueList,OLD.pickUp2PierWHDestination,'~') into oldValueList;
END IF;
IF(NEW.pickUp2PierWHDestination is not null) THEN
select CONCAT(newValueList,NEW.pickUp2PierWHDestination,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.pickUp2PierWHTA <> NEW.pickUp2PierWHTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.pickUp2PierWHTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pickUp2PierWHTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pickUp2PierWHTA,'~') into newValueList;
  END IF;
  IF (OLD.pickUp2PierWHTADestination <> NEW.pickUp2PierWHTADestination ) THEN
      select CONCAT(fieldNameList,'trackingstatus.pickUp2PierWHTADestination~') into fieldNameList;
      select CONCAT(oldValueList,OLD.pickUp2PierWHTADestination,'~') into oldValueList;
      select CONCAT(newValueList,NEW.pickUp2PierWHTADestination,'~') into newValueList;
  END IF;
   IF ((OLD.clearCustomTA <> NEW.clearCustomTA) or (OLD.clearCustomTA is null and NEW.clearCustomTA is not null)
      or (OLD.clearCustomTA is not null and NEW.clearCustomTA is null) 
      or (OLD.clearCustomTA='' and NEW.clearCustomTA<>'')
      or (OLD.clearCustomTA<>'' and NEW.clearCustomTA='')) THEN
      select CONCAT(fieldNameList,'trackingstatus.clearCustomTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.clearCustomTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.clearCustomTA,'~') into newValueList;
  update sodashboard set customFlag=NEW.clearCustomTA where serviceorderid=NEW.id;
  END IF;
 IF ((date_format(OLD.deliveryLastDay,'%Y-%m-%d') <> date_format(NEW.deliveryLastDay,'%Y-%m-%d')) or (date_format(OLD.deliveryLastDay,'%Y-%m-%d') is null and date_format(NEW.deliveryLastDay,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryLastDay,'%Y-%m-%d') is not null and date_format(NEW.deliveryLastDay,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryLastDay~') into fieldNameList;
    IF(OLD.deliveryLastDay is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryLastDay is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryLastDay is not null) THEN
select CONCAT(oldValueList,OLD.deliveryLastDay,'~') into oldValueList;
END IF;
IF(NEW.deliveryLastDay is not null) THEN
select CONCAT(newValueList,NEW.deliveryLastDay,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.preferedLoadDate,'%Y-%m-%d') <> date_format(NEW.preferedLoadDate,'%Y-%m-%d')) or (date_format(OLD.preferedLoadDate,'%Y-%m-%d') is null and date_format(NEW.preferedLoadDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.preferedLoadDate,'%Y-%m-%d') is not null and date_format(NEW.preferedLoadDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.preferedLoadDate~') into fieldNameList;
    IF(OLD.preferedLoadDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.preferedLoadDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.preferedLoadDate is not null) THEN
select CONCAT(oldValueList,OLD.preferedLoadDate,'~') into oldValueList;
END IF;
IF(NEW.preferedLoadDate is not null) THEN
select CONCAT(newValueList,NEW.preferedLoadDate,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.preferedPackDate,'%Y-%m-%d') <> date_format(NEW.preferedPackDate,'%Y-%m-%d')) or (date_format(OLD.preferedPackDate,'%Y-%m-%d') is null and date_format(NEW.preferedPackDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.preferedPackDate,'%Y-%m-%d') is not null and date_format(NEW.preferedPackDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.preferedPackDate~') into fieldNameList;
    IF(OLD.preferedPackDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.preferedPackDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.preferedPackDate is not null) THEN
select CONCAT(oldValueList,OLD.preferedPackDate,'~') into oldValueList;
END IF;
IF(NEW.preferedPackDate is not null) THEN
select CONCAT(newValueList,NEW.preferedPackDate,'~') into newValueList;
END IF;
   END IF;
IF ((date_format(OLD.recivedGBL,'%Y-%m-%d') <> date_format(NEW.recivedGBL,'%Y-%m-%d')) or (date_format(OLD.recivedGBL,'%Y-%m-%d') is null and date_format(NEW.recivedGBL,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedGBL,'%Y-%m-%d') is not null and date_format(NEW.recivedGBL,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.recivedGBL~') into fieldNameList;
    IF(OLD.recivedGBL is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.recivedGBL is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.recivedGBL is not null) THEN
select CONCAT(oldValueList,OLD.recivedGBL,'~') into oldValueList;
END IF;
IF(NEW.recivedGBL is not null) THEN
select CONCAT(newValueList,NEW.recivedGBL,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.requestGBL,'%Y-%m-%d') <> date_format(NEW.requestGBL,'%Y-%m-%d')) or (date_format(OLD.requestGBL,'%Y-%m-%d') is null and date_format(NEW.requestGBL,'%Y-%m-%d') is not null)
      or (date_format(OLD.requestGBL,'%Y-%m-%d') is not null and date_format(NEW.requestGBL,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.requestGBL~') into fieldNameList;
    IF(OLD.requestGBL is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.requestGBL is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.requestGBL is not null) THEN
select CONCAT(oldValueList,OLD.requestGBL,'~') into oldValueList;
END IF;
IF(NEW.requestGBL is not null) THEN
select CONCAT(newValueList,NEW.requestGBL,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.estimate2Account,'%Y-%m-%d') <> date_format(NEW.estimate2Account,'%Y-%m-%d')) or (date_format(OLD.estimate2Account,'%Y-%m-%d') is null and date_format(NEW.estimate2Account,'%Y-%m-%d') is not null)
      or (date_format(OLD.estimate2Account,'%Y-%m-%d') is not null and date_format(NEW.estimate2Account,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.estimate2Account~') into fieldNameList;
    IF(OLD.estimate2Account is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.estimate2Account is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.estimate2Account is not null) THEN
select CONCAT(oldValueList,OLD.estimate2Account,'~') into oldValueList;
END IF;
IF(NEW.estimate2Account is not null) THEN
select CONCAT(newValueList,NEW.estimate2Account,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.sitOriginLetter,'%Y-%m-%d') <> date_format(NEW.sitOriginLetter,'%Y-%m-%d')) or (date_format(OLD.sitOriginLetter,'%Y-%m-%d') is null and date_format(NEW.sitOriginLetter,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitOriginLetter,'%Y-%m-%d') is not null and date_format(NEW.sitOriginLetter,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginLetter~') into fieldNameList;
    IF(OLD.sitOriginLetter is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitOriginLetter is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitOriginLetter is not null) THEN
select CONCAT(oldValueList,OLD.sitOriginLetter,'~') into oldValueList;
END IF;
IF(NEW.sitOriginLetter is not null) THEN
select CONCAT(newValueList,NEW.sitOriginLetter,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.sitDestinationLetter,'%Y-%m-%d') <> date_format(NEW.sitDestinationLetter,'%Y-%m-%d')) or (date_format(OLD.sitDestinationLetter,'%Y-%m-%d') is null and date_format(NEW.sitDestinationLetter,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitDestinationLetter,'%Y-%m-%d') is not null and date_format(NEW.sitDestinationLetter,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationLetter~') into fieldNameList;
    IF(OLD.sitDestinationLetter is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitDestinationLetter is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitDestinationLetter is not null) THEN
select CONCAT(oldValueList,OLD.sitDestinationLetter,'~') into oldValueList;
END IF;
IF(NEW.sitDestinationLetter is not null) THEN
select CONCAT(newValueList,NEW.sitDestinationLetter,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.sitOriginLetterTA <> NEW.sitOriginLetterTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginLetterTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitOriginLetterTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitOriginLetterTA,'~') into newValueList;
  END IF;
  IF (OLD.sitDestinationLetterTA <> NEW.sitDestinationLetterTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationLetterTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationLetterTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationLetterTA,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.mailEvaluation,'%Y-%m-%d') <> date_format(NEW.mailEvaluation,'%Y-%m-%d')) or (date_format(OLD.mailEvaluation,'%Y-%m-%d') is null and date_format(NEW.mailEvaluation,'%Y-%m-%d') is not null)
      or (date_format(OLD.mailEvaluation,'%Y-%m-%d') is not null and date_format(NEW.mailEvaluation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.mailEvaluation~') into fieldNameList;
    IF(OLD.mailEvaluation is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.mailEvaluation is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.mailEvaluation is not null) THEN
select CONCAT(oldValueList,OLD.mailEvaluation,'~') into oldValueList;
END IF;
IF(NEW.mailEvaluation is not null) THEN
select CONCAT(newValueList,NEW.mailEvaluation,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.rating,'%Y-%m-%d') <> date_format(NEW.rating,'%Y-%m-%d')) or (date_format(OLD.rating,'%Y-%m-%d') is null and date_format(NEW.rating,'%Y-%m-%d') is not null)
      or (date_format(OLD.rating,'%Y-%m-%d') is not null and date_format(NEW.rating,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.rating~') into fieldNameList;
    IF(OLD.rating is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.rating is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.rating is not null) THEN
select CONCAT(oldValueList,OLD.rating,'~') into oldValueList;
END IF;
IF(NEW.rating is not null) THEN
select CONCAT(newValueList,NEW.rating,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.recivedEvaluation,'%Y-%m-%d') <> date_format(NEW.recivedEvaluation,'%Y-%m-%d')) or (date_format(OLD.recivedEvaluation,'%Y-%m-%d') is null and date_format(NEW.recivedEvaluation,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedEvaluation,'%Y-%m-%d') is not null and date_format(NEW.recivedEvaluation,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.recivedEvaluation~') into fieldNameList;
    IF(OLD.recivedEvaluation is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.recivedEvaluation is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.recivedEvaluation is not null) THEN
select CONCAT(oldValueList,OLD.recivedEvaluation,'~') into oldValueList;
END IF;
IF(NEW.recivedEvaluation is not null) THEN
select CONCAT(newValueList,NEW.recivedEvaluation,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.recivedPayment,'%Y-%m-%d') <> date_format(NEW.recivedPayment,'%Y-%m-%d')) or (date_format(OLD.recivedPayment,'%Y-%m-%d') is null and date_format(NEW.recivedPayment,'%Y-%m-%d') is not null)
      or (date_format(OLD.recivedPayment,'%Y-%m-%d') is not null and date_format(NEW.recivedPayment,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.recivedPayment~') into fieldNameList;
    IF(OLD.recivedPayment is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.recivedPayment is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.recivedPayment is not null) THEN
select CONCAT(oldValueList,OLD.recivedPayment,'~') into oldValueList;
END IF;
IF(NEW.recivedPayment is not null) THEN
select CONCAT(newValueList,NEW.recivedPayment,'~') into newValueList;
END IF;
   END IF;
    IF ((OLD.fromWH <> NEW.fromWH) or (OLD.fromWH is null and NEW.fromWH is not null)
or (OLD.fromWH is not null and NEW.fromWH is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.fromWH~') into fieldNameList;
        IF(OLD.fromWH is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.fromWH is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.fromWH is not null) then
            select CONCAT(oldValueList,OLD.fromWH,'~') into oldValueList;
        END IF;
        IF(NEW.fromWH is not null) then
            select CONCAT(newValueList,NEW.fromWH,'~') into newValueList;
        END IF;
END IF;
   IF (OLD.gbl <> NEW.gbl ) THEN
      select CONCAT(fieldNameList,'trackingstatus.gbl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gbl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gbl,'~') into newValueList;
      update serviceorder SET gbl = NEW.gbl,updatedBy=NEW.updatedBy,updatedOn=now() where id=OLD.id;
  END IF;
  IF (OLD.billLading <> NEW.billLading ) THEN
      select CONCAT(fieldNameList,'trackingstatus.billLading~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billLading,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billLading,'~') into newValueList;
  END IF;
  
  IF (OLD.proNumber <> NEW.proNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.proNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.proNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.proNumber,'~') into newValueList;
  END IF;
  
   IF ((date_format(OLD.deliveryTA,'%Y-%m-%d') <> date_format(NEW.deliveryTA,'%Y-%m-%d')) or (date_format(OLD.deliveryTA,'%Y-%m-%d') is null and date_format(NEW.deliveryTA,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryTA,'%Y-%m-%d') is not null and date_format(NEW.deliveryTA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryTA~') into fieldNameList;
    IF(OLD.deliveryTA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryTA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryTA is not null) THEN
select CONCAT(oldValueList,OLD.deliveryTA,'~') into oldValueList;
END IF;
IF(NEW.deliveryTA is not null) THEN
select CONCAT(newValueList,NEW.deliveryTA,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.preliminaryNotification,'%Y-%m-%d') <> date_format(NEW.preliminaryNotification,'%Y-%m-%d')) or (date_format(OLD.preliminaryNotification,'%Y-%m-%d') is null and date_format(NEW.preliminaryNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.preliminaryNotification,'%Y-%m-%d') is not null and date_format(NEW.preliminaryNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.preliminaryNotification~') into fieldNameList;
    IF(OLD.preliminaryNotification is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.preliminaryNotification is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.preliminaryNotification is not null) THEN
select CONCAT(oldValueList,OLD.preliminaryNotification,'~') into oldValueList;
END IF;
IF(NEW.preliminaryNotification is not null) THEN
select CONCAT(newValueList,NEW.preliminaryNotification,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.finalNotification,'%Y-%m-%d') <> date_format(NEW.finalNotification,'%Y-%m-%d')) or (date_format(OLD.finalNotification,'%Y-%m-%d') is null and date_format(NEW.finalNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.finalNotification,'%Y-%m-%d') is not null and date_format(NEW.finalNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.finalNotification~') into fieldNameList;
    IF(OLD.finalNotification is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.finalNotification is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.finalNotification is not null) THEN
select CONCAT(oldValueList,OLD.finalNotification,'~') into oldValueList;
END IF;
IF(NEW.finalNotification is not null) THEN
select CONCAT(newValueList,NEW.finalNotification,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.itNumber <> NEW.itNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.itNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.itNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.itNumber,'~') into newValueList;
  END IF;
  IF (OLD.itNumber2 <> NEW.itNumber2 ) THEN
      select CONCAT(fieldNameList,'trackingstatus.itNumber2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.itNumber2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.itNumber2,'~') into newValueList;
  END IF;
  IF (OLD.flagCarrier <> NEW.flagCarrier ) THEN
      select CONCAT(fieldNameList,'trackingstatus.flagCarrier~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flagCarrier,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flagCarrier,'~') into newValueList;
  END IF;
  IF (OLD.returnContainer <> NEW.returnContainer ) THEN
      select CONCAT(fieldNameList,'trackingstatus.returnContainer~') into fieldNameList;
      select CONCAT(oldValueList,OLD.returnContainer,'~') into oldValueList;
      select CONCAT(newValueList,NEW.returnContainer,'~') into newValueList;
  END IF;
 IF (OLD.returnContainerDestination <> NEW.returnContainerDestination ) THEN
      select CONCAT(fieldNameList,'trackingstatus.returnContainerDestination~') into fieldNameList;
      select CONCAT(oldValueList,OLD.returnContainerDestination,'~') into oldValueList;
      select CONCAT(newValueList,NEW.returnContainerDestination,'~') into newValueList;
  END IF;
 IF ((date_format(OLD.containerRecivedWH,'%Y-%m-%d') <> date_format(NEW.containerRecivedWH,'%Y-%m-%d')) or (date_format(OLD.containerRecivedWH,'%Y-%m-%d') is null and date_format(NEW.containerRecivedWH,'%Y-%m-%d') is not null)
      or (date_format(OLD.containerRecivedWH,'%Y-%m-%d') is not null and date_format(NEW.containerRecivedWH,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.containerRecivedWH~') into fieldNameList;
    IF(OLD.containerRecivedWH is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.containerRecivedWH is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.containerRecivedWH is not null) THEN
select CONCAT(oldValueList,OLD.containerRecivedWH,'~') into oldValueList;
END IF;
IF(NEW.containerRecivedWH is not null) THEN
select CONCAT(newValueList,NEW.containerRecivedWH,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.reconfirmShipper,'%Y-%m-%d') <> date_format(NEW.reconfirmShipper,'%Y-%m-%d')) or (date_format(OLD.reconfirmShipper,'%Y-%m-%d') is null and date_format(NEW.reconfirmShipper,'%Y-%m-%d') is not null)
      or (date_format(OLD.reconfirmShipper,'%Y-%m-%d') is not null and date_format(NEW.reconfirmShipper,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.reconfirmShipper~') into fieldNameList;
    IF(OLD.reconfirmShipper is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.reconfirmShipper is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.reconfirmShipper is not null) THEN
select CONCAT(oldValueList,OLD.reconfirmShipper,'~') into oldValueList;
END IF;
IF(NEW.reconfirmShipper is not null) THEN
select CONCAT(newValueList,NEW.reconfirmShipper,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.confirmOriginAgent,'%Y-%m-%d') <> date_format(NEW.confirmOriginAgent,'%Y-%m-%d')) or (date_format(OLD.confirmOriginAgent,'%Y-%m-%d') is null and date_format(NEW.confirmOriginAgent,'%Y-%m-%d') is not null)
      or (date_format(OLD.confirmOriginAgent,'%Y-%m-%d') is not null and date_format(NEW.confirmOriginAgent,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.confirmOriginAgent~') into fieldNameList;
    IF(OLD.confirmOriginAgent is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.confirmOriginAgent is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.confirmOriginAgent is not null) THEN
select CONCAT(oldValueList,OLD.confirmOriginAgent,'~') into oldValueList;
END IF;
IF(NEW.confirmOriginAgent is not null) THEN
select CONCAT(newValueList,NEW.confirmOriginAgent,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.packDayConfirmCall,'%Y-%m-%d') <> date_format(NEW.packDayConfirmCall,'%Y-%m-%d')) or (date_format(OLD.packDayConfirmCall,'%Y-%m-%d') is null and date_format(NEW.packDayConfirmCall,'%Y-%m-%d') is not null)
      or (date_format(OLD.packDayConfirmCall,'%Y-%m-%d') is not null and date_format(NEW.packDayConfirmCall,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packDayConfirmCall~') into fieldNameList;
    IF(OLD.packDayConfirmCall is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packDayConfirmCall is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packDayConfirmCall is not null) THEN
select CONCAT(oldValueList,OLD.packDayConfirmCall,'~') into oldValueList;
END IF;
IF(NEW.packDayConfirmCall is not null) THEN
select CONCAT(newValueList,NEW.packDayConfirmCall,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.loadFollowUpCall,'%Y-%m-%d') <> date_format(NEW.loadFollowUpCall,'%Y-%m-%d')) or (date_format(OLD.loadFollowUpCall,'%Y-%m-%d') is null and date_format(NEW.loadFollowUpCall,'%Y-%m-%d') is not null)
      or (date_format(OLD.loadFollowUpCall,'%Y-%m-%d') is not null and date_format(NEW.loadFollowUpCall,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.loadFollowUpCall~') into fieldNameList;
    IF(OLD.loadFollowUpCall is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.loadFollowUpCall is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.loadFollowUpCall is not null) THEN
select CONCAT(oldValueList,OLD.loadFollowUpCall,'~') into oldValueList;
END IF;
IF(NEW.loadFollowUpCall is not null) THEN
select CONCAT(newValueList,NEW.loadFollowUpCall,'~') into newValueList;
END IF;
   END IF;
IF ((date_format(OLD.deliveryFollowCall,'%Y-%m-%d') <> date_format(NEW.deliveryFollowCall,'%Y-%m-%d')) or (date_format(OLD.deliveryFollowCall,'%Y-%m-%d') is null and date_format(NEW.deliveryFollowCall,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryFollowCall,'%Y-%m-%d') is not null and date_format(NEW.deliveryFollowCall,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryFollowCall~') into fieldNameList;
    IF(OLD.deliveryFollowCall is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryFollowCall is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryFollowCall is not null) THEN
select CONCAT(oldValueList,OLD.deliveryFollowCall,'~') into oldValueList;
END IF;
IF(NEW.deliveryFollowCall is not null) THEN
select CONCAT(newValueList,NEW.deliveryFollowCall,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.deliveryReceiptDate,'%Y-%m-%d') <> date_format(NEW.deliveryReceiptDate,'%Y-%m-%d')) or (date_format(OLD.deliveryReceiptDate,'%Y-%m-%d') is null and date_format(NEW.deliveryReceiptDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryReceiptDate,'%Y-%m-%d') is not null and date_format(NEW.deliveryReceiptDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryReceiptDate~') into fieldNameList;
    IF(OLD.deliveryReceiptDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryReceiptDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryReceiptDate is not null) THEN
select CONCAT(oldValueList,OLD.deliveryReceiptDate,'~') into oldValueList;
END IF;
IF(NEW.deliveryReceiptDate is not null) THEN
select CONCAT(newValueList,NEW.deliveryReceiptDate,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.mailClaim,'%Y-%m-%d') <> date_format(NEW.mailClaim,'%Y-%m-%d')) or (date_format(OLD.mailClaim,'%Y-%m-%d') is null and date_format(NEW.mailClaim,'%Y-%m-%d') is not null)
      or (date_format(OLD.mailClaim,'%Y-%m-%d') is not null and date_format(NEW.mailClaim,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.mailClaim~') into fieldNameList;
    IF(OLD.mailClaim is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.mailClaim is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.mailClaim is not null) THEN
select CONCAT(oldValueList,OLD.mailClaim,'~') into oldValueList;
END IF;
IF(NEW.mailClaim is not null) THEN
select CONCAT(newValueList,NEW.mailClaim,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.returnLocationOrigin <> NEW.returnLocationOrigin ) THEN
      select CONCAT(fieldNameList,'trackingstatus.returnLocationOrigin~') into fieldNameList;
      select CONCAT(oldValueList,OLD.returnLocationOrigin,'~') into oldValueList;
      select CONCAT(newValueList,NEW.returnLocationOrigin,'~') into newValueList;
  END IF;
  IF (OLD.sitAuthorizeOrigin <> NEW.sitAuthorizeOrigin ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitAuthorizeOrigin~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitAuthorizeOrigin,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitAuthorizeOrigin,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.lastFree,'%Y-%m-%d') <> date_format(NEW.lastFree,'%Y-%m-%d')) or (date_format(OLD.lastFree,'%Y-%m-%d') is null and date_format(NEW.lastFree,'%Y-%m-%d') is not null)
      or (date_format(OLD.lastFree,'%Y-%m-%d') is not null and date_format(NEW.lastFree,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.lastFree~') into fieldNameList;
    IF(OLD.lastFree is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.lastFree is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.lastFree is not null) THEN
select CONCAT(oldValueList,OLD.lastFree,'~') into oldValueList;
END IF;
IF(NEW.lastFree is not null) THEN
select CONCAT(newValueList,NEW.lastFree,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.demurrageYN <> NEW.demurrageYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.demurrageYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.demurrageYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.demurrageYN,'~') into newValueList;
  END IF;
  IF (OLD.demurragePayment <> NEW.demurragePayment ) THEN
      select CONCAT(fieldNameList,'trackingstatus.demurragePayment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.demurragePayment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.demurragePayment,'~') into newValueList;
  END IF;
  IF (OLD.sitDestinationInCondition <> NEW.sitDestinationInCondition ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationInCondition~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationInCondition,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationInCondition,'~') into newValueList;
  END IF;
  IF (OLD.sitOriginOnCondition <> NEW.sitOriginOnCondition ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginOnCondition~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitOriginOnCondition,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitOriginOnCondition,'~') into newValueList;
  END IF;
 IF ((OLD.demurrageCostPerDay1 <> NEW.demurrageCostPerDay1) or (OLD.demurrageCostPerDay1 is null and NEW.demurrageCostPerDay1 is not null)
      or (OLD.demurrageCostPerDay1 is not null and NEW.demurrageCostPerDay1 is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.demurrageCostPerDay1~') into fieldNameList;
        IF(OLD.demurrageCostPerDay1 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.demurrageCostPerDay1 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.demurrageCostPerDay1 is not null) then
            select CONCAT(oldValueList,OLD.demurrageCostPerDay1,'~') into oldValueList;
        END IF;
        IF(NEW.demurrageCostPerDay1 is not null) then
            select CONCAT(newValueList,NEW.demurrageCostPerDay1,'~') into newValueList;
        END IF;
    END IF;
  IF (OLD.perdiumYN <> NEW.perdiumYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.perdiumYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.perdiumYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.perdiumYN,'~') into newValueList;
  END IF;
  IF (OLD.perdiumPayment <> NEW.perdiumPayment ) THEN
      select CONCAT(fieldNameList,'trackingstatus.perdiumPayment~') into fieldNameList;
      select CONCAT(oldValueList,OLD.perdiumPayment,'~') into oldValueList;
      select CONCAT(newValueList,NEW.perdiumPayment,'~') into newValueList;
  END IF;
  IF ((OLD.perdiumCostPerDay <> NEW.perdiumCostPerDay) or (OLD.perdiumCostPerDay is null and NEW.perdiumCostPerDay is not null)
      or (OLD.perdiumCostPerDay is not null and NEW.perdiumCostPerDay is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.perdiumCostPerDay~') into fieldNameList;
        IF(OLD.perdiumCostPerDay is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.perdiumCostPerDay is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.perdiumCostPerDay is not null) then
            select CONCAT(oldValueList,OLD.perdiumCostPerDay,'~') into oldValueList;
        END IF;
        IF(NEW.perdiumCostPerDay is not null) then
            select CONCAT(newValueList,NEW.perdiumCostPerDay,'~') into newValueList;
        END IF;
    END IF;
  IF ((OLD.xrayCost <> NEW.xrayCost) or (OLD.xrayCost is null and NEW.xrayCost is not null)
      or (OLD.xrayCost is not null and NEW.xrayCost is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.xrayCost~') into fieldNameList;
        IF(OLD.xrayCost is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.xrayCost is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.xrayCost is not null) then
            select CONCAT(oldValueList,OLD.xrayCost,'~') into oldValueList;
        END IF;
        IF(NEW.xrayCost is not null) then
            select CONCAT(newValueList,NEW.xrayCost,'~') into newValueList;
        END IF;
    END IF;
IF (OLD.inspectorPaymentTrack <> NEW.inspectorPaymentTrack ) THEN
      select CONCAT(fieldNameList,'trackingstatus.inspectorPaymentTrack~') into fieldNameList;
      select CONCAT(oldValueList,OLD.inspectorPaymentTrack,'~') into oldValueList;
      select CONCAT(newValueList,NEW.inspectorPaymentTrack,'~') into newValueList;
  END IF;
IF ((OLD.inspectorCost <> NEW.inspectorCost) or (OLD.inspectorCost is null and NEW.inspectorCost is not null)
      or (OLD.inspectorCost is not null and NEW.inspectorCost is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.inspectorCost~') into fieldNameList;
        IF(OLD.inspectorCost is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.inspectorCost is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.inspectorCost is not null) then
            select CONCAT(oldValueList,OLD.inspectorCost,'~') into oldValueList;
        END IF;
        IF(NEW.inspectorCost is not null) then
            select CONCAT(newValueList,NEW.inspectorCost,'~') into newValueList;
        END IF;
    END IF;
  IF (OLD.inspectorYN <> NEW.inspectorYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.inspectorYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.inspectorYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.inspectorYN,'~') into newValueList;
  END IF;
  IF (OLD.xrayYN <> NEW.xrayYN ) THEN
      select CONCAT(fieldNameList,'trackingstatus.xrayYN~') into fieldNameList;
      select CONCAT(oldValueList,OLD.xrayYN,'~') into oldValueList;
      select CONCAT(newValueList,NEW.xrayYN,'~') into newValueList;
  END IF;
IF ((OLD.usdaCost <> NEW.usdaCost) or (OLD.usdaCost is null and NEW.usdaCost is not null)
      or (OLD.usdaCost is not null and NEW.usdaCost is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.usdaCost~') into fieldNameList;
        IF(OLD.usdaCost is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.usdaCost is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.usdaCost is not null) then
            select CONCAT(oldValueList,OLD.usdaCost,'~') into oldValueList;
        END IF;
        IF(NEW.usdaCost is not null) then
            select CONCAT(newValueList,NEW.usdaCost,'~') into newValueList;
        END IF;
    END IF;
  IF ((OLD.demurrageCostPerDay2 <> NEW.demurrageCostPerDay2) or (OLD.demurrageCostPerDay2 is null and NEW.demurrageCostPerDay2 is not null)
      or (OLD.demurrageCostPerDay2 is not null and NEW.demurrageCostPerDay2 is null)) THEN
        select CONCAT(fieldNameList,'trackingstatus.demurrageCostPerDay2~') into fieldNameList;
        IF(OLD.demurrageCostPerDay2 is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.demurrageCostPerDay2 is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;
        IF(OLD.demurrageCostPerDay2 is not null) then
            select CONCAT(oldValueList,OLD.demurrageCostPerDay2,'~') into oldValueList;
        END IF;
        IF(NEW.demurrageCostPerDay2 is not null) then
            select CONCAT(newValueList,NEW.demurrageCostPerDay2,'~') into newValueList;
        END IF;
    END IF;
  IF ((date_format(OLD.survey,'%Y-%m-%d') <> date_format(NEW.survey,'%Y-%m-%d')) or (date_format(OLD.survey,'%Y-%m-%d') is null and date_format(NEW.survey,'%Y-%m-%d') is not null)
      or (date_format(OLD.survey,'%Y-%m-%d') is not null and date_format(NEW.survey,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.survey~') into fieldNameList;
    IF(OLD.survey is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.survey is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.survey is not null) THEN
select CONCAT(oldValueList,OLD.survey,'~') into oldValueList;
END IF;
IF(NEW.survey is not null) THEN
select CONCAT(newValueList,NEW.survey,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.packA,'%Y-%m-%d') <> date_format(NEW.packA,'%Y-%m-%d')) or (date_format(OLD.packA,'%Y-%m-%d') is null and date_format(NEW.packA,'%Y-%m-%d') is not null)
      or (date_format(OLD.packA,'%Y-%m-%d') is not null and date_format(NEW.packA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packA~') into fieldNameList;
    IF(OLD.packA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packA is not null) THEN
select CONCAT(oldValueList,OLD.packA,'~') into oldValueList;
END IF;
IF(NEW.packA is not null) THEN
select CONCAT(newValueList,NEW.packA,'~') into newValueList;
END IF;
update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
  IF ((date_format(OLD.loadA,'%Y-%m-%d') <> date_format(NEW.loadA,'%Y-%m-%d')) or (date_format(OLD.loadA,'%Y-%m-%d') is null and date_format(NEW.loadA,'%Y-%m-%d') is not null)
      or (OLD.loadA is not null and NEW.loadA is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.loadA~') into fieldNameList;

    IF(OLD.loadA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.loadA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.loadA is not null) THEN
select CONCAT(oldValueList,OLD.loadA,'~') into oldValueList;
END IF;
IF(NEW.loadA is not null) THEN
select CONCAT(newValueList,NEW.loadA,'~') into newValueList;
END IF;
  update sodashboard set actualLoading=NEW.loadA where serviceorderid=NEW.id;
  update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
  IF ((date_format(OLD.packRoomA,'%Y-%m-%d') <> date_format(NEW.packRoomA,'%Y-%m-%d')) or (date_format(OLD.packRoomA,'%Y-%m-%d') is null and date_format(NEW.packRoomA,'%Y-%m-%d') is not null)
      or (date_format(OLD.packRoomA,'%Y-%m-%d') is not null and date_format(NEW.packRoomA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packRoomA~') into fieldNameList;
    IF(OLD.packRoomA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packRoomA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.packRoomA is not null) THEN
select CONCAT(oldValueList,OLD.packRoomA,'~') into oldValueList;
END IF;
IF(NEW.packRoomA is not null) THEN
select CONCAT(newValueList,NEW.packRoomA,'~') into newValueList;
END IF;
   END IF;
IF ((date_format(OLD.sitOriginA,'%Y-%m-%d') <> date_format(NEW.sitOriginA,'%Y-%m-%d')) or (date_format(OLD.sitOriginA,'%Y-%m-%d') is null and date_format(NEW.sitOriginA,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitOriginA,'%Y-%m-%d') is not null and date_format(NEW.sitOriginA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginA~') into fieldNameList;
    IF(OLD.sitOriginA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitOriginA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitOriginA is not null) THEN
select CONCAT(oldValueList,OLD.sitOriginA,'~') into oldValueList;
END IF;
IF(NEW.sitOriginA is not null) THEN
select CONCAT(newValueList,NEW.sitOriginA,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.returnContainerDateA,'%Y-%m-%d') <> date_format(NEW.returnContainerDateA,'%Y-%m-%d')) or (date_format(OLD.returnContainerDateA,'%Y-%m-%d') is null and date_format(NEW.returnContainerDateA,'%Y-%m-%d') is not null)
      or (date_format(OLD.returnContainerDateA,'%Y-%m-%d') is not null and date_format(NEW.returnContainerDateA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.returnContainerDateA~') into fieldNameList;
    IF(OLD.returnContainerDateA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.returnContainerDateA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.returnContainerDateA is not null) THEN
select CONCAT(oldValueList,OLD.returnContainerDateA,'~') into oldValueList;
END IF;
IF(NEW.returnContainerDateA is not null) THEN
select CONCAT(newValueList,NEW.returnContainerDateA,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.returnContainerDateADestination,'%Y-%m-%d') <> date_format(NEW.returnContainerDateADestination,'%Y-%m-%d')) or (date_format(OLD.returnContainerDateADestination,'%Y-%m-%d') is null and date_format(NEW.returnContainerDateADestination,'%Y-%m-%d') is not null)
      or (date_format(OLD.returnContainerDateADestination,'%Y-%m-%d') is not null and date_format(NEW.returnContainerDateADestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.returnContainerDateADestination~') into fieldNameList;
    IF(OLD.returnContainerDateADestination is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.returnContainerDateADestination is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.returnContainerDateADestination is not null) THEN
select CONCAT(oldValueList,OLD.returnContainerDateADestination,'~') into oldValueList;
END IF;
IF(NEW.returnContainerDateADestination is not null) THEN
select CONCAT(newValueList,NEW.returnContainerDateADestination,'~') into newValueList;
END IF;
   END IF;
 IF ((date_format(OLD.sitDestinationA,'%Y-%m-%d') <> date_format(NEW.sitDestinationA,'%Y-%m-%d')) or (date_format(OLD.sitDestinationA,'%Y-%m-%d') is null and date_format(NEW.sitDestinationA,'%Y-%m-%d') is not null)
      or (date_format(OLD.sitDestinationA,'%Y-%m-%d') is not null and date_format(NEW.sitDestinationA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationA~') into fieldNameList;
    IF(OLD.sitDestinationA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sitDestinationA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sitDestinationA is not null) THEN
select CONCAT(oldValueList,OLD.sitDestinationA,'~') into oldValueList;
END IF;
IF(NEW.sitDestinationA is not null) THEN
select CONCAT(newValueList,NEW.sitDestinationA,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.deliveryA,'%Y-%m-%d') <> date_format(NEW.deliveryA,'%Y-%m-%d')) or (date_format(OLD.deliveryA,'%Y-%m-%d') is null and date_format(NEW.deliveryA,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryA,'%Y-%m-%d') is not null and date_format(NEW.deliveryA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryA~') into fieldNameList;
    IF(OLD.deliveryA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryA is not null) THEN
select CONCAT(oldValueList,OLD.deliveryA,'~') into oldValueList;
END IF;
IF(NEW.deliveryA is not null) THEN
select CONCAT(newValueList,NEW.deliveryA,'~') into newValueList;
END IF;
  update sodashboard set actualDelivery=NEW.deliveryA where serviceorderid=NEW.id;
  update serviceorder set updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;
   END IF;
    IF ((date_format(OLD.sentcomp,'%Y-%m-%d') <> date_format(NEW.sentcomp,'%Y-%m-%d')) or (date_format(OLD.sentcomp,'%Y-%m-%d') is null and date_format(NEW.sentcomp,'%Y-%m-%d') is not null)
      or (date_format(OLD.sentcomp,'%Y-%m-%d') is not null and date_format(NEW.sentcomp,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sentcomp~') into fieldNameList;
    IF(OLD.sentcomp is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.sentcomp is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.sentcomp is not null) THEN
select CONCAT(oldValueList,OLD.sentcomp,'~') into oldValueList;
END IF;
IF(NEW.sentcomp is not null) THEN
select CONCAT(newValueList,NEW.sentcomp,'~') into newValueList;
END IF;
   END IF;
   IF (OLD.booker <> NEW.booker ) THEN
      select CONCAT(fieldNameList,'trackingstatus.booker~') into fieldNameList;
      select CONCAT(oldValueList,OLD.booker,'~') into oldValueList;
      select CONCAT(newValueList,NEW.booker,'~') into newValueList;
  END IF;
   IF (OLD.originAgent <> NEW.originAgent ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgent,'~') into newValueList;
  END IF;

   IF (OLD.originAgentCode <> NEW.originAgentCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentCode,'~') into newValueList;
  END IF;
   IF (OLD.destinationAgentCode <> NEW.destinationAgentCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentCode,'~') into newValueList;
  END IF;
   IF (OLD.sitAuthorizeDestination <> NEW.sitAuthorizeDestination ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitAuthorizeDestination~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitAuthorizeDestination,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitAuthorizeDestination,'~') into newValueList;
  END IF;
     IF ((date_format(OLD.statusDate,'%Y-%m-%d') <> date_format(NEW.statusDate,'%Y-%m-%d')) or (date_format(OLD.statusDate,'%Y-%m-%d') is null and date_format(NEW.statusDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.statusDate,'%Y-%m-%d') is not null and date_format(NEW.statusDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.statusDate~') into fieldNameList;
    IF(OLD.statusDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.statusDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.statusDate is not null) THEN
select CONCAT(oldValueList,OLD.statusDate,'~') into oldValueList;
END IF;
IF(NEW.statusDate is not null) THEN
select CONCAT(newValueList,NEW.statusDate,'~') into newValueList;
END IF;
   END IF;
   
   IF ((date_format(OLD.packingWeightOn,'%Y-%m-%d') <> date_format(NEW.packingWeightOn,'%Y-%m-%d')) or (date_format(OLD.packingWeightOn,'%Y-%m-%d') is null and date_format(NEW.packingWeightOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.packingWeightOn,'%Y-%m-%d') is not null and date_format(NEW.packingWeightOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.packingWeightOn~') into fieldNameList;
    IF(OLD.packingWeightOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.packingWeightOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.statusDate is not null) THEN
select CONCAT(oldValueList,OLD.packingWeightOn,'~') into oldValueList;
END IF;
IF(NEW.statusDate is not null) THEN
select CONCAT(newValueList,NEW.packingWeightOn,'~') into newValueList;
END IF;
   END IF;
   
   IF ((date_format(OLD.confirmationOn,'%Y-%m-%d') <> date_format(NEW.confirmationOn,'%Y-%m-%d')) or (date_format(OLD.confirmationOn,'%Y-%m-%d') is null and date_format(NEW.confirmationOn,'%Y-%m-%d') is not null)
      or (date_format(OLD.confirmationOn,'%Y-%m-%d') is not null and date_format(NEW.confirmationOn,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.confirmationOn~') into fieldNameList;
    IF(OLD.confirmationOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.confirmationOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.statusDate is not null) THEN
select CONCAT(oldValueList,OLD.confirmationOn,'~') into oldValueList;
END IF;
IF(NEW.statusDate is not null) THEN
select CONCAT(newValueList,NEW.confirmationOn,'~') into newValueList;
END IF;
   END IF;
   
   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'trackingstatus.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
   END IF;
   IF ((date_format(OLD.initialContact,'%Y-%m-%d') <> date_format(NEW.initialContact,'%Y-%m-%d')) or (date_format(OLD.initialContact,'%Y-%m-%d') is null and date_format(NEW.initialContact,'%Y-%m-%d') is not null)
      or (date_format(OLD.initialContact,'%Y-%m-%d') is not null and date_format(NEW.initialContact,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.initialContact~') into fieldNameList;
    IF(OLD.initialContact is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.initialContact is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.initialContact is not null) THEN
select CONCAT(oldValueList,OLD.initialContact,'~') into oldValueList;
END IF;
IF(NEW.initialContact is not null) THEN
select CONCAT(newValueList,NEW.initialContact,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.cutOffDate,'%Y-%m-%d') <> date_format(NEW.cutOffDate,'%Y-%m-%d')) or (date_format(OLD.cutOffDate,'%Y-%m-%d') is null and date_format(NEW.cutOffDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.cutOffDate,'%Y-%m-%d') is not null and date_format(NEW.cutOffDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.cutOffDate~') into fieldNameList;
    IF(OLD.cutOffDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.cutOffDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.cutOffDate is not null) THEN
select CONCAT(oldValueList,OLD.cutOffDate,'~') into oldValueList;
END IF;
IF(NEW.cutOffDate is not null) THEN
select CONCAT(newValueList,NEW.cutOffDate,'~') into newValueList;
END IF;
   END IF;
   IF (OLD.cutOffDateTA <> NEW.cutOffDateTA ) THEN
      select CONCAT(fieldNameList,'trackingstatus.cutOffDateTA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.cutOffDateTA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.cutOffDateTA,'~') into newValueList;
   END IF;
   IF (OLD.createdBy <> NEW.createdBy ) THEN
      select CONCAT(fieldNameList,'trackingstatus.createdBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.createdBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.createdBy,'~') into newValueList;
   END IF;
  IF ((OLD.createdOn <> NEW.createdOn) or (OLD.createdOn is null and NEW.createdOn is not null)
      or (OLD.createdOn is not null and NEW.createdOn is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.createdOn~') into fieldNameList;
    IF(OLD.createdOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.createdOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
    IF(OLD.createdOn is not null) THEN
select CONCAT(oldValueList,OLD.createdOn,'~') into oldValueList;
END IF;
IF(NEW.createdOn is not null) THEN
select CONCAT(newValueList,NEW.createdOn,'~') into newValueList;
END IF;
   END IF;
   IF (OLD.updatedBy <> NEW.updatedBy ) THEN
      select CONCAT(fieldNameList,'trackingstatus.updatedBy~') into fieldNameList;
      select CONCAT(oldValueList,OLD.updatedBy,'~') into oldValueList;
      select CONCAT(newValueList,NEW.updatedBy,'~') into newValueList;
   END IF;
   IF ((OLD.updatedOn <> NEW.updatedOn) or (OLD.updatedOn is null and NEW.updatedOn is not null)
      or (OLD.updatedOn is not null and NEW.updatedOn is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.updatedOn~') into fieldNameList;
    IF(OLD.updatedOn is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.updatedOn is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
    IF(OLD.updatedOn is not null) THEN
select CONCAT(oldValueList,OLD.updatedOn,'~') into oldValueList;
END IF;
IF(NEW.updatedOn is not null) THEN
select CONCAT(newValueList,NEW.updatedOn,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.destinationAgentVanlineCode <> NEW.destinationAgentVanlineCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentVanlineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentVanlineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentVanlineCode,'~') into newValueList;
  END IF;
  IF (OLD.originAgentVanlineCode <> NEW.originAgentVanlineCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentVanlineCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentVanlineCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentVanlineCode,'~') into newValueList;
  END IF;
IF (OLD.forwarderContact <> NEW.forwarderContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.forwarderContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forwarderContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forwarderContact,'~') into newValueList;
  END IF;
    IF (OLD.forwarderCode <> NEW.forwarderCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.forwarderCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forwarderCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forwarderCode,'~') into newValueList;
  END IF;
    IF (OLD.forwarder <> NEW.forwarder) THEN
      select CONCAT(fieldNameList,'trackingstatus.forwarder~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forwarder,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forwarder,'~') into newValueList;
  END IF;
    IF (OLD.sitDestinationDaysAllowed <> NEW.sitDestinationDaysAllowed ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitDestinationDaysAllowed~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitDestinationDaysAllowed,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitDestinationDaysAllowed,'~') into newValueList;
  END IF;
    IF (OLD.sitOriginDaysAllowed <> NEW.sitOriginDaysAllowed ) THEN
      select CONCAT(fieldNameList,'trackingstatus.sitOriginDaysAllowed~') into fieldNameList;
      select CONCAT(oldValueList,OLD.sitOriginDaysAllowed,'~') into oldValueList;
      select CONCAT(newValueList,NEW.sitOriginDaysAllowed,'~') into newValueList;
  END IF;
    IF ((date_format(OLD.ISFSubmitted,'%Y-%m-%d') <> date_format(NEW.ISFSubmitted,'%Y-%m-%d')) or (date_format(OLD.ISFSubmitted,'%Y-%m-%d') is null and date_format(NEW.ISFSubmitted,'%Y-%m-%d') is not null)
      or (date_format(OLD.ISFSubmitted,'%Y-%m-%d') is not null and date_format(NEW.ISFSubmitted,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.ISFSubmitted~') into fieldNameList;
    IF(OLD.ISFSubmitted is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.ISFSubmitted is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.ISFSubmitted is not null) THEN
select CONCAT(oldValueList,OLD.ISFSubmitted,'~') into oldValueList;
END IF;
IF(NEW.ISFSubmitted is not null) THEN
select CONCAT(newValueList,NEW.ISFSubmitted,'~') into newValueList;
END IF;
   END IF;
          IF ((date_format(OLD.requestOriginSit,'%Y-%m-%d') <> date_format(NEW.requestOriginSit,'%Y-%m-%d')) or (date_format(OLD.requestOriginSit,'%Y-%m-%d') is null and date_format(NEW.requestOriginSit,'%Y-%m-%d') is not null)
      or (date_format(OLD.requestOriginSit,'%Y-%m-%d') is not null and date_format(NEW.requestOriginSit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.requestOriginSit~') into fieldNameList;
    IF(OLD.requestOriginSit is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.requestOriginSit is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.requestOriginSit is not null) THEN
select CONCAT(oldValueList,OLD.requestOriginSit,'~') into oldValueList;
END IF;
IF(NEW.requestOriginSit is not null) THEN
select CONCAT(newValueList,NEW.requestOriginSit,'~') into newValueList;
END IF;
   END IF;
      IF ((date_format(OLD.SITNotify,'%Y-%m-%d') <> date_format(NEW.SITNotify,'%Y-%m-%d')) or (date_format(OLD.SITNotify,'%Y-%m-%d') is null and date_format(NEW.SITNotify,'%Y-%m-%d') is not null)
      or (date_format(OLD.SITNotify,'%Y-%m-%d') is not null and date_format(NEW.SITNotify,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.SITNotify~') into fieldNameList;
    IF(OLD.SITNotify is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.SITNotify is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.SITNotify is not null) THEN
select CONCAT(oldValueList,OLD.SITNotify,'~') into oldValueList;
END IF;
IF(NEW.SITNotify is not null) THEN
select CONCAT(newValueList,NEW.SITNotify,'~') into newValueList;
END IF;
   END IF;
    IF ((date_format(OLD.requiredDeliveryDate,'%Y-%m-%d') <> date_format(NEW.requiredDeliveryDate,'%Y-%m-%d')) or (date_format(OLD.requiredDeliveryDate,'%Y-%m-%d') is null and date_format(NEW.requiredDeliveryDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.requiredDeliveryDate,'%Y-%m-%d') is not null and date_format(NEW.requiredDeliveryDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.requiredDeliveryDate~') into fieldNameList;
    IF(OLD.requiredDeliveryDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.requiredDeliveryDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.requiredDeliveryDate is not null) THEN
select CONCAT(oldValueList,OLD.requiredDeliveryDate,'~') into oldValueList;
END IF;
IF(NEW.requiredDeliveryDate is not null) THEN
select CONCAT(newValueList,NEW.requiredDeliveryDate,'~') into newValueList;
END IF;
   END IF;
    IF ((date_format(OLD.clearCustomOrigin,'%Y-%m-%d') <> date_format(NEW.clearCustomOrigin,'%Y-%m-%d')) or (OLD.clearCustomOrigin is null and NEW.clearCustomOrigin is not null)
      or (OLD.clearCustomOrigin is not null and NEW.clearCustomOrigin is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.clearCustomOrigin~') into fieldNameList;
    IF(OLD.clearCustomOrigin is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.clearCustomOrigin is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.clearCustomOrigin is not null) THEN
select CONCAT(oldValueList,OLD.clearCustomOrigin,'~') into oldValueList;
END IF;
IF(NEW.clearCustomOrigin is not null) THEN
select CONCAT(newValueList,NEW.clearCustomOrigin,'~') into newValueList;
END IF;
   END IF;
    IF (OLD.bookingAgentEmail <> NEW.bookingAgentEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.bookingAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentEmail,'~') into newValueList;
  END IF;
    IF (OLD.bookingAgentContact <> NEW.bookingAgentContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.bookingAgentContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentContact,'~') into newValueList;
  END IF;
    IF (OLD.brokerEmail <> NEW.brokerEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.brokerEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.brokerEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.brokerEmail,'~') into newValueList;
  END IF;
    IF (OLD.brokerContact <> NEW.brokerContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.brokerContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.brokerContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.brokerContact,'~') into newValueList;
  END IF;
    IF (OLD.subDestinationAgentEmail <> NEW.subDestinationAgentEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subDestinationAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subDestinationAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subDestinationAgentEmail,'~') into newValueList;
  END IF;
    IF (OLD.subDestinationAgentAgentContact <> NEW.subDestinationAgentAgentContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subDestinationAgentAgentContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subDestinationAgentAgentContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subDestinationAgentAgentContact,'~') into newValueList;
  END IF;
    IF (OLD.subOriginAgentEmail <> NEW.subOriginAgentEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subOriginAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subOriginAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subOriginAgentEmail,'~') into newValueList;
  END IF;
    IF (OLD.subOriginAgentContact <> NEW.subOriginAgentContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subOriginAgentContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subOriginAgentContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subOriginAgentContact,'~') into newValueList;
  END IF;
    IF (OLD.destinationAgentEmail <> NEW.destinationAgentEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentEmail,'~') into newValueList;
  END IF;
    IF (OLD.destinationAgentContact <> NEW.destinationAgentContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentContact,'~') into newValueList;
  END IF;
    IF (OLD.originAgentEmail <> NEW.originAgentEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentEmail,'~') into newValueList;
  END IF;
    IF (OLD.originAgentContact <> NEW.originAgentContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentContact,'~') into newValueList;
  END IF;
    IF (OLD.surveyTimeTo <> NEW.surveyTimeTo ) THEN
      select CONCAT(fieldNameList,'trackingstatus.surveyTimeTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyTimeTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyTimeTo,'~') into newValueList;
  END IF;
    IF (OLD.surveyTimeFrom <> NEW.surveyTimeFrom ) THEN
      select CONCAT(fieldNameList,'trackingstatus.surveyTimeFrom~') into fieldNameList;
      select CONCAT(oldValueList,OLD.surveyTimeFrom,'~') into oldValueList;
      select CONCAT(newValueList,NEW.surveyTimeFrom,'~') into newValueList;
  END IF;
     IF ((date_format(OLD.surveyDate,'%Y-%m-%d') <> date_format(NEW.surveyDate,'%Y-%m-%d')) or (date_format(OLD.surveyDate,'%Y-%m-%d') is null and date_format(NEW.surveyDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.surveyDate,'%Y-%m-%d') is not null and date_format(NEW.surveyDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.surveyDate~') into fieldNameList;
    IF(OLD.surveyDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.surveyDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.surveyDate is not null) THEN
select CONCAT(oldValueList,OLD.surveyDate,'~') into oldValueList;
END IF;
IF(NEW.surveyDate is not null) THEN
select CONCAT(newValueList,NEW.surveyDate,'~') into newValueList;
END IF;
   END IF;
    IF (OLD.destinationSubAgentCode <> NEW.destinationSubAgentCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationSubAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationSubAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationSubAgentCode,'~') into newValueList;
  END IF;
    IF (OLD.originSubAgentCode <> NEW.originSubAgentCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originSubAgentCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originSubAgentCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originSubAgentCode,'~') into newValueList;
  END IF;
    IF (OLD.actgCodeForDis <> NEW.actgCodeForDis ) THEN
      select CONCAT(fieldNameList,'trackingstatus.actgCodeForDis~') into fieldNameList;
      select CONCAT(oldValueList,OLD.actgCodeForDis,'~') into oldValueList;
      select CONCAT(newValueList,NEW.actgCodeForDis,'~') into newValueList;
  END IF;
    IF (OLD.actgCode <> NEW.actgCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.actgCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.actgCode,'~') into newValueList;
  END IF;
    IF (OLD.brokerName <> NEW.brokerName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.brokerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.brokerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.brokerName,'~') into newValueList;
  END IF;
    IF (OLD.brokerCode <> NEW.brokerCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.brokerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.brokerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.brokerCode,'~') into newValueList;
  END IF;
    IF (OLD.forwarderEmail <> NEW.forwarderEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.forwarderEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.forwarderEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.forwarderEmail,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.documentSentClient,'%Y-%m-%d') <> date_format(NEW.documentSentClient,'%Y-%m-%d')) or (date_format(OLD.documentSentClient,'%Y-%m-%d') is null and date_format(NEW.documentSentClient,'%Y-%m-%d') is not null)
      or (date_format(OLD.documentSentClient,'%Y-%m-%d') is not null and date_format(NEW.documentSentClient,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.documentSentClient~') into fieldNameList;
    IF(OLD.documentSentClient is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.documentSentClient is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.documentSentClient is not null) THEN
select CONCAT(oldValueList,OLD.documentSentClient,'~') into oldValueList;
END IF;
IF(NEW.documentSentClient is not null) THEN
select CONCAT(newValueList,NEW.documentSentClient,'~') into newValueList;
END IF;
   END IF;
    IF ((date_format(OLD.documentReceivedFromOA,'%Y-%m-%d') <> date_format(NEW.documentReceivedFromOA,'%Y-%m-%d')) or (date_format(OLD.documentReceivedFromOA,'%Y-%m-%d') is null and date_format(NEW.documentReceivedFromOA,'%Y-%m-%d') is not null)
      or (date_format(OLD.documentReceivedFromOA,'%Y-%m-%d') is not null and date_format(NEW.documentReceivedFromOA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.documentReceivedFromOA~') into fieldNameList;
    IF(OLD.documentReceivedFromOA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.documentReceivedFromOA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.documentReceivedFromOA is not null) THEN
select CONCAT(oldValueList,OLD.documentReceivedFromOA,'~') into oldValueList;
END IF;
IF(NEW.documentReceivedFromOA is not null) THEN
select CONCAT(newValueList,NEW.documentReceivedFromOA,'~') into newValueList;
END IF;
   END IF;
      IF ((date_format(OLD.documentCHA,'%Y-%m-%d') <> date_format(NEW.documentCHA,'%Y-%m-%d')) or (date_format(OLD.documentCHA,'%Y-%m-%d') is null and date_format(NEW.documentCHA,'%Y-%m-%d') is not null)
      or (date_format(OLD.documentCHA,'%Y-%m-%d') is not null and date_format(NEW.documentCHA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.documentCHA~') into fieldNameList;
    IF(OLD.documentCHA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.documentCHA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.documentCHA is not null) THEN
select CONCAT(oldValueList,OLD.documentCHA,'~') into oldValueList;
END IF;
IF(NEW.documentCHA is not null) THEN
select CONCAT(newValueList,NEW.documentCHA,'~') into newValueList;
END IF;
   END IF;
  IF ((date_format(OLD.deliveryReceiptSentOA,'%Y-%m-%d') <> date_format(NEW.deliveryReceiptSentOA,'%Y-%m-%d')) or (date_format(OLD.deliveryReceiptSentOA,'%Y-%m-%d') is null and date_format(NEW.deliveryReceiptSentOA,'%Y-%m-%d') is not null)
      or (date_format(OLD.deliveryReceiptSentOA,'%Y-%m-%d') is not null and date_format(NEW.deliveryReceiptSentOA,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.deliveryReceiptSentOA~') into fieldNameList;
    IF(OLD.deliveryReceiptSentOA is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.deliveryReceiptSentOA is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.deliveryReceiptSentOA is not null) THEN
select CONCAT(oldValueList,OLD.deliveryReceiptSentOA,'~') into oldValueList;
END IF;
IF(NEW.deliveryReceiptSentOA is not null) THEN
select CONCAT(newValueList,NEW.deliveryReceiptSentOA,'~') into newValueList;
END IF;
   END IF;
    IF ((date_format(OLD.documentReceivedClient,'%Y-%m-%d') <> date_format(NEW.documentReceivedClient,'%Y-%m-%d')) or (date_format(OLD.documentReceivedClient,'%Y-%m-%d') is null and date_format(NEW.documentReceivedClient,'%Y-%m-%d') is not null)
      or (date_format(OLD.documentReceivedClient,'%Y-%m-%d') is not null and date_format(NEW.documentReceivedClient,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.documentReceivedClient~') into fieldNameList;
    IF(OLD.documentReceivedClient is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.documentReceivedClient is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.documentReceivedClient is not null) THEN
select CONCAT(oldValueList,OLD.documentReceivedClient,'~') into oldValueList;
END IF;
IF(NEW.documentReceivedClient is not null) THEN
select CONCAT(newValueList,NEW.documentReceivedClient,'~') into newValueList;
END IF;
   END IF;
     IF (OLD.booker <> NEW.booker ) THEN
      select CONCAT(fieldNameList,'trackingstatus.booker~') into fieldNameList;
      select CONCAT(oldValueList,OLD.booker,'~') into oldValueList;
      select CONCAT(newValueList,NEW.booker,'~') into newValueList;
  END IF;
     IF (OLD.destinationAgent <> NEW.destinationAgent ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgent,'~') into newValueList;
  END IF;
     IF (OLD.originAgent <> NEW.originAgent ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgent,'~') into newValueList;
  END IF;
     IF (OLD.initialContact <> NEW.initialContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.initialContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.initialContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.initialContact,'~') into newValueList;
  END IF;
     IF ((date_format(OLD.lastFreeDestination,'%Y-%m-%d') <> date_format(NEW.lastFreeDestination,'%Y-%m-%d')) or (date_format(OLD.lastFreeDestination,'%Y-%m-%d') is null and date_format(NEW.lastFreeDestination,'%Y-%m-%d') is not null)
      or (date_format(OLD.lastFreeDestination,'%Y-%m-%d') is not null and date_format(NEW.lastFreeDestination,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.lastFreeDestination~') into fieldNameList;
    IF(OLD.lastFreeDestination is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.lastFreeDestination is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.lastFreeDestination is not null) THEN
select CONCAT(oldValueList,OLD.lastFreeDestination,'~') into oldValueList;
END IF;
IF(NEW.lastFreeDestination is not null) THEN
select CONCAT(newValueList,NEW.lastFreeDestination,'~') into newValueList;
END IF;
   END IF;
    IF ((date_format(OLD.missedRDDNotification,'%Y-%m-%d') <> date_format(NEW.missedRDDNotification,'%Y-%m-%d')) or (date_format(OLD.missedRDDNotification,'%Y-%m-%d') is null and date_format(NEW.missedRDDNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.missedRDDNotification,'%Y-%m-%d') is not null and date_format(NEW.missedRDDNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.missedRDDNotification~') into fieldNameList;
    IF(OLD.missedRDDNotification is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.missedRDDNotification is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.missedRDDNotification is not null) THEN
select CONCAT(oldValueList,OLD.missedRDDNotification,'~') into oldValueList;
END IF;
IF(NEW.missedRDDNotification is not null) THEN
select CONCAT(newValueList,NEW.missedRDDNotification,'~') into newValueList;
END IF;
   END IF;
  IF (OLD.reason <> NEW.reason ) THEN
      select CONCAT(fieldNameList,'trackingstatus.reason~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reason,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reason,'~') into newValueList;
  END IF;
  IF (OLD.daShipmentNum <> NEW.daShipmentNum ) THEN
      select CONCAT(fieldNameList,'trackingstatus.daShipmentNum~') into fieldNameList;
      select CONCAT(oldValueList,OLD.daShipmentNum,'~') into oldValueList;
      select CONCAT(newValueList,NEW.daShipmentNum,'~') into newValueList;
  END IF;
    IF (OLD.requiredDeliveryDays <> NEW.requiredDeliveryDays ) THEN
      select CONCAT(fieldNameList,'trackingstatus.requiredDeliveryDays~') into fieldNameList;
      select CONCAT(oldValueList,OLD.requiredDeliveryDays,'~') into oldValueList;
      select CONCAT(newValueList,NEW.requiredDeliveryDays,'~') into newValueList;
  END IF;
   IF (OLD.gsaiffFundingFlag <> NEW.gsaiffFundingFlag ) THEN
       select CONCAT(fieldNameList,'trackingstatus.gsaiffFundingFlag~') into fieldNameList;
       IF(OLD.gsaiffFundingFlag = false) THEN
    select CONCAT(oldValueList,'FALSE','~') into oldValueList;
    END IF;
    IF(OLD.gsaiffFundingFlag = true) THEN
    select CONCAT(oldValueList,'TRUE','~') into oldValueList;
    END IF;
    IF(NEW.gsaiffFundingFlag = false) THEN
    select CONCAT(newValueList,'FALSE','~') into newValueList;
    END IF;
    IF(NEW.gsaiffFundingFlag = true) THEN
    select CONCAT(newValueList,'TRUE','~') into newValueList;
    END IF;
   END IF;
   if(OLD.networkAgentExSO='' and NEW.networkAgentExSO<>'') THEN
 select sourceSoNum,soNumber into ssn,networksonumber from networkcontrol where soNumber=NEW.shipnumber and sourceSoNum like '%UGWW%' limit 1;
   if (ssn<>'' and ssn<>networksonumber) then
   insert into networkcontrol (sourceCorpId, soNumber, sourceSoNum, sourceInfo, sourceStatus, corpId, status, action, actionStatus, updatedBy, updatedOn, createdOn, createdBy, childAgentCode, childAgentType, companyDiv)
values
('UGWW', NEW.networkAgentExSO, ssn, '', 'NEW', SUBSTRING(NEW.networkAgentExSO,1,4), '', 'Create Link', 'Done', NEW.updatedBy, now(), now(), NEW.createdBy, NEW.networkPartnerCode, 'NWA', 'UTS');
   end if;
   END IF;
   if(OLD.destinationAgentExSO='' and NEW.destinationAgentExSO<>'') THEN
   select sourceSoNum,soNumber into ssn,networksonumber from networkcontrol where soNumber=NEW.shipnumber and sourceSoNum like '%UGWW%' limit 1;
   if (ssn<>'' and ssn<>networksonumber) then
   set companyDivisionCode='';
  select companycode into companyDivisionCode from companydivision where bookingAgentCode=NEW.destinationAgentCode limit 1;
   insert into networkcontrol (sourceCorpId, soNumber, sourceSoNum, sourceInfo, sourceStatus, corpId, status, action, actionStatus, updatedBy, updatedOn, createdOn, createdBy, childAgentCode, childAgentType, companyDiv)
values
('UGWW', NEW.destinationAgentExSO, ssn, '', 'NEW', SUBSTRING(NEW.destinationAgentExSO,1,4), '', 'Create Link', 'Done', NEW.updatedBy, now(), now(), NEW.createdBy, NEW.destinationAgentCode, 'DA', companyDivisionCode);
   end if;
   END IF;
   if(OLD.originAgentExSO='' and NEW.originAgentExSO<>'') THEN
   select sourceSoNum,soNumber into ssn,networksonumber from networkcontrol where soNumber=NEW.shipnumber and sourceSoNum like '%UGWW%' limit 1;
   if (ssn<>'' and ssn<>networksonumber) then
   set companyDivisionCode='';
   select companycode into companyDivisionCode from companydivision where bookingAgentCode=NEW.originAgentCode limit 1;
   insert into networkcontrol (sourceCorpId, soNumber, sourceSoNum, sourceInfo, sourceStatus, corpId, status, action, actionStatus, updatedBy, updatedOn, createdOn, createdBy, childAgentCode, childAgentType, companyDiv)
values
('UGWW', NEW.originAgentExSO, ssn, '', 'NEW', SUBSTRING(NEW.originAgentExSO,1,4), '', 'Create Link', 'Done', NEW.updatedBy, now(), now(), NEW.createdBy, NEW.originAgentCode, 'OA', companyDivisionCode);
   end if;
   END IF;
   IF ((date_format(OLD.receivedOriginSit,'%Y-%m-%d') <> date_format(NEW.receivedOriginSit,'%Y-%m-%d')) 
   or (date_format(OLD.receivedOriginSit,'%Y-%m-%d') is null and date_format(NEW.receivedOriginSit,'%Y-%m-%d') is not null)
      or (date_format(OLD.receivedOriginSit,'%Y-%m-%d') is not null and date_format(NEW.receivedOriginSit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.receivedOriginSit~') into fieldNameList;
    IF(OLD.receivedOriginSit is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.receivedOriginSit is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.receivedOriginSit is not null) THEN
    select CONCAT(oldValueList,OLD.receivedOriginSit,'~') into oldValueList;
  END IF;
  IF(NEW.receivedOriginSit is not null) THEN
    select CONCAT(newValueList,NEW.receivedOriginSit,'~') into newValueList;
  END IF;
   END IF; 
   IF ((date_format(OLD.requestDestinationSit,'%Y-%m-%d') <> date_format(NEW.requestDestinationSit,'%Y-%m-%d')) or (date_format(OLD.requestDestinationSit,'%Y-%m-%d') is null and date_format(NEW.requestDestinationSit,'%Y-%m-%d') is not null)
      or (date_format(OLD.requestDestinationSit,'%Y-%m-%d') is not null and date_format(NEW.requestDestinationSit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.requestDestinationSit~') into fieldNameList;
    IF(OLD.requestDestinationSit is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.requestDestinationSit is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.requestDestinationSit is not null) THEN
    select CONCAT(oldValueList,OLD.requestDestinationSit,'~') into oldValueList;
  END IF;
  IF(NEW.requestDestinationSit is not null) THEN
    select CONCAT(newValueList,NEW.requestDestinationSit,'~') into newValueList;
  END IF;
   END IF;
   IF ((date_format(OLD.receivedDestinationSit,'%Y-%m-%d') <> date_format(NEW.receivedDestinationSit,'%Y-%m-%d')) or (date_format(OLD.receivedDestinationSit,'%Y-%m-%d') is null and date_format(NEW.receivedDestinationSit,'%Y-%m-%d') is not null)
      or (date_format(OLD.receivedDestinationSit,'%Y-%m-%d') is not null and date_format(NEW.receivedDestinationSit,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.receivedDestinationSit~') into fieldNameList;
    IF(OLD.receivedDestinationSit is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.receivedDestinationSit is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.receivedDestinationSit is not null) THEN
    select CONCAT(oldValueList,OLD.receivedDestinationSit,'~') into oldValueList;
  END IF;
  IF(NEW.receivedDestinationSit is not null) THEN
    select CONCAT(newValueList,NEW.receivedDestinationSit,'~') into newValueList;
  END IF;
   END IF;
 IF (OLD.destinationSITReason <> NEW.destinationSITReason ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationSITReason~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationSITReason,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationSITReason,'~') into newValueList;
  END IF;
   IF (OLD.originSITReason <> NEW.originSITReason ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originSITReason~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originSITReason,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originSITReason,'~') into newValueList;
  END IF;
   IF (OLD.networkPartnerCode <> NEW.networkPartnerCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkPartnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerCode,'~') into newValueList;
  END IF;
   IF (OLD.networkPartnerName <> NEW.networkPartnerName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkPartnerName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerName,'~') into newValueList;
  END IF;
     IF (OLD.networkContact <> NEW.networkContact ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkContact,'~') into newValueList;
  END IF;
     IF (OLD.networkEmail <> NEW.networkEmail ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkEmail,'~') into newValueList;
  END IF;
   IF ((date_format(OLD.sentToKSD,'%Y-%m-%d') <> date_format(NEW.sentToKSD,'%Y-%m-%d')) or (date_format(OLD.sentToKSD,'%Y-%m-%d') is null and date_format(NEW.sentToKSD,'%Y-%m-%d') is not null)
      or (date_format(OLD.sentToKSD,'%Y-%m-%d') is not null and date_format(NEW.sentToKSD,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.sentToKSD~') into fieldNameList;
    IF(OLD.sentToKSD is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.sentToKSD is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.sentToKSD is not null) THEN
    select CONCAT(oldValueList,OLD.sentToKSD,'~') into oldValueList;
  END IF;
  IF(NEW.sentToKSD is not null) THEN
    select CONCAT(newValueList,NEW.sentToKSD,'~') into newValueList;
  END IF;
   END IF;
      IF (OLD.lateReason <> NEW.lateReason ) THEN
      select CONCAT(fieldNameList,'trackingstatus.lateReason~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lateReason,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lateReason,'~') into newValueList;
  END IF;
      IF (OLD.partyResponsible <> NEW.partyResponsible ) THEN
      select CONCAT(fieldNameList,'trackingstatus.partyResponsible~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partyResponsible,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partyResponsible,'~') into newValueList;
  END IF;
   IF ((date_format(OLD.bookerDeliveryNotification,'%Y-%m-%d') <> date_format(NEW.bookerDeliveryNotification,'%Y-%m-%d')) or (date_format(OLD.bookerDeliveryNotification,'%Y-%m-%d') is null and date_format(NEW.bookerDeliveryNotification,'%Y-%m-%d') is not null)
      or (date_format(OLD.bookerDeliveryNotification,'%Y-%m-%d') is not null and date_format(NEW.bookerDeliveryNotification,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.bookerDeliveryNotification~') into fieldNameList;
    IF(OLD.bookerDeliveryNotification is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.bookerDeliveryNotification is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.bookerDeliveryNotification is not null) THEN
    select CONCAT(oldValueList,OLD.bookerDeliveryNotification,'~') into oldValueList;
  END IF;
  IF(NEW.bookerDeliveryNotification is not null) THEN
    select CONCAT(newValueList,NEW.bookerDeliveryNotification,'~') into newValueList;
  END IF;
   END IF;
     IF (OLD.soNetworkGroup <> NEW.soNetworkGroup ) THEN
       select CONCAT(fieldNameList,'trackingstatus.soNetworkGroup~') into fieldNameList;
       IF(OLD.soNetworkGroup = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.soNetworkGroup = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.soNetworkGroup = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.soNetworkGroup = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
   END IF;
    IF (OLD.accNetworkGroup <> NEW.accNetworkGroup ) THEN
       select CONCAT(fieldNameList,'trackingstatus.accNetworkGroup~') into fieldNameList;
       IF(OLD.accNetworkGroup = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.accNetworkGroup = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.accNetworkGroup = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.accNetworkGroup = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
   END IF;
    IF (OLD.utsiNetworkGroup <> NEW.utsiNetworkGroup ) THEN
       select CONCAT(fieldNameList,'trackingstatus.utsiNetworkGroup~') into fieldNameList;
       IF(OLD.utsiNetworkGroup = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.utsiNetworkGroup = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.utsiNetworkGroup = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.utsiNetworkGroup = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
   END IF;
     IF (OLD.agentNetworkGroup <> NEW.agentNetworkGroup ) THEN
       select CONCAT(fieldNameList,'trackingstatus.agentNetworkGroup~') into fieldNameList;
       IF(OLD.agentNetworkGroup = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.agentNetworkGroup = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(NEW.agentNetworkGroup = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.agentNetworkGroup = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
   END IF;
      IF (OLD.bookingAgentExSO <> NEW.bookingAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.bookingAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentExSO,'~') into newValueList;
  END IF;
   IF ((date_format(OLD.documentationCutOff,'%Y-%m-%d') <> date_format(NEW.documentationCutOff,'%Y-%m-%d')) or (date_format(OLD.documentationCutOff,'%Y-%m-%d') is null and date_format(NEW.documentationCutOff,'%Y-%m-%d') is not null)
      or (date_format(OLD.documentationCutOff,'%Y-%m-%d') is not null and date_format(NEW.documentationCutOff,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.documentationCutOff~') into fieldNameList;
    IF(OLD.documentationCutOff is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.documentationCutOff is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.documentationCutOff is not null) THEN
    select CONCAT(oldValueList,OLD.documentationCutOff,'~') into oldValueList;
  END IF;
  IF(NEW.documentationCutOff is not null) THEN
    select CONCAT(newValueList,NEW.documentationCutOff,'~') into newValueList;
  END IF;
   END IF;
       IF ((date_format(OLD.passportReceived,'%Y-%m-%d') <> date_format(NEW.passportReceived,'%Y-%m-%d')) or (date_format(OLD.passportReceived,'%Y-%m-%d') is null and date_format(NEW.passportReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.passportReceived,'%Y-%m-%d') is not null and date_format(NEW.passportReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.passportReceived~') into fieldNameList;
    IF(OLD.passportReceived is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.passportReceived is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.passportReceived is not null) THEN
    select CONCAT(oldValueList,OLD.passportReceived,'~') into oldValueList;
  END IF;
  IF(NEW.passportReceived is not null) THEN
    select CONCAT(newValueList,NEW.passportReceived,'~') into newValueList;
  END IF;
   END IF;
       IF ((date_format(OLD.einReceived,'%Y-%m-%d') <> date_format(NEW.einReceived,'%Y-%m-%d')) or (date_format(OLD.einReceived,'%Y-%m-%d') is null and date_format(NEW.einReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.einReceived,'%Y-%m-%d') is not null and date_format(NEW.einReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.einReceived~') into fieldNameList;
    IF(OLD.einReceived is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.einReceived is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.einReceived is not null) THEN
    select CONCAT(oldValueList,OLD.einReceived,'~') into oldValueList;
  END IF;
  IF(NEW.einReceived is not null) THEN
    select CONCAT(newValueList,NEW.einReceived,'~') into newValueList;
  END IF;
   END IF;
     IF (OLD.contractType <> NEW.contractType ) THEN
      select CONCAT(fieldNameList,'trackingstatus.contractType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contractType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contractType,'~') into newValueList;
  END IF;
      IF (OLD.ISFConfirmationNo <> NEW.ISFConfirmationNo ) THEN
      select CONCAT(fieldNameList,'trackingstatus.ISFConfirmationNo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ISFConfirmationNo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ISFConfirmationNo,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.contractReceived,'%Y-%m-%d') <> date_format(NEW.contractReceived,'%Y-%m-%d')) or (date_format(OLD.contractReceived,'%Y-%m-%d') is null and date_format(NEW.contractReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.contractReceived,'%Y-%m-%d') is not null and date_format(NEW.contractReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.contractReceived~') into fieldNameList;
    IF(OLD.contractReceived is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.contractReceived is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.contractReceived is not null) THEN
    select CONCAT(oldValueList,OLD.contractReceived,'~') into oldValueList;
  END IF;
  IF(NEW.contractReceived is not null) THEN
    select CONCAT(newValueList,NEW.contractReceived,'~') into newValueList;
  END IF;
   END IF;
       IF ((date_format(OLD.confirmationOfDelievery,'%Y-%m-%d') <> date_format(NEW.confirmationOfDelievery,'%Y-%m-%d')) or (date_format(OLD.confirmationOfDelievery,'%Y-%m-%d') is null and date_format(NEW.confirmationOfDelievery,'%Y-%m-%d') is not null)
      or (date_format(OLD.confirmationOfDelievery,'%Y-%m-%d') is not null and date_format(NEW.confirmationOfDelievery,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.confirmationOfDelievery~') into fieldNameList;
    IF(OLD.confirmationOfDelievery is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.confirmationOfDelievery is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.confirmationOfDelievery is not null) THEN
    select CONCAT(oldValueList,OLD.confirmationOfDelievery,'~') into oldValueList;
  END IF;
  IF(NEW.confirmationOfDelievery is not null) THEN
    select CONCAT(newValueList,NEW.confirmationOfDelievery,'~') into newValueList;
  END IF;
   END IF;
       IF ((date_format(OLD.podToBooker,'%Y-%m-%d') <> date_format(NEW.podToBooker,'%Y-%m-%d')) or (date_format(OLD.podToBooker,'%Y-%m-%d') is null and date_format(NEW.podToBooker,'%Y-%m-%d') is not null)
      or (date_format(OLD.podToBooker,'%Y-%m-%d') is not null and date_format(NEW.podToBooker,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.podToBooker~') into fieldNameList;
    IF(OLD.podToBooker is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.podToBooker is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.podToBooker is not null) THEN
    select CONCAT(oldValueList,OLD.podToBooker,'~') into oldValueList;
  END IF;
  IF(NEW.podToBooker is not null) THEN
    select CONCAT(newValueList,NEW.podToBooker,'~') into newValueList;
  END IF;
   END IF;
   
   IF (OLD.docCutOffTimeTo <> NEW.docCutOffTimeTo ) THEN
      select CONCAT(fieldNameList,'trackingstatus.docCutOffTimeTo~') into fieldNameList;
      select CONCAT(oldValueList,OLD.docCutOffTimeTo,'~') into oldValueList;
      select CONCAT(newValueList,NEW.docCutOffTimeTo,'~') into newValueList;
  END IF;
    IF (OLD.bookingAgentPhoneNumber <> NEW.bookingAgentPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.bookingAgentPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bookingAgentPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bookingAgentPhoneNumber,'~') into newValueList;
  END IF;
    IF (OLD.networkPhoneNumber <> NEW.networkPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPhoneNumber,'~') into newValueList;
  END IF;
    IF (OLD.originAgentPhoneNumber <> NEW.originAgentPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentPhoneNumber,'~') into newValueList;
  END IF;
 IF (OLD.subDestinationAgentPhoneNumber <> NEW.subDestinationAgentPhoneNumber ) THEN
      select CONCAT(fieldNameList,'trackingstatus.subDestinationAgentPhoneNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.subDestinationAgentPhoneNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.subDestinationAgentPhoneNumber,'~') into newValueList;
  END IF;
  IF (OLD.tmss <> NEW.tmss ) THEN
      select CONCAT(fieldNameList,'trackingstatus.tmss~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tmss,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tmss,'~') into newValueList;
  END IF;
  IF (OLD.originGivenCode <> NEW.originGivenCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originGivenCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originGivenCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originGivenCode,'~') into newValueList;
  END IF;
  IF (OLD.originGivenName <> NEW.originGivenName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originGivenName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originGivenName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originGivenName,'~') into newValueList;
  END IF;
  IF (OLD.originReceivedCode <> NEW.originReceivedCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originReceivedCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originReceivedCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originReceivedCode,'~') into newValueList;
  END IF;
  IF (OLD.originReceivedName <> NEW.originReceivedName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originReceivedName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originReceivedName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originReceivedName,'~') into newValueList;
  END IF;
    IF (OLD.destinationGivenCode <> NEW.destinationGivenCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationGivenCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationGivenCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationGivenCode,'~') into newValueList;
  END IF;
    IF (OLD.destinationGivenName <> NEW.destinationGivenName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationGivenName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationGivenName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationGivenName,'~') into newValueList;
  END IF;
    IF (OLD.destinationReceivedCode <> NEW.destinationReceivedCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationReceivedCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationReceivedCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationReceivedCode,'~') into newValueList;
  END IF;
    IF (OLD.destinationReceivedName <> NEW.destinationReceivedName ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationReceivedName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationReceivedName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationReceivedName,'~') into newValueList;
  END IF;
      IF ((date_format(OLD.actualPackBegin,'%Y-%m-%d') <> date_format(NEW.actualPackBegin,'%Y-%m-%d')) or (date_format(OLD.actualPackBegin,'%Y-%m-%d') is null and date_format(NEW.actualPackBegin,'%Y-%m-%d') is not null)
      or (date_format(OLD.actualPackBegin,'%Y-%m-%d') is not null and date_format(NEW.actualPackBegin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.actualPackBegin~') into fieldNameList;
    IF(OLD.actualPackBegin is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.actualPackBegin is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.actualPackBegin is not null) THEN
    select CONCAT(oldValueList,OLD.actualPackBegin,'~') into oldValueList;
  END IF;
  IF(NEW.actualPackBegin is not null) THEN
    select CONCAT(newValueList,NEW.actualPackBegin,'~') into newValueList;
  END IF;
   END IF;
      IF ((date_format(OLD.actualLoadBegin,'%Y-%m-%d') <> date_format(NEW.actualLoadBegin,'%Y-%m-%d')) or (date_format(OLD.actualLoadBegin,'%Y-%m-%d') is null and date_format(NEW.actualLoadBegin,'%Y-%m-%d') is not null)
      or (date_format(OLD.actualLoadBegin,'%Y-%m-%d') is not null and date_format(NEW.actualLoadBegin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.actualLoadBegin~') into fieldNameList;
    IF(OLD.actualLoadBegin is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.actualLoadBegin is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.actualLoadBegin is not null) THEN
    select CONCAT(oldValueList,OLD.actualLoadBegin,'~') into oldValueList;
  END IF;
  IF(NEW.actualLoadBegin is not null) THEN
    select CONCAT(newValueList,NEW.actualLoadBegin,'~') into newValueList;
  END IF;
   END IF;
      IF ((date_format(OLD.targetdeliveryShipper,'%Y-%m-%d') <> date_format(NEW.targetdeliveryShipper,'%Y-%m-%d')) or (date_format(OLD.targetdeliveryShipper,'%Y-%m-%d') is null and date_format(NEW.targetdeliveryShipper,'%Y-%m-%d') is not null)
      or (date_format(OLD.targetdeliveryShipper,'%Y-%m-%d') is not null and date_format(NEW.targetdeliveryShipper,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.targetdeliveryShipper~') into fieldNameList;
    IF(OLD.targetdeliveryShipper is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.targetdeliveryShipper is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.targetdeliveryShipper is not null) THEN
    select CONCAT(oldValueList,OLD.targetdeliveryShipper,'~') into oldValueList;
  END IF;
  IF(NEW.targetdeliveryShipper is not null) THEN
    select CONCAT(newValueList,NEW.targetdeliveryShipper,'~') into newValueList;
  END IF;
   END IF;
      IF ((date_format(OLD.targetLoadEnding,'%Y-%m-%d') <> date_format(NEW.targetLoadEnding,'%Y-%m-%d')) or (date_format(OLD.targetLoadEnding,'%Y-%m-%d') is null and date_format(NEW.targetLoadEnding,'%Y-%m-%d') is not null)
      or (date_format(OLD.targetLoadEnding,'%Y-%m-%d') is not null and date_format(NEW.targetLoadEnding,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.targetLoadEnding~') into fieldNameList;
    IF(OLD.targetLoadEnding is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.targetLoadEnding is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.targetLoadEnding is not null) THEN
    select CONCAT(oldValueList,OLD.targetLoadEnding,'~') into oldValueList;
  END IF;
  IF(NEW.targetLoadEnding is not null) THEN
    select CONCAT(newValueList,NEW.targetLoadEnding,'~') into newValueList;
  END IF;
   END IF;
   IF ((date_format(OLD.actualDeliveryBegin,'%Y-%m-%d') <> date_format(NEW.actualDeliveryBegin,'%Y-%m-%d')) or (date_format(OLD.actualDeliveryBegin,'%Y-%m-%d') is null and date_format(NEW.actualDeliveryBegin,'%Y-%m-%d') is not null)
      or (date_format(OLD.actualDeliveryBegin,'%Y-%m-%d') is not null and date_format(NEW.actualDeliveryBegin,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.actualDeliveryBegin~') into fieldNameList;
    IF(OLD.actualDeliveryBegin is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.actualDeliveryBegin is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.actualDeliveryBegin is not null) THEN
    select CONCAT(oldValueList,OLD.actualDeliveryBegin,'~') into oldValueList;
  END IF;
  IF(NEW.actualDeliveryBegin is not null) THEN
    select CONCAT(newValueList,NEW.actualDeliveryBegin,'~') into newValueList;
  END IF;
   END IF;
    IF ((date_format(OLD.targetPackEnding,'%Y-%m-%d') <> date_format(NEW.targetPackEnding,'%Y-%m-%d')) or (date_format(OLD.targetPackEnding,'%Y-%m-%d') is null and date_format(NEW.targetPackEnding,'%Y-%m-%d') is not null)
      or (date_format(OLD.targetPackEnding,'%Y-%m-%d') is not null and date_format(NEW.targetPackEnding,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.targetPackEnding~') into fieldNameList;
    IF(OLD.targetPackEnding is null) THEN
    select CONCAT(oldValueList," ",'~') into oldValueList;
  END IF;
  IF(NEW.targetPackEnding is null) THEN
    select CONCAT(newValueList," ",'~') into newValueList;
  END IF;
  IF(OLD.targetPackEnding is not null) THEN
    select CONCAT(oldValueList,OLD.targetPackEnding,'~') into oldValueList;
  END IF;
  IF(NEW.targetPackEnding is not null) THEN
    select CONCAT(newValueList,NEW.targetPackEnding,'~') into newValueList;
  END IF;
   END IF;
  if(OLD.originSubAgentExSO='' and NEW.originSubAgentExSO<>'') THEN
      select sourceSoNum ,soNumber into ssn,networksonumber from networkcontrol where soNumber=NEW.shipnumber and sourceSoNum like '%UGWW%' limit 1;
   if (ssn<>'' and ssn<>networksonumber) then
   set companyDivisionCode='';
    select companycode into companyDivisionCode from companydivision where bookingAgentCode=NEW.originSubAgentCode limit 1;
     insert into networkcontrol (sourceCorpId, soNumber, sourceSoNum, sourceInfo, sourceStatus, corpId, status, action, actionStatus, updatedBy, updatedOn, createdOn, createdBy, childAgentCode, childAgentType, companyDiv)
values
('UGWW', NEW.originSubAgentExSO, ssn, '', 'NEW', SUBSTRING(NEW.originSubAgentExSO,1,4), '', 'Create Link', 'Done', NEW.updatedBy, now(), now(), NEW.createdBy, NEW.originSubAgentCode, 'SOA', companyDivisionCode);
   end if;
   END IF;
     if(OLD.destinationSubAgentExSO='' and NEW.destinationSubAgentExSO<>'') THEN
     select sourceSoNum ,soNumber into ssn,networksonumber from networkcontrol where soNumber=NEW.shipnumber and sourceSoNum like '%UGWW%' limit 1;
   if (ssn<>'' and ssn<>networksonumber) then
   set companyDivisionCode='';
  select companycode into companyDivisionCode from companydivision where bookingAgentCode=NEW.destinationSubAgentCode limit 1;
  insert into networkcontrol (sourceCorpId, soNumber, sourceSoNum, sourceInfo, sourceStatus, corpId, status, action, actionStatus, updatedBy, updatedOn, createdOn, createdBy, childAgentCode, childAgentType, companyDiv)
values
('UGWW', NEW.destinationSubAgentExSO, ssn, '', 'NEW', SUBSTRING(NEW.destinationSubAgentExSO,1,4), '', 'Create Link', 'Done', NEW.updatedBy, now(), now(), NEW.createdBy, NEW.destinationSubAgentCode, 'SDA', companyDivisionCode);
   end if;
  END IF;
  select bookingagentcode into BAcode from serviceorder where id=NEW.id;
select haulingAgentCode into HAcode from miscellaneous where id=NEW.id;
update sodashboard set role=(trim( both '/' from concat(if(NEW.originagent is not null and NEW.originagent<>'','OA/',''),
if(NEW.destinationagent is not null and NEW.destinationagent <>'','DA/',''),
if(NEW.originsubagent is not null and NEW.originsubagent<>'','OSA/',''),
if(NEW.destinationsubagent is not null and NEW.destinationsubagent<>'','DSA/',''),
if(NEW.brokercode is not null and NEW.brokercode<>'','BR/',''),
if(NEW.forwardercode is not null and NEW.forwarder<>'','FR/',''),
if(BAcode is not null and BAcode<>'','BA/',''),
if(NEW.networkPartnerCode is not null and NEW.networkpartnercode<>'','NA/',''),
if(HAcode is not null and HAcode<>'','HA/','')))) where serviceorderid=NEW.id;
 IF (OLD.networkPartnerCode <> NEW.networkPartnerCode ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkPartnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkPartnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkPartnerCode,'~') into newValueList;
  END IF;
     /*Updated field by Bhanu Shukla*/
   IF (OLD.originSubAgent <> NEW.originSubAgent ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originSubAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originSubAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originSubAgent,'~') into newValueList;
  END IF;
  IF (OLD.destinationSubAgent <> NEW.destinationSubAgent ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationSubAgent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationSubAgent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationSubAgent,'~') into newValueList;
  END IF;

IF (OLD.networkAgentExSO <> NEW.networkAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.networkAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.networkAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.networkAgentExSO,'~') into newValueList;
  END IF;
  IF (OLD.originAgentExSO <> NEW.originAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originAgentExSO,'~') into newValueList;
  END IF;
  IF (OLD.originSubAgentExSO <> NEW.originSubAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.originSubAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.originSubAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.originSubAgentExSO,'~') into newValueList;
  END IF;
  IF (OLD.destinationAgentExSO <> NEW.destinationAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationAgentExSO,'~') into newValueList;
  END IF;
  IF (OLD.destinationSubAgentExSO <> NEW.destinationSubAgentExSO ) THEN
      select CONCAT(fieldNameList,'trackingstatus.destinationSubAgentExSO~') into fieldNameList;
      select CONCAT(oldValueList,OLD.destinationSubAgentExSO,'~') into oldValueList;
      select CONCAT(newValueList,NEW.destinationSubAgentExSO,'~') into newValueList;
  END IF;
  IF (OLD.hbillLading <> NEW.hbillLading ) THEN
      select CONCAT(fieldNameList,'trackingstatus.hbillLading~') into fieldNameList;
      select CONCAT(oldValueList,OLD.hbillLading,'~') into oldValueList;
      select CONCAT(newValueList,NEW.hbillLading,'~') into newValueList;
  END IF;
  IF ((date_format(OLD.crewArrivalDate,'%Y-%m-%d') <> date_format(NEW.crewArrivalDate,'%Y-%m-%d')) or (date_format(OLD.crewArrivalDate,'%Y-%m-%d') is null and date_format(NEW.crewArrivalDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.crewArrivalDate,'%Y-%m-%d') is not null and date_format(NEW.crewArrivalDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.crewArrivalDate~') into fieldNameList;
    IF(OLD.crewArrivalDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.crewArrivalDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.crewArrivalDate is not null) THEN
select CONCAT(oldValueList,OLD.crewArrivalDate,'~') into oldValueList;
END IF;
IF(NEW.crewArrivalDate is not null) THEN
select CONCAT(newValueList,NEW.crewArrivalDate,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.customClearance,'%Y-%m-%d') <> date_format(NEW.customClearance,'%Y-%m-%d')) or (date_format(OLD.customClearance,'%Y-%m-%d') is null and date_format(NEW.customClearance,'%Y-%m-%d') is not null)
      or (date_format(OLD.customClearance,'%Y-%m-%d') is not null and date_format(NEW.customClearance,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.customClearance~') into fieldNameList;
    IF(OLD.customClearance is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.customClearance is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.customClearance is not null) THEN
select CONCAT(oldValueList,OLD.customClearance,'~') into oldValueList;
END IF;
IF(NEW.customClearance is not null) THEN
select CONCAT(newValueList,NEW.customClearance,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.endCustomClearance,'%Y-%m-%d') <> date_format(NEW.endCustomClearance,'%Y-%m-%d')) or (date_format(OLD.endCustomClearance,'%Y-%m-%d') is null and date_format(NEW.endCustomClearance,'%Y-%m-%d') is not null)
      or (date_format(OLD.endCustomClearance,'%Y-%m-%d') is not null and date_format(NEW.endCustomClearance,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.endCustomClearance~') into fieldNameList;
    IF(OLD.endCustomClearance is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.endCustomClearance is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.endCustomClearance is not null) THEN
select CONCAT(oldValueList,OLD.endCustomClearance,'~') into oldValueList;
END IF;
IF(NEW.endCustomClearance is not null) THEN
select CONCAT(newValueList,NEW.endCustomClearance,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.ffwRequested,'%Y-%m-%d') <> date_format(NEW.ffwRequested,'%Y-%m-%d')) or (date_format(OLD.ffwRequested,'%Y-%m-%d') is null and date_format(NEW.ffwRequested,'%Y-%m-%d') is not null)
      or (date_format(OLD.ffwRequested,'%Y-%m-%d') is not null and date_format(NEW.ffwRequested,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.ffwRequested~') into fieldNameList;
    IF(OLD.ffwRequested is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.ffwRequested is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.ffwRequested is not null) THEN
select CONCAT(oldValueList,OLD.ffwRequested,'~') into oldValueList;
END IF;
IF(NEW.ffwRequested is not null) THEN
select CONCAT(newValueList,NEW.ffwRequested,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.ffwReceived,'%Y-%m-%d') <> date_format(NEW.ffwReceived,'%Y-%m-%d')) or (date_format(OLD.ffwReceived,'%Y-%m-%d') is null and date_format(NEW.ffwReceived,'%Y-%m-%d') is not null)
      or (date_format(OLD.ffwReceived,'%Y-%m-%d') is not null and date_format(NEW.ffwReceived,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.ffwReceived~') into fieldNameList;
    IF(OLD.ffwReceived is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.ffwReceived is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.ffwReceived is not null) THEN
select CONCAT(oldValueList,OLD.ffwReceived,'~') into oldValueList;
END IF;
IF(NEW.ffwReceived is not null) THEN
select CONCAT(newValueList,NEW.ffwReceived,'~') into newValueList;
END IF;
   END IF;
   IF ((date_format(OLD.ediDate,'%Y-%m-%d') <> date_format(NEW.ediDate,'%Y-%m-%d')) or (date_format(OLD.ediDate,'%Y-%m-%d') is null and date_format(NEW.ediDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.ediDate,'%Y-%m-%d') is not null and date_format(NEW.ediDate,'%Y-%m-%d') is null)) THEN
      select CONCAT(fieldNameList,'trackingstatus.ediDate~') into fieldNameList;
    IF(OLD.ediDate is null) THEN
select CONCAT(oldValueList," ",'~') into oldValueList;
END IF;
IF(NEW.ediDate is null) THEN
select CONCAT(newValueList," ",'~') into newValueList;
END IF;
IF(OLD.ediDate is not null) THEN
select CONCAT(oldValueList,OLD.ediDate,'~') into oldValueList;
END IF;
IF(NEW.ediDate is not null) THEN
select CONCAT(newValueList,NEW.ediDate,'~') into newValueList;
END IF;
   END IF;

      call ServiceOrder_UpdateStatus(NEW.shipnumber,NEW.updatedBy);
CALL add_tblHistory (OLD.id,"trackingstatus", fieldNameList, oldValueList, newValueList, NEW.updatedBy, OLD.corpID, now());

  END