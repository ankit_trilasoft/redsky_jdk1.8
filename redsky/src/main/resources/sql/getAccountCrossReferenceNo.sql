DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`getAccountCrossReferenceNo` $$
CREATE DEFINER=`root`@`%` FUNCTION `getAccountCrossReferenceNo`(code varchar(500),rtype1 varchar(50),corpid varchar(5)) RETURNS varchar(5000) CHARSET latin1
BEGIN
  DECLARE Name1 varchar(5000) ;

select accountcrossreference into Name1
from partneraccountref where corpid=corpid and partnercode=code and reftype=rtype1 limit 1;

RETURN Name1;
END $$

DELIMITER ;