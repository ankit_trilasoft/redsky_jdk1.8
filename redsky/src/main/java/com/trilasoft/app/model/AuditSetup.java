//----Created By Bibhash---

package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="auditSetup")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class AuditSetup extends BaseObject{
	
	private Long id;
	private String corpID;
	public String tableName;   
    public String fieldName;
    public String auditable;
    public String description;
    
    private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Date valueDate;
	private Boolean isAlert;
	private String alertRole;
	private String alertValue;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("tableName", tableName).append("fieldName",
				fieldName).append("auditable", auditable).append("description",
				description).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("valueDate", valueDate).append("isAlert",
				isAlert).append("alertRole", alertRole).append("alertValue", alertValue).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AuditSetup))
			return false;
		AuditSetup castOther = (AuditSetup) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(tableName, castOther.tableName)
				.append(fieldName, castOther.fieldName).append(auditable,
						castOther.auditable).append(description,
						castOther.description).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(valueDate,
						castOther.valueDate).append(isAlert, castOther.isAlert)
				.append(alertRole, castOther.alertRole).append(alertValue, castOther.alertValue).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(tableName).append(fieldName).append(auditable).append(
						description).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(valueDate).append(
						isAlert).append(alertRole).append(alertValue).toHashCode();
	}
	public String getAuditable() {
		return auditable;
	}
	public void setAuditable(String auditable) {
		this.auditable = auditable;
	}
	
	@Column( length=50 )
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	@Column( length=25 )
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column( length=82 )
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column( length=25 )
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column( length=82 )
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column( length=25 )
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public Date getValueDate() {
		return valueDate;
	}
	public void setValueDate(Date valueDate) {
		this.valueDate = valueDate;
	}
	@Column( length=10 )
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column( length=1500 )
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column( length=25 )
	public String getAlertRole() {
		return alertRole;
	}
	public void setAlertRole(String alertRole) {
		this.alertRole = alertRole;
	}
	@Column
	public Boolean getIsAlert() {
		return isAlert;
	}
	public void setIsAlert(Boolean isAlert) {
		this.isAlert = isAlert;
	}
	public String getAlertValue() {
		return alertValue;
	}
	public void setAlertValue(String alertValue) {
		this.alertValue = alertValue;
	}
	

	

}
