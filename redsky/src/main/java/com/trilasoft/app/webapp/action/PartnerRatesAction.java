package com.trilasoft.app.webapp.action;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerRates;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerRatesManager;
import com.trilasoft.app.service.RefMasterManager;

public class PartnerRatesAction extends BaseAction{

	    
	    private List partnerRatess;
		private PartnerRatesManager partnerRatesManager;
		PartnerManager partnerManager;
		private PartnerRates partnerRates;
		private Partner partner;
		private Long id;
		private Long partnerId;
		private Long i;
		private Long maxId;
		
		private List refMasters;
	    private RefMasterManager refMasterManager;
	    
	    private static Map<String, String> ratetype;
	    private static List units;
	    
	    private String sessionCorpID;
	   // private List day;
	    
	    public PartnerRatesAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
	    
	    public void setPartnerRatesManager(PartnerRatesManager partnerRatesManager) {
	        this.partnerRatesManager = partnerRatesManager;
	    }
	    public void setPartnerManager(PartnerManager partnerManager) {
	        this.partnerManager = partnerManager;
	    }

	  
	    
	    public List getPartnerRatess() {
	        return partnerRatess;
	    }

	    public String list() {
	    	//System.out.println("Hello");
	        partnerRatess = partnerRatesManager.getAll();
	        return SUCCESS;
	    }
	    
	    
	    
	    
	    
	    public void setId(Long id) {   
	        this.id = id;   
	    }  
	    
	    public void setPartnerId(Long partnerId) {
			this.partnerId = partnerId;
		}
		public PartnerRates getPartnerRates() {   
	        return partnerRates;   
	    }   
	      
	    public void setPartnerRates(PartnerRates partnerRates) {   
	        this.partnerRates = partnerRates;   
	    }   
	      
	 
	    public Partner getPartner() {
			return partner;
		}
		public void setPartner(Partner partner) {
			this.partner = partner;
		}
		public String edit() {
			getComboList(sessionCorpID);
	        if (id != null) {   
	        	partnerRates = partnerRatesManager.get(id);
	            partner = partnerRates.getPartner();
	        } else {   
	        	partner = partnerManager.get(partnerId);
	        	partnerRates = new PartnerRates();
	        	partnerRates.setCorpID(sessionCorpID);
	        	partnerRates.setPartnerCode(partner.getPartnerCode());
	           
	        	partnerRates.setCreatedOn(new Date());
	        	partnerRates.setUpdatedOn(new Date());
	        }   
	        
	        return SUCCESS;   
	    }   
	    
	    public String save() throws Exception { 
	    	getComboList(sessionCorpID);
	        if (cancel != null) {   
	            return "cancel";   
	        }   
	      
	        partnerRates.setCorpID(sessionCorpID);
        	partnerRates.setPartner(partner);
	        boolean isNew = (partnerRates.getId() == null); 
	        if(isNew){
	        	partnerRates.setCreatedOn(new Date());
	        }
	        partnerRates.setUpdatedOn(new Date());
	        //System.out.println(partnerRates.getId());
	        //partnerRates.setCorpID("SSCW");
	        partnerRatesManager.save(partnerRates);   
	        if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
	        String key = (isNew) ? "partnerRates.added" : "partnerRates.updated";   
	        saveMessage(getText(key));   
	        }
	        if (!isNew) {   
	            return INPUT;   
	        }else
	        	maxId = Long.parseLong(((partnerRatesManager.findMaximum().get(0)).toString()));
	        	//System.out.println(maxId);
	        	partnerRates = partnerRatesManager.get(maxId);
	        	partner = partnerRates.getPartner();
	            return SUCCESS;   
	        }   
	    
	    public void setRefMasterManager(RefMasterManager refMasterManager) {
	        this.refMasterManager = refMasterManager;
	    }
	    public List getRefMasters(){
	    	return refMasters;
	    }
	    @SkipValidation
	    public String getComboList(String corpId){
	    	ratetype = refMasterManager.findByParameter(corpId, "RATETYPE");
	    	units = new ArrayList();
	        units.add("Kgs");
	        units.add("Lbs");
	        
	    	return SUCCESS;
	     }
	    public  Map<String, String> getRatetype() {
			return ratetype;
		}
	    public  List getUnits() {
			return units;
		}
	    
	    private String gotoPageString;
		private String validateFormNav;
		public String getValidateFormNav() {
			return validateFormNav;
		}

		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		}
		public String getGotoPageString() {

		return gotoPageString;

		}

		public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

		}
		 
		public String saveOnTabChange() throws Exception {
		    //if (option enabled for this company in the system parameters table then call save) {
				
		        String s = save();    // else simply navigate to the requested page)
		        validateFormNav = "OK";
		        return gotoPageString;
		}
	    
	}