<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>vanLine List</title>   
    <meta name="heading" content="vanLine List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head>   
<s:form id="vanForm" name="vanStCateForm1" method="post" validate="true">
<configByCorp:fieldVisibility componentId="component.field.Alternative.GLCODES">
	<s:hidden name="glCodeFlag" value="YES"/>
</configByCorp:fieldVisibility>
<!--<table class="detailTabLabel" cellpadding="1" cellspacing="1" border="0" style="width:100%;">
<tr>
 <td align="left" class="listwhitetext" ><b>Order List</b></td>
	<td align="right"  style="padding:3px 5px 0px 0px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
--><display:table name="vanLineRegNumList" class="table" requestURI="" id="vanLineRegNumList" pagesize="100" style="width:100%;margin-bottom:0px;" partialList="true" size="100"> 
   <%-- <c:choose>
	<c:when test="${vanLineRegNumList.regNum!='' && vanLineRegNumList.regNum!=null}">	
	<display:column property="regNum"  title="Reg #" style="width:22%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:22%;"/>
	</c:otherwise>
	</c:choose> --%>
   <%--  <c:if test="${vanLineRegNumList.regNum!=null &&  vanLineRegNumList.regNum!=''}">
    <display:column  style="width:10px">	
	<img id="${vanLineRegNumList_rowNum}" onclick ="displayResult('${vanLineRegNumList.regNum}','${vanLineRegNumList.statementCategory}','${vanLineRegNumList.agent}','${vanLineRegNumList.weekEnding }',this,this.id);" src="${pageContext.request.contextPath}/images/plus-small.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
	</display:column>
    <display:column property="regNum" style="width:8%;" title="Reg #"/>    
    </c:if> --%>
    <%-- <display:column property="tranDate" sortable="true" title="Transaction Date" style="width:30px" format="{0,date,dd-MMM-yyyy}"/>	 --%>
	<display:column property="glType"  titleKey="vanLine.glType" style="width:8%;"/> 
	<c:choose>
	<c:when test="${vanLineRegNumList.description!='' && vanLineRegNumList.description!=null}">	
	<display:column property="description"  titleKey="vanLine.description" style="width:22%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:22%;"/>
	</c:otherwise>
	</c:choose>
	<display:column property="driverName" style="width:15%;" titleKey="vanLine.driverName"/>
	<display:column titleKey="vanLine.amtDueAgent" style="text-align: right; width:12%;">
	<a href="javascript:;" onclick="setUrl1(this,${vanLineRegNumList.id});" ><fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineRegNumList.amtDueAgent}"  /></a> 
	</display:column>
	<display:column   titleKey="vanLine.reconcileAmount" style="text-align: right; width:11%;">
	<fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${vanLineRegNumList.reconcileAmount}"  />
	</display:column>		
   <%-- <display:column property="glType"  titleKey="vanLine.glType" style="width:8%;"/>  --%> 
	<display:column property="reconcileStatus"  titleKey="vanLine.reconcileStatus" style="width:12%;"/>
	<%-- <display:column property="driverName" style="width:15%;" titleKey="vanLine.driverName"/> --%>
	<%-- <c:choose>
	<c:when test="${vanLineRegNumList.description!='' && vanLineRegNumList.description!=null}">	
	<display:column property="description"  titleKey="vanLine.description" style="width:22%;"/>	
	</c:when>
	<c:otherwise>
	<display:column property="distributionCodeDescription"  titleKey="vanLine.description" style="width:22%;"/>
	</c:otherwise>
	</c:choose> --%>
</display:table>
</s:form>


