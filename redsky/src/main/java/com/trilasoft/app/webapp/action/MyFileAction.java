package com.trilasoft.app.webapp.action;

import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/*import net.sf.jasperreports.engine.JRParameter;*/




import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.io.FilenameUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;



/*import javax.imageio.ImageIO;

import com.lowagie.text.Image;
import com.aspose.words.Document;
//import com.aspose.words.SaveFormat;

import aspose.pdf.Pdf;
import aspose.pdf.Section;

import com.aspose.cells.SaveFormat;
import com.aspose.cells.Workbook;

import com.aspose.words.FileFormatInfo;
import com.aspose.words.FileFormatUtil;*/
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.trilasoft.app.dao.hibernate.dto.ReportEmailDTO;
import com.trilasoft.app.dao.hibernate.util.FileSizeUtil;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.EmailSetup;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.MyFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.Truck;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.MyFileManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.TruckManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketManager;
import java.util.*;
public class MyFileAction extends BaseAction implements ValidationAware,Preparable{

	private static final long serialVersionUID = -9208910183310010569L;

	private File file;

	private String fileContentType;
	
	private String docCategory;
	
	private String docType;

	private String fileFileName;

	private String name;

	private MyFileManager myFileManager;

	private CustomerFileManager customerFileManager;

	private ServiceOrderManager serviceOrderManager;

	private WorkTicketManager workTicketManager;

	private RefMasterManager refMasterManager;

	private RefMaster refMaster;

	private ClaimManager claimManager;
	private ToDoRuleManager toDoRuleManager;

	private CustomerFile customerFile;

	private ServiceOrder serviceOrder;

	private Partner partner;
	
	private PartnerPrivate partnerPrivate;
	
	private PartnerPrivateManager partnerPrivateManager;
	
	private Truck truck;
	
	private TruckManager truckManager;
	
	private PartnerManager partnerManager;

	private WorkTicket workTicket;

	private Claim claim;

	private AccountLine accountLine;

	private AccountLineManager accountLineManager;

	private Vehicle vehicle;

	private VehicleManager vehicleManager;

	private ServicePartner servicePartner;
	
	private NotesManager notesManager;
	
	private ServicePartnerManager servicePartnerManager;

	private Container container;

	private ContainerManager containerManager;

	private Carton carton;

	private CartonManager cartonManager;

	private List myFiles;

	private List docAccessControlList;

	private List updateMyfileStatusList;

	private MyFile myFile;

	private Long id;

	private Long fid;

	private Long ids;

	private String rId;

	private String Status;

	private String fileType;

	private String fileTypes;

	private String descriptions;

	private String fileIds;

	private String createdBys;

	private Date createdOns;

	private String fileId;

	private String description ;

	private Map<String, String> docsList;
	private  Map<String, String> docsListAccountPortal =new LinkedHashMap<String, String>();;
	private Map<String, String> mapList;

	private Map<String, String> relocationServices;
	
	private String myFileFor;

	private String myFileFrom;

	private String popup;

	private String relatedDocs;

	private String sessionCorpID;

	private Boolean isCportal;

	private Boolean isAccportal;

	private Boolean isPPortal;

	private Boolean isPartnerPortal;
	
	private Boolean isBookingAgent;

	private Boolean isNetworkAgent;

	private Boolean isOriginAgent;

	private Boolean isSubOriginAgent;

	private Boolean isDestAgent;
	
	private Boolean isSubDestAgent;
	
	private Boolean isDriver;
	
	private String index;
	private String asmlFlag;
	private String agentHoFlag;

	private String duplicates;

	private MimetypesFileTypeMap mimetypesFileTypeMap;

	private String seqNum;

	private String fileNotExists;

	private String fileName;

	private String did;

	String checkBoxId;

	private String relTo;

	private String hitFlag;

	private List soNameList;

	private String pageIndex;

	private String mapFolder;

	private String hitflag;
	private String rowId;

	private String userCheckConfirm;

	private Set<Role> userRole;
	private String userType;
	
	private String fieldName;
	private String sortOrder;
	

	private String active;

	private MiscellaneousManager miscellaneousManager;

	private Miscellaneous miscellaneous;

	private TrackingStatusManager trackingStatusManager;

	private TrackingStatus trackingStatus;

	private List docMapList;

	private String secure = "";

	private Billing billing;

	private BillingManager billingManager;
	
	private String from = "";
	
	private String ppCode = "";
	
	private String forQuotation="";
	
	private String myFileIds;
	
	private Notes notes;
	
	public List notesFileId;

	private String flagType;
	
	private String bookingAgentFlag="";
	private String networkAgentFlag="";
	private String originAgentFlag="";
	private String subOriginAgentFlag="";
	private String destAgentFlag="";
	private String subDestAgentFlag="";
	
	private String checkFlagBA="";
	private String checkFlagNA="";
	private String checkFlagOA="";
	private String checkFlagSOA="";
	private String checkFlagDA="";
	private String checkFlagSDA="";
	private String checkAgent;

	private String bookingAgent;
	private String networkAgent;
	private String originAgent;
	private String subOriginAgent;
	private String destAgent;
	private String subDestAgent;
	private String transDocSysDefault;
	private String transDocJobType;
	private String docName;
	private Integer docSize;
	private String TempnotesId;
	private String flagBeforeCheck;
	private String flagBeforeCheckDA;
	private String flagBeforeCheckOA;
	private String flagBeforeCheckNA;
	private String flagBeforeCheckSOA;
	private String flagBeforeCheckSDA;
	private EmailSetupManager emailSetupManager;
	private Map<String, String> emailStatusList;
	private String documentCategory;
	private String categorySearchValue="";
	private Map<String, String> categoryList;
	private Map<String ,List<MyFile>> docTypeFiles;
	private List<SystemDefault> sysDefaultDetail;
	private Boolean flagchk;
	private String docUpload;
	private String fileCabinetView;
	private Boolean isServiceProvider;
	private Boolean invoiceAttachment;
	private String PPID;
	private SystemDefault systemDefault;
	private SystemDefaultManager systemDefaultManager;
	private Map<String, String> defaultSortforFileCabinet;
	private ErrorLogManager errorLogManager;
	private String checkBoxEmailId;
	private String myFileLocation ="" ;
	private String jobNumber;
	private String myFileModule;
	private String reportModule;
	private String noteFor;
	private String reportSignaturePart;
	private String companyDivision;

	private User user1;
	private UserManager userManager;
	private List recipientWithEmailList;
	private String myFileId;
	private Company company; 
	private String voxmeIntergartionFlag;
	private String mmValidation = "NO";
	private CompanyManager companyManager;
	private List fileTypeForPartnerCode;
	private List fileTypePayableProcessing;
	private String oiJobList="";
	 private Boolean visibilityFornewAccountline=false;
	 private Boolean enterpriseLicense = false;
	 private String vendorCode;
	 private String ediBillTo="";

	public void prepare() throws Exception {
		try {
			List<User> user=userManager.findUserDetails(getRequest().getRemoteUser());
			if (!(user == null || user.isEmpty())) {
				for (User user1 : user) {
					//this.reportSignature = user1.getEmail();
					this.reportSignaturePart=user1.getSignature();
				}
			}
			Cookie[] cookies = getRequest().getCookies();
			   for(Cookie c: cookies) {
			           // System.out.println("Cookie: "+c.getName()+" : "+c.getValue());
			            if(c.getName().equals("TempnotesId")){
			            	TempnotesId=c.getValue();		                	
			            }
			   }    
			   systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
			   ediBillTo = systemDefault.getEdiBillToCode();
			   user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			   defaultSortforFileCabinet = refMasterManager.findCountryIso2(sessionCorpID, "DEFAULTSORTFORFILECABINET");
			   fileTypeForPartnerCode = myFileManager.fileTypeForPartnerCode(sessionCorpID); 
			   company=companyManager.findByCorpID(sessionCorpID).get(0);
				 if(company!=null && company.getOiJob()!=null){
					 oiJobList=company.getOiJob()  ;
				 }
				 if(company!=null && company.getLicenseType()!=null && company.getLicenseType().trim().equalsIgnoreCase("E")){
					 enterpriseLicense=true; 
				 }
				 
				} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}

	public MyFileAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		userRole = user.getRoles();
		userType =user.getUserType();
		fileCabinetView = user.getFileCabinetView();
		this.sessionCorpID = user.getCorpID();
		mimetypesFileTypeMap = new MimetypesFileTypeMap();
		mimetypesFileTypeMap.addMimeTypes("text/plain txt text TXT");
		mimetypesFileTypeMap.addMimeTypes("application/msword doc");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-powerpoint ppt");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-excel xls xlsx");
		mimetypesFileTypeMap.addMimeTypes("application/vnd.ms-outlook msg");
		mimetypesFileTypeMap.addMimeTypes("application/pdf pdf PDF");
		mimetypesFileTypeMap.addMimeTypes("image/jpeg jpg jpeg");
		mimetypesFileTypeMap.addMimeTypes("image/gif gif");
		this.flagchk=false;
		
	}

	public String accountPortalFileList(){
		try {
			asmlFlag=this.asmlFlag;
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			customerFile = customerFileManager.get(id);
			myFiles=myFileManager.findAccountPortalFileList(customerFile.getSequenceNumber(),sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	private String resultType;
	private String shipNumber;
	private String myFileJspName;
	public String docTypeFileList() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			categoryList=refMasterManager.findByParameter(sessionCorpID, "DOCUMENT CATEGORY");
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			if(company!=null){
				if(company.getVoxmeIntegration()!=null){
					voxmeIntergartionFlag=company.getVoxmeIntegration().toString();			
					
				}
				 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
					mmValidation= "Yes";
					if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
						mmValidation= "No";
					}
				}
				}
			String tempSortOrder="";
			String sort11=user1.getDefaultSortforFileCabinet();
			if(sort11!=null && !sort11.equals("") &&!sort11.contains(",") ){
				tempSortOrder=defaultSortforFileCabinet.get(sort11);
			}
			if(sort11!=null && !sort11.equals("") && sort11.contains(",") ){
				String[] temp11=sort11.split(",");
				for(String sort111:temp11){
					sort111=sort111.trim();
					if(sort111!=null && !sort111.equals("")){
					if(tempSortOrder.equals("")){
						tempSortOrder=defaultSortforFileCabinet.get(sort111);
					}else{
						tempSortOrder=tempSortOrder+","+defaultSortforFileCabinet.get(sort111);;
					}
					}
				}
			}
			String sort= new String("asc");
			if(user1.getSortOrderForFileCabinet()!=null && user1.getSortOrderForFileCabinet().equalsIgnoreCase("Descending") ){
				sort = "desc";
			}
			if (myFileFor != null && myFileFor.equals("CF")) {
				docTypeFiles = myFilesListFromCustomerDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromCustomer(secure,categorySearchValue);			
				if (myFileFor.equals("CF") && customerFile!=null) {
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
					if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
					Iterator it = shipNumberList1.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }	
			}
			}	
			}
			if (myFileFor != null && myFileFor.equals("SO")) {
				docTypeFiles =myFilesListFromServiceOrderDescDocType(secure,categorySearchValue,tempSortOrder,sort);
//			myFiles = myFilesListFromServiceOrder(secure,categorySearchValue);
				if (myFileFor.equals("SO")) {
					List defCheckedShipNumber = new ArrayList();
					if((shipNumber!=null && !shipNumber.equalsIgnoreCase("")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
						defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
						resultType="errorNoFile";
					}else{
						defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
					}
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList = new ArrayList();
					if(shipNumber!=null && !shipNumber.equalsIgnoreCase("")){
						shipNumberList = myFileManager.getShipNumberList(shipNumber);
					}else{
						shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
					}
					if(shipNumberList!=null && !shipNumberList.isEmpty()){
					Iterator it = shipNumberList.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }
			
			}
			if (myFileFor != null && myFileFor.equals("PO")) {
				docTypeFiles =  myFilesListFromPartnerDocType(secure,categorySearchValue,tempSortOrder,sort);
				if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
					resultType="errorNoFile";
				}
			}
			if (myFileFor != null && myFileFor.equals("Truck")) {
				docTypeFiles = myFilesListFromTruckDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromTruck(secure,categorySearchValue);
			}		
			if (myFileFor != null && myFileFor.equals("WKT")) {
				docTypeFiles = myFilesListFromWorkTicketDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromWorkTicket(secure,categorySearchValue);
			}

			if (myFileFor != null && myFileFor.equals("CLM")) {
				docTypeFiles = myFilesListFromClaimDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromClaim(secure,categorySearchValue);
			}

			if (myFileFor != null && myFileFor.equals("ACC")) {
				docTypeFiles = myFilesListFromAccountingDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromAccounting(secure,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("VEH")) {
				docTypeFiles = myFilesListFromVehicleDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromVehicle(secure,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("CONT")) {
				docTypeFiles = myFilesListFromContainerDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromContainer(secure,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("CART")) {
				docTypeFiles = myFilesListFromCartonDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromCarton(secure,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("SP")) {
				docTypeFiles =  myFilesListFromServicePartnerDocType(secure,categorySearchValue,tempSortOrder,sort);
				//myFiles = myFilesListFromServicePartner(secure,categorySearchValue);
			}
			try{
			String emailSetupIds="";
			for(Map.Entry<String, List<MyFile>> rec:docTypeFiles.entrySet()){
				List<MyFile> myFileTemp=rec.getValue();
				for(MyFile myFile:myFileTemp){
					if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
						if(emailSetupIds.equalsIgnoreCase("")){
							emailSetupIds="'"+myFile.getEmailStatus()+"'";
						}else{
							emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
						}
				    }
				}
			}
			emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
			}catch(Exception e){e.printStackTrace();}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
	
	
	public String list() {
		Long t1=System.currentTimeMillis();
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();			
				
			}
			 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
			}
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			categoryList=refMasterManager.findByParameter(sessionCorpID, "DOCUMENT CATEGORY");
			user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			if(flagchk!=true){
			fieldName =user1.getDefaultSortforFileCabinet();
			}
			if(fieldName==null || fieldName.equals("")){
				fieldName="2";		
			}
			sortOrder=user1.getSortOrderForFileCabinet();
			if(sortOrder!=null && !sortOrder.equals("")){
				sortOrder=sortOrder.toLowerCase();	
			}
			else{
				sortOrder="ascending";	
			}
			if (myFileFor != null && myFileFor.equals("CF")) {
				myFiles = myFilesListFromCustomer(secure,categorySearchValue);			
				if (myFileFor.equals("CF")) {
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
					if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
					Iterator it = shipNumberList1.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }	
			}
			}	
			}
			if (myFileFor != null && myFileFor.equals("SO")) {
				myFiles = myFilesListFromServiceOrder(secure,categorySearchValue);
				if (myFileFor.equals("SO")) {
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
					if(shipNumberList!=null && !shipNumberList.isEmpty()){
					Iterator it = shipNumberList.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }
			}
			if (myFileFor != null && myFileFor.equals("PO")) {
				myFiles = myFilesListFromPartner(secure,categorySearchValue);
				if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
					resultType="errorNoFile";
				}
			}
			if (myFileFor != null && myFileFor.equals("Truck")) {
				myFiles = myFilesListFromTruck(secure,categorySearchValue);
			}		
			if (myFileFor != null && myFileFor.equals("WKT")) {
				myFiles = myFilesListFromWorkTicket(secure,categorySearchValue);
			}

			if (myFileFor.equals("CLM")) {
				myFiles = myFilesListFromClaim(secure,categorySearchValue);
			}

			if (myFileFor.equals("ACC")) {
				myFiles = myFilesListFromAccounting(secure,categorySearchValue);
			}
			if (myFileFor.equals("VEH")) {
				myFiles = myFilesListFromVehicle(secure,categorySearchValue);
			}
			if (myFileFor.equals("CONT")) {
				myFiles = myFilesListFromContainer(secure,categorySearchValue);
			}
			if (myFileFor.equals("CART")) {
				myFiles = myFilesListFromCarton(secure,categorySearchValue);
			}
			if (myFileFor.equals("SP")) {
				myFiles = myFilesListFromServicePartner(secure,categorySearchValue);
			}
			try{
			String emailSetupIds="";
			List<MyFile> myFileTemp=myFiles;
			for(MyFile myFile:myFileTemp){
				if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
					if(emailSetupIds.equalsIgnoreCase("")){
						emailSetupIds="'"+myFile.getEmailStatus()+"'";
					}else{
						emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
					}
			    }
			}
			emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
			}catch(Exception e){e.printStackTrace();}			
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		Long t2=System.currentTimeMillis();
		//System.out.println("time taken in listingggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg"+(t2-t1));
		return SUCCESS;
	}
	public String secureList() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			if (myFileFor != null && myFileFor.equals("CF")) {
				myFiles = myFilesListFromCustomer(secure,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("SO")) {
				myFiles = myFilesListFromServiceOrder(secure,categorySearchValue);
			}

			if (myFileFor != null && myFileFor.equals("WKT")) {
				myFiles = myFilesListFromWorkTicket(secure, categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("PO")) {
				myFiles = myFilesListFromPartner(secure ,categorySearchValue);
			}
			if (myFileFor != null && myFileFor.equals("Truck")) {
				myFiles = myFilesListFromTruck(secure,categorySearchValue);
			}
			if (myFileFor.equals("CLM")) {
				myFiles = myFilesListFromClaim(secure,categorySearchValue);
			}

			if (myFileFor.equals("ACC")) {
				myFiles = myFilesListFromAccounting(secure,categorySearchValue);
			}
			if (myFileFor.equals("VEH")) {
				myFiles = myFilesListFromVehicle(secure,categorySearchValue);
			}
			if (myFileFor.equals("CONT")) {
				myFiles = myFilesListFromContainer(secure,categorySearchValue);
			}
			if (myFileFor.equals("CART")) {
				myFiles = myFilesListFromCarton(secure,categorySearchValue);
			}
			if (myFileFor.equals("SP")) {
				myFiles = myFilesListFromServicePartner(secure,categorySearchValue);
			}
			try{
			String emailSetupIds="";
			List<MyFile> myFileTemp=myFiles;
			for(MyFile myFile:myFileTemp){
				if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
					if(emailSetupIds.equalsIgnoreCase("")){
						emailSetupIds="'"+myFile.getEmailStatus()+"'";
					}else{
						emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
					}
			    }
			}
     		emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
			}catch(Exception e){e.printStackTrace();}
			user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			fieldName =user1.getDefaultSortforFileCabinet();
			if(fieldName==null || fieldName.equals("")){
				fieldName="2";		
			}
			if(fieldName.equals("3")){
				fieldName="0";		
				}
				if(fieldName.equals("4")){
				fieldName="3";		
				}
				if(fieldName.equals("6")){
				fieldName="5";		
				}
				if(fieldName.equals("7")){
				fieldName="6";		
				}
			sortOrder=user1.getSortOrderForFileCabinet();
			if(sortOrder!=null && !sortOrder.equals("")){
				sortOrder=sortOrder.toLowerCase();	
			}else{
				sortOrder="ascending";	
			}
		if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("secureFiles"))){
			resultType="errorNoFile";
		}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	private String sid;
    private String flagUloadInv;
	@SkipValidation
	public String accountFiles() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			if(userType.equalsIgnoreCase("AGENT"))
			serviceOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(sid));
			else
		    serviceOrder = serviceOrderManager.get(Long.parseLong(sid));
			customerFile = serviceOrder.getCustomerFile();
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			transDocJobType = serviceOrder.getJob();
			if(flagUloadInv==null || flagUloadInv.trim().equals(""))
			{
			myFiles = myFileManager.getAccountFiles(serviceOrder.getShipNumber(), serviceOrder.getCorpID());
			}
			else
			{
			myFiles = myFileManager.getdocAgentTypeMapList(serviceOrder.getShipNumber(),vendorCode,serviceOrder.getCorpID());
			}
			if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("accountFiles"))){
				resultType="errorNoFile";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String  accountPortalFiles() {
		try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User)auth.getPrincipal();

			Map filterMap=user.getFilterMap(); 
			StringBuffer allFilterValues1=new StringBuffer();
			String securityQuery="";
			if(filterMap.containsKey("serviceOrderBillToCodeFilter")){ 
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							//String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					} 
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
	    			allFilterValues1=allFilterValues1.append("'");
	    			allFilterValues1=allFilterValues1.append((String)(code));
	    			allFilterValues1=allFilterValues1.append("',"); 
	    		}
	    	} 
	    	}
			
			securityQuery=allFilterValues1.toString();
			securityQuery = securityQuery.substring(0, securityQuery.length() - 1);
			System.out.println("\n\n\n\n"+securityQuery);
			myFiles = myFileManager.accountPortalFiles(securityQuery, sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String docAccessControl() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			docAccessControlList = myFileManager.getDocAccessControl(fileType, sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String listRaleventDocs() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (myFileFrom.equals("CF")) {
				customerFile = customerFileManager.get(id);
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				transDocJobType = customerFile.getJob();
				myFiles = myFileManager.getListByCustomerNumber(customerFile.getSequenceNumber(), active);
			}
			if (myFileFrom.equals("SO")) {
				serviceOrder = serviceOrderManager.get(id);
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(customerFile.getSequenceNumber(), active);
			}
			if (myFileFrom.equals("PO")) {
				String partnerCode="";
				try{
					partner = partnerManager.get(id);
					partnerCode=partner.getPartnerCode();
				}catch(Exception e){
					partnerPrivate=partnerPrivateManager.get(id);
					partnerCode=partnerPrivate.getPartnerCode();
				}
				transDocJobType = "NON";
				myFiles = myFileManager.getListByCustomerNumber(partnerCode, active);
			}
			if (myFileFrom.equals("WKT")) {
				workTicket = workTicketManager.get(id);
				serviceOrder = workTicket.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("CLM")) {
				claim = claimManager.get(id);
				serviceOrder = claim.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("ACC")) {
				accountLine = accountLineManager.get(id);
				serviceOrder = accountLine.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("VEH")) {
				vehicle = vehicleManager.get(id);
				serviceOrder = vehicle.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("SP")) {
				servicePartner = servicePartnerManager.get(id);
				serviceOrder = servicePartner.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("CONT")) {
				container = containerManager.get(id);
				serviceOrder = container.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
			if (myFileFrom.equals("CART")) {
				carton = cartonManager.get(id);
				serviceOrder = carton.getServiceOrder();
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				myFiles = myFileManager.getListByCustomerNumber(serviceOrder.getShipNumber(), active);
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return SUCCESS;
	}

	@SkipValidation
	public List myFilesListFromCustomer(String secure,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if((sequenceNumber!=null && !sequenceNumber.equalsIgnoreCase("")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
				List<CustomerFile> cfList = customerFileManager.findSequenceNumber(sequenceNumber);
				if(cfList!=null && !cfList.equals("")){
			        for(CustomerFile cf:cfList){
			        	id = cf.getId();
			        	resultType="errorNoFile";
			        }
				}
			}
			customerFile = customerFileManager.get(id);
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = customerFile.getJob();
			if (myFileFor.equals("CF")) {
				List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
				if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
					Iterator shipIt =  shipNumberList.iterator();
						while(shipIt.hasNext()){
							String shipNumber = (String) shipIt.next();
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					Iterator it1 = defCheckedShipNumber.iterator();
			         while (it1.hasNext()) {
			        	 String checkAgent = (String)it1.next();
			        		if(checkAgent.equalsIgnoreCase("BA")){
								bookingAgent ="BA";
							}else if(checkAgent.equalsIgnoreCase("NA")){
								networkAgent ="NA";
							}else if(checkAgent.equalsIgnoreCase("OA")){
								originAgent ="OA";
							}else if(checkAgent.equalsIgnoreCase("SOA")){
								subOriginAgent ="SOA";
							}else if(checkAgent.equalsIgnoreCase("DA")){
								destAgent ="DA";
							}else if(checkAgent.equalsIgnoreCase("SDA")){
								subDestAgent ="SDA";
							} 
			         }
				}

				List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
				if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
				Iterator it = shipNumberList1.iterator();
			     while (it.hasNext()) {
			    	String agentFlag=(String)it.next();
			    	if(agentFlag.equalsIgnoreCase("BA")){
						bookingAgent ="BA";
					}else if(agentFlag.equalsIgnoreCase("NA")){
						networkAgent ="NA";
					}else if(agentFlag.equalsIgnoreCase("OA")){
						originAgent ="OA";
					}else if(agentFlag.equalsIgnoreCase("SOA")){
						subOriginAgent ="SOA";
					}else if(agentFlag.equalsIgnoreCase("DA")){
						destAgent ="DA";
					}else if(agentFlag.equalsIgnoreCase("SDA")){
						subDestAgent ="SDA";
					} 
			     }
			}
  }	
}
}
			myFiles = myFileManager.getListByDocId(customerFile.getSequenceNumber(), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	private String sequenceNumber;
	@SkipValidation
	public Map<String,List<MyFile>> myFilesListFromCustomerDocType(String secure,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if((sequenceNumber!=null && !sequenceNumber.equalsIgnoreCase("")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
				List<CustomerFile> cfList = customerFileManager.findSequenceNumber(sequenceNumber);
				if(cfList!=null && !cfList.equals("")){
			        for(CustomerFile cf:cfList){
			        	id = cf.getId();
			        	resultType="errorNoFile";
			        }
				}
			}
			customerFile = customerFileManager.get(id);
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = customerFile.getJob();
			if (myFileFor.equals("CF")) {
				List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
				if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
					Iterator shipIt =  shipNumberList.iterator();
						while(shipIt.hasNext()){
							String shipNumber = (String) shipIt.next();
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					Iterator it1 = defCheckedShipNumber.iterator();
			         while (it1.hasNext()) {
			        	 String checkAgent = (String)it1.next();
			        		if(checkAgent.equalsIgnoreCase("BA")){
								bookingAgent ="BA";
							}else if(checkAgent.equalsIgnoreCase("NA")){
								networkAgent ="NA";
							}else if(checkAgent.equalsIgnoreCase("OA")){
								originAgent ="OA";
							}else if(checkAgent.equalsIgnoreCase("SOA")){
								subOriginAgent ="SOA";
							}else if(checkAgent.equalsIgnoreCase("DA")){
								destAgent ="DA";
							}else if(checkAgent.equalsIgnoreCase("SDA")){
								subDestAgent ="SDA";
							} 
			         }
				}

				List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
				if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
				Iterator it = shipNumberList1.iterator();
			     while (it.hasNext()) {
			    	String agentFlag=(String)it.next();
			    	if(agentFlag.equalsIgnoreCase("BA")){
						bookingAgent ="BA";
					}else if(agentFlag.equalsIgnoreCase("NA")){
						networkAgent ="NA";
					}else if(agentFlag.equalsIgnoreCase("OA")){
						originAgent ="OA";
					}else if(agentFlag.equalsIgnoreCase("SOA")){
						subOriginAgent ="SOA";
					}else if(agentFlag.equalsIgnoreCase("DA")){
						destAgent ="DA";
					}else if(agentFlag.equalsIgnoreCase("SDA")){
						subDestAgent ="SDA";
					} 
			     }
			}
  }	
}
}
Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(customerFile.getSequenceNumber(), active, secure,categorySearchValue,tempSortOrder,sort);
return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public String getWasteBasketAllList() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFiles = myFileManager.getWasteBasketAll(sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String revocerWasteBasketAllList() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			String key = "Document has been recovered.";
			myFileManager.recoverDoc(Long.parseLong(did), getRequest().getRemoteUser());
			searchWasteFiles();
			saveMessage(key);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public Map<String,List<MyFile>> myFilesListFromServiceOrderDescDocType(String secure,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if((shipNumber!=null && !shipNumber.equalsIgnoreCase("")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFileDocType"))){
			List tempIdList=serviceOrderManager.findIdByShipNumber(shipNumber);
				if(tempIdList!=null && !(tempIdList.isEmpty()) && tempIdList.get(0)!=null){
					id=Long.parseLong(tempIdList.get(0).toString());
				}
			}
			if(userType.equalsIgnoreCase("AGENT")){
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			miscellaneous = miscellaneousManager.getForOtherCorpid(id);
			billing = billingManager.getForOtherCorpid(id);
		    }else{
		    	serviceOrder = serviceOrderManager.get(id);
				miscellaneous = miscellaneousManager.get(id);
				billing = billingManager.get(id);	
		    }
			customerFile = serviceOrder.getCustomerFile();
			
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			trackingStatus = trackingStatusManager.get(id);
			transDocJobType = serviceOrder.getJob();
			if (myFileFor.equals("SO")) {
			/*	List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					String checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						bookingAgentFlag ="BA";
					}else if(checkAgent.equalsIgnoreCase("NA")){
						networkAgentFlag ="NA";
					}else if(checkAgent.equalsIgnoreCase("OA")){
						originAgentFlag ="OA";
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						subOriginAgentFlag ="SOA";
					}else if(checkAgent.equalsIgnoreCase("DA")){
						destAgentFlag ="DA";
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						subDestAgentFlag ="SDA";
					} 
				}*/
				List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
				if(shipNumberList!=null && !shipNumberList.isEmpty()){
				Iterator it = shipNumberList.iterator();
			     while (it.hasNext()) {
			    	String agentFlag=(String)it.next();
			    	if(agentFlag.equalsIgnoreCase("BA")){
						bookingAgent ="BA";
					}else if(agentFlag.equalsIgnoreCase("NA")){
						networkAgent ="NA";
					}else if(agentFlag.equalsIgnoreCase("OA")){
						originAgent ="OA";
					}else if(agentFlag.equalsIgnoreCase("SOA")){
						subOriginAgent ="SOA";
					}else if(agentFlag.equalsIgnoreCase("DA")){
						destAgent ="DA";
					}else if(agentFlag.equalsIgnoreCase("SDA")){
						subDestAgent ="SDA";
					} 
			     }
			}
  }
Map<String ,List<MyFile>> docTypeFilesTemp;
if(userType.trim().equalsIgnoreCase("DRIVER")){
	 docTypeFilesTemp = myFileManager.getDriverDocTypeMapList(serviceOrder.getShipNumber(), active, secure,categorySearchValue,tempSortOrder,sort);	
}
else{
   docTypeFilesTemp = myFileManager.getdocTypeMapList(serviceOrder.getShipNumber(), active, secure,categorySearchValue,tempSortOrder,sort);
   }
 return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	
	
	@SkipValidation
	public List myFilesListFromServiceOrder(String secure,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if((shipNumber!=null && !shipNumber.equalsIgnoreCase("")) && (myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("myFiles"))){
				List tempIdList=serviceOrderManager.findIdByShipNumber(shipNumber);
					if(tempIdList!=null && !(tempIdList.isEmpty()) && tempIdList.get(0)!=null){
						id=Long.parseLong(tempIdList.get(0).toString());
						resultType="errorNoFile";
					}
			}
			serviceOrder = serviceOrderManager.get(id);
			miscellaneous = miscellaneousManager.get(id);
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			trackingStatus = trackingStatusManager.get(id);
			transDocJobType = serviceOrder.getJob();
			if (myFileFor.equals("SO")) {
			/*	List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					String checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						bookingAgentFlag ="BA";
					}else if(checkAgent.equalsIgnoreCase("NA")){
						networkAgentFlag ="NA";
					}else if(checkAgent.equalsIgnoreCase("OA")){
						originAgentFlag ="OA";
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						subOriginAgentFlag ="SOA";
					}else if(checkAgent.equalsIgnoreCase("DA")){
						destAgentFlag ="DA";
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						subDestAgentFlag ="SDA";
					} 
				}*/
				List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
				if(shipNumberList!=null && !shipNumberList.isEmpty()){
				Iterator it = shipNumberList.iterator();
			     while (it.hasNext()) {
			    	String agentFlag=(String)it.next();
			    	if(agentFlag.equalsIgnoreCase("BA")){
						bookingAgent ="BA";
					}else if(agentFlag.equalsIgnoreCase("NA")){
						networkAgent ="NA";
					}else if(agentFlag.equalsIgnoreCase("OA")){
						originAgent ="OA";
					}else if(agentFlag.equalsIgnoreCase("SOA")){
						subOriginAgent ="SOA";
					}else if(agentFlag.equalsIgnoreCase("DA")){
						destAgent ="DA";
					}else if(agentFlag.equalsIgnoreCase("SDA")){
						subDestAgent ="SDA";
					} 
			     }
			}
  }
			myFiles = myFileManager.getListByDocId(serviceOrder.getShipNumber(), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public List myFilesListFromPartner(String secure,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			String partnerCode="";
			try{
				partner = partnerManager.get(id);
				partnerCode=partner.getPartnerCode();
			}catch(Exception e){
				partnerPrivate=partnerPrivateManager.get(id);
				partnerCode=partnerPrivate.getPartnerCode();
			}
			transDocJobType = "NON";
			myFiles = myFileManager.getListByDocId(partnerCode, active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromPartnerDocType(String secure,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			String partnerCode="";
			try{
				partner = partnerManager.get(id);
				partnerCode=partner.getPartnerCode();
			}catch(Exception e){
				partnerPrivate=partnerPrivateManager.get(id);
				partnerCode=partnerPrivate.getPartnerCode();
			}
			transDocJobType = "NON";
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(partnerCode, active, secure,categorySearchValue, tempSortOrder, sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	
	@SkipValidation
	public List myFilesListFromTruck(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			truck = truckManager.get(id);
			transDocJobType = "NON";
			myFiles = myFileManager.getListByDocId(truck.getLocalNumber(), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromTruckDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			truck = truckManager.get(id);
			transDocJobType = "NON";
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(truck.getLocalNumber(), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public List myFilesListFromWorkTicket(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			workTicket = workTicketManager.get(id);		
			serviceOrder = workTicket.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(workTicket.getTicket().toString(), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromWorkTicketDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			workTicket = workTicketManager.get(id);		
			serviceOrder = workTicket.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(workTicket.getTicket().toString(), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public List myFilesListFromClaim(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			claim = claimManager.get(id);
			serviceOrder = claim.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(String.valueOf(claim.getClaimNumber()), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromClaimDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			claim = claimManager.get(id);
			serviceOrder = claim.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(String.valueOf(claim.getClaimNumber()), active, secure,categorySearchValue, tempSortOrder, sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public List myFilesListFromAccounting(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			accountLine = accountLineManager.get(id);
			serviceOrder = accountLine.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(String.valueOf(accountLine.getAccountLineNumber()), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromAccountingDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			accountLine = accountLineManager.get(id);
			serviceOrder = accountLine.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(String.valueOf(accountLine.getAccountLineNumber()), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List myFilesListFromVehicle(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			vehicle = vehicleManager.get(id);
			serviceOrder = vehicle.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(serviceOrder.getShipNumber(), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public Map<String, List<MyFile>> myFilesListFromVehicleDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			vehicle = vehicleManager.get(id);
			serviceOrder = vehicle.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(serviceOrder.getShipNumber(), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List myFilesListFromContainer(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			container = containerManager.get(id);
			serviceOrder = container.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(String.valueOf(container.getIdNumber()), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	public Map<String, List<MyFile>> myFilesListFromContainerDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			container = containerManager.get(id);
			serviceOrder = container.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(String.valueOf(container.getIdNumber()), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List myFilesListFromCarton(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			carton = cartonManager.get(id);
			serviceOrder = carton.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(String.valueOf(carton.getIdNumber()), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromCartonDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			carton = cartonManager.get(id);
			serviceOrder = carton.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(String.valueOf(carton.getIdNumber()), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	public List myFilesListFromServicePartner(String secure ,String categorySearchValue) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			servicePartner = servicePartnerManager.get(id);
			serviceOrder = servicePartner.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			myFiles = myFileManager.getListByDocId(String.valueOf(servicePartner.getCarrierNumber()), active, secure,categorySearchValue);
			return myFiles;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}
	@SkipValidation
	public Map<String, List<MyFile>> myFilesListFromServicePartnerDocType(String secure ,String categorySearchValue,String tempSortOrder,String sort) {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			servicePartner = servicePartnerManager.get(id);
			serviceOrder = servicePartner.getServiceOrder();
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			transDocJobType = serviceOrder.getJob();
			 Map<String ,List<MyFile>> docTypeFilesTemp = myFileManager.getdocTypeMapList(String.valueOf(servicePartner.getCarrierNumber()), active, secure,categorySearchValue,tempSortOrder,sort);
			return docTypeFilesTemp;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
	}

	@SkipValidation
	public String save() throws Exception {
		Long t1=System.currentTimeMillis();
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		    if (myFileFor != null && myFileFor.equals("SO") && serviceOrder!=null) {
		    if(userType.equalsIgnoreCase("AGENT")){	
			serviceOrder = serviceOrderManager.getForOtherCorpid(serviceOrder.getId());
			trackingStatus = trackingStatusManager.getForOtherCorpid(serviceOrder.getId());
		    }else{
		    	serviceOrder = serviceOrderManager.get(serviceOrder.getId());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());	
		    }
			}
		   if(myFileFor != null && myFileFor.equals("CF") && customerFile !=null){
			 customerFile = customerFileManager.get(customerFile.getId());
		   }
			if (this.cancel != null) {
				return "cancel";
			}
			try{
				if(fileType!=null && !fileType.equalsIgnoreCase("")){
					docAccessControlList = myFileManager.getDocAccessControl(fileType, sessionCorpID);
					if(docAccessControlList!=null && !docAccessControlList.isEmpty() && docAccessControlList.get(0)!=null){
						String docAccessControlDetails[]=docAccessControlList.get(0).toString().split("#");
						if(docAccessControlDetails[11].equals("true")){
							documentAccessIsVendorCode=true; 
						}
						if(docAccessControlDetails[12].equals("true")){
							documentAccessIsPaymentStatus=true; 
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if (myFileManager.getListByDesc(fid).isEmpty()) {
				if(file.length()==0){
					fileNotExists = "File cannot be uploaded with size 0";
					return INPUT;
				}
				File f=null;
				f = (File) file;
				InputStream in = new FileInputStream(f);
				long bytes = in.available();
				if (bytes == 0 || bytes > 10485760) {
					fileNotExists = "Files greater than 10MB cannot be uploaded, please rescan and upload";
					return INPUT;
				}
				if (!file.exists()) {
					fileNotExists = "File cannot be uploaded as the file is invalid";
					return INPUT;

				}
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/"; 
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + sessionCorpID + "/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir); 
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				} 
				Long t9=System.currentTimeMillis();
				Long kid;
				Object  mid=myFileManager.getMaxId(sessionCorpID).get(0);
				if (mid == null) {
					kid = 1L;
				} else {
					kid = Long.parseLong(mid.toString());
					kid = kid + 1;
				}
				Long t10=System.currentTimeMillis(); 
				 fileFileName = fileFileName.replaceAll("#", " ");
				 final RandomAccessFile inFile = new RandomAccessFile(file, "r" );
				 final RandomAccessFile outFile = new RandomAccessFile(new File(uploadDir + kid + "_" + myFile.getFileId() + "_"+ fileFileName.replaceAll(" ", "_")), "rw" );
				 final FileChannel inChannel = inFile.getChannel();
		            final FileChannel outChannel = outFile.getChannel();
		            long pos = 0;
		            long toCopy = inFile.length();
		            while ( toCopy > 0 )
		            {
		                final long bytes1 = inChannel.transferTo( pos, toCopy, outChannel );
		                pos += bytes1;
		                toCopy -= bytes1;
		            }
		            outFile.close();
		            inFile.close();				
				 
				getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName.replaceAll(" ", "_")); 
				FileSizeUtil fileSizeUtil = new FileSizeUtil();  
				myFile.setFileSize(fileSizeUtil.FindFileSize(file));
				myFile.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_"+ myFile.getFileId() + "_" + fileFileName));
				myFile.setFileFileName(fileFileName.replaceAll(" ", "_"));
				myFile.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_"+ myFile.getFileId() + "_" + fileFileName.replaceAll(" ", "_"));
				String location = (dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_"+ myFile.getFileId() + "_" + fileFileName.replaceAll(" ", "_")).replace("\\","/");
				File  existsFile= new File(location); 
				
				
				
				if (existsFile.exists()) { 
				}else{
					fileNotExists = "File not be uploaded please upload again";
					return INPUT;
				}
				myFile.setFileType(fileType);
				myFile.setCorpID(sessionCorpID);
				myFile.setCreatedOn(new Date());
				myFile.setUpdatedOn(new Date());
				myFile.setActive(true);
				myFile.setMapFolder(mapFolder);
				if(myFile.getDescription()!=null && !myFile.getDescription().equals(""))
					myFile.setDescription(myFile.getDescription());
					else
						myFile.setDescription(fileType);
				List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
				if ((isSecureList != null) && (!isSecureList.isEmpty()) && (isSecureList.get(0)!=null)) {
					String isSecure = isSecureList.get(0).toString();
					if (isSecure.equalsIgnoreCase("Y")) {
						myFile.setIsSecure(true);
					} else {
						myFile.setIsSecure(false);
					}

				}
				List docList = myFileManager.getDocumentCode(fileType, sessionCorpID);
				if (docList != null && !docList.isEmpty() && docList.get(0)!=null) {
						myFile.setDocCode(docList.get(0).toString());
					} else {
						myFile.setDocCode("");
					}
				String key = "";
				if (myFile.getId() != null) {
					key = "File has been successfully  updated.";
				} else {
					key = "File has been successfully  uploaded.";
				}
				if (myFileFor != null && myFileFor.equals("SO")) {
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						myFile.setIsBookingAgent(true);
					}else if(checkAgent.equalsIgnoreCase("NA")){
						myFile.setIsNetworkAgent(true);
					}else if(checkAgent.equalsIgnoreCase("OA")){
						myFile.setIsOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						myFile.setIsSubOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("DA")){
						myFile.setIsDestAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						myFile.setIsSubDestAgent(true);
					} 				    
				  }
				}
				if(myFile.getAccountLineVendorCode()!=null && !myFile.getAccountLineVendorCode().equals("")){
					myFile.setAccountLinePayingStatus("New");
				}
				myFile = myFileManager.save(myFile);
				if (myFileFor != null && myFileFor.equals("SO") && (serviceOrder.getIsNetworkRecord()!=null && serviceOrder.getIsNetworkRecord())) {
				if(myFile.getIsBookingAgent() || myFile.getIsNetworkAgent() || myFile.getIsOriginAgent() || myFile.getIsSubOriginAgent() || myFile.getIsDestAgent() || myFile.getIsSubDestAgent()){																
				List linkedShipNumberList= new ArrayList();	
				String networkLinkId="";
				linkedShipNumberList=myFileManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
				Set set = new HashSet(linkedShipNumberList);
				List linkedSeqNum = new ArrayList(set);
				if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
					Iterator it = linkedSeqNum.iterator();
			         while (it.hasNext()) {
			        	 	 String shipNumber=(String)it.next();
				        	 String removeDatarow = shipNumber.substring(0, 6);
				        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
					        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
					     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
					             MyFile myFileObj = new MyFile();
					        	 myFileObj.setFileId(shipNumber);
					        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
					        	 myFileObj.setActive(myFile.getActive());
					        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
					        	 myFileObj.setDescription(myFile.getDescription());
					        	 //myFileObj.setFile(myFile.getFile());
					        	 myFileObj.setFileContentType(myFile.getFileContentType());
					        	 myFileObj.setFileFileName(myFile.getFileFileName());
					        	 myFileObj.setFileSize(myFile.getFileSize());
					        	 myFileObj.setFileType(myFile.getFileType()); 
					        	 myFileObj.setIsCportal(myFile.getIsCportal());
					        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
					        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
					        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
					        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment()); 
					        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
					        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
					        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
					        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
					        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
					        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
					        	 myFileObj.setIsSecure(myFile.getIsSecure());
					        	 myFileObj.setLocation(myFile.getLocation());
					        	 if(myFile.getAccountLineVendorCode()!=null && !myFile.getAccountLineVendorCode().equals("")){
					        		 myFileObj.setAccountLineVendorCode(myFile.getAccountLineVendorCode()); 
					        	 }	
					        	 if(myFile.getAccountLinePayingStatus()!=null && !myFile.getAccountLinePayingStatus().equals("")){
					        		 myFileObj.setAccountLinePayingStatus(myFile.getAccountLinePayingStatus());
					        	 }
					        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
					        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
					        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
					        			 myFileObj.setDocumentCategory(fileCategory);
					        		 }else{
					        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
					        		 }
					        	 }else{
					        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
					        	 }
					        	 myFileObj.setMapFolder(myFile.getMapFolder());
					        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
					        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
					        	 myFileObj.setCreatedOn(new Date());
					        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
					        	 myFileObj.setUpdatedOn(new Date());
					        	 myFileObj.setNetworkLinkId(networkLinkId); 
					        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
					        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
					        			 && myFile.getFileType().equals("Client Quote")){
					        		 myFileObj.setFileType("Agent Quote"); 
					        	 }
					        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  
					        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
				        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(myFileObj.getFileId()));
				        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
				        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
				        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
				        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
				        	    		myFileObj.setFileType("Invoice Vendor");	
				        	    		}
				        	    	} 
					        	 myFileManager.save(myFileObj);
				        	 }
				          }
				       }
			         }	
				}
				if (myFileFor != null && myFileFor.equals("CF") && customerFile.getIsNetworkRecord() !=null && customerFile.getIsNetworkRecord()) {				
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
								List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
								if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
									checkAgent = defCheckedShipNumber.get(0).toString();				
								if(checkAgent.equalsIgnoreCase("BA")){
									myFile.setIsBookingAgent(true);
								}else if(checkAgent.equalsIgnoreCase("NA")){
									myFile.setIsNetworkAgent(true);
								}else if(checkAgent.equalsIgnoreCase("OA")){
									myFile.setIsOriginAgent(true);
								}else if(checkAgent.equalsIgnoreCase("SOA")){
									myFile.setIsSubOriginAgent(true);
								}else if(checkAgent.equalsIgnoreCase("DA")){
									myFile.setIsDestAgent(true);
								}else if(checkAgent.equalsIgnoreCase("SDA")){
									myFile.setIsSubDestAgent(true);
								} 				    
							}
						}
					}
				}
					myFile = myFileManager.save(myFile);
					if (myFileFor != null && myFileFor.equals("CF") && customerFile.getIsNetworkRecord() !=null &&  customerFile.getIsNetworkRecord()) {
					if(myFile.getIsBookingAgent() || myFile.getIsNetworkAgent() || myFile.getIsOriginAgent() || myFile.getIsSubOriginAgent() || myFile.getIsDestAgent() || myFile.getIsSubDestAgent()){																
					
					String networkLinkId="";
					List previousSequenceNumberList = new ArrayList();
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								List linkedShipNumberList= new ArrayList();	
								String shipNumberOld = (String) shipIt.next();
					linkedShipNumberList=myFileManager.getLinkedShipNumber(shipNumberOld, "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
					Set set = new HashSet(linkedShipNumberList);
					List linkedSeqNum = new ArrayList(set);
					if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
						Iterator it = linkedSeqNum.iterator();
				         while (it.hasNext()) {
				        	 	 String shipNumber=(String)it.next();
					        	 String removeDatarow = shipNumber.substring(0, 6);
					        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
						        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
						        	 String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
						        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
						        	 previousSequenceNumberList.add(sequenceNumber);
						     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
						             MyFile myFileObj = new MyFile();
						        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
						        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
						        	 myFileObj.setActive(myFile.getActive());
						        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
						        	 myFileObj.setDescription(myFile.getDescription()); 
						        	 myFileObj.setFileContentType(myFile.getFileContentType());
						        	 myFileObj.setFileFileName(myFile.getFileFileName());
						        	 myFileObj.setFileSize(myFile.getFileSize());
						        	 myFileObj.setFileType(myFile.getFileType()); 
						        	 myFileObj.setIsCportal(myFile.getIsCportal());
						        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
						        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
						        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
						        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment()); 
						        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
						        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
						        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
						        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
						        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
						        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
						        	 myFileObj.setIsSecure(myFile.getIsSecure());
						        	 myFileObj.setLocation(myFile.getLocation());
						        	 myFileObj.setMapFolder(myFile.getMapFolder());
						        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
						        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
						        	 myFileObj.setCreatedOn(new Date());
						        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
						        	 myFileObj.setUpdatedOn(new Date()); 
						        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
						        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
						        			 && myFile.getFileType().equals("Client Quote")){
						        		 myFileObj.setFileType("Agent Quote"); 
						        	 }
						        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
					        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
					        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
					        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
					        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
					        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
					        	    		myFileObj.setFileType("Invoice Vendor");	
					        	    		}
					        	    	} 
						        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
						        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
						        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
						        			 myFileObj.setDocumentCategory(fileCategory);
						        		 }else{
						        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
						        		 }
						        	 }else{
						        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
						        	 } 
						        	 myFileObj.setNetworkLinkId(networkLinkId);
						        	 myFileManager.save(myFileObj);
						        	 }
					        	 }
					          }
					       }
				        }	
					  }
					}
				}
				saveMessage(getText(key));
				hitFlag = "1";
				if(docUpload!=null && docUpload.equalsIgnoreCase("docCentre")){ 
				}else{ 
				}
			}
			if (!myFileManager.getListByDesc(fid).isEmpty()) {
				myFile = myFileManager.get(fid);
				myFile.setFileType(fileType);
				if(description!=null && !description.equals(""))
				myFile.setDescription(description);
				else
					myFile.setDescription(fileType);
				myFile.setDocumentCategory(documentCategory);
				myFile.setIsCportal(isCportal);
				myFile.setIsAccportal(isAccportal);
				myFile.setIsPartnerPortal(isPartnerPortal);
				myFile.setIsServiceProvider(isServiceProvider);
				myFile.setInvoiceAttachment(invoiceAttachment);
				myFile.setIsBookingAgent(isBookingAgent);
				myFile.setIsNetworkAgent(isNetworkAgent);
				myFile.setIsOriginAgent(isOriginAgent);
				myFile.setIsSubOriginAgent(isSubOriginAgent);
				myFile.setIsDestAgent(isDestAgent);
				myFile.setIsSubDestAgent(isSubDestAgent);	
				myFile.setIsDriver(isDriver);
				myFile.setMapFolder(mapFolder);			
				if(accountLineVendorCode!=null && !accountLineVendorCode.equals("") && ( accountLinePayingStatus==null || accountLinePayingStatus.equals(""))){
				myFile.setAccountLinePayingStatus("New");
				}else{
				myFile.setAccountLinePayingStatus(accountLinePayingStatus);
				}
				myFile.setAccountLineVendorCode(accountLineVendorCode);
				myFile.setUpdatedOn(new Date());
				myFile.setUpdatedBy(getRequest().getRemoteUser());
				myFile.setActive(true);
				List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
				if (isSecureList != null && !isSecureList.isEmpty()) {
					String isSecure = isSecureList.get(0).toString();
					if (isSecure.equalsIgnoreCase("Y")) {
						myFile.setIsSecure(true);
					} else {
						myFile.setIsSecure(false);
					}
				}
				
				List docList = myFileManager.getDocumentCode(fileType, sessionCorpID);
				if (docList != null && !docList.isEmpty() && docList.get(0)!=null) {
						myFile.setDocCode(docList.get(0).toString());
					} else {
						myFile.setDocCode("");
					}
				
				myFile = myFileManager.save(myFile);
				String key = "";
				if (myFile.getId() != null) {
					key = "File has been successfully  updated.";
				} else {
					key = "File has been successfully  uploaded.";
				}
				if (myFileFor != null && myFileFor.equals("SO") && (serviceOrder.getIsNetworkRecord() !=null && serviceOrder.getIsNetworkRecord())) {
				String networkLinkId="";
				if(myFile.getNetworkLinkId()==null || myFile.getNetworkLinkId().equalsIgnoreCase("")){
					 networkLinkId = sessionCorpID + (myFile.getId()).toString();
				}else{
					 networkLinkId =  myFile.getNetworkLinkId();
				}
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();				    
				} 
				//------------------For checked checkbox of SO------------------------------------------------------
					if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){						
						List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
						Set set = new HashSet(linkedShipNumberList);
						List linkedSeqNum = new ArrayList(set);
						if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){		
				     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());			     		
							 Iterator it = linkedSeqNum.iterator();
					         while (it.hasNext()) {
					        	 	 String shipNumber=(String)it.next();
						        	 String removeDatarow = shipNumber.substring(0, 6);
						        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
						        		 List linkedId = myFileManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
							     		 if(linkedId ==null || linkedId.isEmpty()){
							             MyFile myFileObj = new MyFile();
							        	 myFileObj.setFileId(shipNumber);
							        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
							        	 myFileObj.setActive(myFile.getActive());
							        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
							        	 myFileObj.setDescription(myFile.getDescription()); 
							        	 myFileObj.setFileContentType(myFile.getFileContentType());
							        	 myFileObj.setFileFileName(myFile.getFileFileName());
							        	 myFileObj.setFileSize(myFile.getFileSize());
							        	 myFileObj.setFileType(myFile.getFileType()); 
							        	 myFileObj.setIsCportal(myFile.getIsCportal());
							        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
							        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
							        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
							        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment()); 
							        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
							        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
							        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
							        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
							        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
							        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
							        	 myFileObj.setIsSecure(myFile.getIsSecure());
							        	 myFileObj.setLocation(myFile.getLocation());
							        	 myFileObj.setMapFolder(myFile.getMapFolder());
							        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
							        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
							        	 myFileObj.setCreatedOn(new Date());
							        	 myFileObj.setUpdatedBy(getRequest().getRemoteUser());
							        	 myFileObj.setUpdatedOn(new Date()); 
							        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
							        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
							        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
							        			 myFileObj.setDocumentCategory(fileCategory);
							        		 }else{
							        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
							        		 }
							        	 }else{
							        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
							        	 } 
							        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) 
							        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
						        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(myFileObj.getFileId()));
						        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
						        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
						        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
						        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
						        	    		myFileObj.setFileType("Invoice Vendor");	
						        	    		}
						        	    	}
							        	 
							        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
							        			 && (trackingStatus.getOriginAgentExSO().equals(myFileObj.getFileId()) || trackingStatus.getDestinationAgentExSO().equals(myFileObj.getFileId()))
							        			 && myFile.getFileType().equals("Client Quote")){
							        		 myFileObj.setFileType("Agent Quote"); 
							        	 }
							        	 
							        	 myFileObj.setNetworkLinkId(networkLinkId);
							        	 myFileManager.save(myFileObj);
						          }
						      }
						 }
					}	
				}			
				//------------------For case of edit of SO------------------------------------------------------
				
				if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
					Long t5=System.currentTimeMillis();
				List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
				Long t6=System.currentTimeMillis(); 
				List linkedShipNumberList = null;
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA"))){
						linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
					}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 Long id = Long.parseLong(it.next().toString());
			        	 if(myFile.getId().longValue()!=id)
			        	 {
			        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
				        		 newMyFile.setIsBookingAgent(false);
					        	 newMyFile.setIsNetworkAgent(false);
					        	 newMyFile.setIsOriginAgent(false);   
					        	 newMyFile.setIsSubOriginAgent(false);
					        	 newMyFile.setIsDestAgent(false);
					        	 newMyFile.setIsSubDestAgent(false);
					        	 newMyFile.setNetworkLinkId("");
			        	 	}else{
			        		 	if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) {
				        			 newMyFile.setIsBookingAgent(false);
			        	    	}else{
			        	    		 newMyFile.setIsBookingAgent(isBookingAgent);
			        	    	}
			        	    	if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) {
						        	 newMyFile.setIsNetworkAgent(false);

			        	    	}else{
						        	 newMyFile.setIsNetworkAgent(isNetworkAgent);
			        	    	}
			        	    	if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) {
						        	 newMyFile.setIsNetworkAgent(false);
			        	    	}else{
						        	 newMyFile.setIsOriginAgent(isOriginAgent);
			        	    	}
			        	    	if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) {
						        	 newMyFile.setIsSubOriginAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubOriginAgent(isSubOriginAgent);
			        	    	}
			        	    	if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) {
						        	 newMyFile.setIsDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsDestAgent(isDestAgent);
			        	    	}
			        	    	if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")) {
						        	 newMyFile.setIsSubDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubDestAgent(isSubDestAgent);
			        	    	} 
			        	    	if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
			        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(newMyFile.getFileId()));
			        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
			        	    		if(trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null && (!(trackingStatusRemote.getContractType().toString().trim().equals("")))  && (trackingStatusRemote.getContractType().equals("CMM")|| trackingStatusRemote.getContractType().equals("DMM")) ){
			        	    			newMyFile.setDescription(myFile.getDescription());	
			        	    		} 
			        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
			        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
			        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
			        	    			  newMyFile.setFileType("Invoice Vendor");	
			        	    		}
			        	    	}
			        	    	if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
					        			 && (trackingStatus.getOriginAgentExSO().equals(newMyFile.getFileId()) || trackingStatus.getDestinationAgentExSO().equals(newMyFile.getFileId()))
					        			 && myFile.getFileType().equals("Client Quote")){
					        		 newMyFile.setFileType("Agent Quote"); 
					        	 } 		        	 
					        	 newMyFile.setIsCportal(isCportal);
					        	 newMyFile.setIsAccportal(isAccportal);
					        	 newMyFile.setIsPartnerPortal(isPartnerPortal);
					        	 newMyFile.setIsServiceProvider(isServiceProvider);
					        	 newMyFile.setInvoiceAttachment(invoiceAttachment);
			        	 	  } 
			        	 	if(!sessionCorpID.equalsIgnoreCase(newMyFile.getCorpID())){
				        		 String fileCategory = refMasterManager.findDocCategoryByDocType(newMyFile.getCorpID(), myFile.getFileType());
				        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
				        			 newMyFile.setDocumentCategory(fileCategory);
				        		 }else{
				        			 newMyFile.setDocumentCategory(myFile.getDocumentCategory());
				        		 }
				        	 }else{
				        		 newMyFile.setDocumentCategory(myFile.getDocumentCategory());
				        	 } 
			        	 	   if(newMyFile.getCorpID().equals(sessionCorpID)){
					        	 	newMyFile.setUpdatedBy(getRequest().getRemoteUser());	
					        	 }else{
					        	 	newMyFile.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
					        	 }
			        	 	newMyFile.setUpdatedOn(new Date());
			        	 myFileManager.save(newMyFile);	
			        }
				}
			      }
				 }
				}
				if (myFileFor != null && myFileFor.equals("CF") && customerFile.getIsNetworkRecord()!=null &&  customerFile.getIsNetworkRecord()) {
					String networkLinkId="";
					if(myFile.getNetworkLinkId()==null || myFile.getNetworkLinkId().equalsIgnoreCase("")){
						 networkLinkId = sessionCorpID + (myFile.getId()).toString();
					}else{
						 networkLinkId =  myFile.getNetworkLinkId();
					}
					List previousSequenceNumberList = new ArrayList();
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumberOld = (String) shipIt.next();
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumberOld);
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						checkAgent = defCheckedShipNumber.get(0).toString();				    
					}					
						if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){						
											List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
											Set set = new HashSet(linkedShipNumberList);
											List linkedSeqNum = new ArrayList(set);
											if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){		
									     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());								     		
												 Iterator it = linkedSeqNum.iterator();
										         while (it.hasNext()) {
										        	 	 String shipNumber=(String)it.next();
										        	 	String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
											        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
											        		 previousSequenceNumberList.add(sequenceNumber);
											        	 String removeDatarow = shipNumber.substring(0, 6);
											        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
											        		 List linkedId = myFileManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
												     		 if(linkedId ==null || linkedId.isEmpty()){
											        		 MyFile myFileObj = new MyFile();
												        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
												        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
												        	 myFileObj.setActive(myFile.getActive());
												        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2)); 
												        	 myFileObj.setDescription(myFile.getDescription()); 
												        	 myFileObj.setFileContentType(myFile.getFileContentType());
												        	 myFileObj.setFileFileName(myFile.getFileFileName());
												        	 myFileObj.setFileSize(myFile.getFileSize());
												        	 myFileObj.setFileType(myFile.getFileType());  
												        	 myFileObj.setIsCportal(myFile.getIsCportal());
												        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
												        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
												        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
												        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment()); 
												        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
												        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
												        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
												        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
												        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
												        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
												        	 myFileObj.setIsSecure(myFile.getIsSecure());
												        	 myFileObj.setLocation(myFile.getLocation());
												        	 myFileObj.setMapFolder(myFile.getMapFolder());
												        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
												        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
												        	 myFileObj.setCreatedOn(new Date());
												        	 myFileObj.setUpdatedBy(getRequest().getRemoteUser());
												        	 myFileObj.setUpdatedOn(new Date()); 
												        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
												        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
												        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
												        			 myFileObj.setDocumentCategory(fileCategory);
												        		 }else{
												        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
												        		 }
												        	 }else{
												        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
												        	 } 
												        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
											        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
											        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
											        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
											        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
											        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
											        	    		myFileObj.setFileType("Invoice Vendor");	
											        	    		}
											        	    	}
												        	 
												        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
												        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
												        			 && myFile.getFileType().equals("Client Quote")){
												        		 myFileObj.setFileType("Agent Quote"); 
												        	 } 
												        	 myFileObj.setNetworkLinkId(networkLinkId);
												        	 myFileManager.save(myFileObj);
											          }
											      }
										         }
											 }
										}					
									}
								}
					}
					//------------------For case of edit of CF------------------------------------------------------
					
					if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
					List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
					List linkedShipNumberList = null;
					if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
						List shipNumberList1 = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
						if (shipNumberList1!=null && !shipNumberList1.isEmpty()) {
							Iterator shipIt =  shipNumberList1.iterator();
								while(shipIt.hasNext()){
									String shipNumberOld = (String) shipIt.next();
						if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA"))){
							linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
						}
					}
						Iterator it = linkedIdList.iterator();
				         while (it.hasNext()) {
				        	 Long id = Long.parseLong(it.next().toString());
				        	 if(myFile.getId().longValue()!=id){
				        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
				        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
					        		 newMyFile.setIsBookingAgent(false);
						        	 newMyFile.setIsNetworkAgent(false);
						        	 newMyFile.setIsOriginAgent(false);   
						        	 newMyFile.setIsSubOriginAgent(false);
						        	 newMyFile.setIsDestAgent(false);
						        	 newMyFile.setIsSubDestAgent(false);
						        	 newMyFile.setNetworkLinkId("");
				        	 	}else{
				        		 	if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) {
					        			 newMyFile.setIsBookingAgent(false);
				        	    	}else{
				        	    		 newMyFile.setIsBookingAgent(isBookingAgent);
				        	    	}
				        	    	if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) {
							        	 newMyFile.setIsNetworkAgent(false);

				        	    	}else{
							        	 newMyFile.setIsNetworkAgent(isNetworkAgent);
				        	    	}
				        	    	if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) {
							        	 newMyFile.setIsNetworkAgent(false);
				        	    	}else{
							        	 newMyFile.setIsOriginAgent(isOriginAgent);
				        	    	}
				        	    	if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) {
							        	 newMyFile.setIsSubOriginAgent(false);
				        	    	}else{
							        	 newMyFile.setIsSubOriginAgent(isSubOriginAgent);
				        	    	}
				        	    	if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) {
							        	 newMyFile.setIsDestAgent(false);
				        	    	}else{
							        	 newMyFile.setIsDestAgent(isDestAgent);
				        	    	}
				        	    	if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")) {
							        	 newMyFile.setIsSubDestAgent(false);
				        	    	}else{
							        	 newMyFile.setIsSubDestAgent(isSubDestAgent);
				        	    	}	 	        
				        	    	if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
				        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(newMyFile.getFileId()));
				        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
				        	    		if(trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null && (!(trackingStatusRemote.getContractType().toString().trim().equals("")))  && (trackingStatusRemote.getContractType().equals("CMM")|| trackingStatusRemote.getContractType().equals("DMM")) ){
				        	    			newMyFile.setDescription(myFile.getDescription());	
				        	    		} 
				        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
				        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
				        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
				        	    			  newMyFile.setFileType("Invoice Vendor");	
				        	    		}
				        	    	}
				        	    	
				        	    	if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
						        			 && (trackingStatus.getOriginAgentExSO().equals(newMyFile.getFileId()) || trackingStatus.getDestinationAgentExSO().equals(newMyFile.getFileId()))
						        			 && myFile.getFileType().equals("Client Quote")){
						        		 newMyFile.setFileType("Agent Quote"); 
						        	 } 
						        	 newMyFile.setIsCportal(isCportal);
						        	 newMyFile.setIsAccportal(isAccportal);
						        	 newMyFile.setIsPartnerPortal(isPartnerPortal);
						        	 newMyFile.setIsServiceProvider(isServiceProvider);
						        	 newMyFile.setInvoiceAttachment(invoiceAttachment);
				        	 	  } 
				        	 	if(!sessionCorpID.equalsIgnoreCase(newMyFile.getCorpID())){
					        		 String fileCategory = refMasterManager.findDocCategoryByDocType(newMyFile.getCorpID(), myFile.getFileType());
					        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
					        			 newMyFile.setDocumentCategory(fileCategory);
					        		 }else{
					        			 newMyFile.setDocumentCategory(myFile.getDocumentCategory());
					        		 }
					        	 }else{
					        		 newMyFile.setDocumentCategory(myFile.getDocumentCategory());
					        	 } 
				        	 	if(newMyFile.getCorpID().equals(sessionCorpID)){
				        	 	newMyFile.setUpdatedBy(getRequest().getRemoteUser());	
				        	 	}else{
				        	 	newMyFile.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
				        	 	}
				        	 	newMyFile.setUpdatedOn(new Date());
				        	 myFileManager.save(newMyFile);	
			            }
						}
			          }
					}
				}
			}
								
				saveMessage(getText(key));
				hitFlag = "1";
				if(docUpload!=null && docUpload.equalsIgnoreCase("docCentre")){
					Long t2=System.currentTimeMillis();
					return "gotoDocCentre";
				}else{
					Long t2=System.currentTimeMillis();
					return "editDescription";
				}
				
			}
			if(docUpload!=null && docUpload.equalsIgnoreCase("docCentre")){
				Long t2=System.currentTimeMillis();
				return "gotoDocCentre";
			}else{
				Long t2=System.currentTimeMillis();
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	}
	public String editcompressFileUpload() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (id != null) {
				myFile = myFileManager.getForOtherCorpid(id);
				fileType = myFile.getFileType();
				fileName = myFile.getFileFileName();
				mapFolder = myFile.getMapFolder();
				fileId = myFile.getFileId();
				myFile.setDescription(myFile.getDescription());
			} else {
				myFile = new MyFile();
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		
		return SUCCESS;
	}
	@SkipValidation
	public String compressFileUpload() throws Exception {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			docsListAccountPortal=refMasterManager.findDocumentListAccountPortal(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (this.cancel != null) {
				return "cancel";
			} 
			if (!myFileManager.getcompressListId(fid).isEmpty()) {
				myFile=myFileManager.getForOtherCorpid(fid);
				File f=null;
				f = (File) file;
				InputStream in = new FileInputStream(f);
				long bytes = in.available();
				if (bytes == 0 || bytes > 20000000) {
					fileNotExists = "Files greater than 2MB cannot be uploaded, please rescan and upload";
					return INPUT;
				}
				if (!file.exists()) {
					fileNotExists = "File cannot be uploaded as the file is invalid";
					return INPUT;

				}
				String corpid=sessionCorpID;
				if(myFile.getCorpID()!=null && (!(myFile.getCorpID().toString().equals("")))){
					corpid=myFile.getCorpID();
				}
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + corpid + "/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir); 
				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				InputStream stream = new FileInputStream(file); 
				OutputStream bos = new FileOutputStream(uploadDir +fid+"_"+ myFile.getFileFileName());
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				} 
				bos.close();
				stream.close(); 
				getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP +fid+"_"+ myFile.getFileFileName());
				FileSizeUtil fileSizeUtil = new FileSizeUtil();
				myFile.setFileSize(fileSizeUtil.FindFileSize(file));
				myFile.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP +myFile.getFileFileName()));
				myFile.setFileType(fileType);
				if(myFile.getCorpID()==null || myFile.getCorpID().toString().equals("")){
				myFile.setCorpID(sessionCorpID);
				}
				myFile.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + fid + "_" + myFile.getFileFileName());
				myFile.setUpdatedOn(new Date());
				myFile.setUpdatedBy(getRequest().getRemoteUser());
				myFile.setActive(true);
				myFile.setMapFolder(mapFolder);
				myFileManager.save(myFile);
			}
			hitflag = "1";
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;

	}	
	@SkipValidation
	public String removeNetworkLinkId(){
		try {
			if (!myFileManager.getListByDesc(fid).isEmpty()) 
			{
				myFile = myFileManager.get(fid);
				if(myFile.getNetworkLinkId()!=null && !myFile.getNetworkLinkId().equalsIgnoreCase("")){
					myFileManager.removeNetworkLinkId(myFile.getNetworkLinkId(),myFile.getFileId(),sessionCorpID);
				}			
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	private String soCorpId=""; 
	@SkipValidation
	public String uploadAgentsFile() throws Exception {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		if(flagUloadInv !=null && (flagUloadInv.trim().equalsIgnoreCase("true")))
		{ 
			fileTypePayableProcessing = myFileManager.fileTypeForPayableProcessing(sessionCorpID);
			Set set1=docsList.entrySet();
	    	for(Object obj:set1)
	    	{
	    		Map.Entry<String, String> entry=(Map.Entry<String, String>)obj; 
	    		Iterator It =fileTypePayableProcessing.iterator();
	    		while(It.hasNext()){
	    			String fileType=It.next().toString();
	    	    if(entry.getKey().equalsIgnoreCase(fileType))
	    		{
	    			docsListAccountPortal.put(entry.getKey(), entry.getValue());
	    		}
	    		else
	    		{
	    			
	    		}
	    		}
	    	}}
		else
		{
			docsListAccountPortal=refMasterManager.findDocumentListAccountPortal(sessionCorpID, "DOCUMENT");
	
		}
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (this.cancel != null) {
				return "cancel";
			} 
			if (myFileManager.getListByDesc(fid).isEmpty()) {
				if(file.length()==0){
					fileNotExists = "File cannot be uploaded with size 0";
					return INPUT;
				}
				File f=null;
				f = (File) file;
				InputStream in = new FileInputStream(f);
				long bytes = in.available();
				
				if(enterpriseLicense){
					if (bytes == 0 || bytes > 10485760) {
						fileNotExists = "Files greater than 10MB cannot be uploaded, please rescan and upload";
						return INPUT;
					}	
				}else{
				if (bytes == 0 || bytes > 5242880) {
					fileNotExists = "Files greater than 5MB cannot be uploaded, please rescan and upload";
					return INPUT;
				}
				}
				if (!file.exists()) {
					fileNotExists = "File cannot be uploaded as the file is invalid";
					return INPUT;

				}
				String corpid=sessionCorpID;
				if(myFile.getCorpID()!=null && (!(myFile.getCorpID().toString().equals("")))){
					corpid=myFile.getCorpID();
				}
				String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";

				//String copyUploadDir = uploadDir.replace("redsky", "cportal");
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + corpid + "/" + getRequest().getRemoteUser() + "/");
				File dirPath = new File(uploadDir);
				//File copyDirPath = new File(copyUploadDir);

				if (!dirPath.exists()) {
					dirPath.mkdirs();
				}
				/*if (!copyDirPath.exists()) {
					copyDirPath.mkdirs();
				}*/
				Long kid;
				Object mid=myFileManager.getMaxId(sessionCorpID).get(0);
				if ( mid== null) {
					kid = 1L;
				} else {
					kid = Long.parseLong(mid.toString());
					kid = kid + 1;
				}
				InputStream stream = new FileInputStream(file);

				OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName.replaceAll(" ", "_"));
				int bytesRead = 0;
				byte[] buffer = new byte[8192];

				while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
					bos.write(buffer, 0, bytesRead);
				}

				bos.close();
				stream.close();

				/*InputStream copyStream = new FileInputStream(file);

				OutputStream boscopy = new FileOutputStream(copyUploadDir + fileFileName);
				int bytesReadCopy = 0;
				byte[] bufferCopy = new byte[8192];

				while ((bytesReadCopy = copyStream.read(bufferCopy, 0, 8192)) != -1) {
					boscopy.write(bufferCopy, 0, bytesReadCopy);
				}

				boscopy.close();
				copyStream.close();*/

				getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName.replaceAll(" ", "_"));

				//myFile.setFile(file);

				FileSizeUtil fileSizeUtil = new FileSizeUtil();
				myFile.setFileSize(fileSizeUtil.FindFileSize(file));

				myFile.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName.replaceAll(" ", "_")));
				myFile.setFileFileName(fileFileName.replaceAll(" ", "_"));
				myFile.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName.replaceAll(" ", "_"));
				myFile.setFileType(fileType);
				if(myFile.getCorpID()==null || myFile.getCorpID().toString().equals("")){
				myFile.setCorpID(sessionCorpID);
				}
				myFile.setCreatedOn(new Date());
				myFile.setUpdatedOn(new Date());
				if ((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))) {
					myFile.setIsPartnerPortal(true);
				}
				if ((userType.trim().equalsIgnoreCase("ACCOUNT"))) {
					myFile.setIsAccportal(true);
				}
				myFile.setActive(true);

				List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
				if (isSecureList != null) {
					String isSecure = isSecureList.get(0).toString();
					if (isSecure.equalsIgnoreCase("Y")) {
						myFile.setIsSecure(true);
					} else {
						myFile.setIsSecure(false);
					}

				}
				if ((userType.trim().equalsIgnoreCase("ACCOUNT"))) {
					if (myFileFor.equals("SO")) {
						serviceOrder = serviceOrderManager.get(Long.parseLong(fileId));
						if(serviceOrder.getIsNetworkRecord()){
						  trackingStatus = trackingStatusManager.get(Long.parseLong(fileId));
					      if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
					    	  List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
								if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
									checkAgent = defCheckedShipNumber.get(0).toString();
									if(checkAgent.equalsIgnoreCase("BA")){ 
										myFile.setIsBookingAgent(true);
									}else if(checkAgent.equalsIgnoreCase("NA")){ 
										myFile.setIsNetworkAgent(true);
									}else if(checkAgent.equalsIgnoreCase("OA")){ 
										myFile.setIsOriginAgent(true);
									}else if(checkAgent.equalsIgnoreCase("SOA")){ 
										myFile.setIsSubOriginAgent(true);
									}else if(checkAgent.equalsIgnoreCase("DA")){ 
										myFile.setIsDestAgent(true);
									}else if(checkAgent.equalsIgnoreCase("SDA")){ 
										myFile.setIsSubDestAgent(true);
									} 				    
								}   
					      }
						}
					}
				}
				  if((fileType!=null)&&(!fileType.trim().toString().equalsIgnoreCase(""))){
				        List docControl = myFileManager.getDocAccessControl(fileType, corpid);
				        if((docControl!=null)&&(!docControl.isEmpty())&&(docControl.get(0)!=null)&&(!docControl.get(0).toString().trim().equalsIgnoreCase(""))){
				        	String st=docControl.get(0).toString().trim();
				        	if((st.split("#")[10]!=null)&&(st.split("#")[10].toString().equalsIgnoreCase("true"))){
				        		myFile.setInvoiceAttachment(true);
				        	}
				        	
				        	if((st.split("#")[0]!=null)&&(st.split("#")[0].toString().equalsIgnoreCase("true"))){
				        		myFile.setIsCportal(true);
				        	}
				        	if((st.split("#")[1]!=null)&&(st.split("#")[1].toString().equalsIgnoreCase("true"))){
				        		myFile.setIsAccportal(true);
				        	}
				        	if((st.split("#")[2]!=null)&&(st.split("#")[2].toString().equalsIgnoreCase("true"))){
				        		myFile.setIsPartnerPortal(true);
				        	}
				        	if((st.split("#")[3]!=null)&&(st.split("#")[3].toString().equalsIgnoreCase("true"))){
				        		isBookingAgent=true;
				        	}else{
				        		myFile.setIsBookingAgent(false);
				        	}
				        	if((st.split("#")[4]!=null)&&(st.split("#")[4].toString().equalsIgnoreCase("true"))){
				        		isNetworkAgent=true;
				        	}else{
				        		myFile.setIsNetworkAgent(false);
				        	}
				        	if((st.split("#")[5]!=null)&&(st.split("#")[5].toString().equalsIgnoreCase("true"))){
				        		isOriginAgent=true;
				        	}else{
				        		myFile.setIsOriginAgent(false);
				        	}
				        	if((st.split("#")[6]!=null)&&(st.split("#")[6].toString().equalsIgnoreCase("true"))){
				        		isSubOriginAgent=true;
				        	}else{
				        		myFile.setIsSubOriginAgent(false);
				        	}
				        	if((st.split("#")[7]!=null)&&(st.split("#")[7].toString().equalsIgnoreCase("true"))){
				        		isDestAgent=true;
				        	}else{
				        		myFile.setIsDestAgent(false);
				        	}
				        	if((st.split("#")[8]!=null)&&(st.split("#")[8].toString().equalsIgnoreCase("true"))){
				        		isSubDestAgent=true;
				        	}else{
				        		myFile.setIsSubDestAgent(false);
				        	}
				        	if((st.split("#")[9]!=null)&&(st.split("#")[9].toString().equalsIgnoreCase("true"))){
				        		myFile.setIsServiceProvider(true);
				        	}
				        }else{
				        	myFile.setIsBookingAgent(false);
				        	myFile.setIsNetworkAgent(false);
				        	myFile.setIsOriginAgent(false);
				        	myFile.setIsSubOriginAgent(false);
				        	myFile.setIsDestAgent(false);
				        	myFile.setIsSubDestAgent(false);
				        }
				    }
				myFile=myFileManager.save(myFile);
				try{
				if ((userType.trim().equalsIgnoreCase("ACCOUNT"))) {
					String networkLinkId="";
					if (myFileFor.equals("SO")) {
						serviceOrder = serviceOrderManager.get(Long.parseLong(fileId));
						if(serviceOrder.getIsNetworkRecord()){
					    trackingStatus = trackingStatusManager.get(Long.parseLong(fileId));
			          if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
			        	  List linkedShipNumber=findLinkerShipNumber(serviceOrder);
			    		  List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
			    		  synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus,myFile);
			    		  try{
			    			  Iterator  it=serviceOrderRecords.iterator();
			    		    	while(it.hasNext()){
			    		    		ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();  
			    		    		if((!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))) && (!(serviceOrder.getCorpID().equals(serviceOrderToRecods.getCorpID())))){
			    		    			List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrderToRecods.getShipNumber());
										if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
											checkAgent = defCheckedShipNumber.get(0).toString();
											if(checkAgent.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){ 
												myFile.setIsBookingAgent(true);
											}else if(checkAgent.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){ 
												myFile.setIsNetworkAgent(true);
											}else if(checkAgent.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){ 
												myFile.setIsOriginAgent(true);
											}else if(checkAgent.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){ 
												myFile.setIsSubOriginAgent(true);
											}else if(checkAgent.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){ 
												myFile.setIsDestAgent(true);
											}else if(checkAgent.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){ 
												myFile.setIsSubDestAgent(true);
											}
											
										}
			    		    		}
			    		    	}
			    		    	networkLinkId = sessionCorpID + (myFile.getId()).toString();
			    		    	myFile.setNetworkLinkId(networkLinkId);
							    myFile=myFileManager.save(myFile);
			    		  }catch(Exception e){
			    			  
			    		  }
					}
					}
				}	
				}
				}catch(Exception e ){ }
				
				if ((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))) {
					serviceOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(fileId));
					List shipNumberListSo = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
					if(shipNumberListSo!=null && !shipNumberListSo.isEmpty()){
					Iterator it = shipNumberListSo.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA") && isBookingAgent!=null && isBookingAgent==true){
			        		myFile.setIsBookingAgent(true);
						}else if(agentFlag.equalsIgnoreCase("NA") && isNetworkAgent!=null && isNetworkAgent==true){
							myFile.setIsNetworkAgent(true);
						}else if(agentFlag.equalsIgnoreCase("OA") && isOriginAgent!=null && isOriginAgent==true){
							myFile.setIsOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SOA") && isSubOriginAgent!=null && isSubOriginAgent==true){
							myFile.setIsSubOriginAgent(true);
						}else if(agentFlag.equalsIgnoreCase("DA") && isDestAgent!=null && isDestAgent==true){
							myFile.setIsDestAgent(true);
						}else if(agentFlag.equalsIgnoreCase("SDA") && isSubDestAgent!=null && isSubDestAgent==true){
							myFile.setIsSubDestAgent(true);
						} 
			         }
				}
					myFile=myFileManager.save(myFile);
					
				if(myFile.getIsBookingAgent() || myFile.getIsNetworkAgent() || myFile.getIsOriginAgent() 
			    		|| myFile.getIsSubOriginAgent() || myFile.getIsDestAgent() || myFile.getIsSubDestAgent()){																
					List linkedShipNumberList= new ArrayList();	
					String networkLinkId="";
					linkedShipNumberList=myFileManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary", myFile.getIsBookingAgent(), myFile.getIsNetworkAgent(), myFile.getIsOriginAgent(), myFile.getIsSubOriginAgent(), myFile.getIsDestAgent(), myFile.getIsSubDestAgent());
					Set set = new HashSet(linkedShipNumberList);
					List linkedSeqNum = new ArrayList(set);
					if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
						Iterator it = linkedSeqNum.iterator();
				         while (it.hasNext()) {
				        	 	 String shipNumber=(String)it.next();
					        	 String removeDatarow = shipNumber.substring(0, 6);
					        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
						        	 networkLinkId = sessionCorpID + (myFile.getId()).toString();
						     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
						             MyFile myFileObj = new MyFile();
						        	 myFileObj.setFileId(shipNumber);
						        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
						        	 myFileObj.setActive(myFile.getActive());
						        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
						        	 myFileObj.setDescription(myFile.getDescription());
						        	 //myFileObj.setFile(myFile.getFile());
						        	 myFileObj.setFileContentType(myFile.getFileContentType());
						        	 myFileObj.setFileFileName(myFile.getFileFileName());
						        	 myFileObj.setFileSize(myFile.getFileSize());
						        	 myFileObj.setFileType(myFile.getFileType());
						        	 
						        	 myFileObj.setIsCportal(myFile.getIsCportal());
						        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
						        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
						        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
						        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
						        	 
						        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
						        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
						        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
						        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
						        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
						        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
						        	 myFileObj.setIsSecure(myFile.getIsSecure());
						        	 myFileObj.setLocation(myFile.getLocation());
						        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
						        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
						        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
						        			 myFileObj.setDocumentCategory(fileCategory);
						        		 }else{
						        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
						        		 }
						        	 }else{
						        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
						        	 }
						        	 myFileObj.setMapFolder(myFile.getMapFolder());
						        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
						        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
						        	 myFileObj.setCreatedOn(new Date());
						        	 myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
						        	 myFileObj.setUpdatedOn(new Date());
						        	 myFileObj.setNetworkLinkId(networkLinkId);
						        	 myFileManager.save(myFileObj);
					        	 }
					          }
					       }
				         }
				}
				hitflag = "1";
				try {
					if(userType.equalsIgnoreCase("AGENT")){	
					serviceOrder = serviceOrderManager.getForOtherCorpid(Long.parseLong(fileId));
					billing = billingManager.getForOtherCorpid(Long.parseLong(fileId));
					}else{
						serviceOrder = serviceOrderManager.get(Long.parseLong(fileId));
						billing = billingManager.get(Long.parseLong(fileId));	
					}
					String serviceOrderNo=serviceOrder.getShipNumber();
					String url=getRequest().getRequestURL().toString();
					String  requestURL=url.substring(0,url.lastIndexOf("/"));
					String emailAdd = "";
					String website="";
					String msgText1="";
					String subject="";
					 String link = "";
					 if(userType.equalsIgnoreCase("PARTNER")){
						msgText1="<br>File Uploaded from Partner Portal '" + fileFileName + "'"; 
					 }else if(userType.equalsIgnoreCase("ACCOUNT")){
						msgText1="<br>File Uploaded from Account Portal '" + fileFileName + "'"; 
					 }else if(userType.equalsIgnoreCase("AGENT")){
						 msgText1="<br>File Uploaded from Agent Portal '" + fileFileName + "'"; 
					 }
					 
					List coordEmail = customerFileManager.findCoordEmailAddress(serviceOrder.getCoordinator());
					if (!(coordEmail == null) && (!coordEmail.isEmpty())) {
						emailAdd = coordEmail.get(0).toString().trim();					
					}
					if (fileType.equalsIgnoreCase("Invoice Client") || fileType.equalsIgnoreCase("Invoice Vendor")) {
						List payableEmail = customerFileManager.findCoordEmailAddress(billing.getPersonPayable());
						if (!(payableEmail == null) && (!payableEmail.isEmpty())) {
							emailAdd = payableEmail.get(0).toString().trim();
						}
						website=requestURL;
						String permKey = sessionCorpID +"-"+"module.tab.serviceorder.newAccountingTab";
				        visibilityFornewAccountline=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
				        if(visibilityFornewAccountline){
				        website=website+"/pricingList.html?sid="+serviceOrder.getId()+"";
				        }else{
						website=website+"/accountLineList.html?sid="+serviceOrder.getId()+"";
				        }
						link = "<a href=\""+website+"\">"+serviceOrderNo+"</a>";
						msgText1 = msgText1+"<br><br>Please find the uploaded document on "+link+"";
					}else{
						website=requestURL;
						website=website+"/editTrackingStatus.html?id="+serviceOrder.getId()+"";
						 link = "<a href=\""+website+"\">"+serviceOrderNo+"</a>";
						msgText1 = msgText1+"<br><br>Please find the uploaded document on "+link+"";
					}
					String messageText1 = "<html><table>"+msgText1+"</table></html>";
					String from = "support@redskymobility.com";
					String tempRecipient="";
					String tempRecipientArr[]=emailAdd.split(",");
					for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
					emailAdd=tempRecipient;
					subject="File Upload by '" + getRequest().getRemoteUser() + "' " + serviceOrder.getShipNumber() + "---" + serviceOrder.getLastName();
					emailSetupManager.globalEmailSetupProcess(from, emailAdd, "", "", "", messageText1, subject, sessionCorpID,"" ,serviceOrder.getShipNumber(),"");
				}catch(Exception ex){
					String key = "File has been uploaded but Email Sending Failed";				
					errorMessage(getText(key));				
					hitflag = "2";
					return INPUT;
				} 
				
			}
			if (!myFileManager.getListByDesc(fid).isEmpty()) {
				if(userType.equalsIgnoreCase("AGENT")){	
				myFile = myFileManager.getForOtherCorpid(fid);
				}else{
				myFile = myFileManager.get(fid);	
				}
				myFile.setFileType(fileType);
				myFile.setDescription(description);
				myFile.setIsCportal(isCportal);
				if ((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))) {
					myFile.setIsPartnerPortal(true);
				}
				if ((userType.trim().equalsIgnoreCase("ACCOUNT"))) {
					myFile.setIsAccportal(true);
				}
				myFile.setUpdatedOn(new Date());
				myFile.setActive(true);
				myFile.setMapFolder(mapFolder);

				List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
				if (isSecureList != null) {
					String isSecure = isSecureList.get(0).toString();
					if (isSecure.equalsIgnoreCase("Y")) {
						myFile.setIsSecure(true);
					} else {
						myFile.setIsSecure(false);
					}

				}

				myFile=myFileManager.save(myFile);
				hitflag = "1";
				rowId=myFile.getId().toString()+":"+myFile.getDescription();
				return "editDescription";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		rowId=myFile.getId().toString()+":"+myFile.getDescription();
		return SUCCESS;

	}
	
	@SkipValidation
    private List findLinkerShipNumber(ServiceOrder serviceOrder) { 
    	try {
			List linkedShipNumberList= new ArrayList();
			if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
				linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
			}else{
				linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
			} 
			return linkedShipNumberList;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
    }
	
	@SkipValidation
    private List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) { 
		try {
			List<Object> recordList= new ArrayList();
			Iterator it =linkedShipNumber.iterator();
			while(it.hasNext()){
				String shipNumber= it.next().toString();
				ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
				recordList.add(serviceOrderRemote);
			} 
			return recordList;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
		return null;
    	}
	
	@SkipValidation
    private void synchornizeServiceOrder(List<Object> records, ServiceOrder serviceOrder,TrackingStatus trackingStatus, MyFile myFile) { 
		 
    	try {
			Iterator  it=records.iterator();
			String networkLinkId="";
			while(it.hasNext()){
				ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();
				String soJobType = serviceOrderToRecods.getJob();
				if((!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))) && (!(serviceOrder.getCorpID().equals(serviceOrderToRecods.getCorpID())))){
				TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
				if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) ){
				if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
				 try{
					  MyFile myFile1=new MyFile();
					  myFile1.setFileId(serviceOrderToRecods.getShipNumber());
					  myFile1.setCustomerNumber(serviceOrderToRecods.getSequenceNumber());
					  myFile1.setCreatedOn(new Date());
					  myFile1.setUpdatedOn(new Date());
					  myFile1.setUpdatedBy(getRequest().getRemoteUser());
					  myFile1.setCreatedBy(getRequest().getRemoteUser());
					  myFile1.setCorpID(serviceOrderToRecods.getCorpID());
					  myFile1.setDescription(myFile.getDescription());
					  String corpid=serviceOrderToRecods.getCorpID();
					  String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources") + "/" + getRequest().getRemoteUser() + "/";
			          uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "redskydoc" + "/" + corpid + "/" + getRequest().getRemoteUser() + "/");
						File dirPath = new File(uploadDir);
						if (!dirPath.exists()) {
							dirPath.mkdirs();
						} 
						Long kid;
						Object mid=myFileManager.getMaxId(sessionCorpID).get(0);
						if ( mid== null) {
							kid = 1L;
						} else {
							kid = Long.parseLong(mid.toString());
							kid = kid + 1;
						}
						InputStream stream = new FileInputStream(file); 
						OutputStream bos = new FileOutputStream(uploadDir + kid + "_" + fileFileName.replaceAll(" ", "_"));
						int bytesRead = 0;
						byte[] buffer = new byte[8192]; 
						while ((bytesRead = stream.read(buffer, 0, 8192)) != -1) {
							bos.write(buffer, 0, bytesRead);
						}

						bos.close();
						stream.close(); 

						getRequest().setAttribute("location", dirPath.getAbsolutePath() + Constants.FILE_SEP + fileFileName.replaceAll(" ", "_"));

						//myFile1.setFile(file);

						FileSizeUtil fileSizeUtil = new FileSizeUtil();
						myFile1.setFileSize(fileSizeUtil.FindFileSize(file));

						myFile1.setFileContentType(mimetypesFileTypeMap.getContentType(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName.replaceAll(" ", "_")));
						myFile1.setFileFileName(fileFileName.replaceAll(" ", "_"));
						myFile1.setLocation(dirPath.getAbsolutePath() + Constants.FILE_SEP + kid + "_" + fileFileName.replaceAll(" ", "_"));
						myFile1.setFileType(fileType);
						if(myFile1.getCorpID()==null || myFile1.getCorpID().toString().equals("")){
						myFile1.setCorpID(sessionCorpID);
						}
						myFile1.setCreatedOn(new Date());
						myFile1.setUpdatedOn(new Date());
						if ((userType.trim().equalsIgnoreCase("AGENT"))||(userType.trim().equalsIgnoreCase("PARTNER"))) {
							myFile1.setIsPartnerPortal(true);
						}
						if ((userType.trim().equalsIgnoreCase("ACCOUNT"))) {
							myFile1.setIsAccportal(true);
						}
						myFile1.setActive(true); 
						List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
						if (isSecureList != null) {
							String isSecure = isSecureList.get(0).toString();
							if (isSecure.equalsIgnoreCase("Y")) {
								myFile1.setIsSecure(true);
							} else {
								myFile1.setIsSecure(false);
							}

						}
						myFile1.setDocumentCategory(refMasterManager.findDocCategoryByDocType(serviceOrderToRecods.getCorpID(), fileType));
						  List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrderToRecods.getShipNumber());
							if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
								checkAgent = defCheckedShipNumber.get(0).toString();
								if(checkAgent.equalsIgnoreCase("BA")){ 
									myFile1.setIsBookingAgent(true);
								}else if(checkAgent.equalsIgnoreCase("NA")){ 
									myFile1.setIsNetworkAgent(true);
								}else if(checkAgent.equalsIgnoreCase("OA")){ 
									myFile1.setIsOriginAgent(true);
								}else if(checkAgent.equalsIgnoreCase("SOA")){ 
									myFile1.setIsSubOriginAgent(true);
								}else if(checkAgent.equalsIgnoreCase("DA")){ 
									myFile1.setIsDestAgent(true);
								}else if(checkAgent.equalsIgnoreCase("SDA")){ 
									myFile1.setIsSubDestAgent(true);
								} 				    
							}
							 List defCheckedShipNumber1 = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
								if(defCheckedShipNumber1!=null && !defCheckedShipNumber1.isEmpty()){
									checkAgent = defCheckedShipNumber1.get(0).toString();
									if(checkAgent.equalsIgnoreCase("BA")){ 
										myFile1.setIsBookingAgent(true);
									}else if(checkAgent.equalsIgnoreCase("NA")){ 
										myFile1.setIsNetworkAgent(true);
									}else if(checkAgent.equalsIgnoreCase("OA")){ 
										myFile1.setIsOriginAgent(true);
									}else if(checkAgent.equalsIgnoreCase("SOA")){ 
										myFile1.setIsSubOriginAgent(true);
									}else if(checkAgent.equalsIgnoreCase("DA")){ 
										myFile1.setIsDestAgent(true);
									}else if(checkAgent.equalsIgnoreCase("SDA")){ 
										myFile1.setIsSubDestAgent(true);
									} 				    
								}
					    networkLinkId = sessionCorpID + (myFile.getId()).toString();
					    myFile1.setNetworkLinkId(networkLinkId);
						myFile1=myFileManager.save(myFile1);
					 
					 }catch( Exception e){
				     
						 e.printStackTrace();
					 }
					
			    }
			     
			}
			}	 
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}	
    	}

	@SkipValidation
	public String updateMyfileStatus() {
		try {
			myFileManager.updateMyfileStatus(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateIncAttachment() {
		try {
			myFileManager.updateInvoiceAttachment(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String updateMyfileAccStatus() {
		try {
			myFileManager.updateMyfileAccStatus(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String updateMyfilePartnerStatus() {
		try {
			myFileManager.updateMyfilePartnerStatus(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateMyfileServiceProviderStatus() {
		try {
			myFileManager.updateMyfileServiceProviderStatus(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateMyfileDiverStatus() {
		try {
			myFileManager.updateMyfileDriverPortalStatus(Status, ids, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	private Boolean documentAccessIsVendorCode=false;
	private Boolean documentAccessIsPaymentStatus=false;
	private String accountLineVendorCode;
	private String accountLinePayingStatus;
	public String editFileUpload() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (!myFileFor.equals("CF") && !myFileFor.equals("PO") && !myFileFor.equals("Truck")) {
				serviceOrder = serviceOrderManager.get(id);
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				miscellaneous = miscellaneousManager.get(id);
				trackingStatus = trackingStatusManager.get(id);
				docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
				mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());				
			}
			if (myFileFor.equals("CF")) {
				customerFile = customerFileManager.get(id);
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",customerFile.getJob());
				mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",customerFile.getJob());								
			}
			if (myFileFor.equals("PO")) {
				String partnerCode="";
				try{
					partner = partnerManager.get(id);
					partnerCode=partner.getPartnerCode();
				}catch(Exception e){
					partnerPrivate=partnerPrivateManager.get(id);
					partnerCode=partnerPrivate.getPartnerCode();
				}
			}
			if (myFileFor.equals("Truck")) {
				truck = truckManager.get(id);
			}
			//docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			//mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			
			if (id != null) {
				myFile = myFileManager.get(fid);
				//
				docAccessControlList = myFileManager.getDocAccessControl(myFile.getFileType(), sessionCorpID);
				if(docAccessControlList!=null && !docAccessControlList.isEmpty() && docAccessControlList.get(0)!=null){
					String docAccessControlDetails[]=docAccessControlList.get(0).toString().split("#");
					if(docAccessControlDetails[11].equals("true")){
						documentAccessIsVendorCode=true; 
					}
					if(docAccessControlDetails[12].equals("true")){
						documentAccessIsPaymentStatus=true; 
					}
				}		
				/*String  netid=myFile.getNetworkLinkId();
				String  fidds=myFile.getId().toString();			
				if((netid!=null) && (!netid.equals("")) ){
					if(!(netid.contains(fidds))){
						if(myFile.getDocumentCategory()!=null){
					docsList = refMasterManager.findDocumentListByCategory(sessionCorpID, "DOCUMENT",myFile.getDocumentCategory());
						}
					}
				}*/
				//
				fileType = myFile.getFileType();
				fileName = myFile.getFileFileName();
				mapFolder = myFile.getMapFolder();
				accountLineVendorCode=myFile.getAccountLineVendorCode();
				accountLinePayingStatus=myFile.getAccountLinePayingStatus();
				myFile.setDescription(myFile.getDescription());
				if (myFileFor.equals("SO")) {
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
					if(shipNumberList!=null && !shipNumberList.isEmpty()){
					Iterator it = shipNumberList.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }
				if (myFileFor.equals("CF")) {
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
					if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
					Iterator it = shipNumberList1.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }	
			}
			}
			} else {
				myFile = new MyFile();
			}

			return SUCCESS;
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	}
	private String accountlineShipNumber;
	private String accountlineVendorCode;
	private String	payingStatusValue;
	@SkipValidation
    public String findAccountLinePayingStatus(){
	try {
		payingStatusValue=myFileManager.getAccountLinePayingStatus(accountlineShipNumber,accountlineVendorCode,sessionCorpID);
	} catch (Exception e) {
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
  	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
  	 e.printStackTrace();
  	 return "errorlog";
	}
	 return SUCCESS; 
     }
	private String flagAgent;
	public String start() {
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		if(flagUloadInv !=null && (flagUloadInv.trim().equalsIgnoreCase("true")))
		{
		fileTypePayableProcessing = myFileManager.fileTypeForPayableProcessing(sessionCorpID);
    	Set set1=docsList.entrySet();
    	for(Object obj:set1)
    	{
    		Map.Entry<String, String> entry=(Map.Entry<String, String>)obj; 
    		Iterator It =fileTypePayableProcessing.iterator();
    		while(It.hasNext()){
    			String fileType=It.next().toString();
    	    if(entry.getKey().equalsIgnoreCase(fileType))
    		{
    			docsListAccountPortal.put(entry.getKey(), entry.getValue());
    		}
    		else
    		{
    			
    		}
    		}
    	}}
		else
		{
			docsListAccountPortal=refMasterManager.findDocumentListAccountPortal(soCorpId, "DOCUMENT");

		}
		mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		myFile = new MyFile();

		if (myFileFor.equals("CF")) {
			customerFile = customerFileManager.get(id);
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",customerFile.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",customerFile.getJob());											
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			myFile.setFileId(customerFile.getSequenceNumber());
			myFile.setCustomerNumber(customerFile.getSequenceNumber());
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(customerFile.getCorpID());
			List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
			if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
				Iterator shipIt =  shipNumberList.iterator();
					while(shipIt.hasNext()){
						String shipNumber = (String) shipIt.next();
						List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
						if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
							checkAgent = defCheckedShipNumber.get(0).toString();
							if(checkAgent.equalsIgnoreCase("BA")){
								bookingAgentFlag ="BA";
								myFile.setIsBookingAgent(true);
							}else if(checkAgent.equalsIgnoreCase("NA")){
								networkAgentFlag ="NA";
								myFile.setIsNetworkAgent(true);
							}else if(checkAgent.equalsIgnoreCase("OA")){
								originAgentFlag ="OA";
								myFile.setIsOriginAgent(true);
							}else if(checkAgent.equalsIgnoreCase("SOA")){
								subOriginAgentFlag ="SOA";
								myFile.setIsSubOriginAgent(true);
							}else if(checkAgent.equalsIgnoreCase("DA")){
								destAgentFlag ="DA";
								myFile.setIsDestAgent(true);
							}else if(checkAgent.equalsIgnoreCase("SDA")){
								subDestAgentFlag ="SDA";
								myFile.setIsSubDestAgent(true);
							} 				    
						}
						List shipNumberListSo = myFileManager.getShipNumberList(shipNumber);
						if(shipNumberListSo!=null && !shipNumberListSo.isEmpty()){
						Iterator it = shipNumberListSo.iterator();
				         while (it.hasNext()) {
				        	String agentFlag=(String)it.next();
				        	if(agentFlag.equalsIgnoreCase("BA")){
								bookingAgent ="BA";
							}else if(agentFlag.equalsIgnoreCase("NA")){
								networkAgent ="NA";
							}else if(agentFlag.equalsIgnoreCase("OA")){
								originAgent ="OA";
							}else if(agentFlag.equalsIgnoreCase("SOA")){
								subOriginAgent ="SOA";
							}else if(agentFlag.equalsIgnoreCase("DA")){
								destAgent ="DA";
							}else if(agentFlag.equalsIgnoreCase("SDA")){
								subDestAgent ="SDA";
							} 
				         }
					}
			}
		}
			
	}
		if (myFileFor.equals("SO")) {
			if(userType.equalsIgnoreCase("AGENT")){	
			serviceOrder = serviceOrderManager.getForOtherCorpid(id);
			miscellaneous = miscellaneousManager.getForOtherCorpid(id);
			trackingStatus = trackingStatusManager.getForOtherCorpid(id);
			}else{
			 serviceOrder = serviceOrderManager.get(id);
			 miscellaneous = miscellaneousManager.get(id);
			 trackingStatus = trackingStatusManager.get(id);	
			}
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());											
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			
			myFile.setFileId(serviceOrder.getShipNumber());
			myFile.setCustomerNumber(customerFile.getSequenceNumber());
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(customerFile.getCorpID());
			List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();
					if(checkAgent.equalsIgnoreCase("BA")){
						bookingAgentFlag ="BA";
						myFile.setIsBookingAgent(true);
					}else if(checkAgent.equalsIgnoreCase("NA")){
						networkAgentFlag ="NA";
						myFile.setIsNetworkAgent(true);
					}else if(checkAgent.equalsIgnoreCase("OA")){
						originAgentFlag ="OA";
						myFile.setIsOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SOA")){
						subOriginAgentFlag ="SOA";
						myFile.setIsSubOriginAgent(true);
					}else if(checkAgent.equalsIgnoreCase("DA")){
						destAgentFlag ="DA";
						myFile.setIsDestAgent(true);
					}else if(checkAgent.equalsIgnoreCase("SDA")){
						subDestAgentFlag ="SDA";
						myFile.setIsSubDestAgent(true);
					} 				    
				}
				List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
				if(shipNumberList!=null && !shipNumberList.isEmpty()){
				Iterator it = shipNumberList.iterator();
		         while (it.hasNext()) {
		        	String agentFlag=(String)it.next();
		        	if(agentFlag.equalsIgnoreCase("BA")){
						bookingAgent ="BA";
					}else if(agentFlag.equalsIgnoreCase("NA")){
						networkAgent ="NA";
					}else if(agentFlag.equalsIgnoreCase("OA")){
						originAgent ="OA";
					}else if(agentFlag.equalsIgnoreCase("SOA")){
						subOriginAgent ="SOA";
					}else if(agentFlag.equalsIgnoreCase("DA")){
						destAgent ="DA";
					}else if(agentFlag.equalsIgnoreCase("SDA")){
						subDestAgent ="SDA";
					} 
		         }
			}
		}
		if (myFileFor.equals("PO")) {
			String partnerCode="";
			try{
				partner = partnerManager.get(id);
				partnerCode=partner.getPartnerCode();
				myFile.setCorpID(partner.getCorpID());
			}catch(Exception e){
				partnerPrivate=partnerPrivateManager.get(id);
				partnerCode=partnerPrivate.getPartnerCode();
				myFile.setCorpID(partnerPrivate.getCorpID());
			}
			//customerFile = serviceOrder.getCustomerFile();
			//miscellaneous = miscellaneousManager.get(id);
			//trackingStatus = trackingStatusManager.get(id);
			myFile.setFileId(partnerCode);
			myFile.setCustomerNumber(partnerCode);
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			//myFile.setCorpID(partner.getCorpID());
		}
		if (myFileFor.equals("Truck")) {
			truck = truckManager.get(id);
			myFile.setFileId(truck.getLocalNumber());
			myFile.setCustomerNumber(truck.getLocalNumber());
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(truck.getCorpID());
		}
		if (myFileFor.equals("WKT")) {
			workTicket = workTicketManager.get(id);
			serviceOrder = workTicket.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(workTicket.getTicket().toString());
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}
		if (myFileFor.equals("CLM")) {
			claim = claimManager.get(id);
			serviceOrder = claim.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(claim.getClaimNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}

		if (myFileFor.equals("ACC")) {
			accountLine = accountLineManager.get(id);
			serviceOrder = accountLine.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(accountLine.getAccountLineNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}

		if (myFileFor.equals("VEH")) {
			vehicle = vehicleManager.get(id);
			serviceOrder = vehicle.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(vehicle.getShipNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}
		if (myFileFor.equals("SP")) {
			servicePartner = servicePartnerManager.get(id);
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			serviceOrder = servicePartner.getServiceOrder();
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(servicePartner.getCarrierNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}
		if (myFileFor.equals("CONT")) {
			container = containerManager.get(id);
			serviceOrder = container.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(container.getIdNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}
		if (myFileFor.equals("CART")) {
			carton = cartonManager.get(id);
			serviceOrder = carton.getServiceOrder();
			docsList = refMasterManager.findDocumentByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());
			mapList = refMasterManager.findMapByJobList(sessionCorpID, "DOCUMENT",serviceOrder.getJob());														
			customerFile = serviceOrder.getCustomerFile();
			getRequest().setAttribute("soLastName",customerFile.getLastName());
			miscellaneous = miscellaneousManager.get(serviceOrder.getId());
			trackingStatus = trackingStatusManager.get(serviceOrder.getId());
			myFile.setCustomerNumber(serviceOrder.getShipNumber());
			myFile.setFileId(String.valueOf(carton.getIdNumber()));
			myFile.setCreatedOn(new Date());
			myFile.setUpdatedOn(new Date());
			myFile.setCorpID(serviceOrder.getCorpID());
		}
		//docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		//mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		return INPUT;
	}

	public String attachMail() {

		try {
			if (checkBoxId.equals("") || checkBoxId.equals(",")) {
			} else {
				checkBoxId = checkBoxId.trim();
				if (checkBoxId.indexOf(",") == 0) {
					checkBoxId = checkBoxId.substring(1);
				}
				if (checkBoxId.lastIndexOf(",") == checkBoxId.length() - 1) {
					checkBoxId = checkBoxId.substring(0, checkBoxId.length() - 1);
				}
				String[] arrayId = checkBoxId.split(",");
				int arrayLength = arrayId.length;
				for (int i = 0; i < arrayLength; i++) {
					String idInAray = arrayId[i];
					Long idArray = Long.parseLong(idInAray);
					myFile = myFileManager.get(idArray);
					myFile.getFileFileName();
					myFile.getLocation();
				}
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String edit() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFile = myFileManager.get(id);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String deleteDocByCatMethod() {
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		myFileManager.updateActive(Long.parseLong(did), getRequest().getRemoteUser());
		try{
			
			MyFile myFile1=myFileManager.get(Long.parseLong(did));
			if(myFile1.getNetworkLinkId()!=null && (!(myFile1.getNetworkLinkId().toString().equals("")))){
			List myFileList = myFileManager.findlinkedIdList(myFile1.getNetworkLinkId());
			Iterator it = myFileList.iterator();
	         while (it.hasNext()) {
	        	 Long id = Long.parseLong(it.next().toString());
	        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
				if((newMyFile.getEmailStatus()!=null)&&(!newMyFile.getEmailStatus().equalsIgnoreCase(""))){
					EmailSetup emailSetup=emailSetupManager.getForOtherCorpid(Long.parseLong(newMyFile.getEmailStatus()));	
					if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
						emailSetup.setRetryCount(4);
						emailSetupManager.save(emailSetup);
					}
				}
	         }
			}else{
			MyFile myFile=myFileManager.get(Long.parseLong(did));
			if((myFile.getEmailStatus()!=null)&&(!myFile.getEmailStatus().equalsIgnoreCase(""))){
				EmailSetup emailSetup=emailSetupManager.get(Long.parseLong(myFile.getEmailStatus()));	
				if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
					emailSetup.setRetryCount(4);
					emailSetupManager.save(emailSetup);
				}
			}
			}
		}catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		/*if (relatedDocs.equalsIgnoreCase("No")) {
			list();
		} else if (relatedDocs.equalsIgnoreCase("Yes")) {
			relatedFiles();
		}*/
		docTypeFileList();
		return SUCCESS;
	}
	
	
	public String deleteDoc() {
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		myFileManager.updateActive(Long.parseLong(did), getRequest().getRemoteUser());
		try{
			
			MyFile myFile1=myFileManager.get(Long.parseLong(did));
			if(myFile1.getNetworkLinkId()!=null && (!(myFile1.getNetworkLinkId().toString().equals("")))){
			List myFileList = myFileManager.findlinkedIdList(myFile1.getNetworkLinkId());
			Iterator it = myFileList.iterator();
	         while (it.hasNext()) {
	        	 Long id = Long.parseLong(it.next().toString());
	        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
				if((newMyFile.getEmailStatus()!=null)&&(!newMyFile.getEmailStatus().equalsIgnoreCase(""))){
					EmailSetup emailSetup=emailSetupManager.getForOtherCorpid(Long.parseLong(newMyFile.getEmailStatus()));	
					if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
						emailSetup.setRetryCount(4);
						emailSetupManager.save(emailSetup);
					}
				}
	         }
			}else{
			MyFile myFile=myFileManager.get(Long.parseLong(did));
			if((myFile.getEmailStatus()!=null)&&(!myFile.getEmailStatus().equalsIgnoreCase(""))){
				EmailSetup emailSetup=emailSetupManager.get(Long.parseLong(myFile.getEmailStatus()));	
				if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
					emailSetup.setRetryCount(4);
					emailSetupManager.save(emailSetup);
				}
			}
			}
		}catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		if (relatedDocs.equalsIgnoreCase("No")) {
			list();
		} else if (relatedDocs.equalsIgnoreCase("Yes")) {
			relatedFiles();
		}
		try{
		String emailSetupIds="";
		List<MyFile> myFileTemp=myFiles;
		for(MyFile myFile:myFileTemp){
			if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
				if(emailSetupIds.equalsIgnoreCase("")){
					emailSetupIds="'"+myFile.getEmailStatus()+"'";
				}else{
					emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
				}
		    }
		}
		emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
		}catch(Exception e){e.printStackTrace();}
		return SUCCESS;
	}
	private List checkListTodayList;
	private Integer countCheckListTodayResult;
	private List checkListOverDueList;	
	private Integer countCheckListOverDueResult;
	@SkipValidation
	public String myFilecheckList(){
		try {
			if(myFileFrom.equals("SO")){
				serviceOrder=serviceOrderManager.get(id);
				billing = billingManager.get(id);
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(serviceOrder.getShipNumber(),serviceOrder.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(serviceOrder.getShipNumber(),serviceOrder.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
			if(myFileFrom.equals("CF")){
				customerFile= customerFileManager.get(id);
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
				}
			listRaleventDocs();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		   return SUCCESS; 	
	}

	public String relatedFiles() {
		try {
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			fieldName =user1.getDefaultSortforFileCabinet();
			if(fieldName!=null && !fieldName.equals("")){
			
			if(fieldName=="3" || fieldName.equals("3")){
				fieldName="0";		
			}
			if(fieldName=="2" || fieldName.equals("2")){
				fieldName="3";		
			}
			}else{
					fieldName="3";		
			}
			sortOrder=user1.getSortOrderForFileCabinet();
			if(sortOrder!=null && !sortOrder.equals("")){
				sortOrder=sortOrder.toLowerCase();	
			}
			else{
				sortOrder="ascending";	
			}
			listRaleventDocs();
			try{
			String emailSetupIds="";
			List<MyFile> myFileTemp=myFiles;
			for(MyFile myFile:myFileTemp){
				if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
					if(emailSetupIds.equalsIgnoreCase("")){
						emailSetupIds="'"+myFile.getEmailStatus()+"'";
					}else{
						emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
					}
			    }
			}
			emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
			}catch(Exception e){e.printStackTrace();}
		if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("relatedFiles"))){
			resultType="errorNoFile";
		}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String listDocProcess() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFiles = myFileManager.getIndexedList(sessionCorpID, "");
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	public String listDocProcessScan() {
		try {
			hitFlag="222";
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFiles = myFileManager.getIndexedList(sessionCorpID, getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String search() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFiles = myFileManager.searchMyFiles(fileTypes, descriptions, fileIds, createdBys, createdOns, sessionCorpID, index, duplicates);
			
			try{
				List<MyFile> temp1=myFiles;
			 List<MyFile> temp2=new ArrayList<MyFile>();
			 Iterator<MyFile> it=temp1.iterator();
			 while(it.hasNext()){
				   MyFile s=it.next();
				    String size=s.getFileSize();
				    String st[]=null;
				    Integer x;
				   
				    if(null!=size){
				     st=size.split("\\s");
				     if(st.length>1){
				    if(st[1].equals("MB")){
				    x=(Integer.parseInt(st[0]))*1024*1024; 
				    }
				    else if(st[1].equals("KB")){
				    
				     x=(Integer.parseInt(st[0]))*1024;
				   }
				   else {
				    x=Integer.parseInt(st[0]);
				   }
				     }
				     else{
				    	 x=Integer.parseInt(st[0]); 
				     }
				    s.setfSize(x);
				    temp2.add(s);
				   }
				    else{
				    	 x=Integer.parseInt("0");
				    	 s.setfSize(x);
						    temp2.add(s);
				    }
				 }
			 myFiles=temp2;
				   return SUCCESS;
				  }
				  catch(Exception e){
				   System.out.println("listEmp Exception: " + e);
				   String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
			    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
			    	 e.printStackTrace();
				   return INPUT;
				  }
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	}
	public String searchMaxSize() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if(index == null )
			{
			index = "true";
			}
			myFiles = myFileManager.searchMaxSizeFiles(fileTypes, descriptions, fileIds, createdBys, createdOns, sessionCorpID, docName, docSize,index);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String searchWasteFiles() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFiles = myFileManager.searchWasteFiles(fileTypes, descriptions, fileIds, createdBys, createdOns, sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String editFileUploadDoc() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (id != null) {
				myFile = myFileManager.get(id);
				fileType = myFile.getFileType();
				fileName = myFile.getFileFileName();
				myFile.setDescription(myFile.getDescription());
				fileId = myFile.getFileId();
				mapFolder = myFile.getMapFolder();
			} else {
				myFile = new MyFile();
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String saveDoc() throws Exception {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			soNameList = myFileManager.findSOName(fileId, sessionCorpID);
			if (soNameList.isEmpty()) {
				hitFlag = "0";
				return INPUT;
			}
			if(userType.equalsIgnoreCase("AGENT")){	
			myFile = myFileManager.getForOtherCorpid(id);
			}else{
			 myFile = myFileManager.get(id);	
			}
			myFile.setFileType(fileType);
			myFile.setDescription(description);
			myFile.setIsCportal(isCportal);
			myFile.setIsAccportal(isAccportal);
			myFile.setIsPartnerPortal(isPPortal);
			myFile.setIsServiceProvider(isServiceProvider);
			myFile.setInvoiceAttachment(invoiceAttachment);
			myFile.setFileId(fileId);
			myFile.setMapFolder(mapFolder);
			myFile.setUpdatedOn(new Date());
			myFile.setCoverPageStripped(false);

			List seqNo = myFileManager.getSequanceNum(fileId, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());

			myFile.setCorpID(sessionCorpID);

			myFile.setUpdatedBy(getRequest().getRemoteUser());
			myFile.setActive(true);

			FileSizeUtil fileSizeUtil = new FileSizeUtil();
			File newPDFFile = new File(myFile.getLocation());
			myFile.setFileSize(fileSizeUtil.FindFileSize(newPDFFile));

			List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
			if (isSecureList != null) {
				String isSecure = isSecureList.get(0).toString();
				if (isSecure.equalsIgnoreCase("Y")) {
					myFile.setIsSecure(true);
				} else {
					myFile.setIsSecure(false);
				}

			}
			documentCategory = refMasterManager.findDocCategoryByDocType(sessionCorpID, fileType);
			if(documentCategory!=null && !documentCategory.equals(""))
			{
				myFile.setDocumentCategory(documentCategory);
			}else{
				myFile.setDocumentCategory("General");
			}
			myFileManager.save(myFile);
			hitFlag = "1";
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String deleteDocPro() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			String key = "File moved successfully in Waste Basket.";
			myFileManager.updateActive(id, getRequest().getRemoteUser());
			search();
			saveMessage(key);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	@SkipValidation
	public String removeAllDocs() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			String key = "File moved successfully in Waste Basket.";
			userCheckConfirm = userCheckConfirm.trim();
			if (userCheckConfirm.indexOf(",") == 0) {
				userCheckConfirm = userCheckConfirm.substring(1);
			}
			if (userCheckConfirm.lastIndexOf(",") == userCheckConfirm.length() - 1) {
				userCheckConfirm = userCheckConfirm.substring(0, userCheckConfirm.length() - 1);
			}
			String[] arrayid = userCheckConfirm.split(",");
			int arrayLength = arrayid.length;
			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);
				if (id != null) {
					myFileManager.updateActive(id, getRequest().getRemoteUser());
				}
			}
			search();
			saveMessage(key);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String strippedFileOpen() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			soNameList = myFileManager.findSOName(fileId, sessionCorpID);
			if (soNameList.isEmpty()) {
				hitFlag = "0";
				return INPUT;
			}

			int[] stripPageIndex = { 0 };

			rId = rId.trim();
			if (rId.indexOf(",") == 0) {
				rId = rId.substring(1);
			}
			if (rId.lastIndexOf(",") == rId.length() - 1) {
				rId = rId.substring(0, rId.length() - 1);
			}
			String[] arrayid = rId.split(",");
			int arrayLength = arrayid.length;
			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);

				myFile = myFileManager.get(id);
				myFile.setFileType(fileType);
				myFile.setDescription(description);
				myFile.setIsCportal(isCportal);
				myFile.setIsAccportal(isAccportal);
				myFile.setIsPartnerPortal(isPPortal);
				myFile.setIsServiceProvider(isServiceProvider);
				myFile.setInvoiceAttachment(invoiceAttachment);
				myFile.setFileId(fileId);
				myFile.setUpdatedOn(new Date());

				List seqNo = myFileManager.getSequanceNum(fileId, sessionCorpID);
				myFile.setCustomerNumber(seqNo.get(0).toString());

				myFile.setCorpID(sessionCorpID);
				myFile.setUpdatedBy(getRequest().getRemoteUser());

				myFileManager.stripPages(stripPageIndex, myFile);
				myFileManager.updateMyfileStripped(id);
				myFile.setActive(true);

				myFile.setMapFolder(mapFolder);

				FileSizeUtil fileSizeUtil = new FileSizeUtil();
				File newPDFFile = new File(myFile.getLocation());
				myFile.setFileSize(fileSizeUtil.FindFileSize(newPDFFile));

				List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
				if (isSecureList != null && (!(isSecureList.isEmpty())) && isSecureList.get(0)!=null) {
					String isSecure = isSecureList.get(0).toString();
					if (isSecure.equalsIgnoreCase("Y")) {
						myFile.setIsSecure(true);
					} else {
						myFile.setIsSecure(false);
					}

				}
				documentCategory = refMasterManager.findDocCategoryByDocType(sessionCorpID, fileType);
				if(documentCategory!=null && !documentCategory.equals(""))
				{
					myFile.setDocumentCategory(documentCategory);
				}else{
					myFile.setDocumentCategory("General");
				}
				myFileManager.save(myFile);
				saveMessage(getText("Cover page has been stripped from the selected PDF Document."));
				hitFlag = "1";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	private String soName;

	public String serviceOrderName() {
		try {
			soNameList = myFileManager.findSOName(fileId, sessionCorpID);
			if (soNameList != null && !(soNameList.isEmpty())) {
				String[] str1 = soNameList.get(0).toString().split("#");
				if (str1[0].equalsIgnoreCase("1")) {
					str1[0] = "";
				} else {
					str1[0] = str1[0] + ",";
				}
				if (str1[1].equalsIgnoreCase("1")) {
					str1[1] = "";
				} else {
					str1[1] = str1[1];
				}
				if (str1[1].equalsIgnoreCase("") && (!(str1[0].equalsIgnoreCase("")))) {
					soName = str1[0].substring(0, str1[0].length() - 1);
				} else {
					soName = str1[0] + str1[1];
				}
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String splitDoc() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			if (myFileFor != null) {
				if (myFileFor.equals("CF")) {
					customerFile = customerFileManager.get(id);
					getRequest().setAttribute("soLastName",customerFile.getLastName());
				}
				if (myFileFor.equals("SO")) {
					serviceOrder = serviceOrderManager.get(id);
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(id);
					trackingStatus = trackingStatusManager.get(id);
				}
				if (myFileFor.equals("WKT")) {
					workTicket = workTicketManager.get(id);
					serviceOrder = workTicket.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}
				if (myFileFor.equals("CLM")) {
					claim = claimManager.get(id);
					serviceOrder = claim.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}

				if (myFileFor.equals("ACC")) {
					accountLine = accountLineManager.get(id);
					serviceOrder = accountLine.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}

				if (myFileFor.equals("VEH")) {
					vehicle = vehicleManager.get(id);
					serviceOrder = vehicle.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}
				if (myFileFor.equals("SP")) {
					servicePartner = servicePartnerManager.get(id);
					serviceOrder = servicePartner.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}
				if (myFileFor.equals("CONT")) {
					container = containerManager.get(id);
					serviceOrder = container.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}
				if (myFileFor.equals("CART")) {
					carton = cartonManager.get(id);
					serviceOrder = carton.getServiceOrder();
					customerFile = serviceOrder.getCustomerFile();
					getRequest().setAttribute("soLastName",customerFile.getLastName());
					miscellaneous = miscellaneousManager.get(serviceOrder.getId());
					trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				}

			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}

		return SUCCESS;
	}

	public String docSplitEdit() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			MyFile myFileOld = myFileManager.get(id);

			myFile = new MyFile();
			myFile.setFileId(myFileOld.getFileId());
			myFile.setCorpID(myFileOld.getCorpID());
			myFile.setCreatedOn(new Date());
			myFile.setCreatedBy(getRequest().getRemoteUser());
			myFile.setUpdatedOn(new Date());
			myFile.setUpdatedBy(getRequest().getRemoteUser());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}

		return SUCCESS;
	}

	public String saveSplitDocument() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			MyFile myFileOld = myFileManager.get(id);

			myFile.setCorpID(sessionCorpID);
			myFile.setFileContentType(myFileOld.getFileContentType());
			myFile.setFileType(fileType);
			myFile.setDescription(description);
			myFile.setFileId(fileId);

			List seqNo = myFileManager.getSequanceNum(fileId, sessionCorpID);
			myFile.setCustomerNumber(seqNo.get(0).toString());

			myFile.setCreatedOn(new Date());
			myFile.setCreatedBy(getRequest().getRemoteUser());
			myFile.setUpdatedOn(new Date());
			myFile.setUpdatedBy(getRequest().getRemoteUser());
			myFile.setCoverPageStripped(true);
			myFile.setActive(true);
			myFile.setMapFolder(mapFolder);

			List isSecureList = myFileManager.getIsSecure(fileType, sessionCorpID);
			if (isSecureList != null) {
				String isSecure = isSecureList.get(0).toString();
				if (isSecure.equalsIgnoreCase("Y")) {
					myFile.setIsSecure(true);
				} else {
					myFile.setIsSecure(false);
				}

			}

			int stripPageIndex = Integer.parseInt(pageIndex);
			myFileManager.splitPages(stripPageIndex, myFileOld, myFile,sessionCorpID);

			FileSizeUtil fileSizeUtil = new FileSizeUtil();
			File newPDFFile = new File(myFileOld.getLocation());
			String updateSize = fileSizeUtil.FindFileSize(newPDFFile);
			myFileManager.updateFileSize(updateSize, id);
			hitflag = "1";
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String getImages() {
		return SUCCESS;
	}

	public InputStream getImageList() {
		String imageFileId = getRequest().getParameter("fileId");
		return new ByteArrayInputStream(myFileManager.getImageList(Long.parseLong(imageFileId), 0).getBytes());
	}

	public String getDocImages() {
		return SUCCESS;
	}

	public InputStream getImageDocList() {
		String imageFileId = getRequest().getParameter("fileId");
		return new ByteArrayInputStream(myFileManager.getImageList(Long.parseLong(imageFileId), 1).getBytes());
	}

	public String getImage() {
		return SUCCESS;
	}

	public InputStream getImageData() {
		String imageName = getRequest().getParameter("imageName");
		InputStream is = null;
		try {
			is = new BufferedInputStream(new FileInputStream(myFileManager.getImageLocation(imageName)));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return is;
	}

	public String basketDoc() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			fieldName =user1.getDefaultSortforFileCabinet();
			if(fieldName!=null && !fieldName.equals("")){
				flagchk=true;
			if(fieldName.equals("3")){
				fieldName="0";		
			}
			if(fieldName=="4" || fieldName.equals("4")){
				fieldName="3";		
			}
			if(fieldName=="6" || fieldName.equals("6")){
				fieldName="5";		
			}
			if(fieldName=="7" || fieldName.equals("7")){
				fieldName="6";		
			}
			}else{
				fieldName="2";
			}
			sortOrder=user1.getSortOrderForFileCabinet();
			if(sortOrder!=null && !sortOrder.equals("")){
				sortOrder=sortOrder.toLowerCase();
			}else{
				sortOrder="ascending";	
			}
			list();
			if((myFileJspName!=null && !myFileJspName.equalsIgnoreCase("") && myFileJspName.equalsIgnoreCase("basketFiles"))){
				resultType="errorNoFile";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	public String revocerDoc() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFileManager.recoverDoc(Long.parseLong(did), getRequest().getRemoteUser());
			try{
				MyFile myFile=myFileManager.get(Long.parseLong(did));
				if((myFile.getEmailStatus()!=null)&&(!myFile.getEmailStatus().equalsIgnoreCase(""))){
					EmailSetup emailSetup=emailSetupManager.get(Long.parseLong(myFile.getEmailStatus()));	
					if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
						emailSetup.setRetryCount(0);
						emailSetupManager.save(emailSetup);
					}
				}
			}catch(Exception e){}
			list();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}

	
	public String revocerDocByCatMethod() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			myFileManager.recoverDoc(Long.parseLong(did), getRequest().getRemoteUser());
			try{
				MyFile myFile=myFileManager.get(Long.parseLong(did));
				if((myFile.getEmailStatus()!=null)&&(!myFile.getEmailStatus().equalsIgnoreCase(""))){
					EmailSetup emailSetup=emailSetupManager.get(Long.parseLong(myFile.getEmailStatus()));	
					if((emailSetup.getEmailStatus()!=null)&&(emailSetup.getEmailStatus().trim().equalsIgnoreCase("SaveForEmail"))){
						emailSetup.setRetryCount(0);
						emailSetupManager.save(emailSetup);
					}
				}
			}catch(Exception e){
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return "errorlog";
			}
			docTypeFileList();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	public void combinDoc() {
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		try {
			List<InputStream> pdfs = new ArrayList<InputStream>();
			name = name.replaceAll(" ", "_");
			String outfileName = seqNum + "_" + name + ".pdf";
			HttpServletResponse response = getResponse();
			OutputStream output = response.getOutputStream();
			response.setContentType("application/pdf");
			//response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Content-Disposition", "attachment; filename = " + outfileName);
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			rId = rId.trim();
			if (rId.indexOf(",") == 0) {
				rId = rId.substring(1);
			}
			if (rId.lastIndexOf(",") == rId.length() - 1) {
				rId = rId.substring(0, rId.length() - 1);
			}
			String[] arrayid = rId.split(",");
			int arrayLength = arrayid.length;

			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);
				myFile = myFileManager.get(id);
				String fileName = myFile.getLocation();
				pdfs.add(new FileInputStream(fileName));
			}

			output = myFileManager.concatPDFs(pdfs, output, false);

			output.write("\r\n".getBytes());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	}
	
	
	public void mergePdfForAnyDoc() {/*
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
		try {
			List<InputStream> pdfs = new ArrayList<InputStream>();
			name = name.replaceAll(" ", "_");
			String outfileName = seqNum + "_" + name + ".pdf";
			HttpServletResponse response = getResponse();
			OutputStream output = response.getOutputStream();
			response.setContentType("application/pdf");
			//response.setHeader("Cache-Control", "no-cache");
			response.setHeader("Content-Disposition", "attachment; filename = " + outfileName);
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			rId = rId.trim();
			if (rId.indexOf(",") == 0) {
				rId = rId.substring(1);
			}
			if (rId.lastIndexOf(",") == rId.length() - 1) {
				rId = rId.substring(0, rId.length() - 1);
			}
			String[] arrayid = rId.split(",");
			int arrayLength = arrayid.length;

			for (int i = 0; i < arrayLength; i++) {
				id = Long.parseLong(arrayid[i]);
				myFile = myFileManager.get(id);
				String fileName = myFile.getLocation();
				String fileExt = FilenameUtils.getExtension(fileName);
				String  FName = FilenameUtils.getBaseName(fileName);
		        String pathName = FilenameUtils.getPath(fileName);
				System.out.println(pathName+"======================"+FName);
				
				//get File extension
				//FileFormatInfo info = FileFormatUtil.detectFileFormat(fileName);
				 BufferedImage image = ImageIO.read(new File(fileName));
				 System.out.println(image);
				 if(image != null){
						//Instantiate a Pdf object by calling its empty constructor
						Pdf pdf1 = new Pdf();
						//Create a section in the Pdf object
						Section sec1 = pdf1.getSections().add();
						//Create an image object in the section
						aspose.pdf.Image img1 = new aspose.pdf.Image(sec1);
						//Add image object into the Paragraphs collection of the section
						sec1.getParagraphs().add(img1);
						//Set the path of image file
						img1.getImageInfo().setFile(fileName);
						//Set the path of image file                                          
						//img1.getImageInfo().setTitle(fileExt.toUpperCase()+" image");
						//Save the Pdf
						pdf1.save(pathName+"\\"+FName+".pdf"); 
				 }else if(fileExt.equalsIgnoreCase("doc") || fileExt.equalsIgnoreCase("eml") || fileExt.equalsIgnoreCase("docx") || fileExt.equalsIgnoreCase("rtf") || fileExt.equalsIgnoreCase("html") || fileExt.equalsIgnoreCase("xml") || fileExt.equalsIgnoreCase("odt"))
				 {
					 //Document doc = new Document(fileName);
						// Save as PDF.
					//	doc.save(pathName+"\\"+FName+".pdf");
				 }else if(fileExt.equalsIgnoreCase("XLS") || fileExt.equalsIgnoreCase("XLS") || fileExt.equalsIgnoreCase("XLSX") || fileExt.equalsIgnoreCase("XLSM") || fileExt.equalsIgnoreCase("XLSB") || fileExt.equalsIgnoreCase("CSV") || fileExt.equalsIgnoreCase("CSV") || fileExt.equalsIgnoreCase("XLTM") || fileExt.equalsIgnoreCase("XLTX"))
				 {
					 //Workbook workbook = new Workbook(fileName);
						//Save the document in PDF format
					//	workbook.save(pathName+"\\"+FName+".pdf", SaveFormat.PDF);
				 }
				 if(!fileExt.equalsIgnoreCase("pdf")){
				 //pdfs.add(new FileInputStream(pathName+"\\"+FName+".pdf"));
				 }else{
					// pdfs.add(new FileInputStream(fileName));
				 }
			}

			output = myFileManager.concatPDFs(pdfs, output, false);

			output.write("\r\n".getBytes());
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
		}
	*/}

	public String docTransfer() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			categoryList=refMasterManager.findByParameter(sessionCorpID, "DOCUMENT CATEGORY");
			user1 = userManager.getUserByUsername(getRequest().getRemoteUser());
			fieldName =user1.getDefaultSortforFileCabinet();
			if(fieldName==null || fieldName.equals("")){
				fieldName="2";		
			}
			sortOrder=user1.getSortOrderForFileCabinet();
			if(sortOrder==null || sortOrder.equals("")){
				sortOrder="ascending";	
			}else{
				sortOrder=sortOrder.toLowerCase();
			}
			try {
				relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
				List<InputStream> pdfs = new ArrayList<InputStream>();

				rId = rId.trim();
				if (rId.indexOf(",") == 0) {
					rId = rId.substring(1);
				}
				if (rId.lastIndexOf(",") == rId.length() - 1) {
					rId = rId.substring(0, rId.length() - 1);
				}
				String[] arrayid = rId.split(",");
				int arrayLength = arrayid.length;

				for (int i = 0; i < arrayLength; i++) {
					id = Long.parseLong(arrayid[i]);
					myFile = myFileManager.get(id);
					if (myFile.getMapFolder() != null && myFile.getMapFolder().length() > 0) {
						String fileName = myFile.getLocation();
						pdfs.add(new FileInputStream(fileName));
					}
				}
				if (pdfs.size() >= 1) {
					name = name.replaceAll(" ", "_");
					String outfileName = seqNum + "_" + name + ".pdf";
					HttpServletResponse response = getResponse();
					OutputStream output = response.getOutputStream();
					//response.setContentType("application/pdf");
					//response.setHeader("Cache-Control", "no-cache");
					//response.setHeader("Content-Disposition", "attachment; filename = " + outfileName);
					//response.setHeader("Pragma", "public");
					//response.setHeader("Cache-Control", "max-age=0");
					//output = myFileManager.concatPDFs(pdfs, output, false);
				} else {
					hitFlag = "5";
					if(docUpload!=null && docUpload.equals("docCentre")){
						return "gotoDocCentre";
					}else{
						return SUCCESS;
					}
				}
			} catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
		    	 e.printStackTrace();
		    	 return "errorlog";
			}
			myFileManager.getUpdateTransDate(rId,getRequest().getRemoteUser());
			hitFlag = "1";
			if(docUpload!=null && docUpload.equals("docCentre")){
				return "gotoDocCentre";
			}else{
				return SUCCESS;
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	}
	
	
	public String relatedFilesDocTypeMethod() {
		try {
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			listRaleventDocsGrpByType();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String listRaleventDocsGrpByType() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
			transDocSysDefault=myFileManager.findTransDocSystemDefault(sessionCorpID).get(0).toString();
			categoryList=refMasterManager.findByParameter(sessionCorpID, "DOCUMENT CATEGORY");
			String tempSortOrder="";
			String sort11=user1.getDefaultSortforFileCabinet();
			if(sort11!=null && !sort11.equals("") &&!sort11.contains(",") ){
				tempSortOrder=defaultSortforFileCabinet.get(sort11);
			}
			if(sort11!=null && !sort11.equals("") && sort11.contains(",") ){
				String[] temp11=sort11.split(",");
				for(String sort111:temp11){
					sort111=sort111.trim();
					if(sort111!=null && !sort111.equals("")){
					if(tempSortOrder.equals("")){
						tempSortOrder=defaultSortforFileCabinet.get(sort111);
					}else{
						tempSortOrder=tempSortOrder+","+defaultSortforFileCabinet.get(sort111);;
					}
					}
				}
			}
			String sort= new String("asc");
			if(user1.getSortOrderForFileCabinet()!=null && user1.getSortOrderForFileCabinet().equalsIgnoreCase("Descending") ){
				sort = "desc";
			}
			if (myFileFor != null && myFileFor.equals("CF")) {
				customerFile = customerFileManager.get(id);
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				transDocJobType = customerFile.getJob();
				docTypeFiles = myFileManager.getListByCustomerNumberGrpByType(customerFile.getSequenceNumber(), active, tempSortOrder, sort);
				//myFiles = myFilesListFromCustomer(secure,categorySearchValue);			
				if (myFileFor.equals("CF")) {
					List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
						Iterator shipIt =  shipNumberList.iterator();
							while(shipIt.hasNext()){
								String shipNumber = (String) shipIt.next();
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumber);
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList1 = myFileManager.getShipNumberList(shipNumber);
					if(shipNumberList1!=null && !shipNumberList1.isEmpty()){
					Iterator it = shipNumberList1.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }	
			}
			}	
			}else if (myFileFor != null && myFileFor.equals("SO")) {
				serviceOrder = serviceOrderManager.get(id);
				miscellaneous = miscellaneousManager.get(serviceOrder.getId());
				customerFile = serviceOrder.getCustomerFile();
				getRequest().setAttribute("soLastName",customerFile.getLastName());
				trackingStatus = trackingStatusManager.get(serviceOrder.getId());
				transDocJobType = serviceOrder.getJob();
				billing = billingManager.get(serviceOrder.getId());
				docTypeFiles = myFileManager.getListByCustomerNumberGrpByType(customerFile.getSequenceNumber(), active, tempSortOrder, sort);
//			myFiles = myFilesListFromServiceOrder(secure,categorySearchValue);
				if (myFileFor.equals("SO")) {
					List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(serviceOrder.getShipNumber());
					if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
						String checkAgent = defCheckedShipNumber.get(0).toString();
						if(checkAgent.equalsIgnoreCase("BA")){
							bookingAgentFlag ="BA";
						}else if(checkAgent.equalsIgnoreCase("NA")){
							networkAgentFlag ="NA";
						}else if(checkAgent.equalsIgnoreCase("OA")){
							originAgentFlag ="OA";
						}else if(checkAgent.equalsIgnoreCase("SOA")){
							subOriginAgentFlag ="SOA";
						}else if(checkAgent.equalsIgnoreCase("DA")){
							destAgentFlag ="DA";
						}else if(checkAgent.equalsIgnoreCase("SDA")){
							subDestAgentFlag ="SDA";
						} 
					}
					List shipNumberList = myFileManager.getShipNumberList(serviceOrder.getShipNumber());
					if(shipNumberList!=null && !shipNumberList.isEmpty()){
					Iterator it = shipNumberList.iterator();
			         while (it.hasNext()) {
			        	String agentFlag=(String)it.next();
			        	if(agentFlag.equalsIgnoreCase("BA")){
							bookingAgent ="BA";
						}else if(agentFlag.equalsIgnoreCase("NA")){
							networkAgent ="NA";
						}else if(agentFlag.equalsIgnoreCase("OA")){
							originAgent ="OA";
						}else if(agentFlag.equalsIgnoreCase("SOA")){
							subOriginAgent ="SOA";
						}else if(agentFlag.equalsIgnoreCase("DA")){
							destAgent ="DA";
						}else if(agentFlag.equalsIgnoreCase("SDA")){
							subDestAgent ="SDA";
						} 
			         }
				}
			  }
			}	
			else if (myFileFor != null && myFileFor.equals("PO")) {
				docTypeFiles =  myFilesListFromPartnerDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromPartner(secure,categorySearchValue);
			}
			else if (myFileFor != null && myFileFor.equals("Truck")) {
				docTypeFiles = myFilesListFromTruckDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromTruck(secure,categorySearchValue);
			}		
			else if (myFileFor != null && myFileFor.equals("WKT")) {
				docTypeFiles = myFilesListFromWorkTicketDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromWorkTicket(secure,categorySearchValue);
			}

			else if (myFileFor.equals("CLM")) {
				docTypeFiles = myFilesListFromClaimDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromClaim(secure,categorySearchValue);
			}

			else if (myFileFor.equals("ACC")) {
				docTypeFiles = myFilesListFromAccountingDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromAccounting(secure,categorySearchValue);
			}
			else if (myFileFor.equals("VEH")) {
				docTypeFiles = myFilesListFromVehicleDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromVehicle(secure,categorySearchValue);
			}
			else if (myFileFor.equals("CONT")) {
				docTypeFiles = myFilesListFromContainerDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromContainer(secure,categorySearchValue);
			}
			else if (myFileFor.equals("CART")) {
				docTypeFiles = myFilesListFromCartonDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromCarton(secure,categorySearchValue);
			}
			else if (myFileFor.equals("SP")) {
				docTypeFiles =  myFilesListFromServicePartnerDocType(secure,categorySearchValue, tempSortOrder, sort);
				//myFiles = myFilesListFromServicePartner(secure,categorySearchValue);
			}
			try{
			String emailSetupIds="";
			for(Map.Entry<String, List<MyFile>> rec:docTypeFiles.entrySet()){
				List<MyFile> myFileTemp=rec.getValue();
				for(MyFile myFile:myFileTemp){
					if(myFile.getEmailStatus()!=null && !myFile.getEmailStatus().equalsIgnoreCase("")){
						if(emailSetupIds.equalsIgnoreCase("")){
							emailSetupIds="'"+myFile.getEmailStatus()+"'";
						}else{
							emailSetupIds=emailSetupIds+",'"+myFile.getEmailStatus()+"'";
						}
				    }
				}
			}
			emailStatusList=emailSetupManager.getEmailStatusWithId(emailSetupIds,sessionCorpID);	  
			}catch(Exception e){e.printStackTrace();}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}	
		return SUCCESS;
	}
	
	//List check box upload ....
	@SkipValidation
   public String uploadMyFileIsCheckBox(){
		try {
			if (myFileFor != null && myFileFor.equals("SO")) {
			serviceOrder = serviceOrderManager.get(id);
			trackingStatus = trackingStatusManager.get(id);
			myFile = myFileManager.get(fid);
			String networkLinkId=myFile.getNetworkLinkId();
			if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){
			List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
			Set set = new HashSet(linkedShipNumberList);
			List linkedSeqNum = new ArrayList(set);
			if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
				if(networkLinkId ==null || networkLinkId.equals("") || networkLinkId.equalsIgnoreCase("null")){
					networkLinkId = sessionCorpID + (myFile.getId()).toString();	
				}
				 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
				 
				 Iterator it = linkedSeqNum.iterator();
			     while (it.hasNext()) {
			    	 	 String shipNumber=(String)it.next();
			        	 String removeDatarow = shipNumber.substring(0, 6);
			        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber) && !removeDatarow.equals("remove")){
			        		 List linkedId = myFileManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
			         		 if(linkedId ==null || linkedId.isEmpty()){
				             MyFile myFileObj = new MyFile();
				        	 myFileObj.setFileId(shipNumber);
				        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
				        	 myFileObj.setActive(myFile.getActive());
				        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
				        	 myFileObj.setDescription(myFile.getDescription());
				        	 //myFileObj.setFile(myFile.getFile());
				        	 myFileObj.setFileContentType(myFile.getFileContentType());
				        	 myFileObj.setFileFileName(myFile.getFileFileName());
				        	 myFileObj.setFileSize(myFile.getFileSize());
				        	 myFileObj.setFileType(myFile.getFileType());
				        	 
				        	 myFileObj.setIsCportal(myFile.getIsCportal());
				        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
				        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
				        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
				        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
				 			if(checkFlagBA.equals("BA")){
				 				myFileObj.setIsBookingAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
				 			}
				 			if(checkFlagNA.equals("NA")){
				 				myFileObj.setIsNetworkAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
				 			}
				 			if(checkFlagOA.equals("OA")){
				 				myFileObj.setIsOriginAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
				 			}
				 			if(checkFlagSOA.equals("SOA")){
				 				myFileObj.setIsSubOriginAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
				 			}
				 			if(checkFlagDA.equals("DA")){
				 				myFileObj.setIsDestAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
				 			}
				 			if(checkFlagSDA.equals("SDA")){
				 				myFileObj.setIsSubDestAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
				 			}
				        	
				        	 myFileObj.setIsSecure(myFile.getIsSecure());
				        	 myFileObj.setLocation(myFile.getLocation());
				        	 myFileObj.setMapFolder(myFile.getMapFolder());
				        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
				        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
				        	 myFileObj.setCreatedOn(new Date());
				        	 //myFileObj.setUpdatedBy(getRequest().getRemoteUser());
				        	 if(myFileObj.getCorpID().equals(sessionCorpID)){
				        		 	myFileObj.setUpdatedBy(getRequest().getRemoteUser());	
					         }else{
					        		myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
					         }
				        	 myFileObj.setUpdatedOn(new Date());
				        	 //Added For #7990
				        	 //myFileObj.setDocumentCategory(myFile.getDocumentCategory());
				        	 if(!sessionCorpID.equalsIgnoreCase(myFileObj.getCorpID())){
				        		 String fileCategory = refMasterManager.findDocCategoryByDocType(myFileObj.getCorpID(), myFile.getFileType());
				        		 if(fileCategory!=null && !fileCategory.equalsIgnoreCase("")){
				        			 myFileObj.setDocumentCategory(fileCategory);
				        		 }else{
				        			 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
				        		 }
				        	 }else{
				        		 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
				        	 }
				        	 //end
				        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  
				        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
			        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
			        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
			        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
			        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
			        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
			        	    		myFileObj.setFileType("Invoice Vendor");	
			        	    		}
			        	    	}
				        	 
				        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
				        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
				        			 && myFile.getFileType().equals("Client Quote")){
				        		 myFileObj.setFileType("Agent Quote"); 
				        	 }
				        	 
				        	 myFileObj.setNetworkLinkId(networkLinkId);
				        	 myFileManager.save(myFileObj);
			          }else{
			     			myFile = myFileManager.get(fid);
			     			if(checkFlagBA.equals("BA")){
			     				myFile.setIsBookingAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsBookingAgent(myFile.getIsBookingAgent());
				 			}
				 			if(checkFlagNA.equals("NA")){
				 				myFile.setIsNetworkAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
				 			}
				 			if(checkFlagOA.equals("OA")){
				 				myFile.setIsOriginAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsOriginAgent(myFile.getIsOriginAgent());
				 			}
				 			if(checkFlagSOA.equals("SOA")){
				 				myFile.setIsSubOriginAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
				 			}
				 			if(checkFlagDA.equals("DA")){
				 				myFile.setIsDestAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsDestAgent(myFile.getIsDestAgent());
				 			}
				 			if(checkFlagSDA.equals("SDA")){
				 				myFile.setIsSubDestAgent(true);
				 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",myFile.getFileId(),myFile.getId(),"check" );
				 			}else{
				 				myFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
				 			}			        	
			     		 }
			        	 }else{
			     			if(checkFlagBA.equals("BA")){
			    				myFile.setIsBookingAgent(true);
			    			}
			    			if(checkFlagNA.equals("NA")){
			    				myFile.setIsNetworkAgent(true);
			    			}
			    			if(checkFlagOA.equals("OA")){
			     				myFile.setIsOriginAgent(true);
			    			}
			    			if(checkFlagSOA.equals("SOA")){
			     				myFile.setIsSubOriginAgent(true);
			    			}
			    			if(checkFlagDA.equals("DA")){
			     				myFile.setIsDestAgent(true);
			    			}
			    			if(checkFlagSDA.equals("SDA")){
			     				myFile.setIsSubDestAgent(true);
			    			}
			    			myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
			    			myFileManager.save(myFile);
			    		}
			      }
			}else{
				if(checkFlagBA.equals("BA")){
					myFile.setIsBookingAgent(true);
				}
				if(checkFlagNA.equals("NA")){
					myFile.setIsNetworkAgent(true);
				}
				if(checkFlagOA.equals("OA")){
					myFile.setIsOriginAgent(true);
				}
				if(checkFlagSOA.equals("SOA")){
					myFile.setIsSubOriginAgent(true);
				}
				if(checkFlagDA.equals("DA")){
					myFile.setIsDestAgent(true);
				}
				if(checkFlagSDA.equals("SDA")){
					myFile.setIsSubDestAgent(true);
				}
				myFileManager.save(myFile);
			}
			}
			if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
				serviceOrder = serviceOrderManager.get(id);
				myFile = myFileManager.get(fid);
				List linkedShipNumberList = null;
				List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){
						linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
					}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 Long id = Long.parseLong(it.next().toString());
			        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
				        		/* newMyFile.setIsBookingAgent(false);
					        	 newMyFile.setIsNetworkAgent(false);
					        	 newMyFile.setIsOriginAgent(false);   
					        	 newMyFile.setIsSubOriginAgent(false);
					        	 newMyFile.setIsDestAgent(false);
					        	 newMyFile.setIsSubDestAgent(false);
					        	 newMyFile.setNetworkLinkId("");*/
			        	 	}else{
			        		 	if(checkFlagBA.equalsIgnoreCase("BA")) {
				        			 newMyFile.setIsBookingAgent(true);
			        	    	}else{
			        	    		 newMyFile.setIsBookingAgent(myFile.getIsBookingAgent());
			        	    	}
			        	    	if(checkFlagNA.equalsIgnoreCase("NA")) {
						        	 newMyFile.setIsNetworkAgent(true);

			        	    	}else{
						        	 newMyFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
			        	    	}
			        	    	if(checkFlagOA.equalsIgnoreCase("OA")) {
						        	 newMyFile.setIsOriginAgent(true);
			        	    	}else{
						        	 newMyFile.setIsOriginAgent(myFile.getIsOriginAgent());
			        	    	}
			        	    	if(checkFlagSOA.equalsIgnoreCase("SOA")) {
						        	 newMyFile.setIsSubOriginAgent(true);
			        	    	}else{
						        	 newMyFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
			        	    	}
			        	    	if(checkFlagDA.equalsIgnoreCase("DA")) {
						        	 newMyFile.setIsDestAgent(true);
			        	    	}else{
						        	 newMyFile.setIsDestAgent(myFile.getIsDestAgent());
			        	    	}
			        	    	if(checkFlagSDA.equalsIgnoreCase("SDA")) {
						        	 newMyFile.setIsSubDestAgent(true);
			        	    	}else{
						        	 newMyFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
			        	    	}		        	    					        	 
			        	    	 newMyFile.setIsCportal(myFile.getIsCportal());
					        	 newMyFile.setIsAccportal(myFile.getIsAccportal());
					        	 newMyFile.setIsPartnerPortal(myFile.getIsPartnerPortal());
					        	 newMyFile.setIsServiceProvider(myFile.getIsServiceProvider());
					        	 newMyFile.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 	  }
			        	 	    newMyFile.setDocumentCategory(myFile.getDocumentCategory());
			        	 	//newMyFile.setUpdatedBy(getRequest().getRemoteUser());
			        	 	   if(newMyFile.getCorpID().equals(sessionCorpID)){
			        	 		   		newMyFile.setUpdatedBy(getRequest().getRemoteUser());	
			        	 	   }else{
			        	 		   		newMyFile.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
			        	 	   }
			        	 	newMyFile.setUpdatedOn(new Date());
			        	 myFileManager.save(newMyFile);	
			        }
			      }
				 }
			}
			
			if (myFileFor != null && myFileFor.equals("CF")) {
				customerFile = customerFileManager.get(id);
				myFile = myFileManager.get(fid);
				String networkLinkId=myFile.getNetworkLinkId();
				List previousSequenceNumberList = new ArrayList();
				List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
				if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
					Iterator shipIt =  shipNumberList.iterator();
						while(shipIt.hasNext()){
							String shipNumberOld = (String) shipIt.next();
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumberOld);
				trackingStatus = trackingStatusManager.get(Long.parseLong(trackingStatusManager.findIdByShipNumber(shipNumberOld).get(0).toString()));
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();				    
				}	
			if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){						
										List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
										Set set = new HashSet(linkedShipNumberList);
										List linkedSeqNum = new ArrayList(set);
										if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
											if(networkLinkId ==null || networkLinkId.equals("") || networkLinkId.equalsIgnoreCase("null")){
												networkLinkId = sessionCorpID + (myFile.getId()).toString();	
											}
								     		 myFileManager.upDateNetworkLinkId(networkLinkId,myFile.getId());
								     		
											 Iterator it = linkedSeqNum.iterator();
									         while (it.hasNext()) {
									        	 	 String shipNumber=(String)it.next();
									        	 	String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
										        	 if(previousSequenceNumberList.isEmpty() || (!(previousSequenceNumberList.contains(sequenceNumber)))){
										        		 previousSequenceNumberList.add(sequenceNumber);
										        	 String removeDatarow = shipNumber.substring(0, 6);
										        	 if(!(myFile.getFileId().toString()).equalsIgnoreCase(shipNumber.substring(0, shipNumber.length()-2)) && !removeDatarow.equals("remove")){
										        		 List linkedId = myFileManager.getLinkedId(networkLinkId, shipNumber.substring(0, 4));
											     		 if(linkedId ==null || linkedId.isEmpty()){
										        		 MyFile myFileObj = new MyFile();
											        	 myFileObj.setFileId(shipNumber.substring(0, shipNumber.length()-2));
											        	 myFileObj.setCorpID(shipNumber.substring(0, 4));
											        	 myFileObj.setActive(myFile.getActive());
											        	 myFileObj.setCustomerNumber(shipNumber.substring(0, shipNumber.length()-2));
											        	
											        	 myFileObj.setDescription(myFile.getDescription());
											        
											        	 myFileObj.setFileContentType(myFile.getFileContentType());
											        	 myFileObj.setFileFileName(myFile.getFileFileName());
											        	 myFileObj.setFileSize(myFile.getFileSize());
											        	 myFileObj.setFileType(myFile.getFileType());
											        	 
											        	 myFileObj.setIsCportal(myFile.getIsCportal());
											        	 myFileObj.setIsAccportal(myFile.getIsAccportal());
											        	 myFileObj.setIsPartnerPortal(myFile.getIsPartnerPortal());
											        	 myFileObj.setIsServiceProvider(myFile.getIsServiceProvider());
											        	 myFileObj.setInvoiceAttachment(myFile.getInvoiceAttachment());
											 			
											        	 myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
											        	 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
											        	 myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
											        	 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
											        	 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
											        	 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
											        	 if(checkFlagBA.equals("BA")){
												 				myFileObj.setIsBookingAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				myFileObj.setIsBookingAgent(myFile.getIsBookingAgent());
												 			}
												 			if(checkFlagNA.equals("NA")){
												 				myFileObj.setIsNetworkAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				 myFileObj.setIsNetworkAgent(myFile.getIsNetworkAgent());
												 			}
												 			if(checkFlagOA.equals("OA")){
												 				myFileObj.setIsOriginAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				myFileObj.setIsOriginAgent(myFile.getIsOriginAgent());
												 			}
												 			if(checkFlagSOA.equals("SOA")){
												 				myFileObj.setIsSubOriginAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				 myFileObj.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
												 			}
												 			if(checkFlagDA.equals("DA")){
												 				myFileObj.setIsDestAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				 myFileObj.setIsDestAgent(myFile.getIsDestAgent());
												 			}
												 			if(checkFlagSDA.equals("SDA")){
												 				myFileObj.setIsSubDestAgent(true);
												 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",myFile.getFileId(),myFile.getId(),"check" );
												 			}else{
												 				 myFileObj.setIsSubDestAgent(myFile.getIsSubDestAgent());
												 			}
											        	 myFileObj.setIsSecure(myFile.getIsSecure());
											        	 myFileObj.setLocation(myFile.getLocation());
											        	 myFileObj.setMapFolder(myFile.getMapFolder());
											        	 myFileObj.setCoverPageStripped(myFile.getCoverPageStripped());
											        	 myFileObj.setCreatedBy(getRequest().getRemoteUser());
											        	 myFileObj.setCreatedOn(new Date());
											        	 //myFileObj.setUpdatedBy(getRequest().getRemoteUser());
											        	 if(myFileObj.getCorpID().equals(sessionCorpID)){
											        		 	myFileObj.setUpdatedBy(getRequest().getRemoteUser());	
												         }else{
												        		myFileObj.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
												         }
											        	 myFileObj.setUpdatedOn(new Date());										        	
											        	 myFileObj.setDocumentCategory(myFile.getDocumentCategory());
											        	 myFileObj.setNetworkLinkId(networkLinkId);
											        	 
											        	 if(trackingStatus!=null &&  trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals("")))  
											        			 && (trackingStatus.getContractType().equals("CMM")|| trackingStatus.getContractType().equals("DMM")) ){
										        	    		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
										        	    		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
										        	    	if(trackingStatus.getAccNetworkGroup() && trackingStatusRemote!=null &&  trackingStatusRemote.getContractType()!=null 
										        	    				&& (!(trackingStatusRemote.getContractType().toString().trim().equals(""))) 
										        	    				&& (trackingStatusRemote.getContractType().equals("DMM")) && myFile.getFileType().equals("Invoice Client")){
										        	    		myFileObj.setFileType("Invoice Vendor");	
										        	    		}
										        	    	}
											        	 
											        	 if(trackingStatus!=null && trackingStatus.getShipNumber().equals(trackingStatus.getBookingAgentExSO()) 
											        			 && (trackingStatus.getOriginAgentExSO().equals(shipNumber) || trackingStatus.getDestinationAgentExSO().equals(shipNumber))
											        			 && myFile.getFileType().equals("Client Quote")){
											        		 myFileObj.setFileType("Agent Quote"); 
											        	 }
											        	 
											        	 myFileManager.save(myFileObj);
											     		 }else{
															 myFile = myFileManager.get(fid);
															 if(checkFlagBA.equals("BA")){
																 myFile.setIsBookingAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsBookingAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsBookingAgent(myFile.getIsBookingAgent());
													 			}
													 			if(checkFlagNA.equals("NA")){
													 				myFile.setIsNetworkAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsNetworkAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
													 			}
													 			if(checkFlagOA.equals("OA")){
													 				myFile.setIsOriginAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsOriginAgent(myFile.getIsOriginAgent());
													 			}
													 			if(checkFlagSOA.equals("SOA")){
													 				myFile.setIsSubOriginAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubOriginAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
													 			}
													 			if(checkFlagDA.equals("DA")){
													 				myFile.setIsDestAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsDestAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsDestAgent(myFile.getIsDestAgent());
													 			}
													 			if(checkFlagSDA.equals("SDA")){
													 				myFile.setIsSubDestAgent(true);
													 				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),"IsSubDestAgent",myFile.getFileId(),myFile.getId(),"check" );
													 			}else{
													 				myFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
													 			}
														 } 	 
										          }else{
										     			if(checkFlagBA.equals("BA")){
										    				myFile.setIsBookingAgent(true);
										    			}
										    			if(checkFlagNA.equals("NA")){
										    				myFile.setIsNetworkAgent(true);
										    			}
										    			if(checkFlagOA.equals("OA")){
										     				myFile.setIsOriginAgent(true);
										    			}
										    			if(checkFlagSOA.equals("SOA")){
										     				myFile.setIsSubOriginAgent(true);
										    			}
										    			if(checkFlagDA.equals("DA")){
										     				myFile.setIsDestAgent(true);
										    			}
										    			if(checkFlagSDA.equals("SDA")){
										     				myFile.setIsSubDestAgent(true);
										    			}
										    			myFileManager.save(myFile);
										    		}
										      }else{
									     			if(checkFlagBA.equals("BA")){
									    				myFile.setIsBookingAgent(true);
									    			}
									    			if(checkFlagNA.equals("NA")){
									    				myFile.setIsNetworkAgent(true);
									    			}
									    			if(checkFlagOA.equals("OA")){
									     				myFile.setIsOriginAgent(true);
									    			}
									    			if(checkFlagSOA.equals("SOA")){
									     				myFile.setIsSubOriginAgent(true);
									    			}
									    			if(checkFlagDA.equals("DA")){
									     				myFile.setIsDestAgent(true);
									    			}
									    			if(checkFlagSDA.equals("SDA")){
									     				myFile.setIsSubDestAgent(true);
									    			}
									    			myFileManager.save(myFile);
									    		}
									         }									
									}else{
						     			if(checkFlagBA.equals("BA")){
						    				myFile.setIsBookingAgent(true);
						    			}
						    			if(checkFlagNA.equals("NA")){
						    				myFile.setIsNetworkAgent(true);
						    			}
						    			if(checkFlagOA.equals("OA")){
						     				myFile.setIsOriginAgent(true);
						    			}
						    			if(checkFlagSOA.equals("SOA")){
						     				myFile.setIsSubOriginAgent(true);
						    			}
						    			if(checkFlagDA.equals("DA")){
						     				myFile.setIsDestAgent(true);
						    			}
						    			if(checkFlagSDA.equals("SDA")){
						     				myFile.setIsSubDestAgent(true);
						    			}
						    			myFileManager.save(myFile);
						    		}	
								}
							}
				}			
			
			if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
				customerFile = customerFileManager.get(id);
				myFile = myFileManager.get(fid);
				List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
				List linkedShipNumberList = null;
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					List shipNumberList1 = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if (shipNumberList1!=null && !shipNumberList1.isEmpty()) {
						Iterator shipIt =  shipNumberList1.iterator();
							while(shipIt.hasNext()){
								String shipNumberOld = (String) shipIt.next();
								if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("BA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("OA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("SOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("DA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("SDA"))){
						linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "checked");
					}
				}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 Long id = Long.parseLong(it.next().toString());
			        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
				        		 newMyFile.setIsBookingAgent(false);
					        	 newMyFile.setIsNetworkAgent(false);
					        	 newMyFile.setIsOriginAgent(false);   
					        	 newMyFile.setIsSubOriginAgent(false);
					        	 newMyFile.setIsDestAgent(false);
					        	 newMyFile.setIsSubDestAgent(false);
					        	 newMyFile.setNetworkLinkId("");
			        	 	}else{
			        	 		if(checkFlagBA.equals("BA")){
			        	 			newMyFile.setIsBookingAgent(true);
					 			}else{
					 				newMyFile.setIsBookingAgent(myFile.getIsBookingAgent());
					 			}
					 			if(checkFlagNA.equals("NA")){
					 				newMyFile.setIsNetworkAgent(true);
					 			}else{
					 				newMyFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
					 			}
					 			if(checkFlagOA.equals("OA")){
					 				newMyFile.setIsOriginAgent(true);
					 			}else{
					 				newMyFile.setIsOriginAgent(myFile.getIsOriginAgent());
					 			}
					 			if(checkFlagSOA.equals("SOA")){
					 				newMyFile.setIsSubOriginAgent(true);
					 			}else{
					 				newMyFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
					 			}
					 			if(checkFlagDA.equals("DA")){
					 				newMyFile.setIsDestAgent(true);
					 			}else{
					 				newMyFile.setIsDestAgent(myFile.getIsDestAgent());
					 			}
					 			if(checkFlagSDA.equals("SDA")){
					 				newMyFile.setIsSubDestAgent(true);
					 			}else{
					 				newMyFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
					 			}	
			        	    	      	    	
					        	 newMyFile.setIsCportal(myFile.getIsCportal());
					        	 newMyFile.setIsAccportal(myFile.getIsAccportal());
					        	 newMyFile.setIsPartnerPortal(myFile.getIsPartnerPortal());
					        	 newMyFile.setIsServiceProvider(myFile.getIsServiceProvider());
					        	 newMyFile.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 	  	}
				        	 	newMyFile.setDocumentCategory(myFile.getDocumentCategory());
				        	 	//newMyFile.setUpdatedBy(getRequest().getRemoteUser());
				        	 	 if(newMyFile.getCorpID().equals(sessionCorpID)){
				        	 		 newMyFile.setUpdatedBy(getRequest().getRemoteUser());	
						         }else{
						        	 newMyFile.setUpdatedBy("Networking"+":"+getRequest().getRemoteUser());
						         }
				        	 	newMyFile.setUpdatedOn(new Date());
			        	 myFileManager.save(newMyFile);	
			        }
			      }
				}
			}
}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	return SUCCESS;		
		
   }
	@SkipValidation
	public String unCheckMyFileIsCheckBox(){
		try {
			if (myFileFor != null && myFileFor.equals("SO")) {
			serviceOrder = serviceOrderManager.get(id);
			myFile = myFileManager.get(fid);
			String networkLinkId=myFile.getNetworkLinkId();
			List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
			Set set = new HashSet(linkedShipNumberList);
			List linkedSeqNum = new ArrayList(set);
			String valueTarget=""; 
			if(checkFlagBA.equalsIgnoreCase("NBA")){
				valueTarget="isBookingAgent";
			}else if(checkFlagNA.equalsIgnoreCase("NNA")){
				valueTarget="isNetworkAgent";
			}else if(checkFlagOA.equalsIgnoreCase("NOA")){
				valueTarget="isOriginAgent";
			}else if(checkFlagSOA.equalsIgnoreCase("NSOA")){
				valueTarget="isSubOriginAgent";
			}else if(checkFlagDA.equalsIgnoreCase("NDA")){
				valueTarget="isDestAgent";
			}else if(checkFlagSDA.equalsIgnoreCase("NSDA")){
				valueTarget="isSubDestAgent";
			}
			if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
				myFileManager.updateNetworkLinkIdCheck(networkLinkId,linkedSeqNum.get(0).toString(),valueTarget,myFile.getFileId(),myFile.getId(),"uncheck" );
			}
			if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
				serviceOrder = serviceOrderManager.get(id);
				myFile = myFileManager.get(fid);
				List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA"))){
						linkedShipNumberList = myFileManager.getLinkedAllShipNumber(serviceOrder.getShipNumber(),checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
					}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 Long id = Long.parseLong(it.next().toString());
			        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
				        		 /*newMyFile.setIsBookingAgent(false);
					        	 newMyFile.setIsNetworkAgent(false);
					        	 newMyFile.setIsOriginAgent(false);   
					        	 newMyFile.setIsSubOriginAgent(false);
					        	 newMyFile.setIsDestAgent(false);
					        	 newMyFile.setIsSubDestAgent(false);
					        	 newMyFile.setNetworkLinkId("");*/
			        	 	}else{
			        		 	if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) {
				        			 newMyFile.setIsBookingAgent(false);
			        	    	}else{
			        	    		 newMyFile.setIsBookingAgent(myFile.getIsBookingAgent());
			        	    	}
			        	    	if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) {
						        	 newMyFile.setIsNetworkAgent(false);

			        	    	}else{
						        	 newMyFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
			        	    	}
			        	    	if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) {
						        	 newMyFile.setIsOriginAgent(false);
			        	    	}else{
						        	 newMyFile.setIsOriginAgent(myFile.getIsOriginAgent());
			        	    	}
			        	    	if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) {
						        	 newMyFile.setIsSubOriginAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
			        	    	}
			        	    	if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) {
						        	 newMyFile.setIsDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsDestAgent(myFile.getIsDestAgent());
			        	    	}
			        	    	if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")) {
						        	 newMyFile.setIsSubDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
			        	    	}		        	    					        	 
			        	    	 newMyFile.setIsCportal(myFile.getIsCportal());
					        	 newMyFile.setIsAccportal(myFile.getIsAccportal());
					        	 newMyFile.setIsPartnerPortal(myFile.getIsPartnerPortal());
					        	 newMyFile.setIsServiceProvider(myFile.getIsServiceProvider());
					        	 newMyFile.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 	  }
			        	 	    newMyFile.setDocumentCategory(myFile.getDocumentCategory());
			        	 	newMyFile.setUpdatedBy(getRequest().getRemoteUser());
			        	 	newMyFile.setUpdatedOn(new Date());
			        	 myFileManager.save(newMyFile);	
			        }
			      }else{
			    	  if(checkFlagBA.equalsIgnoreCase("NBA")) {
			    		  myFile.setIsBookingAgent(false);
			 	    	}
			 	    	if(checkFlagNA.equalsIgnoreCase("NNA")) {
			 	    		myFile.setIsNetworkAgent(false);	
			 	    	}
			 	    	if(checkFlagOA.equalsIgnoreCase("NOA")) {
			 	    		myFile.setIsOriginAgent(false);
			 	    	}
			 	    	if(checkFlagSOA.equalsIgnoreCase("NSOA")) {
			 	    		myFile.setIsSubOriginAgent(false);
			 	    	}
			 	    	if(checkFlagDA.equalsIgnoreCase("NDA")) {
			 	    		myFile.setIsDestAgent(false);
			 	    	}
			 	    	if(checkFlagSDA.equalsIgnoreCase("NSDA")) {
			 	    		myFile.setIsSubDestAgent(false);
			 	    	}
			 	    	myFileManager.save(myFile);	
			      	}
				 }else{
			    	  if(checkFlagBA.equalsIgnoreCase("NBA")) {
			    		  myFile.setIsBookingAgent(false);
			 	    	}
			 	    	if(checkFlagNA.equalsIgnoreCase("NNA")) {
			 	    		myFile.setIsNetworkAgent(false);	
			 	    	}
			 	    	if(checkFlagOA.equalsIgnoreCase("NOA")) {
			 	    		myFile.setIsOriginAgent(false);
			 	    	}
			 	    	if(checkFlagSOA.equalsIgnoreCase("NSOA")) {
			 	    		myFile.setIsSubOriginAgent(false);
			 	    	}
			 	    	if(checkFlagDA.equalsIgnoreCase("NDA")) {
			 	    		myFile.setIsDestAgent(false);
			 	    	}
			 	    	if(checkFlagSDA.equalsIgnoreCase("NSDA")) {
			 	    		myFile.setIsSubDestAgent(false);
			 	    	}
			 	    	myFileManager.save(myFile);
				 }
			}
			if (myFileFor != null && myFileFor.equals("CF")) {
				customerFile = customerFileManager.get(id);
				myFile = myFileManager.get(fid);
				String networkLinkId=myFile.getNetworkLinkId();
				List previousSequenceNumberList = new ArrayList();
				List shipNumberList = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
				if ((shipNumberList!=null) && (!shipNumberList.isEmpty()) && (shipNumberList.get(0)!=null)) {
					Iterator shipIt =  shipNumberList.iterator();
						while(shipIt.hasNext()){
							String shipNumberOld = (String) shipIt.next();
				List defCheckedShipNumber = myFileManager.getDefCheckedShipNumber(shipNumberOld);
				if(defCheckedShipNumber!=null && !defCheckedShipNumber.isEmpty()){
					checkAgent = defCheckedShipNumber.get(0).toString();				    
				}
				List linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
				if((linkedShipNumberList!=null) && (!linkedShipNumberList.isEmpty()) && (linkedShipNumberList.get(0)!=null)){
				Set set = new HashSet(linkedShipNumberList);
				List linkedSeqNum = new ArrayList(set);
				 Iterator it = linkedSeqNum.iterator();
			     while (it.hasNext()) {
			    	 	 String shipNumber=(String)it.next();
			    	 	String sequenceNumber = shipNumber.substring(0, shipNumber.length()-2);
			    		String valueTarget=""; 
						if(checkFlagBA.equalsIgnoreCase("NBA")){
							valueTarget="isBookingAgent";
						}else if(checkFlagNA.equalsIgnoreCase("NNA")){
							valueTarget="isNetworkAgent";
						}else if(checkFlagOA.equalsIgnoreCase("NOA")){
							valueTarget="isOriginAgent";
						}else if(checkFlagSOA.equalsIgnoreCase("NSOA")){
							valueTarget="isSubOriginAgent";
						}else if(checkFlagDA.equalsIgnoreCase("NDA")){
							valueTarget="isDestAgent";
						}else if(checkFlagSDA.equalsIgnoreCase("NSDA")){
							valueTarget="isSubDestAgent";
						}
						if(linkedSeqNum!=null && !linkedSeqNum.isEmpty()){
							myFileManager.updateNetworkLinkIdCheck(networkLinkId,sequenceNumber.toString(),valueTarget,myFile.getFileId(),myFile.getId(),"uncheck" );
						}
			     }
			}else{
			  	  if(checkFlagBA.equalsIgnoreCase("NBA")) {
					  myFile.setIsBookingAgent(false);
				    	}
				    	if(checkFlagNA.equalsIgnoreCase("NNA")) {
				    		myFile.setIsNetworkAgent(false);	
				    	}
				    	if(checkFlagOA.equalsIgnoreCase("NOA")) {
				    		myFile.setIsOriginAgent(false);
				    	}
				    	if(checkFlagSOA.equalsIgnoreCase("NSOA")) {
				    		myFile.setIsSubOriginAgent(false);
				    	}
				    	if(checkFlagDA.equalsIgnoreCase("NDA")) {
				    		myFile.setIsDestAgent(false);
				    	}
				    	if(checkFlagSDA.equalsIgnoreCase("NSDA")) {
				    		myFile.setIsSubDestAgent(false);
				    	}
				    	myFileManager.save(myFile);
				}
			}	
 	}
			
			if(myFile.getNetworkLinkId()!=null && !(myFile.getNetworkLinkId().equalsIgnoreCase(""))){
				customerFile = customerFileManager.get(id);
				myFile = myFileManager.get(fid);
				List linkedIdList = myFileManager.findlinkedIdList(networkLinkId);
				List linkedShipNumberList = null;
				if(linkedIdList.size() > 1 && linkedIdList!=null && !linkedIdList.isEmpty()){
					List shipNumberList1 = myFileManager.findShipNumberBySequenceNumer(customerFile.getSequenceNumber(),sessionCorpID,customerFile.getIsNetworkRecord());
					if (shipNumberList1!=null && !shipNumberList1.isEmpty()) {
						Iterator shipIt =  shipNumberList1.iterator();
							while(shipIt.hasNext()){
								String shipNumberOld = (String) shipIt.next();
								if(((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) || ((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) || ((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) || ((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) || ((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) || ((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA"))){
									linkedShipNumberList = myFileManager.getLinkedAllShipNumber(shipNumberOld,checkFlagBA, checkFlagNA, checkFlagOA, checkFlagSOA,checkFlagDA, checkFlagSDA, "unchecked");
								}
							}
					Iterator it = linkedIdList.iterator();
			         while (it.hasNext()) {
			        	 Long id = Long.parseLong(it.next().toString());
			        	 MyFile newMyFile = myFileManager.getForOtherCorpid(id);
			        	 	if(linkedShipNumberList!=null && !linkedShipNumberList.isEmpty() && linkedShipNumberList.contains(newMyFile.getFileId())){
				        		 newMyFile.setIsBookingAgent(false);
					        	 newMyFile.setIsNetworkAgent(false);
					        	 newMyFile.setIsOriginAgent(false);   
					        	 newMyFile.setIsSubOriginAgent(false);
					        	 newMyFile.setIsDestAgent(false);
					        	 newMyFile.setIsSubDestAgent(false);
					        	 newMyFile.setNetworkLinkId("");
			        	 	}else{
			        	 		if((!(checkFlagBA.equalsIgnoreCase(""))) && checkFlagBA.equalsIgnoreCase("NBA")) {
				        			 newMyFile.setIsBookingAgent(false);
			        	    	}else{
			        	    		 newMyFile.setIsBookingAgent(myFile.getIsBookingAgent());
			        	    	}
			        	    	if((!(checkFlagNA.equalsIgnoreCase(""))) && checkFlagNA.equalsIgnoreCase("NNA")) {
						        	 newMyFile.setIsNetworkAgent(false);

			        	    	}else{
						        	 newMyFile.setIsNetworkAgent(myFile.getIsNetworkAgent());
			        	    	}
			        	    	if((!(checkFlagOA.equalsIgnoreCase(""))) && checkFlagOA.equalsIgnoreCase("NOA")) {
						        	 newMyFile.setIsOriginAgent(false);
			        	    	}else{
						        	 newMyFile.setIsOriginAgent(myFile.getIsOriginAgent());
			        	    	}
			        	    	if((!(checkFlagSOA.equalsIgnoreCase(""))) && checkFlagSOA.equalsIgnoreCase("NSOA")) {
						        	 newMyFile.setIsSubOriginAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubOriginAgent(myFile.getIsSubOriginAgent());
			        	    	}
			        	    	if((!(checkFlagDA.equalsIgnoreCase(""))) && checkFlagDA.equalsIgnoreCase("NDA")) {
						        	 newMyFile.setIsDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsDestAgent(myFile.getIsDestAgent());
			        	    	}
			        	    	if((!(checkFlagSDA.equalsIgnoreCase(""))) && checkFlagSDA.equalsIgnoreCase("NSDA")) {
						        	 newMyFile.setIsSubDestAgent(false);
			        	    	}else{
						        	 newMyFile.setIsSubDestAgent(myFile.getIsSubDestAgent());
			        	    	}		        	    	      	    	
					        	 newMyFile.setIsCportal(myFile.getIsCportal());
					        	 newMyFile.setIsAccportal(myFile.getIsAccportal());
					        	 newMyFile.setIsPartnerPortal(myFile.getIsPartnerPortal());
					        	 newMyFile.setIsServiceProvider(myFile.getIsServiceProvider());
					        	 newMyFile.setInvoiceAttachment(myFile.getInvoiceAttachment());
			        	 	  	}
				        	 	newMyFile.setDocumentCategory(myFile.getDocumentCategory());
				        	 	newMyFile.setUpdatedBy(getRequest().getRemoteUser());
				        	 	newMyFile.setUpdatedOn(new Date());
			        	 myFileManager.save(newMyFile);	
			        }
			      }else{
			    	  if(checkFlagBA.equalsIgnoreCase("NBA")) {
			    		  myFile.setIsBookingAgent(false);
			 	    	}
			 	    	if(checkFlagNA.equalsIgnoreCase("NNA")) {
			 	    		myFile.setIsNetworkAgent(false);	
			 	    	}
			 	    	if(checkFlagOA.equalsIgnoreCase("NOA")) {
			 	    		myFile.setIsOriginAgent(false);
			 	    	}
			 	    	if(checkFlagSOA.equalsIgnoreCase("NSOA")) {
			 	    		myFile.setIsSubOriginAgent(false);
			 	    	}
			 	    	if(checkFlagDA.equalsIgnoreCase("NDA")) {
			 	    		myFile.setIsDestAgent(false);
			 	    	}
			 	    	if(checkFlagSDA.equalsIgnoreCase("NSDA")) {
			 	    		myFile.setIsSubDestAgent(false);
			 	    	}
			 	    	myFileManager.save(myFile);	
			      	}
				}else{
			    	  if(checkFlagBA.equalsIgnoreCase("NBA")) {
			    		  myFile.setIsBookingAgent(false);
			 	    	}
			 	    	if(checkFlagNA.equalsIgnoreCase("NNA")) {
			 	    		myFile.setIsNetworkAgent(false);	
			 	    	}
			 	    	if(checkFlagOA.equalsIgnoreCase("NOA")) {
			 	    		myFile.setIsOriginAgent(false);
			 	    	}
			 	    	if(checkFlagSOA.equalsIgnoreCase("NSOA")) {
			 	    		myFile.setIsSubOriginAgent(false);
			 	    	}
			 	    	if(checkFlagDA.equalsIgnoreCase("NDA")) {
			 	    		myFile.setIsDestAgent(false);
			 	    	}
			 	    	if(checkFlagSDA.equalsIgnoreCase("NSDA")) {
			 	    		myFile.setIsSubDestAgent(false);
			 	    	}
			 	    	myFileManager.save(myFile);	
			      	}
			}
}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
	return SUCCESS;
	}
	@SkipValidation
	public String docMapFolder() {
		try {
			docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
			mapList = refMasterManager.findMapList(sessionCorpID, "DOCUMENT");
			docMapList = myFileManager.getMapFolder(fileType, sessionCorpID);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String docCategoryByDocTypeDetail() {
		try {
			//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");sessionCorpID,docType	
			if(soCorpId==null || soCorpId.equals("")){
				soCorpId=sessionCorpID; 
			}
			docCategory = refMasterManager.findDocCategoryByDocType(soCorpId, docType);
			if(docCategory==null || docCategory.equals("")){
				docCategory = "General";
			}
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		//logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String getAllMyFiles(){
		try {
			myFiles = myFileManager.getAllMyfile(fileId,sessionCorpID); 
			notes =notesManager.get(id);
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String updateNotesMyFileList(){
 		 try {
			notesManager.updateNoteFileList(id,myFileIds,sessionCorpID);
			getAllMyFiles();
		} catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  errorLogManager.saveLogError(sessionCorpID,new Date(),logMessage,logMethod,getRequest().getRemoteUser() , e.getStackTrace()[0].getFileName().toString().replace(".java", ""));
	    	 e.printStackTrace();
	    	 return "errorlog";
		}
 		 return SUCCESS;
	}
	private String reportSubject="";
	/*private void sendEmailFileCabinet()
	{
		if((noteFor!=null)&&(noteFor.trim().equalsIgnoreCase("serviceOrder"))){
			�Document for <first name> <Last name> #�  <S/O #>�by default
			reportSubject="Document for"+": "+(((serviceOrder.getFirstName()!=null)&&(!serviceOrder.getFirstName().equalsIgnoreCase("")))?serviceOrder.getFirstName()+",":" ")+""+(((serviceOrder.getLastName()!=null)&&(!serviceOrder.getLastName().equalsIgnoreCase("")))?serviceOrder.getLastName():" ")+": "+serviceOrder.getShipNumber()+"";

		}
	}
	*/
	private EmailSetup emailSetup;
	private String reportBody;
	private String reportSignature;
	private String recipientTo;
	private String recipientCC;
	//Map<String,String> reportSubjectSo= new HashMap<String, String>();
	private ReportEmailDTO reportEmail;
	private Map<String, String> partnerCodeList = new TreeMap<String, String>(); 
	private String myFileIdCheck;
	@SkipValidation
	public String attachDocInEmail()
	{
		if((reportSignaturePart==null)||(reportSignaturePart.trim().equalsIgnoreCase(""))){
		List list = customerFileManager.findDefaultBookingAgentCode(companyDivision);
		if((list!=null)&& (!list.isEmpty())&&(list.get(0)!=null)){
			CompanyDivision companyDivision  = (CompanyDivision)list.get(0);
			String description = companyDivision.getDescription();
			if(description == null || description.equals(""))
			{
				description="";	
			}
			
			String compWebsite = companyDivision.getCompanyWebsite();
			if(compWebsite == null || compWebsite.equals(""))
			{
				compWebsite="";	
			}
			
			String BillingCity = companyDivision.getBillingCity();
			if(BillingCity == null || BillingCity.equals(""))
			{
				BillingCity="";	
			}
			
			String BillingZip = companyDivision.getBillingZip();
			if(BillingZip == null || BillingZip.equals(""))
			{
				BillingZip="";	
			}
			
			String BillingPhone = companyDivision.getBillingPhone();
			if(BillingPhone == null || BillingPhone.equals(""))
			{
				BillingPhone="";	
			}
			
			this.reportSignaturePart=(description+"\n")+""+(BillingCity+"\n")+""+(BillingZip+"\n")+""+(BillingPhone+"\n")+""+compWebsite;				
		} }
		if((noteFor!=null)&&(noteFor.trim().equalsIgnoreCase("customerFile"))){
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(jobNumber);
			CustomerFile customerFile=new CustomerFile();
			if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}
				
				if((customerFile.getOriginAgentCode()!=null)&&(!customerFile.getOriginAgentCode().equalsIgnoreCase("")))
					partnerCodeList.put(customerFile.getOriginAgentCode(),customerFile.getOriginAgentName());

				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase("")))
					partnerCodeList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				
				if((customerFile.getAccountCode()!=null)&&(!customerFile.getAccountCode().equalsIgnoreCase("")))
				{
					partnerCodeList.put(customerFile.getAccountCode(),customerFile.getAccountName());
				}
				if(checkBoxEmailId.contains(","))
				{
		    	 String fieldId1="";
		    	 String lName1="";
		    	 String fName1="";
		    	 String lName="";
		    	 String fName="";
		    	 String subjectSo="";
		    	 Map<String,String> reportSubjectSo= new HashMap<String, String>();
				String[] checkBoxEmailId1 = checkBoxEmailId.split(",");
				int emailIdLength = checkBoxEmailId1.length;
				for (int i = 0; i < emailIdLength; i++) {
			    String idInArray12 = checkBoxEmailId1[i];
			    Long idArray1 = Long.parseLong(idInArray12);
			   myFile = myFileManager.get(idArray1);
			   String fieldId = myFile.getFileId();
			   if(!fieldId1.contains(fieldId)){
				if(fieldId.length()==11) {
				   List l1=customerFileManager.findSequenceNumber(fieldId);
				   CustomerFile customerFile1 = (CustomerFile) l1.get(0);
				
				   if(reportSubjectSo.containsKey(customerFile1.getFirstName()+" "+customerFile1.getLastName())){
					 String temp=reportSubjectSo.get(customerFile1.getFirstName()+" "+customerFile1.getLastName());
					 temp=temp+","+customerFile1.getSequenceNumber();
					 reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), temp);
				   }else{
					   reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), customerFile1.getSequenceNumber());  
				   }
						   
				}else{
					   List l=serviceOrderManager.findShipNumber(fieldId);
						ServiceOrder serviceOrder1 = (ServiceOrder) l.get(0);
						if(reportSubjectSo.containsKey(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName()))
						{
							String temp=reportSubjectSo.get(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName());
						     temp=temp+","+serviceOrder1.getShipNumber();
						     reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), temp);
						}else
						{
							reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), serviceOrder1.getShipNumber());
						}
				  }
				fieldId1=fieldId1+","+fieldId; 
			   }
			
				}
				   for (Map.Entry<String, String> entry : reportSubjectSo.entrySet()) {
				        String key = entry.getKey();
				        String value = entry.getValue();
				        if(subjectSo.equals("")){
				        	subjectSo="Document for"+" "+key+"-"+value;
				        }else{
				        	subjectSo=subjectSo+","+"Document for"+" "+key+"-"+value;
				        }
				    }
				reportSubject=	subjectSo;
				}else
				{
					reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				}
				
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
				
		     }
		     
		}
		
		if((noteFor!=null)&&(noteFor.trim().equalsIgnoreCase("agentQuotes"))){
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(jobNumber);
			CustomerFile customerFile=new CustomerFile();
		     if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}
				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase(""))){

					partnerCodeList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				}
				//reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				if(checkBoxEmailId.contains(","))
				{
		    	 String fieldId1="";
		    	 String lName1="";
		    	 String fName1="";
		    	 String lName="";
		    	 String fName="";
		    	 String subjectSo="";
		    	 Map<String,String> reportSubjectSo= new HashMap<String, String>();
				String[] checkBoxEmailId1 = checkBoxEmailId.split(",");
				int emailIdLength = checkBoxEmailId1.length;
				for (int i = 0; i < emailIdLength; i++) {
			    String idInArray12 = checkBoxEmailId1[i];
			    Long idArray1 = Long.parseLong(idInArray12);
			   myFile = myFileManager.get(idArray1);
			   String fieldId = myFile.getFileId();
			   if(!fieldId1.contains(fieldId)){
				if(fieldId.length()==11) {
				   List l1=customerFileManager.findSequenceNumber(fieldId);
				   CustomerFile customerFile1 = (CustomerFile) l1.get(0);
				
				   if(reportSubjectSo.containsKey(customerFile1.getFirstName()+" "+customerFile1.getLastName())){
					 String temp=reportSubjectSo.get(customerFile1.getFirstName()+" "+customerFile1.getLastName());
					 temp=temp+","+customerFile1.getSequenceNumber();
					 reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), temp);
				   }else{
					   reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), customerFile1.getSequenceNumber());  
				   }
						   
				}else{
					   List l=serviceOrderManager.findShipNumber(fieldId);
						ServiceOrder serviceOrder1 = (ServiceOrder) l.get(0);
						if(reportSubjectSo.containsKey(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName()))
						{
							String temp=reportSubjectSo.get(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName());
						     temp=temp+","+serviceOrder1.getShipNumber();
						     reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), temp);
						}else
						{
							reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), serviceOrder1.getShipNumber());
						}
				  }
				fieldId1=fieldId1+","+fieldId; 
			   }
			
				}
				   for (Map.Entry<String, String> entry : reportSubjectSo.entrySet()) {
				        String key = entry.getKey();
				        String value = entry.getValue();
				        if(subjectSo.equals("")){
				        	subjectSo="Document for"+" "+key+"-"+value;
				        }else{
				        	subjectSo=subjectSo+","+"Document for"+" "+key+"-"+value;
				        }
				    }
				reportSubject=	subjectSo;
				}else
				{
					reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				}
				
				
				
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
		     }
		}
		if((noteFor!=null)&&(noteFor.trim().equalsIgnoreCase("agentQuotesSO"))){
			try{
			List<CustomerFile> customerFileList = customerFileManager.findSequenceNumber(serviceOrderManager.get(Long.parseLong((serviceOrderManager.findIdByShipNumber(jobNumber)).get(0).toString())).getSequenceNumber());
			CustomerFile customerFile=new CustomerFile();
			if (!(customerFileList == null || customerFileList.isEmpty())) {
				for (CustomerFile customerFileTemp : customerFileList) {
					customerFile=customerFileTemp;
				}
				if((customerFile.getBillToCode()!=null)&&(!customerFile.getBillToCode().equalsIgnoreCase(""))){

					partnerCodeList.put(customerFile.getBillToCode(),customerFile.getBillToName());
				}
				//reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				if(checkBoxEmailId.contains(","))
				{
		    	 String fieldId1="";
		    	 String lName1="";
		    	 String fName1="";
		    	 String lName="";
		    	 String fName="";
		    	 String subjectSo="";
		    	 Map<String,String> reportSubjectSo= new HashMap<String, String>();
				String[] checkBoxEmailId1 = checkBoxEmailId.split(",");
				int emailIdLength = checkBoxEmailId1.length;
				for (int i = 0; i < emailIdLength; i++) {
			    String idInArray12 = checkBoxEmailId1[i];
			    Long idArray1 = Long.parseLong(idInArray12);
			   myFile = myFileManager.get(idArray1);
			   String fieldId = myFile.getFileId();
			   if(!fieldId1.contains(fieldId)){
				if(fieldId.length()==11) {
				   List l1=customerFileManager.findSequenceNumber(fieldId);
				   CustomerFile customerFile1 = (CustomerFile) l1.get(0);
				
				   if(reportSubjectSo.containsKey(customerFile1.getFirstName()+" "+customerFile1.getLastName())){
					 String temp=reportSubjectSo.get(customerFile1.getFirstName()+" "+customerFile1.getLastName());
					 temp=temp+","+customerFile1.getSequenceNumber();
					 reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), temp);
				   }else{
					   reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), customerFile1.getSequenceNumber());  
				   }
						   
				}else{
					   List l=serviceOrderManager.findShipNumber(fieldId);
						ServiceOrder serviceOrder1 = (ServiceOrder) l.get(0);
						if(reportSubjectSo.containsKey(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName()))
						{
							String temp=reportSubjectSo.get(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName());
						     temp=temp+","+serviceOrder1.getShipNumber();
						     reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), temp);
						}else
						{
							reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), serviceOrder1.getShipNumber());
						}
				  }
				fieldId1=fieldId1+","+fieldId; 
			   }
			
				}
				   for (Map.Entry<String, String> entry : reportSubjectSo.entrySet()) {
				        String key = entry.getKey();
				        String value = entry.getValue();
				        if(subjectSo.equals("")){
				        	subjectSo="Document for"+" "+key+"-"+value;
				        }else{
				        	subjectSo=subjectSo+","+"Document for"+" "+key+"-"+value;
				        }
				    }
				reportSubject=	subjectSo;
				}else
				{
					reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				}
		     
				
				
				if((customerFile.getEmail()!=null)&&(!customerFile.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(customerFile.getId());
				reportEmail.setFirstName(customerFile.getFirstName());
				reportEmail.setLastName(customerFile.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(customerFile.getEmail());
				}
		     }}catch(Exception e){}
		}		
		
		if((noteFor!=null)&&(noteFor.trim().equalsIgnoreCase("serviceOrder"))){
		List<ServiceOrder> serviceOrderList = serviceOrderManager.findShipNumber(jobNumber);
		ServiceOrder serviceOrder=new ServiceOrder();
	     if (!(serviceOrderList == null || serviceOrderList.isEmpty())) {
			for (ServiceOrder serviceOrderTemp : serviceOrderList) {
				serviceOrder=serviceOrderTemp;
			}
			if((trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerCode(),trackingStatusManager.get(serviceOrder.getId()).getNetworkPartnerName());
			if((trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginAgent());
			if((trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationAgent());
			if((trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getOriginSubAgent());
			if((trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgentCode(),trackingStatusManager.get(serviceOrder.getId()).getDestinationSubAgent());
			if((trackingStatusManager.get(serviceOrder.getId()).getBrokerCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getBrokerCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getBrokerCode(),trackingStatusManager.get(serviceOrder.getId()).getBrokerName());
			if((trackingStatusManager.get(serviceOrder.getId()).getForwarderCode()!=null)&&(!trackingStatusManager.get(serviceOrder.getId()).getForwarderCode().equalsIgnoreCase("")))
				partnerCodeList.put(trackingStatusManager.get(serviceOrder.getId()).getForwarderCode(),trackingStatusManager.get(serviceOrder.getId()).getForwarder());
			if((billingManager.get(serviceOrder.getId()).getBillToCode()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillToCode().equalsIgnoreCase("")))
				partnerCodeList.put(billingManager.get(serviceOrder.getId()).getBillToCode(),billingManager.get(serviceOrder.getId()).getBillToName());
			if((billingManager.get(serviceOrder.getId()).getBillTo2Code()!=null)&&(!billingManager.get(serviceOrder.getId()).getBillTo2Code().equalsIgnoreCase("")))
				partnerCodeList.put(billingManager.get(serviceOrder.getId()).getBillTo2Code(),billingManager.get(serviceOrder.getId()).getBillTo2Name());
	     }
	     if(checkBoxEmailId.contains(","))
			{
	    	 String fieldId1="";
	    	 String lName1="";
	    	 String fName1="";
	    	 String lName="";
	    	 String fName="";
	    	 String subjectSo="";
	    	 Map<String,String> reportSubjectSo= new HashMap<String, String>();
			String[] checkBoxEmailId1 = checkBoxEmailId.split(",");
			int emailIdLength = checkBoxEmailId1.length;
			for (int i = 0; i < emailIdLength; i++) {
		    String idInArray12 = checkBoxEmailId1[i];
		    Long idArray1 = Long.parseLong(idInArray12);
		   myFile = myFileManager.get(idArray1);
		   String fieldId = myFile.getFileId();
		   if(!fieldId1.contains(fieldId)){
			if(fieldId.length()==11) {
			   List l1=customerFileManager.findSequenceNumber(fieldId);
			   CustomerFile customerFile1 = (CustomerFile) l1.get(0);
			
			   if(reportSubjectSo.containsKey(customerFile1.getFirstName()+" "+customerFile1.getLastName())){
				 String temp=reportSubjectSo.get(customerFile1.getFirstName()+" "+customerFile1.getLastName());
				 temp=temp+","+customerFile1.getSequenceNumber();
				 reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), temp);
			   }else{
				   reportSubjectSo.put(customerFile1.getFirstName()+" "+customerFile1.getLastName(), customerFile1.getSequenceNumber());  
			   }
					   
			}else{
				   List l=serviceOrderManager.findShipNumber(fieldId);
					ServiceOrder serviceOrder1 = (ServiceOrder) l.get(0);
					if(reportSubjectSo.containsKey(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName()))
					{
						String temp=reportSubjectSo.get(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName());
					     temp=temp+","+serviceOrder1.getShipNumber();
					     reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), temp);
					}else
					{
						reportSubjectSo.put(serviceOrder1.getFirstName()+" "+serviceOrder1.getLastName(), serviceOrder1.getShipNumber());
					}
			  }
			fieldId1=fieldId1+","+fieldId; 
		   }
		
			}
			   for (Map.Entry<String, String> entry : reportSubjectSo.entrySet()) {
			        String key = entry.getKey();
			        String value = entry.getValue();
			        if(subjectSo.equals("")){
			        	subjectSo="Document for"+" "+key+"-"+value;
			        }else{
			        	subjectSo=subjectSo+","+"Document for"+" "+key+"-"+value;
			        }
			    }
			reportSubject=	subjectSo;
			}else
			{
				//reportSubject="Document for"+" "+(((customerFile.getFirstName()!=null)&&(!customerFile.getFirstName().equalsIgnoreCase("")))?customerFile.getFirstName():" ")+" "+(((customerFile.getLastName()!=null)&&(!customerFile.getLastName().equalsIgnoreCase(" ")))?customerFile.getLastName():"  ")+ " -"+customerFile.getSequenceNumber()+"";		
				reportSubject="Document for"+" "+(((serviceOrder.getFirstName()!=null)&&(!serviceOrder.getFirstName().equalsIgnoreCase("")))?serviceOrder.getFirstName():" ")+" "+(((serviceOrder.getLastName()!=null)&&(!serviceOrder.getLastName().equalsIgnoreCase(" ")))?serviceOrder.getLastName():"  ")+ " -"+serviceOrder.getShipNumber()+"";
			}
	     
	     
	     
	     
	    // reportSubject="Document for"+" "+(((serviceOrder.getFirstName()!=null)&&(!serviceOrder.getFirstName().equalsIgnoreCase("")))?serviceOrder.getFirstName():" ")+" "+(((serviceOrder.getLastName()!=null)&&(!serviceOrder.getLastName().equalsIgnoreCase(" ")))?serviceOrder.getLastName():"  ")+ " -"+serviceOrder.+"";
		
	     if((serviceOrder.getEmail()!=null)&&(!serviceOrder.getEmail().equalsIgnoreCase(""))){
				reportEmail=new ReportEmailDTO();
				reportEmail.setId(serviceOrder.getId());
				reportEmail.setFirstName(serviceOrder.getFirstName());
				reportEmail.setLastName(serviceOrder.getLastName());
				reportEmail.setUserName("");
				reportEmail.setEmail(serviceOrder.getEmail());
				}
		}
		try {
			if(checkBoxEmailId.contains(",")){
 String[] arrayId1 = checkBoxEmailId.split(",");
   int arrayLength = arrayId1.length;
   for (int i = 0; i < arrayLength; i++) {
			String idInAray1 = arrayId1[i];
			Long idArray1 = Long.parseLong(idInAray1);
			myFile = myFileManager.get(idArray1);
			myFile.getFileFileName();
			if(myFileLocation.equals("")){
			myFileLocation = myFile.getLocation();
			}
			else{
			myFileLocation = myFileLocation +"~"+myFile.getLocation();
			}
   }}
			else
			{
				Long idArray1 = Long.parseLong(checkBoxEmailId);
				myFile = myFileManager.get(idArray1);
				myFile.getFileFileName();
			    if(myFileLocation.equals("")){
			    myFileLocation = myFile.getLocation();
			    }
			    else{
			    	myFileLocation = myFileLocation +"~"+myFile.getLocation();
				}
			   }
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
	
		return SUCCESS;
	}
	String emailFlag ="0";
	public String sendEmailFileCabi()
	{
		
		attachDocInEmail();
		String tempRecipient="";
		String tempRecipientArr[]=recipientTo.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		recipientTo=tempRecipient;	
		
		tempRecipient="";
		tempRecipientArr=recipientCC.split(",");
		for(String str1:tempRecipientArr){
			if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
				if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
				tempRecipient += str1;
			}
		}
		String fileId=getRequest().getParameter("myFileId");
		if(fileId==null){
			fileId="";
		}
		recipientCC=tempRecipient;
			EmailSetup emailSetup = new EmailSetup();
			emailSetup.setRetryCount(0);
			emailSetup.setRecipientTo(recipientTo);
			emailSetup.setRecipientCc(recipientCC);
			emailSetup.setRecipientBcc("");
			emailSetup.setSubject(reportSubject);
			reportBody=reportBody.replaceAll("\r\n", "<br>");
			emailSetup.setBody(reportBody);
			emailSetup.setSignature(user1.getEmail());
			emailSetup.setAttchedFileLocation(myFileLocation);
			emailSetup.setDateSent(new Date());
			emailSetup.setEmailStatus("SaveForEmail");
			emailSetup.setCorpId(sessionCorpID);
			emailSetup.setCreatedOn(new Date());
			emailSetup.setCreatedBy("EMAILSETUP");
			emailSetup.setUpdatedOn(new Date());
			emailSetup.setUpdatedBy("EMAILSETUP");
			//reportSignaturePart = reportSignaturePart.replaceAll("\n", "<br>");
			reportSignaturePart = reportSignaturePart.replaceAll("\r\n", "<br>");
			emailSetup.setSignaturePart(reportSignaturePart);
			emailSetup.setModule(reportModule);
			emailSetup.setFileNumber(checkBoxEmailId);		
			emailSetup=emailSetupManager.save(emailSetup);
			
			String[] myFileIdCheckArray = myFileIdCheck.split(",");
			for (int i = 0; i < myFileIdCheckArray.length; i++) {
			    String idInArray12 = myFileIdCheckArray[i];
			    Long idArray1 = Long.parseLong(idInArray12);
			    myFile = myFileManager.get(idArray1);
			    myFile.setEmailStatus(emailSetup.getId()+"");
			    myFile.setUpdatedBy(getRequest().getRemoteUser());
			    myFile.setUpdatedOn(new Date());
			    myFileManager.save(myFile);
			}
		//emailSetupManager.globalEmailSetupProcess(user1.getEmail(), recipientTo, recipientCC, "", myFileLocation , reportBody , reportSubject , sessionCorpID, reportModule ,checkBoxEmailId,reportSignaturePart);
	emailFlag="1";
	/* EmailSetup emailSetup = new EmailSetup() ;
	emailSetup.setFileNumber(jobNumber);
	
	emailSetupManager.save(emailSetup);*/
	return SUCCESS;
	}
	
	@SkipValidation
	public String recipientWithEmailStatus()
	{
		String fileId=getRequest().getParameter("myFileId");
		if(fileId==null){
			fileId="";
		}
		recipientWithEmailList = myFileManager.recipientWithEmailStatus(jobNumber ,noteFor,sessionCorpID,fileId);
		return SUCCESS;
	}
	
	public String getRelTo() {
		return relTo;
	}

	public void setRelTo(String relTo) {
		this.relTo = relTo;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setMyFileFor(String myFileFor) {
		this.myFileFor = myFileFor;
	}

	public void setMyFileFrom(String myFileFrom) {
		this.myFileFrom = myFileFrom;
	}

	public MyFile getMyFile() {
		return myFile;
	}

	public void setMyFile(MyFile myFile) {
		this.myFile = myFile;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public void setMyFileManager(MyFileManager myFileManager) {
		this.myFileManager = myFileManager;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public RefMaster getRefMaster() {
		return refMaster;
	}

	public void setRefMaster(RefMaster refMaster) {
		this.refMaster = refMaster;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getMyFiles() {
		return myFiles;
	}

	public Map<String, String> getDocsList() {
		return docsList;
	}

	public void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public File getFile() {
		return file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public String getDocId() {
		return did;
	}

	public void setDid(String did) {
		this.did = did;
	}

	public String getPopup() {
		return popup;
	}

	public void setPopup(String popup) {
		this.popup = popup;
	}

	public String getFileNotExists() {
		return fileNotExists;
	}

	public void setFileNotExists(String fileNotExists) {
		this.fileNotExists = fileNotExists;
	}

	public String getFileType() {
		return fileType;
	}

	public String getRelatedDocs() {
		return relatedDocs;
	}

	public void setRelatedDocs(String relatedDocs) {
		this.relatedDocs = relatedDocs;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public ServicePartner getServicePartner() {
		return servicePartner;
	}

	public void setServicePartner(ServicePartner servicePartner) {
		this.servicePartner = servicePartner;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}

	public Carton getCarton() {
		return carton;
	}

	public void setCarton(Carton carton) {
		this.carton = carton;
	}

	public Container getContainer() {
		return container;
	}

	public void setContainer(Container container) {
		this.container = container;
	}

	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Long getFid() {
		return fid;
	}

	public void setFid(Long fid) {
		this.fid = fid;
	}

	public String getMyFileFor() {
		return myFileFor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCheckBoxId() {
		return checkBoxId;
	}

	public void setCheckBoxId(String checkBoxId) {
		this.checkBoxId = checkBoxId;
	}

	public Boolean getIsCportal() {
		return isCportal;
	}

	public void setIsCportal(Boolean isCportal) {
		this.isCportal = isCportal;
	}

	public List getUpdateMyfileStatusList() {
		return updateMyfileStatusList;
	}

	public void setUpdateMyfileStatusList(List updateMyfileStatusList) {
		this.updateMyfileStatusList = updateMyfileStatusList;
	}

	public Long getIds() {
		return ids;
	}

	public void setIds(Long ids) {
		this.ids = ids;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Boolean getIsAccportal() {
		return isAccportal;
	}

	public void setIsAccportal(Boolean isAccportal) {
		this.isAccportal = isAccportal;
	}

	public String getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Boolean getIsPPortal() {
		return isPPortal;
	}

	public void setIsPPortal(Boolean isPPortal) {
		this.isPPortal = isPPortal;
	}

	public String getRId() {
		return rId;
	}

	public void setRId(String id) {
		rId = id;
	}

	public List getDocAccessControlList() {
		return docAccessControlList;
	}

	public void setDocAccessControlList(List docAccessControlList) {
		this.docAccessControlList = docAccessControlList;
	}

	public Boolean getIsPartnerPortal() {
		return isPartnerPortal;
	}

	public void setIsPartnerPortal(Boolean isPartnerPortal) {
		this.isPartnerPortal = isPartnerPortal;
	}

	public List getSoNameList() {
		return soNameList;
	}

	public void setSoNameList(List soNameList) {
		this.soNameList = soNameList;
	}

	public String getSoName() {
		return soName;
	}

	public void setSoName(String soName) {
		this.soName = soName;
	}

	public String getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(String pageIndex) {
		this.pageIndex = pageIndex;
	}

	public String getHitflag() {
		return hitflag;
	}

	public void setHitflag(String hitflag) {
		this.hitflag = hitflag;
	}

	public Set<Role> getUserRole() {
		return userRole;
	}

	public void setUserRole(Set<Role> userRole) {
		this.userRole = userRole;
	}
	
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getSid() {
		return sid;
	}

	public void setSid(String sid) {
		this.sid = sid;
	}

	public Map<String, String> getMapList() {
		return mapList;
	}

	public void setMapList(Map<String, String> mapList) {
		this.mapList = mapList;
	}

		public String getMapFolder() {
		return mapFolder;
	}

	public void setMapFolder(String mapFolder) {
		this.mapFolder = mapFolder;
	}

	public List getDocMapList() {
		return docMapList;
	}

	public void setDocMapList(List docMapList) {
		this.docMapList = docMapList;
	}

	public String getDuplicates() {
		return duplicates;
	}

	public void setDuplicates(String duplicates) {
		this.duplicates = duplicates;
	}

	public String getSecure() {
		return secure;
	}

	public void setSecure(String secure) {
		this.secure = secure;
	}

	public String getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(String fileTypes) {
		this.fileTypes = fileTypes;
	}

	public String getDescriptions() {
		return descriptions;
	}

	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}

	public String getFileIds() {
		return fileIds;
	}

	public void setFileIds(String fileIds) {
		this.fileIds = fileIds;
	}

	public String getCreatedBys() {
		return createdBys;
	}

	public void setCreatedBys(String createdBys) {
		this.createdBys = createdBys;
	}

	public Date getCreatedOns() {
		return createdOns;
	}

	public void setCreatedOns(Date createdOns) {
		this.createdOns = createdOns;
	}

	public String getUserCheckConfirm() {
		return userCheckConfirm;
	}

	public void setUserCheckConfirm(String userCheckConfirm) {
		this.userCheckConfirm = userCheckConfirm;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Partner getPartner() {
		return partner;
	}

	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getPpCode() {
		return ppCode;
	}

	public void setPpCode(String ppCode) {
		this.ppCode = ppCode;
	}

	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}

	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}

	public String getForQuotation() {
		return forQuotation;
	}

	public void setForQuotation(String forQuotation) {
		this.forQuotation = forQuotation;
	}

	public String getAsmlFlag() {
		return asmlFlag;
	}

	public void setAsmlFlag(String asmlFlag) {
		this.asmlFlag = asmlFlag;
	}

	public Map<String, String> getDocsListAccountPortal() {
		return docsListAccountPortal;
	}

	public void setDocsListAccountPortal(Map<String, String> docsListAccountPortal) {
		this.docsListAccountPortal = docsListAccountPortal;
	}
	
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	
	public String getMyFileIds() {
		return myFileIds;
	}

	public void setMyFileIds(String myFileIds) {
		this.myFileIds = myFileIds;
	}
	
	public Notes getNotes() {
		return notes;
	}
	
	public void setNotes(Notes notes) {
		this.notes = notes;
	}
	
	public List getNotesFileId() {
		return notesFileId;
	}

	public void setNotesFileId(List notesFileId) {
		this.notesFileId = notesFileId;
	}

	public Boolean getIsBookingAgent() {
		return isBookingAgent;
	}

	public void setIsBookingAgent(Boolean isBookingAgent) {
		this.isBookingAgent = isBookingAgent;
	}

	public Boolean getIsNetworkAgent() {
		return isNetworkAgent;
	}

	public void setIsNetworkAgent(Boolean isNetworkAgent) {
		this.isNetworkAgent = isNetworkAgent;
	}

	public Boolean getIsOriginAgent() {
		return isOriginAgent;
	}

	public void setIsOriginAgent(Boolean isOriginAgent) {
		this.isOriginAgent = isOriginAgent;
	}

	public Boolean getIsSubOriginAgent() {
		return isSubOriginAgent;
	}

	public void setIsSubOriginAgent(Boolean isSubOriginAgent) {
		this.isSubOriginAgent = isSubOriginAgent;
	}

	public Boolean getIsDestAgent() {
		return isDestAgent;
	}

	public void setIsDestAgent(Boolean isDestAgent) {
		this.isDestAgent = isDestAgent;
	}

	public Boolean getIsSubDestAgent() {
		return isSubDestAgent;
	}

	public void setIsSubDestAgent(Boolean isSubDestAgent) {
		this.isSubDestAgent = isSubDestAgent;
	}

	public String getFlagType() {
		return flagType;
	}

	public void setFlagType(String flagType) {
		this.flagType = flagType;
	}

	public String getBookingAgentFlag() {
		return bookingAgentFlag;
	}

	public void setBookingAgentFlag(String bookingAgentFlag) {
		this.bookingAgentFlag = bookingAgentFlag;
	}

	public String getNetworkAgentFlag() {
		return networkAgentFlag;
	}

	public void setNetworkAgentFlag(String networkAgentFlag) {
		this.networkAgentFlag = networkAgentFlag;
	}

	public String getOriginAgentFlag() {
		return originAgentFlag;
	}

	public void setOriginAgentFlag(String originAgentFlag) {
		this.originAgentFlag = originAgentFlag;
	}

	public String getSubOriginAgentFlag() {
		return subOriginAgentFlag;
	}

	public void setSubOriginAgentFlag(String subOriginAgentFlag) {
		this.subOriginAgentFlag = subOriginAgentFlag;
	}

	public String getDestAgentFlag() {
		return destAgentFlag;
	}

	public void setDestAgentFlag(String destAgentFlag) {
		this.destAgentFlag = destAgentFlag;
	}

	public String getSubDestAgentFlag() {
		return subDestAgentFlag;
	}

	public void setSubDestAgentFlag(String subDestAgentFlag) {
		this.subDestAgentFlag = subDestAgentFlag;
	}

	public String getCheckFlagBA() {
		return checkFlagBA;
	}

	public void setCheckFlagBA(String checkFlagBA) {
		this.checkFlagBA = checkFlagBA;
	}

	public String getCheckFlagNA() {
		return checkFlagNA;
	}

	public void setCheckFlagNA(String checkFlagNA) {
		this.checkFlagNA = checkFlagNA;
	}

	public String getCheckFlagOA() {
		return checkFlagOA;
	}

	public void setCheckFlagOA(String checkFlagOA) {
		this.checkFlagOA = checkFlagOA;
	}

	public String getCheckFlagSOA() {
		return checkFlagSOA;
	}

	public void setCheckFlagSOA(String checkFlagSOA) {
		this.checkFlagSOA = checkFlagSOA;
	}

	public String getCheckFlagDA() {
		return checkFlagDA;
	}

	public void setCheckFlagDA(String checkFlagDA) {
		this.checkFlagDA = checkFlagDA;
	}

	public String getCheckFlagSDA() {
		return checkFlagSDA;
	}

	public void setCheckFlagSDA(String checkFlagSDA) {
		this.checkFlagSDA = checkFlagSDA;
	}

	public String getAgentHoFlag() {
		return agentHoFlag;
	}

	public void setAgentHoFlag(String agentHoFlag) {
		this.agentHoFlag = agentHoFlag;
	}

	public String getCheckAgent() {
		return checkAgent;
	}

	public void setCheckAgent(String checkAgent) {
		this.checkAgent = checkAgent;
	}

	public String getBookingAgent() {
		return bookingAgent;
	}

	public void setBookingAgent(String bookingAgent) {
		this.bookingAgent = bookingAgent;
	}

	public String getNetworkAgent() {
		return networkAgent;
	}

	public void setNetworkAgent(String networkAgent) {
		this.networkAgent = networkAgent;
	}

	public String getOriginAgent() {
		return originAgent;
	}

	public void setOriginAgent(String originAgent) {
		this.originAgent = originAgent;
	}

	public String getSubOriginAgent() {
		return subOriginAgent;
	}

	public void setSubOriginAgent(String subOriginAgent) {
		this.subOriginAgent = subOriginAgent;
	}

	public String getDestAgent() {
		return destAgent;
	}

	public void setDestAgent(String destAgent) {
		this.destAgent = destAgent;
	}

	public String getSubDestAgent() {
		return subDestAgent;
	}

	public void setSubDestAgent(String subDestAgent) {
		this.subDestAgent = subDestAgent;
	}

	public String getTransDocSysDefault() {
		return transDocSysDefault;
	}

	public void setTransDocSysDefault(String transDocSysDefault) {
		this.transDocSysDefault = transDocSysDefault;
	}

	public String getTransDocJobType() {
		return transDocJobType;
	}

	public void setTransDocJobType(String transDocJobType) {
		this.transDocJobType = transDocJobType;
	}

	public void setTruckManager(TruckManager truckManager) {
		this.truckManager = truckManager;
	}

	public Truck getTruck() {
		return truck;
	}

	public void setTruck(Truck truck) {
		this.truck = truck;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public Integer getDocSize() {
		return docSize;
	}

	public void setDocSize(Integer docSize) {
		this.docSize = docSize;
	}

	public String getTempnotesId() {
		return TempnotesId;
	}

	public void setTempnotesId(String tempnotesId) {
		TempnotesId = tempnotesId;
	}


	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}

	public Map<String, String> getEmailStatusList() {
		return emailStatusList;
	}

	public void setEmailStatusList(Map<String, String> emailStatusList) {
		this.emailStatusList = emailStatusList;
	}

	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}

	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	public String getFlagBeforeCheck() {
		return flagBeforeCheck;
	}

	public void setFlagBeforeCheck(String flagBeforeCheck) {
		this.flagBeforeCheck = flagBeforeCheck;
	}

	public String getFlagBeforeCheckDA() {
		return flagBeforeCheckDA;
	}

	public void setFlagBeforeCheckDA(String flagBeforeCheckDA) {
		this.flagBeforeCheckDA = flagBeforeCheckDA;
	}

	public String getFlagBeforeCheckOA() {
		return flagBeforeCheckOA;
	}

	public void setFlagBeforeCheckOA(String flagBeforeCheckOA) {
		this.flagBeforeCheckOA = flagBeforeCheckOA;
	}

	public String getFlagBeforeCheckNA() {
		return flagBeforeCheckNA;
	}

	public void setFlagBeforeCheckNA(String flagBeforeCheckNA) {
		this.flagBeforeCheckNA = flagBeforeCheckNA;
	}

	public String getFlagBeforeCheckSOA() {
		return flagBeforeCheckSOA;
	}

	public void setFlagBeforeCheckSOA(String flagBeforeCheckSOA) {
		this.flagBeforeCheckSOA = flagBeforeCheckSOA;
	}

	public String getFlagBeforeCheckSDA() {
		return flagBeforeCheckSDA;
	}

	public void setFlagBeforeCheckSDA(String flagBeforeCheckSDA) {
		this.flagBeforeCheckSDA = flagBeforeCheckSDA;
	}

	public String getDocCategory() {
		return docCategory;
	}

	public void setDocCategory(String docCategory) {
		this.docCategory = docCategory;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getDocumentCategory() {
		return documentCategory;
	}

	public void setDocumentCategory(String documentCategory) {
		this.documentCategory = documentCategory;
	}

	public String getCategorySearchValue() {
		return categorySearchValue;
	}

	public void setCategorySearchValue(String categorySearchValue) {
		this.categorySearchValue = categorySearchValue;
	}

	public Map<String, String> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(Map<String, String> categoryList) {
		this.categoryList = categoryList;
	}

	public Map<String, List<MyFile>> getDocTypeFiles() {
		return docTypeFiles;
	}

	public void setDocTypeFiles(Map<String, List<MyFile>> docTypeFiles) {
		this.docTypeFiles = docTypeFiles;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public Boolean getFlagchk() {
		return flagchk;
	}

	public void setFlagchk(Boolean flagchk) {
		this.flagchk = flagchk;
	}

	public String getDocUpload() {
		return docUpload;
	}

	public void setDocUpload(String docUpload) {
		this.docUpload = docUpload;
	}

	public String getFileCabinetView() {
		return fileCabinetView;
	}

	public void setFileCabinetView(String fileCabinetView) {
		this.fileCabinetView = fileCabinetView;
	}

	public Boolean getIsServiceProvider() {
		return isServiceProvider;
	}

	public void setIsServiceProvider(Boolean isServiceProvider) {
		this.isServiceProvider = isServiceProvider;
	}

	public Boolean getInvoiceAttachment() {
		return invoiceAttachment;
	}

	public void setInvoiceAttachment(Boolean invoiceAttachment) {
		this.invoiceAttachment = invoiceAttachment;
	}

	public String getPPID() {
		return PPID;
	}

	public void setPPID(String pPID) {
		PPID = pPID;
	}

	public Boolean getDocumentAccessIsVendorCode() {
		return documentAccessIsVendorCode;
	}

	public void setDocumentAccessIsVendorCode(Boolean documentAccessIsVendorCode) {
		this.documentAccessIsVendorCode = documentAccessIsVendorCode;
	}

	public Boolean getDocumentAccessIsPaymentStatus() {
		return documentAccessIsPaymentStatus;
	}

	public void setDocumentAccessIsPaymentStatus(
			Boolean documentAccessIsPaymentStatus) {
		this.documentAccessIsPaymentStatus = documentAccessIsPaymentStatus;
	}

	public String getAccountlineShipNumber() {
		return accountlineShipNumber;
	}

	public void setAccountlineShipNumber(String accountlineShipNumber) {
		this.accountlineShipNumber = accountlineShipNumber;
	}

	public String getAccountlineVendorCode() {
		return accountlineVendorCode;
	}

	public void setAccountlineVendorCode(String accountlineVendorCode) {
		this.accountlineVendorCode = accountlineVendorCode;
	}

	public String getPayingStatusValue() {
		return payingStatusValue;
	}

	public void setPayingStatusValue(String payingStatusValue) {
		this.payingStatusValue = payingStatusValue;
	}

	public String getAccountLineVendorCode() {
		return accountLineVendorCode;
	}

	public void setAccountLineVendorCode(String accountLineVendorCode) {
		this.accountLineVendorCode = accountLineVendorCode;
	}

	public String getAccountLinePayingStatus() {
		return accountLinePayingStatus;
	}

	public void setAccountLinePayingStatus(String accountLinePayingStatus) {
		this.accountLinePayingStatus = accountLinePayingStatus;
	}

	public String getSoCorpId() {
		return soCorpId;
	}

	public void setSoCorpId(String soCorpId) {
		this.soCorpId = soCorpId;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public Map<String, String> getDefaultSortforFileCabinet() {
		return defaultSortforFileCabinet;
	}

	public void setDefaultSortforFileCabinet(
			Map<String, String> defaultSortforFileCabinet) {
		this.defaultSortforFileCabinet = defaultSortforFileCabinet;
	}

	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}

	public List getCheckListTodayList() {
		return checkListTodayList;
	}

	public void setCheckListTodayList(List checkListTodayList) {
		this.checkListTodayList = checkListTodayList;
	}

	public Integer getCountCheckListTodayResult() {
		return countCheckListTodayResult;
	}

	public void setCountCheckListTodayResult(Integer countCheckListTodayResult) {
		this.countCheckListTodayResult = countCheckListTodayResult;
	}

	public List getCheckListOverDueList() {
		return checkListOverDueList;
	}

	public void setCheckListOverDueList(List checkListOverDueList) {
		this.checkListOverDueList = checkListOverDueList;
	}

	public Integer getCountCheckListOverDueResult() {
		return countCheckListOverDueResult;
	}

	public void setCountCheckListOverDueResult(Integer countCheckListOverDueResult) {
		this.countCheckListOverDueResult = countCheckListOverDueResult;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}


	public String getCheckBoxEmailId() {
		return checkBoxEmailId;
	}

	public void setCheckBoxEmailId(String checkBoxEmailId) {
		this.checkBoxEmailId = checkBoxEmailId;
	}

	public String getMyFileLocation() {
		return myFileLocation;
	}

	public void setMyFileLocation(String myFileLocation) {
		this.myFileLocation = myFileLocation;
	}

	public Map<String, String> getPartnerCodeList() {
		return partnerCodeList;
	}

	public void setPartnerCodeList(Map<String, String> partnerCodeList) {
		this.partnerCodeList = partnerCodeList;
	}

	public String getJobNumber() {
		return jobNumber;
	}

	public void setJobNumber(String jobNumber) {
		this.jobNumber = jobNumber;
	}

	public String getMyFileModule() {
		return myFileModule;
	}

	public void setMyFileModule(String myFileModule) {
		this.myFileModule = myFileModule;
	}

	public String getReportModule() {
		return reportModule;
	}

	public void setReportModule(String reportModule) {
		this.reportModule = reportModule;
	}

	public String getReportSubject() {
		return reportSubject;
	}

	public void setReportSubject(String reportSubject) {
		this.reportSubject = reportSubject;
	}

	public String getNoteFor() {
		return noteFor;
	}

	public void setNoteFor(String noteFor) {
		this.noteFor = noteFor;
	}

	public String getReportSignaturePart() {
		return reportSignaturePart;
	}

	public void setReportSignaturePart(String reportSignaturePart) {
		this.reportSignaturePart = reportSignaturePart;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public EmailSetup getEmailSetup() {
		return emailSetup;
	}

	public void setEmailSetup(EmailSetup emailSetup) {
		this.emailSetup = emailSetup;
	}

	public String getReportBody() {
		return reportBody;
	}

	public void setReportBody(String reportBody) {
		this.reportBody = reportBody;
	}

	public String getReportSignature() {
		return reportSignature;
	}

	public void setReportSignature(String reportSignature) {
		this.reportSignature = reportSignature;
	}

	public String getRecipientTo() {
		return recipientTo;
	}

	public void setRecipientTo(String recipientTo) {
		this.recipientTo = recipientTo;
	}

	public String getRecipientCC() {
		return recipientCC;
	}

	public void setRecipientCC(String recipientCC) {
		this.recipientCC = recipientCC;
	}

	public ReportEmailDTO getReportEmail() {
		return reportEmail;
	}

	public void setReportEmail(ReportEmailDTO reportEmail) {
		this.reportEmail = reportEmail;
	}

	public String getEmailFlag() {
		return emailFlag;
	}

	public void setEmailFlag(String emailFlag) {
		this.emailFlag = emailFlag;
	}



	public User getUser1() {
		return user1;
	}

	public void setUser1(User user1) {
		this.user1 = user1;
	}

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public List getRecipientWithEmailList() {
		return recipientWithEmailList;
	}

	public void setRecipientWithEmailList(List recipientWithEmailList) {
		this.recipientWithEmailList = recipientWithEmailList;
	}

	public String getMyFileId() {
		return myFileId;
	}

	public void setMyFileId(String myFileId) {
		this.myFileId = myFileId;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getMmValidation() {
		return mmValidation;
	}

	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public List getFileTypeForPartnerCode() {
		return fileTypeForPartnerCode;
	}

	public void setFileTypeForPartnerCode(List fileTypeForPartnerCode) {
		this.fileTypeForPartnerCode = fileTypeForPartnerCode;
	}

	public String getMyFileIdCheck() {
		return myFileIdCheck;
	}

	public void setMyFileIdCheck(String myFileIdCheck) {
		this.myFileIdCheck = myFileIdCheck;
	}

	public String getShipNumber() {
		return shipNumber;
	}

	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}

	public String getResultType() {
		return resultType;
	}

	public void setResultType(String resultType) {
		this.resultType = resultType;
	}

	public String getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getMyFileJspName() {
		return myFileJspName;
	}

	public void setMyFileJspName(String myFileJspName) {
		this.myFileJspName = myFileJspName;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}

	public Boolean getEnterpriseLicense() {
		return enterpriseLicense;
	}

	public void setEnterpriseLicense(Boolean enterpriseLicense) {
		this.enterpriseLicense = enterpriseLicense;
	}

	public Boolean getIsDriver() {
		return isDriver;
	}

	public void setIsDriver(Boolean isDriver) {
		this.isDriver = isDriver;
	}

	public String getFlagAgent() {
		return flagAgent;
	}

	public void setFlagAgent(String flagAgent) {
		this.flagAgent = flagAgent;
	}

	public String getRowId() {
		return rowId;
	}

	public void setRowId(String rowId) {
		this.rowId = rowId;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getFlagUloadInv() {
		return flagUloadInv;
	}

	public void setFlagUloadInv(String flagUloadInv) {
		this.flagUloadInv = flagUloadInv;
	}

	/**
	 * @return the ediBillTo
	 */
	public String getEdiBillTo() {
		return ediBillTo;
	}

	/**
	 * @param ediBillTo the ediBillTo to set
	 */
	public void setEdiBillTo(String ediBillTo) {
		this.ediBillTo = ediBillTo;
	}

	 

		
}
