<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  
<head>   
    <title>Email Setup Template Details</title>   
    <meta name="heading" content="Email Setup Template Details"/>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/font-awesome.min.css'/>" />
    <style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		
	</style>
    
</head>
<div class="modal-dialog" style="width:830px;">
 <div class="modal-content">
      <div class="modal-header bg-info" style="text-align:left !important;color:#444;padding-bottom:5px !important;margin-top:-10px;padding-top: 5px;border-top-left-radius: 4px;
			border-top-right-radius: 4px;">
        <button type="button" class="close" style="text-align:right !important;" onclick="closeModalEdit('${editModalId}');" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="line-height:1.2 !important">Email Setup Template Details</h4>
      </div>
<div class="modal-body" style="padding-top:8px;">
<s:form id="editEmailTemplatePopupForm" name="editEmailTemplatePopupForm" action="" method="post" validate="true" enctype="multipart/form-data">
<s:hidden name="emailSetupTemplate.id" value="%{emailSetupTemplate.id}"/>
<s:hidden name="idCurrentValue"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<s:hidden name="cid" value="<%=request.getParameter("cid")%>"/>
<s:hidden name="emailBodyCurrentValue"/>
<s:hidden name="emailToCurrentValue"/>
<s:hidden name="emailFromCurrentValue"/>
<s:hidden name="emailCcCurrentValue"/>
<s:hidden name="emailBccCurrentValue"/>
<s:hidden name="emailSubjectCurrentValue"/>
<s:hidden name="emailSetupTemplateEditURL"/>
<s:hidden id="fileNameIdList${editModalId}" name="fileNameIdList${editModalId}"/>
<s:hidden name="fileCabinetFileIdListValue" />
<s:hidden name="attachedFileNameListEditVal" id="attachedFileNameListEditVal"/>
<s:hidden id="fileCabinetIdListFrom${editModalId}" />
<s:hidden name="fileCabinetIdListFromValue" />
 
				<div style="overflow-y:auto;max-height: calc(100vh - 200px);" id="bodyDiv${editModalId}">
					<table class="table-cmodal" style="margin:0px;padding:0px;width:auto !important;" cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
						<td style="text-align:right;padding-left:26px;" class="listwhitetext">From<font color="red" size="2">*</font></td>
						<td align="left" width="100"><s:textfield name="emailFromEditedValue" id="emailFrom${editModalId}" cssStyle="width:328px;" maxlength="60" cssClass="input-text" /></td>
						<td style="text-align:right;" class="listwhitetext">To<font color="red" size="2">*</font></td>
						<td align="left" width="100"><s:textfield name="emailToEditedValue" id="emailTo${editModalId}" cssStyle="width:328px;" maxlength="250" cssClass="input-text" /></td>
						</tr>	
						<tr>
						<td style="text-align:right;" class="listwhitetext">Cc</td>
						<td align="left" ><s:textfield name="emailCcEditedValue" id="emailCc${editModalId}" cssStyle="width:328px;" maxlength="250" cssClass="input-text" /></td>
						<td style="text-align:right;" class="listwhitetext">Bcc</td>
						<td align="left" ><s:textfield name="emailBccEditedValue" id="emailBcc${editModalId}" cssStyle="width:328px;" maxlength="250" cssClass="input-text" /></td>
						</tr>
						<tr>
						<td style="text-align:right;padding-left:20px;" class="listwhitetext">Subject </td>
						<td align="left" colspan="3"><s:textfield name="emailSubjectEditedValue" id="emailSubject${editModalId}" cssStyle="width:690px;" maxlength="500" cssClass="input-text" /></td>
						</tr>							
					    <tr>
					    <script type="text/javascript">
							 tinymce.init({
								    selector: '#emailBody'+'${editModalId}',
								    theme: 'modern',
								    width: 732,
								    height: 250,
								    branding: false,
								    statusbar: false,
								    menubar: false,
								    content_style: ".mce-content-body {font-size:10px;font-family:Arial,sans-serif;}",
								    plugins: [
								      'advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker',
								      'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
								      'save table contextmenu directionality template paste textcolor'
								    ],
								    toolbar: 'fontselect fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview | forecolor backcolor | cut copy paste | spellchecker | table ',
								    font_formats: "Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Open Sans=Open Sans,helvetica,sans-serif;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva",
									fontsize_formats: "8px 10px 12px 14px 16px 18px 20px",
						            
								});
					   	</script> 
    						<td align="left" colspan="4" style="padding-left:20px;"><s:textarea  cssClass="textarea" id="emailBody${editModalId}" name="emailBodyEditedValue" cols="122" rows="10" /></td>
						</tr>
						
						<c:if test="${noFileFoundListEdit!='[]'}">
						<table style="margin:0px;padding:0px;width:758px;" cellspacing="3" cellpadding="0" border="0" id="noFileFoundListDiv${editModalId}">	
								<tr>
								<td valign="top" style="border:none;padding:0px 0px 0px 17px;">
								<div style="overflow-y:auto;max-height:170px;border-bottom:1px solid #ebccd1;border-top:1px solid #ebccd1;">
								<table class="error-table" style="width:100%; margin:0px;">
									<thead>
										<tr>
											<th style="min-width: 150px;height:20px !important;">File's Not Found</th>
											<th style="width:50px;height:20px !important;" class="centeralign"></th>
										</tr>
									</thead>
									<tbody>
											
										<c:set var = "noFileAttachedFileList" value = "${noFileFoundListEdit}"/>
										<c:set var="noFileAttachedFileList" value="${fn:replace(noFileAttachedFileList,'[', '')}" />
										<c:set var="noFileAttachedFileList" value="${fn:replace(noFileAttachedFileList,']', '')}" />
										<c:forTokens items="${noFileAttachedFileList}" delims="^" var="noFileFileItem">
											<tr>
												<td>
													<span style="font-family: arial,verdana;font-size: 12px;height:15px;text-decoration: none;"><c:out value="${noFileFileItem}"></c:out></span>
												</td>
												<td class="txt-center">
													 <i class="fa fa-exclamation-circle" style="font-size:16px;color:#f44336"></i>
												</td>
											</tr>
										</c:forTokens>
									</tbody>
								</table>
								</div>
								</td>
								</tr>
						</table>
						</c:if>
						
						<table style="margin:0px;padding:0px;width:758px;margin-top:-2px;"  cellspacing="3" cellpadding="0" border="0">	
								
								<td valign="top" style="border:none;padding:0px 0px 0px 17px;">								
								<div id="attachedFileListDiv${editModalId}" style="overflow-y:auto;max-height:170px;border-bottom:1px solid #74B3DC;border-top:1px solid #74B3DC;">
								<table class="table" style="width:100%; margin:0px;">
									<thead>
										<tr>
											<!-- <th width="">#</th> -->
											<th style="min-width: 150px;height:20px !important;">Pre Attached File's</th>
											<th style="width:40px;height:20px !important;" class="centeralign">File</th>
											<th style="width:50px;height:20px !important;" class="centeralign">Remove</th>
										</tr>
									</thead>
									<tbody>
											<c:if test="${not empty attachedFileNameListEdit}">
												<c:set var = "replacedAttachedFileList" value = "${attachedFileNameListEdit}"/>
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,'[', '')}" />
												<c:set var="replacedAttachedFileList" value="${fn:replace(replacedAttachedFileList,']', '')}" />
												<c:forTokens items="${replacedAttachedFileList}" delims="^" var="attachedFileItem">
													<tr>
													<%-- <td>${rowCounter.count}</td> --%>
														<td>
															<span style="font-family: arial,verdana;font-size: 12px;height:15px;text-decoration: none;"><c:out value="${attachedFileItem}"></c:out></span>
														</td>
														<td class="txt-center">
															<a><img align="middle" onclick="downloadAttachedFileEdit('${attachedFileItem}','${emailSetupTemplate.id}');"
																	style="margin: 0px 0px 0px 0px;" src="${pageContext.request.contextPath}/images/download16.png" /></a>
														</td>
														<td class="txt-center">
															<div>
																<a><img align="middle"
																	onclick="deleteAttachedFileName('${attachedFileItem}','${attachedFileNameListEdit}');"
																	style="margin: 0px 0px 0px 0px;"
																	src="${pageContext.request.contextPath}/images/remove16.png" />
																</a>
															</div>
														</td>	
													</tr>
												</c:forTokens>
											</c:if>
									</tbody>
								</table></div>
								</td>
						</table>
						
						<c:if test="${allFileFromFileCabinetList!='[]'}">
						<table style="margin:0px;padding:0px;width:758px;margin-top:-2px;"  cellspacing="3" cellpadding="0" border="0">	
								<tr>							
								<td valign="top" style="border:none;padding:0px 0px 0px 17px;">	
						<div id="fileCabinetListDiv${editModalId}" style="overflow-y:auto;max-height:170px;border-bottom:1px solid #74B3DC;border-top:1px solid #74B3DC;">
							<table class="email-table" style="margin:0px;padding:0px;width:100%;" id="table-File" >
								<thead>
									<tr>
										<th style="width:5px;text-align:center;"></th>
										<!-- <td rowspan="2" class="newtablehead" style="width:0.5%">#&nbsp;&nbsp;</td> -->
										<th style="width:100px;">Document&nbsp;Type</th>
										<th style="width:150px;">Description</th>
										<!-- <td rowspan="2" class="newtablehead" style="width:10%">File&nbsp;Name&nbsp;</td> -->
										<th style="width:70px;">Size</th>
										<th style="width:90px;">Uploaded&nbsp;On</th>										
										<th style="width:80px;">Uploaded&nbsp;By</th>
										
									</tr>
									</thead>
									<tbody>
									<c:forEach var="myFileList" items="${allFileFromFileCabinetList}">
										<tr >
											<c:set var="checkOpt" value="false" />
								           	<c:set var="keyVal" value="${myFileList.fileLocation}" />
	                   						<c:forTokens items="${fileCabinetListByDocumentTpye}" delims="~" var="kc">
										    <c:if test="${kc == keyVal && !checkOpt}">
										    	<c:set var="checkOpt" value="true" />
										    </c:if>
											</c:forTokens>
				 							
				 							<td>
					 							<c:if test="${checkOpt}">
					 								<div class="errorChk tooltipChk" style="position:relative;">
														<input type="checkbox" id="check${myFileList.fileId}" name="checkedIdList" value="${myFileList.fileId}" onclick="setValues('${myFileList.fileId}','${myFileList.fileFileName}',this)"/>
														<span class="tooltiptextChk">Already Attached</span>
													</div>
					 							</c:if>
					 							<c:if test="${!checkOpt}">
				 									<input type="checkbox" id="check${myFileList.fileId}" name="unCheckedIdList" value="${myFileList.fileId}" onclick="setValues('${myFileList.fileId}','${myFileList.fileFileName}',this)"/>
				 								</c:if>
				 							</td>
				 							<%-- <td style="width:5px;text-align:center;">
				 								<input type="checkbox" id="check${myFileList.fileId}" name="checkBoxIdList${editModalId}" value="${myFileList.fileId}" onclick="setValues('${myFileList.fileId}','${myFileList.fileFileName}',this)"/>
				 							</td> --%>
				 							<td>
				 								<c:out value="${myFileList.fileType }" />
				 							</td>
				 							<td>
													<%-- <c:out value="${myFileList.fileDescription}" escapeXml="false"/> --%>
													<s:textfield cssClass="input-textUpper"	name="fileDescription" value="${myFileList.fileDescription}" title="${myFileList.fileDescription}" onclick="downloadFileCabinetFileEdit('${myFileList.fileId}');" readonly="true" cssStyle="width:98%;background:none;border:none;cursor:pointer;text-decoration:underline;text-decoration-color:#003366;" />
				 							</td>
				 							<%-- <td>
				 								<c:out value="${myFileList.fileFileName}" />
				 							</td> --%>
				 							<td>
				 								<c:out value="${myFileList.fileSize}" />
				 							</td>
				 							<td>
				 								<fmt:formatDate pattern="dd-MMM-yyyy" value="${myFileList.updatedOn}" />
				 							</td>
				 							<td>
				 								<c:out value="${myFileList.updatedBy}" />
				 							</td>
				 						</tr>
									</c:forEach>
									</tbody>
							</table>
							</div>
							</td>
							</tr>
							</table>
						</c:if>
				       
			            <div id="selectedFiles${editModalId}" style="overflow-y:auto;max-height:170px;text-align:left;margin-left:20px;border:1px solid #74B3DC;width:733px;margin-top:5px;"> </div>
						
						<div class="modal fade" id="showErrorModal${editModalId}" aria-hidden="true">
							<div class="modal-dialog modal-sm">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg${editModalId}">
											</div>
									    </div>
									    <div class="modal-footer">
									        <button type="button" onclick="closeErrorModal('${editModalId}');" class="btn btn-sm btn-danger">Close</button>
									    </div>
								</div>
							</div>
						</div>
				
					</tbody>
				</table>
				</div>
					<div class="modal-footer" style="padding-bottom:5px !important;margin-top:3px;">
						<button type="button" class="btn btn-sm btn-success btn-blue" style="width:auto;padding:8px;float:left;margin-left:5px;" title="Attach From File Cabinet" onclick="showFileCabinetList()"><i class="fa fa-folder-open-o"></i></button>
						<label class="btn btn-sm btn-success btn-blue tooltip" style="width:auto;padding:7px 9px;float:left;" title="Attach File">
				     			<i class="fa fa-paperclip" style="font-size:14px;"></i>   
				          	<input style="width:80px;display:none;" id="files${editModalId}" name="fileUpload" type="file" multiple onchange="updateFileList()"/>
				          	<span class="tooltiptext">Note : File name should not contains special characters like ^ ~ ' | $ @</span>
						</label>
						<c:if test="${not empty lastSentMailId}">
							<button type="button" class="btn btn-sm btn-success" style="width:auto;padding:8px;float:left;" title="Load Last Sent" onclick="getLastSentMailTemplate()"><i class="fa fa-history"></i></button>
						</c:if>
						<button type="button" class="btn btn-sm btn-success btn-blue" onclick="sendMailFromEmailTemplate()">Send</button>
						<button type="button" class="btn btn-sm btn-danger" onclick="closeModalEdit('${editModalId}');" data-dismiss="modal">Close</button>
					</div>
		</s:form>
	</div>
</div></div>
<script language="javascript" type="text/javascript">
try{
	document.getElementById("fileCabinetListDiv"+'${editModalId}').style.display="none";
	document.getElementById("selectedFiles"+'${editModalId}').style.display="none";
}catch(e){}
function setValues(rowId,fileName,targetElement){
	var userCheckStatus = document.getElementById("fileNameIdList"+'${editModalId}').value;
	var fileIdName = rowId;
	  if(targetElement.checked){
		  if(userCheckStatus == '' ){
			  document.getElementById("fileNameIdList"+'${editModalId}').value = fileIdName;
			}else{
				if(userCheckStatus.indexOf(fileIdName)>=0){
				}else{
					userCheckStatus = userCheckStatus + "^" +fileIdName;
				}
				document.getElementById("fileNameIdList"+'${editModalId}').value = userCheckStatus.replace('^ ^',"^");
			}
	   }else{
			if(userCheckStatus == ''){
				document.getElementById("fileNameIdList"+'${editModalId}').value = fileIdName;
		     }else{
		    	 var check = userCheckStatus.indexOf(fileIdName);
		 		if(check > -1){
		 			var values = userCheckStatus.split("^");
		 		   for(var i = 0 ; i < values.length ; i++) {
		 		      if(values[i]== fileIdName) {
		 		    	values.splice(i, 1);
		 		    	userCheckStatus = values.join("^");
		 		    	document.getElementById("fileNameIdList"+'${editModalId}').value = userCheckStatus;
		 		      }
		 		   }
		 		}else{
		 			userCheckStatus = userCheckStatus + "^" +fileIdName;
		 			document.getElementById("fileNameIdList"+'${editModalId}').value = userCheckStatus.replace('^ ^',"^");
		 		}
		     }
	   }
}
function downloadAttachedFileEdit(fileName,emailId){
	var cid = document.forms['editEmailTemplatePopupForm'].elements['cid'].value;
	var sid = document.forms['editEmailTemplatePopupForm'].elements['sid'].value;
	var pageType = "popUp";
	var url="downloadAttachedFileFromEmailSetupTemplateEdit.html?fileName="+encodeURIComponent(fileName)+"&id="+emailId+"&cid="+cid+"&sid="+sid+"&pageType="+pageType;
	location.href=url;
}
function downloadFileCabinetFileEdit(fileId){
	var cid = document.forms['editEmailTemplatePopupForm'].elements['cid'].value;
	var sid = document.forms['editEmailTemplatePopupForm'].elements['sid'].value;
	var pageType = "popUp";
	var url="downloadFileCabinetFileFromEmailSetupTemplateEdit.html?fId="+fileId+"&cid="+cid+"&sid="+sid+"&pageType="+pageType;
	location.href=url;
}
function deleteAttachedFileName(fileName,fileNameList){
	var $j = jQuery.noConflict();
	var emailSetupTemplateId ='${editModalId}';
	emailSetupTemplateId = emailSetupTemplateId.substring(4,emailSetupTemplateId.length);
	if(emailSetupTemplateId!=null){
		 new Ajax.Request('/redsky/deleteFileInEmailSetupTemplatePopupAjax.html?ajax=1&fileName='+encodeURIComponent(fileName)+'&attachedFileNameListEdit='+encodeURIComponent(fileNameList)+'&id='+emailSetupTemplateId+'&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      $j("#attachedFileListDiv"+'${editModalId}').html(response.trim());
				      document.forms['editEmailTemplatePopupForm'].elements['attachedFileNameListEditVal'].value = document.getElementById('attachedFileNameListPopupEditVal').value;
				    },
				    onFailure: function(){ 
					    }
				  });
	 	
	}
}

function getLastSentMailTemplate(){
	var cid = document.forms['editEmailTemplatePopupForm'].elements['cid'].value;
	var sid = document.forms['editEmailTemplatePopupForm'].elements['sid'].value;
	document.forms['editEmailTemplatePopupForm'].elements['fileUpload'].value='';
	document.getElementById('selectedFiles'+'${editModalId}').innerHTML='';
	document.getElementById("selectedFiles"+'${editModalId}').style.display="none";
	var $j = jQuery.noConflict();
	var emailSetupTemplateId ='${editModalId}';
	emailSetupTemplateId = emailSetupTemplateId.substring(4,emailSetupTemplateId.length);
		 new Ajax.Request('/redsky/getLastSentMailTemplatePopupAjax.html?ajax=1&id='+emailSetupTemplateId+'&cid='+cid+'&sid='+sid+'&editModalId=${editModalId}&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      var lastSentMailVal = response.trim();
				      lastSentMailVal = lastSentMailVal.split("~");
				      document.getElementById('emailFrom'+'${editModalId}').value = lastSentMailVal[0];
				      document.getElementById('emailTo'+'${editModalId}').value = lastSentMailVal[1];
				      document.getElementById('emailCc'+'${editModalId}').value = lastSentMailVal[2];
				      document.getElementById('emailBcc'+'${editModalId}').value = lastSentMailVal[3];
				      document.getElementById('emailSubject'+'${editModalId}').value = lastSentMailVal[4];
				      
				      var activeEditor = tinyMCE.get('emailBody'+'${editModalId}');
				      activeEditor.setContent(lastSentMailVal[5]);
				      
				      findLastSentMailAttachementAjax();
				    },
				    onFailure: function(){ 
					    }
				  });
}

function findLastSentMailAttachementAjax(){
	var $j = jQuery.noConflict();
	var cid = document.forms['editEmailTemplatePopupForm'].elements['cid'].value;
	var sid = document.forms['editEmailTemplatePopupForm'].elements['sid'].value;
	document.getElementById('fileNameIdList'+'${editModalId}').value = '';
    $j('input[name=checkedIdList]').attr('checked', false);
    $j('input[name=unCheckedIdList]').attr('checked', false);
	document.getElementById('fileCabinetIdListFrom'+'${editModalId}').value = 'loadLastSent';
	var $j = jQuery.noConflict();
	var emailSetupTemplateId ='${editModalId}';
	emailSetupTemplateId = emailSetupTemplateId.substring(4,emailSetupTemplateId.length);
		 new Ajax.Request('/redsky/getLastSentMailAttachementAjax.html?ajax=1&id='+emailSetupTemplateId+'&cid='+cid+'&sid='+sid+'&editModalId=${editModalId}&decorator=simple&popup=true',
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				      $j("#attachedFileListDiv"+'${editModalId}').html(response.trim());
				      document.forms['editEmailTemplatePopupForm'].elements['attachedFileNameListEditVal'].value = document.getElementById('attachedFileNameListPopupEditVal').value;
				      document.getElementById("fileCabinetListDiv"+'${editModalId}').style.display="none";
				      document.getElementById("noFileFoundListDiv"+'${editModalId}').style.display="none";
				      document.getElementById("fileCabinetButtonDiv"+'${editModalId}').style.display="none";
				    },
				    onFailure: function(){ 
					    }
				  });
}

function updateFileList() {
	var $j = jQuery.noConflict();
	$j('#bodyDiv'+'${editModalId}').animate({scrollTop: $j('#bodyDiv'+'${editModalId}').prop("scrollHeight")}, 300);
	document.getElementById("selectedFiles"+'${editModalId}').style.display="block";
	  var input = document.getElementById('files'+'${editModalId}');
	  var output = document.getElementById('selectedFiles'+'${editModalId}');
	  output.innerHTML = '';
	  for (var i = 0; i < input.files.length; ++i) {
		  var fileSize = bytesToSize(input.files.item(i).size);
		  output.innerHTML += "<li style='border-bottom:1px solid #e0e0e0;font-family:arial,verdana;font-size:12px;line-height:1.2em;padding-top:4px;'>"+ input.files.item(i).name +"<span style='margin:0px 0px 0px 0px;float:right;margin-right:3%;margin-top:2px;'>"+fileSize+"</span></li>";
	  }
	}
function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return 'n/a';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
function showFileCabinetList(){
	var $j = jQuery.noConflict();
	$j('#bodyDiv'+'${editModalId}').animate({scrollTop: $j('#bodyDiv'+'${editModalId}').prop("scrollHeight")}, 300);
	document.getElementById("fileCabinetListDiv"+'${editModalId}').style.display="block";
}
function sendMailFromEmailTemplate(){
		var $j = jQuery.noConflict();
		var emailFromValue = document.getElementById('emailFrom'+'${editModalId}').value;
		if(emailFromValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email From.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}else if(emailFromValue == null || emailFromValue ==''){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Please enter values for From.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		var emailToValue = document.getElementById('emailTo'+'${editModalId}').value;
		if(emailToValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email To.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}else if(emailToValue==null || emailToValue==''){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Please enter values for To.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		var emailCcValue = document.getElementById('emailCc'+'${editModalId}').value;
		if(emailCcValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email Cc.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		var emailBccValue = document.getElementById('emailBcc'+'${editModalId}').value;
		if(emailBccValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email Bcc.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		var emailSubjectValue = document.getElementById('emailSubject'+'${editModalId}').value;
		if(emailSubjectValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email Subject.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		
		var emailId = '<%=request.getParameter("id")%>';
		var emailBodyValue = tinymce.get('emailBody'+'${editModalId}').getContent();
		if(emailBodyValue.indexOf("ErrorIN")!=-1){
			$j('#getErrorMsg'+'${editModalId}').html("<p>Mail can not be sent because there is some error in email Body.</p>");
			$j('#showErrorModal'+'${editModalId}').modal({show:true,backdrop: 'static',keyboard: false});
			return false;
		}
		document.forms['editEmailTemplatePopupForm'].elements['emailFromCurrentValue'].value = emailFromValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailToCurrentValue'].value = emailToValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailCcCurrentValue'].value = emailCcValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailBccCurrentValue'].value = emailBccValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailSubjectCurrentValue'].value = emailSubjectValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailBodyCurrentValue'].value = emailBodyValue;
		document.forms['editEmailTemplatePopupForm'].elements['emailSetupTemplateEditURL'].value = window.location.href;
		var mailId ='${editModalId}';
		document.forms['editEmailTemplatePopupForm'].elements['idCurrentValue'].value = mailId.substring(4,mailId.length);
		if(document.getElementById('attachedFileNameListEditVal').value =='[]'){
			document.getElementById('attachedFileNameListEditVal').value ='allDeleted';
		}else if(document.getElementById('attachedFileNameListEditVal').value ==null || document.getElementById('attachedFileNameListEditVal').value ==''){
			document.getElementById('attachedFileNameListEditVal').value = '${attachedFileNameListEdit}';
		}
		document.forms['editEmailTemplatePopupForm'].elements['fileCabinetFileIdListValue'].value = document.getElementById('fileNameIdList'+'${editModalId}').value;
		document.forms['editEmailTemplatePopupForm'].elements['fileCabinetIdListFromValue'].value = document.getElementById('fileCabinetIdListFrom'+'${editModalId}').value;
		document.forms['editEmailTemplatePopupForm'].action = 'sendEmailFromEmailTemplateEditAjax.html?ajax=1&decorator=simple&popup=true';
	    document.forms['editEmailTemplatePopupForm'].submit();
	    $j("#emailTemplateDetailsEdit"+emailId).modal("hide");
}

</script>