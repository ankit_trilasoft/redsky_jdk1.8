package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.RefZipGeoCodeMapDao;
import com.trilasoft.app.model.RefZipGeoCodeMap;
import com.trilasoft.app.service.RefZipGeoCodeMapManager;

public class RefZipGeoCodeMapManagerImpl extends GenericManagerImpl<RefZipGeoCodeMap,Long> implements RefZipGeoCodeMapManager {
	RefZipGeoCodeMapDao refZipGeoCodeMapDao;
	public RefZipGeoCodeMapManagerImpl(RefZipGeoCodeMapDao refZipGeoCodeMapDao) {
		super(refZipGeoCodeMapDao);
		this.refZipGeoCodeMapDao = refZipGeoCodeMapDao;
	}
	public List zipCodeExisted(String zipCode, String corpId)
		{
			return refZipGeoCodeMapDao.zipCodeExisted(zipCode, corpId);
		}
	public List refZipGeoCodeMapByCorpId(String corpId){
			return refZipGeoCodeMapDao.refZipGeoCodeMapByCorpId(corpId);
		}
	public List findMaximumId(){
		return refZipGeoCodeMapDao.findMaximumId();
	}
	public Map<String, String> findByParameter(String bucket2, String corpId){
		return refZipGeoCodeMapDao.findByParameter(bucket2, corpId);
	}
	public List refZipGeoCodeMapState(String stateCode){
		return refZipGeoCodeMapDao.refZipGeoCodeMapState(stateCode);
	}
	public List searchByRefZipGeoCodeMap(String zipCode,String city,String state,String country){
		return refZipGeoCodeMapDao.searchByRefZipGeoCodeMap(zipCode, city, state,country);
	}
	public int updatePrimaryRecord(String zipCode, String corpId){
	return refZipGeoCodeMapDao.updatePrimaryRecord(zipCode, corpId);
	}
	public String getStateName(String stateabbreviation, String sessionCorpID, String country) {
		return refZipGeoCodeMapDao.getStateName(stateabbreviation, sessionCorpID, country);
	}
	
	}

