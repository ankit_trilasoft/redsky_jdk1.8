<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Account Line List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="accLinetSO" class="table" requestURI="" id="accountLineList" export="false" defaultsort="1" >
		<display:column title="Acc.Line#" ><a onclick="settSelectedAccountId(${accountLineList.id});setReturnString('gototab.moveSelectAccount');return autoSaveFunc('none');" onmouseover="return chkSelect('2');" ><c:out value="${accountLineList.accountLineNumber}" /></a></display:column>
		<display:column property="estimateVendorName" title="Vendor Name" maxLength="6"  style="width:60px"/>
	    <display:column property="category" titleKey="accountLine.category"  maxLength="7"  style="width:60px"/> 
	</display:table>
</div>
