<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="PartnerOwnership"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Landscape"
		 pageWidth="955"
		 pageHeight="595"
		 columnWidth="895"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select "PartnerOwnership.jrxml",
partnerprivate.`accountHolder` AS 'ACCOUNT_OWNER',
accountprofile.`stage` AS 'STAGE',
if((partnerpublic.isAccount is true && partnerpublic.isPrivateParty is not true),'AC',if((partnerpublic.isAccount is not true && partnerpublic.isPrivateParty is true),'PP',if((partnerpublic.isAccount is true && partnerpublic.isPrivateParty is true),'AC,PP',''))) AS 'Partner_Flag',
partnerpublic.partnerCode AS 'Partner_Code',
partnerpublic.lastName AS 'COMPANY_NAME',
accountprofile.`industry` AS 'INDUSTRY',
(select count(s.shipnumber) from billing b inner join serviceorder s on s.id = b.id where s.status not in ('CNCL','DWNLD') and year(b.revenuerecognition)=year(now()) and b.billToCode = partnerprivate.partnerCode group by year(b.revenuerecognition), b.billToCode) AS 'CURRENT_YEAR_SSCW_BOOKINGS',
(select round(sum((a.actualRevenue)/1000))  from billing b inner join serviceorder s on s.id = b.id inner join accountline a on s.id = a.serviceorderid where s.status not in ('CNCL','DWNLD') and year(b.revenuerecognition)=year(now()) and a.status is true and a.billToCode = partnerprivate.partnerCode and (a.recInvoiceNumber != '' or a.recInvoiceNumber is not null) group by year(b.revenuerecognition), a.billToCode) AS 'CURRENT_YEAR_DYNAMIC_REVENUE',
partnerpublic.billingCity AS 'BILLING_CITY',
partnerpublic.billingCountry AS 'BILLING_COUNTRY'
FROM partnerprivate inner join partnerpublic on partnerpublic.id = partnerprivate.partnerpublicid
inner join accountprofile on accountprofile.partnercode = partnerprivate.partnercode and accountprofile.corpid='SSCW' 
WHERE partnerprivate.corpid='SSCW' and partnerpublic.corpid in ('TSFT','SSCW')
and ((partnerpublic.isAccount is true) || (partnerpublic.isPrivateParty is true))
and (partnerprivate.`accountHolder`!='' or partnerprivate.`accountHolder`!=null)
and partnerprivate.status in ('Approved','Prospect')
order by 2]]></queryString>

	<field name="PartnerOwnership.jrxml" class="java.lang.String"/>
	<field name="ACCOUNT_OWNER" class="java.lang.String"/>
	<field name="STAGE" class="java.lang.String"/>
	<field name="Partner_Flag" class="java.lang.String"/>
	<field name="Partner_Code" class="java.lang.String"/>
	<field name="COMPANY_NAME" class="java.lang.String"/>
	<field name="INDUSTRY" class="java.lang.String"/>
	<field name="CURRENT_YEAR_SSCW_BOOKINGS" class="java.lang.Long"/>
	<field name="CURRENT_YEAR_DYNAMIC_REVENUE" class="java.math.BigDecimal"/>
	<field name="BILLING_CITY" class="java.lang.String"/>
	<field name="BILLING_COUNTRY" class="java.lang.String"/>

		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="30"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="0"
						y="1"
						width="895"
						height="29"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="18" isBold="true"/>
					</textElement>
				<text><![CDATA[Partner OwnerShip Report]]></text>
				</staticText>
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="37"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="1"
						width="86"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-2"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[ACCOUNT]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="86"
						y="1"
						width="67"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-3"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[STAGE]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="153"
						y="1"
						width="50"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-4"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[PARTNER]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="203"
						y="1"
						width="58"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-5"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[PARTNER]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="261"
						y="1"
						width="157"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-7"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[LAST NAME]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="418"
						y="1"
						width="80"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-8"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[INDUSTRY]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="498"
						y="1"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-9"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[CURRENT YEAR]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="0"
						y="19"
						width="86"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-11"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[OWNER]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="86"
						y="19"
						width="67"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-12"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="153"
						y="19"
						width="50"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-13"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[FLAG]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="203"
						y="19"
						width="58"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-14"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[CODE]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="261"
						y="19"
						width="157"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-16"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="418"
						y="19"
						width="80"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-17"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="498"
						y="19"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-18"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[SSCW BOOKINGS]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="598"
						y="1"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-19"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[CURRENT YEAR]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="598"
						y="19"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-20"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[DYNAMIC REVENUE]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="698"
						y="1"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-21"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[BILLING CITY]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="698"
						y="19"
						width="100"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-22"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="798"
						y="1"
						width="97"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-23"/>
					<box></box>
					<textElement verticalAlignment="Bottom">
						<font/>
					</textElement>
				<text><![CDATA[BILLING]]></text>
				</staticText>
				<staticText>
					<reportElement
						mode="Opaque"
						x="798"
						y="19"
						width="97"
						height="18"
						backcolor="#CCCCCC"
						key="staticText-24"/>
					<box></box>
					<textElement verticalAlignment="Top">
						<font/>
					</textElement>
				<text><![CDATA[COUNTRY]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="18"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="0"
						width="86"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{ACCOUNT_OWNER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="86"
						y="0"
						width="67"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{STAGE}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="153"
						y="0"
						width="50"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Partner_Flag}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="203"
						y="0"
						width="58"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Partner_Code}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="261"
						y="0"
						width="157"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{COMPANY_NAME}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="698"
						y="0"
						width="100"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{BILLING_CITY}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="798"
						y="0"
						width="97"
						height="18"
						key="textField"/>
					<box></box>
					<textElement>
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{BILLING_COUNTRY}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="418"
						y="0"
						width="80"
						height="18"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{INDUSTRY}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="498"
						y="0"
						width="100"
						height="18"
						key="textField"/>
					<box rightPadding="5"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.Long"><![CDATA[$F{CURRENT_YEAR_SSCW_BOOKINGS}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="598"
						y="0"
						width="100"
						height="18"
						key="textField"/>
					<box rightPadding="5"></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{CURRENT_YEAR_DYNAMIC_REVENUE}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="16"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="180"
						y="1"
						width="600"
						height="15"
						key="staticText-25"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="10"/>
					</textElement>
				<text><![CDATA[PartnerOwnerShipReport.jrxml]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="780"
						y="1"
						width="50"
						height="15"
						key="textField-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Helvetica" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="830"
						y="1"
						width="65"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single">
						<font fontName="Helvetica" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false" isUnderline="false" isPdfEmbedded ="false" pdfEncoding ="CP1252" isStrikeThrough="false" />
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["  " + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="1"
						width="179"
						height="15"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Helvetica" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
