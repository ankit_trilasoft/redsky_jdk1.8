package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.DataSecurityFiltersDao;
import com.trilasoft.app.model.DataSecurityFilter;

public class DataSecurityFiltersDaoHibernate extends GenericDaoHibernate<DataSecurityFilter, Long> implements DataSecurityFiltersDao{
	
	
	 public DataSecurityFiltersDaoHibernate() {
		super(DataSecurityFilter.class);
		
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from DataSecurityFilter");
	}

	public List findFilters(String filter) {
		return getHibernateTemplate().find("from DataSecurityFilter where name='"+filter+"'");
	}

	public List getTableList(String sessionCorpID) {
		return getHibernateTemplate().find("select code from RefMaster where parameter='DATA_SECURITY_TABLE' group by code");
	}

	public List getFieldList(String tableName) {
		return getHibernateTemplate().find("select bucket from RefMaster where parameter='DATA_SECURITY_TABLE' and code='"+tableName+"'");
	}

	public List getAgentFilterList(String prefix, String sessionCorpID) {
		return getHibernateTemplate().find("from DataSecurityFilter where name like '"+prefix+"%' and corpID='"+sessionCorpID+"'");
	}

	public void deleteFilters(String prefix, String sessionCorpID) {
		prefix=prefix+"_";
		getHibernateTemplate().bulkUpdate("delete from DataSecurityFilter where name like '"+prefix+"%' and corpID='"+sessionCorpID+"'");
	}

	public List search(String name, String tableName, String fieldName, String filterValues) {
		if(name==null){name="";} 
		if(tableName==null){tableName="";} 
		if(fieldName==null){fieldName="";}
		return getHibernateTemplate().find("from DataSecurityFilter where name like '%"+name.replaceAll("'", "''") +"%' and tableName like '%"+tableName.replaceAll("'", "''")+"%' and fieldName like '%"+fieldName.replaceAll("'", "''")+"%' and filterValues like '%"+filterValues.replaceAll("'", "''")+"%'");
	}

	public List getOtherCorpidFilterList(String prefix, String corpidList){
		return this.getSession().createSQLQuery("select * from datasecurityfilter where name like '"+prefix+"%' and corpID in ("+corpidList+") ").list();
	}
}
