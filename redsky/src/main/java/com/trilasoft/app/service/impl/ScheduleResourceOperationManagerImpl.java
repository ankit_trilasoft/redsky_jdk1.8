package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.ScheduleResourceOperationDao;
import com.trilasoft.app.model.ScheduleResourceOperation;
import com.trilasoft.app.service.ScheduleResourceOperationManager;

public class ScheduleResourceOperationManagerImpl extends GenericManagerImpl<ScheduleResourceOperation,Long> implements ScheduleResourceOperationManager {
	ScheduleResourceOperationDao scheduleResourceOperationDao;
	public ScheduleResourceOperationManagerImpl(ScheduleResourceOperationDao scheduleResourceOperationDao) {
		super(scheduleResourceOperationDao);
		this.scheduleResourceOperationDao=scheduleResourceOperationDao;
	}
	public List findTimeSheet(ScheduleResourceOperation scheduleResourceOperation) {
		return scheduleResourceOperationDao.findTimeSheet(scheduleResourceOperation);
	}
	public List findTruckingOperation(ScheduleResourceOperation scheduleResourceOperation) {
		return scheduleResourceOperationDao.findTruckingOperation(scheduleResourceOperation);
	}
	
}
