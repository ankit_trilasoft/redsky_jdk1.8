package com.trilasoft.app.webapp.tags;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.XmlWebApplicationContext;
import com.trilasoft.app.webapp.action.RoleBasedComponentPermissionUtil;

public class RoleAndTransBasedAccessScriptTag extends TagSupport {

	// ~ Instance fields
	// ================================================================================================

	private RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil = null;
	
	private String[] tableList ;

	private String partnerIdList;

	private String transIdList;
	
	private String[] formNameList ;
	
	public RoleAndTransBasedAccessScriptTag(){
	}

	public String getTableList() {
		return tableList.toString();
	}

	public void setTableList(String tables) {
		this.tableList = tables.split(",");;
	}

	public String getFormNameList() {
		return formNameList.toString();
	}


	public void setFormNameList(String formNames) {
		this.formNameList = formNames.split(",");
	}
	
	public String getPartnerIdList() {
		return roleBasedComponentPermissionUtil.getPartnerIdList();
	}

	public void setPartnerIdList(String partnerIdList) {
		this.partnerIdList = partnerIdList;
	}

	public String getTransIdList() {
		return transIdList;
	}

	public void setTransIdList(String transIdList) {
		this.transIdList = transIdList;
	}

	
	private String getFormName(String tableName){
		for (int i = 0; i < tableList.length;i++){
			if (tableList[i].equals(tableName)){
				return formNameList[i]; 
			}
		}
		return null;
	}

	public void setRoleBasedComponentPermissionUtil(
			RoleBasedComponentPermissionUtil roleBasedComponentPermissionUtil) {
		this.roleBasedComponentPermissionUtil = roleBasedComponentPermissionUtil;
	}

	
	// ~ Methods
	// ========================================================================================================

	public int doStartTag() throws JspException {
		if ((null == tableList) || "".equals(tableList)) {
			return Tag.SKIP_BODY;
		}
		
		ServletContext context = pageContext.getServletContext();
		XmlWebApplicationContext ctx = (XmlWebApplicationContext) context.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);

		roleBasedComponentPermissionUtil = (RoleBasedComponentPermissionUtil) ctx.getBean("componentConfigByRoleUtil");		
		//boolean accessAllowed = roleBasedComponentPermissionUtil.getComponentAccessAttrbute(componentId);
		String additionalRoles = null;
		partnerIdList = getPartnerIdList();
		if (transIdList != null && partnerIdList != null){
			String transId = (transIdList.split(","))[0];
			additionalRoles = roleBasedComponentPermissionUtil.getUserRoleInTransaction(transId, partnerIdList);
		}
		pageContext.getRequest().setAttribute("extraRoles", additionalRoles);
		//System.err.println("1-->>"+pageContext.getRequest().getAttribute("extraRoles"));
		StringBuilder script = new StringBuilder("");
		List<String> permissions = roleBasedComponentPermissionUtil.getModulePermissions(tableList, additionalRoles);
		String tableName="";
		
		
		for (String componentPermission: permissions) {
			Integer permission = Integer.parseInt(componentPermission.substring(0,componentPermission.indexOf("#")));
			if (permission > 2) {
				int fieldNamePos = componentPermission.indexOf(".field.")+7;
				tableName = componentPermission.substring(fieldNamePos, componentPermission.lastIndexOf("."));
				String fieldName = componentPermission.substring(componentPermission.lastIndexOf(".")+1);
				String elementId = getFormName(tableName) +"_"+tableName+"_"+fieldName;
				String elementName=tableName+"."+fieldName;
					script.append("if (document.getElementById('"+ elementId +"')) {document.getElementById('"+ elementId +"').readOnly =false;}\r\n");
					script.append("if (document.getElementById('"+ elementId +"')) {document.getElementById('"+ elementId +"').className = 'input-text';}\r\n");
					script.append("if (document.getElementById('"+ elementId +"')) {if (document.getElementById('"+ elementId +"').type == 'select-one')   {document.getElementById('"+ elementId +"').disabled =false; document.getElementById('"+ elementId +"').className = 'list-menu';}}\r\n" );			
					script.append("if (document.getElementById('"+ elementId +"')) {if (document.getElementById('"+ elementId +"').type == 'checkbox')   {document.getElementById('"+ elementId +"').disabled =false;}}\r\n" );
					//script.append("if (document.getElementByName('"+ elementName +"')) {if (document.getElementByName('"+ elementName +"').type == 'radio')   {document.getElementByName('"+ elementName +"').disabled =false;}}\r\n" );
					script.append("if (document.getElementById('"+ elementId +"-trigger'))" +	" { document.getElementById('"+ elementId +"-trigger').src = 'images/calender.png';}\r\n");
					script.append("if (document.getElementById('"+ elementId +"-trigger'))" +	" { document.getElementById('"+ elementId +"-trigger').onclick = test1;}\r\n");
				
				
			}
		}
		if(tableName.equalsIgnoreCase("trackingStatus"))
		{
		if(additionalRoles.toString().contains("ROLE_ORIGIN_AGENT") || additionalRoles.toString().contains("ROLE_ORIGINSUB_AGENT"))
		{
			//script.append("document.forms['trackingStatusForm'].elements['weightType1'].disabled=false;\r\n");
			//script.append("document.forms['trackingStatusForm'].elements['weightType2'].disabled=false;\r\n");
		}
		}
		
		try {
			pageContext.getOut().write(script.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return  Tag.SKIP_BODY;
	}


}
