package com.trilasoft.app.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.PricingControl;
import com.trilasoft.app.model.PricingControlDetails;

public interface PricingControlDetailsDao extends GenericDao<PricingControlDetails, Long>{
	
	 public List checkById(Long id);
	 public List getSelectedAgent(Long id, String tariffApplicability, String sessionCorpID);
	 public List getFreightList(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, String originPortRange, String destinationPortRange, String mode, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude);
	 public List getSelectedRefFreight(Long id, String tariffApplicability, String sessionCorpID); 
	 public List getSelectedFreightFromDetails(Long id, String sessionCorpID);
	 public void selectMinimumQuoteAgent(Long id, String tarrifApplicability, String sessionCorpID, Boolean isFreight);
	 public void selectMinimumFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude);
	 public String getPortsCode(String portRange, String mode, String country, String latitude, String longitude);
	 public String getMinimumFreightCode(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long pricingControlID, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude);
	 public String getPortsPerFreight(String originPOE, String destinationPOE, String containerSize, Date expectedLoadDate, Long id, String baseCurrency, String mode, String contract, String originPortRange, String destinationPortRange, String originCountry, String destinationCountry, String originLatitude, String originLongitude, String destinationLatitude, String destinationLongitude);
	 public List getMasterFreightList(Long pricingControlId, String sessionCorpID);
	 public void updateRespectiveRecords(Long originAgentRecordId, Long destinationAgentRecordId, Long freightRecordId, Long pricingControlID);
	 public void updatePricingControlDetails(Long pricingControlId, Long agentId, String tariffApplicability);
	 public void updateFreightDetails(Long pricingControlId, Long freightAgentId);   
}
