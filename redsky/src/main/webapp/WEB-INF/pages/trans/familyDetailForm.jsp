<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>
<table id="accInnerTable" width="100%" border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding:0px;">
<c:set var="soCoordinator" value="${fn:trim(customerFile.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(user.username)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="passportNumberEncrypt" value="false"/>
<c:if test="${appUserName==soCoordinator}">
<c:set var="passportNumberEncrypt" value="true"/>
</c:if>
<c:set var="rolesAssignFlag" value="N" />
<c:forEach var="item" items="${roles}">
  <c:if test="${item eq 'ROLE_SUPERVISOR' || item eq 'ROLE_ADMIN'}">
    <c:set var="rolesAssignFlag" value="Y" />
  </c:if>
</c:forEach>

<configByCorp:fieldVisibility componentId="component.field.familydetails.passport.encrypted">
<c:set var="passportNumberEncryptShow" value="Y"/>
</configByCorp:fieldVisibility>
	<tr>
		<td>
        	<display:table name="dsFamilyDetailsList" class="table" requestURI="" id="dsFamilyDetailsList"  defaultsort="1"  style="width:100%;margin-bottom:5px;">
			    <display:column title="First Name" maxLength="35" >
				    <a href="javascript:openWindow('editDsFamilyDetails.html?id=${dsFamilyDetailsList.id}&customerFileId=${customerFile.id}&decorator=popup&popup=true',1350,500)">
				    <c:out value="${dsFamilyDetailsList.firstName}"></c:out></a>
			    </display:column>
			 
			    <display:column property="lastName" maxLength="15"  title="Last Name" />
			    <display:column property="relationship"  title="Relationship" />   
			    <display:column property="cellNumber"  title="Mobile Phone"  />
			    <display:column property="email" maxLength="15" title="Email"  />
			    <display:column property="dateOfBirth" title="Date Of Birth" format="{0,date,dd-MMM-yyyy}" />
			    <display:column property="age" title="Age"/>
			  <%-- <display:column  title="Passport #" > --%>
			    <c:choose>
			    <c:when test="${passportNumberEncryptShow=='Y'}">	
			   <display:column  title="Passport #" >
			    <c:choose>
				<c:when test="${(usertype=='ACCOUNT')||(passportNumberEncrypt =='true') || (rolesAssignFlag=='Y')}">
					
						<c:out value="${dsFamilyDetailsList.passportNumber}"></c:out>
						
				</c:when>
				<c:otherwise>
					<c:if test="${fn:length(dsFamilyDetailsList.passportNumber) > 0}" >
			    		<c:set var="encryptedpassportNumber" value="${fn:substring(dsFamilyDetailsList.passportNumber, fn:length(dsFamilyDetailsList.passportNumber)-4, fn:length(dsFamilyDetailsList.passportNumber))}" />
			    		<c:set var="encryptedValue" value="XXXXXXXXXXXX-" />
			    		<c:set var="actualencryptedpassportNumber" value="${encryptedValue}${encryptedpassportNumber}" /> 
			    		<c:out value="${actualencryptedpassportNumber}"></c:out>
			    	</c:if>
				</c:otherwise>
				</c:choose>
				</display:column>
				</c:when>
				<c:otherwise>
				<display:column property="passportNumber" title="Passport #"  />
				</c:otherwise>
				</c:choose>
				<display:column property="expiryDate" title="Expiry Date" format="{0,date,dd-MMM-yyyy}" /> 
				<display:column property="countryOfIssue"  title="Country Of Issue" /> 
			    <display:column property="nationality"  title="Nationality"/>
				<display:column property="relocatingWithCoWorker"  title="Relocating With CoWorker"/>
				 
				
				<c:choose>
					<c:when test="${usertype=='ACCOUNT' || usertype=='AGENT' || visibilityForSalesPortla=='true'}">	
					</c:when>
					<c:otherwise>
						<display:column title="Remove" style="width: 15px;">
							<a><img align="middle" title="User List" onclick="deleteFamilyDetails('${dsFamilyDetailsList.id}');" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recycle.gif"/></a>
						</display:column>
					</c:otherwise>
				</c:choose>
			</display:table>
		</td>
	</tr>
</table>
