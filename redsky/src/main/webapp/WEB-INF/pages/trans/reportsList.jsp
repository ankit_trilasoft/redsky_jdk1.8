<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head> 
<c:choose>
	<c:when test="${marked == 'yes'}">
		 <title><fmt:message key="bookMarkreportsList.title"/></title> 
    	<meta name="heading" content="<fmt:message key='bookMarkreportsList.heading'/>"/>     
	</c:when>
	<c:otherwise>
		 <title><fmt:message key="reportsList.title"/></title> 
    	<meta name="heading" content="<fmt:message key='reportsList.heading'/>"/>     
	</c:otherwise>
</c:choose>
   
<script language="javascript" type="text/javascript">
		function clear_fields(){
			document.forms['searchForm'].elements['reports.menu'].value = "";
			document.forms['searchForm'].elements['reports.module'].value = "";
			document.forms['searchForm'].elements['reports.subModule'].value = "";
			document.forms['searchForm'].elements['reports.description'].value = "";
		}
			
		
	function userStatusCheck(targetElement){
	
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
      		
      		if(userCheckStatus == ''){
	  			document.forms['searchForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['searchForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['searchForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['searchForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}
function bookMarkReports(){
		var reportId = document.forms['searchForm'].elements['userCheck'].value;
		if(reportId =='' || reportId ==','){
			alert('Please select the one or more report to bookmark.');
		}else{
			location.href = 'bookMarked.html?userCheck='+reportId;
		}
}

function removeBookMarkReports(){
		var reportId = document.forms['searchForm'].elements['userCheck'].value;
		if(reportId =='' || reportId ==','){
			alert('Please select the one or more report to remove bookmark.');
		}else{
			location.href = 'removeBookMarked.html?userCheck='+reportId;
		}
}
function checkAll(){
	document.forms['searchForm'].elements['userCheck'].value = "";
	document.forms['searchForm'].elements['userCheck'].value = "";
	var len = document.forms['searchForm'].elements['bookMark'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['bookMark'][i].checked = true ;
		userStatusCheck(document.forms['searchForm'].elements['bookMark'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['searchForm'].elements['bookMark'].length;
	for (i = 0; i < len; i++){
		document.forms['searchForm'].elements['bookMark'][i].checked = false ;
		userStatusCheck(document.forms['searchForm'].elements['bookMark'][i]);
	}
	document.forms['searchForm'].elements['userCheck'].value="";
}

function show(theTable){

     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}

</script>
	
	<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:0px;
margin-top:-18px;
!margin-top:-33px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}

/*#inner-content{margin-top:60px;}*/
#liquid-round{margin-bottom:28px;}
</style>
	
	
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="search" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px; " onclick="clear_fields();"/> 
</c:set> 
<s:form id="searchForm" action="searchReportss" method="post" validate="true"> 
<s:hidden name="userCheck" value="%{userCheck}"/>

<s:hidden name="marked" value="${marked}"/>  

<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%;"  >
	<thead>
		<tr>
			<th align="center"><fmt:message key="reports.menu"/>
			<th align="center"><fmt:message key="reports.module"/>
			<th align="center"><fmt:message key="reports.subModule"/>
			<th align="center"><fmt:message key="reports.description"/></th>	
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td width="" align="left"><s:textfield name="reports.menu" required="true" cssClass="input-text" size="30"/></td>
			<td width="" align="left">
			<s:textfield  name="reports.module" required="true" cssClass="input-text" size="30" />
			</td>
  			<td width="" align="left">
            <s:textfield  name="reports.subModule" required="true" cssClass="input-text" size="30" />    
            </td>
			<td width="" align="left"><s:textfield name="reports.description" required="true" cssClass="input-text" size="30"/></td>
		</tr>
		<tr>
		    <td colspan="3"></td>	
			
			<td width="" align="center" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:out value="${searchresults}" escapeXml="false" />
<s:set name="reportss" value="reportss" scope="request"/> 

<div id="newmnav" style="width: 500px;float: left;margin-top:-15px; ">
	<ul>
	<c:if test="${marked != 'yes'}">
	 <c:choose>
      <c:when test='${jobSearchPersistValue=="Sunil"}'>
			<li><a href="bookMarekdList.html"><span>BookMarked Reports List</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Reports List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</c:when>
		<c:otherwise>
		<li><a href="bookMarekdList.html"><span>BookMarked Reports List</span></a></li>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Reports List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</c:otherwise>
		</c:choose>
		</c:if>
	  <c:if test="${marked == 'yes'}">
	  <c:choose>
      <c:when test='${jobSearchPersistValue=="Sunil"}'>
      <li id="newmnav1" style="background:#FFF "><a class="current"><span>BookMarked Reports List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	  <li><a href="searchReportss.html"><span>Reports List</span></a></li>
	  </c:when>
        <c:otherwise>
        <li id="newmnav1" style="background:#FFF "><a class="current"><span>BookMarked Reports List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	   <li><a href="reportss.html"><span>Reports List</span></a></li>
        </c:otherwise>
    </c:choose>
  </c:if>
	  	  
				<input type="radio" style="vertical-align: sub;"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
				<input type="radio" style="vertical-align: sub;"  name="chk" onClick="uncheckAll()" /><strong>Uncheck All</strong>
		
</ul>
</div>
<div class="spn">&nbsp;</div>	
<display:table name="reportss" class="table" requestURI="" id="reportsList" defaultsort="2" pagesize="10" style="width:100%;">

	<c:choose>
		<c:when test="${marked == 'yes'}">
			<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark" name="bookMark"   value="${reportsList.bid}"  onclick="userStatusCheck(this)"/></display:column>
		</c:when>
		<c:otherwise>
			<c:choose>
	 			<c:when test="${reportsList.id == reportsList.brid && reportsList.bkMarkcreatedBy == pageContext.request.remoteUser}">
	 				<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark1" name="bookMark1"   value="${reportsList.id}"  onclick="userStatusCheck(this)"  disabled="disabled" checked /></display:column>
	 			</c:when>
	 			<c:otherwise>
	 				<display:column title=""><input type="checkbox" style="margin-left:10px;" id="bookMark" name="bookMark"   value="${reportsList.id}"  onclick="userStatusCheck(this)"  /></display:column>
	 			</c:otherwise>
 			</c:choose>
		</c:otherwise>
	</c:choose>	
	<display:column sortable="true" sortProperty="menu" titleKey="reports.menu" style="width:160px"><a href="javascript:openWindow('viewReportParam.html?id=${reportsList.id}&reportName=${reportsList.description}&docsxfer=${reportsList.docsxfer}&list=main&decorator=popup&popup=true',550,650)"><c:out value="${reportsList.menu}" /></a></display:column>
    <display:column property="module" sortable="true" titleKey="reports.module"/>
     <display:column property="subModule" sortable="true" titleKey="reports.subModule"/>
    <display:column property="description" sortable="true" titleKey="reports.description"/>
    <display:column property="reportComment" sortable="true" titleKey="reports.reportComment" style="width:300px"/>
      
      
            
    <display:setProperty name="paging.banner.item_name" value="itemsJbkEquip"/> 
    <display:setProperty name="paging.banner.items_name" value="itemsJbkEquip"/>
    
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/>
</display:table>
<c:if test="${marked == null || marked != 'yes'}">
	<input type="button" class="cssbutton" style="width:150px;" onclick="bookMarkReports();" value="Save As Bookmark"/>
</c:if>
<c:if test="${marked == 'yes'}">
	<input type="button" class="cssbutton" style="width:150px;" onclick="removeBookMarkReports();" value="Remove Bookmark"/>
</c:if>

</s:form>
<script type="text/javascript"> 
try{
if('<%=session.getAttribute("Module")%>'=='null'){
   		document.forms['searchForm'].elements['reports.module'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("Module")%>'!='null'){
   		document.forms['searchForm'].elements['reports.module'].value='<%=session.getAttribute("Module")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("SubModule")%>'=='null'){
   		document.forms['searchForm'].elements['reports.subModule'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("SubModule")%>'!='null'){
   		document.forms['searchForm'].elements['reports.subModule'].value='<%=session.getAttribute("SubModule")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("Menu")%>'=='null'){
   		document.forms['searchForm'].elements['reports.menu'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("Menu")%>'!='null'){
   		document.forms['searchForm'].elements['reports.menu'].value='<%=session.getAttribute("Menu")%>';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("Description")%>'=='null'){
   		document.forms['searchForm'].elements['reports.description'].value='';
   	}
   	}
   	catch(e){}
   	try{
   	if('<%=session.getAttribute("Description")%>'!='null'){
   		document.forms['searchForm'].elements['reports.description'].value='<%=session.getAttribute("Description")%>';
   	}
   	}
   	catch(e){}
</script>
<script type="text/javascript">
if('${searchFlag}'=='0'){
	 document.forms['searchForm'].elements['reports.menu'].value = "";
	document.forms['searchForm'].elements['reports.module'].value = "";
	document.forms['searchForm'].elements['reports.subModule'].value = "";
	document.forms['searchForm'].elements['reports.description'].value = "";
}
	
</script>