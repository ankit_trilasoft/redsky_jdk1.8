package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;

import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;


public class ContactDataSearchAction extends BaseAction implements Preparable {
	
    private List userContactList = new ArrayList();
    static final Logger logger = Logger.getLogger(TrackingStatusAction.class);	
	private String partnerCode;
	private String sessionCorpID;
	private String userName;
	private String userEmailId;
	private String phone_number;
	private String usertype;
	private UserManager userManager;
	public UserManager getUserManager() {
		return userManager;
	}
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String contactsearchList()
	{
		return SUCCESS;
	}
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	public ContactDataSearchAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.usertype = user.getUserType();
	}

	public String searchContact(){

		try {
			userContactList = userManager.getContactList(partnerCode, sessionCorpID,userName,userEmailId,phone_number);
	
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	public List getUserContactList() {
		return userContactList;
	}

	public void setUserContactList(List userContactList) {
		this.userContactList = userContactList;
	}
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	
	
}
