DELIMITER $$
	DROP trigger IF EXISTS `redsky`.`trigger_add_history_partnerpublic` $$

CREATE trigger redsky.trigger_add_history_partnerpublic
BEFORE UPDATE on redsky.partnerpublic
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";

   IF (OLD.parent <> NEW.parent ) THEN
      select CONCAT(fieldNameList,'partnerpublic.parent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.parent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.parent,'~') into newValueList;
  END IF;

   IF (OLD.firstName <> NEW.firstName ) THEN
      select CONCAT(fieldNameList,'partnerpublic.firstName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.firstName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.firstName,'~') into newValueList;
  END IF;

   IF (OLD.lastName <> NEW.lastName ) THEN
      select CONCAT(fieldNameList,'partnerpublic.lastName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastName,'~') into newValueList;
  END IF;
   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'partnerpublic.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;

   IF (OLD.middleInitial <> NEW.middleInitial ) THEN
      select CONCAT(fieldNameList,'partnerpublic.middleInitial~') into fieldNameList;
      select CONCAT(oldValueList,OLD.middleInitial,'~') into oldValueList;
      select CONCAT(newValueList,NEW.middleInitial,'~') into newValueList;
  END IF;
  IF (OLD.PAIMA <> NEW.PAIMA ) THEN
      select CONCAT(fieldNameList,'partnerpublic.PAIMA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.PAIMA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.PAIMA,'~') into newValueList;
  END IF;
  IF (OLD.LACMA <> NEW.LACMA ) THEN
      select CONCAT(fieldNameList,'partnerpublic.LACMA~') into fieldNameList;
      select CONCAT(oldValueList,OLD.LACMA,'~') into oldValueList;
      select CONCAT(newValueList,NEW.LACMA,'~') into newValueList;
  END IF;
  IF (OLD.eurovanNetwork <> NEW.eurovanNetwork ) THEN
      select CONCAT(fieldNameList,'partnerpublic.eurovanNetwork~') into fieldNameList;
      select CONCAT(oldValueList,OLD.eurovanNetwork,'~') into oldValueList;
      select CONCAT(newValueList,NEW.eurovanNetwork,'~') into newValueList;
  END IF;

   IF (OLD.billingAddress1 <> NEW.billingAddress1 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingAddress1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress1,'~') into newValueList;
  END IF;

   IF (OLD.billingAddress2 <> NEW.billingAddress2 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingAddress2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress2,'~') into newValueList;
  END IF;
  IF (OLD.billingAddress3 <> NEW.billingAddress3 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingAddress3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress3,'~') into newValueList;
  END IF;

   IF (OLD.billingAddress4 <> NEW.billingAddress4 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingAddress4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingAddress4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingAddress4,'~') into newValueList;
  END IF;
   IF (OLD.billingCity <> NEW.billingCity ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingCity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCity,'~') into newValueList;
  END IF;
   IF (OLD.billingCountry <> NEW.billingCountry ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCountry,'~') into newValueList;
  END IF;
   IF (OLD.billingCountryCode <> NEW.billingCountryCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingCountryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCountryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCountryCode,'~') into newValueList;
  END IF;
 IF (OLD.billingEmail <> NEW.billingEmail ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingEmail,'~') into newValueList;
  END IF;

   IF (OLD.billingFax <> NEW.billingFax ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingFax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingFax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingFax,'~') into newValueList;
  END IF;

   IF (OLD.billingPhone <> NEW.billingPhone ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingPhone,'~') into newValueList;
    END IF;  
     
    IF (OLD.mergeInto <> NEW.mergeInto ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mergeInto~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mergeInto,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mergeInto,'~') into newValueList;
   END IF; 
     
   IF (OLD.ugwwCarrierCode <> NEW.ugwwCarrierCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.ugwwCarrierCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.ugwwCarrierCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.ugwwCarrierCode,'~') into newValueList; 
  END IF;
  
  IF (OLD.billingState <> NEW.billingState ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingState,'~') into newValueList;
  END IF;

   IF (OLD.billingTelex <> NEW.billingTelex ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingTelex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingTelex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingTelex,'~') into newValueList;
  END IF;
 IF (OLD.billingZip <> NEW.billingZip ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingZip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingZip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingZip,'~') into newValueList;
  END IF;

   IF (OLD.mailingAddress1 <> NEW.mailingAddress1 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingAddress1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingAddress1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingAddress1,'~') into newValueList;
  END IF;
   IF (OLD.mailingAddress2 <> NEW.mailingAddress2 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingAddress2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingAddress2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingAddress2,'~') into newValueList;
  END IF;
   IF (OLD.mailingAddress3 <> NEW.mailingAddress3 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingAddress3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingAddress3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingAddress3,'~') into newValueList;
  END IF;
   IF (OLD.mailingAddress4 <> NEW.mailingAddress4 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingAddress4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingAddress4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingAddress4,'~') into newValueList;
  END IF;



   IF (OLD.mailingCity <> NEW.mailingCity ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingCity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingCity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingCity,'~') into newValueList;
  END IF;



   IF (OLD.mailingCountry <> NEW.mailingCountry ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingCountry,'~') into newValueList;
  END IF;



   IF (OLD.mailingCountryCode <> NEW.mailingCountryCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingCountryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingCountryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingCountryCode,'~') into newValueList;
  END IF;



   IF (OLD.mailingEmail <> NEW.mailingEmail ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingEmail,'~') into newValueList;
  END IF;






   IF (OLD.mailingFax <> NEW.mailingFax ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingFax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingFax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingFax,'~') into newValueList;
  END IF;




   IF (OLD.mailingPhone <> NEW.mailingPhone ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingPhone,'~') into newValueList;
  END IF;




   IF (OLD.mailingState <> NEW.mailingState ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingState,'~') into newValueList;
  END IF;








   IF (OLD.mailingTelex <> NEW.mailingTelex ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingTelex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingTelex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingTelex,'~') into newValueList;
  END IF;









   IF (OLD.mailingZip <> NEW.mailingZip ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mailingZip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mailingZip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mailingZip,'~') into newValueList;
  END IF;
   IF (OLD.partnerCode <> NEW.partnerCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.partnerCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerCode,'~') into newValueList;
  END IF;

   IF (OLD.partnerPrefix <> NEW.partnerPrefix ) THEN
      select CONCAT(fieldNameList,'partnerpublic.partnerPrefix~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerPrefix,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerPrefix,'~') into newValueList;
  END IF;
IF (OLD.partnerSuffix <> NEW.partnerSuffix ) THEN
      select CONCAT(fieldNameList,'partnerpublic.partnerSuffix~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerSuffix,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerSuffix,'~') into newValueList;
  END IF;
IF (OLD.terminalAddress1 <> NEW.terminalAddress1 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalAddress1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalAddress1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalAddress1,'~') into newValueList;
  END IF;
  IF (OLD.terminalAddress2 <> NEW.terminalAddress2 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalAddress2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalAddress2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalAddress2,'~') into newValueList;
  END IF;
   IF (OLD.terminalAddress3 <> NEW.terminalAddress3 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalAddress3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalAddress3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalAddress3,'~') into newValueList;
  END IF;

   IF (OLD.terminalAddress4 <> NEW.terminalAddress4 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalAddress4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalAddress4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalAddress4,'~') into newValueList;
  END IF;
IF (OLD.terminalCity <> NEW.terminalCity ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalCity~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalCity,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalCity,'~') into newValueList;
  END IF;
 IF (OLD.terminalCountry <> NEW.terminalCountry ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalCountry~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalCountry,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalCountry,'~') into newValueList;
  END IF;

   IF (OLD.terminalCountryCode <> NEW.terminalCountryCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalCountryCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalCountryCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalCountryCode,'~') into newValueList;
  END IF;
   IF (OLD.terminalEmail <> NEW.terminalEmail ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalEmail,'~') into newValueList;
  END IF;

   IF (OLD.terminalFax <> NEW.terminalFax ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalFax~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalFax,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalFax,'~') into newValueList;
  END IF;
  IF (OLD.terminalPhone <> NEW.terminalPhone ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalPhone~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalPhone,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalPhone,'~') into newValueList;
  END IF;
   IF (OLD.terminalState <> NEW.terminalState ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalState~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalState,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalState,'~') into newValueList;
  END IF;
   IF (OLD.terminalTelex <> NEW.terminalTelex ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalTelex~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalTelex,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalTelex,'~') into newValueList;
  END IF;
   IF (OLD.terminalZip <> NEW.terminalZip ) THEN
      select CONCAT(fieldNameList,'partnerpublic.terminalZip~') into fieldNameList;
      select CONCAT(oldValueList,OLD.terminalZip,'~') into oldValueList;
      select CONCAT(newValueList,NEW.terminalZip,'~') into newValueList;
  END IF;
   IF (OLD.typeOfVendor <> NEW.typeOfVendor ) THEN
      select CONCAT(fieldNameList,'partnerpublic.typeOfVendor~') into fieldNameList;
      select CONCAT(oldValueList,OLD.typeOfVendor,'~') into oldValueList;
      select CONCAT(newValueList,NEW.typeOfVendor,'~') into newValueList;
  END IF;
   IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'partnerpublic.status~') into fieldNameList;
      select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
      select CONCAT(newValueList,NEW.status,'~') into newValueList;
  END IF;
   IF (OLD.agentParent <> NEW.agentParent ) THEN
      select CONCAT(fieldNameList,'partnerpublic.agentParent~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentParent,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentParent,'~') into newValueList;
  END IF;
   IF (OLD.location1 <> NEW.location1 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.location1~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location1,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location1,'~') into newValueList;
  END IF;
   IF (OLD.location2 <> NEW.location2 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.location2~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location2,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location2,'~') into newValueList;
  END IF;
   IF (OLD.location3 <> NEW.location3 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.location3~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location3,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location3,'~') into newValueList;
  END IF;
   IF (OLD.location4 <> NEW.location4 ) THEN
      select CONCAT(fieldNameList,'partnerpublic.location4~') into fieldNameList;
      select CONCAT(oldValueList,OLD.location4,'~') into oldValueList;
      select CONCAT(newValueList,NEW.location4,'~') into newValueList;
  END IF;
   IF (OLD.companyProfile <> NEW.companyProfile ) THEN
      select CONCAT(fieldNameList,'partnerpublic.companyProfile~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyProfile,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyProfile,'~') into newValueList;
  END IF;
   IF (OLD.url <> NEW.url ) THEN
      select CONCAT(fieldNameList,'partnerpublic.url~') into fieldNameList;
      select CONCAT(oldValueList,OLD.url,'~') into oldValueList;
      select CONCAT(newValueList,NEW.url,'~') into newValueList;
  END IF;
   IF (OLD.trackingUrl <> NEW.trackingUrl ) THEN
      select CONCAT(fieldNameList,'partnerpublic.trackingUrl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.trackingUrl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.trackingUrl,'~') into newValueList;
  END IF;
   IF (OLD.yearEstablished <> NEW.yearEstablished ) THEN
      select CONCAT(fieldNameList,'partnerpublic.yearEstablished~') into fieldNameList;
      select CONCAT(oldValueList,OLD.yearEstablished,'~') into oldValueList;
      select CONCAT(newValueList,NEW.yearEstablished,'~') into newValueList;
  END IF;
   IF (OLD.companyFacilities <> NEW.companyFacilities ) THEN
      select CONCAT(fieldNameList,'partnerpublic.companyFacilities~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyFacilities,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyFacilities,'~') into newValueList;
  END IF;
  IF (OLD.companyCapabilities <> NEW.companyCapabilities ) THEN
      select CONCAT(fieldNameList,'partnerpublic.companyCapabilities~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyCapabilities,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyCapabilities,'~') into newValueList;
  END IF;
   IF (OLD.companyDestiantionProfile <> NEW.companyDestiantionProfile ) THEN
      select CONCAT(fieldNameList,'partnerpublic.companyDestiantionProfile~') into fieldNameList;
      select CONCAT(oldValueList,OLD.companyDestiantionProfile,'~') into oldValueList;
      select CONCAT(newValueList,NEW.companyDestiantionProfile,'~') into newValueList;
  END IF;
   IF (OLD.serviceRangeKms <> NEW.serviceRangeKms ) THEN
      select CONCAT(fieldNameList,'partnerpublic.serviceRangeKms~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceRangeKms,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceRangeKms,'~') into newValueList;
  END IF;
   IF (OLD.serviceRangeMiles <> NEW.serviceRangeMiles ) THEN
      select CONCAT(fieldNameList,'partnerpublic.serviceRangeMiles~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceRangeMiles,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceRangeMiles,'~') into newValueList;
  END IF;
   IF (OLD.fidiNumber <> NEW.fidiNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.fidiNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fidiNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fidiNumber,'~') into newValueList;
  END IF;
   IF (OLD.OMNINumber <> NEW.OMNINumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.OMNINumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.OMNINumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.OMNINumber,'~') into newValueList;
  END IF;
   IF (OLD.IAMNumber <> NEW.IAMNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.IAMNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.IAMNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.IAMNumber,'~') into newValueList;
  END IF;
   IF (OLD.AMSANumber <> NEW.AMSANumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.AMSANumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.AMSANumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.AMSANumber,'~') into newValueList;
  END IF;
   IF (OLD.WERCNumber <> NEW.WERCNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.WERCNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.WERCNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.WERCNumber,'~') into newValueList;
  END IF;
   IF (OLD.facilitySizeSQFT <> NEW.facilitySizeSQFT ) THEN
      select CONCAT(fieldNameList,'partnerpublic.facilitySizeSQFT~') into fieldNameList;
      select CONCAT(oldValueList,OLD.facilitySizeSQFT,'~') into oldValueList;
      select CONCAT(newValueList,NEW.facilitySizeSQFT,'~') into newValueList;
  END IF;
   IF (OLD.qualityCertifications <> NEW.qualityCertifications ) THEN
      select CONCAT(fieldNameList,'partnerpublic.qualityCertifications~') into fieldNameList;
      select CONCAT(oldValueList,OLD.qualityCertifications,'~') into oldValueList;
      select CONCAT(newValueList,NEW.qualityCertifications,'~') into newValueList;
  END IF;
   IF (OLD.vanLineAffiliation <> NEW.vanLineAffiliation ) THEN
      select CONCAT(fieldNameList,'partnerpublic.vanLineAffiliation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLineAffiliation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLineAffiliation,'~') into newValueList;
  END IF;

   IF (OLD.serviceLines <> NEW.serviceLines ) THEN
      select CONCAT(fieldNameList,'partnerpublic.serviceLines~') into fieldNameList;
      select CONCAT(oldValueList,OLD.serviceLines,'~') into oldValueList;
      select CONCAT(newValueList,NEW.serviceLines,'~') into newValueList;
  END IF;

   IF (OLD.facilitySizeSQMT <> NEW.facilitySizeSQMT ) THEN
      select CONCAT(fieldNameList,'partnerpublic.facilitySizeSQMT~') into fieldNameList;
      select CONCAT(oldValueList,OLD.facilitySizeSQMT,'~') into oldValueList;
      select CONCAT(newValueList,NEW.facilitySizeSQMT,'~') into newValueList;
  END IF;

   IF (OLD.mergedCode <> NEW.mergedCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.mergedCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mergedCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mergedCode,'~') into newValueList;
  END IF;

   IF (OLD.agentParentName <> NEW.agentParentName ) THEN
      select CONCAT(fieldNameList,'partnerpublic.agentParentName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentParentName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentParentName,'~') into newValueList;
  END IF;

   IF (OLD.reloContact <> NEW.reloContact ) THEN
      select CONCAT(fieldNameList,'partnerpublic.reloContact~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reloContact,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reloContact,'~') into newValueList;
  END IF;

   IF (OLD.reloContactEmail <> NEW.reloContactEmail ) THEN
      select CONCAT(fieldNameList,'partnerpublic.reloContactEmail~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reloContactEmail,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reloContactEmail,'~') into newValueList;
  END IF;

   IF (OLD.reloServices <> NEW.reloServices ) THEN
      select CONCAT(fieldNameList,'partnerpublic.reloServices~') into fieldNameList;
      select CONCAT(oldValueList,OLD.reloServices,'~') into oldValueList;
      select CONCAT(newValueList,NEW.reloServices,'~') into newValueList;
  END IF;

   IF (OLD.minimumDFB <> NEW.minimumDFB ) THEN
      select CONCAT(fieldNameList,'partnerpublic.minimumDFB~') into fieldNameList;
      select CONCAT(oldValueList,OLD.minimumDFB,'~') into oldValueList;
      select CONCAT(newValueList,NEW.minimumDFB,'~') into newValueList;
  END IF;

   IF (OLD.isNetworkPartner <> NEW.isNetworkPartner ) THEN
      select CONCAT(fieldNameList,'partnerpublic.isNetworkPartner~') into fieldNameList;
      select CONCAT(oldValueList,OLD.isNetworkPartner,'~') into oldValueList;
      select CONCAT(newValueList,NEW.isNetworkPartner,'~') into newValueList;
  END IF;
   IF (OLD.utsNumber <> NEW.utsNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.utsNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.utsNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.utsNumber,'~') into newValueList;
  END IF;
   IF (OLD.contactName <> NEW.contactName ) THEN
      select CONCAT(fieldNameList,'partnerpublic.contactName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contactName,'~') into newValueList;
  END IF;

   IF (OLD.vanLastLocation <> NEW.vanLastLocation ) THEN
      select CONCAT(fieldNameList,'partnerpublic.vanLastLocation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLastLocation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLastLocation,'~') into newValueList;
  END IF;

   IF (OLD.vanAvailCube <> NEW.vanAvailCube ) THEN
      select CONCAT(fieldNameList,'partnerpublic.vanAvailCube~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanAvailCube,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanAvailCube,'~') into newValueList;
  END IF;

   IF (OLD.vanLastReportTime <> NEW.vanLastReportTime ) THEN
      select CONCAT(fieldNameList,'partnerpublic.vanLastReportTime~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vanLastReportTime,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vanLastReportTime,'~') into newValueList;
  END IF;

   IF (OLD.nextVanLocation <> NEW.nextVanLocation ) THEN
      select CONCAT(fieldNameList,'partnerpublic.nextVanLocation~') into fieldNameList;
      select CONCAT(oldValueList,OLD.nextVanLocation,'~') into oldValueList;
      select CONCAT(newValueList,NEW.nextVanLocation,'~') into newValueList;
  END IF;

   IF (OLD.currentVanAgency <> NEW.currentVanAgency ) THEN
      select CONCAT(fieldNameList,'partnerpublic.currentVanAgency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentVanAgency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentVanAgency,'~') into newValueList;
  END IF;
   IF (OLD.currentVanID <> NEW.currentVanID ) THEN
      select CONCAT(fieldNameList,'partnerpublic.currentVanID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentVanID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentVanID,'~') into newValueList;
  END IF;
   IF (OLD.currentTractorAgency <> NEW.currentTractorAgency ) THEN
      select CONCAT(fieldNameList,'partnerpublic.currentTractorAgency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentTractorAgency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentTractorAgency,'~') into newValueList;
  END IF;
   IF (OLD.currentTractorID <> NEW.currentTractorID ) THEN
      select CONCAT(fieldNameList,'partnerpublic.currentTractorID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.currentTractorID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.currentTractorID,'~') into newValueList;
  END IF;
   IF (OLD.aliasName <> NEW.aliasName ) THEN
      select CONCAT(fieldNameList,'partnerpublic.aliasName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.aliasName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.aliasName,'~') into newValueList;
  END IF;
   IF (OLD.movingCompanyType <> NEW.movingCompanyType ) THEN
      select CONCAT(fieldNameList,'partnerpublic.movingCompanyType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.movingCompanyType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.movingCompanyType,'~') into newValueList;
  END IF;

   IF (OLD.partnerType <> NEW.partnerType ) THEN
      select CONCAT(fieldNameList,'partnerpublic.partnerType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.partnerType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.partnerType,'~') into newValueList;
  END IF;

   IF (OLD.associatedAgents <> NEW.associatedAgents ) THEN
      select CONCAT(fieldNameList,'partnerpublic.associatedAgents~') into fieldNameList;
      select CONCAT(oldValueList,OLD.associatedAgents,'~') into oldValueList;
      select CONCAT(newValueList,NEW.associatedAgents,'~') into newValueList;
  END IF;
   IF (OLD.UTSmovingCompanyType <> NEW.UTSmovingCompanyType ) THEN
      select CONCAT(fieldNameList,'partnerpublic.UTSmovingCompanyType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.UTSmovingCompanyType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.UTSmovingCompanyType,'~') into newValueList;
  END IF;

   IF (OLD.transPort <> NEW.transPort ) THEN
      select CONCAT(fieldNameList,'partnerpublic.transPort~') into fieldNameList;
      select CONCAT(oldValueList,OLD.transPort,'~') into oldValueList;
      select CONCAT(newValueList,NEW.transPort,'~') into newValueList;
  END IF;

   IF (OLD.billingCurrency <> NEW.billingCurrency ) THEN
      select CONCAT(fieldNameList,'partnerpublic.billingCurrency~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billingCurrency,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billingCurrency,'~') into newValueList;
  END IF;
   IF (OLD.bankCode <> NEW.bankCode ) THEN
      select CONCAT(fieldNameList,'partnerpublic.bankCode~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankCode,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankCode,'~') into newValueList;
  END IF;
   IF (OLD.bankAccountNumber <> NEW.bankAccountNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.bankAccountNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bankAccountNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bankAccountNumber,'~') into newValueList;
  END IF;
   IF (OLD.vatNumber <> NEW.vatNumber ) THEN
      select CONCAT(fieldNameList,'partnerpublic.vatNumber~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vatNumber,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vatNumber,'~') into newValueList;
  END IF;

   IF (OLD.agentGroup <> NEW.agentGroup ) THEN
      select CONCAT(fieldNameList,'partnerpublic.agentGroup~') into fieldNameList;
      select CONCAT(oldValueList,OLD.agentGroup,'~') into oldValueList;
      select CONCAT(newValueList,NEW.agentGroup,'~') into newValueList;
  END IF;
  IF ((OLD.effectiveDate <> NEW.effectiveDate) or (OLD.effectiveDate is null and NEW.effectiveDate is not null)
      or (OLD.effectiveDate is not null and NEW.effectiveDate is null)) THEN
       select CONCAT(fieldNameList,'partnerpublic.effectiveDate~') into fieldNameList;
       IF(OLD.effectiveDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.effectiveDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.effectiveDate is not null) THEN
        	select CONCAT(oldValueList,OLD.effectiveDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.effectiveDate is not null) THEN
        	select CONCAT(newValueList,NEW.effectiveDate,'~') into newValueList;
      	END IF;
   END IF;


  IF ((OLD.startDate <> NEW.startDate) or (OLD.startDate is null and NEW.startDate is not null)
      or (OLD.startDate is not null and NEW.startDate is null)) THEN
       select CONCAT(fieldNameList,'partnerpublic.startDate~') into fieldNameList;
       IF(OLD.startDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.startDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.startDate is not null) THEN
        	select CONCAT(oldValueList,OLD.startDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.startDate is not null) THEN
        	select CONCAT(newValueList,NEW.startDate,'~') into newValueList;
      	END IF;
   END IF;


  IF ((OLD.vanLastReportOn <> NEW.vanLastReportOn) or (OLD.vanLastReportOn is null and NEW.vanLastReportOn is not null)
      or (OLD.vanLastReportOn is not null and NEW.vanLastReportOn is null)) THEN
       select CONCAT(fieldNameList,'partnerpublic.vanLastReportOn~') into fieldNameList;
       IF(OLD.vanLastReportOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.vanLastReportOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.vanLastReportOn is not null) THEN
        	select CONCAT(oldValueList,OLD.vanLastReportOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.vanLastReportOn is not null) THEN
        	select CONCAT(newValueList,NEW.vanLastReportOn,'~') into newValueList;
      	END IF;
   END IF;





  IF ((OLD.nextReportOn <> NEW.nextReportOn) or (OLD.nextReportOn is null and NEW.nextReportOn is not null)
      or (OLD.nextReportOn is not null and NEW.nextReportOn is null)) THEN
       select CONCAT(fieldNameList,'partnerpublic.nextReportOn~') into fieldNameList;
       IF(OLD.nextReportOn is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.nextReportOn is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.nextReportOn is not null) THEN
        	select CONCAT(oldValueList,OLD.nextReportOn,'~') into oldValueList;
     	END IF;
      	IF(NEW.nextReportOn is not null) THEN
        	select CONCAT(newValueList,NEW.nextReportOn,'~') into newValueList;
      	END IF;
   END IF;



   

  IF (OLD.isAccount <> NEW.isAccount ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isAccount~') into fieldNameList;
       IF(OLD.isAccount = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isAccount = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isAccount = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isAccount = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;





  IF (OLD.isAgent <> NEW.isAgent ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isAgent~') into fieldNameList;
       IF(OLD.isAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;







  IF (OLD.isCarrier <> NEW.isCarrier ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isCarrier~') into fieldNameList;
       IF(OLD.isCarrier = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isCarrier = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isCarrier = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isCarrier = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.isVendor <> NEW.isVendor ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isVendor~') into fieldNameList;
       IF(OLD.isVendor = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isVendor = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isVendor = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isVendor = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;


  IF (OLD.doNotSendEmailtoAgentUser <> NEW.doNotSendEmailtoAgentUser ) THEN
       select CONCAT(fieldNameList,'partnerpublic.doNotSendEmailtoAgentUser~') into fieldNameList;
       IF(OLD.doNotSendEmailtoAgentUser = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.doNotSendEmailtoAgentUser = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.doNotSendEmailtoAgentUser = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.doNotSendEmailtoAgentUser = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;



  IF (OLD.isPrivateParty <> NEW.isPrivateParty ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isPrivateParty~') into fieldNameList;
       IF(OLD.isPrivateParty = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isPrivateParty = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isPrivateParty = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isPrivateParty = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;







  IF (OLD.air <> NEW.air ) THEN
       select CONCAT(fieldNameList,'partnerpublic.air~') into fieldNameList;
       IF(OLD.air = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.air = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.air = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.air = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.sea <> NEW.sea ) THEN
       select CONCAT(fieldNameList,'partnerpublic.sea~') into fieldNameList;
       IF(OLD.sea = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.sea = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.sea = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.sea = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.surface <> NEW.surface ) THEN
       select CONCAT(fieldNameList,'partnerpublic.surface~') into fieldNameList;
       IF(OLD.surface = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.surface = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.surface = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.surface = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.isOwnerOp <> NEW.isOwnerOp ) THEN
       select CONCAT(fieldNameList,'partnerpublic.isOwnerOp~') into fieldNameList;
       IF(OLD.isOwnerOp = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isOwnerOp = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isOwnerOp = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isOwnerOp = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.doNotMerge <> NEW.doNotMerge ) THEN
       select CONCAT(fieldNameList,'partnerpublic.doNotMerge~') into fieldNameList;
       IF(OLD.doNotMerge = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.doNotMerge = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.doNotMerge = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.doNotMerge = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.viewChild <> NEW.viewChild ) THEN
       select CONCAT(fieldNameList,'partnerpublic.viewChild~') into fieldNameList;
       IF(OLD.viewChild = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.viewChild = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.viewChild = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.viewChild = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;






  IF (OLD.partnerPortalActive <> NEW.partnerPortalActive ) THEN
       select CONCAT(fieldNameList,'partnerpublic.partnerPortalActive~') into fieldNameList;
       IF(OLD.partnerPortalActive = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.partnerPortalActive = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.partnerPortalActive = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.partnerPortalActive = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

  IF (OLD.ugwwNetworkGroup <> NEW.ugwwNetworkGroup ) THEN
       select CONCAT(fieldNameList,'partnerpublic.ugwwNetworkGroup~') into fieldNameList;
       IF(OLD.ugwwNetworkGroup = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.ugwwNetworkGroup = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.ugwwNetworkGroup = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.ugwwNetworkGroup = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;

    IF ((OLD.latitude <> NEW.latitude) or (OLD.latitude is null and NEW.latitude is not null)
      or (OLD.latitude is not null and NEW.latitude is null)) THEN
        select CONCAT(fieldNameList,'partnerpublic.latitude~') into fieldNameList;
        IF(OLD.latitude is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.latitude is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.latitude is not null) then
            select CONCAT(oldValueList,OLD.latitude,'~') into oldValueList;
        END IF;
        IF(NEW.latitude is not null) then
            select CONCAT(newValueList,NEW.latitude,'~') into newValueList;
        END IF;
   END IF;     
    


    


    IF ((OLD.longitude <> NEW.longitude) or (OLD.longitude is null and NEW.longitude is not null)
      or (OLD.longitude is not null and NEW.longitude is null)) THEN
        select CONCAT(fieldNameList,'partnerpublic.longitude~') into fieldNameList;
        IF(OLD.longitude is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.longitude is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.longitude is not null) then
            select CONCAT(oldValueList,OLD.longitude,'~') into oldValueList;
        END IF;
        IF(NEW.longitude is not null) then
            select CONCAT(newValueList,NEW.longitude,'~') into newValueList;
        END IF;
   END IF;     

   IF (OLD.vendorCommission <> NEW.vendorCommission ) THEN
      select CONCAT(fieldNameList,'partnerprivate.vendorCommission~') into fieldNameList;
      select CONCAT(oldValueList,OLD.vendorCommission,'~') into oldValueList;
      select CONCAT(newValueList,NEW.vendorCommission,'~') into newValueList;
  END IF;
  
  IF (OLD.networkGroup <> NEW.networkGroup ) THEN
       select CONCAT(fieldNameList,'partnerprivate.networkGroup~') into fieldNameList;
       IF(OLD.networkGroup = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.networkGroup = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.networkGroup = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.networkGroup = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
     IF (OLD.isPartnerExtract <> NEW.isPartnerExtract ) THEN
       select CONCAT(fieldNameList,'partnerprivate.isPartnerExtract~') into fieldNameList;
       IF(OLD.isPartnerExtract = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isPartnerExtract = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isPartnerExtract = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isPartnerExtract = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
   
   if(NEW.isagent is true and OLD.lastname<>NEW.lastname) then
   		update partnerprivate set lastname=NEW.lastname,updatedby=NEW.updatedby,updatedon=now() where partnercode=NEW.partnercode;
   end if;

 CALL add_tblHistory (OLD.id,"partnerpublic", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$
