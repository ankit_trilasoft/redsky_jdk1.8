<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="fn1" uri="http://java.sun.com/jsp/jstl/functions" %>

<style type="text/css">
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; margin-top:-18px; padding:2px 5px; text-align:right;
float:right; width:47%;
}
.handcursor{ cursor:hand; cursor:pointer; font-color:black;
}
</style>

  <script type="text/javascript">
window.onload=function(){
	showOrHideAutoSave(0);
	var elem = document.getElementById("para1");
	  
	  if (screen.width > 1600 ){
				 elem.style.overflow = 'none';
				 elem.style.width = '100%';
		 }else if (screen.width == 1440 ){
				 elem.style.overflow = 'auto';
				 elem.style.width = '1320px';
		 }else if (screen.width >= 1360 ){
				 elem.style.overflow = 'auto';
				 elem.style.width = '1250px';
		 }else if(screen.width == 1280) {
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '1160px';
        }else if(screen.width == 1152) {
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '1035px';
        }else if(screen.width == 1024){
	    	  elem.style.overflow = 'auto';
	    	  elem.style.width = '910px';
        }
        else {      	
      	  elem.style.overflow = 'auto';
      	  elem.style.width = '910px';
      }
	
}
  </script> 
<SCRIPT LANGUAGE="JavaScript">
function addEvent( obj, type, fn )
{
	if (obj.addEventListener)
		obj.addEventListener( type, fn, false );
	else if (obj.attachEvent)
	{
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
} 

function removeEvent( obj, type, fn )
{
	if (obj.removeEventListener)
		obj.removeEventListener( type, fn, false );
	else if (obj.detachEvent)
	{
		obj.detachEvent( "on"+type, obj[type+fn] );
		obj[type+fn] = null;
		obj["e"+type+fn] = null;
	}
}

/* function loadNetworkList(){
	<c:if test ="${empty networkFileList  }">
		showOrHideAutoSave(1);
		location.href = "toDos.html?fromUser=${pageContext.request.remoteUser}&chkNetworkList=YES";
	</c:if>
} */


/*
Create the new window
*/
function openInNewWindow() {
	 //params  = 'width='+screen.width;
 //params += ', height='+screen.height;
 //params += ', top=0, left=0'
 //params += ', fullscreen=yes';

// newwin=window.open(this.getAttribute('goToUrl'),'windowname4', params);
 //if (window.focus) {newwin.focus()}
 //return false;
	// Change "_blank" to something like "newWindow" to load all links in the same new window
    var newWindow = window.open(this.getAttribute('goToUrl'), '_blank');
    newWindow.focus();
    return false;
}

/*
Add the openInNewWindow function to the onclick event of links with a class name of "new-window"
*/
function getNewWindowLinks() {
	// Check that the browser is DOM compliant
	if (document.getElementById && document.createElement && document.appendChild) {
		//Change this to the text you want to use to alert the user that a new window will be opened
		var strNewWindowAlert = "";
		// Find all links
		var links = document.getElementsByTagName('a');
		var objWarningText;
		var strWarningText;
		var link;
		for (var i = 0; i < links.length; i++) {
			link = links[i];
			// Find all links with a class name of "non-html"
			if (/\bnon\-html\b/.exec(link.className)) {
				link.onclick = openInNewWindow;
			}
		}
		objWarningText = null;
	}
}

addEvent(window, 'load', getNewWindowLinks);
  </SCRIPT>
  
<script type="text/javascript">
function toDoSupervisor(){
	document.forms['toDo'].action="toDoSupervisorList.html?for=supervisor";
	document.forms['toDo'].submit();
}

function toDoOwn(){
	document.forms['toDo'].action="toDos.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].submit();
}
function toDoTeam(){

	document.forms['toDo'].action="toDosTeam.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].submit();
}
function getOwnerResult()
{
	//document.forms['toDo'].action="ownerResult.html?fromUser=${pageContext.request.remoteUser}";
	var test="ownerResult.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].setAttribute("action",test);
	document.forms['toDo'].submit();
	//var myform = document.getElementById("toDo");
	///alert(myform)
	///var test="ownerResult.html?fromUser=${pageContext.request.remoteUser}";
	//myform.setAttribute("action",test);
	//myform.submit();
}

function getShiperResult()
{
	document.forms['toDo'].action="ownerResult.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].submit();
}

function getMessageResult(){
	document.forms['toDo'].action="messageResult.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].submit();
}

function getIdResult()
{
	document.forms['toDo'].action="idResult.html?fromUser=${pageContext.request.remoteUser}";
	document.forms['toDo'].submit();
}

function updateCheckUserName(target, target1,gotourl,field,field1,id,sid,isAccLine,ruleNumber){	
		var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
		str="1";
		</configByCorp:fieldVisibility>
	    
	   	var recordId= target;
	   	var fieldToValidate=target1;
	   	var newWindow="";
	   	var url="updateUserCheck.html?ajax=1&decorator=simple&popup=true&recordId="+encodeURI(recordId)+"&fieldToValidate=" + encodeURI(fieldToValidate);
	   	http2.open("GET", url, true);	
		http2.onreadystatechange = httpGoToURL;
		if(isAccLine == 'N') {
			if((str=="1") && (gotourl=='editServicePartner' || gotourl=='editVehicle')){
				gotourl = 'containersAjaxList';
				if(field!='' && field.indexOf("brokerDetails")!= -1){
					
				}else{
				field = field+""+id;
				}
				newWindow = window.open(gotourl+'.html?&id='+sid+'&from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber, '_blank');
				newWindow.focus();
			}else{
		        newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&id='+id+'&ruleNumber='+ruleNumber+'&sid='+sid, '_blank');
				newWindow.focus();
			}
		}else{
				newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber+'&sid='+sid, '_blank');
				newWindow.focus();
		}	
	    http2.send(null);
   	}
function updateCheckUserName1(target, target1,gotourl,field,field1,id,sid,isAccLine,ruleNumber,supportingId)
{
	var str="0";
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
	str="1";
	</configByCorp:fieldVisibility>
   	var recordId= target;
   	var fieldToValidate=target1;
   	var url="updateUserCheck.html?ajax=1&decorator=simple&popup=true&recordId="+encodeURI(recordId)+"&fieldToValidate=" + encodeURI(fieldToValidate);
   	http2.open("GET", url, true);	
	http2.onreadystatechange = httpGoToURL;
	if((str=="1") && (gotourl=='editServicePartner' || gotourl=='editVehicle')){
		gotourl = 'containersAjaxList';
		if(field!='' && field.indexOf("brokerDetails")!= -1){
			
		}else{
		field = field+""+id;
		}
		var newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber+'&id='+supportingId, '_blank');
		newWindow.focus();
	}else{
		var newWindow = window.open(gotourl+'.html?from=rule&field='+field+'&field1='+field1+'&ruleNumber='+ruleNumber+'&id='+id, '_blank');
		newWindow.focus();
	}
		
    http2.send(null);
	}
   	function httpGoToURL()
        {  
                            
           var results = http2.responseText;
           results = results.trim();
        }
   	
function openClientEmail(email,shipNumber,lastName,firstName){
	var subject ="SO# "+shipNumber+" Name: "+firstName+" "+lastName;
	var mailto_link = 'mailto:'+encodeURIComponent(email)+'?subject='+encodeURIComponent(subject)+'&body=';		
	window.location.href = mailto_link;
}
   	
function openGroupByRules(){
	<c:if test="${flagexternalCordinator=='true'}">
	var allradio=document.forms['toDo'].elements['allradio'].value;
   </c:if>
   <c:if test="${flagexternalCordinator=='true' && isSupervisor=='true' }">
   var allradio=document.forms['toDo'].elements['allradio'].value;
	</c:if>
	<c:if test="${flagexternalCordinator!='true'}">
	 var allradio="";
	</c:if>
window.open("openGroupByRules.html?allradio="+allradio+"&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=800, height=480,top=0, left=0,menubar=no")
}
function openGroupByPerson(){
	<c:if test="${flagexternalCordinator=='true'}">
	var allradio=document.forms['toDo'].elements['allradio'].value;
   </c:if>
   <c:if test="${flagexternalCordinator=='true' && isSupervisor=='true' }">
   var allradio=document.forms['toDo'].elements['allradio'].value;
	</c:if>
	<c:if test="${flagexternalCordinator!='true'}">
	 var allradio="";
	</c:if>
window.open("openGroupByPerson.html?allradio="+allradio+"&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=800, height=480,top=0, left=0,menubar=no")
}
function openGroupByShipper(){
	<c:if test="${flagexternalCordinator=='true'}">
	var allradio=document.forms['toDo'].elements['allradio'].value;
   </c:if>
   <c:if test="${flagexternalCordinator=='true' && isSupervisor=='true' }">
   var allradio=document.forms['toDo'].elements['allradio'].value;
	</c:if>
	<c:if test="${flagexternalCordinator!='true'}">
	 var allradio="";
	</c:if>
window.open("openGroupByShipper.html?allradio="+allradio+"&decorator=popup&popup=true","forms","scrollbars=1,resizable=yes, status=1,width=800, height=480,top=0, left=0,menubar=no")
}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}
function getHTTPObject1() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP.4.0");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP.4.0");
        }
    }
    return xmlhttp;
}

function getHTTPObject13()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP.4.0");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP.4.0");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http10 = getHTTPObject1();
    var http12 = getHTTPObject1();
    
    var http13 = getHTTPObject13();


    function showFilePage(historyId,xtranId,tableName,alertFileNumber,detailsId,sid)  {
    	var str="0";
		<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">
		str="1";
		</configByCorp:fieldVisibility>
    	     var noteFor="";
    	     var fileNameFor="";
    	     var xtranId=xtranId;
    	     var fileNumber=alertFileNumber.trim();
        	 var fileId=detailsId.trim();
		   if(tableName.trim()=='customerfile')				               {			            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');			               }
           if(tableName.trim()=='trackingstatus')			               {				            	    window.open('editTrackingStatus.html?id='+xtranId, '_blank');				               }
           if(tableName.trim()=='serviceorder')				               {				            	    window.open('editServiceOrderUpdate.html?id='+xtranId, '_blank');				               }
           if(tableName.trim()=='miscellaneous')				               {				            	    window.open('editServiceOrderUpdate.html?id='+xtranId, '_blank');				               }
           //if(tableName.trim()=='accountline')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
           if(tableName.trim()=='workticket')				               {				            	    window.open('editWorkTicketUpdate.html?id='+xtranId, '_blank');				               }
//           if(tableName.trim()=='service')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
           if(tableName.trim()=='billing')				               {				            	    window.open('editBilling.html?id='+xtranId, '_blank');				               }
//	               if(tableName.trim()=='carton')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//		               if(tableName.trim()=='refMaster')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//			               if(tableName.trim()=='servicePartner')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
//           if(tableName.trim()=='notes')				               {				            	    window.open('editCustomerFile.html?id='+xtranId, '_blank');				               }
           if(tableName.trim()=='container'){
        	   if((str=="1")){
        		   window.open('containersAjaxList.html?id='+sid, '_blank');
        	   }else{
        	   		window.open('containers.html?id='+xtranId, '_blank');
        	   }
           }
           if(tableName.trim()=='servicepartner'){
        	   if((str=="1")){
        		   window.open('containersAjaxList.html?id='+sid, '_blank');
        	   }else{
        	   	window.open('editServicePartner.html?id='+xtranId, '_blank');
        	   }
           }
           if(tableName.trim()=='accountline')	     				{	          	    			window.open('editAccountLine.html?id='+xtranId+'&toDoAlert=YES', '_blank');				               }
           if(tableName.trim()=='myfile'){
        	 //fileId=detailsId;
    		 if(fileNumber.length==11){
    			 fileNameFor="CF";
        		 noteFor="CustomerFile";
    		 }else if(fileNumber.length==13){
    			 fileNameFor="SO";
        		 noteFor="ServiceOrder";
    		 }else if(fileNumber.length==6){
    			 fileNameFor="SO";
        		 noteFor="WorkTicket";
    		 }
    		 window.open('editFileUpload.html?fid='+xtranId+'&id='+fileId+'&myFileFor='+fileNameFor+'&noteFor='+noteFor);
         }
         
         
     	  var url="setIsViewFlag.html?ajax=1&decorator=simple&popup=true&historyId="+historyId;
		http10.open("GET", url, true); 
        http10.onreadystatechange = function(){ handleHttpResponse2222(xtranId,tableName);};
        http10.send(null); 
	}
     
  
function handleHttpResponse2222(xtran,tableName){
			if (http10.readyState == 4){
        		var results = http10.responseText
        		   results = results.trim(); 
        		 	if(results.length>1){
        		 	}
        		 	 window.location.reload();             
			}
}
function gotoRespectiveFile(resultId, url){
	
	var recordId= resultId;
   	var urlCheck="updateUserCheckForCheck.html?ajax=1&decorator=simple&popup=true&recordId="+encodeURI(recordId);
   	http13.open("GET", urlCheck, true);	
   	http13.onreadystatechange = httpGoToCheckListURL;
	
        var documentView = '${user.fileCabinetView}'
        if(url == 'QuotationFile'){
            if(documentView == 'Document Centre'){
            	var newWindow = window.open('myFilesDocType.html?id='+resultId+'&myFileFor=CF&noteFor=agentQuotes&forQuotation=QC&active=true&secure=false', '_blank');
            }else{
            	var newWindow = window.open('myFiles.html?id='+resultId+'&myFileFor=CF&noteFor=agentQuotes&forQuotation=QC&active=true&secure=false', '_blank')
            }
        }
        else if(url == 'CustomerFile'){
        	if(documentView == 'Document Centre'){
            	var newWindow = window.open('myFilesDocType.html?id='+resultId+'&myFileFor=CF&noteFor=CustomerFile&active=true&secure=false', '_blank');
            }else{
            	var newWindow = window.open('myFiles.html?id='+resultId+'&myFileFor=CF&noteFor=CustomerFile&active=true&secure=false', '_blank')
            }
        }        
        else{
        	if(documentView == 'Document Centre'){
            	var newWindow = window.open('myFilesDocType.html?id='+resultId+'&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false', '_blank');
            }else{
            	var newWindow = window.open('myFiles.html?id='+resultId+'&myFileFor=SO&noteFor=ServiceOrder&active=true&secure=false', '_blank')
            }
        }
        http13.send(null);
		newWindow.focus();
}
function httpGoToCheckListURL()
{  
                    
   var results = http13.responseText;
   results = results.trim();
}
</script>
    <script language="javascript" type="text/javascript">
    function makeAsMarked(target){
      var targetId=target.id;
	     if(target.checked==true){
    		var newIds=document.forms['toDo'].elements['newIdList'].value;
    			if(newIds==''){
    				newIds=targetId;
    			}else{
					    newIds=newIds + ',' + targetId;
    			}
    		document.forms['toDo'].elements['newIdList'].value=newIds;
    	  }	else if(target.checked==false){
    			var newIds=document.forms['toDo'].elements['newIdList'].value;
    			if(newIds.indexOf(targetId)>-1){
    				newIds=newIds.replace(targetId,"");
    				newIds=newIds.replace(",,",",");
    				var len=newIds.length-1;
				    if(len==newIds.lastIndexOf(",")){
    					newIds=newIds.substring(0,len);
    				}
    				if(newIds.indexOf(",")==0){
    					newIds=newIds.substring(1,newIds.length);
	    
    				}
    				document.forms['toDo'].elements['newIdList'].value=newIds;
    			}
    		}
    }
    function makeAsViewed()  {
		var newIds=document.forms['toDo'].elements['newIdList'].value;
  	  var url="makeAsViewed.html?ajax=1&decorator=simple&popup=true&newIdList="+newIds;
		http12.open("GET", url, true); 
     http12.onreadystatechange = handleHttpResponse1111;
     http12.send(null); 
	}
function handleHttpResponse1111(){
			if (http12.readyState == 4){
     		var results = http12.responseText
     		   results = results.trim(); 
     		 	if(results.length>1){
     		 	}
     		 	 window.location.reload();             
			}
}
    

function removeTick()
{
	var chkData = document.forms['toDo'].elements['check_box'];
	for(var i=0; i<=chkData.length ; i++){
		chkData[i].checked=false;
		}
}

function updateNetworkControl(childAgentCode,shipnumber){
	var url = "updateNetWorkControl.html?childAgentCode="+childAgentCode+"&shipnumber="+shipnumber ;
	document.forms['toDo'].setAttribute("action",url);
	document.forms['toDo'].submit();
}

function actionResponse(obj,id,bookingAgentName,bookingAgentCode,type,childType,childAgentCode,shipNumber,compDiv){
	if(obj.value=='Accept'){
		var agree= confirm("Are you sure you want to Accept "+shipNumber);
		if(agree){
			 document.getElementById("overlayDetails").style.display = "block";
			var linkNet="linkNetworkFile.html?soId="+id+"&agentType="+type+"&childAgentType="+childType+"&childAgentCode="+childAgentCode+"&agentCompDiv="+compDiv;
			document.forms['toDo'].setAttribute("action",linkNet);
			//document.forms['toDo'].action="linkNetworkFile.html?soId="+id+"&agentType="+type+"&childAgentType="+childType+"&childAgentCode="+childAgentCode+"&agentCompDiv="+compDiv;
			document.forms['toDo'].submit();
		}else{
			obj.value='';
		}
	}
	if(obj.value=='Merge'){
		document.getElementById("overlayDetails").style.display = "block";
		javascript:openWindow("linkServiceOrdersForm.html?decorator=popup&popup=true&bookingAgentName="+bookingAgentName+"&bookingAgentcode="+bookingAgentCode+"&soId="+id+"&agentType="+type+"&childAgentType="+childType+"&childAgentCode="+childAgentCode+"&agentCompDiv="+compDiv,"height=200,width=500");
	}
	if(obj.value=='Decline'){
		var agree= confirm("Are you sure you want to Decline "+shipNumber);
		if(agree){
			// document.getElementById("overlay").style.display = "block";
			//document.forms['toDo'].action="declineNetworkFile.html?soId="+id+"&childAgentType="+childType;
			document.getElementById("overlayDetails").style.display = "block";
			var declineNet="declineNetworkFile.html?soId="+id+"&childAgentType="+childType;
			document.forms['toDo'].setAttribute("action",declineNet);
			document.forms['toDo'].submit();
		}else{
			obj.value='';
		}
	}
}
function closeBoxDetails(){
	 document.getElementById("overlayDetails").style.display = "none";
}
function checkForRoleTransfer(obj,shipNumber,id,urlName,toDoRecordId,roleType){
	var asigneeName = obj.value;
	var url = "makeAsViewed.html?ajax=1&decorator=simple&popup=true&";
	alert(asigneeName);
	
	//updateRoleTransfer(obj,shipNumber,id,urlName,toDoRecordId,roleType)
}
function updateRoleTransfer(obj,shipNumber,id,urlName,toDoRecordId,roleType){
		var agree= confirm("Please Confirm: This task will be transferred to this "+obj.value);
		if(agree){
			document.forms['toDo'].action="updateRoleByTransfer.html?roleForTransfer="+obj.value+"&toDoResultId="+id+"&toDoResultUrl="+urlName+"&toDoRecordId="+toDoRecordId+"&roleType="+roleType+"&shipnumber="+shipNumber ;
			document.forms['toDo'].submit();
		}else{
			obj.value='';
		}
}

function findToolTipNoteForNotes(id,position){		
	var url="findToolTipActivityNote.html?ajax=1&decorator=simple&popup=true&id="+id;
	ajax_showTooltip(url,position);	
	return false;
	}
	var t;
	function getValue(t)
	{
		
	if(t=='Own')
		{
		document.forms['toDo'].elements['ownerName'].value="";
		document.forms['toDo'].elements['ruleId'].value="";
		document.forms['toDo'].elements['messageDisplayed'].value="";
		toDoOwn();
		
		}
	if(t=='Direct Report')
		{
		document.forms['toDo'].elements['ownerName'].value="";
		document.forms['toDo'].elements['ruleId'].value="";
		document.forms['toDo'].elements['messageDisplayed'].value="";
		
		toDoSupervisor();
		
		}
	if(t=='Team')
	{
		document.forms['toDo'].elements['ownerName'].value="";
		document.forms['toDo'].elements['ruleId'].value="";
		document.forms['toDo'].elements['messageDisplayed'].value="";
		toDoTeam();
	
	}
if(t=='All')
	{
	document.forms['toDo'].elements['ownerName'].value="";
	document.forms['toDo'].elements['ruleId'].value="";
	document.forms['toDo'].elements['messageDisplayed'].value="";
	toDoTeam();
	
	}
		
	}
   </script>

 <style>
div.red :hover{ color:red}
#overlayDetails{
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:90; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
.table th.containeralign a{text-align:left !important;}
</style>
<head>
<meta name="save" content="history" />
<meta name="heading" content="<fmt:message key='toDo.heading'/>"/> 
<title><fmt:message key="toDo.title"/></title> 
</head>

<s:hidden name="fileID"  id= "fileID" value=""/> 
<c:set var="fileID"  value=""/>  
<s:form id="toDo" name= "toDo" action="" method="post" validate="true">
<s:hidden name="shipperName" value="" />
<s:hidden name="newIdList" value=""/>
<s:hidden name="flagexternalCordinator" value="${flagexternalCordinator}"/>
<div id="overlayDetails" style="display:none">
</div>
<div id="Layer1" align="center" style="width:100%;margin: 0px;padding: 0px;">
  
	<table border="0" cellspacing="0" cellpadding="0" class="" style="margin: 0px;padding: 0px; margin-bottom: -1px;">
	<tr><td>
	<div id="otabs" style="margin-bottom:-5px;!margin-bottom:7px; ">
	  <ul>
	    <li><a class="current"><span>Activity Management</span></a></li>			    
	  </ul>			  
   	</div>
   </td>
     <c:set var="for" value="<%=request.getParameter("for") %>"/>
     
	<%--
          if(pageContext.getAttribute("isSupervisor")!=null) {
      %>
      <c:if test="${isSupervisor=='true'}">
    	<c:if test="${for!='supervisor'}">
	    	<td align="right"  width="20px"  valign="top"><input name="button1" type="radio" value="true" checked onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b><fmt:message key="todo.own"/></b></td>
        </c:if>
        <c:if test="${for=='supervisor'}">
	    	<td align="right"  width="20px"  valign="top"><input name="button1" type="radio" value="true"  onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b><fmt:message key="todo.own"/></b></td>
        </c:if>
       
        
	        <c:if test="${for=='supervisor'}">
		        <td align="right"  width="20px"  valign="top"><input name="button1" type="radio" value="true" checked onclick="toDoSupervisor();" /></td> 
		        <td align="left" class="listwhitetext"  width="100px"><b><fmt:message key="todo.dreport"/></b></td> 
	        </c:if>  
	        <c:if test="${for!='supervisor'}">
		        <td align="right" width="20px" valign="top"><input name="button1" type="radio" value="true" onclick="toDoSupervisor();" /></td> 
		        <td align="left" class="listwhitetext" width="100px"><b><fmt:message key="todo.dreport"/></b></td> 
	        </c:if>
        </c:if> 
        <% }  --%>
        <c:if test="${flagexternalCordinator=='true'}">
         <td class="listwhitetext"><s:radio name="allradio" list="{'Own'}" onclick="getValue(this.value);" tabindex="4"/></td>

	    </c:if>
	    <c:if test="${flagexternalCordinator=='true' && isSupervisor=='true' }">
	                 <td class="listwhitetext"><s:radio name="allradio" list="{'Direct Report'}" onclick="getValue(this.value);" tabindex="4"/></td>

		</c:if>
		<c:if test="${flagexternalCordinator=='true'}">
		 <td class="listwhitetext"><s:radio name="allradio" list="{'Team'}" onclick="getValue(this.value);" tabindex="4"/></td>

	     <td class="listwhitetext"><s:radio name="allradio" list="{'All'}" onclick="getValue(this.value);" tabindex="4"/></td>

	      </c:if>
       
        <c:if test="${flagexternalCordinator!='true'}">
        <c:if test="${isSupervisor=='true'}">
      <%
          if(request.getParameter("for") != null && request.getParameter("for").equals("supervisor") ) {
        	 /*  System.out.println("\n\n*********************in for = supervisor************************"+request.getParameter("for")); */
      %>
    		<td align="right"  width="20px"  valign="top"><input name="button1" type="radio"  value="true" onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b><fmt:message key="todo.own"/></b></td>
	        <td align="right"  width="20px"  valign="top"><input name="button1" type="radio" value="true" checked onclick="toDoSupervisor();" /></td> 
		    <td align="left" class="listwhitetext"  width="80px"><b><fmt:message key="todo.dreport"/></b></td>
		    <c:if test="${flagexternalCordinator=='true'}">
		    <td align="right"  width="20px"  valign="top"><input name="button1" type="radio"  value="true" onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b>Team</b></td>
	        <td align="right"  width="20px"  valign="top"><input name="button1" type="radio"  value="true" onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b>All</b></td> 
	        </c:if>
        <%}else{
        		/* System.out.println("\n\n*********************in for != supervisor************************"+request.getParameter("for")); */
        %>
       			<td align="right"  width="20px"  valign="top"><input name="button1" type="radio" value="true" checked onclick="toDoOwn();"/></td> 
	        	<td align="left" class="listwhitetext" width="30px"><b><fmt:message key="todo.own"/></b></td>
		        <td align="right" width="20px" valign="top"><input name="button1" type="radio" value="true" onclick="toDoSupervisor();" /></td> 
		        <td align="left" class="listwhitetext" width="80px"><b><fmt:message key="todo.dreport"/></b></td> 
		        <c:if test="${flagexternalCordinator=='true'}">
		        <td align="right"  width="20px"  valign="top"><input name="button1" type="radio"  value="true" onclick="toDoOwn();"/></td> 
		        <td align="left" class="listwhitetext" width="30px"><b>Team</b></td>
	        <td align="right"  width="20px"  valign="top"><input name="button1" type="radio"  value="true" onclick="toDoOwn();"/></td> 
	        <td align="left" class="listwhitetext" width="30px"><b>All</b></td> 
	        </c:if>
	    <%} %>
        </c:if> 
        </c:if>
        
        <td width="20px"></td>
        <td style="margin:0px; " class="listwhitetextAcct" >
        <font style="padding-left:10px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: 600; ">Filters:</font> Message: &nbsp;&nbsp;<s:select cssClass="list-menu" id="entity" name="messageDisplayed" headerKey="" headerValue="" list="%{messageList}" cssStyle="width:100px" onchange="getOwnerResult()"/>&nbsp;&nbsp;&nbsp;&nbsp; Person:
  		<s:select cssClass="list-menu" id="entity" name="ownerName" headerKey="" headerValue="" list="%{userList}"  cssStyle="width:100px" onchange="getOwnerResult()"/>Rule Id:
  		<s:select cssClass="list-menu" id="entity" name="ruleId" headerKey="" headerValue="" list="%{ruleIdList}"  onchange="getOwnerResult()"/>
  		<input type="button" class="cssbuttonA" style="width:100px; height:23px" align="bottom" value="Remove Filters"  onclick="location.href='<c:url value="/toDos.html?fromUser=${pageContext.request.remoteUser}"/>'"/>
			</td>
			</tr>
			</table>
	<div class="spn">&nbsp;</div>
  <div id="content" align="center" style="width:100%">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
   <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
  <tbody>
  <tr align="center">
  	<td align="center">
  	<c:if test="${user.userType!='AGENT' }">
  		<div style="background:#BBD9FE;padding:3px;width:300px; " >
  				<b>
  				<c:if test="${chkRulesRunning=='Y'}">
  					To Do Rule Execution in Progress
  				</c:if>
  				<c:if test="${chkRulesRunning!='Y'}">
  					Last Update : <fmt:formatDate type="both"   pattern="dd-MMM-yyy HH:mm:ss"  value="${companyToDoRuleExecutionUpdate.lastRunDate }" />
  				</c:if>
  				</b>
  		</div>
  	</c:if>
 	 </td>
  </tr>
  <c:if test="${user.userType!='AGENT' && user.userType!='PARTNER'}">
  <tr><td ><img src="${pageContext.request.contextPath}/images/follow-up.jpg" border="0" style="cursor:auto;"/></td></tr>
  <tr>
  <td> </td>
  </tr>
  <tr height="1px"></tr>
 <tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent6-title" class="handcursor">	
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Coming Up</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countDueWeek}&nbsp;</font>)
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_center">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
	</div> 
	<div id="bobcontent6" class="switchgroup1">
	<s:set name="notes2" value="notes2" scope="request"/> 
	<display:table name="notes2" class="table" requestURI="" id="notes2List"  defaultsort="6" defaultorder="ascending" export="" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
	<c:if test="${ notes2List.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes2List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" ><u>	<c:out value="${notes2List.subject}"></c:out>
		</u></font></div>
	</c:if>
	<c:if test="${notes2List.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes2List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column>   
	<c:if test="${notes2List != '[]'}"> 
	<c:if test="${(fn:indexOf(notes2List.note,'<table')>=0) }">
	<display:column  style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes">
	    <c:if test="${notes2List.note != ''}" >
		<c:if test="${fn:length(notes2List.note) > 50}" >
		<c:set var="abc" value="${fn:substring(notes2List.note, 0, 50)}" />
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notes2List.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(notes2List.note) <= 50}" >
		<c:set var="abc" value="${fn:substring(notes2List.note, 0, 50)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
	</display:column>
	</c:if>
	
	<c:if test="${!(fn:indexOf(notes2List.note,'<table')>=0 )}">
	<display:column property="note" maxLength="50" style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/> 
	</c:if>
	</c:if>
	
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/>
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column headerClass="containeralign" style="width:30px; text-align: right" property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" />
	</display:table>
	
</div>
</td>
</tr>     
		
 <tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent5-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
		&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>	
	</div>
	<div id="bobcontent5" class="switchgroup1">
	<s:set name="notes" value="notes" scope="request"/> 
	<display:table name="notes" class="table" requestURI="" id="notesList"  defaultsort="4" export="" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
			<c:if test="${ notesList.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notesList.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
			<font color ="#003366" ><u><c:out value="${notesList.subject}"></c:out></u></font>
		</a></div></c:if>
		<c:if test="${notesList.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notesList.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column> 
	<c:if test="${notesList != '[]'}"> 
	<c:if test="${(fn:indexOf(notesList.note,'<table')>=0) }">
	<display:column  sortable="true" paramId="id" paramProperty="id" titleKey="toDoReminder.notes">
	    <c:if test="${notesList.note != ''}" >
		<c:if test="${fn:length(notesList.note) > 50}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 50)}" />
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notesList.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(notesList.note) <= 50}" >
		<c:set var="abc" value="${fn:substring(notesList.note, 0, 50)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
	</display:column>
	</c:if>
	
	<c:if test="${!(fn:indexOf(notesList.note,'<table')>=0 )}">
	<display:column property="note" maxLength="50" sortable="true" paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/>
	</c:if> 
	</c:if> 
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/> 
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" headerClass="containeralign" style="width:30px; text-align: right"/>
	</display:table>
	
</div>
</td>
</tr> 

<tr>
	<td height="10" width="100%" align="left">	
	<div id="bobcontent4-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countOverDue}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent4-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countOverDue}&nbsp;</font>)</font>
	--></div>
	<div id="bobcontent4" class="switchgroup1">
	<s:set name="notes1" value="notes1" scope="request"/> 
	<display:table name="notes1" class="table" requestURI="" id="notes1List"  defaultsort="6" defaultorder="ascending" export="false" pagesize="10"> 
	<display:column sortable="true" maxLength="50" titleKey="toDoReminder.subject" sortProperty="subject">
		<c:if test="${ notes1List.subject != ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes1List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" ><u>	<c:out value="${notes1List.subject}"></c:out></u></font>
		</a></div></c:if>
		<c:if test="${notes1List.subject == ''}">
		<div onclick="javascript:openWindow('notesControl.html?id=${notes1List.id}&activity=Yes&decorator=popup&popup=true',900,400);" class="red">
		<font color ="#003366" > - </font></div>
	</c:if>
	</display:column>
	<c:if test="${notes1List != '[]'}"> 
	<c:if test="${(fn:indexOf(notes1List.note,'<table')>=0) }">
	<display:column  style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes">
	    <c:if test="${notes1List.note != ''}" >
		<c:if test="${fn:length(notes1List.note) > 50}" >
		<c:set var="abc" value="${fn:substring(notes1List.note, 0, 50)}" />
	    <div align="left" onmouseover="findToolTipNoteForNotes('${notes1List.id}',this);" onmouseout="ajax_hideTooltip();" ><c:out value="${abc}" />&nbsp;.....</div>
		</c:if>
		<c:if test="${fn:length(notes1List.note) <= 50}" >
		<c:set var="abc" value="${fn:substring(notes1List.note, 0, 50)}" />
	    <div align="left" ><c:out value="${abc}" /></div>
		</c:if>
		</c:if>
	</display:column>
	</c:if>
	
	<c:if test="${!(fn:indexOf(notes1List.note,'<table')>=0 )}">   
	<display:column property="note" maxLength="50" style="text-align:justify" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.notes"/> 
	</c:if>
	</c:if>
	<display:column property="noteType" sortable="true"  paramId="id" paramProperty="id" titleKey="notes.noteType" style="width:100px"/> 	
	<display:column property="name" sortable="true"  paramId="id" paramProperty="id" title="Name" style="width:100px"/> 
	<display:column property="followUpFor" sortable="true"  paramId="id" paramProperty="id" title="Follow Up For" style="width:100px"/> 
	<display:column property="forwardDate" sortable="true"  paramId="id" paramProperty="id" titleKey="toDoReminder.date" style="width:80px" format="{0,date,dd-MMM-yyyy}"/>
	<display:column property="remindTime" sortable="true"  paramId="id" paramProperty="id" title="Time" headerClass="containeralign" style="width:30px; text-align: right"/>
	</display:table>
	
</div>
</td>
</tr>
<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/alert-List.jpg" border="0" style="cursor:auto;"/></td>
</tr>
</table>
</td>
</tr>


<tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent10-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Alert</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countAlertToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent2-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)</font>
	--></div>
	<div id="bobcontent10" class="switchgroup1">
	<s:set name="toDayAlertList" value="toDayAlertList" scope="request"/>
	<display:table name="toDayAlertList" class="table" requestURI="" id="toDayAlertList"  defaultsort="10" export="" pagesize="10" style="margin-bottom:2px;">

<display:column title="Remove">
<input type="checkbox" name="check_box" id="${toDayAlertList.id}" value="${toDayAlertList.id}" onclick="makeAsMarked(this);"/>
</display:column>
<display:column sortable="true" title="File Number" sortProperty="alertFileNumber">
	<a href="javascript:showFilePage('${toDayAlertList.id}','${toDayAlertList.xtranId}','${toDayAlertList.tableName}','${toDayAlertList.alertFileNumber}','${toDayAlertList.detailsId}','${toDayAlertList.soId}');" ><c:out value="${toDayAlertList.alertFileNumber}" /></a>
</display:column>
<display:column property="alertShipperName" sortable="true" title="Shipper Name"/>
<display:column property="description" sortable="true" title="Field Updated"/>
<display:column  sortable="true" title="Old Value">

    <c:if test="${fn1:contains(toDayAlertList.oldValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(toDayAlertList.oldValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(toDayAlertList.oldValue,'00:00')}">
       		<c:out value="${toDayAlertList.oldValue }"></c:out>
       </c:if>
       </display:column>
    <display:column  sortable="true" title="New Value">
    
       <c:if test="${fn1:contains(toDayAlertList.newValue,'00:00')}">
       		<c:out value= "${fn1:substringBefore(toDayAlertList.newValue,'00:00')}"></c:out>   
       </c:if>
       <c:if test="${!fn1:contains(toDayAlertList.newValue,'00:00')}">
       		<c:out value="${toDayAlertList.newValue }"></c:out>
       </c:if>
    </display:column>
    <display:column property="user" sortable="true" title="Modify By"/>
    <display:column title="Modify Date" sortable="true"   style="width:125px" sortProperty="dated">
        <fmt:formatDate value="${toDayAlertList.dated}" pattern="${displayDateTimeFormat}"/>
    </display:column>
    <display:column property="alertRole" sortable="true" title="Role"/>
    <display:column property="alertUser" sortable="true" title="Name"/>
    <display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
	 </display:table>
<input type="button" class="cssbutton1" style="width:120px; height:23px;margin-bottom:5px;" align="top"  name="showLine" id="showLine" value="Dismiss Alerts" onclick="makeAsViewed();"/>	
</div>
</td>
</tr>
<!-- Start of Enhancement of 2 step linking bug#6549 - 2Step_merging_and_linking -->
<%-- <sec-auth:authComponent componentId="module.script.form.networkFiles">
<c:if test="${networkLinkSize != 0}">  
<tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent11-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center" ">
	&nbsp;&nbsp;<font color="black">Network File</font> &nbsp;&nbsp;( <font color="red">&nbsp;${networkLinkSize}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="bobcontent11" class="switchgroup1">
	
	<div style="width:1170px;">
		<div id="otabs" style="margin-bottom:2px;!margin-bottom:7px;clear:both;">
			  <ul>
			    <li><a class="current"><span>Linked File List</span></a></li>			    
			  </ul>			  
 		</div>
 		<s:set name="networkFileList" value="networkFileList" scope="request"/>
 		
 		<display:table name="networkFileList" class="table" requestURI="" id="networkFileList"  defaultsort="14" export="" pagesize="10" style="margin-bottom:2px;font-size:1em;">
 		<display:column property="agent" sortable="true" title="Agent Name" style="width:5px" />
    <display:column sortable="true"  title ="Agent SO# / Contact" style="width:5px">
    	<c:out value="${networkFileList.shipNumber }" />/<br/><a href="#" style="font-size:8px;" onclick="openClientEmail('${networkFileList.email }','${networkFileList.shipNumber }','${networkFileList.lastName }','${networkFileList.firstName }')" ><c:out value="${networkFileList.email }" /></a>
    </display:column>
    <display:column property="lastName" sortable="true" title="Last/Company Name" style="width:5px"/>
    <display:column property="firstName" sortable="true" title="First Name" style="width:5px"/>
    <display:column sortable="true" title="Job Type" style="width:5px"> 
    	<c:out value="${networkFileList.jobType }" />,<c:out value="${networkFileList.shipmentType }" />
    </display:column>
    <display:column property="commodity" sortable="true" title="Cmdty" style="width:5px"/>
    <display:column property="mode" sortable="true" title="Mode" style="width:5px"/>
    <display:column property="origin" sortable="true" title="Origin" style="width:5px"/>
    <display:column property="originCity" sortable="true" title="Origin City" style="width:5px"/>
    <display:column property="destin" sortable="true" title="Destin" style="width:5px"/>
    <display:column property="destinCity" sortable="true" title="Destin City" style="width:5px"/>
     <display:column  sortable="true" title="Company Division/<br>Role" style="width:5px">${networkFileList.compDiv }/${networkFileList.childAgentType}</display:column>
    <display:column title="Action" style="width:5px" >
    	<c:if test="${networkFileList.action == null || networkFileList.action== '' || networkFileList.action=='Decline' }">
    		<s:select list="{'','Accept','Merge','Decline'}" name="action" onchange="actionResponse(this,'${networkFileList.id }','${networkFileList.agent }','${networkFileList.agentCode }','${networkFileList.type }','${networkFileList.childAgentType }','${networkFileList.childAgentCode }','${networkFileList.shipNumber}','${networkFileList.compDiv }');" cssClass="list-menu" cssStyle="width:65px;"/> 
   		</c:if>
   		<c:if test="${networkFileList.action != null && networkFileList.action != '' && networkFileList.action != 'Decline' }">
    		<input type ="Button" name ="OK" Value ="OK" onclick ="updateNetworkControl('${networkFileList.childAgentCode }','${networkFileList.shipNumber}')" />
   		</c:if>
    </display:column>
      <display:column title="# Days</br>Pending" style="width:5px" property="daysDiff" sortable="true" />
     </display:table>
     </div>
    
</div>

</td>
</tr>
 </c:if> 
 </sec-auth:authComponent> --%>
 <!--End of Enhancement of 2 step linking bug#6549 - 2Step_merging_and_linking -->
</c:if> 

<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/todo-List.jpg" border="1" style="cursor:auto;"/></td>
<td align="left" width="250px" ><input type="button"  class="groupButton" align="bottom" value="Group by Rules"  onclick="openGroupByRules();"/></td>
<td align="left" ><input type="button" class="groupButton"  align="bottom" value="Group by Person"  onclick="openGroupByPerson();"/></td>
<td align="left" ><input type="button" class="groupButton"  align="bottom" value="Group by Shipper"  onclick="openGroupByShipper();"/></td>
</tr>
</table>
</td>
</tr>
   <tr height="1px"></tr>	
  
  <tr><td></td></tr> 	
  
    <!--  
    <tr>           
         <td align="left" class="listwhitetext"><h4><b><font color="#1465B7" style="padding-left:5px;"><fmt:message key="todo.list"/></font></b></h4></td>  
	</tr>
	-->
 <%--<tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent3-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due This Week</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultDueWeek}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<font id="bobcontent3-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Due This Week</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultDueWeek}&nbsp;</font>)</font>
	</div>
	<div id="bobcontent3" class="switchgroup1">
	<s:set name="toDoResultsDueWeek" value="toDoResultsDueWeek"/>
	<display:table name="toDoResultsDueWeek" class="table" requestURI="" id="toDoResultRow1"  defaultsort="4" export="" pagesize="10"> 
	<display:column sortable="true" titleKey="toDoList.file">
	<a href="<c:out value="${toDoResultRow1.url}"/>.html?from=rule&field=${toDoResultRow1.fieldToValidate1}&id=${toDoResultRow1.resultRecordId}"><c:out value="${toDoResultRow1.fileNumber}" /></a>
	</display:column>
	<display:column property="messagedisplayed" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoRulemessage.message"/>
	<display:column property="fieldDisplay" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoRule.fieldtovalidate"/>
	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoRuleresult.durationAddSub"/>
	<display:column property="shipper" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" title="Role"/>
	<display:column property="owner" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" title="Name"/>
	</display:table>
	
</div>
</td>
</tr>     	
 <tr>
	<td height="10" width="100%" align="left" >
	<div id="bobcontent3-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Coming Up</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultComingUp}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="bobcontent3" class="switchgroup1">
	<s:set name="toDoResultsComingUp" value="toDoResultsComingUp"/>
	<display:table name="toDoResultsComingUp" class="table" requestURI="" id="toDoResultComingUpId"  defaultsort="4" export="" pagesize="10"> 	
	<c:if test="${toDoResultComingUpId.resultRecordType!='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" >
	<a style="width:100px" HREF="javascript:updateCheckUserName('${toDoResultComingUpId.resultRecordId}','${toDoResultComingUpId.fieldToValidate1}','${toDoResultComingUpId.url}','${toDoResultComingUpId.fieldToValidate1}','${toDoResultComingUpId.fieldToValidate2}','${toDoResultComingUpId.resultRecordId}','${toDoResultComingUpId.supportingId }','N');"><c:out value="${toDoResultComingUpId.fileNumber}" /></a>
	</display:column>
	</c:if>
	<c:if test="${toDoResultComingUpId.resultRecordType=='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file"  >
	<a HREF="javascript:updateCheckUserName('${toDoResultComingUpId.resultRecordId}','${toDoResultComingUpId.fieldToValidate1}','${toDoResultComingUpId.url}','${toDoResultComingUpId.fieldToValidate1}','${toDoResultComingUpId.fieldToValidate2}','${toDoResultComingUpId.resultRecordId}','${toDoResultComingUpId.supportingId }','Y');"><c:out value="${toDoResultComingUpId.fileNumber}" /></a>
	</display:column>
	</c:if>
	<display:column property="messagedisplayed" sortable="true" style="width:20%"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoRulemessage.message"/>
	<display:column property="fieldDisplay" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoRule.fieldtovalidate"/>
	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" title="OD"/>
	<display:column property="shipper" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" title="Role"/>
	<display:column property="owner" sortable="true"  paramId="resultRecordId" paramProperty="resultRecordId" title="Name"/>
	<display:column property="checkedUserName" sortable="true"   title="viewedBy"/>
	<display:column title="Note" sortable="true" paramId="resultRecordId" paramProperty="resultRecordId" style="text-align: center; width:10%;" maxLength="40">	
	<c:if test="${toDoResultComingUpId.isNotesAdded == false}">
		<c:if test="${toDoResultComingUpId.note!=''}">
		<a HREF="javascript:openNote('${toDoResultComingUpId.noteId}','${toDoResultComingUpId.noteType}','${toDoResultComingUpId.noteSubType}');"><c:out value="${toDoResultComingUpId.note}" /></a>
	    </c:if>
	    <c:if test="${toDoResultComingUpId.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultComingUpId.fileNumber}','${toDoResultComingUpId.todoRuleId}','${toDoResultComingUpId.id}','${toDoResultComingUpId.noteType}','${toDoResultComingUpId.noteSubType}');"/>
	   </c:if>
	</c:if>
	<c:if test="${toDoResultComingUpId.isNotesAdded== true}">
	<a HREF="javascript:openNote('${toDoResultComingUpId.noteId}','${toDoResultComingUpId.noteType}','${toDoResultComingUpId.noteSubType}');"><c:out value="${toDoResultComingUpId.note}" /></a>
	</c:if>
	</display:column>
	
	</display:table>
	
</div>
</td>
</tr> 
 --%>   
  <tr>
	<td height="20" width="100%" align="left" >
	<div id="bobcontent2-title" class="handcursor">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	<!--<font id="bobcontent2-title" class="handcursor">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultToday}&nbsp;</font>)</font>
	--></div>
	<div id="bobcontent2" class="switchgroup1">
	<s:set name="toDoResultsToday1" value="toDoResultsToday1" scope="request"/>
	<display:table name="toDoResultsToday1" class="table" requestURI="" id="toDoResultRow2"  defaultsort="9" export="" pagesize="10"> 
	<c:if test="${toDoResultRow2.resultRecordType!='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber">
	<a style="width:100px" HREF="javascript:updateCheckUserName('${toDoResultRow2.resultRecordId}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.url}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.fieldToValidate2}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.supportingId }','N','${toDoResultRow2.ruleNumber}');"><c:out value="${toDoResultRow2.fileNumber}" /></a>
	</display:column>
	</c:if>
	<c:if test="${toDoResultRow2.resultRecordType=='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber" >
	<a HREF="javascript:updateCheckUserName('${toDoResultRow2.resultRecordId}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.url}','${toDoResultRow2.fieldToValidate1}','${toDoResultRow2.fieldToValidate2}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.supportingId }','Y','${toDoResultRow2.ruleNumber}');"><c:out value="${toDoResultRow2.fileNumber}" /></a>
	</display:column>  
	</c:if>
	<display:column property="messagedisplayed" style="width:20%" sortable="true"   titleKey="toDoRulemessage.message"/>
	<display:column property="fieldDisplay" sortable="true"   titleKey="toDoRule.fieldtovalidate"/>
	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"  title="OD"/>
	<display:column property="shipper" sortable="true"   titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true"   title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	<c:if test="${flagForSupervisor=='false'}">
	 <display:column title="Note" headerClass="centeralign" sortable="true" style="text-align: center; width:10%;" maxLength="40">
	
	<c:if test="${toDoResultRow2.isNotesAdded == false}">
		<c:if test="${toDoResultRow2.note!=''}">
		<a HREF="javascript:openNote('${toDoResultRow2.noteId}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"><c:out value="${toDoResultRow2.note}" /></a>
	    </c:if>
	    <c:if test="${toDoResultRow2.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow2.fileNumber}','${toDoResultRow2.todoRuleId}','${toDoResultRow2.id}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"/>
	   </c:if>
	</c:if>
	<c:if test="${toDoResultRow2.isNotesAdded== true}">
	<c:if test="${toDoResultRow2.note!='' }">
	<a HREF="javascript:openNote('${toDoResultRow2.noteId}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"><c:out value="${toDoResultRow2.note}" /></a>
	</c:if>
	<c:if test="${toDoResultRow2.note=='' }">
	<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow2.fileNumber}','${toDoResultRow2.todoRuleId}','${toDoResultRow2.id}','${toDoResultRow2.noteType}','${toDoResultRow2.noteSubType}');"/>
	</c:if>
	</c:if>
	</display:column>
	</c:if>
	<%-- <sec-auth:authComponent componentId="module.script.form.roletransfer">
		<display:column title ="Role&nbsp;Transfer">
		<c:if test ="${toDoResultRow2.rolelist=='Coordinator'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer" list="%{coord}"
										cssStyle="width:145px" headerKey="" headerValue=""  
										onchange="updateRoleTransfer(this,'${toDoResultRow2.fileNumber}','${toDoResultRow2.id}','${toDoResultRow2.url}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.rolelist}')" />
									</c:if>	
									<c:if test ="${toDoResultRow2.rolelist=='Consultant'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer"  list="%{sale}"
										cssStyle="width:145px" headerKey="" headerValue="" value="${toDoResultRow2.reassignedOwner}" 
										onchange="updateRoleTransfer(this,'${toDoResultRow2.fileNumber}','${toDoResultRow2.id}','${toDoResultRow2.url}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.rolelist}')" />
									</c:if>	
									<c:if test ="${toDoResultRow2.rolelist=='Billing'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer"  list="%{billingList}"
										cssStyle="width:145px" headerKey="" headerValue="" value="${toDoResultRow2.reassignedOwner}" 
										onchange="updateRoleTransfer(this,'${toDoResultRow2.fileNumber}','${toDoResultRow2.id}','${toDoResultRow2.url}','${toDoResultRow2.resultRecordId}','${toDoResultRow2.rolelist}')" />
									</c:if>	
		</display:column>
	</sec-auth:authComponent>
										
	--%>
	</display:table>
	
</div>
</td>
</tr> 

<tr>
	<td height="10" width="100%" align="left" >
	 <div id="bobcontent1-title" class="handcursor">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countResultOverDue}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="bobcontent1" class="switchgroup1">
	<s:set name="toDoResults1" value="toDoResults1" scope="request"/> 
	<display:table name="toDoResults1" class="table" requestURI="" id="toDoResultRow3"  defaultsort="4" export="" pagesize="10">
	<c:if test="${toDoResultRow3.resultRecordType!='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" sortProperty="fileNumber"> 
	<a HREF="javascript:updateCheckUserName('${toDoResultRow3.resultRecordId}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.url}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.fieldToValidate2}','${toDoResultRow3.resultRecordId}','${toDoResultRow3.supportingId }','N','${toDoResultRow3.ruleNumber}');"><c:out value="${toDoResultRow3.fileNumber}" /></a>
	</display:column>
	</c:if>
	<c:if test="${toDoResultRow3.resultRecordType=='AccountLineList' }">
	<display:column sortable="true" titleKey="toDoList.file" >
	<a HREF="javascript:updateCheckUserName('${toDoResultRow3.resultRecordId}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.url}','${toDoResultRow3.fieldToValidate1}','${toDoResultRow3.fieldToValidate2}','','${toDoResultRow3.resultRecordId}','Y','${toDoResultRow3.ruleNumber}');"><c:out value="${toDoResultRow3.fileNumber}" /></a>
	</display:column>
	</c:if>
	<display:column property="messagedisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
    <display:column property="fieldDisplay" sortable="true"  titleKey="toDoRule.fieldtovalidate"/>
  	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   titleKey="toDoList.shipper"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true" title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	<c:if test="${flagForSupervisor=='false'}">
	<display:column title="Note" sortable="true" headerClass="centeralign" style="text-align: center;width:10%;" maxLength="40">
	<c:if test="${toDoResultRow3.isNotesAdded == false}">
		<c:if test="${toDoResultRow3.note!=''}">
		<a HREF="javascript:openNote('${toDoResultRow3.noteId}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"><c:out value="${toDoResultRow3.note}" /></a>
	    </c:if>
	    <c:if test="${toDoResultRow3.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow3.fileNumber}','${toDoResultRow3.todoRuleId}','${toDoResultRow3.id}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"/>
		</c:if>
	</c:if>
	<c:if test="${toDoResultRow3.isNotesAdded== true}">
	<c:if test="${toDoResultRow3.note!='' }">
	<a HREF="javascript:openNote('${toDoResultRow3.noteId}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"><c:out value="${toDoResultRow3.note}" /></a>
	</c:if>
	<c:if test="${toDoResultRow3.note=='' }">
	<img src="${pageContext.request.contextPath}/images/open.png" onclick="openRulesNotes('${toDoResultRow3.fileNumber}','${toDoResultRow3.todoRuleId}','${toDoResultRow3.id}','${toDoResultRow3.noteType}','${toDoResultRow3.noteSubType}');"/>
	</c:if>
	</c:if>
	</display:column> 
	</c:if>
	<%-- <sec-auth:authComponent componentId="module.script.form.roletransfer">
	<display:column title ="Role&nbsp;Transfer">
							<c:if test ="${toDoResultRow3.rolelist=='Coordinator'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer"  list="%{coord}"
										cssStyle="width:145px" headerKey="" headerValue=""
										onchange="updateRoleTransfer(this,'${toDoResultRow3.fileNumber}','${toDoResultRow3.id}','${toDoResultRow3.url}','${toDoResultRow3.resultRecordId}','${toDoResultRow3.rolelist}')" />
									</c:if>	
									<c:if test ="${toDoResultRow3.rolelist=='Consultant'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer"   list="%{sale}"
										cssStyle="width:145px" headerKey="" headerValue="" 
										onchange="updateRoleTransfer(this,'${toDoResultRow3.fileNumber}','${toDoResultRow3.id}','${toDoResultRow3.url}','${toDoResultRow3.resultRecordId}','${toDoResultRow3.rolelist}')" />
									</c:if>	
									<c:if test ="${toDoResultRow3.rolelist=='Billing'}">
	                                    <s:select cssClass="list-menu"
										name="roleTransfer" id="roleTransfer"  list="%{billingList}"
										cssStyle="width:145px" headerKey="" headerValue="" 
										onchange="updateRoleTransfer(this,'${toDoResultRow3.fileNumber}','${toDoResultRow3.id}','${toDoResultRow3.url}','${toDoResultRow3.resultRecordId}','${toDoResultRow3.rolelist}')" />
									</c:if>	
	</display:column>
</sec-auth:authComponent>
	--%>
	</display:table>
	
</div>
</td>
</tr> 
<c:if test="${user.userType!='AGENT' && user.userType!='PARTNER'}">
<tr>
<td colspan="3" align="left">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0"  style="width: 100%;margin:0px;padding:0px;">
<tr>
<td width="200px"><img src="${pageContext.request.contextPath}/images/check-List.jpg" border="1" style="cursor:auto;"/></td>
</tr>
</table>
</td>
</tr>

<tr>
	<td height="10" width="100%" align="left" >
	 <div id="bobcontent8-title" class="handcursor">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Due Today</font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListTodayResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="bobcontent8" class="switchgroup1">
	<s:set name ="checkListTodaySet" value = "checkListTodaySet" scope="request"/>
	<display:table name="checkListTodaySet" class="table" requestURI="" id="checkListTodayListId"  defaultsort="4" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<a HREF="javascript:gotoRespectiveFile('${checkListTodayListId.resultId}','${checkListTodayListId.url}');"><c:out value="${checkListTodayListId.resultNumber}" /></a>
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
	<display:column property="docType" sortable="true"   title="Document to Validate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true" title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	<display:column title="Note" sortable="true" headerClass="centeralign" style="text-align: center; width:10%;" maxLength="40">
	
	<c:if test="${checkListTodayListId.isNotesAdded == false}">
		<c:if test="${checkListTodayListId.note!=''}">
		<a HREF="javascript:openNote('${checkListTodayListId.noteId}','${checkListTodayListId.noteType}','${checkListTodayListId.noteSubType}');"><c:out value="${checkListTodayListId.note}" /></a>
	    </c:if>
	    <c:if test="${checkListTodayListId.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openCheckListNotes('${checkListTodayListId.resultNumber}','${checkListTodayListId.checkListId}','${checkListTodayListId.id}','${checkListTodayListId.noteType}','${checkListTodayListId.noteSubType}');"/>
	   </c:if>
	</c:if>
	<c:if test="${checkListTodayListId.isNotesAdded== true}">
	<c:if test="${checkListTodayListId.note!='' }">
	<a HREF="javascript:openNote('${checkListTodayListId.noteId}','${checkListTodayListId.noteType}','${checkListTodayListId.noteSubType}');"><c:out value="${checkListTodayListId.note}" /></a>
	</c:if>
	<c:if test="${checkListTodayListId.note=='' }">
	<img src="${pageContext.request.contextPath}/images/open.png" onclick="openCheckListNotes('${checkListTodayListId.resultNumber}','${checkListTodayListId.checkListId}','${checkListTodayListId.id}','${checkListTodayListId.noteType}','${checkListTodayListId.noteSubType}');"/>
	</c:if>
	</c:if>
	</display:column>
	</display:table>
	
</div>
</td>
</tr> 

<tr>
	<td height="10" width="100%" align="left" >
	 <div id="bobcontent9-title" class="handcursor">
	 <table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;" >
	<tr>
	<td class="headtab_left">
	</td>
	<td NOWRAP class="headtab_center">
	&nbsp;&nbsp;<font color="black">Over Due </font> &nbsp;&nbsp;(<font color="red">&nbsp;${countCheckListOverDueResult}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_center">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</table>
	</div>
	<div id="bobcontent9" class="switchgroup1">
	<s:set name ="checkListOverDueSet" value="checkListOverDueSet" scope ="request"/>
	<display:table name="checkListOverDueSet" class="table" requestURI="" id="checkListOverDueListId"  defaultsort="4" export="" pagesize="10">
	<display:column sortable="true" title="File ID" sortProperty="resultNumber">
	<a HREF="javascript:gotoRespectiveFile('${checkListOverDueListId.resultId}','${checkListOverDueListId.url}');"><c:out value="${checkListOverDueListId.resultNumber}" /></a>
	</display:column>
	<display:column property="messageDisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
	<display:column property="docType" sortable="true"   title="Document to Validate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="duration" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="rolelist" sortable="true"   title="Role"/>
	<display:column property="owner" sortable="true"   title="Name"/>
	<display:column property="checkedUserName" sortable="true" title="viewedBy"/>
	<display:column property="billToName" sortable="true"   title="Bill To Name"/>
	
	<display:column title="Note" sortable="true" headerClass="centeralign" style="text-align: center; width:10%;" maxLength="40">
	
		<c:if test="${checkListOverDueListId.isNotesAdded == false}">
		<c:if test="${checkListOverDueListId.note!=''}">
		<a HREF="javascript:openNote('${checkListOverDueListId.noteId}','${checkListOverDueListId.noteType}','${checkListOverDueListId.noteSubType}');"><c:out value="${checkListOverDueListId.note}" /></a>
	    </c:if>
	    <c:if test="${checkListOverDueListId.note==''}">
		<img src="${pageContext.request.contextPath}/images/open.png" onclick="openCheckListNotes('${checkListOverDueListId.resultNumber}','${checkListOverDueListId.checkListId}','${checkListOverDueListId.id}','${checkListOverDueListId.noteType}','${checkListOverDueListId.noteSubType}');"/>
	   </c:if>
	</c:if>
	<c:if test="${checkListOverDueListId.isNotesAdded== true}">
	<c:if test="${checkListOverDueListId.note!='' }">
	<a HREF="javascript:openNote('${checkListOverDueListId.noteId}','${checkListOverDueListId.noteType}','${checkListOverDueListId.noteSubType}');"><c:out value="${checkListOverDueListId.note}" /></a>
	</c:if>
	<c:if test="${checkListOverDueListId.note=='' }">
	<img src="${pageContext.request.contextPath}/images/open.png" onclick="openCheckListNotes('${checkListOverDueListId.resultNumber}','${checkListOverDueListId.checkListId}','${checkListOverDueListId.id}','${checkListOverDueListId.noteType}','${checkListOverDueListId.noteSubType}');"/>
	</c:if>
	</c:if>
	</display:column>
	
	</display:table>
	
</div>
</td>
</tr> 
</c:if>
<tr>
	<td height="10" width="100%" align="left" >
	 <div id="bobcontent17-title" class="handcursor">
	 
	 <table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px">
	<tbody><tr>
	<td class="headtab_left">
	</td>
	
	<td nowrap="" class="headtab_center">
	&nbsp;&nbsp;<font color="black">Agent TDR Email Verification</font>&nbsp;&nbsp;(<font color="red">&nbsp;${countAgentTdrEmail}&nbsp;</font>)
	</td>
	<td width="28" valign="top" class="headtab_bg"></td>
	<td class="headtab_bg_special">&nbsp;
	</td>
	<td class="headtab_right">
	</td>
	</tr>
	</tbody>
	</table>
	</div>
	<div id="bobcontent17" class="switchgroup1">
	<s:set name ="agentTdrEmailSet" value = "agentTdrEmailSet" scope="request"/>
	<display:table name="agentTdrEmailSet" class="table" requestURI="" id="agentTdrEmailSetID"  defaultsort="4" export="" pagesize="10">
   	<display:column sortable="true" title="File Number" sortProperty="fileNumber">
	<a HREF="javascript:updateCheckUserName1('${agentTdrEmailSetID.resultRecordId}','${agentTdrEmailSetID.fieldToValidate1}','${agentTdrEmailSetID.url}','${agentTdrEmailSetID.fieldToValidate1}','${agentTdrEmailSetID.fieldToValidate2}','${agentTdrEmailSetID.resultRecordId}','${agentTdrEmailSetID.resultRecordId}','','${agentTdrEmailSetID.ruleNumber}','${agentTdrEmailSetID.supportingId}');"><c:out value="${agentTdrEmailSetID.fileNumber}" /></a>
	</display:column>
	<display:column property="messagedisplayed"  style="width:20%" sortable="true"     titleKey="toDoRulemessage.message"/>
    <display:column property="fieldDisplay" sortable="true"  titleKey="toDoRule.fieldtovalidate"/>
   	<display:column headerClass="containeralign" style="text-align: right" property="durationAddSub" sortable="true"   title="OD"/>
	<display:column property="shipper" sortable="true"   title="Shipper's Name"/>
	<display:column property="agentName" sortable="true"   title="Agent Name"/>
	<display:column property="rolelist" sortable="true"   title="Agent Role"/>
	<display:column property="emailNotification" sortable="true" title="Agent Email"/>
	</display:table>
	
</div>
</td>
</tr> 
   </td>
   </tr>
   </tbody>
   </table>
   
   </div>
<div class="bottom-header" style="margin-top:35px;!margin-top:45px;"><span></span></div>

</div>
</div>
    
 </div>           
</s:form>

<script type="text/javascript"> 
function openRulesNotes(target, target1, target2, target3, target4){

    openWindow('openRulesNotes.html?fileNumber='+target+'&tdrID='+target1+'&resultRecordId='+target2+'&noteType='+target3+'&noteSubType='+target4+'&decorator=popup&popup=true',650,305);

    }
function openCheckListNotes(target, target1, target2, target3, target4){

    openWindow('openCheckListNotes.html?fileNumber='+target+'&tdrID='+target1+'&resultRecordId='+target2+'&noteType='+target3+'&noteSubType='+target4+'&checkListType=ForCheckList&decorator=popup&popup=true',650,305);

    }
 
 function openNote(id,noteType,noteSubType)
 {

  	openWindow('openRulesNotess.html?id='+id+'&noteType='+noteType+'&noteSubType='+noteSubType+'&decorator=popup&popup=true',650,305);

 }
function openRulesNotess(target, target1, target2){

	openWindow('openRulesNotess.html?id='+target+'&noteType='+target1+'&noteSubType='+target2+'&decorator=popup&popup=true',650,305);

}
highlightTableRows("notes1List"); 
highlightTableRows("notes2List"); 
highlightTableRows("notesList");

try{ document.forms['toDo'].elements['newIdList'].value='';
removeTick();
}
catch(e){}

try{
	var msg = "${successMessage}";
	var chk = "${chkFlag}"; 
	if(chk == "Accept"){
	alert(msg);
	 location.href = "toDos.html?fromUser=${userFirstName}"
	}
	if(chk == "Decline"){
		location.href = "toDos.html?fromUser=${userFirstName}"
	}
}catch(e){alert(e)}
</script> 


<script type="text/javascript">
try{
<c:if test="${not empty toDayAlertList}">
		document.forms['toDo'].elements['showLine'].disabled=false;
</c:if>
<c:if test="${empty toDayAlertList}">
		document.forms['toDo'].elements['showLine'].disabled=true;
</c:if>

}
catch(e){}
<c:if test="${flagexternalCordinator=='true'}">
if(window.location.href.indexOf('toDosTeam.html')>0 ||window.location.href.indexOf('toDoSupervisorList.html')>0 ||window.location.href.indexOf('ownerResult.html')>0)
{

}
else
{
document.getElementById('toDo_allradioOwn').checked=true
document.forms['toDo'].elements['ownerName'].value='${userFirstName}'.toUpperCase();;

}

</c:if>
</script>
