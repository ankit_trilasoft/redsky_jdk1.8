package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsTenancyManagementDao;
import com.trilasoft.app.model.DsTenancyManagement;
import com.trilasoft.app.service.DsTenancyManagementManager;


public class DsTenancyManagementManagerImpl extends GenericManagerImpl<DsTenancyManagement, Long> implements DsTenancyManagementManager { 
	DsTenancyManagementDao dsTenancyManagementDao;
	public DsTenancyManagementManagerImpl(DsTenancyManagementDao dsTenancyManagementDao) {
		super(dsTenancyManagementDao); 
        this.dsTenancyManagementDao = dsTenancyManagementDao; 	
	}
	
	public List getListById(Long id) {
		return dsTenancyManagementDao.getListById(id);
	}

}

