<% 
//Gallery.js in current version was not stable
//copied image folder from dojo/dojox/images of version 1.3 over to current version 
%>

<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<!-- load the dojo toolkit base -->
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>"
    djConfig="parseOnLoad:true, isDebug:false"></script>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/redskydojo.js'/>"></script>


<style type="text/css">
  @import "scripts/dojo/dojox/image/resources/image.css";

.thumbWrapper.thumbHoriz div {
float:left;
margin-right:3px;
padding-right:2px;
}

.thumbWrapper img {
height:75px;
max-width:100px;
border:1px solid #cccccc;
margin-right:2px; 
}  

.thumbWrapper .thumbNotifier {
background-color:#BFDBFF;
font-size:1%;
height:4px;
margin-left:2px;
width:0;
}
.thumbWrapper .thumbLoaded {
background-color:#F38621;
}


</style>
<script type="text/javascript">
/*
.thumbOuter.thumbHoriz {
height:0;
width:0px;
}

.thumbHoriz .thumbScroller {
float:left;
height:0;
width:0px;
}

.slideShowCtrl {
float:left;
height:0;
position:relative;
width:100%;
z-index:999;
}
*/

  dojo.require("dojox.image.Gallery");
  dojo.require("dojox.image.SlideShow");
  dojo.require("dojo.data.ItemFileReadStore");
  dojo.require("dojo.parser");

  function getPageNumber(){
	  
		var imageId = "dojox_image_SlideShow_0_imageDiv0";
		var imageNum = 0; 
		while (imageNum < 50) {
			var imageDiv = document.getElementById(imageId);
			var imageDivStyleOpacity = imageDiv?imageDiv.style.opacity:"";
			
			if (imageDiv && imageDivStyleOpacity != "0"){
				return imageNum;
			} else {
				imageNum = imageNum +1;
				imageId = "dojox_image_SlideShow_0_imageDiv" + imageNum;
			}
		}
		return -1;
	}
  
</script>
<!-- FOR FRONT PAGE ONLY 
	<div jsId="imageItemStore" dojoType="dojo.data.ItemFileReadStore" url="<c:url value='getImages.html?fileId=${myFile.id}'/>"></div>
-->
<div jsId="imageItemStore" dojoType="dojo.data.ItemFileReadStore" url="<c:url value='getImages.html?fileId=${myFile.id}'/>"></div>
<div id="gallery1" dojoType="dojox.image.Gallery" imageHeight="800" imageWidth="665">
  <script type="dojo/connect">
    var itemRequest = {
      query: {},
      count: 20
    };
    var itemNameMap = {
      imageThumbAttr: "thumb",
      imageLargeAttr: "large"
    };
    this.setDataStore(imageItemStore, itemRequest, itemNameMap);
  </script>
</div>