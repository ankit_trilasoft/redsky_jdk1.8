
DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_servicepartner BEFORE DELETE ON servicepartner
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

      IF (OLD.email <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.email~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email,'~') into oldValueList;
   END IF;

    IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;


   IF (OLD.sequenceNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
   END IF;
   IF (OLD.ship <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
   END IF;
   IF (OLD.shipNumber<> '') THEN
       select CONCAT(fieldNameList,'servicepartner.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
   END IF;

   IF (OLD.city <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
   END IF;

   IF (OLD.miles <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.miles~') into fieldNameList;
       select CONCAT(oldValueList,OLD.miles,'~') into oldValueList;
   END IF;

   IF (OLD.bookNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.bookNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.bookNumber,'~') into oldValueList;
   END IF;

   IF (OLD.carrierClicd <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierClicd~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierClicd,'~') into oldValueList;
   END IF;

   IF (OLD.carrierCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierCode,'~') into oldValueList;
   END IF;

   IF (OLD.carrierName <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierName,'~') into oldValueList;
   END IF;

   IF (OLD.carrierNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierNumber,'~') into oldValueList;
   END IF;
   IF (OLD.carrierPhone <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierPhone,'~') into oldValueList;
   END IF;


   IF (OLD.carrierVessels <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierVessels~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierVessels,'~') into oldValueList;
   END IF;
   IF (OLD.ETATA <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.ETATA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ETATA,'~') into oldValueList;
   END IF;
   IF (OLD.ETDTA <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.ETDTA~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ETDTA,'~') into oldValueList;
   END IF;

   IF (OLD.omni <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.omni~') into fieldNameList;
       select CONCAT(oldValueList,OLD.omni,'~') into oldValueList;
   END IF;

    IF (OLD.pdCost1 <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.pdCost1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdCost1,'~') into oldValueList;
   END IF;
   IF (OLD.pdCost2 <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.pdCost2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdCost2,'~') into oldValueList;
   END IF;
   IF (OLD.pdDays2 <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.pdDays2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdDays2,'~') into oldValueList;
   END IF;

   IF (OLD.pdFreedays <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.pdFreedays~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pdFreedays,'~') into oldValueList;
   END IF;

     IF (OLD.carrierArrival <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierArrival,'~') into oldValueList;
   END IF;
   IF (OLD.carrierDeparture <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.carrierDeparture~') into fieldNameList;
       select CONCAT(oldValueList,OLD.carrierDeparture,'~') into oldValueList;
   END IF;
   IF (OLD.atArrival <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.atArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.atArrival,'~') into oldValueList;
   END IF;

   IF (OLD.atDepart <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.atDepart~') into fieldNameList;
       select CONCAT(oldValueList,OLD.atDepart,'~') into oldValueList;
   END IF;
     IF (OLD.etArrival <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.etArrival~') into fieldNameList;
       select CONCAT(oldValueList,OLD.etArrival,'~') into oldValueList;
   END IF;
   IF (OLD.etDepart <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.etDepart~') into fieldNameList;
       select CONCAT(oldValueList,OLD.etDepart,'~') into oldValueList;
   END IF;
   IF (OLD.coord <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.coord~') into fieldNameList;
       select CONCAT(oldValueList,OLD.coord,'~') into oldValueList;
   END IF;

   IF (OLD.partnerType <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.partnerType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerType,'~') into oldValueList;
   END IF;

 IF (OLD.dropPick <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.dropPick~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dropPick,'~') into oldValueList;
   END IF;
    IF (OLD.liveLoad <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.liveLoad~') into fieldNameList;
       select CONCAT(oldValueList,OLD.liveLoad,'~') into oldValueList;
   END IF;
    IF (OLD.blNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.blNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.blNumber,'~') into oldValueList;
   END IF;
    IF (OLD.transhipped <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.transhipped~') into fieldNameList;
       select CONCAT(oldValueList,OLD.transhipped,'~') into oldValueList;
   END IF;
    IF (OLD.polCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.polCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.polCode,'~') into oldValueList;
   END IF;

    IF (OLD.poeCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.poeCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.poeCode,'~') into oldValueList;
   END IF;
    IF (OLD.actgCode <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.actgCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.actgCode,'~') into oldValueList;
   END IF;
    IF (OLD.serviceOrderId <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
   END IF;
    IF (OLD.houseBillIssued <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.houseBillIssued~') into fieldNameList;
       select CONCAT(oldValueList,OLD.houseBillIssued,'~') into oldValueList;
   END IF;
    IF (OLD.cntnrNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
   END IF;
 IF (OLD.aesNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.aesNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.aesNumber,'~') into oldValueList;
   END IF;
    IF (OLD.skids <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.skids~') into fieldNameList;
       select CONCAT(oldValueList,OLD.skids,'~') into oldValueList;
   END IF;
    IF (OLD.clone <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.clone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.clone,'~') into oldValueList;
   END IF;
    IF (OLD.servicePartnerId <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.servicePartnerId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.servicePartnerId,'~') into oldValueList;
   END IF;
  IF (OLD.ugwIntId <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.ugwIntId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
   END IF;
     IF (OLD.houseBillNumber <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.houseBillNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.houseBillNumber,'~') into oldValueList;
   END IF;

   
   
   
   IF (OLD.caed <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.caed~') into fieldNameList;
       select CONCAT(oldValueList,OLD.caed,'~') into oldValueList;
   END IF;

   
    IF (OLD.status <> '') THEN
       select CONCAT(fieldNameList,'servicepartner.status~') into fieldNameList;
       select CONCAT(oldValueList,OLD.status,'~') into oldValueList;
   END IF;
   
   
   
   
   CALL add_tblHistory (OLD.serviceOrderId,"servicepartner", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$

