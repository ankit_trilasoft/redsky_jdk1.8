package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.DocumentAccessControl;
import com.trilasoft.app.service.DocumentAccessControlManager;
import com.trilasoft.app.service.RefMasterManager;

public class DocumentAccessControlAction extends BaseAction {

	private String sessionCorpID;
	
	private DocumentAccessControlManager documentAccessControlManager;

	private DocumentAccessControl documentAccessControl;

	public List documentAccessControls;

	private Long id;
	
	private String hitFlag;
	
	private RefMasterManager refMasterManager;
	
	private static Map<String, String> docsList;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(DocumentAccessControlAction.class);

	 

	public DocumentAccessControlAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		documentAccessControls = documentAccessControlManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		if (id != null) {
			documentAccessControl = documentAccessControlManager.get(id);
		} else {
			documentAccessControl = new DocumentAccessControl();
			documentAccessControl.setCreatedOn(new Date());
			documentAccessControl.setUpdatedOn(new Date());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		documentAccessControl.setCorpID(sessionCorpID);
		boolean isNew = (documentAccessControl.getId() == null); 
		List isexisted=documentAccessControlManager.isExisted(documentAccessControl.getFileType(), sessionCorpID);
		if(!isexisted.isEmpty() && isNew){
			String key = "Document already exist with this document type.";   
			errorMessage(getText(key));
			return INPUT; 
		}else{ 
			if(isNew){
				documentAccessControl.setCreatedOn(new Date());
			}
			documentAccessControl.setUpdatedOn(new Date());
			documentAccessControlManager.save(documentAccessControl);   
		     String key = (isNew) ? "documentAccessControl.added" : "documentAccessControl.updated";   
		     saveMessage(getText(key));   
		     if (!isNew) { 
		     	hitFlag ="1";
		     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		          return INPUT;   
		     }else{
		     	hitFlag ="1";
		     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		        	return SUCCESS; 
		     }
		} 
	}
	
	public String deleteDocAccess(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		documentAccessControlManager.remove(id);
		String key = "documentAccessControl.deleted";   
	     saveMessage(getText(key));
		hitFlag ="1";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public void setDocumentAccessControlManager(DocumentAccessControlManager documentAccessControlManager) {
		this.documentAccessControlManager = documentAccessControlManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public DocumentAccessControl getDocumentAccessControl() {
		return documentAccessControl;
	}

	public void setDocumentAccessControl(DocumentAccessControl documentAccessControl) {
		this.documentAccessControl = documentAccessControl;
	}

	public List getDocumentAccessControls() {
		return documentAccessControls;
	}

	public void setDocumentAccessControls(List documentAccessControls) {
		this.documentAccessControls = documentAccessControls;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public static Map<String, String> getDocsList() {
		return docsList;
	}

	public static void setDocsList(Map<String, String> docsList) {
		DocumentAccessControlAction.docsList = docsList;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
}
