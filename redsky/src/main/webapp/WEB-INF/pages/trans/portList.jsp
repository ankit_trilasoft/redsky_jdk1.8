<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>   
    <title><fmt:message key="portList.title"/></title>   
    <meta name="heading" content="<fmt:message key='portList.heading'/>"/>   
    <script language="javascript" type="text/javascript">
  function clear_fields(){
  
			    
			    document.forms['searchForm'].elements['portCode'].value = "";
			    document.forms['searchForm'].elements['portName'].value = "";
		        document.forms['searchForm'].elements['country'].value = "";
		        document.forms['searchForm'].elements['modeType'].value = "";
		        document.forms['searchForm'].elements['port.active'].checked = false;
}
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
form {
margin-top:-12px;
}

div#main {
margin:-5px 0 0;

}
</style> 
<script>
function goToCustomerDetail(targetValue){
        document.forms['searchForm'].elements['id'].value = targetValue;
        document.forms['searchForm'].action = 'editPort.html?from=list';
        document.forms['searchForm'].submit();
}

function checkPortActiveStatus(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updatePortActiveStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&id=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
</script>  
</head>
<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />


<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="editPort.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set>  
<s:form id="searchForm" action='${empty param.popup?"searchPortList.html":"searchPortList.html?decorator=popup&popup=true"}' method="post" validate="true" >
<div id="layer1" style="width:100%; ">

<div id="otabs" style="">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		  <div class="spnblk">&nbsp;</div>
		</div>
		
		<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<s:hidden name="port.active" value="true" />
	<s:hidden name="active" value="true" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<c:set var="port.active" value="true"/>
	<c:set var="active" value="true"/>
</c:if>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="searchPortList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center">
<div id="liquid-round-top" >
    <div class="top" style="margin-top:-20px;!margin-top:-2px; "><span></span></div>
    <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th><fmt:message key="port.portCode"/></th>
		<th><fmt:message key="port.portName"/></th>
		<th><fmt:message key="port.country"/></th>
		<th><fmt:message key="port.modeType"/></th>
		<c:if test="${empty param.popup}">
		<th>Active</th>
		</c:if>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="portCode" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="portName" required="true" cssClass="input-text" size="30"/>
					</td>
					
					<td width="" align="left">
					    <s:select name="country" list="%{ocountry}" cssClass="list-menu" cssStyle="width:180px" headerKey="" headerValue="" />
					</td>
					<td width="" align="left">
					    <s:select name="modeType" list="%{mode}" cssClass="list-menu" cssStyle="width:90px"/>
					</td>
					
					<c:if test="${empty param.popup}">
					<c:set var="isActive" value="false"/>
					<c:if test="${port.active}">
						<c:set var="isActive" value="true"/>
					</c:if>
					<td width="150px"><s:checkbox key="port.active" value="${isActive}" fieldValue="true"  /></td>
					</c:if>
					</tr>
					<tr>
					<c:if test="${empty param.popup}">
					<td colspan="4"></td>
					</c:if>
					<c:if test="${not empty param.popup}">
					<td colspan="3"></td>
					</c:if>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			
				</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div> 
<s:set name="ports" value="ports" scope="request"/>  

<div id="otabs" style="margin-top: -15px;">
		  <ul>
		    <li><a class="current"><span>Port&nbsp;List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<display:table name="ports" class="table" requestURI="" id="portList" export="${empty param.popup}" defaultsort="2" pagesize="10" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;" 
		decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
    <c:if test="${empty param.popup}">  
    <display:column sortable="true" titleKey="port.portCode"><a onclick="goToCustomerDetail(${portList.id});" style="cursor:pointer"><c:out value="${portList.portCode}" /></a></display:column>
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="port.portCode"/>   
    </c:if>    
    <display:column property="portName" sortable="true" titleKey="port.portName"/>
    <display:column property="country" sortable="true" titleKey="port.country"/>
    <display:column property="mode" sortable="true" titleKey="port.modeType"/>
     <display:column property="refAbbreviation" sortable="true" title="Abbreviation"/>
     <display:column property="longitude" sortable="true" title="Longitude"/>
     <display:column property="latitude" sortable="true" title="Latitude"/>
    <display:column style="width:5px" sortable="true" title="ICD">
		 <c:if test="${portList.ICD == true}">
			<img id="target" src="${pageContext.request.contextPath}/images/A.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
		</c:if>
	<c:if test="${portList.ICD == false || portList.ICD == null}">
	</c:if>
	</display:column>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;">
				    <c:if test="${portList.active == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${portList.id}" onclick="checkPortActiveStatus(${portList.id},this)" checked/>
				    </c:if>
					<c:if test="${portList.active == false || portList.active == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${portList.id}" onclick="checkPortActiveStatus(${portList.id},this)"/>
					</c:if>
   </display:column>

   </c:if>
    <display:setProperty name="paging.banner.item_name" value="port"/>   
    <display:setProperty name="paging.banner.items_name" value="ports"/>   
  
    <!--<display:setProperty name="export.excel.filename" value="Port List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Port List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Port List.pdf"/>   
--></display:table>  
 <c:if test="${empty param.popup}"> 
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<s:hidden name="id"/>
</div>
</s:form>
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/ports.html" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>  
