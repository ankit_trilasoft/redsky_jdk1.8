package com.trilasoft.app.webapp.action;

import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.MenuItemPermission;
import org.appfuse.service.GenericManager;

import java.util.List;

public class MenuItemPermissionAction extends BaseAction {
    private GenericManager<MenuItemPermission, Long> menuItemPermissionManager;
    private List menuItemPermissions;

    public void setMenuItemPermissionManager(GenericManager<MenuItemPermission, Long> menuItemPermissionManager) {
        this.menuItemPermissionManager = menuItemPermissionManager;
    }

    public List getMenuItemPermissions() {
        return menuItemPermissions;
    }

    public String list() {
        menuItemPermissions = menuItemPermissionManager.getAll();
        return SUCCESS;
    }
    private MenuItemPermission menuItemPermission;
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public MenuItemPermission getMenuItemPermission() {
        return menuItemPermission;
    }

    public void setMenuItemPermission(MenuItemPermission menuItemPermission) {
        this.menuItemPermission = menuItemPermission;
    }

    public String delete() {
        menuItemPermissionManager.remove(menuItemPermission.getId());
        saveMessage(getText("menuItemPermission.deleted"));

        return SUCCESS;
    }

    public String edit() {
        if (id != null) {
            menuItemPermission = menuItemPermissionManager.get(id);
        } else {
            menuItemPermission = new MenuItemPermission();
        }

        return SUCCESS;
    }

    public String save() throws Exception {
        if (cancel != null) {
            return "cancel";
        }

        if (delete != null) {
            return delete();
        }

        boolean isNew = (menuItemPermission.getId() == null);

        menuItemPermission = menuItemPermissionManager.save(menuItemPermission);

        String key = (isNew) ? "menuItemPermission.added" : "menuItemPermission.updated";
        saveMessage(getText(key));

        if (!isNew) {
            return INPUT;
        } else {
            return SUCCESS;
        }
    }
}