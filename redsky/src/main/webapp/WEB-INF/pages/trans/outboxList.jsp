<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="myMessageList.title"/></title>   
    <meta name="heading" content="<fmt:message key='myMessageList.heading'/>"/>   



<!--<script language="javascript" type="text/javascript" SRC="/scripts/mktree.js">
</script>
<LINK REL="stylesheet" HREF="/styles/mktree.css"> 

-->
</head>


<s:form id="myMessageForm" action="searchMyMessages" method="post" >  
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editMyMessage.html"/>'"  
        value="<fmt:message key="button.compose"/>"/>   
           
    <input type="button" class="cssbutton" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set> 
<!-- 
<table>
<tbody>
<tr>
			<td rowspan="25" valign="top" align="left" width="120px">
				<ul class="mktree" id="tree1">
					<li>Folders
						<ul>
							<li><font color="red" >Inbox</font></li>
							<li><a href="myOutboxMessages.html?status=sent&loggedInUser=${pageContext.request.remoteUser}" >Outbox</a></li>
							<li><a href="editMyMessage.html" >Compose</a></li>
						</ul>
					</li>
				</ul>
			</td>
<td> -->
<s:label name="fdr" value="You have %{countUnreadMessages} unread messages"/>
<div id="newmnav">
		    <ul>
			  <li><a href="myMessages.html?fromUser=${pageContext.request.remoteUser}"><span>Inbox</span></a></li>
			  <li id="newmnav1" style="background:#FFF"><a class="current" ><span>Outbox<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <li><a href="editMyMessage.html" ><span>Compose</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><br>
<div id="Layer1">

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>

<s:set name="myMessages" value="myMessages" scope="request"/>  
<display:table name="myMessages" class="table" requestURI="" id="myMessageList" export="true" pagesize="10"  >   
    <display:column property="toUser" sortable="true" titleKey="myMessage.toUser" url="/editMyMessage.html?from=list" paramId="id" paramProperty="id"/>
    <display:column property="subject" sortable="true" titleKey="myMessage.subject"/>
    <display:column property="sentOn" sortable="true" titleKey="myMessage.sentOn" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="status" sortable="true" titleKey="myMessage.status" />
    
    <display:setProperty name="paging.banner.item_name" value="message"/>   
    <display:setProperty name="paging.banner.items_name" value="messages"/>   
  
    <display:setProperty name="export.excel.filename" value="Message List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Message List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Message List.pdf"/>   
</display:table>  
  
  
			</td>
		</tr>
	</tbody>
</table>
</div>   
<!-- 
</td>
</tr>
</tbody>
</table> 
 -->
</s:form>


<script type="text/javascript">   
   	highlightTableRows("myMessageList");  
   	Form.focusFirstElement($("myMessageForm"));  
</script>
 
