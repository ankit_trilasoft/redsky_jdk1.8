package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AdAddressesDetailsDao;
import com.trilasoft.app.dao.RefFreightRatesDao;
import com.trilasoft.app.model.AdAddressesDetails;
import com.trilasoft.app.model.RefFreightRates;
import com.trilasoft.app.service.AdAddressesDetailsManager;


public class AdAddressesDetailsManagerImpl extends GenericManagerImpl<AdAddressesDetails, Long> implements AdAddressesDetailsManager {
	AdAddressesDetailsDao adAddressesDetailsDao;
	public AdAddressesDetailsManagerImpl(AdAddressesDetailsDao adAddressesDetailsDao) {
		super(adAddressesDetailsDao); 
        this.adAddressesDetailsDao = adAddressesDetailsDao; 
	}
	public List getAdAddressesDetailsList(Long id, String sessionCorpID) {
		
		return	adAddressesDetailsDao.getAdAddressesDetailsList(id, sessionCorpID);
	} 
	public Map<String, String> getAdAddressesDropDown(Long id, String sessionCorpID,String type, String jobType){
		return adAddressesDetailsDao.getAdAddressesDropDown(id, sessionCorpID, type ,jobType);
	}
	public Long findRemoteAdAddressesDetails(Long networkId, Long id) {
		return adAddressesDetailsDao.findRemoteAdAddressesDetails(networkId, id);
	}
	public int updateAdAddressesDetailsNetworkId(Long id) {
		return adAddressesDetailsDao.updateAdAddressesDetailsNetworkId(id);
	}
	public AdAddressesDetails getFirstAddressDetail(String sessionCorpID,Long id, String adType){
		return  adAddressesDetailsDao.getFirstAddressDetail(sessionCorpID, id, adType);
	}
}
