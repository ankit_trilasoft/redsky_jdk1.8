package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.SoAdditionalDateDao;
import com.trilasoft.app.model.SoAdditionalDate;

public class SoAdditionalDateDaoHibernate extends GenericDaoHibernate<SoAdditionalDate, Long>implements SoAdditionalDateDao{
	
	public SoAdditionalDateDaoHibernate() {
		super(SoAdditionalDate.class);
	}
	
	public List findId(Long sid, String sessionCorpID) {
    	return getHibernateTemplate().find("select id from SoAdditionalDate where serviceOrderId=?",sid);
    }
}
