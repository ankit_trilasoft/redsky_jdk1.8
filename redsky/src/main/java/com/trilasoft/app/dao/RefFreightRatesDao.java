package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RefFreightRates;



public interface RefFreightRatesDao extends GenericDao<RefFreightRates, Long>{
	public 	String saveData(String sessionCorpID, String file, String userName);
	public List getExistingContracts(String sessionCorpID);
	public List getMissingPort(String sessionCorpID);

}
