package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Columns;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
@Entity
@Table(name="storagelibrary")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class StorageLibrary extends BaseObject{
	private static final long serialVersionUID = 1L;
	private Long id;
	private String corpId;		
	private Date createdOn;
	private Date updatedOn;
	private String createdBy;	
	private String updatedBy;
	private String storageId;
	private String storageType;
	private String storageDes;
	private String storageMode;	
	private String owner;
	private String storageDescription;
	private BigDecimal volumeCft;
	private BigDecimal volumeCbm;
	private BigDecimal usedVolumeCft;
	private BigDecimal usedVolumeCbm;
	private BigDecimal availVolumeCft;
	private BigDecimal availVolumeCbm;
	private Boolean storageLocked;
	private Boolean storageAssigned;
	private String locationId;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("storageId", storageId)
				.append("storageType", storageType)
				.append("storageDes", storageDes)
				.append("storageMode", storageMode).append("owner", owner)
				.append("storageDescription", storageDescription)
				.append("volumeCft", volumeCft).append("volumeCbm", volumeCbm)
				.append("usedVolumeCft", usedVolumeCft)
				.append("usedVolumeCbm", usedVolumeCbm)
				.append("availVolumeCft", availVolumeCft)
				.append("availVolumeCbm", availVolumeCbm)
				.append("storageLocked", storageLocked)
				.append("storageAssigned", storageAssigned)
				.append("locationId", locationId).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof StorageLibrary))
			return false;
		StorageLibrary castOther = (StorageLibrary) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(storageId, castOther.storageId)
				.append(storageType, castOther.storageType)
				.append(storageDes, castOther.storageDes)
				.append(storageMode, castOther.storageMode)
				.append(owner, castOther.owner)
				.append(storageDescription, castOther.storageDescription)
				.append(volumeCft, castOther.volumeCft)
				.append(volumeCbm, castOther.volumeCbm)
				.append(usedVolumeCft, castOther.usedVolumeCft)
				.append(usedVolumeCbm, castOther.usedVolumeCbm)
				.append(availVolumeCft, castOther.availVolumeCft)
				.append(availVolumeCbm, castOther.availVolumeCbm)
				.append(storageLocked, castOther.storageLocked)
				.append(storageAssigned, castOther.storageAssigned)
				.append(locationId, castOther.locationId).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(createdOn).append(updatedOn).append(createdBy)
				.append(updatedBy).append(storageId).append(storageType)
				.append(storageDes).append(storageMode).append(owner)
				.append(storageDescription).append(volumeCft).append(volumeCbm)
				.append(usedVolumeCft).append(usedVolumeCbm)
				.append(availVolumeCft).append(availVolumeCbm)
				.append(storageLocked).append(storageAssigned)
				.append(locationId).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column(length=65)
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	
	@Column(nullable=false,length=25,unique=true)
	
	public String getStorageId() {
		return storageId;
	}
	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}
	@Column(length=65)
	public String getStorageMode() {
		return storageMode;
	}
	public void setStorageMode(String storageMode) {
		this.storageMode = storageMode;
	}
	@Column(length=255)
	public String getStorageDescription() {
		return storageDescription;
	}
	public void setStorageDescription(String storageDescription) {
		this.storageDescription = storageDescription;
	}
	@Column
	public BigDecimal getVolumeCft() {
		return volumeCft;
	}
	public void setVolumeCft(BigDecimal volumeCft) {
		this.volumeCft = volumeCft;
	}
	@Column
	public BigDecimal getVolumeCbm() {
		return volumeCbm;
	}
	public void setVolumeCbm(BigDecimal volumeCbm) {
		this.volumeCbm = volumeCbm;
	}
	@Column
	public BigDecimal getUsedVolumeCft() {
		return usedVolumeCft;
	}
	public void setUsedVolumeCft(BigDecimal usedVolumeCft) {
		this.usedVolumeCft = usedVolumeCft;
	}
	@Column
	public BigDecimal getUsedVolumeCbm() {
		return usedVolumeCbm;
	}
	public void setUsedVolumeCbm(BigDecimal usedVolumeCbm) {
		this.usedVolumeCbm = usedVolumeCbm;
	}	
	@Column
	public Boolean getStorageLocked() {
		return storageLocked;
	}
	public void setStorageLocked(Boolean storageLocked) {
		this.storageLocked = storageLocked;
	}
	@Column
	public Boolean getStorageAssigned() {
		return storageAssigned;
	}
	public void setStorageAssigned(Boolean storageAssigned) {
		this.storageAssigned = storageAssigned;
	}
	@Column
	public BigDecimal getAvailVolumeCft() {
		return availVolumeCft;
	}
	public void setAvailVolumeCft(BigDecimal availVolumeCft) {
		this.availVolumeCft = availVolumeCft;
	}
	@Column
	public BigDecimal getAvailVolumeCbm() {
		return availVolumeCbm;
	}
	public void setAvailVolumeCbm(BigDecimal availVolumeCbm) {
		this.availVolumeCbm = availVolumeCbm;
	}
	@Column(length=255)
	public String getStorageDes() {
		return storageDes;
	}
	public void setStorageDes(String storageDes) {
		this.storageDes = storageDes;
	}
	@Column(length=20)
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
    @Column(length=40)
	public String getLocationId() {
		return locationId;
	}
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
}
