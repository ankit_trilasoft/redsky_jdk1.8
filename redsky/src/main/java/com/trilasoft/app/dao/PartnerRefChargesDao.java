package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.PartnerRefCharges;

public interface PartnerRefChargesDao extends GenericDao<PartnerRefCharges, Long>  {
	public List getRefCharesList(String value, String tariffApplicability, String parameter, String countryCode, String grid, String basis, String label, String sessionCorpID);

}
