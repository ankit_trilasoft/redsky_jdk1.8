package com.trilasoft.app.service;

import java.util.Map;

public interface ExpressionManager {
	
	public Object executeExpression(String expression, Map values);
	public String executeTestExpression(String baseExpression, Map values);
	
	public String executeTestWording(String baseExpression, Map values);

}
