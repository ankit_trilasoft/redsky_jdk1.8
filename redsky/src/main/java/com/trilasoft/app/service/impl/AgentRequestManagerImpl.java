package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AgentRequestDao;
import com.trilasoft.app.model.AgentRequest;
import com.trilasoft.app.service.AgentBaseManager;
import com.trilasoft.app.service.AgentRequestManager;


public class AgentRequestManagerImpl  extends GenericManagerImpl<AgentRequest, Long> implements AgentRequestManager {
	    AgentRequestDao agentRequestdao;
		public AgentRequestManagerImpl(AgentRequestDao agentRequestdao) { 
	        super(agentRequestdao); 
	        this.agentRequestdao = agentRequestdao; 
	    } 
		public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID){
			return agentRequestdao.getCountryWithBranch(parameter,sessionCorpID);
		}
		public List getPartnerPublicList(String corpID, String lastName,
				String aliasName, String countryCode, String country,
				String stateCode, String status, Boolean isAgt,
				String counter)
		{
				return agentRequestdao.getPartnerPublicList(corpID, lastName, aliasName, countryCode, country, stateCode, status, isAgt, counter);}
		
		public List getAgentRequestDetailList(String sessionCorpID,String lastName,String aliasName,String billingCountry,String billingCity,String billingState,String status,String billingEmail, boolean isAgt,String billingZip,String billingAddress1,Long agentPartnerId)
		{
			
			
			return agentRequestdao.getAgentRequestDetailList(sessionCorpID,lastName,aliasName,billingCountry,billingCity,billingState,status,billingEmail,isAgt, billingZip,billingAddress1,agentPartnerId);
		}
		public String updateAgentRecord(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,String updatedBy,Long id,String createdBy)
		{
			return agentRequestdao.updateAgentRecord(sessionCorpID, lastName, aliasName, billingCountry, billingCity, billingState, status, billingEmail, isAgt, billingZip, billingAddress1, updatedBy, id,createdBy);
			
			
		}
		
		public String  updateAgentRejected(Long id,String updatedBy){
			return agentRequestdao.updateAgentRejected(id,updatedBy);
		}
		 public List checkById(Long id) {
			 return agentRequestdao.checkById(id);
		 
		 }
			public List getAgentRequestList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status,Boolean isAgt, Boolean isIgnoreInactive) {
			return agentRequestdao.getAgentRequestList(partnerType, corpID, lastName, aliasName, partnerCode, countryCode, country, stateCode, status, isAgt, isIgnoreInactive);
					
			}
			public String getUser(String createdBy)
			{
				return agentRequestdao.getUser(createdBy);
			}
			
			public AgentRequest getByID(Long id) {
			return agentRequestdao.getByID(id);
			
			}
			public String getUserCoprid(String createdBy)
			{
				return agentRequestdao.getUserCoprid(createdBy);
				
			}
}