<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>  
 <head>  
    <title>Orders</title>   
    <meta name="heading" content="Service Order List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
 
</style>
</head>   

<s:form id="trackingInfoListByShipNumberForm" method="post" validate="true">  

<div class="spnblk">&nbsp;</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<td align="left"  style="padding-left:5px;min-width:120px;">
		<b>Orders</b>
	</td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<s:set name="TripNumberList" value="TripNumberList" scope="request"/>
<display:table name="TripNumberList" class="table" requestURI="" id="TripNumberList" style="width:100%;" partialList="true" size="1"> 
   <display:column  title="Est Rev" style="width:190px"> 
   <c:out value="${TripNumberList.tripEstimatedRevenue}"></c:out>
   </display:column>
   <display:column  title="Weight" style="width:190px"> 
   <c:out value="${TripNumberList.tripEstimateGrossWeight}"></c:out>
   </display:column>
  </display:table>
</s:form>