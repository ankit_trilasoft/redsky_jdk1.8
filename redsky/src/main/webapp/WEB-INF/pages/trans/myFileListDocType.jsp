<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="Document Group"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>
<style>
#fc-newmnav1 a {background-position: 0 -28px;background: url(${pageContext.request.contextPath}/images/tableft-big-active-big.png);color: #FFFFFF;font:bold 12px arial,verdana; margin:-1px 0 5px;_margin:1px 0 -3px 0; height:31px;}
#fc-newmnav1 a:hover {background-position: 0 -28px;background: url(${pageContext.request.contextPath}/images/tableft-big-active-big.png);color: #FFFFFF;font:bold 12px arial,verdana; margin:-1px 0 5px;_margin:1px 0 -3px 0; height:31px;}
</style>
<script type="text/javascript">

</script>
<script>

function confirmSubmitForDocRecover(targetElement){
	var agree=confirm("Are you sure you wish to recover this file?");
	var cid = document.forms['myFileForm'].elements['customerFile.id'].value;
	var sid = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var pid="";
	pid = document.forms['myFileForm'].elements['PPID'].value;
	if(pid ==""){
	pid = document.forms['myFileForm'].elements['partner.id'].value;
	}
	var ppType = document.forms['myFileForm'].elements['ppType'].value;
	var tid = document.forms['myFileForm'].elements['truck.id'].value;
	var did = targetElement;
	if (agree){
		if(tid !='')
		{
		location.href="revocerDocByCat.html?id="+tid+"&did="+did+"&myFileFor=Truck&myFileFrom=Truck&relatedDocs=No&active=false&ppType="+ppType+"&forQuotation=${forQuotation}&PPID=${PPID}";
		}else{
	if(pid !='')
	{
	location.href="revocerDocByCat.html?id="+pid+"&did="+did+"&myFileFor=PO&myFileFrom=PO&relatedDocs=No&active=false&ppType="+ppType+"&forQuotation=${forQuotation}&PPID=${PPID}";
	}else{
		if(sid == ''){
			location.href="revocerDocByCat.html?id="+cid+"&did="+did+"&myFileFor=CF&myFileFrom=CF&relatedDocs=No&active=false&forQuotation=${forQuotation}&PPID=${PPID}";
		}else{
			location.href="revocerDocByCat.html?id="+sid+"&did="+did+"&myFileFor=SO&myFileFrom=SO&relatedDocs=No&active=false&forQuotation=${forQuotation}&PPID=${PPID}";
		}
		}
	}
	}else{
		return false;
	}
}


function performAction(targetElement,myFileFor,myFileListId,fileId, fileNameFor,secure,forQuotation,ppType,fileContentType, from){
	var action = targetElement;
	if(action=='Edit'){	
			if(myFileFor!='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&noteFor=${noteFor}&forQuotation="+forQuotation+"&docUpload=docCentre&PPID=${PPID}";
			}
			if(myFileFor=='PO'){
				location.href="editFileUpload.html?fid="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&secure="+secure+"&noteFor=${noteFor}&ppType="+ppType+"&docUpload=docCentre&PPID=${PPID}";
			}
	
			return true;		
	}else if(action=='Split'){
		var agree=confirm("Are you sure you wish to split this file?");
		if (agree){
			if(fileContentType == 'application/pdf' && myFileFor!='PO' && myFileFor!='Truck'){
	           location.href="docSplit.html?fileId="+myFileListId+"&id="+fileId+"&myFileFor="+fileNameFor+"&from=main&relatedDocs=No&secure="+secure+"&noteFor=${noteFor}&forQuotation="+forQuotation+"&docUpload=docCentre&PPID=${PPID}";
			}else{
				alert('Only pdf documents can be split to TransDoc.');
			}
		}else{
			return false;
		}
	}else if(action=='Remove'){
		if(from != 'View'){
			var agree=confirm("Are you sure you wish to remove this file?");
			if (agree){
				confirmSubmit(myFileListId);
			}else{
				return false;
			}
		}
	}else{
		return false;
	}
	
}

function confirmSubmit(targetElement){
	var cid = document.forms['myFileForm'].elements['customerFile.id'].value;
	var sid = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var pid="";
	pid = document.forms['myFileForm'].elements['PPID'].value;
	if(pid ==""){
	pid = document.forms['myFileForm'].elements['partner.id'].value;
	}
	var ppType = document.forms['myFileForm'].elements['ppType'].value;
	var tid = document.forms['myFileForm'].elements['truck.id'].value;
	var did = targetElement;
	if(tid !=''){
    	location.href="deleteDocByCat.html?id="+tid+"&did="+did+"&myFileFor=Truck&myFileFrom=Truck&relatedDocs=No&active=true&secure=false&ppType="+ppType+"&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
    }
       else{
           if(pid !=''){
	    	location.href="deleteDocByCat.html?id="+pid+"&did="+did+"&myFileFor=PO&myFileFrom=PO&relatedDocs=No&active=true&secure=false&ppType="+ppType+"&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
	    }
	    else{
			if(sid == ''){
				location.href="deleteDocByCat.html?id="+cid+"&did="+did+"&myFileFor=CF&myFileFrom=CF&relatedDocs=No&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
			}else{
				networkUnLinkedAgentSO(did, sid);
				location.href="deleteDocByCat.html?id="+sid+"&did="+did+"&myFileFor=SO&myFileFrom=SO&relatedDocs=No&active=true&secure=false&noteFor=${noteFor}&forQuotation=${forQuotation}&PPID=${PPID}";
			}
		}
       }
}

function generatePortalId() {
		var daReferrer = document.referrer; 
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to e-mail.');
		}else{
			window.open('attachMail.html?checkBoxId='+checkBoxId+'&decorator=popup&popup=true&from=file','','width=650,height=170');
		}
} 

function checkStatusId(rowId,fileType,targetElement) {	
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")	
		 targetElement.checked=false;		
	}else{		
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);	
	}	
} 

function updateInvoice(rowId,fileType,targetElement) 
 {		
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")	
		 targetElement.checked=false;		
	}else{
		var Status = targetElement.checked;
		var url="updateInvoiceAttachment.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		httpupdateInvoice.open("GET", url, true);
        httpupdateInvoice.onreadystatechange = handleHttpResponseupdateInvoice;
        httpupdateInvoice.send(null);	
	     }
 } 
function handleHttpResponseupdateInvoice(){
      if (httpupdateInvoice.readyState == 4){
           var result= httpupdateInvoice.responseText         
      }
}
function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId,fileType, targetElement) {
	//var checkboxId="checkboxId"+rowId;
	if(fileType=='Credit Card Authorization'){		 
		 alert("Secure Authorized Document ")		 		
		 targetElement.checked=false;		
	}else{
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
	}
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}
function checkStatusServiceProviderId(rowId, targetElement){
	var Status = targetElement.checked;
	var url="updateMyfileServiceProviderStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
	http22.open("GET", url, true);
    http22.onreadystatechange = handleHttpResponse4;
    http22.send(null);       
}
function handleHttpResponse4(){
 if (http22.readyState == 4){
       var result= http22.responseText         
 }
}
function checkStatusDriverId(rowId,fileType, targetElement) {
	//var checkboxId="checkboxId"+rowId;
	if(fileType=='Credit Card Authorization'){		 
		 alert("Secure Authorized Document ")		 		
		 targetElement.checked=false;		
	}else{
		var Status = targetElement.checked;
		var url="updateMyfileDiverStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
	}
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}
function checkStatusPartnerId(rowId,fileType, targetElement){
	if(fileType=='Credit Card Authorization'){
		alert("Secure Authorized Document")
		targetElement.checked=false;
	}else{
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);    
	}   
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	
var http22 = getHTTPObject22();
var httpupdateInvoice = getHTTPObject22();
function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function networkUnLinkedAgentSO(id, fid){
	var url="networkUnlinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
	http221.open("GET", url, true);
	http221.onreadystatechange = handleHttpResponse31;
	http221.send(null);			    	
}

function networkLinkedAgentSO(fid,targetElement){
	var status = targetElement.checked;
	var id = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var url="";
	if( status == true){
		var agree=confirm("Are you sure want to link this file to network partners?");
		if (agree){
	    	url="networkLinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
			http221.open("GET", url, true);
			http221.onreadystatechange = handleHttpResponse31;
			http221.send(null);	
		}else{ 
			targetElement.checked=false;
			return false;}
	   }else{
		   var agree=confirm("Are you sure want to Unlink this file to network partners?");
			if (agree){
		    	url="networkUnlinkedAgentSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(id)+"&fid=" + encodeURI(fid);
				http221.open("GET", url, true);
				http221.onreadystatechange = handleHttpResponse31;
				http221.send(null);	
			}else{ 
				targetElement.checked=true;
				return false;}
      }
}

function handleHttpResponse31(){
    if (http221.readyState == 4){
          var result= http221.responseText ;        
    }
}
function handleHttpResponse32(){
    if (http221.readyState == 4){
          var result= http222.responseText ;        
    }
}
var http221 = getHTTPObject221();
var http222 = getHTTPObject222();
function getHTTPObject221(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function getHTTPObject222(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
// 1 visible, 0 hidden   
function userStatusCheck(target){
		var targetElement = target;
		var tempValue=targetElement.value.split("`");
		var ids=tempValue[0];		
		var fileName=tempValue[1];
		var transDoc = tempValue[2];
		
		if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = ids;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
      		
      		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
      		if(userCheckFileName == ''){
	  			document.forms['myFileForm'].elements['userCheckFile'].value = fileName;
      		}else{
      			var userCheckFileName =	document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName + ',' + fileName;
      			document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName.replace( ',,' , ',' );
      		}
      		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
      		if(userCheckTransDoc == ''){
	  			document.forms['myFileForm'].elements['userTransDocStatus'].value = transDoc;
      		}else{
      			var userCheckTransDoc =	document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc + ',' + transDoc;
      			document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc.replace( ',,' , ',' );
      		}
      		
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		userCheckStatus = userCheckStatus.replace( ids , '' );
     		userCheckStatus = userCheckStatus.replace( ',,' , ',' );
     		var len = userCheckStatus.length-1;
     	    if(len==userCheckStatus.lastIndexOf(",")){
     	    	userCheckStatus=userCheckStatus.substring(0,len);
     	      }
     	      if(userCheckStatus.indexOf(",")==0){
     	    	 userCheckStatus=userCheckStatus.substring(1,userCheckStatus.length);
     	      }
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus;
     		
     		
     		var userCheckFileName = document.forms['myFileForm'].elements['userCheckFile'].value;
     		userCheckFileName = userCheckFileName.replace( fileName , '' );
     		userCheckFileName = userCheckFileName.replace( ',,' , ',' );
     		len = userCheckFileName.length-1;
     	    if(len==userCheckFileName.lastIndexOf(",")){
     	    	userCheckFileName=userCheckFileName.substring(0,len);
     	      }
     	      if(userCheckFileName.indexOf(",")==0){
     	    	 userCheckFileName=userCheckFileName.substring(1,userCheckFileName.length);
     	      }
     		document.forms['myFileForm'].elements['userCheckFile'].value = userCheckFileName;

     		
     		var userCheckTransDoc = document.forms['myFileForm'].elements['userTransDocStatus'].value;
     		userCheckTransDoc = userCheckTransDoc.replace( transDoc , '' );
     		userCheckTransDoc = userCheckTransDoc.replace( ',,' , ',' );
     		var len = userCheckTransDoc.length-1;
     	    if(len==userCheckTransDoc.lastIndexOf(",")){
     	    	userCheckTransDoc=userCheckTransDoc.substring(0,len);
     	      }
     	      if(userCheckTransDoc.indexOf(",")==0){
     	    	 userCheckTransDoc=userCheckTransDoc.substring(1,userCheckTransDoc.length);
     	      }
     		document.forms['myFileForm'].elements['userTransDocStatus'].value = userCheckTransDoc;
     		
   		}
  	 	collectFileId();
   	}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var partnerCode = document.forms['myFileForm'].elements['fileNameFor'].value;
		if(partnerCode=='Truck'){
			var seqNo = document.forms['myFileForm'].elements['truck.localNumber'].value;
			}else{
				if(partnerCode=='PO'){
		var seqNo = document.forms['myFileForm'].elements['partner.partnerCode'].value;
		}else{
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		}}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
		
} 
function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheckFile'].value = "";
	document.forms['myFileForm'].elements['userTransDocStatus'].value = "";
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	collectFileId();
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
	document.forms['myFileForm'].elements['userCheckFile'].value="";
	document.forms['myFileForm'].elements['userTransDocStatus'].value = "";
	collectFileId();
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}


function conbinedDoc(){
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
	var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	if(seqNo == '' ){
		seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
	}
	
	var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
	name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
	
	if(checkBoxFile != ''){
		checkBoxFile = checkBoxFile.trim();
		if (checkBoxFile.indexOf(",") == 0) {
			checkBoxFile = checkBoxFile.substring(1);
		}
		if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
			checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
		}
		
		var arrayid = checkBoxFile.split(",");
		var arrayLength = arrayid.length;
		
		for (var i = 0; i < arrayLength; i++) {
			var fName = arrayid[i];
			if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf'  && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
				alert('Please select only PDF Files.');
				return false;
			}
		}
	}
	
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select one or more document to merge.');
	}else{
		var url = 'conbinedDoc.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
		location.href=url;
	}
}

function conbinedDocInPdf(){
    var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
       var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
       var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
       if(seqNo == '' ){
           seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
       }
       
       var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
       name += ' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
       
       if(checkBoxFile != ''){
           checkBoxFile = checkBoxFile.trim();
           if (checkBoxFile.indexOf(",") == 0) {
               checkBoxFile = checkBoxFile.substring(1);
           }
           if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
               checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
           }
           
           var arrayid = checkBoxFile.split(",");
           var arrayLength = arrayid.length;
       }
       
       if(checkBoxId =='' || checkBoxId ==','){
           alert('Please select one or more document to convert in PDF file.');
       }else{
           var url = 'conbinedDocInPdf.html?rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name;
           location.href=url;
       }
   
}

function transDoc(){
	var id = document.forms['myFileForm'].elements['fileId'].value;
	var fileNameFor = document.forms['myFileForm'].elements['fileNameFor'].value;
	var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
	var checkBoxFile = document.forms['myFileForm'].elements['userCheckFile'].value;
	var transDocStatus = document.forms['myFileForm'].elements['userTransDocStatus'].value;
	var transDocArray = transDocStatus.split(',');
	var count=0;
	for(var i=0;i<transDocArray.length ; i++)
	{
		if(transDocArray[i]=='UPLOADED'){
			count++;
		}
	}
	if(count>0){
		var agree = confirm("You have chosen to upload files that were previously sent. Do you wish to upload them again?");
		if(agree){
			}else{return false;}
	}
	var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	if(seqNo == '' ){
		seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
	}
	
	var name = document.forms['myFileForm'].elements['customerFile.firstName'].value;
	name +=	' '+document.forms['myFileForm'].elements['customerFile.lastName'].value;
	name=name.trim();
	name = name.replace("%","&#37;");
	if(checkBoxFile != ''){
		checkBoxFile = checkBoxFile.trim();
		if (checkBoxFile.indexOf(",") == 0) {
			checkBoxFile = checkBoxFile.substring(1);
		}
		if (checkBoxFile.lastIndexOf(",") == checkBoxFile.length - 1) {
			checkBoxFile = checkBoxFile.substring(0, checkBoxFile.length - 1);
		}
		
		var arrayid = checkBoxFile.split(",");
		var arrayLength = arrayid.length;
		for (var i = 0; i < arrayLength; i++) {
			var fName = arrayid[i];
			if(fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'pdf' && fName.substring(fName.lastIndexOf(".")+1, fName.length) != 'PDF'){
				alert('Only pdf documents can be transferred to TransDoc.');
				return false;
			}
		}
	}
	if(checkBoxId =='' || checkBoxId ==','){
		alert('Please select document to transfer.');
	}else{
		var url = 'docTransfer.html?id='+id+'&myFileFor='+fileNameFor+'&rId='+checkBoxId+'&seqNum='+seqNo+'&name='+name+'&docUpload=docCentre';
		location.href=url;
	}
}
function disabledAll(){
	var elementsLen=document.forms['myFileForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
		if(document.forms['myFileForm'].elements[i].type=='text'){
			document.forms['myFileForm'].elements[i].readOnly =true;
			document.forms['myFileForm'].elements[i].className = 'input-textUpper';
			document.forms['myFileForm'].elements[i].onkeydown ="";
		}else if(document.forms['myFileForm'].elements[i].type=='textarea'){
			document.forms['myFileForm'].elements[i].readOnly =true;
			document.forms['myFileForm'].elements[i].className = 'textareaUpper';
		}else{
			document.forms['myFileForm'].elements[i].disabled=true;
		} 
	}
}
function getfilterbycategory(target)
{
	var id = document.forms['myFileForm'].elements['fileId'].value;
	document.forms['myFileForm'].action="categoryResult.html?id="+id+"&myFileFor=${myFileFor}&noteFor=${noteFor}&categorySearchValue="+target+"&active=true&secure=false";
	document.forms['myFileForm'].submit();
}

function showHideMyDiv(targetDiv,docName){
	//$("table.table-fc tr[id='div_']").toggle();
	$("table.table-fc tr[id='"+targetDiv+"']").toggle();
	var plusImgID = 'plus_'+docName;
	var minusImgID = 'minus_'+docName;
	var plusImgObj = document.getElementById(plusImgID);
	var minusImgObj = document.getElementById(minusImgID);
	$(plusImgObj).toggle();
	$(minusImgObj).toggle();
}


</script>

   
<style>  
#header-fixed {  position: fixed;    top: 0px; display:none; background: none repeat scroll 0 0 #FFFFFF; border: 2px solid #cfcfcf; border-collapse: collapse;
    color: #000000; width: 100%; font-size:1.1em; margin-bottom:5px;}
.table-fc td, .table th, .tableHeaderTable td {padding:0.3em;font-size:11px;}
#mainPopup {padding-left:10px;padding-right:10px;}
span.pagelinks {display:block;font-size:0.95em;margin-bottom:5px;margin-top:-18px;padding:2px 0;text-align:right;width:100%;}
.table-fc td, .table th, .tableHeaderTable td {padding:0.2em;}
</style>
</head>

<s:form id="myFileForm" action="searchMyFiles" method="post" > 
<c:set var="soCoordinator" value="${fn:trim(serviceOrder.coordinator)}" />
<c:set var="appUserName" value="${fn:trim(user1.username)}" />
<c:set var="soCoordinator" value="${fn:toUpperCase(soCoordinator)}" />
<c:set var="appUserName" value="${fn:toUpperCase(appUserName)}" />
<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
<c:set var="appUserCorpid" value="${fn:trim(user1.corpID)}" />
<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
<c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
<s:hidden name="noteForTemp" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteForTemp" value="<%=request.getParameter("noteFor") %>"/>
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<c:set var="idOfTasks" value="" scope="session"/>
<c:set var="tableName" value="" scope="session"/>
<s:hidden name="userCheck"/>
<s:hidden name="myFileIdCheck" value=""/>  
<c:set var="myFileStatusCheck" value="" />
<s:hidden name="userCheckFile"/>
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="partner.id" />
<s:hidden name="truck.id" />
<s:hidden name="truck.localNumber" />
<s:hidden name="partner.partnerCode" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden id="forQuotation" name="forQuotation" />
<s:hidden name="myFileForVal" value="<%=request.getParameter("myFileFor")%>" />
<s:hidden name="noteForVal" value="<%=request.getParameter("noteFor")%>" />
<s:hidden name="activeVal" value="<%=request.getParameter("active")%>" />
<s:hidden name="secureVal" value="<%=request.getParameter("secure")%>" />
<s:hidden name="userTransDocStatus" />
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:if test="${myFileFor =='CF'}">
	 <c:set var="idOfWhom" value="" scope="session"/>
	<c:set var="noteID" value="" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="" scope="session"/> 
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="buttons"> 
<c:if test="${ppType ==''}">  
	<input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&forQuotation=${forQuotation}&docUpload=docCentre"/>'" value="<fmt:message key="button.upload"/>"/>
</c:if>
	<c:if test="${ppType !=''}"> 
	 <input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=${secure}&ppType=${ppType}&forQuotation=${forQuotation}&PPID=${PPID}&docUpload=docCentre"/>'" value="<fmt:message key="button.upload"/>"/>
	</c:if>
</c:set> 
<c:if test="${myFileFor=='PO'}"> 
<c:set var="isTrue" value="true" scope="request"/>
<c:set var="from" value="<%=request.getParameter("from") %>"/>
<s:hidden name="from"  id="from" value="<%=request.getParameter("from") %>" />
</c:if>
<c:if test="${myFileFor!='CF' && myFileFor!='PO'}"> 
<s:hidden name="customerFile.firstName" />
<s:hidden name="customerFile.lastName" />
<s:hidden name="customerFile.sequenceNumber" />
<c:set var="idOfWhom" value="" scope="session"/>
<c:set var="noteID" value="" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${noteFor=='WorkTicket' }">
<c:set var="noteFor" value="" /> 
</c:if>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
<div id="layer6" style="width:100%; ">

<div id="newmnav" style="float:left; ">
    <c:choose>
	<c:when test="${forQuotation!='QC'}">
            <ul>
				<s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
             
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	            <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
	            	</c:if>
	            <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
	            	</c:if>	
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		             <c:if test="${serviceOrder.moveType!='Quote'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
							 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				    </c:if> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${serviceOrder.moveType!='Quote'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
							 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				    </c:if> 
			     </sec-auth:authComponent>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
			     
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     	<c:if test="${serviceOrder.job !='RLO'}">
			     	<c:if test="${forwardingTabVal!='Y'}">
			     	
			       <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}">  
			     		<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			     		</c:if>
			     	</c:if>
			     	<c:if test="${forwardingTabVal=='Y'}">
			     	  <c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
			     		<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			     		</c:if>
			     	</c:if>
	               </c:if>
	               </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	              <c:if test="${userType!='DRIVER'}">
	              <c:if test="${serviceOrder.job =='RLO'}">  
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>
	             </sec-auth:authComponent>
	              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             	<c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             	</c:if>
	             	</c:if>
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	           	  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ userType=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		     <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
	 	<c:if test="${ userType=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
	  	</c:if>
	       </ul>
	       </c:when>
	      <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	         <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         <li><a href="operationResource.html?id=${serviceOrder.id}&quoteFlag=y"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	          </c:if>
		     <c:if test="${voxmeIntergartionFlag=='true'}">             
		 <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 
		    <c:if test="${userType=='USER'}">
		<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
	  	    </c:if>
  	      </ul>
		</c:otherwise></c:choose>
</div>
    <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
 <c:if test="${userType!='DRIVER'}">
<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
</c:if>
</div>
</div>
</c:if>
 </c:if>
 <c:if test="${noteForTemp!='' && noteForTemp!='WorkTicket' }">
    <c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>" scope="session"/>
	<c:if test="${noteFor=='WorkTicket' }">
     <c:set var="noteFor" value="" /> 
    </c:if>
   </c:if>
 <c:if test="${userType!='DRIVER'}">
 <c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:95%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		  <ul>
		    <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li> 
		    </c:if>
		     <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=customerFile&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		    </c:if> 
		  	 <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		    	<c:if test="${voxmeIntergartionFlag=='true'}">
		    		<li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    	</c:if>
		    	<c:if test="${not empty customerFile.id && mmValidation =='Yes'}"> 
		    		<li><a href="inventoryDetailsForm.html?cid=${customerFile.id}"><span>Inventory Details</span></a></li>
		    	</c:if>
		    </sec-auth:authComponent>
		      <sec-auth:authComponent componentId="module.tab.customerFile.serviceOrderTab">
		    <c:if test="${ userType=='AGENT' && surveyTab}">
		  		 <li><a href="surveyDetails.html?cid=${customerFile.id}"><span>Survey Details</span></a></li>
		    </c:if>
		     </sec-auth:authComponent>
		     <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
	 		<c:if test="${userType=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameCF.html?cid=${customerFile.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
	  	    </c:if>
		  </ul>
		</div>
	 </c:when>
	 <c:otherwise>
		<div id="newmnav">
		  <ul>
		    <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}&quoteFlag=y"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 
		  	 <c:if test="${voxmeIntergartionFlag=='true'}">             
		  <li><a href='surveyDetails.html?cid=${customerFile.id}&Quote=y'><span>Survey Details</span></a></li>              
             </c:if>
             <c:if test="${userType=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameCF.html?cid=${customerFile.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
	  	     </c:if>
		  </ul>
		</div>
		</c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:90%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>
 
</c:if>
<div id="Layer1" style="width:100%;">
		<div id="fc-newmnav" style="width:100%;" >
			<ul>
			    <c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">	
				<%-- <li ><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
				  <c:if test="${userType!='DRIVER'}">
				<%-- <li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&noteFor=${noteFor}&active=true&secure=${secure}&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
				<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&secure=${secure}&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li> --%>
				</c:if>
				<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
					<%-- <li><a href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List</span></a></li> --%>
				</sec-auth:authComponent>
				<li id="fc-newmnav1" style="background:#FFF " ><a class="current"  href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				<%--  <font style="padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: 600; ">Filter By Category: </font>
				<s:select cssClass="list-menu" id="entity" name="filterbycategory" headerKey="" headerValue="All" list="%{categoryList}" onchange="getfilterbycategory(this.value)"/>
				--%>
				<input type="button" class="cssbutton" style="margin-right: 5px; margin-left: 30px;height: 28px;width:120px; font-size: 15" onclick="location.href='<c:url value="relatedFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=${secure}&forQuotation=${forQuotation}"/>'" value="Display All SO"/>
				<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:120px; font-size: 15" onclick="location.href='<c:url value="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"/>'" value="Display SO Category"/> --%>
				<%-- <font style="padding-left:5px;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: 600; cursor:pointer " id="collapseall" >Collapse All</font> --%>
				<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:80px;" id="collapseall" value="Collapse All" />
				 <c:if test="${userType!='DRIVER'}">
				<li id="fc-newmnav1" style="background:#FFF " ><a href="checkListDocFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Check List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				</c:if>
				</c:if>
				<c:if test="${myFileFor=='Truck'}">	
				<li id="fc-newmnav1" style="background:#FFF " ><a class="current"  href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<%-- <li id="fc-newmnav1" style="background:#FFF "><a class="current"  href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=${secure}&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
					<li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&secure=${secure}&ppType=${ppType}"><span>Waste Basket</span></a></li> --%>
					<li><a href="editTruck.html?id=${fileId}" ><span>Truck Details</span></a></li>
				</c:if>
				<c:if test="${myFileFor=='PO'}">
				<li id="fc-newmnav1" style="background:#FFF " ><a class="current"  href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>	
					<%-- <li id="fc-newmnav1" style="background:#FFF "><a class="current" href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=${secure}&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
					<c:if test="${from != 'View'}">
						<%-- <li><a href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&secure=${secure}&ppType=${ppType}"><span>Waste Basket</span></a></li> --%>
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
						</c:if>
					</c:if>
					<c:if test="${from == 'View'}">
						<c:if test="${!param.popup && ppType=='AG'}"> 
							<li><a href="findPartnerProfileList.html?from=view&code=${ppCode}&partnerType=${ppType}&id=${fileId}"><span>Agent Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='AC'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='PP'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='CR'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='VN'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
						</c:if>
						<c:if test="${!param.popup && ppType=='OO'}"> 
							<li><a href="viewPartner.html?id=${fileId}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
						</c:if>
					</c:if>
				</c:if>
			</ul>
			
		</div>
		<sec-auth:authComponent componentId="module.table.partner.docList.Admin">
			<div id="chkAllButton"  class="listwhitetext" style="display:none;" >
				<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
				<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
			</div>
		</sec-auth:authComponent>
	<%-- New code for table  --%>	
<!-- <table id="header-fixed"></table>	 -->
<div class="spn"></div>
<table style="width:100%">
<tr>
<td>
<table id="header-fixed" style="width:94.9% " ></table>	
</td>
</tr>
<tr><td>		
<table class="table-fc" style="width:100%;margin-top:-3px; " id="table-File" >
<thead>
<tr style="height:25px">
		<td rowspan="2" class="newtablehead" style="width:0.1%">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td rowspan="2" class="newtablehead" style="width:0.5%">#&nbsp;&nbsp;</td>
		
				<td rowspan="2" class="newtablehead" style="width:1%">Document&nbsp;Type&nbsp;&nbsp;</td>
		<td rowspan="2" class="newtablehead" style="width:10%">Description&nbsp;&nbsp;</td>
		<td rowspan="2" class="newtablehead" style="width:7%;text-align:center;">Actions</td> 
		<td rowspan="2" class="newtablehead" style="width:1%">Size&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		<td rowspan="2" class="newtablehead" style="width:3%">Uploaded&nbsp;On&nbsp;&nbsp;</td>
		
		<td rowspan="2" class="newtablehead" style="width:3%">Uploaded&nbsp;By</td>
		<c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
		<td style="padding-left: 1px; width: 7.5%;" class="newtablehead" rowspan="2">Trans Doc Status</td>
		</c:if>
		<td style="padding-left: 0.01em; padding-right: 0.001em; width: 5.5%;" class="newtablehead" rowspan="2">Email Status</td>
		<c:if test="${fn1:indexOf(ediBillTo,serviceOrder.billToCode)>=0}">
		<td style="padding-left: 0.01em; padding-right: 0.001em; width: 5.5%;" class="newtablehead" rowspan="2">Edi Status</td>
		</c:if>
		<td rowspan="2" class="newtablehead" style="width:3%">Invoice Attachment</td>
		<td colspan="3" class="newtablehead" style="text-align: center; width: 16%;">Portal Access</td>
		
		<c:if test="${customerFile.controlFlag == 'C'}">
			<td colspan="6" class="newtablehead" style="text-align:center;width:28.6%">Network Access Control</td>
		</c:if>
		<c:if test="${myFileFor=='PO'}">
		<td  rowspan="2" colspan="6" class="newtablehead" style="text-align:center;width:28.6%">Service&nbsp;Provider</td>
		</c:if>
	</tr>
	<tr style="height:25px">	
	  <configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">	
		<td style="width:41px;" class="newtabletd_orange">Cust</td>
		</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
		<td style="width:41px;" class="newtabletd_orange">Account</td>
		</configByCorp:fieldVisibility>
		<td style="width:41px;" class="newtabletd_orange" style="">Partner</td>
	<configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
			<td style="width:41px;" class="newtabletd_orange" style="">Driver</td>
			</configByCorp:fieldVisibility>
		<c:if test="${customerFile.controlFlag == 'C'}">
			<td width="2" class="newtabletd_blue" style="padding-left:0.01em">BA&nbsp;&nbsp;</td>
			<td width="2" class="newtabletd_blue" style="padding-left:0.15em">NA&nbsp;&nbsp;</td>
			<td width="2" class="newtabletd_blue" style="padding-left: 0.5em;">OA</td>
			<td width="2" class="newtabletd_blue" style="padding-left: 0.01em;">SOA</td>
			<td width="2" class="newtabletd_blue" style="padding-left: 0.8em;">DA</td>
			<td width="2" class="newtabletd_blue" style="padding-left: 0.1em;">SDA</td>
		</c:if>
		
	</tr>
	
	</thead>
	<tbody>
	
		<c:forEach var="doc" items="${docTypeFiles}">
		  	<tr >
		  	<c:set var="showValueforSecure" value='0'/>
		  	<c:set var="padding" value=''/>
		  	<c:if test ="${doc.key eq 'ZZ-Secure Doc' && appUserName ne soCoordinator}">
		  	<c:set var="padding" value='!padding-top:15px;'/>
		  	</c:if>
		  		<td colspan="20" style="padding-top:2px;${padding} padding-bottom:0px">
		  			<div onClick="showHideMyDiv('div_${doc.key}','${doc.key}')" style="margin: 0px;"> 
					<div  style="float:left; padding-top:2px" class="headtab_leftFc">
						<img id="plus_${doc.key}" src="${pageContext.request.contextPath}/images/plus-small.png" style="padding-left:2px;display: none;padding-left:5px;"  />
						<img id="minus_${doc.key}" src="${pageContext.request.contextPath}/images/minus-small.png" style="padding-left:8px;padding-top:3px;" />
						</div>
						<div style="float:left;width:40%;line-height:21px;" class="headtab_center" >
						<c:set var="validPermission" value='No'/>
							<c:if test ="${doc.key ne 'ZZ-Secure Doc' && doc.key ne 'ZZ-Waste Basket'}">
								${doc.key}
								<c:set var="validPermission" value='Yes'/>
							</c:if>
							<c:if test="${doc.key=='ZZ-Secure Doc' }"> 
							
								<c:out value="Secure Doc"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/secure-blue.png" style="width: 14px; height: 14px; vertical-align: text-top;margin-top:-1px;" />
								<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
									<c:set var="validPermission" value='Yes'/>
								</sec-auth:authComponent>
							</c:if>
							<c:if test="${doc.key=='ZZ-Waste Basket' }"> 
								<c:out value="Waste Basket"/>&nbsp;&nbsp;<img src="${pageContext.request.contextPath}/images/basket-blue.png" style="width: 14px; height: 14px; vertical-align: text-top;margin-top:-1px;" />
								<c:set var="validPermission" value='Yes'/>
							</c:if>
						</div>
					 <div  class="headtab_bg" style="float:left"></div>
					  <div   class="headtab_bg_special" style="float:left;width:53.5%;"></div>
					   <div   class="headtab_right" style="float:left"></div>
					</div>
					  		</td>
		 	</tr>
		 	<c:if test="${validPermission =='Yes'}">
			 	<c:forEach var="myFileList" items="${doc.value}">
			 	 <c:if test="${myFileStatusCheck == ''}"> 
			 	 <c:set var="myFileStatusCheck" value="${myFileList.id}^${myFileList.emailStatus}" /> 
			 	 </c:if> 
			 	 <c:if test="${myFileStatusCheck != ''}">  
				 <c:set var="myFileStatusCheck" value="${myFileStatusCheck}^${myFileList.id}:${myFileList.emailStatus}" />
			 	 </c:if>  
			 			 <tr id ="div_${doc.key}">
				 			<td>
				 		 		<c:if test="${from != 'View'}">
				 		 		<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:choose>
									<c:when test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
				 		 			<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
				 		 				<input type="checkbox" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/>
				 		 			</c:if>
				 		 			</c:when>
				 		 			<c:when test="${appUserCorpid =='UNIP'}">
				 		 			<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
				 		 				<input type="checkbox" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/>
				 		 			</c:if>
				 		 			</c:when>
				 		 			<c:otherwise>
				 		 			<c:if test="${showValueforSecure=='0'}">
				 		 			<c:set var="showValueforSecure" value='1'/>
				 		 			<div style="display: inline-block; position: absolute; background:#fff; width: 100%; height: 13px; font-size: 12px;">You&nbsp;are&nbsp;not&nbsp;authorize&nbsp;to&nbsp;view&nbsp;Details.</div>
				 		 			</c:if>
				 		 			</c:otherwise>
				 		 			</c:choose>
				 		 			</c:when>
				 		 			<c:otherwise>
				 		 			<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
				 		 				<input type="checkbox" id="checkboxId" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/>
				 		 			</c:if>
				 		 			</c:otherwise>
				 		 			</c:choose>
				 		 			<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
				 		 				<input type="checkbox" id="checkboxId" disabled="disabled" name="DD" value="${myFileList.id}`${myFileList.fileFileName}`${myFileList.transDocStatus}" onclick="userStatusCheck(this)"/>
				 		 			</c:if>
									
								</c:if>
							</td>
							<td>
								<c:set var="orNum" value="${fn:substring(myFileList.fileId,11,14)}" />
								<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
											<c:if test="${orNum == ''}"> <c:out value="00"/> </c:if>
											<c:if test="${orNum != ''}"> <c:out value="${orNum}"/> </c:if>
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
											<c:if test="${orNum == ''}"> <c:out value="00"/> </c:if>
											<c:if test="${orNum != ''}"> <c:out value="${orNum}"/> </c:if>
										</c:if>
									</c:when>
									<c:otherwise>
									<c:if test="${orNum == ''}"> <c:out value="00"/> </c:if>
									<c:if test="${orNum != ''}"> <c:out value="${orNum}"/> </c:if>
									</c:otherwise>
								</c:choose>
										
							</td>
							<td>
							<c:choose>
								<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
										<c:out value="${myFileList.fileType }" />
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
										<c:out value="${myFileList.fileType }" />
									</c:if>
								</c:when>
								<c:otherwise>
									<c:out value="${myFileList.fileType }" />
								</c:otherwise>
							</c:choose>
							</td>
							<td>
							<c:choose>
								<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
										<a onclick="downloadSelectedFile('${myFileList.id}');">
											<c:out value="${myFileList.description}" escapeXml="false"/>
										</a>
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
										<a onclick="downloadSelectedFile('${myFileList.id}');">
											<c:out value="${myFileList.description}" escapeXml="false"/>
										</a>
									</c:if>
								</c:when>
								<c:otherwise>
									<a onclick="downloadSelectedFile('${myFileList.id}');">
									<c:out value="${myFileList.description}" escapeXml="false"/>
									</a>
								</c:otherwise>
							</c:choose>
							</td>
							<td style="text-align:center">
							<c:choose>
								<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator  && appUserCorpid !='UNIP'}">
							
									<c:if test="${doc.key ne 'ZZ-Waste Basket' }"> 
										<c:if test="${myFileList.accountLinePayingStatus eq 'New'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/small-astric.gif" style="vertical-align: middle;" alt="Paying Status" title="Paying Status - New" /> </b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Approved'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/A.gif" style="vertical-align: text-bottom;" alt="Paying Status" title="Paying Status - Approved" /></b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Pending with note' }">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/ques-tiny.gif" style="vertical-align: inherit;" alt="Paying Status" title="Paying Status - Pending with note" /></b>
										</c:if>
										<img src="${pageContext.request.contextPath}/images/edit.png"  alt="Edit" title="Edit" onclick="return performAction('Edit', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" `/>
										<img src="${pageContext.request.contextPath}/images/split.png" alt="Split" title="Split" onclick="return performAction('Split', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										<img src="${pageContext.request.contextPath}/images/delete.png" alt="Waste Basket" title="Waste Basket" onclick="return performAction('Remove', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										
									</c:if>
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
							
									<c:if test="${doc.key ne 'ZZ-Waste Basket' }"> 
										<c:if test="${myFileList.accountLinePayingStatus eq 'New'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/small-astric.gif" style="vertical-align: middle;" alt="Paying Status" title="Paying Status - New" /> </b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Approved'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/A.gif" style="vertical-align: text-bottom;" alt="Paying Status" title="Paying Status - Approved" /></b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Pending with note' }">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/ques-tiny.gif" style="vertical-align: inherit;" alt="Paying Status" title="Paying Status - Pending with note" /></b>
										</c:if>
										<img src="${pageContext.request.contextPath}/images/edit.png"  alt="Edit" title="Edit" onclick="return performAction('Edit', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" `/>
										<img src="${pageContext.request.contextPath}/images/split.png" alt="Split" title="Split" onclick="return performAction('Split', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										<img src="${pageContext.request.contextPath}/images/delete.png" alt="Waste Basket" title="Waste Basket" onclick="return performAction('Remove', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										
									</c:if>
									</c:if>
								</c:when>
									<c:otherwise>
									<c:if test="${doc.key ne 'ZZ-Waste Basket' }"> 
										<c:if test="${myFileList.accountLinePayingStatus eq 'New'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/small-astric.gif" style="vertical-align: middle;" alt="Paying Status" title="Paying Status - New" /> </b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Approved'}">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/A.gif" style="vertical-align: text-bottom;" alt="Paying Status" title="Paying Status - Approved" /></b>
										</c:if>
										<c:if test="${myFileList.accountLinePayingStatus eq 'Pending with note' }">
											<b style="font-size:13px;vertical-align:top;color:#444;"><img src="${pageContext.request.contextPath}/images/ques-tiny.gif" style="vertical-align: inherit;" alt="Paying Status" title="Paying Status - Pending with note" /></b>
										</c:if>
										<img src="${pageContext.request.contextPath}/images/edit.png"  alt="Edit" title="Edit" onclick="return performAction('Edit', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" `/>
										<img src="${pageContext.request.contextPath}/images/split.png" alt="Split" title="Split" onclick="return performAction('Split', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										<img src="${pageContext.request.contextPath}/images/delete.png" alt="Waste Basket" title="Waste Basket" onclick="return performAction('Remove', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										
									</c:if>
									</c:otherwise>
									</c:choose>
									<c:if test="${doc.key=='ZZ-Waste Basket' }"> 
										<img alt="Recover" title="Recover" onclick="confirmSubmitForDocRecover(${myFileList.id});" style="margin: 0px 0px 0px 8px;" src="images/recover.png"/>
									</c:if>
									<%-- <s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" /> --%>
								</td>
							<td>
							<c:choose>
								<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
										<c:out value="${myFileList.fileSize }" />
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
										<c:out value="${myFileList.fileSize }" />
									</c:if>
								</c:when>
								<c:otherwise>
									<c:out value="${myFileList.fileSize }" />
								</c:otherwise>
								</c:choose>
							</td>
							 <c:if test="${from != 'View'}">
								<td>
								<c:choose>
								<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
										<fmt:formatDate pattern="dd-MMM-yyyy" value="${myFileList.createdOn }" />
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
										<fmt:formatDate pattern="dd-MMM-yyyy" value="${myFileList.createdOn }" />
									</c:if>
								</c:when>
								<c:otherwise>
									<fmt:formatDate pattern="dd-MMM-yyyy" value="${myFileList.createdOn }" />
								</c:otherwise>
								</c:choose>
								</td>
								<td>
								<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
											<c:out value="${myFileList.createdBy }" />
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
											<c:out value="${myFileList.createdBy }" />
										</c:if>
									</c:when>
									<c:otherwise>
										<c:out value="${myFileList.createdBy }" />
									</c:otherwise>
									</c:choose>
								</td>
								<c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
									<td>
								    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
								    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
								    	</c:if>
								    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
								    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
								    	</c:if>
								    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
								    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
								    	</c:if>
								    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
								    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
								    	</c:if>
								    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
								    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
								    	</c:if>
								    </td>
								</c:if>	
								
								<td>
									<c:url value="openMailView.html" var="url1" >
										<c:param name="decorator" value="popup"/>
										<c:param name="popup" value="true"/>
										<c:param name="emailStatusVal" value="${myFileList.emailStatus}"/>
									</c:url>
									<c:if test="${myFileList.emailStatus==null || myFileList.emailStatus=='' || myFileList.emailStatus==' '}">
									</c:if>						
									<c:if test="${myFileList.emailStatus!=null && myFileList.emailStatus!=''}">
										<c:forEach var="entry" items="${emailStatusList}">
											<c:if test="${myFileList.emailStatus==entry.key}">
												<c:set var="str" value="${entry.value}" />
											</c:if>
										</c:forEach>
									    <c:forEach var="num1" items="${fn:split(str, '^')}" varStatus="count">
									    	<c:choose> 
												<c:when test="${count.last}">
			 										<c:set var="strqq1" value="${num1}" />
			 									</c:when>
												<c:otherwise>
													<c:set var="strqq2" value="${num1}" />
												</c:otherwise>			
											</c:choose>			    
							            </c:forEach>	
							            <c:choose> 
							            	<c:when test="${strqq2 < 3}">
			     				        		<c:set var="strqq" value="Ready For Sending" />
							           			 <c:set var="emailSt" value="pendingState" />
							            	</c:when>
							            	<c:when test="${strqq2 > 2 && fn:indexOf(strqq1,'SaveForEmail')>-1}">
							            		<c:set var="ste" value="${fn:replace(strqq1,'SaveForEmail','')}"/>
							             		<c:set var="strqq" value="Email sending has failed ${strqq2} times,${ste}" />
							             		<c:set var="emailSt" value="failed" />
							            	</c:when>
							            	<c:otherwise>
							           		</c:otherwise>
							            </c:choose>
										<c:set var="str1" value="SaveForEmail"/>
										<c:if test="${fn:indexOf(str,str1)>-1}">
											 <c:if test="${emailSt=='pendingState'}">
											 <c:choose>
												<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
													<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												 		<img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
												 	</c:if>
												 	<c:if test="${appUserCorpid =='UNIP'}">
												 		<img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
												 	</c:if>
											 </c:when>
											 <c:otherwise>
											 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/ques-small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
											 </c:otherwise>
											 </c:choose>
											 </c:if>
											 <c:if test="${emailSt!='pendingState'}">
											 <c:choose>
											<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
												<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
											 		<img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
											 	</c:if>
											 	<c:if test="${appUserCorpid =='UNIP'}">
											 		<img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
											 	</c:if>
											 </c:when>
											 <c:otherwise>
											 <img src="<c:url value='/images/email_small.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/cancel001.gif'/>" title="${strqq}" onclick="javascript:openWindow('${url1}',800,600);"/>
											 </c:otherwise>
											 </c:choose>
										 </c:if>
										 </c:if>
										 <c:if test="${fn:indexOf(str,str1)<0}">
										 <c:choose>
											<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
												<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
										 			<img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>
										    		<%-- <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this);"/> --%>
										    	</c:if>
										    	<c:if test="${appUserCorpid =='UNIP'}">
										 			<img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>
										    	</c:if> 
										    </c:when>
										    <c:otherwise>
										    <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>
										    		<%-- <img src="<c:url value='/images/email_small.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/tick01.gif'/>" title="${strqq1}" onclick="javascript:openWindow('${url1}',800,600);"/>&nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this);"/> --%>
										    </c:otherwise>
										    </c:choose>
										 </c:if>
									</c:if>
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
								 				&nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/>
								 			</c:if>
								 			<c:if test="${appUserCorpid =='UNIP'}">
								 				&nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/>
								 			</c:if>
								 		</c:when>
								 		<c:otherwise>
								 			&nbsp;<img src="<c:url value='/images/user-cabinet.png'/>" title="Email Status"  onclick="recipientWithEmailStatus(this,'${myFileList.id}');"/>
								 		</c:otherwise>
								 	</c:choose> 	
								 </td>
								 <c:if test="${fn1:indexOf(ediBillTo,serviceOrder.billToCode)>=0}">
								 <td>
								             <c:if test="${myFileList.docSent !=null}">
								 				&nbsp;<img src="<c:url value='/images/email_small.gif'/>" title="Sent to the state department" />
								 			</c:if>
								 </td>
								 </c:if>
								 <td style="text-align:center" class="newtabletd_orange">
								 <c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
									<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
							 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
										<c:if test="${myFileList.invoiceAttachment == true || myFileList.invoiceAttachment=='true'}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.invoiceAttachment == false || myFileList.invoiceAttachment == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
									</c:if>
									<c:if test="${appUserCorpid =='UNIP'}">
							 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
										<c:if test="${myFileList.invoiceAttachment == true || myFileList.invoiceAttachment=='true'}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.invoiceAttachment == false || myFileList.invoiceAttachment == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
									</c:if>
									</c:when>
									<c:otherwise>
									<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
										<c:if test="${myFileList.invoiceAttachment == true || myFileList.invoiceAttachment=='true'}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.invoiceAttachment == false || myFileList.invoiceAttachment == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
									</c:otherwise>
									</c:choose>
									<c:if test="${doc.key eq 'ZZ-Waste Basket'}">									
										<c:if test="${myFileList.invoiceAttachment == true || myFileList.invoiceAttachment=='true'}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>
										<c:if test="${myFileList.invoiceAttachment == false || myFileList.invoiceAttachment == null}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="updateInvoice('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
								</td>
								<configByCorp:fieldVisibility componentId="component.standard.cPortalActivation">
								<td style="width:38px; text-align:center" class="newtabletd_orange">
								<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
											<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											
												<c:if test="${myFileList.isCportal == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												</c:if>
												<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
													<input type="checkbox" id="checkboxId"  value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>
												
											</c:if>
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
											<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											
												<c:if test="${myFileList.isCportal == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												</c:if>
												<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
													<input type="checkbox" id="checkboxId"  value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>
												
											</c:if>
										</c:if>
									</c:when>
									<c:otherwise>
									<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
										<c:if test="${myFileList.isCportal == true}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										</c:if>
										<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
											<input type="checkbox" id="checkboxId"  value="${myFileList.id}"  onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>
									</c:if>
									</c:otherwise>
								</c:choose>
									<c:if test="${doc.key eq 'ZZ-Waste Basket'}">									
										<c:if test="${myFileList.isCportal == true}">
											<input type="checkbox"  disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										</c:if>
										<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
								</td>
								</configByCorp:fieldVisibility>
								<configByCorp:fieldVisibility componentId="component.standard.accountPotralActivation">
							 	<td style="text-align:center" class="newtabletd_orange">
							 	<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
									 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
												<c:if test="${myFileList.isAccportal == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												 </c:if>										
												<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>										
											</c:if>
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
									 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
												<c:if test="${myFileList.isAccportal == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												 </c:if>										
												<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>										
											</c:if>
										</c:if>
									</c:when>
									<c:otherwise>
									<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
										<c:if test="${myFileList.isAccportal == true}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
									</c:otherwise>
									</c:choose>
									<c:if test="${doc.key eq 'ZZ-Waste Basket'}">									
										<c:if test="${myFileList.isAccportal == true}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>
										<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
								</td>
								</configByCorp:fieldVisibility>
								<td style="text-align:center" class="newtabletd_orange">
								<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
											<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
												<c:if test="${myFileList.isPartnerPortal == true}">
													<input type="checkbox" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);" checked/>
												</c:if>
												<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);"/>
												</c:if>
											</c:if>
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
											<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
												<c:if test="${myFileList.isPartnerPortal == true}">
													<input type="checkbox" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);" checked/>
												</c:if>
												<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);"/>
												</c:if>
											</c:if>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
										<c:if test="${myFileList.isPartnerPortal == true}">
											<input type="checkbox" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);" checked/>
										</c:if>
										<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);"/>
										</c:if>
										</c:if>
									</c:otherwise>
									</c:choose>
									<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
										<c:if test="${myFileList.isPartnerPortal == true}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);" checked/>
										</c:if>
										<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId('${myFileList.id}','${myFileList.fileType }',this);"/>
										</c:if>
									</c:if>
								</td>
	                      <configByCorp:fieldVisibility componentId="component.field.Resource.DriverPortal">
							 	<td style="text-align:center" class="newtabletd_orange">
							 	<c:choose>
									<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
										<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
									 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
												<c:if test="${myFileList.isDriver == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												 </c:if>										
												<c:if test="${myFileList.isDriver == false || myFileList.isDriver == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>										
											</c:if>
										</c:if>
										<c:if test="${appUserCorpid =='UNIP'}">
									 		<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
												<c:if test="${myFileList.isDriver == true}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
												 </c:if>										
												<c:if test="${myFileList.isDriver == false || myFileList.isDriver == null}">
													<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)"/>
												</c:if>										
											</c:if>
										</c:if>
									</c:when>
									<c:otherwise>
									<c:if test="${doc.key ne 'ZZ-Waste Basket'}">							 		
										<c:if test="${myFileList.isDriver == true}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>										
										<c:if test="${myFileList.isDriver == false || myFileList.isDriver == null}">
											<input type="checkbox"  id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId('${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
									</c:otherwise>
									</c:choose>
									<c:if test="${doc.key eq 'ZZ-Waste Basket'}">									
										<c:if test="${myFileList.isDriver == true}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId'(${myFileList.id}','${myFileList.fileType }',this)" checked/>
										 </c:if>
										<c:if test="${myFileList.isDriver == false || myFileList.isDriver == null}">
											<input type="checkbox" disabled="disabled" id="checkboxId" value="${myFileList.id}" onclick="checkStatusDriverId'(${myFileList.id}','${myFileList.fileType }',this)"/>
										</c:if>										
									</c:if>
								</td>
								</configByCorp:fieldVisibility>
								
								 <c:if test="${customerFile.controlFlag == 'C'}">
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedBA" value="false"/>
													<c:if test="${myFileList.isBookingAgent}">
														<c:set var="ischeckedBA" value="true"/>
													</c:if>
											    	<c:if test="${bookingAgentFlag=='BA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${bookingAgentFlag==''}">
														<c:if test="${myFileList.isBookingAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}"  onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isBookingAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" unchecked/>
														</c:if>
													</c:if>	
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedBA" value="false"/>
													<c:if test="${myFileList.isBookingAgent}">
														<c:set var="ischeckedBA" value="true"/>
													</c:if>
											    	<c:if test="${bookingAgentFlag=='BA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${bookingAgentFlag==''}">
														<c:if test="${myFileList.isBookingAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}"  onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isBookingAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" unchecked/>
														</c:if>
													</c:if>	
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedBA" value="false"/>
											<c:if test="${myFileList.isBookingAgent}">
												<c:set var="ischeckedBA" value="true"/>
											</c:if>
									    	<c:if test="${bookingAgentFlag=='BA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${bookingAgentFlag==''}">
												<c:if test="${myFileList.isBookingAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}"  onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isBookingAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedBA" value="false"/>
											<c:if test="${myFileList.isBookingAgent}">
												<c:set var="ischeckedBA" value="true"/>
											</c:if>
									    	<c:if test="${bookingAgentFlag=='BA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${bookingAgentFlag==''}">
												<c:if test="${myFileList.isBookingAgent}">	
													<input type="checkbox"  disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}"  onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isBookingAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedBA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagBA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
									</td>
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedNA" value="false"/>
													<c:if test="${myFileList.isNetworkAgent}">
														<c:set var="ischeckedNA" value="true"/>
													</c:if>	
											   		<c:if test="${networkAgentFlag=='NA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${networkAgentFlag==''}">
														<c:if test="${myFileList.isNetworkAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" checked/>
														</c:if>	
														<c:if test="${!myFileList.isNetworkAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" unchecked/>
														</c:if>	
													</c:if>	
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedNA" value="false"/>
													<c:if test="${myFileList.isNetworkAgent}">
														<c:set var="ischeckedNA" value="true"/>
													</c:if>	
											   		<c:if test="${networkAgentFlag=='NA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${networkAgentFlag==''}">
														<c:if test="${myFileList.isNetworkAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" checked/>
														</c:if>	
														<c:if test="${!myFileList.isNetworkAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" unchecked/>
														</c:if>	
													</c:if>	
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedNA" value="false"/>
											<c:if test="${myFileList.isNetworkAgent}">
												<c:set var="ischeckedNA" value="true"/>
											</c:if>	
									   		<c:if test="${networkAgentFlag=='NA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${networkAgentFlag==''}">
												<c:if test="${myFileList.isNetworkAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" checked/>
												</c:if>	
												<c:if test="${!myFileList.isNetworkAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" unchecked/>
												</c:if>	
											</c:if>	
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedNA" value="false"/>
											<c:if test="${myFileList.isNetworkAgent}">
												<c:set var="ischeckedNA" value="true"/>
											</c:if>	
									   		<c:if test="${networkAgentFlag=='NA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${networkAgentFlag==''}">
												<c:if test="${myFileList.isNetworkAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" checked/>
												</c:if>	
												<c:if test="${!myFileList.isNetworkAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedNA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagNA');" unchecked/>
												</c:if>	
											</c:if>	
										</c:if>
									</td>
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedOA" value="false"/>
													<c:if test="${myFileList.isOriginAgent}">
														<c:set var="ischeckedOA" value="true"/>
													</c:if>
											     	<c:if test="${originAgentFlag=='OA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${originAgentFlag==''}">
														<c:if test="${myFileList.isOriginAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isOriginAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" unchecked/>
														</c:if>		
													</c:if>
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedOA" value="false"/>
													<c:if test="${myFileList.isOriginAgent}">
														<c:set var="ischeckedOA" value="true"/>
													</c:if>
											     	<c:if test="${originAgentFlag=='OA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${originAgentFlag==''}">
														<c:if test="${myFileList.isOriginAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isOriginAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" unchecked/>
														</c:if>		
													</c:if>
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedOA" value="false"/>
											<c:if test="${myFileList.isOriginAgent}">
												<c:set var="ischeckedOA" value="true"/>
											</c:if>
									     	<c:if test="${originAgentFlag=='OA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${originAgentFlag==''}">
												<c:if test="${myFileList.isOriginAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isOriginAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" unchecked/>
												</c:if>		
											</c:if>
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedOA" value="false"/>
											<c:if test="${myFileList.isOriginAgent}">
												<c:set var="ischeckedOA" value="true"/>
											</c:if>
									     	<c:if test="${originAgentFlag=='OA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${originAgentFlag==''}">
												<c:if test="${myFileList.isOriginAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isOriginAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagOA');" unchecked/>
												</c:if>		
											</c:if>
										</c:if>
									</td>
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
												<c:set var="ischeckedSOA" value="false"/>
												<c:if test="${myFileList.isSubOriginAgent}">
													<c:set var="ischeckedSOA" value="true"/>
												</c:if>												
										   		<c:if test="${subOriginAgentFlag=='SOA' }">						   				 
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="" disabled="true" checked/>											
												</c:if>
												<c:if test="${subOriginAgentFlag=='' }">
													<c:if test="${myFileList.isSubOriginAgent}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" checked/>
													</c:if>
													<c:if test="${!myFileList.isSubOriginAgent}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" unchecked/>
													</c:if>
												</c:if>	
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
												<c:set var="ischeckedSOA" value="false"/>
												<c:if test="${myFileList.isSubOriginAgent}">
													<c:set var="ischeckedSOA" value="true"/>
												</c:if>												
										   		<c:if test="${subOriginAgentFlag=='SOA' }">						   				 
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="" disabled="true" checked/>											
												</c:if>
												<c:if test="${subOriginAgentFlag=='' }">
													<c:if test="${myFileList.isSubOriginAgent}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" checked/>
													</c:if>
													<c:if test="${!myFileList.isSubOriginAgent}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" unchecked/>
													</c:if>
												</c:if>	
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedSOA" value="false"/>
											<c:if test="${myFileList.isSubOriginAgent}">
												<c:set var="ischeckedSOA" value="true"/>
											</c:if>												
									   		<c:if test="${subOriginAgentFlag=='SOA' }">						   				 
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="" disabled="true" checked/>											
											</c:if>
											<c:if test="${subOriginAgentFlag=='' }">
												<c:if test="${myFileList.isSubOriginAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isSubOriginAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedSOA" value="false"/>
											<c:if test="${myFileList.isSubOriginAgent}">
												<c:set var="ischeckedSOA" value="true"/>
											</c:if>												
									   		<c:if test="${subOriginAgentFlag=='SOA' }">						   				 
												<input type="checkbox"  style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="" disabled="true" checked/>											
											</c:if>
											<c:if test="${subOriginAgentFlag=='' }">
												<c:if test="${myFileList.isSubOriginAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isSubOriginAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedSOA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSOA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
									</td>
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedDA" value="false"/>
													<c:if test="${myFileList.isDestAgent}">
														<c:set var="ischeckedDA" value="true"/>
													</c:if>
											    	<c:if test="${destAgentFlag=='DA' }">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${destAgentFlag==''}">
														<c:if test="${myFileList.isDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" unchecked/>
														</c:if>
													</c:if>	
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedDA" value="false"/>
													<c:if test="${myFileList.isDestAgent}">
														<c:set var="ischeckedDA" value="true"/>
													</c:if>
											    	<c:if test="${destAgentFlag=='DA' }">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${destAgentFlag==''}">
														<c:if test="${myFileList.isDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" unchecked/>
														</c:if>
													</c:if>	
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedDA" value="false"/>
											<c:if test="${myFileList.isDestAgent}">
												<c:set var="ischeckedDA" value="true"/>
											</c:if>
									    	<c:if test="${destAgentFlag=='DA' }">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${destAgentFlag==''}">
												<c:if test="${myFileList.isDestAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isDestAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedDA" value="false"/>
											<c:if test="${myFileList.isDestAgent}">
												<c:set var="ischeckedDA" value="true"/>
											</c:if>
									    	<c:if test="${destAgentFlag=='DA' }">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${destAgentFlag==''}">
												<c:if test="${myFileList.isDestAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isDestAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagDA');" unchecked/>
												</c:if>
											</c:if>	
										</c:if>
									</td>
									<td class="newtabletd_blue">
									<c:choose>
										<c:when test="${doc.key eq 'ZZ-Secure Doc' && myFileFor =='SO'}">
											<c:if test="${appUserName eq soCoordinator && appUserCorpid !='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedSDA" value="false"/>
													<c:if test="${myFileList.isSubDestAgent}">
														<c:set var="ischeckedSDA" value="true"/>
													</c:if>	
											    	<c:if test="${subDestAgentFlag=='SDA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${subDestAgentFlag==''}">
														<c:if test="${myFileList.isSubDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isSubDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" unchecked/>
														</c:if>
													</c:if>
												</c:if>
											</c:if>
											<c:if test="${appUserCorpid =='UNIP'}">
												<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
													<c:set var="ischeckedSDA" value="false"/>
													<c:if test="${myFileList.isSubDestAgent}">
														<c:set var="ischeckedSDA" value="true"/>
													</c:if>	
											    	<c:if test="${subDestAgentFlag=='SDA'}">	
														<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="" disabled="true" checked/>
													</c:if>
													<c:if test="${subDestAgentFlag==''}">
														<c:if test="${myFileList.isSubDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" checked/>
														</c:if>
														<c:if test="${!myFileList.isSubDestAgent}">	
															<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" unchecked/>
														</c:if>
													</c:if>
												</c:if>
											</c:if>
										</c:when>
										<c:otherwise>
										<c:if test="${doc.key ne 'ZZ-Waste Basket'}">
											<c:set var="ischeckedSDA" value="false"/>
											<c:if test="${myFileList.isSubDestAgent}">
												<c:set var="ischeckedSDA" value="true"/>
											</c:if>	
									    	<c:if test="${subDestAgentFlag=='SDA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${subDestAgentFlag==''}">
												<c:if test="${myFileList.isSubDestAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isSubDestAgent}">	
													<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" unchecked/>
												</c:if>
											</c:if>
										</c:if>
										</c:otherwise>
										</c:choose>
										<c:if test="${doc.key eq 'ZZ-Waste Basket'}">
											<c:set var="ischeckedSDA" value="false"/>
											<c:if test="${myFileList.isSubDestAgent}">
												<c:set var="ischeckedSDA" value="true"/>
											</c:if>	
									    	<c:if test="${subDestAgentFlag=='SDA'}">	
												<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="" disabled="true" checked/>
											</c:if>
											<c:if test="${subDestAgentFlag==''}">
												<c:if test="${myFileList.isSubDestAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" checked/>
												</c:if>
												<c:if test="${!myFileList.isSubDestAgent}">	
													<input type="checkbox" disabled="disabled" style="margin-left:10px;" id="checkboxId" value="${ischeckedSDA}" onclick="findSoAllPricing('${myFileList.id}','${myFileList.fileType }',this,'checkFlagSDA');" unchecked/>
												</c:if>
											</c:if>
										</c:if>
									</td>
								</c:if>
								<c:if test="${myFileFor=='PO'}">
								<td style="text-align:center" colspan="6">
									<c:if test="${myFileList.isServiceProvider == true}">
						   				 <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusServiceProviderId(${myFileList.id},this);" checked/>
						   			 </c:if>
									<c:if test="${myFileList.isServiceProvider == false || myFileList.isServiceProvider == null}">
										<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusServiceProviderId(${myFileList.id},this);"/>
									</c:if>
								</td>
								</c:if>
							</c:if>
						</tr>
						
				 	</c:forEach>
			 	</c:if>
			 	<c:if test="${validPermission =='No'}">
			 		<tr id ="div_ZZ-Secure Doc">
			 			<td colspan="20">
			 				<div style="display: inline-block; position: absolute; background:#fff; width: 100%; height: 13px; font-size: 12px;">You&nbsp;are&nbsp;not&nbsp;authorize&nbsp;to&nbsp;view&nbsp;Details.</div>
			 			</td>
			 		</tr>
			 	</c:if>
			 	<tr>
					<td style="border: 0px none; height: 5px; background: none repeat scroll 0% 0% rgb(255, 255, 255);" colspan="20"></td>					
				</tr>
		</c:forEach>
	</tbody>
</table>
</td></tr>
</table>
</div>
<c:if test="${from != 'View'}">
    <c:if test="${userType!='DRIVER'}">
<c:out value="${buttons}" escapeXml="false" />

<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>

<input type="button" class="cssbutton" style="margin-right: 5px;width:70px;" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
</c:if>
<c:if test="${myFileFor!='PO' && myFileFor!='Truck' && userType!='DRIVER'}">
	<input type="button" class="cssbutton" style="margin-right: 5px;width:70px; " name="combBtn"  value="Merge PDF" onclick="return conbinedDoc();"/>
	<c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
		<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:95px;" name="mailBtn"  value="TransDoc Xfer" onclick="return transDoc();"/>
	</c:if>
</c:if>
</c:if>
<!--  <input type="button" class="cssbutton" style="margin-right: 5px;width:85px;" name="combBtn"  value="Covert&nbsp;To&nbsp;PDF" onclick="return conbinedDocInPdf();"/> -->
 
 <input type="button" class="cssbutton" style="margin-right: 5px;width:65px;"  value="Email" onclick=" attachDocInEmail();"/>

</s:form>
<!--<script type="text/javascript" src="scripts/jquery-2.0.3.min.js"></script>
-->
<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script>

$("#collapseall").click(function(){
	if($("#collapseall").val() == "Collapse All"){
		$("[id^=div_]").hide();
		$("#collapseall").val('Expand All')
	    $("[id^=plus_]").show();
	  	$("[id^=minus_]").hide();
	}else{		
		$("[id^=div_]").show();
		$("#collapseall").val('Collapse All')
		$("[id^=minus_]").show();
		$("[id^=plus_]").hide();
	}
});

$( ".table-fc tbody tr:odd" ).addClass("odd");

 var tableOffset = $("#table-File").offset().top;
var $header = $("#table-File > thead").clone();
var $fixedHeader = $("#header-fixed").append($header);

$(window).bind("scroll", function() {
    var offset = $(this).scrollTop();

    if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
        $fixedHeader.show();
    }
    else if (offset < tableOffset) {
        $fixedHeader.hide();
    }
});  
</script>

</script>

<script type="text/javascript">
function findSoAllPricing(fid,fileType,event,targetValue){
	if(fileType=='Credit Card Authorization'){
		 alert("Secure Authorized Document")
		 event.checked=false;
		}else{
	progressBarAutoSave('1');
	var myFileFor='${fileNameFor}';
	var id="";
   if(myFileFor=='SO'){
	    id='${serviceOrder.id}';
   }else if(myFileFor=='CF'){
	   id='${customerFile.id}';
   }
	var fid=fid;
	var valueTarget="";	
  if(event.checked==true){
	  if(targetValue=='checkFlagBA'){
			valueTarget="BA";
		}else if(targetValue=='checkFlagNA'){
			valueTarget="NA";
		}else if(targetValue=='checkFlagOA'){
			valueTarget="OA";
		}else if(targetValue=='checkFlagSOA'){
			valueTarget="SOA";
		}else if(targetValue=='checkFlagDA'){
			valueTarget="DA";
		}else if(targetValue=='checkFlagSDA'){
			valueTarget="SDA";
		}
	new Ajax.Request('uploadMyFileIsCheckBox.html?ajax=1&decorator=simple&popup=true&'+targetValue+'='+valueTarget+'&myFileFor=${fileNameFor}&fid='+fid+'&id='+id,
				  {
				    method:'get',
				    onSuccess: function(transport){
				      var response = transport.responseText || "no response text";
				 				
				      container.innerHTML = response;
				      //container.update(response);
				       showOrHide(0);
				      container.show();
				     
				    },
				    
				    onLoading: function()
				       {
				    	var loading = document.getElementById("loading").style.display = "block";
				    	
				    	 loading.innerHTML;
				    },
				    
				    onFailure: function(){ 
					    //alert('Something went wrong...')
					     }
				  });
  }else{
	 if (event.checked==false){
	  if(targetValue=='checkFlagBA'){
			valueTarget="NBA";
		}else if(targetValue=='checkFlagNA'){
			valueTarget="NNA";
		}else if(targetValue=='checkFlagOA'){
			valueTarget="NOA";
		}else if(targetValue=='checkFlagSOA'){
			valueTarget="NSOA";
		}else if(targetValue=='checkFlagDA'){
			valueTarget="NDA";
		}else if(targetValue=='checkFlagSDA'){
			valueTarget="NSDA";
		}
	  new Ajax.Request('unCheckMyFileIsCheckBox.html?ajax=1&decorator=simple&popup=true&'+targetValue+'='+valueTarget+'&myFileFor=${fileNameFor}&fid='+fid+'&id='+id,
			  {
			    method:'get',
			    onSuccess: function(transport){
			      var response = transport.responseText || "no response text";
			   				
			      container.innerHTML = response;
			      //container.update(response);
			       showOrHide(0);
			      container.show();
			     
			    },
			    onLoading: function()
			       {
			    	var loading = document.getElementById("loading").style.display = "block";
			    	
			    	 loading.innerHTML;
			    },
			    
			    onFailure: function(){ 
				    //alert('Something went wrong...')
				     }
			  });  
  }
  }
  setTimeout("pageReload()",10000);  
		}
		}

function pageReload(){
	//progressBarAutoSave('0');
	location.reload(true);
}



</script>

<script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script>
<%--
<c:forEach var="doc" items="${docTypeFiles}">
		<c:if test="${ doc.value !=null &&  doc.value!='' && doc.value!='[]'}">
					
 <script>
$(document).ready(function(){
	$('table.table-fc tbody').append('<tr><td colspan=17><div onClick="showHideMyDiv(div_${doc.key},${doc.key})" style=margin: 0px; width:1371px> <table cellpadding=0 cellspacing=0 width=72% style=margin: 0px><tr><td class=headtab_leftFc><img id=plus_${doc.key} src=${pageContext.request.contextPath}/images/plus-small.png style=padding-left:2px;display: none;padding-left:5px;  /><img id=minus_${doc.key} src=${pageContext.request.contextPath}/images/minus-small.png style=padding-left:8px; /></td>					<td NOWRAP class=headtab_center>${ doc.key}</td><td width=28 valign=top class=headtab_bg></td>'
					+'<td class=headtab_bg_special >&nbsp;</td>'
					+'<td class=headtab_right></td>'
				+'</tr>'
			+'</table>'			
		 +'</div>'
		+'</td></tr><tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>');

})
</script>

</c:if>
</c:forEach>--%>
<script> 
$(document).ready(function(){
	$("#Layer1 div.exportlinks").addClass('CFEXP_left');
	$("#Layer1 div.exportlinks").after('<div class="CFEXP_right"></div>');
	$("#Layer1 div.exportlinks a").first().css('margin-left','65px');
	$("#Layer1 div.exportlinks").css('padding-top','7px'); 
	$("#Layer1 div.exportlinks").css('margin-bottom','5px'); 
	$("#Layer1 div.exportlinks").css('margin-bottom','0px'); 
	$("#Layer1 div.exportlinks").css('text-transform','uppercase');
	$("#Layer1 div.exportlinks a").css('text-transform','capitalize');
})

</script>

<script type="text/javascript"> 
try{
	function validatefields(emailStatus) { 		
		
		
		javascript:openWindow('openMailView.html?emailStatusVal='+encodeURI(emailStatus)+'&decorator=popup&popup=true&from=file',650,320) ;
		}
	<c:if test="${hitFlag == 1}" >
		location.href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&ppType=${ppType}&PPID=${PPID}&active=true&secure=false&forQuotation=${forQuotation}";
	</c:if>
	<c:if test="${hitFlag == 5}" >
	alert('The selected document type is not mapped to TransDoc so it will not be transferred.');
		location.href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteForTemp}&active=true&secure=false";
	</c:if>
 }
 catch(e){}
    try{
    var len = document.forms['myFileForm'].elements['DD'].length;
	<sec-auth:authComponent componentId="module.tab.workTicket.DRIVER">
	 disabledAll();
	</sec-auth:authComponent>
    }
    catch(e){}
   try{
    if(len>1){
    	show('chkAllButton');
    }
    }
    catch(e){}
</script>
<script type="text/javascript">
 function recipientWithEmailStatus(position,myFileId) {
	 var myFileFor='${fileNameFor}';
		if(myFileFor=='SO')
	{
	    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
	}
		if(myFileFor=='CF')
	{
			 var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	}
 var url="recipientWithEmailStatus.html?ajax=1&decorator=simple&popup=true&jobNumber="+jobNumber+"&noteFor=${noteFor}&myFileId="+myFileId;
 ajax_showTooltip(url,position);
 }
</script>
<script type="text/javascript">
function attachDocInEmail()
{
		var myFileFor='${fileNameFor}';
		if(myFileFor=='SO')
	{
	    var jobNumber=document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
	}
		if(myFileFor=='CF')
	{
			 
			var jobNumber=document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
	}
var checkBoxEmailId = document.forms['myFileForm'].elements['userCheck'].value;
var myFileIdCheck = document.forms['myFileForm'].elements['myFileIdCheck'].value;
var checkForInvalid=true;
if(myFileIdCheck!=null && myFileIdCheck!=undefined && myFileIdCheck!=''){
	checkForInvalid=checkForInvalidSelect(myFileIdCheck);
}
if(checkBoxEmailId ==''){
	alert('Please select the one or more document to e-mail.');
}else if(!checkForInvalid){
	alert('You are not allowed to send the attachment of pending Email.');
}else{                           
	var url = 'sendEmailFile.html?decorator=popup&popup=true&checkBoxEmailId='+checkBoxEmailId+'&jobNumber='+jobNumber+"&noteFor=${noteFor}&companyDivision=${customerFile.companyDivision}&myFileIdCheck="+myFileIdCheck;
	window.openWindow(url,height=570,width=520);
}
}
function collectFileId(){
	document.forms['myFileForm'].elements['myFileIdCheck'].value = "";
	var idList='';
	var temp='';
	if(document.forms['myFileForm'].elements['DD'].length!=undefined){
		var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		if(document.forms['myFileForm'].elements['DD'][i].checked){
			temp=document.forms['myFileForm'].elements['DD'][i].value;
			if(idList==''){
				idList=temp.split("`")[0];
			}else{
				idList=idList+","+temp.split("`")[0];
			}
		}
		document.forms['myFileForm'].elements['myFileIdCheck'].value = idList;
	}
	}else{
		temp=document.forms['myFileForm'].elements['DD'].value;
		idList=temp.split("`")[0];
		document.forms['myFileForm'].elements['myFileIdCheck'].value = idList;
	}
}
function checkForInvalidSelect(myFileIdCheck){
	var flag =true;
	var idArr= myFileIdCheck.split(",");
	var tempId='';
	var tempVal='';
	for(var i=0;i<idArr.length && flag;i++){
		flag = checkForPendingStatus(idArr[i]);
	}
	return flag;
}
function checkForPendingStatus(id){
	var flag =true;
	var emailId=getEmailSetupId(id);	
	var retryCont = getRetryCount(emailId); 
	if(retryCont<=2 && getDescription(emailId)=='SaveForEmail'){
		flag =false;
	}
	return flag;
}
function getDescription(id){
	var tempId='';
	var tempValue='';
	<c:forEach var="entry" items="${emailStatusList}">
		if(tempValue==''){
			tempId="${entry.key}";
			if(tempId==id){
				tempValue="${entry.value}";
				tempValue=tempValue.split('^')[1];
			}
		}
	</c:forEach>
	return tempValue;
}
function getRetryCount(id){
	var tempId='';
	var tempCount='';
	<c:forEach var="entry" items="${emailStatusList}">
		if(tempCount==''){
			tempId="${entry.key}";
			if(tempId==id){
				tempCount="${entry.value}";
				tempCount=tempCount.split('^')[0];
			}
		}
	</c:forEach>
	return tempCount;
}
function getEmailSetupId(id){
	var tempId='';
	var tempValue='';
	<c:forEach var="num1" items="${fn:split(myFileStatusCheck, '^')}" varStatus="count">
	if(tempValue==''){
		tempId="${num1}";
		if(tempId.split(":")[0]==id){
			if(tempId.split(":")[1]!=undefined){
			tempValue=tempId.split(":")[1];
			}
		}
	}
	</c:forEach>
	return tempValue;
}
function downloadSelectedFile(id){
	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
	var fieldVal = document.forms['myFileForm'].elements['myFileForVal'].value;
	var fieldVal1 = document.forms['myFileForm'].elements['noteForVal'].value;
	var fieldVal2 = document.forms['myFileForm'].elements['activeVal'].value;
	var fieldVal3 = document.forms['myFileForm'].elements['secureVal'].value;
	var myFileJspName = "myFileDocType";
	var url ="";
	if(fieldVal=='SO'){
		var fieldVal4 = document.forms['myFileForm'].elements['serviceOrder.shipNumber'].value;
		 	url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&shipNumber="+fieldVal4+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else if(fieldVal=='CF'){
		var fieldVal5 = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
			url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&sequenceNumber="+fieldVal5+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"";
	}else if(fieldVal=='PO'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"&ppTypeVal="+ppTypeVal+"&ppIdVal="+ppIdVal+"";
	}
	else if(fieldVal=='Truck'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		url="ImageServletAction.html?id="+id+"&myFileForVal="+fieldVal+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&secureVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"&ppTypeVal="+ppTypeVal+"&ppIdVal="+ppIdVal+"";
	}
	 location.href=url;
}
var fileVal = '${resultType}';
if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
	alert('This File is Temporarily Unavailable.')
	var replaceURL = "";
	var myFileForVal =  "<%=request.getAttribute("myFileFor")%>";
	var noteForVal = "<%=request.getAttribute("noteFor")%>";
	var activeVal = "<%=request.getAttribute("active")%>";
	var secureVal = "<%=request.getAttribute("secure")%>";
	var query = window.location.search.substring(1);
	var vars = query.split("=");
	var totalVars= vars[1];
	var idArr = totalVars.split("&");
	var fileIdVal = idArr[0];
	if(myFileForVal=='SO' || myFileForVal=='CF'){
		replaceURL = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"";
	}else if(myFileForVal=='PO'){
		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
		replaceURL = "myFilesDocType.html?id="+fileIdVal+"&myFileFor="+myFileForVal+"&noteFor="+noteForVal+"&active="+activeVal+"&secure="+secureVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"";
	}
	window.location.replace(replaceURL);
}
</script>