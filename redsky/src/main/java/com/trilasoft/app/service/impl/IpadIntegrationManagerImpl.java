package com.trilasoft.app.service.impl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.*;


import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.service.IpadIntegrationManager;

public class IpadIntegrationManagerImpl implements IpadIntegrationManager{
	
private Document dom;

public Element addElement(String value,String tagName){
	 Element ele = dom.createElement(tagName);
     Text nameText=dom.createTextNode(value);
     ele.appendChild(nameText);
     return ele;
}

public void creatCustomerXML(CustomerFile customerFile){
	createDocument();
	String filePath=""+customerFile.getFirstName()+"_"+customerFile.getLastName()+"_"+customerFile.getSequenceNumber();
	File file = new File(filePath);
	Element rootEle = dom.createElement("MovingData");
	rootEle.setAttribute("ID",customerFile.getId().toString());
	dom.appendChild(rootEle);
	   Element generalInfoEle = dom.createElement("GeneralInfo");
	        Element Name = dom.createElement("Name");
	        Text nameText=dom.createTextNode(customerFile.getFirstName()+" "+customerFile.getLastName());
	        Name.appendChild(nameText);
	   generalInfoEle.appendChild(Name);
	        if(customerFile.getEstimator()==null)customerFile.setEstimator("");
	   generalInfoEle.appendChild(addElement(customerFile.getEstimator(),"EstimatorName")); 
	        if(customerFile.getSource()==null)customerFile.setSource("");
	   generalInfoEle.appendChild(addElement(customerFile.getSource(),"SourceOfRequest"));
	        if(customerFile.getSurvey()==null)customerFile.setSurvey(new Date());
	        SimpleDateFormat dateformat = new SimpleDateFormat("mm/dd/yy");
	   generalInfoEle.appendChild(addElement(dateformat.format(customerFile.getSurvey())+" "+customerFile.getSurveyTime(),"EstimationDate"));
	        if(customerFile.getSequenceNumber()==null)customerFile.setSequenceNumber("");
	   generalInfoEle.appendChild(addElement(customerFile.getSequenceNumber(),"EMFID"));
	            
	           Element addressEle = dom.createElement("Address");
	                   if(customerFile.getOriginAddress1()==null)customerFile.setOriginAddress1("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1(),"Company"));
	                   if(customerFile.getOriginAddress2()==null)customerFile.setOriginAddress2("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress2(),"Street"));
	                   if(customerFile.getOriginCity()==null)customerFile.setOriginCity("");
	                   addressEle.appendChild(addElement(customerFile.getOriginCity(),"City"));
	                   if(customerFile.getOriginState()==null)customerFile.setOriginState("");
	                   addressEle.appendChild(addElement(customerFile.getOriginState(),"State"));
	                   if(customerFile.getOriginCountry()==null)customerFile.setOriginCountry("");
	                   addressEle.appendChild(addElement(customerFile.getOriginAddress1(),"Country"));
	                   if(customerFile.getOriginZip()==null)customerFile.setOriginZip("");
	                   addressEle.appendChild(addElement(customerFile.getOriginZip(),"Zip"));
	                   if(customerFile.getOriginDayPhone()==null)customerFile.setOriginDayPhone("");
	                   addressEle.appendChild(addElement(customerFile.getOriginDayPhone(),"PrimaryPhone"));
	                   if(customerFile.getOriginMobile()==null)customerFile.setOriginMobile("");
	                   addressEle.appendChild(addElement(customerFile.getOriginMobile(),"SecondaryPhone"));
	                   if(customerFile.getEmail()==null)customerFile.setEmail("");
	                   addressEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                   if(customerFile.getOriginFax()==null)customerFile.setOriginFax("");
	                   addressEle.appendChild(addElement(customerFile.getOriginFax(),"fax"));
	                           
	                       Element accessInfoEle = dom.createElement("AccessInfo");
	                       accessInfoEle.appendChild(addElement("0","Floor"));
	                       accessInfoEle.appendChild(addElement("false","HasElevator"));
	                       accessInfoEle.appendChild(addElement(" ","ElevatorDetails"));
	                       accessInfoEle.appendChild(addElement("0","DistanceToParking"));
	                       accessInfoEle.appendChild(addElement("false","NeedCrane"));
	                       accessInfoEle.appendChild(addElement(" ","PictureFileName"));
	                       accessInfoEle.appendChild(addElement(" ","PropertyType"));
	                       accessInfoEle.appendChild(addElement(" ","PropertySize"));
	                       accessInfoEle.appendChild(addElement("false","ParkingReservationRequired"));
	                       accessInfoEle.appendChild(addElement(" ","ParkingType"));
	                       accessInfoEle.appendChild(addElement("0","NumOfParkingSpots"));
	                       accessInfoEle.appendChild(addElement("0","ParkingSpotSize"));
	                       accessInfoEle.appendChild(addElement("false","CarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","CarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","CarryDescription"));
	                       accessInfoEle.appendChild(addElement(" ","ExternalElevatorType"));
	                       accessInfoEle.appendChild(addElement("false","ShuttleRequired"));
	                       accessInfoEle.appendChild(addElement("0","ShuttleDistance"));
	                       accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
	                       accessInfoEle.appendChild(addElement("0","StairCarryLength"));
	                       accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
	                       accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
	                       accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                  
	                  addressEle.appendChild(accessInfoEle);
	                  addressEle.appendChild(addElement("","Comment"));
	                      
	                       Element roomsEle = dom.createElement("Rooms");
	                           Element roomEle = dom.createElement("Room"); 
	                           roomEle.setAttribute("name","XXX");
	                               roomEle.appendChild(addElement(" ","Nickname"));
	                               roomEle.appendChild(addElement("0","NumOfPeople"));
	                               roomEle.appendChild(addElement("0","EstimatedVolume"));
	                           roomsEle.appendChild(roomEle);
	                 addressEle.appendChild(roomsEle); 
	                 
	    generalInfoEle.appendChild(addressEle); 
	       
	                Element destinationEle = dom.createElement("Destination");
	                      if(customerFile.getDestinationAddress1()==null)customerFile.setDestinationAddress1("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationAddress1(),"Company"));
	                      if(customerFile.getDestinationAddress2()==null)customerFile.setDestinationAddress2("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationAddress2(),"Street"));
	                      if(customerFile.getDestinationCity()==null)customerFile.setDestinationCity("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCity(),"City"));
	                      if(customerFile.getDestinationState()==null)customerFile.setDestinationState("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationState(),"State"));
	                      if(customerFile.getDestinationCountry()==null)customerFile.setDestinationCountry("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationCountry(),"Country"));
	                      if(customerFile.getDestinationZip()==null)customerFile.setDestinationZip("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationZip(),"Zip"));
	                      if(customerFile.getDestinationDayPhone()==null)customerFile.setDestinationDayPhone("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationDayPhone(),"PrimaryPhone"));
	                      if(customerFile.getDestinationMobile()==null)customerFile.setDestinationMobile("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationMobile(),"SecondaryPhone"));
	                      if(customerFile.getEmail()==null)customerFile.setEmail("");
	                      destinationEle.appendChild(addElement(customerFile.getEmail(),"email"));
	                      if(customerFile.getDestinationFax()==null)customerFile.setDestinationFax("");
	                      destinationEle.appendChild(addElement(customerFile.getDestinationFax(),"fax"));
	                         
	                         Element accessInfoEle2 = dom.createElement("AccessInfo");
	                         accessInfoEle2.appendChild(addElement("0","Floor"));
	                         accessInfoEle2.appendChild(addElement("false","HasElevator"));
	                         accessInfoEle2.appendChild(addElement(" ","ElevatorDetails"));
	                         accessInfoEle2.appendChild(addElement("0","DistanceToParking"));
	                         accessInfoEle2.appendChild(addElement("false","NeedCrane"));
	                         accessInfoEle2.appendChild(addElement(" ","PictureFileName"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertyType"));
	                         accessInfoEle2.appendChild(addElement(" ","PropertySize"));
	                         accessInfoEle2.appendChild(addElement("false","ParkingReservationRequired"));
	                         accessInfoEle2.appendChild(addElement(" ","ParkingType"));
	                         accessInfoEle2.appendChild(addElement("0","NumOfParkingSpots"));
	                         accessInfoEle2.appendChild(addElement("0","ParkingSpotSize"));
	                         accessInfoEle2.appendChild(addElement("false","CarryRequired"));
	                         accessInfoEle2.appendChild(addElement("0","CarryLength"));
	                         accessInfoEle2.appendChild(addElement(" ","CarryDescription"));
	                         accessInfoEle2.appendChild(addElement(" ","ExternalElevatorType"));
	                         accessInfoEle2.appendChild(addElement("false","ShuttleRequired"));
	                         accessInfoEle2.appendChild(addElement("0","ShuttleDistance"));
	                         accessInfoEle.appendChild(addElement("false","StairCarryRequired"));
		                     accessInfoEle.appendChild(addElement("0","StairCarryLength"));
		                     accessInfoEle.appendChild(addElement(" ","StairCarryDescription"));
		                     accessInfoEle.appendChild(addElement("false","AdditionalStopRequired"));
		                     accessInfoEle.appendChild(addElement(" ","AdditionalStop"));
	                 destinationEle.appendChild(accessInfoEle2);
	                 destinationEle.appendChild(addElement("","Comment"));
	       generalInfoEle.appendChild(destinationEle); 
  rootEle.appendChild(generalInfoEle);  
  try{
 //TransformerFactory instance is used to create Transformer objects. 
  /*TransformerFactory factory = TransformerFactory.newInstance();
  Transformer transformer = factory.newTransformer(); 
  transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
  // create string from xml tree
  StringWriter sw = new StringWriter();
  StreamResult result = new StreamResult(sw);
  DOMSource source = new DOMSource(dom);
  transformer.transform(source, result);
  String xmlString = sw.toString();
  //String uploadDir = ServletActionContext.getServletContext().getRealPath("/resources");
	//uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/XML" + "/");
  BufferedWriter bw = new BufferedWriter
                (new OutputStreamWriter(new FileOutputStream(file)));
  bw.write(xmlString);
  bw.flush();
  bw.close();*/
	  TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(dom);
      StreamResult result =  new StreamResult(System.out);
      transformer.transform(source, result);
  }catch(Exception e){e.printStackTrace();}
  
 /* try {
	    String msgText1="";
	    String subject = "XML  of "+customerFile.getFirstName()+"_"+customerFile.getLastName()+"'s survey at "+customerFile.getSurveyTime();
		String smtpHost = "localhost"; 
		String from = "vsoni@trilasoft.com";
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", smtpHost);
		Session session = Session.getInstance(props, null);
		session.setDebug(true);
		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		String to[]= from.split(",");
		InternetAddress[] address = new InternetAddress[to.length];
		for (int i = 0; i < to.length; i++)
		address[i] = new InternetAddress(to[i]);
		msg.setRecipients(Message.RecipientType.TO, address); 
		//msg.setRecipients(Message.RecipientType.CC, estimatorEmail);
		 msg.setSubject(subject);
		 MimeBodyPart mbp1 = new MimeBodyPart();
		 mbp1.setText(msgText1);
		 MimeBodyPart mbp2 = new MimeBodyPart();
         FileDataSource fds = new FileDataSource(file);
         mbp2.setDataHandler(new DataHandler(fds));
         mbp2.setFileName(fds.getName());
         
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(mbp1);
		mp.addBodyPart(mbp2);
		msg.setContent(mp);
		msg.setSentDate(new Date());
		Transport.send(msg);
		
  } catch (MessagingException mex) {
		mex.printStackTrace();
		Exception ex = null;
		if ((ex = mex.getNextException()) != null) {
			ex.printStackTrace(); 
		} else {
		}
	}*/
  
} 
private void createDocument() {
	DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	try {
	DocumentBuilder db = dbf.newDocumentBuilder();
       dom = db.newDocument();
	}catch(ParserConfigurationException pce) {
		System.out.println("Error while trying to instantiate DocumentBuilder " + pce);
	}
}

}
