// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`company` ADD COLUMN `singleCompanyDivisionInvoicing` BIt(1) DEFAULT b'0'

ALTER TABLE `redsky`.`customersurveyfeedback` ADD COLUMN `serviceDetails` VARCHAR(25) AFTER `feedBack`;

ALTER TABLE `redsky`.`customersurveyfeedback` ADD COLUMN `serviceForDestEmail` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`defaultaccountline` ADD COLUMN `equipment` VARCHAR(20) NULL  AFTER `destinationCountry` ;

ALTER TABLE `redsky`.`refmaster` ADD COLUMN `status` VARCHAR(10) AFTER `label6`;

update refmaster set status='Active' ;

ALTER TABLE `redsky`.`refquoteservices` ADD COLUMN `displayOrder` INTEGER DEFAULT null AFTER `updatedBy`;

insert  into networkdatafields(modelName, fieldName, transactionType,createdBy, createdOn, updatedBy, updatedOn,useSysDefault,toModelName,toFieldName,type)
values('Billing','wareHousereceiptDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
('Billing','wareHousereceiptDate','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

insert  into networkdatafields(modelName, fieldName, transactionType,createdBy, createdOn, updatedBy, updatedOn,useSysDefault,toModelName,toFieldName,type)
values('Billing','locationAgentCode','Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentCode','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentName','Create','amishra',now(),'amishra',now(),false,'','','CMM/DMM'),
      ('Billing','locationAgentName','Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `contract` VARCHAR(100) AFTER `emailPrintOption`,
 ADD COLUMN `networkPartnerCode` VARCHAR(10) AFTER `contract`,
 ADD COLUMN `source` VARCHAR(40) AFTER `networkPartnerCode`,
 ADD COLUMN `vendorCode` VARCHAR(8) AFTER `source`,
 ADD COLUMN `insuranceHas` VARCHAR(1) AFTER `vendorCode`,
 ADD COLUMN `insuranceOption` VARCHAR(150) AFTER `insuranceHas`,
 ADD COLUMN `defaultVat` VARCHAR(30) AFTER `insuranceOption`,
 ADD COLUMN `billToAuthorization` VARCHAR(50) AFTER `defaultVat`;


ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `HOM_estimatedHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL  AFTER `DSS_comment` , 
ADD COLUMN `HOM_actualHSRReferral` DECIMAL(19,2) NULL DEFAULT NULL  AFTER `HOM_estimatedHSRReferral` , 
ADD COLUMN `HSM_offerDate` DATETIME NULL DEFAULT NULL  AFTER `HOM_actualHSRReferral` , 
ADD COLUMN `HSM_closingDate` DATETIME NULL DEFAULT NULL  AFTER `HSM_offerDate` , 
ADD COLUMN `TAC_timeAuthorized` VARCHAR(65) NULL DEFAULT NULL  AFTER `HSM_closingDate` ;

Insert into refmaster (corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)values
('HSRG', 'Days', 'Days', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Weeks', 'Weeks', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar'),
('HSRG', 'Months', 'Months', 40, 'Time_Authorized','en',now(),'kkumar',now(),'kkumar');

INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType)
VALUES('HSRG','Time_Authorized',20,true,now(),'kkumar',now(),'kkumar',1,1,'');


ALTER TABLE `redsky`.`accountline` ADD COLUMN `dynamicNavisionflag` BIT(1) DEFAULT b'0' AFTER `accountLineScostElementDescription`,
 ADD COLUMN `deleteDynamicNavisionflag` BIT(1) DEFAULT b'0' AFTER `dynamicNavisionflag`,
 ADD COLUMN `oldCompanyDivision` VARCHAR(10) AFTER `deleteDynamicNavisionflag`,
 ADD COLUMN `sendDynamicNavisionDate` DATETIME AFTER `oldCompanyDivision`,
 ADD COLUMN `dynamicNavXfer` VARCHAR(100) AFTER `sendDynamicNavisionDate`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `networkPartnerName` VARCHAR(255) NULL  AFTER `billToAuthorization` , 
ADD COLUMN `vendorName` VARCHAR(255) NULL  AFTER `networkPartnerName` ;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `soListView` VARCHAR(25) DEFAULT '' AFTER `moveType`;

ALTER TABLE `redsky`.`sodashboard` ADD COLUMN `customFlag` VARCHAR(45) DEFAULT '' AFTER `loadingDt`,
ADD COLUMN `customDate` DATETIME DEFAULT null  AFTER `customFlag`;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `localJobs` VARCHAR(500) DEFAULT '' AFTER `hubWarehouseOperationalLimit`;

update accountline set dynamicNavisionflag=false,deleteDynamicNavisionflag=false,oldCompanyDivision=null,dynamicNavXfer=null,sendDynamicNavisionDate=null
where corpId in ('VOER','VOCZ','VOAO','VORU');



update systemdefault set localJobs="('LOC','OFF','RLO','DOM','LOG')";


###################
#9382




DROP TABLE IF EXISTS `redsky`.`sodashboard`;
CREATE TABLE  `redsky`.`sodashboard` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `shipnumber` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `serviceOrderId` bigint(20) NOT NULL,
  `role` varchar(29) CHARACTER SET utf8 DEFAULT '',
  `uvlstatus` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `survey` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `estvolume` decimal(19,2) DEFAULT NULL,
  `ldpkticket` varchar(10) CHARACTER SET utf8 DEFAULT '',
  `loading` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `actualweight` varchar(21) CHARACTER SET utf8 DEFAULT NULL,
  `actVolume` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `truck` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `deliveryticket` varchar(10) CHARACTER SET utf8 DEFAULT '',
  `delivery` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `claims` varchar(400) DEFAULT '',
  `qc` datetime DEFAULT NULL,
  `corpID` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `estSurvey` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estLoding` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `estDelivery` varchar(19) CHARACTER SET utf8 DEFAULT NULL,
  `surveyDt` varchar(45) DEFAULT '',
  `deliveryDt` varchar(45) DEFAULT '',
  `loadingDt` varchar(45) DEFAULT '',
    PRIMARY KEY (`id`),
      KEY `serviceorderid` (`serviceOrderId`))
       ENGINE=InnoDB DEFAULT CHARSET=latin1;



ALTER TABLE `redsky`.`sodashboard` ADD COLUMN `customFlag` VARCHAR(45) DEFAULT '' AFTER `loadingDt`,
 ADD COLUMN `customDate` DATETIME DEFAULT null  AFTER `customFlag`;




insert into sodashboard(shipnumber,serviceorderid,corpid)
select shipnumber,id,corpid from serviceorder
where controlflag='C';




select if(loadA is null,beginLoad,loadA),if(deliveryA is null,deliveryShipper,deliveryA)
,if(cf.survey is null,cf.actualsurveydate,cf.survey),
if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(cf.actualsurveydate is not null,cf.actualsurveydate,
if(date_format(cf.survey,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(cf.survey,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%survey%',
if(cf.actualsurveydate is not null,cf.actualsurveydate,
if(date_format(cf.survey,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(cf.survey,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA')),

if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(t.loadA is not null,t.loadA,
if(date_format(t.beginLoad,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.beginLoad,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%Loading%',
if(t.loadA is not null,t.loadA,
if(date_format(t.beginLoad,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.beginLoad,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA')),

if(
(select count(*) from companydivision c where c.corpid=s.corpid and c.bookingagentcode=s.bookingagentcode)>0,
if(t.deliveryA is not null,t.deliveryA,
if(date_format(t.deliveryShipper,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.deliveryShipper,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD',cf.survey))),
if((select distinct flex5 from refmaster where parameter='SERVICE' and code=s.servicetype) like '%Delivery%',
if(t.deliveryA is not null,t.deliveryA,
if(date_format(t.deliveryShipper,'%Y-%m-%d')=date_format(now(),'%Y-%m-%d'),'DT',
if(date_format(t.deliveryShipper,'%Y-%m-%d')<date_format(now(),'%Y-%m-%d'),'OD','NA'))),'NA'))

from trackingstatus t,serviceorder s,customerfile cf where
s.id=t.id and cf.id=s.customerfileid ;





select concat("update sodashboard set loadingDt='",beginLoad,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and beginLoad is not null;


select concat("update sodashboard set deliveryDt='",deliveryShipper,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
 s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and deliveryShipper is not null;


select concat("update sodashboard set surveyDt='",cf.survey,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
 s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.survey is not null;


select concat("update sodashboard set loading='",t.loadA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.loadA is not null;




select concat("update sodashboard set survey='",cf.actualsurveydate,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and cf.actualsurveydate is not null;



select concat("update sodashboard set delivery='",t.deliveryA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.deliveryA is not null;






select estimatednetweightkilo,ActualNetWeightKilo,NetEstimateCubicMtr,NetActualCubicMtr
from miscellaneous m,systemdefault sd ,serviceorder s
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit;



select concat("update sodashboard set estweight='",estimatednetweightkilo,"' where serviceorderid='",m.id,"';")
from miscellaneous m,systemdefault sd ,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and m.unit1=sd.weightunit and estimatednetweightkilo is not null
and so.serviceorderid=m.id;


select concat("update sodashboard set actualweight='",actualnetweightkilo,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and actualnetweightkilo is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set estvolume='",NetEstimateCubicMtr,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and NetEstimateCubicMtr is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set ActVolume='",NetActualCubicMtr,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Kgs'
and s.id=m.id and m.unit1=sd.weightunit and NetActualCubicMtr is not null
and so.serviceorderid=s.id;








select concat("update sodashboard set estweight='",m.EstimatedNetWeight,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id  and m.unit1=sd.weightunit and m.EstimatedNetWeight is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set actualweight='",m.ActualNetWeight,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and m.ActualNetWeight is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set estvolume='",NetEstimateCubicFeet,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and NetEstimateCubicFeet is not null
and so.serviceorderid=s.id;


select concat("update sodashboard set ActVolume='",NetActualCubicFeet,"' where serviceorderid='",s.id,"';")
from miscellaneous m,systemdefault sd ,serviceorder s,sodashboard so
where sd.corpid=m.corpid and sd.weightunit='Lbs'
and s.id=m.id and m.unit1=sd.weightunit and NetActualCubicFeet is not null
and so.serviceorderid=s.id;





select concat("update sodashboard set customFlag='",t.clearCustomTA,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so where
s.id=t.id and cf.id=s.customerfileid
and so.serviceorderid=s.id and t.clearCustomTA is not null;



select concat("update sodashboard set claims='",cl.ClaimNumber,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,claim cl where
s.id=t.id and cf.id=s.customerfileid and cl.serviceorderid=s.id
and so.serviceorderid=s.id and cl.ClaimNumber is not null;





select concat("update sodashboard set qc='",b.AuditComplete,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,billing b where
s.id=t.id and cf.id=s.customerfileid and b.id=s.id
and so.serviceorderid=s.id and b.AuditComplete is not null;





select concat('update sodashboard set driver="',m.DriverName,'" where serviceorderid="',s.id,'";')
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id and m.DriverName <>'';




select concat("update sodashboard set truck='",m.Carrier,"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id and m.Carrier <>'';





select concat("update sodashboard set role='",(trim( both '/' from concat(if(t.originagent is not null and t.originagent<>'','OA/',''),
if(destinationagent is not null and destinationagent <>'','DA/',''),
if(t.originsubagent is not null and t.originsubagent<>'','OSA/',''),
if(t.destinationsubagent is not null and t.destinationsubagent<>'','DSA/',''),
if(t.brokercode is not null and t.brokercode<>'','BR/',''),
if(t.forwardercode is not null and t.forwarder<>'','FR/',''),
if(s.bookingagentcode is not null and s.bookingagentcode<>'','BA/',''),
if(t.networkPartnerCode is not null and t.networkpartnercode<>'','NA/',''),
if(m.haulingAgentCode is not null and m.haulingagentcode<>'','HA/','')))),"' where serviceorderid='",s.id,"';")
from trackingstatus t,serviceorder s,customerfile cf,sodashboard so,miscellaneous m where
 s.id=t.id and cf.id=s.customerfileid and m.id=s.id
and so.serviceorderid=s.id ;

select concat("update refmaster set flex5='",flex5,"' where corpid='",corpid,"' and code='",code,"' and description='",description,"' and parameter='",parameter,"';")
from flex5;

update refmaster set flex5='survey, loading, ETD, ETA, customs , delivery' where corpid='TSFT' and code='D/D' and description='Door to Door' and parameter='SERVICE';
update refmaster set flex5='Loading, ETD, ETA, Custom, Delivery' where corpid='STAR' and code='Port to Door' and description='Port to Door' and parameter='SERVICE';
update refmaster set flex5='survey, loading' where corpid='TSFT' and code='D/OP' and description='Door to Origin Port' and parameter='SERVICE';
update refmaster set flex5='survey, loading, ETD, ETA' where corpid='TSFT' and code='D/DP' and description='Door to Destination Port' and parameter='SERVICE';
update refmaster set flex5='ETD,ETA' where corpid='TSFT' and code='DP/D' and description='Destination Port to Door Import' and parameter='SERVICE';
update refmaster set flex5='ETD,ETA' where corpid='TSFT' and code='OP/DP' and description='Origin Port to Destination Port' and parameter='SERVICE';
update refmaster set flex5='' where corpid='TSFT' and code='' and description='' and parameter='SERVICE';
update refmaster set flex5='Survey' where corpid='STAR' and code='Packing Only' and description='Packing Only' and parameter='SERVICE';
update refmaster set flex5='Survey' where corpid='STAR' and code='Documentation Only' and description='Documentation Only' and parameter='SERVICE';
update refmaster set flex5='Survey' where corpid='STAR' and code='Forwarding Only' and description='Forwarding Only' and parameter='SERVICE';
update refmaster set flex5='' where corpid='STAR' and code='Unpacking Only' and description='Unpacking Only' and parameter='SERVICE';
update refmaster set flex5='Loading,Delivery' where corpid='VOER' and code='Warehouse to Destination Port' and description='Warehouse to Destination Port' and parameter='SERVICE';
update refmaster set flex5='Loading,survey' where corpid='VOER' and code='Door to Destination Warehouse' and description='Door to Destination Warehouse' and parameter='SERVICE';
update refmaster set flex5='Loading,Delivery,ETD,ETA' where corpid='VOER' and code='Warehouse to Destination Door' and description='Warehouse to Destination Door' and parameter='SERVICE';
update refmaster set flex5='' where corpid='VOER' and code='Free on Truck to Door' and description='Free on Truck to Door' and parameter='SERVICE';
update refmaster set flex5='' where corpid='VOER' and code='Free on Truck to Destination Port' and description='Free on Truck to Destination Port' and parameter='SERVICE';
update refmaster set flex5='survey, loading, ETA,' where corpid='BOUR' and code='RWF' and description='Residence to Warehouse (FOT)' and parameter='SERVICE';
update refmaster set flex5='' where corpid='BOUR' and code='Labour Assistance' and description='Labour Assistance' and parameter='SERVICE';
update refmaster set flex5='Loading,delivery,ETD' where corpid='BOUR' and code='WFR' and description='Warehouse (FOT) to Residence' and parameter='SERVICE';
update refmaster set flex5='loading,ETA' where corpid='BOUR' and code='WTW' and description='Warehouse (FOT) to Warehouse (FOT)' and parameter='SERVICE';
update refmaster set flex5='' where corpid='BOUR' and code='(FOT) Residence to Residence' and description='(FOT) Residence to Residence' and parameter='SERVICE';
update refmaster set flex5='survey, loading, ETA,ETD, customs , delivery' where corpid='BOUR' and code='Residence to Residence (FOT)' and description='Residence to Residence (FOT)' and parameter='SERVICE';
update refmaster set flex5='' where corpid='BOUR' and code='Residence (FOT) to Residence (FOT)' and description='Residence (FOT) to Residence (FOT)' and parameter='SERVICE';
update refmaster set flex5='survey,loading,ETA' where corpid='BOUR' and code='RTW' and description='Residence to Warehouse' and parameter='SERVICE';
update refmaster set flex5='ETA, ETD,loading' where corpid='BOUR' and code='Destination Port to Warehouse' and description='Destination Port to Warehouse' and parameter='SERVICE';
update refmaster set flex5='Loading,Delivery' where corpid='BOUR' and code='W/DP' and description='Warehouse to Destination Port' and parameter='SERVICE';
update refmaster set flex5='survey, loading' where corpid='TSFT' and code='D/OW' and description='Door to Origin Warehouse' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Asset Management' and description='Asset Management' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Commercial Move' and description='Commercial Move' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='FF&E' and description='FF&E' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Receive from outside carrier' and description='Receive from outside carrier' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Release to outside carrier' and description='Release to outside carrier' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Warehouse Labor' and description='Warehouse Labor' and parameter='SERVICE';
update refmaster set flex5='' where corpid='CWMS' and code='Repatriation Services 1' and description='Repatriation Services 1' and parameter='SERVICE';
update refmaster set flex5='Loading, ETD, Delivery' where corpid='SSCW' and code='Destination Warehouse to Door' and description='Destination Warehouse to Door' and parameter='SERVICE';
update refmaster set flex5='Loading, ETD, Delivery' where corpid='JOHN' and code='DW/D' and description='Destination Warehouse to Door' and parameter='SERVICE';
update refmaster set flex5='Loading,survey' where corpid='TSFT' and code='D/W' and description='Door to Warehouse' and parameter='SERVICE';
update refmaster set flex5='Survey,Loading,ETD,ETA' where corpid='TSFT' and code='D/P' and description='Door to Port' and parameter='SERVICE';
update refmaster set flex5='Loading,ETD,ETA' where corpid='TSFT' and code='P/W' and description='Port to Warehouse' and parameter='SERVICE';
update refmaster set flex5='Loading' where corpid='TSFT' and code='W/P' and description='Warehouse to Port' and parameter='SERVICE';
update refmaster set flex5='Loading,delivery, ETD, ETA' where corpid='TSFT' and code='W/D' and description='Warehouse to Door' and parameter='SERVICE';
update refmaster set flex5='loading,ETA' where corpid='TSFT' and code='W/W' and description='Warehouse to Warehouse' and parameter='SERVICE';
update refmaster set flex5='' where corpid='TSFT' and code='PSTO' and description='Permanent Storage' and parameter='SERVICE';
update refmaster set flex5='' where corpid='JOHN' and code='DO' and description='Delivery Only' and parameter='SERVICE';
update refmaster set flex5='Loading, ETD, ETA, Custom, Delivery' where corpid='DECA' and code='P/D' and description='Port to Door ' and parameter='SERVICE';
update refmaster set flex5='' where corpid='BONG' and code='INT' and description='Internal Move' and parameter='SERVICE';
update refmaster set flex5='' where corpid='RSKY' and code='CMOVE' and description='Commercial Move' and parameter='SERVICE';
update refmaster set flex5='' where corpid='RSKY' and code='WHLAB' and description='Warehouse Labor' and parameter='SERVICE';
update refmaster set flex5='Loading, survey' where corpid='ICMG' and code='RTD' and description='Residence to Warehouse (FOT)' and parameter='SERVICE';
update refmaster set flex5='' where corpid='ICMG' and code='LAA' and description='Labour Assistance' and parameter='SERVICE';
update refmaster set flex5='Loading,delivery, ETD, ETA' where corpid='ICMG' and code='WTR' and description='Warehouse (FOT) to Residence' and parameter='SERVICE';
update refmaster set flex5='' where corpid='ICMG' and code='WTW' and description='Warehouse (FOT) to Warehouse (FOT)' and parameter='SERVICE';
update refmaster set flex5='survey, loading, ETA,ETD, customs , delivery' where corpid='ICMG' and code='FOTR' and description='(FOT) Residence to Residence' and parameter='SERVICE';
update refmaster set flex5='survey, loading, ETA,ETD, customs , delivery' where corpid='ICMG' and code='RFOT' and description='Residence to Residence (FOT)' and parameter='SERVICE';
update refmaster set flex5='' where corpid='ICMG' and code='RRFOT' and description='Residence (FOT) to Residence (FOT)' and parameter='SERVICE';
update refmaster set flex5='survey,loading,ETA' where corpid='ICMG' and code='R/W' and description='Residence to Warehouse' and parameter='SERVICE';
update refmaster set flex5='ETA, ETD,loading' where corpid='ICMG' and code='DPW' and description='Destination Port to Warehouse' and parameter='SERVICE';
update refmaster set flex5='Loading,Delivery' where corpid='ICMG' and code='WTP' and description='Warehouse to Destination Port' and parameter='SERVICE';
update refmaster set flex5='Delivery, Loading, custom' where corpid='HOLL' and code='Destination Warehouse to Door' and description='Destination Warehouse to Door' and parameter='SERVICE';
update refmaster set flex5='Loading,ETD,ETA,Custom' where corpid='UGCA' and code='OP/D' and description='Origin Port to Door' and parameter='SERVICE';
update refmaster set flex5='servicePartner.atArrival' where corpid='UTSI' and code='Warehouse to Destination Port' and description='Warehouse to Destination Port' and parameter='SERVICE';
update refmaster set flex5='Loading,survey' where corpid='UTSI' and code='Door to Destination Warehouse' and description='Door to Destination Warehouse' and parameter='SERVICE';
update refmaster set flex5='Loading,Delivery,ETD,ETA' where corpid='UTSI' and code='Warehouse to Destination Door' and description='Warehouse to Destination Door' and parameter='SERVICE';
update refmaster set flex5='' where corpid='UTSI' and code='Free on Truck to Door' and description='Free on Truck to Door' and parameter='SERVICE';
update refmaster set flex5='' where corpid='UTSI' and code='Free on Truck to Destination Port' and description='Free on Truck to Destination Port' and parameter='SERVICE';
