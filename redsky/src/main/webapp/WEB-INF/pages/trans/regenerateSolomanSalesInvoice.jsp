<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 

<head>

	<title>Regenerate Sales Invoice</title>   
    <meta name="heading" content="Regenerate Sales Invoice"/> 
<style type="text/css">
		h2 {background-color: #FBBFFF}
		
</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
	function validateEnteredFile()
	{
var fileName=document.forms['regenerateInvoiceExtractForm'].elements['invoiceFileName'].value;
	if(fileName!='')
	{
    var url='validateEnteredFile.html?ajax=1&decorator=simple&popup=true&fileName='+encodeURI(fileName);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
    
	}
	}
	function handleHttpResponse2()
        {
			
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
				results = results.replace('[','');
				results = results.replace(']','');					
				var count = parseInt(results);
				if(count>0)
				{}			    
				else
				{
				alert('No Such File Exist');
			 	}
             } 
  	   }
   var http2 = getHTTPObject();
function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validatingField()
{
var fileName=document.forms['regenerateInvoiceExtractForm'].elements['invoiceFileName'].value;
	fileName=fileName.trim();
	if(fileName=='')
	{
	alert('Please enter the file name');
	return false;
	}
	if(fileName.length>30)
	{
	alert('Please enter valid file name');
	document.forms['regenerateInvoiceExtractForm'].elements['invoiceFileName'].value='';
	return false;
	}
	if(fileName.indexOf("'")>-1)
	{
	alert('Please enter valid file name');
	document.forms['regenerateInvoiceExtractForm'].elements['invoiceFileName'].value='';
	return false;
	}	
	
	if(fileName!='')
	{
       var agree = confirm("Press OK to continue for regenerate or Press Cancel to Continue without regenerate ");
						       if(agree)
						        {
						    	   document.forms['regenerateInvoiceExtractForm'].action = 'regenerateSolomanSalesInvoiceExtracts.html';
						            document.forms['regenerateInvoiceExtractForm'].submit()
						        }
						       else
						        {
						        return false;
								}
	}
}
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 
<style type="text/css"> 
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; } 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;} 
</style> 
</head>
<body style="background-color:#444444;">
<s:form id="regenerateInvoiceExtractForm" name="regenerateInvoiceExtractForm" action="regenerateSolomanSalesInvoiceExtracts" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" > <tr>
<td width="38" align="right"></td>
<td class="listwhitetext" align="right">Enter The Extract File Name</td>
<td><s:textfield cssClass="input-text" name="invoiceFileName" value="" cssStyle="width:100px" /></td><td></td></tr>
<tr><td height="10"></td></tr>
<tr><td width="238" align="right" colspan="3">
<input type="button"  class="cssbutton" value="Regenerate Sales Invoice" align="right" onclick="return validatingField();">
</td><td></td></tr>

</table>
</div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body> 