package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.IntegrationLog;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServiceOrderDashboard;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.IntegrationLogManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VehicleManager;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public class MiscellaneousAction extends BaseAction implements Preparable {
	
	private List miscellaneouss;

	private List refMasters;
	private List status;
	private List partners;
	private Long i;
    private String tripIds;
	private RefMasterManager refMasterManager;
	private  Map<String, String> DOMINST;
	private  Map<String, String> yesno;
	private  Map<String, String> tan;
	private  Map<String, String> Special;
	private List<String> opshub;
	private  Map<String, String> Peak;
    private Map<String, String> driverAgencyList;
	private  Map<String, String> AGTPER;

	private  Map<String, String> Howpack;
	private Map<String, String> EQUIP;
	private  List weightunits;
	private List findByOwnerList;
	private List findByOwnerFromList;
	private List findByOwnerPerticularList;
	private List findByTrailerPerticularList;
	private  List volumeunits;
    private String partnerCode;
    private String truckNumber;
    private List findSalesStatus;
	private MiscellaneousManager miscellaneousManager;

	private ServiceOrderManager serviceOrderManager;
    private IntegrationLogManager integrationLogManager; 
	private ServiceOrder serviceOrder;

	private  TrackingStatus trackingStatus;

	private TrackingStatusManager trackingStatusManager;

	private  CustomerFile customerFile;

	private Billing billing;

	private BillingManager billingManager;

	private String sessionCorpID;
	private String hitFlag;
	private String shipSize;
	private List customerSO;
	private String minShip;
	private String countShip;
	private CustomerFileManager customerFileManager;
	private PartnerManager partnerManager;
	private  Map<String, String>lateReasonList;
	// private List day;
	private String lateReason;
	private String partyResponsible;
	private Date fromDate;
	private Date toDate;
	private String fromdateF;
	private String todateF;	
	private List haulingMgt;
	private Long customerFileId;
	private String filterdDrivers;
	private String opsHubList;
	private String driverAgency;
	private List selectedDriverTypeList;
	private String selfHaul;
	private String fromArea;
	private String toArea;
	private List radiusList;
	private String fromZip;
	private String toZip;
	private String fromRadius;
	private String toRadius;
	
	private List zipCodeList;
	private String zipCodes;
	private String latLong;
	private List latLongList;
	private String zipCode;
	private String selectedDriverAgency;
	private String selDriverAgency;
	private  List jobTypes;
	private String selJobTypes;
	private List selectedJobTypesList; 
	private String selectedJob;
	private List maxTripNumber;
	private Long autoTripNumber;
	private List statusNumberCountList=new ArrayList();
	private List statusNumberList=new ArrayList();
    private List jobstatusNumberList=new ArrayList();
    private String filterdTripNumber;
    private String filterdRegumber;
    private List selectedJobStatusList;
    private String jobStatus;
    private String orderNo;
    private String soStatus;
    private VehicleManager vehicleManager;
    private String job;
    private String estWgt;
    private String voxmeIntergartionFlag;
    private Company company;
	private CompanyManager companyManager;
	private boolean networkAgent;
    private String oiJobList;
    private String dashBoardHideJobsList;
	public MiscellaneousAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(MiscellaneousAction.class);	
	private Map fromAreaList;
	private Map toAreaList;
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
			getComboList(sessionCorpID);
			fromAreaList=refMasterManager.findByParameter(sessionCorpID,"AREA");
			toAreaList=refMasterManager.findByParameter(sessionCorpID,"AREA");
			partyForResposible=miscellaneousManager.partyResponsible(sessionCorpID,"");
			/****************The code below will need to change based on the program logic
			 * I am using empty maps but we will need to initialize them with proper data**/
		}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
			  }

        if (company != null && company.getDashBoardHideJobs() != null) {
       					dashBoardHideJobsList = company.getDashBoardHideJobs();	 
       				}
		if(company!=null){
		if(company.getVoxmeIntegration()!=null){
			voxmeIntergartionFlag=company.getVoxmeIntegration().toString();
			}
		if(company.getUTSI()!=null){
			 networkAgent=company.getUTSI();
	    }else{
		     networkAgent=false;
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public List getMiscellaneouss() {
		return miscellaneouss;
	}

	public List getRefMasters() {
		return refMasters;
	}

	//	  ****for auto save******
	private String gotoPageString;

	private String validateFormNav;

	public String getValidateFormNav() {

		return validateFormNav;

	}

	public void setValidateFormNav(String validateFormNav) {

		this.validateFormNav = validateFormNav;

	}

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String saveOnTabChange() throws Exception {
		//if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
		String s = save(); // else simply navigate to the requested page)
		return gotoPageString;
	}

	////////********************************************

	public String list() {
		miscellaneouss = miscellaneousManager.getAll();
		//getComboList("SSCW");
		return SUCCESS;
	}
	@SkipValidation
	public String miscellaneousListPopUpForCarrier() {  
    	partners = new ArrayList(partnerManager.findByCarriersForHeavy());
      	return SUCCESS;   
      } 
	public String getComboList(String corpID) {		
		lateReasonList = refMasterManager.findByParameter(corpID, "DELAYREASONCODE");
		yesno = refMasterManager.findByParameter(corpID, "YESNO");
		status=refMasterManager.findByParameters_status(corpID, "SETTLEMENT_STATUS");
		tan=refMasterManager.findByParameter(corpID, "TAN");
		Special = refMasterManager.findByParameter(corpID, "SPECIAL");
		Peak = refMasterManager.findByParameter(corpID, "PEAK");
		AGTPER = refMasterManager.findByParameter(corpID, "AGTPER");
		Howpack = refMasterManager.findByParameter(corpID, "HOWPACK");
		DOMINST = refMasterManager.findByParameter(corpID, "DOMINST");
		EQUIP = refMasterManager.findByParameter(corpID, "EQUIP");		
		weightunits = new ArrayList();
		weightunits.add("Lbs");
		weightunits.add("Kgs");

		volumeunits = new ArrayList();
		volumeunits.add("Cft");
		volumeunits.add("Cbm");
		return SUCCESS;
	}
    @SkipValidation
	public String findByOwnerListInMiscellaneous() {		
		findByOwnerList=miscellaneousManager.findByOwner(partnerCode, sessionCorpID);
		return SUCCESS;
	}
    @SkipValidation
	public String findByOwnerListMiscellaneous() {		
		findByOwnerList=miscellaneousManager.findByOwnerAndContact(partnerCode, sessionCorpID);
		return SUCCESS;
	}
    @SkipValidation
	public String findByOwnerListFromTruck() {		
		findByOwnerFromList=miscellaneousManager.findByOwnerFromTruck(partnerCode,truckNumber, sessionCorpID);
		return SUCCESS;
	}
    
    @SkipValidation
	public String findByOwnerListFromPerticularTruck() {		
		findByOwnerPerticularList=miscellaneousManager.findByOwnerPerticularTruck(truckNumber, sessionCorpID);
		return SUCCESS;
	}
    @SkipValidation
    public String findByTrailerListFromPerticularTruck(){
    	findByTrailerPerticularList=miscellaneousManager.findByTrailerPerticularTruck(truckNumber, sessionCorpID);
    	return SUCCESS;	
    }
	public Map<String, String> getYesno() {
		return yesno;
	}

	public Map<String, String> getSpecial() {
		return Special;
	}

	public Map<String, String> getPeak() {
		return Peak;
	}

	public Map<String, String> getAGTPER() {
		return AGTPER;
	}

	public Map<String, String> getHowpack() {
		return Howpack;
	}

	public List getWeightunits() {
		return weightunits;
	}

	public List getVolumeunits() {
		return volumeunits;
	}
	public  Map<String, String> getTan() {
		return tan;
	}
	private Miscellaneous miscellaneous;

	private Long id;
	private Long sid;

	private String countForDomesticNotes;

	private String countForDomesticServiceNotes;

	private String countForDomesticStateCountryNotes;
	private String countInterStateNotes;

	private NotesManager notesManager;

	public void setId(Long id) {
		this.id = id;
	}

	public Long getId() {
		return id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public String delete() {
		miscellaneousManager.remove(miscellaneous.getId());
		saveMessage(getText("miscellaneous.deleted"));

		return SUCCESS;
	}
	private String redirected;
	private List partyForResposible;
	public List getPartyForResposible() {
		//return partyForResposible;
		return (partyForResposible!=null && !partyForResposible.isEmpty())?partyForResposible:miscellaneousManager.partyResponsible(sessionCorpID,miscellaneous.getShipNumber());
	}
	public void setPartyForResposible(List partyForResposible) {
		this.partyForResposible = partyForResposible;
	}
	private List<SystemDefault> sysDefaultDetail;
	private String msgClicked;
	public String edit() {
		if (id != null) {
			serviceOrder = serviceOrderManager.get(id);
			getRequest().setAttribute("soLastName",serviceOrder.getLastName());
			customerFile = serviceOrder.getCustomerFile();
			billing=billingManager.get(id);
			//getComboList(sessionCorpID);
			if ((miscellaneousManager.checkById(id)).isEmpty())
			{	    
				miscellaneous=new Miscellaneous();
				miscellaneous.setShipNumber(serviceOrder.getShipNumber());
				miscellaneous.setSequenceNumber(serviceOrder.getSequenceNumber());
				miscellaneous.setCreatedOn(serviceOrder.getCreatedOn());
				miscellaneous.setUpdatedOn(serviceOrder.getUpdatedOn());
				miscellaneous.setCreatedBy(serviceOrder.getCreatedBy());
				miscellaneous.setUpdatedBy(serviceOrder.getUpdatedBy());
				miscellaneous.setCorpID(sessionCorpID);
				miscellaneous.setEstimateTareWeight(new BigDecimal(0));
				miscellaneous.setActualTareWeight(new BigDecimal(0));
				miscellaneous.setRwghTare(new BigDecimal(0));
				miscellaneous.setChargeableTareWeight(new BigDecimal(0));
				miscellaneous.setEntitleTareWeight(new BigDecimal(0));
				
				miscellaneous.setEstimateTareWeightKilo(new BigDecimal(0));
				miscellaneous.setActualTareWeightKilo(new BigDecimal(0));
				miscellaneous.setRwghTareKilo(new BigDecimal(0));
				miscellaneous.setChargeableTareWeightKilo(new BigDecimal(0));
				miscellaneous.setEntitleTareWeightKilo(new BigDecimal(0));
				sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
				if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
					for (SystemDefault systemDefault : sysDefaultDetail) {
						miscellaneous.setUnit1(systemDefault.getWeightUnit());
						miscellaneous.setUnit2(systemDefault.getVolumeUnit());
						serviceOrder.setDistanceInKmMiles(systemDefault.getDefaultDistanceCalc());
					}
				}
				miscellaneous.setId(serviceOrder.getId());
			}else{
				miscellaneous = miscellaneousManager.get(id);		
			}
			if((trackingStatusManager.checkById(id)).isEmpty())
			{
				trackingStatus = new TrackingStatus();
				trackingStatus.setId(serviceOrder.getId());
	            trackingStatus.setCorpID(serviceOrder.getCorpID());
	            trackingStatus.setSitOriginDays(0);
	            trackingStatus.setSitDestinationDays(0);
	            trackingStatus.setFromWH(0);
	            trackingStatus.setDemurrageCostPerDay2(0);
	            trackingStatus.setUsdaCost(0);
	            trackingStatus.setInspectorCost(0);
	            trackingStatus.setXrayCost(0);
	            trackingStatus.setPerdiumCostPerDay(0);
	            trackingStatus.setDemurrageCostPerDay1(0);
	            trackingStatus.setSequenceNumber(serviceOrder.getSequenceNumber());
	            trackingStatus.setShipNumber(serviceOrder.getShipNumber());
	            trackingStatus.setStatus(serviceOrder.getCustomerFile().getStatus());
	        	trackingStatus.setCreatedOn(serviceOrder.getCreatedOn());
	        	trackingStatus.setUpdatedOn(serviceOrder.getUpdatedOn());
	        	trackingStatus.setCreatedBy(serviceOrder.getCreatedBy());
	        	trackingStatus.setUpdatedBy(serviceOrder.getUpdatedBy());
	        	trackingStatus.setStatusDate(serviceOrder.getCustomerFile().getStatusDate());
	        	trackingStatus.setSurvey(serviceOrder.getCustomerFile().getSurvey());
	        	trackingStatus.setInitialContact(serviceOrder.getCustomerFile().getInitialContactDate());
	        	trackingStatus.setServiceOrder(serviceOrder);
	            trackingStatus.setMiscellaneous(miscellaneous);
	            trackingStatus.setSurveyDate(serviceOrder.getCustomerFile().getSurvey());
	            trackingStatus.setSurveyTimeFrom(serviceOrder.getCustomerFile().getSurveyTime());
	            trackingStatus.setSurveyTimeTo(serviceOrder.getCustomerFile().getSurveyTime2());
	            trackingStatus.setCutOffTimeTo("00:00");
			}else{
			trackingStatus = trackingStatusManager.get(id);
			}
			
			partyForResposible=miscellaneousManager.partyResponsible(sessionCorpID,miscellaneous.getShipNumber());
			miscellaneous.setCorpID(sessionCorpID);
			miscellaneous.setVanLineOrderNumber(serviceOrder.getRegistrationNumber());
			miscellaneous.setCreatedBy(serviceOrder.getCreatedBy());
			//miscellaneous.setStatus("New");
			
			shipSize = customerFileManager.findMaximumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	        minShip =  customerFileManager.findMinimumShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	        countShip =  customerFileManager.findCountShip(customerFile.getSequenceNumber(),"",customerFile.getCorpID()).get(0).toString();
	        if(trackingStatus.getPackA()!=null){
	        	packATime=trackingStatus.getPackA().toString().substring(11, 16);
	        }else{
	        	packATime="00:00";
	        }
	        if(trackingStatus.getDeliveryA()!= null){
	        	deliveryATime=trackingStatus.getDeliveryA().toString().substring(11, 16);
	        }else{
	        	deliveryATime="00:00";
	        }
	        if(trackingStatus.getDeliveryTA()!=null){
	        deliveryTATime=trackingStatus.getDeliveryTA().toString().substring(11, 16);
	        }else{
	        	deliveryTATime="00:00";
	        }if(trackingStatus.getLoadA()!=null){
	        	loadATime=trackingStatus.getLoadA().toString().substring(11, 16);
	        }else{
	        	loadATime="00:00";
	        }
	        
			if(redirected!=null)
			{			
			if(redirected.equalsIgnoreCase("yes"))
			{
				miscellaneous.setUpdatedOn(new Date());
				miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
			}
			}
		} else {
			miscellaneous.setStatus("New");
			miscellaneous.setUpdatedOn(new Date());
		}
		getNotesForIconChange();
		return SUCCESS;
	}
	private Date beginPacking;
	private Date endPacking;
	private Date packA;
	private Date beginLoad;
	private Date endLoad;
	private Date loadA;
	private Date deliveryShipper;
	private Date deliveryLastDay;
	private Date deliveryTA;
	private Date deliveryA;
	private Date sitDestinationIn;
	private Date sitDestinationA;
	private String sitDestinationTA;
	private int sitDestinationDays;
	private String packATime;
	private String deliveryATime;
	private String deliveryTATime;
	private String loadATime;	
	
	public String save() throws Exception {
		billing = billingManager.get(id);
		serviceOrder = serviceOrderManager.get(id);		
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		trackingStatus=trackingStatusManager.get(id);		
		boolean isNew = (miscellaneous.getId() == null);
		if (isNew) {
			miscellaneous.setCreatedOn(new Date());
			if(serviceOrder.getCorpID().toString().equalsIgnoreCase("UGWW")){
				miscellaneous.setUgwIntId(serviceOrder.getId().toString());
				
        	 }
		}
		miscellaneous.setUpdatedOn(new Date());
		partyForResposible=miscellaneousManager.partyResponsible(sessionCorpID,miscellaneous.getShipNumber());
		miscellaneous.setCorpID(sessionCorpID);
		miscellaneous.setUpdatedBy(getRequest().getRemoteUser());
          if(packA!=null || loadA!=null || deliveryA!=null){
			if(billing.getBillToCode()==null || billing.getBillToCode().equalsIgnoreCase("")){
				String key = "Please select the Bill To in Billing screen.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
			}
			if(billing.getContract()==null || billing.getContract().equalsIgnoreCase("")){
				String key = "Please select the Contract Type in Billing screen.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
			}
			if(serviceOrder.getBookingAgentCode()==null || serviceOrder.getBookingAgentCode().equalsIgnoreCase("")){
				String key = "Please select the Booking Agent in S/O Details.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
			}
			if(serviceOrder.getCoordinator()==null || serviceOrder.getCoordinator().equalsIgnoreCase("") ){
				String key = "Please select the Coordinator in S/O Details.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
			}
			if(serviceOrder.getCompanyDivision()==null || serviceOrder.getCompanyDivision().equalsIgnoreCase("") ){
				String key = "Please select the Company Division Type in S/O Details.";
				validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
			}
		}
          if((beginPacking!=null) && (endPacking!=null) &&( beginPacking.after(endPacking))){
        	  String key="End Packing Date should be greater or equal to Begin Packing Date.";
        	  	validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
          }
          
          if((beginLoad!=null) && (endLoad!=null) &&( beginLoad.after(endLoad))){
        	  String key="End Load Date should be greater or equal to Begin Load Date.";
        	  	validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
          }
          
          if((deliveryLastDay != null) && (deliveryShipper != null) && (deliveryShipper.after(deliveryLastDay))){
        	  String key="End Delivery Date should be greater or equal to Begin Delivery Date.";
        	  	validateFormNav = "NOTOK";
				errorMessage(getText(key));
	            return SUCCESS;
          }
          
        try
  		{		
  		trackingStatus.setUpdatedBy(miscellaneous.getUpdatedBy());		
  		trackingStatus.setUpdatedOn(new Date());
  		trackingStatusManager.save(trackingStatus);
  		}catch(Exception e){e.printStackTrace();}
  		
		miscellaneousManager.save(miscellaneous);
		miscellaneousManager.updateTrackingStatus(beginPacking, endPacking, packA, beginLoad, endLoad, loadA, deliveryShipper, deliveryLastDay, deliveryTA, deliveryA, id,packATime,deliveryATime,deliveryTATime,loadATime);
		miscellaneousManager.updateTrackingStatusDestination(sitDestinationIn, id);
		miscellaneousManager.updateTrackingStatussitDestinationA(sitDestinationA, id);
		miscellaneousManager.updateTrackingStatusDestinationTA(sitDestinationTA, id);
		miscellaneousManager.updateTrackingStatusDestinationDays(sitDestinationDays, id);
		miscellaneousManager.updateTrackingStatusLateReason(partyResponsible,lateReason, id);
				
		findSalesStatus = trackingStatusManager.findSalesStatus(trackingStatus.getSequenceNumber());
		
		if(!(findSalesStatus.isEmpty()) && (findSalesStatus!=null)){
			trackingStatusManager.updateSalesStatus(trackingStatus.getSequenceNumber(),getRequest().getRemoteUser());
		}
		List bookingAgentCode = billingManager.findBookingAgent(sessionCorpID);
		if((!(serviceOrder.getJob().toString().equals("LOG"))) && (!(serviceOrder.getJob().toString().equals("OFF")))&& (!(serviceOrder.getJob().toString().equals("FFG")))&& (!(serviceOrder.getJob().toString().equals("MLL")))&& (!(serviceOrder.getJob().toString().equals("ULL"))) ){
         if (serviceOrder.getBookingAgentCode() != null) {
			if ((!(bookingAgentCode.contains(serviceOrder.getBookingAgentCode())) && serviceOrder.getRouting()!=null && (serviceOrder.getRouting().equals("IMP")|| serviceOrder.getRouting().equals("NRI") || serviceOrder.getRouting().equals("DIN")))) {
				miscellaneousManager.updateBilling(trackingStatus.getDeliveryA(), id, getRequest().getRemoteUser());
				miscellaneousManager.updateTargetRevenueRecognition(trackingStatus.getDeliveryShipper(), id, getRequest().getRemoteUser());
			} else {
				miscellaneousManager.updateBilling(trackingStatus.getLoadA(), id,getRequest().getRemoteUser());
				miscellaneousManager.updateTargetRevenueRecognition(trackingStatus.getBeginLoad(), id, getRequest().getRemoteUser());
			}
		}
	}
//      *************************************Buisness logic for status field********************************
		serviceOrder = serviceOrderManager.get(id); 
		getRequest().setAttribute("soLastName",serviceOrder.getLastName());
		String errorMsg="";
		Date cal = new Date();
        logger.warn("system Date : "+cal);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
        formatter.setTimeZone(TimeZone.getTimeZone(company.getTimeZone())); 
        String ss =formatter.format(cal);
        SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd"); 
		Date currentTimeZoneWise =  dateformatYYYYMMDD.parse(ss);
		trackingStatus=trackingStatusManager.get(id);
		trackingStatus.setBeginPacking(beginPacking);
		trackingStatus.setEndPacking(endPacking);
		//trackingStatus.setPackA(packA);
		trackingStatus.setBeginLoad(beginLoad);
		trackingStatus.setEndLoad(endLoad);
		if((company.getAllowFutureActualDateEntry()!=null) && (!company.getAllowFutureActualDateEntry())){
		String key1="System cannot accept actualizing future dates, Please edit following field:";
			if(loadA!=null ){
			if(loadA.after(currentTimeZoneWise) ){
				String key = "Actual Loading Date.";
				errorMsg+=key+"<br>";
				 loadA = null;
				}else
				{
					trackingStatus.setLoadA(loadA);
				}
			}
		
		if(deliveryA!=null){
			if(deliveryA.after(currentTimeZoneWise)){
				String key = "Actual Delivery Date.";
				errorMsg+=key+"<br>";
				deliveryA = null;
				}else
				{
					trackingStatus.setDeliveryA(deliveryA);
			
				}}
		
		if(packA!=null ){
		if(packA.after(currentTimeZoneWise)){
			 String key = "Actual Packing Date.";
				errorMsg+=key+"<br>";
			 packA = null;
			}
		else
			{
			trackingStatus.setPackA(packA);
			}
			}
		}
		else
		{
		trackingStatus.setPackA(packA);
		trackingStatus.setDeliveryA(deliveryA);
		trackingStatus.setLoadA(loadA);
		}
		if(!errorMsg.equalsIgnoreCase("")){
			String key1 ="System cannot accept actualizing future dates, Please edit following fields:";
			validateFormNav = "NOTOK";
			errorMessage(getText(key1+"<BR>"+errorMsg));
	        return INPUT;
		}
		trackingStatus.setDeliveryShipper(deliveryShipper);
		trackingStatus.setDeliveryLastDay(deliveryLastDay);
		trackingStatus.setDeliveryTA(deliveryTA);
		//trackingStatus.setDeliveryA(deliveryA);
		trackingStatus.setSitDestinationIn(sitDestinationIn);
		trackingStatus.setSitDestinationA(sitDestinationA);
		if((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS")))&& (!(serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL"))))
        {  
     	   if((serviceOrder.getStatusNumber().intValue()<10) &&(trackingStatus.getPackA()!=null || trackingStatus.getLoadA()!=null)){
     		   trackingStatus.getPackA();
     		   //Bug 11839 - Possibility to modify Current status to Prior status
     		   //serviceOrder.setStatus("OSRV");
     		   //serviceOrder.setStatusDate(new Date());
     		   //serviceOrder.setStatusNumber(10);
     	   }
     	   if((serviceOrder.getStatusNumber().intValue()<20)&& trackingStatus.getSitOriginOn()!=null){
     		//Bug 11839 - Possibility to modify Current status to Prior status
     		   //serviceOrder.setStatus("OSIT");
     		   //serviceOrder.setStatusDate(new Date());
     		   //serviceOrder.setStatusNumber(20); 
     	   } 
     	   if((serviceOrder.getStatusNumber().intValue()<50)&& trackingStatus.getSitDestinationIn()!=null){
     		//Bug 11839 - Possibility to modify Current status to Prior status
     		   //serviceOrder.setStatus("DSIT");
     		  // serviceOrder.setStatusDate(new Date());
     		   //serviceOrder.setStatusNumber(50); 
     	   }
     	   if((serviceOrder.getStatusNumber().intValue()<60)&& trackingStatus.getDeliveryA()!=null){
     		//Bug 11839 - Possibility to modify Current status to Prior status
     		   //serviceOrder.setStatus("DLVR");
     		  // serviceOrder.setStatusDate(new Date());
     		   //serviceOrder.setStatusNumber(60); 
     	   }        	   
        } 
        serviceOrder.setBrokerCode(trackingStatus.getBrokerCode());
        serviceOrder.setOriginAgentCode(trackingStatus.getOriginAgentCode());
        serviceOrder.setOriginSubAgentCode(trackingStatus.getOriginSubAgentCode());
        serviceOrder.setDestinationAgentCode(trackingStatus.getDestinationAgentCode());
        serviceOrder.setDestinationSubAgentCode(trackingStatus.getDestinationSubAgentCode());
        serviceOrder.setForwarderCode(trackingStatus.getForwarderCode());
//      *************************************Buisness logic for CustomerFile status field******************************** 
        customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
        if(trackingStatus.getDeliveryA()!=null || serviceOrder.getETA()!= null || trackingStatus.getDeliveryShipper()!= null){
     	   trackingStatusManager.updateSalesStatus(customerFile.getSequenceNumber(),getRequest().getRemoteUser());
     	   customerFile.setSalesStatus("Booked");
        }
        if(customerFile.getStatus()!=null){
        	if((customerFile.getStatus().toString().equals("DWNLD")||customerFile.getStatus().toString().equals("DWND"))&& ((!(serviceOrder.getStatus().toString().equals("DWNLD")))||(serviceOrder.getStatusNumber().intValue()>=10 ))){
        		customerFile.setStatus("NEW");
    			customerFile.setStatusNumber(1);
    			customerFile.setStatusDate(new Date());
        		customerFileManager.save(customerFile); 
        	}
        } 
        if(!isNew){
          statusNumberCountList=serviceOrderManager.getStatusNumberCount(serviceOrder.getSequenceNumber(),serviceOrder.getId(),serviceOrder.getCorpID());
        } 
        if(isNew){
            miscellaneous.setStatus("New");
        	statusNumberCountList=serviceOrderManager.getStatusNumberCount(serviceOrder.getSequenceNumber(),serviceOrder.getCorpID());	
        }
        if((serviceOrder.getStatusNumber().intValue()>=10 && serviceOrder.getStatusNumber().intValue()<=60)||(Integer.parseInt(statusNumberCountList.get(0).toString())>0)){
        	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
        	if(customerFile.getStatusNumber()<20){
        		//Bug 12317 - Delete update Status from Application
        		//customerFile.setStatus("ACTIVE");
        		//customerFile.setStatusNumber(20);
        		//customerFile.setStatusDate(new Date());
        		customerFileManager.save(customerFile); 
        	}
        }
        if(serviceOrder.getStatusNumber().intValue()==60 || serviceOrder.getStatusNumber().intValue()==70 ){
        	boolean statusCheck=false;
        	TreeSet statusNumberSet=new TreeSet();
        	if(!isNew){
        		statusNumberList=serviceOrderManager.getStatusNumberList(serviceOrder.getSequenceNumber(),serviceOrder.getCorpID(),serviceOrder.getId());	
            }
        	if(!statusNumberList.isEmpty()){
        		Iterator it=statusNumberList.iterator();
        	while(it.hasNext()){
        	Integer number=(Integer)it.next();
        	if((!(number.toString().equals("60"))) && (!(number.toString().equals("70")))){
        		statusCheck=false;
        		break;
        	}
        	else {
        		statusNumberSet.add(number);
        		statusCheck=true;
        	}
        	}
        	if(statusCheck){
        		if(serviceOrder.getStatusNumber().intValue()==60){
        		if(	statusNumberSet.contains(70)){
        			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
                	if(customerFile.getStatusNumber()<40){
                		//Bug 12317 - Delete update Status from Application
                		//customerFile.setStatus("CLAIM");
                		//customerFile.setStatusNumber(40);
                		//customerFile.setStatusDate(new Date());
                		customerFileManager.save(customerFile); 
                	} 
        		}
        		} 
        		if(serviceOrder.getStatusNumber().intValue()==70){
        			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
                	if(customerFile.getStatusNumber()<40){
                		//customerFile.setStatus("CLAIM");
                		//customerFile.setStatusNumber(40);
                		//customerFile.setStatusDate(new Date());
                		customerFileManager.save(customerFile); 
        		}
        	}
        	} 
        	} else {
        		if(serviceOrder.getStatusNumber().intValue()==70){
        			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
                	if(customerFile.getStatusNumber()<40){
                		//Bug 12317 - Delete update Status from Application
                		//customerFile.setStatus("CLAIM");
                		//customerFile.setStatusNumber(40);
                		//customerFile.setStatusDate(new Date());
                		customerFileManager.save(customerFile); 
        		}
        	
        	}
        }
        }
        if(serviceOrder.getStatusNumber().intValue()==100 ){
        	if(!isNew){
        		statusNumberList=serviceOrderManager.getStatusNumberList(serviceOrder.getSequenceNumber(),serviceOrder.getCorpID(),serviceOrder.getId());	
            }
        	boolean statusCheck=false;
        	TreeSet statusNumberSet=new TreeSet();
        	if(!statusNumberList.isEmpty()){
        		Iterator it=statusNumberList.iterator();
            	while(it.hasNext()){
            	Integer number=(Integer)it.next();
            	if(!(number.toString().equals("100"))){
            		statusCheck=false;
            		break;
            	}
            	else {
            		statusNumberSet.add(number);
            		statusCheck=true;
            	}
            	}
            	if(statusCheck){
            		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
            		if(customerFile.getStatusNumber()<200){
                		customerFile.setStatus("HOLD");
                		customerFile.setStatusNumber(200);
                		customerFile.setStatusDate(new Date());
                		customerFileManager.save(customerFile); 
        		}
            		
            	}
        	}else {
        		customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
            	if(customerFile.getStatusNumber()<200){
            		customerFile.setStatus("HOLD");
            		customerFile.setStatusNumber(200);
            		customerFile.setStatusDate(new Date());
            		customerFileManager.save(customerFile); 
    		}
        		
        	}
        }
        if((serviceOrder.getJob().equals("STO")|| serviceOrder.getJob().equals("TPS") || serviceOrder.getJob().equals("STF")|| serviceOrder.getJob().equals("STL"))){
        	
        	jobstatusNumberList=serviceOrderManager.getJobstatusNumberList(serviceOrder.getSequenceNumber(),serviceOrder.getCorpID(),serviceOrder.getId());
        	 if(jobstatusNumberList.isEmpty()){
        		if(!(serviceOrder.getStatus().trim().equals("CLSD"))){
        			customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
                	if(customerFile.getStatusNumber()<50){
                		customerFile.setStatus("STORG");
                		customerFile.setStatusNumber(50);
                		customerFile.setStatusDate(new Date());
                		customerFileManager.save(customerFile); 
        			
        		}
        	}
        	
        	}
        	if(!(jobstatusNumberList.isEmpty())){
        		boolean statusCheck=false; 
            		Iterator it=jobstatusNumberList.iterator();
                	while(it.hasNext()){
                	String condition=(String)it.next();
                	String[] conditionArray=condition.split("#");
                	String job=conditionArray[0];
                	String stausNumber=conditionArray[1];
                	String staus=conditionArray[2];
                	if(((!(job.equals("STO")))&& (!(job.equals("TPS"))) && (!(job.equals("STF")))&& (!(job.equals("STL"))))&&(!(staus.equals("CLSD")))) {
                	 statusCheck=false;
                		break;
                	}
                	else { 
                		statusCheck=true;
                	} 
        	      }
        if(statusCheck){
        	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
        	if(customerFile.getStatusNumber()<50){
        		//Bug 12317 - Delete update Status from Application
        		//customerFile.setStatus("STORG");
        		//customerFile.setStatusNumber(50);
        		//customerFile.setStatusDate(new Date());
        		customerFileManager.save(customerFile); 
			
		}
        } 
        	}
        }
        if(((!(serviceOrder.getJob().equals("STO")))&& (!(serviceOrder.getJob().equals("TPS"))) && (!( serviceOrder.getJob().equals("STF")))&& (!(serviceOrder.getJob().equals("STL")))) &&(serviceOrder.getStatus().trim().equals("CLSD")) ){
        	jobstatusNumberList=serviceOrderManager.getJobstatusNumberList(serviceOrder.getSequenceNumber(),serviceOrder.getCorpID(),serviceOrder.getId());	
        	if(!(jobstatusNumberList.isEmpty())){
        		boolean statusCheck=false; 
            		Iterator it=jobstatusNumberList.iterator();
                	while(it.hasNext()){
                	String condition=(String)it.next();
                	String[] conditionArray=condition.split("#");
                	String job=conditionArray[0];
                	String stausNumber=conditionArray[1];
                	String staus=conditionArray[2];
                	if(((!(job.equals("STO")))&& (!(job.equals("TPS"))) && (!(job.equals("STF")))&& (!(job.equals("STL"))))&&(!(staus.equals("CLSD")))) {
                	 statusCheck=false;
                		break;
                	}
                	else { 
                		statusCheck=true;
                	} 
        	      }
        if(statusCheck){
        	customerFile=customerFileManager.get(serviceOrder.getCustomerFileId());	
        	if(customerFile.getStatusNumber()<50){
        		//customerFile.setStatus("STORG");
        		//customerFile.setStatusNumber(50);
        		//customerFile.setStatusDate(new Date());
        		customerFileManager.save(customerFile); 
			
		}
        } 
        	}
        }
        serviceOrderManager.save(serviceOrder);  
//        *************************************End of Buisness logic for CustomerFile status field********************************
		//********Logic to insert data in integration log**********
        if(serviceOrder.getRegistrationNumber()!=null && !serviceOrder.getRegistrationNumber().equals("")){
        if(miscellaneous.getDriverId()!=null && !miscellaneous.getDriverId().equals("")) {
        	IntegrationLog intlog = new IntegrationLog();
        	intlog.setCorpID(miscellaneous.getCorpID());
        	intlog.setFileName("du"+serviceOrder.getRegistrationNumber().replaceAll("-", "").toLowerCase());
        	intlog.setBatchID("");
        	intlog.setProcessedOn(new Date());
        	intlog.setFilelastModified(new Date());
        	intlog.setMessageType("DISPATCH_UPLOAD");
        	intlog.setMessageStatus("F");
        	intlog.setMessageEvent("1");
        	intlog.setRecordID(serviceOrder.getRegistrationNumber());
        	intlog.setTransactionID(miscellaneous.getShipNumber());
        	intlog.setMessage("Dispatch file ready to upload.");
        	integrationLogManager.save(intlog);
        }
        if(trackingStatus.getLoadA()!=null) {
        	IntegrationLog intlog = new IntegrationLog();
        	intlog.setCorpID(miscellaneous.getCorpID());
        	intlog.setFileName("du"+serviceOrder.getRegistrationNumber().replaceAll("-", "").toLowerCase());
        	intlog.setBatchID("");
        	intlog.setProcessedOn(new Date());
        	intlog.setFilelastModified(new Date());
        	intlog.setMessageType("DISPATCH_UPLOAD");
        	intlog.setMessageStatus("F");
        	intlog.setMessageEvent("2");
        	intlog.setRecordID(serviceOrder.getRegistrationNumber());
        	intlog.setTransactionID(miscellaneous.getShipNumber());
        	intlog.setMessage("Dispatch file ready to upload.");
        	integrationLogManager.save(intlog);
        }
        if(trackingStatus.getDeliveryA()!=null) {
        	IntegrationLog intlog = new IntegrationLog();
        	intlog.setCorpID(miscellaneous.getCorpID());
        	intlog.setFileName("du"+serviceOrder.getRegistrationNumber().replaceAll("-", "").toLowerCase());
        	intlog.setBatchID("");
        	intlog.setProcessedOn(new Date());
        	intlog.setFilelastModified(new Date());
        	intlog.setMessageType("DISPATCH_UPLOAD");
        	intlog.setMessageStatus("F");
        	intlog.setMessageEvent("3");
        	intlog.setRecordID(serviceOrder.getRegistrationNumber());
        	intlog.setTransactionID(miscellaneous.getShipNumber());
        	intlog.setMessage("Dispatch file ready to upload.");
        	integrationLogManager.save(intlog);
        }
        }
        try{
			if(serviceOrder.getIsNetworkRecord()){
				List linkedShipNumber=findLinkerShipNumber(serviceOrder);
				List<Object> miscellaneousRecords=findMiscellaneousRecords(linkedShipNumber,serviceOrder);
				synchornizeMiscellaneous(miscellaneousRecords,miscellaneous,trackingStatus);
				List<Object> trackingStatusRecords=findTrackingStatusRecords(linkedShipNumber,serviceOrder);
				synchornizeTrackingStatus(trackingStatusRecords,trackingStatus,serviceOrder);
				List<Object> serviceOrderRecords=findServiceOrderRecords(linkedShipNumber,serviceOrder);
				synchornizeServiceOrder(serviceOrderRecords,serviceOrder,trackingStatus,customerFile);
			}	 
			}catch(Exception e){
				e.printStackTrace();
			}
        //**********end to insert logic for integration log**********
		if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
			String key = (isNew) ? "miscellaneous.added" : "miscellaneous.updated";
			saveMessage(getText(key));
		}
		if (!isNew) {
			hitFlag="1";
			return INPUT;

		} else {
			i = Long.parseLong(miscellaneousManager.findMaximumId().get(0).toString());
			//System.out.println(i);
			miscellaneous = miscellaneousManager.get(i);
			getNotesForIconChange();
			hitFlag="1";
			return SUCCESS;
		}
	}
        
   @SkipValidation
   public List findLinkerShipNumber(ServiceOrder serviceOrder) {
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        	List linkedShipNumberList= new ArrayList();
        	if(serviceOrder.getBookingAgentShipNumber()==null || serviceOrder.getBookingAgentShipNumber().equals("") ){
        		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Primary");
        	}else{
        		linkedShipNumberList=serviceOrderManager.getLinkedShipNumber(serviceOrder.getShipNumber(), "Secondary");
        	}
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        	return linkedShipNumberList;
   }
   
   @SkipValidation 
   public List<Object> findMiscellaneousRecords(List linkedShipNumber,ServiceOrder serviceOrder2) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List<Object> recordList= new ArrayList();
   	Iterator it =linkedShipNumber.iterator();
   	while(it.hasNext()){
   		String shipNumber= it.next().toString();
   		Miscellaneous miscellaneousRemote=miscellaneousManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
   		recordList.add(miscellaneousRemote);
   	}
   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   	return recordList;
	}
   
   @SkipValidation
   public void synchornizeMiscellaneous(List<Object> miscellaneousRecords, Miscellaneous miscellaneous,TrackingStatus trackingStatus) {
		String fieldType="";
		String uniqueFieldType=""; 
   	Iterator  it=miscellaneousRecords.iterator();
   	while(it.hasNext()){
   		Miscellaneous miscellaneousToRecods=(Miscellaneous)it.next();
   		if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
   		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(miscellaneousToRecods.getId());
   		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
   			uniqueFieldType=trackingStatusToRecods.getContractType();
   		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
   			fieldType="CMM/DMM";
   		}
   		List fieldToSync=customerFileManager.findFieldToSync("Miscellaneous",fieldType,uniqueFieldType);
   		fieldType="";
  		    uniqueFieldType=""; 
   		Iterator it1=fieldToSync.iterator();
   		while(it1.hasNext()){
   			String field=it1.next().toString();
   			String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
   			try{
   				//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
   				//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
   				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
   				try{
   				beanUtilsBean.setProperty(miscellaneousToRecods,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneous, fieldFrom.trim()));
   				}catch(Exception ex){
   					ex.printStackTrace();
					}
   				
   			}catch(Exception ex){
   				ex.printStackTrace();
   			}
   			
   		}
   		try{
       	 if(!(miscellaneous.getShipNumber().equals(miscellaneousToRecods.getShipNumber()))){
   		miscellaneousToRecods.setUpdatedBy(miscellaneous.getCorpID()+":"+getRequest().getRemoteUser());
   		miscellaneousToRecods.setUpdatedOn(new Date());
       	 }}catch( Exception e ){
       		 e.printStackTrace();
       	 }
       	miscellaneousToRecods=miscellaneousManager.save(miscellaneousToRecods);
       	ServiceOrder so1=serviceOrderManager.getForOtherCorpid(miscellaneousToRecods.getId());
       	//save for linked dashboard       	
       	//end
   	}
   	}
	}
   
   @SkipValidation
   public List<Object> findTrackingStatusRecords(List linkedShipNumber,	ServiceOrder serviceOrder) {
   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
   	List<Object> recordList= new ArrayList();
   	Iterator it =linkedShipNumber.iterator();
   	while(it.hasNext()){
   		String shipNumber= it.next().toString();
   		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid(serviceOrderManager.findRemoteServiceOrder(shipNumber));
   		//System.out.println("\n\n\n\n serviceOrderRemote----> "+serviceOrderRemote.getId()+"--"+serviceOrderRemote.getCorpID());
   		TrackingStatus trackingStatusRemote=trackingStatusManager.getForOtherCorpid(serviceOrderRemote.getId());
   		recordList.add(trackingStatusRemote);
   	}
   	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
   	return recordList;
   	}
   
   @SkipValidation
   public void synchornizeTrackingStatus(List<Object> records, TrackingStatus trackingStatus,ServiceOrder serviceOrder) {
   	String fieldType="";
		String uniqueFieldType="";

		Iterator  it=records.iterator();
   	while(it.hasNext()){
   		TrackingStatus trackingStatusToRecods=(TrackingStatus)it.next();
   		String forwarderCode="";
   		String brokerCode="";
   	    if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
   	    	forwarderCode=trackingStatusToRecods.getForwarderCode();
   	    }
   	    if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
   	    	brokerCode =trackingStatusToRecods.getBrokerCode();
   	    }
   		if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
   		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
   			uniqueFieldType=trackingStatusToRecods.getContractType();
   		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
   			fieldType="CMM/DMM";
   		}
   		List fieldToSync=customerFileManager.findFieldToSync("TrackingStatus",fieldType,uniqueFieldType);
   		fieldType="";
  		    uniqueFieldType=""; 
   		Iterator it1=fieldToSync.iterator();
   		while(it1.hasNext()){
   			String field=it1.next().toString();
   			String[] splitField=field.split("~");
				String fieldFrom=splitField[0].trim();
				String fieldTo=splitField[1].trim();
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
   			try{
   				beanUtilsBean.setProperty(trackingStatusToRecods,fieldTo,beanUtilsBean.getProperty(trackingStatus, fieldFrom));
   			}catch(Exception ex){
   				ex.printStackTrace();
   			}
   		}
   		try{
              	 if(!(trackingStatus.getShipNumber().equals(trackingStatusToRecods.getShipNumber()))){
       		  trackingStatusToRecods.setUpdatedBy(trackingStatus.getCorpID()+":"+getRequest().getRemoteUser());
       		  trackingStatusToRecods.setUpdatedOn(new Date());
              	 }
           }catch(Exception e){
           	e.printStackTrace();
           }
   		try{
			 if(trackingStatusToRecods.getForwarderCode()!=null && (!(trackingStatusToRecods.getForwarderCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getForwarderCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusToRecods.setForwarderCode("");	
				}
			 }else{
			 if(forwarderCode!=null && (!(forwarderCode.equals("")))){
				 trackingStatusToRecods.setForwarderCode(forwarderCode); 
			 }
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 try{
			 if(trackingStatusToRecods.getBrokerCode()!=null && (!(trackingStatusToRecods.getBrokerCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusToRecods.getCorpID(),trackingStatusToRecods.getBrokerCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusToRecods.setBrokerCode("");	
				}
			 }else{
			 if(brokerCode!=null && (!(brokerCode.equals("")))){
				 trackingStatusToRecods.setBrokerCode(brokerCode); 
			 }
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
           trackingStatusToRecods=trackingStatusManager.save(trackingStatusToRecods);
   	 ServiceOrder serviceOrderRecords1=serviceOrderManager.getForOtherCorpid(trackingStatusToRecods.getId());
	    //saving linked so dashboard info	    
   	}
   	}
   }
   
   @SkipValidation
   public List<Object> findServiceOrderRecords(List linkedShipNumber,	ServiceOrder serviceOrderLocal) {
   	List<Object> recordList= new ArrayList();
   	Iterator it =linkedShipNumber.iterator();
   	while(it.hasNext()){
   		String shipNumber= it.next().toString();
   		ServiceOrder serviceOrderRemote=serviceOrderManager.getForOtherCorpid (serviceOrderManager.findRemoteServiceOrder(shipNumber));
   		recordList.add(serviceOrderRemote);
   	}
   	return recordList;
   	}
   
   @SkipValidation
   public void synchornizeServiceOrder(List<Object> records, ServiceOrder serviceOrder,TrackingStatus trackingStatus,CustomerFile customerFile) {
		String fieldType="";
		String uniqueFieldType="";
		 
   	Iterator  it=records.iterator();
   	while(it.hasNext()){
   		ServiceOrder serviceOrderToRecods=(ServiceOrder)it.next();
   		String soJobType = serviceOrderToRecods.getJob();
   		if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
   		TrackingStatus trackingStatusToRecods=trackingStatusManager.getForOtherCorpid(serviceOrderToRecods.getId());
   		if(trackingStatus.getContractType()!=null && (!(trackingStatus.getContractType().toString().trim().equals(""))) && trackingStatusToRecods.getContractType()!=null && (!(trackingStatusToRecods.getContractType().toString().trim().equals(""))) )
   			uniqueFieldType=trackingStatusToRecods.getContractType();
   		if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
   			fieldType="CMM/DMM";
   		}
   		List fieldToSync=customerFileManager.findFieldToSync("ServiceOrder",fieldType,uniqueFieldType);
   		fieldType="";
  		    uniqueFieldType=""; 
   		Iterator it1=fieldToSync.iterator();
   		while(it1.hasNext()){
   			String field=it1.next().toString();
   			String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
   			try{
   				//BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
   				//beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
   				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
   				try{
   				beanUtilsBean.setProperty(serviceOrderToRecods,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrder, fieldFrom.trim()));
   				}catch(Exception ex){
   					ex.printStackTrace();
					}
   				
   			}catch(Exception ex){
   				ex.printStackTrace();
   			}
   			
   		}
   		try{
   	    if(!(serviceOrder.getShipNumber().equals(serviceOrderToRecods.getShipNumber()))){
   	    if(serviceOrder.getJob()!=null && (!(serviceOrder.getJob().toString().equals(""))) && (serviceOrder.getJob().toString().equalsIgnoreCase("RLO")) ) {
       	    	serviceOrderToRecods.setServiceType(serviceOrder.getServiceType());	
       	}
   		serviceOrderToRecods.setUpdatedBy(serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());
   		serviceOrderToRecods.setUpdatedOn(new Date());
   	    }}catch( Exception e){
   	    e.printStackTrace();
   	    }
   	    try{
   	    	if(!networkAgent){
   	    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
   	    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
   	    		/*List aliasNameList=userManager.findCoordinatorByUsername(serviceOrder.getCoordinator(),sessionCorpID);
					if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
						serviceOrderToRecods.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
					}*/
   	    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
       		}
   	    	}
   	    	}
   	    	if(networkAgent){ 
       	    	if(trackingStatus.getSoNetworkGroup() && trackingStatusToRecods.getSoNetworkGroup()){
       	    		if(serviceOrder.getCoordinator()!=null && (!(serviceOrder.getCoordinator().toString().equals("")))){
       	    		/*List aliasNameList=userManager.findCoordinatorByNetworkCoordinator(serviceOrder.getCoordinator(),serviceOrderToRecods.getCorpID());
   					if(aliasNameList!=null && (!(aliasNameList.isEmpty())) && aliasNameList.get(0)!=null && (!(aliasNameList.get(0).toString().trim().equals("")))){
   						serviceOrderToRecods.setCoordinator(aliasNameList.get(0).toString().trim().toUpperCase());	
   					}*/
       	    			serviceOrderToRecods.setCoordinator(serviceOrder.getCoordinator());
           		}
       	    	}
       	    	
   	    		
   	    	}	
   	    }catch( Exception e){
   	    	e.printStackTrace();
   	    }
   	    if(serviceOrderToRecods.getCorpID().toString().equalsIgnoreCase("UGWW"))
   	    	serviceOrderToRecods.setJob(soJobType);
   	    
   		 serviceOrderManager.save(serviceOrderToRecods);
   		 try{
   			 CustomerFile customerFileToRecods=customerFileManager.getForOtherCorpid(serviceOrderToRecods.getCustomerFileId());
   			if(customerFile.getContractType()!=null && (!(customerFile.getContractType().toString().trim().equals(""))) && (customerFile.getContractType().toString().trim().equalsIgnoreCase("DMM") || customerFile.getContractType().toString().trim().equalsIgnoreCase("CMM") ) && customerFileToRecods.getContractType()!=null &&(!(customerFileToRecods.getContractType().toString().trim().equals(""))) && (customerFileToRecods.getContractType().toString().trim().equalsIgnoreCase("DMM") || customerFileToRecods.getContractType().toString().trim().equalsIgnoreCase("CMM"))){
   				customerFileManager.updateLinkedCustomerFileStatus(customerFile.getStatus(),customerFile.getStatusNumber(),customerFile.getStatusDate(),serviceOrderToRecods.getCustomerFileId(),serviceOrder.getCorpID()+":"+getRequest().getRemoteUser());	
   			}
   		 }catch( Exception e){
    	    e.printStackTrace();
    	    }
   	}
   	}	
   	}
        
	@SkipValidation
	public String getNotesForIconChange() {
		List mo = notesManager.countForDomesticNotes(serviceOrder.getShipNumber());
		List md = notesManager.countForDomesticServiceNotes(serviceOrder.getShipNumber());
		List mc = notesManager.countForDomesticStateCountryNotes(serviceOrder.getShipNumber());
		List mis = notesManager.countInterStateNotes(serviceOrder.getShipNumber());
		if (mo.isEmpty()) {
			countForDomesticNotes = "0";
		} else {
			countForDomesticNotes = ((mo).get(0)).toString();
		}
		if (md.isEmpty()) {
			countForDomesticServiceNotes = "0";
		} else {
			countForDomesticServiceNotes = ((md).get(0)).toString();
		}
		if (mc.isEmpty()) {
			countForDomesticStateCountryNotes = "0";
		} else {
			countForDomesticStateCountryNotes = ((mc).get(0)).toString();
		}
		if (mis.isEmpty()) {
			countInterStateNotes = "0";
		} else {
			countInterStateNotes = ((mis).get(0)).toString();
		}

		return SUCCESS;
	}
	
	@SkipValidation
	public String updateTripStatus() {
		String tempTripNumber="";
		String tempTripName="";
		maxTripNumber=miscellaneousManager.findmaxTripNumber(sessionCorpID);
	List companyTripNumber=miscellaneousManager.findTripName(sessionCorpID);
		if(maxTripNumber!=null && !maxTripNumber.isEmpty() && !maxTripNumber.get(0).toString().equals("")){
			autoTripNumber=Long.parseLong(maxTripNumber.get(0).toString().trim())+1;
			tempTripNumber=""+autoTripNumber;
			tempTripNumber=tempTripNumber.trim();
		}else{
			if(companyTripNumber!=null && !companyTripNumber.isEmpty() && companyTripNumber.get(0)!=null && !companyTripNumber.get(0).equals("")){
		       autoTripNumber=Long.parseLong(companyTripNumber.get(0).toString().trim());
		       tempTripName=""+autoTripNumber;
		       tempTripNumber=tempTripName.trim();
			}else{
				tempTripNumber="10001";	
			}
			//tempTripNumber="Trip10001";
		}
		miscellaneousManager.updateTripNumber(tempTripNumber,tripIds);
		String key="TRIP successfully created "+tempTripNumber+" ";
		saveMessage(getText(key));
		return SUCCESS;
	}
	@SkipValidation
	public String updateJobStatus(){
		miscellaneousManager.updateJobStatus(sessionCorpID,soStatus,orderNo);
		return SUCCESS;
	}
	/*private List tripOpenList;
	@SkipValidation
	public String getopenTripList(){
		tripOpenList=miscellaneousManager.getOpenTripList(tripNumber,sessionCorpID);
		return SUCCESS;
	}*/
	@SkipValidation
	public String clearTripStatus() {
		miscellaneousManager.removeTripNumber(tripIds);
		saveMessage(getText("clearTrip"));
		return SUCCESS;
	}
	
   private List tripOrderList;
   private String tripNumber;
	public String getTripOrdersList(){
		tripOrderList=miscellaneousManager.getTripOrder(tripNumber, sessionCorpID);
		return SUCCESS;
	}
	private List TripNumberList;
	@SkipValidation
	public String getTripList(){
		TripNumberList=miscellaneousManager.getTripNumber(tripNumber, sessionCorpID);
		return SUCCESS;
	}
	public String getFromZip() {
		return fromZip;
	}
	public void setFromZip(String fromZip) {
		this.fromZip = fromZip;
	}
	public String getToZip() {
		return toZip;
	}
	public void setToZip(String toZip) {
		this.toZip = toZip;
	}
	public String getFromRadius() {
		return fromRadius;
	}
	public void setFromRadius(String fromRadius) {
		this.fromRadius = fromRadius;
	}
	public String getToRadius() {
		return toRadius;
	}
	public void setToRadius(String toRadius) {
		this.toRadius = toRadius;
	}

	
	
	@SkipValidation
	public String haulingMgtForm(){		
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yy");  
		Calendar cal = Calendar.getInstance();
		Calendar cal1 = Calendar.getInstance();
		cal.add(Calendar.DATE, -14);	
		fromdateF = dateformat.format(cal.getTime());
		cal1.add(Calendar.DATE,28);	
		todateF = dateformat.format(cal1.getTime());
		opshub=refMasterManager.occupied("OPSHUB", sessionCorpID);
		driverAgencyList=miscellaneousManager.getDriverAgency(sessionCorpID);
		radiusList = refMasterManager.findByParameterRadius(sessionCorpID, "RADIUS");
		jobTypes = refMasterManager.findByParameterRadius(sessionCorpID, "HAULINGJOB");
		/*jobTypes = new ArrayList();
		jobTypes.add("UVL");
		jobTypes.add("MVL");
		jobTypes.add("VAI");*/
		return SUCCESS;
	}
   
	@SkipValidation
	public String haulingMgtList(){	
		String geoLatitude = "";
		String geoLongitude = "";
		String maxLatitude = "";
		String minLatitude = "";
		String maxLogitude = "";
		String minLogitude = "";
		String fromZipCodes = "";
		String toZipCodes = "";
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yy");
		fromdateF = dateformat.format(fromDate);		
		todateF = dateformat.format(toDate);
		opshub=refMasterManager.occupied("OPSHUB", sessionCorpID);
		driverAgencyList=miscellaneousManager.getDriverAgency(sessionCorpID);
		radiusList = refMasterManager.findByParameterRadius(sessionCorpID, "RADIUS");
		jobTypes = refMasterManager.findByParameterRadius(sessionCorpID, "HAULINGJOB");
	/*	jobTypes = new ArrayList();
		jobTypes.add("UVL");
		jobTypes.add("MVL");
		jobTypes.add("VAI");*/
		
		if(fromZip!=null && !fromZip.equalsIgnoreCase("")){		
		List latLongList = miscellaneousManager.findLattLongList(sessionCorpID,fromZip);
			if(latLongList!=null && !latLongList.isEmpty()){
			String str[]=latLongList.get(0).toString().split("#");
			geoLatitude = str[0];
			geoLongitude = str[1];	
			if(!geoLatitude.equals("")){
				if(!fromRadius.equals("")){
					Double val1 =Double.parseDouble(geoLatitude)-((Double.parseDouble(fromRadius)/(68.70768)));
					Double val2 =Double.parseDouble(geoLatitude)+((Double.parseDouble(fromRadius)/(68.70768)));
					if(val1>val2)
					{
						minLatitude= val2.toString();				
						maxLatitude= val1.toString();				
					}
					else
					{
						minLatitude= val1.toString();				
						maxLatitude= val2.toString();				
					}
			    }else{
					maxLatitude= geoLatitude;	
			    }				
			}

			if(!geoLongitude.equals("")){
				if(!fromRadius.equals("")){					
					Double val1 =Double.parseDouble(geoLongitude)-((Double.parseDouble(fromRadius)/(69.17073)));
					Double val2 =Double.parseDouble(geoLongitude)+((Double.parseDouble(fromRadius)/(69.17073)));
					if(val1>val2)
					{
						minLogitude= val2.toString();				
						maxLogitude= val1.toString();				
					}
					else
					{
						minLogitude= val1.toString();				
						maxLogitude= val2.toString();				
					}
			    }else{				
				maxLogitude= geoLongitude;
			    }
			  }
			}else{
				    String key="From ZipCode is not valid";
					saveMessage(getText(key));
					return INPUT;
			}
			List zipCodeList = miscellaneousManager.findZipCodeList(sessionCorpID,maxLatitude,minLatitude,maxLogitude,minLogitude); 
			if(zipCodeList!=null && !zipCodeList.isEmpty()){
				Iterator it = zipCodeList.iterator();
		         while (it.hasNext()) {
		        	 fromZipCodes = fromZipCodes +","+ (it.next().toString());   	 	            
		         }
					fromZipCodes = fromZipCodes.substring(1,fromZipCodes.length());
			}
		}
		if(toZip!=null && !toZip.equalsIgnoreCase("")){		
			 List latLongList = miscellaneousManager.findLattLongList(sessionCorpID,toZip);
				if(latLongList!=null && !latLongList.isEmpty()){
				String str[]=latLongList.get(0).toString().split("#");
				geoLatitude = str[0];
				geoLongitude = str[1];	
				if(!geoLatitude.equals("")){
					if(!toRadius.equals("")){
						Double val1 =Double.parseDouble(geoLatitude)-((Double.parseDouble(toRadius)/(68.70768)));
						Double val2 =Double.parseDouble(geoLatitude)+((Double.parseDouble(toRadius)/(68.70768)));
						if(val1>val2)
						{
							minLatitude= val2.toString();				
							maxLatitude= val1.toString();				
						}
						else
						{
							minLatitude= val1.toString();				
							maxLatitude= val2.toString();				
						}
				    }else{
					maxLatitude= geoLatitude;
				    }
				}

				if(!geoLongitude.equals("")){
					if(!toRadius.equals("")){					
						Double val1 =Double.parseDouble(geoLongitude)-((Double.parseDouble(toRadius)/(69.17073)));
						Double val2 =Double.parseDouble(geoLongitude)+((Double.parseDouble(toRadius)/(69.17073)));
						if(val1>val2)
						{
							minLogitude= val2.toString();				
							maxLogitude= val1.toString();				
						}
						else
						{
							minLogitude= val1.toString();				
							maxLogitude= val2.toString();				
						}
				    }else{
					maxLogitude= geoLongitude;
				    }
				  }
				}else{
					String key="To ZipCode is not valid!";
					saveMessage(getText(key));
					return INPUT;
				}
				List zipCodeList = miscellaneousManager.findZipCodeList(sessionCorpID,maxLatitude,minLatitude,maxLogitude,minLogitude); 
				if(zipCodeList!=null && !zipCodeList.isEmpty()){
					Iterator it = zipCodeList.iterator();
			         while (it.hasNext()) {
			        	 toZipCodes = toZipCodes +","+ (it.next().toString());   	 	            
			         }
			         toZipCodes = toZipCodes.substring(1,toZipCodes.length());
				}
			}
		
		String[] arrayDriverType = selDriverAgency.split(",");
		int arrayLength = arrayDriverType.length;
		selectedDriverTypeList = new ArrayList();
		for (int i = 0; i < arrayLength; i++) {
			selectedDriverTypeList.add(arrayDriverType[i]);
		}
		String[] arrayJobTypes = selJobTypes.split(",");
		int arrayLength1 = arrayJobTypes.length;
		selectedJobTypesList = new ArrayList();
		for (int i = 0; i < arrayLength1; i++) {
			selectedJobTypesList.add(arrayJobTypes[i]);
		}
		String tempjob="";
		if((selectedJob.equals("''")) || (selectedJob.equals(""))){
			//selectedJob=jobTypes.toString();
			if(jobTypes.size()>0){
				Iterator it=jobTypes.iterator();
				while(it.hasNext()){
					if(tempjob.equals("")){
						tempjob = "''"+",'"+it.next().toString()+"'";
					}else{
						tempjob =tempjob+ ",'"+it.next().toString()+"'";
					}
				}
			}
			selectedJob=tempjob;
		}
		String DriverAgency="";
		if((selectedDriverAgency.equals("''")) || (selectedDriverAgency.equals(""))){
			//selectedJob=jobTypes.toString();
			if(driverAgencyList != null && driverAgencyList.size()>0){
				for (Map.Entry<String,String> entry : driverAgencyList.entrySet()) {
				    if(DriverAgency.equals("")){
						DriverAgency = "''" + ",'"+entry.getKey().toString()+"'";
					}else{
						DriverAgency =DriverAgency+ ",'"+entry.getKey().toString()+"'";
					}
				}
			}
			selectedDriverAgency=DriverAgency;
		}
		haulingMgt = miscellaneousManager.findHaulingMgtList(fromDate, toDate, sessionCorpID,filterdDrivers,opsHubList,selectedDriverAgency,selfHaul,fromArea,toArea,fromZipCodes,toZipCodes,fromRadius,toRadius,selectedJob,filterdTripNumber,jobStatus,filterdRegumber);
		return SUCCESS;
	}
	public void exportHaulingMgt() throws Exception{
		String geoLatitude = "";
		String geoLongitude = "";
		String maxLatitude = "";
		String minLatitude = "";
		String maxLogitude = "";
		String minLogitude = "";
		String fromZipCodes = "";
		String toZipCodes = "";
		SimpleDateFormat dateformat = new SimpleDateFormat("dd-MMM-yy");
		fromdateF = dateformat.format(fromDate);		
		todateF = dateformat.format(toDate);
		if(fromZip!=null && !fromZip.equalsIgnoreCase("")){	
			List latLongList = miscellaneousManager.findLattLongList(sessionCorpID,fromZip);
			if(latLongList!=null && !latLongList.isEmpty()){
				String str[]=latLongList.get(0).toString().split("#");
				geoLatitude = str[0];
				geoLongitude = str[1];	
				if(!geoLatitude.equals("")){
					if(!fromRadius.equals("")){
						Double val1 =Double.parseDouble(geoLatitude)-((Double.parseDouble(fromRadius)/(68.70768)));
						Double val2 =Double.parseDouble(geoLatitude)+((Double.parseDouble(fromRadius)/(68.70768)));
						if(val1>val2)
						{
							minLatitude= val2.toString();				
							maxLatitude= val1.toString();				
						}
						else
						{
							minLatitude= val1.toString();				
							maxLatitude= val2.toString();				
						}
				    }else{
						maxLatitude= geoLatitude;	
				    }				
				}
				if(!geoLongitude.equals("")){
					if(!fromRadius.equals("")){					
						Double val1 =Double.parseDouble(geoLongitude)-((Double.parseDouble(fromRadius)/(69.17073)));
						Double val2 =Double.parseDouble(geoLongitude)+((Double.parseDouble(fromRadius)/(69.17073)));
						if(val1>val2)
						{
							minLogitude= val2.toString();				
							maxLogitude= val1.toString();				
						}
						else
						{
							minLogitude= val1.toString();				
							maxLogitude= val2.toString();				
						}
				    }else{				
					maxLogitude= geoLongitude;
				    }
					if(!geoLongitude.equals("")){
						if(!toRadius.equals("")){					
							Double val1 =Double.parseDouble(geoLongitude)-((Double.parseDouble(toRadius)/(69.17073)));
							Double val2 =Double.parseDouble(geoLongitude)+((Double.parseDouble(toRadius)/(69.17073)));
							if(val1>val2)
							{
								minLogitude= val2.toString();				
								maxLogitude= val1.toString();				
							}
							else
							{
								minLogitude= val1.toString();				
								maxLogitude= val2.toString();				
							}
					    }else{
						maxLogitude= geoLongitude;
					    }
					  }
				  }
				List zipCodeList = miscellaneousManager.findZipCodeList(sessionCorpID,maxLatitude,minLatitude,maxLogitude,minLogitude); 
				if(zipCodeList!=null && !zipCodeList.isEmpty()){
					Iterator it = zipCodeList.iterator();
			         while (it.hasNext()) {
			        	 fromZipCodes = fromZipCodes +","+ (it.next().toString());   	 	            
			         }
						fromZipCodes = fromZipCodes.substring(1,fromZipCodes.length());
				}
			}
		}
		if(toZip!=null && !toZip.equalsIgnoreCase("")){	
			 List latLongList = miscellaneousManager.findLattLongList(sessionCorpID,toZip);
				if(latLongList!=null && !latLongList.isEmpty()){
					String str[]=latLongList.get(0).toString().split("#");
					geoLatitude = str[0];
					geoLongitude = str[1];	
				}
				if(!geoLatitude.equals("")){
					if(!toRadius.equals("")){
						Double val1 =Double.parseDouble(geoLatitude)-((Double.parseDouble(toRadius)/(68.70768)));
						Double val2 =Double.parseDouble(geoLatitude)+((Double.parseDouble(toRadius)/(68.70768)));
						if(val1>val2)
						{
							minLatitude= val2.toString();				
							maxLatitude= val1.toString();				
						}
						else
						{
							minLatitude= val1.toString();				
							maxLatitude= val2.toString();				
						}
				    }else{
					maxLatitude= geoLatitude;
				    }
				}
				List zipCodeList = miscellaneousManager.findZipCodeList(sessionCorpID,maxLatitude,minLatitude,maxLogitude,minLogitude); 
				if(zipCodeList!=null && !zipCodeList.isEmpty()){
					Iterator it = zipCodeList.iterator();
			         while (it.hasNext()) {
			        	 toZipCodes = toZipCodes +","+ (it.next().toString());   	 	            
			         }
			         toZipCodes = toZipCodes.substring(1,toZipCodes.length());
				}
		}
		haulingMgt = miscellaneousManager.findHaulingMgtList(fromDate, toDate, sessionCorpID,filterdDrivers,opsHubList,selectedDriverAgency,selfHaul,fromArea,toArea,fromZipCodes,toZipCodes,fromRadius,toRadius,selectedJob,filterdTripNumber,jobStatus,filterdRegumber);
		String str[]= new String[]{"Id","Driver","Shipper","Job","Order No","Origin","Destination","Load Beg","Load End","Delivery Beg","Delivery End","ETA","Driver Paper","Est.Wgt","Est.Rev.","Hauling VanLine Code","Self Haul","From Area","To Area","Pack Authorize","Tariff","Register","Survey","Discount","MIL","Trip No","Origin Agent","Destination Agent","Origin Agent Address","Destination Agent Address","Pack Beg","Pack End","Pack Count","Delivery Act","Unpack","Van#","SIT @ Dest","G11","Ship Type","Origin Agent","Destination Agent","Status"};
		if(!haulingMgt.isEmpty()){
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "Hauling Management", str, haulingMgt, "Hauling Info", ",");
		}
		
		
	}
	/* * 
	@SkipValidation
	public String findZipCodeList(){
		zipCodeList = miscellaneousManager.findZipCodeList(sessionCorpID,lattitude1,lattitude2,longitude1,longitude2);
		return SUCCESS;
	}
	@SkipValidation
	public String findlatLongList(){
		latLongList = miscellaneousManager.findLattLongList(sessionCorpID,zipCode);
		if(latLongList!=null && !latLongList.isEmpty()){
			String str[]=latLongList.get(0).toString().split("#");
			latLong = str[0]+"~"+str[1];
		}
		return SUCCESS;
	}
	
   private List driverLocList;
	    
    @SkipValidation
	public String driverLocationPoint(){
    	
		return SUCCESS;
	}
	public InputStream getDriverLocationPointMarkup()
	{
		ByteArrayInputStream output = null;
		if(driverLocationId==null)
		{
			driverLocationId="";
		}
		driverLocList = miscellaneousManager.getDriverLocationList(sessionCorpID,driverLocationId);
		if(driverLocList!=null)
		  {
	  		StringBuilder ports = new StringBuilder();
			int count = 0;
			Iterator it=driverLocList.iterator();
			
			while(it.hasNext()){
				count++;
				Object []obj=(Object[]) it.next();
				if (count > 1)
					ports.append(",");
				else
					ports.append("");
				ports.append("{\"vanLastLocation\": \"" + obj[0] + "\", \"vanAvailCube\": \"" + obj[1] + "\", \"vanLastReportOn\": \"" + obj[2] +  "\", \"vanLastReportTime\": \"" + obj[3] + "\"}");
			}
			
			String prependStr = "{ \"count\":" + count + ", \"ports\":[";
			String appendStr = "]}";
			String s=prependStr + ports.toString() + appendStr;
    		        output=new ByteArrayInputStream(s.getBytes());		  
    	   }
		return output;
	}

	public String driverLocationId;
	@SkipValidation	
	public String searchDriverLocation() {
		if(driverLocationId=="")
		{
		String key = "Please enter your search criteria below";
		saveMessage(getText(key));
		}
		return SUCCESS;

	}*/
	private List tooltipList;
	@SkipValidation
	public String findToolTipForVehicle(){
		tooltipList=miscellaneousManager.findToolTipForVehicle(orderNo, sessionCorpID, job);
		return SUCCESS;
	}
	
	public  CustomerFile getCustomerFile() {
		return (customerFile!=null ?customerFile:customerFileManager.get(customerFileId));
	}

	public  void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public TrackingStatus getTrackingStatus() {
					return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getCountForDomesticNotes() {
		return countForDomesticNotes;
	}

	public void setCountForDomesticNotes(String countForDomesticNotes) {
		this.countForDomesticNotes = countForDomesticNotes;
	}

	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public String getCountForDomesticServiceNotes() {
		return countForDomesticServiceNotes;
	}

	public void setCountForDomesticServiceNotes(String countForDomesticServiceNotes) {
		this.countForDomesticServiceNotes = countForDomesticServiceNotes;
	}

	public String getCountForDomesticStateCountryNotes() {
		return countForDomesticStateCountryNotes;
	}

	public void setCountForDomesticStateCountryNotes(String countForDomesticStateCountryNotes) {
		this.countForDomesticStateCountryNotes = countForDomesticStateCountryNotes;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}
	public Date getBeginPacking() {
		trackingStatus=trackingStatusManager.get(sid);
		return( beginPacking!=null )?beginPacking:trackingStatus.getBeginPacking();
	}
	public void setBeginPacking(Date beginPacking) {
		this.beginPacking = beginPacking;
	}
	public Date getEndPacking() {
		trackingStatus=trackingStatusManager.get(sid);
		return( endPacking!=null )?endPacking:trackingStatus.getEndPacking();
	}
	public void setEndPacking(Date endPacking) {
		this.endPacking = endPacking;
	}
	public Date getBeginLoad() {
		trackingStatus=trackingStatusManager.get(sid);
		return( beginLoad!=null )?beginLoad:trackingStatus.getBeginLoad();
	}
	public void setBeginLoad(Date beginLoad) {
		this.beginLoad = beginLoad;
	}
	public Date getDeliveryA() {
		trackingStatus=trackingStatusManager.get(sid);
		return( deliveryA!=null )?deliveryA:trackingStatus.getDeliveryA();
	}
	public void setDeliveryA(Date deliveryA) {
		this.deliveryA = deliveryA;
	}
	public Date getDeliveryLastDay() {
		trackingStatus=trackingStatusManager.get(sid);
		return( deliveryLastDay!=null )?deliveryLastDay:trackingStatus.getDeliveryLastDay();
	}
	public void setDeliveryLastDay(Date deliveryLastDay) {
		this.deliveryLastDay = deliveryLastDay;
	}
	public Date getDeliveryShipper() {
		trackingStatus=trackingStatusManager.get(sid);
		return( deliveryShipper!=null )?deliveryShipper:trackingStatus.getDeliveryShipper();
	}
	public void setDeliveryShipper(Date deliveryShipper) {
		this.deliveryShipper = deliveryShipper;
	}
	public Date getDeliveryTA() {
		trackingStatus=trackingStatusManager.get(sid);
		return( deliveryTA!=null )?deliveryTA:trackingStatus.getDeliveryTA();
	}
	public void setDeliveryTA(Date deliveryTA) {
		this.deliveryTA = deliveryTA;
	}
	public Date getEndLoad() {
		trackingStatus=trackingStatusManager.get(sid);
		return( endLoad!=null )?endLoad:trackingStatus.getEndLoad();
	}
	public void setEndLoad(Date endLoad) {
		this.endLoad = endLoad;
	}
	public Date getLoadA() {
		trackingStatus=trackingStatusManager.get(sid);
		return( loadA!=null )?loadA:trackingStatus.getLoadA();
	}
	public void setLoadA(Date loadA) {
		this.loadA = loadA;
	}
	public Date getPackA() {
		trackingStatus=trackingStatusManager.get(sid);
		return( packA!=null )?packA:trackingStatus.getPackA();
	}
	public void setPackA(Date packA) {
		this.packA = packA;
	}
	public String getRedirected() {
		return redirected;
	}
	public void setRedirected(String redirected) {
		this.redirected = redirected;
	}
	public Map<String, String> getDOMINST() {
		return DOMINST;
	}
	public String getCountInterStateNotes() {
		return countInterStateNotes;
	}
	public void setCountInterStateNotes(String countInterStateNotes) {
		this.countInterStateNotes = countInterStateNotes;
	}
	public Long getSid() {
		return sid;
	}
	public void setSid(Long sid) {
		this.sid = sid;
	}
	/**
	 * @return the eQUIP
	 */
	public Map<String, String> getEQUIP() {
		return EQUIP;
	}
	/**
	 * @return the hitFlag
	 */
	public String getHitFlag() {
		return hitFlag;
	}
	/**
	 * @param hitFlag the hitFlag to set
	 */
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public String getCountShip() {
		return countShip;
	}
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}
	public List getCustomerSO() {
		return customerSO;
	}
	public void setCustomerSO(List customerSO) {
		this.customerSO = customerSO;
	}
	public String getMinShip() {
		return minShip;
	}
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}
	public String getShipSize() {
		return shipSize;
	}
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	/**
	 * @return the sitDestinationIn
	 */
	public Date getSitDestinationIn() {
		return sitDestinationIn;
	}
	/**
	 * @param sitDestinationIn the sitDestinationIn to set
	 */
	public void setSitDestinationIn(Date sitDestinationIn) {
		this.sitDestinationIn = sitDestinationIn;
	}
	/**
	 * @return the sitDestinationA
	 */
	public Date getSitDestinationA() {
		return sitDestinationA;
	}
	/**
	 * @param sitDestinationA the sitDestinationA to set
	 */
	public void setSitDestinationA(Date sitDestinationA) {
		this.sitDestinationA = sitDestinationA;
	}
	/**
	 * @return the sitDestinationTA
	 */
	public String getSitDestinationTA() {
		return sitDestinationTA;
	}
	/**
	 * @param sitDestinationTA the sitDestinationTA to set
	 */
	public void setSitDestinationTA(String sitDestinationTA) {
		this.sitDestinationTA = sitDestinationTA;
	}
	/**
	 * @return the sitDestinationDays
	 */
	public int getSitDestinationDays() {
		return sitDestinationDays;
	}
	/**
	 * @param sitDestinationDays the sitDestinationDays to set
	 */
	public void setSitDestinationDays(int sitDestinationDays) {
		this.sitDestinationDays = sitDestinationDays;
	}
	
	public List getHaulingMgt() {
		return haulingMgt;
	}
	public void setHaulingMgt(List haulingMgt) {
		this.haulingMgt = haulingMgt;
	}
	public String getFromdateF() {
		return fromdateF;
	}
	public void setFromdateF(String fromdateF) {
		this.fromdateF = fromdateF;
	}
	public String getTodateF() {
		return todateF;
	}
	public void setTodateF(String todateF) {
		this.todateF = todateF;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	/**
	 * @return the findByOwnerList
	 */
	public List getFindByOwnerList() {
		return findByOwnerList;
	}
	/**
	 * @param findByOwnerList the findByOwnerList to set
	 */
	public void setFindByOwnerList(List findByOwnerList) {
		this.findByOwnerList = findByOwnerList;
	}
	/**
	 * @return the partnerCode
	 */
	public String getPartnerCode() {
		return partnerCode;
	}
	/**
	 * @param partnerCode the partnerCode to set
	 */
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	/**
	 * @return the findByOwnerFromList
	 */
	public List getFindByOwnerFromList() {
		return findByOwnerFromList;
	}
	/**
	 * @param findByOwnerFromList the findByOwnerFromList to set
	 */
	public void setFindByOwnerFromList(List findByOwnerFromList) {
		this.findByOwnerFromList = findByOwnerFromList;
	}
	/**
	 * @return the truckNumber
	 */
	public String getTruckNumber() {
		return truckNumber;
	}
	/**
	 * @param truckNumber the truckNumber to set
	 */
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	/**
	 * @return the findByOwnerPerticularList
	 */
	public List getFindByOwnerPerticularList() {
		return findByOwnerPerticularList;
	}
	/**
	 * @param findByOwnerPerticularList the findByOwnerPerticularList to set
	 */
	public void setFindByOwnerPerticularList(List findByOwnerPerticularList) {
		this.findByOwnerPerticularList = findByOwnerPerticularList;
	}
	/**
	 * @return the partners
	 */
	public List getPartners() {
		return partners;
	}
	/**
	 * @param partners the partners to set
	 */
	public void setPartners(List partners) {
		this.partners = partners;
	}
	/**
	 * @param partnerManager the partnerManager to set
	 */
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	public List getStatus() {
		return status;
	}
	public void setStatus(List status) {
		this.status = status;
	}
	public List getFindSalesStatus() {
		return findSalesStatus;
	}
	public void setFindSalesStatus(List findSalesStatus) {
		this.findSalesStatus = findSalesStatus;
	}
	public List getFindByTrailerPerticularList() {
		return findByTrailerPerticularList;
	}
	public void setFindByTrailerPerticularList(List findByTrailerPerticularList) {
		this.findByTrailerPerticularList = findByTrailerPerticularList;
	}
	public String getFilterdDrivers() {
		return filterdDrivers;
	}
	public void setFilterdDrivers(String filterdDrivers) {
		this.filterdDrivers = filterdDrivers;
	}
	public List<String> getOpshub() {
		return opshub;
	}
	public void setOpshub(List<String> opshub) {
		this.opshub = opshub;
	}
	
	public String getOpsHubList() {
		return opsHubList;
	}
	public void setOpsHubList(String opsHubList) {
		this.opsHubList = opsHubList;
	}
	public String getDriverAgency() {
		return driverAgency;
	}
	public void setDriverAgency(String driverAgency) {
		this.driverAgency = driverAgency;
	}
	public Map<String, String> getDriverAgencyList() {
		return driverAgencyList;
	}
	public void setDriverAgencyList(Map<String, String> driverAgencyList) {
		this.driverAgencyList = driverAgencyList;
	}
	public List getSelectedDriverTypeList() {
		return selectedDriverTypeList;
	}
	public void setSelectedDriverTypeList(List selectedDriverTypeList) {
		this.selectedDriverTypeList = selectedDriverTypeList;
	}
	public String getSelfHaul() {
		return selfHaul;
	}
	public void setSelfHaul(String selfHaul) {
		this.selfHaul = selfHaul;
	}
	/*public String getDriverLocationId() {
		return driverLocationId;
	}
	public void setDriverLocationId(String driverLocationId) {
		this.driverLocationId = driverLocationId;
	}
	public List getDriverLocList() {
		return driverLocList;
	}
	public void setDriverLocList(List driverLocList) {
		this.driverLocList = driverLocList;
	}
	*/
	public Map getFromAreaList() {
		return fromAreaList;
	}
	public void setFromAreaList(Map fromAreaList) {
		this.fromAreaList = fromAreaList;
	}
	public Map getToAreaList() {
		return toAreaList;
	}
	public void setToAreaList(Map toAreaList) {
		this.toAreaList = toAreaList;
	}
	public String getFromArea() {
		return fromArea;
	}
	public void setFromArea(String fromArea) {
		this.fromArea = fromArea;
	}
	public String getToArea() {
		return toArea;
	}
	public void setToArea(String toArea) {
		this.toArea = toArea;
	}
	

	public Map<String, String> getLateReasonList() {
		return lateReasonList;
	}
	public String getLateReason() {
		return lateReason;
	}
	public void setLateReason(String lateReason) {
		this.lateReason = lateReason;
	}
	public String getPartyResponsible() {
		return partyResponsible;
	}
	public void setPartyResponsible(String partyResponsible) {
		this.partyResponsible = partyResponsible;
	}
	public String getPackATime() {
		return packATime;
	}
	public void setPackATime(String packATime) {
		this.packATime = packATime;
	}
	public String getDeliveryATime() {
		return deliveryATime;
	}
	public void setDeliveryATime(String deliveryATime) {
		this.deliveryATime = deliveryATime;
	}
	public String getDeliveryTATime() {
		return deliveryTATime;
	}
	public void setDeliveryTATime(String deliveryTATime) {
		this.deliveryTATime = deliveryTATime;
	}
	public String getLoadATime() {
		return loadATime;
	}
	public void setLoadATime(String loadATime) {
		this.loadATime = loadATime;
	}
	public List getZipCodeList() {
		return zipCodeList;
	}
	public void setZipCodeList(List zipCodeList) {
		this.zipCodeList = zipCodeList;
	}
	
	public String getZipCodes() {
		return zipCodes;
	}
	public void setZipCodes(String zipCodes) {
		this.zipCodes = zipCodes;
	}

	public List getLatLongList() {
		return latLongList;
	}
	public void setLatLongList(List latLongList) {
		this.latLongList = latLongList;
	}
	public String getLatLong() {
		return latLong;
	}
	public void setLatLong(String latLong) {
		this.latLong = latLong;
	}

	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public List getRadiusList() {
		return radiusList;
	}
	public void setRadiusList(List radiusList) {
		this.radiusList = radiusList;
	}
	public String getSelectedDriverAgency() {
		return selectedDriverAgency;
	}
	public void setSelectedDriverAgency(String selectedDriverAgency) {
		this.selectedDriverAgency = selectedDriverAgency;
	}
	public String getSelDriverAgency() {
		return selDriverAgency;
	}
	public void setSelDriverAgency(String selDriverAgency) {
		this.selDriverAgency = selDriverAgency;
	}
	public List getJobTypes() {
		return jobTypes;
	}
	public void setJobTypes(List jobTypes) {
		this.jobTypes = jobTypes;
	}
	public String getSelJobTypes() {
		return selJobTypes;
	}
	public void setSelJobTypes(String selJobTypes) {
		this.selJobTypes = selJobTypes;
	}
	public List getSelectedJobTypesList() {
		return selectedJobTypesList;
	}
	public void setSelectedJobTypesList(List selectedJobTypesList) {
		this.selectedJobTypesList = selectedJobTypesList;
	}
	public String getSelectedJob() {
		return selectedJob;
	}
	public void setSelectedJob(String selectedJob) {
		this.selectedJob = selectedJob;
	}
	public String getTripIds() {
		return tripIds;
	}
	public void setTripIds(String tripIds) {
		this.tripIds = tripIds;
	}
	public List getTripOrderList() {
		return tripOrderList;
	}
	public void setTripOrderList(List tripOrderList) {
		this.tripOrderList = tripOrderList;
	}
	public String getTripNumber() {
		return tripNumber;
	}
	public void setTripNumber(String tripNumber) {
		this.tripNumber = tripNumber;
	}
	public List getMaxTripNumber() {
		return maxTripNumber;
	}
	public void setMaxTripNumber(List maxTripNumber) {
		this.maxTripNumber = maxTripNumber;
	}
	public List getTripNumberList() {
		return TripNumberList;
	}
	public void setTripNumberList(List tripNumberList) {
		TripNumberList = tripNumberList;
	}
	/*	public List getTripOpenList() {
	return tripOpenList;
}
public void setTripOpenList(List tripOpenList) {
	this.tripOpenList = tripOpenList;
}*/
	public List getStatusNumberCountList() {
		return statusNumberCountList;
	}
	public void setStatusNumberCountList(List statusNumberCountList) {
		this.statusNumberCountList = statusNumberCountList;
	}
	public List getStatusNumberList() {
		return statusNumberList;
	}
	public void setStatusNumberList(List statusNumberList) {
		this.statusNumberList = statusNumberList;
	}
	public List getJobstatusNumberList() {
		return jobstatusNumberList;
	}
	public void setJobstatusNumberList(List jobstatusNumberList) {
		this.jobstatusNumberList = jobstatusNumberList;
	}
	public String getFilterdTripNumber() {
		return filterdTripNumber;
	}
	public void setFilterdTripNumber(String filterdTripNumber) {
		this.filterdTripNumber = filterdTripNumber;
	}

	public String getJobStatus() {
		return jobStatus;
	}
	public void setJobStatus(String jobStatus) {
		this.jobStatus = jobStatus;
	}
	public String getFilterdRegumber() {
		return filterdRegumber;
	}
	public void setFilterdRegumber(String filterdRegumber) {
		this.filterdRegumber = filterdRegumber;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public String getSoStatus() {
		return soStatus;
	}
	public void setSoStatus(String soStatus) {
		this.soStatus = soStatus;
	}
	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public List getTooltipList() {
		return tooltipList;
	}
	public void setTooltipList(List tooltipList) {
		this.tooltipList = tooltipList;
	}
	public String getEstWgt() {
		return estWgt;
	}
	public void setEstWgt(String estWgt) {
		this.estWgt = estWgt;
	}
	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}
	public Company getCompany() {
		return company;
	}
	public CompanyManager getCompanyManager() {
		return companyManager;
	}
	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public void setIntegrationLogManager(IntegrationLogManager integrationLogManager) {
		this.integrationLogManager = integrationLogManager;
	}
	public boolean isNetworkAgent() {
		return networkAgent;
	}
	public void setNetworkAgent(boolean networkAgent) {
		this.networkAgent = networkAgent;
	}
	public String getMsgClicked() {
		return msgClicked;
	}
	public void setMsgClicked(String msgClicked) {
		this.msgClicked = msgClicked;
	}
	public String getOiJobList() {
		return oiJobList;
	}
	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	public String getDashBoardHideJobsList() {
		return dashBoardHideJobsList;
	}
	public void setDashBoardHideJobsList(String dashBoardHideJobsList) {
		this.dashBoardHideJobsList = dashBoardHideJobsList;
	}
	
}
