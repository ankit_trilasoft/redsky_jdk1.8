<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="operationResource.title"/></title>   
    <meta name="heading" content="<fmt:message key='O&I.heading' />"/>   
    <style> #ajax_tooltipObj .ajax_tooltip_content {background-color:#FFF !important;}</style>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	 <td align="left"><b>Work Order List </b></td>
	<td align="right"  style="width:30px;">
	<img  valign="top"align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="showDistinctWorkOrder" class="table" requestURI="" id="showDistinctWorkOrder" style="margin:0px;margin-bottom:8px">
	<display:column title=" " style="width:3px;"><input type="checkbox" id="checkboxId${showDistinctWorkOrder.id}" name="DD" value="${showDistinctWorkOrder.id}" onclick="setValues('${showDistinctWorkOrder.workorder}',this)"/></display:column>
		 <display:column  title="WO#"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${showDistinctWorkOrder.id}" id="workOrderOI${showDistinctWorkOrder.id}"  value="${showDistinctWorkOrder.workorder}" style="width:40px" disabled="true"/> 
    </display:column>
    
     <display:column  title="Ticket"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${showDistinctWorkOrder.id}" id="ticket${showDistinctWorkOrder.id}"  value="${showDistinctWorkOrder.ticket}" style="width:40px" disabled="true"/> 
    </display:column>
    
	</display:table>
	
	 <c:if test="${showDistinctWorkOrder!='[]'}"> 
  <input type="button"  Class="cssbutton" align="center" style="width: 90px; font-size: 10px; height: 21px; margin: 0px 70px 5px;" id="okButton"  value="Copy WorkOrder" onclick="copyWorkOrder();"/>
	</c:if>
	<c:if test="${showDistinctWorkOrder=='[]'}"> 
  <input type="button"  Class="cssbutton" align="center" style="width: 90px; font-size: 10px; height: 21px; margin: 0px 70px 5px;" id="okButton" readonly="readonly" disabled="true" value="Open WorkOrder" onclick="findAllResource('flag');"/>
	</c:if> 