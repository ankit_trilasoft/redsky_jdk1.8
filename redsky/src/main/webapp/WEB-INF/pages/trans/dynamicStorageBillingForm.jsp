<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title><fmt:message key="storageBillingForm.title"/></title>
    <meta name="heading" content="<fmt:message key='storageBillingForm.heading'/>"/>
    <style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->

	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>
	<script language="javascript" type="text/javascript">
	function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
}
                  
	function findPreviewBillingReport(){
	 
   var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
   var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("Bill Through To should be greater than  or equal to Bill Through From");
    document.forms['storageBillingForm'].elements['billTo'].value='';
    return false;
  }
	
      var StoInv =document.forms['storageBillingForm'].elements['billing.preview'].value ;
      		var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
			
       if (btn == null) 
		   {
	          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
	         
	       	  return false;
	       }
      else if(document.forms['storageBillingForm'].elements['billTo2'].value=='')
	       {
	       	alert("Please enter the Bill Through From date"); 
	       	
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['billTo'].value==''){
	        alert("Please enter the Bill Through To date "); 
	       
	       	return false;
	       }
	       
	    
	       else
	       {
	       	  if(StoInv == 'Preview Billing Report'){
        		var url="billingStoragesList.html";
          		document.forms['storageBillingForm'].action ='billingStoragesList.html';
           		document.forms['storageBillingForm'].submit();
           		}
           		 showOrHide2(1);
        		//showOrHide(1);
        		//showOrHide1(0);
        		//ajexFunction();
        		
		
	        	return true;
	   	  }
     
     
      
}


	function findDynamicBillingStoragesVatList(){
	 
   var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
   var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("Bill Through To should be greater than  or equal to Bill Through From");
    document.forms['storageBillingForm'].elements['billTo'].value='';
    return false;
  }
	
      var StoInv =document.forms['storageBillingForm'].elements['billing.previewVat'].value ;
      		var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
      		var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
      		if(btn1==null){
          	  btn1="";  
            }
            //var storageBillingList = document.forms['storageBillingForm'].elements['storageBillingList'].value ;
            var fld = document.getElementById('storageBillingListId');
             var storageBillingList = [];
             for (var i = 0; i < fld.options.length; i++) {
             if (fld.options[i].selected) {
        	  storageBillingList.push(fld.options[i].value);
             }
            } 
            var bookingCodeType = document.forms['storageBillingForm'].elements['bookingCodeType'].value;
            var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
            var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 	
       if (btn == null) 
		   {
	          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
	         
	       	  return false;
	       }
      else if(document.forms['storageBillingForm'].elements['billTo2'].value=='')
	       {
	       	alert("Please enter the Bill Through From date"); 
	       	
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['billTo'].value==''){
	        alert("Please enter the Bill Through To date "); 
	       
	       	return false;
	       }
	       
	    
	       else
	       {
	       	  if(StoInv == 'Preview Billing Report'){
        		//var url="billingStoragesList.html";
          		window.open('dynamicBillingStoragesVatList.html?radiobilling='+btn+'&storageBillingList='+storageBillingList+'&radiobExcInc='+btn1+'&bookingCodeType='+bookingCodeType+'&billTo2='+date1+'&billTo='+date2,'_blank');
           		//document.forms['storageBillingForm'].submit();
           		}
           		 //showOrHide2(1);
        		//showOrHide(1);
        		//showOrHide1(0);
        		//ajexFunction();
        		
		
	        	return true;
	   	  }
     
     
      
}

function ajexFunction() {
	
	var billToCode = document.forms['storageBillingForm'].elements['billToCodeType'].value;
    //  alert(billToCode);
    var url="getTotalNumberOfCount.html?decorator=simple&popup=true";
    // alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = getTotalNumberOfCount;
     http2.send(null);

 }
 
 function getTotalNumberOfCount()
 {
 
	if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                document.forms['storageBillingForm'].elements['count'].value=results
             }
 
 }

function createAccountLineStoIns(){
		var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
   var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("Bill Through To should be greater than  or equal to Bill Through From");
    document.forms['storageBillingForm'].elements['billTo'].value='';
    return false;
  }
      var StoInv =document.forms['storageBillingForm'].elements['billing.invoice'].value ;
      // alert(StoInv);
      var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
      var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
    if(document.forms['storageBillingForm'].elements['billTo2'].value=='')
	       {
	       	alert("Please enter the bill through from date"); 
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['billTo'].value==''){
	        alert("Please enter the bill through to date "); 
	       	return false;
	       }
	       	else if (btn == null) 
	       {
	          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
	       	  return false;
	       }
	       //else if(document.forms['storageBillingForm'].elements['bookingCodeType'].value==''){
	      //  alert("Please enter the booker's code"); 
	      // 	return false;
	      // }
	       //else if(document.forms['storageBillingForm'].elements['storageBillingList'].value==''){
	        //alert("Please enter the storage billing group"); 
	       	//return false;
	       //}
	       //	else if (btn1 == null) 
	       //{
	         // alert("Please select the Include Group/Exclude Group radio button"); 
	       	  //return false;
	       //}
	       
	       else if(document.forms['storageBillingForm'].elements['invoiceBillingDate'].value==''){
	        alert("Please enter the invoice billing date "); 
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['postDate'].value==''){
	        alert("Please enter the post date "); 
	       	return false;
	       }
	       else
	       {
	       	 if(StoInv == 'Create Invoice Lines'){
	       	 agree= confirm("This Process will create invoices and change the data, are you sure? Hit OK to continue or cancel to stop");
				if(agree){
             document.forms['storageBillingForm'].action ='invoiceStorages.html';
             document.forms['storageBillingForm'].submit();
             document.forms['storageBillingForm'].elements['storageErrorFlagValue'].value=""; 
             my_window = window.open('previewSuccessTemp.html?decorator=simple&popup=true','mywin','location=1,status=1,scrollbars=1,width=550,height=350, top=300, left=300');
           	//childWin = openWindow('previewSuccessTemp.html?decorator=simple&popup=true',550,310);
        		
            //showOrHide(0);
            //showOrHide1(1);
             }
	        return true;
	    }
	    else
	    {
	    	return false;
	    }
	    }
     
      
}

function createDynamicAccountLineVatStoIns(){
		var date1 = document.forms['storageBillingForm'].elements['billTo2'].value;	 
   var date2 = document.forms['storageBillingForm'].elements['billTo'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("Bill Through To should be greater than  or equal to Bill Through From");
    document.forms['storageBillingForm'].elements['billTo'].value='';
    return false;
  }
      var StoInv =document.forms['storageBillingForm'].elements['billing.invoiceVat'].value ;
      // alert(StoInv);
      var btn = valButton(document.forms['storageBillingForm'].elements['radiobilling']);
      var  btn1 = valButton(document.forms['storageBillingForm'].elements['radiobExcInc']);
    if(document.forms['storageBillingForm'].elements['billTo2'].value=='')
	       {
	       	alert("Please enter the bill through from date"); 
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['billTo'].value==''){
	        alert("Please enter the bill through to date "); 
	       	return false;
	       }
	       	else if (btn == null) 
	       {
	          alert("Please select the Storage Billing/SIT Billing/TPS radio button"); 
	       	  return false;
	       }
	      
	       
	       else if(document.forms['storageBillingForm'].elements['invoiceBillingDate'].value==''){
	        alert("Please enter the invoice billing date "); 
	       	return false;
	       }
	       else if(document.forms['storageBillingForm'].elements['postDate'].value==''){
	        alert("Please enter the post date "); 
	       	return false;
	       }
	       else
	       {
	       	 if(StoInv == 'Create Invoice Lines'){
	       	 agree= confirm("This Process will create invoices and change the data, are you sure? Hit OK to continue or cancel to stop");
				if(agree){
             document.forms['storageBillingForm'].action ='invoiceDynamicVatStorages.html';
             document.forms['storageBillingForm'].submit();
             document.forms['storageBillingForm'].elements['storageErrorFlagValue'].value=""; 
             my_window = window.open('previewSuccessTemp.html?decorator=simple&popup=true','mywin','location=1,status=1,scrollbars=1,width=550,height=350, top=300, left=300');
           
             }
	        return true;
	    }
	    else
	    {
	    	return false;
	    }
	    }
     
      

}

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
function findBillToCode()
{
     var billToCode = document.forms['storageBillingForm'].elements['billToCodeType'].value;
    //  alert(billToCode);
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(billToCode);
    // alert(url);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}
function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
                 }
                 else
                 {
                     alert("Invalid Bill To code, please select another");
                     document.forms['storageBillingForm'].elements['billToCodeType'].value = "";
					
                 }
             }
        }
        
 function findBookingAgentCode(){
         var bookingAgentCode = document.forms['storageBillingForm'].elements['bookingCodeType'].value;
         bookingAgentCode=bookingAgentCode.trim();
         if(bookingAgentCode!=''){
         progressBarAutoSave('1');
         var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
         http2.open("GET", url, true);
         http2.onreadystatechange = handleHttpResponse4;
         http2.send(null);
         }
  }
  function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
 				}
                 else
                 {
                     alert("Invalid Booking Agent code, please select another");
                     document.forms['storageBillingForm'].elements['bookingCodeType'].value="";
					 
                 }
                 progressBarAutoSave('0');
             } 
    }  
     

function openPopWindow(){
		
		javascript:openWindow('partnersPopup.html?partnerType=PP&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCodeType');
}
function openBookingAgentPopWindow(){		
		javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=bookingCodeType');
} 

function progressBar(tar){
showOrHide(tar);
}

function showOrHide(value) {
    if (value==0) {
        if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
} 

function showOrHide1(value) {
    if (value==0) {
        if (document.layers)
           document.layers["layerH1"].visibility='hide';
        else
           document.getElementById("layerH1").style.visibility='hidden';
   }
   else if (value==1) {
       if (document.layers)
          document.layers["layerH1"].visibility='show';
       else
          document.getElementById("layerH1").style.visibility='visible';
   }
}

function showOrHide2(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
} 
</script>

<style type="text/css">

.heading{background:#DCDCDC url(images/greylinebg.gif) repeat scroll 0% 0%;
border-color:#DCDCDC -moz-use-text-color;
border-style:solid none;
border-width:1px medium;
color:#15428B;
font-family:Arial,Helvetica,sans-serif;
font-size:12px;
font-weight:bold;
height:22px}
.input-text{margin-top:5px;}
.list-menu{margin-top:5px;}


		
</style>
</head>

<s:form id="storageBillingForm" name="storageBillingForm" action="billingStoragesList" method="post" validate="true"> 
<s:hidden name="count"/> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:90%;">

<DIV ID="layerH" style="position:absolute;width:400px;height:10px;left:250px;top:150px; background-color:white;z-index:150;">
	<td align="justify"  class="listwhitetext" valign="middle"><font size="4" color="#1666C9">
	<b>Preview Execution is in progress....</b>
	</font></td>
</DIV>
<DIV ID="layerH1" style="position:absolute;width:400px;height:10px;left:250px;top:150px; background-color:white;z-index:150;">
	<td align="justify"  class="listwhitetext" valign="middle"><font size="4" color="#1666C9">
	<b>Generating invoice ....</b>
	</font></td>
</DIV>
<script type="text/javascript"> 
showOrHide(0);
showOrHide1(0);
</script>
<s:hidden name="reports.id" value="%{reports.id}"/> 
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Storage Billing</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" style="width:600px;" cellspacing="2" cellpadding="0" border="0">
	<!--<div class="subcontent-tab" style="height:16px; padding-top:5px;">Storage Billing</div>	-->
	
	  		<tr>
	  		<td  class="listwhitetext" align="right" width="130px" style="!vertical-align:middle;">
	  		   <label for="r1">Storage Billing </td><td><input type="radio" name="radiobilling" id="r1" value="1"/></label> 
               </td>
               
               <td  class="listwhitetext" align="right" width="130px" style="!vertical-align:middle;">
               <label for="r2">SIT Billing</td><td><input type="radio" name="radiobilling" id="r2" value="2"/></label>
               </td>
               
               <td  class="listwhitetext" align="right" style="!vertical-align:middle;">
               <label for="r3">TPS</td><td><input type="radio" name="radiobilling" id="r3" value="3"/></label>
               </td>
	  		    </tr>
	  		    <tr>
	  			<td class="listwhitetext" align="right" width="130px">Bill Through From</td>
	  					<c:if test="${not empty billTo2}">
						<s:text id="billTo2" name="${FormDateValue}"> <s:param name="value" value="billTo2" /></s:text>
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo2" name="billTo2" value="%{billTo2}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="billTo2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty billTo2}" >
						<td width="130px" style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo2" name="billTo2" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="billTo2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		
	  		
	  		<td class="listwhitetext" align="right" >Bill Through To</td>
	  		<c:if test="${not empty billTo}">
						<s:text id="billTo" name="${FormDateValue}"> <s:param name="value" value="billTo" /></s:text>
						<td  style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo" name="billTo" value="%{billTo}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="billTo_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty billTo}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="billTo" name="billTo" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="billTo_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
	  		<td class="listwhitetext" align="right" >Booker's Code</td>
	  		<td class="listwhitetext" style="padding-left:2px"><s:textfield  cssClass="input-text" name="bookingCodeType" size="11" maxlength="6" onchange="findBookingAgentCode()"/>
	  		<img class="openpopup" align="absmiddle" width="17" height="20" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		<td class="listwhitetext" align="right" >Storage Billing Group</td>
	  		<td style="padding-left:2px">
	  			<s:select cssClass="list-menu" cssStyle="width:130px; height:100px" id="storageBillingListId" name="storageBillingList" list="%{storageBillingGroup}"  multiple="true" headerKey="" headerValue=""  />
	  		</td>
	  		
	  		</tr>
	  		
	  		<tr>
	  		<td class="listwhitetext" align="right">
	  		<label for="i1">Include Group 
	  		 <td width="185" class="listwhitetext" align="left"><input type="radio" name="radiobExcInc" id="i1" value="1" />	  		  
              </td></label>
	  		
	  	 
	  	   <td  class="listwhitetext" align="right">
	  	   <label for="i1">Exclude Group
	  	   <td width="185" class="listwhitetext" align="left"><input type="radio" name="radiobExcInc" id="i2" value="2" /></td></label>
	  	   </tr>
	  		<tr>
	  			<td class="listwhitetext" align="right" >Invoice Date for Billing</td>
	  			<c:if test="${not empty invoiceBillingDate}">
						<s:text id="invoiceBillingDate" name="${FormDateValue}"> <s:param name="value" value="invoiceBillingDate" /></s:text>
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="invoiceBillingDate" name="invoiceBillingDate" value="%{invoiceBillingDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoiceBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoiceBillingDate}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="invoiceBillingDate" name="invoiceBillingDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoiceBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
			
	  			<td class="listwhitetext" align="right" >Posting Date</td>
	  			
	  		<c:if test="${not empty postDate}">
						<s:text id="postDate" name="${FormDateValue}"> <s:param name="value" value="postDate" /></s:text>
						<td style="padding-left:2px;" ><s:textfield cssClass="input-text" id="postDate" name="postDate" value="%{postDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty postDate}" >
						<td style="padding-left:2px"><s:textfield cssClass="input-text" id="postDate" name="postDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>

	  		<!--<tr>	
	  			<td align="center"><input type="button" class="cssbutton" style="width:200px; height:25px" align="top" onclick="findPreviewBillingReport()" name="billing.preview" value="Preview Billing Report" /></td>
	  			<td align="center"><input type="button" class="cssbutton" style="width:200px; height:25px" align="top" onclick="createAccountLineStoIns()" name="billing.invoice" value="Create Invoice Lines" /></td>
	    	</tr>

	  		--><tr><td style="!height:20px;"></td></tr>

	  		</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
 	<tr>
 	 <%--<c:if test="${systemDefaultVatCalculation!='true'}">	
	  <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return findPreviewBillingReport()" name="billing.preview" value="Preview Billing Report" /></td>
	  <sec-auth:authComponent componentId="module.button.storageBilling.createInvoice">
	  <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return createAccountLineStoIns()" name="billing.invoice" value="Create Invoice Lines" /></td>
	 </sec-auth:authComponent>
	 </c:if>
	 <c:if test="${systemDefaultVatCalculation=='true'}">--%>
	 <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return findDynamicBillingStoragesVatList()" name="billing.previewVat" value="Preview Billing Report" /></td>
	  <sec-auth:authComponent componentId="module.button.storageBilling.createInvoice">
	  <td align="center"><input type="button" class="cssbutton" style="width:150px; height:25px" align="top" onclick="return createDynamicAccountLineVatStoIns()" name="billing.invoiceVat" value="Create Invoice Lines" /></td>
	 </sec-auth:authComponent>
	<%-- </c:if>--%>
	 </tr>
</div>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />

<c:if test="${storageErrorFlag=='Error'}">
<s:hidden name="storageErrorFlagValue" value="${storageErrorFlag}"/>
</c:if> 
<c:if test="${storageErrorFlag!='Error'}">
<s:hidden name="storageErrorFlagValue" value=""/>
</c:if>
</s:form>


<script type="text/javascript"> 
try{
showOrHide(0);
}
catch(e){}
try{
showOrHide1(0);
}
catch(e){}
Form.focusFirstElement($("storageBillingForm"));   
</script> 
<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>