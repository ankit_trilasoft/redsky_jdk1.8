/**
 *  @Class Name	 ServiceOrderDashboardDaoHibernate
 *  @author      Surya 
 *  @Date        14-Jul-2014
 */
package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;

import com.trilasoft.app.dao.ServiceOrderDashboardDao;
import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.SODashboardDTO;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.ServiceOrderDashboard;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.webapp.action.CommonUtil;

import org.hibernate.Hibernate;



public class ServiceOrderDashboardDaoHibernate extends GenericDaoHibernate<ServiceOrderDashboard, Long> implements ServiceOrderDashboardDao{
	private CompanyManager companyManager;
	public ServiceOrderDashboardDaoHibernate() {
		super(ServiceOrderDashboard.class);
	} 
	
	public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String companyDivision,String billToName,Boolean activeStatus,String bookingAgentShipNumber,String serviceOrderMoveType,String recordLimit,String cityCountryZipOption,String originCityOrZip,String destinationCityOrZip,String originCountry,String destinationCountry,String salesMan,String certificateNumber,String daShipmentNumber, String estimator){
		
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        Map filterMap=user.getFilterMap(); 
    	List userList=new ArrayList();
    	
			
			String companydivisionSet="";
			String billtocodeSet ="";
			String bookingAgentSet="";
			try {	
			if (filterMap!=null && (!(filterMap.isEmpty()))) { 
				List list1 = new ArrayList();
				List list2 = new ArrayList();
				List list3 = new ArrayList();
				
				if(filterMap.containsKey("serviceOrderCompanyDivisionFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderCompanyDivisionFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		list2.add("'" + listIterator.next().toString() + "'");	
		    	}
		    	companydivisionSet= list2.toString().replace("[","").replace("]", "");
		    	}
		    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list1.add("'" + code + "'");	
		    		} 
		    	}
		    	billtocodeSet = list1.toString().replace("[","").replace("]", "");
		    	}
		    	
		    	if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--")))){ 
		    		list3.add("'" + code + "'");	
		    		} 
		    	}
		    	bookingAgentSet = list3.toString().replace("[","").replace("]", "");
		    	}
			}
    	}catch (Exception e) {
			logger.error("Error executing query"+ e,e); 
	    	 e.printStackTrace();
		}
		    	String companyDivisionCondision="  ";
		    	if (companydivisionSet != null && (!(companydivisionSet.equals(""))) ) {
		    		companyDivisionCondision="  and s.companyDivision in ("+ companydivisionSet + ")   ";
		    	}
		    	
		    	String billtocodeCondision="  ";
		    	if (billtocodeSet != null && (!(billtocodeSet.equals(""))) ) {
		    		billtocodeCondision="  and s.billToCode  in ("+ billtocodeSet + ")   ";
		    	}
		    	
		    	String bookingAgentCondision="  ";
		    	if (bookingAgentSet != null && (!(bookingAgentSet.equals(""))) ) {
		    		bookingAgentCondision="  and s.bookingAgentCode in ("+ bookingAgentSet + ")   ";
		    	}
		    	
		
		    	String originZip = "";
				String destinationZip = "";
				String originCity = "";
				String destinationCity = "";
				if((cityCountryZipOption!=null && !cityCountryZipOption.equalsIgnoreCase("")) && (cityCountryZipOption.equalsIgnoreCase("Zip"))){
					originZip = CommonUtil.getOriginZip(originCityOrZip,"");
				    destinationZip = CommonUtil.getDestinationZip(destinationCityOrZip,"");
					
				}else if((cityCountryZipOption!=null && !cityCountryZipOption.equalsIgnoreCase("")) && (cityCountryZipOption.equalsIgnoreCase("City"))){
					originCity = originCityOrZip;
					destinationCity = destinationCityOrZip;
				}
		
		
		List soDashboardSearchList = new ArrayList();
		Map <String,String> servFlex=getFieldsNameToDisplay();
		String vlJobTypes=getVlJobTypes(corpId);
		String stoJobTypes=getStorageJobTypes(corpId);
		String lcjobs=getLocalJobsTypes(corpId);
		String wherecl="";
		String flag="0";
	    //String MasterList="select s.shipnumber as shipnumber,s.registrationnumber as registrationnumber,replace(replace(role,'SD','SDA'),'SO','SOA') as role,s.job as job,s.serviceType as shipmenttype,s.coordinator as coordinator,s.status as status,s.id as id,if(d.actualSurvey is not null,d.actualSurvey,d.estimateSurvey) as survey,estweight as estweight,'' as ldpkticket,if(actualLoading is not null,actualLoading,estimateLoading) as loading,actualweight as actualweight,driver as driver,truck as truck,'' as deliveryticket,if(actualDelivery is not null,actualDelivery,estimateDelivery) as delivery,claims as claims,if((qc!='DT' and qc!='OD' and qc!='NA'),date_format(qc,'%d-%b-%Y'),qc) as qc,if((estimateSurvey!='DT' and estimateSurvey!='OD' and estimateSurvey!='NA'),date_format(estimateSurvey,'%d-%b-%Y'),estimateSurvey) as estSurvey,if((estimateLoading!='DT' and estimateLoading!='OD' and estimateLoading!='NA'),date_format(estimateLoading,'%d-%b-%Y'),estimateLoading) as estLoding,if((estimateDelivery!='DT' and estimateDelivery!='OD' and estimateDelivery!='NA'),date_format(estimateDelivery,'%d-%b-%Y'),estimateDelivery) as estDelivery,s.lastName as lastName,s.firstName as firstName,mode,actVolume,estvolume,if(s.ATD is not null,date_format(s.ATD,'%d-%b-%Y'),date_format(s.ETD,'%d-%b-%Y'))as dpt,if(s.ATA is not null,date_format(s.ATA,'%d-%b-%Y'),date_format(s.ETA,'%d-%b-%Y'))as arvl,date_format(estimateLoading,'%d-%b-%Y') as loadingDt,date_format(estimateDelivery,'%d-%b-%Y') as deliveryDt,date_format(estimateSurvey,'%d-%b-%Y') as surveyDt,if(s.ATD is not null,'A','T')as dptF,if(s.ATA is not null,'A','T')as arvlF,date_format(d.customDate,'%d-%b-%Y') as clearCustom ,d.customFlag as clearCustomFlag,s.bookingAgentCode as bookingAgentCode,s.serviceType as serviceType,s.billToName as billToName,if(s.originCity is not null and s.originCity!='',concat(s.originCity,', ',if(s.originCountryCode is not null,s.originCountryCode,'-')),concat('-',if(s.originCountryCode is not null,s.originCountryCode,'-'))) as OrginCC,if(s.destinationCity is not null and s.destinationCity!='',concat(s.destinationCity,', ',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-')),concat('-',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-'))) as destinCC,if(t.loadA is not null,'A', 'T') as loadF,if(t.deliveryA is not null,'A', 'T') as deliveryF,if(d.actualSurvey is not null,'A', 'T') as surveyF,if(t.loadA is not null,date_format(t.loadA,'%d-%b-%Y'),date_format(t.beginLoad,'%d-%b-%Y') ) as ld,if(t.deliveryA is not null,date_format(t.deliveryA,'%d-%b-%Y'),date_format(t.deliveryShipper,'%d-%b-%Y')) as dt,if(d.actualSurvey is not null,date_format(d.actualSurvey,'%d-%b-%Y'),date_format(d.estimateSurvey,'%d-%b-%Y')) as asd,if(s.ATD is not null,s.ATD,s.ETD) as sortDpt,if(s.ATA is not null,s.ATA,s.ETA) as sortArv,d.customDate as sortCustDt,qc as qcSort,s.moveType as serviceOrderMoveType,s.commodity as commodity,s.routing as routing,date_format(s.createdOn,'%d-%b-%Y') as createdOn,s.customerFileId as cid ,s.salesMan as salesMan,s.estimator as estimator from serviceorder s inner join trackingstatus t on s.id=t.id and t.corpID='"+corpId+"' inner join sodashboard d on d.serviceOrderId=s.id  and s.controlFlag='C' where  s.corpID='"+corpId+"' ";
	    //if((certificateNumber!=null && !certificateNumber.equals(""))|| (daShipmentNumber!=null && !daShipmentNumber.equals(""))){
	    String MasterList="select s.shipnumber as shipnumber,s.registrationnumber as registrationnumber,replace(replace(role,'SD','SDA'),'SO','SOA') as role,s.job as job,s.serviceType as shipmenttype,s.coordinator as coordinator,s.status as status,s.id as id,if(d.actualSurvey is not null,d.actualSurvey,d.estimateSurvey) as survey,estweight as estweight,'' as ldpkticket,if(actualLoading is not null,actualLoading,estimateLoading) as loading,actualweight as actualweight,driver as driver,truck as truck,'' as deliveryticket,if(actualDelivery is not null,actualDelivery,estimateDelivery) as delivery,claims as claims,if((qc!='DT' and qc!='OD' and qc!='NA'),date_format(qc,'%d-%b-%Y'),qc) as qc,if((estimateSurvey!='DT' and estimateSurvey!='OD' and estimateSurvey!='NA'),date_format(estimateSurvey,'%d-%b-%Y'),estimateSurvey) as estSurvey,if((estimateLoading!='DT' and estimateLoading!='OD' and estimateLoading!='NA'),date_format(estimateLoading,'%d-%b-%Y'),estimateLoading) as estLoding,if((estimateDelivery!='DT' and estimateDelivery!='OD' and estimateDelivery!='NA'),date_format(estimateDelivery,'%d-%b-%Y'),estimateDelivery) as estDelivery,s.lastName as lastName,s.firstName as firstName,mode,actVolume,estvolume,if(s.ATD is not null,date_format(s.ATD,'%d-%b-%Y'),date_format(s.ETD,'%d-%b-%Y'))as dpt,if(s.ATA is not null,date_format(s.ATA,'%d-%b-%Y'),date_format(s.ETA,'%d-%b-%Y'))as arvl,date_format(estimateLoading,'%d-%b-%Y') as loadingDt,date_format(estimateDelivery,'%d-%b-%Y') as deliveryDt,date_format(estimateSurvey,'%d-%b-%Y') as surveyDt,if(s.ATD is not null,'A','T')as dptF,if(s.ATA is not null,'A','T')as arvlF,date_format(d.customDate,'%d-%b-%Y') as clearCustom ,d.customFlag as clearCustomFlag,s.bookingAgentCode as bookingAgentCode,s.serviceType as serviceType,b.billToName as billToName,if(s.originCity is not null and s.originCity!='',concat(s.originCity,', ',if(s.originCountryCode is not null,s.originCountryCode,'-')),concat('-',if(s.originCountryCode is not null,s.originCountryCode,'-'))) as OrginCC,if(s.destinationCity is not null and s.destinationCity!='',concat(s.destinationCity,', ',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-')),concat('-',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-'))) as destinCC,if(t.loadA is not null,'A', 'T') as loadF,if(t.deliveryA is not null,'A', 'T') as deliveryF,if(d.actualSurvey is not null,'A', 'T') as surveyF,if(t.loadA is not null,date_format(t.loadA,'%d-%b-%Y'),date_format(t.beginLoad,'%d-%b-%Y') ) as ld,if(t.deliveryA is not null,date_format(t.deliveryA,'%d-%b-%Y'),date_format(t.deliveryShipper,'%d-%b-%Y')) as dt,if(d.actualSurvey is not null,date_format(d.actualSurvey,'%d-%b-%Y'),date_format(d.estimateSurvey,'%d-%b-%Y')) as asd,if(s.ATD is not null,s.ATD,s.ETD) as sortDpt,if(s.ATA is not null,s.ATA,s.ETA) as sortArv,d.customDate as sortCustDt,qc as qcSort,s.moveType as serviceOrderMoveType,s.commodity as commodity,s.routing as routing,date_format(s.createdOn,'%d-%b-%Y') as createdOn,s.customerFileId as cid ,s.salesMan as salesMan,s.estimator as estimator from serviceorder s inner join trackingstatus t on s.id=t.id and t.corpID='"+corpId+"' inner join sodashboard d on d.serviceOrderId=s.id  and s.controlFlag='C' inner join billing b on s.id=b.id  where  s.corpID='"+corpId+"' ";	
	   // }
	    if(shipNumber!=null && !shipNumber.equals("")){
			shipNumber=shipNumber.replaceAll("'","");
			shipNumber=shipNumber.replaceAll("-","");
			wherecl=" and s.shipnumber like '%"+shipNumber+"%'";
			flag="1";
		}
		if(bookingAgentShipNumber!=null && !bookingAgentShipNumber.equals("")){
			bookingAgentShipNumber=bookingAgentShipNumber.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and (s.bookingAgentShipNumber like '"+bookingAgentShipNumber+"%' or t.originAgentExSO like '"+bookingAgentShipNumber+"%' or t.networkAgentExSO like '"+bookingAgentShipNumber+"%' or t.originSubAgentExSO like '"+bookingAgentShipNumber+"%' or t.destinationSubAgentExSO like '"+bookingAgentShipNumber+"%' or t.bookingAgentExSO like '"+bookingAgentShipNumber+"%' or t.bookingAgentExSO like '"+bookingAgentShipNumber+"%' or t.destinationAgentExSO like '"+bookingAgentShipNumber+"%')";
			}else{
				wherecl=" and (s.bookingAgentShipNumber like '"+bookingAgentShipNumber+"%' or t.originAgentExSO like '"+bookingAgentShipNumber+"%' or t.networkAgentExSO like '"+bookingAgentShipNumber+"%' or t.originSubAgentExSO like '"+bookingAgentShipNumber+"%' or t.destinationSubAgentExSO like '"+bookingAgentShipNumber+"%' or t.bookingAgentExSO like '"+bookingAgentShipNumber+"%' or t.bookingAgentExSO like '"+bookingAgentShipNumber+"%' or t.destinationAgentExSO like '"+bookingAgentShipNumber+"%')";
			flag="1";
			}
		}
		if(registrationNumber!=null && !registrationNumber.equals("")){
			registrationNumber=registrationNumber.replaceAll("'","");
			registrationNumber = registrationNumber.replaceAll("-", "%-%");
			if(flag.equals("1")){
			wherecl=wherecl+" and registrationnumber like '%"+registrationNumber+"%'";
			}else{
				wherecl="and registrationnumber like '%"+registrationNumber+"%'";
			flag="1";
			}
		}
		if(lastName!=null && !lastName.equals("")){
			lastName=lastName.replaceAll("'","''");
			if(flag.equals("1")){
			wherecl=wherecl+" and lastName like '%"+lastName+"%'";
			}else{
				wherecl="and lastName like '%"+lastName+"%'";
			flag="1";
			}
		}
		if(firstName!=null && !firstName.equals("")){
			firstName=firstName.replaceAll("'","''");
			if(flag.equals("1")){
			wherecl=wherecl+" and firstName like '%"+firstName+"%'";
			}else{
				wherecl="and firstName like '%"+firstName+"%'";
			flag="1";
			}
		}
		if(job1!=null && !job1.equals("")){
			job1=job1.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and job like '%"+job1+"%'";
			}else{
				wherecl="and job like '%"+job1+"%'";
			flag="1";
			}
		}
		if(coordinator!=null && !coordinator.equals("")){
			coordinator=coordinator.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and coordinator like '%"+coordinator+"%'";
			}else{
				wherecl="and coordinator like '%"+coordinator+"%'";
			flag="1";
			}
		}
		if((status!=null && !status.equals("")) && status.equalsIgnoreCase("Not Delivered")){
			if(flag.equals("1")){
				wherecl=wherecl+" and t.deliveryA is null and job not in ('RLO')";
			}else{
				wherecl=" and t.deliveryA is null  and job not in ('RLO')";
				flag="1";
			}
		}else if((status!=null && !status.equals("")) && status.equalsIgnoreCase("Not Loaded")){
				if(flag.equals("1")){
					wherecl=wherecl+" and t.loadA is null  and job not in ('RLO')";
				}else{
					wherecl=" and t.loadA is null  and job not in ('RLO')";
					flag="1";
				}
		}else if(status!=null && !status.equals("")){
			status=status.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.status like '%"+status+"%'";
			}else{
				wherecl="and s.status like '%"+status+"%'";
			flag="1";
			}
		}
		if(role!=null && !role.equals("")){
			role=role.replaceAll("'","");
			String roleArray[]=role.split(",");	
			String con="";
			if(flag.equals("1")){
				for(int i=0;i<roleArray.length;i++){
			//wherecl=wherecl+" and replace(role,'/',',') like '%"+roleArray[i]+"%'";
			con=con+" and replace(role,'/',',') like '%"+roleArray[i]+"%'";
				}
				wherecl=wherecl+con;
			}else{
				for(int i=0;i<roleArray.length;i++){
				con=con+" and replace(role,'/',',') like '%"+roleArray[i]+"%'";
				}
				wherecl=wherecl+con;
			flag="1";
			}
		}
		if(companyDivision!=null && !companyDivision.equals("")){
			if(flag.equals("1")){
			wherecl=wherecl+" and s.companyDivision like '%"+companyDivision+"%'";
			}else{
				wherecl="and s.companyDivision like '%"+companyDivision+"%'";
			flag="1";
			}
		}
		if(serviceOrderMoveType!=null && !serviceOrderMoveType.equals("")){
			if(flag.equals("1")){
				wherecl=wherecl+" and s.moveType like '%"+serviceOrderMoveType+"%'";
				}else{
					wherecl="and s.moveType like '%"+serviceOrderMoveType+"%'";
				flag="1";
				}
		}
		if(billToName!=null && !billToName.equals("")){
			billToName=billToName.replaceAll("'","''");
			if(flag.equals("1")){
			wherecl=wherecl+" and b.billToName like '%"+billToName+"%'";
			}else{
				wherecl="and b.billToName like '%"+billToName+"%'";
			flag="1";
			}
		}
		if(originCity!=null && !originCity.equals("")){
			originCity=originCity.replaceAll("'","''");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.originCity like '%"+originCity+"%'";
			}else{
				wherecl="and s.originCity like '%"+originCity+"%'";
			flag="1";
			}
		}
		if(destinationCity!=null && !destinationCity.equals("")){
			destinationCity=destinationCity.replaceAll("'","''");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.destinationCity like '%"+destinationCity+"%'";
			}else{
				wherecl="and s.destinationCity like '%"+destinationCity+"%'";
			flag="1";
			}
		}
		if(originZip!=null && !originZip.equals("")){
			originZip=originZip.replaceAll("'","''");
			if(flag.equals("1")){
				wherecl=wherecl+" and s.originZip like '%"+originZip+"%'";
			}else{
				wherecl="and s.originZip like '%"+originZip+"%'";
			flag="1";
			}
		}
		if(destinationZip!=null && !destinationZip.equals("")){
			destinationZip=destinationZip.replaceAll("'","''");
			if(flag.equals("1")){
				wherecl=wherecl+" and s.destinationZip like '%"+destinationZip+"%'";
			}else{
				wherecl="and s.destinationZip like '%"+destinationZip+"%'";
			flag="1";
			}
		}
		if(originCountry!=null && !originCountry.equals("")){
			if(flag.equals("1")){
				wherecl=wherecl+" and s.originCountry ='" + originCountry + "'";
			}else{
				wherecl="and s.originCountry = '"+originCountry+"'";
			flag="1";
			}
		}
		if(destinationCountry!=null && !destinationCountry.equals("")){
			if(flag.equals("1")){
				wherecl=wherecl+" and s.destinationCountry = '"+destinationCountry+"'";
			}else{
				wherecl="and s.destinationCountry = '"+destinationCountry+"'";
			flag="1";
			}
		}
		if(activeStatus!=null && activeStatus){
			if(flag.equals("1")){
			wherecl=wherecl+" and s.status not in ('CNCL','CLSD')";
			}else{
				wherecl="and s.status not in ('CNCL','CLSD')";
			flag="1";
			}
		}
		if(salesMan!=null && !salesMan.equals("")){
			salesMan=salesMan.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and salesMan like '%"+salesMan+"%'";
			}else{
				wherecl="and salesMan like '%"+salesMan+"%'";
			flag="1";
			}
		}
		
		if(certificateNumber!=null && !certificateNumber.equals("")){
			certificateNumber=certificateNumber.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and b.certnum like '%"+certificateNumber+"%'";
			}else{
				wherecl="and b.certnum like '%"+certificateNumber+"%'";
			flag="1";
			}
		}
		if(daShipmentNumber!=null && !daShipmentNumber.equals("")){
			daShipmentNumber=daShipmentNumber.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and t.daShipmentNum like '%"+daShipmentNumber+"%'";
			}else{
				wherecl="and t.daShipmentNum like '%"+daShipmentNumber+"%'";
			flag="1";
			}
		}
		if(estimator!=null && !estimator.equals("")){
			if(flag.equals("1")){
				wherecl=wherecl+" and s.estimator like '%"+estimator+"%'";
				}else{
					wherecl="and s.estimator like '%"+estimator+"%'";
				flag="1";
				}
		}
		wherecl=wherecl+companyDivisionCondision+billtocodeCondision+bookingAgentCondision;
		
 		if(!wherecl.equals("")){
			MasterList=MasterList+" "+wherecl;	
		}
		List listMasterList = this.getSession().createSQLQuery(MasterList+" order by id desc limit "+recordLimit+" ")
		.addScalar("shipnumber")
		.addScalar("registrationnumber")
		.addScalar("role")
		.addScalar("job")
		.addScalar("shipmenttype")
		.addScalar("coordinator")
		.addScalar("status")
		.addScalar("id")
		.addScalar("survey",Hibernate.DATE)
		.addScalar("estweight")
		.addScalar("ldpkticket")
		.addScalar("loading",Hibernate.DATE)
		.addScalar("actualweight")
		.addScalar("driver")
		.addScalar("truck")
		.addScalar("deliveryticket")
		.addScalar("delivery",Hibernate.DATE)
		.addScalar("claims")
		.addScalar("qc")
		.addScalar("estSurvey")
		.addScalar("estLoding")
		.addScalar("estDelivery")
		.addScalar("lastName")
		.addScalar("firstName")
		.addScalar("mode")
		.addScalar("actVolume")
		.addScalar("estvolume")
		.addScalar("dpt")
		.addScalar("arvl")
		.addScalar("loadingDt")
		.addScalar("deliveryDt")
		.addScalar("surveyDt")
		.addScalar("dptF")
		.addScalar("arvlF")
		.addScalar("clearCustom")
		.addScalar("clearCustomFlag")
		.addScalar("bookingAgentCode")
		.addScalar("serviceType")
		.addScalar("billToName")
		.addScalar("OrginCC")
		.addScalar("destinCC")
		.addScalar("loadF")
		.addScalar("deliveryF")
		.addScalar("surveyF")
		.addScalar("ld")
		.addScalar("dt")
		.addScalar("asd")
		.addScalar("sortDpt",Hibernate.DATE)
		.addScalar("sortArv",Hibernate.DATE)
		.addScalar("sortCustDt",Hibernate.DATE)
		.addScalar("qcSort",Hibernate.DATE)
		.addScalar("serviceOrderMoveType",Hibernate.STRING)
		.addScalar("commodity")
		.addScalar("routing")
		.addScalar("createdOn")
		.addScalar("cid")
		.addScalar("salesMan")
		.list();
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			SODashboardDTO dasboardDTO = new SODashboardDTO();
			String serviceType="";
			String flex5="";
			String loadF="";
			String deliveryF="";
			String surveyF="";
			 if (row[41] == null) {
				 dasboardDTO.setLoadAT("");	
	   			} else {
	   				loadF=row[41].toString();
	   			 dasboardDTO.setLoadAT(loadF);
	   			}
			 if (row[3] == null) {
					dasboardDTO.setJob("");
				} else {
					dasboardDTO.setJob(row[3].toString());
				}
	             if (row[42] == null) {
	            	 dasboardDTO.setDelAT("");		
	   			} else {
	   				deliveryF=(row[42].toString());
	   				dasboardDTO.setDelAT(deliveryF);
	   			}
	             if (row[43] == null) {
	            	 dasboardDTO.setSurvAT("");
	   			} else {
	   				surveyF=(row[43].toString());
	   				dasboardDTO.setSurvAT(surveyF);
	   			}
			if (row[37] == null) {
				
			} else {
				serviceType=row[37].toString();
			}
			flex5=servFlex.get(serviceType);
			if (row[36] == null) {
				dasboardDTO.setIsBA("");
			} else {
				String bacode=row[36].toString();
				String isBAquery="select corpID from companydivision where corpid = '"+corpId+"' and bookingagentcode = '"+bacode+"'";
				List lista= this.getSession().createSQLQuery(isBAquery).list(); 
				if(lista!=null && !(lista.isEmpty()) && (lista.get(0)!=null)){
					dasboardDTO.setIsBA("yes");
				}else{
					dasboardDTO.setIsBA("");
				}
			}
			//Start survey
			if (row[46] == null) {				
				dasboardDTO.setsD("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setsDFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Survey")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setsDFlag("NA");
						}
					}
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setsDFlag("NA");
				}
			} else {
				String dt=row[46].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);				
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setsD(row[46].toString());
				if(row[8]!=null){
				dasboardDTO.setSortSurveyDt(row[8]);
				}
				
				}catch(Exception e){
					dasboardDTO.setsD(dt);
					if(row[8]!=null){
						dasboardDTO.setSortSurveyDt(row[8]);
						}
					
				}
				if(surveyF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setsDFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Survey")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setsDFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
					Company company = companyManager.findByCorpID(corpId).get(0);
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
					format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
					String curr=format.format(dt1);
					try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);
					if(date1.equals(date2)){
						dasboardDTO.setsDFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setsDFlag("OD");
					}else{
						dasboardDTO.setsDFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setDepFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dt1=new Date();
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setsDFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setsDFlag("OD");
						}else{
							dasboardDTO.setsDFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Survey")>-1){
	                	  dt1=new Date();
							curr=format.format(dt1);
						try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setsDFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setsDFlag("OD");
	  					}else{
	  						dasboardDTO.setsDFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setsDFlag("NA");
							}
						}
					}	
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setsDFlag("NA");
				}
					
			}
			//end survey
			
			//Start Delivery
			if (row[45] == null) {				
				dasboardDTO.setdL("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setdLFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setdLFlag("NA");
						}
					}
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setdLFlag("NA");
				}
			} else {
				String dt=row[45].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setdL(row[45].toString());
				if(row[16]!=null){
					dasboardDTO.setSortDeliveryDt(row[16]);
					}
				}catch(Exception e){
					dasboardDTO.setdL(dt);
					if(row[16]!=null){
						dasboardDTO.setSortDeliveryDt(row[16]);
						}
				}
				if(deliveryF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setdLFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setdLFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
					Company company = companyManager.findByCorpID(corpId).get(0);
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
					format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
					String curr=format.format(dt1);
  					try{
  					Date date1 = format.parse(dt);
  					Date date2 = format.parse(curr);					
  					if(date1.equals(date2)){
  						dasboardDTO.setdLFlag("DT");
  					}else if(date1.before(date2)){
  						dasboardDTO.setdLFlag("OD");
  					}else{
  						dasboardDTO.setdLFlag("");	
  					}
  					}catch(Exception e){
  						//dasboardDTO.setDepFlag("NA");	
  					}	
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setdLFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setdLFlag("OD");
						}else{
							dasboardDTO.setdLFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
	                	  dt1=new Date();	
	  					  curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setdLFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setdLFlag("OD");
	  					}else{
	  						dasboardDTO.setdLFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setdLFlag("NA");
							}
						}
					}	
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setdLFlag("NA");
				}	
			}
			//End Delivery
			//Start Loading
			if (row[44] == null) {				
				dasboardDTO.setLd("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setLdFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Loading")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setLdFlag("NA");
						}
					}
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setLdFlag("NA");
				}
			} else {
				String dt=row[44].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setLd(row[44].toString());
				if(row[11]!=null){
					dasboardDTO.setSortLoadingDt(row[11]);
					}
				}catch(Exception e){
					dasboardDTO.setLd(dt);	
					if(row[11]!=null){
						dasboardDTO.setSortLoadingDt(row[11]);
						}
				}
				if(loadF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setLdFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Loading")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setLdFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
					Company company = companyManager.findByCorpID(corpId).get(0);
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
					format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
					String curr=format.format(dt1);	
					try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);					
					if(date1.equals(date2)){
						dasboardDTO.setLdFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setLdFlag("OD");
					}else{
						dasboardDTO.setLdFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setDepFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setLdFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setLdFlag("OD");
						}else{
							dasboardDTO.setLdFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Loading")>-1){
	                	   dt1=new Date();	
	   					curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setLdFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setLdFlag("OD");
	  					}else{
	  						dasboardDTO.setLdFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setLdFlag("NA");
							}
						}
					}	
				}
				if(dasboardDTO.getJob().equals("RLO")){
					dasboardDTO.setLdFlag("NA");
				}		
			}
			//End Loading
			if (row[37] == null) {
				
			} else {
				serviceType=row[37].toString();
			}
			flex5=servFlex.get(serviceType);
			if (row[36] == null) {
				dasboardDTO.setIsBA("");
			} else {
				String bacode=row[36].toString();
				String isBAquery="select corpID from companydivision where corpid = '"+corpId+"' and bookingagentcode = '"+bacode+"'";
				List lista= this.getSession().createSQLQuery(isBAquery).list(); 
				if(lista!=null && !(lista.isEmpty()) && (lista.get(0)!=null)){
					dasboardDTO.setIsBA("yes");
				}else{
					dasboardDTO.setIsBA("");
				}
			}
			if (row[0] == null) {
				dasboardDTO.setShipnumber("");
			} else {
				dasboardDTO.setShipnumber(row[0].toString());
			}
			if (row[1] == null) {
				dasboardDTO.setRegistrationnumber("");
			} else {
				dasboardDTO.setRegistrationnumber(row[1].toString());
			}
			
			if (row[2] == null) {
				dasboardDTO.setRole("");
			} else {
				dasboardDTO.setRole(row[2].toString());
			}			
			
			
			if (row[4] == null) {
				dasboardDTO.setShipmenttype("");
			} else {
				dasboardDTO.setShipmenttype(row[4].toString());
			}
			if (row[5] == null) {
				dasboardDTO.setCoordinator("");
			} else {
				dasboardDTO.setCoordinator(row[5].toString());
			}
			if (row[6] == null) {
				dasboardDTO.setStatus("");
			} else {
				dasboardDTO.setStatus(row[6].toString());
			}
			if (row[7] == null) {
				dasboardDTO.setId(new Long("00"));
			} else {
				dasboardDTO.setId(new Long(row[7].toString()));
			}
			
			if (row[9] == null) {
				dasboardDTO.setEstweight("");
			} else {
				dasboardDTO.setEstweight(row[9].toString());
			}
			if (row[10] == null) {
				dasboardDTO.setLdpkticket("");
			} else {
				dasboardDTO.setLdpkticket(row[10].toString());
			}
			
			if (row[12] == null) {
				dasboardDTO.setActualweight("");
			} else {
				dasboardDTO.setActualweight(row[12].toString());
			}
			if (row[24] == null) {
				dasboardDTO.setMode("");
			} else {
				dasboardDTO.setMode(row[24].toString());
			}
			if (row[13] == null) {
				dasboardDTO.setDriver("");
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDriver1("");
					}else{
						dasboardDTO.setDriver1("NA");
					}
				}
			} else {
				dasboardDTO.setDriver(row[13].toString());
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setDriver1("");
					}else{
					dasboardDTO.setDriver1("NA");
					}
				}
			}
			if (row[14] == null) {
				dasboardDTO.setTruck("");
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setTruck1("");
					}else{
					dasboardDTO.setTruck1("NA");
					}
				}
			} else {
				dasboardDTO.setTruck(row[14].toString());
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setTruck1("");
					}else{
					dasboardDTO.setTruck1("NA");
					}
				}
			}			
			if (row[15] == null) {
				dasboardDTO.setDeliveryticket("");
			} else {
				dasboardDTO.setDeliveryticket(row[15].toString());
			}
			
			if (row[17] == null) {
				dasboardDTO.setClaims("");
			} else {
				dasboardDTO.setClaims(row[17].toString());
			}
			if (row[18] == null) {
				dasboardDTO.setQc("");
			} else {
				String dt=row[18].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setQc(row[18].toString());
				}catch(Exception e){
					dasboardDTO.setQc(dt);
				}
			}
			if (row[19] == null) {
				dasboardDTO.setEstSurvey("");
			} else {
				String dt=row[19].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstSurvey(row[19].toString());
				}catch(Exception e){
					dasboardDTO.setEstSurvey(dt);
				}
				//
			}
			if (row[20] == null) {
				dasboardDTO.setEstLoding("");
			} else {
				String dt=row[20].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstLoding(row[20].toString());
				}catch(Exception e){
					dasboardDTO.setEstLoding(dt);
				}
				//
			}
			if (row[21] == null) {
				dasboardDTO.setEstDelivery("");
			} else {
				String dt=row[21].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstDelivery(row[21].toString());
				}catch(Exception e){
					dasboardDTO.setEstDelivery(dt);
				}
				//
			}
			if (row[22] == null) {
				dasboardDTO.setLastName("");
			} else {
				dasboardDTO.setLastName(row[22].toString());
			}
			if (row[23] == null) {
				dasboardDTO.setFirstName("");
			} else {
				dasboardDTO.setFirstName(row[23].toString());
			}
			
			if (row[25] == null) {
				dasboardDTO.setActualVolume("");
			} else {
				dasboardDTO.setActualVolume(row[25].toString());
			}
			if (row[26] == null) {
				dasboardDTO.setEstVolume("");
			} else {
				dasboardDTO.setEstVolume(row[26].toString());
			}
			if (row[32] == null) {
				dasboardDTO.setDptF("");
			} else {
				dasboardDTO.setDptF(row[32].toString());
			}
			if (row[33] == null) {
				dasboardDTO.setArvlF("");
			} else {
				dasboardDTO.setArvlF(row[33].toString());
			}
			//Start departure
			if (row[27] == null) {				
				dasboardDTO.setDep("");				
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setDepFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("ETD")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setDepFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
			} else {
				String dt=row[27].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setDep(row[27].toString());	
				if(row[47]!=null){
					dasboardDTO.setSortDpt(row[47]);
					}
				}catch(Exception e){
					dasboardDTO.setDep(dt);
					if(row[47]!=null){
						dasboardDTO.setSortDpt(row[47]);
						}
					
				}
				if(dasboardDTO.getDptF().equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setDepFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("ETD")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setDepFlag("NA");
							}
						}
					}
				}else{				
					  Date dt1=new Date();	
					  	Company company = companyManager.findByCorpID(corpId).get(0);
						SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
						format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
						String curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setDepFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setDepFlag("OD");
	  					}else{
	  						dasboardDTO.setDepFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setDepFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setDepFlag("OD");
						}else{
							dasboardDTO.setDepFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("ETD")>-1){
	                	  dt1=new Date();	
	  					curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setDepFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setDepFlag("OD");
	  					}else{
	  						dasboardDTO.setDepFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setDepFlag("NA");
							}
						}
					}	
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}	
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
			}
			//End Departure
			//Start Arrival
			if (row[28] == null) {
				dasboardDTO.setArv("");				
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setArvFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("ETA")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setArvFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
			} else {
				String dt=row[28].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setArv(row[28].toString());
				if(row[48]!=null){
					dasboardDTO.setSortArv(row[48]);
					}
				}catch(Exception e){
					dasboardDTO.setArv(dt);	
					if(row[48]!=null){
						dasboardDTO.setSortArv(row[48]);
						}
				}
				if(dasboardDTO.getArvlF().equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setArvFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("ETA")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setArvFlag("NA");
							}
						}
					}
				}else{	
					Date dt1=new Date();	
					Company company = companyManager.findByCorpID(corpId).get(0);
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
					format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
					String curr=format.format(dt1);	
					try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setArvFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setArvFlag("OD");
						}else{
							dasboardDTO.setArvFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setArvFlag("NA");	
						}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){				
					 dt1=new Date();	
					 curr=format.format(dt1);	
					try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setArvFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setArvFlag("OD");
						}else{
							dasboardDTO.setArvFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setArvFlag("NA");	
						}					
					}else{
						if(flex5!=null && flex5.indexOf("ETA")>-1){
							 dt1=new Date();	
							 curr=format.format(dt1);	
							try{
								Date date1 = format.parse(dt);
								Date date2 = format.parse(curr);					
								if(date1.equals(date2)){
									dasboardDTO.setArvFlag("DT");
								}else if(date1.before(date2)){
									dasboardDTO.setArvFlag("OD");
								}else{
									dasboardDTO.setArvFlag("");	
								}
								}catch(Exception e){
									//dasboardDTO.setArvFlag("NA");	
								}		
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setArvFlag("NA");
							}
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
			}
			//End Arrival
			if (row[29] == null) {
				dasboardDTO.setLoadingDt("");
			} else {
				String dt=row[29].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setLoadingDt(row[29].toString());
				}catch(Exception e){
					dasboardDTO.setLoadingDt(dt);
				}
			}
			if (row[30] == null) {
				dasboardDTO.setDeliveryDt("");
			} else {
				String dt=row[30].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setDeliveryDt(row[30].toString());
				}catch(Exception e){
					dasboardDTO.setDeliveryDt(dt);
				}
			}
			if (row[31] == null) {
				dasboardDTO.setSurveyDt("");
			} else {
				String dt=row[31].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setSurveyDt(row[31].toString());
				}catch(Exception e){
					dasboardDTO.setSurveyDt(dt);
				}
			}
			
			if (row[35] == null) {
				dasboardDTO.setCustomDateF("");
			} else {
				dasboardDTO.setCustomDateF(row[35].toString());
			}
			if (row[34] == null) {
				dasboardDTO.setCustomDate("");
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setCustFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("CC")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setCustFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				
			} else {
				dasboardDTO.setCustomDate(row[34].toString());
				if(row[49]!=null){
					dasboardDTO.setSortCustDt(row[49]);
					}
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setCustFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("CC")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setCustFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
			}
			//start CustomDate
			if(dasboardDTO.getCustomDateF()!=null && dasboardDTO.getCustomDateF().toString().equals("T")){	
				Date dt1=new Date();
				String dt="";
				if(dasboardDTO.getCustomDate()!=null){
				dt=dasboardDTO.getCustomDate().toString();
				}
				Company company = companyManager.findByCorpID(corpId).get(0);
				SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");
				format.setTimeZone(TimeZone.getTimeZone(company.getTimeZone()));
				String curr=format.format(dt1);
				try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);					
					if(date1.equals(date2)){
						dasboardDTO.setCustFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setCustFlag("OD");
					}else{
						dasboardDTO.setCustFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setCustFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();
						 dt="";
						if(dasboardDTO.getCustomDate()!=null){
						dt=dasboardDTO.getCustomDate().toString();
						}
						curr=format.format(dt1);	
						try{
							Date date1 = format.parse(dt);
							Date date2 = format.parse(curr);					
							if(date1.equals(date2)){
								dasboardDTO.setCustFlag("DT");
							}else if(date1.before(date2)){
								dasboardDTO.setCustFlag("OD");
							}else{
								dasboardDTO.setCustFlag("");	
							}
							}catch(Exception e){
								//dasboardDTO.setCustFlag("NA");	
							}
					}else{
	                  if(flex5!=null && flex5.indexOf("CC")>-1){
	                	   dt1=new Date();
	      				 dt="";
	      				if(dasboardDTO.getCustomDate()!=null){
	      				dt=dasboardDTO.getCustomDate().toString();
	      				}
						curr=format.format(dt1);	
	      				try{
	      					Date date1 = format.parse(dt);
	      					Date date2 = format.parse(curr);					
	      					if(date1.equals(date2)){
	      						dasboardDTO.setCustFlag("DT");
	      					}else if(date1.before(date2)){
	      						dasboardDTO.setCustFlag("OD");
	      					}else{
	      						dasboardDTO.setCustFlag("");	
	      					}
	      					}catch(Exception e){
	      						//dasboardDTO.setCustFlag("NA");	
	      					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setCustFlag("NA");
							}
						}
					}
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
					if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
					if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
			}
			//End CustomDate
			 if (row[38] == null) {
	 				dasboardDTO.setBillToName("");
	 			} else {
	 				dasboardDTO.setBillToName(row[38].toString());
	 			}
	             if (row[39] == null) {
	  				dasboardDTO.setOrginCC("");
	  			} else {
	  				dasboardDTO.setOrginCC(row[39].toString());
	  			}
	             if (row[40] == null) {
	  				dasboardDTO.setDestinCC("");
	  			} else {
	  				dasboardDTO.setDestinCC(row[40].toString());
	  			}
	             if(row[50]!=null){
						dasboardDTO.setQcSortDt(row[50]);
						}
	             if(row[51]==null){
						dasboardDTO.setServiceOrderMoveType("");
						}
	             if(row[51]!=null){
						dasboardDTO.setServiceOrderMoveType(row[51].toString());
						}
	             if(row[52]==null){
						dasboardDTO.setCommodity("");
						}
	             if(row[52]!=null){
						dasboardDTO.setCommodity(row[52].toString());
						}
	             if(row[53]==null){
						dasboardDTO.setRouting("");
						}
	             if(row[53]!=null){
						dasboardDTO.setRouting(row[53].toString());
						}
	             if(row[54]==null){
						dasboardDTO.setCreatedOn("");
						}
	             if(row[54]!=null){
						dasboardDTO.setCreatedOn(row[54].toString());
						}
	 			if (row[55] == null) {
					dasboardDTO.setCid(new Long("00"));
				} else {
					dasboardDTO.setCid(new Long(row[55].toString()));
				}
	 			if (row[56] == null) {
	 				dasboardDTO.setSalesMan("");
				} else {
					dasboardDTO.setSalesMan(row[56].toString());
				}
	             
	             soDashboardSearchList.add(dasboardDTO);
	}
		return soDashboardSearchList;
	}
	public String getVlJobTypes(String corpid){
		String query="select miscVl from systemdefault where corpid='"+corpid+"'";
		String vljob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			vljob=listMasterList.get(0).toString();
		}
		return vljob;
	}
	public String getStorageJobTypes(String corpid){
		String query="select storage from systemdefault where corpid='"+corpid+"'";
		String stojob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			stojob=listMasterList.get(0).toString();
		}
		return stojob;
	}
	public String getLocalJobsTypes(String corpid){
		String query="select localJobs from systemdefault where corpid='"+corpid+"'";
		String lcjob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			lcjob=listMasterList.get(0).toString();
		}
		return lcjob;
	}
	public Map<String,String> getFieldsNameToDisplay(){
		Map visibleFields = new LinkedHashMap<String, String>();
		String query="select code,flex5 from refmaster where corpId='TSFT' ";
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
			while(it.hasNext())
			{
				Object []row= (Object [])it.next();
				String flex5="";
				if(row[1]==null){
					
				}else{
					flex5=row[1].toString();
				}
				visibleFields.put(row[0].toString(),flex5 );			
				
			}
		return visibleFields;
	}
	public class SODashboardDTO {
		private String shipnumber;
		private String role;
		private String job;
		private String shipmenttype;
		private String coordinator;
		private String status;
		private String registrationnumber;
		private Object id;
		private Object survey;
		private Object estweight;
		private Object ldpkticket;
		private Object loading;
		private Object actualweight;
		private Object driver;
		private Object truck;
		private Object deliveryticket;
		private Object delivery;
		private Object claims;
		private Object qc;
		private Object estSurvey;
		private Object estLoding;
		private Object estDelivery;
		private Object lastName;
		private Object firstName;
		private String mode;
		private String actualVolume;
		private String estVolume;
		private Object dep;
		private Object arv;
		private Object loadingDt;
		private Object deliveryDt;
		private Object surveyDt;
		private Object dptF;
		private Object arvlF;
		private Object depFlag;
		private Object arvFlag;
		private Object customDate;
		private Object customDateF;
		private Object custFlag;
		private String isBA;
		private Object survey1;
		private Object loading1;
		private Object delivery1;
		private Object billToName;
		private Object orginCC;
		private Object destinCC;
		private Object ld;
		private Object dL;
		private Object sD;
		private Object sortSurveyDt;
		private Object sortLoadingDt;
		private Object sortDeliveryDt;
		private Object sortDpt;
		private Object sortArv;
		private Object sortCustDt;
		private Object qcSortDt;
		private Object ldFlag;
		private Object dLFlag;
		private Object sDFlag;
		private Object loadAT;
		private Object delAT;
		private Object survAT;
		private Object driver1;
		private Object truck1;
		private Object serviceOrderMoveType;
		private Object commodity;
		private Object routing;
		private Object createdOn;
		private Object cid;
		private Object salesMan;
		
		public Object getCustFlag() {
			return custFlag;
		}
		public void setCustFlag(Object custFlag) {
			this.custFlag = custFlag;
		}
		public Object getCustomDate() {
			return customDate;
		}
		public void setCustomDate(Object customDate) {
			this.customDate = customDate;
		}
		public Object getCustomDateF() {
			return customDateF;
		}
		public void setCustomDateF(Object customDateF) {
			this.customDateF = customDateF;
		}
		public Object getArvFlag() {
			return arvFlag;
		}
		public void setArvFlag(Object arvFlag) {
			this.arvFlag = arvFlag;
		}
		public Object getDptF() {
			return dptF;
		}
		public void setDptF(Object dptF) {
			this.dptF = dptF;
		}
		public Object getArvlF() {
			return arvlF;
		}
		public void setArvlF(Object arvlF) {
			this.arvlF = arvlF;
		}
		public Object getDepFlag() {
			return depFlag;
		}
		public void setDepFlag(Object depFlag) {
			this.depFlag = depFlag;
		}
		public Object getLoadingDt() {
			return loadingDt;
		}
		public void setLoadingDt(Object loadingDt) {
			this.loadingDt = loadingDt;
		}
		public Object getDeliveryDt() {
			return deliveryDt;
		}
		public void setDeliveryDt(Object deliveryDt) {
			this.deliveryDt = deliveryDt;
		}
		public Object getSurveyDt() {
			return surveyDt;
		}
		public void setSurveyDt(Object surveyDt) {
			this.surveyDt = surveyDt;
		}
		public Object getDep() {
			return dep;
		}
		public void setDep(Object dep) {
			this.dep = dep;
		}
		public Object getArv() {
			return arv;
		}
		public void setArv(Object arv) {
			this.arv = arv;
		}
		public String getShipnumber() {
			return shipnumber;
		}
		public void setShipnumber(String shipnumber) {
			this.shipnumber = shipnumber;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getJob() {
			return job;
		}
		public void setJob(String job) {
			this.job = job;
		}
		public String getShipmenttype() {
			return shipmenttype;
		}
		public void setShipmenttype(String shipmenttype) {
			this.shipmenttype = shipmenttype;
		}
		public String getCoordinator() {
			return coordinator;
		}
		public void setCoordinator(String coordinator) {
			this.coordinator = coordinator;
		}
		public Object getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getRegistrationnumber() {
			return registrationnumber;
		}
		public void setRegistrationnumber(String registrationnumber) {
			this.registrationnumber = registrationnumber;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getSurvey() {
			return survey;
		}
		public void setSurvey(Object survey) {
			this.survey = survey;
		}
		public Object getEstweight() {
			return estweight;
		}
		public void setEstweight(Object estweight) {
			this.estweight = estweight;
		}
		public Object getLdpkticket() {
			return ldpkticket;
		}
		public void setLdpkticket(Object ldpkticket) {
			this.ldpkticket = ldpkticket;
		}
		public Object getLoading() {
			return loading;
		}
		public void setLoading(Object loading) {
			this.loading = loading;
		}
		public Object getActualweight() {
			return actualweight;
		}
		public void setActualweight(Object actualweight) {
			this.actualweight = actualweight;
		}
		public Object getDriver() {
			return driver;
		}
		public void setDriver(Object driver) {
			this.driver = driver;
		}
		public Object getTruck() {
			return truck;
		}
		public void setTruck(Object truck) {
			this.truck = truck;
		}
		public Object getDeliveryticket() {
			return deliveryticket;
		}
		public void setDeliveryticket(Object deliveryticket) {
			this.deliveryticket = deliveryticket;
		}
		public Object getDelivery() {
			return delivery;
		}
		public void setDelivery(Object delivery) {
			this.delivery = delivery;
		}
		public Object getClaims() {
			return claims;
		}
		public void setClaims(Object claims) {
			this.claims = claims;
		}
		public Object getQc() {
			return qc;
		}
		public void setQc(Object qc) {
			this.qc = qc;
		}
		public Object getEstSurvey() {
			return estSurvey;
		}
		public void setEstSurvey(Object estSurvey) {
			this.estSurvey = estSurvey;
		}
		public Object getEstLoding() {
			return estLoding;
		}
		public void setEstLoding(Object estLoding) {
			this.estLoding = estLoding;
		}
		public Object getEstDelivery() {
			return estDelivery;
		}
		public void setEstDelivery(Object estDelivery) {
			this.estDelivery = estDelivery;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public String getMode() {
			return mode;
		}
		public void setMode(String mode) {
			this.mode = mode;
		}
		public String getActualVolume() {
			return actualVolume;
		}
		public void setActualVolume(String actualVolume) {
			this.actualVolume = actualVolume;
		}
		public String getEstVolume() {
			return estVolume;
		}
		public void setEstVolume(String estVolume) {
			this.estVolume = estVolume;
		}
		public String getIsBA() {
			return isBA;
		}
		public void setIsBA(String isBA) {
			this.isBA = isBA;
		}
		public Object getSurvey1() {
			return survey1;
		}
		public void setSurvey1(Object survey1) {
			this.survey1 = survey1;
		}
		public Object getLoading1() {
			return loading1;
		}
		public void setLoading1(Object loading1) {
			this.loading1 = loading1;
		}
		public Object getDelivery1() {
			return delivery1;
		}
		public void setDelivery1(Object delivery1) {
			this.delivery1 = delivery1;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getOrginCC() {
			return orginCC;
		}
		public void setOrginCC(Object orginCC) {
			this.orginCC = orginCC;
		}
		public Object getDestinCC() {
			return destinCC;
		}
		public void setDestinCC(Object destinCC) {
			this.destinCC = destinCC;
		}
		public Object getLd() {
			return ld;
		}
		public void setLd(Object ld) {
			this.ld = ld;
		}
		public Object getdL() {
			return dL;
		}
		public void setdL(Object dL) {
			this.dL = dL;
		}
		public Object getsD() {
			return sD;
		}
		public void setsD(Object sD) {
			this.sD = sD;
		}
		public Object getLdFlag() {
			return ldFlag;
		}
		public void setLdFlag(Object ldFlag) {
			this.ldFlag = ldFlag;
		}
		public Object getdLFlag() {
			return dLFlag;
		}
		public void setdLFlag(Object dLFlag) {
			this.dLFlag = dLFlag;
		}
		public Object getsDFlag() {
			return sDFlag;
		}
		public void setsDFlag(Object sDFlag) {
			this.sDFlag = sDFlag;
		}
		public Object getLoadAT() {
			return loadAT;
		}
		public void setLoadAT(Object loadAT) {
			this.loadAT = loadAT;
		}
		public Object getDelAT() {
			return delAT;
		}
		public void setDelAT(Object delAT) {
			this.delAT = delAT;
		}
		public Object getSurvAT() {
			return survAT;
		}
		public void setSurvAT(Object survAT) {
			this.survAT = survAT;
		}
		public Object getDriver1() {
			return driver1;
		}
		public void setDriver1(Object driver1) {
			this.driver1 = driver1;
		}
		public Object getTruck1() {
			return truck1;
		}
		public void setTruck1(Object truck1) {
			this.truck1 = truck1;
		}
		public Object getSortSurveyDt() {
			return sortSurveyDt;
		}
		public void setSortSurveyDt(Object sortSurveyDt) {
			this.sortSurveyDt = sortSurveyDt;
		}
		public Object getSortLoadingDt() {
			return sortLoadingDt;
		}
		public void setSortLoadingDt(Object sortLoadingDt) {
			this.sortLoadingDt = sortLoadingDt;
		}
		public Object getSortDeliveryDt() {
			return sortDeliveryDt;
		}
		public void setSortDeliveryDt(Object sortDeliveryDt) {
			this.sortDeliveryDt = sortDeliveryDt;
		}
		public Object getSortDpt() {
			return sortDpt;
		}
		public void setSortDpt(Object sortDpt) {
			this.sortDpt = sortDpt;
		}
		public Object getSortArv() {
			return sortArv;
		}
		public void setSortArv(Object sortArv) {
			this.sortArv = sortArv;
		}
		public Object getSortCustDt() {
			return sortCustDt;
		}
		public void setSortCustDt(Object sortCustDt) {
			this.sortCustDt = sortCustDt;
		}
		public Object getQcSortDt() {
			return qcSortDt;
		}
		public void setQcSortDt(Object qcSortDt) {
			this.qcSortDt = qcSortDt;
		}
		public Object getServiceOrderMoveType() {
			return serviceOrderMoveType;
		}
		public void setServiceOrderMoveType(Object serviceOrderMoveType) {
			this.serviceOrderMoveType = serviceOrderMoveType;
		}
		public Object getCommodity() {
			return commodity;
		}
		public void setCommodity(Object commodity) {
			this.commodity = commodity;
		}
		public Object getRouting() {
			return routing;
		}
		public void setRouting(Object routing) {
			this.routing = routing;
		}
		public Object getCreatedOn() {
			return createdOn;
		}
		public void setCreatedOn(Object createdOn) {
			this.createdOn = createdOn;
		}
		public Object getCid() {
			return cid;
		}
		public void setCid(Object cid) {
			this.cid = cid;
		}
		public Object getSalesMan() {
			return salesMan;
		}
		public void setSalesMan(Object salesMan) {
			this.salesMan = salesMan;
		}
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
}
