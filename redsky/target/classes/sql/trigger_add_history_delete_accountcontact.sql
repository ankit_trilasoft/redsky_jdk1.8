DELIMITER $$
CREATE TRIGGER trigger_add_history_delete_accountcontact BEFORE DELETE ON accountcontact
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

IF (OLD.corpID <> '' or OLD.corpID is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;
IF (OLD.department <> '' or OLD.department is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.department~') into fieldNameList;
       select CONCAT(oldValueList,OLD.department,'~') into oldValueList;
   END IF;

IF (OLD.jobType <> '' or OLD.jobType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.jobType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
   END IF;
IF (OLD.contactName <> '' or OLD.contactName is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.contactName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactName,'~') into oldValueList;
   END IF;
IF (OLD.contactEmail <> '' or OLD.contactEmail is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.contactEmail~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactEmail,'~') into oldValueList;
   END IF;
IF (OLD.partnerCode <> '' or OLD.partnerCode is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.partnerCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.partnerCode,'~') into oldValueList;
   END IF;
IF (OLD.contractType <> '' or OLD.contractType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.contractType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contractType,'~') into oldValueList;
   END IF;
IF (OLD.jobTypeOwner <> '' or OLD.jobTypeOwner is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.jobTypeOwner~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobTypeOwner,'~') into oldValueList;
   END IF;
IF (OLD.primaryPhone <> '' or OLD.primaryPhone is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.primaryPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.primaryPhone,'~') into oldValueList;
   END IF;
IF (OLD.primaryPhoneType <> '' or OLD.primaryPhoneType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.primaryPhoneType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.primaryPhoneType,'~') into oldValueList;
   END IF;
IF (OLD.secondaryPhone <> '' or OLD.secondaryPhone is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.secondaryPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.secondaryPhone,'~') into oldValueList;
   END IF;
IF (OLD.secondaryPhoneType <> '' or OLD.secondaryPhoneType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.secondaryPhoneType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.secondaryPhoneType,'~') into oldValueList;
   END IF;
IF (OLD.thirdPhone <> '' or OLD.thirdPhone is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.thirdPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.thirdPhone,'~') into oldValueList;
   END IF;
IF (OLD.thirdPhoneType <> '' or OLD.thirdPhoneType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.thirdPhoneType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.thirdPhoneType,'~') into oldValueList;
   END IF;
IF (OLD.contactFrequency <> '' or OLD.contactFrequency is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.contactFrequency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactFrequency,'~') into oldValueList;
   END IF;
IF (OLD.title <> '' or OLD.title is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.title~') into fieldNameList;
       select CONCAT(oldValueList,OLD.title,'~') into oldValueList;
   END IF;
IF (OLD.dateOfBirth <> '' or OLD.dateOfBirth is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.dateOfBirth~') into fieldNameList;
       select CONCAT(oldValueList,OLD.dateOfBirth,'~') into oldValueList;
   END IF;
IF (OLD.doNotCall <> '' or OLD.doNotCall is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.doNotCall~') into fieldNameList;
       select CONCAT(oldValueList,OLD.doNotCall,'~') into oldValueList;
   END IF;
IF (OLD.salutation <> '' or OLD.salutation is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.salutation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.salutation,'~') into oldValueList;
   END IF;
IF (OLD.contactLastName <> '' or OLD.contactLastName is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.contactLastName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.contactLastName,'~') into oldValueList;
   END IF;
IF (OLD.leadSource <> '' or OLD.leadSource is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.leadSource~') into fieldNameList;
       select CONCAT(oldValueList,OLD.leadSource,'~') into oldValueList;
   END IF;
IF (OLD.reportTo <> '' or OLD.reportTo is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.reportTo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reportTo,'~') into oldValueList;
   END IF;
IF (OLD.assistant <> '' or OLD.assistant is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.assistant~') into fieldNameList;
       select CONCAT(oldValueList,OLD.assistant,'~') into oldValueList;
   END IF;
IF (OLD.assistantPhone <> '' or OLD.assistantPhone is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.assistantPhone~') into fieldNameList;
       select CONCAT(oldValueList,OLD.assistantPhone,'~') into oldValueList;
   END IF;
IF (OLD.email2 <> '' or OLD.email2 is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.email2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.email2,'~') into oldValueList;
   END IF;
IF (OLD.emailType <> '' or OLD.emailType is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.emailType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.emailType,'~') into oldValueList;
   END IF;
IF (OLD.optedOut1 <> '' or OLD.optedOut1 is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.optedOut1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.optedOut1,'~') into oldValueList;
   END IF;
IF (OLD.invalid1 <> '' or OLD.invalid1 is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.invalid1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invalid1,'~') into oldValueList;
   END IF;
IF (OLD.optedOut2 <> '' or OLD.optedOut2 is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.optedOut2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.optedOut2,'~') into oldValueList;
   END IF;
IF (OLD.invalid2 <> '' or OLD.invalid2 is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.invalid2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.invalid2,'~') into oldValueList;
   END IF;
IF (OLD.address <> '' or OLD.address is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.address~') into fieldNameList;
       select CONCAT(oldValueList,OLD.address,'~') into oldValueList;
   END IF;
IF (OLD.country <> '' or OLD.country is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.country~') into fieldNameList;
       select CONCAT(oldValueList,OLD.country,'~') into oldValueList;
   END IF;
IF (OLD.state <> '' or OLD.state is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.state~') into fieldNameList;
       select CONCAT(oldValueList,OLD.state,'~') into oldValueList;
   END IF;
IF (OLD.city <> '' or OLD.city is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.city~') into fieldNameList;
       select CONCAT(oldValueList,OLD.city,'~') into oldValueList;
   END IF;
IF (OLD.zip <> '' or OLD.zip is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.zip~') into fieldNameList;
       select CONCAT(oldValueList,OLD.zip,'~') into oldValueList;
   END IF;
IF (OLD.visitedOn <> '' or OLD.visitedOn is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.visitedOn~') into fieldNameList;
       select CONCAT(oldValueList,OLD.visitedOn,'~') into oldValueList;
   END IF;
IF (OLD.leadSourceOther <> '' or OLD.leadSourceOther is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.leadSourceOther~') into fieldNameList;
       select CONCAT(oldValueList,OLD.leadSourceOther,'~') into oldValueList;
   END IF;
IF (OLD.userName <> '' or OLD.userName is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.userName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.userName,'~') into oldValueList;
   END IF;
IF (OLD.basedAt <> '' or OLD.basedAt is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.basedAt~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basedAt,'~') into oldValueList;
   END IF;
IF (OLD.basedAtName <> '' or OLD.basedAtName is not null) THEN
       select CONCAT(fieldNameList,'accountcontact.basedAtName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.basedAtName,'~') into oldValueList;
   END IF;

   
   CALL add_tblHistory (OLD.id,"accountcontact", fieldNameList, oldValueList, newValueList, OLD.updatedby, OLD.corpID, now());
END
$$