package com.trilasoft.app.service.impl;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.apache.pdfbox.exceptions.CryptographyException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.BadSecurityHandlerException;
import org.apache.pdfbox.pdmodel.encryption.StandardDecryptionMaterial;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDCcitt;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.appfuse.Constants;

import com.asprise.util.tiff.TIFFReader;
import com.lowagie.text.pdf.PdfReader;

public class PdfImageExtractor {

	private int imageCounter = 1;

	private static final String PASSWORD = "-password";

	private static final String PREFIX = "-prefix";

	private String tempFolderPath;

	public PdfImageExtractor() {
	}

	public String getTempFolderPath() {
		return tempFolderPath;
	}

	public void setTempFolderPath(String tempFolderPath) {
		this.tempFolderPath = tempFolderPath;
	}

	public static void main(String[] args) throws Exception {
		PdfImageExtractor extractor = new PdfImageExtractor();
		extractor.extractImages(args, 0);
	}

	public List<String> extractImages(String[] args, int pageCount) throws IOException {
		List<String> imageList = new ArrayList<String>();
		if (args.length < 1 || args.length > 3) {
			usage();
		} else {
			String pdfFile = null;
			String password = "";
			String prefix = null;
			for (int i = 0; i < args.length; i++) {
				if (args[i].equals(PASSWORD)) {
					i++;
					if (i >= args.length) {
						usage();
					}
					password = args[i];
				} else if (args[i].equals(PREFIX)) {
					i++;
					if (i >= args.length) {
						usage();
					}
					prefix = args[i];
				} else {
					if (pdfFile == null) {
						pdfFile = args[i];
					}
				}
			}
			if (pdfFile == null) {
				usage();
			} else {
				if (prefix == null && pdfFile.length() > 4) {
					prefix = pdfFile.substring(0, pdfFile.length() - 4);
				}

				PDDocument document = null;

				try {
					new PdfReader(pdfFile); //to ensure the file is valid. PDDocument does not recover if made to open corrupt files
					document = PDDocument.load(pdfFile);

					if (document.isEncrypted()) {

						StandardDecryptionMaterial spm = new StandardDecryptionMaterial(password);
						try {
							document.openProtection(spm);
						} catch (BadSecurityHandlerException e) {
							e.printStackTrace();
							throw (new IOException(e.getMessage()));
						} catch (CryptographyException e) {
							e.printStackTrace();
							throw (new IOException(e.getMessage()));
						}
						AccessPermission ap = document.getCurrentAccessPermission();

						if (!ap.canExtractContent()) {
							throw new IOException("Error: You do not have permission to extract images.");
						}
					}

					List<PDPage> pages = document.getDocumentCatalog().getAllPages();
					int imageCounter = 0;
					for (int pageNum = 0; pageNum < pages.size(); pageNum++) {
						PDPage page = pages.get(pageNum);
						PDResources resources = page.getResources();
						Map<String, PDXObjectImage> images = resources.getImages();
						String separator = pdfFile.lastIndexOf("/") > -1 ? "/" : "\\";
						imageCounter++;
						//getUniqueFileName(key, image.getSuffix());

						if (images != null && images.keySet().iterator().hasNext() && images.get(images.keySet().iterator().next()) instanceof PDCcitt) {
							Iterator<String> imageIter = images.keySet().iterator();
							while (imageIter.hasNext()) {
								try {
									String key = imageIter.next();
									PDXObjectImage image = images.get(key);
									String imagefilePath = tempFolderPath
											+ Constants.FILE_SEP
											+ pdfFile
													.substring(
															pdfFile.lastIndexOf(separator) + 1,
															pdfFile.lastIndexOf("."))
											+ imageCounter;
									image.write2file(imagefilePath);
									TIFFReader reader = new TIFFReader(
											new File(imagefilePath + ".tiff"));
									ImageIO.write(
											(BufferedImage) reader.getPage(0),
											"png", new File(imagefilePath
													+ ".png"));
									imageList.add(imagefilePath + ".png");
									if (imageIter.hasNext())
										imageCounter++;
									//String outFile = imageFileName + "_thumb.png";
									//createThumbnail(inFile, outFile);
								} catch (Exception e) {
									// TODO: handle exception
								}
							}
						} else if (images != null && images.keySet().iterator().hasNext() && images.get(images.keySet().iterator().next()) instanceof PDJpeg) {
							Iterator<String> imageIter = images.keySet().iterator();
							while (imageIter.hasNext()) {
								try{
								String key = imageIter.next();
								PDXObjectImage image = images.get(key);
								String imagefilePath = tempFolderPath + Constants.FILE_SEP + pdfFile.substring(pdfFile.lastIndexOf(separator) + 1, pdfFile.lastIndexOf(".")) + imageCounter;

								image.write2file(imagefilePath);

								ImageProcessor ct = new ImageProcessor(imagefilePath + ".jpg");
								ct.getThumbnail(400, ImageProcessor.HORIZONTAL);
								ct.saveThumbnail(new File(imagefilePath + "_thumb.jpg"), ImageProcessor.IMAGE_JPG);

								imageList.add(imagefilePath + "_thumb.jpg");

								//imageList.add(imagefilePath + ".jpg");
								if (imageIter.hasNext())
									imageCounter++;
								}
								catch(Exception e)
								{
									
								}

							}
						} else {
							String imagefilePrefix = tempFolderPath + Constants.FILE_SEP + pdfFile.substring(pdfFile.lastIndexOf(separator) + 1, pdfFile.lastIndexOf(".")) + imageCounter;

							BufferedImage pageAsImage = page.convertToImage();
							ImageIO.write(pageAsImage, "png", new File(imagefilePrefix + ".png"));
							ImageProcessor ct = new ImageProcessor(imagefilePrefix + ".png");
							ct.getThumbnail(400, ImageProcessor.HORIZONTAL);
							ct.saveThumbnail(new File(imagefilePrefix + "_thumb.png"), ImageProcessor.IMAGE_PNG);

							imageList.add(imagefilePrefix + "_thumb.png");

						}
						if ((pageNum + 1) == pageCount)
							break;
					}
				} finally {
					if (document != null) {
						document.close();
					}
				}
			}
		}
		return imageList;
	}

	private String getUniqueFileName(String prefix, String suffix) {
		String uniqueName = null;
		File f = null;
		while (f == null || f.exists()) {
			uniqueName = prefix + "-" + imageCounter;
			f = new File(uniqueName + "." + suffix);
			imageCounter++;
		}
		return uniqueName;
	}

	/**
	 * This will print the usage requirements and exit.
	 */
	private static void usage() {
		System.out.println("Usage: PdfImageExtractor [OPTIONS] <PDF file>\n" + "  -password  <password> Password to decrypt document\n"
				+ "  -prefix  <image-prefix> Image prefix(default to pdf name)\n" + "  <PDF file> The PDF document to use\n");
	}

}
