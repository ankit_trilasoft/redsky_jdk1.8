package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.PartnerVanLineRefDao;
import com.trilasoft.app.model.PartnerVanLineRef;

public class PartnerVanLineRefDaoHibernate extends GenericDaoHibernate<PartnerVanLineRef, Long> implements PartnerVanLineRefDao {

	public PartnerVanLineRefDaoHibernate() {
		super(PartnerVanLineRef.class);
	}

	public List getPartnerVanLineReferenceList(String corpId, String partnerCodeForRef) {
		return getHibernateTemplate().find("from PartnerVanLineRef where  partnerCode ='" + partnerCodeForRef + "' ");
	}

	public List getpartner(String partnerCode, String sessionCorpID) {
		return getHibernateTemplate().find("from PartnerPublic where partnerCode='" + partnerCode + "'");
	}

	public List checkJobType(String vanLineCode, String corpID, String jobType) {
		return getHibernateTemplate().find("from PartnerVanLineRef where vanLineCode='" + vanLineCode + "' and corpID='" + corpID + "' and jobType='" + jobType + "' ");
	}

	public int updateDeleteStatus(Long ids) {
		return getHibernateTemplate().bulkUpdate("delete PartnerVanLineRef where id='" + ids + "'");
	}
}
