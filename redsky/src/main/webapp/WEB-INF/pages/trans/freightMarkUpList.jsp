<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title>Freight MarkUp</title> 
<meta name="heading" content="Freight MarkUp"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-17px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
}

.table thead th, .tableHeaderTable td .headerClassLeft .sortable{
text-align:left;
}

.table thead th, .tableHeaderTable td .headerClassRight .sortable
{
text-align:left;
}
</style>

<script language="javascript" type="text/javascript">

function clear_fields(){
	var i;
			for(i=0;i<=3;i++)
			{
					document.forms['freightMarkUpList'].elements[i].value = "";
			}
}

function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to delete this row?");
	if (agree){
			location.href = "deleteFreightMarkUp.html?id="+encodeURI(targetElement);
	}else{
		return false;
	}
}

</script>



</head>





	<s:form id="freightMarkUpList" name="freightMarkUpList" action="freightMarkUpSearch" method="post" validate="true">   
	<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/freightMarkUpForm.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
	</c:set>
	
	<div id="Layer1" style="width:80%;">
	<!--<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
			<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   --><!--<div class="center-content">		
	<table class="table" style="width:98%;">
	<thead>
	<tr>
	<th><fmt:message key="freightMarkUpList.name"/></th>
	<th><fmt:message key="freightMarkUpList.tableName"/></th>
	<th><fmt:message key="freightMarkUpList.fieldName"/></th>
	<th><fmt:message key="freightMarkUpList.filterValues"/></th>
	
	
	</tr></thead>
	
	<tbody>
		<tr>
		    <td width="20" align="left"><s:textfield name="dataSecurityFilter.name" required="true" cssClass="input-text" size="40"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.tableName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.fieldName" required="true" cssClass="input-text" size="20"/></td>
			<td width="20" align="left"><s:textfield name="dataSecurityFilter.filterValues" required="true" cssClass="input-text" size="20"/></td>
			</tr>
			 <tr>
			 <td colspan="3"></td>
			 <td style="border-left: hidden;">
       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" key="button.search"/>  
       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/> 
       
   			</td>
		</tr>
		</tbody>
	</table>
	</div>
--><!--<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${searchresults}" escapeXml="false" /> 
	-->
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Freight MarkUp List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="freightMarkUpList" class="table" requestURI="" id="freightMarkUpListId" export="true" defaultsort="1" pagesize="10" style="width:100%" >   
	<display:column property="contract" style="text-align:left; width:40%;" href="freightMarkUpForm.html" paramId="id" paramProperty="id" sortable="true" title="Contract"/>
	<display:column property="mode" sortable="true" title="Mode"/>
	<display:column property="lowCost" headerClass="containeralign" style="text-align:right;width:10%;"  sortable="true" title="Low Cost"/>
   	<display:column property="highCost" headerClass="containeralign" style="text-align:right;width:10%;" sortable="true" title="High Cost"/>
   	<display:column property="currency"  sortable="true" title="Currency" style="text-align:right; width:8%;"/>
   	<display:column property="profitFlat" headerClass="containeralign" style="text-align:right;width:10%;" sortable="true" title="Profit Flat"/>
    <display:column property="profitPercent" headerClass="containeralign" style="text-align:right;width:9%;" sortable="true" title="Profit %"/>
   	<display:column title="Remove" style="width:7%;">
		<a>
			<img align="middle" title="" onclick="confirmSubmit('${freightMarkUpListId.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
		</a>
	</display:column>
	<display:setProperty name="export.excel.filename" value="Freight MarkUp List.xls"/>   
<display:setProperty name="export.csv.filename" value="Freight MarkUp List.csv"/>   
<display:setProperty name="export.pdf.filename" value="Freight MarkUp List.pdf"/> 
   	</display:table>
	
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 


<script type="text/javascript"> 
try{
<c:if test="${hitFlag == 1}" >
		<c:redirect url="/freightMarkUpList.html"  />
</c:if>
}
catch(e){}
</script> 