package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.EquipMaterialsCostDao;
import com.trilasoft.app.dao.ItemsJEquipDao;
import com.trilasoft.app.model.EquipMaterialsCost;
import com.trilasoft.app.model.ItemsJEquip;
import com.trilasoft.app.service.EquipMaterialsCostManager;
import com.trilasoft.app.service.ItemsJEquipManager;

public class EquipMaterialsCostManagerImpl extends GenericManagerImpl<EquipMaterialsCost, Long> implements EquipMaterialsCostManager{
	EquipMaterialsCostDao equipMaterialsCostDao;   
	  
	    public EquipMaterialsCostManagerImpl(EquipMaterialsCostDao equipMaterialsCostDao) {   
	        super(equipMaterialsCostDao);   
	        this.equipMaterialsCostDao= equipMaterialsCostDao;   
	    } 
	    public EquipMaterialsCost getCostById(long id){
	    	return equipMaterialsCostDao.getCostById( id);
	    }
	    public EquipMaterialsCost getByMaterailId(long id){
	    	return equipMaterialsCostDao.getByMaterailId( id);
	    }

}
