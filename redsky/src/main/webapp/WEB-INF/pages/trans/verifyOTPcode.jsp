 <%@ include file="/common/taglibs.jsp"%>
 
<head>

    <title><fmt:message key="userIDHelp.title"/></title>
    <meta name="heading" content="<fmt:message key='userIDHelp.heading'/>"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" /> 
    <script language="javascript" type="text/javascript"> 
    </script>

<style>
div#branding{float: left;width: 100%;margin: 0;text-align: left;height:0px;}        
div#footer div#divider div {border-top:5px solid #EBF5FC;margin:1px 0 0;}
#clearfooter {height:0;}

body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}

#logincontainer{ width:925px; height:375px; background-image:url(images/loginbg-opt2.png); background-repeat:no-repeat; margin-top:-7px;}
#errormessage {width:300px; list-style:none; padding-right:20px; float:right; position:absolute; top:95px;!top:100px; left:520px;!left:510px;}
		
.cssbutton{width: 55px;height: 25px;padding:0px;font-family:'arial';font-weight:normal;color:#000;font-size:12px;background:url(/redsky/images/btn_bg.gif) repeat-x;
background-color:#e7e7e7;border:1px solid #cccccc;}
.cssbutton:hover{color: black; background:url(/redsky/images/btn_hoverbg.gif) repeat-x; background-color: #c1c2bf; }

.textbox_username {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/user.png') no-repeat;background-position: 1 1;padding-left: 19px;}
.textbox_password {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/icon_password.png') no-repeat;background-position: 1 1;padding-left: 19px;}

div.error, span.error, li.error, div.message {-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;
background:#B3DAF4 none repeat scroll 0 0;border:1px solid #000000;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;margin:10px auto;
margin-top:80px;padding:3px;text-align:center;vertical-align:bottom;width:500px;z-index:1000;}

.supportalert{ background:#FFDFDF none repeat scroll 0%;border:1px solid #DF5353;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;
font-size:1em;padding:2px;text-align:center;vertical-align:bottom;margin-top: 5px;margin-bottom: 0px;}

#indexlogo {width:800px;float:left;	text-align:right;margin-right:0px;margin-top:10px;padding-top:0px;padding-right:40px;padding-bottom:0px;padding-left:0px;}

#leftcolumn {float:left;margin-top:107px;top:150px;width:380px;padding-right: 40px;}

#rightcolumn { width: 408px;

float: left;

margin-top: 59px;

border: 1px dashed #61616166;

border-radius: 6px;

padding: 3px 0px 8px 11px;}

.login-heading {color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:13px;font-weight:bold;margin:0 3px 0 10px;padding:5px 0 4px;
text-align:left;}

.signup-heading {color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;padding:5px 0 3px;text-align:right;}

.left-head {color:#71B9C3;font-family:Arial,Helvetica,sans-serif;font-size:28px;font-weight:bold;line-height:35px;}
  </style>
  <noscript>
  <style type="text/css">
  #overlay{ display:none !important;}
 .opaque{ display:none !important;}
 </style>
 </noscript>
    
<script> 

function validate(){ 
	 var chkvalflagReq="0";
	 var sysPaswdPolicy='${systemDefault.passwordPolicy}';
	 var verificationCode=	document.forms['userIDHelpForm'].elements['verificationCode'].value; 
	 var newPassword=	document.forms['userIDHelpForm'].elements['newPassword'].value;
	 var rePassword=	document.forms['userIDHelpForm'].elements['rePassword'].value;
	if(verificationCode=='')
		{
		alert("Please enter the Verification Pin.");
		return false;
		}
	
	if(newPassword.trim()=='')
		{
		alert("Please enter the New Password.");
		document.forms['userIDHelpForm'].elements['newPassword'].value=='';
		return false;
		}
	if(rePassword.trim()=='')
		{
		
		alert("Please enter the Confirm Password.");
		document.forms['userIDHelpForm'].elements['rePassword'].value=='';
		return false;
		}
	if(newPassword!=rePassword)
		{
		alert("Please enter the same password in the 'Confirm Password' field as in the 'New Password' field.");
		return false;
		}
	if((sysPaswdPolicy!=null && sysPaswdPolicy=='true')){
			chkvalflagReq="1";
		}
	var regx=/^(?=.*[A-Z])(?=.*[+/*-<>/?!@#$()]).{6,}$/;
  	var msgval="The entered password does not meet the requirement of the password policy. Secure password require at least:6 characters, 1 capital letter (A-Z), 1 symbol  {+/*-<>/?!@#$()} "
  		if((chkvalflagReq=='1') && newPassword!=null && newPassword!='' && !(newPassword.match(regx)))
  		{
  			alert(msgval);
  			document.forms['userIDHelpForm'].elements['newPassword'].focus();
  			return false;
  		}
}

function check() {
    document.forms['userResendForm'].elements['OTPusername'].value = document.forms['userIDHelpForm'].elements['OTPusername'].value;
    location.href= "findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true";
    document.forms['userResendForm'].submit();
} 

</script>

</head>

<body id="userIDHelp"/>
<div class="separator"></div>
<div id="wrapper"> 
<div id="logincontainer"> 
<div id="leftcolumn" >
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td align="right" class="left-head" colspan="2">&nbsp;<img src="images/redsky-text1.png" style="cursor: default; width: 200px;"/></td>
    </tr> 
    </table></td>
    </tr>
  </table>
</div> 
	<s:form name="userIDHelpForm" action="changePassword.html" method="post" validate="true" >
    <input type="hidden" name="encryptPass" value="true" /> 
	<div id="rightcolumn" >
	<table border="0" width="100%" cellpadding="3" cellspacing="1"  style="margin-bottom:0px">	
		<tbody>
			<tr>
              <td  class="login-heading" style="color:#272222cc;line-height:21px;  font-weight: 500;" colspan="4">
                 <br>We've sent verification pin to your registered email. Please enter it below to complete verification and reset password. 
                 <span style="height: 0px;margin-bottom: 4px;  background:#B2B2B2;  width:406px;    display: block;  margin-top: 9px;"></span>
              </td>
              <input type="hidden" class="textbox" id="bypass_sessionfilter" name="bypass_sessionfilter"   value="YES"/> 
            </tr>
			<tr>
		      <td  style="text-align:left;border:none !important;font-size: 12px;width: 65px;color:#616161; padding-top:6px;" class="listwhitetext" >Username</td>	
			  <c:if test="${OTPusername != ''}">
			  <td style="text-align:left;border:none !important;padding-top:6px;" colspan="2" ><s:textfield   cssStyle="width:185px; height:15px;  padding-left:5px; border:1px solid #B2B2B2;"  id="OTPusername" name="OTPusername" cssClass="input-text" readonly="true" /></td>
			  </c:if>
			  <c:if test="${OTPusername == ''}">
			  <td style="text-align:left;border:none !important;padding-top:6px;" colspan="2" ><s:textfield   cssStyle="width:185px; height:15px;  padding-left:5px; border:1px solid #B2B2B2;"  id="OTPusername" name="OTPusername" cssClass="input-text"  /></td>
			  </c:if>
		    </tr>
			<tr>
			  <td  style="text-align:left;border:none !important; width:117px; font-size:12px; padding-top:6px;color:#616161;" class="listwhitetext" >Verification Pin</td>					
			  <td style="text-align:left;border:none !important; padding-top:6px;" colspan="2" ><s:textfield cssStyle="width:185px; height:15px;  padding-left:5px; border:1px solid #B2B2B2;" id="verificationCode" name="verificationCode"   /></td>
					
			</tr>
		    <tr>
		      <td  style="text-align:left;border:none !important; font-size:12px; padding-top:6px; color:#616161;" class="listwhitetext" > New Password</td>					
			  <td style="text-align:left;border:none !important; padding-top:6px;" colspan="2" ><s:password showPassword="true" required="true" cssStyle="width:185px; height:15px;  padding-left:5px; border:1px solid #B2B2B2;" id="newPassword" name="newPassword"   /></td>
			</tr>
			<tr>
		      <td  style="text-align:left;border:none !important; font-size:12px; padding-top:6px;width: 98px;color:#616161;" class="listwhitetext" >Confirm Password</td>					
		      <td style="text-align:left;border:none !important; padding-top:6px;" colspan="2" ><s:password showPassword="true" required="true" cssStyle="width:185px; height:15px;  padding-left:5px; border:1px solid #B2B2B2;" id="rePassword" name="rePassword"   /></td>
		    </tr> 	
		</tbody>
	</table> 
    <table border="0" cellpadding="2"  style="margin:0px">
        <tbody>
            <tr> 
              <div class="modal-footer" style="padding:0px;">
              <td style="width: 117px;"></td>
              <td style="border:none !important;"><s:submit cssClass="btn btn-sm btn-primary" cssStyle="font-weight: normal; color: #000;margin-top: 10px;  font-size: 12px;  background: url(/redsky/images/btn_bg.gif) repeat-x;   width: 80px;  height: 25px;   background-color: #e7e7e7;    border: 1px solid #cccccc;" id="save" key="Submit" onclick="return validate();" /></td>
			  </div> 
			  <td style="padding-top: 13px;">
				 <a href="javascript:check()" class="otherlinks" style="font-size:13px;"><font color="#0066c0a">Resend Pin</font></a>
			  </td>
			</tr>
		</tbody>
    </table>
    </s:form>     
    <s:form name="userResendForm" action="findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true" method="post" validate="true" >
      <input type="hidden" class="textbox" id="OTPusername" name="OTPusername" tabindex="1"  value=""/>
      <input type="hidden" class="textbox" id="bypass_sessionfilter" name="bypass_sessionfilter"   value="YES"/>
    </s:form>         
 
   
        
  </div>
 