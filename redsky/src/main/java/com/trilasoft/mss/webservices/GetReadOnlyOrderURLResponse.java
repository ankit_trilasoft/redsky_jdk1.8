
package com.trilasoft.mss.webservices;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetReadOnlyOrderURLResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getReadOnlyOrderURLResult"
})
@XmlRootElement(name = "GetReadOnlyOrderURLResponse")
public class GetReadOnlyOrderURLResponse {

    @XmlElement(name = "GetReadOnlyOrderURLResult")
    protected String getReadOnlyOrderURLResult;

    /**
     * Gets the value of the getReadOnlyOrderURLResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetReadOnlyOrderURLResult() {
        return getReadOnlyOrderURLResult;
    }

    /**
     * Sets the value of the getReadOnlyOrderURLResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetReadOnlyOrderURLResult(String value) {
        this.getReadOnlyOrderURLResult = value;
    }

}
