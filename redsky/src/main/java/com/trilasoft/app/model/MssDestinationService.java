package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="mssdestinationservice")
public class MssDestinationService extends BaseObject{
	private Long id;
	private Long mssId;
	private String corpId;
	private String destinationItems;
	private Boolean destinationAppr=new Boolean(true);
	private Boolean destinationCod=new Boolean(false);
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("mssId", mssId).append("corpId", corpId)
				.append("destinationItems", destinationItems)
				.append("destinationAppr", destinationAppr)
				.append("destinationCod", destinationCod)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MssDestinationService))
			return false;
		MssDestinationService castOther = (MssDestinationService) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(mssId, castOther.mssId)
				.append(corpId, castOther.corpId)
				.append(destinationItems, castOther.destinationItems)
				.append(destinationAppr, castOther.destinationAppr)
				.append(destinationCod, castOther.destinationCod)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(mssId).append(corpId)
				.append(destinationItems).append(destinationAppr)
				.append(destinationCod).append(createdOn).append(createdBy)
				.append(updatedOn).append(updatedBy).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Long getMssId() {
		return mssId;
	}
	public void setMssId(Long mssId) {
		this.mssId = mssId;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public Boolean getDestinationAppr() {
		return destinationAppr;
	}
	public void setDestinationAppr(Boolean destinationAppr) {
		this.destinationAppr = destinationAppr;
	}
	@Column
	public Boolean getDestinationCod() {
		return destinationCod;
	}
	public void setDestinationCod(Boolean destinationCod) {
		this.destinationCod = destinationCod;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public String getDestinationItems() {
		return destinationItems;
	}
	public void setDestinationItems(String destinationItems) {
		this.destinationItems = destinationItems;
	}


}
