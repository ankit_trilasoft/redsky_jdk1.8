<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="agentList.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentList.heading'/>"/>   
</head>
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab"><a href=agents.html>Agents
			</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="carriers.html?id=<%=request.getParameter("id")%>&sid=${ServiceOrderID}">Carriers</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
      
<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editAgent.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
       
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>   
  
  
  
<s:set name="agents" value="agents" scope="request"/>
   
<display:table name="agents" class="table" requestURI="" id="agentList" export="true" pagesize="25">   
      <display:column property="bookingCode" sortable="true" titleKey="agent.bookingCode" href="editAgent.html"    
        paramId="id" paramProperty="id"/>   
    <display:column property="bookingCoord" sortable="true" titleKey="agent.bookingCoord"/>
    <display:column property="freightForworderCoord" sortable="true" titleKey="agent.freightForworderCoord"/>
    <display:column property="originAgentCode" sortable="true" titleKey="agent.originAgentCode"/>
    <display:column property="originAgentCoord" sortable="true" titleKey="agent.originAgentCoord"/>
    <display:setProperty name="paging.banner.item_name" value="agent"/>  
    <display:setProperty name="paging.banner.items_name" value="agent"/>
  
    <display:setProperty name="export.excel.filename" value="Agent List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Agent List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Agent List.pdf"/>   
</display:table>   
  </tr>
  </tbody>
  </table>
  </div>
  <c:out value="${buttons}" escapeXml="false" />
  
<script type="text/javascript">   
    highlightTableRows("agentList");   
</script>  
