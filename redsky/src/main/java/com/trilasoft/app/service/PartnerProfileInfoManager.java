package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.PartnerProfileInfo;

public interface PartnerProfileInfoManager extends GenericManager<PartnerProfileInfo, Long> {

	public List getProfileInfoListByPartnerCode(String sessionCorpID, String partnerCode);
}
