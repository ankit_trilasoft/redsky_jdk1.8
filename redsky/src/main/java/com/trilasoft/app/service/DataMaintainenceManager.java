package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.DataMaintainence;

public interface DataMaintainenceManager extends GenericManager<DataMaintainence, Long> {
	public List executeQuery(String query );
	public String bulkUpdate(String query);

}
