package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.service.VanLineGLTypeManager;
import com.trilasoft.app.dao.VanLineGLTypeDao;
import com.trilasoft.app.model.VanLineGLType;

public class VanLineGLTypeManagerImpl  extends GenericManagerImpl<VanLineGLType, Long> implements VanLineGLTypeManager {

	VanLineGLTypeDao vanLineGLTypeDao; 

    public VanLineGLTypeManagerImpl(VanLineGLTypeDao vanLineGLTypeDao) { 
        super(vanLineGLTypeDao); 
        this.vanLineGLTypeDao = vanLineGLTypeDao; 
    } 
    
    public List<VanLineGLType> vanLineGLTypeList(){
    	return vanLineGLTypeDao.vanLineGLTypeList();   
    }
    
    public List<VanLineGLType> searchVanLineGLType(String glType, String description, String glCode){
		return vanLineGLTypeDao.searchVanLineGLType(glType, description, glCode);
	}
    
    public Map<String, String> findByParameter(String corpId){
    	return vanLineGLTypeDao.findByParameter(corpId);
    }
    
    public List isExisted(String code, String corpId) {
    	return vanLineGLTypeDao.isExisted(code, corpId);
    }
    
    public Map<String, String> findGlCode(String corpId){
    	return vanLineGLTypeDao.findGlCode(corpId);
    }
    public List<VanLineGLType> getGLPopupList(String chargesGl){
		return vanLineGLTypeDao.getGLPopupList(chargesGl);
	}
}
