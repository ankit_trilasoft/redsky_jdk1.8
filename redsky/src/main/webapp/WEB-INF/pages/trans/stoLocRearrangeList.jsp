<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Storage Location Rearrange</title>
<meta name="heading" content="Storage Location Rearrange" />
<script language="javascript" type="text/javascript">
function clear_fields()
{  		    	
	document.forms['stoLocRearrangeList'].elements['locationId'].value = "";
	document.forms['stoLocRearrangeList'].elements['storageId'].value ="";
	
}

function check1(value){
	if( value != ''){
			document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false;		    
		}else{
			document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
		}
	return true;
}

function userStatusCheck(target){
	var targetElement = target;
	var ids = targetElement.value;
	if(targetElement.checked){
  		var userCheckStatus = document.forms['assignItemsForm'].elements['userCheck'].value;
  		if(userCheckStatus == ''){
  			document.forms['assignItemsForm'].elements['userCheck'].value = ids;
  		}else{
  			var userCheckStatus=document.forms['assignItemsForm'].elements['userCheck'].value = userCheckStatus + ',' + ids;
  			document.forms['assignItemsForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
  		}
  	}
	 	if(targetElement.checked==false){
 		var userCheckStatus = document.forms['assignItemsForm'].elements['userCheck'].value;
 		var userCheckStatus=document.forms['assignItemsForm'].elements['userCheck'].value = userCheckStatus.replace( ids , '' );
 		document.forms['assignItemsForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
 	}
}

function findStorageLibraryVolume1(storageId,position){
	var url="findStorageLibraryVolume.html?storageId="+storageId+"&decorator=simple&popup=true";
	ajax_showTooltip(url,position);		
}
</script>

<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
<s:form id="stoLocRearrangeList" name="stoLocRearrangeList" action="stoLocRearrangeSearch.html" method='post'>
<s:hidden name="userCheck"/>
<div id="Layer1" style="width:100%">
    <c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="id" value="<%=request.getParameter("id") %>"/>
	 <div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
 <div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">	
	<table class="table" width="98%">
		<thead>
			<tr>
			    <th>Location Id</th>
			    <th>Storage Id</th>
			     <th></th>
			</tr>
		</thead>
		<tbody>
			<tr>
			    <td><s:textfield name="locationId" tabindex="2" cssClass="text medium" /></td>
			    <td><s:textfield name="storageId" tabindex="2" cssClass="text medium" /></td>
				<td style="border-left: hidden; align:right"><s:submit cssClass="cssbutton" method="searchStoLocRearrange" cssStyle="width:60px; height:25px;" key="button.search"/>
			    <input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;" onclick="clear_fields();"/></td> 
			</tr>
		</tbody>
	</table>
 	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	<c:out value="${stolocrearrangebuttons}" escapeXml="false" />
	<div id="Layer5" style="width:100%;">
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Storage Location Rearrange List</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>	
	<s:set name="stoLoRearranges" value="stoLoRearranges" scope="request" />
	<display:table name="stoLoRearranges" class="table" requestURI="" id="stoLoRearrangeList" export="true" pagesize="50">
	<display:column title=" " group="2" style="width:5px"><input type="checkbox" style="margin-left:5px;" id="checkboxId" name="DD" value="${stoLoRearrangeList.storageId}~${stoLoRearrangeList.locationId}" onclick="userStatusCheck(this),check1(this)"/></display:column>
	<%--
	<c:if test="${stoLoRearrangeList.releaseDate==null}">
					<display:column style="width:5px">
					<input type="checkbox"  name="dd" onclick="check(this)" value="${stoLoRearrangeList.id}" style="width:5px">
					</display:column>
				</c:if>
				<c:if test="${stoLoRearrangeList.releaseDate!=null}">
					<display:column style="width:5px">
					<input type="checkbox" disabled="disabled" name="dd" onclick="check(this)" value="${stoLoRearrangeList.id}" style="width:5px">
					</display:column>
				</c:if>
	 --%>
		
	<display:column property="locationId" sortable="true" title="Location Id" style="width:50px"/>
	<display:column property="storageId" sortable="true" title="Storage Id" style="width:50px"/>
	<display:column property="ticket" sortable="true" title="Ticket#" style="width:50px"/>
	<display:column property="shipNumber" sortable="true" title="Ship#" style="width:50px"/>
	<display:column sortable="true" title="Volume" style="width:100px">
	<c:out value="${stoLoRearrangeList.volume}"/>&nbsp;<c:out value="${stoLoRearrangeList.volUnit}"></c:out>
	<c:if test="${stoLoRearrangeList.volume != '' && stoLoRearrangeList.volume != null}">
	<img align="top" onclick="findStorageLibraryVolume1('${stoLoRearrangeList.storageId}',this);" src="${pageContext.request.contextPath}/images/invoice.png"/>
	</c:if>
	</display:column>
	
    <display:setProperty name="export.excel.filename" value="StorageLocationRearrange List.xls"/>
	<display:setProperty name="export.csv.filename" value="StorageLocationRearrange List.csv"/>
	</display:table>
	</div>
	</div>
</s:form>
<s:form id="assignItemsForm" action="locationRearrange.html?locationId=&type=&occupied=&warehouse=" method="post">
	<s:hidden name="id" /> 
	<s:hidden name="locate"/>
	<s:hidden name="userCheck"/>
	<input type="submit" name="forwardBtn"  disabled value="Move" class="cssbutton" style="width:50px; height:25px" />
</s:form>  

<script type="text/javascript"> 
highlightTableRows("stoLocRearrangeList"); 
</script>
<script type="text/javascript">
		function check(targetElement) {
		try{
		  	document.forms['assignItemsForm'].elements['id'].value=targetElement.value;
			document.forms['assignItemsForm'].elements['locate'].value=document.forms['assignItemsForm'].elements['id'].value;
		}
		catch(e){}
		}
</script>
