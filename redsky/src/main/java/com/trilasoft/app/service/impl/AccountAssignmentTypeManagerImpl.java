package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccountAssignmentTypeDao;
import com.trilasoft.app.dao.TableCatalogDao;
import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.TableCatalog;
import com.trilasoft.app.service.AccountAssignmentTypeManager;
import com.trilasoft.app.service.TableCatalogManager;

public class AccountAssignmentTypeManagerImpl extends GenericManagerImpl<AccountAssignmentType, Long> implements AccountAssignmentTypeManager {
	
	AccountAssignmentTypeDao accountAssignmentTypeDao;
	public AccountAssignmentTypeManagerImpl(AccountAssignmentTypeDao accountAssignmentTypeDao) {
		super(accountAssignmentTypeDao); 
        this.accountAssignmentTypeDao = accountAssignmentTypeDao; 
		// TODO Auto-generated constructor stub
	}
	public List getaccountAssignmentTypeReference(String partnerCode, String corpID,Long parentId){
		return accountAssignmentTypeDao.getaccountAssignmentTypeReference(partnerCode, corpID, parentId);
	}
	public List findAssignmentByParentAgent(String corpId,String parentAgent){
		return accountAssignmentTypeDao.findAssignmentByParentAgent(corpId, parentAgent);
	}
	public List getAssignmentTypeList(String partnerCode,Long partnerId, String corpId){
		return accountAssignmentTypeDao.getAssignmentTypeList(partnerCode,partnerId, corpId);
	}
	public List checkAssignmentDuplicate(String partnerCode, String corpId,String assignment) {
		return accountAssignmentTypeDao.checkAssignmentDuplicate(partnerCode,corpId, assignment);
	}
}
