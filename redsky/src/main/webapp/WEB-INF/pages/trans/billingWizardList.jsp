<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
    <style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

span.pagelinks {
display:block;
font-size:0.8em;
margin-bottom:4px;
margin-top:-22px;
padding:2px 0;
text-align:right;
width:100%
}
</style> 
 
   
</head>

<table><tr><td width="10%"></td>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<td>
<div id="newmnav">
  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
   <li id="newmnav1" style="background:#FFF "><a href="openBillingWizard.html?sid=${serviceOrder.id}" class="current" ><span>Billing Wizard<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
  <li ><a href="accountLineList.html?sid=${serviceOrder.id}" ><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <li ><a href="pricingList.html?sid=${serviceOrder.id}" ><span>Accounting</span></a></li>
  </sec-auth:authComponent>
   <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
  <c:if test="${serviceOrder.job =='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Accounting&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  
</div><div class="spn">&nbsp;</div>

<div style="padding-bottom:3px;"></div>
 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0" style="width:850px;">
	<tbody>
	<tr> 
	<td align="left" class="listwhitebox" width="90%">
		<table class="detailTabLabel" border="0" width="98%">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td>
		  	<td align="left" colspan="2"><s:textfield name="serviceOrder.firstName"   size="21"  cssClass="input-textUpper" readonly="true"/>
		  	<td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="21" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"  size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="5" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<fmt:message key="billing.routing"/></td>
		  	<td align="right"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="5" readonly="true"/></td>
		  	</tr>
		  	<tr>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td>
		  	<td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="15" readonly="true"/></td>
		  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="15" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="21" readonly="true"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td>
		  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="3" readonly="true"/></td>
		  	<td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td>
		  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="30" readonly="true"/></td>
		  	</tr>
		  	<tr>
		  	<td align="left" height="5px"></td>
		  	</tr>
   		  </tbody>
	  </table>
	  </td></tr> 
<tr>
<td>
<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
<tbody>
<tr>
<td align="left" colspan="10"><img height="1px" width="850px" src="<c:url value='/images/vertlinedata.jpg'/>" /></td>
</tr>
											<tr>
											<td align="center" class="listwhitetext"><fmt:message key='labels.estimatedWeight'/></td>
											<c:if test="${serviceOrder.mode =='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimateGrossWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="estimateGrossWeight" value="%{miscellaneous.estimatedNetWeight}" size="6" maxlength="10" required="true" readonly="true"/></td>
											</c:if>
											<td align="center" class="listwhitetext"><fmt:message key='labels.actualWeight'/></td>
											<c:if test="${serviceOrder.mode =='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualGrossWeight}" size="6" required="true" maxlength="10" readonly="true" /></td>
											</c:if>
											<c:if test="${serviceOrder.mode !='Air'}">
											<td align="left" ><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" id="actualGrossWeight" value="%{miscellaneous.actualNetWeight}" size="6" required="true" maxlength="10" readonly="true"/></td>
											</c:if>
											<td align="left" ><s:textfield cssStyle="border:1px solid #FFFFFF;color:#003366;font-family:arial,verdana;font-size:11px;height:15px;text-decoration:none;" id="unit1" value="%{miscellaneous.unit1}" size="3"  maxlength="10" readonly="true"/></td>
										    <td align="right" class="listwhitetext"><fmt:message key='trackingStatus.beginLoad'/></td>
											<c:if test="${empty trackingStatus.loadA}">
											<c:if test="${empty trackingStatus.beginLoad}"> 
											<td><s:textfield cssClass="input-textUpper" name="loadingDate" value="${trackingStatusBeginLoadFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>
											<c:if test="${not empty trackingStatus.beginLoad}">
											<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>  
											</c:if>
											<c:if test="${not empty trackingStatus.loadA}">
											<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="loadingDate" value="${trackingStatusLoadAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>
											<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryShipper'/></td>
											<c:if test="${empty trackingStatus.deliveryA}">
											<c:if test="${empty trackingStatus.deliveryShipper}"> 
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryShipperFormattedValue}" required="true" cssStyle="width:73px" maxlength="11" readonly="true"/></td>
											</c:if>
											<c:if test="${not empty trackingStatus.deliveryShipper}">
											<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if>  
											</c:if>
											<c:if test="${not empty trackingStatus.deliveryA}">
											<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
											<td><s:textfield cssClass="input-textUpper"  name="DeliveryDate" value="${trackingStatusDeliveryAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true"/></td>
											</c:if> 
											<td align="center" class="listwhitetext"><fmt:message key='labels.accountName1'/></td>
											<td align="left"><s:textfield cssClass="input-textUpper" name="customerFile.accountName" size="30"  maxlength="250" readonly="true"/></td>
											</tr>
											<tr>
											<td align="left" colspan="10"><img height="1px" width="850px" src="<c:url value='/images/vertlinedata.jpg'/>" /></td>
											</tr>
											
</tbody>
</table>
</td>
</tr> 
</tbody>
</table></td></tr></table>
<s:form id="billingWizardList" action="invoiceDetailBilling" method="post">  
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="accountInterface" value="${accountInterface}"/>
<s:hidden name="recInvNumBilling" />
<s:hidden name="shipNumber" value="${shipNumber}"/> 
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/> 
<s:hidden name="billingWizardButton" value="<%=request.getParameter("billingWizardButton") %>"/> 
    <c:set var="billingWizardButton"  value="<%=request.getParameter("billingWizardButton") %>"/>
<s:set name="billingWizardList" value="billingWizardList" scope="request"/>  
	<table border="0" style="width:90%"> 
	<tr>
	<td width="10%"></td>
	<td>
	<div id="newmnav">
     <ul> 
       <li id="newmnav1" style="background:#FFF "><a href="openBillingWizard.html?sid=${serviceOrder.id}" class="current" ><span>Billing Wizard<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
       <li ><a onclick="fillReverseInvoice();"><span>Show Invoice Detail</span></a></li>
       <li><a onclick="openNonInvBillingWizard();"><span>Show NonInvoice Detail</span></a></li>
       <li><a onclick="accountLineList();"><span>Return To Summary</span></a></li>
    </ul>
  </div>
     <div class="spn">&nbsp;</div> 
     <div style="padding-bottom:3px;"></div>
	</td>
	</tr>
	<tr>
	<td width="10%"></td>
	<td>
<display:table name="billingWizardList" class="table" requestURI="" id="billingWizardList" style="width:100%" defaultsort="1" pagesize="100" >   
 		
		<display:column property="billToCode" sortable="true" title="Bill To Code" style="width:70px" />
		<display:column property="billToName" sortable="true" title="Bill To Name" style="width:70px" />
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice #" style="width:70px" />
		<display:column property="receivedInvoiceDate" sortable="true" title="Date"   style="width:120px" format="{0,date,dd-MMM-yyyy}"/> 
	   <display:column property="actualRevenueSum" sortable="true" title="Tot.Amt  " style="width:70px" />
	   <display:column   title="" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${billingWizardList.recInvoiceNumber} value=${billingWizardList.recInvoiceNumber} />
 		</display:column>
	     
</display:table> 
</td>
</tr>
<tr>
<td width="10%"></td>
<%-- <td><input type="button" class="cssbuttonA" style="width:120px; height:25px"   value="Show Invoice Detail" onclick="fillReverseInvoice();"/>
<input type="button" class="cssbuttonA" style="width:140px; height:25px"   value="Show NonInvoice Detail" onclick="openNonInvBillingWizard();"/>
<input type="button" class="cssbuttonA" style="width:140px; height:25px"   value="Return To Summary" onclick="accountLineList();"/>--%>
<td><input type="button" class="cssbuttonA" style="width:110px; height:25px"   value="Reverse Invoices" onclick="findReverseInvoice();"/>
</td>
</tr>
</table>
<c:if test="${billingWizardButton=='yes'}"> 
<s:set name="invoiceDetailList" value="invoiceDetailList" scope="request"/>  
	<table border="0" style="width:90%"> 
	<tr>
	<td width="10%"></td>
	<td>
<display:table name="invoiceDetailList" class="table" requestURI="" id="invoiceDetailList" style="width:100%" defaultsort="1" pagesize="100" >   
 		<display:column property="chargeCode" sortable="true" title="charge Code" style="width:70px" />
		<display:column property="recQuantity" sortable="true" title="Quantity" style="width:70px" />
		<display:column property="recRate" sortable="true" title="Rate" style="width:70px" />
		<display:column property="basis" sortable="true" title="Unit" style="width:70px" />
		<display:column property="checkNew" sortable="true" title="Item(per or X)" style="width:70px" />
		<display:column  sortable="true" title="Amount" style="width:70px" >
		 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${invoiceDetailList.actualRevenue}" /></div>
        </display:column> 
        <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
        <display:column  sortable="true" title="Dist Amt" style="width:70px" >
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${invoiceDetailList.distributionAmount}" /></div>
        </display:column>    
        </c:if>         
		<display:column  sortable="true" title="Revised Amt" style="width:70px" >
		   <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${invoiceDetailList.revisionRevenueAmount}" /></div>
        </display:column>    
	    <display:column  sortable="true" title="Est Amt" style="width:70px">
	    <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" 
                  groupingUsed="true" value="${invoiceDetailList.estimateRevenueAmount}" /></div>
        </display:column> 
	    <display:column property="recGl" sortable="true" title="GL Code" style="width:70px" />
	     <display:column property="description" sortable="true" title="Description" style="width:70px" />
<display:footer>
       <tr>
        <td align="right" colspan="5" style="  "><b><div align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></div></b></td>
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${actRevSumBilling}" /></div></td>
        <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}">
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${distAmtSumBilling}" /></div></td>
        </c:if>          
        <td align="right" width="70px" style=""><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${revRevAmtSumBilling}" /></div></td>
        <td align="right" width="70px" style=" "><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${estRevAmtSumBilling}" /></div></td>
        <td colspan="2"></td>  
        </tr>
 </display:footer>
</display:table> 
</td>
</tr> 
</table>
<table border="0" style="width:90%"> 
	<tr>
	<td width="10%"></td>
	<td> 
		 <table width="100%" style="background-color:#D7E4EC; margin-bottom:0;" border="0">
			 <tr style="background-color:#D7E4EC;">
				 <td align="right" height="20" width="100px" class="listwhitetextAcct"><fmt:message key="accountLine.sendEstToClient" /></td>
						 <c:if test="${not empty accountLine.sendEstToClient}">
							 <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendEstToClient" /></s:text>
							 <td width="120px" class="listwhitetextAcct"><s:textfield cssClass="input-text" id="sendEstToClient" name="accountLine.sendEstToClient" value="%{accountLineFormattedValue}" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
							 <!--<img id="calender11" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].sendEstToClient,'calender11',document.forms['accountLineForms'].dateFormat.value); return false;" />
								 --></td>
							 </c:if>
							 <c:if test="${empty accountLine.sendEstToClient}">
							 <td width="120px"><s:textfield cssClass="input-text" id="sendEstToClient" name="accountLine.sendEstToClient" required="true" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
							   <!--<img id="calender11" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
								 onclick="cal.select(document.forms['accountLineForms'].sendEstToClient,'calender11',document.forms['accountLineForms'].dateFormat.value); return false;" />
								 --></td>
								 </c:if> 
							 <td align="right" width="70px" class="listwhitetextAcct"><fmt:message key="accountLine.sendActualToClient" /></td>
							 <c:if test="${not empty accountLine.sendActualToClient}">
							 <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.sendActualToClient" /></s:text>
							 <td width="120px"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
							   <!--<img id="calender12" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
								 onclick="cal.select(document.forms['accountLineForms'].sendActualToClient,'calender12',document.forms['accountLineForms'].dateFormat.value); return false;" />
						    --></td>
							 </c:if>
							 <c:if test="${empty accountLine.sendActualToClient}">
							 <td width="120px"><s:textfield cssClass="input-text" id="sendActualToClient" name="accountLine.sendActualToClient" required="true" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
							  <!--<img id="calender12" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
								 onclick="cal.select(document.forms['accountLineForms'].sendActualToClient,'calender12',document.forms['accountLineForms'].dateFormat.value); return false;" />
							 --></td>
							 </c:if> 
							 <td align="right" width="91px"  class="listwhitetextAcct"><fmt:message key="accountLine.doNotSendtoClient" /></td>
							 <c:if test="${not empty accountLine.doNotSendtoClient}">
							 <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.doNotSendtoClient" /></s:text>
							 <td width="120px"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" value="%{accountLineFormattedValue}" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
								 <!--<img id="calender13" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
									 onclick="cal.select(document.forms['accountLineForms'].doNotSendtoClient,'calender13',document.forms['accountLineForms'].dateFormat.value); return false;" />
							 --></td>
							 </c:if>
							 <c:if test="${empty accountLine.doNotSendtoClient}">
							 <td width="120px"><s:textfield cssClass="input-text" id="doNotSendtoClient" name="accountLine.doNotSendtoClient" required="true" size="8" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
								 <!--<img id="calender13" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
									 onclick="cal.select(document.forms['accountLineForms'].doNotSendtoClient,'calender13',document.forms['accountLineForms'].dateFormat.value); return false;" />
						     --></td>
																</c:if> 
															<td align="right" class="listwhitetextAcct" ><fmt:message key="accountLine.accrueRevenue" /></td> 
																	<c:if test="${not empty accountLine.accrueRevenue}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenue" /></s:text>
																	<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="accrueRevenue" name="accountLine.accrueRevenue" value="%{accountLineFormattedValue}" size="8"
																		maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/>
																		 <!--<img id="calender2222" lign="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 
																		 onclick="cal.select(document.forms['accountLineForms'].accrueRevenue,'calender2222',document.forms['accountLineForms'].dateFormat.value); return false;" />
																		 --></td>
																  
																	</td>
																 </c:if>
																 <c:if test="${empty accountLine.accrueRevenue}">
																	<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="accrueRevenue" name="accountLine.accrueRevenue" required="true" size="8" maxlength="12"
																		 onkeydown="return onlyDel(event,this)"readonly="true"/> 
																		 
																		 
																		<!--<img id="calender2222"
																		align="top"
																		src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].accrueRevenue,'calender2222',document.forms['accountLineForms'].dateFormat.value); return false;" />
																		--></td>
																	 
																	</td>
																</c:if> 
                                                             <c:if test="${accountInterface=='Y'}"> 		
															<td align="right"  class="listwhitetextAcct"><fmt:message key="accountLine.accrueRevenueReverse" /></td>
	                                                         <c:if test="${not empty accountLine.accrueRevenueReverse}"> 
		                                                          <s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.accrueRevenueReverse"/></s:text>
		                                                          <td><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" value="%{accountLineFormattedValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/></td>
	                                                         </c:if>
	                                                         <c:if test="${empty accountLine.accrueRevenueReverse}">
	                                                              <td><s:textfield id="accrueRevenueReverse" name="accountLine.accrueRevenueReverse" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/></td>
	                                                          </c:if></c:if>
															
														</tr>															
                                                         <tr>   <td height="20" align="right" class="listwhitetextAcct"><fmt:message key="charges.glCode"/></td>  
                                                                <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" key="accountLine.recGl" size="8"  maxlength="7" readonly="true" /></td> 
																<td align="right" class="listwhitetextAcct" ><fmt:message key="accountLine.recPostDate" /></td> 
																	<c:if test="${not empty accountLine.recPostDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.recPostDate" /></s:text>
																	<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" value="%{accountLineFormattedValue}" size="7"
																		maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/>
																		 
																		<!--<c:if test="${empty accountLine.recAccDate}">
																		   <img id="calender1" align="top" name="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
																		   onclick="cal.select(document.forms['accountLineForms'].recPostDate,'calender1',document.forms['accountLineForms'].dateFormat.value);  document.forms['accountLineForms'].recPostDate.focus(); return false;" />
																		</c:if>
																		<c:if test="${not empty accountLine.recAccDate}">
																		   <img id="calender1" align="top" name="calender1" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
																		   onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																		</c:if>
																		 
																		
																		--></td>
																</c:if>
																<c:if test="${empty accountLine.recPostDate}">
																	<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="recPostDate" name="accountLine.recPostDate" required="true" size="7" maxlength="12"
																		 onkeydown="return onlyDel(event,this)"readonly="true"/>
																	 
																	<!--<c:if test="${empty accountLine.recAccDate}">
																		 <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
																		 onclick="cal.select(document.forms['accountLineForms'].recPostDate,'calender',document.forms['accountLineForms'].dateFormat.value); return false;" />
																	</c:if>	
																	<c:if test="${not empty accountLine.recAccDate}">
																		 <img id="calender" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20
																		 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	</c:if>
																	 
																		
																	--></td>
																</c:if> 
																<td height="20" align="right" class="listwhitetextAcct"><fmt:message key="accountLine.recAccDate"/></td>  
                                                                <c:if test="${not empty accountLine.recAccDate}">
																<s:text id="recAccDate" name="${FormDateValue}"><s:param name="value" value="accountLine.recAccDate" /></s:text>
																<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="recAccDate" name="accountLine.recAccDate" value="%{recAccDate}" cssStyle="width:68px" maxlength="11" readonly="true" />
																 
																		<!--<img id="calender222"
																		align="top"
																		src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].recAccDate,'calender222',document.forms['accountLineForms'].dateFormat.value); return false;" /></td>
																	 
																
																--></td>
																
																
																</c:if>
																<c:if test="${empty accountLine.recAccDate}">
																<td class="listwhitetextAcct"><s:textfield cssClass="input-text" id="recAccDate" name="accountLine.recAccDate" cssStyle="width:68px" maxlength="11" readonly="true" />
																 
																		<!--<img id="calender222"
																		align="top"
																		src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['accountLineForms'].recAccDate,'calender222',document.forms['accountLineForms'].dateFormat.value); return false;" />
																		
																		
																	 --></td>
																</c:if> 
																<td align="right" class="listwhitetextAcct"><fmt:message key="accountLine.recXfer"/></td>  
                                                                <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" key="accountLine.recXfer" size="15"  maxlength="20" readonly="true"  /></td> 
										                        <c:if test="${accountInterface=='Y'}">
                                                                <td height="20" align="right" class="listwhitetextAcct"><fmt:message key="accountLine.recXferUser"/></td>  
                                                                <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" key="accountLine.recXferUser" size="8"  maxlength="7" readonly="true" /></td> 	 
                                                                </c:if>
                                                           </tr>  
													</table>  </td></tr></table>
													<!--<table border="0" style="width:90%"> 
	                                                 <tr>
	                                                  <td width="10%"></td>
	                                                   <td><input type="button" class="cssbuttonA" style="width:60px; height:25px"   value="Billing" onclick=""/></td>
	                                                 </tr></table>
--></c:if> 
</s:form> 

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript"> 
	function fillReverseInvoice(){ 
       var invoiceNumber = valButton(document.forms['billingWizardList'].elements['radiobilling']);   
	    if (invoiceNumber == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       {
	       //alert(invoiceNumber);
	        invoiceNumber = invoiceNumber.replace('/','');
	        //alert(invoiceNumber);
	        document.forms['billingWizardList'].elements['recInvNumBilling'].value=invoiceNumber;
	        //alert("invoiceNumber is set");
	        var agree = confirm("Press OK to proceed, or press Cancel.");
            if(agree)
             {
             //alert("ok in agree");
               document.forms['billingWizardList'].action ='invoiceDetailBilling.html?billingWizardButton=yes&decorator=popup&popup=true';
               document.forms['billingWizardList'].submit(); 
             }
             else {
             return false; 
             } 
           }  
      } 
   function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['billingWizardList'].elements['radiobilling'].value; 
    } 
  }
</script>
  
<script type="text/javascript">
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
</script>
  
<script type="text/javascript">
  
     function openNonInvBillingWizard()
     { 
       var sid = document.forms['billingWizardList'].elements['sid'].value;
       var url="findNonInvoiceList.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
       http2.open("GET", url, true);
       http2.onreadystatechange = handleHttpResponse4;
       http2.send(null);
     }
     
   function handleHttpResponse4()
        {
            if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results=results.replace(']',''); 
                if(results=='0')
                {
                 alert("There is no non invoiced accounts ");
                }
                else if(results!='0')
                { 
                   var sid = document.forms['billingWizardList'].elements['sid'].value;
                    window.location ="nonInvListWizard.html?sid="+sid;
                }
 }
 }
 
  function accountLineList()
   {
      var sid = document.forms['billingWizardList'].elements['sid'].value;
      var newAccounting=false;
      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
      newAccounting=true;
      </sec-auth:authComponent>
      if(newAccounting==true){
      window.location ="pricingList.html?sid="+sid; 
      }else{
      window.location ="accountLineList.html?sid="+sid;
      }
   }
</script>
  
<script type="text/javascript">
 function findReverseInvoice() 
 {
  //var shipNumberForTickets=document.forms['serviceForm1'].elements['serviceOrder.shipNumber'].value;
  var sid=document.forms['billingWizardList'].elements['sid'].value; 
  openWindow('reverseInvoiceList.html?buttonType=invoice&sid='+sid+'&decorator=popup&popup=true',900,300);
  }
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript">  
try{
document.forms['billingWizardList'].elements['radiobilling'].checked=true;
}
catch(e){}
</script>  