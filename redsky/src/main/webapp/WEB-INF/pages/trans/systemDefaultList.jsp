<%@ include file="/common/taglibs.jsp"%>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
</script>
<head>   
    <title><fmt:message key="systemDefaultList.title"/></title>   
    <meta name="heading" content="<fmt:message key='systemDefaultList.heading'/>"/>   
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
<c:set var="buttons">  

     <input type="button" class="cssbutton" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editSystemDefault.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	
</c:set>   

      
<s:form id="companyListForm" method="post"> 
<div id="Layer1">
<table cellspacing="0" cellpadding="0" border="0" width="700px">
	<tbody>
	<tr><td height="10px"></td></tr>
	<tr><td>
   <display:table name="systemDefaults" class="table" requestURI="" id="systemDefaultList" defaultsort="2" export="true" pagesize="10">   
   
    <display:column property="id" sortable="true" titleKey="systemDefault.id" url="/editSystemDefault.html" paramId="id" paramProperty="id"/>
    <display:column property="corpID" sortable="true" titleKey="systemDefault.corpID"/>
    <display:column property="company" sortable="true" titleKey="systemDefault.companyName"/>
    <display:column property="zip" sortable="true" titleKey="systemDefault.zip"/>
  
    <display:setProperty name="export.excel.filename" value="Company List.xls"/>   
    <display:setProperty name="export.csv.filename" value="company List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="company List.pdf"/>   
</display:table>   
  </td></tr></tbody></table>
  </div>
<!-- <c:out value="${buttons}" escapeXml="false" />    -->
</s:form>  

<script type="text/javascript">   
    highlightTableRows("systemDefaultList");   
</script>  
