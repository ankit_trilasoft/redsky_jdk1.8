package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;

@Entity
@Table(name="genericsurveyquestion")
public class GenericSurveyQuestion extends BaseObject{

	private Long id;
	private String corpId;
	private Integer sequenceNumber;
	private String question;
	private String answerType;
	private String jobType;
	private String routing;
	private boolean bookingAgent = false;
	private boolean originAgent = false;
	private boolean destinationAgent = false;
	private String language;
	private boolean status= false;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String category;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpId", corpId)
				.append("sequenceNumber", sequenceNumber)
				.append("question", question)
				.append("answerType", answerType)
				.append("jobType", jobType)
				.append("routing", routing)
				.append("bookingAgent", bookingAgent)
				.append("originAgent", originAgent)
				.append("destinationAgent", destinationAgent)
				.append("language", language)
				.append("status", status)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("category", category)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof GenericSurveyQuestion))
			return false;
		GenericSurveyQuestion castOther = (GenericSurveyQuestion) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(question, castOther.question)
				.append(answerType, castOther.answerType)
				.append(jobType, castOther.jobType)
				.append(routing, castOther.routing)
				.append(bookingAgent, castOther.bookingAgent)
				.append(originAgent, castOther.originAgent)
				.append(destinationAgent, castOther.destinationAgent)
				.append(language, castOther.language)
				.append(status, castOther.status)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(category, castOther.category)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
				.append(id).append(corpId)
				.append(sequenceNumber).append(question)
				.append(answerType).append(jobType)
				.append(routing).append(bookingAgent)
				.append(originAgent).append(destinationAgent)
				.append(language).append(status)
				.append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn)
				.append(category)
				.toHashCode();
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCorpId() {
		return corpId;
	}

	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}

	public Integer getSequenceNumber() {
		return sequenceNumber;
	}

	public void setSequenceNumber(Integer sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswerType() {
		return answerType;
	}

	public void setAnswerType(String answerType) {
		this.answerType = answerType;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getRouting() {
		return routing;
	}

	public void setRouting(String routing) {
		this.routing = routing;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public boolean isBookingAgent() {
		return bookingAgent;
	}

	public void setBookingAgent(boolean bookingAgent) {
		this.bookingAgent = bookingAgent;
	}

	public boolean isOriginAgent() {
		return originAgent;
	}

	public void setOriginAgent(boolean originAgent) {
		this.originAgent = originAgent;
	}

	public boolean isDestinationAgent() {
		return destinationAgent;
	}

	public void setDestinationAgent(boolean destinationAgent) {
		this.destinationAgent = destinationAgent;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	

}
