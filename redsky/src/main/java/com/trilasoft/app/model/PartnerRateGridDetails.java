package com.trilasoft.app.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="partnerrategriddetails")
@FilterDef(name = "partnerCorpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "partnerCorpID", condition = "corpID in (:forCorpID)")

public class PartnerRateGridDetails extends BaseObject {
	
	private Long id;
	private Long partnerRateGridID;
	private String corpID;
	private String grid;
	private String gridSection;
	private String parameter;
	private String label;
	private String basis;
	private BigDecimal weight1;
	private BigDecimal weight2;
	private BigDecimal value;
	private Long  sortRateGrid;
	private Long  sortGrid;
	private String editable;
	
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append(
				"partnerRateGridID", partnerRateGridID)
				.append("corpID", corpID).append("grid", grid).append(
						"gridSection", gridSection).append("parameter",
						parameter).append("label", label)
				.append("basis", basis).append("weight1", weight1).append(
						"weight2", weight2).append("value", value).append(
						"sortRateGrid", sortRateGrid).append("sortGrid",
						sortGrid).append("editable", editable).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PartnerRateGridDetails))
			return false;
		PartnerRateGridDetails castOther = (PartnerRateGridDetails) other;
		return new EqualsBuilder().append(id, castOther.id).append(
				partnerRateGridID, castOther.partnerRateGridID).append(corpID,
				castOther.corpID).append(grid, castOther.grid).append(
				gridSection, castOther.gridSection).append(parameter,
				castOther.parameter).append(label, castOther.label).append(
				basis, castOther.basis).append(weight1, castOther.weight1)
				.append(weight2, castOther.weight2).append(value,
						castOther.value).append(sortRateGrid,
						castOther.sortRateGrid).append(sortGrid,
						castOther.sortGrid)
				.append(editable, castOther.editable).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(partnerRateGridID)
				.append(corpID).append(grid).append(gridSection).append(
						parameter).append(label).append(basis).append(weight1)
				.append(weight2).append(value).append(sortRateGrid).append(
						sortGrid).append(editable).toHashCode();
	}
	@Column(length=20)
	public String getBasis() {
		return basis;
	}
	public void setBasis(String basis) {
		this.basis = basis;
	}
	
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	
	
	@Column(length=4)
	public String getGrid() {
		return grid;
	}
	public void setGrid(String grid) {
		this.grid = grid;
	}
	
	@Column(length=10)
	public String getGridSection() {
		return gridSection;
	}
	public void setGridSection(String gridSection) {
		this.gridSection = gridSection;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=50)
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Column(length=50)
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	@Column(length=20)
	public Long getPartnerRateGridID() {
		return partnerRateGridID;
	}
	public void setPartnerRateGridID(Long partnerRateGridID) {
		this.partnerRateGridID = partnerRateGridID;
	}
	
	@Column(length=12)
	public BigDecimal getWeight1() {
		return weight1;
	}
	public void setWeight1(BigDecimal weight1) {
		this.weight1 = weight1;
	}
	
	@Column(length=12)
	public BigDecimal getWeight2() {
		return weight2;
	}
	public void setWeight2(BigDecimal weight2) {
		this.weight2 = weight2;
	}
	@Column(length=3)
	public Long getSortRateGrid() {
		return sortRateGrid;
	}
	public void setSortRateGrid(Long sortRateGrid) {
		this.sortRateGrid = sortRateGrid;
	}
	@Column(length=3)
	public Long getSortGrid() {
		return sortGrid;
	}
	public void setSortGrid(Long sortGrid) {
		this.sortGrid = sortGrid;
	}
	@Column(length=20)
	public BigDecimal getValue() {
		return value;
	}
	public void setValue(BigDecimal value) {
		this.value = value;
	}
	@Column(length=1)
	public String getEditable() {
		return editable;
	}
	public void setEditable(String editable) {
		this.editable = editable;
	}
	
	

}
