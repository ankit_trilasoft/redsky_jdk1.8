package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.loader.custom.Return;

import com.trilasoft.app.dao.RefZipGeoCodeMapDao;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefZipGeoCodeMap;

public class RefZipGeoCodeMapDaoHibernate extends GenericDaoHibernate<RefZipGeoCodeMap,Long> implements RefZipGeoCodeMapDao{
	public RefZipGeoCodeMapDaoHibernate() 
    {   
        super(RefZipGeoCodeMap.class);   
    
    }
	Logger logger = Logger.getLogger(RefZipGeoCodeMapDaoHibernate.class);
	
	public List zipCodeExisted(String zipCode, String corpId) {
		try{
		return getHibernateTemplate().find("from RefZipGeoCodeMap where zipcode = '"+zipCode+"' and primaryRecord='P' and corpID in ('"+corpId+"','TSFT')");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List refZipGeoCodeMapByCorpId(String corpId){
		try{
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("from RefZipGeoCodeMap where corpID in ('" + corpId + "','XXXX','TSFT','NULL') ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List findMaximumId(){
		try{
		return getHibernateTemplate().find("select max(id) from RefZipGeoCodeMap");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public Map<String, String> findByParameter(String bucket2, String corpId){
		try{
		if(bucket2 == null || bucket2.equals("")){
			bucket2 = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		List<RefMaster> list = getHibernateTemplate().find("from RefMaster where  bucket2 like '"+bucket2+"%' order by description");
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List refZipGeoCodeMapState(String stateCode){
		try{
		return getHibernateTemplate().find("select description from RefMaster where code='"+stateCode+"' and bucket2='USA' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public List searchByRefZipGeoCodeMap(String zipCode, String city, String state,String country) {
		try{
		return getHibernateTemplate().find("from RefZipGeoCodeMap where zipcode like '%"+ zipCode.replaceAll("'", "''").replaceAll(":", "''") + "%' AND city like '%"+ city.replaceAll("'", "''").replaceAll(":", "''") +"%' AND state like '%"+state.replaceAll("'", "''").replaceAll(":", "''") +"%' AND country like '%"+ country.replaceAll("'", "''").replaceAll(":", "''") + "%' ");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	public int updatePrimaryRecord(String zipCode, String corpId){
		try{
		return getHibernateTemplate().bulkUpdate("update RefZipGeoCodeMap set primaryRecord='' where primaryRecord='P' and zipcode='"+zipCode+"' and corpID in ('"+corpId+"','TSFT')");
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return 0;
	}

	public String getStateName(String stateabbreviation, String sessionCorpID, String country) {
		try{
		List getState =new ArrayList();
		String getStateName=new String("");
		getState = getHibernateTemplate().find("select description from RefMaster where parameter='STATE' and bucket2='"+country+"' and code='"+stateabbreviation+"'");
		if(getState!=null && !(getState.isEmpty())){
			getStateName=getState.get(0).toString();
		}else{
			getStateName="";
		}
		return getStateName;
		} catch (Exception e) {
			logger.error("Error executing query"+ e,e);
		}
		return null;
	}
	
	
	
}
