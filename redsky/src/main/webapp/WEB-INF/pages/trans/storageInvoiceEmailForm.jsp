<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Storage Invoice Email</title>
<meta name="heading" content="Email Storage Invoice" />	
<style>
 span.pagelinks {display:block;font-size:0.85em;margin-bottom:3px;!margin-bottom:1px;margin-top:-21px;padding:2px 0px;text-align:right;width:100%;!width:100%;}
#overlayForAgentMarket {filter:alpha(opacity=70);-moz-opacity:0.7;-khtml-opacity: 0.7;opacity: 0.7;position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);}
</style>

    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
	<script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery-1.10.1.min.js"></script> 
   <script type="text/javascript" src="${pageContext.request.contextPath}/scripts/jquery.tablesorter.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>	
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
</head>
<script type="text/javascript">
$(document).ready(function() {
	$('#emailInvoiceList').tablesorter(); 
	
} );
</script>
<s:form cssClass="form_magn" name="storageInvoiceEmailForm" id="storageInvoiceEmailForm" action="emailInvoiceList" method="post" validate="true">
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<s:hidden name="subject" value="Your Storage Invoice"></s:hidden>
<s:hidden name="jrxmlName" value="InvoiceStorage.jrxml"></s:hidden>  <!-- InvoiceStorage.jrxml DriverReceiptNew.jrxml -->
<s:hidden name="validshipOption" value=""></s:hidden> 
<configByCorp:fieldVisibility componentId="component.field.storageInvoiceEmail.creditCard">
<s:hidden name="storageInvoiceEmailCreditCard" value="yes"></s:hidden> 
</configByCorp:fieldVisibility>

<c:set var="seprateFile" value="F"/>
<configByCorp:fieldVisibility componentId="component.field.storageInvoiceEmail.seprateInvoiceEmail">
<c:set var="seprateFile" value="T"/>
</configByCorp:fieldVisibility>

<c:set var="dateFormat" value="{0,date,dd-MMM-yy}"/>

	<div id="layer1" style="width:100%;">
		<div id="otabs">
		  	<ul>
		    	<li><a class="current"><span>Storage Invoice Email</span></a></li>
		  	</ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center" >
			<div id="liquid-round">
   				<div class="top" style="margin-top:11px;!margin-top:-4px;"><span></span></div>
   				<div class="center-content">
					<table class="" cellspacing="1" cellpadding="0" border="0" style="" width="100%">	
						<tr><td height="5px" colspan="4">&nbsp;</td></tr>	
						<tr>		  		
								<td align="right" width="130px" class="listwhitetext" style="padding-bottom:5px">Storage Invoice From</td>
						  	  	<td width="120px" style="padding-bottom:5px; padding-left:5px;">
						  	  	<c:if test="${not empty fromDate}">
						  	  		<s:text id="dateFormattedValue" name="${dateFormat}"><s:param name="value" value="fromDate" /></s:text>
							  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{dateFormattedValue}" size="9" maxlength="10"  onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/> 
							  		<img id="fromDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
							  	</c:if>
							  	<c:if test="${empty fromDate}">
							  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" size="9" maxlength="10"  onkeydown="return onlyDelNoQuotes(event,this)" readonly="true"/> 
							  		<img id="fromDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
							  	</c:if>
							  	</td>
								<td align="right" class="listwhitetext" style="padding-bottom:5px"> To</td>	
							  	<td width="137px" align="left" class="listwhitetext" style="padding-bottom:5px; padding-left:5px;">
							  	<c:if test="${not empty toDate}">
							  		<s:text id="dateFormattedValue" name="${dateFormat}"><s:param name="value" value="toDate" /></s:text>
							  		<s:textfield cssClass="input-text" id="toDate" name="toDate" value="%{dateFormattedValue}" onkeydown="return onlyDelNoQuotes(event,this)" size="9" maxlength="10"  readonly="true" /> 
							  		<img id="toDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
							  	</c:if>
							  	<c:if test="${empty toDate}">
							  		<s:textfield cssClass="input-text" id="toDate" name="toDate" size="9" maxlength="10"  onkeydown="return onlyDelNoQuotes(event,this)" readonly="true" /> 
							  		<img id="toDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 />
							  	</c:if>
							  	</td>
							  	
							  	<s:hidden name="emailPrintOption"/>
							  	<s:hidden name="shipOption"/>
							  	<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
							  	<s:hidden name="isPrintOptFlag" value="Yes"></s:hidden>	
							  	<s:hidden name="isPrintOpt" value="Yes"/>						
							    <td width="350px" class="listwhitetext">Print Option for All: <s:select title="" cssClass="list-menu" name="emailPrintOption1" list="%{printOptions}" onchange="setVal(this);" cssStyle="width:175px"  value=" "/>	</td>		
						        </configByCorp:fieldVisibility>
							  	
							    <td width="110px" class="listwhitetext" align="right" >Storage Billing Group</td>
		  		                <td style="padding-left:2px">
		  			            <s:select cssClass="list-menu" cssStyle="width:130px; height:70px; overflow-y:scroll;" id="storageBillingListId"  name="storageBillingList" list="%{storageBillingGroup}"  multiple="true" headerKey="" headerValue=""  />
		  		                </td>
						</tr>
						<tr><td height="20px" colspan="5"></td></tr>
						<tr>
							<td></td>
							<td colspan="6">
								<input type="button" id="invoiceList" name="invoiceList" class="cssbutton1" onclick="getInvoiceList();" value="Preview Invoice List" style="width:140px; height:25px" tabindex="83"/>
								<c:choose>
								<c:when test="${seprateFile=='T'}">
								<input type="button" id="emailInvoice" name="emailInvoice" class="cssbutton1" onclick="sendMailForVoer();" value="Email Invoice" style="width:120px; height:25px" tabindex="83"/>								
								</c:when>
								<c:otherwise>
								<input type="button" id="emailInvoice" name="emailInvoice" class="cssbutton1" onclick="sendMail();" value="Email Invoice" style="width:120px; height:25px" tabindex="83"/>								 								
								</c:otherwise>
								</c:choose>
							   &nbsp; <img src="${pageContext.request.contextPath}/images/xls1.gif" onclick="javascript:getInvoiceListExcel();" style="vertical-align:text-bottom;"/> <font color="#003366" >Preview Invoice List</font>  </td>
							</td>
						</tr>
						<tr><td height="20" colspan="4"></td></tr>
					</table> 
				</div>
				<div class="bottom-header"><span></span></div>
			</div>
		</div>
	</div>
	
	
	<div style="overflow:auto;height:300px;width:80%;">
	 	<c:if test="${sessionScope.errMSG != null || sessionScope.errMSG != ''}">
	 		<h3><c:out value="${sessionScope.errMSG}"></c:out></h3>
	 	</c:if>
		<c:choose>
			<c:when test="${empty pdfPathMap}">
				<c:if test="${not empty emailInvoiceList}">
				<s:set name="emailInvoiceList" value="emailInvoiceList" scope="request"/> 
				<display:table name="emailInvoiceList" class="table" id="emailInvoiceList" defaultsort="4" style="width:100%;" >
				<c:choose>
                  <c:when test="${newAccountline=='Y'}">
                  <display:column  title="Invoice Number" style="width:20px;" sortable="true" sortProperty="recInvoiceNumber">
					<div >
					<a href="pricingList.html?sid=${emailInvoiceList.id}" target="_blank"><c:out value="${emailInvoiceList.recInvoiceNumber}" /></a> 
					</div>
					 </display:column>
                  </c:when>
                  <c:otherwise>
					<display:column  title="Invoice Number" style="width:20px;" sortable="true" sortProperty="recInvoiceNumber">
					<div >
					<a href="accountLineList.html?sid=${emailInvoiceList.id}" target="_blank"><c:out value="${emailInvoiceList.recInvoiceNumber}" /></a> 
					</div>
					 </display:column>
				  </c:otherwise>
				  </c:choose>	 
					<display:column property="receivedInvoiceDate"  title="Received Invoice Date" format="{0,date,dd-MMM-yyyy}" style="width:20px;" maxLength="35" sortable="true"/>
					<display:column  title="Ship Number" style="width:25px;" sortable="true" sortProperty="shipNumber">
					<div >
					<a href="editBilling.html?id=${emailInvoiceList.id}" target="_blank"><c:out value="${emailInvoiceList.shipNumber}" /></a>
					</div>
					</display:column>
					<display:column title="Email Flag" style="width:15px;" >
						<c:choose>
							<c:when test="${emailInvoiceList.emailInvoice == true}">
								<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
							</c:when>
							<c:otherwise>
								<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
							</c:otherwise>
						</c:choose>
					</display:column>
					<display:column title="Email Id" style="width:100px;" sortable="true" >
						<c:choose>
							<c:when test="${empty emailInvoiceList.storageEmail}">
								<c:if test="${emailInvoiceList.emailInvoice == true}">
									<font size="2" color="red"><c:out value="Required"></c:out></font>
								</c:if>
							</c:when>
							<c:otherwise>
								<c:out value="${emailInvoiceList.storageEmail}"></c:out>
							</c:otherwise>
						</c:choose>
					</display:column>
					<configByCorp:fieldVisibility componentId="component.field.storageInvoiceEmail.creditCard">
					<display:column title="Credit Card" style="width:15px;" >
						<c:choose>
							<c:when test="${emailInvoiceList.automatedStorageBillingProcessing == true}">
								<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
							</c:when>
							<c:otherwise>
								<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
							</c:otherwise>
						</c:choose>
					</display:column>
					</configByCorp:fieldVisibility>
					<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
					<display:column title="<font size='2' color='red'>*</font>Print Options" style="width:15px;" sortable="true">									
					<c:choose>
							<c:when test="${emailInvoiceList.emailInvoice == true}">		
							<select name="emailOption${emailInvoiceList.shipNumber}"class="list-menu" onchange="resetPrintOpt('${emailInvoiceList.shipNumber}','${emailInvoiceList.recInvoiceNumber}',this)">	
							<option value="" >	</option>		
						<c:forEach var="chrms" items="${emailPrintOptionMap}" varStatus="loopStatus">
                                  <c:choose>
	                                  <c:when test="${chrms.key == emailInvoiceList.emailPrintOption}">
	                                  <c:set var="selectedInd" value=" selected"></c:set>
	                                  </c:when>
	                                 <c:otherwise>
	                                  <c:set var="selectedInd" value=""></c:set>
	                                 </c:otherwise>
                                  </c:choose>
                                 <option value="<c:out value='${chrms.key}' />" <c:out value='${selectedInd}' />>
                                <c:out value="${chrms.value}"></c:out>
                                </option>
						</c:forEach>
					</select>
						</c:when>
						
							<c:otherwise>								
							</c:otherwise>							
						</c:choose>
						</display:column>
						<display:column  title="Storage Email Type" style="width:30px;" sortable="true" >
							<c:out value="${emailInvoiceList.storageEmailType}"></c:out>
						</display:column>
						</configByCorp:fieldVisibility>
				</display:table>
				</c:if>
			</c:when>
			<c:otherwise>
				<table class="table" id="dataTable" style="width:50%;">
				 	<thead>
					 	<tr>
					 		<th width="5%">#</th>
					 		<th style="width: 25%">Email Id</th>
					 		<th style="width: 13%">Mail Send Status</th>
					 	</tr>
					</thead>
					<tbody>
						<c:set var="countLine" value="${0}" />
		 				<c:forEach var="invoiceMailVar" items="${pdfPathMap}" >
		 					<c:set var="countLine" value="${countLine+1}" />
		 					<c:set var="invoiceParts" value="${fn:split(invoiceMailVar.value, '-')}" />
		 					<tr>
			 					<td  class="" align="right" ><c:out value="${countLine}"></c:out></td>
			 					<td  class="" align="right" ><c:out value="${invoiceMailVar.key}"></c:out></td>
			 					<td  class="listwhitetext" align="right" >
									<c:choose>
										<c:when test="${invoiceParts[1] == 'True'}">
											<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
										</c:when>
										<c:otherwise>
											<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
										</c:otherwise>
									</c:choose>
			 					</td>
		 					</tr>
		 				</c:forEach>
		 			</tbody>
				</table>
			</c:otherwise>
		</c:choose>
	</div>
<div id="overlayForAgentMarket" style="display:none">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/ajax-loader-hr.gif'/>" />       
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>	
</s:form>
 
 <script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script> 
 <script language="javascript" type="text/javascript">
 
 function getInvoiceList(){
	 if(fieldValidate()){
		 document.getElementById("overlayForAgentMarket").style.display = "block";
		 var url ="getStorageInvoiceList.html?ajax=1";
	 	 document.forms['storageInvoiceEmailForm'].action =url;
	 	 document.forms['storageInvoiceEmailForm'].submit();
	 }
 }
 
 function getInvoiceListExcel(){
	 if(fieldValidate()){ 
		 //var storageBillingList=document.forms['storageInvoiceEmailForm'].elements['storageBillingList'].value;
		 var fld = document.getElementById('storageBillingListId');
         var storageBillingList = [];
         for (var i = 0; i < fld.options.length; i++) {
         if (fld.options[i].selected) {
        	storageBillingList.push(fld.options[i].value);
          }
         }
		 var fromDate=document.forms['storageInvoiceEmailForm'].elements['fromDate'].value;
		 fromDate=fromDate.trim();
		   if(fromDate!=''){
			   fromDate=convertItProperDateFormate(fromDate);
		   }
		 var toDate=document.forms['storageInvoiceEmailForm'].elements['toDate'].value;
		 toDate=toDate.trim();
		   if(toDate!=''){
			   toDate=convertItProperDateFormate(toDate);
		   }
		   window.location="getStorageInvoiceListExcel.html?ajax=1&storageBillingList="+ encodeURI(storageBillingList)+"&fromDate1="+ encodeURI(fromDate)+"&toDate1="+ encodeURI(toDate)+"&decorator=popup&popup=true";
	 	  
	 }
 }

 function convertItProperDateFormate(target){ 
		var calArr=target.split("-");
		var year=calArr[2];
		year="20"+year;
		var month=calArr[1];
		  	   if(month == 'Jan') { month = "01"; }  
		  else if(month == 'Feb') { month = "02"; } 
		  else if(month == 'Mar') { month = "03"; } 
		  else if(month == 'Apr') { month = "04"; } 
		  else if(month == 'May') { month = "05"; } 
		  else if(month == 'Jun') { month = "06"; }  
		  else if(month == 'Jul') { month = "07"; } 
		  else if(month == 'Aug') { month = "08"; }  
		  else if(month == 'Sep') { month = "09"; }  
		  else if(month == 'Oct') { month = "10"; }  
		  else if(month == 'Nov') { month = "11"; }  
		  else if(month == 'Dec') { month = "12"; }	
		var day=calArr[0];
		var date=year+"-"+month+"-"+day;
		return date;
	}
 function sendMailForVoer(){
	 if(fieldValidate()){
		 var fromDate=document.forms['storageInvoiceEmailForm'].elements['fromDate'].value;
		 fromDate=fromDate.trim();
		   if(fromDate!=''){
			   fromDate=convertItProperDateFormate(fromDate);
		   }
		 var toDate=document.forms['storageInvoiceEmailForm'].elements['toDate'].value;
		 toDate=toDate.trim();
		   if(toDate!=''){
			   toDate=convertItProperDateFormate(toDate);
		   }
		   var resetPrintOp=document.forms['storageInvoiceEmailForm'].elements['shipOption'].value;
		   var isPrintOptFlagChk=""; 
		   var isPrintOpt=""; 
		   <configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
		      isPrintOptFlagChk=document.forms['storageInvoiceEmailForm'].elements['isPrintOptFlag'].value;	
		      isPrintOpt=document.forms['storageInvoiceEmailForm'].elements['isPrintOpt'].value;	 	
		      </configByCorp:fieldVisibility> 
		      document.getElementById("overlayForAgentMarket").style.display = "block";
    		  document.forms['storageInvoiceEmailForm'].action ="sendStorageSeprateInvoiceMailAjax.html?fromDate1="+ encodeURI(fromDate)+"&toDate1="+ encodeURI(toDate);
       		  document.forms['storageInvoiceEmailForm'].submit();
	 }
}
 function sendMail(){
	 if(fieldValidate()){
		 var fromDate=document.forms['storageInvoiceEmailForm'].elements['fromDate'].value;
		 fromDate=fromDate.trim();
		   if(fromDate!=''){
			   fromDate=convertItProperDateFormate(fromDate);
		   }
		 var toDate=document.forms['storageInvoiceEmailForm'].elements['toDate'].value;
		 toDate=toDate.trim();
		   if(toDate!=''){
			   toDate=convertItProperDateFormate(toDate);
		   }
		   var resetPrintOp=document.forms['storageInvoiceEmailForm'].elements['shipOption'].value;
		   var isPrintOptFlagChk=""; 
		   var isPrintOpt=""; 
		   <configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
		      isPrintOptFlagChk=document.forms['storageInvoiceEmailForm'].elements['isPrintOptFlag'].value;	
		      isPrintOpt=document.forms['storageInvoiceEmailForm'].elements['isPrintOpt'].value;	 	
		      </configByCorp:fieldVisibility> 
		      document.getElementById("overlayForAgentMarket").style.display = "block";
    		  document.forms['storageInvoiceEmailForm'].action ="sendStorageInvoiceMailAjax.html?fromDate1="+ encodeURI(fromDate)+"&toDate1="+ encodeURI(toDate);
       		  document.forms['storageInvoiceEmailForm'].submit();
	 }
}
	    
	         
function fieldValidate(){
       if(document.forms['storageInvoiceEmailForm'].elements['fromDate'].value==''){
	      	alert("Please select the from date");
	     	return false;
	   }if(document.forms['storageInvoiceEmailForm'].elements['toDate'].value==''){
	       	alert("Please select the to date ");
	       	return false;
	   }
	   <configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
	   var resetPrintOp=document.forms['storageInvoiceEmailForm'].elements['validshipOption'].value; 
	   if(resetPrintOp=='false'){
		   alert("Please select print options from the list ");
		   //document.forms['storageInvoiceEmailForm'].elements['validshipOption'].value='';
	       	return false;
	   }
	   </configByCorp:fieldVisibility>
	   return true;
}
</script>
<script type="text/javascript">	

function resetPrintOpt(ship,recinv,opt){
	if(opt.value!=""){
	 var resetPrintOp=document.forms['storageInvoiceEmailForm'].elements['shipOption'].value; 
	 var newoptSet="";
	//
	//alert(opt.value);	
	  var os=resetPrintOp.split(",");
	  if(os!=null && os.length>0){
		  for(var i=0;i<os.length;i++){
		  var oss=os[i];
		  var ss=oss.split("~");
		  if(ss!=null && ss.length>1){
			var sp=ss[0];
			var  opt1=ss[1];
			var  invNum=ss[2];			
			if(sp==ship && invNum==recinv){	
				if(newoptSet!=""){			
				newoptSet=newoptSet+","+ship+"~"+opt.value+"~"+recinv;
				}else{
					newoptSet=ship+"~"+opt.value+"~"+recinv;
				}
					
			}else{
				if(newoptSet!=""){			
					newoptSet=newoptSet+","+sp+"~"+opt1+"~"+invNum;
					}else{
						newoptSet=sp+"~"+opt1+"~"+invNum;
					}
			}
		  }
		  }
	  }
	  //alert(newoptSet);
	  document.forms['storageInvoiceEmailForm'].elements['shipOption'].value=newoptSet;
	  document.forms['storageInvoiceEmailForm'].elements['validshipOption'].value='';
	}else{
		document.forms['storageInvoiceEmailForm'].elements['validshipOption'].value="false"; 
		//alert("can not be blank");
	}
		//
}

function setVal(dp){
	//alert(dp.value);
	document.forms['storageInvoiceEmailForm'].elements['emailPrintOption'].value=dp.value;
}

function onlyDelNoQuotes(evt,targetElement) 
{
		var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==46 || keyCode==8){
  		targetElement.value = '';
  }else{
 return false;
  	
  }
}

//alert('${emailPrintOption}');
<configByCorp:fieldVisibility componentId="component.field.partnerPrivate.emailPrintOption">
var tmp=document.forms['storageInvoiceEmailForm'].elements['emailPrintOption1'];
for(var i=0;i<tmp.options.length;i++){
	if('${emailPrintOption}'==tmp.options[i].value){
		//alert("found............................");
		document.forms['storageInvoiceEmailForm'].elements['emailPrintOption1'].value='${emailPrintOption}';
	}
}
</configByCorp:fieldVisibility>
    </script>

