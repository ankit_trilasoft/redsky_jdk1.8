package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.UpdateRuleResult;

public interface UpdateRuleResultManager extends GenericManager<UpdateRuleResult, Long> {
	public List findMaximumId();
}
