package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;  
import com.trilasoft.app.dao.PartnerRateGridContractsDao; 
import com.trilasoft.app.model.PartnerRateGridContracts;

public class PartnerRateGridContractsDaoHibernate extends GenericDaoHibernate<PartnerRateGridContracts, Long> implements PartnerRateGridContractsDao{
	public PartnerRateGridContractsDaoHibernate() {
		super(PartnerRateGridContracts.class); 
	}

	public List getRateGridContractsList(String sessionCorpID, Long partnerRateGridID) {
		
		return getHibernateTemplate().find("from PartnerRateGridContracts where partnerRateGridID = '"+partnerRateGridID+"' and corpID='"+sessionCorpID+"' ");
	}

	public List getdDiscountCompanyList() {
		return getHibernateTemplate().find("select corpID from Company where networkFlag = true ");
	}

	public List getContractList(String sessionCorpID) {
		
		return this.getSession().createSQLQuery("select  distinct contract from contract where corpID='"+sessionCorpID+"' and ending is null and displayonAgentTariff = true").list();
	}

	public int findUniqueCount(Long partnerRateGridID,String sessionCorpID, String discountCompany, String contract, String effectiveDate) {
		List countList= getHibernateTemplate().find("select count(*) from PartnerRateGridContracts where contract in ("+contract+") and corpID='"+sessionCorpID+"' and discountCompany='"+discountCompany+"'  and effectiveDate='"+effectiveDate+"' and partnerRateGridID='"+partnerRateGridID+"'");
		int count=Integer.parseInt(countList.get(0).toString());
		return count;
	} 

	public List findMaxEffectiveDate(String sessionCorpID, Long partnerRateGridID) {
		List effectiveDateList=new ArrayList();
		effectiveDateList= getHibernateTemplate().find("select max(effectiveDate) from PartnerRateGridContracts where corpid='"+sessionCorpID+"' and partnerRateGridID='"+partnerRateGridID+"'");
		return effectiveDateList; 
	}

	public int findUniqueCount(Long partnerRateGridID,String sessionCorpID, String discountCompany, String contract, String effectiveDate, Long id) {
		List countList= getHibernateTemplate().find("select count(*) from PartnerRateGridContracts where contract in ("+contract+") and corpID='"+sessionCorpID+"' and discountCompany='"+discountCompany+"'  and effectiveDate='"+effectiveDate+"' and id != '"+id+"' and partnerRateGridID='"+partnerRateGridID+"'");
		int count=Integer.parseInt(countList.get(0).toString());
		return count;
	} 
}
