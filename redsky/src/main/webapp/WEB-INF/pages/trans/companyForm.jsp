<%@ include file="/common/taglibs.jsp"%>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>input[type=checkbox]{margin:0px;}</style>
<head>   
    <title><fmt:message key="companyDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='companyDetail.heading'/>"/>   
</head>  

<s:form id="companyForm" name="companyForm" action="saveCompany" method="post" validate="true">      
 
<s:hidden name="company.id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="company.storageBillingStatus" />
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="company.rulesRunning"/>
<div id="Layer1" style="width:100%;">
<div id="newmnav">
  <ul>
  	<li><a href="companies.html"><span>Company List</span></a></li>
   <li  id="newmnav1" style="background:#FFF"><a class="current""><span>Company Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    <li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${company.id}&tableName=company&decorator=popup&popup=true&corpIds=${company.corpID}&companyListTSFT=Y','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
   </ul></div><div class="spn">&nbsp;</div><br>
<div id="content" align="center" style="margin-bottom:10px;">
<div id="liquid-round-top">
    <div class="top" style="margin-top:-12px;!margin-top:-8px;"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="1" cellpadding="1" border="0">
	<tbody>
		<tr>
	<td class="listwhitetext" align="left" colspan="35">
				<fieldset >
					<legend>Basic Info</legend>
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
			 <tr class="listwhitetext">
		   		<td align="right" width="120"><fmt:message key="company.companyName"/></td>
		   		<td align="left"><s:textfield name="company.companyName" maxlength="40" required="true" cssClass="input-text" size="30"/></td>
		   		<td align="right" width="110"><fmt:message key="company.corpID"/></td>
		   		<td align="left"><s:textfield name="company.corpID" maxlength="15" required="true" cssClass="input-text" size="15"/></td>
		   	    <td align="right" width="88">Parent&nbsp;Corp&nbsp;ID</td>
		   		<td align="left"><s:textfield name="company.parentCorpId" maxlength="15" required="true" cssClass="input-text" size="17"/></td>
		   		<c:if test="${(company.parentCorpId)!=(company.corpID)}">
		   		 <td align="right">Acct&nbsp;Ref&nbsp;same&nbsp;as&nbsp;Parent</td>
		      	 <td align="left"><s:checkbox name="company.acctRefSameParent" value="${company.acctRefSameParent}" cssStyle="margin:0px;padding:0px;" required="true" /></td>
		      	 </c:if>
		   	</tr>
		   <tr class="listwhitetext">
		   		<td align="right"><fmt:message key="company.address1"/></td>
		   		<td align="left"><s:textfield name="company.address1" maxlength="30" required="true" cssClass="input-text" size="30"/></td>
		   		<td align="right"><fmt:message key="company.city"/></td>
		   		<td align="left"><s:textfield name="company.city" maxlength="15" required="true" cssClass="input-text" size="15"/></td>
		   		<td align="right" ><fmt:message key="company.countryID"/></td>
		   		<td align="left"><s:select cssClass="list-menu" name="company.countryID" list="%{CountryIdList}" headerKey=""	headerValue="" onchange="" cssStyle="width:130px" tabindex="9" /><%-- <s:textfield name="company.countryID" maxlength="15" required="true" cssClass="input-text" size="15"/>--%></td>
		   		
		   		
		   	</tr>
		   <tr class="listwhitetext">
		   		<td align="right"><fmt:message key="company.address2"/></td>
		   		<td align="left"><s:textfield name="company.address2" maxlength="30" required="true" cssClass="input-text" size="30"/></td>
		   		<td align="right"><fmt:message key="company.networkFlag"/></td>
		   		<td colspan="4" align="left" >
		   		<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;">
		   		<tr>
		   		<td align="left"><s:checkbox name="company.networkFlag" required="true" cssStyle="margin:0px;padding:0px;" /></td>
		   		<td align="left" width="60"></td>
		   		<td align="right">Security Enabled&nbsp;</td>
		      	<td align="left"><s:checkbox name="company.securityChecked" value="${company.securityChecked}" cssStyle="margin:0px;padding:0px;" required="true" /></td>
		        <td align="left" width="29"></td>
		        <td align="right">UTSI&nbsp;</td>
		       <td align="left"><s:checkbox name="company.UTSI" value="${company.UTSI}" cssStyle="margin:0px;padding:0px;" required="true" /></td>
		   		<td align="left" width="25"></td>
		   		<td align="right">Survey Linking&nbsp;</td>
		       <td align="left"><s:checkbox name="company.surveyLinking" value="${company.surveyLinking}" cssStyle="margin:0px;padding:0px;" required="true" /></td>
		   		</tr>
		   		</table>
		   	</tr>
		   <tr class="listwhitetext">
		   		<td align="right"><fmt:message key="company.postal"/></td>
		   		<td align="left"><s:textfield name="company.postal" maxlength="15" required="true" cssClass="input-text" size="30"/></td>
		   		<td align="right">Survey Email Sign</td>
		   		<td align="left" colspan="4"><s:textfield name="company.surveyEmailSign" maxlength="500" required="true" cssClass="input-text" size="88"/></td>
		   	</tr>
		   <tr class="listwhitetext">
		   		<td align="right" style="width:164px;"><fmt:message key="company.contactNumber"/></td>
		   		<td align="left"><s:textfield name="company.contactNumber" maxlength="20" required="true" cssClass="input-text" size="30"/></td>
		  		 <td align="right">Automatic&nbsp;Link&nbsp;Up</td>
		      	 <td align="left"><s:checkbox name="company.automaticLinkup" value="${company.automaticLinkup}" cssStyle="margin:0px;padding:0px;" required="true" /></td>
		   		<td align="right" colspan="2">FTP Path
		      	 <s:textfield name="company.ftpDir" maxlength="15" required="true" cssClass="input-text" size="35"/></td>
		   
		  </tr>
		  <tr class="listwhitetext" > 
		  <td>Landing&nbsp;Page&nbsp;Welcome&nbsp;Message</td>
		  <td><s:textfield name="company.landingPageWelcomeMsg" maxlength="200" required="true" cssClass="input-text" size="30" /></td>
		  <td align="right">License Type</td>
		  <td><s:select cssStyle="width:106px" cssClass="list-menu"  name="company.licenseType" list="%{redSkyLicenseTypeList}" headerKey="" headerValue=""/></td>
		 <td align="right">Status&nbsp;&nbsp;</td>
		  <td align="left"><s:checkbox name="company.status" value="${company.status}" cssStyle="margin:0px;padding:0px;"  /></td>
		  </tr>		  
		 </table>
	 </fieldset>
	</td>
	 </tr>
	 <tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset>
		<legend>Set Up</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1" width="100%">
			<tr><td align="left"  width="120px"></td><td></td><td width="190px"></td><td></td><td width="55"></td><td class="listwhitetext">Job Type</td> <td class="listwhitetext" align="right" >Distance From</td></tr>
			<tr>
				
				<td align="right" class="listwhitetext"><fmt:message key="company.companyDivisionFlag"/></td> 
   	        	<td align="left"><s:select cssClass="list-menu"   name="company.companyDivisionFlag" list="{'Yes','No'}" headerKey="" headerValue="" cssStyle="width:97px"/></td>  				
				<td align="right" class="listwhitetext"><fmt:message key="company.reportValidationFlag"/></td> 
   	        	<td align="left"><s:select cssClass="list-menu"   name="company.reportValidationFlag" list="{'Yes','No'}" headerKey="" headerValue="" cssStyle="width:106px"/></td> 				
				<td align="left"></td>
				<td><s:textfield name="company.jobType1" cssClass="input-text" maxlength="100" cssStyle="width:110px"/></td>
		       	<td align="right" ><s:select name="company.distanceFrom1" list="%{distanceFrom1}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:105px;" /></td>
			</tr>
			<tr class="listwhitetext">
								
				<td align="right"><fmt:message key="company.lastTicketNumber"/></td>
		   		<td align="left"><s:textfield name="company.lastTicketNumber" maxlength="20" required="true" cssClass="input-text" cssStyle="width:95px" onchange="onlyNumeric(this);"/></td>
		   		<td align="right">Auto Save Promt</td>
		   		<td><s:select cssStyle="width:106px" cssClass="list-menu"  name="company.autoSavePrompt" list="{'Yes','No'}" headerKey="" headerValue=""/></td>
		   		<td align="left"></td>
		       	<td><s:textfield name="company.jobType2"  cssClass="input-text" maxlength="100" cssStyle="width:110px"/></td>
		       	<td align="right" ><s:select name="company.distanceFrom2" list="%{distanceFrom2}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:105px;" /></td>
		   	</tr>
		   	<tr class="listwhitetext">	
		   		  		
		   		<td align="right" >Trip Number</td>
		   		<td ><s:textfield name="company.lastTripNumber"  cssClass="input-text" maxlength="15" onchange="return onlyNumeric(this);" cssStyle="width:95px"/></td>	 		
		   		<td align="right" width="">Groupage Default</td>
		     	 <td> <s:select name="company.grpDefault" list="%{groupAge}" headerKey="" headerValue="" cssStyle="width:106px;" cssClass="list-menu"/></td>
		   		<td align="left"></td>
		        <td><s:textfield name="company.jobType3" cssClass="input-text" maxlength="100" cssStyle="width:110px"/></td>
		        <td align="right" ><s:select name="company.distanceFrom3" list="%{distanceFrom3}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:105px;" /></td>
		   	</tr>
		   	<tr class="listwhitetext" >
		   		<td align="right" width="196px" >Force&nbsp;SO&nbsp;Dashboard <s:checkbox  key="company.forceSoDashboard" cssStyle="margin:0px;vertical-align:middle;" value="${company.forceSoDashboard}"  /></td>
		   		<td align="right"  width="115px">Force&nbsp;Doc&nbsp;Center <s:checkbox  key="company.forceDocCenter"  cssStyle="vertical-align:middle;margin:0px;" value="${company.forceDocCenter}" /></td>	 		   		
		   		<td align="right">Certificate&nbsp;Additional&nbsp;Number</td>
		   		<td align="left"><s:textfield name="company.certificateAdditionalNumber" maxlength="5" required="true" cssClass="input-text" cssStyle="width:104px" onchange="onlyNumericNew(this);"/></td>
		   		<td></td>
		   		<td align="right" >Order&nbsp;of&nbsp;Include/Exclude&nbsp;services</td>
		     	<td align="left" > <s:select name="company.quoteServices" list="%{quoteservicesorder}" headerKey="" headerValue="" cssStyle="width:105px;" cssClass="list-menu"/></td>
		     	 
		   	</tr>
		   	<tr class="listwhitetext"></tr>
		   	<tr><td style="height:5px;"></td></tr>		 	 	
		   	<tr>
		   	<td colspan="10">
		   	<table style="margin:0px;padding:0px;width:100%;">
		   	<tr class="listwhitetext">
		   	<td align="right">Allow&nbsp;Future&nbsp;Actual&nbsp;Date&nbsp;Entry</td>
		   	<td align="left"><s:checkbox name="company.allowFutureActualDateEntry" cssStyle="vertical-align:middle;margin:0px;" value="${company.allowFutureActualDateEntry}"  required="true" /> </td>
		   	<td align="right">CMM&nbsp;DMM&nbsp;Agent</td>
		   	<td align="left"><s:checkbox name="company.cmmdmmAgent"  cssStyle="vertical-align:middle;margin:0px;" value="${company.cmmdmmAgent }"  required="true"/></td>
		   	
		    <td align="right" >Mobile Access Enabled</td>
			<td align="left"><s:checkbox name="company.accessRedSkyCportalapp"  cssStyle="vertical-align:middle;margin:0px;" value="${company.accessRedSkyCportalapp}" /></td>
		   	</tr>
		   	<tr><td style="height:2px;"></td></tr>		 	
		   	<tr class="listwhitetext">
		   	<td align="right">Internal&nbsp;User&nbsp;Logging</td>
		   	<td align="left"><s:checkbox name="company.tracInternalUser"  cssStyle="vertical-align:middle;margin:0px;" value="${company.tracInternalUser}"  required="true" /></td>
		   	<td align="right">Work&nbsp;Ticket&nbsp;Queue</td>
		   	<td align="left"><s:checkbox name="company.workticketQueue"  cssStyle="vertical-align:middle;margin:0px;" value="${company.workticketQueue}"  required="true" /></td>
		   	<td align="right">Print&nbsp;Insurance&nbsp;Certificate</td>
		   	<td align="left"><s:checkbox name="company.printInsuranceCertificate"  cssStyle="vertical-align:middle;margin:0px;" value="${company.printInsuranceCertificate}"  required="true" /></td>
		   	</tr>
		   	<tr><td style="height:2px;"></td></tr>	
		   	
		 	<tr class="listwhitetext">	
		   			   	<c:set var="company.cportalAccessCompanyDivisionLevel" value="false" />
    <c:if test="${company.cportalAccessCompanyDivisionLevel}">
	 <c:set var="company.cportalAccessCompanyDivisionLevel" value="true" />
	</c:if>
        <td align="right" class="listwhitetext" width="">Enable&nbsp;Cportal&nbsp;and&nbsp;Account&nbsp;on&nbsp;Company&nbsp;Division</td>
          <td class="listwhitetext" width=""><s:checkbox id="cportalAccessCompanyDiv" key="company.cportalAccessCompanyDivisionLevel" onclick="enableDisableURLField();changeStatus();" value="${company.cportalAccessCompanyDivisionLevel}" fieldValue="true" tabindex="17" cssStyle="margin:0px;" /></td>
		   	
		   	<c:set var="company.accessQuotationFromCustomerFile" value="false" />
             <c:if test="${company.accessQuotationFromCustomerFile}">
	         <c:set var="company.accessQuotationFromCustomerFile" value="true" />
	         </c:if>
        <td align="right" class="listwhitetext" width="">Access&nbsp;Quotation&nbsp;From&nbsp;CustomerFile</td>
          <td class="listwhitetext" width=""><s:checkbox key="company.accessQuotationFromCustomerFile" onclick="changeStatus();" value="${company.accessQuotationFromCustomerFile}" fieldValue="true" tabindex="17" cssStyle="margin:0px;" /></td>
		   <td align="right" colspan="7">Cportal&nbsp;Branding&nbsp;URL
		      	 <s:textfield id="cportalBrandingU" name="company.cportalBrandingURL"  maxlength="200" required="true" cssClass="input-text" size="45" /></td>	
		   	</tr >
		   	<tr><td style="height:2px;"></td></tr>	
		   	
		 	<tr class="listwhitetext">
		 	<td align="right" class="listwhitetext" width="">Agent&nbsp;TDR&nbsp;Email</td>
          <td class="listwhitetext" width=""><s:checkbox  key="company.agentTDREmail"  value="${company.agentTDREmail}" fieldValue="true" cssStyle="margin:0px;" /></td>		   
		   <td align="right" class="listwhitetext" width="">OA/DA/Weight/Vol&nbsp;mandatory&nbsp;validation</td>
          <td class="listwhitetext" width=""><s:checkbox  key="company.oAdAWeightVolumeMandatoryValidation"  value="${company.oAdAWeightVolumeMandatoryValidation}"  onclick="autoCheckVal();" fieldValue="true" cssStyle="margin:0px;" /></td>
		 <td align="right" class="listwhitetext" width="205">Sub&nbsp;OA/DA&nbsp;soft&nbsp;validation</td>
          <td class="listwhitetext" width=""><s:checkbox  key="company.subOADASoftValidation"  value="${company.subOADASoftValidation}" fieldValue="true" cssStyle="margin:0px;" onclick="autoCheckOADAVal();" /></td>
		 
		 <td align="right" class="listwhitetext" width="50">Full-Audit</td>
          <td class="listwhitetext" width=""><s:checkbox  key="company.fullAudit" value="${company.fullAudit}" fieldValue="true" cssStyle="margin:0px;" /></td>
		 </tr>
		 <tr class="listwhitetext">
            <td align="right" class="listwhitetext" >Extracted&nbsp;File&nbsp;Audit</td>
          <td class="listwhitetext" width="">
          <s:checkbox  key="company.extractedFileAudit" value="${company.extractedFileAudit}" fieldValue="true" cssStyle="margin:0px;" /></td> 
            <td align="right" class="listwhitetext" width="50">Activity&nbsp;Mgmt.&nbsp;Version II</td>
          <td class="listwhitetext" width="">
          <s:checkbox  key="company.activityMgmtVersion2" value="${company.activityMgmtVersion2}" fieldValue="true" cssStyle="margin:0px;" /></td> 
            </tr>
            <tr class="listwhitetext">
            <td align="right">Restrict&nbsp;Outside&nbsp;Access</td>
		   	<td align="left"><s:checkbox name="company.restrictAccess"  cssStyle="vertical-align:middle;margin:0px;" value="${company.restrictAccess}"  required="true" /></td>
		   	<td align="right" colspan="5"><s:textfield id="cportalBrandingU" name="company.restrictedMassage"  maxlength="200" required="true" cssClass="input-text" size="125" /></td></tr>
		    <tr class="listwhitetext">
            <td align="right">OIjob</td> 
		    <td colspan="6"><s:textfield name="company.oiJob" cssClass="input-text" maxlength="100" cssStyle="width: 97.6%;margin-left: 17px;"/></td>
		    
		    </tr>
		    <tr class="listwhitetext">
            <td align="right">DashBoard Hide Jobs </td> 
		    <td colspan="6"><s:textfield name="company.dashBoardHideJobs" cssClass="input-text" maxlength="100" cssStyle="width: 97.6%;margin-left: 17px;"/></td>
   </tr>
    <tr class="listwhitetext">
            <td align="right">Company Specific</td> 
		    <td colspan="6"><s:textfield name="company.corporateId" cssClass="input-text" maxlength="100" cssStyle="width: 97.6%;margin-left: 17px;"/></td>
   </tr>
   <tr>
    <!-- chayanDobriyal -->
   	
   			   	<td align="right" width="20">BillToExtractDate
   	
		   	<c:if test="${not empty company.billToExtractDate}">
				 <s:text id="NextCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.billToExtractDate"/></s:text>
				 <s:textfield cssClass="input-text" id="billToExtractDate" name="company.billToExtractDate" value="%{NextCheckOnFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="billToExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.billToExtractDate}">
				 <s:textfield cssClass="input-text" id="billToExtractDate" name="company.billToExtractDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="billToExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
			 	<td align="right" width="20">VendorExtractDate
   	
		   		<c:if test="${not empty company.vendorExtractDate}">
				 <s:text id="CheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.vendorExtractDate"/></s:text>
				 <s:textfield cssClass="input-text" id="vendorExtractDate" name="company.vendorExtractDate" value="%{CheckOnFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="vendorExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.vendorExtractDate}">
				 <s:textfield cssClass="input-text" id="vendorExtractDate" name="company.vendorExtractDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="vendorExtractDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
			<td align="right" class="listwhitetext" width="">Allow  Agent Portal For Invoice Upload</td>
          <td class="listwhitetext" width=""><s:checkbox  name="company.allowAgentInvoiceUpload " key=""  value="${company.allowAgentInvoiceUpload}" cssStyle="margin:0px;" /></td>		   
		   <td align="right" width="20">EmailUploadStart
		   		<c:if test="${not empty company.emailUploadStart}">
				<s:text id="emailUploadStart1" name="${FormDateValue}"><s:param name="value" value="company.emailUploadStart"/></s:text>
				 <s:textfield cssClass="input-text" id="emailUploadStart" name="company.emailUploadStart" value="%{emailUploadStart1}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="emailUploadStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.emailUploadStart}">
				 <s:textfield cssClass="input-text" id="emailUploadStart" name="company.emailUploadStart" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="emailUploadStart_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
		    </tr>
		  </table>
		   	</td>
		   	</tr> 
		</table>
	 </fieldset>
	</td>
</tr>
<tr><td colspan="35" height="10px"></td></tr>


 <tr>
<td class="listwhitetext" align="left" colspan="35">
<table style="margin:0px;" border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td class="listwhitetext" align="left" >
<fieldset >
<legend>Integration Options</legend>
<table style="margin:0 0 5px;" border="0" cellspacing="1" cellpadding="1" width="100%">
<tr class="listwhitetext">
<td align="right" width="200">US&nbsp;Vanline&nbsp;Functionality</td>
<td align="left"><s:checkbox name="company.vanlineEnabled "  cssStyle="vertical-align:middle;margin:0px;" value="${company.vanlineEnabled }"  required="true"/></td>
<td align="right" width="100">Voxme Integration</td>		   		
<td align="left"><s:checkbox cssStyle="vertical-align:middle;margin:0px;" name="company.voxmeIntegration" value="${company.voxmeIntegration}"  required="true" /></td>
<td align="right" class="listwhitetext" width="100">MSS</td>
<td class="listwhitetext" width=""><s:checkbox  key="company.enableMSS"  value="${company.enableMSS}" fieldValue="true" cssStyle="margin:0px;" onclick="autoCheckOADAVal();" /></td>
<td align="right" class="listwhitetext" width="100">Move4U</td>
<td class="listwhitetext" width=""><s:checkbox  key="company.enableMoveForYou"  value="${company.enableMoveForYou}" fieldValue="true" cssStyle="margin:0px;" onclick="autoCheckOADAVal();" /></td>
</tr>
</table>
<table style="margin:0px;" border="0" cellspacing="0" cellpadding="0" width="100%">
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset style="width:864px;">
		<legend style="font-size:11px;">Price Point Setup</legend>
		<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1" width="100%">
		<tr class="listwhitetext">
		   	<td align="right" width="">Enable&nbsp;Price&nbsp;Point</td>
		   	<td align="left"><s:checkbox cssStyle="vertical-align:middle;margin:0px;" name="company.enablePricePoint" value="${company.enablePricePoint}"  required="true" /></td>	
		   	
		   	<td align="right" >Membership&nbsp;ID </td>
		   	<td colspan="3"><s:textfield name="company.clientID"  cssClass="input-text" maxlength="8" onchange="return onlyNumeric(this);" cssStyle="width:43px"/></td>	
		   	
		   	<td align="right" width="20">Start&nbsp;Date</td>
		   	<td width="100">
		   	<c:if test="${not empty company.priceStartDate}">
				 <s:text id="priceStartDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.priceStartDate"/></s:text>
				 <s:textfield cssClass="input-text" id="priceStartDate" name="company.priceStartDate" value="%{priceStartDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="priceStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.priceStartDate}">
				 <s:textfield cssClass="input-text" id="priceStartDate" name="company.priceStartDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="priceStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
			
			  <td align="right" width="50">End&nbsp;Date</td>
		   	<td width="100">
		   	<c:if test="${not empty company.priceEndDate}">
				 <s:text id="priceEndDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.priceEndDate"/></s:text>
				 <s:textfield cssClass="input-text" id="priceEndDate" name="company.priceEndDate" value="%{priceEndDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="priceEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.priceEndDate}">
				 <s:textfield cssClass="input-text" id="priceEndDate" name="company.priceEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="priceEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
			<td align="right">PP&nbsp;$:</td>
		   	<td align="left" width="60"><s:textfield name="company.subscriptionAmt" maxlength="10" required="true" cssClass="input-text" size="6" /></td>
		   	
		   	<td align="right">RS&nbsp;Inc&nbsp;$:</td>
		   	<td align="left" width="60"><s:textfield name="company.rsAmount" maxlength="10" required="true" cssClass="input-text" size="6" /></td>
		   	<td align="right" >#&nbsp;Users</td>
		   	<td><s:textfield name="company.numberOfPricePointUser" maxlength="8" required="true" cssClass="input-text" size="4" onchange="onlyNumeric(this);"/></td>
		   	</tr>
		   	<tr class="listwhitetext">	
		   	    
		   	</tr>	   		
		</table>
		</fieldset>
</td>
</tr>

<tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset style="width:865px;">
		<legend style="font-size:11px;">Mobile Mover</legend>
		<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
		<td align="right" class="listwhitetext">Enable&nbsp;Mobile&nbsp;Mover</td>
		<td align="left"><s:checkbox cssStyle="vertical-align:middle;margin:0px;" name="company.enableMobileMover" value="${company.enableMobileMover}"   required="true" /></td>	
		<td align="right" width="80" class="listwhitetext">Start&nbsp;&nbsp;Date</td>
		   	<td width="120">
		   	 	<c:if test="${not empty company.mmStartDate}">
				 <s:text id="mobileMoverStartDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.mmStartDate"/></s:text>
				 <s:textfield cssClass="input-text" id="mmStartDate" name="company.mmStartDate" value="%{mobileMoverStartDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.mmStartDate}">
				 <s:textfield cssClass="input-text" id="mmStartDate" name="company.mmStartDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
		   	
		<td align="right" width="50" class="listwhitetext">End&nbsp;&nbsp;Date</td>
		   	<td width="120">
		   		<c:if test="${not empty company.mmEndDate}">
				 <s:text id="mobileMoverDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.mmEndDate"/></s:text>
				 <s:textfield cssClass="input-text" id="mmEndDate" name="company.mmEndDate" value="%{mobileMoverDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.mmEndDate}">
				 <s:textfield cssClass="input-text" id="mmEndDate" name="company.mmEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
		   	
		<td align="right" class="listwhitetext">Monthly $:</td>
		   	<td align="left" width="120"><s:textfield name="company.mmSubscriptionAmt" maxlength="10" onchange="return onlyNumeric(this);" required="true" cssClass="input-text" size="15" /></td>
		   	<td align="right" class="listwhitetext" >Membership ID </td>
		   		<td colspan="3"><s:textfield name="company.mmClientID"  cssClass="input-text" maxlength="8"  cssStyle="width:63px"/></td>	
		
		
		   	<tr class="listwhitetext">	
		   	    
		   	</tr>	   		
		</table>
		</fieldset>
		
</td>
</tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset style="width:865px;">
		<legend style="font-size:11px;">LISA Integration</legend>
		<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
		<%-- <td align="right" class="listwhitetext">Enable&nbsp;Mobile&nbsp;Mover</td>
		<td align="left"><s:checkbox cssStyle="vertical-align:middle;margin:0px;" name="company.enableMobileMover" value="${company.enableMobileMover}"   required="true" /></td>	
		<td align="right" width="80" class="listwhitetext">Start&nbsp;&nbsp;Date</td>
		   	<td width="120">
		   	 	<c:if test="${not empty company.mmStartDate}">
				 <s:text id="mobileMoverStartDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.mmStartDate"/></s:text>
				 <s:textfield cssClass="input-text" id="mmStartDate" name="company.mmStartDate" value="%{mobileMoverStartDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.mmStartDate}">
				 <s:textfield cssClass="input-text" id="mmStartDate" name="company.mmStartDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmStartDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
		   	
		<td align="right" width="50" class="listwhitetext">End&nbsp;&nbsp;Date</td>
		   	<td width="120">
		   		<c:if test="${not empty company.mmEndDate}">
				 <s:text id="mobileMoverDateCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.mmEndDate"/></s:text>
				 <s:textfield cssClass="input-text" id="mmEndDate" name="company.mmEndDate" value="%{mobileMoverDateCheckOnFormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.mmEndDate}">
				 <s:textfield cssClass="input-text" id="mmEndDate" name="company.mmEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="mmEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td> --%>
		<tr> 
		<td align="right" class="listwhitetext">User</td>
		<td align="left" width="120"><s:textfield name="company.integrationUser" maxlength="82"  cssClass="input-text" cssStyle="width:175px" /></td>
		<td width="20"></td>
		<td align="right" class="listwhitetext">Password</td>
		<td colspan="3"><s:textfield name="company.integrationPassword"  cssClass="input-text" maxlength="45"  cssStyle="width:95px"/></td>		
		</tr> 			   		
		</table>
		</fieldset>
		
</td>
</tr>
</table>
</fieldset>
</td>
</tr>
</table>
</td>
</tr>



<tr><td colspan="35" height="10px"></td></tr>
<tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset>
		<legend>Account Controls</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
			<tr class="listwhitetext">
			<td align="right" width="120"><fmt:message key="company.lastCreditInvoice"/></td>
		   		<td align="left"><s:textfield name="company.lastCreditInvoice" maxlength="20" required="true" cssClass="input-text" size="15" onchange="onlyNumeric(this);"/></td>
				<td align="right"><fmt:message key="company.lastInvoiceNumber"/></td>
		   		<td align="left"><s:textfield name="company.lastInvoiceNumber" maxlength="20" required="true" cssClass="input-text" cssStyle="width:71px;" onchange="onlyNumeric(this);"/></td>
				
				<td align="right" >Credit&nbsp;Invoice&nbsp;Sequence </td>
		      	<td align="left"><s:checkbox name="company.creditInvoiceSequence" value="${company.creditInvoiceSequence}" cssStyle="margin:0px;"  required="true" /></td> 
			   <c:set var="isInfochecked" value="false"/>
				<c:if test="${company.transfereeInfopackage}">
				<c:set var="isInfochecked" value="true"/>
				</c:if>												
				<td align="right" width="">Transferee&nbsp;Info&nbsp;package</td>
				<td><s:checkbox key="company.transfereeInfopackage" value="${isInfochecked}" fieldValue="true" cssStyle="margin:0px;" /></td>
			</tr>
			<tr class="listwhitetext">	
				<td align="right"><fmt:message key="company.lastClaimNumber"/></td>
		   		<td align="left" colspan="1"><s:textfield name="company.lastClaimNumber" maxlength="20" required="true" cssClass="input-text" size="15" onchange="onlyNumeric(this);"/></td>
		   		<td align="right" class="listwhitetext"><fmt:message key="company.multiCurrency"/></td> 
 	            <td align="left"><s:select cssClass="list-menu"   name="company.multiCurrency" list="%{multiCurrency}" headerKey="" headerValue="" cssStyle="width:75px"/></td>
 	            <c:set var="ischecked" value="false"/>
				<c:if test="${company.flagDefaultVendorAccountLines}">
				<c:set var="ischecked" value="true"/>
				</c:if>												
				<td align="right" width="275">isDefaultVendorAccountLines</td>
				<td><s:checkbox key="company.flagDefaultVendorAccountLines" value="${ischecked}" cssStyle="margin:0px;" fieldValue="true"/></td>
				<td align="right" class="listwhitetext" width="180px">Auto&nbsp;Generate&nbsp;Acc.&nbsp;Ref.</td>
			    <td align="left"><s:select cssClass="list-menu" name="company.autoGenerateAccRef" list="{'Y','N'}" headerKey="" headerValue="" /></td>	
		   	</tr>		   	
		   	<tr class="listwhitetext">
		   		<td align="right" class="listwhitetext">Estimate VAT Flag</td> 
 	          	<td align="left"><s:select cssClass="list-menu"   name="company.estimateVATFlag" list="%{multiCurrency}" headerKey="" headerValue="" cssStyle="width:105px"/></td>
		   		<td align="right" >Weekly Storage Billing </td>
		      	<td align="left"><s:checkbox name="company.weeklyBilling" value="${company.weeklyBilling}"  required="true" /></td> 
		      	<td align="right">Suspense&nbsp;Account&nbsp;Flag</td>
		      <td align="left"><s:checkbox name="company.useCostTransferred" value="${company.useCostTransferred}"  required="true" cssStyle="margin:0px;"  /></td>
		      <td align="right" class="listwhitetext">Posting&nbsp;Date&nbsp;Stop </td>
		   <td align="left"><s:checkbox name="company.postingDateStop" value="${company.postingDateStop}" cssStyle="margin:0px;" required="true" /></td> 
		   
		   	</tr>
		   	<tr class="listwhitetext">
		   	<td align="right" class="listwhitetext">Freeze&nbsp;Accountline&nbsp;After</td> 
 	          	<td align="left"><s:select cssClass="list-menu"   name="company.accountLineNonEditable" list="%{accountLineNonEditableList}" cssStyle="width:105px"/></td>
		      	<td align="right" class="listwhitetext">Allow&nbsp;Payables&nbsp;Xfer&nbsp;With&nbsp;Approval&nbsp;Only </td>
		      	<td align="left"><s:checkbox name="company.payablesXferWithApprovalOnly" value="${company.payablesXferWithApprovalOnly}"  required="true" /></td> 
		      	<td align="right" class="listwhitetext">Posting&nbsp;Date&nbsp;Flexibility</td>
		      	<td align="left"><s:checkbox name="company.postingDateFlexibility" value="${company.postingDateFlexibility}"  required="true" cssStyle="margin:0px;"/></td>
		      	<td align="right" class="listwhitetext">Keep&nbsp;History&nbsp;Of&nbsp;Currency </td>
		   		<td align="left"><s:checkbox name="company.historyFx" value="${company.historyFx}" cssStyle="margin:0px;"  required="true" /></td>  
		   		</tr>
		   	<tr class="listwhitetext">
		   	<td align="right" class="listwhitetext">agent&nbsp;Invoice&nbsp;Seed</td>
		   	<td align="left" ><s:checkbox name="company.agentInvoiceSeed" value="${company.agentInvoiceSeed}"  required="true" /></td>
		   	<td align="right" class="listwhitetext">Auto&nbsp;Fx&nbsp;Updater</td>
		   	<td align="left" ><s:checkbox name="company.autoFxUpdater" value="${company.autoFxUpdater}"  required="true" /></td>
		   	<td align="right" class="listwhitetext">Charge&nbsp;Discount</td>
		   	<td align="left" ><s:checkbox name="company.chargeDiscountSetting " value="${company.chargeDiscountSetting }"  required="true" /></td>
		   	 <td align="right" class="listwhitetext">Single&nbsp;Company&nbsp;Division&nbsp;Invoicing</td>
		   	 <td align="left" ><s:checkbox name="company.singleCompanyDivisionInvoicing" value="${company.singleCompanyDivisionInvoicing}" cssStyle="margin:0px;" required="true" /></td>
		   </tr>		    
		   <tr class="listwhitetext">
		   	<td align="right" class="listwhitetext">Insurance&nbsp;Premium&nbsp;Tax</td>
		   	<td align="left" ><s:checkbox name="company.insurancePremiumTax" value="${company.insurancePremiumTax}"  required="true" /></td>
		   	<td align="right" class="listwhitetext">Insurance&nbsp;Premium&nbsp;Tax&nbsp;Value</td>
		   	<td align="left" ><s:textfield name="company.insurancePremiumTaxValue" maxlength="7" required="true" cssClass="input-text" size="6" onblur="validateNum(this)"/><span>%</span></td>
		   	<td align="right" class="listwhitetext">Vat&nbsp;for&nbsp;Insurance&nbsp;From&nbsp;Tax</td>
	    	<td><s:select cssClass="list-menu" headerKey=""  headerValue="" name="company.vatInsurancePremiumTax" list="%{vatforInsuranceTax}"   tabindex="" /></td>
		   	<td align="right" class="listwhitetext">Display&nbsp;A-&nbsp;Portal&nbsp;Revision</td>
		   	<td align="left" ><s:checkbox name="company.displayAPortalRevision" value="${company.displayAPortalRevision}"  required="true" /></td>
		   	
		   	 
		   </tr> 
		   <tr class="listwhitetext">
		   	<td align="right" class="listwhitetext">Rec&nbsp;Invoice&nbsp;Per&nbsp;SO</td>
		   	<td align="left" ><s:checkbox name="company.recInvoicePerSO" value="${company.recInvoicePerSO}" /></td> 
		   	<td align="right" class="listwhitetext">Generate&nbsp;Invoice&nbsp;By</td>
		   	<td align="left"><s:select cssClass="list-menu"   name="company.generateInvoiceBy" list="{'Company','CompanyDivision'}" headerKey="" headerValue=""  cssStyle="width:105px"/></td> 
		   <td align="right" class="listwhitetext">Rec&nbsp;Invoice&nbsp;Per&nbsp;SO&nbsp;Job</td>
		   	<td align="left" colspan="2"><s:textfield name="company.recInvoicePerSOJob" maxlength="55" cssClass="input-text" size="41" /></td>
		   </tr> 		   
		   <tr>
		   </tr>
		</table>
	 </fieldset>
	</td>
</tr>
<tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset >
		<legend>Security</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
			<tr class="listwhitetext">		   		
		   		<td align="right" width="120">User Action On Expiry</td>
		   		<td align="left"><s:select cssClass="list-menu"   name="company.userActionOnExpiry" list="{'expire','reset'}" headerKey="" headerValue="" cssStyle="width:105px"/></td> 
		   		<td align="right" width="200">User Password Expiry Duration</td>
		   		<td align="left"><s:textfield name="company.userPasswordExpiryDuration" maxlength="20" required="true" cssClass="input-text" size="13" /></td>
		   		
		   	</tr>
		   	<tr class="listwhitetext">
		   		<td align="right">Partner Action On Expiry</td>
		   		<td align="left"><s:select cssClass="list-menu"   name="company.partnerActionOnExpiry" list="{'expire','reset'}" headerKey="" headerValue="" cssStyle="width:105px"/></td> 
		   		<td align="right">Partner Password Expiry Duration</td>
		   		<td align="left"><s:textfield name="company.partnerPasswordExpiryDuration" maxlength="20" required="true" cssClass="input-text" size="13" /></td>
		   	</tr>			
		</table>
	 </fieldset>
	</td>
</tr>
<tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset>
		<legend>i18n</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
			<tr class="listwhitetext">
		   		<td align="right" width="145"><fmt:message key="company.localeLanguage"/></td>
		   		<td align="left"><s:textfield name="company.localeLanguage" maxlength="2" required="true" cssClass="input-text" size="10" /></td>
		   		<td align="right" width="190"><fmt:message key="company.localeCountry"/></td>
		   		<td align="left"><s:textfield name="company.localeCountry" maxlength="2" required="true" cssClass="input-text" cssStyle="width:102px;"/></td>
		   		<td width="50">&nbsp;</td>
		   		<td align="right">Scaling</td>
		   		<td><s:textfield name="company.scaling" maxlength="10" required="true" cssClass="input-text" size="10"/></td>
		   	</tr>
		   	<tr class="listwhitetext">
		   		<td align="right">Currency Sign</td>		   		
		   		<td align="left"><s:textfield name="company.currencySign" maxlength="10" required="true" cssClass="input-text" size="10"/></td>	
		   		<td align="right"><fmt:message key="company.timeZone"/></td>
		   		<td><s:select cssStyle="width:106px" cssClass="list-menu" name="company.timeZone" list="%{timeZone}" headerKey="" headerValue=""/></td>		   		
		   	</tr>			
		</table>
	 </fieldset>
	</td>
</tr>
<tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset style="width:898px;">
		<legend>RedSky LLC Billing</legend>
			<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
			<tr class="listwhitetext">
			<td align="right" width="100">Client Billing Date </td>
		   	<td width="111">
		   	<c:if test="${not empty company.clientBillingDate}">
				 <s:text id="trackingStatusNextCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.clientBillingDate"/></s:text>
				 <s:textfield cssClass="input-text" id="clientBillingDate" name="company.clientBillingDate" value="%{trackingStatusNextCheckOnFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="clientBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20"/>
			</c:if>	
			<c:if test="${empty company.clientBillingDate}">
				 <s:textfield cssClass="input-text" id="clientBillingDate" name="company.clientBillingDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="clientBillingDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 "/>
			</c:if>
			</td>
			<td align="right" width="80">Go Live Date</td>
			<td width="111">
			<c:if test="${not empty company.goLiveDate}">
			<s:text id="goLiveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="company.goLiveDate"/></s:text>
				<s:textfield cssClass="input-text" id="goLiveDate" name="company.goLiveDate" value="%{goLiveDateFormattedValue}" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="goLiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			</c:if>
			<c:if test="${empty company.goLiveDate}">
				<s:textfield cssClass="input-text" id="goLiveDate" name="company.goLiveDate" required="true" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/>
				 <img id="goLiveDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
			</c:if>
			</td>
			<td  width="80" class="listwhitetext" align="right">RedSky Billing&nbsp;Note</td>
			<td ><s:textarea  name="company.RSBillingInstructions" cssStyle="height:40px;width:210px;" readonly="false" cssClass="textarea"/></td>
	       	<td  class="listwhitetext" align="right">Bill&nbsp;Group</td>
            <td><s:select cssStyle="width:170px" cssClass="list-menu" name="company.rskyBillGroup" list="%{rskyBillGroup}" headerKey="" headerValue=""/></td>		
			</tr>
			<tr class="listwhitetext">
			 	<td  class="listwhitetext" align="right">Default&nbsp;$</td>
			    <td><s:textfield cssClass="input-text" id="defaultBillingRate" name="company.defaultBillingRate" onblur="validateNum(this)" required="true" cssStyle="width:65px;" maxlength="5" /></td>
			    
			    <td  class="listwhitetext" align="right">Rate&nbsp;2</td>
			    <td><s:textfield cssClass="input-text" id="rate2" name="company.rate2" onblur="validateNum(this)" required="true" cssStyle="width:65px;" maxlength="5" /></td>
			 	<td class="listwhitetext" align="right">Minimum&nbsp;Bill&nbsp;Amount</td>
		   	<td colspan="1"><s:textfield cssClass="input-text" id="minimumBillAmount" name="company.minimumBillAmount"  onblur="validateNum1(this)" onchange="return onlyFloat(this);" maxlength="7" required="true" cssStyle="width:65px;" /></td>
		   	<td align="right" class="listwhitetext">UGRN&nbsp;Netting</td>		
			<td align="left"><s:checkbox name="company.UGRNNetting" value="${company.UGRNNetting}"  required="true" /></td> 
		   	</tr>
			<tr class="listwhitetext">
			<td  class="listwhitetext" align="right">ToEmail&nbsp;</td>
			<td colspan="3"><s:textfield cssClass="input-text" id="email" name="company.email" onchange="return checkEmail()"  size="41" maxlength="55" /></td>
			<td class ="listwhitetext" align="right">CcEmail&nbsp;</td>
			<td colspan="2"><s:textfield cssClass="input-text" id="ccEmail" name="company.ccEmail" onchange="return checkCcEmail()" size="37" ></s:textfield></td>			
		</tr>
		</table>
	 </fieldset>
	</td>
</tr>

<tr><td colspan="35" height="10px"></td></tr>
 <tr>
<td class="listwhitetext" align="left" colspan="35">
	<fieldset style="width:898px;padding-top:0px;">
		<legend>Customer Feedback</legend>		
		<table style="margin:0px;float:left;padding-right:10px;" border="0" cellspacing="1" cellpadding="1">
		<tr>
		<td align="right" width="75" class="listwhitetext">Just Say Yes</td>		
		<td align="left"><s:checkbox name="company.justSayYes" value="${company.justSayYes}"  required="true" /></td> 
		<td align="right" width="100" class="listwhitetext">Quality Survey</td>
		<td><s:checkbox key="company.qualitySurvey" value="${company.qualitySurvey}" required="true"/></td>	
		<td align="right" width="151" class="listwhitetext">Private&nbsp;Customers&nbsp;T&nbsp;Code</td>
		<td> <s:textfield name="company.privateCustomerTCode"  maxlength="7" required="true" cssClass="input-text" size="10" /></td>
		</tr>
		</table>
		<table style="margin:0px;float:left;" border="0" cellspacing="0" cellpadding="0">
		<tr>
		<td align="right" style="position: absolute; margin-top: -6px; line-height: 50px;" class="vertlinedata_vert">&nbsp;</td>		
		</tr>
		</table>
		<table style="margin:0px;float:left;padding-left:10px;" border="0" cellspacing="1" cellpadding="1">
		<tr>		
		<td align="right" class="listwhitetext" >Customer Survey by e-mail</td>	
		<td align="left" style="padding-right:45px;"><s:checkbox name="company.customerSurveyByEmail" value="${company.customerSurveyByEmail}" /></td>	
		<td align="right" class="listwhitetext">Survey On</td>
		<td><s:select cssStyle="width:100px" cssClass="list-menu" name="company.genericSurveyOnCfOrSo" list="%{genericSurveyOnCfOrSoList}" headerKey="" headerValue="" /></td>
		</tr>
		<tr>		
		<td align="left" class="listwhitetext">Customer Survey on Customer Portal</td>
		<td align="left"><s:checkbox name="company.customerSurveyOnCustomerPortal" value="${company.customerSurveyOnCustomerPortal}" /></td>		
		</tr>			
		</table>
		</fieldset>
	</td>
</tr>

</tbody>
</table>
 </div>			 
<div class="bottom-header"><span></span></div>
</div>
</div>
  <table class="detailTabLabel" border="0" width="750px">
				<tbody>				
					<tr>
						<td align="right" class="listwhitetext"><b><fmt:message key='company.createdOn'/></b></td>
						<s:hidden name="company.createdOn" />
						<td><s:date name="company.createdOn" format="dd-MMM-yyyy HH:mm"/></td>		
						<td align="right" class="listwhitetext"><b><fmt:message key='company.createdBy' /></b></td>
						
						<td>
						<c:if test="${not empty company.id}">
							<s:label name="createdBy" value="%{company.createdBy}"/>
							<s:hidden name="company.createdBy"/>
						</c:if>
						<c:if test="${empty company.id}">
							<s:label name="createdBy" value="${pageContext.request.remoteUser}"/>
							<s:hidden name="company.createdBy" value="${pageContext.request.remoteUser}" />
						</c:if>
						</td>
						<td align="right" class="listwhitetext"><b><fmt:message key='company.updatedOn'/></b></td>
						<s:hidden name="company.updatedOn"/>
						<td><s:date name="company.updatedOn" format="dd-MMM-yyyy HH:mm"/></td>
						<td align="right" class="listwhitetext"><b><fmt:message key='company.updatedBy' /></b></td>
						<s:hidden name="company.updatedBy" value="${pageContext.request.remoteUser}" />
						<td><s:label name="customerFile.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table>  
 		</div> 
        <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" onclick="enableField();" key="button.save"/>
        <td></td>   
        <input class="cssbutton" style="width:55px; height:25px;" type="button" value="Reset" onclick="this.form.reset();enableDisableURLField();"/>
</s:form> 

       <script type="text/javascript">
function checkEmail() {
	
		var status = false;     
		var emailRegEx = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$\b/i;
		     if (document.forms['companyForm'].elements['company.email'].value.search(emailRegEx) == -1) {
		    	 document.forms['companyForm'].elements['company.email'].value="";
		          alert("Please enter a valid email address.");
		     }else {
		        status = true;
		     }
		     return status;
		}
      </script>  
	  <script type="text/javascript">
 function checkCcEmail() {
	
	var sEmail = false;     
	var ccemailRegEx = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$\b/i;
	     if (document.forms['companyForm'].elements['company.ccEmail'].value.search(ccemailRegEx) == -1) {
	    	 document.forms['companyForm'].elements['company.ccEmail'].value="";
	          alert("Please enter a valid Cc email address.");
	     }else {
	    	 sEmail = true;
	     }
	     return sEmail;
	} 
		
		
    </script>      
<script language="javascript" type="text/javascript">
function validateNum(target){
	var tempVal = target.value.trim();
	if(tempVal!=''){
		var num = Number(tempVal);
	    if(num==0){
	    	return;
	    }
	    if (num >= 1 && num < 100 ) {
	    var decnum	=num.toString().split('.')[1];
	    if(decnum.length>2){
	    	alert('invalid only 2 digit allowed after decimal');
	    	  target.value="";
	    return;
	    }
	    } else {
	         alert('Please enter number less than 100');
	         target.value="";
	    }
	}else{
		target.value="";
	}
	    
}
</script>
 <script language="javascript" type="text/javascript">
function validateNum1(target){
	var tempVal = target.value.trim();
	if(tempVal!=''){
		var num = Number(tempVal);
	    if(num==0){
	    	return;
	    }
	    if (num >= 1 && num < 10000 ) {
	    var decnum	=num.toString().split('.')[1];
	    if(decnum.length>2){
	    	  alert("'Invalid' \x0A 'Only 2 digit allowed after decimal'" );
	    	  target.value="";
	    return;
	    }
	    } else {
	         alert('Please enter number less than 10000');
	         target.value="";
	    }
	}else{
		target.value="";
	} 
}
 </script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
	var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns();
		function onlyNumericNew(targetElement)
		{   
		var i;
		    var s = targetElement.value;		    
		    for (i = 0; i < s.length; i++)
		    {   
		        var c = s.charAt(i);
		        if (((c < "0") || (c > "9"))) {
		        alert("Enter valid Number");
		        targetElement.value="";
		        return false;
		        }
		    }
		    return checkForLength(targetElement);
		}
		function checkForLength(targetElement){
			var s = targetElement.value;
			var no=0;
			no=parseInt(s);
			if(no<20000){
		        alert("Certificate Additional Number Less than 20000");
		        targetElement.value="";
		        return false;
			}
			return true;
		}
		 function enableDisableURLField(){
			 if(document.forms['companyForm'].elements['company.cportalAccessCompanyDivisionLevel'].checked){
				 	document.forms['companyForm'].elements['company.cportalBrandingURL'].disabled=true;
					document.forms['companyForm'].elements['company.cportalBrandingURL'].className = 'input-textUpper';
			 }else{
				 	document.forms['companyForm'].elements['company.cportalBrandingURL'].disabled=false;
					document.forms['companyForm'].elements['company.cportalBrandingURL'].className = 'input-text';				 
			 }
			}
		 function enableField(){
			 document.forms['companyForm'].elements['company.cportalBrandingURL'].disabled=false;
		 }
	</script>
	  
<script type="text/javascript">   
    Form.focusFirstElement($("companyForm"));   
</script>
<script type="text/javascript">
enableDisableURLField();
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
<script type="text/javascript">
function autoCheckOADAVal()
{
	var autoCheckFlag = document.forms['companyForm'].elements['company.subOADASoftValidation'].checked;
	if(autoCheckFlag==true)
		{
		document.forms['companyForm'].elements['company.oAdAWeightVolumeMandatoryValidation'].checked=true
		}
	}

function autoCheckVal()
 {
	var autoCheckFlag = document.forms['companyForm'].elements['company.oAdAWeightVolumeMandatoryValidation'].checked;
	if(autoCheckFlag==false)
		{
		document.forms['companyForm'].elements['company.subOADASoftValidation'].checked=false;
		}
	}
	
</script>