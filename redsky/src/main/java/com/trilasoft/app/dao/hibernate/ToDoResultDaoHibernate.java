package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.ToDoResultDao;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;


public class ToDoResultDaoHibernate extends GenericDaoHibernate<ToDoResult, Long> implements ToDoResultDao{
	
	private List toDoResults;
	public ToDoResultDaoHibernate() {   
	    super(ToDoResult.class);   
	    }
	public class SDTO{
		private Object owner;
		private  Object messagedisplayed;
		private Object durationAddSub;
		private Object note;
		private Object subject;
		private Object toDoRuleId;
		private Object notesKeyId;
		public Object getOwner() {
			return owner;
		}
		public void setOwner(Object owner) {
			this.owner = owner;
		}
		public Object getMessagedisplayed() {
			return messagedisplayed;
		}
		public void setMessagedisplayed(Object messagedisplayed) {
			this.messagedisplayed = messagedisplayed;
		}
		public Object getDurationAddSub() {
			return durationAddSub;
		}
		public void setDurationAddSub(Object durationAddSub) {
			this.durationAddSub = durationAddSub;
		}
		public Object getNote() {
			return note;
		}
		public void setNote(Object note) {
			this.note = note;
		}
		public Object getSubject() {
			return subject;
		}
		public void setSubject(Object subject) {
			this.subject = subject;
		}
		public Object getToDoRuleId() {
			return toDoRuleId;
		}
		public void setToDoRuleId(Object toDoRuleId) {
			this.toDoRuleId = toDoRuleId;
		}
		public Object getNotesKeyId() {
			return notesKeyId;
		}
		public void setNotesKeyId(Object notesKeyId) {
			this.notesKeyId = notesKeyId;
		}
	}
	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from ToDoResult");
	}

	public List getToDoResults() {
		return toDoResults;
	}

	public void setToDoResults(List toDoResults) {
		this.toDoResults = toDoResults;
	}

	public void updateToDoResult(String sessionCorpID) {
		//getHibernateTemplate().bulkUpdate("update Notes  set noteStatus = 'CMP' where  corpID='"+sessionCorpID+"' and noteSubType='ActivityManagement' and notesId not in (select fileNumber from ToDoResult where isNotesAdded = true)");
		String  notesId="";
		//return getHibernateTemplate().find("select billToCode,chargeCode,recGl  from AccountLine where billToCode = '"+invoiceBillToCode+"' and actualRevenue <> 0 and status=true and (recInvoiceNumber='' or recInvoiceNumber=0 or recInvoiceNumber is null) and shipNumber=?",shipNumber);
		List idList= this.getSession().createSQLQuery("select toDoRuleId,notesKeyId from notes where noteSubType='ActivityManagement' and noteStatus !='CMP' and corpID='"+sessionCorpID+"'").list(); 
		if(idList.size()>0)
		{
		Iterator it=idList.iterator();
		 while(it.hasNext())
	       {
	         Object []row= (Object [])it.next();
	         String  toDoRuleId= row[0].toString();
	         if(row[1]!=null){
	        	 notesId= row[1].toString();  
	         }else{
	        	 notesId=""; 
	         }
	         
		     int i=getHibernateTemplate().bulkUpdate("update ToDoResult  set isNotesAdded = true where corpID='"+sessionCorpID+"' and  fileNumber ='"+notesId+"' and todoRuleId='"+toDoRuleId+"'"); 	
		}  
	  }
	}
	//13674 - Create dashboard
	public List getTaskList(String filenumber,String sessionCorpID,Long id) {
		try{
		List list2 = new ArrayList();
		if(filenumber!=null ){
		 List sotaskList= this.getSession().createSQLQuery("select owner,messagedisplayed,durationAddSub,toDoRuleId from todoresult where corpID='"+sessionCorpID+"' and filenumber='"+filenumber+"' order by id desc limit 5").list();
		
		 Iterator it=sotaskList.iterator();
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
				SDTO dTO=new SDTO();
				dTO.setOwner(row[0]);
				dTO.setMessagedisplayed(row[1]);
				dTO.setDurationAddSub(row[2]);
				List noteList = this.getSession().createSQLQuery("select concat(subject,'~',note) from notes where  corpID='"+sessionCorpID+"' and notesid='"+filenumber+"'  and toDoRuleId='"+row[3]+"'  and notetype='Activity'  and noteSubType='ActivityManagement' order by id desc limit 5").list();
				if(noteList!=null && !noteList.isEmpty() && noteList.get(0)!=null){
					String s[] = noteList.get(0).toString().split("~");
					dTO.setSubject(s[0]);
					dTO.setNote(s[1]);
				}else{
					dTO.setSubject("");
					dTO.setNote("");
				}
				list2.add(dTO);
		       }
		}
		return list2;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null; 
	}	
	//End dashboard
}
