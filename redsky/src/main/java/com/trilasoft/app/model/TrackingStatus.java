/**
 * This class represents the basic "TrackingStatus" object in Redsky that allows for TrackingStatus of Shipment.
 * @Class Name	TrackingStatus
 * @Author      Sangeeta Dwivedi
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.model;

import java.util.Date;
import org.appfuse.model.BaseObject;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table; 
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "trackingstatus")
@FilterDef(name = "partnerAccessCorpIdsFilter", parameters = { @ParamDef(name = "forCorpID", type = "string")})
@Filter(name = "partnerAccessCorpIdsFilter", condition = "corpID in (:forCorpID)")
public class TrackingStatus extends BaseObject implements ToDoRuleRecord {
	
	private String corpID;
	private Long id;
	private String sequenceNumber;
	private String shipNumber;
	private Date activate;
	private Date quoteOn;
	private Date beginPacking;
	private Date endPacking;
	private Date beginLoad;
	private Date endLoad;
	private String sitOriginYN;
	private int sitOriginDays;
	private Date sitOriginOn;
	private String sitOriginTA;
	private Date packDone;
	private Date paymentRecived;
	private String managerOkInital;
	private Date managerOkOn;
	private Date instruction2FF;
	private Date leftWHOn;
	private String sitDestinationYN;
	private int sitDestinationDays;
	private Date sitDestinationIn;
	private String sitDestinationTA;
	private Date deliveryShipper;
	private Date omniReport;
	private Date orderDate;
	private Date vehicleRecived;
	private String freeEntry3299;
	private Date mail3299;
	private Date recived3299;
	private Date sent3299ToBroker;
	private Date originBL;
	private Date clearCustom;
	private Date clearCustomOrigin;
	private Date brokerDeliveryOrder;
	private Date bookerDeliveryNotification;
	private Date pickUp2PierWH;
	private Date pickUp2PierWHDestination;
	private String pickUp2PierWHTA;
	private String pickUp2PierWHTADestination;
	private String clearCustomTA;;
	private Date deliveryLastDay;
	private Date preferedLoadDate;
	private Date preferedPackDate;
	private Date recivedGBL;
	private Date requestGBL;
	private Date estimate2Account;
	private Date sitOriginLetter;
	private Date sitDestinationLetter;
	private String sitOriginLetterTA;
	private String sitDestinationLetterTA;
	private Date mailEvaluation;
	private Date rating;
	private Date recivedEvaluation;
	private Date recivedPayment;
	private int fromWH;
	private String gbl;
	private String billLading;
	private Date deliveryTA;
	private Date preliminaryNotification;
	private Date finalNotification;
	private String itNumber;
	private String itNumber2;
	private String flagCarrier;
	private String returnContainer;
	private String returnContainerDestination;
	private Date containerRecivedWH;
	private Date reconfirmShipper;
	private Date confirmOriginAgent;
	private Date packDayConfirmCall;
	private Date loadFollowUpCall;
	private Date deliveryFollowCall;
	private Date deliveryReceiptDate;
	private Date mailClaim;
	private String returnLocationOrigin;
	private String sitAuthorizeOrigin;
	private Date lastFree;
	private Date lastFreeDestination;
	/* Added By Kunal for ticket number: 6360 */
	private Date documentationCutOffDate;
	private Date passportReceived;
	private Date einReceived;
	/* Modification closed here */
	private String demurrageYN;
	private String demurragePayment;

	private double demurrageCostPerDay1;

	private String perdiumYN;

	private String perdiumPayment;

	private double perdiumCostPerDay;
	private double xrayCost;

	private String inspectorPaymentTrack;

	private double inspectorCost;

	private String inspectorYN;

	private String xrayYN;
	private double usdaCost;
	private double demurrageCostPerDay2;
	private Date survey;
	private Date packA;
	private Date loadA;
	private Date packRoomA;
	private Date sitOriginA;
	private Date returnContainerDateA;

	private Date returnContainerDateADestination;
	private Date sitDestinationA;

	private Date deliveryA;

	private Date sentcomp;

	private String booker;

	private String originAgent;

	private String destinationAgent;

	private String originAgentCode;

	private String destinationAgentCode;
	private String sitAuthorizeDestination;
	private Date statusDate;

	private String status;

	private Date documentSentClient;

	private Date documentReceivedClient;

	private Date documentReceivedFromOA;

	private Date deliveryReceiptSentOA;

	private Date documentCHA;
	private Date cutOffDate;

	private String cutOffDateTA;

	private Date initialContact;
	private String brokerCode;

	private String brokerName;

	private String actgCode;

	private String actgCodeForDis;

	private String originSubAgent;

	private String destinationSubAgent;

	private String originSubAgentCode;

	private String destinationSubAgentCode;

	private String surveyTimeFrom;

	private String surveyTimeTo;

	private Date surveyDate;

	private String createdBy;

	private Date createdOn;

	private String updatedBy;

	private Date updatedOn;

	private String originAgentContact;

	private String originAgentEmail;

	private String destinationAgentContact;

	private String destinationAgentEmail;

	private String subOriginAgentContact;

	private String subOriginAgentEmail;

	private String subDestinationAgentAgentContact;

	private String subDestinationAgentEmail;

	private String brokerContact;

	private String brokerEmail;

	private String bookingAgentContact;

	private String bookingAgentEmail;

	private int sitOriginDaysAllowed;

	private int sitDestinationDaysAllowed;

	private String forwarder;

	private String forwarderCode;

	private String forwarderContact;

	private String forwarderEmail;

	private Boolean gsaiffFundingFlag;

	private String originAgentVanlineCode;

	private String destinationAgentVanlineCode;
	
	private Date requiredDeliveryDate;
	//ISFSubmitted
	private Date ISFSubmitted;
	private String daShipmentNum;
	private Date SITNotify;
	//	 Mapping Variables
	private ServiceOrder serviceOrder;//One To One RelationShip With ServiceOrder

	private Miscellaneous miscellaneous;//One To One RelationShip With Miscellaneous
	
	private Date requestOriginSit;
	private Date receivedOriginSit;
	private Date requestDestinationSit;
	private Date receivedDestinationSit;
	private String destinationSITReason;
	private String originSITReason;
	private String cutOffTimeTo;
	
	private String networkPartnerCode;
	private String networkContact;
	private String networkEmail;
	private String networkPartnerName;
	
	private String networkAgentExSO;
	private String originAgentExSO;
	private String originSubAgentExSO;
	private String destinationAgentExSO;
	private String destinationSubAgentExSO; 
	private Date sentToKSD;
	private Integer requiredDeliveryDays;
    private String lateReason;
    private String partyResponsible;
    private Boolean soNetworkGroup = new Boolean(false);
    private Boolean accNetworkGroup = new Boolean(false);
    private Boolean utsiNetworkGroup = new Boolean(false);
    private Boolean agentNetworkGroup = new Boolean(false);
    private String bookingAgentExSO;
    private String contractType=new String ("");
    private String ISFConfirmationNo;
    private String 	ugwIntId;    
    private Date serviceCompleteDate;
    private Date contractReceived;
    private Date confirmationOfDelievery;
    private Date podToBooker;
    private Date missedRDDNotification;
    private String reason;
    private String docCutOffTimeTo;
    private String bookingAgentPhoneNumber;
    private String networkPhoneNumber;
    private String originAgentPhoneNumber;
    private String destinationAgentPhoneNumber;
    private String subOriginAgentPhoneNumber;
    private String subDestinationAgentPhoneNumber;
    private String tmss;
    private String originGivenCode;
    private String originGivenName;
    private String originReceivedCode;
    private String originReceivedName;
    private String destinationGivenCode;
    private String destinationGivenName;
    private String destinationReceivedCode;
    private String destinationReceivedName;
    private Date actualPackBegin;
    private Date actualLoadBegin;
    private Date actualDeliveryBegin;
    private Date targetPackEnding;
    private Date targetLoadEnding;
    private Date targetdeliveryShipper;
    private String proNumber;
    private Date packingWeightOn;
    private Date confirmationOn;
    private String sitDestinationInCondition;
    private String sitOriginOnCondition;
    private String originLocationType;
    private String destinationLocationType;
    private Date packConfirmCallPriorOneDay;
    private Date packConfirmCallPriorThreeDay;
    private Date customClearance;
    private Date crewArrivalDate;
    private Date endCustomClearance;
    private Date ffwRequested;
    private Date ffwReceived;
    private Date ediDate;
    private String hbillLading;
	private String PackingConfirmationSent;






@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TrackingStatus))
			return false;
		TrackingStatus castOther = (TrackingStatus) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(shipNumber, castOther.shipNumber)
				.append(activate, castOther.activate)
				.append(quoteOn, castOther.quoteOn)
				.append(beginPacking, castOther.beginPacking)
				.append(endPacking, castOther.endPacking)
				.append(beginLoad, castOther.beginLoad)
				.append(endLoad, castOther.endLoad)
				.append(sitOriginYN, castOther.sitOriginYN)
				.append(sitOriginDays, castOther.sitOriginDays)
				.append(sitOriginOn, castOther.sitOriginOn)
				.append(sitOriginTA, castOther.sitOriginTA)
				.append(packDone, castOther.packDone)
				.append(paymentRecived, castOther.paymentRecived)
				.append(managerOkInital, castOther.managerOkInital)
				.append(managerOkOn, castOther.managerOkOn)
				.append(instruction2FF, castOther.instruction2FF)
				.append(leftWHOn, castOther.leftWHOn)
				.append(sitDestinationYN, castOther.sitDestinationYN)
				.append(sitDestinationDays, castOther.sitDestinationDays)
				.append(sitDestinationIn, castOther.sitDestinationIn)
				.append(sitDestinationTA, castOther.sitDestinationTA)
				.append(deliveryShipper, castOther.deliveryShipper)
				.append(omniReport, castOther.omniReport)
				.append(orderDate, castOther.orderDate)
				.append(vehicleRecived, castOther.vehicleRecived)
				.append(freeEntry3299, castOther.freeEntry3299)
				.append(mail3299, castOther.mail3299)
				.append(recived3299, castOther.recived3299)
				.append(sent3299ToBroker, castOther.sent3299ToBroker)
				.append(originBL, castOther.originBL)
				.append(clearCustom, castOther.clearCustom)
				.append(clearCustomOrigin, castOther.clearCustomOrigin)
				.append(brokerDeliveryOrder, castOther.brokerDeliveryOrder)
				.append(bookerDeliveryNotification,
						castOther.bookerDeliveryNotification)
				.append(pickUp2PierWH, castOther.pickUp2PierWH)
				.append(pickUp2PierWHDestination,
						castOther.pickUp2PierWHDestination)
				.append(pickUp2PierWHTA, castOther.pickUp2PierWHTA)
				.append(pickUp2PierWHTADestination,
						castOther.pickUp2PierWHTADestination)
				.append(clearCustomTA, castOther.clearCustomTA)
				.append(deliveryLastDay, castOther.deliveryLastDay)
				.append(preferedLoadDate, castOther.preferedLoadDate)
				.append(preferedPackDate, castOther.preferedPackDate)
				.append(recivedGBL, castOther.recivedGBL)
				.append(requestGBL, castOther.requestGBL)
				.append(estimate2Account, castOther.estimate2Account)
				.append(sitOriginLetter, castOther.sitOriginLetter)
				.append(sitDestinationLetter, castOther.sitDestinationLetter)
				.append(sitOriginLetterTA, castOther.sitOriginLetterTA)
				.append(sitDestinationLetterTA,
						castOther.sitDestinationLetterTA)
				.append(mailEvaluation, castOther.mailEvaluation)
				.append(rating, castOther.rating)
				.append(recivedEvaluation, castOther.recivedEvaluation)
				.append(recivedPayment, castOther.recivedPayment)
				.append(fromWH, castOther.fromWH)
				.append(gbl, castOther.gbl)
				.append(billLading, castOther.billLading)
				.append(deliveryTA, castOther.deliveryTA)
				.append(preliminaryNotification,
						castOther.preliminaryNotification)
				.append(finalNotification, castOther.finalNotification)
				.append(itNumber, castOther.itNumber)
				.append(itNumber2, castOther.itNumber2)
				.append(flagCarrier, castOther.flagCarrier)
				.append(returnContainer, castOther.returnContainer)
				.append(returnContainerDestination,
						castOther.returnContainerDestination)
				.append(containerRecivedWH, castOther.containerRecivedWH)
				.append(reconfirmShipper, castOther.reconfirmShipper)
				.append(confirmOriginAgent, castOther.confirmOriginAgent)
				.append(packDayConfirmCall, castOther.packDayConfirmCall)
				.append(loadFollowUpCall, castOther.loadFollowUpCall)
				.append(deliveryFollowCall, castOther.deliveryFollowCall)
				.append(deliveryReceiptDate, castOther.deliveryReceiptDate)
				.append(mailClaim, castOther.mailClaim)
				.append(returnLocationOrigin, castOther.returnLocationOrigin)
				.append(sitAuthorizeOrigin, castOther.sitAuthorizeOrigin)
				.append(lastFree, castOther.lastFree)
				.append(lastFreeDestination, castOther.lastFreeDestination)
				.append(documentationCutOffDate,
						castOther.documentationCutOffDate)
				.append(passportReceived, castOther.passportReceived)
				.append(einReceived, castOther.einReceived)
				.append(demurrageYN, castOther.demurrageYN)
				.append(demurragePayment, castOther.demurragePayment)
				.append(demurrageCostPerDay1, castOther.demurrageCostPerDay1)
				.append(perdiumYN, castOther.perdiumYN)
				.append(perdiumPayment, castOther.perdiumPayment)
				.append(perdiumCostPerDay, castOther.perdiumCostPerDay)
				.append(xrayCost, castOther.xrayCost)
				.append(inspectorPaymentTrack, castOther.inspectorPaymentTrack)
				.append(inspectorCost, castOther.inspectorCost)
				.append(inspectorYN, castOther.inspectorYN)
				.append(xrayYN, castOther.xrayYN)
				.append(usdaCost, castOther.usdaCost)
				.append(demurrageCostPerDay2, castOther.demurrageCostPerDay2)
				.append(survey, castOther.survey)
				.append(packA, castOther.packA)
				.append(loadA, castOther.loadA)
				.append(packRoomA, castOther.packRoomA)
				.append(sitOriginA, castOther.sitOriginA)
				.append(returnContainerDateA, castOther.returnContainerDateA)
				.append(returnContainerDateADestination,
						castOther.returnContainerDateADestination)
				.append(sitDestinationA, castOther.sitDestinationA)
				.append(deliveryA, castOther.deliveryA)
				.append(sentcomp, castOther.sentcomp)
				.append(booker, castOther.booker)
				.append(originAgent, castOther.originAgent)
				.append(destinationAgent, castOther.destinationAgent)
				.append(originAgentCode, castOther.originAgentCode)
				.append(destinationAgentCode, castOther.destinationAgentCode)
				.append(sitAuthorizeDestination,
						castOther.sitAuthorizeDestination)
				.append(statusDate, castOther.statusDate)
				.append(status, castOther.status)
				.append(documentSentClient, castOther.documentSentClient)
				.append(documentReceivedClient,
						castOther.documentReceivedClient)
				.append(documentReceivedFromOA,
						castOther.documentReceivedFromOA)
				.append(deliveryReceiptSentOA, castOther.deliveryReceiptSentOA)
				.append(documentCHA, castOther.documentCHA)
				.append(cutOffDate, castOther.cutOffDate)
				.append(cutOffDateTA, castOther.cutOffDateTA)
				.append(initialContact, castOther.initialContact)
				.append(brokerCode, castOther.brokerCode)
				.append(brokerName, castOther.brokerName)
				.append(actgCode, castOther.actgCode)
				.append(actgCodeForDis, castOther.actgCodeForDis)
				.append(originSubAgent, castOther.originSubAgent)
				.append(destinationSubAgent, castOther.destinationSubAgent)
				.append(originSubAgentCode, castOther.originSubAgentCode)
				.append(destinationSubAgentCode,
						castOther.destinationSubAgentCode)
				.append(surveyTimeFrom, castOther.surveyTimeFrom)
				.append(surveyTimeTo, castOther.surveyTimeTo)
				.append(surveyDate, castOther.surveyDate)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(originAgentContact, castOther.originAgentContact)
				.append(originAgentEmail, castOther.originAgentEmail)
				.append(destinationAgentContact,
						castOther.destinationAgentContact)
				.append(destinationAgentEmail, castOther.destinationAgentEmail)
				.append(subOriginAgentContact, castOther.subOriginAgentContact)
				.append(subOriginAgentEmail, castOther.subOriginAgentEmail)
				.append(subDestinationAgentAgentContact,
						castOther.subDestinationAgentAgentContact)
				.append(subDestinationAgentEmail,
						castOther.subDestinationAgentEmail)
				.append(brokerContact, castOther.brokerContact)
				.append(brokerEmail, castOther.brokerEmail)
				.append(bookingAgentContact, castOther.bookingAgentContact)
				.append(bookingAgentEmail, castOther.bookingAgentEmail)
				.append(sitOriginDaysAllowed, castOther.sitOriginDaysAllowed)
				.append(sitDestinationDaysAllowed,
						castOther.sitDestinationDaysAllowed)
				.append(forwarder, castOther.forwarder)
				.append(forwarderCode, castOther.forwarderCode)
				.append(forwarderContact, castOther.forwarderContact)
				.append(forwarderEmail, castOther.forwarderEmail)
				.append(gsaiffFundingFlag, castOther.gsaiffFundingFlag)
				.append(originAgentVanlineCode,
						castOther.originAgentVanlineCode)
				.append(destinationAgentVanlineCode,
						castOther.destinationAgentVanlineCode)
				.append(requiredDeliveryDate, castOther.requiredDeliveryDate)
				.append(ISFSubmitted, castOther.ISFSubmitted)
				.append(daShipmentNum, castOther.daShipmentNum)
				.append(SITNotify, castOther.SITNotify) 
				.append(requestOriginSit, castOther.requestOriginSit)
				.append(receivedOriginSit, castOther.receivedOriginSit)
				.append(requestDestinationSit, castOther.requestDestinationSit)
				.append(receivedDestinationSit,
						castOther.receivedDestinationSit)
				.append(destinationSITReason, castOther.destinationSITReason)
				.append(originSITReason, castOther.originSITReason)
				.append(cutOffTimeTo, castOther.cutOffTimeTo)
				.append(networkPartnerCode, castOther.networkPartnerCode)
				.append(networkContact, castOther.networkContact)
				.append(networkEmail, castOther.networkEmail)
				.append(networkPartnerName, castOther.networkPartnerName)
				.append(networkAgentExSO, castOther.networkAgentExSO)
				.append(originAgentExSO, castOther.originAgentExSO)
				.append(originSubAgentExSO, castOther.originSubAgentExSO)
				.append(destinationAgentExSO, castOther.destinationAgentExSO)
				.append(destinationSubAgentExSO,
						castOther.destinationSubAgentExSO)
				.append(sentToKSD, castOther.sentToKSD)
				.append(requiredDeliveryDays, castOther.requiredDeliveryDays)
				.append(lateReason, castOther.lateReason)
				.append(partyResponsible, castOther.partyResponsible)
				.append(soNetworkGroup, castOther.soNetworkGroup)
				.append(accNetworkGroup, castOther.accNetworkGroup)
				.append(utsiNetworkGroup, castOther.utsiNetworkGroup)
				.append(agentNetworkGroup, castOther.agentNetworkGroup)
				.append(bookingAgentExSO, castOther.bookingAgentExSO)
				.append(contractType, castOther.contractType)
				.append(ISFConfirmationNo, castOther.ISFConfirmationNo)
				.append(ugwIntId, castOther.ugwIntId)
				.append(serviceCompleteDate, castOther.serviceCompleteDate)
				.append(contractReceived, castOther.contractReceived)
				.append(confirmationOfDelievery,
						castOther.confirmationOfDelievery)
				.append(podToBooker, castOther.podToBooker)
				.append(missedRDDNotification, castOther.missedRDDNotification)
				.append(reason, castOther.reason)
				.append(docCutOffTimeTo, castOther.docCutOffTimeTo)
				.append(bookingAgentPhoneNumber, castOther.bookingAgentPhoneNumber)
				.append(networkPhoneNumber, castOther.networkPhoneNumber)
				.append(destinationAgentPhoneNumber, castOther.destinationAgentPhoneNumber)
				.append(originAgentPhoneNumber, castOther.originAgentPhoneNumber)
				.append(subOriginAgentPhoneNumber, castOther.subOriginAgentPhoneNumber)
				.append(subDestinationAgentPhoneNumber, castOther.subDestinationAgentPhoneNumber)
				.append(tmss, castOther.tmss)
				.append(originGivenCode, castOther.originGivenCode)
				.append(originGivenName, castOther.originGivenName)
				.append(originReceivedCode, castOther.originReceivedCode)
				.append(originReceivedName, castOther.originReceivedName)
				.append(destinationGivenCode, castOther.destinationGivenCode)
				.append(destinationGivenName, castOther.destinationGivenName)
				.append(destinationReceivedCode, castOther.destinationReceivedCode)
				.append(destinationReceivedName, castOther.destinationReceivedName)
				.append(actualPackBegin, castOther.actualPackBegin)
				.append(actualLoadBegin, castOther.actualLoadBegin)
				.append(actualDeliveryBegin, castOther.actualDeliveryBegin)
				.append(targetPackEnding, castOther.targetPackEnding)
				.append(targetLoadEnding, castOther.targetLoadEnding)
				.append(targetdeliveryShipper, castOther.targetdeliveryShipper)
				.append(proNumber, castOther.proNumber)
				.append(packingWeightOn, castOther.packingWeightOn)
				.append(confirmationOn, castOther.confirmationOn)
				.append(sitDestinationInCondition, castOther.sitDestinationInCondition)
				.append(sitOriginOnCondition, castOther.sitOriginOnCondition)
				.append(originLocationType, castOther.originLocationType)
				.append(destinationLocationType, castOther.destinationLocationType)
				.append(packConfirmCallPriorOneDay, castOther.packConfirmCallPriorOneDay)
				.append(packConfirmCallPriorThreeDay, castOther.packConfirmCallPriorThreeDay)
				.append(customClearance, castOther.customClearance)
				.append(crewArrivalDate, castOther.crewArrivalDate)
				.append(endCustomClearance, castOther.endCustomClearance)
				.append(ffwRequested, castOther.ffwRequested)
				.append(ffwReceived, castOther.ffwReceived).append(ediDate,castOther.ediDate)
				.append(hbillLading,castOther.hbillLading)
				.append(PackingConfirmationSent,castOther.PackingConfirmationSent)
				.isEquals();
	}

	

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(sequenceNumber).append(shipNumber).append(activate)
				.append(quoteOn).append(beginPacking).append(endPacking)
				.append(beginLoad).append(endLoad).append(sitOriginYN)
				.append(sitOriginDays).append(sitOriginOn).append(sitOriginTA)
				.append(packDone).append(paymentRecived)
				.append(managerOkInital).append(managerOkOn)
				.append(instruction2FF).append(leftWHOn)
				.append(sitDestinationYN).append(sitDestinationDays)
				.append(sitDestinationIn).append(sitDestinationTA)
				.append(deliveryShipper).append(omniReport).append(orderDate)
				.append(vehicleRecived).append(freeEntry3299).append(mail3299)
				.append(recived3299).append(sent3299ToBroker).append(originBL)
				.append(clearCustom).append(clearCustomOrigin)
				.append(brokerDeliveryOrder).append(bookerDeliveryNotification)
				.append(pickUp2PierWH).append(pickUp2PierWHDestination)
				.append(pickUp2PierWHTA).append(pickUp2PierWHTADestination)
				.append(clearCustomTA).append(deliveryLastDay)
				.append(preferedLoadDate).append(preferedPackDate)
				.append(recivedGBL).append(requestGBL).append(estimate2Account)
				.append(sitOriginLetter).append(sitDestinationLetter)
				.append(sitOriginLetterTA).append(sitDestinationLetterTA)
				.append(mailEvaluation).append(rating)
				.append(recivedEvaluation).append(recivedPayment)
				.append(fromWH).append(gbl).append(billLading)
				.append(deliveryTA).append(preliminaryNotification)
				.append(finalNotification).append(itNumber).append(itNumber2)
				.append(flagCarrier).append(returnContainer)
				.append(returnContainerDestination).append(containerRecivedWH)
				.append(reconfirmShipper).append(confirmOriginAgent)
				.append(packDayConfirmCall).append(loadFollowUpCall)
				.append(deliveryFollowCall).append(deliveryReceiptDate)
				.append(mailClaim).append(returnLocationOrigin)
				.append(sitAuthorizeOrigin).append(lastFree)
				.append(lastFreeDestination).append(documentationCutOffDate)
				.append(passportReceived).append(einReceived)
				.append(demurrageYN).append(demurragePayment)
				.append(demurrageCostPerDay1).append(perdiumYN)
				.append(perdiumPayment).append(perdiumCostPerDay)
				.append(xrayCost).append(inspectorPaymentTrack)
				.append(inspectorCost).append(inspectorYN).append(xrayYN)
				.append(usdaCost).append(demurrageCostPerDay2).append(survey)
				.append(packA).append(loadA).append(packRoomA)
				.append(sitOriginA).append(returnContainerDateA)
				.append(returnContainerDateADestination)
				.append(sitDestinationA).append(deliveryA).append(sentcomp)
				.append(booker).append(originAgent).append(destinationAgent)
				.append(originAgentCode).append(destinationAgentCode)
				.append(sitAuthorizeDestination).append(statusDate)
				.append(status).append(documentSentClient)
				.append(documentReceivedClient).append(documentReceivedFromOA)
				.append(deliveryReceiptSentOA).append(documentCHA)
				.append(cutOffDate).append(cutOffDateTA).append(initialContact)
				.append(brokerCode).append(brokerName).append(actgCode)
				.append(actgCodeForDis).append(originSubAgent)
				.append(destinationSubAgent).append(originSubAgentCode)
				.append(destinationSubAgentCode).append(surveyTimeFrom)
				.append(surveyTimeTo).append(surveyDate).append(createdBy)
				.append(createdOn).append(updatedBy).append(updatedOn)
				.append(originAgentContact).append(originAgentEmail)
				.append(destinationAgentContact).append(destinationAgentEmail)
				.append(subOriginAgentContact).append(subOriginAgentEmail)
				.append(subDestinationAgentAgentContact)
				.append(subDestinationAgentEmail).append(brokerContact)
				.append(brokerEmail).append(bookingAgentContact)
				.append(bookingAgentEmail).append(sitOriginDaysAllowed)
				.append(sitDestinationDaysAllowed).append(forwarder)
				.append(forwarderCode).append(forwarderContact)
				.append(forwarderEmail).append(gsaiffFundingFlag)
				.append(originAgentVanlineCode)
				.append(destinationAgentVanlineCode)
				.append(requiredDeliveryDate).append(ISFSubmitted)
				.append(daShipmentNum).append(SITNotify)
				.append(requestOriginSit)
				.append(receivedOriginSit).append(requestDestinationSit)
				.append(receivedDestinationSit).append(destinationSITReason)
				.append(originSITReason).append(cutOffTimeTo)
				.append(networkPartnerCode).append(networkContact)
				.append(networkEmail).append(networkPartnerName)
				.append(networkAgentExSO).append(originAgentExSO)
				.append(originSubAgentExSO).append(destinationAgentExSO)
				.append(destinationSubAgentExSO).append(sentToKSD)
				.append(requiredDeliveryDays).append(lateReason)
				.append(partyResponsible).append(soNetworkGroup)
				.append(accNetworkGroup).append(utsiNetworkGroup)
				.append(agentNetworkGroup).append(bookingAgentExSO)
				.append(contractType).append(ISFConfirmationNo)
				.append(ugwIntId).append(serviceCompleteDate)
				.append(contractReceived).append(confirmationOfDelievery)
				.append(podToBooker).append(missedRDDNotification)
				.append(reason).append(docCutOffTimeTo)
				.append(bookingAgentPhoneNumber).append(networkPhoneNumber)
				.append(originAgentPhoneNumber).append(destinationAgentPhoneNumber)
				.append(subOriginAgentPhoneNumber).append(subDestinationAgentPhoneNumber)
				.append(tmss)
				.append(originGivenCode)
				.append(originGivenName)
				.append(originReceivedCode)
				.append(originReceivedName)
				.append(destinationGivenCode)
				.append(destinationGivenName)
				.append(destinationReceivedCode)
				.append(destinationReceivedName)
				.append(actualPackBegin)
				.append(actualLoadBegin)
				.append(actualDeliveryBegin)
				.append(targetPackEnding)
				.append(targetLoadEnding)
				.append(targetdeliveryShipper)
				.append(proNumber)
				.append(packingWeightOn)
				.append(confirmationOn)
				.append(sitDestinationInCondition)
				.append(sitOriginOnCondition)
				.append(originLocationType)
				.append(destinationLocationType)
				.append(packConfirmCallPriorOneDay)
				.append(packConfirmCallPriorThreeDay)
				.append(customClearance)
				.append(crewArrivalDate)
				.append(endCustomClearance)
				.append(ffwRequested)
				.append(ffwReceived).append(ediDate)
				.append(hbillLading)
				.append(PackingConfirmationSent)
				.toHashCode();
	}



	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("id", id)
				.append("sequenceNumber", sequenceNumber)
				.append("shipNumber", shipNumber)
				.append("activate", activate)
				.append("quoteOn", quoteOn)
				.append("beginPacking", beginPacking)
				.append("endPacking", endPacking)
				.append("beginLoad", beginLoad)
				.append("endLoad", endLoad)
				.append("sitOriginYN", sitOriginYN)
				.append("sitOriginDays", sitOriginDays)
				.append("sitOriginOn", sitOriginOn)
				.append("sitOriginTA", sitOriginTA)
				.append("packDone", packDone)
				.append("paymentRecived", paymentRecived)
				.append("managerOkInital", managerOkInital)
				.append("managerOkOn", managerOkOn)
				.append("instruction2FF", instruction2FF)
				.append("leftWHOn", leftWHOn)
				.append("sitDestinationYN", sitDestinationYN)
				.append("sitDestinationDays", sitDestinationDays)
				.append("sitDestinationIn", sitDestinationIn)
				.append("sitDestinationTA", sitDestinationTA)
				.append("deliveryShipper", deliveryShipper)
				.append("omniReport", omniReport)
				.append("orderDate", orderDate)
				.append("vehicleRecived", vehicleRecived)
				.append("freeEntry3299", freeEntry3299)
				.append("mail3299", mail3299)
				.append("recived3299", recived3299)
				.append("sent3299ToBroker", sent3299ToBroker)
				.append("originBL", originBL)
				.append("clearCustom", clearCustom)
				.append("clearCustomOrigin", clearCustomOrigin)
				.append("brokerDeliveryOrder", brokerDeliveryOrder)
				.append("bookerDeliveryNotification",
						bookerDeliveryNotification)
				.append("pickUp2PierWH", pickUp2PierWH)
				.append("pickUp2PierWHDestination", pickUp2PierWHDestination)
				.append("pickUp2PierWHTA", pickUp2PierWHTA)
				.append("pickUp2PierWHTADestination",
						pickUp2PierWHTADestination)
				.append("clearCustomTA", clearCustomTA)
				.append("deliveryLastDay", deliveryLastDay)
				.append("preferedLoadDate", preferedLoadDate)
				.append("preferedPackDate", preferedPackDate)
				.append("recivedGBL", recivedGBL)
				.append("requestGBL", requestGBL)
				.append("estimate2Account", estimate2Account)
				.append("sitOriginLetter", sitOriginLetter)
				.append("sitDestinationLetter", sitDestinationLetter)
				.append("sitOriginLetterTA", sitOriginLetterTA)
				.append("sitDestinationLetterTA", sitDestinationLetterTA)
				.append("mailEvaluation", mailEvaluation)
				.append("rating", rating)
				.append("recivedEvaluation", recivedEvaluation)
				.append("recivedPayment", recivedPayment)
				.append("fromWH", fromWH)
				.append("gbl", gbl)
				.append("billLading", billLading)
				.append("deliveryTA", deliveryTA)
				.append("preliminaryNotification", preliminaryNotification)
				.append("finalNotification", finalNotification)
				.append("itNumber", itNumber)
				.append("itNumber2", itNumber2)
				.append("flagCarrier", flagCarrier)
				.append("returnContainer", returnContainer)
				.append("returnContainerDestination",
						returnContainerDestination)
				.append("containerRecivedWH", containerRecivedWH)
				.append("reconfirmShipper", reconfirmShipper)
				.append("confirmOriginAgent", confirmOriginAgent)
				.append("packDayConfirmCall", packDayConfirmCall)
				.append("loadFollowUpCall", loadFollowUpCall)
				.append("deliveryFollowCall", deliveryFollowCall)
				.append("deliveryReceiptDate", deliveryReceiptDate)
				.append("mailClaim", mailClaim)
				.append("returnLocationOrigin", returnLocationOrigin)
				.append("sitAuthorizeOrigin", sitAuthorizeOrigin)
				.append("lastFree", lastFree)
				.append("lastFreeDestination", lastFreeDestination)
				.append("documentationCutOffDate", documentationCutOffDate)
				.append("passportReceived", passportReceived)
				.append("einReceived", einReceived)
				.append("demurrageYN", demurrageYN)
				.append("demurragePayment", demurragePayment)
				.append("demurrageCostPerDay1", demurrageCostPerDay1)
				.append("perdiumYN", perdiumYN)
				.append("perdiumPayment", perdiumPayment)
				.append("perdiumCostPerDay", perdiumCostPerDay)
				.append("xrayCost", xrayCost)
				.append("inspectorPaymentTrack", inspectorPaymentTrack)
				.append("inspectorCost", inspectorCost)
				.append("inspectorYN", inspectorYN)
				.append("xrayYN", xrayYN)
				.append("usdaCost", usdaCost)
				.append("demurrageCostPerDay2", demurrageCostPerDay2)
				.append("survey", survey)
				.append("packA", packA)
				.append("loadA", loadA)
				.append("packRoomA", packRoomA)
				.append("sitOriginA", sitOriginA)
				.append("returnContainerDateA", returnContainerDateA)
				.append("returnContainerDateADestination",
						returnContainerDateADestination)
				.append("sitDestinationA", sitDestinationA)
				.append("deliveryA", deliveryA)
				.append("sentcomp", sentcomp)
				.append("booker", booker)
				.append("originAgent", originAgent)
				.append("destinationAgent", destinationAgent)
				.append("originAgentCode", originAgentCode)
				.append("destinationAgentCode", destinationAgentCode)
				.append("sitAuthorizeDestination", sitAuthorizeDestination)
				.append("statusDate", statusDate)
				.append("status", status)
				.append("documentSentClient", documentSentClient)
				.append("documentReceivedClient", documentReceivedClient)
				.append("documentReceivedFromOA", documentReceivedFromOA)
				.append("deliveryReceiptSentOA", deliveryReceiptSentOA)
				.append("documentCHA", documentCHA)
				.append("cutOffDate", cutOffDate)
				.append("cutOffDateTA", cutOffDateTA)
				.append("initialContact", initialContact)
				.append("brokerCode", brokerCode)
				.append("brokerName", brokerName)
				.append("actgCode", actgCode)
				.append("actgCodeForDis", actgCodeForDis)
				.append("originSubAgent", originSubAgent)
				.append("destinationSubAgent", destinationSubAgent)
				.append("originSubAgentCode", originSubAgentCode)
				.append("destinationSubAgentCode", destinationSubAgentCode)
				.append("surveyTimeFrom", surveyTimeFrom)
				.append("surveyTimeTo", surveyTimeTo)
				.append("surveyDate", surveyDate)
				.append("createdBy", createdBy)
				.append("createdOn", createdOn)
				.append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("originAgentContact", originAgentContact)
				.append("originAgentEmail", originAgentEmail)
				.append("destinationAgentContact", destinationAgentContact)
				.append("destinationAgentEmail", destinationAgentEmail)
				.append("subOriginAgentContact", subOriginAgentContact)
				.append("subOriginAgentEmail", subOriginAgentEmail)
				.append("subDestinationAgentAgentContact",
						subDestinationAgentAgentContact)
				.append("subDestinationAgentEmail", subDestinationAgentEmail)
				.append("brokerContact", brokerContact)
				.append("brokerEmail", brokerEmail)
				.append("bookingAgentContact", bookingAgentContact)
				.append("bookingAgentEmail", bookingAgentEmail)
				.append("sitOriginDaysAllowed", sitOriginDaysAllowed)
				.append("sitDestinationDaysAllowed", sitDestinationDaysAllowed)
				.append("forwarder", forwarder)
				.append("forwarderCode", forwarderCode)
				.append("forwarderContact", forwarderContact)
				.append("forwarderEmail", forwarderEmail)
				.append("gsaiffFundingFlag", gsaiffFundingFlag)
				.append("originAgentVanlineCode", originAgentVanlineCode)
				.append("destinationAgentVanlineCode",
						destinationAgentVanlineCode)
				.append("requiredDeliveryDate", requiredDeliveryDate)
				.append("ISFSubmitted", ISFSubmitted)
				.append("daShipmentNum", daShipmentNum)
				.append("SITNotify", SITNotify)  
				.append("requestOriginSit", requestOriginSit)
				.append("receivedOriginSit", receivedOriginSit)
				.append("requestDestinationSit", requestDestinationSit)
				.append("receivedDestinationSit", receivedDestinationSit)
				.append("destinationSITReason", destinationSITReason)
				.append("originSITReason", originSITReason)
				.append("cutOffTimeTo", cutOffTimeTo)
				.append("networkPartnerCode", networkPartnerCode)
				.append("networkContact", networkContact)
				.append("networkEmail", networkEmail)
				.append("networkPartnerName", networkPartnerName)
				.append("networkAgentExSO", networkAgentExSO)
				.append("originAgentExSO", originAgentExSO)
				.append("originSubAgentExSO", originSubAgentExSO)
				.append("destinationAgentExSO", destinationAgentExSO)
				.append("destinationSubAgentExSO", destinationSubAgentExSO)
				.append("sentToKSD", sentToKSD)
				.append("requiredDeliveryDays", requiredDeliveryDays)
				.append("lateReason", lateReason)
				.append("partyResponsible", partyResponsible)
				.append("soNetworkGroup", soNetworkGroup)
				.append("accNetworkGroup", accNetworkGroup)
				.append("utsiNetworkGroup", utsiNetworkGroup)
				.append("agentNetworkGroup", agentNetworkGroup)
				.append("bookingAgentExSO", bookingAgentExSO)
				.append("contractType", contractType)
				.append("ISFConfirmationNo", ISFConfirmationNo)
				.append("ugwIntId", ugwIntId)
				.append("serviceCompleteDate", serviceCompleteDate)
				.append("contractReceived", contractReceived)
				.append("confirmationOfDelievery", confirmationOfDelievery)
				.append("podToBooker", podToBooker)
				.append("missedRDDNotification", missedRDDNotification)
				.append("reason", reason)
				.append("docCutOffTimeTo", docCutOffTimeTo)
				.append("bookingAgentPhoneNumber",bookingAgentPhoneNumber)
				.append("networkPhoneNumber",networkPhoneNumber)
				.append("destinationPhoneNumber", destinationAgentPhoneNumber)
				.append("originAgentPhoneNumber", originAgentPhoneNumber)
				.append("subOriginAgentPhoneNumber", subOriginAgentPhoneNumber)
				.append("subDestinationAgentPhoneNumber", subDestinationAgentPhoneNumber)
				.append("tmss", tmss)
				.append("originGivenCode",originGivenCode)
				.append("originGivenName",originGivenName)
				.append("originReceivedCode",originReceivedCode)
				.append("originReceivedName",originReceivedName)
				.append("destinationGivenCode", destinationGivenCode)
				.append("destinationGivenName", destinationGivenName)
				.append("destinationReceivedCode", destinationReceivedCode)
				.append("destinationReceivedName", destinationReceivedName)
				.append("actualPackBegin", actualPackBegin)
				.append("actualLoadBegin", actualLoadBegin)
				.append("actualDeliveryBegin", actualDeliveryBegin)
				.append("targetPackEnding", targetPackEnding)
				.append("targetLoadEnding", targetLoadEnding)
				.append("targetdeliveryShipper", targetdeliveryShipper)
				.append("proNumber", proNumber)
				.append("packingWeightOn", packingWeightOn)
				.append("confirmationOn", confirmationOn)
				.append("sitDestinationInCondition", sitDestinationInCondition)
				.append("sitOriginOnCondition", sitOriginOnCondition)
				.append("originLocationType", originLocationType)
				.append("destinationLocationType", destinationLocationType)
				.append("packConfirmCallPriorOneDay", packConfirmCallPriorOneDay)
				.append("packConfirmCallPriorThreeDay", packConfirmCallPriorThreeDay)
				.append("customClearance", customClearance)
				.append("crewArrivalDate", crewArrivalDate)
				.append("endCustomClearance", endCustomClearance)
				.append("ffwRequested", ffwRequested)
				.append("ffwReceived", ffwReceived).append("ediDate",ediDate)
				.append("hbillLading", hbillLading)
				.append("PackingConfirmationSent", PackingConfirmationSent)
				.toString();
	}



	@OneToOne
	@JoinColumn(name = "id",  insertable = false, updatable = false, referencedColumnName = "id")
	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	@OneToOne
	@JoinColumn(name = "id", insertable = false, updatable = false, referencedColumnName = "id")
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	/* @OneToOne
	 @JoinColumn(name="ship", nullable=false, updatable=false,
	 referencedColumnName="ship")
	 public Miscellaneous getMiscellaneous() {
	 return miscellaneous;
	 }

	 public void setMiscellaneous(Miscellaneous miscellaneous) {
	 this.miscellaneous = miscellaneous;
	 } 
	 */
	@Column
	public Date getActivate() {
		return activate;
	}
	public void setActivate(Date activate) {
		this.activate = activate;
	}
	@Column
	public Date getBeginLoad() {
		return beginLoad;
	}
	public void setBeginLoad(Date beginLoad) {
		this.beginLoad = beginLoad;
	}
	@Column
	public Date getBeginPacking() {
		return beginPacking;
	}
	public void setBeginPacking(Date beginPacking) {
		this.beginPacking = beginPacking;
	}
	@Column(length = 30)
	public String getBillLading() {
		return billLading;
	}
	public void setBillLading(String billLading) {
		this.billLading = billLading;
	}
	@Column
	public Date getBrokerDeliveryOrder() {
		return brokerDeliveryOrder;
	}
	public void setBrokerDeliveryOrder(Date brokerDeliveryOrder) {
		this.brokerDeliveryOrder = brokerDeliveryOrder;
	}
	@Column
	public Date getBookerDeliveryNotification() {
		return bookerDeliveryNotification;
	}
	public void setBookerDeliveryNotification(Date bookerDeliveryNotification) {
		this.bookerDeliveryNotification = bookerDeliveryNotification;
	}
	@Column
	public Date getClearCustom() {
		return clearCustom;
	}
	public void setClearCustom(Date clearCustom) {
		this.clearCustom = clearCustom;
	}
	@Column(length = 1)
	public String getClearCustomTA() {
		return clearCustomTA;
	}
	public void setClearCustomTA(String clearCustomTA) {
		this.clearCustomTA = clearCustomTA;
	}
	@Column
	public Date getConfirmOriginAgent() {
		return confirmOriginAgent;
	}
	public void setConfirmOriginAgent(Date confirmOriginAgent) {
		this.confirmOriginAgent = confirmOriginAgent;
	}
	@Column
	public Date getContainerRecivedWH() {
		return containerRecivedWH;
	}
	public void setContainerRecivedWH(Date containerRecivedWH) {
		this.containerRecivedWH = containerRecivedWH;
	}
	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getDeliveryA() {
		return deliveryA;
	}
	public void setDeliveryA(Date deliveryA) {
		this.deliveryA = deliveryA;
	}
	@Column
	public Date getDeliveryFollowCall() {
		return deliveryFollowCall;
	}
	public void setDeliveryFollowCall(Date deliveryFollowCall) {
		this.deliveryFollowCall = deliveryFollowCall;
	}
	@Column
	public Date getDeliveryLastDay() {
		return deliveryLastDay;
	}
	public void setDeliveryLastDay(Date deliveryLastDay) {
		this.deliveryLastDay = deliveryLastDay;
	}
	@Column
	public Date getDeliveryReceiptDate() {
		return deliveryReceiptDate;
	}
	public void setDeliveryReceiptDate(Date deliveryReceiptDate) {
		this.deliveryReceiptDate = deliveryReceiptDate;
	}
	@Column
	public Date getDeliveryShipper() {
		return deliveryShipper;
	}
	public void setDeliveryShipper(Date deliveryShipper) {
		this.deliveryShipper = deliveryShipper;
	}
	@Column
	public Date getDeliveryTA() {
		return deliveryTA;
	}
	public void setDeliveryTA(Date deliveryTA) {
		this.deliveryTA = deliveryTA;
	}
	@Column(length = 20)
	public double getDemurrageCostPerDay1() {
		return demurrageCostPerDay1;
	}
	public void setDemurrageCostPerDay1(double demurrageCostPerDay1) {
		this.demurrageCostPerDay1 = demurrageCostPerDay1;
	}
	@Column(length = 20)
	public double getDemurrageCostPerDay2() {
		return demurrageCostPerDay2;
	}
	public void setDemurrageCostPerDay2(double demurrageCostPerDay2) {
		this.demurrageCostPerDay2 = demurrageCostPerDay2;
	}
	@Column(length = 1)
	public String getDemurragePayment() {
		return demurragePayment;
	}
	public void setDemurragePayment(String demurragePayment) {
		this.demurragePayment = demurragePayment;
	}
	@Column(length = 1)
	public String getDemurrageYN() {
		return demurrageYN;
	}
	public void setDemurrageYN(String demurrageYN) {
		this.demurrageYN = demurrageYN;
	}
	@Column
	public Date getEndLoad() {
		return endLoad;
	}
	public void setEndLoad(Date endLoad) {
		this.endLoad = endLoad;
	}
	@Column
	public Date getEndPacking() {
		return endPacking;
	}
	public void setEndPacking(Date endPacking) {
		this.endPacking = endPacking;
	}
	@Column
	public Date getEstimate2Account() {
		return estimate2Account;
	}
	public void setEstimate2Account(Date estimate2Account) {
		this.estimate2Account = estimate2Account;
	}
	@Column
	public Date getFinalNotification() {
		return finalNotification;
	}
	public void setFinalNotification(Date finalNotification) {
		this.finalNotification = finalNotification;
	}
	@Column(length = 20)
	public String getFlagCarrier() {
		return flagCarrier;
	}
	public void setFlagCarrier(String flagCarrier) {
		this.flagCarrier = flagCarrier;
	}
	@Column(length = 10)
	public String getFreeEntry3299() {
		return freeEntry3299;
	}
	public void setFreeEntry3299(String freeEntry3299) {
		this.freeEntry3299 = freeEntry3299;
	}
	@Column(length = 1)
	public int getFromWH() {
		return fromWH;
	}
	public void setFromWH(int fromWH) {
		this.fromWH = fromWH;
	}
	@Column(length = 20)
	public String getGbl() {
		return gbl;
	}
	public void setGbl(String gbl) {
		this.gbl = gbl;
	}
	@Column(length = 20)
	public double getInspectorCost() {
		return inspectorCost;
	}
	public void setInspectorCost(double inspectorCost) {
		this.inspectorCost = inspectorCost;
	}
	@Column(length = 1)
	public String getInspectorPaymentTrack() {
		return inspectorPaymentTrack;
	}
	public void setInspectorPaymentTrack(String inspectorPaymentTrack) {
		this.inspectorPaymentTrack = inspectorPaymentTrack;
	}
	@Column(length = 1)
	public String getInspectorYN() {
		return inspectorYN;
	}
	public void setInspectorYN(String inspectorYN) {
		this.inspectorYN = inspectorYN;
	}
	@Column
	public Date getInstruction2FF() {
		return instruction2FF;
	}
	public void setInstruction2FF(Date instruction2FF) {
		this.instruction2FF = instruction2FF;
	}
	@Column(length = 20)
	public String getItNumber() {
		return itNumber;
	}
	public void setItNumber(String itNumber) {
		this.itNumber = itNumber;
	}
	@Column
	public Date getLastFree() {
		return lastFree;
	}
	public void setLastFree(Date lastFree) {
		this.lastFree = lastFree;
	}
	@Column
	public Date getLeftWHOn() {
		return leftWHOn;
	}
	public void setLeftWHOn(Date leftWHOn) {
		this.leftWHOn = leftWHOn;
	}
	@Column
	public Date getLoadA() {
		return loadA;
	}
	public void setLoadA(Date loadA) {
		this.loadA = loadA;
	}
	@Column
	public Date getLoadFollowUpCall() {
		return loadFollowUpCall;
	}
	public void setLoadFollowUpCall(Date loadFollowUpCall) {
		this.loadFollowUpCall = loadFollowUpCall;
	}
	@Column
	public Date getMail3299() {
		return mail3299;
	}
	public void setMail3299(Date mail3299) {
		this.mail3299 = mail3299;
	}
	@Column
	public Date getMailClaim() {
		return mailClaim;
	}
	public void setMailClaim(Date mailClaim) {
		this.mailClaim = mailClaim;
	}
	@Column
	public Date getMailEvaluation() {
		return mailEvaluation;
	}
	public void setMailEvaluation(Date mailEvaluation) {
		this.mailEvaluation = mailEvaluation;
	}
	@Column(length = 3)
	public String getManagerOkInital() {
		return managerOkInital;
	}
	public void setManagerOkInital(String managerOkInital) {
		this.managerOkInital = managerOkInital;
	}
	@Column
	public Date getManagerOkOn() {
		return managerOkOn;
	}
	public void setManagerOkOn(Date managerOkOn) {
		this.managerOkOn = managerOkOn;
	}
	@Column
	public Date getOmniReport() {
		return omniReport;
	}
	public void setOmniReport(Date omniReport) {
		this.omniReport = omniReport;
	}
	@Column
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	@Column
	public Date getOriginBL() {
		return originBL;
	}
	public void setOriginBL(Date originBL) {
		this.originBL = originBL;
	}
	@Column
	public Date getPackA() {
		return packA;
	}
	public void setPackA(Date packA) {
		this.packA = packA;
	}
	@Column
	public Date getPackDayConfirmCall() {
		return packDayConfirmCall;
	}
	public void setPackDayConfirmCall(Date packDayConfirmCall) {
		this.packDayConfirmCall = packDayConfirmCall;
	}
	@Column
	public Date getPackDone() {
		return packDone;
	}
	public void setPackDone(Date packDone) {
		this.packDone = packDone;
	}
	@Column
	public Date getPackRoomA() {
		return packRoomA;
	}
	public void setPackRoomA(Date packRoomA) {
		this.packRoomA = packRoomA;
	}
	@Column
	public Date getPaymentRecived() {
		return paymentRecived;
	}
	public void setPaymentRecived(Date paymentRecived) {
		this.paymentRecived = paymentRecived;
	}
	@Column(length = 20)
	public double getPerdiumCostPerDay() {
		return perdiumCostPerDay;
	}
	public void setPerdiumCostPerDay(double perdiumCostPerDay) {
		this.perdiumCostPerDay = perdiumCostPerDay;
	}
	@Column(length = 2)
	public String getPerdiumPayment() {
		return perdiumPayment;
	}
	public void setPerdiumPayment(String perdiumPayment) {
		this.perdiumPayment = perdiumPayment;
	}
	@Column(length = 1)
	public String getPerdiumYN() {
		return perdiumYN;
	}
	public void setPerdiumYN(String perdiumYN) {
		this.perdiumYN = perdiumYN;
	}
	@Column
	public Date getPickUp2PierWH() {
		return pickUp2PierWH;
	}
	public void setPickUp2PierWH(Date pickUp2PierWH) {
		this.pickUp2PierWH = pickUp2PierWH;
	}
	@Column(length = 1)
	public String getPickUp2PierWHTA() {
		return pickUp2PierWHTA;
	}
	public void setPickUp2PierWHTA(String pickUp2PierWHTA) {
		this.pickUp2PierWHTA = pickUp2PierWHTA;
	}
	@Column
	public Date getPreferedLoadDate() {
		return preferedLoadDate;
	}
	public void setPreferedLoadDate(Date preferedLoadDate) {
		this.preferedLoadDate = preferedLoadDate;
	}
	@Column
	public Date getPreferedPackDate() {
		return preferedPackDate;
	}
	public void setPreferedPackDate(Date preferedPackDate) {
		this.preferedPackDate = preferedPackDate;
	}
	@Column
	public Date getPreliminaryNotification() {
		return preliminaryNotification;
	}
	public void setPreliminaryNotification(Date preliminaryNotification) {
		this.preliminaryNotification = preliminaryNotification;
	}
	@Column
	public Date getQuoteOn() {
		return quoteOn;
	}
	public void setQuoteOn(Date quoteOn) {
		this.quoteOn = quoteOn;
	}
	@Column
	public Date getRating() {
		return rating;
	}
	public void setRating(Date rating) {
		this.rating = rating;
	}
	@Column
	public Date getRecived3299() {
		return recived3299;
	}
	public void setRecived3299(Date recived3299) {
		this.recived3299 = recived3299;
	}
	@Column
	public Date getRecivedEvaluation() {
		return recivedEvaluation;
	}
	public void setRecivedEvaluation(Date recivedEvaluation) {
		this.recivedEvaluation = recivedEvaluation;
	}
	@Column
	public Date getRecivedGBL() {
		return recivedGBL;
	}
	public void setRecivedGBL(Date recivedGBL) {
		this.recivedGBL = recivedGBL;
	}
	@Column
	public Date getRecivedPayment() {
		return recivedPayment;
	}
	public void setRecivedPayment(Date recivedPayment) {
		this.recivedPayment = recivedPayment;
	}
	@Column
	public Date getReconfirmShipper() {
		return reconfirmShipper;
	}
	public void setReconfirmShipper(Date reconfirmShipper) {
		this.reconfirmShipper = reconfirmShipper;
	}
	@Column
	public Date getRequestGBL() {
		return requestGBL;
	}
	public void setRequestGBL(Date requestGBL) {
		this.requestGBL = requestGBL;
	}
	@Column(length = 1)
	public String getReturnContainer() {
		return returnContainer;
	}
	public void setReturnContainer(String returnContainer) {
		this.returnContainer = returnContainer;
	}
	@Column
	public Date getReturnContainerDateA() {
		return returnContainerDateA;
	}
	public void setReturnContainerDateA(Date returnContainerDateA) {
		this.returnContainerDateA = returnContainerDateA;
	}
	@Column
	public Date getSent3299ToBroker() {
		return sent3299ToBroker;
	}
	public void setSent3299ToBroker(Date sent3299ToBroker) {
		this.sent3299ToBroker = sent3299ToBroker;
	}
	@Column
	public Date getSentcomp() {
		return sentcomp;
	}
	public void setSentcomp(Date sentcomp) {
		this.sentcomp = sentcomp;
	}
	@Column(length = 20)
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column(length = 20)
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Date getSitDestinationA() {
		return sitDestinationA;
	}
	public void setSitDestinationA(Date sitDestinationA) {
		this.sitDestinationA = sitDestinationA;
	}
	@Column(length = 5)
	public int getSitDestinationDays() {
		return sitDestinationDays;
	}
	public void setSitDestinationDays(int sitDestinationDays) {
		this.sitDestinationDays = sitDestinationDays;
	}
	@Column
	public Date getSitDestinationIn() {
		return sitDestinationIn;
	}
	public void setSitDestinationIn(Date sitDestinationIn) {
		this.sitDestinationIn = sitDestinationIn;
	}
	@Column
	public Date getSitDestinationLetter() {
		return sitDestinationLetter;
	}
	public void setSitDestinationLetter(Date sitDestinationLetter) {
		this.sitDestinationLetter = sitDestinationLetter;
	}
	@Column(length = 1)
	public String getSitDestinationLetterTA() {
		return sitDestinationLetterTA;
	}
	public void setSitDestinationLetterTA(String sitDestinationLetterTA) {
		this.sitDestinationLetterTA = sitDestinationLetterTA;
	}
	@Column(length = 1)
	public String getSitDestinationTA() {
		return sitDestinationTA;
	}
	public void setSitDestinationTA(String sitDestinationTA) {
		this.sitDestinationTA = sitDestinationTA;
	}
	@Column(length = 1)
	public String getSitDestinationYN() {
		return sitDestinationYN;
	}
	public void setSitDestinationYN(String sitDestinationYN) {
		this.sitDestinationYN = sitDestinationYN;
	}
	@Column
	public Date getSitOriginA() {
		return sitOriginA;
	}
	public void setSitOriginA(Date sitOriginA) {
		this.sitOriginA = sitOriginA;
	}
	@Column(length = 5)
	public int getSitOriginDays() {
		return sitOriginDays;
	}
	public void setSitOriginDays(int sitOriginDays) {
		this.sitOriginDays = sitOriginDays;
	}
	@Column
	public Date getSitOriginLetter() {
		return sitOriginLetter;
	}
	public void setSitOriginLetter(Date sitOriginLetter) {
		this.sitOriginLetter = sitOriginLetter;
	}
	@Column(length = 1)
	public String getSitOriginLetterTA() {
		return sitOriginLetterTA;
	}
	public void setSitOriginLetterTA(String sitOriginLetterTA) {
		this.sitOriginLetterTA = sitOriginLetterTA;
	}
	@Column
	public Date getSitOriginOn() {
		return sitOriginOn;
	}
	public void setSitOriginOn(Date sitOriginOn) {
		this.sitOriginOn = sitOriginOn;
	}
	@Column(length = 1)
	public String getSitOriginTA() {
		return sitOriginTA;
	}
	public void setSitOriginTA(String sitOriginTA) {
		this.sitOriginTA = sitOriginTA;
	}
	@Column(length = 1)
	public String getSitOriginYN() {
		return sitOriginYN;
	}
	public void setSitOriginYN(String sitOriginYN) {
		this.sitOriginYN = sitOriginYN;
	}
	@Column
	public Date getSurvey() {
		return survey;
	}
	public void setSurvey(Date survey) {
		this.survey = survey;
	}
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 20)
	public double getUsdaCost() {
		return usdaCost;
	}
	public void setUsdaCost(double usdaCost) {
		this.usdaCost = usdaCost;
	}
	@Column
	public Date getVehicleRecived() {
		return vehicleRecived;
	}
	public void setVehicleRecived(Date vehicleRecived) {
		this.vehicleRecived = vehicleRecived;
	}
	@Column(length = 20)
	public double getXrayCost() {
		return xrayCost;
	}
	public void setXrayCost(double xrayCost) {
		this.xrayCost = xrayCost;
	}
	@Column(length = 1)
	public String getXrayYN() {
		return xrayYN;
	}
	public void setXrayYN(String xrayYN) {
		this.xrayYN = xrayYN;
	}
	@Id
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBooker() {
		return booker;
	}
	public void setBooker(String booker) {
		this.booker = booker;
	}
	public String getDestinationAgent() {
		return destinationAgent;
	}
	public void setDestinationAgent(String destinationAgent) {
		this.destinationAgent = destinationAgent;
	}
	public String getOriginAgent() {
		return originAgent;
	}
	public void setOriginAgent(String originAgent) {
		this.originAgent = originAgent;
	}
	public String getReturnLocationOrigin() {
		return returnLocationOrigin;
	}
	public void setReturnLocationOrigin(String returnLocationOrigin) {
		this.returnLocationOrigin = returnLocationOrigin;
	}
	public String getSitAuthorizeDestination() {
		return sitAuthorizeDestination;
	}
	public void setSitAuthorizeDestination(String sitAuthorizeDestination) {
		this.sitAuthorizeDestination = sitAuthorizeDestination;
	}
	public String getSitAuthorizeOrigin() {
		return sitAuthorizeOrigin;
	}
	public void setSitAuthorizeOrigin(String sitAuthorizeOrigin) {
		this.sitAuthorizeOrigin = sitAuthorizeOrigin;
	}
	public Date getInitialContact() {
		return initialContact;
	}
	public void setInitialContact(Date initialContact) {
		this.initialContact = initialContact;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getStatusDate() {
		return statusDate;
	}
	public void setStatusDate(Date statusDate) {
		this.statusDate = statusDate;
	}
	public Date getLastFreeDestination() {
		return lastFreeDestination;
	}
	public void setLastFreeDestination(Date lastFreeDestination) {
		this.lastFreeDestination = lastFreeDestination;
	}
	public Date getPickUp2PierWHDestination() {
		return pickUp2PierWHDestination;
	}
	public void setPickUp2PierWHDestination(Date pickUp2PierWHDestination) {
		this.pickUp2PierWHDestination = pickUp2PierWHDestination;
	}
	public String getPickUp2PierWHTADestination() {
		return pickUp2PierWHTADestination;
	}
	public void setPickUp2PierWHTADestination(String pickUp2PierWHTADestination) {
		this.pickUp2PierWHTADestination = pickUp2PierWHTADestination;
	}
	public Date getReturnContainerDateADestination() {
		return returnContainerDateADestination;
	}
	public void setReturnContainerDateADestination(Date returnContainerDateADestination) {
		this.returnContainerDateADestination = returnContainerDateADestination;
	}
	public String getReturnContainerDestination() {
		return returnContainerDestination;
	}
	public void setReturnContainerDestination(String returnContainerDestination) {
		this.returnContainerDestination = returnContainerDestination;
	}
	public Date getCutOffDate() {
		return cutOffDate;
	}
	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}
	public String getItNumber2() {
		return itNumber2;
	}
	public void setItNumber2(String itNumber2) {
		this.itNumber2 = itNumber2;
	}
	public String getCutOffDateTA() {
		return cutOffDateTA;
	}
	public void setCutOffDateTA(String cutOffDateTA) {
		this.cutOffDateTA = cutOffDateTA;
	}
	public String getDestinationAgentCode() {
		return destinationAgentCode;
	}
	public void setDestinationAgentCode(String destinationAgentCode) {
		this.destinationAgentCode = destinationAgentCode;
	}
	public String getOriginAgentCode() {
		return originAgentCode;
	}
	public void setOriginAgentCode(String originAgentCode) {
		this.originAgentCode = originAgentCode;
	}
	@Column
	public Date getDeliveryReceiptSentOA() {
		return deliveryReceiptSentOA;
	}
	public void setDeliveryReceiptSentOA(Date deliveryReceiptSentOA) {
		this.deliveryReceiptSentOA = deliveryReceiptSentOA;
	}
	@Column
	public Date getDocumentCHA() {
		return documentCHA;
	}
	public void setDocumentCHA(Date documentCHA) {
		this.documentCHA = documentCHA;
	}
	@Column
	public Date getDocumentReceivedClient() {
		return documentReceivedClient;
	}
	public void setDocumentReceivedClient(Date documentReceivedClient) {
		this.documentReceivedClient = documentReceivedClient;
	}
	@Column
	public Date getDocumentSentClient() {
		return documentSentClient;
	}
	public void setDocumentSentClient(Date documentSentClient) {
		this.documentSentClient = documentSentClient;
	}
	@Column
	public Date getDocumentReceivedFromOA() {
		return documentReceivedFromOA;
	}
	public void setDocumentReceivedFromOA(Date documentReceivedFromOA) {
		this.documentReceivedFromOA = documentReceivedFromOA;
	}
	@Column(length = 25)
	public String getBrokerCode() {
		return brokerCode;
	}
	public void setBrokerCode(String brokerCode) {
		this.brokerCode = brokerCode;
	}
	@Column(length = 255)
	public String getBrokerName() {
		return brokerName;
	}
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	@Column(length = 20)
	public String getActgCode() {
		return actgCode;
	}
	public void setActgCode(String actgCode) {
		this.actgCode = actgCode;
	}
	@Column(length = 20)
	public String getActgCodeForDis() {
		return actgCodeForDis;
	}
	public void setActgCodeForDis(String actgCodeForDis) {
		this.actgCodeForDis = actgCodeForDis;
	}
	@Column(length = 255)
	public String getDestinationSubAgent() {
		return destinationSubAgent;
	}
	public void setDestinationSubAgent(String destinationSubAgent) {
		this.destinationSubAgent = destinationSubAgent;
	}
	@Column(length = 8)
	public String getDestinationSubAgentCode() {
		return destinationSubAgentCode;
	}
	public void setDestinationSubAgentCode(String destinationSubAgentCode) {
		this.destinationSubAgentCode = destinationSubAgentCode;
	}
	@Column(length = 255)
	public String getOriginSubAgent() {
		return originSubAgent;
	}
	public void setOriginSubAgent(String originSubAgent) {
		this.originSubAgent = originSubAgent;
	}
	@Column(length = 8)
	public String getOriginSubAgentCode() {
		return originSubAgentCode;
	}
	public void setOriginSubAgentCode(String originSubAgentCode) {
		this.originSubAgentCode = originSubAgentCode;
	}
	@Column
	public Date getSurveyDate() {
		return surveyDate;
	}
	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}
	@Column(length = 5)
	public String getSurveyTimeFrom() {
		return surveyTimeFrom;
	}
	public void setSurveyTimeFrom(String surveyTimeFrom) {
		this.surveyTimeFrom = surveyTimeFrom;
	}
	@Column(length = 5)
	public String getSurveyTimeTo() {
		return surveyTimeTo;
	}
	public void setSurveyTimeTo(String surveyTimeTo) {
		this.surveyTimeTo = surveyTimeTo;
	}
	@Column(length = 200)
	public String getBrokerContact() {
		return brokerContact;
	}
	public void setBrokerContact(String brokerContact) {
		this.brokerContact = brokerContact;
	}
	@Column(length = 100)
	public String getBrokerEmail() {
		return brokerEmail;
	}
	public void setBrokerEmail(String brokerEmail) {
		this.brokerEmail = brokerEmail;
	}
	@Column(length = 200)
	public String getDestinationAgentContact() {
		return destinationAgentContact;
	}
	public void setDestinationAgentContact(String destinationAgentContact) {
		this.destinationAgentContact = destinationAgentContact;
	}
	@Column(length = 100)
	public String getDestinationAgentEmail() {
		return destinationAgentEmail;
	}
	public void setDestinationAgentEmail(String destinationAgentEmail) {
		this.destinationAgentEmail = destinationAgentEmail;
	}
	@Column(length = 200)
	public String getOriginAgentContact() {
		return originAgentContact;
	}
	public void setOriginAgentContact(String originAgentContact) {
		this.originAgentContact = originAgentContact;
	}
	@Column(length = 100)
	public String getOriginAgentEmail() {
		return originAgentEmail;
	}
	public void setOriginAgentEmail(String originAgentEmail) {
		this.originAgentEmail = originAgentEmail;
	}
	@Column(length = 200)
	public String getSubDestinationAgentAgentContact() {
		return subDestinationAgentAgentContact;
	}
	public void setSubDestinationAgentAgentContact(String subDestinationAgentAgentContact) {
		this.subDestinationAgentAgentContact = subDestinationAgentAgentContact;
	}
	@Column(length = 100)
	public String getSubDestinationAgentEmail() {
		return subDestinationAgentEmail;
	}
	public void setSubDestinationAgentEmail(String subDestinationAgentEmail) {
		this.subDestinationAgentEmail = subDestinationAgentEmail;
	}
	@Column(length = 200)
	public String getSubOriginAgentContact() {
		return subOriginAgentContact;
	}
	public void setSubOriginAgentContact(String subOriginAgentContact) {
		this.subOriginAgentContact = subOriginAgentContact;
	}
	@Column(length = 100)
	public String getSubOriginAgentEmail() {
		return subOriginAgentEmail;
	}
	public void setSubOriginAgentEmail(String subOriginAgentEmail) {
		this.subOriginAgentEmail = subOriginAgentEmail;
	}
	@Column(length = 200)
	public String getBookingAgentContact() {
		return bookingAgentContact;
	}
	public void setBookingAgentContact(String bookingAgentContact) {
		this.bookingAgentContact = bookingAgentContact;
	}
	@Column(length = 100)
	public String getBookingAgentEmail() {
		return bookingAgentEmail;
	}
	public void setBookingAgentEmail(String bookingAgentEmail) {
		this.bookingAgentEmail = bookingAgentEmail;
	}
	public Date getClearCustomOrigin() {
		return clearCustomOrigin;
	}
	public void setClearCustomOrigin(Date clearCustomOrigin) {
		this.clearCustomOrigin = clearCustomOrigin;
	}
	@Column(length = 11)
	public int getSitDestinationDaysAllowed() {
		return sitDestinationDaysAllowed;
	}
	public void setSitDestinationDaysAllowed(int sitDestinationDaysAllowed) {
		this.sitDestinationDaysAllowed = sitDestinationDaysAllowed;
	}
	@Column(length = 11)
	public int getSitOriginDaysAllowed() {
		return sitOriginDaysAllowed;
	}
	public void setSitOriginDaysAllowed(int sitOriginDaysAllowed) {
		this.sitOriginDaysAllowed = sitOriginDaysAllowed;
	}
	@Column(length = 100)
	public String getForwarder() {
		return forwarder;
	}
	public void setForwarder(String forwarder) {
		this.forwarder = forwarder;
	}
	@Column(length = 10)
	public String getForwarderCode() {
		return forwarderCode;
	}
	public void setForwarderCode(String forwarderCode) {
		this.forwarderCode = forwarderCode;
	}
	@Column(length = 200)
	public String getForwarderContact() {
		return forwarderContact;
	}
	public void setForwarderContact(String forwarderContact) {
		this.forwarderContact = forwarderContact;
	}
	@Column(length = 100)
	public String getForwarderEmail() {
		return forwarderEmail;
	}
	public void setForwarderEmail(String forwarderEmail) {
		this.forwarderEmail = forwarderEmail;
	}
	@Column
	public Boolean getGsaiffFundingFlag() {
		return gsaiffFundingFlag;
	}
	public void setGsaiffFundingFlag(Boolean gsaiffFundingFlag) {
		this.gsaiffFundingFlag = gsaiffFundingFlag;
	}
	@Column(length = 25)
	public String getDestinationAgentVanlineCode() {
		return destinationAgentVanlineCode;
	}
	public void setDestinationAgentVanlineCode(String destinationAgentVanlineCode) {
		this.destinationAgentVanlineCode = destinationAgentVanlineCode;
	}
	@Column(length = 25)
	public String getOriginAgentVanlineCode() {
		return originAgentVanlineCode;
	}
	public void setOriginAgentVanlineCode(String originAgentVanlineCode) {
		this.originAgentVanlineCode = originAgentVanlineCode;
	}
	public Date getRequiredDeliveryDate() {
		return requiredDeliveryDate;
	}
	public void setRequiredDeliveryDate(Date requiredDeliveryDate) {
		this.requiredDeliveryDate = requiredDeliveryDate;
	}
	/**
	 * @return the iSFSubmitted
	 */
	@Column
	public Date getISFSubmitted() {
		return ISFSubmitted;
	}
	/**
	 * @param submitted the iSFSubmitted to set
	 */
	public void setISFSubmitted(Date submitted) {
		ISFSubmitted = submitted;
	}
	/**
	 * @return the daShipmentNum
	 */
	@Column(length=20)
	public String getDaShipmentNum() {
		return daShipmentNum;
	}
	/**
	 * @param daShipmentNum the daShipmentNum to set
	 */
	public void setDaShipmentNum(String daShipmentNum) {
		this.daShipmentNum = daShipmentNum;
	}
	/**
	 * @return the sITNotify
	 */
	@Column
	public Date getSITNotify() {
		return SITNotify;
	}
	/**
	 * @param notify the sITNotify to set
	 */
	public void setSITNotify(Date notify) {
		SITNotify = notify;
	}
    @Column(length=20)
	public String getDestinationSITReason() {
		return destinationSITReason;
	}
	public void setDestinationSITReason(String destinationSITReason) {
		this.destinationSITReason = destinationSITReason;
	}
	 @Column(length=20)
	public String getOriginSITReason() {
		return originSITReason;
	}
	public void setOriginSITReason(String originSITReason) {
		this.originSITReason = originSITReason;
	}
	 @Column
	public Date getReceivedDestinationSit() {
		return receivedDestinationSit;
	}
	public void setReceivedDestinationSit(Date receivedDestinationSit) {
		this.receivedDestinationSit = receivedDestinationSit;
	}
	 @Column
	public Date getReceivedOriginSit() {
		return receivedOriginSit;
	}
	public void setReceivedOriginSit(Date receivedOriginSit) {
		this.receivedOriginSit = receivedOriginSit;
	}
	@Column
	public Date getRequestDestinationSit() {
		return requestDestinationSit;
	}
	public void setRequestDestinationSit(Date requestDestinationSit) {
		this.requestDestinationSit = requestDestinationSit;
	}
    @Column
	public Date getRequestOriginSit() {
		return requestOriginSit;
	}
	public void setRequestOriginSit(Date requestOriginSit) {
		this.requestOriginSit = requestOriginSit;
	}
	public String getNetworkPartnerCode() {
		return networkPartnerCode;
	}
	public void setNetworkPartnerCode(String networkPartnerCode) {
		this.networkPartnerCode = networkPartnerCode;
	}
	public String getNetworkContact() {
		return networkContact;
	}
	public void setNetworkContact(String networkContact) {
		this.networkContact = networkContact;
	}
	public String getNetworkEmail() {
		return networkEmail;
	}
	public void setNetworkEmail(String networkEmail) {
		this.networkEmail = networkEmail;
	}
	public String getNetworkAgentExSO() {
		return networkAgentExSO;
	}
	public void setNetworkAgentExSO(String networkAgentExSO) {
		this.networkAgentExSO = networkAgentExSO;
	}
	public String getOriginAgentExSO() {
		return originAgentExSO;
	}
	public void setOriginAgentExSO(String originAgentExSO) {
		this.originAgentExSO = originAgentExSO;
	}
	public String getOriginSubAgentExSO() {
		return originSubAgentExSO;
	}
	public void setOriginSubAgentExSO(String originSubAgentExSO) {
		this.originSubAgentExSO = originSubAgentExSO;
	}
	public String getDestinationAgentExSO() {
		return destinationAgentExSO;
	}
	public void setDestinationAgentExSO(String destinationAgentExSO) {
		this.destinationAgentExSO = destinationAgentExSO;
	}
	public String getDestinationSubAgentExSO() {
		return destinationSubAgentExSO;
	}
	public void setDestinationSubAgentExSO(String destinationSubAgentExSO) {
		this.destinationSubAgentExSO = destinationSubAgentExSO;
	}
	public String getNetworkPartnerName() {
		return networkPartnerName;
	}
	public void setNetworkPartnerName(String networkPartnerName) {
		this.networkPartnerName = networkPartnerName;
	}
	@Column(length = 5)
	public String getCutOffTimeTo() {
		return cutOffTimeTo;
	}
	public void setCutOffTimeTo(String cutOffTimeTo) {
		this.cutOffTimeTo = cutOffTimeTo;
	}
	@Column
	public Date getSentToKSD() {
		return sentToKSD;
	}
	public void setSentToKSD(Date sentToKSD) {
		this.sentToKSD = sentToKSD;
	}
	@Column
	public Integer getRequiredDeliveryDays() {
		return requiredDeliveryDays;
	}
	public void setRequiredDeliveryDays(Integer requiredDeliveryDays) {
		this.requiredDeliveryDays = requiredDeliveryDays;
	}
    public String getLateReason() {
		return lateReason;
	}
	public void setLateReason(String lateReason) {
		this.lateReason = lateReason;
	}
	public String getPartyResponsible() {
		return partyResponsible;
	}
	public void setPartyResponsible(String partyResponsible) {
		this.partyResponsible = partyResponsible;
	} 
	@Column
	public Boolean getSoNetworkGroup() {
		return soNetworkGroup;
	}
	public void setSoNetworkGroup(Boolean soNetworkGroup) {
		this.soNetworkGroup = soNetworkGroup;
	}
	@Column
	public Boolean getAccNetworkGroup() {
		return accNetworkGroup;
	}
	public void setAccNetworkGroup(Boolean accNetworkGroup) {
		this.accNetworkGroup = accNetworkGroup;
	}
	@Column(name="documentationCutOff")
	public Date getDocumentationCutOffDate() {
		return documentationCutOffDate;
	}
	public void setDocumentationCutOffDate(Date documentationCutOffDate) {
		this.documentationCutOffDate = documentationCutOffDate;
	}
	@Column
	public Date getPassportReceived() {
		return passportReceived;
	}
	public void setPassportReceived(Date passportReceived) {
		this.passportReceived = passportReceived;
	}
	@Column
	public Date getEinReceived() {
		return einReceived;
	}
	public void setEinReceived(Date einReceived) {
		this.einReceived = einReceived;
	}
	@Column
	public Boolean getUtsiNetworkGroup() {
		return utsiNetworkGroup;
	}
	public void setUtsiNetworkGroup(Boolean utsiNetworkGroup) {
		this.utsiNetworkGroup = utsiNetworkGroup;
	}
	@Column
	public Boolean getAgentNetworkGroup() {
		return agentNetworkGroup;
	}
	public void setAgentNetworkGroup(Boolean agentNetworkGroup) {
		this.agentNetworkGroup = agentNetworkGroup;
	}
	@Column(length=25)
	public String getBookingAgentExSO() {
		return bookingAgentExSO;
	}
	public void setBookingAgentExSO(String bookingAgentExSO) {
		this.bookingAgentExSO = bookingAgentExSO;
	}
	@Column(length=3)
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	@Column(length=20)
	public String getISFConfirmationNo() {
		return ISFConfirmationNo;
	}
	public void setISFConfirmationNo(String iSFConfirmationNo) {
		ISFConfirmationNo = iSFConfirmationNo;
	}
	@Column
	public String getUgwIntId() {
		return ugwIntId;
	}
	public void setUgwIntId(String ugwIntId) {
		this.ugwIntId = ugwIntId;
	}
	@Column
	public Date getServiceCompleteDate() {
		return serviceCompleteDate;
	}
	public void setServiceCompleteDate(Date serviceCompleteDate) {
		this.serviceCompleteDate = serviceCompleteDate;
	}
	@Column
	public Date getContractReceived() {
		return contractReceived;
	}
	public void setContractReceived(Date contractReceived) {
		this.contractReceived = contractReceived;
	}
	public Date getConfirmationOfDelievery() {
		return confirmationOfDelievery;
	}
	public void setConfirmationOfDelievery(Date confirmationOfDelievery) {
		this.confirmationOfDelievery = confirmationOfDelievery;
	}
	public Date getPodToBooker() {
		return podToBooker;
	}
	public void setPodToBooker(Date podToBooker) {
		this.podToBooker = podToBooker;
	}
	@Column
	public Date getMissedRDDNotification() {
		return missedRDDNotification;
	}
	@Column(length=45)
	public String getReason() {
		return reason;
	}
	public void setMissedRDDNotification(Date missedRDDNotification) {
		this.missedRDDNotification = missedRDDNotification;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	@Column(length = 5)
	public String getDocCutOffTimeTo() {
		return docCutOffTimeTo;
	}
	public void setDocCutOffTimeTo(String docCutOffTimeTo) {
		this.docCutOffTimeTo = docCutOffTimeTo;
	}
	@Column
	public String getBookingAgentPhoneNumber() {
		return bookingAgentPhoneNumber;
	}
	@Column
	public String getNetworkPhoneNumber() {
		return networkPhoneNumber;
	}
	@Column
	public String getOriginAgentPhoneNumber() {
		return originAgentPhoneNumber;
	}
	@Column
	public String getDestinationAgentPhoneNumber() {
		return destinationAgentPhoneNumber;
	}
	@Column
	public String getSubOriginAgentPhoneNumber() {
		return subOriginAgentPhoneNumber;
	}
	@Column
	public String getSubDestinationAgentPhoneNumber() {
		return subDestinationAgentPhoneNumber;
	}
	public void setBookingAgentPhoneNumber(String bookingAgentPhoneNumber) {
		this.bookingAgentPhoneNumber = bookingAgentPhoneNumber;
	}
	public void setNetworkPhoneNumber(String networkPhoneNumber) {
		this.networkPhoneNumber = networkPhoneNumber;
	}
	public void setOriginAgentPhoneNumber(String originAgentPhoneNumber) {
		this.originAgentPhoneNumber = originAgentPhoneNumber;
	}
	public void setDestinationAgentPhoneNumber(String destinationAgentPhoneNumber) {
		this.destinationAgentPhoneNumber = destinationAgentPhoneNumber;
	}
	public void setSubOriginAgentPhoneNumber(String subOriginAgentPhoneNumber) {
		this.subOriginAgentPhoneNumber = subOriginAgentPhoneNumber;
	}
	public void setSubDestinationAgentPhoneNumber(
			String subDestinationAgentPhoneNumber) {
		this.subDestinationAgentPhoneNumber = subDestinationAgentPhoneNumber;
	}
	@Column
	public String getTmss() {
		return tmss;
	}
	public void setTmss(String tmss) {
		this.tmss = tmss;
	}
	@Column
	public String getOriginGivenCode() {
		return originGivenCode;
	}
	public void setOriginGivenCode(String originGivenCode) {
		this.originGivenCode = originGivenCode;
	}
	@Column
	public String getOriginGivenName() {
		return originGivenName;
	}	
	public void setOriginGivenName(String originGivenName) {
		this.originGivenName = originGivenName;
	}
	@Column
	public String getOriginReceivedCode() {
		return originReceivedCode;
	}
	public void setOriginReceivedCode(String originReceivedCode) {
		this.originReceivedCode = originReceivedCode;
	}
	@Column
	public String getOriginReceivedName() {
		return originReceivedName;
	}
	public void setOriginReceivedName(String originReceivedName) {
		this.originReceivedName = originReceivedName;
	}
	@Column
	public String getDestinationGivenCode() {
		return destinationGivenCode;
	}
	public void setDestinationGivenCode(String destinationGivenCode) {
		this.destinationGivenCode = destinationGivenCode;
	}
	@Column
	public String getDestinationGivenName() {
		return destinationGivenName;
	}
	public void setDestinationGivenName(String destinationGivenName) {
		this.destinationGivenName = destinationGivenName;
	}	
	@Column
	public String getDestinationReceivedCode() {
		return destinationReceivedCode;
	}
	public void setDestinationReceivedCode(String destinationReceivedCode) {
		this.destinationReceivedCode = destinationReceivedCode;
	}	
	@Column
	public String getDestinationReceivedName() {
		return destinationReceivedName;
	}
	public void setDestinationReceivedName(String destinationReceivedName) {
		this.destinationReceivedName = destinationReceivedName;
	}
	@Column
	public Date getActualPackBegin() {
		return actualPackBegin;
	}
	public void setActualPackBegin(Date actualPackBegin) {
		this.actualPackBegin = actualPackBegin;
	}
	@Column
	public Date getActualLoadBegin() {
		return actualLoadBegin;
	}
	public void setActualLoadBegin(Date actualLoadBegin) {
		this.actualLoadBegin = actualLoadBegin;
	}
	@Column
	public Date getActualDeliveryBegin() {
		return actualDeliveryBegin;
	}
	public void setActualDeliveryBegin(Date actualDeliveryBegin) {
		this.actualDeliveryBegin = actualDeliveryBegin;
	}
	@Column
	public Date getTargetPackEnding() {
		return targetPackEnding;
	}
	public void setTargetPackEnding(Date targetPackEnding) {
		this.targetPackEnding = targetPackEnding;
	}
	@Column
	public Date getTargetLoadEnding() {
		return targetLoadEnding;
	}
	public void setTargetLoadEnding(Date targetLoadEnding) {
		this.targetLoadEnding = targetLoadEnding;
	}
	@Column
	public Date getTargetdeliveryShipper() {
		return targetdeliveryShipper;
	}
	public void setTargetdeliveryShipper(Date targetdeliveryShipper) {
		this.targetdeliveryShipper = targetdeliveryShipper;
	}
	@Column
	public String getProNumber() {
		return proNumber;
	}
	public void setProNumber(String proNumber) {
		this.proNumber = proNumber;
	}
	@Column
	public Date getPackingWeightOn() {
		return packingWeightOn;
	}
	public void setPackingWeightOn(Date packingWeightOn) {
		this.packingWeightOn = packingWeightOn;
	}
	@Column
	public Date getConfirmationOn() {
		return confirmationOn;
	}
	public void setConfirmationOn(Date confirmationOn) {
		this.confirmationOn = confirmationOn;
	}
	@Column 
	public String getSitDestinationInCondition() {
		return sitDestinationInCondition;
	}  
	public void setSitDestinationInCondition(String sitDestinationInCondition) {
		this.sitDestinationInCondition = sitDestinationInCondition;
	}
	@Column
	public String getSitOriginOnCondition() {
		return sitOriginOnCondition;
	} 
	public void setSitOriginOnCondition(String sitOriginOnCondition) {
		this.sitOriginOnCondition = sitOriginOnCondition;
	}
	@Column
	public String getOriginLocationType() {
		return originLocationType;
	}
	public void setOriginLocationType(String originLocationType) {
		this.originLocationType = originLocationType;
	}
	@Column
	public String getDestinationLocationType() {
		return destinationLocationType;
	}
	public void setDestinationLocationType(String destinationLocationType) {
		this.destinationLocationType = destinationLocationType;
	}
	@Column
	public Date getPackConfirmCallPriorOneDay() {
		return packConfirmCallPriorOneDay;
	}
	public void setPackConfirmCallPriorOneDay(Date packConfirmCallPriorOneDay) {
		this.packConfirmCallPriorOneDay = packConfirmCallPriorOneDay;
	}
    @Column
	public Date getPackConfirmCallPriorThreeDay() {
		return packConfirmCallPriorThreeDay;
	}
	public void setPackConfirmCallPriorThreeDay(Date packConfirmCallPriorThreeDay) {
		this.packConfirmCallPriorThreeDay = packConfirmCallPriorThreeDay;
	}


	@Column
	public Date getCustomClearance() {
		return customClearance;
	}
    public void setCustomClearance(Date customClearance) {
		this.customClearance = customClearance;
	}



	public Date getCrewArrivalDate() {
		return crewArrivalDate;
	}



	public void setCrewArrivalDate(Date crewArrivalDate) {
		this.crewArrivalDate = crewArrivalDate;
	}


    @Column
	public Date getEndCustomClearance() {
		return endCustomClearance;
	}



	public void setEndCustomClearance(Date endCustomClearance) {
		this.endCustomClearance = endCustomClearance;
	}
    @Column
	public Date getFfwRequested() {
			return ffwRequested;
		}
    
    public void setFfwRequested(Date ffwRequested) {
			this.ffwRequested = ffwRequested;
		}


	@Column
	public Date getFfwReceived() {
		return ffwReceived;
	}



	public void setFfwReceived(Date ffwReceived) {
		this.ffwReceived = ffwReceived;
	}


	@Column
	public Date getEdiDate() {
		return ediDate;
	}



	public void setEdiDate(Date ediDate) {
		this.ediDate = ediDate;
	}
	@Column(length = 30)
	public String getHbillLading() {
		return hbillLading;
	}
	public void setHbillLading(String hbillLading) {
		this.hbillLading = hbillLading;
	}


	@Column(length = 1)
	public String getPackingConfirmationSent() {
		return PackingConfirmationSent;
	}



	public void setPackingConfirmationSent(String packingConfirmationSent) {
		PackingConfirmationSent = packingConfirmationSent;
	}

}
