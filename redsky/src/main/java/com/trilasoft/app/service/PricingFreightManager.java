package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.PricingFreight;

public interface PricingFreightManager extends GenericManager<PricingFreight, Long>{
	 public List checkById(Long id);
	public List getFreightList(Long pricingControlId);
	public List getMarkUpValue(BigDecimal price, String contract, String mode);
	public List getPricingFreightRecord(Long id, String sessionCorpID);  
}
