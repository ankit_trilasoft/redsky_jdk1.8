/**
 * @Class Name	ResourceCommonUtil.java
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Since       1.0
 * @Date        31-Aug-2012
 */

package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.service.ItemsJEquipManager;
import com.trilasoft.app.service.ItemsJbkEquipManager;
import com.trilasoft.app.service.OperationsIntelligenceManager;


public class ResourceCommonUtil {

	public static Map<String, Map> findAllResourcesAjax(ItemsJbkEquipManager itemsJbkEquipManager,String shipNumber, 
			String sessionCorpID,String dayFocus){
		Map<String, Map> resourceMap1 = itemsJbkEquipManager.findByShipNumAndDayMap(shipNumber, sessionCorpID,dayFocus);
		 return resourceMap1;
	}
	
	public static void addResourceTemplate(ItemsJEquipManager itemsJEquipManager,ItemsJbkEquipManager itemsJbkEquipManager,
			String contract,String shipNumber,String sessionCorpID,String userName){
    	List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
    	int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
    	if(!resourceList.isEmpty()){
    		Iterator it=resourceList.iterator();
    		while(it.hasNext()){
	    	    Object []row= (Object [])it.next();
		    		ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
		    		jbkEquip.setShipNum(shipNumber);
		    		jbkEquip.setCorpID(sessionCorpID);
		    		jbkEquip.setType(row[0].toString());
		    		jbkEquip.setDescript(row[1].toString());
		    		jbkEquip.setCost(Double.valueOf(row[2].toString()));
		    		try {
		    			double d = Double.valueOf(row[2].toString());
						jbkEquip.setQty((int)d);
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					jbkEquip.setEstHour(Double.valueOf(1));
		    		jbkEquip.setCreatedBy(userName);
		    		jbkEquip.setCreatedOn(new Date());
		    		jbkEquip.setUpdatedBy(userName);
		    		jbkEquip.setUpdatedOn(new Date());
		    		jbkEquip.setDay(String.valueOf(day+1));
		    		jbkEquip.setTicketTransferStatus(false);
		    		jbkEquip.setRevisionInvoice(false);
		    		itemsJbkEquipManager.save(jbkEquip);
    		}
    	}
	}
	public static String findMaxWorkOrder(OperationsIntelligenceManager operationsIntelligenceManager,String shipNumber,String corpId){
		String maxNumber="";
		List<OperationsIntelligence> OIlist=operationsIntelligenceManager.findAllResourcesAjax(shipNumber,corpId,"","true");
		Integer temp=0;
		for(OperationsIntelligence oi:OIlist){
			if(oi.getWorkorder() != null ){
				
			Integer it=0;
			try {
				it = Integer.parseInt(oi.getWorkorder().replaceAll("WO_", ""));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
			}
			
			if(it>temp){
				temp=it;
			}
		}}
		maxNumber=formatOder(++temp);
		return maxNumber;
	}
	public static String formatOder(Integer i){
		String maxNumber="";
		if(i<10){
			maxNumber="0"+i;
		}else{
			maxNumber=i+"";
		}
		maxNumber="WO_"+maxNumber;
		return maxNumber;
	}
	public static void addResourceTemplate1(ItemsJEquipManager itemsJEquipManager,OperationsIntelligenceManager operationsIntelligenceManager,
			String contract,String shipNumber,String sessionCorpID,String userName ,String workOrderForOI){
    	List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
    	//int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
    	if(!resourceList.isEmpty()){
    		Iterator it=resourceList.iterator();
    		while(it.hasNext()){
	    	    Object []row= (Object [])it.next();
	    	    OperationsIntelligence operationsIntelligence = new OperationsIntelligence();
	    	    operationsIntelligence.setShipNumber(shipNumber);
	    	    operationsIntelligence.setCorpID(sessionCorpID);
	    	    operationsIntelligence.setType(row[0].toString());
	    	    operationsIntelligence.setDescription(row[1].toString());
	    	    boolean di = (Boolean) row[4];
	    	    if(di){
	    	    	operationsIntelligence.setDisplayPriority(1);
	    	    	}else{
	    	    		operationsIntelligence.setDisplayPriority(0);	
	    	    	}
	    		try {
	    			double d = Double.valueOf(row[2].toString());
	    			operationsIntelligence.setQuantitiy(new BigDecimal(d));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				double d = Double.valueOf(row[2].toString());
				operationsIntelligence.setEsthours(Double.valueOf(1));
				//Integer estimatedQuantity1=(Integer.valueOf(1)*(int)d);
				BigDecimal estimatedQuantity1 = new BigDecimal(d);
			/*	operationsIntelligence.setEstimatedquantity(new BigDecimal(estimatedQuantity1) );*/
				operationsIntelligence.setEstimatedquantity(estimatedQuantity1);
				 try {
		    			Long d1 = Long.valueOf(row[3].toString());
		    			List contractCharge = itemsJEquipManager.checkContract(d1 ,sessionCorpID ,shipNumber);
		    			if(!contractCharge.isEmpty()){
		    				Iterator it1=contractCharge.iterator();
		    				while(it1.hasNext()){
		    					Object []obj=(Object[]) it1.next();
		    					operationsIntelligence.setEstimatedsellrate(new BigDecimal(obj[0]+""));
		    					operationsIntelligence.setEstimatedbuyrate(new BigDecimal(obj[1]+""));
		    					operationsIntelligence.setRevisionsellrate(new BigDecimal(obj[0]+""));
		    					operationsIntelligence.setRevisionbuyrate(new BigDecimal(obj[1]+""));
		    					/*Integer	tempSellRate = Integer.parseInt(obj[0].toString());
		    					Integer	tempBuyRate = Integer.parseInt(obj[1].toString());*/
		    					//int tempSellRate = Double.valueOf(obj[0].toString()).intValue();
		    					//int tempBuyRate = Double.valueOf(obj[1].toString()).intValue();
		    					
		    					
		    					/*BigDecimal estimatedexpense1 = new BigDecimal(estimatedQuantity1*tempBuyRate);
		    					BigDecimal estimaterevenue1 = new BigDecimal(estimatedQuantity1*tempSellRate);*/
		    					BigDecimal tempSellRate = new BigDecimal(obj[0].toString());
		    					BigDecimal tempBuyRate = new BigDecimal(obj[1].toString());
		    					BigDecimal estimatedexpense1 = estimatedQuantity1.multiply(tempBuyRate);
		    					BigDecimal estimaterevenue1 = estimatedQuantity1.multiply(tempSellRate);
		    					operationsIntelligence.setEstimatedexpense(estimatedexpense1);
		    					operationsIntelligence.setEstimatedrevenue(estimaterevenue1);
		    					
		    				}
		    		
		    		} }catch (NumberFormatException e) {
						e.printStackTrace();
					}
				if(workOrderForOI != null && !workOrderForOI.equalsIgnoreCase("")){
					operationsIntelligence.setWorkorder(workOrderForOI);
				}else{
					operationsIntelligence.setWorkorder("WO_01");
				}
				//operationsIntelligence.setWorkorder(workOrderForOI);
	    	    operationsIntelligenceManager.save(operationsIntelligence);
    		}
    	}
	}
	
	
	public static void findResourceTemplateForAccPortal(ItemsJEquipManager itemsJEquipManager,OperationsIntelligenceManager operationsIntelligenceManager,
			String contract,String shipNumber,String sessionCorpID,String userName ,String workOrderForOI, String sid){
    	List resourceList = itemsJEquipManager.findResourceTemplateForAccPortal(contract,sessionCorpID);
    	//int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
    	if(!resourceList.isEmpty()){
    		Iterator it=resourceList.iterator();
    		while(it.hasNext()){
	    	    Object []row= (Object [])it.next();
	    	    OperationsIntelligence operationsIntelligence = new OperationsIntelligence();
	    	    operationsIntelligence.setShipNumber(shipNumber);
	    	    operationsIntelligence.setServiceOrderId(Long.parseLong(sid));
	    	    operationsIntelligence.setCorpID(sessionCorpID);
	    	    operationsIntelligence.setType(row[0].toString());
	    	    operationsIntelligence.setDescription(row[1].toString());
	    	    boolean di = (Boolean) row[4];
	    	    if(di){
	    	    	operationsIntelligence.setDisplayPriority(1);
	    	    	}else{
	    	    		operationsIntelligence.setDisplayPriority(0);	
	    	    	}
	    		try {
	    			double d = Double.valueOf(row[2].toString());
	    			operationsIntelligence.setQuantitiy(new BigDecimal((int)d));
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				double d = Double.valueOf(row[2].toString());
				operationsIntelligence.setEsthours(Double.valueOf(1));
				Integer estimatedQuantity1=(Integer.valueOf(1)*(int)d);
				operationsIntelligence.setEstimatedquantity(new BigDecimal(estimatedQuantity1));
				 try {
		    			Long d1 = Long.valueOf(row[3].toString());
		    			List contractCharge = itemsJEquipManager.checkContract(d1 ,sessionCorpID ,shipNumber);
		    			if(!contractCharge.isEmpty()){
		    				Iterator it1=contractCharge.iterator();
		    				while(it1.hasNext()){
		    					Object []obj=(Object[]) it1.next();
		    					operationsIntelligence.setEstimatedsellrate(new BigDecimal(obj[0]+""));
		    					operationsIntelligence.setEstimatedbuyrate(new BigDecimal(obj[1]+""));
		    					operationsIntelligence.setRevisionsellrate(new BigDecimal(obj[0]+""));
		    					operationsIntelligence.setRevisionbuyrate(new BigDecimal(obj[1]+""));
		    					/*Integer	tempSellRate = Integer.parseInt(obj[0].toString());
		    					Integer	tempBuyRate = Integer.parseInt(obj[1].toString());*/
		    					int tempSellRate = Double.valueOf(obj[0].toString()).intValue();
		    					int tempBuyRate = Double.valueOf(obj[1].toString()).intValue();
		    					BigDecimal estimatedexpense1 = new BigDecimal(estimatedQuantity1*tempBuyRate);
		    					BigDecimal estimaterevenue1 = new BigDecimal(estimatedQuantity1*tempSellRate);
		    					operationsIntelligence.setEstimatedexpense(estimatedexpense1);
		    					operationsIntelligence.setEstimatedrevenue(estimaterevenue1);
		    					
		    				}
		    		
		    		} }catch (NumberFormatException e) {
						e.printStackTrace();
					}
				
				operationsIntelligence.setWorkorder(workOrderForOI);
	    	    operationsIntelligenceManager.save(operationsIntelligence);
    		}
    	}
	}
	
	
	 public static void addRowToResourceTemplateGrid(ItemsJbkEquipManager itemsJbkEquipManager,String shipNumber,
			 String sessionCorpID,String userName,String day,String type) {
		 	List<ItemsJbkEquip> jbk = itemsJbkEquipManager.findByShipNumAndDay(shipNumber, day);
		 	
		 	ItemsJbkEquip jbkEquip = new ItemsJbkEquip();
		 	jbkEquip.setType(type);
		 	jbkEquip.setShipNum(shipNumber);
		 	jbkEquip.setCorpID(sessionCorpID);
		 	jbkEquip.setCost(Double.valueOf(0));		 	
		 	jbkEquip.setReturned(Double.valueOf(0));
		 	jbkEquip.setActualQty(Integer.valueOf(0));
		 	jbkEquip.setActual(Double.valueOf(0));		 	
		 	jbkEquip.setQty(0);
		 	jbkEquip.setDay(day);
		 	jbkEquip.setCreatedBy(userName);
	 		jbkEquip.setCreatedOn(new Date());
	 		jbkEquip.setUpdatedBy(userName);
	 		jbkEquip.setUpdatedOn(new Date());
	 		jbkEquip.setTicketTransferStatus(false);
	 		jbkEquip.setRevisionInvoice(false);
	 		jbkEquip.setEstHour(Double.valueOf(1));
 		
	 		for (ItemsJbkEquip itemsJbkEquip : jbk) {
	 			jbkEquip.setBeginDate(itemsJbkEquip.getBeginDate());
	 			jbkEquip.setEndDate(itemsJbkEquip.getEndDate());
	 			break;
			}
	    	itemsJbkEquipManager.save(jbkEquip); 
	    }
	 
	 public static void addResourceCWMSTemplate1(ItemsJEquipManager itemsJEquipManager,OperationsIntelligenceManager operationsIntelligenceManager,
				String contract,String shipNumber,String sessionCorpID,String userName ,String  distinctWorkOrder,String action,String usertype){
	    	List resourceList = itemsJEquipManager.findResourceTemplate(contract,sessionCorpID);
	    	
			System.out.println("resourceList----"+resourceList);
			Integer workOrderIntValue;
    		String woNumberFromOI = distinctWorkOrder.split("_")[1];
		
		
		if(action.equalsIgnoreCase("Before"))
		{ workOrderIntValue = Integer.parseInt(woNumberFromOI)-1;
			 
		     if(workOrderIntValue==0)
		         {
				 workOrderIntValue = 1;
	              }
			 else
			 {
				 workOrderIntValue = Integer.parseInt(woNumberFromOI)-1;
			 }
		}
		else
		{		 workOrderIntValue = Integer.parseInt(woNumberFromOI)+1;
		
        }
    	Integer woValue = Integer.parseInt(woNumberFromOI);
	    	//int day = itemsJbkEquipManager.findDayTemplate(shipNumber,sessionCorpID);
	    	if(!resourceList.isEmpty()){
	    		Iterator it=resourceList.iterator();
	    		while(it.hasNext()){
		    	    Object []row= (Object [])it.next();
		    	    OperationsIntelligence operationsIntelligence = new OperationsIntelligence();
		    	    operationsIntelligence.setShipNumber(shipNumber);
		    	    operationsIntelligence.setCorpID(sessionCorpID);
		    	    operationsIntelligence.setType(row[0].toString());
		    	    operationsIntelligence.setDescription(row[1].toString());
		    	    boolean di = (Boolean) row[4];
		    	    if(di){
		    	    	operationsIntelligence.setDisplayPriority(1);
		    	    	}else{
		    	    		operationsIntelligence.setDisplayPriority(0);	
		    	    	}
		    		try {
		    			double d = Double.valueOf(row[2].toString());
		    			operationsIntelligence.setQuantitiy(new BigDecimal(d));
					} catch (NumberFormatException e) {
						e.printStackTrace();
					}
					double d = Double.valueOf(row[2].toString());
					operationsIntelligence.setEsthours(Double.valueOf(1));
					//Integer estimatedQuantity1=(Integer.valueOf(1)*(int)d);
					BigDecimal estimatedQuantity1 = new BigDecimal(d);
				/*	operationsIntelligence.setEstimatedquantity(new BigDecimal(estimatedQuantity1) );*/
					operationsIntelligence.setEstimatedquantity(estimatedQuantity1);
					 try {
			    			Long d1 = Long.valueOf(row[3].toString());
			    			List contractCharge = itemsJEquipManager.checkContract(d1 ,sessionCorpID ,shipNumber);
			    			if(!contractCharge.isEmpty()){
			    				Iterator it1=contractCharge.iterator();
			    				while(it1.hasNext()){
			    					Object []obj=(Object[]) it1.next();
			    					operationsIntelligence.setEstimatedsellrate(new BigDecimal(obj[0]+""));
			    					operationsIntelligence.setEstimatedbuyrate(new BigDecimal(obj[1]+""));
			    					operationsIntelligence.setRevisionsellrate(new BigDecimal(obj[0]+""));
			    					operationsIntelligence.setRevisionbuyrate(new BigDecimal(obj[1]+""));
			    					/*Integer	tempSellRate = Integer.parseInt(obj[0].toString());
			    					Integer	tempBuyRate = Integer.parseInt(obj[1].toString());*/
			    					//int tempSellRate = Double.valueOf(obj[0].toString()).intValue();
			    					//int tempBuyRate = Double.valueOf(obj[1].toString()).intValue();
			    					
			    					
			    					/*BigDecimal estimatedexpense1 = new BigDecimal(estimatedQuantity1*tempBuyRate);
			    					BigDecimal estimaterevenue1 = new BigDecimal(estimatedQuantity1*tempSellRate);*/
			    					BigDecimal tempSellRate = new BigDecimal(obj[0].toString());
			    					BigDecimal tempBuyRate = new BigDecimal(obj[1].toString());
			    					BigDecimal estimatedexpense1 = estimatedQuantity1.multiply(tempBuyRate);
			    					BigDecimal estimaterevenue1 = estimatedQuantity1.multiply(tempSellRate);
			    					operationsIntelligence.setEstimatedexpense(estimatedexpense1);
			    					operationsIntelligence.setEstimatedrevenue(estimaterevenue1);
			    					
			    				}
			    		
			    		} }catch (NumberFormatException e) {
							e.printStackTrace();
						}
			    		
			    		if(workOrderIntValue <10){
			    			operationsIntelligence.setWorkorder("WO_0"+workOrderIntValue.intValue());
			    			}else{
			    				operationsIntelligence.setWorkorder("WO_"+workOrderIntValue.intValue());	
			    			}
					//operationsIntelligence.setWorkorder(workOrderForOI);
		    	    operationsIntelligenceManager.save(operationsIntelligence);
	    		}
	    	}
		}
		
	 
}
