package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;   
import com.trilasoft.app.model.Person;   
  
import java.util.List;   
  
public interface PersonDao extends GenericDao<Person, Long> {   
    public List<Person> findByLastName(String lastName);   
}  
