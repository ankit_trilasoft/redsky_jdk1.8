<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title>Standard Deductions List</title>   
    <meta name="heading" content="Standard Deductions List"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 
<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Standard Deductions?");
	var did = targetElement;
	if (agree){
		location.href="deleteStandardDeductions.html?partnerId=${partner.id}&id="+did;
	}else{
		return false;
	}
}
		
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="OO" />
<c:set var="ppType" value="OO"/>
<s:form id="standardDeductionListForm" action="" method="post" validate="true">	
<s:hidden name="partner.id"/>
<div id="newmnav">
				  <ul>
				  	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=OO"><span>Partner Detail</span></a></li>
				    <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=OO"><span>Acct Ref</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Standard Deductions</span></a></li>
					<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<c:if test='${paramValue == "View"}'>
						<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
					</c:if>
				  </ul>
		</div>
		<div style="width: 100%" >
		<div class="spn">&nbsp;</div>
		
		</div>
		<div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
			<tr>
		  		<td align="right" class="listwhitetext">Code</td>
				<td><s:textfield key="partner.partnerCode" required="true" readonly="true" cssClass="text medium"/></td>
				<td align="right" class="listwhitetext" width="70px">Name</td>
				<td><s:textfield key="partner.lastName" required="true" size="35" readonly="true" cssClass="text medium"/></td>
			</tr>
		</table>
	</div>
<div class="bottom-header" style="margin-top:35px;"><span></span></div>
</div>
</div>
<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:70px; height:25px" onclick="location.href='<c:url value="/editStandardDeduction.html?partnerId=${partner.id}"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="standardDeduction" value="standardDeduction" scope="request"/>  

				<display:table name="standardDeduction" class="table" requestURI="" id="standardDeductionList" export="true" defaultsort="1" pagesize="10" style="width: 100%; " >   
					<display:column property="chargeCode" sortable="true" style="width: 90px;" href="editStandardDeduction.html?partnerId=${partner.id}" paramId="id" paramProperty="id" title="Charge Code" />
					<display:column property="chargeCodeDesc" sortable="true" title="Description" style="width: 190px;"/>
					<display:column property="status" sortable="true" title="Status"/>
					<display:column sortable="true" title="Amount" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionList.amount}" /></div>
                  	</display:column>
					<display:column sortable="true" title="Annual Int. Rate %" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionList.rate}" /></div>
					</display:column>
					<display:column sortable="true" title="Amt. Deduction" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionList.deduction}" /></div>
					</display:column>
					<display:column sortable="true" title="Max Amt. Deduction" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionList.maxDeductAmt}" /></div>
					</display:column>
					<display:column sortable="true" title="Amt. Deducted Till Date" headerClass="containeralign">
						<div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" value="${standardDeductionList.amtDeducted}" /></div>
					</display:column>
					<display:column property="lastDeductionDate" sortable="true" title="Last Deduction Date" style="width:105px" format="{0,date,dd-MMM-yyyy}"/>
					
				    <display:setProperty name="paging.banner.item_name" value="Standard Deductions"/>   
				    <display:setProperty name="paging.banner.items_name" value="Standard Deductions"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Standard Deductions List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Standard Deductions List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Standard Deductions List.pdf"/>   
				</display:table>  
				
 			</td>
		</tr>
	</tbody>
</table> 
<table> 
<c:out value="${buttons}" escapeXml="false" /> 

<tr><td style="height:70px; !height:100px"></td></tr></table>
</div>
</s:form> 
<script type="text/javascript">   
	<c:if test="${hitflag == 1}" >
		<c:redirect url="/standardDeductionsList.html?id=${partner.id}" ></c:redirect>
	</c:if>

</script> 
