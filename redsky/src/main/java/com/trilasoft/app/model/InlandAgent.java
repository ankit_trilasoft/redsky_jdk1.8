package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="inlandagent")
public class InlandAgent extends BaseObject {
	
	private String corpID;
	private Long id;
	private Long serviceOrderId;
	private String sequenceNumber;
	private String ship;
	private String shipNumber;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn; 
	private String vendorId;
    private String vendorName;
    private String grade;
    private String containerType;
    private Date freeDatewithContainer;
    private Boolean chasisProvide=false;
    private String otherChargesDesc;
    private Date sentTOFPickUp;
    private Date freeDateAtPort;
    private Date outOfPort;
    private Date requestPickDate;
    private Date actualPickDate;
    private Date requestDropDate;
    private Date actualDropDate;
    private Date sentTruckingOrderforReturn;
    private Date returntoPortActual;
    private Date liveLoadDate;
    private Date liveUnLoadDate;
    private Double otherChargesAmount;
    private Double inLandTotalAmount;
    private Double chasisfreeDaysSubTotal;
    private Double chasisfreeDaysTotal;
    private Integer containerDays;
    private Integer containerFreeDays;
    private Double containerFreeDaysSubTotal;
    private Double containerFreeDaysTotal;
    private Integer chasisDays;
    private Integer chasisfreeDays;
    private ServiceOrder serviceOrder;
    private String idNumber;
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("id",id)
				.append("serviceOrderId", serviceOrderId)
				.append("sequenceNumber", sequenceNumber)
				.append("ship", ship).append("shipNumber", shipNumber)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("vendorId", vendorId)
				.append("vendorName", vendorName)
				.append("grade", grade)
				.append("containerType", containerType)
				.append("freeDatewithContainer", freeDatewithContainer)
				.append("chasisProvide", chasisProvide)
				.append("otherChargesDesc", otherChargesDesc)
				.append("sentTOFPickUp", sentTOFPickUp)
				.append("freeDateAtPort", freeDateAtPort)
				.append("outOfPort", outOfPort)
				.append("requestPickDate", requestPickDate)
				.append("actualPickDate", actualPickDate)
				.append("requestDropDate", requestDropDate)
				.append("actualDropDate", actualDropDate)
				.append("sentTruckingOrderforReturn", sentTruckingOrderforReturn)
				.append("returntoPortActual", returntoPortActual)
				.append("liveLoadDate", liveLoadDate)
				.append("liveUnLoadDate", liveUnLoadDate)
				.append("otherChargesAmount", otherChargesAmount)
				.append("inLandTotalAmount", inLandTotalAmount)
				.append("chasisfreeDaysSubTotal", chasisfreeDaysSubTotal)
				.append("chasisfreeDaysTotal", chasisfreeDaysTotal)
				.append("containerDays", containerDays)
				.append("containerFreeDays", containerFreeDays)
				.append("containerFreeDaysSubTotal", containerFreeDaysSubTotal)
				.append("containerFreeDaysTotal", containerFreeDaysTotal)
				.append("chasisDays", chasisDays)
				.append("chasisfreeDays", chasisfreeDays)
				.append("serviceOrder", serviceOrder)
				.append("idNumber", idNumber)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof InlandAgent))
			return false;
		InlandAgent castOther = (InlandAgent) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id,castOther.id)
				.append(serviceOrderId, castOther.serviceOrderId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship,castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(createdBy,castOther.createdBy)
				.append(updatedBy,castOther.updatedBy)
				.append(createdOn,castOther.createdOn)
				.append(updatedOn,castOther.updatedOn)
				.append(vendorId, castOther.vendorId)
				.append(vendorName, castOther.vendorName)
				.append(grade, castOther.grade)
				.append(containerType, castOther.containerType)
				.append(freeDatewithContainer, castOther.freeDatewithContainer)
				.append(chasisProvide, castOther.chasisProvide)
				.append(otherChargesDesc, castOther.otherChargesDesc)
				.append(sentTOFPickUp, castOther.sentTOFPickUp)
				.append(freeDateAtPort, castOther.freeDateAtPort)
				.append(outOfPort, castOther.outOfPort)
				.append(requestPickDate, castOther.requestPickDate)
				.append(actualPickDate, castOther.actualPickDate)
				.append(requestDropDate, castOther.requestDropDate)
				.append(actualDropDate, castOther.actualDropDate)
				.append(sentTruckingOrderforReturn, castOther.sentTruckingOrderforReturn)
				.append(returntoPortActual, castOther.returntoPortActual)
				.append(liveLoadDate, castOther.liveLoadDate)
				.append(liveUnLoadDate, castOther.liveUnLoadDate)
				.append(otherChargesAmount, castOther.otherChargesAmount)
				.append(inLandTotalAmount, castOther.inLandTotalAmount)
				.append(chasisfreeDaysSubTotal, castOther.chasisfreeDaysSubTotal)
				.append(chasisfreeDaysTotal, castOther.chasisfreeDaysTotal)
				.append(containerDays, castOther.containerDays)
				.append(containerFreeDays, castOther.containerFreeDays)
				.append(containerFreeDaysSubTotal, castOther.containerFreeDaysSubTotal)
				.append(containerFreeDaysTotal, castOther.containerFreeDaysTotal)
				.append(chasisDays, castOther.chasisDays)
				.append(chasisfreeDays, castOther.chasisfreeDays)
				.append(serviceOrder, castOther.serviceOrder)
				.append(idNumber, castOther.idNumber)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID)
				.append(id).append(serviceOrderId).append(sequenceNumber).append(ship).append(
				shipNumber).append(createdBy).append(updatedBy).append(createdOn).append(updatedOn)
				.append(vendorId)
				.append(vendorName)
				.append(grade)
				.append(containerType)
				.append(freeDatewithContainer)
				.append(chasisProvide)
				.append(otherChargesDesc)
				.append(sentTOFPickUp)
				.append(freeDateAtPort)
				.append(outOfPort)
				.append(requestPickDate)
				.append(actualPickDate)
				.append(requestDropDate)
				.append(actualDropDate)
				.append(sentTruckingOrderforReturn)
				.append(returntoPortActual)
				.append(liveLoadDate)
				.append(liveUnLoadDate)
				.append(otherChargesAmount)
				.append(inLandTotalAmount)
				.append(chasisfreeDaysSubTotal)
				.append(chasisfreeDaysTotal)
				.append(containerDays)
				.append(containerFreeDays)
				.append(containerFreeDaysSubTotal)
				.append(containerFreeDaysTotal)
				.append(chasisDays)
				.append(chasisfreeDays)
				.append(serviceOrder)
				.append(idNumber)
				.toHashCode();
	}
	
	@ManyToOne
	@JoinColumn(name="serviceOrderId", nullable=false, updatable=false,
			referencedColumnName="id")
			public ServiceOrder getServiceOrder() {
			        return serviceOrder;
			    }
			    
				 public void setServiceOrder(ServiceOrder serviceOrder) {
						this.serviceOrder = serviceOrder;
				}
	
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=20, insertable=false, updatable=false)
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getVendorId() {
		return vendorId;
	}
	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}
	@Column
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	@Column
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	@Column
	public String getContainerType() {
		return containerType;
	}
	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}
	@Column
	public Date getFreeDatewithContainer() {
		return freeDatewithContainer;
	}
	public void setFreeDatewithContainer(Date freeDatewithContainer) {
		this.freeDatewithContainer = freeDatewithContainer;
	}
	@Column
	public Boolean getChasisProvide() {
		return chasisProvide;
	}
	public void setChasisProvide(Boolean chasisProvide) {
		this.chasisProvide = chasisProvide;
	}
	@Column
	public String getOtherChargesDesc() {
		return otherChargesDesc;
	}
	public void setOtherChargesDesc(String otherChargesDesc) {
		this.otherChargesDesc = otherChargesDesc;
	}
	@Column
	public Date getSentTOFPickUp() {
		return sentTOFPickUp;
	}
	public void setSentTOFPickUp(Date sentTOFPickUp) {
		this.sentTOFPickUp = sentTOFPickUp;
	}
	@Column
	public Date getFreeDateAtPort() {
		return freeDateAtPort;
	}
	public void setFreeDateAtPort(Date freeDateAtPort) {
		this.freeDateAtPort = freeDateAtPort;
	}
	@Column
	public Date getOutOfPort() {
		return outOfPort;
	}
	public void setOutOfPort(Date outOfPort) {
		this.outOfPort = outOfPort;
	}
	@Column
	public Date getRequestPickDate() {
		return requestPickDate;
	}
	public void setRequestPickDate(Date requestPickDate) {
		this.requestPickDate = requestPickDate;
	}
	@Column
	public Date getActualPickDate() {
		return actualPickDate;
	}
	public void setActualPickDate(Date actualPickDate) {
		this.actualPickDate = actualPickDate;
	}
	@Column
	public Date getRequestDropDate() {
		return requestDropDate;
	}
	public void setRequestDropDate(Date requestDropDate) {
		this.requestDropDate = requestDropDate;
	}
	@Column
	public Date getActualDropDate() {
		return actualDropDate;
	}
	public void setActualDropDate(Date actualDropDate) {
		this.actualDropDate = actualDropDate;
	}
	@Column
	public Date getSentTruckingOrderforReturn() {
		return sentTruckingOrderforReturn;
	}
	public void setSentTruckingOrderforReturn(Date sentTruckingOrderforReturn) {
		this.sentTruckingOrderforReturn = sentTruckingOrderforReturn;
	}
	@Column
	public Date getReturntoPortActual() {
		return returntoPortActual;
	}
	public void setReturntoPortActual(Date returntoPortActual) {
		this.returntoPortActual = returntoPortActual;
	}
	@Column
	public Date getLiveLoadDate() {
		return liveLoadDate;
	}
	public void setLiveLoadDate(Date liveLoadDate) {
		this.liveLoadDate = liveLoadDate;
	}
	@Column
	public Date getLiveUnLoadDate() {
		return liveUnLoadDate;
	}
	public void setLiveUnLoadDate(Date liveUnLoadDate) {
		this.liveUnLoadDate = liveUnLoadDate;
	}
	@Column
	public Double getOtherChargesAmount() {
		return otherChargesAmount;
	}
	public void setOtherChargesAmount(Double otherChargesAmount) {
		this.otherChargesAmount = otherChargesAmount;
	}
	@Column
	public Double getInLandTotalAmount() {
		return inLandTotalAmount;
	}
	public void setInLandTotalAmount(Double inLandTotalAmount) {
		this.inLandTotalAmount = inLandTotalAmount;
	}
	@Column
	public Double getChasisfreeDaysSubTotal() {
		return chasisfreeDaysSubTotal;
	}
	public void setChasisfreeDaysSubTotal(Double chasisfreeDaysSubTotal) {
		this.chasisfreeDaysSubTotal = chasisfreeDaysSubTotal;
	}
	@Column
	public Double getChasisfreeDaysTotal() {
		return chasisfreeDaysTotal;
	}
	public void setChasisfreeDaysTotal(Double chasisfreeDaysTotal) {
		this.chasisfreeDaysTotal = chasisfreeDaysTotal;
	}
	@Column
	public Integer getContainerDays() {
		return containerDays;
	}
	public void setContainerDays(Integer containerDays) {
		this.containerDays = containerDays;
	}
	@Column
	public Integer getContainerFreeDays() {
		return containerFreeDays;
	}
	public void setContainerFreeDays(Integer containerFreeDays) {
		this.containerFreeDays = containerFreeDays;
	}
	@Column
	public Double getContainerFreeDaysSubTotal() {
		return containerFreeDaysSubTotal;
	}
	public void setContainerFreeDaysSubTotal(Double containerFreeDaysSubTotal) {
		this.containerFreeDaysSubTotal = containerFreeDaysSubTotal;
	}
	@Column
	public Double getContainerFreeDaysTotal() {
		return containerFreeDaysTotal;
	}
	public void setContainerFreeDaysTotal(Double containerFreeDaysTotal) {
		this.containerFreeDaysTotal = containerFreeDaysTotal;
	}
	@Column
	public Integer getChasisDays() {
		return chasisDays;
	}
	public void setChasisDays(Integer chasisDays) {
		this.chasisDays = chasisDays;
	}
	@Column
	public Integer getChasisfreeDays() {
		return chasisfreeDays;
	}
	public void setChasisfreeDays(Integer chasisfreeDays) {
		this.chasisfreeDays = chasisfreeDays;
	}
	@Column
	public String getIdNumber() {
		return idNumber;
	}
	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

}
