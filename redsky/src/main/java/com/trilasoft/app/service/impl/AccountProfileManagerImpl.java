package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccountProfileDao;
import com.trilasoft.app.model.AccountProfile;
import com.trilasoft.app.service.AccountProfileManager;

public class AccountProfileManagerImpl extends GenericManagerImpl<AccountProfile, Long> implements AccountProfileManager {
	AccountProfileDao accountProfileDao;   
	public AccountProfileManagerImpl(AccountProfileDao accountProfileDao) {   
        super(accountProfileDao);   
        this.accountProfileDao = accountProfileDao;   
    } 
	
	public List<AccountProfile> findAccountProfileByPC(String partnerCode, String corpID){
		return accountProfileDao.findAccountProfileByPC(partnerCode, corpID);
	}
    
	public List findMaximumId(){
		return accountProfileDao.findMaximumId();
	}

	public List getRelatedNotesForProfile(String corpID, String partnerCode) {
		return accountProfileDao.getRelatedNotesForProfile(corpID, partnerCode);
	}
	public List findAccountProfile(String corpID, String partnerCode){
		return accountProfileDao.findAccountProfile(corpID, partnerCode);	
	}
	public List getAccountOwner(String corpID) {
		return accountProfileDao.getAccountOwner(corpID);
	}
}
