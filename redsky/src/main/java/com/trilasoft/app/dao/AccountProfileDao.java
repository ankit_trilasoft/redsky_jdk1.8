package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.AccountProfile;

public interface AccountProfileDao extends GenericDao<AccountProfile, Long> {
	
	public List<AccountProfile> findAccountProfileByPC(String partnerCode, String corpID);
	
	public List findMaximumId();
	public List getRelatedNotesForProfile(String corpID, String partnerCode);
	public List findAccountProfile(String corpID, String partnerCode);
	public List getAccountOwner(String corpID);

}
