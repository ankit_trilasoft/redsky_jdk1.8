<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>User Guide</title> 
<style>

body{font-family:Arial, Helvetica, sans-serif;margin:0px;}
#wrapper-main {width:998px;margin:0px auto;border:1px solid #c7c6c6;}
#guide-left {width:241px;min-height:805px;margin:0px;float:left;background:url(images/blue-left.png) repeat-y;clear:both;}
#guide-right {width:757px;margin:0px;float:left;}
#guide-top {width:757px; height:71px;margin:0px;margin-bottom:8px;float:left;background:url(images/uguide-top.png) repeat-x; border-bottom: 1px solid silver;}
#guide-top h1 {text-align:center;color:#060202; font-size:32px;font-family:arial; margin-top:10px;}
#guide-left h2 {text-align:center;color:#6f6f6f;margin:0px;}
#guide-middle {background:url(images/uguide-bg.png) no-repeat; width:768px;height:auto;clear:both;padding-left:3px;padding-top:7px;}
.listblacktextLarge{
    color: #464646;
    font-family: arial,verdana;
    font-size: 14px;
    font-weight: normal;
    text-decoration: none;
	background-color:#f8f8f8;
	padding:4px 0px;
	}
.table th.containeraligncenter{text-align:center;}
ul.guidemenu {
padding: 0;
margin: 0;
list-style-type: none;
}
ul.guidemenu li a{
font: 13px Arial;
padding: 8px 3px;
display: block;
width: 168px; /*Define width for IE6's sake*/
color: #1c78ea;
padding-left:60px;
text-decoration: underline;
background-color:#eeeeee;
list-style-type: none;
border-bottom:1px solid #dcdcdc;
}
ul.guidemenu li a.active {
    background:url("images/guidemenu-act.png") repeat-x scroll 50% 50% transparent;
    }

.guidemenu a:visited{
color: #1c78ea;
}
p{margin:0px;padding:0px;}
html>body .guidemenu a{ /*Non IE rule*/
width: 168px;
}

.guidemenu a:hover{
background-color: #f3f3f3;
color: #1c78ea;
}
#mainPopup {
    padding:2px 0px 0px 2px;    
}

#guideSearchBox {
margin:45px 0 10px 0px;
margin-left:520px; 
z-index:1;
position:absolute;
}

#searchBox {
    background-image: url("images/searchblue-left.png");
    background-position: right top;
    background-repeat: no-repeat;
    float: left;
    height: 23px;    
    padding: 0;
    width: 158px;
}

.ajaxSearch_input {
    background-color: transparent;
    border: medium none;
    color: #444444;
    float: left;
    font-size: 11px;
    height: 17px;
    margin: 2px 2px 0 14px;
    width: 132px;
}
#searchBox .ajaxSearch_input:hover{
    background-color: transparent;       
}
input[type=text]:hover
{
  background: none;
}
input::-webkit-input-placeholder {
    color:#999;
}
input:-moz-placeholder {
    color:#999;
}
input:-ms-input-placeholder {
    color:#999;
}
.ajaxSearch_submit {
    margin: 5px 0 0;
    width: 25px;
    border:none;
}
span.pagelinks {width:92%;padding-bottom:2px;}
form li {line-height:0px;padding:0px;}
</style>
</head>
<s:form name="userGuidesForm" id="userGuidesForm" action="salesUserModuleBy.html?decorator=popup&popup=true" method="post" validate="true">
<div id="wrapper-main">
 
<div id="guide-left">
<img src="images/logo_redsky.png" border="0" align="middle" style="padding:6px 0px 0px 60px;"/>
<h2>List&nbsp;of&nbsp;Modules</h2>
<ul class="guidemenu">
<c:forEach  items="${userGuidesModule}" var="moduleItr" varStatus="rowCounter">
<c:choose>
<c:when test="${user.userType=='USER' && moduleItr.key!='Portals'}">
<li>
		<c:if test="${moduleItr.key == userGuideModule }">
			<a id="module${rowCounter.count}" class="active" href="salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=${moduleItr.key}">
				<c:out value="${moduleItr.key}"></c:out>
			</a>
		</c:if>
		<c:if test="${moduleItr.key != userGuideModule }">
			<a id="module${rowCounter.count}" href="salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=${moduleItr.key}">
				<c:out value="${moduleItr.key}"></c:out>
			</a>
		</c:if>
	</li>
</c:when>

<c:when  test="${(user.userType=='AGENT' || user.userType=='ACCOUNT' || user.userType=='PARTNER' ) && moduleItr.key=='Portals'}">
<li><c:if test="${moduleItr.key == userGuideModule }">
			<a id="module${rowCounter.count}" class="active" href="salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=${moduleItr.key}">
				<c:out value="${moduleItr.key}"></c:out>
			</a>
	</c:if>
	<c:if test="${moduleItr.key != userGuideModule }">
			<a id="module${rowCounter.count}" href="salesUserModuleBy.html?decorator=popup&popup=true&userGuideModule=${moduleItr.key}">
				<c:out value="${moduleItr.key}"></c:out>
			</a>
	</c:if>
</li>
</c:when>
<c:otherwise></c:otherwise>
 </c:choose>
	</c:forEach>
</ul>
</div>
<div id="guide-right">
<div id="guideSearchBox">
<div id="searchBox"> 
 <input type="text" value=""  placeholder="Quick Search..." class="ajaxSearch_input" id="docSearch" onkeyup="validCheck(this,'special1')"/>
  </div>
 <img src="${pageContext.request.contextPath}/images/searchblue-right.png" border="0" style="vertical-align:bottom;" onclick="return showDocumentName();" />
</div>
<div id="guide-top">
<h1>User Guide<h1>
</div>
<c:forEach  items="${userGuidesModule}" var="moduleItr" varStatus="rowCounter">
<c:if test="${moduleItr.key == userGuideModule }">
<span style="margin:5px 0px 0px 5px;"><font size=2.5><b>Module �  <c:out value="${moduleItr.key}"></c:out>
</b></font></span></c:if>
</c:forEach>

<div id="guide-middle" >
<s:set name="userModuleGuidesList" value="userModuleGuidesList" scope="request"/>  
	<display:table name="userModuleGuidesList" class="table" requestURI="" id="userModuleGuidesListId"  pagesize="15" style="width:92%;" >   
	<c:if test="${userModuleGuidesListId.videoFlag!=null && userModuleGuidesListId.videoFlag ==false }">  
	<display:column title="Document/&nbsp;Video&nbsp;Name" property="documentName"  sortable="true" style="width:200px;"/>
	</c:if>
	<c:if test="${userModuleGuidesListId.videoFlag!=null && userModuleGuidesListId.videoFlag ==true }">  
	<display:column title="Document/&nbsp;Video&nbsp;Name" property="documentName"  sortable="true" style="width:200px;"/>
	</c:if>
   
    <c:if test="${userModuleGuidesListId.videoFlag!=null && userModuleGuidesListId.videoFlag ==true }">
    <display:column title="Download"  style="width:50px;text-align:center;" maxLength="30" headerClass="containeraligncenter">
    <!-- <a><img src="images/videoicon.png" border="0" align="middle" style="cursor:pointer;"/></a>  --> 
    </display:column>
    </c:if>
    <c:if test="${userModuleGuidesListId.videoFlag!=null && userModuleGuidesListId.videoFlag ==false}">
    <display:column title="Download" style="width:50px;text-align:center;" maxLength="30" headerClass="containeraligncenter">
	 <a onclick="javascript:openWindow('UserGuidesResourceMgmtImageServletAction.html?id=${userModuleGuidesListId.id}&decorator=popup&popup=true',900,600);">
    <img src="images/downarrow.png" border="0" align="middle" style="cursor:pointer;"/></a>  
    </display:column>
    </c:if>
    </display:table>
</div> 
</div>
<div style="clear:both;"></div>
</div>
</s:form>
<script type="text/javascript">
function showDocumentName(){
	var myText = document.getElementById('docSearch').value;
	location.href='searchUserGuidesDocumentType.html?decorator=popup&popup=true&userGuideModule&mySearchText='+myText;
	alert("In Progress....");
	//return;
	
}
</script>
<script type="text/javascript">
var r1={
		 'special1':/['\#'&'\$'&'\~'&'\!'&'\@'&'\+'&'\\'&'\/'&'\%'&'\^'&'\&'&'\*'&'\:'&'\;'&'\>'&'\<'&'\?'&'\{'&'\}'&'\|'&'\['&'\]'&'\,'&'\`'&'\='&'('&'\)']/g,
		 'quotes':/['\''&'\"']/g,
		 'notnumbers':/[^\d]/g
		};
		
function validCheck(targetElement,w){
	 targetElement.value = targetElement.value.replace(r1[w],'');
	}
</script>
<script type="text/javascript">
 window.onkeypress = getActiveHTMLElement;
 function getActiveHTMLElement(keyevent) {
  keyevent = (keyevent) ? keyevent : ((window.event) ? event : null);
  if (keyevent) {
  if(keyevent.keyCode == 13) {
  if(document.activeElement.id =='docSearch'){
	  showDocumentName();
	 }
	
   }
  }
 }
</script>