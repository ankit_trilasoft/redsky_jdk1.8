// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`company` ADD COLUMN `rulesRunning` BIT(1) DEFAULT b'0' AFTER `rate2`;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `localName` VARCHAR(95) DEFAULT NULL AFTER `converted`;

ALTER TABLE `redsky`.`serviceorder` CHANGE COLUMN `firstName` `firstName` VARCHAR(80) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NULL DEFAULT NULL  ;

update rolebased_comp_permission set role=replace(role,'ATL','AIF') where corpid='CWMS' and role='ROLE_EMPLOYEE_ATL';

update defaultaccountline set contract='INTL - AIF' where corpid='CWMS' and contract='INTL - ATL';

update accountline set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update app_user set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update categoryrevenue set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update commission set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update commissionlocking set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update contract set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update crew set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update customerfile set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update defaultaccountline set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update mss set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update partner set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update partneraccountref set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update partnerprivate set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update payrollallocation set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update payrollsharing set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update redskybilling set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update refjobtype set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update reports set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update serviceorder set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update subcontractorcharges set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update workticket set companydivision='AIF' where corpid='CWMS' and companydivision='ATL';

update accountline set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update app_user set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update billing set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update charges set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update contract set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update costing set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update customerfile set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update defaultaccountline set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update drivercommissionplan set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update glcommission set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update itemsjequip set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update partnerrategridcontracts set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update pricingcontrol set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update serviceorder set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update timesheet set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update track set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update uvlcontracttemp set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update workticket set contract=replace(contract,'ATL','AIF') where contract like '%ATL%' and corpid='CWMS';

update todorule set expression =replace(expression,'ATL','AIF') where corpid='CWMS' and expression like '%\'ATL\'%';

