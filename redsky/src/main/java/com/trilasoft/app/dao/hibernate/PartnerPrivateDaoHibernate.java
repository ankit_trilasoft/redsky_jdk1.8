package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.PartnerPrivateDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.SystemDefault;

public class PartnerPrivateDaoHibernate extends GenericDaoHibernate<PartnerPrivate, Long> implements PartnerPrivateDao {
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	
	public PartnerPrivateDaoHibernate() {
		super(PartnerPrivate.class);
	}
	
	public PartnerPrivate findPartnerPrivateByCode(String partnerCode,String sessionCorpID){
		List list = this.getSession().createSQLQuery("select id from partnerprivate where partnercode='" + partnerCode +"' and corpid='" + sessionCorpID +"'").list();
		PartnerPrivate partnerPrivate;
		if(!list.isEmpty())
			partnerPrivate = this.get(Long.parseLong(list.get(0).toString()));
		else
			partnerPrivate = new PartnerPrivate();
		return partnerPrivate;
	}
	
	public List findMaxByCode() {
		Long l = Long.parseLong((getHibernateTemplate().find("select max(id) from PartnerPrivate")).get(0).toString());
		List pList = getHibernateTemplate().find("select partnerCode from PartnerPrivate where id =?", l);
		return pList;
	}

	public List findPartnerCode(String partnerCode) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("from PartnerPrivate where partnerCode=?", partnerCode);
	}

	public List getPartnerPrivateList(String partnerType, String corpID, String firstName, String lastName, String partnerCode, String status) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("FROM PartnerPrivate " +
										   "WHERE firstName like '%" + firstName.replaceAll("'", "''") +"%' " +
										   "AND lastName like '%" + lastName.replaceAll("'", "''") + "%' " +
									       "AND partnerCode like '%" + partnerCode.replaceAll("'", "''") + "%' " +
										   "AND status like '"+ status +"%'");
	}

	public List getPartnerPrivateListByPartnerCode(String corpID, String partnerCode) {
		getHibernateTemplate().setMaxResults(500);
		return getHibernateTemplate().find("FROM PartnerPrivate WHERE partnerCode = '"+ partnerCode +"' ");
	}

	public List getPartnerPrivate(String partnerCode, String corpID) {
		List pPrivateList = new ArrayList();
		String columns = "firstName, lastName, corpID, partnerCode, abbreviation, billingInstruction, qc, billPayType, billPayOption, salesMan, updatedBy, validNationalCode, warehouse, coordinator, billingInstructionCode, billToGroup, driverAgency, createdBy, createdOn, updatedOn, longPercentage, companyDivision, needAuth, storageBillingGroup, accountHolder, paymentMethod, payOption, multiAuthorization, payableUploadCheck, invoiceUploadCheck, billingUser, payableUser, pricingUser, partnerPortalActive, partnerPortalId, associatedAgents, viewChild, creditTerms, accountingDefault, acctDefaultJobType, status, stopNotAuthorizedInvoices, doNotCopyAuthorizationSO";
		pPrivateList = getSession().createSQLQuery("select "+columns+" from partnerprivate where partnerCode='"+partnerCode+"' and corpID in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"')").list();
		try {
			pPrivateList = hibernateUtil.convertScalarToComponent(columns, pPrivateList, PartnerPrivate.class);
	     } catch (Exception e) {
	          e.printStackTrace();
	     }
		return pPrivateList;
	}

	public List isExistPartnerPrivate(String corpID, String partnerCode) {
		return getSession().createSQLQuery("select * from partnerprivate where partnerCode='"+partnerCode+"' AND corpID  in ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') and status <> 'Inactive'" ).list();
	}

	public List getIsoCode(String vatNumber) {
		return getHibernateTemplate().find("select bucket2 from RefMaster where parameter='COUNTRY' and bucket2=?",vatNumber);
	}
	public List getPartnerPrivateId(String partnerCode) { 
		List pPrivateList = new ArrayList(); 
		pPrivateList = getSession().createSQLQuery("select id from partnerprivate where partnerCode='"+partnerCode+"'").list();
		
		return pPrivateList;
	
	}
	public String getTheCreditTerm(String partnerCode, String corpID){
		List creditList = getHibernateTemplate().find("select creditTerms from PartnerPrivate where partnerCode ='"+partnerCode+"' ");
		String credit="";
		try{
		if((creditList!=null) && (!(creditList.isEmpty()))&& (creditList.get(0)!=null))
		{
			credit=creditList.get(0).toString();
		}
		}catch(Exception e){
			credit="";
		}
		return credit;
	}
	
	public List getGroupAgePermission(String billToCode,String corpID){
		return getHibernateTemplate().find("select grpAllowed from PartnerPrivate where partnerCode='"+billToCode+"'");
		
	}
	public List findprivateParty(String partnerCode,String corpID){
		return getHibernateTemplate().find("select concat(partnerCode,'#',firstName,'#',lastName) from PartnerPrivate where partnerCode='"+partnerCode+"'");
	}
	public String getUsedAmount(String partnerCode,String tempCurrency,String corpID){
		List usedAmt = new ArrayList();
		String exchangeRate="";
		List eRate = this.getSession().createSQLQuery("select baseCurrencyRate from exchangerate e where currency = '"+tempCurrency+"' and corpID='"+corpID+"' and currencyBaseRate is not null and valueDate =(select max(valueDate) from exchangerate e2 where e.currency = e2.currency and corpID='"+corpID+"' and date_format(valueDate,'%Y-%m-%d') <= date_format(now(),'%Y-%m-%d')) limit 1").list();
		 if((eRate!=null)&&(!eRate.isEmpty()))
		 {
		 exchangeRate=eRate.toString().replace("[", "").replace("]","");
		 }else{
			 exchangeRate="1"; 
		 }		
		String usedAmount="0";
//		usedAmt=getHibernateTemplate().find("select sum(actualRevenue) from AccountLine where paymentstatus not in ('Fully Paid') and status is true and billToCode=?", partnerCode);
		usedAmt=this.getSession().createSQLQuery("select sum(if(paymentstatus ='Partially Paid',(actualRevenue-receivedAmount),actualRevenue)) from accountline where paymentstatus !='Fully Paid' and status is true and billToCode='"+partnerCode+"' and corpID='"+corpID+"'").list();		
		if((usedAmt!=null)&&(!usedAmt.isEmpty())&&(usedAmt.get(0)!=null))
		{
			usedAmount=usedAmt.get(0).toString();
		}		 
       	 Double  D1= Double.parseDouble(usedAmount);
       	 Double  D2= Double.parseDouble(exchangeRate);
       	 Double D3=D1*D2;       	 
       	 usedAmount=D3+"";
		return usedAmount;
	}
	public String getBillMc(String billToCode, String sessionCorpID){
		List tempMCValue;
		String mcValue="";
		tempMCValue= getHibernateTemplate().find("select mc from PartnerPrivate where partnerCode='"+billToCode+"'");
		try{
		if((tempMCValue!=null) && (!(tempMCValue.isEmpty()))&& (tempMCValue.get(0)!=null)){
			mcValue = tempMCValue.get(0).toString();
		}
		}catch(Exception e){
			mcValue="";
		}
		return mcValue;
	}

	public Map<String, String> getPartnerPrivateDescLinkMap(String partnerCode,String CorpID) {
		String query = "from PartnerPrivate where partnerCode='"+partnerCode+"' and corpID='"+CorpID+"'";
		List<PartnerPrivate> descLinkMap = getHibernateTemplate().find(query);
		Map<String, String> linkMap = new HashMap<String, String>();
		
		for (PartnerPrivate partnerPrivate : descLinkMap) {
			if(partnerPrivate.getDescription1()!= null && partnerPrivate.getDescription1().trim().length() > 0)
				linkMap.put(partnerPrivate.getDescription1(), partnerPrivate.getLink1());
			if(partnerPrivate.getDescription2()!= null && partnerPrivate.getDescription2().trim().length() > 0)
				linkMap.put(partnerPrivate.getDescription2(), partnerPrivate.getLink2());
			if(partnerPrivate.getDescription3()!= null && partnerPrivate.getDescription3().trim().length() > 0)
				linkMap.put(partnerPrivate.getDescription3(), partnerPrivate.getLink3());
		}
		
		return linkMap;
	}
	public int updatePartnerPrivate(String excludeFPU,String sessionCorpID,String partnerCode,String excludeOption){
		if(excludeOption.equalsIgnoreCase("AIS"))
		{
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludeFromParentUpdate="+excludeFPU+" WHERE partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"'");
		}else if(excludeOption.equalsIgnoreCase("CPF")){
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludeFromParentPolicyUpdate="+excludeFPU+" WHERE partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"'");			
		}else if(excludeOption.equalsIgnoreCase("FAQ")){
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludeFromParentFaqUpdate="+excludeFPU+" WHERE partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"'");			
		}else if(excludeOption.equalsIgnoreCase("PUB")){
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludePublicFromParentUpdate="+excludeFPU+" WHERE partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"'");			
		}else if(excludeOption.equalsIgnoreCase("PRS")){
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludePublicReloSvcsParentUpdate="+excludeFPU+" WHERE partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"'");			
		}else{
			String[] temp=partnerCode.split("~");
			if(!temp[1].equalsIgnoreCase("Relocation")){
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludeFromParentQualityUpdate="+excludeFPU+" WHERE partnerCode='"+temp[0]+"' and corpID='"+sessionCorpID+"'");
			}else{
			return getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET excludeFromParentQualityUpdateRelo="+excludeFPU+" WHERE partnerCode='"+temp[0]+"' and corpID='"+sessionCorpID+"'");				
			}
		}
	}  
	public List getPartnerPrivateChildAccount(String partnerCode,String corpId){
		return getHibernateTemplate().find("FROM PartnerPrivate where status='Approved' and corpID='"+corpId+"' and partnerCode=?",partnerCode);
	}
	public List getPartnerPrivateDefaultAccount(String partnerCode,String corpId){
		return getHibernateTemplate().find("FROM PartnerPrivate where  corpID='"+corpId+"' and partnerCode=?",partnerCode);
	}
	
	public void updateLastName(String sessionCorpID, String lName, Long ppId){
		getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET lastName=? WHERE partnerPublicId='"+ppId+"'",lName);
		//getHibernateTemplate().bulkUpdate("UPDATE PartnerPrivate SET lastName='"+lName+"' WHERE partnerPublicId='"+ppId+"' and corpID='"+sessionCorpID+"'");
	}
	public List getPartnerPrivateByPartnerCode(String otherCorpId,	String partnerCode){
		List pPrivateList = new ArrayList(); 
		pPrivateList = getSession().createSQLQuery("select id from partnerprivate where partnerCode='"+partnerCode+"' and corpID='"+otherCorpId+"' ").list();
		return pPrivateList;
	}
	public List<SystemDefault> findsysDefault(String corpID){
    	return getHibernateTemplate().find("from SystemDefault where corpID = ?", corpID);
    }
	public List findContract(String sessionCorpID){
		String query = "select distinct contract  from Contract where corpID='"+sessionCorpID+"'"
				+ " and ((ending is null or date_format(ending,'%Y-%m-%d') >= DATE_FORMAT(now(),'%Y-%m-%d'))"  
				+ " and (begin is null or date_format(begin,'%Y-%m-%d') <= DATE_FORMAT(now(),'%Y-%m-%d'))) order by contract";
		return getHibernateTemplate().find(query);
	}
	
	public Double getminMargin(String billToCode){ 
		Double minimumMargin= 0.00; 
		try{
		List minimumMarginList = getHibernateTemplate().find("select minimumMargin from PartnerPrivate where partnerCode='"+billToCode+"'");
         if((minimumMarginList!=null) && (!(minimumMarginList.isEmpty())) && (minimumMarginList.get(0)!=null) && (!((minimumMarginList).get(0).toString().trim().equals("")))){
				minimumMargin =  Double.parseDouble(minimumMarginList.get(0).toString());
			}
			}catch(Exception e){
				minimumMargin=0.00;
			} 
		return minimumMargin;
	}
	
	
	public String getPartnerCodeFromDefaultTemplate(String sessionCorpID,String partnerCode){
		String vendorCode="";
		String bilToCode="";
		String returnValue="";
		try{
		List vendorCodeList = this.getSession().createSQLQuery("select if(vendorCode is null or vendorCode='','N~V','Y~V') as partnerCode from defaultaccountline where corpId='"+sessionCorpID+"' and vendorCode='"+partnerCode+"'").list();
        if((vendorCodeList!=null) && (!(vendorCodeList.isEmpty())) && (vendorCodeList.get(0)!=null) && (!((vendorCodeList).get(0).toString().trim().equals("")))){
        		vendorCode =  vendorCodeList.get(0).toString();
        		returnValue=vendorCode;
			}
        
        if(vendorCode.equalsIgnoreCase("N") || vendorCode.equalsIgnoreCase("")){
        	List billToCodeList = this.getSession().createSQLQuery("select if(billtocode is null or billtocode='','N~B','Y~B') as partnerCode from defaultaccountline where corpId='"+sessionCorpID+"' and billtocode='"+partnerCode+"'").list();
            if((billToCodeList!=null) && (!(billToCodeList.isEmpty())) && (billToCodeList.get(0)!=null) && (!((billToCodeList).get(0).toString().trim().equals("")))){
            	bilToCode =  billToCodeList.get(0).toString();
            	returnValue=bilToCode;
    			}
        }
        if(returnValue.equalsIgnoreCase("")){
        	returnValue="N~N";
        }
			}catch(Exception e){
				returnValue="N~N";
				
			} 
		return returnValue;
	}
	
	public int getParentCompensationYear(String corpId,String agentParentcode){
		List Partnercodelist;
		List CompensationYear;
		String childPartnerCode="";
		int parentCompensationYear = 0;
		CompensationYear= getSession().createSQLQuery("select p.compensationYear from partnerpublic pp,partnerprivate p where p.partnercode='"+agentParentcode+"' and pp.partnerCode=p.partnerCode and p.corpId=pp.corpId and p.corpId='"+corpId+"' ").list();
		try{
			if((CompensationYear!=null) && (!(CompensationYear.isEmpty()))&& (CompensationYear.get(0)!=null)){
				parentCompensationYear = Integer.parseInt(CompensationYear.get(0).toString());
			}
			}catch(Exception e){
				parentCompensationYear=0;
			}
		Partnercodelist= getSession().createSQLQuery("select partnercode from partnerpublic where agentParent = '"+agentParentcode+"' and status = 'Approved'").list();
		try{
			if(Partnercodelist!=null && Partnercodelist.size()>0 && !Partnercodelist.isEmpty()){
			Iterator itr = Partnercodelist.iterator();
			while(itr.hasNext()){
				if(childPartnerCode.equals("")){
					childPartnerCode = "'"+itr.next().toString()+"'";
				}
				else{
					childPartnerCode = childPartnerCode+","+"'"+itr.next().toString()+"'";
				}
			}
			}
			
			if(childPartnerCode.equals("")){
				childPartnerCode="'"+childPartnerCode+"'";
			}
			
			int i= getHibernateTemplate().bulkUpdate("update PartnerPrivate set compensationYear='"+parentCompensationYear+"' where partnercode in("+childPartnerCode+") and corpId = '"+corpId+"' ");
			}catch(Exception e){
				parentCompensationYear=0;
			}
		
			return parentCompensationYear;
	}

	public List findDoNotInvoicePartnerList(String sessionCorpID) {
		return getHibernateTemplate().find("select partnerCode from PartnerPrivate where doNotInvoice= true and corpID='"+sessionCorpID+"' ");
	}

	public List getDefaultContactUsersList(String partnerCode, String sessionCorpID) {
		
		String condition1="";
		
		String tempPartnerCode="";
		
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"')").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+partnerCode+"'";
		}else{
			partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
		}
		
        String corpId="";
		List list = this.getSession().createSQLQuery("SELECT corpid FROM companydivision c where bookingAgentCode='"+partnerCode+"'" ).list();	
		if(list!=null && (!(list.isEmpty())) && list.get(0)!=null)
		{
			corpId=list.get(0).toString();
		}
		if(corpId==null ||corpId=="")
		{
			 condition1= "" ;
		}
		else 
		{
			 condition1=" union " +
				"Select distinct username,if(alias !=null || alias!='',alias,username)  as aliasName " +
				"from app_user ap  " +
				"where ap.corpID in ('"+corpId+"') "+ 
				"and account_enabled = true " +
				"and userType='USER' ";
			
		}
	
		List agentUsers= this.getSession().createSQLQuery("Select distinct username, " +
				"if(alias !=null || alias!='',alias,username)  as aliasName from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
				"datasecurityfilter dsf, datasecuritypermission dsp " +
				"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
				
				"and dsf.id=dsp.datasecurityfilter_id " +
				"and dss.id=dsp.datasecurityset_id " +
				// "and ap.parentAgent =  dsf.filtervalues " +
				"and dsf.filtervalues in("+partnerCodes+")" +
				"and ap.corpID in ('TSFT','"+sessionCorpID+"')" +
				" union " +
				"Select distinct username,if(alias !=null || alias!='',alias,username)  as aliasName " +
				"from app_user " +
				"where basedAt in("+partnerCodes+") " +
				"and corpID in ('TSFT','"+sessionCorpID+"')" +
				
				""+condition1+" "+
				"order by aliasName"
				).list();
		
		
			List result= new ArrayList(); 
		
		if(agentUsers.size()>0){
			Iterator it=agentUsers.iterator();
			while(it.hasNext()) 
			{
				Object []obj=(Object[]) it.next();
				UserDTO userDTO=new UserDTO();
				userDTO.setUsername(obj[0]);
				userDTO.setAlias(obj[1]);
				result.add(userDTO);
			}
		}
		
		return result;
	}

	
		 public List loadUserByUsername(String userName){
			 List agentUsers= this.getSession().createSQLQuery(" Select  first_name, last_name,email,phone_Number " +
				"from app_user   " +
				 
				"where username='"+userName+"'").list();

				List result= new ArrayList(); 
			
			if(agentUsers.size()>0){
				Iterator it=agentUsers.iterator();
				while(it.hasNext()) 
				{
					Object []obj=(Object[]) it.next();
					UserDTO userDTO=new UserDTO();
					userDTO.setFirst_name(obj[0]);
					userDTO.setLast_name(obj[1]);
					userDTO.setEmail(obj[2]);
					userDTO.setPhoneNumber(obj[3]);
					result.add(userDTO);}
				}
			return result;
			}
	
	public  class UserDTO
	 {
		private Object username;
		private Object first_name;
		private Object last_name;
		private Object userType;
		private Object id;
		private Object account_enabled;
		private Object email;
		private Object contact;
		private Object relocationContact;
		private Object alias;
		private Object phoneNumber;
		
		public Object getFirst_name() {
			return first_name;
		}
		public void setFirst_name(Object first_name) {
			this.first_name = first_name;
		}
		public Object getLast_name() {
			return last_name;
		}
		public void setLast_name(Object last_name) {
			this.last_name = last_name;
		}
		public Object getUsername() {
			return username;
		}
		public void setUsername(Object username) {
			this.username = username;
		}
		public Object getUserType() {
			return userType;
		}
		public void setUserType(Object userType) {
			this.userType = userType;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getAccount_enabled() {
			return account_enabled;
		}
		public void setAccount_enabled(Object account_enabled) {
			this.account_enabled = account_enabled;
		}
		public Object getEmail() {
			return email;
		}
		public void setEmail(Object email) {
			this.email = email;
		}
		public Object getContact() {
			return contact;
		}
		public void setContact(Object contact) {
			this.contact = contact;
		}
		public Object getRelocationContact() {
			return relocationContact;
		}
		public void setRelocationContact(Object relocationContact) {
			this.relocationContact = relocationContact;
		}
		public Object getAlias() {
			return alias;
		}
		public void setAlias(Object alias) {
			this.alias = alias;
		}
		public Object getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(Object phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
	 }
	
	public String getClassifiedAgentValue(String agentCode){
		List classifiedList = new ArrayList();
		String classifiedValue="";
		classifiedList= getHibernateTemplate().find("select agentClassification from PartnerPrivate where partnerCode='"+agentCode+"'");
		try{
		if((classifiedList!=null) && (!(classifiedList.isEmpty()))&& (classifiedList.get(0)!=null)){
			classifiedValue = classifiedList.get(0).toString().trim();
		}
		}catch(Exception e){
			classifiedValue="";
		}
		return classifiedValue;
	}
	public PartnerPrivate checkById(Long id)
	{
		
		List pList=getSession().createSQLQuery("select * from partnerprivate where partnerPublicId='"+id+"'").addEntity(PartnerPrivate.class).list();
		if(pList!=null && !pList.isEmpty())
		{
			return (PartnerPrivate)pList.get(0);
		}
		else
		{ 
			PartnerPrivate p=new PartnerPrivate();
				return p;
		}
	}

	@Override
	public List getBillingCycle(String billToCode, String sessionCorpID) {
		List pPrivateList = new ArrayList(); 
		pPrivateList = getSession().createSQLQuery("select billingCycle from partnerprivate where partnerCode='"+billToCode+"' and corpID='"+sessionCorpID+"' and billingCycle is NOT NULL").list();
		return pPrivateList;
	}
	public List findVatBillimngGroup(String sessionCorpID, String partnerCode) {
		// TODO Auto-generated method stub
		List pPrivateList = new ArrayList(); 
		
		String a1="select vatBillingGroup ,defaultVat from partnerprivate where partnerCode='"+partnerCode+"' and corpID='"+sessionCorpID+"' ";
		System.out.println("Query "+a1);
		  pPrivateList = this.getSession().createSQLQuery(a1).list();
		
		
		return pPrivateList;
	}

	public Map<String, String> getVatBillingGroupMap(String sessionCorpID, String parterCodeList) { 
		List<RefMaster> list1 = new ArrayList<RefMaster>();
		list1 = getHibernateTemplate().find(
				"from RefMaster where  parameter= 'vatBillingGroup'  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> vatBillingGroupParameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list1) {
			vatBillingGroupParameterMap.put(refMaster.getCode(), refMaster.getDescription().trim());
		} 
		String parterList = "";
		List<PartnerPrivate> list=new ArrayList(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try{
			for(String str:parterCodeList.split(",")){
				if(parterList.equalsIgnoreCase("")){
					parterList="'"+str+"'";
				}else{
					parterList=parterList+",'"+str+"'";
				}
			}
	   list = getHibernateTemplate().find("from PartnerPrivate where partnerCode in (" + parterList + ") and vatBillingGroup <> '' and vatBillingGroup is not null "); 
		 
			for (PartnerPrivate partnerPrivate : list) {
				if(vatBillingGroupParameterMap.containsKey(partnerPrivate.getVatBillingGroup())){
				parameterMap.put(partnerPrivate.getPartnerCode(),vatBillingGroupParameterMap.get(partnerPrivate.getVatBillingGroup()));
				}
			}
	 

		return parameterMap;
		}
	 catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
		return parameterMap;
	}





}
