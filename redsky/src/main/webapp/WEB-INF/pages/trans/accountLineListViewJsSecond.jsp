<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/common/taglibs.jsp"%>
<script language="JavaScript" type="text/javascript">
/*JS Start ********************************************/

// Compute Functionality for Revision Section End

function findRevisedQuantitys(aid){
	var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var billingContract = '${billing.contract}';
	var shipNumber='${serviceOrder.shipNumber}';
	var rev=document.forms['pricingListForm'].elements['category'+aid].value;
	var comptetive = document.forms['pricingListForm'].elements['inComptetive'].value;
	if(charge!=''){
    if(rev == "Internal Cost"){
        if(charge=="VANPAY" ){
         getVanPayCharges("Revision",aid);
         } else if(charge=="DOMCOMM",aid){
         getDomCommCharges("Revision");
         }
        else if(charge=="SALESCM" && comptetive=="Y") { 
        var totalExpense = eval(document.forms['pricingListForm'].elements['inRevisedTotalExpense'].value);
        var totalRevenue =eval(document.forms['pricingListForm'].elements['inRevisedTotalRevenue'].value); 
        var commisionRate =eval(document.forms['pricingListForm'].elements['salesCommisionRate'].value); 
        var grossMargin = eval(document.forms['pricingListForm'].elements['grossMarginThreshold'].value);
        var commision=((totalRevenue*commisionRate)/100);
        var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
        calGrossMargin=calGrossMargin*100;
        var calGrossMargin=Math.round(calGrossMargin);
        if(calGrossMargin>=grossMargin){
         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=Math.round(commision*100)/100; 
         document.forms['pricingListForm'].elements['basis'+aid].value='each';
         document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
         <c:if test="${contractType}">
         document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
         </c:if>	
        }else{ 
        commision=((.8*totalRevenue)-totalExpense);
        commision=Math.round(commision*100)/100;
        if(commision<0){
        document.forms['pricingListForm'].elements['revisionExpense'+aid].value=0;
        document.forms['pricingListForm'].elements['basis'+aid].value='each';
        document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1; 
        <c:if test="${contractType}">
        document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
        </c:if>
        }else{
         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=Math.round(commision*100)/100; 
         document.forms['pricingListForm'].elements['basis'+aid].value='each';
         document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
         <c:if test="${contractType}">
         document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
         </c:if>
        } }  
        } else if(charge=="SALESCM" && comptetive!="Y") {
        var agree =confirm("This job is not competitive; do you still want to calculate commission?");
        if(agree) {
        var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'+aid].value;  
        var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
        http99.open("GET", url, true);
        http99.onreadystatechange = handleHttpResponse300;
        http99.send(null); 
        } else {
        return false;
        }
        } else {
        var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'+aid].value;  
        var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
        http99.open("GET", url, true);
        http99.onreadystatechange = handleHttpResponse300;
        http99.send(null);
        }
        }  else{
        var url="invoiceExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
        http99.open("GET", url, true);
        http99.onreadystatechange = function(){handleHttpResponse3(aid);}
        http99.send(null); 
        }
	}else{
		alert("Please select Charge Code.");
	}
}

function handleHttpResponse3(aid)  { 
    if (http99.readyState == 4)   {
       var results = http99.responseText
       var oldRevenue = eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value);
       var oldExpense=eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value);
       <c:if test="${!contractType}">    
       results = results.trim();  
       if(results !=''){ 
       var res = results.split("#");
       if(res[9]=="AskUser2"){
       var reply = prompt("Please Enter Quantity", "");
       if(reply){
       if((reply>0 || reply<9)|| reply=='.') { 
       document.forms['pricingListForm'].elements['revisionQuantity'+aid].value = Math.round(reply*100)/100;
       <c:if test="${contractType}">
       document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value= Math.round(reply*100)/100;
       </c:if>
        } else {
     		alert("Please Enter legitimate Quantity");
     	    }
     	} else{
     	document.forms['pricingListForm'].elements['revisionQuantity'+aid].value= document.forms['pricingListForm'].elements['oldRevisionQuantity'+aid].value;
     	<c:if test="${contractType}">
       document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value= document.forms['pricingListForm'].elements['oldRevisionSellQuantity'+aid].value;
       </c:if>
     	    } 
       }  else{
       if(res[7] == undefined){
       document.forms['pricingListForm'].elements['revisionQuantity'+aid].value ="";
       <c:if test="${contractType}">
       document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value= "";
       </c:if>
       }else{                                  
       document.forms['pricingListForm'].elements['revisionQuantity'+aid].value =Math.round(res[7]*100)/100 ;
       <c:if test="${contractType}">
       document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=Math.round(res[7]*100)/100 ;
       </c:if>
      } }
      if(res[10]=="AskUser"){
       var reply = prompt("Please Enter Rate", "");
       if(reply){
       if((reply>0 || reply<9)|| reply=='.')  { 
       document.forms['pricingListForm'].elements['revisionSellRate'+aid].value =Math.round(reply*100)/100 ;
      } else {
     		alert("Please Enter legitimate Rate");
     	} 
       }  else{
       document.forms['pricingListForm'].elements['revisionSellRate'+aid].value = document.forms['pricingListForm'].elements['oldRevisionSellRate'+aid].value;
       }
       } else{
         if(res[8] == undefined){
         document.forms['pricingListForm'].elements['revisionSellRate'+aid].value ="";
         }else{
       document.forms['pricingListForm'].elements['revisionSellRate'+aid].value =Math.round(res[8]*100)/100 ;
      }  }
      if(res[10]=="BuildFormula"){
      if(res[11] == undefined){
         document.forms['pricingListForm'].elements['revisionSellRate'+aid].value ="";
         }else{
      document.forms['pricingListForm'].elements['revisionSellRate'+aid].value = Math.round(res[11]*100)/100;
     }  }
      var Q1 = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
      var Q2 = document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
      if(res[10]=="BuildFormula" && res[11] != undefined){
   	   Q2 = res[11]
          }
      var E1=Q1*Q2;
      document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value = E1;
     var Q5 = res[5];             
      var type = res[3];
      var E2= "";
      var E3= ""; 
      if(type == 'Division')  {
      	 if(Q5==0) {
      	   E2=0*1; 
      	   document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value = E2;
      	 } else {	
      		E2=(Q1*Q2)/Q5;
      		E3=Math.round(E2*100)/100; 
      		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value = E3;
      	 } }
      if(type == ' '  || type == '') {
      		E2=(Q1*Q2);
      		E3=Math.round(E2*100)/100; 
      		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value = E3;
      }
      if(type == 'Multiply') {
      		E2=(Q1*Q2*Q5);
      		E3=Math.round(E2*100)/100;
      	    document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value = E3;
      } }
     document.forms['pricingListForm'].elements['basis'+aid].value=res[13];
      var chargedeviation=res[18];
      document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation;
      if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
      //document.getElementById("revisionDeviationShow").style.display="block"; 
      document.forms['pricingListForm'].elements['buyDependSellNew'+aid].value=res[15];
      var buyDependSellAccount=res[15]
      var revisionRevenueAmount=0;
      var finalRevisionRevenueAmount=0; 
      var finalRevisionExpenceAmount=0;
      var revisionSellRate=0; 
      var sellDeviation=0;
      var  revisionpasspercentage=0
      var revisionpasspercentageRound=0;
      var buyDeviation=0;
      sellDeviation=res[16]
      buyDeviation=res[17] 
      revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;  
      finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*100)/100;
      document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=finalRevisionRevenueAmount; 
      if(buyDependSellAccount=="Y"){
       finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(buyDeviation/100))*100)/100;
      }else{
       finalRevisionExpenceAmount= Math.round((revisionRevenueAmount*(buyDeviation/100))*100)/100;
      }
      document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value=sellDeviation;
      document.forms['pricingListForm'].elements['oldRevisionSellDeviation'+aid].value=sellDeviation;
      document.forms['pricingListForm'].elements['revisionDeviation'+aid].value=buyDeviation;
      document.forms['pricingListForm'].elements['oldRevisionDeviation'+aid].value=buyDeviation;
      document.forms['pricingListForm'].elements['revisionExpense'+aid].value=finalRevisionExpenceAmount;
      revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
        revisionpasspercentageRound=Math.round(revisionpasspercentage);
        if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
	             document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
        }else{
	             document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionpasspercentageRound;
        }
        calculateRevisionRate('none',aid);  
        }else {
      //document.getElementById("revisionDeviationShow").style.display="none";
      document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value=0;
      document.forms['pricingListForm'].elements['oldRevisionSellDeviation'+aid].value=0;
      document.forms['pricingListForm'].elements['revisionDeviation'+aid].value=0;
      document.forms['pricingListForm'].elements['oldRevisionDeviation'+aid].value=0; 
      var bayRate=0 
      bayRate = res[20] ; 
      if(bayRate!=0){
      var recQuantityPay=0;
      recQuantityPay= document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
      Q1=recQuantityPay;
      Q2=bayRate;
      if(type == 'Division')  {
      	 if(Q5==0) {
      	   E2=0*1; 
      	   document.forms['pricingListForm'].elements['revisionExpense'+aid].value = E2;
      	 } else {	
      		E2=(Q1*Q2)/Q5;
      		E3=Math.round(E2*100)/100; 
      		document.forms['pricingListForm'].elements['revisionExpense'+aid].value = E3;
      	 } }
      if(type == ' ' || type == '') {
      		E2=(Q1*Q2);
      		E3=Math.round(E2*100)/100; 
      		document.forms['pricingListForm'].elements['revisionExpense'+aid].value = E3;
      }
      if(type == 'Multiply') {
      		E2=(Q1*Q2*Q5);
      		E3=Math.round(E2*100)/100;
      	    document.forms['pricingListForm'].elements['revisionExpense'+aid].value = E3;
      }
       document.forms['pricingListForm'].elements['revisionRate'+aid].value = Q2;
      var  revisionpasspercentage=0
      var revisionpasspercentageRound=0;
      var finalRevisionRevenueAmount=0; 
      var finalRevisionExpenceAmount=0;
      finalRevisionExpenceAmount=document.forms['pricingListForm'].elements['revisionExpense'+aid].value
      finalRevisionRevenueAmount= document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value
      revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
        revisionpasspercentageRound=Math.round(revisionpasspercentage);
        if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
	             document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
        }else{
	             document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionpasspercentageRound;
        }
      }else{ 
      var quantity =0;
      quantity= document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
      var rate =0;
      rate=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
      finalRevisionExpenceAmount = (quantity*rate); 
      if( document.forms['pricingListForm'].elements['basis'+aid].value=="cwt" || document.forms['pricingListForm'].elements['basis'+aid].value=="%age")  {
	          finalRevisionExpenceAmount = finalRevisionExpenceAmount/100; 
      }
     if( document.forms['pricingListForm'].elements['basis'+aid].value=="per 1000")  {
	        finalRevisionExpenceAmount = finalRevisionExpenceAmount/1000; 
      }
      finalRevisionExpenceAmount= Math.round((finalRevisionExpenceAmount)*100)/100;
      document.forms['pricingListForm'].elements['revisionExpense'+aid].value=finalRevisionExpenceAmount;
      }} 
      try{
    	var basisValue = document.forms['pricingListForm'].elements['basis'+aid].value;
   	    checkFloatNew('pricingListForm',document.forms['pricingListForm'].elements['revisionQuantity'+aid].value,'Nothing');
   	    var quantityValue = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
   	    		             
	        var buyRate1=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
	        var estCurrency1=document.forms['pricingListForm'].elements['revisionCurrency'+aid].value;
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					var revisionExpenseQ1=0;
					Q1=buyRate1;
					Q2=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
		            var E1=Q1*Q2;
		            E1=Math.round(E1*100)/100;
		            document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=E1;
		            var estLocalAmt1=0;
		            revisionExpenseQ1 = document.forms['pricingListForm'].elements['revisionExpense'+aid].value 
		            estLocalAmt1 =revisionExpenseQ1*Q2;
		            estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
		            document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=estLocalAmt1;     
			}
			<c:if test="${multiCurrency=='Y'}"> 
	        buyRate1=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	        estCurrency1=document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;
			if(estCurrency1.trim()!=""){
					var Q1=0;
					var Q2=0;
					var revisionRevenueAmountQ1=0;
					Q1=buyRate1;
					Q2=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
		            var E1=Q1*Q2;
		            E1=Math.round(E1*100)/100;
		            document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=E1;
		            var estLocalAmt1=0;
		            revisionRevenueAmountQ1=   document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value
		            estLocalAmt1 =revisionRevenueAmountQ1*Q2;
		            estLocalAmt1=Math.round(estLocalAmt1*100)/100;    
		            document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=estLocalAmt1;     
			}
			</c:if>
             var typeConversion01 = res[5];             
              var type01 = res[3];
			}catch(e){}
      </c:if>
      <c:if test="${contractType}">
      results = results.trim();  
      if(results !=''){ 
      var res = results.split("#");
      <c:choose>
      <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
      if(document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value==res[21]){
   	   
      }else{
   	   document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value=res[21];
   	   var revSellCurreny = document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;
   	   var revContCurrency = document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value;
   	   var revSellValDate = document.forms['pricingListForm'].elements['revisionSellValueDate'+aid].value;
   	   var revContValDate = document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value;
   	   var revSellExcRate = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
   	   var revContExcRate = document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
   	   rateOnActualizationDate('revSellCurreny~revContCurrency','revSellValDate~revContValDate','revSellExcRate~revContExcRate');
      }
      document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value=res[22]; 
      </c:when>
      <c:otherwise> 
      document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value=res[21];
      document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value=res[22]; 
      findExchangeContractRateByCharges('revision',aid);
      </c:otherwise>
	       </c:choose>	
  	   findPayableContractCurrencyExchangeRateByCharges('revision',aid);  
      if(res[9]=="AskUser2"){
      var reply = prompt("Please Enter Quantity", "");
      if(reply){
      if((reply>0 || reply<9)|| reply=='.') { 
      document.forms['pricingListForm'].elements['revisionQuantity'+aid].value = Math.round(reply*100)/100;
      <c:if test="${contractType}">
      document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=Math.round(reply*100)/100;
      </c:if>
       } else {
    		alert("Please Enter legitimate Quantity");
    	    }
    	} else{
    	document.forms['pricingListForm'].elements['revisionQuantity'+aid].value= document.forms['pricingListForm'].elements['oldRevisionQuantity'+aid].value;
    	<c:if test="${contractType}">
       document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value= document.forms['pricingListForm'].elements['oldRevisionQuantity'+aid].value;
       </c:if>
    	    } 
      }  else{
      if(res[7] == undefined){
      document.forms['pricingListForm'].elements['revisionQuantity'+aid].value ="";
      <c:if test="${contractType}">
      document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value="";
      </c:if>
      }else{                                  
      document.forms['pricingListForm'].elements['revisionQuantity'+aid].value =Math.round(res[7]*100)/100 ;
      <c:if test="${contractType}">
      document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=Math.round(res[7]*100)/100 ;
      </c:if>
     } }
     if(res[10]=="AskUser"){
      var reply = prompt("Please Enter Rate", "");
      if(reply){
      if((reply>0 || reply<9)|| reply=='.')  { 
      document.forms['pricingListForm'].elements['revisionContractRate'+aid].value =Math.round(reply*100)/100 ;
     } else {
    		alert("Please Enter legitimate Rate");
    	} 
      }  else{
    	  document.forms['pricingListForm'].elements['revisionContractRate'+aid].value = document.forms['pricingListForm'].elements['oldRevisionSellRate'+aid].value;;
      }
      } else{
        if(res[8] == undefined){
        	document.forms['pricingListForm'].elements['revisionContractRate'+aid].value ="";
        }else{
        	document.forms['pricingListForm'].elements['revisionContractRate'+aid].value =Math.round(res[8]*100)/100 ;
     }  }
     if(res[10]=="BuildFormula"){
     if(res[11] == undefined){
    	 document.forms['pricingListForm'].elements['revisionContractRate'+aid].value ="";
     }else{
    	 document.forms['pricingListForm'].elements['revisionContractRate'+aid].value = Math.round(res[11]*100)/100;
    }  }
     var Q1 = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
     var Q2 = document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
     if(res[10]=="BuildFormula" && res[11] != undefined){
   	  Q2 =res[11];
     }
     var E1=Q1*Q2;
     document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value = E1;
    var Q5 = res[5];             
     var type = res[3];
     var E2= "";
     var E3= ""; 
     if(type == 'Division')  {
     	 if(Q5==0) {
     	   E2=0*1; 
     	   document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value = E2;
     	 } else {	
     		E2=(Q1*Q2)/Q5;
     		E3=Math.round(E2*100)/100; 
     		document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value = E3;
     	 } }
     if(type == ' ' || type == '') {
     		E2=(Q1*Q2);
     		E3=Math.round(E2*100)/100; 
     		document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value = E3;
     }
     if(type == 'Multiply') {
     		E2=(Q1*Q2*Q5);
     		E3=Math.round(E2*100)/100;
     	    document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value = E3;
     } } 
    document.forms['pricingListForm'].elements['basis'+aid].value=res[13];
     var chargedeviation=res[18];
     document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation;
     if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
     //document.getElementById("revisionDeviationShow").style.display="block"; 
     document.forms['pricingListForm'].elements['buyDependSell'+aid].value=res[15];
     var buyDependSellAccount=res[15]
     var revisionRevenueAmount=0;
     var finalRevisionRevenueAmount=0; 
     var finalRevisionExpenceAmount=0;
     var revisionSellRate=0; 
     var sellDeviation=0;
     var  revisionpasspercentage=0
     var revisionpasspercentageRound=0;
     var revisionContractRateAmmount=0;
     var finalRevisionContractRateAmmount=0;
     var revisionPayableContractRateAmmount=0;
     var buyDeviation=0;
     sellDeviation=res[16]
     buyDeviation=res[17]
     var revisionContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value
     revisionContractRateAmmount =document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value
     revisionRevenueAmount=revisionContractRateAmmount/revisionContractExchangeRate;
     revisionRevenueAmount=Math.round(revisionRevenueAmount*100)/100; 
     finalRevisionRevenueAmount=Math.round((revisionRevenueAmount*(sellDeviation/100))*100)/100;
     finalRevisionContractRateAmmount=Math.round((revisionContractRateAmmount*(sellDeviation/100))*100)/100;
     document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value=finalRevisionContractRateAmmount; 
     if(buyDependSellAccount=="Y"){
      finalRevisionExpenceAmount= Math.round((finalRevisionRevenueAmount*(buyDeviation/100))*100)/100;
     }else{
      finalRevisionExpenceAmount= Math.round((revisionRevenueAmount*(buyDeviation/100))*100)/100;
     }
     document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value=sellDeviation;
     document.forms['pricingListForm'].elements['oldRevisionSellDeviation'+aid].value=sellDeviation;
     document.forms['pricingListForm'].elements['revisionDeviation'+aid].value=buyDeviation;
     document.forms['pricingListForm'].elements['oldRevisionDeviation'+aid].value=buyDeviation;
     var revisionPayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value
     revisionPayableContractRateAmmount=finalRevisionExpenceAmount*revisionPayableContractExchangeRate;
     revisionPayableContractRateAmmount=Math.round(revisionPayableContractRateAmmount*100)/100;
     document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value=revisionPayableContractRateAmmount;
     calRevisionPayableContractRate(aid);  
     }else {
     //document.getElementById("revisionDeviationShow").style.display="none";
     document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value=0;
     document.forms['pricingListForm'].elements['oldRevisionSellDeviation'+aid].value=0;
     document.forms['pricingListForm'].elements['revisionDeviation'+aid].value=0;
     document.forms['pricingListForm'].elements['oldRevisionDeviation'+aid].value=0; 
     var bayRate=0 
     bayRate = res[20] ; 
     if(bayRate!=0){
     var recQuantityPay=0;
     recQuantityPay= document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
     Q1=recQuantityPay;
     Q2=bayRate;
     if(type == 'Division'){
     	 if(Q5==0) {
     	   E2=0*1; 
     	   document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value = E2;
     	 } else {	
     		E2=(Q1*Q2)/Q5;
     		E3=Math.round(E2*100)/100; 
     		document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value = E3;
     	 } }
     if(type == ' ' || type == '') {
     		E2=(Q1*Q2);
     		E3=Math.round(E2*100)/100; 
     		document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value = E3;
     }
     if(type == 'Multiply') {
     		E2=(Q1*Q2*Q5);
     		E3=Math.round(E2*100)/100;
     	    document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value = E3;
     }
      document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value = Q2;
      }}                 	                 
     var revisionPayableContractRatecal= document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value
     var revisionPayableContractRateAmmountcal= document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value
     var  revisionPayableContractCurrencycal=document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value;			        
	 if(revisionPayableContractRatecal.trim()!=""){
	 var revisionPayableContractExchangeRatecal=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
     var revisionRatecal=revisionPayableContractRatecal/revisionPayableContractExchangeRatecal;
     revisionRatecal=Math.round(revisionRatecal*100)/100;
     var revisionExpensecal=revisionPayableContractRateAmmountcal/revisionPayableContractExchangeRatecal;
     revisionExpensecal=Math.round(revisionExpensecal*100)/100;
     document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRatecal;
     document.forms['pricingListForm'].elements['revisionExpense'+aid].value=revisionExpensecal;
     }
	 var revisionRatecal= document.forms['pricingListForm'].elements['revisionRate'+aid].value;
     var revisionExpensecal= document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
     var revisionCurrencycal=document.forms['pricingListForm'].elements['revisionCurrency'+aid].value;
	 if(revisionCurrencycal.trim()!=""){
	 var revisionExchangeRatecal = document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
	 var revisionLocalRatecal=revisionRatecal*revisionExchangeRatecal;
	 revisionLocalRatecal=Math.round(revisionLocalRatecal*100)/100;
	 var revisionLocalAmountcal=revisionExpensecal*revisionExchangeRatecal;
	 revisionLocalAmountcal=Math.round(revisionLocalAmountcal*100)/100;
	 document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionLocalRatecal;
     document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmountcal;
	 }
	 var revisionContractRatecal= document.forms['pricingListForm'].elements['revisionContractRate'+aid].value
     var revisionContractRateAmmountcal= document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value
     var  revisionContractCurrencycal=document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value;
     if(revisionContractCurrencycal.trim()!=""){
     var revisionContractExchangeRatecal=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
     var revisionSellRatecal=revisionContractRatecal/revisionContractExchangeRatecal;
     revisionSellRatecal=Math.round(revisionSellRatecal*100)/100;
     var revisionRevenueAmountcal=revisionContractRateAmmountcal/revisionContractExchangeRatecal;
     revisionRevenueAmountcal=Math.round(revisionRevenueAmountcal*100)/100;
     document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRatecal;
     document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=revisionRevenueAmountcal;
     }	 
     var revisionSellRatecal= document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
     var revisionRevenueAmountcal= document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
     var revisionSellCurrencycal=document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value;
	 if(revisionSellCurrencycal.trim()!=""){
		 var revisionSellExchangeRatecal = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
		 var revisionSellLocalRatecal=revisionSellRatecal*revisionSellExchangeRatecal;
		revisionSellLocalRatecal=Math.round(revisionSellLocalRatecal*100)/100;
		 var revisionSellLocalAmountcal=revisionRevenueAmountcal*revisionSellExchangeRatecal;
		revisionSellLocalAmountcal=Math.round(revisionSellLocalAmountcal*100)/100;
		 document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellLocalRatecal;
         document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmountcal;
	 }      	
      </c:if> 
      calculateVatAmtRevision(aid);  
      calculateREVVatAmt(aid);
      
      
      var revenueValue=eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value);
		var revenueRound=0;
		var oldRevenueSum=0;
		var balanceRevenue=0;
		<c:if test="${not empty serviceOrder.id}">
		oldRevenueSum=eval(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value);
		</c:if>
		revenueRound=Math.round(revenueValue*100)/100;
		balanceRevenue=revenueRound-oldRevenue;
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		balanceRevenue=Math.round(balanceRevenue*100)/100;
		oldRevenueSum=oldRevenueSum+balanceRevenue; 
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		var revenueRoundNew=""+revenueRound;
		if(revenueRoundNew.indexOf(".") == -1){
			revenueRoundNew=revenueRoundNew+".00";
		} 
		if((revenueRoundNew.indexOf(".")+3 != revenueRoundNew.length)){
			revenueRoundNew=revenueRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=revenueRoundNew;
		<c:if test="${not empty serviceOrder.id}">
		var newRevenueSum=""+oldRevenueSum;
		if(newRevenueSum.indexOf(".") == -1){
			newRevenueSum=newRevenueSum+".00";
		} 
		if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			newRevenueSum=newRevenueSum+"0";
		} 
		document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		</c:if>
		if(revenueRoundNew>0){
			//document.forms['pricingListForm'].elements['displayOnQuote'+aid].checked = true;
		}
		//--------------------End--revisionRevenueAmount-------------------------//

		//-------------------Start---revisionExpense-------------------------//                
		var expenseValue=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
		var expenseRound=0;
		var oldExpenseSum=0;
		var balanceExpense=0;
		<c:if test="${not empty serviceOrder.id}">
		oldExpenseSum=eval(document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value);
		</c:if>
		expenseRound=Math.round(expenseValue*100)/100;
		balanceExpense=expenseRound-oldExpense;
		oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		balanceExpense=Math.round(balanceExpense*100)/100; 
		oldExpenseSum=oldExpenseSum+balanceExpense;
		oldExpenseSum=Math.round(oldExpenseSum*100)/100;
		var expenseRoundNew=""+expenseRound;
		if(expenseRoundNew.indexOf(".") == -1){
			expenseRoundNew=expenseRoundNew+".00";
		} 
		if((expenseRoundNew.indexOf(".")+3 != expenseRoundNew.length)){
			expenseRoundNew=expenseRoundNew+"0";
		}
		document.forms['pricingListForm'].elements['revisionExpense'+aid].value=expenseRoundNew;
		<c:if test="${not empty serviceOrder.id}">
		var newExpenseSum=""+oldExpenseSum;
		if(newExpenseSum.indexOf(".") == -1){
			newExpenseSum=newExpenseSum+".00";
		} 
		if((newExpenseSum.indexOf(".")+3 != newExpenseSum.length)){
			newExpenseSum=newExpenseSum+"0";
		} 
		document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value=newExpenseSum;
		</c:if>
		if(expenseRoundNew>0){
			//document.forms['pricingListForm'].elements['displayOnQuote'+aid].checked = true;
		}
		//-------------------End---revisionExpense-------------------------//
		<c:if test="${not empty serviceOrder.id}">
		var revenueValue=0;
		var expenseValue=0;
		var grossMarginValue=0; 
		var grossMarginPercentageValue=0; 
		revenueValue=eval(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value);
		expenseValue=eval(document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value); 
		grossMarginValue=revenueValue-expenseValue;
		grossMarginValue=Math.round(grossMarginValue*100)/100; 
		document.forms['pricingListForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
		if(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == 0.00){
			document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value='';
		}else{
			grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
			grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
			document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
		}
		</c:if>
      
      }
    var  revisionpasspercentage=0
    var revisionpasspercentageRound=0;
    var finalRevisionRevenueAmount=0; 
    var finalRevisionExpenceAmount=0;
      finalRevisionExpenceAmount=document.forms['pricingListForm'].elements['revisionExpense'+aid].value
      finalRevisionRevenueAmount= document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value
      revisionpasspercentage = (finalRevisionRevenueAmount/finalRevisionExpenceAmount)*100;
      revisionpasspercentageRound=Math.round(revisionpasspercentage);
      if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
	       document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
      }else{
	       document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionpasspercentageRound;
      }
      
}
var http99 = getHTTPObject99();
function getHTTPObject99() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}

function changeRevisionPassPercentage(basis,quantity,buyRate,expense,sellRate,revenue,estimatePassPercentage,displayOnQuots,aid){
	var estimate=0; 
	var estimateround=0;
	var estimateRevenue=0;
	var estimateRevenueRound=0;
	var sellRateValue=0;
	var sellRateround=0;
	var	estimatepasspercentage =0;
	var quantityValue = 0;
	var oldRevenue=0;
	var oldRevenueSum=0;
	var balanceRevenue=0; 
	var buyRateValue=0;  
	oldRevenue=eval(document.forms['pricingListForm'].elements[revenue].value); 
	<c:if test="${not empty serviceOrder.id}">
	oldRevenueSum=eval(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value);
	</c:if> 
	quantityValue = eval(document.forms['pricingListForm'].elements[quantity].value); 
	buyRateValue = eval(document.forms['pricingListForm'].elements[buyRate].value); 
	estimatepasspercentage= document.forms['pricingListForm'].elements[estimatePassPercentage].value;
	estimate = (quantityValue*buyRateValue); 
	if(quantityValue>0 && estimate>0){
		if( document.forms['pricingListForm'].elements[basis].value=="cwt" || document.forms['pricingListForm'].elements[basis].value=="%age"){
			estimate = estimate/100; 	  	
		} 
		if( document.forms['pricingListForm'].elements[basis].value=="per 1000"){
			estimate = estimate/1000; 	  	
		}
		estimateRevenue=((estimate)*estimatepasspercentage/100)
		estimateRevenueRound=Math.round(estimateRevenue*100)/100;
		document.forms['pricingListForm'].elements[revenue].value=estimateRevenueRound;   
		if( document.forms['pricingListForm'].elements[basis].value=="cwt" || document.forms['pricingListForm'].elements[basis].value=="%age"){
			sellRateValue=(estimateRevenue/quantityValue)*100;
			sellRateround=Math.round(sellRateValue*100)/100;

			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		}
		else if( document.forms['pricingListForm'].elements[basis].value=="per 1000"){
			sellRateValue=(estimateRevenue/quantityValue)*1000;
			sellRateround=Math.round(sellRateValue*100)/100; 
			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		} else if(document.forms['pricingListForm'].elements[basis].value!="cwt" || document.forms['pricingListForm'].elements[basis].value!="%age"||document.forms['pricingListForm'].elements[basis].value!="per 1000")
		{
			sellRateValue=(estimateRevenue/quantityValue);
			sellRateround=Math.round(sellRateValue*100)/100; 
			document.forms['pricingListForm'].elements[sellRate].value=sellRateround;	  	
		}
		balanceRevenue=estimateRevenueRound-oldRevenue;
		oldRevenueSum=Math.round(oldRevenueSum*100)/100;
		balanceRevenue=Math.round(balanceRevenue*100)/100; 
		oldRevenueSum=oldRevenueSum+balanceRevenue;  
		<c:if test="${not empty serviceOrder.id}">
		var newRevenueSum=""+oldRevenueSum;
		if(newRevenueSum.indexOf(".") == -1){
			newRevenueSum=newRevenueSum+".00";
		} 
		if((newRevenueSum.indexOf(".")+3 != newRevenueSum.length)){
			newRevenueSum=newRevenueSum+"0";
		} 
		document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value=newRevenueSum;
		</c:if>
		if(estimateRevenueRound>0){
			document.forms['pricingListForm'].elements[displayOnQuots].checked = true;
		}
		try{
			<c:if test="${systemDefaultVatCalculationNew=='Y'}">
			//calculateVatAmtSection('REV',aid);
			calculateVatAmtRevision(aid);
			</c:if>
		}catch(e){}
		calculateRevisionGrossMargin();
	}else{
		document.forms['pricingListForm'].elements[estimatePassPercentage].value=0;
	}
}

function calculateRevisionGrossMargin(){
	<c:if test="${not empty serviceOrder.id}">
	var revenueValue=0;
	var expenseValue=0;
	var grossMarginValue=0; 
	var grossMarginPercentageValue=0; 
	revenueValue=eval(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value);
	expenseValue=eval(document.forms['pricingListForm'].elements['revisedTotalExpense'+${serviceOrder.id}].value); 
	grossMarginValue=revenueValue-expenseValue;
	grossMarginValue=Math.round(grossMarginValue*100)/100; 
	document.forms['pricingListForm'].elements['revisedGrossMargin'+${serviceOrder.id}].value=grossMarginValue; 
	if(document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == '' || document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == 0||document.forms['pricingListForm'].elements['revisedTotalRevenue'+${serviceOrder.id}].value == 0.00){
		document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value='';
	}else{
		grossMarginPercentageValue=(grossMarginValue*100)/revenueValue; 
		grossMarginPercentageValue=Math.round(grossMarginPercentageValue*100)/100; 
		document.forms['pricingListForm'].elements['revisedGrossMarginPercentage'+${serviceOrder.id}].value = grossMarginPercentageValue;
	}	
	</c:if>    
}

function calculateVatAmtRevision(aid){
	 <c:if test="${systemDefaultVatCalculation=='true'}">
		var revisionVatAmt=0;
		if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
		var revisionVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
		var revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value);
		<c:if test="${contractType}">
		revisionRevenueAmount= eval(document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value);
		</c:if>
		revisionVatAmt=(revisionRevenueAmount*revisionVatPercent)/100;
		revisionVatAmt=Math.round(revisionVatAmt*100)/100; 
		document.forms['pricingListForm'].elements['revisionVatAmt'+aid].value=revisionVatAmt;
		}
	</c:if>
}

function calculateREVVatAmt(aid){
	  <c:if test="${systemDefaultVatCalculation=='true'}">
	    var payVatAmt=0;
		if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
		var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
		var actualExpense= eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value);
		<c:if test="${contractType}"> 
		actualExpense= eval(document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value);
		</c:if>
		payVatAmt=(actualExpense*payVatPercent)/100;
		payVatAmt=Math.round(payVatAmt*100)/100; 
		document.forms['pricingListForm'].elements['revisionExpVatAmt'+aid].value=payVatAmt;
		}
	</c:if>
}

function calculateRevisionRate(target,aid){
	   <c:if test="${contractType}">
	   if((target=='revisionQuantity'+aid)||(target=='revisionLocalRate'+aid)||(target=='revisionPayableContractRate'+aid)||(target=='basis'+aid)){
	      var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
	      var estimatePayableContractRate=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
	      estimatePayableContractRate=Math.round(estimatePayableContractRate*100)/100;
	      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
	      var roundValue=Math.round(estimateRate*100)/100;
	      document.forms['pricingListForm'].elements['revisionRate'+aid].value=roundValue ;
	   }else{
	        var revisionQuantity =0;
	        var revisionDeviation=100;
	        revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
		    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
		    var revisionExpense =eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value); 
		    var revisionRate=0;
		    var revisionRateRound=0; 
		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
	         revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	         } 
	        if( basis=="cwt" || basis=="%age"){
	       	  revisionRate=(revisionExpense/revisionQuantity)*100;
	       	  revisionRateRound=Math.round(revisionRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  	
	       	} else if(basis=="per 1000"){
	       	  revisionRate=(revisionExpense/revisionQuantity)*1000;
	       	  revisionRateRound=Math.round(revisionRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  	  	
	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	          revisionRate=(revisionExpense/revisionQuantity);
	       	  revisionRateRound=Math.round(revisionRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  			  	
		    } 
		    revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value 
		    if(revisionDeviation!='' && revisionDeviation>0){
		    	revisionRateRound=revisionRateRound*100/revisionDeviation;
		        revisionRateRound=Math.round(revisionRateRound*100)/100;
		    document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;		 
		       }   }
		   </c:if>
		   <c:if test="${!contractType}">
     var revisionQuantity =0;
     var revisionDeviation=100;
     revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
	    var revisionExpense =eval(document.forms['pricingListForm'].elements['revisionExpense'+aid].value); 
	    var revisionRate=0;
	    var revisionRateRound=0; 
	    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
      revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
      } 
     if( basis=="cwt" || basis=="%age"){
    	  revisionRate=(revisionExpense/revisionQuantity)*100;
    	  revisionRateRound=Math.round(revisionRate*100)/100;
    	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  	
    	} else if(basis=="per 1000"){
    	  revisionRate=(revisionExpense/revisionQuantity)*1000;
    	  revisionRateRound=Math.round(revisionRate*100)/100;
    	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  	  	
    	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
       revisionRate=(revisionExpense/revisionQuantity);
    	  revisionRateRound=Math.round(revisionRate*100)/100;
    	  document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;	  			  	
	    } 
	    revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value 
	    if(revisionDeviation!='' && revisionDeviation>0){
	    	revisionRateRound=revisionRateRound*100/revisionDeviation;
	        revisionRateRound=Math.round(revisionRateRound*100)/100;
	    document.forms['pricingListForm'].elements['revisionRate'+aid].value=revisionRateRound;		 
	       }
	   </c:if>
	}
function rateOnActualizationDate(field1,field2,field3){
	<c:if test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
    <c:if test="${contractType}">	
	var currency=""; 
		var currentDate=currentDateRateOnActualization();
		try{
		currency=document.forms['pricingListForm'].elements[field1.trim().split('~')[0]].value;
		document.forms['pricingListForm'].elements[field2.trim().split('~')[0]].value=currentDate;
		document.forms['pricingListForm'].elements[field3.trim().split('~')[0]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}

		try{
		currency=document.forms['pricingListForm'].elements[field1.trim().split('~')[1]].value;
		document.forms['pricingListForm'].elements[field2.trim().split('~')[1]].value=currentDate;
		document.forms['pricingListForm'].elements[field3.trim().split('~')[1]].value=findExchangeRateRateOnActualization(currency);
		}catch(e){}		
	</c:if>
	</c:if>
}
function currentDateRateOnActualization(){
	 var mydate=new Date();
  var daym;
  var year=mydate.getFullYear()
  var y=""+year;
  if (year < 1000)
  year+=1900
  var day=mydate.getDay()
  var month=mydate.getMonth()+1
  if(month == 1)month="Jan";
  if(month == 2)month="Feb";
  if(month == 3)month="Mar";
  if(month == 4)month="Apr";
  if(month == 5)month="May";
  if(month == 6)month="Jun";
  if(month == 7)month="Jul";
  if(month == 8)month="Aug";
  if(month == 9)month="Sep";
  if(month == 10)month="Oct";
  if(month == 11)month="Nov";
  if(month == 12)month="Dec";
  var daym=mydate.getDate()
  if (daym<10)
  daym="0"+daym
  var datam = daym+"-"+month+"-"+y.substring(2,4); 
  return datam;
}
function findPayableContractCurrencyExchangeRateByCharges(temp,aid){
    var country =document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value; 
    if(temp=='estimate'){
        country =document.forms['pricingListForm'].elements['estimatePayableContractCurrency'+aid].value;
        }
        if(temp=='revision'){
        	country =document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value;
        }
    var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
    http7777.open("GET", url, true);
    http7777.onreadystatechange =function(){ handleHttpResponsePayableContractCurrencyRateByCharges(temp,aid);};  
    http7777.send(null);
}
function handleHttpResponsePayableContractCurrencyRateByCharges(temp,aid) { 
         if (http7777.readyState == 4) {
            var results = http7777.responseText
            results = results.trim(); 
            results = results.replace('[','');
            results=results.replace(']','');
            if(results.length>1) {
            	if(temp=='estimate'){
                	document.forms['pricingListForm'].elements['estimatePayableContractExchangeRate'+aid].value=results    
                }
            	else if(temp=='revision'){
                	 document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=results   
                }
            	else if(temp=='payable'){ 
                	 document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=results
                } else{  
              document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=results 
              document.forms['pricingListForm'].elements['estimatePayableContractExchangeRate'+aid].value=results
              document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=results
              }} else {
            	  if(temp=='estimate'){
                  	document.forms['pricingListForm'].elements['estimatePayableContractExchangeRate'+aid].value=1; 
                  }
            	  else if(temp=='revision'){
                  	 document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=1; 
                  }
            	  else if(temp=='payable'){ 
                  	 document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=1; 
                  } else{   
              document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value=1; 
              document.forms['pricingListForm'].elements['estimatePayableContractExchangeRate'+aid].value=1; 
              document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=1; 
              }}
              var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4); 
				  if(temp=='estimate'){
					  document.forms['pricingListForm'].elements['estimatePayableContractValueDate'+aid].value=datam;
					  calculateEstimateRateByContractRate('none',aid);
	              }else if(temp=='revision'){
	                	  document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value=datam;
	                	  calculateRevisionRateByContractRate('none',aid);
	              }else if(temp=='payable'){ 
	                	  document.forms['pricingListForm'].elements['payableContractValueDate'+aid].value=datam;
	                	  calculateActualExpenseByContractRate('none',aid);
	              }else{
				  document.forms['pricingListForm'].elements['payableContractValueDate'].value=datam; 
				  document.forms['pricingListForm'].elements['estimatePayableContractValueDate'+aid].value=datam; 
				  document.forms['pricingListForm'].elements['revisionPayableContractValueDate'+aid].value=datam; 
	              calculateEstimateRateByContractRate('none',aid);
	              calculateRevisionRateByContractRate('none',aid);
	              calculateActualExpenseByContractRate('none',aid);
	            }   
			}
     }
var http7777 = getHTTPObject77();
function getHTTPObject77() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
function  calculateRevisionRateByContractRate(target,aid) {
	   <c:if test="${contractType}">
	        var country =document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value; 
	        if(country=='') {
	          alert("Please select Contract Currency ");
	          document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=0;
	          document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value=1;
	        } else if(country!=''){
	        var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value
	        var estimatePayableContractRate=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value
	        estimatePayableContractRate=Math.round(estimatePayableContractRate*100)/100;
	        var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
	        var roundValue=Math.round(estimateRate*100)/100;
	        document.forms['pricingListForm'].elements['revisionRate'+aid].value=roundValue ; 
	        calRevisionLocalRate('NoCal',target,aid); 
	        calRevisionPayableContractRateAmmount(target,aid);
	        }   
	   </c:if>
	   }
function checkFloatNew(formName,fieldName,message){
	var i;
	var s = document.forms[formName].elements[fieldName].value;
	var count = 0;
	if(s.trim()=='.'){
		document.forms[formName].elements[fieldName].value='0.00';
	}else{
    for (i = 0; i < s.length; i++)  {   
        var c = s.charAt(i); 
        if(c == '.') {
        	count = count+1
        }
        if ((c > '9') && (c != '.') || (count>'1')) {
	        document.forms[formName].elements[fieldName].value='0.00';
        }  }    } }

function getVanPayCharges(temp,aid){
	 var sid=document.forms['pricingListForm'].elements['serviceOrder.id'].value;
	 var url="getVanPayCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid);
	      http9.open("GET", url, true);
	      http9.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp,aid);}; ;
	      http9.send(null);
	}
	function handleHttpResponseVanPayCharges(temp,aid){
	           if (http9.readyState == 4)   {
	                var results = http9.responseText
	                results = results.trim(); 
	                results = results.replace('[','');
	                results=results.replace(']',''); 
	                if(results.length > 0) {
	                }else{
	                results=0;
	                }
	                if(temp=='Estimate'){
	                document.forms['pricingListForm'].elements['estimateExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['estimateRate'+aid].value=results;
	                document.forms['pricingListForm'].elements['basis'+aid].value='each';
	                document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
	                <c:if test="${contractType}">
	                document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	                </c:if>		
	                }
	                if(temp=='Revision'){
	                document.forms['pricingListForm'].elements['revisionExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['revisionRate'+aid].value=results;  
	                document.forms['pricingListForm'].elements['basis'+aid].value='each';
	                document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	                <c:if test="${contractType}">
	                document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	                </c:if>
	                }
	                if(temp=='Actual'){
	                document.forms['pricingListForm'].elements['actualExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['localAmount'+aid].value=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
	                document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1;  
	                } } }
	function getDomCommCharges(temp){
	var comptetive = document.forms['pricingListForm'].elements['inComptetive'].value;
	if(comptetive=="Y"){
	 var sid=document.forms['pricingListForm'].elements['serviceOrder.id'].value;
	 var job=document.forms['pricingListForm'].elements['serviceOrder.Job'].value;
	 var url="getDomCommCharges.html?ajax=1&decorator=simple&popup=true&sid=" + encodeURI(sid)+"&job="+encodeURI(job);
	      http9.open("GET", url, true);
	      http9.onreadystatechange =function(){ handleHttpResponseVanPayCharges(temp);}; ;
	      http9.send(null);
	      } }
	function handleHttpResponseDomCommCharges(temp){
	           if (http9.readyState == 4)   {
	                var results = http9.responseText
	                results = results.trim(); 
	                results = results.replace('[','');
	                results=results.replace(']',''); 
	                if(results.length > 0)  {
	                }else{
	                results=0;
	                }
	                if(temp=='Estimate'){
	                document.forms['pricingListForm'].elements['estimateExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['estimateRate'+aid].value=results;
	                document.forms['pricingListForm'].elements['basis'+aid].value='each';
	                document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
	                <c:if test="${contractType}">
	                document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	                </c:if>	
	                }
	                if(temp=='Revision'){
	                document.forms['pricingListForm'].elements['revisionExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['revisionRate'+aid].value=results;  
	                document.forms['pricingListForm'].elements['basis'+aid].value='each';
	                document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	                <c:if test="${contractType}">
	                document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	                </c:if>
	                }
	                if(temp=='Actual'){
	                document.forms['pricingListForm'].elements['actualExpense'+aid].value=results; 
	                document.forms['pricingListForm'].elements['localAmount'+aid].value=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
	                document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1;  
	                }  
	        }
	}
	
	function findExchangeContractRateByCharges(temp,aid){
	    <c:if test="${contractType}">
	    var country =document.forms['pricingListForm'].elements['contractCurrency'+aid].value;
	    if(temp=='estimate'){
	    	country =document.forms['pricingListForm'].elements['estimateContractCurrency'+aid].value;
	    }
	    if(temp=='revision'){
	    	country =document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value;
	    } 
	    var url="findExchangeRate.html?ajax=1&decorator=simple&popup=true&country="+encodeURI(country);
	    http77.open("GET", url, true);
	    http77.onreadystatechange =function(){ handleHttpResponseContractRateByCharges(temp,aid);}; 
	    http77.send(null); 
	    </c:if>
	}
	function handleHttpResponseContractRateByCharges(temp,aid) { 
	         if (http77.readyState == 4) {
	            var results = http77.responseText
	            results = results.trim(); 
	            results = results.replace('[','');
	            results=results.replace(']',''); 
	            if(results.length>1) { 
		            if(temp=='estimate'){
		            	document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value=results    
		            }else if(temp=='revision'){
		            	 document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=results   
		            }else if(temp=='receivable'){ 
		            	 document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=results
		            }else{  
		              document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=results
		              document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value=results
		              document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=results
		            } 
	            }else{
	            	  if(temp=='estimate'){
	                  	document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value=1; 
	                  }else if(temp=='revision'){
	                  	 document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value=1; 
	                  }else if(temp=='receivable'){ 
	                  	 document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=1; 
	                  }else{    
			              document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=1; 
			              document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value=1; 
			              document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=1; 
	              		}
	            }
	              var mydate=new Date();
		          var daym;
		          var year=mydate.getFullYear()
		          var y=""+year;
		          if (year < 1000)
		          year+=1900
		          var day=mydate.getDay()
		          var month=mydate.getMonth()+1
		          if(month == 1)month="Jan";
		          if(month == 2)month="Feb";
		          if(month == 3)month="Mar";
				  if(month == 4)month="Apr";
				  if(month == 5)month="May";
				  if(month == 6)month="Jun";
				  if(month == 7)month="Jul";
				  if(month == 8)month="Aug";
				  if(month == 9)month="Sep";
				  if(month == 10)month="Oct";
				  if(month == 11)month="Nov";
				  if(month == 12)month="Dec";
				  var daym=mydate.getDate()
				  if (daym<10)
				  daym="0"+daym
				  var datam = daym+"-"+month+"-"+y.substring(2,4);
				  if(temp=='estimate'){
					  document.forms['pricingListForm'].elements['estimateContractValueDate'+aid].value=datam;
					  calculateEstimateSellRateByContractRate('none',aid);
	                }else if(temp=='revision'){
	                	document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value=datam;
	                	calculateRevisionSellRateByContractRate('none',aid);
	                } else if(temp=='receivable'){ 
	                	 document.forms['pricingListForm'].elements['contractValueDate'+aid].value=datam;
	                	 calculateRecRateByContractRate('none',aid);
	                }else{
						  document.forms['pricingListForm'].elements['contractValueDate'+aid].value=datam;
						  document.forms['pricingListForm'].elements['estimateContractValueDate'+aid].value=datam; 
						  document.forms['pricingListForm'].elements['revisionContractValueDate'+aid].value=datam; 
						  calculateRecRateByContractRate('none',aid);
						  calculateEstimateSellRateByContractRate('none',aid);
						  calculateRevisionSellRateByContractRate('none',aid);
	                }  
			}   
	}
	var http77 = getHTTPObject77();
	function getHTTPObject77() {
	    var xmlhttp;
	    if(window.XMLHttpRequest)  {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)  {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)  {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        } }
	    return xmlhttp;
	}
	function  calculateRecRateByContractRate(target,aid) {
  		<c:if test="${contractType}">
	       var country =document.forms['pricingListForm'].elements['contractCurrency'+aid].value; 
	       if(country=='') {
	         alert("Please select Contract Currency ");
	         document.forms['pricingListForm'].elements['contractRate'+aid].value=0;
	         document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value=1;
	       }else if(country!=''){
		       var recRateExchange=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
		       var recCurrencyRate=document.forms['pricingListForm'].elements['contractRate'+aid].value
		       recCurrencyRate=document.forms['pricingListForm'].elements['contractRate'+aid].value=Math.round(recCurrencyRate*100)/100;
		       var recRate=recCurrencyRate/recRateExchange;
		       var roundValue=Math.round(recRate*10000)/10000;
		       document.forms['pricingListForm'].elements['recRate'+aid].value=roundValue ; 
		       calRecCurrencyRate(target,aid);
		       calContractRateAmmount(target,aid);
	       }   
	  	</c:if>
  	}
	
	function  calculateEstimateSellRateByContractRate(target,aid) {
		   <c:if test="${contractType}">
		        var country =document.forms['pricingListForm'].elements['estimateContractCurrency'+aid].value; 
		        if(country=='') {
		          alert("Please select Contract Currency ");
		          document.forms['pricingListForm'].elements['estimateContractRate'+aid].value=0;
		          document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value=1;
		        } else if(country!=''){
		        var estimateContractExchangeRate=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value
		        var estimateContractRate=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value
		        estimateContractRate=Math.round(estimateContractRate*100)/100;
		        var estimateSellRate=estimateContractRate/estimateContractExchangeRate;
		        var roundValue=Math.round(estimateSellRate*100)/100;
		        document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=roundValue ; 
		        calEstSellLocalRate('NoCal',target,aid);
		        //calRecCurrencyRate();
		        calEstimateContractRateAmmount(target,aid);
		        }   
		   </c:if>
	}
	
	function  calculateRevisionSellRateByContractRate(target,aid) {
		   <c:if test="${contractType}">
		        var country =document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value; 
		        if(country=='') {
		          alert("Please select Contract Currency ");
		          document.forms['pricingListForm'].elements['revisionContractRate'+aid].value=0;
		          document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value=1;
		        } else if(country!=''){
		        var estimateContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value
		        var estimateContractRate=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value
		        estimateContractRate=Math.round(estimateContractRate*100)/100;
		        var estimateSellRate=estimateContractRate/estimateContractExchangeRate;
		        var roundValue=Math.round(estimateSellRate*100)/100;
		        document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=roundValue ; 
		        calRevisionSellLocalRate('NoCal',target,aid);
		        //calRecCurrencyRate();
		        calRevisionContractRateAmmount(target,aid);
		        }   
		   </c:if>
		}
	
	function calRecCurrencyRate(target,aid){
		   <c:if test="${multiCurrency=='Y'}">
		          var country =document.forms['pricingListForm'].elements['recRateCurrency'+aid].value; 
		          if(country=='') {
		          alert("Please select billing currency "); 
		          document.forms['pricingListForm'].elements['recRate'+aid].value=0; 
		          document.forms['pricingListForm'].elements['actualRevenue'+aid].value=0;
		        } else if(country!=''){
		        	 <c:if test="${contractType}">
		        	 if(target=='recRate'){
		 		        var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
				        var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*100)/100;
				        document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundValue ; 
		        	 }else if(target=='actualRevenue'){
		        		 var roundValue='0.00';
		        	     var ETemp=0.00;
		        	     var ATemp=0.00;
		        		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		        		 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
		        		 var baseRateVal=1;
		        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		        			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
		        	         } 
		        	        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	        	baseRateVal=100;
		        	         }  else if(bassisVal=="per 1000"){
		        	        	 baseRateVal=1000;
		        	       	 } else {
		        	       		baseRateVal=1;  	
		        		    }
		        	        ATemp=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;  
		        	        ATemp=(ATemp*baseRateVal)/quantityVal; 
		        	     ETemp = document.forms['pricingListForm'].elements['recRateExchange'+aid].value;  
		        	     ATemp=ATemp*ETemp;
		        	     roundValue=Math.round(ATemp*100)/100;
		            	document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundValue;
		        	 }else if(target=='recCurrencyRate'){
		        	 }else{
			            var recRateExchangeTemp=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
			            var recCurrencyRateTemp=document.forms['pricingListForm'].elements['contractRate'+aid].value
			            var recRateTemp=recCurrencyRateTemp/recRateExchangeTemp;
			            
				        var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
				        var recRate=recRateTemp;
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*100)/100;
				        document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundValue ; 
		        	 }    
					</c:if>
					<c:if test="${!contractType}">
				        var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
				        var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value
				        var recCurrencyRate=recRate*recRateExchange;
				        var roundValue=Math.round(recCurrencyRate*100)/100;
				        document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundValue ; 
					</c:if>
		        convertedAmountBilling(target,aid); 
		        calFrgnCurrAmount(target,aid);
		        calRecContractRate(target,aid);
		        }
		  </c:if>
		  }
	
	   function calContractRateAmmount(target,aid) { 
		   <c:if test="${contractType}">
		   if(target=='contractRate'){
		 		 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
				 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
				 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['pricingListForm'].elements['contractRate'+aid].value;
			        ATemp=(ATemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
			        roundValue=Math.round(ATemp*100)/100;
			        document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=roundValue;
		   }else if(target=='recCurrencyRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
				 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
				 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
			        ETemp=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			        ATemp=ATemp/ETemp;
			        ETemp=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
			        BTemp=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}	   
		       roundValue=Math.round(ATemp*100)/100;
		       document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=roundValue;
		   }else if(target=='recRate'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
				 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
				 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['pricingListForm'].elements['recRate'+aid].value;
			        ETemp=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
			        BTemp=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			        
			        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
		       roundValue=Math.round(ATemp*100)/100;
		       document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=roundValue;
		   }else if(target=='actualRevenue'){
				 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;    		 
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			     ETemp = document.forms['pricingListForm'].elements['recQuantity'+aid].value;  
			     ATemp = document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
			     BTemp = document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
			     ATemp=ATemp*BTemp;
					var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
		       roundValue=Math.round(ATemp*100)/100;
		       document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=roundValue;
		   } else{
				 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
				 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
				 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;
			     var BTemp =0.00;    	     	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			         } 
			        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	baseRateVal=100;
			         }  else if(bassisVal=="per 1000"){
			        	 baseRateVal=1000;
			       	 } else {
			       		baseRateVal=1;  	
				    }
			        ATemp=document.forms['pricingListForm'].elements['contractRate'+aid].value;
			        ATemp=(ATemp*quantityVal)/baseRateVal;
					var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
					if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
						   ATemp=(ATemp*receivableSellDeviation)/100;
					}
			        roundValue=Math.round(ATemp*100)/100;
			        document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=roundValue;
		   		}  
		   </c:if>
	}
	   
	   function calEstSellLocalRate(temp,target,aid){
			<c:if test="${multiCurrency=='Y'}">
			          var country =document.forms['pricingListForm'].elements['estSellCurrency'+aid].value; 
			          if(country=='') {
			          alert("Please select billing currency "); 
			          document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=0; 
			        } else if(country!=''){  
			        	<c:if test="${contractType}"> 
			          	 if(target=='estSellLocalAmount'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			        		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
			        		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value;
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    	ATemp=Math.round(ATemp*100)/100;
			        		    }
			             document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=ATemp; 
			        	 }else if(target=='estimateSellRate'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        	     ETemp = document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;  
			        	     ATemp = document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*100)/100;
			            	document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue; 
			        	 }else if(target=='estimateRevenueAmount'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			        		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
			        		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
			        		 var baseRateVal=1;
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value; 
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    }
			        	     ETemp = document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;  
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*100)/100; 
			            	document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue; 
			        	 }else if(target=='estSellLocalRate'){
			        	 }else{
			        		 	var QTemp=0.00; 
			        		    var ATemp =0.00;
			        		    var FTemp =0.00;
			        		    var ETemp =0.00;
			        		    ETemp= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ; 
			        		    FTemp =document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
			        		    FTemp=FTemp/ETemp;
			        		    ETemp= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ;
			        		    FTemp=FTemp*ETemp;
			                roundValue=Math.round(FTemp*100)/100;
			                document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue;
			              	 }		       </c:if>
				       <c:if test="${!contractType}"> 
					        var recRateExchange=document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value
					        var recRate=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value
					        var recCurrencyRate=recRate*recRateExchange;
					        var roundValue=Math.round(recCurrencyRate*100)/100;
					        document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue ; 
					        </c:if>
			        calculateEstimateSellLocalAmount('cal',target,aid);
			        if(temp=='cal'){
			        	calEstimateContractRate('none',aid);
			        }  } 
		     </c:if>		        
			  }
	   
		function calEstimateContractRateAmmount(target,aid) { 
			   <c:if test="${contractType}">
			   if(target=='estSellLocalRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
					 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;
				        ETemp=document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
						   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
							   ATemp=(ATemp*estimateSellDeviation)/100;
							    }		        
				     	roundValue=Math.round(ATemp*100)/100;
			    	document.forms['pricingListForm'].elements['estimateContractRateAmmount'+aid].value=roundValue; 
						 }else if(target=='estimateSellRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
					 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value;
				        ATemp=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
						   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
							   ATemp=(ATemp*estimateSellDeviation)/100;
							    }		        
				     	roundValue=Math.round(ATemp*100)/100;
			    	document.forms['pricingListForm'].elements['estimateContractRateAmmount'+aid].value=roundValue; 
						 }else if(target=='estimateContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
								 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
									   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
									   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
										   ATemp=(ATemp*estimateSellDeviation)/100;
										    }					        
							     	roundValue=Math.round(ATemp*100)/100;
						    	document.forms['pricingListForm'].elements['estimateContractRateAmmount'+aid].value=roundValue;
					 	 }	
			             calculateEstimateRevenueAmountByContractRate(target,aid); 
			   </c:if>
	}
		
		function calRevisionSellLocalRate(temp,target,aid){
			<c:if test="${multiCurrency=='Y'}">
			          var country =document.forms['pricingListForm'].elements['revisionSellCurrency'+aid].value; 
			          if(country=='') {
			          alert("Please select billing currency "); 
			          document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=0; 
			        } else if(country!=''){
			        	 <c:if test="${contractType}">  
			        	 if(target=='revisionSellLocalAmount'){
			        		 var roundValue='0.00';
			        		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			        		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
			        		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
			        		 var baseRateVal=1;
			        	 	 var ATemp=0.00; 
			        	     var ETemp =0.00;	    		 
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value;
			        	        ATemp=(ATemp*baseRateVal)/quantityVal;
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    	ATemp=Math.round(ATemp*100)/100;
			        		    }
			             document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=ATemp; 
			        	 }else if(target=='revisionSellRate'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        	     ETemp = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;  
			        	     ATemp = document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*100)/100;
			            	document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue; 
			        	 }else if(target=='revisionRevenueAmount'){
			        		 var roundValue='0.00';
			        	     var ETemp=0.00;
			        	     var ATemp=0.00;
			        		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			        		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
			        		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
			        		 var baseRateVal=1;
			        		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			        			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
			        	         } 
			        	        if( bassisVal=="cwt" || bassisVal=="%age"){
			        	        	baseRateVal=100;
			        	         }  else if(bassisVal=="per 1000"){
			        	        	 baseRateVal=1000;
			        	       	 } else {
			        	       		baseRateVal=1;  	
			        		    }
			        	        ATemp=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;  
			        	        ATemp=(ATemp*baseRateVal)/quantityVal; 
			        		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
			        		    	ATemp=ATemp*100/estimateDeviationVal;
			        		    }
			        	     ETemp = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;  
			        	     ATemp=ATemp*ETemp;
			        	     roundValue=Math.round(ATemp*100)/100;
			            	document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue; 
			        	 }else if(target=='revisionSellLocalRate'){
			        	 }else{
			        		 	var QTemp=0.00; 
			        		    var ATemp =0.00;
			        		    var FTemp =0.00;
			        		    var ETemp =0.00;
			        		    ETemp= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ; 
			        		    FTemp =document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
			        		    FTemp=FTemp/ETemp;
			        		    ETemp= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ;
			        		    FTemp=FTemp*ETemp;
			                roundValue=Math.round(FTemp*100)/100;
			                document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue;
			              	 }
						</c:if>
						 <c:if test="${!contractType}"> 
					        var recRateExchange=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value
					        var recRate=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value
					        var recCurrencyRate=recRate*recRateExchange;
					        var roundValue=Math.round(recCurrencyRate*100)/100;
					        document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue ;
						</c:if> 
				        calculateRevisionSellLocalAmount('cal',target,aid);
				        if(temp=='cal'){
				        	calRevisionContractRate('none',aid);  
				        }  } 
		  </c:if>		        
	}
	
		function calRevisionContractRateAmmount(target,aid) { 
			   <c:if test="${contractType}">
			   if(target=='revisionSellLocalRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
					 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    } 
				        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
				        ETemp=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
				        ATemp=ATemp/ETemp;
				        ETemp=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}
				     	roundValue=Math.round(ATemp*100)/100;
			    	document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value=roundValue; 
						 }else if(target=='revisionSellRate'){
					 var roundValue='0.00';
					 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
					 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;   		
					 var baseRateVal=1;
				 	 var ATemp=0.00; 
				     var ETemp =0.00;	    		 
					 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
						 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
				         } 
				        if( bassisVal=="cwt" || bassisVal=="%age"){
				        	baseRateVal=100;
				         }  else if(bassisVal=="per 1000"){
				        	 baseRateVal=1000;
				       	 } else {
				       		baseRateVal=1;  	
					    }
				        ETemp=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
				        ATemp=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
				        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
						var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}		        
				     	roundValue=Math.round(ATemp*100)/100;
			    	document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value=roundValue; 
						 }else if(target=='revisionContractRateAmmount'){
						  }else{
								 var roundValue='0.00';
								 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
								 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
								 var baseRateVal=1;
							 	 var ATemp=0.00; 
							     var ETemp =0.00;	    		 
								 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
									 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
							         } 
							        if( bassisVal=="cwt" || bassisVal=="%age"){
							        	baseRateVal=100;
							         }  else if(bassisVal=="per 1000"){
							        	 baseRateVal=1000;
							       	 } else {
							       		baseRateVal=1;  	
								    }
							        ATemp=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
							        ATemp=(ATemp*quantityVal)/baseRateVal;
									var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
									if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
										   ATemp=(ATemp*revisionSellDeviation)/100;
									}					        
							     	roundValue=Math.round(ATemp*100)/100;
						    	document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value=roundValue;
					 	 }	
			              calculateRevisionRevenueAmountByContractRate(target,aid);   
			   </c:if>
	}
		
		function convertedAmountBilling(target,aid) {
		   var estimate=0;
		   var estimaternd=0;
		   var receivableSellDeviation=0;
		       var quantity = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
		       if(quantity=='' ||quantity=='0' ||quantity=='0.0' ||quantity=='0.00') {
		    	   quantity = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    }
		       var rate = document.forms['pricingListForm'].elements['recRate'+aid].value;
		       rate = document.forms['pricingListForm'].elements['recRate'+aid].value=Math.round(rate*10000)/10000;
		       receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
		       if(quantity<999999999999&& rate<999999999999) {
		  	 	estimate = (quantity*rate);
		  	 	estimaternd=Math.round(estimate*100)/100;
		  	 	} else { 
		  	 	 estimate=0;
		  	 	 estimaternd=Math.round(estimate*100)/100;
		  	 	} 
		         if( document.forms['pricingListForm'].elements['basis'+aid].value=="cwt" || document.forms['pricingListForm'].elements['basis'+aid].value=="%age") {
		      	    estimate = estimate/100; 
		      	    estimaternd=Math.round(estimate*100)/100;	 
		      	    document.forms['pricingListForm'].elements['actualRevenue'+aid].value = estimaternd;
		      	    <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
		      	    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		             document.forms['pricingListForm'].elements['distributionAmount'+aid].value = estimaternd;
		             </c:if>
		             </c:if> 	
		          }
		          else if( document.forms['pricingListForm'].elements['basis'+aid].value=="per 1000") {
		      	    estimate = estimate/1000; 
		      	    estimaternd=Math.round(estimate*100)/100;	 
		      	    document.forms['pricingListForm'].elements['actualRevenue'+aid].value = estimaternd; 
		      	     <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
		      	    <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		             document.forms['pricingListForm'].elements['distributionAmount'+aid].value = estimaternd;
		            </c:if> 
		            </c:if> 	
		          } else{              
		            document.forms['pricingListForm'].elements['actualRevenue'+aid].value = estimaternd;
		           <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
		            <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		            document.forms['pricingListForm'].elements['distributionAmount'+aid].value = estimaternd;
		            </c:if> 
		            </c:if> 
		          }
		          if(receivableSellDeviation!='' && receivableSellDeviation>0){
		           estimaternd=estimaternd*receivableSellDeviation/100;
		           estimaternd=Math.round(estimaternd*100)/100;
		           document.forms['pricingListForm'].elements['actualRevenue'+aid].value = estimaternd;
		           <c:if test="${automaticReconcile && (!(ownbillingVanline))}">
		           <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
		           document.forms['pricingListForm'].elements['distributionAmount'+aid].value = estimaternd;
		           </c:if> 
		           </c:if> 	 
		           } 
		          //chechCreditInvoice();
		          calFrgnCurrAmount(target,aid); 
		          calContractRateAmmount(target,aid);
		          driverCommission(aid); 
		  		}
		
		  function calFrgnCurrAmount(target,aid) { 
			  <c:if test="${multiCurrency=='Y'}">
			         <c:if test="${contractType}"> 
			    	 if(target=='contractRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			    		 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
			    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;
			    	     var BTemp =0.00;    	     	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['pricingListForm'].elements['contractRate'+aid].value;
			    	        ETemp=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
			    	        BTemp=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			    	        ATemp=((ATemp/ETemp)*quantityVal*BTemp)/baseRateVal;
							var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
							if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
								   ATemp=(ATemp*receivableSellDeviation)/100;
							}
			    	        roundValue=Math.round(ATemp*100)/100;
					        document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundValue; 
			    	 }else if(target=='recCurrencyRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;
			    	     var BTemp =0.00;    	     	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	     ETemp = document.forms['pricingListForm'].elements['recQuantity'+aid].value;  
			    	     ATemp = document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
			    	     ATemp=(ATemp*ETemp)/baseRateVal;
							var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
							if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
								   ATemp=(ATemp*receivableSellDeviation)/100;
							}    	     
			    	     roundValue=Math.round(ATemp*100)/100;
			        	document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundValue; 
			    	 }else if(target=='recRate'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;    		 
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;
			    	     var BTemp =0.00;    	     	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	     ETemp = document.forms['pricingListForm'].elements['recQuantity'+aid].value;  
			    	     ATemp = document.forms['pricingListForm'].elements['recRate'+aid].value;
			    	     BTemp = document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			    	     ATemp=(ATemp*BTemp*ETemp)/baseRateVal;
							var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
							if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
								   ATemp=(ATemp*receivableSellDeviation)/100;
							}
			    	     roundValue=Math.round(ATemp*100)/100;
			        	document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundValue; 
			    	 }else if(target=='actualRevenue'){
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;    		 
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;
			    	     var BTemp =0.00;    	     	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	     ETemp = document.forms['pricingListForm'].elements['recQuantity'+aid].value;  
			    	     ATemp = document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
			    	     BTemp = document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			    	     ATemp=ATemp*BTemp;
							var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
							if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
								   ATemp=(ATemp*receivableSellDeviation)/100;
							}
			    	     roundValue=Math.round(ATemp*100)/100;
			        	document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundValue; 
			    	 }else{
			    		 var roundValue='0.00';
			    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			    		 var quantityVal=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
			    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
			    		 var baseRateVal=1;
			    	 	 var ATemp=0.00; 
			    	     var ETemp =0.00;
			    	     var BTemp =0.00;    	     	    		 
			    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			    			 quantityVal = document.forms['pricingListForm'].elements['recQuantity'+aid].value=1;
			    	         } 
			    	        if( bassisVal=="cwt" || bassisVal=="%age"){
			    	        	baseRateVal=100;
			    	         }  else if(bassisVal=="per 1000"){
			    	        	 baseRateVal=1000;
			    	       	 } else {
			    	       		baseRateVal=1;  	
			    		    }
			    	        ATemp=document.forms['pricingListForm'].elements['contractRate'+aid].value;
			    	        ETemp=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
			    	        BTemp=document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
			    	        ATemp=(((ATemp/ETemp)*BTemp)*quantityVal)/baseRateVal;
			 				var receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
							if(receivableSellDeviation!='' && receivableSellDeviation>0){ 
								   ATemp=(ATemp*receivableSellDeviation)/100;
							}
			    	     roundValue=Math.round(ATemp*100)/100;
			        	document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=roundValue;
			               }
			    	 </c:if>
			    	 <c:if test="${!contractType}">
			    	 var estimate=0;
			  	   var estimaternd=0;
			  	   var receivableSellDeviation=0;
			  	       var quantity = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
			  	       var rate = document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
			  	   	   if(quantity<999999999999&& rate<999999999999) {
			  	  	 	estimate = (quantity*rate);
			  	  	 	estimaternd=Math.round(estimate*100)/100;
			  	  	 	} else { 
			  	  	 	 estimate=0;
			  	  	 	 estimaternd=Math.round(estimate*100)/100;
			  	  	 	} 
			  	         if( document.forms['pricingListForm'].elements['basis'+aid].value=="cwt" || document.forms['pricingListForm'].elements['basis'+aid].value=="%age") {
			  	      	    estimate = estimate/100; 
			  	      	    estimaternd=Math.round(estimate*100)/100;	 
			  	      	    document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value = estimaternd; 
			  	      	  }else if( document.forms['pricingListForm'].elements['basis'+aid].value=="per 1000")  {
			  	      	    estimate = estimate/1000; 
			  	      	    estimaternd=Math.round(estimate*100)/100;	 
			  	      	    document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value = estimaternd; 
			  	      	   } else{
			  	            document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value = estimaternd;
			  	          }
			  	         
			          receivableSellDeviation=document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value;
			           if(receivableSellDeviation!='' && receivableSellDeviation>0){
			             estimaternd=estimaternd*receivableSellDeviation/100;
			             estimaternd=Math.round(estimaternd*100)/100;
			             document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value = estimaternd;	 
			         }
			    	 </c:if>
			            if(document.forms['pricingListForm'].elements['paymentStatus'+aid].value=='Fully Paid'){
			               document.forms['pricingListForm'].elements['receivedAmount'+aid].value= estimaternd;
			             }
			            //calculateVatAmt();
			            calculateVatAmtRevision(aid);
			            calculateVatAmtEst(aid);
			   </c:if>
			}
		  
		  function calRecContractRate(target,aid){
		        <c:if test="${contractType}"> 
		          var country =document.forms['pricingListForm'].elements['contractCurrency'+aid].value; 
		          if(country=='') {
		          alert("Please select Contract Currency "); 
		          document.forms['pricingListForm'].elements['recRate'+aid].value=0; 
		        } else if(country!=''){
		        	var roundValue=0.00;
		        	 <c:if test="${contractType}">        	
					 	var QTemp=0.00; 
					    var ATemp =0.00;
					    var FTemp =0.00;
					    var ETemp =0.00;
					    ETemp= document.forms['pricingListForm'].elements['recRateExchange'+aid].value*1 ; 
					    FTemp =document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
					    FTemp=FTemp/ETemp;
					    ETemp= document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value*1 ;
					    FTemp=FTemp*ETemp;
					    roundValue=Math.round(FTemp*100)/100;
		          </c:if>
		          <c:if test="${!contractType}">        
				        var recRateExchange=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
				        var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value
				        var recCurrencyRate=recRate*recRateExchange;
				        roundValue=Math.round(recCurrencyRate*100)/100;
		            </c:if>
		        if(target!='contractRate'){
		        document.forms['pricingListForm'].elements['contractRate'+aid].value=roundValue ;
		        }
		        calContractRateAmmount(target,aid);
		        }
		      </c:if>
		  }
	
	function calculateEstimateSellLocalAmount(temp,target,aid){
	    	   <c:if test="${contractType}"> 
	       	  if(target=='estSellLocalRate'){
	    		 var roundValue='0.00';
	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;   		
	    		 var baseRateVal=1;
	    	 	 var ATemp=0.00; 
	    	     var ETemp =0.00;	    		 
	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	    	         } 
	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	        	baseRateVal=100;
	    	         }  else if(bassisVal=="per 1000"){
	    	        	 baseRateVal=1000;
	    	       	 } else {
	    	       		baseRateVal=1;  	
	    		    }
	    	        ATemp=document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;
	    	        ATemp=(ATemp*quantityVal)/baseRateVal;
	    	        var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
	    				   ATemp=(ATemp*estimateSellDeviation)/100;
	    				    }
	    	     	roundValue=Math.round(ATemp*100)/100;
	         	document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=roundValue; 
	    			 }else if(target=='estimateSellRate'){
	    		 var roundValue='0.00';
	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;   		
	    		 var baseRateVal=1;
	    	 	 var ATemp=0.00; 
	    	     var ETemp =0.00;	    		 
	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	    	         } 
	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	        	baseRateVal=100;
	    	         }  else if(bassisVal=="per 1000"){
	    	        	 baseRateVal=1000;
	    	       	 } else {
	    	       		baseRateVal=1;  	
	    		    }
	    	        ETemp=document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;
	    	        ATemp=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
	    	        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
	    	        var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
	    				   ATemp=(ATemp*estimateSellDeviation)/100;
	    				    }
	    	     	roundValue=Math.round(ATemp*100)/100;
	         	document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=roundValue; 
	    			 }else if(target=='estSellLocalAmount'){
	  			  }else{
	    		 var roundValue='0.00';
	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;   		
	    		 var estimatePayableContractExchangeRateVal=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value;
	    		 var estExchangeRateVal=document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value; 
	    		 var baseRateVal=1;
	    	 	 var ATemp=0.00; 
	    	     var ETemp =0.00;	    		 
	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	    	         } 
	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	        	baseRateVal=100;
	    	         }  else if(bassisVal=="per 1000"){
	    	        	 baseRateVal=1000;
	    	       	 } else {
	    	       		baseRateVal=1;  	
	    		    }
	    	        ATemp=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
	    	        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
	    	        var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	    			   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
	    				   ATemp=(ATemp*estimateSellDeviation)/100;
	    				    }
	    	     roundValue=Math.round(ATemp*100)/100;
	         document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=roundValue;
	   		 	 }		     
	 	  </c:if>
	 	   <c:if test="${!contractType}"> 
	 	  var estimateQuantity =0;
		   var estLocalRate =0;
		   	    estimateQuantity=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value; 
		   	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
		   	    estSellLocalRate =document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;  
		   	    var estimateSellLocalAmount=0;
		   	    var estimateSellLocalAmountRound=0; 
		   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
		            estimateQuantity =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
		            } 
		           if( basis=="cwt" || basis=="%age"){
		        	   estimateSellLocalAmount=(estSellLocalRate*estimateQuantity)/100;
		        	   estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*100)/100;
		          	  document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=estimateSellLocalAmountRound;	  	
		          	}  else if(basis=="per 1000"){
		          		estimateSellLocalAmount=(estSellLocalRate*estimateQuantity)/1000;
		          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*100)/100;
		          	  document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=estimateSellLocalAmountRound;		  	
		          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		          		estimateSellLocalAmount=(estSellLocalRate*estimateQuantity); 
		          		estimateSellLocalAmountRound=Math.round(estimateSellLocalAmount*100)/100; 
		          	  document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value=estimateSellLocalAmountRound;		  	
		   	    }
		           estimateDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
		             if(estimateDeviation!='' && estimateDeviation>0){
		            	estimateSellLocalAmountRound=estimateSellLocalAmountRound*estimateDeviation/100;
		            	estimateSellLocalAmountRound=Math.round(estimateSellLocalAmountRound*100)/100;
		               document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value = estimateSellLocalAmountRound;	 
		           }  
	   			  </c:if>
	   	    	   var contractRate=0;
	 			  <c:if test="${contractType}">
	         contractRate = document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
	         </c:if> 
 	           if(temp == "form"){
 	           		calEstimateContractRate(target,aid);
	       		}
   	           calculateEstimateRevenue(target,aid);
   	           calculateVatAmtEst(aid);
	  }
	       
      function calEstimateContractRate(target,aid){
           <c:if test="${contractType}"> 
             var country =document.forms['pricingListForm'].elements['estimateContractCurrency'+aid].value; 
             if(country=='') { 
             document.forms['pricingListForm'].elements['estimateContractRate'+aid].value=0; 
           } else if(country!=''){
           	var roundValue=0.00;
           	<c:if test="${contractType}"> 
			 	var QTemp=0.00; 
			    var ATemp =0.00;
			    var FTemp =0.00;
			    var ETemp =0.00;
			    ETemp= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ; 
			    FTemp =document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;
			    FTemp=FTemp/ETemp;
			    ETemp= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ;				    
			    FTemp=FTemp*ETemp;
            roundValue=Math.round(FTemp*100)/100; 
        	</c:if>
        	<c:if test="${!contractType}"> 
                   var recRateExchange=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value
                   var recRate=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value
                   var recCurrencyRate=recRate*recRateExchange;
                   roundValue=Math.round(recCurrencyRate*100)/100;
                   </c:if>
           if(target!='estimateContractRate'){
           document.forms['pricingListForm'].elements['estimateContractRate'+aid].value=roundValue ;
           }
           calEstimateContractRateAmmount(target,aid);
           }
         </c:if>
     }
      
		function calculateEstimateRevenueAmountByContractRate(target,aid) { 
			   var estSellLocalAmount= document.forms['pricingListForm'].elements['estimateContractRateAmmount'+aid].value ;
			   var estSellExchangeRate= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ;
			   var roundValue=0; 
			  	 if(estSellExchangeRate ==0) {
			         document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=0*1;
			     } else if(estSellExchangeRate >0) {
			       <c:if test="${contractType}"> 
		        	 if(target=='estimateContractRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		      		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
		      	        ETemp= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ;
		      	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal); 
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*100)/100;
		           document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue; 
		      	 }else if(target=='estSellLocalRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		      		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		      		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateDeviation'+aid].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;
		      	        ETemp= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ;
		      	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
		      		   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*100)/100;
		           document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue; 
		      	 }else if(target=='estimateRevenueAmount'){
		      	 }else if(target=='estimateSellRate'){
		      		 var roundValue='0.00';
		      		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		      		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		      		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateDeviation'+aid].value;
		      		 var baseRateVal=1;
		      	 	 var ATemp=0.00; 
		      	     var ETemp =0.00;	    		 
		      		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		      			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
		      	         } 
		      	        if( bassisVal=="cwt" || bassisVal=="%age"){
		      	        	baseRateVal=100;
		      	         }  else if(bassisVal=="per 1000"){
		      	        	 baseRateVal=1000;
		      	       	 } else {
		      	       		baseRateVal=1;  	
		      		    }
		      	        ATemp=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
		      	        ATemp=((ATemp*quantityVal)/baseRateVal);
		      		   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
		    		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
		    			   ATemp=(ATemp*estimateSellDeviation)/100;
		    			    }      	        
		      	        roundValue=Math.round(ATemp*100)/100;
		           		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue;
		      	 	}else{
			      	       var amount=estSellLocalAmount/estSellExchangeRate; 
			    	       roundValue=Math.round(amount*100)/100;
			    	       document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
		            	 }		       
		        	 </c:if>
			       <c:if test="${!contractType}"> 
			      	       var amount=estSellLocalAmount/estSellExchangeRate; 
			    	       roundValue=Math.round(amount*100)/100;
			    	       document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
			        </c:if>
			     }
			     var estimatepasspercentageRound=0;
			     var estimatepasspercentage = 0;
			     var estimateExpense = document.forms['pricingListForm'].elements['estimateExpense'+aid].value; 
			     estimatepasspercentage = (roundValue/estimateExpense)*100;
			  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
			  	 if(document.forms['pricingListForm'].elements['estimateExpense'+aid].value == '' || document.forms['pricingListForm'].elements['estimateExpense'+aid].value == 0){
			  	   	document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
			  	   }else{
			  	   	document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRound;
			  	   } 
		}
		
   	 function calculateRevisionSellLocalAmount(temp,target,aid) {
		 <c:if test="${contractType}"> 
	  	  if(target=='revisionSellLocalRate'){
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;   		
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
		        ATemp=(ATemp*quantityVal)/baseRateVal;
				var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
					   ATemp=(ATemp*revisionSellDeviation)/100;
				}    		        
		     	roundValue=Math.round(ATemp*100)/100;
	    	document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=roundValue; 
				 }else if(target=='revisionSellRate'){
	    			 var roundValue='0.00';
	    			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    			 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;   		
	    			 var baseRateVal=1;
	    		 	 var ATemp=0.00; 
	    		     var ETemp =0.00;	    		 
	    			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    				 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	    		         } 
	    		        if( bassisVal=="cwt" || bassisVal=="%age"){
	    		        	baseRateVal=100;
	    		         }  else if(bassisVal=="per 1000"){
	    		        	 baseRateVal=1000;
	    		       	 } else {
	    		       		baseRateVal=1;  	
	    			    }
	    		        ETemp=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;
	    		        ATemp=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	    		        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
	    				var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
	    				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
	    					   ATemp=(ATemp*revisionSellDeviation)/100;
	    				}		    		        
	    		     	roundValue=Math.round(ATemp*100)/100;
	    	    	document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=roundValue;
			 
				 }else if(target=='revisionSellLocalAmount'){
				  }else{
				 var roundValue='0.00';
				 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
				 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;   		
				 var estimatePayableContractExchangeRateVal=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
				 var estExchangeRateVal=document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value; 
				 var baseRateVal=1;
			 	 var ATemp=0.00; 
			     var ETemp =0.00;	    		 
				 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
					 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
		        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
				var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
				if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
					   ATemp=(ATemp*revisionSellDeviation)/100;
				}
		     	roundValue=Math.round(ATemp*100)/100;
	    		document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=roundValue;
			 	 }		     
	  		</c:if>
	   	<c:if test="${!contractType}"> 
	   	var revisionQuantity =0;
 	  	var revisionLocalRate=0;
 	        revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
 		    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
 		    revisionSellLocalRate =document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value; 
 		    var revisionSellLocalAmount=0;
 		    var revisionSellLocalAmountRound=0; 
 		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
 	         revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
 	         } 
 	        if( basis=="cwt" || basis=="%age"){
 	        	revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity)/100;
 	        	revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*100)/100;
 	       	  document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmountRound;	  	
 	       	} else if(basis=="per 1000"){
 	       		revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity)/1000;
 	       		revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*100)/100;
 	       	  document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmountRound;	  	  	
 	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
 	       		revisionSellLocalAmount=(revisionSellLocalRate*revisionQuantity);
 	       		revisionSellLocalAmountRound=Math.round(revisionSellLocalAmount*100)/100;
 	       	  document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value=revisionSellLocalAmountRound;	  			  	
 		    }
 	        revisionDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
	             if(revisionDeviation!='' && revisionDeviation>0){
	            	 revisionSellLocalAmountRound=revisionSellLocalAmountRound*revisionDeviation/100;
	            	 revisionSellLocalAmountRound=Math.round(revisionSellLocalAmountRound*100)/100;
	               document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value = revisionSellLocalAmountRound;	 
	           } 
			</c:if> 
             var contractRate=0;
 			<c:if test="${contractType}">
         	contractRate = document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
         	</c:if> 
             if(temp ==="form"){
            	 calRevisionContractRate(target,aid);   
         	}   
           calculateRevisionRevenueContract(target,aid);
     }
   	 
   	function calRevisionContractRate(target,aid){ 
        <c:if test="${contractType}"> 
          var country =document.forms['pricingListForm'].elements['revisionContractCurrency'+aid].value; 
          if(country=='') { 
          document.forms['pricingListForm'].elements['revisionContractRate'+aid].value=0; 
        } else if(country!=''){
        	var roundValue=0.00;
        	 <c:if test="${contractType}">
					 	var QTemp=0.00; 
				    var ATemp =0.00;
				    var FTemp =0.00;
				    var ETemp =0.00;
				    ETemp= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ; 
				    FTemp =document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
				    FTemp=FTemp/ETemp;
				    ETemp= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ;				    
				    FTemp=FTemp*ETemp;
	            roundValue=Math.round(FTemp*100)/100;
	            </c:if>
	            <c:if test="${!contractType}">
		            var recRateExchange=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value
		            var recRate=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value
		            var recCurrencyRate=recRate*recRateExchange;
		            roundValue=Math.round(recCurrencyRate*100)/100;
		            
		            </c:if>
				if(target!='revisionContractRate'){
		            document.forms['pricingListForm'].elements['revisionContractRate'+aid].value=roundValue ;
		         }
        		calRevisionContractRateAmmount(target,aid);
        	}
      	</c:if>
  	}
   	
	function  calculateRevisionRevenueAmountByContractRate(target,aid){ 
		   var revisionSellLocalAmount= document.forms['pricingListForm'].elements['revisionContractRateAmmount'+aid].value ;
		   var revisionSellExchangeRate= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ;
		   var roundValue=0; 
		  	 if(revisionSellExchangeRate ==0) {
		         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=0*1;
		     } else if(revisionSellExchangeRate >0) {
		       <c:if test="${contractType}"> 
		      	 if(target=='revisionContractRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
		    	        ETemp= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue; 
		    	 }else if(target=='revisionSellLocalRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
		    	        ETemp= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue; 
		    	 }else if(target=='revisionRevenueAmount'){
		    	 }else if(target=='revisionSellRate'){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
		    	        ATemp=((ATemp*quantityVal)/baseRateVal);
						var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
						if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
							   ATemp=(ATemp*revisionSellDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue; 
		    	 }else{
				       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
				       roundValue=Math.round(amount*100)/100;
				       document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue ; 
		          	 }		       
		      	 </c:if>
		       <c:if test="${!contractType}"> 
				       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
				       roundValue=Math.round(amount*100)/100;
				       document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue ;
		        </c:if>
		     }
		      var revisionExpense = document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
		      var revisionPassPercentageRound=0;
		      var revisionPassPercentage=0; 
		      revisionPassPercentage = (roundValue/revisionExpense)*100;
		  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
		  	  if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
		  	   	document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
		  	   }else{
		  	    document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionPassPercentageRound;
		  	   }  
	}
	
	function driverCommission(aid){
		var chargeCheck=document.forms['pricingListForm'].elements['chargeCodeValidationVal'].value;
		if(chargeCheck=='' || chargeCheck==null){
		var actualRevenue="0";
		var distributionAmount="0";
		var estimateRevenueAmount="0";
		var driverCommissionSetUp='${driverCommissionSetUp}';
		var revisionRevenueAmount="0"; 
		var category=document.forms['pricingListForm'].elements['category'+aid].value;
		var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value;
		var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
		<c:if test="${discountUserFlag.pricingActual && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">
		 actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
		 </c:if>
		 <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0}"> 
	     distributionAmount=document.forms['pricingListForm'].elements['distributionAmount'+aid].value;
	     </c:if>
	     estimateRevenueAmount=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
	     revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value; 
	     var sid='<%=request.getParameter("sid")%>';
		if(category!=null && category!='' && vendorCode!=null && vendorCode!='' && chargeCode!=null && chargeCode!='' && (driverCommissionSetUp=='true' || driverCommissionSetUp==true)){
			 var url="driverCommissionAjax.html?ajax=1&decorator=simple&popup=true&sid="+sid+"&actualRevenue="+actualRevenue+"&accountLineDistribution="+distributionAmount+"&accountEstimateRevenueAmount="+estimateRevenueAmount+"&accountRevisionRevenueAmount="+revisionRevenueAmount+"&chargeCode="+chargeCode+"&vendorCode="+vendorCode;
			http100.open("GET", url, true);
		    http100.onreadystatechange = function(){handleHttpResponseDriverCommission(aid);}
		    http100.send(null);
			}} 
	}
	function handleHttpResponseDriverCommission(aid) { 
		if (http100.readyState == 4) {
			var results = http100.responseText
			results = results.trim();
		    if(results.length>1 && results!='0.00')
	        {
		    	results=Math.round(results*100)/100;
		    	document.forms['pricingListForm'].elements['actualExpense'+aid].value=results;
			    document.forms['pricingListForm'].elements['localAmount'+aid].value=results;
	        }    } }
	var http100 = getHTTPObject77();
	function calculateVatAmtEst(aid){
    	<c:if test="${systemDefaultVatCalculation=='true'}">
		var estVatAmt=0;
		if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
		var estVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
		var estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value);
		<c:if test="${contractType}">
		  estimateRevenueAmount= eval(document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value);
		</c:if>
		estVatAmt=(estimateRevenueAmount*estVatPercent)/100;
		estVatAmt=Math.round(estVatAmt*100)/100; 
		document.forms['pricingListForm'].elements['estVatAmt'+aid].value=estVatAmt;
		}
	    </c:if>
	}
	
    function calculateEstimateRevenue(target,aid){
	   var contractRate=0;
		<c:if test="${contractType}">
   			contractRate = document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
   		</c:if>
	   var estSellLocalAmount= document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value ;
	   var estSellExchangeRate= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ;
	   var roundValue=0; 
	  	 if(estSellExchangeRate ==0) {
	         document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=0*1;
	     } else if(estSellExchangeRate >0) {
	    	 <c:if test="${contractType}"> 
         	 if(target=='estimateContractRate'){
       		 var roundValue='0.00';
       		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
       		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
       		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
       		 var baseRateVal=1;
       	 	 var ATemp=0.00; 
       	     var ETemp =0.00;	    		 
       		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
       			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
       	         } 
       	        if( bassisVal=="cwt" || bassisVal=="%age"){
       	        	baseRateVal=100;
       	         }  else if(bassisVal=="per 1000"){
       	        	 baseRateVal=1000;
       	       	 } else {
       	       		baseRateVal=1;  	
       		    }
       	        ATemp=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
       	        ETemp= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ;
       	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
	         		   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	        		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
	        			   ATemp=(ATemp*estimateSellDeviation)/100;
	        			    }
       	        roundValue=Math.round(ATemp*100)/100;
            document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue; 
       	 }else if(target=='estSellLocalRate'){
       		 var roundValue='0.00';
       		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
       		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
       		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
       		 var baseRateVal=1;
       	 	 var ATemp=0.00; 
       	     var ETemp =0.00;	    		 
       		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
       			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
       	         } 
       	        if( bassisVal=="cwt" || bassisVal=="%age"){
       	        	baseRateVal=100;
       	         }  else if(bassisVal=="per 1000"){
       	        	 baseRateVal=1000;
       	       	 } else {
       	       		baseRateVal=1;  	
       		    }
       	        ATemp=document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value;
       	        ETemp= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ;
       	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
	         		   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
	        		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
	        			   ATemp=(ATemp*estimateSellDeviation)/100;
	        			    }
       	        roundValue=Math.round(ATemp*100)/100;
            document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue; 
       	 }else if(target=='estimateRevenueAmount'){
       	 }else if(target=='estimateSellRate'){
       		 var roundValue='0.00';
       		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
       		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
       		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
       		 var baseRateVal=1;
       	 	 var ATemp=0.00; 
       	     var ETemp =0.00;	    		 
       		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
       			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
       	         } 
       	        if( bassisVal=="cwt" || bassisVal=="%age"){
       	        	baseRateVal=100;
       	         }  else if(bassisVal=="per 1000"){
       	        	 baseRateVal=1000;
       	       	 } else {
       	       		baseRateVal=1;  	
       		    }
       	        ATemp=document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
       	        ATemp=((ATemp*quantityVal)/baseRateVal);
        		   var estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
       		   if(estimateSellDeviation!='' && estimateSellDeviation>0){ 
       			   ATemp=(ATemp*estimateSellDeviation)/100;
       			    }	        	        
       	        roundValue=Math.round(ATemp*100)/100;
            		document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue;
       		 
       	 		}else{
	      	       var amount=estSellLocalAmount/estSellExchangeRate; 
	    	       roundValue=Math.round(amount*100)/100;
	    	       document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
	    	     }		       
         	 </c:if>
	       <c:if test="${!contractType}"> 
	      	       var amount=estSellLocalAmount/estSellExchangeRate; 
	    	       roundValue=Math.round(amount*100)/100;
	    	       document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value=roundValue ; 
	        </c:if>
	     }
	     var estimatepasspercentageRound=0;
	     var estimatepasspercentage = 0;
	     var estimateExpense = document.forms['pricingListForm'].elements['estimateExpense'+aid].value; 
	     estimatepasspercentage = (roundValue/estimateExpense)*100;
	  	 estimatepasspercentageRound=Math.round(estimatepasspercentage);
	  	 if(document.forms['pricingListForm'].elements['estimateExpense'+aid].value == '' || document.forms['pricingListForm'].elements['estimateExpense'+aid].value == 0){
	  	   	document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value='';
	  	   }else{
	  	   	document.forms['pricingListForm'].elements['estimatePassPercentage'+aid].value=estimatepasspercentageRound;
	  	   }   	   
	  	   if(target!='estimateSellRate'){
	  	 		calculateSellEstimateRate(target,aid);
	  	   } 
	  		calculateEstimateSellLocalRate(target,aid);
	  		<c:if test="${contractType}">
		        try{
		        calEstimateContractRate(target,aid);
		        }catch(e){}
	        </c:if>  
	  		calculateVatAmtEst(aid); 
      }
 
      function calculateRevisionRevenueContract(target,aid) {
	    	var contractRate=0;
			<c:if test="${contractType}">
	    		contractRate = document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	    	</c:if>
	    	   var revisionSellLocalAmount= document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value ;
	    	   var revisionSellExchangeRate= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ;
	    	   var roundValue=0; 
	    	  	 if(revisionSellExchangeRate ==0) {
	    	         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=0*1;
	    	     } else if(revisionSellExchangeRate >0) {
	    	    	 <c:if test="${contractType}"> 
	    	      	 if(target=='revisionContractRate'){
	    	    		 var roundValue='0.00';
	    	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    	    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	    	    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	    	    		 var baseRateVal=1;
	    	    	 	 var ATemp=0.00; 
	    	    	     var ETemp =0.00;	    		 
	    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    	    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	    	    	         } 
	    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	    	        	baseRateVal=100;
	    	    	         }  else if(bassisVal=="per 1000"){
	    	    	        	 baseRateVal=1000;
	    	    	       	 } else {
	    	    	       		baseRateVal=1;  	
	    	    		    }
	    	    	        ATemp=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	    	    	        ETemp= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ;
	    	    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
	    					var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
	    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
	    						   ATemp=(ATemp*revisionSellDeviation)/100;
	    					}
	    	    	        roundValue=Math.round(ATemp*100)/100;
	    	         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue; 
	    	    	 }else if(target=='revisionSellLocalRate'){
	    	    		 var roundValue='0.00';
	    	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    	    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	    	    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	    	    		 var baseRateVal=1;
	    	    	 	 var ATemp=0.00; 
	    	    	     var ETemp =0.00;	    		 
	    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    	    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	    	    	         } 
	    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	    	        	baseRateVal=100;
	    	    	         }  else if(bassisVal=="per 1000"){
	    	    	        	 baseRateVal=1000;
	    	    	       	 } else {
	    	    	       		baseRateVal=1;  	
	    	    		    }
	    	    	        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value;
	    	    	        ETemp= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ;
	    	    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
	    					var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
	    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
	    						   ATemp=(ATemp*revisionSellDeviation)/100;
	    					}
	    	    	        roundValue=Math.round(ATemp*100)/100;
	    	         document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue; 
	    	    	 }else if(target=='revisionRevenueAmount'){
	    	    	 }else if(target=='revisionSellRate'){
	    	    		 var roundValue='0.00';
	    	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    	    		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
	    	    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	    	    		 var baseRateVal=1;
	    	    	 	 var ATemp=0.00; 
	    	    	     var ETemp =0.00;	    		 
	    	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    	    			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
	    	    	         } 
	    	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	    	        	baseRateVal=100;
	    	    	         }  else if(bassisVal=="per 1000"){
	    	    	        	 baseRateVal=1000;
	    	    	       	 } else {
	    	    	       		baseRateVal=1;  	
	    	    		    }
	    	    	        ATemp=document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
	    	    	        ATemp=((ATemp*quantityVal)/baseRateVal);
	    					var revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
	    					if(revisionSellDeviation!='' && revisionSellDeviation>0){ 
	    						   ATemp=(ATemp*revisionSellDeviation)/100;
	    					}
	    	    	        roundValue=Math.round(ATemp*100)/100;
	    	         		document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue;
	    	    		 
	    	    	 }else{
	    	    	       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
	    	    	       roundValue=Math.round(amount*100)/100;
	    	    	       document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue ; 
	    	          	 }		       
	    	      	 </c:if>
	    	       <c:if test="${!contractType}"> 
			    	       var amount=revisionSellLocalAmount/revisionSellExchangeRate; 
			    	       roundValue=Math.round(amount*100)/100;
			    	       document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value=roundValue ; 
	    	        </c:if>
	    	     }
	    	      var revisionExpense = document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
	    	      var revisionPassPercentageRound=0;
	    	      var revisionPassPercentage=0; 
	    	      revisionPassPercentage = (roundValue/revisionExpense)*100;
	    	  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
	    	  	  if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
	    	  	   	document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
	    	  	   }else{
	    	  	    document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionPassPercentageRound;
	    	  	   }   
	   	  	   if(target!='revisionSellRate'){
	    	     calculateRevisionSelllRate(target,aid);
	   	  	   }
	     	     calculateRevisionSellLocalRate(target,aid);
	     	 <c:if test="${contractType}">
	   	     try{
	   	     calRevisionContractRate(target,aid);   
	   	     }catch(e){}
	   	     </c:if> 
	   	    calculateVatAmtRevision(aid); 
	}
	    
      function calculateSellEstimateRate(target,aid){
   	   <c:if test="${contractType}">
   	   if((target=='estimateSellQuantity')||(target=='estSellLocalRate')||(target=='estimateContractRate')||(target=='basis')){
   	      var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value;
   	      var estimatePayableContractRate=document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
   	      estimatePayableContractRate=Math.round(estimatePayableContractRate*100)/100;
   	      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
   	      var roundValue=Math.round(estimateRate*100)/100;
   	      document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=roundValue ;
   	   }else{
  	   	    var estimateQuantity =0;
	   	    var estimateDeviation=100;
	   	    estimateQuantity=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value; 
	   	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
	   	    var estimateRevenueAmount =eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value); 
	   	    var estimateSellRate=0;
	   	    var estimateSellRateRound=0; 
	   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
	            estimateQuantity =document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	            } 
	           if( basis=="cwt" || basis=="%age"){
	        	   estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
	        	   estimateSellRateRound=Math.round(estimateSellRate*100)/100;
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;	  	
	          	}  else if(basis=="per 1000"){
	          		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
	          		estimateSellRateRound=Math.round(estimateSellRate*100)/100;
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		  	
	          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	          		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
	          		estimateSellRateRound=Math.round(estimateSellRate*100)/100; 
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		  	
	   	    }
	           estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value 
	   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
	   	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation;
	   	    	estimateSellRateRound=Math.round(estimateSellRateRound*100)/100;
	   	    document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		
	   	    }   }
			</c:if>
			<c:if test="${!contractType}">
	   	    var estimateQuantity =0;
	   	    var estimateDeviation=100;
	   	    estimateQuantity=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value; 
	   	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
	   	    var estimateRevenueAmount =eval(document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value); 
	   	    var estimateSellRate=0;
	   	    var estimateSellRateRound=0; 
	   	    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
	            estimateQuantity =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
	            } 
	           if( basis=="cwt" || basis=="%age"){
	        	   estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
	        	   estimateSellRateRound=Math.round(estimateSellRate*100)/100;
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;	  	
	          	}  else if(basis=="per 1000"){
	          		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
	          		estimateSellRateRound=Math.round(estimateSellRate*100)/100;
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		  	
	          	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	          		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
	          		estimateSellRateRound=Math.round(estimateSellRate*100)/100; 
	          	  document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		  	
	   	    }
	           estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value 
	   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
	   	    	estimateSellRateRound=estimateSellRateRound*100/estimateSellDeviation;
	   	    	estimateSellRateRound=Math.round(estimateSellRateRound*100)/100;
	   	    document.forms['pricingListForm'].elements['estimateSellRate'+aid].value=estimateSellRateRound;		
	   	    }  
			</c:if>
   }
      
   function calculateEstimateSellLocalRate(target,aid){
   	   <c:if test="${contractType}">  
  	 if(target=='estSellLocalAmount'){
		 var roundValue='0.00';
		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value;
		 var baseRateVal=1;
	 	 var ATemp=0.00; 
	     var ETemp =0.00;	    		 
		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	         } 
	        if( bassisVal=="cwt" || bassisVal=="%age"){
	        	baseRateVal=100;
	         }  else if(bassisVal=="per 1000"){
	        	 baseRateVal=1000;
	       	 } else {
	       		baseRateVal=1;  	
		    }
	        ATemp=document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value;
	        ATemp=(ATemp*baseRateVal)/quantityVal;
		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    	ATemp=ATemp*100/estimateDeviationVal;
		    	ATemp=Math.round(ATemp*100)/100;
		    }
    	document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=ATemp; 
	 	}else if(target=='estimateSellRate'){
		 var roundValue='0.00';
	     var ETemp=0.00;
	     var ATemp=0.00;
	     ETemp = document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;  
	     ATemp = document.forms['pricingListForm'].elements['estimateSellRate'+aid].value;
	     ATemp=ATemp*ETemp;
	     roundValue=Math.round(ATemp*100)/100;
   		document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue; 
	 	}else if(target=='estimateRevenueAmount'){
		 var roundValue='0.00';
	     var ETemp=0.00;
	     var ATemp=0.00;
		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		 var quantityVal=document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value;
		 var estimateDeviationVal=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value; 
		 var baseRateVal=1;
		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
			 quantityVal = document.forms['pricingListForm'].elements['estimateSellQuantity'+aid].value=1;
	         } 
	        if( bassisVal=="cwt" || bassisVal=="%age"){
	        	baseRateVal=100;
	         }  else if(bassisVal=="per 1000"){
	        	 baseRateVal=1000;
	       	 } else {
	       		baseRateVal=1;  	
		    }
	        ATemp=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;  
	        ATemp=(ATemp*baseRateVal)/quantityVal;
	        
		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    	ATemp=ATemp*100/estimateDeviationVal;
		    }
	     ETemp = document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value;  
	     ATemp=ATemp*ETemp;
	     roundValue=Math.round(ATemp*100)/100;
   		document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue;
		 
	 	}else if(target=='estSellLocalRate'){
	 	}else{
		 	var QTemp=0.00; 
		    var ATemp =0.00;
		    var FTemp =0.00;
		    var ETemp =0.00;
		    ETemp= document.forms['pricingListForm'].elements['estimateContractExchangeRate'+aid].value*1 ; 
		    FTemp =document.forms['pricingListForm'].elements['estimateContractRate'+aid].value;
		    FTemp=FTemp/ETemp;
		    ETemp= document.forms['pricingListForm'].elements['estSellExchangeRate'+aid].value*1 ;				    
		    FTemp=FTemp*ETemp;
       roundValue=Math.round(FTemp*100)/100;
       document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=roundValue;
     	 }        
   	   </c:if>
   	   <c:if test="${!contractType}"> 
   	    var estimateQuantity =0;
   	    var estimateRevenueAmount =0;
   		    estimateQuantity=document.forms['pricingListForm'].elements['estimateQuantity'+aid].value; 
   		    var basis =document.forms['pricingListForm'].elements['basis'].value; 
   		    estimateRevenueAmount =document.forms['pricingListForm'].elements['estSellLocalAmount'+aid].value; 
   		    var estimateSellRate=0;
   		    var estimateSellRateRound=0; 
   		    if(estimateQuantity=='' ||estimateQuantity=='0' ||estimateQuantity=='0.0' ||estimateQuantity=='0.00') {
   	         estimateQuantity =document.forms['pricingListForm'].elements['estimateQuantity'+aid].value=1;
   	         } 
   	        if( basis=="cwt" || basis=="%age"){
   	        	estimateSellRate=(estimateRevenueAmount/estimateQuantity)*100;
   	        	estimateSellRateRound=Math.round(estimateSellRate*100)/100;
   	       	  document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=estimateSellRateRound;	  	
   	       	}  else if(basis=="per 1000"){
   	       		estimateSellRate=(estimateRevenueAmount/estimateQuantity)*1000;
   	       		estimateSellRateRound=Math.round(estimateSellRate*100)/100;
   	       	  document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=estimateSellRateRound;		  	
   	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
   	       		estimateSellRate=(estimateRevenueAmount/estimateQuantity); 
   	       		estimateSellRateRound=Math.round(estimateSellRate*100)/100; 
   	       	  document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=estimateSellRateRound;		  	
   		    }
   	        estimateSellDeviation=document.forms['pricingListForm'].elements['estimateSellDeviation'+aid].value 
   	   	    if(estimateSellDeviation!='' && estimateSellDeviation>0){
   	   	    	estimateSellRate=estimateSellRateRound*100/estimateSellDeviation;
   	   	    	estimateSellRateRound=Math.round(estimateSellRate*100)/100;
   	   	    document.forms['pricingListForm'].elements['estSellLocalRate'+aid].value=estimateSellRateRound;		
   	   	    }
   	    </c:if>  
   	 }
   
   function calculateRevisionSelllRate(target,aid){
	   <c:if test="${contractType}">
	   if((target=='revisionSellQuantity')||(target=='revisionSellLocalRate')||(target=='revisionContractRate')||(target=='basis')){
	      var estimatePayableContractExchangeRate=document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value;
	      var estimatePayableContractRate=document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
	      estimatePayableContractRate=Math.round(estimatePayableContractRate*100)/100;
	      var estimateRate=estimatePayableContractRate/estimatePayableContractExchangeRate;
	      var roundValue=Math.round(estimateRate*100)/100;
	      document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=roundValue ;
	   }else{
	        var revisionQuantity =0;
        var revisionDeviation=100;
        revisionQuantity=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value; 
	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
	    var revisionRevenueAmount =eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value); 
	    var revisionSellRate=0;
	    var revisionSellRateRound=0; 
	    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
         revisionQuantity =document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
         } 
        if( basis=="cwt" || basis=="%age"){
        	revisionSellRate=(revisionRevenueAmount/revisionQuantity)*100;
        	revisionSellRateRound=Math.round(revisionSellRate*100)/100;
       	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  	
       	} else if(basis=="per 1000"){
       		revisionSellRate=(revisionRevenueAmount/revisionQuantity)*1000;
       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
       	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  	  	
       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
       		revisionSellRate=(revisionRevenueAmount/revisionQuantity);
       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
       	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  			  	
	    } 
        revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value 
	    if(revisionSellDeviation!='' && revisionSellDeviation>0){
	    	revisionSellRateRound=revisionSellRateRound*100/revisionSellDeviation;
	    	revisionSellRateRound=Math.round(revisionSellRateRound*100)/100;
	    document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;		 
	       }  }
	   </c:if>
	   <c:if test="${!contractType}">
       var revisionQuantity =0;
    var revisionDeviation=100;
    revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
    var revisionRevenueAmount =eval(document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value); 
    var revisionSellRate=0;
    var revisionSellRateRound=0; 
    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
     revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
     } 
    if( basis=="cwt" || basis=="%age"){
    	revisionSellRate=(revisionRevenueAmount/revisionQuantity)*100;
    	revisionSellRateRound=Math.round(revisionSellRate*100)/100;
   	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  	
   	} else if(basis=="per 1000"){
   		revisionSellRate=(revisionRevenueAmount/revisionQuantity)*1000;
   		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
   	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  	  	
   	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
   		revisionSellRate=(revisionRevenueAmount/revisionQuantity);
   		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
   	  document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;	  			  	
    } 
    revisionSellDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value 
    if(revisionSellDeviation!='' && revisionSellDeviation>0){
    	revisionSellRateRound=revisionSellRateRound*100/revisionSellDeviation;
    	revisionSellRateRound=Math.round(revisionSellRateRound*100)/100;
    document.forms['pricingListForm'].elements['revisionSellRate'+aid].value=revisionSellRateRound;		 
       }    		   
	   </c:if>
}
   
   function calculateRevisionSellLocalRate(target,aid){
   	 var roundValue=0.00; 
       <c:if test="${contractType}">
       if(target=='revisionSellLocalAmount'){
  		 var roundValue='0.00';
  		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
  		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
  		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
  		 var baseRateVal=1;
  	 	 var ATemp=0.00; 
  	     var ETemp =0.00;	    		 
  		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
  			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
  	         } 
  	        if( bassisVal=="cwt" || bassisVal=="%age"){
  	        	baseRateVal=100;
  	         }  else if(bassisVal=="per 1000"){
  	        	 baseRateVal=1000;
  	       	 } else {
  	       		baseRateVal=1;  	
  		    }
  	        ATemp=document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value;
  	        ATemp=(ATemp*baseRateVal)/quantityVal;
  		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
  		    	ATemp=ATemp*100/estimateDeviationVal;
  		    	ATemp=Math.round(ATemp*100)/100;
  		    }
       document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=ATemp; 
  	 }else if(target=='revisionSellRate'){
  		 var roundValue='0.00';
  	     var ETemp=0.00;
  	     var ATemp=0.00;
  	     ETemp = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;  
  	     ATemp = document.forms['pricingListForm'].elements['revisionSellRate'+aid].value;
  	     ATemp=ATemp*ETemp;
  	     roundValue=Math.round(ATemp*100)/100;
      	document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue; 
  	 }else if(target=='revisionRevenueAmount'){
  		 var roundValue='0.00';
  	     var ETemp=0.00;
  	     var ATemp=0.00;
  		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
  		 var quantityVal=document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value;
  		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value;
  		 var baseRateVal=1;
  		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
  			 quantityVal = document.forms['pricingListForm'].elements['revisionSellQuantity'+aid].value=1;
  	         } 
  	        if( bassisVal=="cwt" || bassisVal=="%age"){
  	        	baseRateVal=100;
  	         }  else if(bassisVal=="per 1000"){
  	        	 baseRateVal=1000;
  	       	 } else {
  	       		baseRateVal=1;  	
  		    }
  	        ATemp=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;  
  	        ATemp=(ATemp*baseRateVal)/quantityVal; 
  		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
  		    	ATemp=ATemp*100/estimateDeviationVal;
  		    }
  	     ETemp = document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value;  
  	     ATemp=ATemp*ETemp;
  	     roundValue=Math.round(ATemp*100)/100;
      	document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue; 
  	 }else if(target=='revisionSellLocalRate'){
  	 }else{
  		 	var QTemp=0.00; 
  		    var ATemp =0.00;
  		    var FTemp =0.00;
  		    var ETemp =0.00;
  		    ETemp= document.forms['pricingListForm'].elements['revisionContractExchangeRate'+aid].value*1 ; 
  		    FTemp =document.forms['pricingListForm'].elements['revisionContractRate'+aid].value;
  		    FTemp=FTemp/ETemp;
  		    ETemp= document.forms['pricingListForm'].elements['revisionSellExchangeRate'+aid].value*1 ;				    
  		    FTemp=FTemp*ETemp;
          roundValue=Math.round(FTemp*100)/100;
          document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=roundValue;
        	 }
		</c:if>
		<c:if test="${!contractType}"> 
	    var revisionQuantity =0;
	    var revisionExpense=0;
	        revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
		    var basis =document.forms['pricingListForm'].elements['basis'].value; 
		    revisionRevenue = document.forms['pricingListForm'].elements['revisionSellLocalAmount'+aid].value; 
		    var revisionSellRate=0;
		    var revisionSellRateRound=0; 
		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
	         revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	         } 
	        if( basis=="cwt" || basis=="%age"){
	        	revisionSellRate=(revisionRevenue/revisionQuantity)*100;
	        	revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellRateRound;	  	
	       	} else if(basis=="per 1000"){
	       		revisionSellRate=(revisionRevenue/revisionQuantity)*1000;
	       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellRateRound;	  	  	
	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	       		revisionSellRate=(revisionRevenue/revisionQuantity);
	       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellRateRound;	  			  	
		    } 
	        revisionDeviation=document.forms['pricingListForm'].elements['revisionSellDeviation'+aid].value 
		    if(revisionDeviation!='' && revisionDeviation>0){
		    	revisionSellRateRound=revisionSellRateRound*100/revisionDeviation;
		    	revisionSellRateRound=Math.round(revisionSellRateRound*100)/100;
		    document.forms['pricingListForm'].elements['revisionSellLocalRate'+aid].value=revisionSellRateRound;
	
 		 }
	</c:if>
}
   
   
   
   function  calRevisionPayableContractRate(aid){
  	 var roundValue=0.00; 
		 var revisionQuantity =0;
	    var revisionExpense=0;
	        revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
		    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
		    revisionRevenue = document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value; 
		    var revisionSellRate=0;
		    var revisionSellRateRound=0; 
		    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
	         revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	         } 
	        if( basis=="cwt" || basis=="%age"){
	        	revisionSellRate=(revisionRevenue/revisionQuantity)*100;
	        	revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=revisionSellRateRound;	  	
	       	} else if(basis=="per 1000"){
	       		revisionSellRate=(revisionRevenue/revisionQuantity)*1000;
	       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=revisionSellRateRound;	  	  	
	       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	       		revisionSellRate=(revisionRevenue/revisionQuantity);
	       		revisionSellRateRound=Math.round(revisionSellRate*100)/100;
	       	  document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=revisionSellRateRound;	  			  	
		    }
	        revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value 
		    if(revisionDeviation!='' && revisionDeviation>0){
		    revisionSellRateRound=revisionSellRateRound*100/revisionDeviation;
		    revisionSellRateRound=Math.round(revisionSellRateRound*100)/100;
		    document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=revisionSellRateRound;
		    }
	}
   
   function calRevisionLocalRate(temp,target,aid){ 
       var country =document.forms['pricingListForm'].elements['revisionCurrency'+aid].value; 
       if(country=='') {
       alert("Please select currency "); 
       document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=0; 
     } else if(country!=''){  
     	 	<c:if test="${contractType}"> 
		        if(target=='revisionLocalAmount'+aid){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value;
		    	        ATemp=(ATemp*baseRateVal)/quantityVal;
		    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    		    	ATemp=ATemp*100/estimateDeviationVal;
		    		    	ATemp=Math.round(ATemp*100)/100;
		    		    }
		         document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=ATemp; 
		    	 }else if(target=='revisionRate'+aid){
		    		 var roundValue='0.00';
		    	     var ETemp=0.00;
		    	     var ATemp=0.00;
		    	     ETemp = document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;  
		    	     ATemp = document.forms['pricingListForm'].elements['revisionRate'+aid].value;
		    	     ATemp=ATemp*ETemp;
		    	     roundValue=Math.round(ATemp*100)/100;
		        	document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue; 
		    	 }else if(target=='revisionExpense'+aid){
		    		 var roundValue='0.00';
		    	     var ETemp=0.00;
		    	     var ATemp=0.00;
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;  
		    	        ATemp=(ATemp*baseRateVal)/quantityVal; 
		    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
		    		    	ATemp=ATemp*100/estimateDeviationVal;
		    		    }
		    	     ETemp = document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;  
		    	     ATemp=ATemp*ETemp;
		    	     roundValue=Math.round(ATemp*100)/100;
		        	document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue; 
		    	 }else if(target=='revisionLocalRate'+aid){
		    	 }else{
				 	var QTemp=0.00; 
				    var ATemp =0.00;
				    var FTemp =0.00;
				    var ETemp =0.00;
				    ETemp= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ; 
				    FTemp =document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
				    FTemp=FTemp/ETemp;
				    ETemp= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ;				    
				    FTemp=FTemp*ETemp;
		     roundValue=Math.round(FTemp*100)/100;
		     document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue;
		    	 } 
		        </c:if>
		        <c:if test="${!contractType}"> 
		        var recRateExchange=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value
		        var recRate=document.forms['pricingListForm'].elements['revisionRate'+aid].value
		        var recCurrencyRate=recRate*recRateExchange;
		        var roundValue=Math.round(recCurrencyRate*100)/100;
		        document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue ; 
				</c:if>
	         	calculateRevisionLocalAmount('cal',target,aid);
		        if(temp=='cal'){
		        	calRevisionPayableContractRateByBase('none',aid);
		        }
		    }
       }
   
   function calRevisionPayableContractRateAmmount(target,aid) { 
	   <c:if test="${contractType}"> 
	   if(target=='revisionLocalRate'+aid){
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    } 
		        ATemp=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
		        ETemp=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
		        ATemp=ATemp/ETemp;
		        ETemp=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
		        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
		     	roundValue=Math.round(ATemp*100)/100;
	    		document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value=roundValue; 
			}else if(target=='revisionRate'+aid){
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ETemp=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
		        ATemp=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
		        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}		        
		     	roundValue=Math.round(ATemp*100)/100;
	    		document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value=roundValue; 
				 }else if(target=='revisionPayableContractRateAmmount'+aid){
				  }else{
						 var roundValue='0.00';
						 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
						 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
						 var baseRateVal=1;
					 	 var ATemp=0.00; 
					     var ETemp =0.00;	    		 
						 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
							 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
					         } 
					        if( bassisVal=="cwt" || bassisVal=="%age"){
					        	baseRateVal=100;
					         }  else if(bassisVal=="per 1000"){
					        	 baseRateVal=1000;
					       	 } else {
					       		baseRateVal=1;  	
						    }
					        ATemp=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
					        ATemp=(ATemp*quantityVal)/baseRateVal;
							var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
							if(revisionDeviation!='' && revisionDeviation>0){ 
								   ATemp=(ATemp*revisionDeviation)/100;
							}
					     	roundValue=Math.round(ATemp*100)/100;
				    	document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value=roundValue;
			 	 } 
	             calculateRevisionExpenseByContractRate(target,aid) ;     
	   			</c:if>
	  		}
   
   function calculateRevisionLocalAmount(temp,target,aid){
		 <c:if test="${contractType}"> 
	  	  if(target=='revisionLocalRate'+aid){
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
		        ATemp=(ATemp*quantityVal)/baseRateVal;
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}	        
		     	roundValue=Math.round(ATemp*100)/100;
	    	document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=roundValue; 
				 }else if(target=='revisionRate'+aid){
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ETemp=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
		        ATemp=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
		        ATemp=(ATemp*ETemp*quantityVal)/baseRateVal;
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
		     	roundValue=Math.round(ATemp*100)/100;
	    	document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=roundValue;
			 
				 }else if(target=='revisionLocalAmount'+aid){
				  }else{
			 var roundValue='0.00';
			 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
			 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;   		
			 var estimatePayableContractExchangeRateVal=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value;
			 var estExchangeRateVal=document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;
			 var baseRateVal=1;
		 	 var ATemp=0.00; 
		     var ETemp =0.00;	    		 
			 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
				 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( bassisVal=="cwt" || bassisVal=="%age"){
		        	baseRateVal=100;
		         }  else if(bassisVal=="per 1000"){
		        	 baseRateVal=1000;
		       	 } else {
		       		baseRateVal=1;  	
			    }
		        ATemp=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
		        ATemp=(((ATemp/estimatePayableContractExchangeRateVal)*estExchangeRateVal)*quantityVal)/baseRateVal;
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
		     	roundValue=Math.round(ATemp*100)/100;
	    		document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=roundValue;
			 	 }		     
	  </c:if>
	   <c:if test="${!contractType}"> 
	   var revisionQuantity =0;
	   var revisionLocalRate=0;
	         revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
	 	    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
	 	     revisionLocalRate =document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value; 
	 	    var revisionLocalAmount=0;
	 	    var revisionLocalAmountRound=0; 
	 	    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
	          revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	          } 
	         if( basis=="cwt" || basis=="%age"){
	        	  revisionLocalAmount=(revisionLocalRate*revisionQuantity)/100;
	        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*100)/100;
	        	  document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmountRound;	  	
	        	} else if(basis=="per 1000"){
	        	  revisionLocalAmount=(revisionLocalRate*revisionQuantity)/1000;
	        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*100)/100;
	        	  document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmountRound;	  	  	
	        	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
	           revisionLocalAmount=(revisionLocalRate*revisionQuantity);
	        	  revisionLocalAmountRound=Math.round(revisionLocalAmount*100)/100;
	        	  document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value=revisionLocalAmountRound;	  			  	
	 	    } 
	         revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	         if(revisionDeviation!='' && revisionDeviation>0){
	         	revisionLocalAmountRound=revisionLocalAmountRound*revisionDeviation/100;
	         	revisionLocalAmountRound=Math.round(revisionLocalAmountRound*100)/100;
	           document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value = revisionLocalAmountRound;	 
	       }  
				  </c:if>
	        var contractRate=0;
			  <c:if test="${contractType}">
	  contractRate = document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
	  </c:if>
	    if(temp == "form"){
	    	calRevisionPayableContractRateByBase(target,aid);
  		} 
		calculateRevisionExpense(target,aid);
	}
	
   function calRevisionPayableContractRateByBase(target,aid){
       <c:if test="${contractType}"> 
         var country =document.forms['pricingListForm'].elements['revisionPayableContractCurrency'+aid].value; 
         if(country=='') { 
         document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=0; 
       } else if(country!=''){
           var roundValue=0.00;
           <c:if test="${contractType}"> 
			 	var QTemp=0.00; 
			    var ATemp =0.00;
			    var FTemp =0.00;
			    var ETemp =0.00;
			    ETemp= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ; 
			    FTemp =document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
			    FTemp=FTemp/ETemp;
			    ETemp= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ;				    
			    FTemp=FTemp*ETemp;
           roundValue=Math.round(FTemp*100)/100;       
           </c:if>
           <c:if test="${!contractType}"> 
                   var recRateExchange=document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value
                   var recRate=document.forms['pricingListForm'].elements['revisionRate'+aid].value
                   var recCurrencyRate=recRate*recRateExchange;
                  roundValue=Math.round(recCurrencyRate*100)/100; 
              	
                  </c:if>
           if(target!='revisionPayableContractRate'+aid){
           	document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value=roundValue ;
           }             	
       calRevisionPayableContractRateAmmount(target,aid);
       }
     </c:if>
 }
   
   function calculateRevisionExpenseByContractRate(target,aid){ 
		var revisionLocalAmount= document.forms['pricingListForm'].elements['revisionPayableContractRateAmmount'+aid].value ;
		var revisionExchangeRate= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ;
		var roundValue=0; 
			 if(revisionExchangeRate ==0) {
		      document.forms['pricingListForm'].elements['revisionExpense'+aid].value=0*1;
		  } else if(revisionExchangeRate >0) {
		    	 <c:if test="${contractType}"> 
		      	 if(target=='revisionPayableContractRate'+aid){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
		    	        ETemp= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue;

		    	 }else if(target=='revisionLocalRate'+aid){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
		    	        ETemp= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ;
		    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue; 
		    	 }else if(target=='revisionExpense'+aid){
		    	 }else if(target=='revisionRate'+aid){
		    		 var roundValue='0.00';
		    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
		    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
		    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
		    		 var baseRateVal=1;
		    	 	 var ATemp=0.00; 
		    	     var ETemp =0.00;	    		 
		    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
		    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		    	         } 
		    	        if( bassisVal=="cwt" || bassisVal=="%age"){
		    	        	baseRateVal=100;
		    	         }  else if(bassisVal=="per 1000"){
		    	        	 baseRateVal=1000;
		    	       	 } else {
		    	       		baseRateVal=1;  	
		    		    }
		    	        ATemp=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
		    	        ATemp=((ATemp*quantityVal)/baseRateVal);
						var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
						if(revisionDeviation!='' && revisionDeviation>0){ 
							   ATemp=(ATemp*revisionDeviation)/100;
						}
		    	        roundValue=Math.round(ATemp*100)/100;
		         		document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue;
		    		 
		    	 }else{
		    	       var amount=revisionLocalAmount/revisionExchangeRate; 
		    	       roundValue=Math.round(amount*100)/100;
		    	       document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue ; 
		          	 }		       
		      	 </c:if>
		       <c:if test="${!contractType}"> 
			       var amount=revisionLocalAmount/revisionExchangeRate; 
			       roundValue=Math.round(amount*100)/100;
			       document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue ; 
		        </c:if>
		  }
		   var revisionRevenue = document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
		   var revisionPassPercentageRound=0;
		   var revisionPassPercentage=0; 
		   revisionPassPercentage = (revisionRevenue/roundValue)*100;
			  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
			  if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
			   	document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
			   }else{
			    document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionPassPercentageRound;
			   }  
	}
   
   function calculateRevisionExpense(target,aid){
	   var contractRate=0;
		  <c:if test="${contractType}">
    contractRate = document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
    </c:if>
   var revisionLocalAmount= document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value ;
   var revisionExchangeRate= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ;
   var roundValue=0; 
  	 if(revisionExchangeRate ==0) {
         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=0*1;
     } else if(revisionExchangeRate >0) { 
    	 <c:if test="${contractType}"> 
      	 if(target=='revisionPayableContractRate'+aid){
    		 var roundValue='0.00';
    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
    		 var baseRateVal=1;
    	 	 var ATemp=0.00; 
    	     var ETemp =0.00;	    		 
    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
    	         } 
    	        if( bassisVal=="cwt" || bassisVal=="%age"){
    	        	baseRateVal=100;
    	         }  else if(bassisVal=="per 1000"){
    	        	 baseRateVal=1000;
    	       	 } else {
    	       		baseRateVal=1;  	
    		    }
    	        ATemp=document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
    	        ETemp= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ;
    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
    	        roundValue=Math.round(ATemp*100)/100;
         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue; 
    	 }else if(target=='revisionLocalRate'+aid){
    		 var roundValue='0.00';
    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
    		 var baseRateVal=1;
    	 	 var ATemp=0.00; 
    	     var ETemp =0.00;	    		 
    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
    	         } 
    	        if( bassisVal=="cwt" || bassisVal=="%age"){
    	        	baseRateVal=100;
    	         }  else if(bassisVal=="per 1000"){
    	        	 baseRateVal=1000;
    	       	 } else {
    	       		baseRateVal=1;  	
    		    }
    	        ATemp=document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value;
    	        ETemp= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ;
    	        ATemp=(((ATemp/ETemp)*quantityVal)/baseRateVal);
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
    	        roundValue=Math.round(ATemp*100)/100;
         document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue; 
    	 }else if(target=='revisionExpense'+aid){
    	 }else if(target=='revisionRate'+aid){
    		 var roundValue='0.00';
    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
    		 var baseRateVal=1;
    	 	 var ATemp=0.00; 
    	     var ETemp =0.00;	    		 
    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
    	         } 
    	        if( bassisVal=="cwt" || bassisVal=="%age"){
    	        	baseRateVal=100;
    	         }  else if(bassisVal=="per 1000"){
    	        	 baseRateVal=1000;
    	       	 } else {
    	       		baseRateVal=1;  	
    		    }
    	        ATemp=document.forms['pricingListForm'].elements['revisionRate'+aid].value;
    	        ATemp=((ATemp*quantityVal)/baseRateVal);
				var revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
				if(revisionDeviation!='' && revisionDeviation>0){ 
					   ATemp=(ATemp*revisionDeviation)/100;
				}
    	        roundValue=Math.round(ATemp*100)/100;
         		document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue; 
    	 }else{
    	       var amount=revisionLocalAmount/revisionExchangeRate; 
    	       roundValue=Math.round(amount*100)/100;
    	       document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue ; 
          	 }		       
      	 </c:if>
       <c:if test="${!contractType}"> 
	       var amount=revisionLocalAmount/revisionExchangeRate; 
	       roundValue=Math.round(amount*100)/100;
	       document.forms['pricingListForm'].elements['revisionExpense'+aid].value=roundValue ; 
        </c:if>
     }
      var revisionRevenue = document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
      var revisionPassPercentageRound=0;
      var revisionPassPercentage=0; 
      revisionPassPercentage = (revisionRevenue/roundValue)*100;
  	  revisionPassPercentageRound=Math.round(revisionPassPercentage); 
  	  if(document.forms['pricingListForm'].elements['revisionExpense'+aid].value == '' || document.forms['pricingListForm'].elements['revisionExpense'+aid].value == 0){
  	   	document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value='';
  	   }else{
  	    document.forms['pricingListForm'].elements['revisionPassPercentage'+aid].value=revisionPassPercentageRound;
  	   }    
 	   if(target!='revisionRate'){
     	calculateRevisionRate(target,aid);
 	   }
     calculateRevisionLocalRate(target,aid);
     <c:if test="${contractType}">
     try{
    calRevisionPayableContractRateByBase(target,aid);
     }catch(e){}
     </c:if>
     calculateREVVatAmt(aid); 
   }
   
   function calculateRevisionLocalRate(target,aid){
	     <c:if test="${contractType}"> 
	         if(target=='revisionLocalAmount'+aid){
	    		 var roundValue='0.00';
	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
	    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	    		 var baseRateVal=1;
	    	 	 var ATemp=0.00; 
	    	     var ETemp =0.00;	    		 
	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	    	         } 
	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	        	baseRateVal=100;
	    	         }  else if(bassisVal=="per 1000"){
	    	        	 baseRateVal=1000;
	    	       	 } else {
	    	       		baseRateVal=1;  	
	    		    }
	    	        ATemp=document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value;
	    	        ATemp=(ATemp*baseRateVal)/quantityVal;
	    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
	    		    	ATemp=ATemp*100/estimateDeviationVal;
	    		    	ATemp=Math.round(ATemp*100)/100;
	    		    }
	         document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=ATemp; 
	    	 }else if(target=='revisionRate'+aid){
	    		 var roundValue='0.00';
	    	     var ETemp=0.00;
	    	     var ATemp=0.00;
	    	     ETemp = document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;  
	    	     ATemp = document.forms['pricingListForm'].elements['revisionRate'+aid].value;
	    	     ATemp=ATemp*ETemp;
	    	     roundValue=Math.round(ATemp*100)/100;
	        	document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue; 
	    	 }else if(target=='revisionExpense'+aid){
	    		 var roundValue='0.00';
	    	     var ETemp=0.00;
	    	     var ATemp=0.00;
	    		 var bassisVal=document.forms['pricingListForm'].elements['basis'+aid].value;
	    		 var quantityVal=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value;
	    		 var estimateDeviationVal=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value;
	    		 var baseRateVal=1;
	    		 if(quantityVal=='' ||quantityVal=='0' ||quantityVal=='0.0' ||quantityVal=='0.00') {
	    			 quantityVal = document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
	    	         } 
	    	        if( bassisVal=="cwt" || bassisVal=="%age"){
	    	        	baseRateVal=100;
	    	         }  else if(bassisVal=="per 1000"){
	    	        	 baseRateVal=1000;
	    	       	 } else {
	    	       		baseRateVal=1;  	
	    		    }
	    	        ATemp=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;  
	    	        ATemp=(ATemp*baseRateVal)/quantityVal; 
	    		    if(estimateDeviationVal!='' && estimateDeviationVal>0){
	    		    	ATemp=ATemp*100/estimateDeviationVal;
	    		    }
	    	     ETemp = document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value;  
	    	     ATemp=ATemp*ETemp;
	    	     roundValue=Math.round(ATemp*100)/100;
	        	document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue; 
	    	 }else if(target=='revisionLocalRate'+aid){
	    	 }else{
			 	var QTemp=0.00; 
			    var ATemp =0.00;
			    var FTemp =0.00;
			    var ETemp =0.00;
			    ETemp= document.forms['pricingListForm'].elements['revisionPayableContractExchangeRate'+aid].value*1 ; 
			    FTemp =document.forms['pricingListForm'].elements['revisionPayableContractRate'+aid].value;
			    FTemp=FTemp/ETemp;
			    ETemp= document.forms['pricingListForm'].elements['revisionExchangeRate'+aid].value*1 ;				    
			    FTemp=FTemp*ETemp;
	     roundValue=Math.round(FTemp*100)/100;
	     document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=roundValue;
	    	 }        
			</c:if>
			<c:if test="${!contractType}">
		    var revisionQuantity =0;
		    var revisionExpense=0;
		        revisionQuantity=document.forms['pricingListForm'].elements['revisionQuantity'+aid].value; 
			    var basis =document.forms['pricingListForm'].elements['basis'+aid].value; 
			    revisionExpense = document.forms['pricingListForm'].elements['revisionLocalAmount'+aid].value; 
			    var revisionRate=0;
			    var revisionRateRound=0; 
			    if(revisionQuantity=='' ||revisionQuantity=='0' ||revisionQuantity=='0.0' ||revisionQuantity=='0.00') {
		         revisionQuantity =document.forms['pricingListForm'].elements['revisionQuantity'+aid].value=1;
		         } 
		        if( basis=="cwt" || basis=="%age"){
		       	  revisionRate=(revisionExpense/revisionQuantity)*100;
		       	  revisionRateRound=Math.round(revisionRate*100)/100;
		       	  document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionRateRound;	  	
		       	} else if(basis=="per 1000"){
		       	  revisionRate=(revisionExpense/revisionQuantity)*1000;
		       	  revisionRateRound=Math.round(revisionRate*100)/100;
		       	  document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionRateRound;	  	  	
		       	} else if(basis!="cwt" && basis!="%age" && basis!="per 1000") {
		          revisionRate=(revisionExpense/revisionQuantity);
		       	  revisionRateRound=Math.round(revisionRate*100)/100;
		       	  document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionRateRound;	  			  	
			    } 
		        revisionDeviation=document.forms['pricingListForm'].elements['revisionDeviation'+aid].value 
			    if(revisionDeviation!='' && revisionDeviation>0){
			    	revisionRateRound=revisionRateRound*100/revisionDeviation;
			    	revisionRateRound=Math.round(revisionRateRound*100)/100;
			    document.forms['pricingListForm'].elements['revisionLocalRate'+aid].value=revisionRateRound;		 
			       }
			</c:if>  
	}
// Compute Functionality for Revision Section End
   
   
// Compute Functionality for Receivable Section Start  
   
   function findRevisedReceivableQuantitys(aid){
		 document.forms['pricingListForm'].elements['checkComputeDescription'].value='';
		 var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	     var billingContract = '${billing.contract}';
	 	 var shipNumber='${serviceOrder.shipNumber}';
	     var rev = document.forms['pricingListForm'].elements['category'+aid].value;
	     if(charge!=''){
	     if(rev == "Internal Cost"){
	     var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'+aid].value  
	     var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
	     http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse900;
	     http2.send(null);
	     } else{
	     var url="invoiceExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id}  ;
	     http2.open("GET", url, true);
	     http2.onreadystatechange = function(){handleHttpResponse9(aid);}
	     http2.send(null); 
	     }
	   }else{
			alert("Please select Charge Code.");
		}
	}
   
   function handleHttpResponse9(aid) {
       if (http2.readyState == 4) {
          var results = http2.responseText
           results = results.trim(); 
          <c:if test="${!contractType}">
           if(results !='') {
          var res = results.split("#"); 
          if(res[9]=="AskUser2"){
          var reply = prompt("Please Enter Quantity", "");
          if(reply){
          if((reply>0 || reply<9)|| reply=='.')  {
          document.forms['pricingListForm'].elements['recQuantity'+aid].value =Math.round(reply*100)/100; 
       	  }else {
       		alert("Please Enter legitimate Quantity");
       	  } 
          }else{
          document.forms['pricingListForm'].elements['recQuantity'+aid].value = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
          }
          }else{
          if(res[7] == undefined){
          document.forms['pricingListForm'].elements['recQuantity'+aid].value ="";
          }else{ 
          document.forms['pricingListForm'].elements['recQuantity'+aid].value =Math.round(res[7]*100)/100;
         }  }
         if(res[10]=="AskUser"){
          var reply = prompt("Please Enter Rate", "");
          if(reply){
          if((reply>0 || reply<9)|| reply=='.'){
            document.forms['pricingListForm'].elements['recRate'+aid].value = Math.round(reply*10000)/10000;
       	  }else {
       		alert("Please Enter legitimate Rate");
       	  }
       	  } else{
       	    document.forms['pricingListForm'].elements['recRate'+aid].value = document.forms['pricingListForm'].elements['oldRecRate'+aid].value; 
       	  } 
          }  else{ 
           if(res[8] == undefined){
           document.forms['pricingListForm'].elements['recRate'+aid].value =""; 
           }else{
          document.forms['pricingListForm'].elements['recRate'+aid].value =Math.round(res[8]*10000)/10000 ;
          }  }
         if(res[10]=="BuildFormula"){
         if(res[11] == undefined){
         document.forms['pricingListForm'].elements['recRate'+aid].value = ""; 
         }else{ 
         document.forms['pricingListForm'].elements['recRate'+aid].value = Math.round(res[11]*10000)/10000;
          }  }
         var Q3 = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
         var Q4 = document.forms['pricingListForm'].elements['recRate'+aid].value;
         if(res[10]=="BuildFormula" && res[11] != undefined){
        	 Q4 =res[11];
           }
         var E1=Q3*Q4; 
         var Q5 = res[5];
         if(res[5] == undefined){
         document.forms['pricingListForm'].elements['basisNew'+aid].value="";
         }else{
         document.forms['pricingListForm'].elements['basisNew'+aid].value=Q5; 
         }
         var type = res[3];
         var E2= "";
         var E3= "";
         if(type == 'Division') {
         	 if(Q5==0) 	 {
         	   E2=0*1;
         	   document.forms['pricingListForm'].elements['actualRevenue'+aid].value = E2;
          }  else {	
         		E2=(Q3*Q4)/Q5;
         		E3=Math.round(E2*100)/100;
         		document.forms['pricingListForm'].elements['actualRevenue'+aid].value = E3;
         	 }    }
         if(type == ' ' || type == '')   {
         		E2=(Q3*Q4);
         		E3=Math.round(E2*100)/100;
         		document.forms['pricingListForm'].elements['actualRevenue'+aid].value = E3;
         }
         if(type == 'Multiply')   {
         		E2=(Q3*Q4*Q5);
         		E3=Math.round(E2*100)/100;
         		document.forms['pricingListForm'].elements['actualRevenue'+aid].value = E3; 
         } 
         if(res[2]=='') {
         } else {
         if(res[2] == undefined){
         document.forms['pricingListForm'].elements['itemNew'+aid].value = "";
         }else{
         document.forms['pricingListForm'].elements['itemNew'+aid].value = res[2];
         }  }
         if(res[3]==' ') { 
         document.forms['pricingListForm'].elements['checkNew'+aid].value = res[3];
         } else{ 
         document.forms['pricingListForm'].elements['checkNew'+aid].value = res[3];
         } 
         if(document.forms['pricingListForm'].elements['description'+aid].value.trim()=='') {
         if(res[4]=='')  {
      	   document.forms['pricingListForm'].elements['checkComputeDescription'].value="computeButton";
      		fillDescription(aid);
         } else{ 
          var chk = res[4];
			var chkReplace=(chk.replace('"','').replace('"',''));
			if(res[12]=='')
			{
				document.forms['pricingListForm'].elements['description'+aid].value = chkReplace;
			}else{
         	document.forms['pricingListForm'].elements['description'+aid].value = chkReplace+" ("+res[12]+")";
         	}  } }
         if(res[5]=='') {
         } else{
         if(res[5] == undefined)  {
         document.forms['pricingListForm'].elements['basisNew'+aid].value = "";
         }  else{
         document.forms['pricingListForm'].elements['basisNew'+aid].value = Q5;
         }  }
         if(res[6]=='') {
         } else{
         document.forms['pricingListForm'].elements['basisNewType'+aid].value = res[6];
         }
         document.forms['pricingListForm'].elements['basis'+aid].value=res[13]; 
         if(res[19]=='Y'){
          	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
          }else{document.forms['pricingListForm'].elements['VATExclude'+aid].value=false; }
          setVatExcluded('1',aid) ; 
         var chargedeviation=res[18];
         document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation; 
         if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
         //document.getElementById("receivableDeviationShow").style.display="block"; 
         //document.getElementById("payableDeviationShow").style.display="block";   
         var buyDependSellAccount=res[15]
         var actualRevenue=0;
         var finalActualRevenue=0; 
         var finalActualExpenceAmount=0;
         var sellDeviation=0;  
         sellDeviation=res[16] 
         var buyDeviation=0; 
         buyDeviation=res[17] 
         actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;  
         finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*100)/100;
         if(buyDependSellAccount=="Y"){
          finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*100)/100;
         }else{
          finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*100)/100;
         }
         document.forms['pricingListForm'].elements['actualRevenue'+aid].value=finalActualRevenue; 
         document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value=sellDeviation;
         document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=sellDeviation;
         if (document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') { 
         }
          }else {
         //document.getElementById("receivableDeviationShow").style.display="none";
        // document.getElementById("payableDeviationShow").style.display="none";
         document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value=0;
         document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=0; 
         var bayRate=0
         bayRate = res[20] ;
         if(bayRate!=0 &&  document.forms['pricingListForm'].elements['payAccDate'+aid].value=='' ){
         var recQuantityPay=0;
         recQuantityPay= document.forms['pricingListForm'].elements['recQuantity'+aid].value;
         Q3=recQuantityPay;
         Q4=bayRate;
         if(type == 'Division') {
         	 if(Q5==0) 	 {
         	   E2=0*1;
          }  else {	
         		E2=(Q3*Q4)/Q5;
         		E3=Math.round(E2*100)/100;
         	 }    }
         if(type == ' ' || type == '')   {
         		E2=(Q3*Q4);
         		E3=Math.round(E2*100)/100;
         }
         if(type == 'Multiply')   {
         		E2=(Q3*Q4*Q5);
         		E3=Math.round(E2*100)/100;
         }
         }
         } 
         <c:if test="${multiCurrency=='Y'}"> 
         var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
         var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value
         var actualRevenueCal= document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
         var recCurrencyRate=recRate*recRateExchange;
         var roundValue=Math.round(recCurrencyRate*100)/100;
         document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=roundValue ; 
         var actualRevenueForeignCal=actualRevenueCal*recRateExchange; 
         actualRevenueForeignCal=Math.round(actualRevenueForeignCal*100)/100;
         document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeignCal; 
         </c:if>
         calculateVatAmt(aid);
         calculateVatAmtRevision(aid);
         calculateVatAmtEst(aid);        
		 var type01 = res[3];
       var typeConversion01=res[5]; 
           }      
       </c:if>
       <c:if test="${contractType}">
       var type01 = '';
       var typeConversion01=0; 
       if(results !='') {
           var res = results.split("#"); 
           <c:choose>
	         <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
	          if(document.forms['pricingListForm'].elements['contractCurrency'+aid].value==res[21]){
	    	   
	       }else{
	    	 document.forms['pricingListForm'].elements['contractCurrency'+aid].value=res[21];
	    	   var recRateCurrency = document.forms['pricingListForm'].elements['recRateCurrency'+aid].value;
	     	   var contractCurrency = document.forms['pricingListForm'].elements['contractCurrency'+aid].value;
	     	   var racValueDate = document.forms['pricingListForm'].elements['racValueDate'+aid].value;
	     	   var contractValueDate = document.forms['pricingListForm'].elements['contractValueDate'+aid].value;
	     	   var recRateExchange = document.forms['pricingListForm'].elements['recRateExchange'+aid].value;
	     	   var contractExchangeRate = document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value;
	    	 rateOnActualizationDate('recRateCurrency~contractCurrency','racValueDate~contractValueDate','recRateExchange~contractExchangeRate');
	       } 
	       </c:when>
	       <c:otherwise> 
           document.forms['pricingListForm'].elements['contractCurrency'+aid].value=res[21];
           findExchangeContractRateByCharges('receivable',aid); 
           </c:otherwise>
		     </c:choose>
           if(res[9]=="AskUser2"){
           var reply = prompt("Please Enter Quantity", "");
           if(reply){
           if((reply>0 || reply<9)|| reply=='.')  {
           document.forms['pricingListForm'].elements['recQuantity'+aid].value =Math.round(reply*100)/100; 
         	} else {
         		alert("Please Enter legitimate Quantity");
         	} 
           }   else{
           document.forms['pricingListForm'].elements['recQuantity'+aid].value = document.forms['pricingListForm'].elements['tempRecQuantity'+aid].value;
           }
           }    else{
             if(res[7] == undefined){
             document.forms['pricingListForm'].elements['recQuantity'+aid].value ="";
             }else{ 
           document.forms['pricingListForm'].elements['recQuantity'+aid].value =Math.round(res[7]*100)/100;
          }  }
          if(res[10]=="AskUser"){
           var reply = prompt("Please Enter Rate", "");
           if(reply){
           if((reply>0 || reply<9)|| reply=='.')
            {
             document.forms['pricingListForm'].elements['contractRate'+aid].value = Math.round(reply*10000)/10000;
         	 } else {
         		alert("Please Enter legitimate Rate");
         	}
         	} else{
         	document.forms['pricingListForm'].elements['contractRate'+aid].value = document.forms['pricingListForm'].elements['oldRecRate'+aid].value; 
         	} 
           }  else{ 
            if(res[8] == undefined){
            document.forms['pricingListForm'].elements['contractRate'+aid].value =""; 
            }else{
           document.forms['pricingListForm'].elements['contractRate'+aid].value =Math.round(res[8]*10000)/10000 ;
           }  }
          if(res[10]=="BuildFormula"){
          if(res[11] == undefined){
          document.forms['pricingListForm'].elements['contractRate'+aid].value = ""; 
          }else{ 
          document.forms['pricingListForm'].elements['contractRate'+aid].value = Math.round(res[11]*10000)/10000;
           }  }
          var Q3 = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
          var Q4 = document.forms['pricingListForm'].elements['contractRate'+aid].value;
          if(res[10]=="BuildFormula" && res[11] != undefined){
          	Q4 = res[11];
          }
          var E1=Q3*Q4; 
          var Q5 = res[5];
          if(res[5] == undefined){
          document.forms['pricingListForm'].elements['basisNew'+aid].value="";
          }else{
          document.forms['pricingListForm'].elements['basisNew'+aid].value=Q5; 
          }
          var type = res[3];
          var E2= "";
          var E3= "";
          if(type == 'Division') {
          	 if(Q5==0) 	 {
          	   E2=0*1;
          	   document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value = E2;
           }  else {	
          		E2=(Q3*Q4)/Q5;
          		E3=Math.round(E2*100)/100;
          		document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value = E3;
          	 }    }
          if(type == ' ' || type == '')   {
          		E2=(Q3*Q4);
          		E3=Math.round(E2*100)/100;
          		document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value = E3;
          }
          if(type == 'Multiply')   {
          		E2=(Q3*Q4*Q5);
          		E3=Math.round(E2*100)/100;
          		document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value = E3; 
          } 
          if(res[2]=='') {
          } else {
          if(res[2] == undefined){
          document.forms['pricingListForm'].elements['itemNew'+aid].value = "";
          }else{
          document.forms['pricingListForm'].elements['itemNew'+aid].value = res[2];
          }  }
          if(res[3]==' ') { 
          document.forms['pricingListForm'].elements['checkNew'+aid].value = res[3];
          } else{ 
          document.forms['pricingListForm'].elements['checkNew'+aid].value = res[3];
          }
          if(document.forms['pricingListForm'].elements['description'+aid].value.trim()=='') { 
          if(res[4]=='')  {
          	document.forms['pricingListForm'].elements['checkComputeDescription'].value="computeButton";
          fillDescription(aid);
          } else{ 
           var chk = res[4];
			var chkReplace=(chk.replace('"','').replace('"',''));
			if(res[12]=='')
			{
				document.forms['pricingListForm'].elements['description'+aid].value = chkReplace;
			}else{
          	document.forms['pricingListForm'].elements['description'+aid].value = chkReplace+" ("+res[12]+")";
          	}  } }
          if(res[5]=='') {
          } else{
          if(res[5] == undefined)  {
          document.forms['pricingListForm'].elements['basisNew'+aid].value = "";
          }  else{
          document.forms['pricingListForm'].elements['basisNew'+aid].value = Q5;
          }  }
          if(res[6]=='') {
          } else{
          document.forms['pricingListForm'].elements['basisNewType'+aid].value = res[6];
          }
          document.forms['pricingListForm'].elements['basis'+aid].value=res[13]; 
          if(res[19]=='Y'){
           	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
           }else{document.forms['pricingListForm'].elements['VATExclude'+aid].value=false; }
           setVatExcluded('1',aid) ;
          
          var chargedeviation=res[18];
          document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation; 
          if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
          //document.getElementById("receivableDeviationShow").style.display="block"; 
          //document.getElementById("payableDeviationShow").style.display="block";   
          var buyDependSellAccount=res[15]
          var actualRevenue=0;
          var finalActualRevenue=0; 
          var finalActualExpenceAmount=0;
          var contractRateAmmount=0;
          var finalContractRateAmmount=0;
          var payableContractRateAmmount=0;
          var sellDeviation=0;  
          sellDeviation=res[16] 
          var buyDeviation=0; 
          buyDeviation=res[17]
          var contractExchangeRate=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
          contractRateAmmount =document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value
          actualRevenue=contractRateAmmount/contractExchangeRate;
          actualRevenue=Math.round(actualRevenue*100)/100;  
          finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*100)/100;
          finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*100)/100;
          document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value=finalContractRateAmmount; 
          if(buyDependSellAccount=="Y"){
           finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*100)/100;
          }else{
           finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*100)/100;
          }
          var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
          payableContractRateAmmount=finalActualExpenceAmount*payableContractExchangeRate;
          payableContractRateAmmount=Math.round(payableContractRateAmmount*100)/100;
          
          document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value=sellDeviation;
          document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=sellDeviation;
          if (document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') {
          }
           }else {
          //document.getElementById("receivableDeviationShow").style.display="none";
          //document.getElementById("payableDeviationShow").style.display="none";
          document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value=0;
          document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=0; 
          var bayRate=0
          bayRate = res[20] ;
          if(bayRate!=0 && document.forms['pricingListForm'].elements['payAccDate'+aid].value==''){
          var recQuantityPay=0;
          recQuantityPay= document.forms['pricingListForm'].elements['recQuantity'+aid].value;
          Q3=recQuantityPay;
          Q4=bayRate;
          if(type == 'Division') {
          	 if(Q5==0) 	 {
          	   E2=0*1;
           }  else {	
          		E2=(Q3*Q4)/Q5;
          		E3=Math.round(E2*100)/100;
          	 }    }
          if(type == ' ' || type == '')   {
          		E2=(Q3*Q4);
          		E3=Math.round(E2*100)/100;
          }
          if(type == 'Multiply')   {
          		E2=(Q3*Q4*Q5);
          		E3=Math.round(E2*100)/100;
          }
          
          }
          }  
          type01 = res[3];
          typeConversion01=res[5];
       var contractCurrencyCal =document.forms['pricingListForm'].elements['contractCurrency'+aid].value; 
       if(contractCurrencyCal!=''){
       var contractExchangeRateCal=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
       var contractRateCal=document.forms['pricingListForm'].elements['contractRate'+aid].value
       var contractRateAmmountCal = document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value 
       var recRateCal=contractRateCal/contractExchangeRateCal;
       recRateCal=Math.round(recRateCal*10000)/10000;
       var actualRevCal=contractRateAmmountCal/contractExchangeRateCal;
       actualRevCal=Math.round(actualRevCal*100)/100;
       document.forms['pricingListForm'].elements['recRate'+aid].value=recRateCal ;
       document.forms['pricingListForm'].elements['actualRevenue'+aid].value=actualRevCal ;  
        }  
       var recRateCurrencyCal =document.forms['pricingListForm'].elements['recRateCurrency'+aid].value; 
       if(recRateCurrencyCal!=''){
      	 var recRateExchangeCal=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
           var recRateCal=document.forms['pricingListForm'].elements['recRate'+aid].value
           var actualRevenueCal= document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
           var recCurrencyRateCal=recRateCal*recRateExchangeCal;
           recCurrencyRateCal=Math.round(recCurrencyRateCal*100)/100;
           document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value=recCurrencyRateCal ;
           var actualRevenueForeignCal=actualRevenueCal*recRateExchangeCal; 
           actualRevenueForeignCal=Math.round(actualRevenueForeignCal*100)/100;
           document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value=actualRevenueForeignCal;
       }
       if (document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') {
       var payableContractCurrencyCal =document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value; 
        if(payableContractCurrencyCal!=''){
        var payableContractExchangeRateCal=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
        var payableContractRateAmmountCal=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value
        var actualExpenseCal=payableContractRateAmmountCal/payableContractExchangeRateCal;
        actualExpenseCal=Math.round(actualExpenseCal*100)/100;
        }
       var exchangeRate= document.forms['pricingListForm'].elements['exchangeRate'+aid].value*1 ;
       var actualExpense= document.forms['pricingListForm'].elements['actualExpense'+aid].value ;
       if(exchangeRate ==0) {
       } else  {
           var localAmount=0;
           localAmount=exchangeRate*actualExpense
           }
       }     
       }                          	                                 
       </c:if>
         <c:if test="${systemDefaultVatCalculation=='true'}">
         try{ 
             calculateVatAmt(aid);
             calculateVatAmtRevision(aid);
             calculateVatAmtEst(aid);        
           }catch(e){} 
         </c:if>       
      }
   }
   
   function fillDescription(aid) { 
	    var description =document.forms['pricingListForm'].elements['description'+aid].value 
	    var revenue =document.forms['pricingListForm'].elements['actualRevenue'+aid].value 
	    var category =document.forms['pricingListForm'].elements['category'+aid].value 
	    var quantity =document.forms['pricingListForm'].elements['recQuantity'+aid].value 
	    var itemNew =document.forms['pricingListForm'].elements['itemNew'+aid].value
	    var recRate =document.forms['pricingListForm'].elements['recRate'+aid].value
	    var basisNewType =document.forms['pricingListForm'].elements['basisNewType'+aid].value
	    var checkDescription =document.forms['pricingListForm'].elements['checkComputeDescription'].value;
	    if(document.forms['pricingListForm'].elements['description'+aid].value==''&& revenue!=0.00 && revenue!=0 && checkDescription!='computeButton')   {
	     document.forms['pricingListForm'].elements['description'+aid].value=quantity+" "+itemNew+" "+"@"+" "+recRate+" "+basisNewType;
	   	}
	    if(document.forms['pricingListForm'].elements['description'+aid].value==''&& revenue!=0.00 && revenue!=0 && checkDescription=='computeButton')   {
	    	checkDescriptionfromCharges(aid);
	    }
	       
	 }
   
	function calculateVatAmt(aid){
		<c:if test="${systemDefaultVatCalculation=='true'}"> 
		var recVatAmt=0;
		if(document.forms['pricingListForm'].elements['recVatPercent'+aid].value!=''){
		var recVatPercent=eval(document.forms['pricingListForm'].elements['recVatPercent'+aid].value);
		var actualRevenueForeign= eval(document.forms['pricingListForm'].elements['actualRevenue'+aid].value);
		<c:if test="${contractType}"> 
		actualRevenueForeign= eval(document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value);
		</c:if>
			var firstPersent="";
			var secondPersent=""; 
			var relo="" 
			     <c:forEach var="entry" items="${qstEuvatRecGLAmtMap}">
			        if(relo==""){ 
			        if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
				        var arr='${entry.value}';
				        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
				        	firstPersent=arr.split(':')[0];
				        	secondPersent=arr.split(':')[1];
				        }
			        relo="yes"; 
			       }  }
			    </c:forEach> 
			    if(firstPersent!='' && secondPersent!=''){
				    var p1=parseFloat(firstPersent);
				    var p2=parseFloat(secondPersent);
					recVatAmt=(actualRevenueForeign*p1)/100;
					recVatAmt=Math.round(recVatAmt*100)/100; 
					document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
					recVatAmt=(actualRevenueForeign*p2)/100;
					recVatAmt=Math.round(recVatAmt*100)/100;
					document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=recVatAmt;
					//recQstHideBlock();
			    }else{
					recVatAmt=(actualRevenueForeign*recVatPercent)/100;
					recVatAmt=Math.round(recVatAmt*100)/100;
					document.forms['pricingListForm'].elements['recVatAmt'+aid].value=recVatAmt;
					document.forms['pricingListForm'].elements['qstRecVatAmt'+aid].value=0;
					//recQstHideBlock();
			    }
		}
	    </c:if>
	}
// Compute Functionality for Receivable Section End
	
// Compute Functionality for Payable Section Start

function findRevisedQuantityLocalAmounts(aid){
     var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
     var vendorCode = document.forms['pricingListForm'].elements['vendorCode'+aid].value;
     var billingContract = '${billing.contract}';
     var shipNumber= '${serviceOrder.shipNumber}';
     var rev = document.forms['pricingListForm'].elements['category'+aid].value;
     var comptetive = document.forms['pricingListForm'].elements['inComptetive'].value;
     var actgCode = document.forms['pricingListForm'].elements['actgCode'+aid].value;
     if(charge!=''){
     if(vendorCode!=''){
     if(actgCode!=''){
     if(rev == "Internal Cost"){
     if(charge=="VANPAY" ){
      getVanPayCharges("Actual");
      } else if(charge=="DOMCOMM" ){
      	getDomCommCharges("Actual");
      }else if(charge=="SALESCM" && comptetive=="Y") { 
     var totalExpense = eval(document.forms['pricingListForm'].elements['inActualExpense'].value);
     var totalRevenue =eval(document.forms['pricingListForm'].elements['inActualRevenue'].value); 
     var commisionRate =eval(document.forms['pricingListForm'].elements['salesCommisionRate'].value); 
     var grossMargin = eval(document.forms['pricingListForm'].elements['grossMarginThreshold'].value);
     var commision=((totalRevenue*commisionRate)/100);
     var calGrossMargin=((totalRevenue-(totalExpense + commision))/totalRevenue);
     calGrossMargin=calGrossMargin*100;
     var calGrossMargin=Math.round(calGrossMargin);
     if(calGrossMargin>=grossMargin) {
     document.forms['pricingListForm'].elements['actualExpense'+aid].value=Math.round(commision*100)/100; 
     document.forms['pricingListForm'].elements['localAmount'+aid].value=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
     document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1; 
     }  else  { 
     commision=((.8*totalRevenue)-totalExpense);
     commision=Math.round(commision*100)/100;
     if(commision<0) {
     document.forms['pricingListForm'].elements['actualExpense'+aid].value=0;
     document.forms['pricingListForm'].elements['localAmount'+aid].value=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
     document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1;
     }  else {
     document.forms['pricingListForm'].elements['actualExpense'+aid].value=Math.round(commision*100)/100;
     document.forms['pricingListForm'].elements['localAmount'+aid].value=document.forms['pricingListForm'].elements['actualExpense'+aid].value; 
     document.forms['pricingListForm'].elements['exchangeRate'+aid].value=1;
     }  }  
     } else if(charge=="SALESCM" && comptetive!="Y") {
     var agree =confirm("This job is not competitive; do you still want to calculate commission?");
     if(agree)  { 
         var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'+aid].value  
         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse301;
	     http2.send(null); 
     } else {
     return false;
     }
     } else { 
         var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'+aid].value  
         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
         http2.open("GET", url, true);
	     http2.onreadystatechange = handleHttpResponse301;
	     http2.send(null);
	     } }else{
           var url="invoiceExtractsPayable.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&billingContract=" + encodeURI(billingContract)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id}  ;
           http2.open("GET", url, true);
           http2.onreadystatechange = function(){handleHttpResponsePayable(aid);}
           http2.send(null);
	     }
     	}else{
	  		alert("This Vendor Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept.");
	  	}
	    }else{
	  		alert("Please select Vendor Code.");
	  	}
		}else{
			alert("Please select Charge Code.");
		}
  }
  
function handleHttpResponsePayable(aid) {
	  if (http2.readyState == 4) {
              var results = http2.responseText
               results = results.trim(); 
              if(document.forms['pricingListForm'].elements['recAccDate'+aid].value==''){
              <c:if test="${!contractType}"> 
              if(results !='') {
              var res = results.split("#"); 
              if(res[9]=="AskUser2"){
              var reply = prompt("Please Enter Quantity", "");
              if(reply){
              if((reply>0 || reply<9)|| reply=='.')  {
            	} else {
            		alert("Please Enter legitimate Quantity");
            	} 
              }   else{
              }
              }    else{
                if(res[7] == undefined){
                }else{ 
             }  }
             if(res[10]=="AskUser"){
              var reply = prompt("Please Enter Rate", "");
              if(reply){
              if((reply>0 || reply<9)|| reply=='.'){
            	 } else {
            		alert("Please Enter legitimate Rate");
            	}
            	} else{
            	} 
              }  else{ 
               if(res[8] == undefined){
               }else{
              }  }
             if(res[10]=="BuildFormula"){
             if(res[11] == undefined){
             }else{ 
              }  }
             var Q3 = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
             var Q4 = document.forms['pricingListForm'].elements['recRate'+aid].value;
             if(res[10]=="BuildFormula" && res[11] != undefined){
          	 Q4 =res[11];
             }
             var E1=Q3*Q4; 
             var Q5 = res[5];
             if(res[5] == undefined){
             }else{
             }
             var type = res[3];
             var E2= "";
             var E3= "";
             if(type == 'Division') {
             	 if(Q5==0) 	 {
             	   E2=0*1;
              }  else {	
             		E2=(Q3*Q4)/Q5;
             		E3=Math.round(E2*100)/100;
             	 }    }
             if(type == ' ' || type == '')   {
             		E2=(Q3*Q4);
             		E3=Math.round(E2*100)/100;
             }
             if(type == 'Multiply')   {
             		E2=(Q3*Q4*Q5);
             		E3=Math.round(E2*100)/100;
             } 
             if(res[2]=='') {
             } else {
             if(res[2] == undefined){
             }else{
             }  }
             if(res[3]==' ') { 
             } else{ 
             }
             if(document.forms['pricingListForm'].elements['description'+aid].value.trim()=='') {
             if(res[4]=='')  {
             } else{ 
              var chk = res[4];
				var chkReplace=(chk.replace('"','').replace('"',''));
				if(res[12]==''){
				}else{
             	}  } }
             if(res[5]=='') {
             } else{
             if(res[5] == undefined)  {
             }  else{
             }  }
             if(res[6]=='') {
             } else{
             }
             document.forms['pricingListForm'].elements['basis'+aid].value=res[13]; 
             if(res[19]=='Y'){
              	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
              }else{document.forms['pricingListForm'].elements['VATExclude'+aid].value=false; }
             setVatExcluded('1',aid) ;
             var chargedeviation=res[18];
             document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation; 
             if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
             //document.getElementById("payableDeviationShow").style.display="block";   
             var buyDependSellAccount=res[15]
             var actualRevenue=0;
             var finalActualRevenue=0; 
             var finalActualExpenceAmount=0;
             var sellDeviation=0;  
             sellDeviation=res[16] 
             var buyDeviation=0; 
             buyDeviation=res[17] 
             actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;  
             finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*100)/100;
             if(buyDependSellAccount=="Y"){
              finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*100)/100;
             }else{
              finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*100)/100;
             }
             document.forms['pricingListForm'].elements['oldPayDeviation'+aid].value=buyDeviation;
             document.forms['pricingListForm'].elements['payDeviation'+aid].value=buyDeviation;
             document.forms['pricingListForm'].elements['actualExpense'+aid].value=finalActualExpenceAmount;
             convertLocalAmount('none',aid);
              }else {
             //document.getElementById("payableDeviationShow").style.display="none";
             document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=0; 
             var bayRate=0
             bayRate = res[20] ;
             if(bayRate!=0){
             var recQuantityPay=0;
             recQuantityPay= document.forms['pricingListForm'].elements['recQuantity'+aid].value;
             Q3=recQuantityPay;
             Q4=bayRate;
             if(type == 'Division') {
             	 if(Q5==0) 	 {
             	   E2=0*1;
             	   document.forms['pricingListForm'].elements['actualExpense'+aid].value = E2;
              }  else {	
             		E2=(Q3*Q4)/Q5;
             		E3=Math.round(E2*100)/100;
             		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
             	 }    }
             if(type == ' ' || type == '')   {
             		E2=(Q3*Q4);
             		E3=Math.round(E2*100)/100;
             		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
             }
             if(type == 'Multiply')   {
             		E2=(Q3*Q4*Q5);
             		E3=Math.round(E2*100)/100;
             		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3; 
             } 
             convertLocalAmount('none',aid); 
             } } 
             <c:if test="${multiCurrency=='Y'}"> 
             var recRateExchange=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
             var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value
             var actualRevenueCal= document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
             var recCurrencyRate=recRate*recRateExchange;
             var roundValue=Math.round(recCurrencyRate*100)/100;
             var actualRevenueForeignCal=actualRevenueCal*recRateExchange; 
             actualRevenueForeignCal=Math.round(actualRevenueForeignCal*100)/100;
             var typeConversion01 = res[5]; 
             var type01 = res[3];
             </c:if>
               }     
               </c:if>
               <c:if test="${contractType}">
               var type01='';
               var typeConversion01=0;
               if(results !='') {
                   var res = results.split("#"); 
                   <c:choose>
    		         <c:when test="${multiCurrency=='Y' && (billing.fXRateOnActualizationDate != null && (billing.fXRateOnActualizationDate =='true' || billing.fXRateOnActualizationDate == true || billing.fXRateOnActualizationDate))}">
    		          if(document.forms['pricingListForm'].elements['contractCurrency'+aid].value==res[21]){
    		    	   
    		       }else{
    		    	 document.forms['pricingListForm'].elements['contractCurrency'+aid].value=res[21];
    		       } 
    		       </c:when>
    		       <c:otherwise> 
                   findExchangeContractRateByCharges('receivable',aid);
                   </c:otherwise>
      		     </c:choose>
                   if(res[9]=="AskUser2"){
                   var reply = prompt("Please Enter Quantity", "");
                   if(reply){
                   if((reply>0 || reply<9)|| reply=='.')  {
                 	} else {
                 		alert("Please Enter legitimate Quantity");
                 	} 
                   }   else{
                   }
                   }    else{
                     if(res[7] == undefined){
                     }else{ 
                  }  }
                  if(res[10]=="AskUser"){
                   }  else{ 
                    if(res[8] == undefined){
                    }else{
                   }  }
                  if(res[10]=="BuildFormula"){
                  if(res[11] == undefined){
                  }else{ 
                   }  }
                  var Q3 = document.forms['pricingListForm'].elements['recQuantity'+aid].value;
                  var Q4 = document.forms['pricingListForm'].elements['contractRate'+aid].value;
                  if(res[10]=="BuildFormula" && res[11] != undefined){
                  	Q4 = res[11];
                  }
                  var E1=Q3*Q4; 
                  var Q5 = res[5];
                  if(res[5] == undefined){
                  }else{
                  }
                  var type = res[3];
                  var E2= "";
                  var E3= "";
                  if(type == 'Division') {
                  	 if(Q5==0) 	 {
                  	   E2=0*1;
                   }  else {	
                  		E2=(Q3*Q4)/Q5;
                  		E3=Math.round(E2*100)/100;
                  	 }    }
                  if(type == ' ' || type == '')   {
                  		E2=(Q3*Q4);
                  		E3=Math.round(E2*100)/100;
                  }
                  if(type == 'Multiply')   {
                  		E2=(Q3*Q4*Q5);
                  		E3=Math.round(E2*100)/100;
                  } 
                  if(res[2]=='') {
                  } else {
                  if(res[2] == undefined){
                  }else{
                  }  }
                  if(res[3]==' ') { 
                  } else{ 
                  } 
                  if(document.forms['pricingListForm'].elements['description'+aid].value.trim()=='') {
                  if(res[4]=='')  {
                  } else{ 
                   var chk = res[4];
   				var chkReplace=(chk.replace('"','').replace('"',''));
   				if(res[12]==''){
   				}else{
                  	}  } }
                  if(res[5]=='') {
                  } else{
                  if(res[5] == undefined)  {
                  }  else{
                  }  }
                  if(res[6]=='') {
                  } else{
                  }
                  if(res[19]=='Y'){
                   	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
                   }else{document.forms['pricingListForm'].elements['VATExclude'+aid].value=false; }
                   setVatExcluded('1',aid) ; 
                  var chargedeviation=res[18];
                  document.forms['pricingListForm'].elements['deviation'+aid].value=chargedeviation; 
                  if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){
                  //document.getElementById("receivableDeviationShow").style.display="block"; 
                  //document.getElementById("payableDeviationShow").style.display="block";   
                  var buyDependSellAccount=res[15]
                  var actualRevenue=0;
                  var finalActualRevenue=0; 
                  var finalActualExpenceAmount=0;
                  var contractRateAmmount=0;
                  var finalContractRateAmmount=0;
                  var payableContractRateAmmount=0;
                  var sellDeviation=0;  
                  sellDeviation=res[16] 
                  var buyDeviation=0; 
                  buyDeviation=res[17]
                  var contractExchangeRate=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
                  contractRateAmmount =document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value
                  actualRevenue=contractRateAmmount/contractExchangeRate;
                  actualRevenue=Math.round(actualRevenue*100)/100;  
                  finalActualRevenue=Math.round((actualRevenue*(sellDeviation/100))*100)/100;
                  finalContractRateAmmount=Math.round((contractRateAmmount*(sellDeviation/100))*100)/100;
                  if(buyDependSellAccount=="Y"){
                   finalActualExpenceAmount= Math.round((finalActualRevenue*(buyDeviation/100))*100)/100;
                  }else{
                   finalActualExpenceAmount= Math.round((actualRevenue*(buyDeviation/100))*100)/100;
                  }
                  var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
                  payableContractRateAmmount=finalActualExpenceAmount*payableContractExchangeRate;
                  payableContractRateAmmount=Math.round(payableContractRateAmmount*100)/100;
                  document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value=payableContractRateAmmount;
                  document.forms['pricingListForm'].elements['oldPayDeviation'+aid].value=buyDeviation;
                  document.forms['pricingListForm'].elements['payDeviation'+aid].value=buyDeviation;
                  document.forms['pricingListForm'].elements['actualExpense'+aid].value=finalActualExpenceAmount;
                   }else {
                  //document.getElementById("payableDeviationShow").style.display="none";
                  document.forms['pricingListForm'].elements['receivableSellDeviation'+aid].value=0;
                  document.forms['pricingListForm'].elements['oldReceivableDeviation'+aid].value=0; 
                  var bayRate=0
                  bayRate = res[20] ;
                  if(bayRate!=0){
                  	document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value=res[22]; 
                  	findPayableContractCurrencyExchangeRateByCharges('payable');   
                  var recQuantityPay=0;
                  recQuantityPay= document.forms['pricingListForm'].elements['recQuantity'+aid].value;
                  Q3=recQuantityPay;
                  Q4=bayRate;
                  if(type == 'Division') {
                  	 if(Q5==0) 	 {
                  	   E2=0*1;
                  	   document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E2;
                   }  else {	
                  		E2=(Q3*Q4)/Q5;
                  		E3=Math.round(E2*100)/100;
                  		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3;
                  	 }    }
                  if(type == ' ' || type == '')   {
                  		E2=(Q3*Q4);
                  		E3=Math.round(E2*100)/100;
                  		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3;
                  }
                  if(type == 'Multiply')   {
                  		E2=(Q3*Q4*Q5);
                  		E3=Math.round(E2*100)/100;
                  		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3; 
                  } } }
                  typeConversion01 = res[5];             
                  type01 = res[3]; 
                  } 
               var contractCurrencyCal =document.forms['pricingListForm'].elements['contractCurrency'+aid].value; 
               if(contractCurrencyCal!=''){
               var contractExchangeRateCal=document.forms['pricingListForm'].elements['contractExchangeRate'+aid].value
               var contractRateCal=document.forms['pricingListForm'].elements['contractRate'+aid].value
               var contractRateAmmountCal = document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value 
               var recRateCal=contractRateCal/contractExchangeRateCal;
               recRateCal=Math.round(recRateCal*10000)/10000;
               var actualRevCal=contractRateAmmountCal/contractExchangeRateCal;
               actualRevCal=Math.round(actualRevCal*100)/100;
                }  
               var recRateCurrencyCal =document.forms['pricingListForm'].elements['recRateCurrency'+aid].value; 
               if(recRateCurrencyCal!=''){
              	 var recRateExchangeCal=document.forms['pricingListForm'].elements['recRateExchange'+aid].value
                   var recRateCal=document.forms['pricingListForm'].elements['recRate'+aid].value
                   var actualRevenueCal= document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
                   var recCurrencyRateCal=recRateCal*recRateExchangeCal;
                   recCurrencyRateCal=Math.round(recCurrencyRateCal*100)/100;
                   var actualRevenueForeignCal=actualRevenueCal*recRateExchangeCal; 
                   actualRevenueForeignCal=Math.round(actualRevenueForeignCal*100)/100;
               }
               var payableContractCurrencyCal =document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value; 
   	         if(payableContractCurrencyCal!=''){
   	         var payableContractExchangeRateCal=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
   	         var payableContractRateAmmountCal=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value
   	         var actualExpenseCal=payableContractRateAmmountCal/payableContractExchangeRateCal;
   	         actualExpenseCal=Math.round(actualExpenseCal*100)/100;
   	         document.forms['pricingListForm'].elements['actualExpense'+aid].value=actualExpenseCal ; 
   	         }
   	        var exchangeRate= document.forms['pricingListForm'].elements['exchangeRate'+aid].value*1 ;
   	        var actualExpense= document.forms['pricingListForm'].elements['actualExpense'+aid].value ;
   	        if(exchangeRate ==0) {
   	                document.forms['pricingListForm'].elements['localAmount'+aid].value=0*1;
   	        } else  {
   	            var localAmount=0;
   	            localAmount=exchangeRate*actualExpense
   	            document.forms['pricingListForm'].elements['localAmount'+aid].value=Math.round(localAmount*100)/100; 
   	            }
  	                 </c:if>
  	  	           <c:if test="${systemDefaultVatCalculation=='true'}">
  		           try{
  		       			calculatePayVatAmt(aid);
  		       		    calculateESTVatAmt(aid);
  		       		    calculateREVVatAmt(aid);	      
  		       				
  		           }catch(e){}
  		           </c:if>   
                }else{
              	  <c:if test="${!contractType}"> 
                    if(results !='') {
                    var res = results.split("#");
                    var chargedeviation=res[18];
                    if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){ 
                         }else {
                        //document.getElementById("payableDeviationShow").style.display="none";
                        var bayRate=0
                        bayRate = res[20] ;
                        if(bayRate!=0){
                        var recQuantityPay=0;
                        if(res[9]=="AskUser2"){
                            var reply = prompt("Please Enter Quantity", "");
                            if(reply){
                            if((reply>0 || reply<9)|| reply=='.')  {
                          	  recQuantityPay =Math.round(reply*100)/100; 
                          	} else {
                          		alert("Please Enter legitimate Quantity");
                          	} 
                            }   else{
                          	  recQuantityPay = document.forms['pricingListForm'].elements['tempRecQuantity'+aid].value;
                            }
                            }    else{
                              if(res[7] == undefined){
                              	
                              }else{ 
                              	recQuantityPay =Math.round(res[7]*100)/100;
                           }  }
                        var Q3="";
                        var Q4="";
                        var Q5 = res[5];                           
                        Q3=recQuantityPay;
                        Q4=bayRate;
                        var type = res[3];
                        var E2= "";
                        var E3= "";
                        if(type == 'Division') {
                        	 if(Q5==0) 	 {
                        	   E2=0*1;
                        	   document.forms['pricingListForm'].elements['actualExpense'+aid].value = E2;
                         }  else {	
                        		E2=(Q3*Q4)/Q5;
                        		E3=Math.round(E2*100)/100;
                        		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
                        	 }    }
                        if(type == ' ' || type == '')   {
                        		E2=(Q3*Q4);
                        		E3=Math.round(E2*100)/100;
                        		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
                        }
                        if(type == 'Multiply')   {
                        		E2=(Q3*Q4*Q5);
                        		E3=Math.round(E2*100)/100;
                        		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3; 
                        } 
                        convertLocalAmount('none',aid);
                        }  }   }
                    </c:if>
                    <c:if test="${contractType}"> 
                    if(results !='') {
                        var res = results.split("#"); 
                        var chargedeviation=res[18]; 
                        if(chargedeviation!='' && chargedeviation!='NCT' && chargedeviation!=undefined){    
                        }else {
                            //document.getElementById("payableDeviationShow").style.display="none";
                            var bayRate=0
                            bayRate = res[20] ;
                            if(bayRate!=0){
                            	document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value=res[22]; 
                            	findPayableContractCurrencyExchangeRateByCharges('payable');   
                            var recQuantityPay=0;
                            if(res[9]=="AskUser2"){
                                var reply = prompt("Please Enter Quantity", "");
                                if(reply){
                                if((reply>0 || reply<9)|| reply=='.')  {
                              	  recQuantityPay =Math.round(reply*100)/100; 
                              	} else {
                              		alert("Please Enter legitimate Quantity");
                              	} 
                                }   else{
                              	  recQuantityPay = document.forms['pricingListForm'].elements['tempRecQuantity'+aid].value;;
                                }
                                }    else{
                                  if(res[7] == undefined){
                                  	
                                  }else{ 
                                  	recQuantityPay =Math.round(res[7]*100)/100;
                               }  }
                            var Q3="";
                            var Q4="";
                            var Q5 = res[5]; 
                            Q3=recQuantityPay;
                            Q4=bayRate;
                            var type = res[3];
                            var E2= "";
                            var E3= ""; 
                            if(type == 'Division') {
                            	 if(Q5==0) 	 {
                            	   E2=0*1;
                            	   document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E2;
                             }  else {	
                            		E2=(Q3*Q4)/Q5;
                            		E3=Math.round(E2*100)/100;
                            		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3;
                            	 }    }
                            if(type == ' ' || type == '')   {
                            		E2=(Q3*Q4);
                            		E3=Math.round(E2*100)/100;
                            		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3;
                            }
                            if(type == 'Multiply')   {
                            		E2=(Q3*Q4*Q5);
                            		E3=Math.round(E2*100)/100;
                            		document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value = E3; 
                            }  }  }
                        calculateActualExpenseByContractRate('none');
                    }
                    </c:if> 
               }  } }
   
function setVatExcluded(temp,aid){
	var createdBy = document.forms['pricingListForm'].elements['createdBy'+aid].value;
	var networkSynchedId = document.forms['pricingListForm'].elements['networkSynchedId'+aid].value;
	var chargeCode = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	var accNetworkGroup = '${trackingStatus.accNetworkGroup}';
	var soNetworkGroup = '${trackingStatus.soNetworkGroup}';
	<c:if test="${(((createdBy != 'Networking'  && ( !(((fn1:indexOf(createdBy,'Stg Bill')>=0 ) &&  not empty networkSynchedId) && !(accNetworkGroup)) )) && (chargeCode != 'MGMTFEE'))|| !(soNetworkGroup))}">
	<c:if test="${fn1:indexOf(createdBy,'DMM')<0}">
	<c:if test="${systemDefaultVatCalculation=='true'}"> 
	var vatExcludeFlag = document.forms['pricingListForm'].elements['VATExclude'+aid].value; 
	if(chargeCode!=''){ 
		if(vatExcludeFlag !='false') { 
    		var vatRec=0.00;
    		vatRec=document.forms['pricingListForm'].elements['recVatAmt'+aid].value;
    		vatRec=parseFloat(vatRec);
    		var vatPay=0.00;
    		vatPay=document.forms['pricingListForm'].elements['payVatAmt'+aid].value;
    		vatPay=parseFloat(vatPay);
    		if((vatRec>0)||(vatPay>0)) {
        		if(temp=='2'){
        			document.forms['pricingListForm'].elements['checkChargeCodePopup'].value="found";
        		}
    		if(temp=='1'){
			var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
			 if(agree) {
            	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
				document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
				document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;
	    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
		        document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
		        document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		        document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
		        document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
		        document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
			   }else { 
				   document.forms['pricingListForm'].elements['chargeCode'+aid].value=document.forms['pricingListForm'].elements['accChargeCodeTemp'+aid].value;	
				   document.forms['pricingListForm'].elements['VATExclude'+aid].value=document.forms['pricingListForm'].elements['oldVATExclude'+aid].value;	
			   }
    		} }	else{ 
				document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
				document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;	 
	    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
    		}
			} else { 
				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=false;  
				var rev = document.forms['pricingListForm'].elements['category'+aid].value; 
               if(rev != "Internal Cost"){
				document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=false;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=false;
				document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=false; 
				}
				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=false;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=false; 
				<c:if test="${((!(trackingStatus.soNetworkGroup))  && billingCMMContractType && networkAgent )}">
				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true;
				</c:if>
			} 
		}
		fillRecVatByChargeCode(aid); 
		fillPayVatByChargeCode(aid); 
	 </c:if>
	 </c:if> 
	 </c:if>
}

function fillRecVatByChargeCode(aid){
	if(document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value=='') {
	if(document.forms['pricingListForm'].elements['VATExclude'+aid].value!='true'){
	if(document.forms['pricingListForm'].elements['accRecVatDescr'+aid].value !=''){	
	 document.forms['pricingListForm'].elements['recVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldRecVatDescr'+aid].value;
	}else{
		fillRecVatByBillToCode(aid);
	}
	}else{
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0; 
	} 
	} 
}

function fillPayVatByChargeCode(aid){
	if(document.forms['pricingListForm'].elements['payPostDate'+aid].value=='' && document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') {
	if(document.forms['pricingListForm'].elements['VATExclude'+aid].value!='true'){
	if(document.forms['pricingListForm'].elements['accPayVatDescr'+aid].value !=''){	
	 document.forms['pricingListForm'].elements['payVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldPayVatDescr'+aid].value;
	 }else{
		fillPayVatByVendorCode(aid);
	}
	}else{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0; 
		document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0; 
		} 
	} 
}

function fillRecVatByBillToCode(aid) {	
	var oldVat = document.forms['pricingListForm'].elements['recVatDescr'+aid].value;
	if(document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value=='') {
	if(document.forms['pricingListForm'].elements['VATExclude'+aid].value!='true'){
	if(document.forms['pricingListForm'].elements['billToCode'+aid].value =='${billing.billToCode}'){
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value='${billing.primaryVatCode}';
	}
	if(document.forms['pricingListForm'].elements['billToCode'+aid].value =='${billing.billTo2Code}'){
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value='${billing.secondaryVatCode}';
		}
	if(document.forms['pricingListForm'].elements['billToCode'+aid].value =='${billing.privatePartyBillingCode}'){
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value='${billing.privatePartyVatCode}';
		}
	/* <c:if test="${empty accountLine.id}">
	var sysreceivable='${systemDefault.receivableVat}';
    if(document.forms['pricingListForm'].elements['recVatDescr'].value =='' && sysreceivable!=''){
		try{
		document.forms['pricingListForm'].elements['recVatDescr'].value=sysreceivable;
		}catch(e){}
		}
	</c:if> */
    if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value ==''){
    	document.forms['pricingListForm'].elements['recVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldRecVatDescr'+aid].value;
    }
    if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value ==''){
    	document.forms['pricingListForm'].elements['recVatDescr'+aid].value = oldVat;
    }
	getVatPercent(aid);
	}else{
		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
		document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
		document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0; 
		} 
	} 
}

function fillPayVatByVendorCode(aid) {
	var oldVat=document.forms['pricingListForm'].elements['payVatDescr'+aid].value;
	if(document.forms['pricingListForm'].elements['payPostDate'+aid].value=='' && document.forms['pricingListForm'].elements['payAccDate'+aid].value=='') {
	if(document.forms['pricingListForm'].elements['VATExclude'+aid].value!='true'){
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.originAgentCode}'){
	document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.originAgentVatCode}';
	}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationAgentCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.destinationAgentVatCode}';
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.originSubAgentCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.originSubAgentVatCode}';
		} 
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.destinationSubAgentCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.destinationSubAgentVatCode}';
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.forwarderCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.forwarderVatCode}';
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.brokerCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.brokerVatCode}';
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${trackingStatus.networkPartnerCode}'){
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.networkPartnerVatCode}';
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${serviceOrder.bookingAgentCode}'){
		try{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.bookingAgentVatCode}';
		}catch(e){}
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${billing.vendorCode}'){
		try{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode}';
		}catch(e){}
		}
	if(document.forms['pricingListForm'].elements['vendorCode'+aid].value =='${billing.vendorCode1}'){
		try{
		document.forms['pricingListForm'].elements['payVatDescr'+aid].value='${billing.vendorCodeVatCode1}';
		}catch(e){}
		}
	/* <c:if test="${empty accountLine.id}">	
        var syspayable='${systemDefault.payableVat}';
        if(document.forms['pricingListForm'].elements['payVatDescr'].value =='' && syspayable!=''){
    		try{
    		document.forms['pricingListForm'].elements['payVatDescr'].value=syspayable;
    		}catch(e){}
    		}
		</c:if> */
        if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value ==''){
        	document.forms['pricingListForm'].elements['payVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldPayVatDescr'+aid].value;
        }	
        if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value ==''){
        	document.forms['pricingListForm'].elements['payVatDescr'+aid].value = oldVat
            }
        
	getPayPercent(aid);
	}  } 
}

function getVatPercent(aid){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var recAccDate='';
	 <c:if test="${accountInterface=='Y'}">
	  recAccDate=document.forms['pricingListForm'].elements['recAccDate'+aid].value;
     </c:if>
     utsiRecAccDateFlag=document.forms['pricingListForm'].elements['utsiRecAccDateFlag'+aid].value;
     utsiPayAccDateFlag=document.forms['pricingListForm'].elements['utsiPayAccDateFlag'+aid].value;
     if (!utsiRecAccDateFlag && !utsiPayAccDateFlag){
       if (!'${accNonEditFlag}' && recAccDate!='')  { 
        alert("You can not change the VAT as Sent To Acc has been already filled");
        document.forms['pricingListForm'].elements['recVatDescr'+aid].value= document.forms['pricingListForm'].elements['oldRecVatDescr'+aid].value;
        } else if ('${accNonEditFlag}' && document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value!=''){
        	alert("You can not change the VAT as as invoice has been generated. ");
            document.forms['pricingListForm'].elements['recVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldRecVatDescr'+aid].value;	
        }else{
	 var relo="" 
     <c:forEach var="entry" items="${euVatPercentList}">
        if(relo==""){ 
        if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry.key}') {
        document.forms['pricingListForm'].elements['recVatPercent'+aid].value='${entry.value}'; 
        calculateVatAmt(aid);
        calculateVatAmtRevision(aid);
        calculateVatAmtEst(aid);        
        relo="yes"; 
       }  }
    </c:forEach> 
    <configByCorp:fieldVisibility componentId="accountLine.recVatGL"> 
    var relo1=""  
        <c:forEach var="entry1" items="${euvatRecGLMap}">
           if(relo1==""){ 
           if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}') {
           document.forms['pricingListForm'].elements['recVatGl'+aid].value='${entry1.value}';  
           relo1="yes"; 
          }  }
       </c:forEach>
       relo1='';
	 	<c:forEach var="entry1" items="${qstEuvatRecGLMap}" > 
	 	if(relo1 ==''){
	 	  if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value=='${entry1.key}'){
	 	      document.forms['pricingListForm'].elements['qstRecVatGl'+aid].value='${entry1.value}'; 
	 	      relo1 ="yes";
	 	      }   }
	 	     </c:forEach>        
      </configByCorp:fieldVisibility> 
      if(document.forms['pricingListForm'].elements['recVatDescr'+aid].value==''){
      document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
      calculateVatAmt(aid);
      calculateVatAmtRevision(aid);  
      calculateVatAmtEst(aid);
          
      }  }
	}
     if (utsiRecAccDateFlag || utsiPayAccDateFlag){ 
     alert("You can not change the VAT as sent to Accounting and/or already Invoiced in UTSI Instance");
     document.forms['pricingListForm'].elements['recVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldRecVatDescr'+aid].value; 
     }
      </c:if> 
	}
	
function getPayPercent(aid){ 
	<c:if test="${systemDefaultVatCalculation=='true'}">
	var payAccDate='';
	 <c:if test="${accountInterface=='Y'}">
	  payAccDate=document.forms['pricingListForm'].elements['payAccDate'+aid].value;
     </c:if>
       if (payAccDate!='') { 
        alert("You can not change the VAT as Sent To Acc has been already filled");
        document.forms['pricingListForm'].elements['payVatDescr'+aid].value=document.forms['pricingListForm'].elements['oldPayVatDescr'+aid].value;
        } else{
	var relo='';
	<c:forEach var="entry" items="${payVatPercentList}" > 
	if(relo==''){
	  if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}'){
	      document.forms['pricingListForm'].elements['payVatPercent'+aid].value='${entry.value}';
	      calculatePayVatAmt(aid);
	      calculateESTVatAmt(aid);
	      calculateREVVatAmt(aid);	      
	      relo="yes";
	      }   }
	     </c:forEach>
	     <configByCorp:fieldVisibility componentId="accountLine.payVatGl">
	     var relo1='';
		 	<c:forEach var="entry1" items="${payVatPayGLMap}" > 
		 	if(relo1 ==''){
		 	  if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
		 	      document.forms['pricingListForm'].elements['payVatGl'+aid].value='${entry1.value}'; 
		 	      relo1 ="yes";
		 	      }   }
		 	     </c:forEach>
		 	    relo1='';
			 	<c:forEach var="entry1" items="${qstPayVatPayGLMap}" > 
			 	if(relo1 ==''){
			 	  if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry1.key}'){
			 	      document.forms['pricingListForm'].elements['qstPayVatGl'+aid].value='${entry1.value}'; 
			 	      relo1 ="yes";
			 	      }   }
			 	     </c:forEach>		 	     
	 </configByCorp:fieldVisibility> 
	     if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value==''){
            document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
            calculatePayVatAmt(aid);
  	        calculateESTVatAmt(aid);
	        calculateREVVatAmt(aid);	      
            
      }  }
      </c:if>
	}
function calculatePayVatAmt(aid){
	   <c:if test="${systemDefaultVatCalculation=='true'}">
	    var payVatAmt=0;
		if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
		var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
		var actualExpense= eval(document.forms['pricingListForm'].elements['actualExpense'+aid].value);
		<c:if test="${contractType}"> 
		actualExpense= eval(document.forms['pricingListForm'].elements['localAmount'+aid].value);
		</c:if>
		var firstPersent="";
		var secondPersent=""; 
		var relo="" 
		     <c:forEach var="entry" items="${qstPayVatPayGLAmtMap}">
		        if(relo==""){ 
		        if(document.forms['pricingListForm'].elements['payVatDescr'+aid].value=='${entry.key}') {
			        var arr='${entry.value}';
			        if(arr!=null && arr !=undefined && arr!='' && arr.indexOf(":")> -1){
			        	firstPersent=arr.split(':')[0];
			        	secondPersent=arr.split(':')[1];
			        }
		        relo="yes"; 
		       }  }
		    </c:forEach> 
		    if(firstPersent!='' && secondPersent!=''){
			    var p1=parseFloat(firstPersent);
			    var p2=parseFloat(secondPersent);
			    payVatAmt=(actualExpense*p1)/100;
			    payVatAmt=Math.round(payVatAmt*100)/100; 
				document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
				payVatAmt=(actualExpense*p2)/100;
				payVatAmt=Math.round(payVatAmt*100)/100; 
				document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=payVatAmt;
				//payQstHideBlock();
		    }else{
				payVatAmt=(actualExpense*payVatPercent)/100;
				payVatAmt=Math.round(payVatAmt*100)/100; 
				document.forms['pricingListForm'].elements['payVatAmt'+aid].value=payVatAmt;
				document.forms['pricingListForm'].elements['qstPayVatAmt'+aid].value=0;
				//payQstHideBlock();
		    } }
		</c:if>
	}

	function calculateESTVatAmt(aid){
	   <c:if test="${systemDefaultVatCalculation=='true'}">
	    var estExpVatAmt=0;
		if(document.forms['pricingListForm'].elements['payVatPercent'+aid].value!=''){
		var payVatPercent=eval(document.forms['pricingListForm'].elements['payVatPercent'+aid].value);
		var estimateExpense= eval(document.forms['pricingListForm'].elements['estimateExpense'+aid].value);
		<c:if test="${contractType}"> 
		estimateExpense= eval(document.forms['pricingListForm'].elements['estLocalAmount'+aid].value);
		</c:if>
		estExpVatAmt=(estimateExpense*payVatPercent)/100;
		estExpVatAmt=Math.round(estExpVatAmt*100)/100; 
		document.forms['pricingListForm'].elements['estExpVatAmt'+aid].value=estExpVatAmt;
		}
		</c:if>
	}
	
	function convertLocalAmount(target,aid){
		if(target=='payableContractRateAmmount'+aid){
	    var payableContractExchangeRateTemp=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
	    var payableContractRateAmmountTemp=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value
	    var actualExpenseTemp=payableContractRateAmmountTemp/payableContractExchangeRateTemp; 
	    var exchangeRate= document.forms['pricingListForm'].elements['exchangeRate'+aid].value*1 ;
	    var actualExpense= actualExpenseTemp ;
	    if(exchangeRate ==0) {
	             document.forms['pricingListForm'].elements['localAmount'+aid].value=0*1;
	         } else  {
	         var localAmount=0;
	         localAmount=exchangeRate*actualExpense
	         document.forms['pricingListForm'].elements['localAmount'+aid].value=Math.round(localAmount*100)/100;
	         calculatePayVatAmt(aid);
		      calculateESTVatAmt(aid);
		      calculateREVVatAmt(aid);	 
	         if(target!='payableContractRateAmmount'+aid){
	         calPayableContractExchangeRate('none',aid);
	         }  } 
		}else{
	var exchangeRate= document.forms['pricingListForm'].elements['exchangeRate'+aid].value*1 ;
	var actualExpense= document.forms['pricingListForm'].elements['actualExpense'+aid].value ;
	if(exchangeRate ==0) {
	         document.forms['pricingListForm'].elements['localAmount'+aid].value=0*1;
	     } else  {
	     var localAmount=0;
	     localAmount=exchangeRate*actualExpense
	     document.forms['pricingListForm'].elements['localAmount'+aid].value=Math.round(localAmount*100)/100;
	     calculatePayVatAmt(aid);
	     calculateESTVatAmt(aid);
	     calculateREVVatAmt(aid);	      

	     if(target!='payableContractRateAmmount'+aid){
	     calPayableContractExchangeRate('none',aid);
	     }  } }  
	}
	
	function calPayableContractExchangeRate(target,aid){
		<c:if test="${contractType}"> 
	    var country =document.forms['pricingListForm'].elements['payableContractCurrency'+aid].value; 
	    if(country=='') {
	    document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value=0; 
	  } else if(country!=''){
		  <c:if test="${contractType}"> 
				   var x= document.forms['pricingListForm'].elements['localAmount'+aid].value ;
				   var a= document.forms['pricingListForm'].elements['exchangeRate'+aid].value*1 ;
				   var amount=0.00;
				   amount=x/a;
				   var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value;
				   var actualExpense=amount;
				   var payableContractRateAmmount=actualExpense*payableContractExchangeRate;
				   var roundValue=Math.round(payableContractRateAmmount*100)/100;
				   document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value=roundValue ;
					  
			</c:if>
			 <c:if test="${!contractType}"> 
				  var payableContractExchangeRate=document.forms['pricingListForm'].elements['payableContractExchangeRate'+aid].value
				  var actualExpense=document.forms['pricingListForm'].elements['actualExpense'+aid].value
				  var payableContractRateAmmount=actualExpense*payableContractExchangeRate;
				  var roundValue=Math.round(payableContractRateAmmount*100)/100;
				  document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value=roundValue ;
			</c:if>
	  }
	</c:if>
	}
	
	function checkBilltoCode(aid){
		accountLineIdScript=aid;
        var invoiceNumber = document.forms['pricingListForm'].elements['recInvoiceNumber'+aid].value;
        var billToCode = document.forms['pricingListForm'].elements['billToCode'+aid].value;
        var billToName = document.forms['pricingListForm'].elements['billToName'+aid].value;
        invoiceNumber=invoiceNumber.trim();
        if(invoiceNumber!='' && (document.forms['pricingListForm'].elements['userRoleExecutive'].value=="no")){
        alert('You can not change the Billing Party as Invoice# has been already generated')
        } else if (invoiceNumber==''||(document.forms['pricingListForm'].elements['userRoleExecutive'].value=="yes")){
        if(invoiceNumber!=''){
          var agree =confirm("Invoice for this line has already been generated and it will change all corresponding value for the invoice no "+invoiceNumber+".\n\n Are you sure you want to continue with the changes?");
          if(agree) {
            //document.forms['pricingListForm'].elements['oldRecRateCurrency'].value='${accountLine.recRateCurrency}';
            openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+billToName+'&fld_code='+billToCode+'');
          }else { 
        }  
      }else if (invoiceNumber==''){
        openWindow('accountLineBillToCode.html?id=${serviceOrder.id}&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description='+billToName+'&fld_code='+billToCode+'');
       } }  
   }
	
	
	function getReceivableFieldDetails(aid,clickType){
		var fieldValues = "";
		var varianceExpenseAmount=0;
		var revisionExpense=0;
		var revisionRevenueAmount=0;
		var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
		var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
		var billToCode=document.forms['pricingListForm'].elements['billToCode'+aid].value;
		var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
		var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value;
		var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
		
		var acref="${accountInterface}";
		acref=acref.trim();
		if(vendorCode.trim()==''){
			alert("Vendor Code Missing. Please select");
		}else if(chargeCode.trim()==''){
			alert("Charge code needs to be selected");
		}else if(payGl.trim()==''){
			alert("Pay GL missing. Please select another charge code.");
		}else if(recGl.trim()==''){
			alert("Receivable GL missing. Please select another charge code.");
		}else if(actgCode.trim()=='' && acref =='Y'){
			alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
		}else if(billToCode.trim()==''){
			alert("Bill To Code is Missing. Please select");
		}else if(chargeCode.trim()==''){
			alert("Charge code needs to be selected");
		}else if(recGl.trim()==''){
			alert("Receivable GL is missing. Please select another charge code.");
		}else{
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">	
			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
				varianceExpenseAmount = document.forms['pricingListForm'].elements['varianceExpenseAmount'+aid].value;
			</configByCorp:fieldVisibility>
				revisionExpense=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
				revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
			</c:if>
			var actualExpense=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
			var localAmount=document.forms['pricingListForm'].elements['localAmount'+aid].value;
			var estimateVendorName=document.forms['pricingListForm'].elements['estimateVendorName'+aid].value;
			var category = document.forms['pricingListForm'].elements['category'+aid].value;
			var actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
			var estimateRevenueAmount=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
		    var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value;
			var recQuantity=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
			var payingStatus = document.forms['pricingListForm'].elements['payingStatus'+aid].value;
			fieldValues = vendorCode+"~"+estimateVendorName+"~"+chargeCode+"~"+actgCode+"~"+varianceExpenseAmount+"~"+actualExpense+"~"+revisionExpense+"~"+localAmount+"~"+category;
			fieldValues = fieldValues+"~"+actualRevenue+"~"+estimateRevenueAmount+"~"+revisionRevenueAmount+"~"+recRate+"~"+recQuantity+"~"+payingStatus+"~"+payGl+"~"+recGl;
			<c:if test="${contractType}">
				var payableContractRateAmmount=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
				var contractRate=document.forms['pricingListForm'].elements['contractRate'+aid].value;
				var contractRateAmmount=document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value;
				fieldValues = fieldValues+"~"+payableContractRateAmmount+"~"+contractRate+"~"+contractRateAmmount;
			</c:if>
		    <c:if test="${multiCurrency=='Y'}">
				var recCurrencyRate=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
				var actualRevenueForeign=document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
				fieldValues = fieldValues+"~"+recCurrencyRate+"~"+actualRevenueForeign;
			</c:if>
			var sid = '${serviceOrder.id}';
			var clickType = clickType ;
			var $j = jQuery.noConflict();
			$j("#loading-indicator"+aid).show();
			$j("#payableDetails"+aid).modal({show:true,backdrop: 'static'});
			 $j.ajax({
	            url: 'getReceivableSectionDetails.html?id='+aid+'&sid='+sid+'&fieldValues='+fieldValues+'&clickType='+clickType+'&decorator=modal&popup=true',
	            success: function(data){
					$j("#payableDetails"+aid).html(data);
					$j("#loading-indicator"+aid).hide();
	            },
	            error: function () {
	                alert('Some error happens');
	                $j("#payableDetails"+aid).modal("hide");
	             }   
	        });
	        
		}
	}
    
    function payableModalClose(aid){
    	var $j = jQuery.noConflict();
    	$j("#payableDetails"+aid).modal("hide");
    }
	
	function getPayableSectionDetails(aid,clickType){
		var fieldValues = "";
		var varianceExpenseAmount=0;
		var revisionExpense=0;
		var revisionRevenueAmount=0;
		var recGl=document.forms['pricingListForm'].elements['recGl'+aid].value;
		var payGl=document.forms['pricingListForm'].elements['payGl'+aid].value;   
		var billToCode=document.forms['pricingListForm'].elements['billToCode'+aid].value;
		var chargeCode=document.forms['pricingListForm'].elements['chargeCode'+aid].value;
		var vendorCode=document.forms['pricingListForm'].elements['vendorCode'+aid].value;
		var actgCode=document.forms['pricingListForm'].elements['actgCode'+aid].value;
		
		var type='PAY';
		var acref="${accountInterface}";
		acref=acref.trim();
		if(vendorCode.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
			alert("Vendor Code Missing. Please select");
		}else if(chargeCode.trim()=='' && type!='BAS' && type!='FX' && (type=='REC' || type=='PAY')){
			alert("Charge code needs to be selected");
		}else if(payGl.trim()=='' && type!='BAS' && type!='FX' && type=='PAY'){
			alert("Pay GL missing. Please select another charge code.");
		}else if(recGl.trim()=='' && type!='BAS' && type!='FX' && type=='REC'){
			alert("Receivable GL missing. Please select another charge code.");
		}else if(actgCode.trim()=='' && acref =='Y' && type!='BAS' && type!='FX' && type=='PAY'){
			alert("This Partner Code cannot be processed as the Accounting code for the vendor is missing, please contact your accounting dept");
		}else if(billToCode.trim()==''){
			alert("Bill To Code is Missing. Please select");
		}else if(chargeCode.trim()==''){
			alert("Charge code needs to be selected");
		}else if(recGl.trim()==''){
			alert("Receivable GL is missing. Please select another charge code.");
		}else{
			<c:if test="${discountUserFlag.pricingRevision && (customerFile.moveType == null  || customerFile.moveType!='Quote')}">	
			<configByCorp:fieldVisibility componentId="component.accountLine.varienceAmount">
				varianceExpenseAmount = document.forms['pricingListForm'].elements['varianceExpenseAmount'+aid].value;
			</configByCorp:fieldVisibility>
				revisionExpense=document.forms['pricingListForm'].elements['revisionExpense'+aid].value;
				revisionRevenueAmount=document.forms['pricingListForm'].elements['revisionRevenueAmount'+aid].value;
			</c:if>
			var actualExpense=document.forms['pricingListForm'].elements['actualExpense'+aid].value;
			var localAmount=document.forms['pricingListForm'].elements['localAmount'+aid].value;
			var estimateVendorName=document.forms['pricingListForm'].elements['estimateVendorName'+aid].value;
			var category = document.forms['pricingListForm'].elements['category'+aid].value;
			var actualRevenue=document.forms['pricingListForm'].elements['actualRevenue'+aid].value;
			var estimateRevenueAmount=document.forms['pricingListForm'].elements['estimateRevenueAmount'+aid].value;
		    var recRate=document.forms['pricingListForm'].elements['recRate'+aid].value;
			var recQuantity=document.forms['pricingListForm'].elements['recQuantity'+aid].value;
			var payingStatus = document.forms['pricingListForm'].elements['payingStatus'+aid].value;
			fieldValues = vendorCode+"~"+estimateVendorName+"~"+chargeCode+"~"+actgCode+"~"+varianceExpenseAmount+"~"+actualExpense+"~"+revisionExpense+"~"+localAmount+"~"+category;
			fieldValues = fieldValues+"~"+actualRevenue+"~"+estimateRevenueAmount+"~"+revisionRevenueAmount+"~"+recRate+"~"+recQuantity+"~"+payingStatus+"~"+payGl+"~"+recGl;
			<c:if test="${contractType}">
				var payableContractRateAmmount=document.forms['pricingListForm'].elements['payableContractRateAmmount'+aid].value;
				var contractRate=document.forms['pricingListForm'].elements['contractRate'+aid].value;
				var contractRateAmmount=document.forms['pricingListForm'].elements['contractRateAmmount'+aid].value;
				fieldValues = fieldValues+"~"+payableContractRateAmmount+"~"+contractRate+"~"+contractRateAmmount;
			</c:if>
		    <c:if test="${multiCurrency=='Y'}">
				var recCurrencyRate=document.forms['pricingListForm'].elements['recCurrencyRate'+aid].value;
				var actualRevenueForeign=document.forms['pricingListForm'].elements['actualRevenueForeign'+aid].value;
				fieldValues = fieldValues+"~"+recCurrencyRate+"~"+actualRevenueForeign;
			</c:if>
			var sid = '${serviceOrder.id}';
			var clickType = clickType;
			openWindow('getPayableSectionDetails.html?id='+aid+'&sid='+sid+'&fieldValues='+fieldValues+'&clickType='+clickType+'&decorator=popup&popup=true',1050,450);
		}
	}	
	
/* 	function setVatExcludeNew(aid){
		var chargeCheck=document.forms['pricingListForm'].elements['chargeCodeValidationVal'].value;
		if(chargeCheck=='' || chargeCheck==null){
		<c:if test="${systemDefaultVatCalculation=='true'}"> 
		var vatExcludeFlag = document.forms['pricingListForm'].elements['VATExclude'+aid].value;
		var flagVat=document.forms['pricingListForm'].elements['checkChargeCodePopup'].value;
		if(flagVat=="found"){ 
		if(document.forms['pricingListForm'].elements['chargeCode'+aid].value!=''){ 
			if(vatExcludeFlag !='false') { 
	    		var vatRec=0.00;
	    		vatRec=document.forms['pricingListForm'].elements['recVatAmt'+aid].value;
	    		vatRec=parseFloat(vatRec);
	    		var vatPay=0.00;
	    		vatPay=document.forms['pricingListForm'].elements['payVatAmt'+aid].value;
	    		vatPay=parseFloat(vatPay);
	    		if((vatRec>0)||(vatPay>0)) {
				var agree = confirm("The new charge code entered is VAT Exempt. Click OK to continue or Click Cancel.");
				 if(agree) {
	            	document.forms['pricingListForm'].elements['VATExclude'+aid].value=true;
					document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
					document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
					document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
					document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
					document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
					document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;
		    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
			        document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
			        document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
			        document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
			        document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
			        document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
				   }else { 
					   document.forms['pricingListForm'].elements['chargeCode'+aid].value=document.forms['pricingListForm'].elements['accChargeCodeTemp'+aid].value;	
					   document.forms['pricingListForm'].elements['VATExclude'+aid].value=document.forms['pricingListForm'].elements['oldVATExclude'+aid].value;
				   }
	    		}	else{ 
				document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=true; 
				document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=true;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=true;	 
	    		document.forms['pricingListForm'].elements['recVatDescr'+aid].value="";
				document.forms['pricingListForm'].elements['payVatDescr'+aid].value=""; 
				document.forms['pricingListForm'].elements['recVatPercent'+aid].value=0;
				document.forms['pricingListForm'].elements['recVatAmt'+aid].value=0;
				document.forms['pricingListForm'].elements['payVatPercent'+aid].value=0;
				document.forms['pricingListForm'].elements['payVatAmt'+aid].value=0;
	    		}
				} else { 
					document.forms['pricingListForm'].elements['payVatDescr'+aid].disabled=false;  
					var rev = document.forms['pricingListForm'].elements['category'+aid].value; 
	               if(rev != "Internal Cost"){
					document.forms['pricingListForm'].elements['recVatPercent'+aid].disabled=false;
					document.forms['pricingListForm'].elements['recVatAmt'+aid].disabled=false;
					document.forms['pricingListForm'].elements['recVatDescr'+aid].disabled=false; 
					}
					document.forms['pricingListForm'].elements['payVatPercent'+aid].disabled=false;
					document.forms['pricingListForm'].elements['payVatAmt'+aid].disabled=false; 
				} } }
		fillRecVatByChargeCode(aid);
		fillPayVatByChargeCode(aid);
		 </c:if>}
	} */
	
	function fillRevQtyLocalAmounts(aid){
		  var chargeCheck=document.forms['pricingListForm'].elements['chargeCodeValidationVal'].value;
			if(chargeCheck=='' || chargeCheck==null){
	     var charge = document.forms['pricingListForm'].elements['chargeCode'+aid].value;
	     var billingContract = '${billing.contract}';
	     var shipNumber=document.forms['pricingListForm'].elements['serviceOrder.shipNumber'].value;
	     var rev = document.forms['pricingListForm'].elements['category'+aid].value;
	     var comptetive = document.forms['pricingListForm'].elements['inComptetive'].value;
	     charge=charge.trim(); 
	     if(charge!="") { 
	     if(rev == "Internal Cost" && charge!="SALESCM" && charge!="VANPAY" && charge!="DOMCOMM" ){
	         var accountCompanyDivision =  document.forms['pricingListForm'].elements['companyDivision'].value  
	         var url="internalCostsExtracts1.html?ajax=1&decorator=simple&popup=true&charge=" + encodeURI(charge)+"&accountCompanyDivision=" + encodeURI(accountCompanyDivision)+ "&shipNum=" + encodeURI(shipNumber)+ "&sid=" +${serviceOrder.id} ;
	         http2.open("GET", url, true);
		     http2.onreadystatechange = handleHttpResponseFillRevQty;
		     http2.send(null);
		     } else if(rev == "Internal Cost"&&charge!="SALESCM" && (charge=="VANPAY" || charge=="DOMCOMM") ){ 
		     } } }}
	 function handleHttpResponseFillRevQty() { 
	             if (http2.readyState == 4){ 
	                var results = http2.responseText
	                 results = results.trim(); 
	                if(results !=''){
	                var res = results.split("#");
	                if(res[10]=="Preset"){
	                if(res[9]=="AskUser2")
	                {
	                var reply = prompt("Please Enter Quantity", "");
	                if((reply>0 || reply<9)|| reply=='.') {
	                document.forms['pricingListForm'].elements['localAmountPaybleHid'].value = reply;
	               	}else {
	              		alert("Please Enter legitimate Quantity");
	              	} 
	                } else{ 
	                if(res[7] == undefined){
	                document.forms['pricingListForm'].elements['localAmountPaybleHid'].value = "";
	                }else{                                           
	                document.forms['pricingListForm'].elements['localAmountPaybleHid'].value = res[7];
	                    }  }
	               if(res[10]=="AskUser"){
	                var reply = prompt("Please Enter Rate", "");
	                if((reply>0 || reply<9)|| reply=='.')  {
	              	document.forms['pricingListForm'].elements['ratePaybleHid'].value = reply;
	               } else {
	              		alert("Please Enter legitimate Rate");
	              	} 
	                } else{
	                  if(res[8] == undefined){ 
	                  document.forms['pricingListForm'].elements['ratePaybleHid'].value = "";
	                  }else{                                          
	                document.forms['pricingListForm'].elements['ratePaybleHid'].value = res[8];
	               }  }
	               if(res[10]=="BuildFormula"){
	               if(res[11] == undefined){
	               document.forms['pricingListForm'].elements['ratePaybleHid'].value ="";
	               }else{
	               document.forms['pricingListForm'].elements['ratePaybleHid'].value = res[11]; 
	               } 
	               }
		           var Q2 = document.forms['pricingListForm'].elements['localAmountPaybleHid'].value;
	               var Q1 = document.forms['pricingListForm'].elements['ratePaybleHid'].value;
	               var E1="";
	               if(Q1==0 && Q2==0)  {
	               E1=0*1;
	               }
	               else if(Q2==0)
	               {
	               E1=0*1;
	               } else {
	               E1=Q1*Q2;  
	               var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
	  		       var localAmount=0;
	  		       localAmount=E1*exchangeRate;
	  		       document.forms['pricingListForm'].elements['localAmount'+aid].value =Math.round(localAmount*100)/100;
	               document.forms['pricingListForm'].elements['actualExpense'+aid].value = E1;
	               }
	               var Q5 = res[5];            
	               var type = res[3];
	               var E2= "";
	               var E3= ""; 
	               if(type == 'Division')  {
	               	 if(Q5==0)   {
	               	   E2=0*1;
	                   var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
	  		           var localAmount=0;
	  		           localAmount=E2*exchangeRate;
	  		           document.forms['pricingListForm'].elements['localAmount'+aid].value =Math.round(localAmount*100)/100;
	               	   document.forms['pricingListForm'].elements['actualExpense'+aid].value = E2;
	                } else {	
	               		E2=(E1)/Q5;
	               		E3=Math.round(E2*100)/100;
	               		var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
	  		            var localAmount=0;
	  		            localAmount=E3*exchangeRate;
	  		            document.forms['pricingListForm'].elements['localAmount'+aid].value =Math.round(localAmount*100)/100;
	               		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
	                }  }
	               if(type == ' ' || type == '') {
	               		E2=(E1);
	               		E3=Math.round(E2*100)/100;
	               		var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
	  		            var localAmount=0;
	  		            localAmount=E3*exchangeRate;
	  		            document.forms['pricingListForm'].elements['localAmount'+aid].value =Math.round(localAmount*100)/100;
	               		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
	               }
	               if(type == 'Multiply') {
	               		E2=(E1)*Q5;
	               		E3=Math.round(E2*100)/100;
	               		var exchangeRate=document.forms['pricingListForm'].elements['exchangeRate'+aid].value;
	  		            var localAmount=0;
	  		            localAmount=E3*exchangeRate;
	  		            document.forms['pricingListForm'].elements['localAmount'+aid].value =Math.round(localAmount*100)/100;
	               		document.forms['pricingListForm'].elements['actualExpense'+aid].value = E3;
	                } 
	              document.forms['pricingListForm'].elements['invoiceNumber'+aid].value ="INT";
	              document.forms['pricingListForm'].elements['payingStatus'+aid].options[2].selected=true;
	        var mydate=new Date();
			var daym;
			var year=mydate.getFullYear()
			var y=""+year;
			if (year < 1000)
			year+=1900
			var day=mydate.getDay()
			var month=mydate.getMonth()+1
			if(month == 1)month="Jan";
			if(month == 2)month="Feb";
			if(month == 3)month="Mar";
			if(month == 4)month="Apr";
			if(month == 5)month="May";
			if(month == 6)month="Jun";
			if(month == 7)month="Jul";
			if(month == 8)month="Aug";
			if(month == 9)month="Sep";
			if(month == 10)month="Oct";
			if(month == 11)month="Nov";
			if(month == 12)month="Dec";
			 var daym=mydate.getDate()
			if (daym<10)
			daym="0"+daym
			var datam = daym+"-"+month+"-"+y.substring(2,4);
			if(document.forms['pricingListForm'].elements['invoiceNumber'+aid].value!='') {
			   document.forms['pricingListForm'].elements['receivedDate'+aid].value=datam;
			   document.forms['pricingListForm'].elements['valueDate'+aid].value=datam;
			   document.forms['pricingListForm'].elements['invoiceDate'+aid].value=datam;
		     } else {
		     document.forms['pricingListForm'].elements['valueDate'+aid].value='';
		     document.forms['pricingListForm'].elements['receivedDate'+aid].value='';
		     document.forms['pricingListForm'].elements['invoiceDate'+aid].value='';
		    } 
			findRevisedEstimateQuantitys(aid);
	        findRevisedQuantitys(aid);  
	      } }  }  }
		
	  
/*JS End ********************************************/
</script>