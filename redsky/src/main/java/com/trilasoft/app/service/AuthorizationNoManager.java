package com.trilasoft.app.service;


import java.util.List;

import java.util.List;


import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.AuthorizationNo;
public interface AuthorizationNoManager extends GenericManager<AuthorizationNo, Long>  { 
	public List findByJobAuth(String authNo,  String shipNumber);

	public List searchAccAuthorization(String shipNumber);
	public List searchAuthorizationNo(String shipNumber);
	public List findAuthorizationNo(String authNoInvoice);
	public List findMaximum(String shipNumber);
	public List findAuthNumber(String shipNumber, String sessionCorpID);

}
