<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title><fmt:message key="bookStorageDetail.title" /></title>
<meta name="heading" content="<fmt:message key='bookStorageDetail.heading'/>" />
<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
</style>
	
</head>
<s:form id="bookStorageForm" action="saveBookStorage" method="post" validate="true">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<div id="Layer1" style="width:100%">	
	<!-- Edited By Siddhartha -->
	<s:hidden name="countLocationDetailNotes" value="${countTicketLocationNotes}"/>
	<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />
	<s:hidden name="wtId" value="<%=request.getParameter("id1") %>" />
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="bookStorage.serviceOrderId" value="%{workTicket.serviceOrderId}" />
	<s:hidden name="soId" value="%{workTicket.serviceOrderId}" />
	<c:set var="countLocationDetailNotes" value="${countTicketLocationNotes}"/>
	<c:set var="id" value="<%=request.getParameter("id") %>" />
	<c:set var="id1" value="<%=request.getParameter("id1") %>" />
	
	<s:hidden name="serviceOrder.id" />
	<div id="Layer5" style="width:100%;">
		<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <c:if test="${forwardingTabVal!='Y'}"> 
	     			<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	     		</c:if>
	     		<c:if test="${forwardingTabVal=='Y'}">
	     			<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	     		</c:if>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
               <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  <c:if test="${serviceOrder.job =='RLO'}">
			  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <c:if test="${serviceOrder.job !='RLO'}">
			  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
			  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
		
		</div>
		<table width="100%"><tr><td>
		<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%></td></tr></table>
		
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id1") %>"><span>Work Ticket</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Location Detail</span></a></li>
					  	<li><a href="bookStorages.html?id=<%=request.getParameter("id1") %>"><span>Location List</span></a></li>
						<li><a href="searchLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=<%=request.getParameter("id1") %>"><span>Add Item To Location</span></a></li>
						<li><a href="storages.html?id=<%=request.getParameter("id1") %>"><span>Access/ Release Items</span></a></li>
						<li><a href="storagesMove.html?id=<%=request.getParameter("id1") %>"><span>Move Items</span></a></li>
					</ul>
		</div><div class="spn">&nbsp;</div>
		
	
	<div id="content" align="center">
<div id="liquid-round-top" style="!margin-top:7px;">
    <div class="top"><span></span></div>
    <div class="center-content">
	<table class="" cellspacing="1" cellpadding="0"border="0">
		<tbody>
			<tr>
				<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td class="listwhite" align="right" width="20px"></td>
							<td class="listwhitetext" align="right" width="150px">Location</td>
							<td><s:textfield name="bookStorage.locationId" readonly="true" cssClass="input-textUpper"/></td>
							<td></td>
							<td class="listwhitetext" align="right" width="200px">ID</td>
							<td><s:textfield name="bookStorage.id" size="10" maxlength="10" readonly="true" cssClass="input-textUpper"/></td>
							
							<c:if test="${empty bookStorage.id}">
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty bookStorage.id}">
								<c:choose>
									<c:when test="${countTicketLocationNotes == '0' || countTicketLocationNotes == '' || countTicketLocationNotes == null}">
								
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&ticket=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:when>
								<c:otherwise>
								
								<td align="right" width="400px"><img src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${id1}&ticket=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);"/><a onclick="javascript:openWindow('notess.html?id=${id1}&notesId=${ticket}&noteFor=WorkTicket&subType=TicketLocation&decorator=popup&popup=true',740,400);" ></a></td>
								</c:otherwise>
								</c:choose> 
							</c:if>
							
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right" valign="top" style="padding-top:5px">Description</td>
							<td colspan="2">
								<c:if test="${bookStorage.what=='R'}">
									<s:textarea name="bookStorage.description" cols="43" rows="4" cssClass="textarea" disabled="true"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textarea name="bookStorage.description" cols="43" rows="4" cssClass="textarea"/>
								</c:if>
							</td>
							<td class="listwhitetext" align="right" valign="top" style="padding-top:5px">Storage Action</td>
							<td align="left" valign="top"><s:textfield name="bookStorage.what" size="10" maxlength="10" cssClass="input-textUpper" tabindex="5" readonly="true"/></td>
						</tr>
						<tr>
							<td ></td>
							<td class="listwhitetext" align="right">Container ID</td>
							<td>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.containerId" size="20" maxlength="10" cssClass="input-text" />
								</c:if>	
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.containerId" size="20" maxlength="10" cssClass="input-textUpper" readonly="true"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Model</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-textUpper" readonly="true"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="storage.model" size="20" maxlength="10" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
			
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Serial</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-textUpper" readonly="true" />
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="storage.serial" size="20" maxlength="10" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Weight</td>
							<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="storage.measQuantity" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="storage.measQuantity" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-text"/>
							</c:if>	
							</td>
							
							<td align="left" colspan="" valign="middle" class="listwhitetext" width="150px"><fmt:message key='labels.units'/>
								<c:if test="${bookStorage.what=='R'}">
									<s:radio name="storage.unit" list="%{weightunits}" disabled="true"  onchange="changeStatus();"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">	
									<s:radio name="storage.unit" list="%{weightunits}" onchange="changeStatus();"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
						<td></td>
						<td class="listwhitetext" align="right">Volume</td>
						<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="storage.volume" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="storage.volume" size="20" maxlength="10" cssStyle="text-align:right" cssClass="input-text"/>
							</c:if>	
							</td>
							
							<td align="left" colspan="" valign="middle" class="listwhitetext" width="150px"><fmt:message key='labels.units'/>
								<c:if test="${bookStorage.what=='R'}">
									<s:radio name="storage.volUnit" list="%{volumeunits}" disabled="true"  onchange="changeStatus();"/>
								</c:if>
								<c:if test="${bookStorage.what!='R'}">	
									<s:radio name="storage.volUnit" list="%{volumeunits}" onchange="changeStatus();"/>
								</c:if>
							</td>
							<td></td>
						</tr>
						
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Item Tag</td>
							<td>
							<c:if test="${bookStorage.what=='R'}">
								<s:textfield name="bookStorage.itemTag" size="20" maxlength="10" cssClass="input-textUpper" readonly="true" />
							</c:if>
							<c:if test="${bookStorage.what!='R'}">
								<s:textfield name="bookStorage.itemTag" size="20" maxlength="10" cssClass="input-text" />
							</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Pieces</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.pieces" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true"/>
								</c:if>	
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.pieces" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-text" />
								</c:if>	
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">Price</td>
							<td>
								<c:if test="${bookStorage.what=='R'}">
									<s:textfield name="bookStorage.price" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-textUpper" readonly="true" />
								</c:if>
								<c:if test="${bookStorage.what!='R'}">
									<s:textfield name="bookStorage.price" size="20" maxlength="7" cssStyle="text-align:right" cssClass="input-text" />
								</c:if>
							</td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td class="listwhitetext" align="right">WareHouse</td>
						<td colspan="3">
						<c:if test="${bookStorage.what=='R'}">
						<s:select name="bookStorage.warehouse" list="%{house}" cssStyle="width:135px" cssClass="list-menu" headerKey="" headerValue="" onchange="" />
						</c:if>
						<c:if test="${bookStorage.what!='R'}">
						<s:select name="bookStorage.warehouse" list="%{house}" cssStyle="width:135px" cssClass="list-menu" headerKey="" headerValue="" onchange="" />
						</c:if>
						</td>
						</tr>
					  	<!-- 
					  		<s:hidden name="bookStorage.pieces" required="true" cssClass="text medium" />
					  	 -->
						<tr>
							
							<s:hidden name="bookStorage.idNum" required="true"  />
							<s:hidden name="bookStorage.model" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.oldLocation" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.ticket" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.jobNumber" required="true"  />
							<s:hidden name="bookStorage.measQuantity" required="true" cssClass="text medium" />
							<s:hidden name="bookStorage.itemNumber" required="true" cssClass="text medium" />
							
							<s:hidden name="bookStorage.shipNumber" required="true"  />
							<s:hidden name="bookStorage.corpID"/>
							<s:hidden name="bookStorage.type"/>
							<s:hidden name="bookStorage.dated"/>
							<s:hidden name="bookStorage.storageId"/>
							
							<s:hidden name="storage.id" required="true" cssClass="text medium" />
							<s:hidden name="storage.idNum" required="true"  />
							<s:hidden name="storage.description" required="true" cssClass="text medium" />
							<s:hidden name="storage.jobNumber" required="true"  />
							<s:hidden name="storage.itemNumber" required="true" cssClass="text medium" />
							<s:hidden name="storage.shipNumber" required="true"  />
							<s:hidden name="storage.corpID" required="true"/>
							<s:hidden name="storage.locationId" required="true" cssClass="text medium" />
							<s:hidden name="storage.price" required="true" cssClass="text medium" />
							<s:hidden name="storage.containerId" required="true"/>
							<s:hidden name="storage.pieces"/>
							<s:hidden name="storage.storageId"/>
							
							<s:hidden name="storage.originalPieces"/>
							<s:hidden name="storage.originalMeasQuantity"/>
							<s:hidden name="storage.originalVolume"/>
							
							<s:hidden name="storage.itemTag" required="true" cssClass="text medium" />
							<s:hidden name="storage.warehouse" required="true" cssClass="text medium" />
							<s:hidden name="storage.createdOn" required="true" cssClass="text medium" />
							<s:hidden name="storage.createdBy" required="true" cssClass="text medium" />
							<s:hidden name="storage.updatedOn" required="true" cssClass="text medium" />
							<s:hidden name="storage.updatedBy" required="true" cssClass="text medium" />
							<c:if test="${not empty storage.releaseDate}">
		 <s:text id="storageFormattedReleaseDate" name="${FormDateValue}"> <s:param name="value" value="storage.releaseDate" /></s:text>
			 <s:hidden  name="storage.releaseDate" value="%{storageFormattedReleaseDate}" /> 
	 </c:if>
	 <c:if test="${empty storage.releaseDate}">
		 <s:hidden   name="storage.releaseDate"/> 
	 </c:if>
							<s:hidden name="ticket" value="<%=request.getParameter("ticket") %>"/> 
						</tr>
						
						
						<tr>
							<td></td>
							<td> </td>
							<td></td>
							<td> </td>
						</tr>

					</tbody>
				</table>
				
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 

<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.createdOn' /></td>
							<td style="width:120px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${bookStorage.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="bookStorage.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${bookStorage.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.createdBy' /></td>
								<c:if test="${not empty bookStorage.id}">
									<s:hidden name="bookStorage.createdBy"/>
									<td><s:label name="createdBy" value="%{bookStorage.createdBy}"/></td>
								</c:if>
								<c:if test="${empty bookStorage.id}">
									<s:hidden name="bookStorage.createdBy" value="${pageContext.request.remoteUser}"/>
									<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
								</c:if>
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.updatedOn' /></td>
							
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${bookStorage.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="bookStorage.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${bookStorage.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='bookStorage.updatedBy' /></td>
								<c:if test="${not empty bookStorage.id}">
									<s:hidden name="bookStorage.updatedBy"/>
									<td><s:label name="updatedBy" value="%{bookStorage.updatedBy}"/></td>
								</c:if>
								<c:if test="${empty bookStorage.id}">
									<s:hidden name="bookStorage.updatedBy" value="${pageContext.request.remoteUser}"/>
									<td><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
								</c:if>
						</tr>
					</tbody>
				</table>

<div style="float:left;">
<div style="float:left;">
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="save" key="button.save" onmouseover="return chkSelect();" onclick="forwardToMyMessage();" /> 
	</div>
	<div style="margin-left:10px;float:left;">
		<s:reset type="button" key="Reset" cssClass="cssbutton" cssStyle="width:55px; height:25px"/>
	</div>
</div>
</div>
</s:form>

<%-- Script Shifted from Top to Botton on 12-Sep-2012 By Kunal --%>
<script type="text/javascript"> 
	function forwardToMyMessage(){ 
			document.forms['bookStorageForm'].elements['storage.price'].value=document.forms['bookStorageForm'].elements['bookStorage.price'].value;		
			document.forms['bookStorageForm'].elements['storage.description'].value=document.forms['bookStorageForm'].elements['bookStorage.description'].value;		
			document.forms['bookStorageForm'].elements['storage.containerId'].value=document.forms['bookStorageForm'].elements['bookStorage.containerId'].value;		
			document.forms['bookStorageForm'].elements['storage.pieces'].value=document.forms['bookStorageForm'].elements['bookStorage.pieces'].value;		
			document.forms['bookStorageForm'].elements['storage.locationId'].value=document.forms['bookStorageForm'].elements['bookStorage.locationId'].value;
			document.forms['bookStorageForm'].elements['storage.itemNumber'].value=document.forms['bookStorageForm'].elements['bookStorage.itemNumber'].value;		
			//document.forms['bookStorageForm'].elements['storage.corpID'].value=document.forms['bookStorageForm'].elements['bookStorage.corpID'].value;	
			document.forms['bookStorageForm'].elements['storage.shipNumber'].value=document.forms['bookStorageForm'].elements['bookStorage.shipNumber'].value;	
			document.forms['bookStorageForm'].elements['storage.itemTag'].value=document.forms['bookStorageForm'].elements['bookStorage.itemTag'].value;
			document.forms['bookStorageForm'].elements['storage.warehouse'].value=document.forms['bookStorageForm'].elements['bookStorage.warehouse'].value;			
			//document.forms['bookStorageForm'].elements['storage.unit'].value=document.forms['bookStorageForm'].elements['storage.price'].value;		
	}
</script>
<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || ( keyCode==110); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109); 
	}	
	
	function notExists(){
		alert("The Ticket information is not saved yet");
	}
		
	function chkSelect()
	{
		if (checkFloat('bookStorageForm','storage.measQuantity','Invalid data in Weight') == false)
        {
           document.forms['bookStorageForm'].elements['storage.measQuantity'].focus();
           return false
        }
        if (checkFloat('bookStorageForm','storage.volume','Invalid data in Volume') == false)
        {
           document.forms['bookStorageForm'].elements['storage.volume'].focus();
           return false
        }
        if (checkFloat('bookStorageForm','bookStorage.pieces','Invalid data in Pieces') == false)
        {
           document.forms['bookStorageForm'].elements['bookStorage.pieces'].focus();
           return false
        }
        if (checkFloat('bookStorageForm','bookStorage.price','Invalid data in Price') == false)
        {
           document.forms['bookStorageForm'].elements['bookStorage.price'].focus();
           return false
        }	
	}
</script>
<%-- Shifting Closed Here --%>

<script type="text/javascript"> 
  try{  
<c:if test="${bookStorage.what=='R'}">
document.forms['bookStorageForm'].elements['method:save'].disabled=true;
document.forms['bookStorageForm'].elements['Reset'].disabled=true;
</c:if>
}
catch(e){}

    document.forms['bookStorageForm'].elements['bookStorage.price'].value = '${storage.price}';
	document.forms['bookStorageForm'].elements['bookStorage.description'].value = '${storage.description}';	
	document.forms['bookStorageForm'].elements['bookStorage.containerId'].value = '${storage.containerId}';
	document.forms['bookStorageForm'].elements['bookStorage.pieces'].value = '${storage.pieces}';		
	document.forms['bookStorageForm'].elements['bookStorage.locationId'].value = '${storage.locationId}';
	document.forms['bookStorageForm'].elements['bookStorage.itemNumber'].value = '${storage.itemNumber}';		
	document.forms['bookStorageForm'].elements['bookStorage.shipNumber'].value = '${storage.shipNumber}';	
	document.forms['bookStorageForm'].elements['bookStorage.itemTag'].value = '${storage.itemTag}';	

</script>

