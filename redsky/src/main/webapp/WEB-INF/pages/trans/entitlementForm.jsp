<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Entitlement Details</title>   
    <meta name="heading" content="Entitlement"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">

    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }
  
    function closeWIN(){
    	window.close();
    }
    
    	function validation(){
        	var opt = document.forms['entitlementForm'].elements['entitlement.eOption'].value;
        	var otValueChange = document.forms['entitlementForm'].elements['optionChange'].value;
        	if(opt.length == 0 ){
        		alert("Option is a required field.");
        		return false;
        	}else 
        	{
        		if(otValueChange=='true'){
        			return checkChar();
        		}
        	
//        		valid(document.forms['entitlementForm'].elements['entitlement.eOption'],'special');
//            	checkOptionDuplicate(opt);
        	}
        	if(document.forms['entitlementForm'].elements['entitlement.eOption'].value.length ==0){
        		alert("Option is a required field.");
        		return false;
        	}
        	return true;
    	}
    	function checkOptionDuplicate(option){
    		var option = document.forms['entitlementForm'].elements['entitlement.eOption'].value;
    		var partnerCode = "<%=request.getParameter("partnerCode")%>";
    		if(option.trim() != ''){
	    		document.getElementById('save').disabled = true; 
	    		var url="entitlementOptionDuplicateCheckAjax.html?ajax=1&decorator=simple&popup=true&partnerCode=" +partnerCode+"&eOption="+ encodeURI(option);
	    	    http2.open("GET", url, true);
	    	    http2.onreadystatechange = handleHttpResponse44;
	    	    http2.send(null);
    		}
        }
    	function handleHttpResponse44(){
    		if (http2.readyState == 4) {
    			document.getElementById('save').disabled = false; 
    	          var results = http2.responseText
    	          results = results.trim();
    	          results =	results.replace('[','');
    	          results =	results.replace(']','');
    	          if(results == 'YES'){
    	        	  alert('Option with the same name already exists.');
    	        	  document.forms['entitlementForm'].elements['entitlement.eOption'].value;
    	          }
    	    }
    		else{
    			
    			document.getElementById('save').disabled = false; 
    		}
    	} 
    function checkValidOption(){
        var option=document.forms['entitlementForm'].elements['entitlement.eOption'].value;
        option=option.trim();
        if(option==''){
        	alert('Please enter valid value.');
        	document.forms['entitlementForm'].elements['entitlement.eOption'].value='';
		return false;
        }
}   
    </script>

</head>

<c:set var="buttons"> 
    <input name="save" id="save" type="submit"" class="cssbutton1" onclick="return validation();" value="Save" style="width:55px; height:25px" tabindex="83"/> 
	<input type="reset" class="cssbutton1" onclick="closeWIN();" value="Close" style="width:55px; height:25px" tabindex="83"/> 
</c:set>

<s:form id="entitlementForm" action="saveEntitlement.html?btntype=yes&decorator=popup&popup=true" method="post" validate="true">

<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<s:hidden name="checkDup" value="<%=request.getParameter("checkDup") %>"/>
<s:hidden name="entitlement.id" />
<s:hidden name="optionChange" />
<s:hidden name="id" value="${entitlement.id}"/>  
<s:hidden name="entitlement.partnerPrivateId" value="<%=request.getParameter("partnerPrivateId")%>"/>
<s:hidden name="entitlement.partnerCode" value="<%=request.getParameter("partnerCode")%>"/>


<div id="layer1" style="width:500px; margin-top: 10px;">
<div id="newmnav">
			<ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Entitlement<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      
<div id="Layer5" style="width:95%" >
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:95%;margin:0px;" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0"  style="margin-left:30px">
<tbody>
<tr>
<td align="right" width="75px" class="listwhitetext" valign="bottom">Option<font color="red" size="2">*</font></td> 
<td style="width:10px"></td>
<td align="left" class="listwhitetext" valign="top"><s:textfield name="entitlement.eOption" onchange="checkChar();" maxlength="60"  required="true" cssClass="input-text" size="60" tabindex="1" /></td>
</tr>
<tr>
<td align="left" width="75px" class="listwhitetext" valign="top">Description</font></td>
<td style="width:10px"></td>
<td  align="left" valign="top"><s:textarea name="entitlement.eDescription" required="true" cssClass="textarea" tabindex="1" cols="57" rows="5"  
	onkeydown="limitText(500);" 
	onkeyup="limitText(500);"/></td> 
 </tr>
<tr>	<td align="left" colspan="2">&nbsp;</td>
     <td align="left" class="listwhitetext" colspan="2" >						
		<c:out value="${buttons}" escapeXml="false" />					
	</td>
	</tr>
	<tr></tr>
	<tr></tr>
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 



<table width="500px" >
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${entitlement.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="entitlement.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:100px"><fmt:formatDate value="${entitlement.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>						
						<c:if test="${not empty entitlement.id}">
								<s:hidden name="entitlement.createdBy"/>
								<td ><s:label name="createdBy" value="%{entitlement.createdBy}"/></td>
							</c:if>
							<c:if test="${empty entitlement.id}">
								<s:hidden name="entitlement.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
						
						<tr>	
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${entitlement.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="entitlement.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:100px"><fmt:formatDate value="${entitlement.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty entitlement.id}">
							<s:hidden name="entitlement.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{entitlement.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty entitlement.id}">
							<s:hidden name="entitlement.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
</div> 
 
</div>
</s:form> 
<div id="mydiv" style="position:absolute;top:110px;margin-top:-15px;"></div>

<script type="text/javascript">  
function pick() {
		try {
			parent.window.opener.document.location.reload();
  		}catch(e){
  			parent.window.opener.document.location.reload();
  		}
		window.close();
}


try{
	var chDup = document.forms['entitlementForm'].elements['checkDup'].value;
//	alert(chDup);
	if(chDup == 'NO'){
 		pick();
	}else if(chDup == 'YES'){
		alert('OPTION with same name already exists');
//		window.close();
	//	pick();
	}
}
 catch(e){}
	 </script>
	 
	 <script type="text/javascript">  
	
	 
	 function limitText(size) {
 	 	var descLimit = document.forms['entitlementForm'].elements['entitlement.eDescription'].value;
 	 	if(descLimit.length > size){
 	 		descLimit = descLimit.substring(0, size);
 	 		document.forms['entitlementForm'].elements['entitlement.eDescription'].value = descLimit;
 	 	}
	 } 

	 function checkChar() {
		var capString = document.forms['entitlementForm'].elements['entitlement.eOption'].value;
		document.forms['entitlementForm'].elements['entitlement.eOption'].value = capString.capitalize();
		document.forms['entitlementForm'].elements['optionChange'].value='true';
		 valid(document.forms['entitlementForm'].elements['entitlement.eOption'],'special');
//		 return;
			 var chArr = new Array("%","&","'","|","\\","\"");
		 	 var origString = document.forms['entitlementForm'].elements['entitlement.eOption'].value;
		 	for ( var i = 0; i < chArr.length; i++ ) {
		 		origString = origString.split(chArr[i]).join(''); 
		 	}
		 	document.forms['entitlementForm'].elements['entitlement.eOption'].value = origString;
		 	var strString = document.forms['entitlementForm'].elements['entitlement.eOption'].value;
			 if(parseInt(strString) == strString){
				 alert('Only numeric is not allowed.');
				 document.forms['entitlementForm'].elements['entitlement.eOption'].value='';
				 return false;
			 }else if(/^\d+\.\d+$/.test(strString)){
				 alert('only decimal number is not valid');
				 document.forms['entitlementForm'].elements['entitlement.eOption'].value='';
				  return false;
			 }
			 checkOptionDuplicate();
			 checkValidOption();
		 	return true;
	 }
	 var http2 = getHTTPObject();
	</script>