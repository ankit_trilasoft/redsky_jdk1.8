<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="chargesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargesList.heading'/>"/> 
<style>

span.pagelinks {
	display:block;
	font-size:0.95em;
	margin-bottom:3px;
	!margin-bottom:1px;
	margin-top:-18px;
	!margin-top:-18px;
	padding:2px 0px;
	text-align:right;
	width:99%;
}
</style>
<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['chargesListForm'].elements['charges.charge'].value = "";
	document.forms['chargesListForm'].elements['charges.description'].value = "";
	}
</script>   
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="searchChargesForPopup" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
</c:set>

<s:form id="chargesListForm" action='${empty param.popup?"searchChargesContractPricing.html":"searchChargesContractPricing.html?decorator=popup&popup=true&originCountry=%{originCountry}&destinationCountry=%{destinationCountry}&serviceMode=%{serviceMode}"}' method="post" >
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:12px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%"  >
<thead>
<tr>

<th><fmt:message key="charges.charge"/></th>

<th><fmt:message key="charges.description"/></th>
<th></th>
</tr></thead>	
		<tbody>
		<tr>
			
			<td>
			    <s:textfield name="charges.charge" size="9" required="true" cssClass="text medium" onfocus="onFormLoad();" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/></td>
			    <td><s:textfield name="charges.description" size="25" required="true" cssClass="text medium" onkeyup="valid(this,'special')" onblur="valid(this,'special')"/>
			    <s:hidden name="contract" value="${contract}"/>
			</td>
			<td width="130px" align="center">
			    <c:out value="${searchbuttons}" escapeXml="false" />   
			</td>
			</tr>
		</tbody>
	</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>


<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCharges.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
           
    <input type="button" onclick="location.href='<c:url value="/mainMenu.html"/>'"  
        value="<fmt:message key="button.done"/>"/>   
</c:set>  



<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden name="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
	<s:hidden  name="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
	<s:hidden  name="fld_ninthDescription" value="${param.fld_ninthDescription}" />	
	<s:hidden  name="fld_tenthDescription" value="${param.fld_tenthDescription}" />	
	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
    <c:set var="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
     <c:set var="fld_ninthDescription" value="${param.fld_ninthDescription}" />	
      <c:set var="fld_tenthDescription" value="${param.fld_tenthDescription}" />	
</c:if>

<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Charges List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<s:set name="chargess" value="chargess" scope="request"/>  
<s:hidden name="billing.contract" />
<s:hidden name="billing.id" value="%{billing.id}"/>

<display:table name="chargess" class="table" requestURI="" id="chargesList" defaultsort="2" pagesize="30" >   
   	<display:column sortable="true" titleKey="charges.charge">
   		 <a onclick="setChargeCode('${chargesList.charge}','${chargesList.description}')"> <c:out value="${chargesList.charge}"></c:out> </a>
   	</display:column>   
    <display:column property="description" sortable="true" titleKey="charges.description"/>
    <c:if test="${chargesList.contract == 'Driver Settlement'}"> 
    	<display:column property="deductionType" sortable="true" title="Deduction Type"/>
        <display:column property="frequency" sortable="true" title="Frequency"/>
    </c:if>
    <display:setProperty name="paging.banner.item_name" value="charges"/>   
    <display:setProperty name="paging.banner.items_name" value="chargess"/>   
  
    <display:setProperty name="export.excel.filename" value="Charges List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Charges List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Charges List.pdf"/>   
</display:table>  

<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
</s:form>
<script type="text/javascript">   
   
   
   function setChargeCode(chargeCode,desc){
	   window.opener.setFieldValue('${param.fld_code}',chargeCode);
	   //window.opener.setFieldValue('${param.fld_seventhDescription}',desc);
	   window.close();
   }
</script>