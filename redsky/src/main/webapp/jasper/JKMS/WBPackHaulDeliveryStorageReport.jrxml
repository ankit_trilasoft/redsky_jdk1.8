<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="WBPackHaulDeliveryStorageReport" pageWidth="2736" pageHeight="595" orientation="Landscape" whenNoDataType="AllSectionsNoDetail" columnWidth="2736" leftMargin="0" rightMargin="0" topMargin="20" bottomMargin="20" uuid="93cfaece-592b-480e-a554-dd5c9bd3c1b0">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[select "WBPackHaulDeliverStorageReport-X-JKMS.jrxml",
s.shipnumber AS 'JK REF',
a.authorization AS 'SAP',
a.recInvoiceNumber AS 'Invnum',
if(s.firstName is null || trim(s.firstName)="",s.lastName,concat(s.lastName,", ",s.firstName)) as 'Staff Name',
s.job AS 'STO Type',
sum(ROUND(a.actualRevenue, 2)) AS 'Invoice Amount',
sum(ROUND(if(a.chargeCode in('JKORIGIN','OASERV','JKDESTIN','DASERV'),a.actualRevenue,0), 2)) AS 'Pack and Haul and Delivery out',
sum(ROUND(if(a.chargeCode in('JKSTO','STOR'),a.actualRevenue,0), 2)) AS 'Storage',
sum(ROUND(if(a.chargeCode in('JKCRATE','CRATE'),a.actualRevenue,0), 2)) AS 'Crating',
sum(ROUND(if(a.chargeCode in('JKSHUTTLE','SHUTTLE'),a.actualRevenue,0), 2)) AS 'Shuttle',
sum(ROUND(if(a.chargeCode in('COORDFEE'),a.actualRevenue,0), 2)) AS 'Coordination fee in/out',
sum(ROUND(if(a.chargeCode in('JKORIGIN','OASERV','JKDESTIN','DASERV','JKSTO','STOR','JKCRATE','CRATE','JKSHUTTLE','SHUTTLE','COORDFEE'),0,a.actualRevenue), 2)) AS 'Others',
group_concat(if(a.description is null || a.description='','',trim(a.description)) separator ", ") AS 'Description',
m.actualNetWeight AS 'ActualWgt',
t.originAgent AS 'Origin Agent',
s.originCity AS 'Origin City',
s.originState AS 'Origin State',
s.originCountry AS 'Origin Country'
from serviceorder s
inner join miscellaneous m on m.id = s.id and m.corpid=$P{Corporate ID}
inner join trackingstatus t on t.id = s.id and t.corpid=$P{Corporate ID}
inner join accountline a on s.id = a.serviceOrderId and a.corpid=$P{Corporate ID}
where s.corpid=$P{Corporate ID}
and s.Job in('STO','TPS')
and (a.sendActualToClient is null || a.sendActualToClient='')
and a.actualRevenue <> 0
and a.billToCode='T166386'
and a.recInvoiceNumber !=''
and ((a.storageDateRangeFrom is null || a.storageDateRangeFrom='') and (a.storageDateRangeTo is null || a.storageDateRangeTo=''))
and s.status <> 'CNCL'
and a.status is true
and a.status = true
group by a.recInvoiceNumber
order by a.shipnumber,a.recInvoiceNumber]]>
	</queryString>
	<field name="WBPackHaulDeliverStorageReport-X-JKMS.jrxml" class="java.lang.String"/>
	<field name="JK REF" class="java.lang.String"/>
	<field name="SAP" class="java.lang.String"/>
	<field name="Invnum" class="java.lang.String"/>
	<field name="Staff Name" class="java.lang.String"/>
	<field name="STO Type" class="java.lang.String"/>
	<field name="Invoice Amount" class="java.math.BigDecimal"/>
	<field name="Pack and Haul and Delivery out" class="java.math.BigDecimal"/>
	<field name="Storage" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Crating" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Shuttle" class="java.math.BigDecimal"/>
	<field name="Coordination fee in/out" class="java.math.BigDecimal"/>
	<field name="Others" class="java.math.BigDecimal">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="Description" class="java.lang.String">
		<fieldDescription><![CDATA[]]></fieldDescription>
	</field>
	<field name="ActualWgt" class="java.math.BigDecimal"/>
	<field name="Origin Agent" class="java.lang.String"/>
	<field name="Origin City" class="java.lang.String"/>
	<field name="Origin State" class="java.lang.String"/>
	<field name="Origin Country" class="java.lang.String"/>
	<variable name="TotalInvoice" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{Invoice Amount}]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="27" splitType="Stretch">
			<staticText>
				<reportElement x="120" y="0" width="121" height="27" uuid="9761580d-fd8f-4d62-b0c1-038c887ffcc1"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[SAP]]></text>
			</staticText>
			<staticText>
				<reportElement x="345" y="0" width="210" height="27" uuid="e07772f6-20f0-434b-9e58-732e9160f862"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Staff Name]]></text>
			</staticText>
			<staticText>
				<reportElement x="241" y="0" width="104" height="27" uuid="49710bb1-1449-4758-b186-e9b41f7c49fa"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Invnum]]></text>
			</staticText>
			<staticText>
				<reportElement x="555" y="0" width="72" height="27" uuid="0ef365b6-1685-49f4-819b-03a270cd7521"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[STO Type]]></text>
			</staticText>
			<staticText>
				<reportElement x="627" y="0" width="125" height="27" uuid="773e6dba-7c83-4460-9b56-e7c7060eb3a9"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[ Invoice Amount]]></text>
			</staticText>
			<staticText>
				<reportElement x="752" y="0" width="153" height="27" uuid="37145b79-a03e-41c2-b2f3-239bb0941f46"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Pack and Haul and Delivery out]]></text>
			</staticText>
			<staticText>
				<reportElement x="995" y="0" width="90" height="27" uuid="97588468-2982-45fb-b946-8d4b0cb8b848"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Crating]]></text>
			</staticText>
			<staticText>
				<reportElement x="905" y="0" width="90" height="27" uuid="b75dd144-010a-4b7d-a5f9-c025bdcf0a51"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Storage]]></text>
			</staticText>
			<staticText>
				<reportElement x="1085" y="0" width="90" height="27" uuid="1b26ef0f-1341-4a15-ba47-ca8bfcb63121"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Shuttle]]></text>
			</staticText>
			<staticText>
				<reportElement x="1293" y="0" width="110" height="27" uuid="f80183d6-15ec-4532-98da-49c64a7d805a"/>
				<box rightPadding="6"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Others]]></text>
			</staticText>
			<staticText>
				<reportElement x="1175" y="0" width="118" height="27" uuid="9ea49169-047f-4727-9f6a-68fcc882252e"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Coordination fee in/out]]></text>
			</staticText>
			<staticText>
				<reportElement x="1925" y="0" width="100" height="27" uuid="4f11f549-3e8c-46e4-8a79-7e6735604d05"/>
				<box leftPadding="0" rightPadding="10"/>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[ Actual Wgt]]></text>
			</staticText>
			<staticText>
				<reportElement x="2250" y="0" width="160" height="27" uuid="01c92e46-6219-4244-8751-5d017aab0c21"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin City]]></text>
			</staticText>
			<staticText>
				<reportElement x="2025" y="0" width="225" height="27" uuid="ac43160d-2a75-4d58-8294-066da0844c35"/>
				<box rightPadding="3"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin Agent]]></text>
			</staticText>
			<staticText>
				<reportElement x="2410" y="0" width="125" height="27" uuid="101a5a8d-6f97-4493-b992-9314da0b998a"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin State]]></text>
			</staticText>
			<staticText>
				<reportElement x="2535" y="0" width="201" height="27" uuid="4339c602-46cd-4ef0-9f86-b64ac61e27dc"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Origin Country]]></text>
			</staticText>
			<staticText>
				<reportElement x="0" y="0" width="120" height="27" uuid="fd573fb5-a748-4903-821a-d833b17a720f"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[JK REF]]></text>
			</staticText>
			<staticText>
				<reportElement x="1403" y="0" width="522" height="27" uuid="17aeb985-aa5f-483e-8fe2-9f20230482a4"/>
				<box leftPadding="3" rightPadding="0"/>
				<textElement>
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Description]]></text>
			</staticText>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="27" splitType="Stretch">
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="120" y="0" width="121" height="27" isPrintWhenDetailOverflows="true" backcolor="#FFFFFF" uuid="574c1b49-7711-48ff-89f1-b0135030d864"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{SAP}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="241" y="0" width="104" height="27" isPrintWhenDetailOverflows="true" uuid="daa0f227-bd2b-434c-94b8-7f5af9d5e9f1"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Invnum}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="345" y="0" width="210" height="27" isPrintWhenDetailOverflows="true" uuid="0d6803c8-d3af-40ee-a4c9-208b8876991b"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Staff Name}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="555" y="0" width="72" height="27" isPrintWhenDetailOverflows="true" uuid="4d3881c7-121f-436c-8ed3-ef644fa873c8"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{STO Type}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="627" y="0" width="125" height="27" isPrintWhenDetailOverflows="true" uuid="2ac08373-2bb3-4764-a8d5-1bbefbbcbb61"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Invoice Amount}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="752" y="0" width="153" height="27" isPrintWhenDetailOverflows="true" uuid="87baf597-8f93-4c36-8027-97d0f213a72a"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Pack and Haul and Delivery out}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="905" y="0" width="90" height="27" isPrintWhenDetailOverflows="true" uuid="35a6177f-49f3-447a-bf80-08fd530f1ca9"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Storage}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="995" y="0" width="90" height="27" isPrintWhenDetailOverflows="true" uuid="86b8fff4-abc0-4667-8855-490f211b2ab8"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Crating}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="1085" y="0" width="90" height="27" isPrintWhenDetailOverflows="true" uuid="4df170ed-6c49-4b4b-a349-ac60c03b5454"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shuttle}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="1175" y="0" width="118" height="27" isPrintWhenDetailOverflows="true" uuid="a64fc20c-3b8a-442d-b8cb-24ba9ad67146"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Coordination fee in/out}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="1293" y="0" width="110" height="27" isPrintWhenDetailOverflows="true" uuid="4e65c93d-b354-4d07-af8a-46db3aaf3cc5"/>
				<box rightPadding="6"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Others}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="1925" y="0" width="100" height="27" isPrintWhenDetailOverflows="true" uuid="b62aaa2b-21d0-4cf5-b84d-663e4511ecfe"/>
				<box leftPadding="0" rightPadding="10"/>
				<textElement textAlignment="Right">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{ActualWgt}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="2025" y="0" width="225" height="27" isPrintWhenDetailOverflows="true" uuid="24f7bc26-2360-491b-9fba-00f8a9cb1a79"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Origin Agent}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="2250" y="0" width="160" height="27" isPrintWhenDetailOverflows="true" uuid="38acf753-fce9-4f8c-a139-3abacccf9a39"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Origin City}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="2410" y="0" width="125" height="27" isPrintWhenDetailOverflows="true" uuid="60a69162-bef7-4707-b624-64fe5047d7b9"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Origin State}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="2535" y="0" width="201" height="27" isPrintWhenDetailOverflows="true" uuid="4ed0c838-9151-44f7-81d2-aedea33bddb4"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Origin Country}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="0" y="0" width="120" height="27" isPrintWhenDetailOverflows="true" uuid="89e446fd-e618-4519-a1f6-d01a18823d82"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{JK REF}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="1403" y="0" width="522" height="27" isPrintWhenDetailOverflows="true" uuid="520c26cb-054b-45b7-a026-fe59bd3f7cd9"/>
				<box leftPadding="3">
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement>
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Description}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band height="27" splitType="Stretch">
			<textField isStretchWithOverflow="true" pattern="$ #,##0.00" isBlankWhenNull="true">
				<reportElement mode="Transparent" x="627" y="0" width="125" height="27" isPrintWhenDetailOverflows="true" uuid="2c1a9392-f89e-4f9f-b8e4-693b775fc798"/>
				<box>
					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{TotalInvoice}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="120" height="27" uuid="16b22f13-2899-4de5-9629-81612c0f60ca"/>
				<textElement verticalAlignment="Middle">
					<font fontName="Arial" size="10" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Total]]></text>
			</staticText>
		</band>
	</summary>
</jasperReport>
