package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.PartnerBankInformation;

public interface PartnerBankInfoDao extends GenericDao <PartnerBankInformation,Long> {
	
	public List findBankData(String paetnercode, String sessionCorpID);

}
