<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Tasks Check List</title>
    <meta name="heading" content="Task Check List"/>
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:0px; !margin-bottom:2px; margin-top:-18px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
.hidden {
 display: none;
}
.table-price thead th {height:22px;}
</style>
</head>
<%-- <div id="newmnav" style="float:left;">
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">      
    <c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">        
        <c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<ul><c:if test="${not empty serviceOrder.id && serviceOrder.controlFlag=='C' && (empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove')}">
        <sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
        <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
        <li ><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a>
        </li>
        </c:if>
        <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
        <li ><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a>
        </li>
        </c:if>     
        </sec-auth:authComponent>
        <sec-auth:authComponent componentId="module.tab.serviceorder.billingTab">
        <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
            <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
        </sec-auth:authComponent>
        </sec-auth:authComponent>
        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
         <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
        <c:choose>
            <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
               <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>  
            </c:when>
            <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li> 
            </c:when>
            <c:otherwise> 
               <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a>
               </li>
            </c:otherwise>
      </c:choose>
      </c:if>
      </sec-auth:authComponent>
      <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
      <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
        <c:choose> 
            <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
                <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li> 
            </c:when>
            <c:otherwise> 
               <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a>
               </li>
            </c:otherwise>
      </c:choose>
      </c:if>
      </sec-auth:authComponent>
      <c:if test="${serviceOrder.job =='OFF'}">
             <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
             <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
             </sec-auth:authComponent>
       </c:if>
        <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
         <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
     </sec-auth:authComponent>
      <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
      <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
      <c:if test="${serviceOrder.job !='RLO'}">
        <c:if test="${forwardingTabVal!='Y'}"> 
            <li><a href="containers.html?id=${serviceOrder.id}" ><span>Forwarding</span></a></li>
        </c:if>
        <c:if test="${forwardingTabVal=='Y'}">
            <li><a href="containersAjaxList.html?id=${serviceOrder.id}" ><span>Forwarding</span></a></li>
        </c:if>
    </c:if>
    </c:if>
    </sec-auth:authComponent>
     <sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
        <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
        <c:if test="${serviceOrder.job !='RLO'}"> 
            <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
        </c:if>
        </c:if>
    </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.notes.statusTab">
                    <c:if test="${serviceOrder.job =='RLO'}"> 
                    <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                    </c:if>
                    <c:if test="${serviceOrder.job !='RLO'}"> 
                    <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                    </c:if>                     
</sec-auth:authComponent>   
<sec-auth:authComponent componentId="module.tab.notes.ticketTab">
<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
                <c:if test="${serviceOrder.job !='RLO'}"> 
                    <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
                    </c:if>
                    </c:if>
                </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
                <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
                <c:if test="${serviceOrder.job !='RLO'}"> 
                    <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
                    </c:if>
                    </c:if>
                </sec-auth:authComponent>
                <!--<sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
                    <li><a href="editCustomerFile.html?id=${serviceOrder.customerFileId}" ><span>Customer File</span></a></li>
                </sec-auth:authComponent>
    --><sec-auth:authComponent componentId="module.tab.serviceorder.reportTab"> 
            <li><a onmouseover="return chkSelect();" onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
    </sec-auth:authComponent>
  </c:if>
  <c:if test="${not empty serviceOrder.id && serviceOrder.controlFlag=='Q'}">
    <sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
    <li ><a href="editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}" class=""><span>Quotes</span></a></li>
        </sec-auth:authComponent> 

  </c:if>
  <c:if test="${not empty customerFile.id && customerFile.controlFlag=='C' && (empty customerFile.moveType || customerFile.moveType=='BookedMove')}">
  <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
    <li ><a href="editCustomerFile.html?id=${customerFile.id}"  ><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  </sec-auth:authComponent>
  </c:if>
   <c:if test="${not empty customerFile.id && (customerFile.controlFlag=='Q' ||( not empty customerFile.moveType && customerFile.moveType=='Quote'))}">
        <sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
                        <li><a href="QuotationFileForm.html?id=${customerFile.id}"><span>Quotation File</span></a></li>
        </sec-auth:authComponent>
  </c:if>
  </ul>
  </div> --%>

<%-- <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float:left;">
    <tr>
        <c:if test="${not empty soObj.id}">
            <c:if test="${soQuickView=='Y'}">
                <td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
                    <a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${soObj.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
                </td>
            </c:if>
        </c:if>
    </tr>
</table> --%>






<s:form id="weeklyXMLForm" action="" method="post" validate="true"> 
<div style="margin-left:12px;" id="pnav">
    <ul>
    <li><a><span>Task Check List</span></a></li>
    </ul>
    </div>
    <div style="width:50%;margin-left:140px;" class=""><img src="<c:url value='/images/key_tasklist.png'/>" ></div>
<div class="spn">&nbsp;</div>
<s:set name="checkListForTasks" value="checkListForTasks" scope="request"/>
<c:if test="${not empty checkListForTasks}">
<display:table name="checkListForTasks" class="table-price" requestURI="" id="checkListForTasks"  style="width:100%;margin-top:1px;margin-bottom:0px;" >
 <c:if test="${checkListForTasks.type eq 'taskValue'}">
 <display:column  title="Type" style="width:5px;text-align:center;">
 <img  id="" src="<c:url value='/images/task.png'/>" />
 </display:column>

 <display:column property="details" title="Details" maxLength="85" style="width:500px;"></display:column>
 <display:column property="date"  title="Date" style="width:70px;" />
 <display:column property="user" title="User"></display:column>
 
 <display:column property="completedOn" title="Completed&nbsp;On" />
 <display:column property="completedBy" title="Completed&nbsp;By" />
 <%-- <display:column title="Document" style="width:50px;">NA</display:column> --%>
  <c:choose>
  <c:when test="${not empty checkListForTasks.myfileId}">
     <display:column title="Document" style="width:50px;">
     <img  id="" src="<c:url value='/images/notes-activity.png'/>" onmouseover="" onmouseout="" onclick="javascript:openWindow('ImageServletAction.html?id=${checkListForTasks.myfileId}&decorator=popup&popup=true',900,600);"/>
     </display:column>
  </c:when>
  <c:otherwise>
  <display:column title="Document" style="width:50px;">NA</display:column>
  </c:otherwise>
  </c:choose> 
 
  <display:column  title="Transfer User" style="width:80px;"></display:column>
  <display:column  title="Transfer Date" style="width:80px;"></display:column>

  <c:choose>
  <c:when test="${checkListForTasks.date ne 'NA'}">
  <display:column style="text-align:center;">
  <img  id="" src="<c:url value='/images/tick-circle-frame.png'/>" onmouseover="" onmouseout="" onclick=""/>
  </display:column>
  </c:when>
  <c:otherwise>
  <display:column style="text-align:center;">
  <img  id="" src="<c:url value='/images/forecast.png'/>" onmouseover="" onmouseout="" onclick=""/>
  </display:column>
  </c:otherwise>
  </c:choose> 

 </c:if>
 
 
 <c:if test="${checkListForTasks.type ne 'taskValue'}">
  <display:column  title="Type" style="width:5px;text-align:center;"  url="/editNewNoteForServiceOrder.html?from=list&id1=${setNotesKeyId}&notesId=${notesId}&noteFor=" paramId="id" paramProperty="id">
 <c:choose>
     <c:when test="${checkListForTasks.forwardDate ne 'NA'}">
  <img  id="" src="<c:url value='/images/followup.png'/>" onmouseover="" onmouseout="" onclick="javascript:openWindow('editNewNoteForServiceOrder.html?id=${checkListForTasks.id}&id1=${checkListForTasks.notesKeyId}&notesId=${checkListForTasks.customerNumber}&noteFor=&decorator=popup&popup=true',950,400);"/>
  </c:when>
   <c:otherwise>
  <img  id="" src="<c:url value='/images/notes_round.png'/>" onmouseover="" onmouseout="" onclick=""/>
  </c:otherwise>
  </c:choose>
 </display:column>
 
 <display:column property="details" title="Details" maxLength="85"  style="width:500px;"></display:column>
 <display:column title="Date" style="width:70px;" >${checkListForTasks.forwardDate}</display:column>
 <display:column  title="User">${checkListForTasks.updatedBy}</display:column>

    <c:choose>
 <c:when test="${checkListForTasks.noteStatus eq 'CMP'}">
 <display:column title="Completed&nbsp;On" > ${checkListForTasks.updatedOn}</display:column>
 <display:column  title="Completed&nbsp;By" > ${checkListForTasks.updatedBy}</display:column>
 </c:when>
 <c:otherwise>
 <display:column title="Completed&nbsp;On" />
 <display:column title="Completed&nbsp;By" />
</c:otherwise>
</c:choose>
 <display:column title="Document" style="width:50px;">NA</display:column>
 <display:column  title="Transfer User" style="width:80px;"></display:column>
 <display:column  title="Transfer Date" style="width:80px;"></display:column> 
 
  
    <c:if test="${checkListForTasks.forwardDate eq 'NA'}">
    <display:column>
    <img  id="" src="<c:url value='/images/notes_round.png'/>" onmouseover="" onmouseout="" onclick=""/>
    </display:column>
    </c:if>
    <c:if test="${checkListForTasks.forwardDate ne 'NA'}">
  <c:choose>
  <c:when test="${checkListForTasks.noteStatus eq 'CMP'}">
  <display:column style="text-align:center;">
  <img  id="" src="<c:url value='/images/tick-circle-frame.png'/>" onmouseover="" onmouseout="" onclick=""/>
  </display:column>
  </c:when>
  <c:otherwise>
  <display:column style="text-align:center;" >
  <img  id="" src="<c:url value='/images/stop_round.png'/>" onmouseover="" onmouseout="" onclick=""/>
  </display:column>
  </c:otherwise>
  </c:choose>
 </c:if>
 
 </c:if>
</display:table>
</c:if>

<c:if test="${checkListForTasks =='[]'}">
<display:table name="checkListForTasks" class="table-price" requestURI="" id="checkListForTasks"  style="width:100%;margin-top:1px;margin-bottom:0px;" >
 <display:column  title="Type" style="width:5px;" />
 <display:column  title="Details" style="width:500px;"></display:column>
  <display:column title="Date" style="width:70px;"></display:column>
  <display:column title="User"></display:column>
   <display:column  title="Completed&nbsp;On" format="{0,date,dd-MMM-YYYY}"></display:column>
  <display:column  title="Completed&nbsp;By" ></display:column>
   <display:column title="Document" style="width:50px;" />
   
   <display:column  title="Transfer User" style="width:80px;"></display:column>
  <display:column  title="Transfer Date" style="width:80px;"></display:column>
</display:table>
</c:if>
<div style="margin-top:10px;">
<input type="button" class="pricelistbtn"  style="width:55px;" value="Add"
           onclick="javascript:openWindow('editNewNoteForServiceOrder.html?id=${id}&notesId=${shipnumber}&noteFor=ServiceOrder&subType=ServiceOrder&decorator=popup&popup=true',800,600);"/>
 </div>         
</s:form>
