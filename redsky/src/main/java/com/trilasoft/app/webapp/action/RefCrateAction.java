package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.RefCrate;
import com.trilasoft.app.service.RefCrateManager;

public class RefCrateAction extends BaseAction implements Preparable {

	private RefCrateManager refCrateManager;

	private List refCrates;

	private RefCrate refCrate;

	private Long id;

	private List ls;
	private  List weightunits;
    private  List volumeunits;
    private  List lengthunits;
   
	private String sessionCorpID;

	// private List day;

	public RefCrateAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
    	getComboList();
		}
	public void setRefCrateManager(RefCrateManager refCrateManager) {
		this.refCrateManager = refCrateManager;
	}

	public List getRefCrates() {
		return refCrates;
	}

	public String list() {
		refCrates = refCrateManager.refCrateList(sessionCorpID);
		return SUCCESS;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public RefCrate getRefCrate() {
		return refCrate;
	}

	public void setRefCrate(RefCrate refCrate) {
		this.refCrate = refCrate;
	}

	public String delete() {
		refCrateManager.remove(refCrate.getId());
		saveMessage(getText("refCrate.deleted"));

		return SUCCESS;
	}

	public String getComboList() {
    	weightunits = new ArrayList();
      	weightunits.add("Lbs");
      	weightunits.add("Kgs"); 
      	volumeunits = new ArrayList();
      	volumeunits.add("Cft");
      	volumeunits.add("Cbm");  
      	lengthunits=new ArrayList();
      	lengthunits.add("Inches");
      	lengthunits.add("Ft");
      	lengthunits.add("Cm");
      	lengthunits.add("Mtr");
      	return SUCCESS;
    	
    }
	public String edit() {
		//getComboList();
		if (id != null) {
			refCrate = refCrateManager.get(id);
		} else {
			refCrate = new RefCrate();
			refCrate.setUnit1("Lbs");
			refCrate.setUnit2("Ft");
			refCrate.setUnit3("Cft");
			refCrate.setUpdatedOn(new Date());
			refCrate.setCreatedOn(new Date());
			refCrate.setLength(new BigDecimal(0));
			refCrate.setWidth(new BigDecimal(0));
			refCrate.setHeight(new BigDecimal(0));
			refCrate.setEmptyContWeight(new BigDecimal(0));
			refCrate.setVolume(new BigDecimal(0));
		}

		return SUCCESS;
	}

	public String save() throws Exception {
		if (cancel != null) {
			return "cancel";
		}

		if (delete != null) {
			return delete();
		}
        
		boolean isNew = (refCrate.getId() == null);
		refCrate.setUpdatedOn(new Date());
        refCrate.setUpdatedBy(getRequest().getRemoteUser());
        if(isNew){
        	refCrate.setCreatedOn(new Date());
        }
		refCrate.setCorpID(sessionCorpID);
		refCrateManager.save(refCrate);

		String key = (isNew) ? "refCrate.added" : "refCrate.updated";
		saveMessage(getText(key));

		if (!isNew) {
			return INPUT;
		} else {
			return SUCCESS;

		}
	}

	/**
	 * @return the weightunits
	 */
	public List getWeightunits() {
		return weightunits;
	}
	
	/**
	 * @return the volumeunits
	 */
	public List getVolumeunits() {
		return volumeunits;
	}

	/**
	 * @return the lengthunits
	 */
	public List getLengthunits() {
		return lengthunits;
	}
}
