package com.trilasoft.app.service;
import com.trilasoft.app.model.ProposalManagement;
import org.appfuse.service.GenericManager;

import java.util.Date;
import java.util.List;


	public interface ProposalManagementManager extends GenericManager<ProposalManagement, Long> {
		  public List findDistinct();
		  public List findListByModuleCorpStatus(String module,String corpid,String status ,Date approvalDate , Date initiationDate,String ticketNo,Boolean activeStatus);
		  public String findProposalDescForToolTip(Long id, String sessionCorpID);
		  public void autoSaveProposalManagementAjax(String fieldName,String fieldVal,Long serviceId);
		  public List listOnLoad();
	   }

