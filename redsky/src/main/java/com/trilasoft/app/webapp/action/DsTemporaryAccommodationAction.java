package com.trilasoft.app.webapp.action;
import java.util.*;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsTemporaryAccommodation;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsTemporaryAccommodationManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
public class DsTemporaryAccommodationAction extends BaseAction implements Preparable {
	

	private Long id;
	private String sessionCorpID;
	private  DsTemporaryAccommodation dsTemporaryAccommodation;
	private DsTemporaryAccommodationManager dsTemporaryAccommodationManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;	
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDSTemporaryAccommodationNotes;	
	private NotesManager notesManager;
    private Map<String, String> currency;
    private RefMasterManager refMasterManager;
    private String gotoPageString;
	private String validateFormNav;

	
	public void prepare() throws Exception { 
		
	}
	
	
	public DsTemporaryAccommodationAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;

	
    
   	public String edit() { 
		if (id != null) {
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsTemporaryAccommodation = dsTemporaryAccommodationManager.get(id);
		} else {
			dsTemporaryAccommodation = new DsTemporaryAccommodation(); 
			dsTemporaryAccommodation.setCreatedOn(new Date());
			dsTemporaryAccommodation.setUpdatedOn(new Date());
			dsTemporaryAccommodation.setServiceOrderId(serviceOrderId);
		} 
		currency = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		
		getNotesForIconChange();
		return SUCCESS;
	}
   	
	
	
	public String save() throws Exception {
		
		boolean isNew = (dsTemporaryAccommodation.getId() == null);  
				if (isNew) { 
					dsTemporaryAccommodation.setCreatedOn(new Date());
		        }
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
		    	dsTemporaryAccommodation.setCorpID(sessionCorpID);
		    	dsTemporaryAccommodation.setUpdatedOn(new Date());
		    	dsTemporaryAccommodation.setUpdatedBy(getRequest().getRemoteUser()); 
		    	dsTemporaryAccommodationManager.save(dsTemporaryAccommodation);  
		    	if(!(validateFormNav=="OK"))
		    	{			    
		    	String key = (isNew) ? "dsTemporaryAccommodation.added" : "dsTemporaryAccommodation.updated";

			    saveMessage(getText(key));
		    	}
			    currency = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
			    getNotesForIconChange();
				return SUCCESS;
				
			
	}
	@SkipValidation
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
		String s = save();
		return SUCCESS;
	}

	/**
	 * @return the gotoPageString
	 */
	public String getGotoPageString() {
		return gotoPageString;
	}


	/**
	 * @param gotoPageString the gotoPageString to set
	 */
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}


	/**
	 * @return the validateFormNav
	 */
	public String getValidateFormNav() {
		return validateFormNav;
	}


	/**
	 * @param validateFormNav the validateFormNav to set
	 */
	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}


	@SkipValidation
	public String getNotesForIconChange() {
		List noteDS = notesManager.countForDSTemporaryAccommodationNotes(serviceOrder.getShipNumber());
		
		if (noteDS.isEmpty()) {
			countDSTemporaryAccommodationNotes = "0";
		} else {
			countDSTemporaryAccommodationNotes = ((noteDS).get(0)).toString();
		}
		currency = refMasterManager.findCodeOnleByParameter(sessionCorpID, "CURRENCY");
		return SUCCESS;
	}


	/**
	 * @return the countDSTemporaryAccommodationNotes
	 */
	public String getCountDSTemporaryAccommodationNotes() {
		return countDSTemporaryAccommodationNotes;
	}


	/**
	 * @param countDSTemporaryAccommodationNotes the countDSTemporaryAccommodationNotes to set
	 */
	public void setCountDSTemporaryAccommodationNotes(
			String countDSTemporaryAccommodationNotes) {
		this.countDSTemporaryAccommodationNotes = countDSTemporaryAccommodationNotes;
	}


	/**
	 * @return the customerFile
	 */
	public CustomerFile getCustomerFile() {
		return customerFile;
	}


	/**
	 * @param customerFile the customerFile to set
	 */
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}


	/**
	 * @return the dsTemporaryAccommodation
	 */
	public DsTemporaryAccommodation getDsTemporaryAccommodation() {
		return dsTemporaryAccommodation;
	}


	/**
	 * @param dsTemporaryAccommodation the dsTemporaryAccommodation to set
	 */
	public void setDsTemporaryAccommodation(
			DsTemporaryAccommodation dsTemporaryAccommodation) {
		this.dsTemporaryAccommodation = dsTemporaryAccommodation;
	}


	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}


	/**
	 * @return the miscellaneous
	 */
	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}
	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setDsTemporaryAccommodationManager(
			DsTemporaryAccommodationManager dsTemporaryAccommodationManager) {
		this.dsTemporaryAccommodationManager = dsTemporaryAccommodationManager;
	}
	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
	public Map<String, String> getCurrency() {
		return currency;
	}
	public void setCurrency(Map<String, String> currency) {
		this.currency = currency;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}


	/**
	 * @return the countShip
	 */
	public String getCountShip() {
		return countShip;
	}


	/**
	 * @param countShip the countShip to set
	 */
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}


	/**
	 * @return the minShip
	 */
	public String getMinShip() {
		return minShip;
	}


	/**
	 * @param minShip the minShip to set
	 */
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}


	/**
	 * @return the relocationServices
	 */
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}


	/**
	 * @param relocationServices the relocationServices to set
	 */
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}


	/**
	 * @return the shipSize
	 */
	public String getShipSize() {
		return shipSize;
	}


	/**
	 * @param shipSize the shipSize to set
	 */
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}


	

}
