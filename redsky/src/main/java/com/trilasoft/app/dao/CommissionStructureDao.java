package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.CommissionStructure;

public interface CommissionStructureDao extends GenericDao<CommissionStructure, Long>{
public List getCommPersentAcRange(String corpId,String contract,String chargeCode);
}
