package com.trilasoft.app.webapp.action;



import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.apache.commons.lang.time.DateUtils;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;

import org.appfuse.Constants;
import org.appfuse.model.Address;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserExistsException;
import org.appfuse.service.UserManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;


public class AgentPortalUserSearchAction extends BaseAction implements Preparable {
	
    private List agentRequestSearchList = new ArrayList();


	static final Logger logger = Logger.getLogger(AgentPortalUserSearchAction.class);	
	private String partnerCode;
    private String sessionCorpID;
	private String userName;
	private String userEmailId;
	private String phone_number;
	private String usertype;
	private UserManager userManager;
	private User user;
	private String id;
	public UserManager getUserManager() {
		return userManager;
	}
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	  private String status;
	public String searchAgentPortal()
	{
		return SUCCESS;
	}
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		
	}
	public AgentPortalUserSearchAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		this.usertype = user.getUserType();
	}

	public String searchAgentPortalUser(){

		try {
			agentRequestSearchList = userManager.getagentRequestList(partnerCode, sessionCorpID,userName,userEmailId,phone_number,status);
		} catch (Exception e) {
			 e.printStackTrace();
			logger.error("Exception Occour: "+ e.getStackTrace()[0]);
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  
	    	 return "errorlog";
		}
		return SUCCESS;
	}
	 private String userStatus;
	 
     private DataSecuritySet dataSecuritySet;
	 private List availRoles;
	 private String partnerType;
	 private Map<String, String> userJobTypeMap;
	 private PartnerManager partnerManager;

	 private List availPermissions;

     private DataSecuritySetManager dataSecuritySetManager;
     private  Map<String, List> childAgentCodeMap=new LinkedHashMap<String, List> ();
     private List<SystemDefault> sysDefaultDetail;
 	private CustomerFileManager customerFileManager;

    private String fromEmail;
	private Boolean checkTransfereeInfopackage;
	private String emailTo;
	private Company company;
    private EmailSetupManager  emailSetupManager;
	private String mailStatus;
	private String mailFailure; 
	private String confirmPasswordNew;
	private CompanyManager companyManager;
	private DataSecurityFilter dataSecurityFilter;
   private DataSecurityFilterManager dataSecurityFilterManager;
	private PartnerPublicManager partnerPublicManager;
	Date currentdate = new Date();

    public String updatedUserData() throws Exception  {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  try {
		user = userManager.getUser(id);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  partnerType="AG";
	  if(userStatus.equals("Approved"))
	  {
		  String pcode=user.getParentAgent();
			//company = companyManager.findByCorpID(sessionCorpID).get(0);
			availRoles = userManager.findUserRoles(user.getCorpID());
			childAgentCodeMap = dataSecuritySetManager.getChildAgentCodeMap();	
			List associatedList = new ArrayList();
			String prefixDataSet = "";
			availPermissions=new ArrayList();
			if(!partnerType.equalsIgnoreCase("VN")){

				if(partnerType.equalsIgnoreCase("AC")){
					prefixDataSet = "DATA_SECURITY_SET_SO_BILLTOCODE_" + pcode;
				}else{
					prefixDataSet = "DATA_SECURITY_SET_AGENT_" + pcode;
				}
				List dataset = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, user.getCorpID());
				if (!dataset.isEmpty()) {
					availPermissions.add(prefixDataSet);
				}
				else
				{

					try {
						String prefix = "DATA_SECURITY_FILTER_AGENT_" + user.getParentAgent();
						 prefixDataSet = "DATA_SECURITY_SET_AGENT_" + user.getParentAgent();
						List agentFilterList = dataSecurityFilterManager.getAgentFilterList(prefix, sessionCorpID);
						List agentFilterSetList = dataSecuritySetManager.getDataSecuritySet(prefixDataSet, sessionCorpID);
						if ((agentFilterList ==null || agentFilterList.isEmpty()) && (agentFilterSetList == null || agentFilterSetList.isEmpty())) {
							List nameList = new ArrayList();
							nameList.add("BOOKINGAGENTCODE");
							nameList.add("BROKERCODE");
							nameList.add("ORIGINAGENTCODE");
							nameList.add("ORIGINSUBAGENTCODE");
							nameList.add("DESTINATIONAGENTCODE");
							nameList.add("DESTINATIONSUBAGENTCODE");
							dataSecuritySet = new DataSecuritySet();
							Iterator it = nameList.iterator();
							while (it.hasNext()) {
								String fieldName = it.next().toString();
								String postFix = prefix + "_" + fieldName;
								dataSecurityFilter = new DataSecurityFilter();
								dataSecurityFilter.setName(postFix);
								dataSecurityFilter.setTableName("serviceorder");
								dataSecurityFilter.setFieldName(fieldName.toLowerCase());
								dataSecurityFilter.setFilterValues(user.getParentAgent().toString());
								dataSecurityFilter.setCorpID(sessionCorpID);
								dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
								dataSecuritySet.addFilters(dataSecurityFilter);
							}
							dataSecuritySet.setName(prefixDataSet);
		
							dataSecuritySet.setDescription(prefixDataSet);
							dataSecuritySet.setCorpID(sessionCorpID);
							dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
						}
							associatedList.add(user.getParentAgent());
							Iterator it2 = associatedList.iterator();
							while (it2.hasNext()) {
								String pcode1 = it2.next().toString().trim();
								if (!pcode.equalsIgnoreCase("")) {
									String prefix1 = "DATA_SECURITY_FILTER_AGENT_" + pcode1;
									String prefixDataSet1 = "DATA_SECURITY_SET_AGENT_" + pcode1;
									List agentFilterList1 = dataSecurityFilterManager.getAgentFilterList(prefix1, sessionCorpID);
									List agentFilterSetList1 = dataSecuritySetManager.getDataSecuritySet(prefixDataSet1, sessionCorpID);
									if ((agentFilterList1  ==null || agentFilterList1.isEmpty()) && (agentFilterSetList1 ==null || agentFilterSetList1.isEmpty())) {
										List nameList = new ArrayList();
										nameList.add("BOOKINGAGENTCODE");
										nameList.add("BROKERCODE");
										nameList.add("ORIGINAGENTCODE");
										nameList.add("ORIGINSUBAGENTCODE");
										nameList.add("DESTINATIONAGENTCODE");
										nameList.add("DESTINATIONSUBAGENTCODE");
										dataSecuritySet = new DataSecuritySet();
										Iterator it3 = nameList.iterator();
										while (it3.hasNext()) {
											String fieldName = it3.next().toString();
											String postFix = prefix1 + "_" + fieldName;
											dataSecurityFilter = new DataSecurityFilter();
											dataSecurityFilter.setName(postFix);
											dataSecurityFilter.setTableName("serviceorder");
											dataSecurityFilter.setFieldName(fieldName.toLowerCase());
											dataSecurityFilter.setFilterValues(pcode1);
											dataSecurityFilter.setCorpID(sessionCorpID);
											dataSecurityFilter = dataSecurityFilterManager.save(dataSecurityFilter);
											dataSecuritySet.addFilters(dataSecurityFilter);
										}
										dataSecuritySet.setName(prefixDataSet1);
										dataSecuritySet.setDescription(prefixDataSet1);
										dataSecuritySet.setCorpID(sessionCorpID);
										dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet);
									}
								}

							}
							availPermissions.add(prefixDataSet);
						partnerPublicManager.updatePartnerPortal(user.getParentAgent());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				
				}
			
			}
			List notesSet = new ArrayList();
			if(partnerType.equalsIgnoreCase("AC")){
				notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS", sessionCorpID);
				if (!notesSet.isEmpty()) {
					availPermissions.add("DATA_SECURITY_SET_NOTES_ACCOUNTPORTAL_AGENTS");
				}
			}else{
				notesSet = dataSecuritySetManager.getDataSecuritySet("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT", sessionCorpID);
				if (!notesSet.isEmpty() && (notesSet.get(0)!=null)) {
					availPermissions.add("DATA_SECURITY_SET_NOTES_PARTNERPORTAL_AGENT");
				}
			}
			StringBuffer sb1=new StringBuffer();
			userJobTypeMap=new LinkedHashMap<String, String>() ;
			boolean isNew = (user.getId() == null);
			user.setEnabled(true);

			//user.setCorpID("TSFT");
					
					try{	            
			            Iterator it2=availPermissions.iterator();
				            while(it2.hasNext()){
				            	String filterSet=(String)it2.next();
				            	user.addPermissions((DataSecuritySet)userManager.getPermission(filterSet).get(0));
				            	//user.addPermissions((DataSecuritySet)userManager.getPermission(filterSet).get(1));
				            }	            
			            }catch(Exception ex){
			            	ex.printStackTrace();
			            }	
			
			user.setUpdatedOn(new Date());
			user.setUpdatedBy(getRequest().getRemoteUser());
		    user.setParentAgent(user.getParentAgent());

		    userManager.saveUser(user);
			
			  company = companyManager.findByCorpID(user.getCorpID()).get(0);
			
			  fromEmail= "support@redskymobility.com";	
			 String str=new  String("QAa0bcLdUK2eHfJgTP8XhiFj61DOklNm9nBoI5pGqYVrs3CtSuMZvwWx4yE7zR");
		      StringBuffer sb=new StringBuffer();
		      Random r = new Random();
		      int te=0;
		      for(int i=1;i<=8;i++){
		         te=r.nextInt(62);
		         sb.append(str.charAt(te));
		      } 
			  String toAddress = user.getEmail();
			  String subject = "Redsky agent portal login credentials";
			  String msgText="<html><head></head><body>";
			  msgText = msgText + " Dear "+user.getFirstName()+",<br><br>"+"Please find below the login credentials for Redsky agent portal"+"<br>"+"<b style='color:black;'>URL: </b><a href='http://skyrelo.com/redsky/login.jsp'>http://skyrelo.com/redsky/login.jsp</a>"+"<br>"+"<b style='color:black;'>"+"Username: "+"</b>"+user.getUsername()+"<br>";			  
			  msgText = msgText + " <b style='color:black;'>"+"Password: "+"</b>"+ sb.toString() +"<br><br>"+"On your first login you will be prompted to change password screen."+"<br>";
			  msgText = msgText + " Please verify your profile details and update it accordingly."+"<br><br>";
			  msgText = msgText + " For any queries, please send a mail to <a href='mailto:support@redskymobility.com'>support@redskymobility.com</a>. ";
			  msgText= msgText + "<br><br>"+"Best Regards," +"<br>"+"Redsky Team";
			  msgText= msgText + "</body></html>";
			  try {
				String from =fromEmail;
				
				String tempRecipient="";
				String tempRecipientArr[]=toAddress.split(",");
				for(String str1:tempRecipientArr){
					if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
						if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
						tempRecipient += str1;
					}
				}
				toAddress=tempRecipient;
				String location="";
		  		String pdf1 = "/usr/local/redskydoc/agentUserData/redskyData/RedSky Agent Portal User Manual v1.6 2020.pdf";
		  		
		  		location = pdf1;
		  		emailSetupManager.globalEmailSetupProcess(from, toAddress, user.getEmail(), "", location, msgText, subject, user.getCorpID(),"",user.getUsername(),"");
				mailStatus = "sent";
				String algorithm = (String)	getConfiguration().get(Constants.ENC_ALGORITHM);
				if (algorithm == null) { 
					  log.debug("assuming testcase, setting algorithm to 'SHA'");
					  algorithm = "SHA"; 
				}
				String passwordNew = StringUtil.encodePassword(sb.toString(),algorithm);
		    	confirmPasswordNew=passwordNew;
		    	Date compareWithDate=null;
				if(company.getPartnerPasswordExpiryDuration()!=null){
					compareWithDate = DateUtils.addDays(getCurrentDate(), Integer.parseInt(company.getPartnerPasswordExpiryDuration().toString()));	
				}else{
					compareWithDate=DateUtils.addDays(getCurrentDate(), (60));
				} 
		    	customerFileManager.resetPassword(passwordNew, confirmPasswordNew, user.getUsername(),compareWithDate);
			  }catch (Exception mex){
				mex.printStackTrace();
				mailStatus = "notSent";
				mailFailure=mex.toString(); 
			}		  
		    status="Approved";
			//userId ="editUser.html?from=list&id="+id;
			
		    //System.out.println(userId);
		    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return "success"; 
	  }
	  else
	  {     user = userManager.getUser(id);
		     user.setUpdatedOn(new Date());
		     user.setUpdatedBy(getRequest().getRemoteUser());
		     userManager.saveUser(user);
		     status="Rejected";
		     saveMessage("Request for "+user.getUsername()+" Has been Rejected Successfully");
		     return "rejected"; 
			  
		  }
	  
    	
    	}

    private java.util.Date getCurrentDate() {
			DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = dfm.format(new Date());
			Date currentDate = null;
			try {
				currentDate = dfm.parse(dateStr);
				
			} catch (java.text.ParseException e) {
				
				e.printStackTrace();
			}			
			return currentDate;
			
		}
  private String userId;
	private List vieUserList = new ArrayList();
	    public String viewUserDetail() { 
			String email;
			  user = userManager.getUser(id);
			 if (!(user == null)) { 
				
				if ((user.getEmail() == null)) {
					email = "";
				}
				else
				{
					
					email = user.getEmail();
				}
				
				vieUserList=userManager.getUserRequestDetailList(email);
			  } 
	    	return SUCCESS;   
	    }

    public List getAgentRequestSearchList() {
		return agentRequestSearchList;
	}
	public void setAgentRequestSearchList(List agentRequestSearchList) {
		this.agentRequestSearchList = agentRequestSearchList;
	}



	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmailId() {
		return userEmailId;
	}

	public void setUserEmailId(String userEmailId) {
		this.userEmailId = userEmailId;
	}

	public String getPhone_number() {
		return phone_number;
	}

	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
	
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	public List getVieUserList() {
		return vieUserList;
	}
	public void setVieUserList(List vieUserList) {
		this.vieUserList = vieUserList;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public List getAvailRoles() {
		return availRoles;
	}
	public void setAvailRoles(List availRoles) {
		this.availRoles = availRoles;
	}
	public void setDataSecuritySetManager(
			DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}
	public Map<String, List> getChildAgentCodeMap() {
		return childAgentCodeMap;
	}
	public void setChildAgentCodeMap(Map<String, List> childAgentCodeMap) {
		this.childAgentCodeMap = childAgentCodeMap;
	}
	public List getAvailPermissions() {
		return availPermissions;
	}
	public void setAvailPermissions(List availPermissions) {
		this.availPermissions = availPermissions;
	}
	public String getPartnerType() {
		return partnerType;
	}
	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}
	public Map<String, String> getUserJobTypeMap() {
		return userJobTypeMap;
	}
	public void setUserJobTypeMap(Map<String, String> userJobTypeMap) {
		this.userJobTypeMap = userJobTypeMap;
	}
	
	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}
	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}
	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}
	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public String getFromEmail() {
		return fromEmail;
	}
	public void setFromEmail(String fromEmail) {
		this.fromEmail = fromEmail;
	}
	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}
	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}
	public String getEmailTo() {
		return emailTo;
	}
	public void setEmailTo(String emailTo) {
		this.emailTo = emailTo;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	public String getMailStatus() {
		return mailStatus;
	}
	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}
	public String getMailFailure() {
		return mailFailure;
	}
	public void setMailFailure(String mailFailure) {
		this.mailFailure = mailFailure;
	}
	public String getConfirmPasswordNew() {
		return confirmPasswordNew;
	}
	public void setConfirmPasswordNew(String confirmPasswordNew) {
		this.confirmPasswordNew = confirmPasswordNew;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}
	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}
	public DataSecurityFilterManager getDataSecurityFilterManager() {
		return dataSecurityFilterManager;
	}
	public void setDataSecurityFilterManager(DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}
	
	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}
}
