package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.EmailSetupTemplateDao;
import com.trilasoft.app.model.EmailSetupTemplate;
import com.trilasoft.app.service.EmailSetupTemplateManager;


public class EmailSetupTemplateManagerImpl extends GenericManagerImpl<EmailSetupTemplate, Long> implements EmailSetupTemplateManager{

	EmailSetupTemplateDao emailSetupTemplateDao; 
	
	public EmailSetupTemplateManagerImpl(EmailSetupTemplateDao emailSetupTemplateDao) {   
        super(emailSetupTemplateDao);   
        this.emailSetupTemplateDao = emailSetupTemplateDao;   
    }
	
	public List getListByCorpId(String corpId){
		return emailSetupTemplateDao.getListByCorpId(corpId);
	}
	public void updateFormsIdInEmailSetupTemplate(String reportsId,long id){
		emailSetupTemplateDao.updateFormsIdInEmailSetupTemplate(reportsId,id);
	}
	public Map<String,String> getTableWithColumnsNameMap(){
		return emailSetupTemplateDao.getTableWithColumnsNameMap();
	}
	public List getListByModuleName(String sessionCorpID,String moduleName,String job,String language,String moveType, String companyDivision,String mode,String routing,String serviceType,String fileNumber,Long id){
		return emailSetupTemplateDao.getListByModuleName(sessionCorpID, moduleName, job, language, moveType, companyDivision, mode, routing, serviceType, fileNumber, id);
	}
	public int getMaxOrderNumber(String sessionCorpID){
		return emailSetupTemplateDao.getMaxOrderNumber(sessionCorpID);
	}
	public String findDocsFromFileCabinet(String fileType, String fileId,String sessionCorpID){
		return emailSetupTemplateDao.findDocsFromFileCabinet(fileType,fileId,sessionCorpID);
	}
	public String testEmailSetupTestConditionQuery(String moduleFieldValue,String othersFieldValue,String sessionCorpID){
		return emailSetupTemplateDao.testEmailSetupTestConditionQuery(moduleFieldValue,othersFieldValue,sessionCorpID);
	}
	public List getListBySearch(String moduleName,String languageName,String descriptionName,String corpId){
		return emailSetupTemplateDao.getListBySearch(moduleName,languageName, descriptionName,corpId);
	}
	public List findLastSentMail(String sessionCorpID,String moduleName,String fileNumber,String saveAsVal){
		return emailSetupTemplateDao.findLastSentMail(sessionCorpID,moduleName, fileNumber,saveAsVal);
	}
	public List findAllDocsFromFileCabinet(String fileType,String fileId,String sessionCorpID){
		return emailSetupTemplateDao.findAllDocsFromFileCabinet(fileType,fileId,sessionCorpID);
	}
}
