package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Sharing;

public interface SharingDao extends GenericDao<Sharing, Long>{

	
	public List findById(Long id);
	public List findMaximumId(); 
	public List<Sharing> searchSharing(String code, String discription);
}
