<%@ include file="/common/taglibs.jsp"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="java.util.List"%>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<%@page import="java.util.Iterator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="heading" content="<fmt:message key='toDoRule.heading'/>"/> 
<title>ToDo Rule</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1; <fmt:message key='toDoRule.heading'/>">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/comman.css'/>" />
  <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/saverules.css'/>" />
  <style>
 
  <%@ include file="/styles/comman.css"%> 
    <%@ include file="/styles/saverules.css"%>
         </style>
        
         
         <style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		/* .modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}	 */	
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		/* .modal-header .close {margin-right:5px;margin-top: -2px; text-align: right; padding:0px;} */
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xmll;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-860
linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		

		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid transparent; min-height: 20.43px; padding: 10px; }		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
 	.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;} 
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		
	</style>
         
</head>
<body>
<s:form id="toDoRule" name="toDoRule" action="savetodoRules.html" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden name="toDoRule.corpID"/>
<s:hidden name="btn"/>
<s:hidden name="formStatus" value=""/>
<s:hidden name="toDoRule.id"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="status" />
<s:hidden id="tempField" name="toDoRule.tempField"/>
<s:hidden name="toDoRule.messagedisplayed"/>
<s:hidden name="toDoRule.rolelist"/>
<s:hidden name="toDoRule.noteType"/>
<s:hidden name="toDoRule.noteSubType"/>
<s:hidden name="toDoRule.manualEmail"/>
<s:hidden name="toDoRule.ruleNumber"/>
<s:hidden name="activeStatus"/>
<c:if test="${not empty toDoRule.emailOn}">
    <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="toDoRule.emailOn" /></s:text>
	<s:hidden  name="toDoRule.emailOn" value="%{customerFileSurveyFormattedValue}" /> 
</c:if>
<c:if test="${empty toDoRule.emailOn}">
	<s:hidden   name="toDoRule.emailOn"/> 
</c:if>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.ruleList' }">
    <c:redirect url="/toDoRuleLists.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
</c:if>

<s:hidden name="actionList" value="${actionList}"/>

<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF"><a  class="current"><span>ToDo Rule<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="toDoRuleLists.html"><span>Rule List</span></a></li>
		<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${toDoRule.id}&tableName=todorule&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
  </ul>
		</div><div class="spn">&nbsp;</div>
<div class="row ml-0 mr-0 to-dolist">    
                              <div class="col-md-12">                              
                                    <div class="card rules border-b">
                                             <div class="card-header clearfix">
                                                   <h6 class="float-left mt-1">Rule </h6>
                                                   
                                                   <div class="btn-group float-right mr-2">
                                                    <button type="button" class="btn  btn-sm dropdown-toggle pr-0 " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                     <i class="fa fa-ellipsis-v" aria-hidden="true"></i>
                                                    </button>
                                                    <div class="dropdown-menu">
                                                    <!--  <a class="dropdown-item" onclick="copyToTable();">Copy</a> -->
                                                      
                                                      <s:submit cssClass="dropdown-item"  type="button" value="Test Condition"  onclick="return btntype(this);" id="btnSave" />  
                                                      <div class="dropdown-divider"></div>
                                                       <s:submit cssClass="dropdown-item"  type="button" value="Execute This Rule"  onclick="return btntype(this);" id="btnSave" />  
                                                     
                                                    
                                                     
                                                      
                                                      
                                                     
                                                    </div>
                                                  </div>
                                             </div>
                                             <div class="card-body row">
                                                
                                                     <div class="col-md-12 col-xl-7">                                                       
                                                                                                          <div class="row">
                                                                  <div class="col-md-3">
                                                                      <label class="label-heading mt-2">Category<span class="validate-icon">*</span></label>
                                                                  </div>

                                                                  <div class="col-md-3">
                                                                   <div class="form-group form-md-line-input">
                                                                      <s:select cssClass="list-menu form-control" id="entity" name="toDoRule.entitytablerequired" list="%{pentity}" headerKey="" headerValue=""  onchange="getFieldList(this);getEmailList(this);"/>
                                                                      </div>
                                                                  </div>
                                                                  <div class="col-md-3 pl-0">
                                                                  <label class="mt-2 rule_no"> Rule# ${toDoRule.ruleNumber}</label>
                                                                  </div>
                                                                  

                                                                  <div class="col-md-2">
                                                                      <label class="label-heading mt-2 ">Enable</label>
                                                                  </div>
                                                                  <c:set var="ischecked" value="false"/>
				                                                  <c:if test="${toDoRule.checkEnable}">
				                                                  <c:set var="ischecked" value="true"/>
				                                                  </c:if>
                                                                  <div class="col-md-1 text-center">
                                                              
                                                                      <s:checkbox key="toDoRule.checkEnable" cssClass="checkbx-rule" value="${ischecked}" fieldValue="true" onclick="changeStatus();"/>                       
                                                                       </div>
                                                             </div>

                                                             <div class="row">
                                                              <div class="col-md-3">
                                                                <label class="label-heading mt-2">Control Date<span class="validate-icon">*</span>	</label>
                                                              </div>

                                                              <div class="col-md-3">
                                                                <div class="form-group form-md-line-input">
<%--                                                                            <s:select cssClass="list-menu form-control" id="" name="toDoRule.emailNotification"   list="%{toEmailList}"  headerKey="" headerValue=""  onchange="changeStatus();"/>
 --%>                                                                
                                                              <s:select cssClass="list-menu form-control"  id="" list="%{selectedDateField}"  headerKey="" headerValue="" name="toDoRule.testdate" onchange="changeStatus();"/> 
                                                              
                                                              <s:select cssClass="list-menu form-control"  id="emailNo" list="%{toEmailList}"  headerKey="" headerValue="" name="toDoRule.emailNotification" onchange="changeStatus();"/> 
                                                         </div>
                                                          </div>

                                                          <div class="col-md-3 pl-0 pr-0">
                                                      
                                                            <label class="label-heading ctrl-label"> is less than equal to +/- days<span class="validate-icon">*</span><s:textfield name="toDoRule.durationAddSub" required="true" maxlength="4" size="1"  onkeydown="return onlyNumsAllowed(event)" onkeyup="shiftCode(event)"  cssClass="input-text add-duration" readonly="false" /> from today</label>
                                                        </div>

                                                            <div class="col-md-2">
                                                            
                                                                <label class="label-heading mt-2"> Agent TDR </label>
                                                                 </div>
                                                                 <div class="col-md-1 text-center">
                                                                  
                                                                  
                                                                 
                                                                   
													                                                                   <c:set var="publish" value= "0" />
															  		<sec-auth:authComponent componentId="module.script.form.todoRulePublish">
															  			<c:set var="publish" value= "1" />
															  		</sec-auth:authComponent>
															  		
															  		<c:if test="${publish=='0'}">
															  		     <s:checkbox name="toDoRule.publishRule"  cssClass="checkbx-rule"  value="${toDoRule.publishRule}" fieldValue="true" disabled="true" />
															  			
															  		</c:if>
															  		<c:if test="${publish=='1'}">
															  		    <s:checkbox name="toDoRule.publishRule"  cssClass="checkbx-rule"  value="${toDoRule.publishRule}" fieldValue="true" />
															  			
															  		</c:if>


                                                                   
                                                                 
                                                                <%--<c:if test="${toDoRule.status!='Error'}">
		  		                                               
		  		                                                       <s:textfield name="toDoRule.status" size="4"  cssClass="status green statuscheck"  readonly="true"/>
		  		                                                        <s:textfield name="toDoRule.status" size="4"  cssClass="status warning statuserror"  readonly="true"/>
		  		                                                     
		  		                                               
		  		                                                </c:if>
		  		                                                <c:if test="${toDoRule.status=='Error'}">
		  		                               
		  		                                                     <s:textfield name="toDoRule.status" size="4" cssClass="status warning statuscheck"  readonly="true"/>
		  		                                                      <s:textfield name="toDoRule.status" size="4"  cssClass=" status statuscheckerror"  readonly="true"/>
		  		                                                     
		  		                                                
		  		                                                </c:if>
		  		                              --%>                   </div>
                                                           
                                                           
                                                             
                                                         </div>
                                                               
                                                              

                                                               <div class="row">
                                                                     <div class="col-md-3">
                                                                     
                                                                           <label class="label-heading mt-2">Field To Validate #1<span class="validate-icon">*</span></label>
                                                                       
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                     <div class="form-group form-md-line-input">
                                                                      <s:textfield name="toDoRule.fieldToValidate1"  maxlength="75" size="72"  cssClass="input-text form-control" readonly="false" onchange="getDescription(this)"/>
                                                                      </div>
                                                                      </div>
                                                                      
                                                                      <div class="col-md-2">
                                                                        <label class="label-heading mt-2">Status</label>
                                                                      </div>
                                                                      <div class="col-md-1 text-center">
                                                                      
                                                                 
                                                                <c:if test="${toDoRule.status=='Tested'}">
		  		                                               
		  		                                                       <s:textfield name="toDoRule.status" size="4"  cssClass="status green mt-1 statuscheck disable-condition"  readonly="true"/>
		  		                                                     
		  		                                               
		  		                                                </c:if>
		  		                                                <c:if test="${toDoRule.status=='Error'}">
		  		                               
		  		                                                     <s:textfield name="toDoRule.status" size="4" cssClass="status warning mt-1 statuscheck"  readonly="true"/>
		  		                                                     
		  		                                                
		  		                                                </c:if>
		  		                                                
                                                                      </div>
                                                                    
                                                               </div>
                                                                

                                                              <div class="row">
                                                                     <div class="col-md-3">
                                                                     
                                                                           <label class="label-heading mt-2">Field To Validate #2<span class="validate-icon">*</span></label>
                                                                       
                                                                     </div>
                                                                     <div class="col-md-6">
                                                                     <div class="form-group form-md-line-input">
                                                                      <s:textfield name="toDoRule.fieldToValidate2"  maxlength="50" size="30"  cssClass="input-text form-control" readonly="false"/>
                                                                      </div>
                                                                      </div>
                                                                      <c:if test="${toDoRule.corpID=='UTSI'}"> 
                                                                      <div class="col-md-2">
                                                                        <label class="label-heading mt-2">Account TDR</label>
                                                                      </div>
                                                                      <div class="col-md-1 text-center">
                                                                      
                                                                 
                                                                 <c:set var="isAccount" value= "0" />
															  		<sec-auth:authComponent componentId="module.script.form.todoRulePublish">
															  			<c:set var="isAccount" value= "1" />
															  		</sec-auth:authComponent>
															  		
															  		<c:if test="${isAccount=='0'}">
															  		     <s:checkbox name="toDoRule.isAccount"  cssClass="checkbx-rule"  value="${toDoRule.isAccount}" fieldValue="true" disabled="true" />
															  			
															  		</c:if>
															  		<c:if test="${isAccount=='1'}">
															  		    <s:checkbox name="toDoRule.isAccount"  cssClass="checkbx-rule"  value="${toDoRule.isAccount}" fieldValue="true" />
															  			
															  		</c:if>
		  		                                                
                                                                      </div>
                                                                          </c:if>
                                                                    
                                                               </div>
                                                            <div class="row">
                                                                <div class="col-md-3"> 
                                                                      <label class="label-heading mt-2">Display Field	</label>
                                                                 
                                                                </div>

                                                                <div class="col-md-6">
                                                                <div class="form-group form-md-line-input">
                                                                        <s:textfield name="toDoRule.fieldDisplay"  maxlength="75" size="72"  cssClass="input-text form-control" readonly="false"/>
                                                                
                                                                </div></div>
                                                               
                                                          </div>
                                                          
                                                          <div class="row">
                                                             <div class="col-md-3">
                                                             <label class="label-heading mt-2">Remark</label>
                                                            
                                                            </div>
                                                            
                                                            <div class="col-md-6">
                                                            
                                                               <s:textarea cssClass="textarea remark-sec"  cssStyle="width:100%" cols="54" rows="2" name="toDoRule.remarks" onchange="return checkLength();"/> 
                                                             
                                                            </div>
                                                          </div>
                                                           
                                                          <!-- <div class="row">
                                                              <div class="col-md-12">
                                                                    <button type="button" class="font-weight-semibold" data-toggle="modal" data-target="#exampleModal">Condition Field</button>
                                                              </div>
                                                              
                                                        </div> -->
                                                              
                                                        <!-- <div class="row mt-4">
                                                          <div class="col">
                                                             <button type="button " class="btn default blue-stripe" >Copy</button>
                                                          </div>
                                                          <div class="col">
                                                              <button type="button "  class="btn default blue-stripe">Test Condition</button>
                                                           </div>
                                                           <div class="col">
                                                              <button type="button "  class="btn default blue-stripe">Execute This Rule</button>
                                                           </div>
                                                        </div> -->

                                                              
                                                        

                                                        </div>

                                                        <div class="col-md-12 col-xl-5">                                                       
                                                            
                                                                 
                                                              
                                                               
                                                                   
                                                                   <!-- <div class="row">
                                                              <div class="col-md-12">
                                                                    <button type="button" class="font-weight-semibold btn default" data-toggle="modal" data-target="#exampleModal">Condition Field</button>
                                                              </div>
                                                              
                                                        </div> -->
    
                                                                    
                                                                  
                                                                  
    
                                                                   <div class="row">
                                                                         
                                                                         <div class="col-md-12">
                                                                          <button type="button" class="float-right btn btn-sm condition-dropdorn" onclick="viewCondition();">Condition Field</button>
                                                                               <label for="exampleFormControlTextarea1" class="label-heading"> Validation Condition<span class="validate-icon">*</span></label>
                                                                               <s:textarea cssClass="form-control" cssStyle="margin-top:5px;" name="toDoRule.expression"  rows="10" cols="90" readonly="false"/>
                                                                              
                                                                             </div>
                                                                              
                                                                   </div>
                                                            
    
                                                            </div>

                                                       
                                                       



                                                   
                                               
                                             </div>
                                    </div>
                              </div>

                              <div class="col-md-12">                              
                                   
                                      <!--testing-->
                      
                           
                                    <c:forEach var="myFileList" items="${actionList}" varStatus="rowCounter" >
                              <div class="row" >                            
                                  <div class="accordion col-md-12" id="accordionExample">
                                  <div class="input_fields_wrap card border-bottom" id="sha">
                                    <div class="card-header " >
                                      <h2 class="mb-0">
                                        
                                   
                                     
                                     
<%--                                         <i class="fa fa-trash float-right" aria-hidden="true" onclick="confirmDelete('${toDoRule.id}','${myFileList.id}');" ></i>
                                        <i class="fa fa-pencil-square-o float-right mr-3" data-toggle="modal" aria-hidden="true" onclick="editAction('${toDoRule.id}','${myFileList.id}');"></i> --%> 
                                      
                                      </h2> 
                             
                                      <div class="row">
                                     
                                      <div class="col-md-2">
                                          <button id="myButton" class="btn btn-link pl-0 tstbutton" type="button" data-toggle="collapse" data-target="#collapseOne${myFileList.id}" aria-expanded="false" aria-controls="collapseOne${myFileList.id}" >
                                              Action 
                                             </button>
                                      </div>
                                    
                                      <div class="col-md-1 pl-0">
                                          <label class="label-heading isha">Message For User</label>
                                         
                                      </div>
                                      <div class="col-md-3 pl-0">
                                         
                                          <label class=" ml-1 isha">${myFileList.messagedisplayed}</label>
                                      </div>
                                      <div class="col-md-3 col-lg-2 col-xl-2">
                                          <label class="label-heading isha">Display Message To </label>
                                         
                                      </div>
                                      <div class="col-md-1 col-lg-1 pl-0">
                                      <c:choose> 
                                      <c:when test="${not empty myFileList.displayToUser}">
                                      <label class="isha">${myFileList.displayToUser}</label>
                                      </c:when> 
                                      <c:otherwise>
                                      <label class="isha">${myFileList.rolelist}</label>
	                                  </c:otherwise></c:choose>
                                      </div>
                                      
                                      <div class="col-md-1 text-right">
                                      <label class="label-heading isha">Status</label>                                     
                                       </div>
                                       <div class="col-md-1 isha" id="customSwitch1">
                                       <div class="custom-control custom-switch isha">
                                        <c:set var="ischecked" value="false"/>
				                          <c:if test="${myFileList.status}">
				                          <c:set var="ischecked" value="true"/>
				                           </c:if>
                                                                 
                                                              
                                                                      <s:checkbox id="${myFileList.id}" key="rule.status" cssClass="custom-control-input isha" value="${ischecked}" fieldValue="true"/>                       
                                                                  
                                       <label class="custom-control-label isha" for="${myFileList.id}"></label>
                                       </div>
									   </div>
                                         <div id="agentDetailDiv${toDoRule.id}" class="modal fade" ></div>
                                         <button type="button" class="btn  btn-sm dropdown-toggle pr-0" id="editButton${myFileList.id}" data-toggle="dropdown"  aria-hidden="true" onclick="editAction('${toDoRule.id}','${myFileList.id}');">
                                                     <i class="fa fa-pencil-square-o float-right mr-3 mt-0" aria-hidden="true"></i>
                                         </button>
                          
                                    </div>

                                    </div>  
                                
                                    <div id="collapseOne${myFileList.id}" class="collapse " aria-labelledby="headingOne" data-parent="#accordionExample">
                                     <div class="card-body">
                                          <div class="row">
                                              <div class="col-md-12 col-xl-6">    
                                            <!-- <div class="row">
                                                <div class="col-md-3">
                                                    <label class="label-heading">Control Date*	</label>
                                                </div>

                                                <div class="col-md-9">
                                                    <select id="inputState" class="form-control">
                                                        <option selected>billing.recSignedDisclaimerDate</option>
                                                        <option value="Claim">Claim</option>
                                                        <option value="CreditCard">CreditCard</option>
                                                        <option value="CustomerFile">CustomerFile</option>
                                                        <option value="DspDetails">DspDetails</option>
                                                        <option value="LeadCapture">LeadCapture</option>
                                                       
                                                  </select>
                                                </div>
                                           </div> -->

                                           <div class="row">
                                              <div class="col-md-3">
                                                    <label class="label-heading eclipse-text">Message For User	</label>
                                               
                                              </div>

                                              <div class="col-md-9">
                                                 <label class=" ml-1">${myFileList.messagedisplayed}</label>
                                           </div>
                                             
                                        </div>

                                         

                                         <div class="row">
                                            <div class="col-md-3">
                                                <label class="label-heading ">Display Message To</label>
                                            </div>

                                            <div class="col-md-9">
                                       
                                          <c:choose> 
                                          <c:when test="${not empty myFileList.displayToUser && toDoRule.isAccount}">
                                          
                                          <label class="isha">${myFileList.displayToUser}</label>
                                           </c:when> 
                                           <c:otherwise>
                                           <label class="isha">${myFileList.rolelist}</label>
	                                       </c:otherwise></c:choose>
                                            </div>
                                       </div>

                                       <div class="row">
                                          <div class="col-md-3">
                                              <label class="label-heading eclipse-text">Note Type	</label>
                                          </div>

                                          <div class="col-md-9">

                                              <label class=" ml-1">${myFileList.noteType}</label>
                                             
                                          </div>

<%--                                           <div class="col-md-2">
                                              <label class="label-heading ">Note Sub Type1	</label>
                                          </div>

                                          <div class="col-md-3">

                                              <label class=" ml-1">${myFileList.noteSubType}</label>
                                             
                                          </div> --%>

                                         
                                     </div>

                                    


                                     

                                  </div>
                                  <div class="col-md-12 col-xl-6">
                                      <%-- <div class="row">
                                        <div class="col-md-3">
                                            <label class="label-heading"> Agent TDR</label></div>
                                        <div class="col-md-4 "><s:checkbox name="rule.status"   value="${myFileList.status}" fieldValue="true" disabled="true" /></div>
                                      </div>
                                       --%>
                                      

                                      <div class="row">
                                          <div class="col-md-3">
                                              <label class="label-heading ">Email Notification</label>
                                          </div>

                                          <div class="col-md-9">
                                              <label class=" ml-1">${myFileList.emailNotification}</label>
                                           
                                          </div>
                                     </div>

                                     <div class="row">
                                        <div class=" col-md-3">
                                              <label class="label-heading">Manual Email		</label>
                                         
                                        </div>

                                        <div class="col-md-9">
                                            <label class=" ml-1">${myFileList.manualEmail}</label>
                                     </div>
                                        
                                  </div>
                                  <div class="row">
                                     <div class="col-md-3">
                                              <label class="label-heading ">Note Sub Type	</label>
                                          </div>

                                          <div class="col-md-9">

                                              <label class=" ml-1">${myFileList.noteSubType}</label>
                                             
                                          </div>
                                  </div>
                                </div>



                                </div>
                                      </div>
                                    </div>
                                  </div>
                                
                                 
                                
                                </div>
                              </div>
                              
                                
</c:forEach>
                                <!--end testing-->
                                
                                <div class="row">
                                <div class="col-md-8 mt-4 pl-0">
                                <ul class="bottom-created-info">
                                   <li>
                                      <fmt:message  key='customerFile.createdOn'/> <s:text id="toDoRuleFormattedValue" name="${FormDateValue}"> 
                                       <s:param name="value" value="toDoRule.createdOn" /></s:text>
                                       <s:hidden name="toDoRule.createdOn" value="%{toDoRuleFormattedValue}" />
                                       <span class="date-li-bottom"><fmt:formatDate value="${toDoRule.createdOn}" pattern="${displayDateTimeFormat}"/></span>
                                      
                                   </li>
                                  
                                   <li>
                                      <fmt:message key='customerFile.createdBy' />
                                      <c:if test="${not empty toDoRule.id}">
										<s:hidden name="toDoRule.createdBy"/>
										: <s:label name="createdBy" value="%{toDoRule.createdBy}"/>
									   </c:if>
									   <c:if test="${empty toDoRule.id}">
								      <s:hidden name="toDoRule.createdBy" value="${pageContext.request.remoteUser}"/>
								      : <s:label name="createdBy" value="${pageContext.request.remoteUser}"/>
							         </c:if>
                                   </li>
                                   
                                   <li>
                                     <fmt:message key='customerFile.updatedOn'/>
                                     <s:text id="toDoRuleupdatedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="toDoRule.updatedOn" /></s:text> 
                                     <s:hidden name="toDoRule.updatedOn" value="%{toDoRuleupdatedOnFormattedValue}" />
                                     <span class="date-li-bottom"><fmt:formatDate value="${toDoRule.updatedOn}" pattern="${displayDateTimeFormat}"/></span>
                                    </li>
                                   
                                    <li>
                                     <fmt:message key='customerFile.updatedBy' />
                                     <c:if test="${not empty toDoRule.id}">
								    <s:hidden name="toDoRule.updatedBy"/>
								    : <s:label name="updatedBy" value="%{toDoRule.updatedBy}"/>
							</c:if>
							<c:if test="${empty toDoRule.id}">
								<s:hidden name="toDoRule.updatedBy" value="${pageContext.request.remoteUser}"/>
								: <s:label name="updatedBy" value="${pageContext.request.remoteUser}"/>
							</c:if>
                                    </li>
                                </ul>
                                


                                 
                                
                                 
                         
                        
                                  	
                                 
                              
                              </div>
                              
                              <div class="col-md-4 mt-4 pl-0 text-right action-btn-sec">
                               <s:submit cssClass="btn btn-blue" type="submit" method="save" key="button.save"  onclick="return btntype(this);"  /> 
                                <c:if test="${toDoRule.id!=null}">
		     			       <c:if test="${company.licenseType=='B' && fn:length(actionList)<2}">
		     			       <button type="button" id="addAction" class="btn btn-blue  add-actions" data-toggle="modal"  onclick="addNewAction();">Add Action</button>
		     			        </c:if>
		     			       <c:if test="${company.licenseType!='B' && fn:length(actionList)<5}">
		     			       <button type="button" id="addAction" class="btn btn-blue  add-actions" data-toggle="modal"  onclick="addNewAction();">Add Action</button>
		     			        </c:if>
		     			         </c:if>
                              
                              </div>
                              </div>
                              
                              </div>

                            
                              

                                 
                             
                            
                           </div>
 
                                    
                                 <div class="tab-pane fade" id="pills-rulelist" role="tabpanel" aria-labelledby="pills-rulelist-tab">2...</div>
                                 <div class="tab-pane fade" id="pills-audit" role="tabpanel" aria-labelledby="pills-audittab">3...</div>
                                
                                
                               </div>
                            
                              <!-- BEGIN Portlet PORTLET-->
                            
                           </div>
                        </div>
                     </div>


                  <!--test-->
                 
                  <!--end second row-->       
               
            </div>
         </div>
      </div>
      <!-- END Portlet PORTLET-->
      </div>
      </div>
      </div>
      <!-- END PAGE BASE CONTENT -->
      </div>
      </div>
      </div>
      <!-- END CONTAINER -->
      <!-- full width -->
     <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h6 class="modal-title" id="exampleModalLabel">Please Select Condition</h6>
            <button type="button" class="close pt-0" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          
  <div id="accordion">
  <div class="card mb-2">
    <div class="card-header pl-2" id="headingOne">
      <h5 class="mb-0" data-toggle="collapse" data-target="#collapseOne" id="ishaK" aria-expanded="true" aria-controls="collapseOne">
         <!-- <div   id="ishaK" class="card-body">
         </div> -->
      </h5>
    </div>

    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
      <s:hidden name="text" id="txt"/>
     
      
      
      <ul id="friends" class="list-group codition-ul">
      </ul>
    </div>
  </div>
<!--   <div class="card">
    <div class="card-header pl-2" id="headingTwo">
      <h5 class="mb-0 collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
        <button class="btn btn-link  pl-0" >
        Customer File
        </button>
      </h5>
    </div>
    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
      <div class="card-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
 --> 
</div>
  
  
  
            
            
          </div>
         <!--  <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
           
          </div> -->
        </div>
      </div>
    
    </div>
    <div class="modal fade" id="mapModal" aria-hidden="true">
<div class="modal-dialog  modal-lg"  id="myModal" >
 <div class="modal-content pt-3 pr-3 pl-3 pb-3">
 <!-- <div class="modal-header  header-bdr">
       <h4 class="modal-title" id="myModal" style="line-height:1.32 !important">Agent Template Details</h4>
       
</div> -->
     
<div class="modal-body" id="myModal">
<div class="modal fade" id="showErrorModal" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									    <div class="modal-body" style="font-weight:600;">
										    <div class="alert alert-danger" id="getErrorMsg">
											</div>
									    </div>
									    
								</div>
							</div>
						</div>
<div class="modal fade" id="caseModal" aria-hidden="true">
							
</div>

      <s:form id="rule" name="rule" action="" > 
   
        <s:hidden  name="ruleId" value="${toDoRule.id}" />
        <s:hidden name="tdid" value=""/>
         
       
        
                                   
          <div class="card mb-2 border-b">
                   <div class="card-header">
                       <h6>Action</h6>
                   </div>
                   <div class="card-body pb-1">
                      <div class="row">
                        <div class="col-md-12">    
                      

                     <div class="row">
                        <div class="col-md-3">
                        
                              <label class="label-heading  mt-2">Message For User<span class="validate-icon">*</span></label>
                         
                        </div>

                        <div class="col-md-9">
                         <div class="form-group form-md-line-input">
                           <s:textfield name="rule.messagedisplayed" required="true"  maxlength="100" size="80"   cssClass="input-text form-control" readonly="false"/>
                     </div>
                     </div>
                       
                  </div>

                   

                   <div class="row">
                      <div class="col-md-3">
                          <label class="label-heading  mt-2">Display Message To<span class="validate-icon">*</span></label>
                      </div>

                      <div class="col-md-9">
                       <div class="form-group form-md-line-input">
                      <s:select cssClass="list-menu form-control"  name="rule.rolelist"  headerKey="" headerValue="" list="%{prulerole}"  />
                     </div>
                      </div>
                      
                      
                      
                      
                 </div>

                 <div class="row">
                    <div class="col-md-3">
                        <label class="label-heading eclipse-text mt-2">Note Type	</label>
                    </div>

                    <div class="col-md-4">
                    <div class="form-group form-md-line-input">
                       <s:select cssClass="list-menu form-control" id="entity" name="rule.noteType"  list="%{notetype}" headerKey="" headerValue=""  onchange="getSubType(this);"/>
                   </div>
                    </div>

                    <div class="col-md-2">
                        <label class="label-heading mt-2">Note Sub Type	</label>
                    </div>

                    <div class="col-md-3">
                    <div class="form-group form-md-line-input">
                        <s:select cssClass="list-menu form-control" id="entity" name="rule.noteSubType"    list="%{notesubtype}" headerKey="" headerValue="" />
                    </div>
                    </div>

                   
               </div>

              


               

            </div>
            <div class="col-md-12">
                

                <div class="row">
                    <div class="col-md-3">
                        <label class="label-heading mt-2">Email Notification</label>
                    </div>

                    <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                     <!--  <select id="emailNotddd">
        <option value="a">Apple</option>
        <option value="o">Orange</option>
        <option value="p">Pineapple</option>
        <option value="b">Banana</option>
    </select> -->
                       <s:select cssClass="list-menu form-control" id="emailNotddd" name="rule.emailNotification"    headerKey="" headerValue="" list="%{toEmailList}"  onchange="changeStatus();"/>
                    </div>
                    </div>
               </div>

               <div class="row">
                  <div class="col-md-3">
                        <label class="label-heading mt-2">Manual Email		</label>
                   
                  </div>

                  <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                    
                     <s:textfield name="rule.manualEmail"    required="true" maxlength="50"  cssClass="input-text  form-control" readonly="false"/>
                  
               </div>
                 
              </div>
          </div>
          <div class="row">
                  <div class="col-md-3">
                        <label class="label-heading mt-2">Display To		</label>
                   
                  </div>

                  <div class="col-md-9">
                     <div class="form-group form-md-line-input">
                    <s:select cssClass="list-menu form-control"  name="rule.displayToUser"  list="%{taskUser}" headerKey="" headerValue="" />
                     </div>
               </div>
                 
              </div>
          </div>


          </div>
            </div>
          </div>

    <div class="float-right mb-3">
    
                <button type="button" id="spanghclose" class="btn btn-blue float-right mr-3" data-dismiss="modal" onclick="refersh();" >Close</button>
        
			     <s:submit cssClass="btn btn-blue float-right mr-3"  type="button" key="button.save"  id="btnSave" onclick="return modalData();" />  
					</div> 
	  
      </s:form>
   

</div>
</div> 
</div></div>

 </s:form>

      <!-- long modals -->


      <!-- Modal -->

   
    

  <!-- long modals -->

      <!-- END FOOTER -->
      <!-- jQuery library -->
   <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
      <!-- Popper JS -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <!-- Latest compiled JavaScript -->
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script>
         $(document).ready(function(){
           $('[data-toggle="tooltip"]').tooltip();   
         });
      </script>
      <script type="text/javascript">
         $(document).ready(function(){
             $('[data-toggle="popover"]').popover({
                 placement : 'top',
                 trigger : 'hover'
             });
         });
         
      </script>
      
        <script>
         $(document).ready(function(){
        	 
           $('[data-toggle="tooltip"]').tooltip();   
         });
      </script>

<script language="JavaScript">

var testvar = "";

function btntype(targerelement){
	

	var category= document.forms['toDoRule'].elements['toDoRule.entitytablerequired'].value;
	var field1= document.forms['toDoRule'].elements['toDoRule.fieldToValidate1'].value;
	var expression= document.forms['toDoRule'].elements['toDoRule.expression'].value;
	var testDate= document.forms['toDoRule'].elements['toDoRule.testdate'].value;
	var duration= document.forms['toDoRule'].elements['toDoRule.durationAddSub'].value;
	if(category!='' && field1!='' && expression!='' && testDate!=''  && duration!=''){
		document.forms['toDoRule'].elements['btn'].value=targerelement.value; 
	}else{
		alert("Please fill all required field!");
		return false;
	}
}
	var http21 = getHTTPObject(); 
	
	function modalData()
	{
		var user =document.forms['toDoRule'].elements['rule.displayToUser'].value
		var rolelist =document.forms['toDoRule'].elements['rule.rolelist'].value

		 if(document.forms['toDoRule'].elements['rule.messagedisplayed'].value =="" ){
				
				alert("Enter Value for the field Message For User");	
					
				return false;
				}
			if(document.forms['toDoRule'].elements['rule.rolelist'].value =="")
				{
				
				alert("Enter Value for the field Display Message To");
				return false;
				
				}
			  if(document.forms['toDoRule'].elements['rule.displayToUser'].value !="")
				{
				
				alert("Please note that the '"+user+"'  will be notified and not the '"+rolelist+"'");
				
				}
				document.forms['toDoRule'].action = 'saveruleAction.html';
	              document.forms['toDoRule'].submit();
	 	
	}
	
	
	function getEmailList(target){
		var fieldToValiD = document.forms['toDoRule'].elements['toDoRule.fieldToValidate1'].value;
		var tenpField = fieldToValiD.split('.');
		var fieldToValiDate1 = tenpField[0];
		var tableName= target.value;
		var parameter = '';
        if(tableName=='ServiceOrder'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='CustomerFile'){
        	parameter = 'PENTITY_CUSTOMERFILE';
        }else if(tableName=='QuotationFile'){
        	parameter = 'PENTITY_QUOTATIONFILE';
        }else if(tableName=='WorkTicket'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='AccountLine'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='AccountLineList'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='Billing'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='Claim'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='Miscellaneous'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='ServicePartner'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='Vehicle'){
        	parameter = 'PENTITY_SERVICEORDER';
        }else if(tableName=='TrackingStatus'){
        	parameter = 'PENTITY_SERVICEORDER';
        }		
		var url="getTempEmailList.html?ajax=1&decorator=simple&popup=true&param=" + encodeURI(parameter);
		http22.open("GET", url, true);
    	http22.onreadystatechange = handleHttpResponseEmailList;
     	http22.send(null);
	}
	function handleHttpResponseEmailList(){            
     if (http22.readyState == 4){
            var results = http22.responseText
            results = results.trim(); 
            
            var res = results.split("@"); 
                       	var modal = document.getElementById("exampleModal");
                 
            if (modal != null && modal.open) {
	            
	            var lblFirstName = modal.document.getElementById("emailNot").value;
	          
	            
	           
	           modal.focus();
	            
	            }
            
            
            
            
            
            var results = http22.responseText
            results = results.trim(); 
            var res = results.split("@");      
            targetElement = document.forms['toDoRule'].elements['toDoRule.emailNotification'];
			targetElement.length = res.length;			
			for(i=0;i<res.length;i++){
			  if(res[i] == ''){
				document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].text = '';
				document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].value = '';
			  }else{
				document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].text = res[i];
				document.forms['toDoRule'].elements['toDoRule.emailNotification'].options[i].value = res[i];
			  }			
            } 
			document.forms['toDoRule'].elements['toDoRule.emailNotification'].value='${toDoRule.emailNotification}';       
   		}
	}

	
	function getFieldList(target){
 		var tableName= target.value;
		var url="getTempFieldList.html?ajax=1&decorator=simple&popup=true&tableName=" + encodeURI(tableName);
		http21.open("GET", url, true);
    	http21.onreadystatechange = function(){ handleHttpResponseFieldList(target);};
     	http21.send(null);
 }
	
var tar;
	function handleHttpResponseFieldList(tar)
        {             
		 document.getElementById("ishaK").innerHTML = "";
		
         if (http21.readyState == 4)
             {
                var results = http21.responseText
                results = results.trim();
                // alert(results);
                
                var res = results.split("@");
               if(res[1]!='undefined' && res[1]!='')
            	  {
               // var t=res[1].split(".");
              // console.log(t)
             	 var container1 = document.getElementById("ishaK"); 
					
				    var t = document.createTextNode(tar.value);
				    t.className=" ml-1";
				    container1.appendChild(t); 
				    
					 document.getElementById("friends").innerHTML = "";

 				for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){					
						//document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].text = '';
						//document.forms['toDoRule'].elements['toDoRule.tempField'].options[i].value = '';
					}else{
					 
					var container = document.getElementById("friends");
			            
				       var listItem = document.createElement("li");
				       var candidate = document.getElementById("txt");
				       listItem.setAttribute('id',candidate.value);
				       listItem.className="list-group-item";
				       var listValue = document.createTextNode(res[i]);
				       listItem.appendChild(listValue); 
				       
				     
				       var btn = document.createElement("BUTTON"); 
				       btn.innerHTML = "Copy";  
				       btn.onclick=function()
				    	 {
				    		editUnit(this);
				    	 }
				    	
				    	listItem.appendChild(btn);
					    container.appendChild(listItem);
						}
					
					
             }}
 				 
           
        
	}}
	try{
		disableCheckBox();
		}
		catch(e){}
		 showOrHide(0);
		function showOrHide(value) {
		    if (value==0) {
		       if (document.layers)
		           document.layers["overlay11"].visibility='hide';
		        else
		           document.getElementById("overlay11").style.visibility='hidden';
		   }
		   else if (value==1) {
		      if (document.layers)
		          document.layers["overlay11"].visibility='show';
		       else
		          document.getElementById("overlay11").style.visibility='visible';
		   }
		}
		</script>
		<script type="text/javascript">
		 try{
			var catagory = document.forms['toDoRule'].elements['toDoRule.entitytablerequired'];
			getEmailList(catagory);
		}
		catch(e){}
		
		
		String.prototype.trim = function() {
		    return this.replace(/^\s+|\s+$/g,"");
		}
		String.prototype.ltrim = function() {
		    return this.replace(/^\s+/,"");
		}
		String.prototype.rtrim = function() {
		    return this.replace(/\s+$/,"");
		}
		String.prototype.lntrim = function() {
		    return this.replace(/^\s+/,"","\n");
		}
		
		 var http22 = getHTTPObject22();
		 function getHTTPObject22()
		{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsofrstruts.cmlt.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
		}
/* 		function getHTTPObject()
		{
	    var xmlhttp;
	    if(window.XMLHttpRequest)
	    {
	        xmlhttp = new XMLHttpRequest();
	    }
	    else if (window.ActiveXObject)
	    {
	        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	        if (!xmlhttp)
	        {
	            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	        }
	    }
	    return xmlhttp;
		}
	    var http21 = getHTTPObject();

	    
	    var http22 = getHTTPObject22(); */
	    
	   
	    var temp,ruleId;
	     function addNewAction(){
		   var $j = jQuery.noConflict();
	    	var modal = document.getElementById("mapModal");
	           
			   if (modal != null && !modal.closed) {
				
						
		           console.log("data in modal >>>"+modal)
		            var y = document.getElementById("emailNo");
	        var txt = "All options: ";
	        var i;
	          for (i = 0; i < y.length; i++) {
	            txt = txt + "\n" + y.options[i].value;
	            console.log(y.options[i].value);
  				    var x = document.getElementById("emailNotddd");
					var option = document.createElement("option");
					option.text = y.options[i].value; 
					x.add(option);
	          }
	           
	        $j('#mapModal').modal({show:true,backdrop: 'static',keyboard: false}); 
		          
		            
		            }
		   

	      		
	    } 	
	    var id;
	    function editAction(ruleId,id){
	    	
	    	console.log("test")
            
			
            var y = document.getElementById("emailNo");
    var txt = "All options: ";
    var i;
      for (i = 0; i < y.length; i++) {
        txt = txt + "\n" + y.options[i].value;
       // console.log(y.options[i].value);
			    var x = document.getElementById("emailNotddd");
			    //console.log(x)
			var option = document.createElement("option");
			option.text = y.options[i].value; 
			x.add(option);
      }
	    	
	    	var $j = jQuery.noConflict();
	    	var catagory = document.forms['toDoRule'].elements['toDoRule.entitytablerequired'].value;
	    	$j.ajax({
	    		
		  		 url: 'editRule.html?tdid='+id+'&ruleId='+ruleId+'&param='+catagory+'&decorator=modal&popup=true',
		            success: function(data){
		            	testvar = document.getElementById("emailNo");
		            	$j("#agentDetailDiv"+ruleId).modal({show:true,backdrop: 'static',keyboard: false});
		            	
	      	
		
	    	

						$j("#agentDetailDiv"+ruleId).html(data);
	            },
	            error: function () {
	                alert('Some error occurs');
	                $j("#agentDetailDiv"+ruleId).modal("hide");
	             }   
		});  
	      		
	      		
	    } 	
	   
         function confirmDelete(ruleId,id)
         {
        	
        	 var $j = jQuery.noConflict();
 	    	
 	    	$j.ajax({
 	    		
 		  		 url: 'deleteRule.html?tdid='+id+'&ruleId='+ruleId+'&decorator=simple&popup=true',
 		            success: function(data){
 		                location.reload();    
 	            },
 	            error: function () {
 	               
 	             }   
 		});  
 	      		
        	 
        	 
         }
         
         

     	function copyToTable()
     	{
     		changeStatus();
     	  //alert("Hi Copying"+document.forms['toDoRule'].elements['toDoRule.expression'].value);
     	  
     	  var temp=document.forms['toDoRule'].elements['rule.tempField'].value;
     	  
     	  if(document.forms['toDoRule'].elements['toDoRule.expression'].value=="")
     	  {
     	  	 document.forms['toDoRule'].elements['toDoRule.expression'].value=document.forms['toDoRule'].elements['toDoRule.expression'].value + temp;
     	  	 
     	  }
     	  else
     	  {
     	   document.forms['toDoRule'].elements['toDoRule.expression'].value=document.forms['toDoRule'].elements['toDoRule.expression'].value +' ' + temp;
     	   
     	  }
     	  
     	}
     	
     	
     	
     	function viewCondition(){
     		
		   var $j = jQuery.noConflict();
     		 $j('#isha').val('');
		   $j('#exampleModal').modal({ });
		   
		   
     		 
     		  // calculate the length of array
     		  
     		}
     	function refersh()
        {       
        	
        	window.location.reload(true);
		     			  
        }
     	var t="";
   	  changeStatus();
   	  //alert("Hi Copying"+document.forms['toDoRule'].elements['toDoRule.expression'].value);
   	  
   	 
   	  
     	var editUnit = function (button) {
     		 var buttonText = button.innerText;
     		 var fullText = button.parentElement.innerText;
     		 var wanted = fullText.replace(buttonText, '');
     		 console.log(wanted);
     		var $j = jQuery.noConflict();
     		document.getElementById('tempField').value=wanted
     		document.forms['toDoRule'].elements['toDoRule.tempField'].value=wanted;
     		 if(document.forms['toDoRule'].elements['toDoRule.expression'].value=="")
     	   	  {
     	   	  	 document.forms['toDoRule'].elements['toDoRule.expression'].value=document.forms['toDoRule'].elements['toDoRule.expression'].value + wanted;
     	   	 
     	   	  	 $j("#exampleModal").modal("hide");
     	   	
     	   	  }
     	   	  else
     	   	  {
     	   	   document.forms['toDoRule'].elements['toDoRule.expression'].value=document.forms['toDoRule'].elements['toDoRule.expression'].value +' ' + wanted;
     	   
     	   	   $j("#exampleModal").modal("hide");
     		
     	   	  }
     		 
     		 
     		};
    	function changeStatus(){
    		document.forms['toDoRule'].elements['formStatus'].value = '1';
    	}
    	
    	 var pay;
    	function getOption(pay){
    		 document.getElementsById('rolelist').value=pay.value;
    	 }
    	
</script> 
<script type="text/javascript">
try{
	var catagory = document.forms['toDoRule'].elements['toDoRule.entitytablerequired'];
	getEmailList(catagory);
	getFieldList(catagory);
	document.getElementById("emailNo").style.display = "none";
	
}
catch(e){}


</script>
<script language="JavaScript">
    var shift=0;
	function onlyNumsAllowed(evt, strList, bAllow)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  if(keyCode==16) {
	 shift++;
	 }
	 if(shift!=0 && (keyCode >= 48 && keyCode <= 57))
	 return false;
	 else
   return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)||(keyCode==109) ||(keyCode==173); 
	}
	function shiftCode(evt){
    var keyCode = evt.which ? evt.which : evt.keyCode;
     if(keyCode==16) {
	 shift=0;
	 }
	 }
	var httpStatus = getHTTPObject31();
	function getHTTPObject31()
	{
	var xmlhttp;
	if(window.XMLHttpRequest)
	{
	    xmlhttp = new XMLHttpRequest();
	}
	else if (window.ActiveXObject)
	{
	    xmlhttp=new ActiveXObject("Microsofrstruts.cmlt.XMLHTTP");
	    if (!xmlhttp)
	    {
	        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
	    }
	}
	return xmlhttp;
	}	
	window.onload = function() { 
		<sec-auth:authComponent componentId="module.script.form.toDoRuleScript">
		 var elementsLen=document.forms['toDoRule'].elements.length;
			for(i=0;i<=elementsLen-1;i++)
				{
					if(document.forms['toDoRule'].elements[i].type=='text')
						{
							document.forms['toDoRule'].elements[i].readOnly =true;
							document.forms['toDoRule'].elements[i].className = 'input-text form-control';
							
						}
					if(document.forms['toDoRule'].elements[i].type=='textarea')
					{
						document.forms['toDoRule'].elements[i].readOnly =true;
						document.forms['toDoRule'].elements[i].className = 'textarea remark-sec';
						
					}
					
						else
						{
							
							document.forms['toDoRule'].elements[i].disabled=true;
						}
	
					
				  
	
				}
			
				if(document.forms['toDoRule'].elements['Execute This Rule'])
				{
					document.forms['toDoRule'].elements['Execute This Rule'].disabled=false;
				}
			 </sec-auth:authComponent>
			
			var publish = '${toDoRule.publishRule}';
			var isAccount='${toDoRule.isAccount}';
			if(publish=='true'){
				var chkPublish ="0";
			 	<sec-auth:authComponent componentId="module.script.form.todoRulePublish">
			    	chkPublish = "1";
			 	</sec-auth:authComponent>
			 	if(chkPublish=="0"){
			 		var elementsLen=document.forms['toDoRule'].elements.length;
					for(i=0;i<=elementsLen-1;i++){
						if(document.forms['toDoRule'].elements[i].type=='text'){
							document.forms['toDoRule'].elements[i].readOnly =true;
							document.forms['toDoRule'].elements[i].className = 'input-text form-control';
						}
						if(document.forms['toDoRule'].elements[i].type=='textarea')
						{
							 document.forms['toDoRule'].elements[i].readOnly =true;
							document.forms['toDoRule'].elements[i].className = 'form-control'; 
							
						}
						else{									
							document.forms['toDoRule'].elements[i].disabled=true;
						}
					}
			 	}
			}
		
		}
	
	
	$(document).ready(function(){
		 var $j = jQuery.noConflict();
		var exapnded=false;
		$j(".tstbutton").on("click",function(){
			//alert("clicked");
			//console.log($(this).parent().parent().html());
			
			exapnded = $j(this).attr('aria-expanded');
			
			console.log(exapnded);
			
			if(exapnded == "true")
				$j(this).parent().parent().find('.isha').show()
			else
				$j(this).parent().parent().find('.isha').hide()
			
		});
		
		var isChecked =false;
		$j(".custom-control-input").on('change', function() {
		
		isChecked = this.checked;
		
		var id=this.id;
		
	   	if(isChecked)
			{
	   		document.forms['toDoRule'].elements['activestatus']=true;
			}
		else
			{
			document.forms['toDoRule'].elements['activestatus']=false;
			}
		  
	    	$j.ajax({
	    		
		  		 url: 'updateActionStatus.html?activestatus='+isChecked+'&actionId='+id+'&ruleId='+${toDoRule.id}+'&decorator=simple&popup=true',
		            success: function(data){
		            	console.log('success');
		                //location.reload();    
	            },
	            error: function () {
	               
	             }   
		});  
		
	});
	
	});

	
	function handleHttpResponseValues(){
		if (httpStatus.readyState == 4){
	}
	
	}
	
	


	
</script>
   </body>
</html>