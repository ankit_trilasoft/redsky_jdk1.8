package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;  
import com.trilasoft.app.model.NetworkPartners;

public interface NetworkPartnersManager extends GenericManager<NetworkPartners, Long>{

	public  List getCorpidList(String sessionCorpID);

	public List findNetworkPartnerList(String sessionCorpID, String selectedCorpId);

	public List findByCorpId(String sessionCorpID, String selectedCorpId);

	public Integer countNetworkPartnerByCorpId(String sessionCorpID, String selectedCorpId);

	public void removeByCorpId(String sessionCorpID, String selectedCorpId);

	public Integer countCompanyDivisionByCorpId(String sessionCorpID, String selectedCorpId);

}
