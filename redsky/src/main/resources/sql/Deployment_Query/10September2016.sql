ALTER TABLE `redsky`.`claim` ADD COLUMN `additionalbankinformation` VARCHAR(1000) NULL  AFTER `accountname` ;

alter table todorule 
add column service varchar(255), 
add mode varchar(255),
add routing  varchar(255),
add jobType  varchar(255),
add billToCode  varchar(255),
add bookingAgent  varchar(255) ,
add column serviceCondition varchar(15), 
add modeCondition varchar(15),
add routingCondition  varchar(15),
add jobTypeCondition varchar(15),
add billToCodeCondition  varchar(15),
add bookingAgentCondition  varchar(15);

CREATE TABLE  `redsky`.`taskchecklist` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `owner` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `corpID` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `durationAddSub` bigint(20) DEFAULT NULL,
  `fieldToValidate1` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `messagedisplayed` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `testdate` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `resultRecordId` bigint(20) DEFAULT NULL,
  `resultRecordType` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `todoRuleId` bigint(20) DEFAULT NULL,
  `duration` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `shipper` varchar(160) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rolelist` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `checkedUserName` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supportingId` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fieldDisplay` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fileNumber` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `isNotesAdded` bit(1) DEFAULT NULL,
  `noteType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteSubType` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fieldToValidate2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ruleNumber` bigint(20) DEFAULT NULL,
  `emailNotification` varchar(550) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailOn` datetime DEFAULT NULL,
  `manualEmail` varchar(55) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agentCode` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `billToName` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `recordToDel` varchar(5) COLLATE utf8_unicode_ci DEFAULT '0',
  `mailStauts` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reassignedOwner` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agentName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `myfileId` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completedBy` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `transferUser` varchar(82) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completedOn` datetime DEFAULT NULL,
  `transferDate` datetime DEFAULT NULL,
  `controlDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `corpIDidx` (`corpID`),
  KEY `Index_shipnumber` (`fileNumber`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `redsky`.`servicepartner` 
ADD COLUMN `subcarrierName` VARCHAR(255) AFTER `status`,
 ADD COLUMN `subcarrierCode` VARCHAR(8) AFTER `subcarrierName`,
 ADD COLUMN `subcarrierBooking` VARCHAR(45) AFTER `subcarrierCode`,
 ADD COLUMN `cost` VARCHAR(45) AFTER `subcarrierBooking`,
 ADD COLUMN `bookingdate` DATETIME AFTER `cost`;
 
 ALTER TABLE `redsky`.`company` ADD COLUMN `activityMgmtVersion2` bit(1) DEFAULT b'0' ;
 
 CREATE TABLE `intellead` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdBy` varchar(50) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `corpId` varchar(10) DEFAULT NULL,
  `updatedBy` varchar(50) DEFAULT NULL,
  `employeeName` varchar(50) DEFAULT NULL,
  `emplid` varchar(50) DEFAULT NULL,
  `assignmentName` varchar(50) DEFAULT NULL,
  `employeeEmailAddress` varchar(50) DEFAULT NULL,
  `supplierCode` varchar(50) DEFAULT NULL,
  `supplierName` varchar(50) DEFAULT NULL,
  `serviceCountryID` varchar(50) DEFAULT NULL,
  `serviceCountryName` varchar(50) DEFAULT NULL,
  `accountName` varchar(50) DEFAULT NULL,
  `accountDescription` varchar(50) DEFAULT NULL,
  `authID` varchar(50) DEFAULT NULL,
  `authDate` datetime DEFAULT NULL,
  `authType` varchar(50) DEFAULT NULL,
  `assignmentType` varchar(50) DEFAULT NULL,
  `assignmentStartDate` datetime DEFAULT NULL,
  `assignmentProjectedReturnDate` datetime DEFAULT NULL,
  `serviceStartDate` datetime DEFAULT NULL,
  `serviceProjectedEndDate` datetime DEFAULT NULL,
  `duration` varchar(50) DEFAULT NULL,
  `flexibility` varchar(50) DEFAULT NULL,
  `howMany` varchar(50) DEFAULT NULL,
   `howManyDesc` varchar(50) DEFAULT NULL,
  `limitAmount` varchar(50) DEFAULT NULL,
   `currencyCode` varchar(50) DEFAULT NULL,
  `homePhone` varchar(50) DEFAULT NULL,
  `workPhone` varchar(50) DEFAULT NULL,
  `otherPhone` varchar(50) DEFAULT NULL,
  `fromAddress1` varchar(50) DEFAULT NULL,
  `fromAddress2` varchar(50) DEFAULT NULL,
  `fromAddress3` varchar(50) DEFAULT NULL,
  `fromCity` varchar(50) DEFAULT NULL,
  `fromState` varchar(50) DEFAULT NULL,
  `fromPostalCode` varchar(50) DEFAULT NULL,
  `fromCountry` varchar(50) DEFAULT NULL,
  `toCityToState` varchar(50) DEFAULT NULL,
  `toCountry` varchar(50) DEFAULT NULL,
`comments` varchar(50) DEFAULT NULL,
`action` VARCHAR(50) DEFAULT NULL,
`customerFileNumber` VARCHAR(20) DEFAULT NULL,
`serviceOrderNumber` VARCHAR(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
)  AUTO_INCREMENT=0 DEFAULT ;

ALTER TABLE `redsky`.`refjobtype` 
ADD COLUMN `opsPerson` VARCHAR(82) NULL  AFTER `internalBillingPerson` ,
ADD COLUMN `personForwarder` VARCHAR(82) NULL  AFTER `opsPerson` ; 

ALTER TABLE `redsky`.`serviceorder` 
ADD COLUMN `estimateOverallMarkupPercentage` INT(11) NULL  AFTER `accrueChecked` ;

update refmaster set sequenceNo=0  where sequenceNo is null;
ALTER TABLE refmaster ALTER COLUMN  sequenceNo SET DEFAULT 0;
