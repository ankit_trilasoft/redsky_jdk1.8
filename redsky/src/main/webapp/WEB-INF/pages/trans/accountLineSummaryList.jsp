<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="accountLineDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineDetail.heading'/>"/> 
    <style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>  
<script type="text/javascript">
	function onLoad(){
	
	var sumRevenue=0;
	var sumExp=0;
    
    <c:forEach var="accountLine" items="${accountLines}">
      sumRevenue += ${accountLine.estimateRevenueAmount};
	  sumExp += ${accountLine.estimateExpense};
    </c:forEach >
	
	document.forms['serviceForm1'].elements['estimateRevenueAmt'].value = sumRevenue;
	document.forms['serviceForm1'].elements['estimateExp'].value = sumExp;
	document.forms['serviceForm1'].elements['grossMrgn'].value = sumRevenue - sumExp;
	document.forms['serviceForm1'].elements['gmPercent'].value = ((sumRevenue - sumExp)*100)/sumRevenue;
}

</script>  
</head>   

<s:form id="serviceForm1" action="updateSOfromaccountLine" method="post"> 
<div id="newmnav">
  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  <li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>
  
  <li style="background:#FFF "><a href="accountLineList.html?sid=${serviceOrder.id}" class="current" ><span>Accounting<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
   <li><a href="containers.html?id=${serviceOrder.id}"><span>Container</span></a></li>
  <c:if test="${serviceOrder.job !='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
   <c:if test="${serviceOrder.job =='INT'}">
   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
   </c:if>
   </sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  <li><a href="costings.html?id=${serviceOrder.id}"><span>Invoice</span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
</div><div class="spn">&nbsp;</div><br>

 <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" onfocus="myDate();"  size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

 
 <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  
  <li><a href="accountLineList.html?sid=${ServiceOrderID}"><span>Entitlement</span></a></li>
  <li ><a href="accountLineEstimateList.html?sid=${ServiceOrderID}" ><span>Estimate</span></a></li>
  <li><a href="accountLineRevisionList.html?sid=${serviceOrder.id}"><span>Revision</span></a></li>
 <li style="background:#FFF "><a href="accountLineSummaryList.html?sid=${serviceOrder.id}" class="current"><span>Summary<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
  
</ul>
</td></tr>
</tbody></table>
 
 
  <c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
  
  <c:set var="buttons">  

 
     <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editAccountLineEstimate.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
         
     
</c:set>
  
  
<s:set name="accountLineSummaryList" value="accountLineSummaryList" scope="request"/>
   
<display:table name="accountLineList" class="table" requestURI="" id="accountLineListRaj" pagesize="10" style="width:700px" >   
		 <display:column property="category" sortable="true" titleKey="accountLine.category" href="editAccountLineEstimate.html?sid=${ServiceOrderID}" paramId="id" paramProperty="id"/> 
		 <display:column property="chargeCode" sortable="true" titleKey="accountLine.chargeCode"/>
		 <display:column property="estimateVendorName" sortable="true" titleKey="accountLine.estimateVendorNames"/>
	     <display:column property="entitlementAmmount" sortable="true" titleKey="accountLine.entitlementAmmounts"/>
	     <display:column property="estimateExpense" sortable="true" titleKey="accountLine.estimateExpenses" />
	    <display:column property="estimateRevenueAmount" sortable="true" titleKey="accountLine.estimateRevenueAmounts" />
		 <display:column property="revisionExpense" sortable="true" titleKey="accountLine.revisionExpenses"/>
		 <display:column property="revisionRevenueAmount" sortable="true" titleKey="accountLine.revisionRevenueAmounts"/> 
		<display:column property="actualExpense" sortable="true" titleKey="accountLine.actualExpense"/>
		<display:column property="actualRevenue" sortable="true" titleKey="accountLine.actualRevenue"/>
</display:table> 
     
     <table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td width="100" align="right"><fmt:message key="serviceOrder.entitledTotalAmounted"/></td><td align="left" ><s:textfield name="serviceOrder.entitledTotalAmount"   size="7"  cssClass="input-textUpper" readonly="true"/><td align="left"><s:textfield name="serviceOrder.estimatedTotalExpanse"   size="7"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.estimatedTotalRevenu" cssClass="input-textUpper" size="7" readonly="true"/><td align="right"></td><td align="left" ><s:textfield name="serviceOrder.revisedTotalExpans"   size="7"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.revisedTotalRevenu" cssClass="input-textUpper" size="7" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.revisedTotalRevenu" cssClass="input-textUpper" size="7" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.revisedTotalRevenu" cssClass="input-textUpper" size="7" readonly="true"/>
		  	<tr>
		  	<td width="200" align="right"><fmt:message key="serviceOrder.estimatedGrossMargined"/></td><td align="left"><s:textfield name="serviceOrder.estimatedGrossMargin"  cssClass="input-textUpper" size="7" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.revisedGrossMargin"  cssClass="input-textUpper" size="7" readonly="true"/></td><td align="left"><td align="left"><td align="left"><td align="left"><td align="left"><td align="left"><s:textfield name="serviceOrder.revisedGrossMargin"  cssClass="input-textUpper" size="7" readonly="true"/></td></td></td></td></td></td>
		  	</tr>
		  	<tr>
		  	<td  width="200"align="right"><fmt:message key="serviceOrder.estimatedGrossMarginPercentageed"/></td><td align="left"><s:textfield name="serviceOrder.estimatedGrossMarginPercentage" cssClass="input-textUpper"  size="7" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.revisedGrossMarginPercentage" cssClass="input-textUpper"  size="7" readonly="true"/></td><td align="left"><td align="left"><td align="left"><td align="left"><td align="left"><td align="left"><s:textfield name="serviceOrder.revisedGrossMarginPercentage" cssClass="input-textUpper"  size="7" readonly="true"/></td></td></td></td></td></td>
		  	</tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

    
    <%-- 
    <input type="submit" value="Calc" onclick="onLoad()" />
    <s:hidden name="estimateRevenueAmt" />
    <s:hidden name="estimateExp" />
    <s:hidden name="grossMrgn" />
    <s:hidden name="gmPercent" />
    <c:out value="${buttons}" escapeXml="false" />
     --%>
    
    
    <s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
   
</s:form>

<script type="text/javascript">   
    highlightTableRows("accountLineEstimateList");  
    Form.focusFirstElement($("serviceForm1")); 
	
</script>  
