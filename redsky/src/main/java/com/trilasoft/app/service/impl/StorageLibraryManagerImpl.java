package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.StorageLibraryDao;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.service.StorageLibraryManager;

public class StorageLibraryManagerImpl extends GenericManagerImpl<StorageLibrary,Long> implements StorageLibraryManager {
	StorageLibraryDao storageLibraryDao;
	public StorageLibraryManagerImpl(StorageLibraryDao storageLibraryDao) {
		super(storageLibraryDao);
        this.storageLibraryDao=storageLibraryDao;
	}
	public List storageLibrarySearch(String storageId, String storageType,String storageMode) {
		return storageLibraryDao.storageLibrarySearch(storageId,storageType,storageMode);
	}
	public List storageLibraryList(String sessionCorpID) {
		return storageLibraryDao.storageLibraryList(sessionCorpID);
	}
	public List findStorageLibraryList(String sessionCorpID) {
		return storageLibraryDao.findStorageLibraryList(sessionCorpID);
	}
	public List storageLibraryUnitSearch(String storageId, String storageType,String storageMode) {
		return storageLibraryDao.storageLibraryUnitSearch(storageId,storageType,storageMode);
	}
	public StorageLibrary getByStorageId(String storageId,String sessionCorpID){
		return storageLibraryDao.getByStorageId(storageId, sessionCorpID);
	}
	public List findStoLocRearrangeList(String sessionCorpID) {
		return storageLibraryDao.findStoLocRearrangeList(sessionCorpID);
	}
	public List searchStoLocRearrangeList(String sessionCorpID,	String locationId,String ticket) {
		return storageLibraryDao.searchStoLocRearrangeList(sessionCorpID,locationId,ticket);
	}
}
