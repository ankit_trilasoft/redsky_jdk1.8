package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.DsRelocationExpenseManagementDao;
import com.trilasoft.app.model.DsRelocationExpenseManagement;
import com.trilasoft.app.service.DsRelocationExpenseManagementManager;

public class DsRelocationExpenseManagementManagerImpl extends GenericManagerImpl<DsRelocationExpenseManagement, Long> implements DsRelocationExpenseManagementManager {
	
	DsRelocationExpenseManagementDao dsRelocationExpenseManagementDao;
	
	public DsRelocationExpenseManagementManagerImpl(DsRelocationExpenseManagementDao dsRelocationExpenseManagementDao) {
		super(dsRelocationExpenseManagementDao); 
        this.dsRelocationExpenseManagementDao = dsRelocationExpenseManagementDao; 
	}
	public List getListById(Long id) {
		return dsRelocationExpenseManagementDao.getListById(id);
	}
}
