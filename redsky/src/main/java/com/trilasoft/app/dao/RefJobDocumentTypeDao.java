package com.trilasoft.app.dao;

import java.util.List;
import java.util.Set;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.RefJobDocumentType;

public interface RefJobDocumentTypeDao extends GenericDao<RefJobDocumentType, Long> { 
	 public List findRefJobDocumentList(String corpID);   
	 public List searchRefJobDocument(String jobType,String corpID);
	 public String getAllDoument(String jobType,String corpID);
}