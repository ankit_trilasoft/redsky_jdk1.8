package com.trilasoft.app.service;

import java.math.BigDecimal;
import java.util.List;
import org.appfuse.service.GenericManager;

import com.trilasoft.app.dao.hibernate.OperationsHubLimitsDaoHibernate.HubLimitDTO;
import com.trilasoft.app.model.OperationsHubLimits;

public interface OperationsHubLimitsManager  extends GenericManager<OperationsHubLimits, Long> {
	
	public List isExisted(String hub, String sessionCorpID);

	public List<OperationsHubLimits> getHub(String hubID);
	
	public List getAllRecord(String sessionCorpID);	
	
	public List findListByHub(String sessionCorpID);	
	
	public void updateDailyOpHubLimitParent(Integer tempHubLimit,Integer tempHubLimitOld,Integer tempHubLimitNew, String sessionCorpID,String hubID);
	
	public void updateDailyMaxEstWtParent(BigDecimal tempdailyMaxEstWt,BigDecimal dailyMaxEstWtOld,BigDecimal dailyMaxEstWtNew, String sessionCorpID,String hubID);

	public void updateDailyMaxEstVolParent(BigDecimal tempdailyMaxEstVol,BigDecimal dailyMaxEstVolOld,BigDecimal dailyMaxEstVolNew, String sessionCorpID,String hubID);
	
	public List findUnassignedParentValue(String sessionCorpID,String wareHouse);
	public void updateDailyDefLimit(String sessionCorpID,String dailyOpHubLimitPC);
	public void updateMinServDay(String sessionCorpID,String minServiceDayHub);
	public void updateCrewLimitParent(Integer tempCrewLimit,Integer tempCrewValueOld,Integer tempCrewValueNew, String sessionCorpID,String hubID);
	public void updateCrewDailyLimit(String sessionCorpID,String dailyCrewLimit);
	public void updateCrewThreshHoldLimit(String sessionCorpID,String dailyCrewThreshHoldLimit);
}
