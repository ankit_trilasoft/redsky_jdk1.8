<%@ include file="/common/taglibs.jsp"%>  

 <head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading'/>"/>   

    </head>
    <s:form id="childOrderListPage" action="updateMasterOrder" method="post" validate="true"> 
    <c:set var="updateButtons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px;" align="top"  key="button.save" onclick="return checkAvaliableContainer();"/>   
    <input type="button" class="cssbutton1" value="Cancel" style="width:55px; height:25px;" onclick="self.close();"/> 
</c:set>  
 <s:hidden name="newIdList" value=""/>
  <s:hidden name="oldIdList" value=""/>
    <s:hidden name="masterGroupStatus" />
    <s:hidden name="defaulVolUnit"/>
 <s:hidden name="masterID" value="<%=request.getParameter("id") %>"/>
<s:hidden name="updateRecords" value=""/>  
 
 
 
 <display:table name="childOrdersList" class="table" requestURI=""  id="childOrdersList"   pagesize="25" style="width:100%;margin-bottom:0px" > 
<display:column title="Row Number">

<c:out value="${childOrdersList_rowNum}"/>

</display:column>
<display:column title="S/O#" >
<a onclick=window.open('editServiceOrderUpdate.html?id=${childOrdersList.id}',"width=250,height=70,scrollbars=1");>
<c:out value="${childOrdersList.shipNumber}"/>
</a>
</display:column>
<display:column title="Last/Company Name" property="lastName" />
<display:column title="First Name" property="firstName" />
<display:column title="Dest. City" property="destinationCity" />
<display:column title="Dest. State" property="destinationState" />
<display:column title="Dest. Country" property="destinationCountry" />
<display:column title="Pref. Delivery" property="grpDeliveryDate" />
<display:column title="Volume (E/A)" style="text-align:right;" sortProperty="volume">
<c:choose>
<c:when test="${childOrdersList.weight1!=''}">
<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0" value="${childOrdersList.weight1}" />
<font color="red">(A)</font>
</c:when>
<c:when test="${childOrdersList.weight1=='' && childOrdersList.weight2!=''}">
<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="0" value="${childOrdersList.weight2}" />
<font color="red">(E)</font>
</c:when>
</c:choose>
</display:column>
<display:column title="Select">

      <c:if test="${childOrdersList.grpStatus=='draft' || childOrdersList.grpStatus=='Draft'}">
         <input type="checkbox" id="${childOrdersList.id}" checked="checked" onclick="confirmUser('${childOrdersList.volume}',this);consolidateGroup(this,'${childOrdersList.grpStatus}');"  />
      </c:if>
      <c:if test="${childOrdersList.grpStatus=='finalized' || childOrdersList.grpStatus=='Finalized'}">
         <input type="checkbox" id="${childOrdersList.id}"  checked="checked" disabled="disabled"/>
      </c:if>
      </display:column>
</display:table>
 
 
   <table class="detailTabLabel">
   <tr></tr>
     <tr></tr>
<tr>
<td class="listwhitetext">Groupage Order#</td>
<td class="listwhitetext">Container</td>
<td class="listwhitetext">Available</td>
<td class="listwhitetext">Used</td>
</tr>

<c:if test="${masterGroupStatus=='draft' || masterGroupStatus=='Draft'}">
<tr>
<td><s:textfield name="masterShipNo"  cssClass="input-textUpper" required="true"  cssStyle="width:100px" size="15" readonly="true"/></td>
<td><s:select list="%{EQUIP}" name="containerSize" id="containerSizeId" headerKey="" headerValue="" onchange="getNewVolume();" cssClass="list-menu" cssStyle="min-width:105px;"></s:select></td>
<td><s:textfield name="avaliableVolume" id="avaliableVolumeId" cssStyle="width:80px" cssClass="input-textUpper" readonly="true"></s:textfield></td>
<td><s:textfield name="usedVolume" id="usedVolumeId"  cssStyle="width:80px" cssClass="input-textUpper" readonly="true"></s:textfield></td>
</tr>
<tr></tr>
<tr>
<td  align="left" colspan="2">
			    <c:out value="${updateButtons}" escapeXml="false" />   
			</td>
</tr>
</c:if>
<c:if test="${masterGroupStatus=='finalized' || masterGroupStatus=='Finalized'}">
<tr>
<td><s:textfield name="masterShipNo"  cssClass="input-textUpper" required="true"  cssStyle="width:100px" size="15" readonly="true"/></td>
<td><s:textfield name="containerSize"  cssClass="input-textUpper" cssStyle="min-width:105px;" readonly="true"></s:textfield></td>
<td><s:textfield name="avaliableVolume" id="avaliableVolumeId" cssClass="input-textUpper" cssStyle="width:80px" readonly="true"></s:textfield></td>
<td><s:textfield name="usedVolume" id="usedVolumeId" cssClass="input-textUpper" cssStyle="width:80px" readonly="true"></s:textfield></td>
<tr></tr>
<tr>
<td><input type="button" class="cssbutton1" value="Close" style="width:55px; height:25px;" onclick="self.close();"/> </td>
</tr>
</tr>
</c:if>
<tr></tr>

</table>
</s:form>

<script language="javascript" type="text/javascript">
function confirmUser(targetVolume,targetElement){
     var targetId=targetElement.id;
     if(targetElement.checked==false){ 
	      var agree=confirm("You are about to remove this service order from a group. Do you wish to continue?");
	      if(agree){
	        updateUsedVolume(targetVolume,targetElement);
	      }
	      else{
	       	document.getElementById(targetId).checked='true';
	      }
      }
      else{
  		 updateUsedVolume(targetVolume,targetElement);
      }
}
    
function consolidateGroup(target,grpStatus){
      var targetId=target.id;
      if((target.checked==false) && (grpStatus=='Draft' || grpStatus=='draft' )){
        var flag=document.forms['childOrderListPage'].elements['oldIdList'].value;
        if(flag==''){
        	 flag=targetId;
        }
        else{
         	 flag = flag + ',' + targetId;
        }
       document.forms['childOrderListPage'].elements['oldIdList'].value=flag;
     }
    if((target.checked==true) && (grpStatus=='Draft' || grpStatus=='draft' )){
    	var flag1=document.forms['childOrderListPage'].elements['oldIdList'].value;
    
   		if(flag1.indexOf(targetId)>-1){
		    flag1=flag1.replace(targetId,"");
		    flag1=flag1.replace(",,",",");
		    var len=flag1.length-1;
		   
		    if(len==flag1.lastIndexOf(",")){
		    	flag1=flag1.substring(0,len);
		    }
		    if(flag1.indexOf(",")==0){
		    	flag1=flag1.substring(1,flag1.length);
		    }
	   	    document.forms['childOrderListPage'].elements['oldIdList'].value=flag1;
	    }
    }
    else if((target.checked==true) && (grpStatus=='Unassigned' || grpStatus=='unassigned' || grpStatus=='' )){
	    var newIds=document.forms['childOrderListPage'].elements['newIdList'].value;
	    if(newIds==''){
	    	newIds=targetId;
	    }
	    else{
	   		newIds=newIds + ',' + targetId;
	    }
	    document.forms['childOrderListPage'].elements['newIdList'].value=newIds;
    }
    else if((target.checked==false) && (grpStatus=='Unassigned' || grpStatus=='unassigned' || grpStatus=='' )){
	    var newIds=document.forms['childOrderListPage'].elements['newIdList'].value;
	    if(newIds.indexOf(targetId)>-1){
		    newIds=newIds.replace(targetId,"");
		    newIds=newIds.replace(",,",",");
		    var len=newIds.length-1;
		    if(len==newIds.lastIndexOf(",")){
		    	newIds=newIds.substring(0,len);
		    }
		    if(newIds.indexOf(",")==0){
		    	newIds=newIds.substring(1,newIds.length);
		    }
	    	document.forms['childOrderListPage'].elements['newIdList'].value=newIds;
	    }
    }
}
    
function updateUsedVolume(targetVolume,targetElement){
   var usedVol=document.forms['childOrderListPage'].elements['usedVolume'].value;
   var availVol=document.forms['childOrderListPage'].elements['avaliableVolume'].value;
   var total;
   if(targetVolume==''){
   		targetVolume=0;
   }
   if(targetElement.checked==false){
	   total=parseFloat(usedVol)-parseFloat(targetVolume);
	   availVol=parseFloat(availVol)+parseFloat(targetVolume);
	   total=parseFloat(total).toFixed(2);
	   availVol=parseFloat(availVol).toFixed(2);
	   document.forms['childOrderListPage'].elements['usedVolume'].value=total;
	   document.forms['childOrderListPage'].elements['avaliableVolume'].value=availVol;
   }
   else{
	   total=parseFloat(usedVol)+parseFloat(targetVolume);
	   availVol=parseFloat(availVol)-parseFloat(targetVolume);
	   total=parseFloat(total).toFixed(2);
	   availVol=parseFloat(availVol).toFixed(2);
	   if(parseFloat(availVol)>0){
		   document.forms['childOrderListPage'].elements['usedVolume'].value=total;
		   document.forms['childOrderListPage'].elements['avaliableVolume'].value=availVol;
	   }else{
		   var targetId=targetElement.id;
		   document.getElementById(targetId).checked=false;
		   alert("Volume of selected service orders exceeds maximum capacity of the choosen container. Please make an adjustment.");
	   }
   }
}
function getHttpObject(){
   var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
//Use this method to pick multiple list
   /*
   function GetSelectedItem() {

	   len = document.forms['childOrderListPage'].elements['containerSize'].length;
	   i = 0
	   chosen = ""

	   for (i = 0; i < len; i++) {
	   if (document.forms['childOrderListPage'].containerSize[i].selected) {
		   if(chosen=="")
		   {
			   chosen=document.forms['childOrderListPage'].containerSize[i].value;
		   }else{
	   		   chosen = chosen+","+document.forms['childOrderListPage'].containerSize[i].value;
		   }
	   }
	   }

	   return chosen
	   }*/ 
var http1=getHttpObject();
function getNewVolume(){
   var defaultVol=document.forms['childOrderListPage'].elements['defaulVolUnit'].value;
   var containerSize=document.forms['childOrderListPage'].elements['containerSize'].value;
   var url="findNewVolume.html?ajax=1&decorator=simple&popup=true&defaulVolUnit="+encodeURI(defaultVol)+"&containerSize="+encodeURI(containerSize);
   http1.open("GET",url,true);
   http1.onreadystatechange=httpResponseNewVolume;
   http1.send(null);
}
function httpResponseNewVolume(){
   if(http1.readyState==4){
     var result=http1.responseText
     var usedVol=document.forms['childOrderListPage'].elements['usedVolume'].value;
     
     result=result.trim();
     document.forms['childOrderListPage'].elements['avaliableVolume'].value=result;
     if(usedVol!=''){
     var newVol=document.forms['childOrderListPage'].elements['avaliableVolume'].value;
	     if(newVol!=''){
		     var total=parseFloat(newVol)-parseFloat(usedVol);
		     total=parseFloat(total).toFixed(2);
		     document.forms['childOrderListPage'].elements['avaliableVolume'].value=total;
	     }
	     else{
		     total=0-parseFloat(usedVol);
		     total=parseFloat(total).toFixed(2);
		     document.forms['childOrderListPage'].elements['avaliableVolume'].value=total;
	     }
     }
   }
}
function checkAvaliableContainer(){
  var sizeValue=document.getElementById("containerSizeId").value;
  var availVolume=document.forms['childOrderListPage'].elements['avaliableVolume'].value;
  var usedVolume=document.forms['childOrderListPage'].elements['usedVolume'].value;
  if(sizeValue==''){
     alert("Please select a container size before save.");
     return false;
  }
  else{
  if(availVolume<0){
	   alert("You cannot save as the used volume exceeds avaliable volume. Please make an adjustment.");
   	   return false;
   }
  else{
/*
  var agree=confirm("Would you like to update the same information for all service orders for this groupage order?  Click Ok to confirm or Cancel to update information for master service order only.");
  if(agree){
  document.forms['childOrderListPage'].elements['updateRecords'].value='updateAll';
  }
  else{*/
  document.forms['childOrderListPage'].elements['updateRecords'].value='updateCurrent';
 /* }*/

  return true;
  }}
   }
</script>