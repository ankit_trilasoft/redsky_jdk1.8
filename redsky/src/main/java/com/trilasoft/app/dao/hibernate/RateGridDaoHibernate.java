package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.RateGridDao;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.model.RateGrid;

public class RateGridDaoHibernate extends GenericDaoHibernate<RateGrid,Long>implements RateGridDao{

	public RateGridDaoHibernate() {
		super(RateGrid.class);
		
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from RateGrid");
	}
	public List findGridIds(String charge, String contractName,String sessionCorpID){
		return getHibernateTemplate().find("select id from RateGrid where charge='"+charge+"' and contractName='"+contractName+"' and corpID='"+sessionCorpID+"'");
	}
	public List getRates(String chid, String contractName,String sessionCorpID){
			return getHibernateTemplate().find("select quantity2,quantity1,rate1,buyRate from RateGrid where charge='"+chid+"' and contractName='"+contractName+"' and corpID='"+sessionCorpID+"' order by quantity2,quantity1");
	}
	public int findTwoDNumber(String chargeID, String sessionCorpID){
		String temp= getHibernateTemplate().find("select count(*) from RateGrid where charge='"+chargeID+"' and quantity2 is not null and  quantity2<>'' and quantity2 <> 0.0  and corpID='"+sessionCorpID+"' ").get(0).toString();
		int count = Integer.parseInt(temp);
		return count;
	}
	public List findSaleCommissionRateGrid(String corpId,String contract,String compDiv,String job){
		String contracts="";
		if("".equals(compDiv) || compDiv==null)
			compDiv="CMS";
		List contractList = this.getSession().createSQLQuery("select contract from contract where companyDivision='"+compDiv+"' and corpId='"+corpId+"' and internalCostContract is  true").list();
		if(contractList != null){
			contracts=contractList.toString().replace("[","'").replace("]","'").replaceAll(",","','");
		}
		String query="select r.id,r.quantity1,r.rate1"+
		 " from charges ch, contract c,rategrid r"+
		" where ch.contract=c.contract and r.contractName=c.contract and c.contract in ("+contracts+")"+
		" and r.corpId=ch.corpId and c.corpId = ch.corpId and ch.corpId='"+corpId+"'"+
		" and r.charge=ch.id"+
		 " and ch.bucket= '"+job+"'";
		
		String idList="";
		List list = this.getSession().createSQLQuery(query).list();
		Iterator it = list.iterator();
		while (it.hasNext()){
			Object []obj=(Object[]) it.next();
			if(idList.equalsIgnoreCase("")){
				idList=obj[0].toString();
			}else{
				idList=idList+","+obj[0].toString();
			}
		}
		List al=new ArrayList();
		if(!idList.equals("")){
			al=getSession().createQuery("from RateGrid where id in ("+idList+")").list();
		}
		return al;
	}
	public String findSaleCommissionContractAndCharge(String compDiv,String bucket, String sessionCorpID){
		if("".equals(compDiv) || compDiv==null)
			compDiv="CMS";
		String temp= "select concat(ch.contract,'~',ch.charge) from contract c,charges ch"+
					" where c.contract= ch.contract"+
					" and c.corpId=ch.corpId"+
					" and c.corpId='"+sessionCorpID+"'"+
					" and ch.bucket='"+bucket+"'"+
					" and c.internalCostContract is true"+
					" and c.companyDivision='"+compDiv+"'";
		
		List list = this.getSession().createSQLQuery(temp).list();
		if(list != null && list.size() > 0 && list.get(0) != null && !"".equals(list.get(0).toString().trim())){
			return list.get(0).toString().trim();
		}
		return "";
	}
	
}
