package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;

import java.util.List;

import com.trilasoft.app.model.AccountAssignmentType;
import com.trilasoft.app.model.TableCatalog;

public interface AccountAssignmentTypeManager extends GenericManager<AccountAssignmentType, Long>{
	
	public List getaccountAssignmentTypeReference(String partnerCode, String corpID,Long parentId);
	public List findAssignmentByParentAgent(String corpId,String parentAgent);
	public List getAssignmentTypeList(String partnerCode,Long partnerId, String corpId);
	public List checkAssignmentDuplicate(String partnerCode, String corpId,String assignment);
}
