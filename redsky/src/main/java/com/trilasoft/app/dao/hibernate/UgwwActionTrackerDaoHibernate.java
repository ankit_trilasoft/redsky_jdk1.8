package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.UgwwActionTrackerDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DtoAccountPayablePost;
import com.trilasoft.app.dao.hibernate.IntegrationLogDaoHibernate.IntegrationLogDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.IntegrationLogInfoDTO;
import com.trilasoft.app.model.UgwwActionTracker;

public class UgwwActionTrackerDaoHibernate extends
		GenericDaoHibernate<UgwwActionTracker, Long> implements
		UgwwActionTrackerDao {
	private HibernateUtil hibernateUtil;

	public UgwwActionTrackerDaoHibernate() {
		super(UgwwActionTracker.class);
		// TODO Auto-generated constructor stub
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		   this.hibernateUtil = hibernateUtil;
	}

	String statusDateNew;

	public List getAllugwwactiontrackeList() {

		List uList = this.getSession().createQuery(" from UgwwActionTracker")
				.list();

		return uList;

	}

	public List findId(String sequenceNumber,String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from customerfile where sequenceNumber like'%"+sequenceNumber+"%' and corpid= '"+corpId+"' and controlflag='C'").list();
								
		if(list ==null || list.isEmpty()){

			list = this
					.getSession()
					.createSQLQuery("select distinct s.customerFileId from serviceorder s,trackingstatus t where s.id =t.id and s.controlflag='C' and s.corpid= '"+corpId+"' and (s.bookingAgentShipNumber like '"+sequenceNumber+"%' or t.originAgentExSO like '"+sequenceNumber+"%' or t.networkAgentExSO like '"+sequenceNumber+"%' or t.originSubAgentExSO like '"+sequenceNumber+"%' or t.destinationSubAgentExSO like '"+sequenceNumber+"%' or t.bookingAgentExSO like '"+sequenceNumber+"%' or t.bookingAgentExSO like '"+sequenceNumber+"%' or t.destinationAgentExSO like '"+sequenceNumber+"%')").list();
			
		
		}
		return list;

	}

	public List findsoId(String sequenceNumber, String corpId) {

		List list = this
				.getSession()
				.createSQLQuery("select id from serviceorder where sequenceNumber like'%"+sequenceNumber+"' and corpid= '"+corpId+"' ").list();
						
								
		return list;

	}

	public List findRateRequestId(String sequenceNumber , String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from customerfile where sequenceNumber like'%"+sequenceNumber+"' and corpid= '"+corpId+"' ").list();
								
		return list;

	}

	public List findAccountPolicyUrlId(String sequenceNumber  , String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id,billToCode from customerfile where sequenceNumber like'%"+sequenceNumber+"' and corpid= '"+corpId+"' ").list();
							
		return list;

	}

	public List findFormPageUrlId(String sequenceNumber  , String corpId) {
		List list = this
				.getSession()
				.createSQLQuery(
						"select id,sequenceNumber,billToCode,job,companydivision from customerfile where sequenceNumber like'%"+sequenceNumber+"' and corpid= '"+corpId+"' ").list();
							
		return list;
	}
	
	public List findsodId(String shipNumber , String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"'and controlflag='C' ").list();
		if(list==null || list.isEmpty()){
			list = this
					.getSession()
					.createSQLQuery("select s.id from serviceorder s,trackingstatus t where s.id =t.id and s.controlflag='C' and s.corpid= '"+corpId+"'  and (s.bookingAgentShipNumber like '"+shipNumber+"%' or t.originAgentExSO like '"+shipNumber+"%' or t.networkAgentExSO like '"+shipNumber+"%' or t.originSubAgentExSO like '"+shipNumber+"%' or t.destinationSubAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.destinationAgentExSO like '"+shipNumber+"%')").list();
			
		}
								
		return list;

	}
	
	public List findAccountLineId(String shipNumber,String corpId,Boolean accessQFromCF) {
		List list=null;
		if(accessQFromCF){
			list = this
			.getSession()
			.createSQLQuery(
					"select id from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"' and controlflag='C' and (moveType='BookedMove' or moveType ='' or moveType is NULL)").list();
		}else{
		list = this
				.getSession()
				.createSQLQuery(
						"select id from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"' and controlflag='C'").list();
		}
		if(list ==null || list.isEmpty()){
			if(accessQFromCF){
				list = this
						.getSession()
						.createSQLQuery("select s.id from serviceorder s,trackingstatus t where s.id =t.id and s.controlflag='C' and (moveType='BookedMove' or moveType ='' or moveType is NULL) and s.corpid= '"+corpId+"' and (s.bookingAgentShipNumber like '"+shipNumber+"%' or t.originAgentExSO like '"+shipNumber+"%' or t.networkAgentExSO like '"+shipNumber+"%' or t.originSubAgentExSO like '"+shipNumber+"%' or t.destinationSubAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.destinationAgentExSO like '"+shipNumber+"%')").list();
				
			}else{
			
			list = this
					.getSession()
					.createSQLQuery("select s.id from serviceorder s,trackingstatus t where s.id =t.id and s.controlflag='C'  and s.corpid= '"+corpId+"' and (s.bookingAgentShipNumber like '"+shipNumber+"%' or t.originAgentExSO like '"+shipNumber+"%' or t.networkAgentExSO like '"+shipNumber+"%' or t.originSubAgentExSO like '"+shipNumber+"%' or t.destinationSubAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.bookingAgentExSO like '"+shipNumber+"%' or t.destinationAgentExSO like '"+shipNumber+"%')").list();
			
			}
		}
		return list;

	}
	
	public List findFormPageId(String shipNumber ,String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id,shipNumber,registrationNumber,companyDivision,job,mode,billToCode from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"'").list();
							
		return list;

	}
	
	public List findServiceAuditPageId(String shipNumber ,String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"' ").list();
								
		return list;

	}
	public List findCustID(String shipNumber,String corpId){
		List list = this
		.getSession()
		.createSQLQuery(
				"select customerfileid from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"' ").list();
						
return list;
		
	}
	public List getTrackerList(String soNumber, String action, String userName, Date actionTime, String corpID){
		List<UgwwActionTracker> uList =null;
		if (actionTime != null) {	
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(actionTime) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				
				e.printStackTrace();
			}
		
		 uList = this.getSession()
		.createQuery(" from UgwwActionTracker where sonumber like '%"+soNumber+"%' AND action like '%"+action+"%' AND  username like '%"+userName+"%' AND actiontime like '%"+statusDateNew+"%' AND corpid like '%"+corpID+"%'")
		.list();
		}
		if (actionTime == null) {
			
		 uList = this.getSession()
			.createQuery(" from UgwwActionTracker where sonumber like '%"+soNumber+"%' AND action like '%"+action+"%' AND  username like '%"+userName+"%'  AND corpid like '%"+corpID+"%'")
			.list();
		}
		return uList;

		 
	 }
	public List findQuotesID(String sequencenumber,String corpId){
		List list = this
		.getSession()
		.createSQLQuery(
				"select id,controlflag from customerfile where sequencenumber like'%"+sequencenumber+"' and corpid= '"+corpId+"' ").list();
						
return list;
		
	}
	public List findServiceQuotesID(String shipNumber,String corpId){
		List list = this
		.getSession()
		.createSQLQuery(
				"select id from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"'and controlflag='Q' ").list();
						
return list;
		
	}
	
	public List findPartnerID(String shipNumber,String corpId){
		List list = this
		.getSession()
		.createSQLQuery(
				"select customerfileid from serviceorder where shipnumber like'%"+shipNumber+"' and corpid= '"+corpId+"'and controlflag='Q' ").list();
						
return list;
		
	}
	public List getPartner(String partnerCode, String sessionCorpID){
	
	
		List list = this.getSession()
		.createSQLQuery(
				"select id,partnercode,isagent,isprivateparty,isaccount,isvendor,isownerop,iscarrier from partnerpublic where partnerCode like'%"+partnerCode+"%' and corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')").list();
	
		return list;
		
	}
	public List findTicketDetailsId(String searchstr, String sessionCorpID){

		List list = this.getSession()
		.createSQLQuery(
				"select id from workticket where ticket like'%"+searchstr+"' and corpid= '"+sessionCorpID+"'").list();
		return list;
	
	
		
	}
	public List findClaimDetailsId(String searchstr, String sessionCorpID){
		List list = this.getSession()
		.createSQLQuery(
				"select id from claim where claimNumber like'%"+searchstr+"' and corpid= '"+sessionCorpID+"'").list();
		return list;
	}
	
	public List findQuotesList(String searchstr, String sessionCorpID){
		List list = this
		.getSession()
		.createSQLQuery(
				"select customerfileid from serviceorder where sequencenumber like'%"+searchstr+"' and corpid= '"+sessionCorpID+"'and controlflag='Q' ").list();
						
return list;
	}
	public List findQuotesListId(String searchstr, String sessionCorpID){
		List list = this
		.getSession()
		.createSQLQuery(
				"select id from serviceorder where customerfileid like'%"+searchstr+"' and corpid= '"+sessionCorpID+"'and controlflag='Q' ").list();
						
return list;
	}
	public List findOrderId(String sequenceNumber,String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from customerfile where sequenceNumber like'%"+sequenceNumber+"%' and corpid= '"+corpId+"' and controlflag='A' and OrderIntiationStatus ='Submitted' ").list();
								
		// customerfile where sequenceNumber like '"+corpID+"%' and corpID in ('"+corpID+"')").list();
		return list;

	}

	@SuppressWarnings("unchecked")
	public List<IntegrationLogInfoDTO> getErrorLogList(String integrationId,String operation, String orderComplete,Date effectiveDate, String statusCode,String so,String corpID ) {
		List<IntegrationLogInfoDTO> uList = new ArrayList<IntegrationLogInfoDTO>();
		List list=null;
		String sql="";
		if (effectiveDate != null) {
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(effectiveDate) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
			 if(so!=null && !so.equals(""))	{
				 sql="select s.ugwIntId,i.operation,i.orderComplete,i.integrationId,i.effectiveDate,i.statuscode,"+
					" i.detailedStatus,i.source,i.destination,nc.sonumber from integrationloginfo i, serviceorder s,networkcontrol nc where i.sourceordernum = nc.sourcesonum and i.sourceorderNum = s.shipnumber "+
					" and operation like '%"+operation+"%' AND orderComplete like '%"+orderComplete+"%' AND "+
					" s.ugwIntId like '%"+integrationId+"%' and nc.sonumber<>'' AND effectiveDate like '%"+statusDateNew+"%' AND  statusCode like '%"+statusCode+"%' and nc.sonumber like '%"+so+"%'";
				 
			 }else{
		 	sql="select s.ugwIntId,i.operation,i.orderComplete,i.integrationId,i.effectiveDate,i.statuscode,"+
				" i.detailedStatus,i.source,i.destination,nc.sonumber from integrationloginfo i, serviceorder s,networkcontrol nc where i.sourceordernum = nc.sourcesonum and i.sourceorderNum = s.shipnumber "+
				" and operation like '%"+operation+"%' AND orderComplete like '%"+orderComplete+"%' AND "+
				" s.ugwIntId like '%"+integrationId+"%' and nc.sonumber<>'' AND effectiveDate like '%"+statusDateNew+"%' AND  statusCode like '%"+statusCode+"%'";
			 }
			 }else{
				 if(so!=null && !so.equals(""))	{
					 sql="select s.ugwIntId,i.operation,i.orderComplete,i.integrationId,i.effectiveDate,i.statuscode,"+
						" i.detailedStatus,i.source,i.destination,nc.sonumber from integrationloginfo i, serviceorder s ,networkcontrol nc where i.sourceordernum = nc.sourcesonum and i.sourceorderNum = s.shipnumber "+
						" and operation like '%"+operation+"%' AND orderComplete like '%"+orderComplete+"%' AND "+
						" s.ugwIntId like '%"+integrationId+"%' and nc.sonumber<>'' AND  statusCode like '%"+statusCode+"%' and nc.sonumber like '%"+so+"%'";	 
				 }else{
				 sql="select s.ugwIntId,i.operation,i.orderComplete,i.integrationId,i.effectiveDate,i.statuscode,"+
				" i.detailedStatus,i.source,i.destination,nc.sonumber from integrationloginfo i, serviceorder s ,networkcontrol nc where i.sourceordernum = nc.sourcesonum and i.sourceorderNum = s.shipnumber "+
				" and operation like '%"+operation+"%' AND orderComplete like '%"+orderComplete+"%' AND "+
				" s.ugwIntId like '%"+integrationId+"%' and nc.sonumber<>'' AND  statusCode like '%"+statusCode+"%'";
				 }
				 }
			if(!"TSFT".equals(corpID)){
				sql = sql +" and i.corpid ='"+corpID+"' and nc.corpid='"+corpID+"' ";
			}
		    list = this.getSession().createSQLQuery(sql).list();
		    
		    Iterator it = list.iterator();
		    while(it.hasNext()){
		    	IntegrationLogInfoDTO dto = new IntegrationLogInfoDTO();
		    	Object[] obj = (Object[]) it.next();
		    	
		    	if(obj[0]!=null)
		    	dto.setRegistrationNumber(obj[0].toString());
		    	
		    	if(obj[1]!=null)
		    	dto.setOperation(obj[1].toString());
		    	
		    	if(obj[2]!=null)
		    	dto.setOrderComplete(obj[2].toString());
		    	
		    	if(obj[3]!=null)
			    	dto.setIntegrationId(obj[3].toString());
		    	
		    	if(obj[4]!=null)
		    	dto.setEffectiveDate((Date) obj[4]);
		    	
		       	if(obj[5]!=null)
		    	dto.setStatusCode(obj[5].toString());
		       	
		    	if(obj[6]!=null)
		    	dto.setDetailedStatus(obj[6].toString());
		    	
		    	if(obj[7]!=null)
			    	dto.setSource(obj[7].toString());
		    	
		    	if(obj[8]!=null)
			    	dto.setDestination(obj[8].toString());
		    	if(obj[9]!=null)
			    	dto.setsO(obj[9].toString());
		    	
		    	uList.add(dto);
		
		    }
		return uList;
	}
	
	@SuppressWarnings("unchecked")
	public List<IntegrationLogInfoDTO> getLisaLogList(String integrationId,String operation, String orderComplete,Date effectiveDate, String statusCode,String registrationNumber,String corpID ) {
		List<IntegrationLogInfoDTO> uList = new ArrayList<IntegrationLogInfoDTO>();
		List list=null;
		String sql="";
		if (effectiveDate != null) {
			try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(effectiveDate) );
		        statusDateNew = nowYYYYMMDD.toString();
		        
		 	}catch (Exception e) {
				e.printStackTrace();
			}
			 		 	sql="select i.crewName,i.ticket,i.shipper,i.workDate,i.crewEmail,i.integrationStatus from timesheet i where i.integrationStatus <>'' "
		 			+ " AND workDate like '%"+statusDateNew+"%' and crewName like '%"+registrationNumber+"%' and ticket like '%"+operation+"%' and crewEmail like '%"+integrationId+"%'  and shipper like '%"+orderComplete+"%' and integrationStatus like '%"+statusCode+"%'";
			 
			 }else{
				 sql="select i.crewName,i.ticket,i.shipper,i.workDate,i.crewEmail,i.integrationStatus from timesheet i where i.integrationStatus <>'' "
				 		+ " and crewName like '%"+registrationNumber+"%' and ticket like '%"+operation+"%' and crewEmail like '%"+integrationId+"%'  and shipper like '%"+orderComplete+"%' and integrationStatus like '%"+statusCode+"%'";
				 }
				 
			if(!"TSFT".equals(corpID)){
				sql = sql +" and i.corpid ='"+corpID+"' ";
			}
		    list = this.getSession().createSQLQuery(sql).list();
		    
		    Iterator it = list.iterator();
		    while(it.hasNext()){
		    	IntegrationLogInfoDTO dto = new IntegrationLogInfoDTO();
		    	Object[] obj = (Object[]) it.next();
		    	
		    	if(obj[0]!=null)
		    	dto.setRegistrationNumber(obj[0].toString());
		    	
		    	if(obj[1]!=null)
		    	dto.setOperation(obj[1].toString());
		    	
		    	if(obj[2]!=null)
		    	dto.setOrderComplete(obj[2].toString());
		    	
		    	if(obj[4]!=null)
			    	dto.setIntegrationId(obj[4].toString());
		    	
		    	if(obj[3]!=null)
		    	dto.setEffectiveDate((Date) obj[3]);
		    	
		       	if(obj[5]!=null)
		    	dto.setStatusCode(obj[5].toString());
		       			    		    	
		    	uList.add(dto);
		
		    }
		return uList;
	}
	
	public class IntegrationLogDTO {
		private Object id;

		private Object transactionID;

		private Object message;

		private Object fileName;

		private Object batchID;

		private Object processedOn;

		private Object recordID;

		private Object serviceOrderId;
		
		private Object count;
		
		private Object xmlCount;
		
		private Object controlFlag;

		public Object getBatchID() {
			return batchID;
		}

		public void setBatchID(Object batchID) {
			this.batchID = batchID;
		}

		public Object getFileName() {
			return fileName;
		}

		public void setFileName(Object fileName) {
			this.fileName = fileName;
		}

		public Object getId() {
			return id;
		}

		public void setId(Object id) {
			this.id = id;
		}

		public Object getMessage() {
			return message;
		}

		public void setMessage(Object message) {
			this.message = message;
		}

		public Object getProcessedOn() {
			return processedOn;
		}

		public void setProcessedOn(Object processedOn) {
			this.processedOn = processedOn;
		}

		public Object getRecordID() {
			return recordID;
		}

		public void setRecordID(Object recordID) {
			this.recordID = recordID;
		}

		public Object getTransactionID() {
			return transactionID;
		}

		public void setTransactionID(Object transactionID) {
			this.transactionID = transactionID;
		}

		public Object getServiceOrderId() {
			return serviceOrderId;
		}

		public void setServiceOrderId(Object serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}

		public Object getCount() {
			return count;
		}

		public void setCount(Object count) {
			this.count = count;
		}

		public Object getXmlCount() {
			return xmlCount;
		}

		public void setXmlCount(Object xmlCount) {
			this.xmlCount = xmlCount;
		}

		public Object getControlFlag() {
			return controlFlag;
		}

		public void setControlFlag(Object controlFlag) {
			this.controlFlag = controlFlag;
		}

	}
	public class SODashboardDTO {
		private String shipnumber;
		private String role;
		private String job;
		private String shipmenttype;
		private String coordinator;
		private String status;
		private String registrationnumber;
		private Object id;
		private Object survey;
		private Object estweight;
		private Object ldpkticket;
		private Object loading;
		private Object actualweight;
		private Object driver;
		private Object truck;
		private Object deliveryticket;
		private Object delivery;
		private Object claims;
		private Object qc;
		private Object estSurvey;
		private Object estLoding;
		private Object estDelivery;
		private Object lastName;
		private Object firstName;
		private String mode;
		private String actualVolume;
		private String estVolume;
		private Object dep;
		private Object arv;
		private Object loadingDt;
		private Object deliveryDt;
		private Object surveyDt;
		private Object dptF;
		private Object arvlF;
		private Object depFlag;
		private Object arvFlag;
		private Object customDate;
		private Object customDateF;
		private Object custFlag;
		private String isBA;
		private Object survey1;
		private Object loading1;
		private Object delivery1;
		private Object billToName;
		private Object orginCC;
		private Object destinCC;
		private Object ld;
		private Object dL;
		private Object sD;
		private Object ldFlag;
		private Object dLFlag;
		private Object sDFlag;
		private Object loadAT;
		private Object delAT;
		private Object survAT;
		private Object driver1;
		private Object truck1;
		
		public Object getCustFlag() {
			return custFlag;
		}
		public void setCustFlag(Object custFlag) {
			this.custFlag = custFlag;
		}
		public Object getCustomDate() {
			return customDate;
		}
		public void setCustomDate(Object customDate) {
			this.customDate = customDate;
		}
		public Object getCustomDateF() {
			return customDateF;
		}
		public void setCustomDateF(Object customDateF) {
			this.customDateF = customDateF;
		}
		public Object getArvFlag() {
			return arvFlag;
		}
		public void setArvFlag(Object arvFlag) {
			this.arvFlag = arvFlag;
		}
		public Object getDptF() {
			return dptF;
		}
		public void setDptF(Object dptF) {
			this.dptF = dptF;
		}
		public Object getArvlF() {
			return arvlF;
		}
		public void setArvlF(Object arvlF) {
			this.arvlF = arvlF;
		}
		public Object getDepFlag() {
			return depFlag;
		}
		public void setDepFlag(Object depFlag) {
			this.depFlag = depFlag;
		}
		public Object getLoadingDt() {
			return loadingDt;
		}
		public void setLoadingDt(Object loadingDt) {
			this.loadingDt = loadingDt;
		}
		public Object getDeliveryDt() {
			return deliveryDt;
		}
		public void setDeliveryDt(Object deliveryDt) {
			this.deliveryDt = deliveryDt;
		}
		public Object getSurveyDt() {
			return surveyDt;
		}
		public void setSurveyDt(Object surveyDt) {
			this.surveyDt = surveyDt;
		}
		public Object getDep() {
			return dep;
		}
		public void setDep(Object dep) {
			this.dep = dep;
		}
		public Object getArv() {
			return arv;
		}
		public void setArv(Object arv) {
			this.arv = arv;
		}
		public String getShipnumber() {
			return shipnumber;
		}
		public void setShipnumber(String shipnumber) {
			this.shipnumber = shipnumber;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public String getJob() {
			return job;
		}
		public void setJob(String job) {
			this.job = job;
		}
		public String getShipmenttype() {
			return shipmenttype;
		}
		public void setShipmenttype(String shipmenttype) {
			this.shipmenttype = shipmenttype;
		}
		public String getCoordinator() {
			return coordinator;
		}
		public void setCoordinator(String coordinator) {
			this.coordinator = coordinator;
		}
		public Object getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
		public String getRegistrationnumber() {
			return registrationnumber;
		}
		public void setRegistrationnumber(String registrationnumber) {
			this.registrationnumber = registrationnumber;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getSurvey() {
			return survey;
		}
		public void setSurvey(Object survey) {
			this.survey = survey;
		}
		public Object getEstweight() {
			return estweight;
		}
		public void setEstweight(Object estweight) {
			this.estweight = estweight;
		}
		public Object getLdpkticket() {
			return ldpkticket;
		}
		public void setLdpkticket(Object ldpkticket) {
			this.ldpkticket = ldpkticket;
		}
		public Object getLoading() {
			return loading;
		}
		public void setLoading(Object loading) {
			this.loading = loading;
		}
		public Object getActualweight() {
			return actualweight;
		}
		public void setActualweight(Object actualweight) {
			this.actualweight = actualweight;
		}
		public Object getDriver() {
			return driver;
		}
		public void setDriver(Object driver) {
			this.driver = driver;
		}
		public Object getTruck() {
			return truck;
		}
		public void setTruck(Object truck) {
			this.truck = truck;
		}
		public Object getDeliveryticket() {
			return deliveryticket;
		}
		public void setDeliveryticket(Object deliveryticket) {
			this.deliveryticket = deliveryticket;
		}
		public Object getDelivery() {
			return delivery;
		}
		public void setDelivery(Object delivery) {
			this.delivery = delivery;
		}
		public Object getClaims() {
			return claims;
		}
		public void setClaims(Object claims) {
			this.claims = claims;
		}
		public Object getQc() {
			return qc;
		}
		public void setQc(Object qc) {
			this.qc = qc;
		}
		public Object getEstSurvey() {
			return estSurvey;
		}
		public void setEstSurvey(Object estSurvey) {
			this.estSurvey = estSurvey;
		}
		public Object getEstLoding() {
			return estLoding;
		}
		public void setEstLoding(Object estLoding) {
			this.estLoding = estLoding;
		}
		public Object getEstDelivery() {
			return estDelivery;
		}
		public void setEstDelivery(Object estDelivery) {
			this.estDelivery = estDelivery;
		}
		public Object getLastName() {
			return lastName;
		}
		public void setLastName(Object lastName) {
			this.lastName = lastName;
		}
		public Object getFirstName() {
			return firstName;
		}
		public void setFirstName(Object firstName) {
			this.firstName = firstName;
		}
		public String getMode() {
			return mode;
		}
		public void setMode(String mode) {
			this.mode = mode;
		}
		public String getActualVolume() {
			return actualVolume;
		}
		public void setActualVolume(String actualVolume) {
			this.actualVolume = actualVolume;
		}
		public String getEstVolume() {
			return estVolume;
		}
		public void setEstVolume(String estVolume) {
			this.estVolume = estVolume;
		}
		public String getIsBA() {
			return isBA;
		}
		public void setIsBA(String isBA) {
			this.isBA = isBA;
		}
		public Object getSurvey1() {
			return survey1;
		}
		public void setSurvey1(Object survey1) {
			this.survey1 = survey1;
		}
		public Object getLoading1() {
			return loading1;
		}
		public void setLoading1(Object loading1) {
			this.loading1 = loading1;
		}
		public Object getDelivery1() {
			return delivery1;
		}
		public void setDelivery1(Object delivery1) {
			this.delivery1 = delivery1;
		}
		public Object getBillToName() {
			return billToName;
		}
		public void setBillToName(Object billToName) {
			this.billToName = billToName;
		}
		public Object getOrginCC() {
			return orginCC;
		}
		public void setOrginCC(Object orginCC) {
			this.orginCC = orginCC;
		}
		public Object getDestinCC() {
			return destinCC;
		}
		public void setDestinCC(Object destinCC) {
			this.destinCC = destinCC;
		}
		public Object getLd() {
			return ld;
		}
		public void setLd(Object ld) {
			this.ld = ld;
		}
		public Object getdL() {
			return dL;
		}
		public void setdL(Object dL) {
			this.dL = dL;
		}
		public Object getsD() {
			return sD;
		}
		public void setsD(Object sD) {
			this.sD = sD;
		}
		public Object getLdFlag() {
			return ldFlag;
		}
		public void setLdFlag(Object ldFlag) {
			this.ldFlag = ldFlag;
		}
		public Object getdLFlag() {
			return dLFlag;
		}
		public void setdLFlag(Object dLFlag) {
			this.dLFlag = dLFlag;
		}
		public Object getsDFlag() {
			return sDFlag;
		}
		public void setsDFlag(Object sDFlag) {
			this.sDFlag = sDFlag;
		}
		public Object getLoadAT() {
			return loadAT;
		}
		public void setLoadAT(Object loadAT) {
			this.loadAT = loadAT;
		}
		public Object getDelAT() {
			return delAT;
		}
		public void setDelAT(Object delAT) {
			this.delAT = delAT;
		}
		public Object getSurvAT() {
			return survAT;
		}
		public void setSurvAT(Object survAT) {
			this.survAT = survAT;
		}
		public Object getDriver1() {
			return driver1;
		}
		public void setDriver1(Object driver1) {
			this.driver1 = driver1;
		}
		public Object getTruck1() {
			return truck1;
		}
		public void setTruck1(Object truck1) {
			this.truck1 = truck1;
		}
		
	}
	public class MSSLogDTO {
		private Object mssOrderNumber;

		private Object workTicketNumber;

		private Object updatedBy;

		private Object updatedon;
		
		private Object UpdatedDefaultSort ;

		public Object getMssOrderNumber() {
			return mssOrderNumber;
		}

		public void setMssOrderNumber(Object mssOrderNumber) {
			this.mssOrderNumber = mssOrderNumber;
		}

		public Object getWorkTicketNumber() {
			return workTicketNumber;
		}

		public void setWorkTicketNumber(Object workTicketNumber) {
			this.workTicketNumber = workTicketNumber;
		}

		public Object getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(Object updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Object getUpdatedon() {
			return updatedon;
		}

		public void setUpdatedon(Object updatedon) {
			this.updatedon = updatedon;
		}

		public Object getUpdatedDefaultSort() {
			return UpdatedDefaultSort;
		}

		public void setUpdatedDefaultSort(Object UpdatedDefaultSort) {
			this.UpdatedDefaultSort = UpdatedDefaultSort;
		}

	

		
	}
	
	public List getMssLogList(String corpId){
		  List mssLogList = new ArrayList();
		  SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");
	//	String MasterList = "select if(left(filename,2)='20' or left(filename,2)='CW','Scanned Docs', (if(left(filename,2)='dp','Dispatch', (if(left(filename,2)='ds','Distribution', (if(left(filename,2)='me','Memo', (if(left(filename,2)='rg','Registration', (if(left(filename,2)='st','Wkly.Stmnt',(if(left(filename,2)='DO','DOC',if(left(filename,2)='cl','Claims',(if(left(filename,2)='rt','Rating',left(filename,2))))))))))))))))) 'XML', date_format(processedon,'%Y-%m-%d') 'DATE', count(*) 'Elements_Updated', count(distinct filename) 'No_Of_XML' from integrationlog where corpID ='"+ corpId +"' AND processedon >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by left( filename,2), date_format(processedon,'%Y-%m-%d')order by left( filename,2), date_format(processedon,'%Y-%m-%d') desc";
		String MasterList="select mssOrderNumber as MSSOrderNumber,workTicketNumber as WorkTicketNumber, updatedBy as UpdatedBy,updatedon as Updatedon , updatedon as UpdatedDefaultSort from mss where (mssOrderNumber is not null and mssOrderNumber!='') and corpID ='"+ corpId+"' order by updatedon desc";
		List listMasterList = this.getSession().createSQLQuery(MasterList)
		.addScalar("mSSOrderNumber")
		.addScalar("workTicketNumber")
		.addScalar("updatedBy")
		.addScalar("updatedon")
		.addScalar("updatedon")
		.list();
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			MSSLogDTO integrationLogDTO = new MSSLogDTO();
			if (row[0] == null) {
				integrationLogDTO.setMssOrderNumber("");
			} else {
				integrationLogDTO.setMssOrderNumber(row[0].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setWorkTicketNumber("");
			} else {
				integrationLogDTO.setWorkTicketNumber(row[1].toString());
			}
			
			if (row[2] == null) {
				integrationLogDTO.setUpdatedBy("");
			} else {
				integrationLogDTO.setUpdatedBy(row[2].toString());
			}
			
			if (row[3] == null) {
				integrationLogDTO.setUpdatedon("");
			} else {
			String dt = formatter.format(row[3]);
				integrationLogDTO.setUpdatedon(dt.toString());
			}
			
			if (row[4] == null) {
				integrationLogDTO.setUpdatedDefaultSort("");
			} else {
				integrationLogDTO.setUpdatedDefaultSort(row[4].toString());
			}

			mssLogList.add(integrationLogDTO);
		}
		return mssLogList;
		
		//return getHibernateTemplate().find("select left( fileName,2) 'XML', date_format(processedOn,'%Y-%m-%d') 'DATE', count(*) 'Records' from IntegrationLog where processedOn >= date_sub(sysdate(), interval 7 DAY) group by left( fileName,2), date_format(processedOn,'%Y-%m-%d')order by left( fileName,2), date_format(processedOn,'%Y-%m-%d') desc");
	}
	
	public String getVlJobTypes(String corpid){
		String query="select miscVl from systemdefault where corpid='"+corpid+"'";
		String vljob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			vljob=listMasterList.get(0).toString();
		}
		return vljob;
	}
	public String getStorageJobTypes(String corpid){
		String query="select storage from systemdefault where corpid='"+corpid+"'";
		String stojob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			stojob=listMasterList.get(0).toString();
		}
		return stojob;
	}
	public String getLocalJobsTypes(String corpid){
		String query="select localJobs from systemdefault where corpid='"+corpid+"'";
		String lcjob="";
		List listMasterList = this.getSession().createSQLQuery(query).list();
		if(listMasterList!=null && !(listMasterList.isEmpty()) && listMasterList.get(0)!=null){
			lcjob=listMasterList.get(0).toString();
		}
		return lcjob;
	}
	
	public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String companyDivision,String billToName,String originCity,String destinationCity,Boolean activeStatus){
		List centreVanlineList = new ArrayList();
		Map <String,String> servFlex=getFieldsNameToDisplay();
		String vlJobTypes=getVlJobTypes(corpId);
		String stoJobTypes=getStorageJobTypes(corpId);
		String lcjobs=getLocalJobsTypes(corpId);
		String wherecl="";
		String flag="0";
		String MasterList="select s.shipnumber as shipnumber,s.registrationnumber as registrationnumber,replace(replace(role,'SD','SDA'),'SO','SOA') as role,s.job as job,s.serviceType as shipmenttype,s.coordinator as coordinator,s.status as status,s.id as id,if((d.actualSurvey!='DT' and d.actualSurvey!='OD' and d.actualSurvey!='NA'),date_format(d.actualSurvey,'%d-%b-%Y'),d.actualSurvey) as survey,estweight as estweight,'' as ldpkticket,if((actualLoading!='DT' and actualLoading!='OD' and actualLoading!='NA'),date_format(actualLoading,'%d-%b-%Y'),actualLoading) as loading,actualweight as actualweight,driver as driver,truck as truck,'' as deliveryticket,if((actualDelivery!='DT' and actualDelivery!='OD' and actualDelivery!='NA'),date_format(actualDelivery,'%d-%b-%Y'),actualDelivery) as delivery,claims as claims,if((qc!='DT' and qc!='OD' and qc!='NA'),date_format(qc,'%d-%b-%Y'),qc) as qc,if((estimateSurvey!='DT' and estimateSurvey!='OD' and estimateSurvey!='NA'),date_format(estimateSurvey,'%d-%b-%Y'),estimateSurvey) as estSurvey,if((estimateLoading!='DT' and estimateLoading!='OD' and estimateLoading!='NA'),date_format(estimateLoading,'%d-%b-%Y'),estimateLoading) as estLoding,if((estimateDelivery!='DT' and estimateDelivery!='OD' and estimateDelivery!='NA'),date_format(estimateDelivery,'%d-%b-%Y'),estimateDelivery) as estDelivery,s.lastName as lastName,s.firstName as firstName,mode,actVolume,estvolume,if(s.ATD is not null,date_format(s.ATD,'%d-%b-%Y'),date_format(s.ETD,'%d-%b-%Y'))as dpt,if(s.ATA is not null,date_format(s.ATA,'%d-%b-%Y'),date_format(s.ETA,'%d-%b-%Y'))as arvl,date_format(estimateLoading,'%d-%b-%Y') as loadingDt,date_format(estimateDelivery,'%d-%b-%Y') as deliveryDt,date_format(estimateSurvey,'%d-%b-%Y') as surveyDt,if(s.ATD is not null,'A','T')as dptF,if(s.ATA is not null,'A','T')as arvlF,date_format(d.customDate,'%d-%b-%Y') as clearCustom ,d.customFlag as clearCustomFlag,s.bookingAgentCode as bookingAgentCode,s.serviceType as serviceType,s.billToName as billToName,if(s.originCity is not null and s.originCity!='',concat(s.originCity,',',if(s.originCountryCode is not null,s.originCountryCode,'-')),concat('-',if(s.originCountryCode is not null,s.originCountryCode,'-'))) as OrginCC,if(s.destinationCity is not null and s.destinationCity!='',concat(s.destinationCity,',',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-')),concat('-',if(s.destinationCountryCode is not null,s.destinationCountryCode,'-'))) as destinCC,if(d.actualLoading is not null,'A', 'T') as loadF,if(d.actualDelivery is not null,'A', 'T') as deliveryF,if(d.actualSurvey is not null,'A', 'T') as surveyF,if(d.actualLoading is not null,date_format(d.actualLoading,'%d-%b-%Y'),date_format(d.estimateLoading,'%d-%b-%Y') ) as ld,if(d.actualDelivery is not null,date_format(d.actualDelivery,'%d-%b-%Y'),date_format(d.estimateDelivery,'%d-%b-%Y')) as dt,if(d.actualSurvey is not null,date_format(d.actualSurvey,'%d-%b-%Y'),date_format(d.estimateSurvey,'%d-%b-%Y')) as asd from serviceorder s inner join sodashboard d on d.serviceOrderId=s.id  where  s.corpID='"+corpId+"' ";
		if(shipNumber!=null && !shipNumber.equals("")){
			shipNumber=shipNumber.replaceAll("'","");
			wherecl=" and s.shipnumber like '%"+shipNumber+"%'";
			flag="1";
		}
		if(registrationNumber!=null && !registrationNumber.equals("")){
			registrationNumber=registrationNumber.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and registrationnumber like '%"+registrationNumber+"%'";
			}else{
				wherecl="and registrationnumber like '%"+registrationNumber+"%'";
			flag="1";
			}
		}
		if(lastName!=null && !lastName.equals("")){
			lastName=lastName.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and lastName like '%"+lastName+"%'";
			}else{
				wherecl="and lastName like '%"+lastName+"%'";
			flag="1";
			}
		}
		if(firstName!=null && !firstName.equals("")){
			firstName=firstName.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and firstName like '%"+firstName+"%'";
			}else{
				wherecl="and firstName like '%"+firstName+"%'";
			flag="1";
			}
		}
		if(job1!=null && !job1.equals("")){
			job1=job1.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and job like '%"+job1+"%'";
			}else{
				wherecl="and job like '%"+job1+"%'";
			flag="1";
			}
		}
		if(coordinator!=null && !coordinator.equals("")){
			coordinator=coordinator.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and coordinator like '%"+coordinator+"%'";
			}else{
				wherecl="and coordinator like '%"+coordinator+"%'";
			flag="1";
			}
		} 
		if(status!=null && !status.equals("")){
			status=status.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.status like '%"+status+"%'";
			}else{
				wherecl="and s.status like '%"+status+"%'";
			flag="1";
			}
		}
		if(role!=null && !role.equals("")){
			role=role.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and replace(role,'/',',') like '%"+role+"%'";
			}else{
				wherecl="and replace(role,'/',',') like '%"+role+"%'";
			flag="1";
			}
		}
		if(companyDivision!=null && !companyDivision.equals("")){
			if(flag.equals("1")){
			wherecl=wherecl+" and s.companyDivision like '%"+companyDivision+"%'";
			}else{
				wherecl="and s.companyDivision like '%"+companyDivision+"%'";
			flag="1";
			}
		}
		if(billToName!=null && !billToName.equals("")){
			billToName=billToName.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.billToName like '%"+billToName+"%'";
			}else{
				wherecl="and s.billToName like '%"+billToName+"%'";
			flag="1";
			}
		}
		if(originCity!=null && !originCity.equals("")){
			originCity=originCity.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.originCity like '%"+originCity+"%'";
			}else{
				wherecl="and s.originCity like '%"+originCity+"%'";
			flag="1";
			}
		}
		if(destinationCity!=null && !destinationCity.equals("")){
			destinationCity=destinationCity.replaceAll("'","");
			if(flag.equals("1")){
			wherecl=wherecl+" and s.destinationCity like '%"+destinationCity+"%'";
			}else{
				wherecl="and s.destinationCity like '%"+destinationCity+"%'";
			flag="1";
			}
		}
		if(activeStatus!=null && activeStatus){
			if(flag.equals("1")){
			wherecl=wherecl+" and s.status not in ('CNCL','CLSD')";
			}else{
				wherecl="and s.status not in ('CNCL','CLSD')";
			flag="1";
			}
		}
 		if(!wherecl.equals("")){
			MasterList=MasterList+" "+wherecl;	
		}
		List listMasterList = this.getSession().createSQLQuery(MasterList+" order by id desc limit 100 ")
		.addScalar("shipnumber")
		.addScalar("registrationnumber")
		.addScalar("role")
		.addScalar("job")
		.addScalar("shipmenttype")
		.addScalar("coordinator")
		.addScalar("status")
		.addScalar("id")
		.addScalar("survey")
		.addScalar("estweight")
		.addScalar("ldpkticket")
		.addScalar("loading")
		.addScalar("actualweight")
		.addScalar("driver")
		.addScalar("truck")
		.addScalar("deliveryticket")
		.addScalar("delivery")
		.addScalar("claims")
		.addScalar("qc")
		.addScalar("estSurvey")
		.addScalar("estLoding")
		.addScalar("estDelivery")
		.addScalar("lastName")
		.addScalar("firstName")
		.addScalar("mode")
		.addScalar("actVolume")
		.addScalar("estvolume")
		.addScalar("dpt")
		.addScalar("arvl")
		.addScalar("loadingDt")
		.addScalar("deliveryDt")
		.addScalar("surveyDt")
		.addScalar("dptF")
		.addScalar("arvlF")
		.addScalar("clearCustom")
		.addScalar("clearCustomFlag")
		.addScalar("bookingAgentCode")
		.addScalar("serviceType")
		.addScalar("billToName")
		.addScalar("OrginCC")
		.addScalar("destinCC")
		.addScalar("loadF")
		.addScalar("deliveryF")
		.addScalar("surveyF")
		.addScalar("ld")
		.addScalar("dt")
		.addScalar("asd")
		.list();
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			SODashboardDTO dasboardDTO = new SODashboardDTO();
			String serviceType="";
			String flex5="";
			String loadF="";
			String deliveryF="";
			String surveyF="";
			 if (row[41] == null) {
				 dasboardDTO.setLoadAT("");	
	   			} else {
	   				loadF=row[41].toString();
	   			 dasboardDTO.setLoadAT(loadF);
	   			}
			 if (row[3] == null) {
					dasboardDTO.setJob("");
				} else {
					dasboardDTO.setJob(row[3].toString());
				}
	             if (row[42] == null) {
	            	 dasboardDTO.setDelAT("");		
	   			} else {
	   				deliveryF=(row[42].toString());
	   				dasboardDTO.setDelAT(deliveryF);
	   			}
	             if (row[43] == null) {
	            	 dasboardDTO.setSurvAT("");
	   			} else {
	   				surveyF=(row[43].toString());
	   				dasboardDTO.setSurvAT(surveyF);
	   			}
			if (row[37] == null) {
				
			} else {
				serviceType=row[37].toString();
			}
			flex5=servFlex.get(serviceType);
			if (row[36] == null) {
				dasboardDTO.setIsBA("");
			} else {
				String bacode=row[36].toString();
				String isBAquery="select corpID from companydivision where corpid = '"+corpId+"' and bookingagentcode = '"+bacode+"'";
				List lista= this.getSession().createSQLQuery(isBAquery).list(); 
				if(lista!=null && !(lista.isEmpty()) && (lista.get(0)!=null)){
					dasboardDTO.setIsBA("yes");
				}else{
					dasboardDTO.setIsBA("");
				}
			}
			//survey
			if (row[46] == null) {				
				dasboardDTO.setsD("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setsDFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Survey")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setsDFlag("NA");
						}
					}
				}
			} else {
				String dt=row[46].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setsD(row[46].toString());
				
				}catch(Exception e){
					dasboardDTO.setsD(dt);
					
				}
				if(surveyF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setsDFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Survey")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setsDFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
					String curr=format.format(dt1);	
					try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);					
					if(date1.equals(date2)){
						dasboardDTO.setsDFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setsDFlag("OD");
					}else{
						dasboardDTO.setsDFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setDepFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 format=new SimpleDateFormat("dd-MMM-yyyy");					
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setsDFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setsDFlag("OD");
						}else{
							dasboardDTO.setsDFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Survey")>-1){
	                	   dt1=new Date();	
	  					 format=new SimpleDateFormat("dd-MMM-yyyy");					
	  					 curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setsDFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setsDFlag("OD");
	  					}else{
	  						dasboardDTO.setsDFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setsDFlag("NA");
							}
						}
					}	
				}
					
			}
			//end survey
			
			//dt
			if (row[45] == null) {				
				dasboardDTO.setdL("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setdLFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setdLFlag("NA");
						}
					}
				}
			} else {
				String dt=row[45].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setdL(row[45].toString());
				}catch(Exception e){
					dasboardDTO.setdL(dt);
				}
				if(deliveryF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setdLFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setdLFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
  					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
  					String curr=format.format(dt1);	
  					try{
  					Date date1 = format.parse(dt);
  					Date date2 = format.parse(curr);					
  					if(date1.equals(date2)){
  						dasboardDTO.setdLFlag("DT");
  					}else if(date1.before(date2)){
  						dasboardDTO.setdLFlag("OD");
  					}else{
  						dasboardDTO.setdLFlag("");	
  					}
  					}catch(Exception e){
  						//dasboardDTO.setDepFlag("NA");	
  					}	
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 format=new SimpleDateFormat("dd-MMM-yyyy");					
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setdLFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setdLFlag("OD");
						}else{
							dasboardDTO.setdLFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Delivery")>-1){
	                	  dt1=new Date();	
	  					 format=new SimpleDateFormat("dd-MMM-yyyy");					
	  					 curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setdLFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setdLFlag("OD");
	  					}else{
	  						dasboardDTO.setdLFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setdLFlag("NA");
							}
						}
					}	
				}
					
			}
			//end dt
			//
			if (row[44] == null) {				
				dasboardDTO.setLd("");
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setLdFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("Loading")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setLdFlag("NA");
						}
					}
				}
			} else {
				String dt=row[44].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setLd(row[44].toString());				
				}catch(Exception e){
					dasboardDTO.setLd(dt);					
				}
				if(loadF.equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setLdFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("Loading")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setLdFlag("NA");
							}
						}
					}
				}else{				
					Date dt1=new Date();	
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
					String curr=format.format(dt1);	
					try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);					
					if(date1.equals(date2)){
						dasboardDTO.setLdFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setLdFlag("OD");
					}else{
						dasboardDTO.setLdFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setDepFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 format=new SimpleDateFormat("dd-MMM-yyyy");					
						 curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setLdFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setLdFlag("OD");
						}else{
							dasboardDTO.setLdFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("Loading")>-1){
	                	   dt1=new Date();	
	  					 format=new SimpleDateFormat("dd-MMM-yyyy");					
	  					 curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setLdFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setLdFlag("OD");
	  					}else{
	  						dasboardDTO.setLdFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setLdFlag("NA");
							}
						}
					}	
				}
					
			}
			if (row[37] == null) {
				
			} else {
				serviceType=row[37].toString();
			}
			flex5=servFlex.get(serviceType);
			if (row[36] == null) {
				dasboardDTO.setIsBA("");
			} else {
				String bacode=row[36].toString();
				String isBAquery="select corpID from companydivision where corpid = '"+corpId+"' and bookingagentcode = '"+bacode+"'";
				List lista= this.getSession().createSQLQuery(isBAquery).list(); 
				if(lista!=null && !(lista.isEmpty()) && (lista.get(0)!=null)){
					dasboardDTO.setIsBA("yes");
				}else{
					dasboardDTO.setIsBA("");
				}
			}
			if (row[0] == null) {
				dasboardDTO.setShipnumber("");
			} else {
				dasboardDTO.setShipnumber(row[0].toString());
			}
			if (row[1] == null) {
				dasboardDTO.setRegistrationnumber("");
			} else {
				dasboardDTO.setRegistrationnumber(row[1].toString());
			}
			
			if (row[2] == null) {
				dasboardDTO.setRole("");
			} else {
				dasboardDTO.setRole(row[2].toString());
			}			
			
			
			if (row[4] == null) {
				dasboardDTO.setShipmenttype("");
			} else {
				dasboardDTO.setShipmenttype(row[4].toString());
			}
			if (row[5] == null) {
				dasboardDTO.setCoordinator("");
			} else {
				dasboardDTO.setCoordinator(row[5].toString());
			}
			if (row[6] == null) {
				dasboardDTO.setStatus("");
			} else {
				dasboardDTO.setStatus(row[6].toString());
			}
			if (row[7] == null) {
				dasboardDTO.setId(new Long("00"));
			} else {
				dasboardDTO.setId(new Long(row[7].toString()));
			}
			
			if (row[9] == null) {
				dasboardDTO.setEstweight("");
			} else {
				dasboardDTO.setEstweight(row[9].toString());
			}
			if (row[10] == null) {
				dasboardDTO.setLdpkticket("");
			} else {
				dasboardDTO.setLdpkticket(row[10].toString());
			}
			
			if (row[12] == null) {
				dasboardDTO.setActualweight("");
			} else {
				dasboardDTO.setActualweight(row[12].toString());
			}
			if (row[24] == null) {
				dasboardDTO.setMode("");
			} else {
				dasboardDTO.setMode(row[24].toString());
			}
			if (row[13] == null) {
				dasboardDTO.setDriver("");
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDriver1("");
					}else{
						dasboardDTO.setDriver1("NA");
					}
				}
			} else {
				dasboardDTO.setDriver(row[13].toString());
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setDriver1("");
					}else{
					dasboardDTO.setDriver1("NA");
					}
				}
			}
			if (row[14] == null) {
				dasboardDTO.setTruck("");
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setTruck1("");
					}else{
					dasboardDTO.setTruck1("NA");
					}
				}
			} else {
				dasboardDTO.setTruck(row[14].toString());
				if((dasboardDTO.getMode()==null || dasboardDTO.getMode().equals("")) || (dasboardDTO.getMode()!=null && !dasboardDTO.getMode().equals("Rail") && !dasboardDTO.getMode().equals("Road") && !dasboardDTO.getMode().equals("Truck") && !dasboardDTO.getMode().equals("Overland"))){
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setTruck1("");
					}else{
					dasboardDTO.setTruck1("NA");
					}
				}
			}			
			if (row[15] == null) {
				dasboardDTO.setDeliveryticket("");
			} else {
				dasboardDTO.setDeliveryticket(row[15].toString());
			}
			
			if (row[17] == null) {
				dasboardDTO.setClaims("");
			} else {
				dasboardDTO.setClaims(row[17].toString());
			}
			if (row[18] == null) {
				dasboardDTO.setQc("");
			} else {
				String dt=row[18].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setQc(row[18].toString());
				}catch(Exception e){
					dasboardDTO.setQc(dt);
				}
			}
			if (row[19] == null) {
				dasboardDTO.setEstSurvey("");
			} else {
				String dt=row[19].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstSurvey(row[19].toString());
				}catch(Exception e){
					dasboardDTO.setEstSurvey(dt);
				}
				//
			}
			if (row[20] == null) {
				dasboardDTO.setEstLoding("");
			} else {
				String dt=row[20].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstLoding(row[20].toString());
				}catch(Exception e){
					dasboardDTO.setEstLoding(dt);
				}
				//
			}
			if (row[21] == null) {
				dasboardDTO.setEstDelivery("");
			} else {
				String dt=row[21].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setEstDelivery(row[21].toString());
				}catch(Exception e){
					dasboardDTO.setEstDelivery(dt);
				}
				//
			}
			if (row[22] == null) {
				dasboardDTO.setLastName("");
			} else {
				dasboardDTO.setLastName(row[22].toString());
			}
			if (row[23] == null) {
				dasboardDTO.setFirstName("");
			} else {
				dasboardDTO.setFirstName(row[23].toString());
			}
			
			if (row[25] == null) {
				dasboardDTO.setActualVolume("");
			} else {
				dasboardDTO.setActualVolume(row[25].toString());
			}
			if (row[26] == null) {
				dasboardDTO.setEstVolume("");
			} else {
				dasboardDTO.setEstVolume(row[26].toString());
			}
			if (row[32] == null) {
				dasboardDTO.setDptF("");
			} else {
				dasboardDTO.setDptF(row[32].toString());
			}
			if (row[33] == null) {
				dasboardDTO.setArvlF("");
			} else {
				dasboardDTO.setArvlF(row[33].toString());
			}
			if (row[27] == null) {				
				dasboardDTO.setDep("");				
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setDepFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("ETD")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setDepFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
			} else {
				String dt=row[27].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setDep(row[27].toString());				
				}catch(Exception e){
					dasboardDTO.setDep(dt);
					
				}
				if(dasboardDTO.getDptF().equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setDepFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("ETD")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setDepFlag("NA");
							}
						}
					}
				}else{				
					  Date dt1=new Date();	
	  					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
	  					String curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setDepFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setDepFlag("OD");
	  					}else{
	  						dasboardDTO.setDepFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();	
						 format=new SimpleDateFormat("dd-MMM-yyyy");					
						curr=format.format(dt1);	
						try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setDepFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setDepFlag("OD");
						}else{
							dasboardDTO.setDepFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setDepFlag("NA");	
						}
					}else{
	                  if(flex5!=null && flex5.indexOf("ETD")>-1){
	                	  dt1=new Date();	
	  				     format=new SimpleDateFormat("dd-MMM-yyyy");					
	  					 curr=format.format(dt1);	
	  					try{
	  					Date date1 = format.parse(dt);
	  					Date date2 = format.parse(curr);					
	  					if(date1.equals(date2)){
	  						dasboardDTO.setDepFlag("DT");
	  					}else if(date1.before(date2)){
	  						dasboardDTO.setDepFlag("OD");
	  					}else{
	  						dasboardDTO.setDepFlag("");	
	  					}
	  					}catch(Exception e){
	  						//dasboardDTO.setDepFlag("NA");	
	  					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setDepFlag("NA");
							}
						}
					}	
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}	
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setDepFlag("NA");
				}
			}
			if (row[28] == null) {
				dasboardDTO.setArv("");				
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setArvFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("ETA")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setArvFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
			} else {
				String dt=row[28].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setArv(row[28].toString());				
				}catch(Exception e){
					dasboardDTO.setArv(dt);					
				}
				if(dasboardDTO.getArvlF().equals("A")){
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						dasboardDTO.setArvFlag("");
					}else{
	                  if(flex5!=null && flex5.indexOf("ETA")>-1){
							
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setArvFlag("NA");
							}
						}
					}
				}else{	
					Date dt1=new Date();	
					SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
					String curr=format.format(dt1);	
					try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setArvFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setArvFlag("OD");
						}else{
							dasboardDTO.setArvFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setArvFlag("NA");	
						}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){				
					 dt1=new Date();	
					 format=new SimpleDateFormat("dd-MMM-yyyy");					
					 curr=format.format(dt1);	
					try{
						Date date1 = format.parse(dt);
						Date date2 = format.parse(curr);					
						if(date1.equals(date2)){
							dasboardDTO.setArvFlag("DT");
						}else if(date1.before(date2)){
							dasboardDTO.setArvFlag("OD");
						}else{
							dasboardDTO.setArvFlag("");	
						}
						}catch(Exception e){
							//dasboardDTO.setArvFlag("NA");	
						}					
					}else{
						if(flex5!=null && flex5.indexOf("ETA")>-1){
							 dt1=new Date();	
							 format=new SimpleDateFormat("dd-MMM-yyyy");					
							 curr=format.format(dt1);	
							try{
								Date date1 = format.parse(dt);
								Date date2 = format.parse(curr);					
								if(date1.equals(date2)){
									dasboardDTO.setArvFlag("DT");
								}else if(date1.before(date2)){
									dasboardDTO.setArvFlag("OD");
								}else{
									dasboardDTO.setArvFlag("");	
								}
								}catch(Exception e){
									//dasboardDTO.setArvFlag("NA");	
								}		
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setArvFlag("NA");
							}
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setArvFlag("NA");
				}
			}			
			if (row[29] == null) {
				dasboardDTO.setLoadingDt("");
			} else {
				String dt=row[29].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setLoadingDt(row[29].toString());
				}catch(Exception e){
					dasboardDTO.setLoadingDt(dt);
				}
			}
			if (row[30] == null) {
				dasboardDTO.setDeliveryDt("");
			} else {
				String dt=row[30].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setDeliveryDt(row[30].toString());
				}catch(Exception e){
					dasboardDTO.setDeliveryDt(dt);
				}
			}
			if (row[31] == null) {
				dasboardDTO.setSurveyDt("");
			} else {
				String dt=row[31].toString();
				SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-mm-dd");
				try{
				Date dd=dateformatYYYYMMDD1.parse(dt);
				dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
				dd.setMonth(dd.getMonth()+1);
				String str=dateformatYYYYMMDD1.format(dd);
				dasboardDTO.setSurveyDt(row[31].toString());
				}catch(Exception e){
					dasboardDTO.setSurveyDt(dt);
				}
			}
			
			if (row[35] == null) {
				dasboardDTO.setCustomDateF("");
			} else {
				dasboardDTO.setCustomDateF(row[35].toString());
			}
			if (row[34] == null) {
				dasboardDTO.setCustomDate("");
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setCustFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("CC")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setCustFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				
			} else {
				dasboardDTO.setCustomDate(row[34].toString());
				if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
					dasboardDTO.setCustFlag("");
				}else{
                  if(flex5!=null && flex5.indexOf("CC")>-1){
						
					}else{
						if(flex5!=null && !flex5.equals("")){
						dasboardDTO.setCustFlag("NA");
						}
					}
				}
				if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
				if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
					dasboardDTO.setCustFlag("NA");
				}
			}
			if(dasboardDTO.getCustomDateF()!=null && dasboardDTO.getCustomDateF().toString().equals("T")){	
				Date dt1=new Date();
				String dt="";
				if(dasboardDTO.getCustomDate()!=null){
				dt=dasboardDTO.getCustomDate().toString();
				}
				SimpleDateFormat format=new SimpleDateFormat("dd-MMM-yyyy");					
				String curr=format.format(dt1);	
				try{
					Date date1 = format.parse(dt);
					Date date2 = format.parse(curr);					
					if(date1.equals(date2)){
						dasboardDTO.setCustFlag("DT");
					}else if(date1.before(date2)){
						dasboardDTO.setCustFlag("OD");
					}else{
						dasboardDTO.setCustFlag("");	
					}
					}catch(Exception e){
						//dasboardDTO.setCustFlag("NA");	
					}
					if(dasboardDTO.getIsBA()!=null && dasboardDTO.getIsBA().equals("yes")){
						 dt1=new Date();
						 dt="";
						if(dasboardDTO.getCustomDate()!=null){
						dt=dasboardDTO.getCustomDate().toString();
						}
						 format=new SimpleDateFormat("dd-MMM-yyyy");					
						 curr=format.format(dt1);	
						try{
							Date date1 = format.parse(dt);
							Date date2 = format.parse(curr);					
							if(date1.equals(date2)){
								dasboardDTO.setCustFlag("DT");
							}else if(date1.before(date2)){
								dasboardDTO.setCustFlag("OD");
							}else{
								dasboardDTO.setCustFlag("");	
							}
							}catch(Exception e){
								//dasboardDTO.setCustFlag("NA");	
							}
					}else{
	                  if(flex5!=null && flex5.indexOf("CC")>-1){
	                	   dt1=new Date();
	      				 dt="";
	      				if(dasboardDTO.getCustomDate()!=null){
	      				dt=dasboardDTO.getCustomDate().toString();
	      				}
	      				 format=new SimpleDateFormat("dd-MMM-yyyy");					
	      				curr=format.format(dt1);	
	      				try{
	      					Date date1 = format.parse(dt);
	      					Date date2 = format.parse(curr);					
	      					if(date1.equals(date2)){
	      						dasboardDTO.setCustFlag("DT");
	      					}else if(date1.before(date2)){
	      						dasboardDTO.setCustFlag("OD");
	      					}else{
	      						dasboardDTO.setCustFlag("");	
	      					}
	      					}catch(Exception e){
	      						//dasboardDTO.setCustFlag("NA");	
	      					}	
						}else{
							if(flex5!=null && !flex5.equals("")){
							dasboardDTO.setCustFlag("NA");
							}
						}
					}
					if(vlJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
					if(stoJobTypes.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
					if(lcjobs.indexOf(dasboardDTO.getJob())>-1){
						dasboardDTO.setCustFlag("NA");
					}
			}
			 if (row[38] == null) {
	 				dasboardDTO.setBillToName("");
	 			} else {
	 				dasboardDTO.setBillToName(row[38].toString());
	 			}
	             if (row[39] == null) {
	  				dasboardDTO.setOrginCC("");
	  			} else {
	  				dasboardDTO.setOrginCC(row[39].toString());
	  			}
	             if (row[40] == null) {
	  				dasboardDTO.setDestinCC("");
	  			} else {
	  				dasboardDTO.setDestinCC(row[40].toString());
	  			}

			centreVanlineList.add(dasboardDTO);
			
			
	}
		return centreVanlineList;
	}
	
	public List findSoDashboardList(String corpId){
		return null;
	}
	//end
	public List getCentreVanlineList(String corpId){
		  List centreVanlineList = new ArrayList();		
	//	String MasterList = "select if(left(filename,2)='20' or left(filename,2)='CW','Scanned Docs', (if(left(filename,2)='dp','Dispatch', (if(left(filename,2)='ds','Distribution', (if(left(filename,2)='me','Memo', (if(left(filename,2)='rg','Registration', (if(left(filename,2)='st','Wkly.Stmnt',(if(left(filename,2)='DO','DOC',if(left(filename,2)='cl','Claims',(if(left(filename,2)='rt','Rating',left(filename,2))))))))))))))))) 'XML', date_format(processedon,'%Y-%m-%d') 'DATE', count(*) 'Elements_Updated', count(distinct filename) 'No_Of_XML' from integrationlog where corpID ='"+ corpId +"' AND processedon >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by left( filename,2), date_format(processedon,'%Y-%m-%d')order by left( filename,2), date_format(processedon,'%Y-%m-%d') desc";
		//Date start
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='') and transdocstatus!='DOWNLOADED' and corpID ='"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD1.format(new Date());
		  }
		 
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		  //end
		  String MasterList="select 'Trans Docs' as XML, date_format(updatedOn,'%d-%b-%Y') as DATE ,count(*) 'No_Of_XML' from myfile where (transdocstatus is not null and transdocstatus!='') and transdocstatus!='DOWNLOADED' and corpID ='"+corpId+"' and  updatedOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by date_format(updatedOn,'%Y-%m-%d')";
		List listMasterList = this.getSession().createSQLQuery(MasterList)
		.addScalar("XML")
		.addScalar("DATE")		
		.addScalar("No_Of_XML")
		.list();
		//Date start		
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(n.memoLastUploadedDate) FROM  notes n, serviceorder so " +
				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL')").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD11 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD11.format(new Date());
			 // maxProcessDate="";
		  }
		 
		 dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		 dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		 maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		  //end
		String memoQuery="select 'Memo' as XML,date_format(n.memoLastUploadedDate,'%d-%b-%Y') as DATE,count(*) 'No_Of_XML' FROM  notes n, serviceorder so " +
				"where n.corpid=so.corpid and n.customernumber = so.sequencenumber and (n.memoUploadStatus is not null and n.memoUploadStatus!='' and n.memoUploadStatus='UPLOADED') and n.corpid = '"+corpId+"' and n.uvlSentStatus = 'true' and (so.registrationNumber is not null and so.registrationNumber !='') and so.job in ('UVL','MVL') and  n.memoLastUploadedDate >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by date_format(n.memoLastUploadedDate,'%Y-%m-%d')";
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			if (row[0] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[0].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[1].toString());
			}
			
			if (row[2] == null) {
				integrationLogDTO.setXmlCount("");
			} else {
				integrationLogDTO.setXmlCount(row[2].toString());
			}
			centreVanlineList.add(integrationLogDTO);
		}
		//Date start		
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
			 // maxProcessDate="";
		  }
		 
		 dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		 dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		 maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		  //end
			
			
			List memolistMasterList = this.getSession().createSQLQuery(memoQuery)
			.addScalar("XML")
			.addScalar("DATE")		
			.addScalar("No_Of_XML")
			.list();
			Iterator it1 = memolistMasterList.iterator();
			while (it1.hasNext()) {
				Object[] row1 = (Object[]) it1.next();
				IntegrationLogDTO integrationLogDTO1 = new IntegrationLogDTO();
				if (row1[0] == null) {
					integrationLogDTO1.setFileName("");
				} else {
					integrationLogDTO1.setFileName(row1[0].toString());
				}
				
				if (row1[1] == null) {
					integrationLogDTO1.setProcessedOn("");
				} else {
					integrationLogDTO1.setProcessedOn(row1[1].toString());
				}
				
				if (row1[2] == null) {
					integrationLogDTO1.setXmlCount("");
				} else {
					integrationLogDTO1.setXmlCount(row1[2].toString());
				}			
			centreVanlineList.add(integrationLogDTO1);
		}
			
	 String qtg="select 'Quotes-2-Go' as XML,date_format(createdOn,'%d-%b-%Y') as DATE,count(estimatenumber) 'No_Of_XML' from customerfile where (estimateNumber is not null and estimateNumber!='') and corpid='"+corpId+"' and createdOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by date_format(createdOn,'%Y-%m-%d')";		
	 List qtgList = this.getSession().createSQLQuery(qtg)
		.addScalar("XML")
		.addScalar("DATE")		
		.addScalar("No_Of_XML")
		.list();
		Iterator it2 = qtgList.iterator();
		while (it2.hasNext()) {
			Object[] row2 = (Object[]) it2.next();
			IntegrationLogDTO integrationLogDTO2 = new IntegrationLogDTO();
			if (row2[0] == null) {
				integrationLogDTO2.setFileName("");
			} else {
				integrationLogDTO2.setFileName(row2[0].toString());
			}
			
			if (row2[1] == null) {
				integrationLogDTO2.setProcessedOn("");
			} else {
				integrationLogDTO2.setProcessedOn(row2[1].toString());
			}
			
			if (row2[2] == null) {
				integrationLogDTO2.setXmlCount("");
			} else {
				integrationLogDTO2.setXmlCount(row2[2].toString());
			}			
		centreVanlineList.add(integrationLogDTO2);
	}
		
		try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(processedon) from integrationlog where corpid = '"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
			  //maxProcessDate="";
		  }
		 
		 dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		 dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		 maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		
		String dpUpload="select 'Dispatch Upload' as 'XML', date_format(processedon,'%d-%b-%Y') 'DATE', count(distinct filename) 'No_Of_XML' from integrationlog where corpID ='"+corpId+"' AND processedon >= date_sub('"+maxProcessDate1+"', interval 7 DAY) and left(filename,2)='du' group by left( filename,2), date_format(processedon,'%Y-%m-%d')order by left( filename,2),date_format(processedon,'%Y-%m-%d') desc";		
		 List dpUploadList = this.getSession().createSQLQuery(dpUpload)
			.addScalar("XML")
			.addScalar("DATE")		
			.addScalar("No_Of_XML")
			.list();
			Iterator it4 = dpUploadList.iterator();
			while (it4.hasNext()) {
				Object[] row4 = (Object[]) it4.next();
				IntegrationLogDTO integrationLogDTO2 = new IntegrationLogDTO();
				if (row4[0] == null) {
					integrationLogDTO2.setFileName("");
				} else {
					integrationLogDTO2.setFileName(row4[0].toString());
				}
				
				if (row4[1] == null) {
					integrationLogDTO2.setProcessedOn("");
				} else {
					integrationLogDTO2.setProcessedOn(row4[1].toString());
				}
				
				if (row4[2] == null) {
					integrationLogDTO2.setXmlCount("");
				} else {
					integrationLogDTO2.setXmlCount(row4[2].toString());
				}			
			centreVanlineList.add(integrationLogDTO2);
		}
	 
	 return centreVanlineList;
		
		//return getHibernateTemplate().find("select left( fileName,2) 'XML', date_format(processedOn,'%Y-%m-%d') 'DATE', count(*) 'Records' from IntegrationLog where processedOn >= date_sub(sysdate(), interval 7 DAY) group by left( fileName,2), date_format(processedOn,'%Y-%m-%d')order by left( fileName,2), date_format(processedOn,'%Y-%m-%d') desc");
	}
	
	
	public List getWeeklyIntegrationCenterXML(String corpId){
		  List integrationLogs = new ArrayList();
		  
		 
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(processedon) from integrationlog where corpid = '"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
			 // maxProcessDate="";
		  }
		 
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		
		String MasterList = "select if(left(filename,2)='20' or left(filename,2)='CW','Scanned Docs', (if(left(filename,2)='dp','Dispatch Download', (if(left(filename,2)='ds','Distribution', (if(left(filename,2)='me','Memo', (if(left(filename,2)='rg','Registration', (if(left(filename,2)='st','Wkly.Stmnt',(if(left(filename,2)='DO','DOC',if(left(filename,2)='cl','Claims',(if(left(filename,2)='dp','Dispatch',(if(left(filename,2)='rt','Rating',left(filename,2))))))))))))))))))) 'XML', date_format(processedon,'%d-%b-%Y') 'DATE', count(*) 'Elements_Updated', count(distinct filename) 'No_Of_XML' from integrationlog where corpID ='"+ corpId +"' AND processedon >= date_sub('"+maxProcessDate1+"', interval 7 DAY) and left(filename,2)!='du' group by left( filename,2), date_format(processedon,'%Y-%m-%d')order by left( filename,2), date_format(processedon,'%Y-%m-%d') desc";
		List listMasterList = this.getSession().createSQLQuery(MasterList)
		.addScalar("XML")
		.addScalar("DATE")
		.addScalar("Elements_Updated")
		.addScalar("No_Of_XML")
		.list();
		
		Iterator it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			if (row[0] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[0].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[1].toString());
			}
			
			if (row[2] == null) {
				integrationLogDTO.setCount("");
			} else {
				integrationLogDTO.setCount(row[2].toString());
			}
			
			if (row[3] == null) {
				integrationLogDTO.setXmlCount("");
			} else {
				integrationLogDTO.setXmlCount(row[3].toString());
			}

			integrationLogs.add(integrationLogDTO);
		}
		//
		maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(updatedOn) from myfile where (transdocstatus is not null and transdocstatus!='') and transdocstatus='DOWNLOADED' and corpID ='"+corpId+"'").list().get(0).toString();
			  
		  }catch(Exception e){
			  SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
		  }
		 
		 dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		 dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		 maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd));
		  //end
		  String transdoc="select id as iid ,count(distinct id)  as itid,'Trans Docs Downloaded Successfully' as imsg,'Trans Docs' as ifn,updatedOn as ibid,date_format(updatedOn,'%d-%b-%Y') as ipon,customerNumber as irid,id as sid,'' as controlFlag from myfile where (transdocstatus is not null and transdocstatus!='') and transdocstatus='DOWNLOADED' and corpID ='"+corpId+"' and  updatedOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY) ";
		
		 listMasterList = this.getSession().createSQLQuery(transdoc).addScalar("iid").addScalar("itid").addScalar("imsg").addScalar("ifn").addScalar("ibid").addScalar(
		"ipon").addScalar("irid").addScalar("sid").addScalar("controlFlag").setMaxResults(500).list();

		 it = listMasterList.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			IntegrationLogDTO integrationLogDTO = new IntegrationLogDTO();
			if (row[3] == null) {
				integrationLogDTO.setFileName("");
			} else {
				integrationLogDTO.setFileName(row[3].toString());
			}
			
			if (row[5] == null) {
				integrationLogDTO.setProcessedOn("");
			} else {
				integrationLogDTO.setProcessedOn(row[5].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setCount("");
			} else {
				integrationLogDTO.setCount(row[1].toString());
			}
			
			if (row[1] == null) {
				integrationLogDTO.setXmlCount("");
			} else {
				integrationLogDTO.setXmlCount(row[1].toString());
			}

			integrationLogs.add(integrationLogDTO);
		}				
		
		
		//
		return integrationLogs;
		
		//return getHibernateTemplate().find("select left( fileName,2) 'XML', date_format(processedOn,'%Y-%m-%d') 'DATE', count(*) 'Records' from IntegrationLog where processedOn >= date_sub(sysdate(), interval 7 DAY) group by left( fileName,2), date_format(processedOn,'%Y-%m-%d')order by left( fileName,2), date_format(processedOn,'%Y-%m-%d') desc");
	}
	public List findDriverDashboardList(Date beginDate,Date endDate,String driverId,String driverAgency,String corpId){
		String beginDate1 = "";
		if (!(beginDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(beginDate));
			beginDate1 = nowYYYYMMDD1.toString();
		}
		String endDate1 = "";
		if (!(endDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(endDate));
			endDate1 = nowYYYYMMDD1.toString();
		}
		List driverDsBdList=new ArrayList();
		
		String query=" select a.vendorcode,p2.driveragency,CONCAT(if(p2.firstname is null,'',p2.firstname),' ',if(p2.lastname is null,' ',p2.lastname)) as drivername "+					
					" from accountline a "+
					" inner join partnerpublic p1 on a.vendorcode = p1.partnercode and p1.corpid = '"+corpId+"' and p1.isownerop = true "+
					" inner join partnerprivate p2 on p2.partnerpublicid=p1.id and p2.corpid = p1.corpid and p2.partnercode=a.vendorcode and p2.corpid = '"+corpId+"' " +
					" and p2.partnercode like '"+driverId+"%' and a.vendorcode like '"+driverId+"%' and p2.driveragency like '%"+driverAgency+"%'"+					
					" where a.corpid='"+corpId+"' and a.actualexpense <> 0 and a.payingstatus = 'A' "+
					" and ((a.invoicedate between '"+beginDate1+"' and '"+endDate1+"') "+
					" or (invoicedate is null and (receivedinvoicedate between '"+beginDate1+"' and '"+endDate1+"')))"+
					" Union "+
					" select personid,branch,CONCAT(if(y.firstname is null,'',y.firstname),' ',if(y.lastname is null,' ',y.lastname)) as drivername "+
					" from subcontractorcharges x , partnerprivate y "+
					" where x.corpid = '"+corpId+"' "+
					" and y.corpid = '"+corpId+"' "+
					" and x.persontype = 'D' "+
					" and x.personid = y.partnercode "+
					" and x.personid like '"+driverId+"%' and y.driveragency like '"+driverAgency+"%' "+
					" and x.approved between '"+beginDate1+"' and '"+endDate1+"' order by null ";
		List driverDsBoardList=this.getSession().createSQLQuery(query).list();
		
		DriverDashboardDTO dto = null;
		Iterator it = driverDsBoardList.iterator();
		while(it.hasNext()){
			Object []row= (Object [])it.next();
			dto = new DriverDashboardDTO();
			dto.setVenderCode(row[0]);
			dto.setAgencyName(row[1]);
			dto.setDriverName(row[2]);
			String actualExpenseQuery =" select sum(actualexpense),count(distinct(shipnumber)) from accountline where corpid ='"+corpId+"' "+
										" and vendorcode like '"+row[0]+"%' "+
										" and actualexpense <> 0 and payingstatus = 'A' "+
										" and ((invoicedate between '"+beginDate1+"' and '"+endDate1+"') "+
										" or (invoicedate is null and (receivedinvoicedate between '"+beginDate1+"' and '"+endDate1+"')))";
			List actualAmount = this.getSession().createSQLQuery(actualExpenseQuery).list();
			if(actualAmount!=null && !actualAmount.isEmpty() && actualAmount.get(0)!=null){
				Iterator itt=actualAmount.iterator();
				while (itt.hasNext()) {
					Object []row1= (Object [])itt.next();
					if(row1[0]!=null){
						dto.setActualExpense(row1[0]);
					}else{
						dto.setActualExpense("0.00");
					}
					dto.setShipNumber(row1[1]);
				}				
			}else{
				dto.setActualExpense("0.00");
			}			
			String balanceAmountQuery="select amount,approved from subcontractorcharges where corpid='"+corpId+"' "+
									  "	and personid like '"+row[0]+"%' and branch like '"+row[1]+"%' and (approved between '"+beginDate1+"' and '"+endDate1+"') "+
									  "	and description like 'Balance as of%'";
			List balanceAmount = this.getSession().createSQLQuery(balanceAmountQuery).list();
			if(balanceAmount!=null && !balanceAmount.isEmpty() && balanceAmount.get(0)!=null){
				Iterator itr = balanceAmount.iterator();
				while (itr.hasNext()){
					Object []row2= (Object [])itr.next();
					if(row2[0]!=null){
						dto.setBalanceAmount(row2[0]);
					}else{        		
						dto.setBalanceAmount("0.00");
					}
					dto.setBalanceAsOfDate(row2[1]);
				}
			}else{        		
        		dto.setBalanceAmount("0.00");
        	}
			String chequeAmountQuery="select amount from subcontractorcharges where corpid='"+corpId+"' "+
									  "	and personid like '"+row[0]+"%' and branch like '"+row[1]+"%' and (approved between '"+beginDate1+"' and '"+endDate1+"') "+
									  "	and description like 'Ck#%'";
			List chequeAmount = this.getSession().createSQLQuery(chequeAmountQuery).list();
			if(chequeAmount!=null && !chequeAmount.isEmpty() && chequeAmount.get(0)!=null){
				dto.setChequeAmount(chequeAmount.get(0).toString());
			}else{        		
				dto.setChequeAmount("0.00");
			}
			
			String adavanceChargeBackQuery="select sum(amount) from subcontractorcharges where corpid='"+corpId+"' "+
											" and personid like '"+row[0]+"%' and branch like '"+row[1]+"%' and (approved between '"+beginDate1+"' and '"+endDate1+"') "+
											" and description not like 'Balance as of%' and description not like 'Ck#%' ";	
			List advanceChargeAmount = this.getSession().createSQLQuery(adavanceChargeBackQuery).list();
			if(advanceChargeAmount!=null && !advanceChargeAmount.isEmpty() && advanceChargeAmount.get(0)!=null){
				dto.setAdvanceChargeBack(advanceChargeAmount.get(0).toString());
			}else{        		
				dto.setAdvanceChargeBack("0.00");
			}
			BigDecimal  newPriorBalance=new BigDecimal(0.00);			
			BigDecimal  newBalanceAmount=new BigDecimal(0.00);
			BigDecimal  ba=null;
			BigDecimal  ca=null;
			BigDecimal  ax=null;
			BigDecimal  pb=null;
			BigDecimal  adc=null;
			 ba = new BigDecimal(dto.getBalanceAmount()+"");
			 ca = new BigDecimal (dto.getChequeAmount()+"");			 	
			newPriorBalance = ba.add(ca);
			dto.setPriorBalance(newPriorBalance);
			pb = new BigDecimal (dto.getPriorBalance()+"");	
			adc = new BigDecimal (dto.getAdvanceChargeBack()+"");
			ax = new BigDecimal (dto.getActualExpense()+"");	
			newBalanceAmount = pb.add(adc).add(ax);
			dto.setNewBalance(newBalanceAmount);
			driverDsBdList.add(dto);
		}
		return driverDsBdList;		
	}
	
	public List findDriverDBChildList(Date beginDate,Date endDate,String driverId,String corpId){
		String beginDate1 = "";
		if (!(beginDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(beginDate));
			beginDate1 = nowYYYYMMDD1.toString();
		}
		String endDate1 = "";
		if (!(endDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(endDate));
			endDate1 = nowYYYYMMDD1.toString();
		}
		List driverDBList = new ArrayList();
		String query="select glcode,approved,description,amount from subcontractorcharges where corpid='"+corpId+"' "+
					" and personid='"+driverId+"' and (approved between '"+beginDate1+"' and '"+endDate1+"') "+
					" and description not like 'Balance as of%' and description not like 'Ck#%' order by glcode ";
		List list=this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext()){
			 Object []row= (Object [])it.next();
			 DriverDBChildDTO driverDBChild= new DriverDBChildDTO();
			 driverDBChild.setGlcode(row[0]);
			 driverDBChild.setApproved(row[1]);
			 driverDBChild.setDescription(row[2]);
			 driverDBChild.setAmount(row[3]);
			 driverDBList.add(driverDBChild);
		}
		return driverDBList;		
	}
	
	public List findDriverDBSoDetails(Date beginDate,Date endDate,String driverId,String corpId){
		String beginDate1 = "";
		if (!(beginDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(beginDate));
			beginDate1 = nowYYYYMMDD1.toString();
		}
		String endDate1 = "";
		if (!(endDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(endDate));
			endDate1 = nowYYYYMMDD1.toString();
		}
		List driverDBSoList = new ArrayList();
		String query="select if(v.firstname is null or v.firstname='' ,v.lastname,CONCAT(v.lastname,', ',v.firstname)) As shipper, "+
					" v.shipnumber,v.registrationNumber, a.chargeCode, "+
					" (if((a.description ='' || a.description=null),a.note,a.description )) as description,a.actualexpense,a.distributionAmount,v.job "+
					" from accountline a "+
					" inner join serviceorder v on a.serviceorderid = v.id and v.corpid = '"+corpId+"' "+
					" and a.vendorcode = '"+driverId+"' "+
					" where a.corpid='"+corpId+"' and a.actualexpense <> 0 and a.payingstatus = 'A' "+
					" and ((a.invoicedate between '"+beginDate1+"' and '"+endDate1+"') "+
					" or (a.invoicedate is null and (a.receivedinvoicedate between '"+beginDate1+"' and '"+endDate1+"'))) order by v.shipnumber,a.chargeCode ";
		List list=this.getSession().createSQLQuery(query).list();
		Iterator it=list.iterator();
		while(it.hasNext()){
			 Object []row= (Object [])it.next();
			 DriverDBSoDTO driverDBSo= new DriverDBSoDTO();
			 driverDBSo.setShipperName(row[0]);
			 driverDBSo.setShipNumber(row[1]);
			 driverDBSo.setRegNumber(row[2]);
			 driverDBSo.setChargeCode(row[3]);
			 driverDBSo.setDescription(row[4]);
			 driverDBSo.setActualAmount(row[5]);
			 driverDBSo.setDistributionAmount(row[6]);
			 driverDBSo.setJobType(row[7]);
			 driverDBSoList.add(driverDBSo);
		}
		return driverDBSoList;		
	}
	
	public class DriverDBSoDTO{
		private Object shipperName;
		private Object shipNumber;
		private Object regNumber;
		private Object chargeCode;
		private Object description;
		private Object actualAmount;
		private Object distributionAmount;
		private Object jobType;
		public Object getShipperName() {
			return shipperName;
		}
		public void setShipperName(Object shipperName) {
			this.shipperName = shipperName;
		}
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		public Object getRegNumber() {
			return regNumber;
		}
		public void setRegNumber(Object regNumber) {
			this.regNumber = regNumber;
		}
		public Object getChargeCode() {
			return chargeCode;
		}
		public void setChargeCode(Object chargeCode) {
			this.chargeCode = chargeCode;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getActualAmount() {
			return actualAmount;
		}
		public void setActualAmount(Object actualAmount) {
			this.actualAmount = actualAmount;
		}
		public Object getDistributionAmount() {
			return distributionAmount;
		}
		public void setDistributionAmount(Object distributionAmount) {
			this.distributionAmount = distributionAmount;
		}
		public Object getJobType() {
			return jobType;
		}
		public void setJobType(Object jobType) {
			this.jobType = jobType;
		}
	}
	
	public class DriverDBChildDTO{
		private Object glcode;
		private Object approved;
		private Object description;
		private Object amount;
		public Object getGlcode() {
			return glcode;
		}
		public void setGlcode(Object glcode) {
			this.glcode = glcode;
		}
		public Object getApproved() {
			return approved;
		}
		public void setApproved(Object approved) {
			this.approved = approved;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
	}
	
	public class DriverDashboardDTO{
		private Object venderCode;		
		private Object actualExpense;		
		private Object shipNumber;		
		private Object driverName;
		private Object agencyName;
		private Object balanceAmount;
		private Object chequeAmount;
		private Object priorBalance;
		private Object advanceChargeBack;
		private Object newBalance;
		private Object balanceAsOfDate;
		
		public Object getVenderCode() {
			return venderCode;
		}
		public void setVenderCode(Object venderCode) {
			this.venderCode = venderCode;
		}
		
		public Object getActualExpense() {
			return actualExpense;
		}
		public void setActualExpense(Object actualExpense) {
			this.actualExpense = actualExpense;
		}
		
		public Object getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(Object shipNumber) {
			this.shipNumber = shipNumber;
		}
		
		public Object getDriverName() {
			return driverName;
		}
		public void setDriverName(Object driverName) {
			this.driverName = driverName;
		}
		public Object getAgencyName() {
			return agencyName;
		}
		public void setAgencyName(Object agencyName) {
			this.agencyName = agencyName;
		}
		public Object getBalanceAmount() {
			return balanceAmount;
		}
		public void setBalanceAmount(Object balanceAmount) {
			this.balanceAmount = balanceAmount;
		}
		public Object getChequeAmount() {
			return chequeAmount;
		}
		public void setChequeAmount(Object chequeAmount) {
			this.chequeAmount = chequeAmount;
		}		
		public Object getAdvanceChargeBack() {
			return advanceChargeBack;
		}
		public void setAdvanceChargeBack(Object advanceChargeBack) {
			this.advanceChargeBack = advanceChargeBack;
		}
		public Object getNewBalance() {
			return newBalance;
		}
		public void setNewBalance(Object newBalance) {
			this.newBalance = newBalance;
		}
		public Object getPriorBalance() {
			return priorBalance;
		}
		public void setPriorBalance(Object priorBalance) {
			this.priorBalance = priorBalance;
		}
		public Object getBalanceAsOfDate() {
			return balanceAsOfDate;
		}
		public void setBalanceAsOfDate(Object balanceAsOfDate) {
			this.balanceAsOfDate = balanceAsOfDate;
		}				
	}
	public List findDriverDetails(String driverId,String corpId){
		String query="select partnerCode from partnerpublic where isOwnerOp is true and partnerCode like '"+driverId+"%' and status='Approved'" ;		
		List temp =  this.getSession().createSQLQuery(query).list();
		return temp;
	}
	public Map<String,String> getFieldsNameToDisplay(){
		Map visibleFields = new LinkedHashMap<String, String>();
		String query="select code,flex5 from refmaster where corpId='TSFT' ";
		List lista= this.getSession().createSQLQuery(query).list(); 
		Iterator it=lista.iterator();
			while(it.hasNext())
			{
				Object []row= (Object [])it.next();
				String flex5="";
				if(row[1]==null){
					
				}else{
					flex5=row[1].toString();
				}
				visibleFields.put(row[0].toString(),flex5 );			
				
			}
		return visibleFields;
	}	
	 public List getMoveForYouList(String corpId)
	 {
		 List moveForYouList = new ArrayList();
		  
		 SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from customerfile where corpid = '"+corpId+"' and createdby = 'move4u' ").list().get(0).toString();
		  }catch(Exception e){
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
		  }
		 
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD111.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD111.format(dd)); 
		  String query="select id,sequenceNumber, 'Leads Downloaded' as msg,'Move 4 U' as fn, date_format(createdOn,'%d-%b-%Y') as DATE, count(*) as count from customerfile where createdby = 'move4u' and corpid='"+corpId+"' and createdOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by date_format(createdOn,'%Y-%m-%d')";
			
		  List listMasterList =   this.getSession().createSQLQuery(query).addScalar("id").addScalar("sequenceNumber").addScalar("msg").addScalar("fn").addScalar("DATE").addScalar("count").setMaxResults(500).list();
		  Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				MoveForYouDTO moveForYouDTO = new MoveForYouDTO();
				if (row[0] == null) {
					moveForYouDTO.setId("");
				} else {
					moveForYouDTO.setId(row[0].toString());
				}
				
				if (row[1] == null) {
					moveForYouDTO.setSequenceNumb("");
				} else {
					moveForYouDTO.setSequenceNumb(row[1].toString());
				}
				
				if (row[2] == null) {
					moveForYouDTO.setMessage("");
				}else{
					moveForYouDTO.setMessage(row[2].toString());
				}
				
				if (row[3] == null) {
					moveForYouDTO.setFileName("");
				}else{
					moveForYouDTO.setFileName(row[3].toString());
				}
				
				if (row[4] == null) {
					moveForYouDTO.setDate("");
				}else{
					moveForYouDTO.setDate(row[4].toString());
				}
				
				if (row[5] == null) {
					moveForYouDTO.setCount("");
				}else{
					moveForYouDTO.setCount(row[5].toString());
				}
				
				moveForYouList.add(moveForYouDTO);
			}
			try{
				  maxProcessDate = this.getSession().createSQLQuery("select max(effectiveDate) from integrationloginfo where corpid = '"+corpId+"' and orderComplete ='Y' and statusCode='S' ").list().get(0).toString();
				  
			  }catch(Exception e){
				  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
				 // maxProcessDate="";
			  }
			 
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			Date dd1=null;
			try{
			dd1=dateformatYYYYMMDD2.parse(maxProcessDate);
			}catch(Exception e){}
			StringBuilder maxProcessDate2 = new StringBuilder(dateformatYYYYMMDD111.format(dd1)); 
			  String query1="select id, '' as cid, 'Sent to MoveCloud' as msg, '' as fn, date_format(effectiveDate,'%d-%b-%Y') as DATE , count(*) as count from integrationloginfo where corpid='"+corpId+"' and effectiveDate >= date_sub('"+maxProcessDate2+"', interval 7 DAY) group by date_format(effectiveDate,'%Y-%m-%d')";
			
			  List listMasterList1 =   this.getSession().createSQLQuery(query1).addScalar("id").addScalar("cid").addScalar("msg").addScalar("fn").addScalar("DATE").addScalar("count").setMaxResults(500).list();
			  Iterator it1 = listMasterList1.iterator();
				while (it1.hasNext()) {
					Object[] row = (Object[]) it1.next();
					MoveForYouDTO moveForYouDTO = new MoveForYouDTO();
					if (row[0] == null) {
						moveForYouDTO.setId("");
					} else {
						moveForYouDTO.setId(row[0].toString());
					}
					
					if (row[1] == null) {
						moveForYouDTO.setCustomerFileId("");
					} else {
						moveForYouDTO.setCustomerFileId("");
					}
					
					if (row[2] == null) {
						moveForYouDTO.setMessage("");
					}else{
						moveForYouDTO.setMessage(row[2].toString());
					}
					
					if (row[3] == null) {
						moveForYouDTO.setFileName("");
					}else{
						moveForYouDTO.setFileName(row[3].toString());
					}
					
					if (row[4] == null) {
						moveForYouDTO.setDate("");
					}else{
						moveForYouDTO.setDate(row[4].toString());
					}
					
					if (row[5] == null) {
						moveForYouDTO.setCount("");
					}else{
						moveForYouDTO.setCount(row[5].toString());
					}
					
					moveForYouList.add(moveForYouDTO);
				}
		return moveForYouList;
	 }
	 
	 public List getVoxmeEstimatList(String corpId)
	 {
		 List VoxmeEstimatList = new ArrayList();
		 SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
		 
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from inventorydata where corpid = '"+corpId+"' and createdBy like '%xml' ").list().get(0).toString();
			  
		  }catch(Exception e){
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
			 // maxProcessDate="";
		  }
		 
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd)); 
		  String query="select id, customerFileId as cid, 'Survey Details Downloaded' as msg, createdBy as fn, date_format(createdOn,'%d-%b-%Y') as DATE , count(distinct customerFileId) as count from inventorydata where createdBy like '%- XML' and corpid='"+corpId+"' and createdOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY) group by date_format(createdOn,'%Y-%m-%d')";
		  String queryFromEmailSetUp="";	
		  List listMasterList =   this.getSession().createSQLQuery(query).addScalar("id").addScalar("cid").addScalar("msg").addScalar("fn").addScalar("DATE").addScalar("count").setMaxResults(500).list();
		  Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				MoveForYouDTO VoxmeEstimatListDto = new MoveForYouDTO();
				if (row[0] == null) {
					VoxmeEstimatListDto.setId("");
				} else {
					VoxmeEstimatListDto.setId(row[0].toString());
				}
				
				if (row[1] == null) {
					VoxmeEstimatListDto.setCustomerFileId("");
				} else {
					VoxmeEstimatListDto.setCustomerFileId(row[1].toString());
				}
				
				if (row[2] == null) {
					VoxmeEstimatListDto.setMessage("");
				}else{
					VoxmeEstimatListDto.setMessage(row[2].toString());
				}
				
				if (row[3] == null) {
					VoxmeEstimatListDto.setFileName("");
				}else{
					VoxmeEstimatListDto.setFileName(row[3].toString());
				}
				
				if (row[4] == null) {
					VoxmeEstimatListDto.setDate("");
				}else{
					VoxmeEstimatListDto.setDate(row[4].toString());
				}
				
				if (row[5] == null) {
					VoxmeEstimatListDto.setCount("");
				}else{
					VoxmeEstimatListDto.setCount(row[5].toString());
				}
				
				VoxmeEstimatList.add(VoxmeEstimatListDto);
			}
			
			try{
				  maxProcessDate = this.getSession().createSQLQuery("select max(dateSent) from emailsetup where corpid = '"+corpId+"' and subject like 'voxme%' ").list().get(0).toString();
				  
			  }catch(Exception e){
				  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
				 // maxProcessDate="";
			  }
			 
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			Date dd1=null;
			try{
			dd1=dateformatYYYYMMDD2.parse(maxProcessDate);
			}catch(Exception e){}
			StringBuilder maxProcessDate2 = new StringBuilder(dateformatYYYYMMDD1.format(dd1)); 
			  String query1="select id, '' as cid, 'Sent to Voxme' as msg, '' as fn, date_format(dateSent,'%d-%b-%Y') as DATE , count(*) as count from emailsetup where subject like 'voxme%' and corpid='"+corpId+"' and dateSent >= date_sub('"+maxProcessDate2+"', interval 7 DAY) group by date_format(dateSent,'%Y-%m-%d')";
			  List listMasterList1 =   this.getSession().createSQLQuery(query1).addScalar("id").addScalar("cid").addScalar("msg").addScalar("fn").addScalar("DATE").addScalar("count").setMaxResults(500).list();
			  Iterator it1 = listMasterList1.iterator();
				while (it1.hasNext()) {
					Object[] row = (Object[]) it1.next();
					MoveForYouDTO VoxmeEstimatListDto = new MoveForYouDTO();
					if (row[0] == null) {
						VoxmeEstimatListDto.setId("");
					} else {
						VoxmeEstimatListDto.setId(row[0].toString());
					}
					
					if (row[1] == null) {
						VoxmeEstimatListDto.setCustomerFileId("");
					} else {
						VoxmeEstimatListDto.setCustomerFileId("");
					}
					
					if (row[2] == null) {
						VoxmeEstimatListDto.setMessage("");
					}else{
						VoxmeEstimatListDto.setMessage(row[2].toString());
					}
					
					if (row[3] == null) {
						VoxmeEstimatListDto.setFileName("");
					}else{
						VoxmeEstimatListDto.setFileName(row[3].toString());
					}
					
					if (row[4] == null) {
						VoxmeEstimatListDto.setDate("");
					}else{
						VoxmeEstimatListDto.setDate(row[4].toString());
					}
					
					if (row[5] == null) {
						VoxmeEstimatListDto.setCount("");
					}else{
						VoxmeEstimatListDto.setCount(row[5].toString());
					}
					
					VoxmeEstimatList.add(VoxmeEstimatListDto);
				}
		 
		 return VoxmeEstimatList;
	 }
	 
	 
	 public List getPricePointListForIntegration(String corpId)
	 {
		 List pricePointList = new ArrayList();
		 SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from accountline where ExternalIntegrationReference='pp' and corpid = '"+corpId+"'  ").list().get(0).toString();
		  }catch(Exception e){
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
		  }
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDate1 = new StringBuilder(dateformatYYYYMMDD1.format(dd)); 
		  String query="select id, serviceorderId, 'Quotes Requested' as msg, 'Price Point' as fn, date_format(createdOn,'%d-%b-%Y') as DATE, count(*) as count from accountline where ExternalIntegrationReference='pp' and createdOn >= date_sub('"+maxProcessDate1+"', interval 7 DAY)  and corpid='"+corpId+"' group by date_format(createdOn,'%Y-%m-%d') ";
		  List listMasterList =   this.getSession().createSQLQuery(query).addScalar("id").addScalar("serviceorderId").addScalar("msg").addScalar("fn").addScalar("DATE").addScalar("count").setMaxResults(500).list();
	 
		  Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				MoveForYouDTO pricePointDto = new MoveForYouDTO();
				if (row[0] == null) {
					pricePointDto.setId("");
				} else {
					pricePointDto.setId(row[0].toString());
				}
				
				if (row[1] == null) {
					pricePointDto.setServiceorderId("");
				} else {
					pricePointDto.setServiceorderId(row[1].toString());
				}
				
				if (row[2] == null) {
					pricePointDto.setMessage("");
				}else{
					pricePointDto.setMessage(row[2].toString());
				} 
				
				if (row[3] == null) {
					pricePointDto.setFileName("");
				}else{
					pricePointDto.setFileName(row[3].toString());
				}
				
				if (row[4] == null) {
					pricePointDto.setDate("");
				}else{
					pricePointDto.setDate(row[4].toString());
				}
				
				if (row[5] == null) {
					pricePointDto.setCount("");
				}else{
					pricePointDto.setCount(row[5].toString());
				}
				
				pricePointList.add(pricePointDto);
			}
		  return pricePointList;
	 }
	 
	 public List UGWWListForIntegration(String corpId)
	 {
		 List UGWWListForIntegration = new ArrayList();
		 SimpleDateFormat dateformatYYYYMMDD111 = new SimpleDateFormat("yyyy-MM-dd");
		 
		  String maxProcessDate ="";
		  try{
			  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from networkcontrol where corpid = '"+corpId+"' and createdby ='Ugww' ").list().get(0).toString();
			  
		  }catch(Exception e){
			  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
			 // maxProcessDate="";
		  }
		 
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		Date dd=null;
		try{
		dd=dateformatYYYYMMDD1.parse(maxProcessDate);
		}catch(Exception e){}
		StringBuilder maxProcessDateForDownload = new StringBuilder(dateformatYYYYMMDD1.format(dd)); 
		  String query="select 'Downloads' as msg, date_format(createdOn,'%d-%b-%Y') as DATE, count(*) as count from networkcontrol where createdby ='Ugww' and corpid='"+corpId+"' and createdOn >= date_sub('"+maxProcessDateForDownload+"', interval 7 DAY) group by date_format(createdOn,'%Y-%m-%d')";
		  String queryFromEmailSetUp="";	
		  List listMasterList =   this.getSession().createSQLQuery(query).addScalar("msg").addScalar("DATE").addScalar("count").setMaxResults(500).list();
		  Iterator it = listMasterList.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				MoveForYouDTO UGWWListDto = new MoveForYouDTO();
				
				if (row[0] == null) {
					UGWWListDto.setMessage("");
				}else{
					UGWWListDto.setMessage(row[0].toString());
				}
				
				if (row[1] == null) {
					UGWWListDto.setDate("");
				}else{
					UGWWListDto.setDate(row[1].toString());
				}
				
				if (row[2] == null) {
					UGWWListDto.setCount("");
				}else{
					UGWWListDto.setCount(row[2].toString());
				}
				
				UGWWListForIntegration.add(UGWWListDto);
			}
			
			try{
				  maxProcessDate = this.getSession().createSQLQuery("select max(createdOn) from networkcontrol where corpid = '"+corpId+"' and createdby !='Ugww' ").list().get(0).toString();
				  
			  }catch(Exception e){
				  maxProcessDate=dateformatYYYYMMDD111.format(new Date());
				 // maxProcessDate="";
			  }
			 
			SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
			Date dd1=null;
			try{
			dd1=dateformatYYYYMMDD2.parse(maxProcessDate);
			}catch(Exception e){}
			StringBuilder maxProcessDateForUpload = new StringBuilder(dateformatYYYYMMDD1.format(dd1)); 
			  String query1="select 'Uploads' as msg, date_format(createdOn,'%d-%b-%Y') as DATE, count(*) as count from networkcontrol where createdby !='Ugww' and corpid='"+corpId+"' and createdOn >= date_sub('"+maxProcessDateForUpload+"', interval 7 DAY) group by date_format(createdOn,'%Y-%m-%d')";
			  List listMasterList1 =   this.getSession().createSQLQuery(query1).addScalar("msg").addScalar("DATE").addScalar("count").setMaxResults(500).list();
			  Iterator it1 = listMasterList1.iterator();
				while (it1.hasNext()) {
					Object[] row = (Object[]) it1.next();
					MoveForYouDTO UGWWListDto = new MoveForYouDTO();
					
					if (row[0] == null) {
						UGWWListDto.setMessage("");
					}else{
						UGWWListDto.setMessage(row[0].toString());
					}
					
					if (row[1] == null) {
						UGWWListDto.setDate("");
					}else{
						UGWWListDto.setDate(row[1].toString());
					}
					
					if (row[2] == null) {
						UGWWListDto.setCount("");
					}else{
						UGWWListDto.setCount(row[2].toString());
					}
					
					UGWWListForIntegration.add(UGWWListDto);
				
				}
		 
		 return UGWWListForIntegration;
	 }
	 
	 public class MoveForYouDTO
	 {
		 	private Object id;
		 	
		 	private Object sequenceNumb;

			private Object message;

			private Object fileName;

			private Object date;
			
			private Object customerFileId;
			
			private Object serviceorderId;
			
			private Object count;
			
			public Object getId() {
				return id;
			}

			public void setId(Object id) {
				this.id = id;
			}

			public Object getMessage() {
				return message;
			}

			public void setMessage(Object message) {
				this.message = message;
			}

			public Object getFileName() {
				return fileName;
			}

			public void setFileName(Object fileName) {
				this.fileName = fileName;
			}

			public Object getSequenceNumb() {
				return sequenceNumb;
			}

			public void setSequenceNumb(Object sequenceNumb) {
				this.sequenceNumb = sequenceNumb;
			}

			public Object getDate() {
				return date;
			}

			public void setDate(Object date) {
				this.date = date;
			}

			public Object getCustomerFileId() {
				return customerFileId;
			}

			public void setCustomerFileId(Object customerFileId) {
				this.customerFileId = customerFileId;
			}

			public Object getServiceorderId() {
				return serviceorderId;
			}

			public void setServiceorderId(Object serviceorderId) {
				this.serviceorderId = serviceorderId;
			}

			public Object getCount() {
				return count;
			}

			public void setCount(Object count) {
				this.count = count;
			}
	 }

	public List findsoFullId(String shipNumber, String corpId) {

		List list = this
				.getSession()
				.createSQLQuery(
						"select id from serviceorder where shipnumber ='"+shipNumber+"' and corpid= '"+corpId+"'and controlflag='C' ").list();
		if(list==null || list.isEmpty()){
			list = this
					.getSession()
					.createSQLQuery("select s.id from serviceorder s,trackingstatus t where s.id =t.id and s.controlflag='C'  and s.corpid= '"+corpId+"' and (s.bookingAgentShipNumber = '"+shipNumber+"' or t.originAgentExSO = '"+shipNumber+"' or t.networkAgentExSO = '"+shipNumber+"' or t.originSubAgentExSO = '"+shipNumber+"' or t.destinationSubAgentExSO = '"+shipNumber+"' or t.bookingAgentExSO = '"+shipNumber+"' or t.bookingAgentExSO = '"+shipNumber+"' or t.destinationAgentExSO = '"+shipNumber+"')").list();
			
		}
								
		return list;

	}
		
	
}



