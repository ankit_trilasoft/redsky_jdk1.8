package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.UpdateRule;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.UpdateRuleManager;

public class UpdateRuleAction extends BaseAction implements Preparable {
	
	private String sessionCorpID;
	private User user;
	private Boolean activeStatus;
	private Set updateRules;	
	private UpdateRuleManager updateRuleManager;
	private List ls;
	private UpdateRule updateRule;
	private Long id;
	private String entity;
	private static List selectedField;
	private static List<DataCatalog> selectedDateField;
	private static Map<String, String> pentity;
	private RefMasterManager refMasterManager;
	private List updateRulesList;
	private String btn;

	public void prepare() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	 static final Logger logger = Logger.getLogger(UpdateRuleAction.class);
	    Date currentdate = new Date();
		
	    
		public UpdateRuleAction() {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			user = (User) auth.getPrincipal(); 
			this.sessionCorpID = user.getCorpID(); 
			 if(user.isDefaultSearchStatus()){
		 		   activeStatus=true;
		 	   }
		 	   else{
		 		   activeStatus=false;
		 	   }
			
		}
		
		
		@SkipValidation
		public String getComboList(String corpId){
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			pentity = refMasterManager.findByParameter(corpId, "PENTITY");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		 }
		
		@SkipValidation
		public String search() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			boolean id=(updateRule.getId()== null);
			boolean fieldup = (updateRule.getFieldToUpdate()== null);
			boolean massage = (updateRule.getValidationContion() == null);
			boolean status = (updateRule.getRuleStatus()== null);
			if(!id|| !fieldup || !massage || !status ) {
				updateRulesList=updateRuleManager.searchById(updateRule.getId(),updateRule.getFieldToUpdate(),updateRule.getValidationContion(), updateRule.getRuleStatus());
				//updateRulesList = new HashSet(ls);
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    return SUCCESS;   
		}
		private int countNumberOfResult;
		public String save() throws Exception 
		{
			updateRule.setUpdatedOn(new Date());			
			updateRule.setUpdatedBy(getRequest().getRemoteUser());
			updateRule.setCorpID(sessionCorpID);
			boolean isNew = (updateRule.getId() == null); 	
			if(isNew){
				updateRule.setCreatedBy(getRequest().getRemoteUser());
				updateRule.setCreatedOn(new Date());
				List maxRuleNumber = updateRuleManager.findMaximum(sessionCorpID);
				if (maxRuleNumber.get(0) == null) {
					updateRule.setRuleNumber(01L);
				} else {
					Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
					updateRule.setRuleNumber(autoRuleNumber);
				}
		    } 
			updateRule = updateRuleManager.save(updateRule);
			String executeRules=updateRuleManager.executeTestRule(updateRule, sessionCorpID);
			if(btn.equalsIgnoreCase("Test Condition")){
				if(executeRules==null){
					updateRule.setRuleStatus("Tested");
					updateRuleManager.changestatus("Tested",updateRule.getId());
					saveMessage("Update Rule has been tested successfully.");			
				}else{
					String sqlError = "";
					try{
						sqlError = executeRules;
						StringBuffer buffer = new StringBuffer();
						buffer.append(executeRules);
						buffer.append(" ");
						sqlError = buffer.toString();
					}catch(Exception ex){ }
					updateRule.setRuleStatus("Error");
					updateRuleManager.changestatus("Error",updateRule.getId());
					errorMessage("Update Rule cannot be executed. "+ sqlError);
				}
			}else if (btn.equalsIgnoreCase("Execute This Rule")){
				updateRuleManager.deleteUpdateByRuleNumber(updateRule.getRuleNumber(),updateRule.getCorpID());
				List executeThisRule= updateRuleManager.executeThisRule(updateRule, sessionCorpID,getRequest().getRemoteUser());
				if(executeThisRule == null ){
					updateRule.setRuleStatus("Error");
					updateRuleManager.changestatus("Error",updateRule.getId());
					errorMessage("Entered Update Rule is not executable.");
				}else{
					updateRule.setRuleStatus("Tested");
					updateRuleManager.changestatus("Tested",updateRule.getId());
					List numberOfResult=updateRuleManager.findNumberOfResultEntered(sessionCorpID, updateRule.getId());
					countNumberOfResult=Integer.parseInt(numberOfResult.get(0).toString());
					saveMessage("Update Rule has been executed successfully.  "   + countNumberOfResult + " Record Entered");
					updateRecordsResults(updateRule.getRuleNumber());
				}
			}
		return SUCCESS;
			
		}
		public String updateRecordsResults(Long ruleNumber){
			updateRuleManager.updateResults(ruleNumber);
			return SUCCESS;
		}
		@SkipValidation
		public String edit() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if (id != null){			
				updateRule=updateRuleManager.getForOtherCorpid(id);
				entity=updateRule.getTableName();
				selectedField=updateRuleManager.selectfield(entity,sessionCorpID);
				selectedDateField=updateRuleManager.selectDatefield(sessionCorpID);
				getComboList(sessionCorpID);		
			}else{
				updateRule=new UpdateRule();
				entity=updateRule.getTableName();
				selectedField=updateRuleManager.selectfield(entity,sessionCorpID);
				selectedDateField=updateRuleManager.selectDatefield(sessionCorpID);
				getComboList(sessionCorpID);
				updateRule.setCreatedOn(new Date());
				updateRule.setUpdatedOn(new Date());
				updateRule.setCreatedBy(getRequest().getRemoteUser());
				updateRule.setUpdatedBy(getRequest().getRemoteUser());
				
			}
			
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return "success";
		}  
		
		
		@SkipValidation
		public String list(){ 
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			ls = updateRuleManager.getByCorpID(sessionCorpID);
			updateRulesList = updateRuleManager.getByCorpID(sessionCorpID);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    updateRules = new HashSet(ls);
	    	return SUCCESS;   
	    }


		public Set getUpdateRules() {
			return updateRules;
		}
		public void setUpdateRuleManager(UpdateRuleManager updateRuleManager) {
			this.updateRuleManager = updateRuleManager;
		}


		public void setId(Long id) {
			this.id = id;
		}


		public String getEntity() {
			return entity;
		}


		public void setEntity(String entity) {
			this.entity = entity;
		}


		public static List getSelectedField() {
			return selectedField;
		}


		public static void setSelectedField(List selectedField) {
			UpdateRuleAction.selectedField = selectedField;
		}


		public static List<DataCatalog> getSelectedDateField() {
			return selectedDateField;
		}


		public static void setSelectedDateField(List<DataCatalog> selectedDateField) {
			UpdateRuleAction.selectedDateField = selectedDateField;
		}


		public static Map<String, String> getPentity() {
			return pentity;
		}


		public static void setPentity(Map<String, String> pentity) {
			UpdateRuleAction.pentity = pentity;
		}


		public RefMasterManager getRefMasterManager() {
			return refMasterManager;
		}


		public void setRefMasterManager(RefMasterManager refMasterManager) {
			this.refMasterManager = refMasterManager;
		}


		public UpdateRule getUpdateRule() {
			return updateRule;
		}


		public void setUpdateRule(UpdateRule updateRule) {
			this.updateRule = updateRule;
		}


		public Long getId() {
			return id;
		}


		public List getUpdateRulesList() {
			return updateRulesList;
		}


		public void setUpdateRulesList(List updateRulesList) {
			this.updateRulesList = updateRulesList;
		}


		public String getBtn() {
			return btn;
		}


		public void setBtn(String btn) {
			this.btn = btn;
		}


		public int getCountNumberOfResult() {
			return countNumberOfResult;
		}


		public void setCountNumberOfResult(int countNumberOfResult) {
			this.countNumberOfResult = countNumberOfResult;
		}


		

		/*public List getLs() {
			return ls;
		}


		public void setLs(List ls) {
			ls = ls;
		}
*/

		
	
	

}
