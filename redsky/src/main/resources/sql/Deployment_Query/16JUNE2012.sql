ALTER TABLE `redsky`.`newsupdate` ADD COLUMN `effectiveDate` DATETIME AFTER `publishedDate`,ADD COLUMN `endDisplayDate` DATETIME AFTER `effectiveDate`,ADD COLUMN `module` VARCHAR(80) DEFAULT NULL AFTER `endDisplayDate`,ADD COLUMN `retention` VARCHAR(5) DEFAULT NULL AFTER `module`,ADD COLUMN `location` VARCHAR(450) DEFAULT NULL AFTER `retention`,ADD COLUMN `visible` BOOLEAN AFTER `location`;

ALTER TABLE `redsky`.`company` ADD COLUMN `surveyEmailSign` VARCHAR(255) DEFAULT NULL AFTER `creditInvoiceSequence`;

ALTER TABLE `redsky`.`companydivision` ADD COLUMN `swiftcode` VARCHAR(50) DEFAULT '' AFTER `vanlineJob`;

