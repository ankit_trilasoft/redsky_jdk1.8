package com.trilasoft.app.webapp.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.Region;



public class ExcelCreator {
	private ExcelCreator(){}
	private String dataBasedCondition;
	private int dataIndex;	
	private int rowNumber;
	private int startPoint;
	private int endPoint;
	private short headerBackgroundColor;
	private short evenRowColor;
	private short oddRowColor;
	private short conditionalRowColor;
	private boolean additionalRow;
	private boolean mergeCells;
	private String[] additionalColumnList = {};


	{
		headerBackgroundColor = ExcelCreatorColor.RED;
		dataIndex = 0;
		evenRowColor = ExcelCreatorColor.GREY_25_PERCENT;
		oddRowColor = ExcelCreatorColor.WHITE;
		conditionalRowColor = ExcelCreatorColor.RED;
		additionalRow = false;
		mergeCells = false;
		rowNumber = 0;
		startPoint = 0;
	}
	
	public static ExcelCreator getExcelCreator(){
		return new ExcelCreator();
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Header Background Color
	 * @param color <br>It holds the value of ExcelCreatorColor
	 * @param Example <br> ExcelCreatorColor.AQUA
	 * @param Default : ExcelCreatorColor.RED
	 */
	public void setHeaderRowBackgroundColor(short color){
		headerBackgroundColor = color;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Even Row Background Color
	 * @param color <br>It holds the value of ExcelCreatorColor
	 * @param Example <br> ExcelCreatorColor.AQUA
	 * @param Default : ExcelCreatorColor.GREY_25_PERCENT
	 */
	public void setEvenDataRowColor(short evenRowColor){
		this.evenRowColor = evenRowColor;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Odd Row Background Color
	 * @param color <br>It holds the value of ExcelCreatorColor
	 * @param Example <br> ExcelCreatorColor.AQUA
	 * @param Default : ExcelCreatorColor.WHITE
	 */
	public void setOddDataRowColor(short oddRowColor){
		this.oddRowColor = oddRowColor;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Conditional Row Background Color
	 * @param color <br>It holds the value of ExcelCreatorColor
	 * @param Example <br> ExcelCreatorColor.AQUA
	 * @param Default : ExcelCreatorColor.RED
	 */
	
	public void setConditionalRowColor(short conditionalRowColor){
		this.conditionalRowColor = conditionalRowColor;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Additional Row Before Header Row
	 * @param Parameter1 <br>Provide Additional Column String Array
	 * @param Parameter2 <br>Set true For Additional Row Else false
	 * @param DefaultColor : As Header Color
	 */
	public void rowBeforeHeader(String[] additionalColumnList, boolean additionalRow, boolean mergeCells){
		this.additionalColumnList = additionalColumnList;
		this.additionalRow = additionalRow;
		this.mergeCells = mergeCells;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Use this method to set Additional Row Before Header Row
	 * @param Parameter1 <br>Set Total Number of Rows to be Merged
	 * @param Parameter2 <br>Set Initial Merge Point
	 * @param Parameter2 <br>Set End Merge Point
	 * @param DefaultColor : As Header Color
	 */
	public void mergeCells(int rowNumber, int startPoint, int endPoint){
		if(mergeCells){
			this.rowNumber = rowNumber;
			this.startPoint = startPoint;
			this.endPoint = endPoint;
		}
	}
	
	/**
	 * @author Kunal Sharma
	 * @param Don't use unless necessary<br><br>
	 * 
	 * @param dataBasedCondition<br>In Parameter 1: Provide Conditional Data
	 * @param dataIndex<br>In Parameter 1: Provide Conditional Data Index
	 */
	public void setConditionalDataAndIndex(String dataBasedCondition, int dataIndex) {
		this.dataBasedCondition = dataBasedCondition;
		this.dataIndex = dataIndex;
	}
	
	private HSSFCellStyle conditionalRow(HSSFWorkbook hwb){
		HSSFCellStyle cellStyleOdd = hwb.createCellStyle();
		cellStyleOdd.setFillForegroundColor(conditionalRowColor);
		cellStyleOdd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		return cellStyleOdd;
	}
	/* Setting Row Color (Even & Odd) */
	private HSSFCellStyle[] setRowCellFormat(HSSFWorkbook hwb)
	{
		
		HSSFCellStyle cellStyleOdd = hwb.createCellStyle();
		cellStyleOdd.setFillForegroundColor(oddRowColor);
		cellStyleOdd.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
				
		HSSFCellStyle cellStyleEven = hwb.createCellStyle();
		cellStyleEven.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyleEven.setFillForegroundColor(evenRowColor);
		
		return new HSSFCellStyle[]{cellStyleOdd,cellStyleEven};
	}
	/* Method Ends Here */
	
	/** Method to create Excel Sheet 
	 *  @author Kunal Sharma
	 *  @param columnList <br> The String Type Column Header Name Array<br>
	 *  @param dataList <br> The java.util.List Holds the list of Resultant Data
	 *  @param sheetName <br> The String Type Sheet Name Field
	 *  @param delimiter <br> The String Type data to separate Data from list
	 */
	public HSSFWorkbook createExcelStructure(String[] columnList, List dataList,String sheetName,String delimiter){
		HSSFWorkbook hwb=new HSSFWorkbook();
		HSSFSheet sheet =  hwb.createSheet(sheetName);
		HSSFCell cell = null;
		int cellLength = 0 ;
		HSSFCellStyle cellStyleHeader = hwb.createCellStyle();
		cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyleHeader.setFillForegroundColor(headerBackgroundColor);
		HSSFFont hssfFont = hwb.createFont();
		hssfFont.setColor((short)1);
		hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		hssfFont.setFontHeight((short)230);
		cellStyleHeader.setFont(hssfFont);
		cellStyleHeader.setWrapText(true);
		cellStyleHeader.setVerticalAlignment(cellStyleHeader.VERTICAL_TOP);
		HSSFRow rowhead = sheet.createRow((short)0); 
		
		if(additionalRow){
			if(mergeCells){
				sheet.addMergedRegion(new Region(rowNumber, (short)startPoint, rowNumber, (short)endPoint));
			}
			for(int j = 0 ; j < additionalColumnList.length ; j++){
				rowhead.setHeight((short)700);
				cell = rowhead.createCell((short) j);
				cell.setCellValue(additionalColumnList[j]);
				cell.setCellStyle(cellStyleHeader);
				cellLength = 0;
				cellLength += additionalColumnList[j].length();
				sheet.setColumnWidth((short)j, (short)(cellLength + 4500));
				cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
			}
		}
		if(additionalRow){
			rowhead = sheet.createRow((short)1);
		}
		for(int i = 0 ; i < columnList.length ; i++){
			rowhead.setHeight((short)700);
			cell = rowhead.createCell((short)i);
			cell.setCellValue(columnList[i]);
			cell.setCellStyle(cellStyleHeader);
			cellLength = 0;
			cellLength= columnList[i].length();
			sheet.setColumnWidth((short)i, (short)(cellLength + 4500));
			cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
		}
		
		HSSFCellStyle cellStyle, cellStyleArray[] = setRowCellFormat(hwb);
		HSSFCellStyle cellStyleConditional = conditionalRow(hwb);
		int formatter = 2;
		int objectNumber = 0 ;
		if(dataList != null){
			if(!dataList.isEmpty() && dataList.size() > 0){
				for (int data = 0; data < dataList.size(); data++) {
					if(formatter%2 == 0){
						cellStyle = cellStyleArray[1];
						formatter++;
					}
					else{
						cellStyle = cellStyleArray[0];
						formatter++;
					}
						String[] dataArray = (dataList.get(objectNumber++)).toString().split(delimiter);
						HSSFRow row=   sheet.createRow((short)(data+((additionalRow)?2:1)));
						row.setHeight((short)500);
						short count = 0;
						if(count != rowhead.getLastCellNum()) {
							for(int j = 0 ; j < dataArray.length ; j++){
								if(dataArray[j] != null && !dataArray[j].equals("")){
									cell = row.createCell((short)count++);
									if(getDataBasedCondition() != null && (!getDataBasedCondition().equals(""))){
										if(getDataBasedCondition().trim().equalsIgnoreCase(dataArray[dataIndex].toString().trim())){
											cell.setCellValue(dataArray[j].toString());
											cellLength= dataArray[j].toString().length() ;
											cell.setCellStyle(cellStyleConditional);
										}
										else{
											cell.setCellValue(dataArray[j].toString());
											cellLength= dataArray[j].toString().length() ;
											cell.setCellStyle(cellStyle);
										}
									}else{
										cell.setCellValue(dataArray[j].toString());
										cellLength= dataArray[j].toString().length() ;
										cell.setCellStyle(cellStyle);
									}
									sheet.setColumnWidth(count, (short)(cellLength + 4500));
								}else{
									cell = row.createCell(count++);
									cell.setCellStyle(cellStyle);
								}
							}
						}
				}
			}
		}
		return hwb;
	}
	
	/**
	 * @author Kunal Sharma
	 * @param fileName <br> Name For ExcelSheet
	 * @param columnList <br>Array of Column Name
	 * @param dataList <br> java.util.List (Resultant Data Fetched from DB)
	 * @param sheetName <br> Excel Sheet Name
	 * @param delimiter <br> This field is useful for the separation of fields
	 * @throws IOException
	 */
	
	public void createExcelBook(String fileName, String[] columnList, List dataList,String sheetName,String delimiter) throws IOException{
		HSSFWorkbook hwb = createExcelStructure(columnList, dataList,sheetName, delimiter);
		File file = new File(fileName);
		FileOutputStream fout = new FileOutputStream(file);
		hwb.write(fout);
	}
	
	/**
	 * @author Kunal Sharma
	 * @param response <br> HttpServletResponse reference to attach file on web
	 * @param fileName <br> Name For ExcelSheet without extension<br> e.g: File name should be ABC not ABC.xls
	 * @param columnList <br>Array of Column Name
	 * @param dataList <br> java.util.List (Resultant Data Fetched from DB)
	 * @param sheetName <br> Excel Sheet Name
	 * @param delimiter <br> This field is useful for the separation of fields
	 * @throws IOException
	 */
	public void createExcelForAttachment(HttpServletResponse response, String fileName, String[] columnList, List dataList,String sheetName,String delimiter) throws IOException{
		HSSFWorkbook hwb = createExcelStructure(columnList, dataList,sheetName, delimiter);
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		hwb.write(outputStream);
		outputStream.close();
	}

	public String getDataBasedCondition() {
		return dataBasedCondition;
	}
	
	
	/**
	 * For Extract from Reports Start
	 * 
	 */
  
	public HSSFWorkbook createExcelStructure(String[] columnList, List dataList,String sheetName,String delimiter,boolean isDTO){
			HSSFWorkbook hwb=new HSSFWorkbook();
			HSSFSheet sheet =  hwb.createSheet(sheetName);
			HSSFCell cell = null;
			int cellLength = 0 ;
			HSSFCellStyle cellStyleHeader = hwb.createCellStyle();
			cellStyleHeader.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
			cellStyleHeader.setFillForegroundColor(headerBackgroundColor);
			HSSFFont hssfFont = hwb.createFont();
			hssfFont.setColor((short)1);
			hssfFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			hssfFont.setFontHeight((short)230);
			cellStyleHeader.setFont(hssfFont);
			cellStyleHeader.setWrapText(true);
			cellStyleHeader.setVerticalAlignment(cellStyleHeader.VERTICAL_TOP);
			HSSFRow rowhead = sheet.createRow((short)0); 
			
			if(additionalRow){
				if(mergeCells){
					sheet.addMergedRegion(new Region(rowNumber, (short)startPoint, rowNumber, (short)endPoint));
				}
				for(int j = 0 ; j < additionalColumnList.length ; j++){
					rowhead.setHeight((short)700);
					cell = rowhead.createCell((short) j);
					cell.setCellValue(additionalColumnList[j]);
					cell.setCellStyle(cellStyleHeader);
					cellLength = 0;
					cellLength += additionalColumnList[j].length();
					sheet.setColumnWidth((short)j, (short)(cellLength + 4500));
					cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
				}
			}
			if(additionalRow){
				rowhead = sheet.createRow((short)1);
			}
			for(int i = 0 ; i < columnList.length ; i++){
				rowhead.setHeight((short)700);
				cell = rowhead.createCell((short)i);
				cell.setCellValue(columnList[i]);
				cell.setCellStyle(cellStyleHeader);
				cellLength = 0;
				cellLength= columnList[i].length();
				sheet.setColumnWidth((short)i, (short)(cellLength + 4500));
				cellStyleHeader.setAlignment(cellStyleHeader.ALIGN_CENTER);
			}
			
			HSSFCellStyle cellStyle, cellStyleArray[] = setRowCellFormat(hwb);
			HSSFCellStyle cellStyleConditional = conditionalRow(hwb);
			int formatter = 2;
			int objectNumber = 0 ;
			if(dataList != null){
				if(!dataList.isEmpty() && dataList.size() > 0){
					for (int data = 0; data < dataList.size(); data++) {
						if(formatter%2 == 0){
							cellStyle = cellStyleArray[1];
							formatter++;
						}
						else{
							cellStyle = cellStyleArray[0];
							formatter++;
						}				
							Object[] dataArray = (Object[])dataList.get(objectNumber++);
							HSSFRow row=   sheet.createRow((short)(data+((additionalRow)?2:1)));
							row.setHeight((short)500);
							short count = 0;
							if(count != rowhead.getLastCellNum()) {
								for(int j = 1 ; j < columnList.length+1 ; j++){
									if(dataArray[j] != null && !dataArray[j].equals("")){
										cell = row.createCell((short)count++);
										if(getDataBasedCondition() != null && (!getDataBasedCondition().equals(""))){
											if(getDataBasedCondition().trim().equalsIgnoreCase(dataArray[dataIndex].toString().trim())){
												cell.setCellValue(dataArray[j].toString());
												cellLength= dataArray[j].toString().length() ;
												cell.setCellStyle(cellStyleConditional);
											}
											else{
												cell.setCellValue(dataArray[j].toString());
												cellLength= dataArray[j].toString().length() ;
												cell.setCellStyle(cellStyle);
											}
										}else{
											cell.setCellValue(dataArray[j].toString());
											cellLength= dataArray[j].toString().length() ;
											cell.setCellStyle(cellStyle);
										}
										sheet.setColumnWidth(count, (short)(cellLength + 4500));
									}else{
										cell = row.createCell(count++);
										cell.setCellStyle(cellStyle);
									}
								}
							}
					}
				}
			}
			return hwb;
		}

		public void createExcelAttachment(HttpServletResponse response, String fileName, String[] columnList, List dataList,String sheetName,String delimiter, boolean isDTO) throws IOException{
			HSSFWorkbook hwb = createExcelStructure(columnList, dataList,sheetName, delimiter, isDTO);
			ServletOutputStream outputStream = response.getOutputStream();
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "attachment; filename=" + fileName + ".xls");
			response.setHeader("Pragma", "public");
			response.addHeader("Expires", "-1");
	        response.addHeader("Cache-Control", "no-cache");
			hwb.write(outputStream);
			outputStream.close();
		}

		/**
		 * For Extract from Reports End
		 * 
		 */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
