package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DataSecurityFilter;

public interface DataSecurityFilterDao extends GenericDao<DataSecurityFilter, Long> {
	
	public List findById(Long id);
	
	public List getFilterData(Long userId, String sessionCorpID);
	
	public List getUsingId(Long id);
	public String getParentCorpId(String sessionCorpID);
	public List<String> PartnerAccessCorpIdList();
	public List getChildAgentCode(String filterValues, String sessionCorpID,String parentCorpID);
	public Map getChildAgentCodeMap();



}
