package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.AccessInfoDao;
import com.trilasoft.app.model.AccessInfo;

public class AccessInfoDaoHibernate extends GenericDaoHibernate<AccessInfo, Long> implements AccessInfoDao{

	public AccessInfoDaoHibernate() {
		super(AccessInfo.class);
	}
	public List getAccessInfo(Long cid){
		return getHibernateTemplate().find("from AccessInfo where customerFileId=?",cid);
	}
	public List getAccessInfoForSO(Long cid){
		return getHibernateTemplate().find("from AccessInfo where serviceOrderId=?",cid);
	}
	public List getIdBySoID(Long cid){
		return getHibernateTemplate().find("select id from  AccessInfo where serviceOrderId=?",cid);
	}
}
