package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsTenancyManagement;

public interface DsTenancyManagementDao extends GenericDao<DsTenancyManagement, Long>{
	
	List getListById(Long id);

}
