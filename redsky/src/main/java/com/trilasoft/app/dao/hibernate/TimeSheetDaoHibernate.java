package com.trilasoft.app.dao.hibernate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.logging.Logger;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.User;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.omg.CORBA.portable.ValueOutputStream;

import com.trilasoft.app.dao.TimeSheetDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOForPDeatil;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.AvailableCrew;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.DayDistributionHoursDTO;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.OperationsHubLimits;
import com.trilasoft.app.model.OperationsResourceLimits;
import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.PayrollAllocation;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.ScheduleResourceOperation;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TimeSheet;
import com.trilasoft.app.model.WorkTicket;


public class TimeSheetDaoHibernate extends GenericDaoHibernate<TimeSheet, Long> implements TimeSheetDao {  
	
	private List workTickets;
	private List payrolls;
	private List timeSheets;
	private HibernateUtil hibernateUtil;
	private ItemsJbkEquip itemsJbkEquip;
	private WorkTicket workTicket;
	
	public TimeSheetDaoHibernate() {   
        super(TimeSheet.class); 
        
    } 
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
    }
	
	public List<WorkTicket> findTickets(String warehouse,String service,String date1, String shipper, String tkt,String sessionCorpID ){
		 warehouse = warehouse.replaceAll("'", "''");
		 service = service.replaceAll("'", "''");
		 shipper = shipper.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 date1 = date1.replaceAll("'", "");
		 	List workTickets = null;
	        String columns = "id,ticket, shipNumber,lastName,firstName,date1,date2,service,warehouse,estimatedWeight";
	        
		 	if(tkt.equalsIgnoreCase("")){
		 		if(date1.equalsIgnoreCase("")){
	    			workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND  service like '" +service+"%' AND (lastName like '"+shipper+"%' OR firstName like '"+shipper+"%')   and targetActual !='C' ");
			    }else{
			    	workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND  service like '" +service+"%' AND (date1<='"+date1+"' AND date2>='"+date1+"') AND (lastName like '"+shipper+"%' OR firstName like '"+shipper+"%')  and targetActual !='C' ");
			    }
		 	}else{
		 		if(date1.equalsIgnoreCase("")){
	    			workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"%' AND  service like '" +service+"%' AND (lastName like '"+shipper+"%' OR firstName like '"+shipper+"%') AND ticket ='"+tkt+"'   and targetActual !='C' ");
			    }else{
			    	workTickets = getHibernateTemplate().find("select " + columns + " from WorkTicket where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND  service like '" +service+"%' AND (date1<='"+date1+"' AND date2>='"+date1+"') AND (lastName like '"+shipper+"%' OR firstName like '"+shipper+"%') AND ticket = '"+tkt+"'  and targetActual !='C' ");
			    }
		 		
		 	}
		 	
	        try {
	        	workTickets= hibernateUtil.convertScalarToComponent(columns, workTickets, WorkTicket.class);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			return workTickets;   	
	 }
	 
	 /*public List<Payroll> findPayrolls(String warehouse,Date date1){
		 if(date1 == null){
			 payrolls = getHibernateTemplate().find("from Payroll where warehouse like '" +warehouse+"%'");
		 }else{
			 payrolls = getHibernateTemplate().find("from Payroll where warehouse like '" +warehouse+"%' AND (terminatedDate is null OR terminatedDate>=?)", date1);
		 }
		 return payrolls;
	 }*/
	 
	 
	 public List<WorkTicket> findTicket(Long tkt){
		 return getHibernateTemplate().find("from WorkTicket where ticket=?", tkt);
	 }
	 
	 public List<Billing> getBilling(String shipNum){
	    	return getHibernateTemplate().find("from Billing where shipNumber=?", shipNum);
	    }
	 public List<PayrollAllocation> findDistAndCalcCode(String job, String service, String sessionCorpID,String mode,String comDiv){
		 List<PayrollAllocation> list=null;
		 list=getHibernateTemplate().find("from PayrollAllocation where job='"+job+"' AND corpID='"+sessionCorpID +"' and ( companyDivision is null or companyDivision = '' or companyDivision = '"+comDiv+"' ) and ( mode is null or mode = '' or mode = '"+mode+"' ) AND service=?", service);
		 if(list.isEmpty()){
			 return getHibernateTemplate().find("from PayrollAllocation where corpID='"+sessionCorpID +"' and calculation = 'H' and code='180' ");
		 }else{
			 return getHibernateTemplate().find("from PayrollAllocation where job='"+job+"' AND corpID='"+sessionCorpID +"' and ( companyDivision is null or companyDivision = '' or companyDivision = '"+comDiv+"' ) and ( mode is null or mode = '' or mode = '"+mode+"' ) AND service=?", service);
		 }
		 //return getHibernateTemplate().find("from PayrollAllocation where job='"+job+"' AND corpID='"+sessionCorpID +"' and ( mode is null or mode = '' or mode = '"+mode+"' ) AND service=?", service);
	 }
	 
	 public List<Payroll> findTimeSheets(String warehouse,String date1, String shipper, String tkt, String crewNm, String sessionCorpID){
		 warehouse = warehouse.replaceAll("'", "''");
		 shipper = shipper.replaceAll("'", "''");
		 crewNm = crewNm.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 date1 = date1.replaceAll("'", "");
		/*if(!tkt.equalsIgnoreCase("")){
				 if(date1.equalsIgnoreCase("")){
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+" ");
				 }else{
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND workDate ='"+date1+"' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+"");
				 }
		 	}else{*/
		 		 if(date1.equalsIgnoreCase("")){
		 			 if(tkt == null || tkt.equalsIgnoreCase("")){
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND crewName like '%"+crewNm+"%' ORDER BY crewName, beginHours");
		 			 }else{
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' ORDER BY crewName, beginHours");
			 		 }
				 }else{
					 if(tkt == null || tkt.equalsIgnoreCase("")){
						 //System.out.println("from TimeSheet where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND workDate like '"+date1+"%'  AND crewName like '%"+crewNm+"%' ORDER BY crewName, beginHours");
					 	 timeSheets = getHibernateTemplate().find("from TimeSheet where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND workDate like '"+date1+"%'  AND crewName like '%"+crewNm+"%' ORDER BY crewName, beginHours");
					 }else{
						 
						 timeSheets = getHibernateTemplate().find("from TimeSheet where corpID='"+sessionCorpID+"' AND warehouse = '" +warehouse+"' AND workDate like '"+date1+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' ORDER BY crewName, beginHours");
					 }
				 }
		 	/*}*/
		 
		 
		 return timeSheets;
	    }
	 
	 public List<SystemDefault> findSystemDefault(String corpID){
	    	return getHibernateTemplate().find("from SystemDefault where corpID = ?", corpID);
	    }
	 
	 public List getTimeHoursFormSysDefault(String crewCorpId){
	    	return getHibernateTemplate().find("select CONCAT(dayAt1,'#',dayAt2) from SystemDefault where corpID = ?", crewCorpId);
	    }
	 
	 public List<AvailableCrew> findCrewList(){
		 List list1 = new ArrayList();
		 String availCrewQuery = "select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(t.regularHours) from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username and c.ignoreForTimeSheet is false group By c.username";
		 List listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
		 Iterator it=listAvailCrew.iterator();
	       while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew = new AvailableCrew();
	           availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[1] == null){availableCrew.setFirstName("");}else{availableCrew.setFirstName(row[1].toString());}
	           if(row[2] == null){availableCrew.setLastName("");}else{availableCrew.setLastName(row[2].toString());}
	           if(row[3] == null){availableCrew.setWarehouse("");}else{availableCrew.setWarehouse(row[3].toString());}
	           if(row[4] == null){availableCrew.setTypeofWork("");}else{availableCrew.setTypeofWork(row[4].toString());}
	           if(row[5] == null){availableCrew.setAvailableHours("");}else{availableCrew.setAvailableHours(row[5].toString());}
	           
	           
	           list1.add(availableCrew);
	       }
	    	return list1;
	    }
	 private Date wrkDtTmp;
	 public List<AvailableCrew> searchAvailableCrews(String warehouse,String date1, String crewNm, String sessionCorpID){
		 warehouse = warehouse.replaceAll("'", "''");
		 crewNm = crewNm.replaceAll("'", "''");
		 date1 = date1.replaceAll("'", "");
		 
		 List list1 = new ArrayList();
		 List listAvailCrew;
		 String availCrewQuery;
		 if(date1.equalsIgnoreCase("")){
		 	availCrewQuery = "select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(distinct t.regularHours),t.doneForTheDay from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' AND c.warehouse = '" +warehouse+"' and (c.lastName like '" +crewNm+"%' OR c.firstName like '" +crewNm+"%') and c.ignoreForTimeSheet is false group By c.username";
		 	listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
		 }else{
			 /*availCrewQuery = "(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(t.regularHours) from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.warehouse like '" +warehouse+"%' and c.lastName like '" +crewNm+"%' and t.workDate= :date1 group By c.username, t.workDate) UNION "+
			 					"(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(t.regularHours) from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.warehouse like '" +warehouse+"%' and c.lastName like '" +crewNm+"%' and c.username not in (select username from timesheet where workDate= :date1) group By c.username, t.workDate)";*/
			 availCrewQuery = "(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(distinct t.regularHours), c.unionName,t.doneForTheDay, c.overTimeType from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"' and (c.lastName like '" +crewNm+"%' OR c.firstName like '" +crewNm+"%') and t.workDate= :date1 and t.doneForTheDay<>'true' and c.corpID='"+sessionCorpID+"' group By c.lastName,c.firstName, t.workDate) UNION "+
				"(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, '' , c.unionName,'',  c.overTimeType from crew as c where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"' and (c.lastName like '" +crewNm+"%' OR c.firstName like '" +crewNm+"%') and c.username not in (select username from timesheet where workDate= :date1 and warehouse = c.warehouse) and c.corpID='"+sessionCorpID+"' group By c.lastName, c.id order by lastName,firstName)";

			 //availCrewQuery = "(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(distinct t.regularHours), c.unionName,t.doneForTheDay from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' AND c.warehouse = '" +warehouse+"' and (c.lastName like '" +crewNm+"%' OR c.firstName like '" +crewNm+"%') and t.workDate= :date1 and t.doneForTheDay<>'true' group By c.username, t.workDate)";

			listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).setString("date1", date1).list();
		 }
		 String wrkDt;
		 
		 Iterator it=listAvailCrew.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew = new AvailableCrew();
	           availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[1] == null){availableCrew.setFirstName("");}else{availableCrew.setFirstName(row[1].toString());}
	           if(row[2] == null){availableCrew.setLastName("");}else{availableCrew.setLastName(row[2].toString());}
	           if(row[3] == null){availableCrew.setWarehouse("");}else{availableCrew.setWarehouse(row[3].toString());}
	           if(row[4] == null){availableCrew.setTypeofWork("");}else{availableCrew.setTypeofWork(row[4].toString());}
	           if(row[5] == null){availableCrew.setAvailableHours("");}else{availableCrew.setAvailableHours(row[5].toString());}
	           if(!date1.equalsIgnoreCase("")){
	           if(row[8] == null){availableCrew.setIsUnioun("N");}else{
	           /*wrkDt=row[6].toString().replace(".0", "");
	           
	           try {
	        	   wrkDtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(wrkDt);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				if(wrkDtTmp.before(new Date())){
					availableCrew.setIsUnioun("U");
				}else{
					availableCrew.setIsUnioun("N");
				}*/
				if(row[8].toString().contains("S") || row[8].toString().contains("W") || row[8].toString().contains("U"))
				{
					availableCrew.setIsUnioun("U");
				}else{
					availableCrew.setIsUnioun("N");
				}
	           }
	           }
	           if(row[7] == null){availableCrew.setDoneForTheDay("");}else{availableCrew.setDoneForTheDay(row[7].toString());}
	           list1.add(availableCrew);
	       }
	    	return list1;
	 }
	 
	 
	 public List existsTimeSheet(Date wDate, String uName, String wHouse){
	    	return getHibernateTemplate().find("from TimeSheet where warehouse = '" +wHouse+"' AND userName like '" +uName+"%' AND action='L' AND workDate =?", wDate);
	    }
	 public List findTimeSheetByTicket(Long ticket){
		 return getHibernateTemplate().find("from TimeSheet where ticket=?", ticket);
	    }
	 public List checkCH(Long ticket, Date workdtTmp, String sessionCorpID) {
		 return getHibernateTemplate().find("from TimeSheet where ticket="+ticket+" AND corpID='"+sessionCorpID+"' and crewType in ('CH', 'CD', 'NC') AND workDate=?", workdtTmp);
	 }
	 public List findTimeSheetByTicketAndDate(Long ticket, Date wrkDt){
		 return getHibernateTemplate().find("from TimeSheet where ticket="+ticket+" AND workDate=?", wrkDt);
	    }
	 public List findTotalHoursWorked(Long ticket){
		 return getHibernateTemplate().find("Select SUM(regularHours) from TimeSheet where ticket = "+ticket+" GROUP BY ticket");
	 }
	 /*private Date workdtTmp;
	 String perHrs;*/
	 public List findDayDistributionHoursList(Long ticket, String actWgt) {
		 /*List list = new ArrayList();
		 List totalHoursWorked = findTotalHoursWorked(ticket); 
		 List distHours = getHibernateTemplate().find("Select workDate,SUM(regularHours) from TimeSheet where ticket = "+ticket+"  GROUP BY workDate");
		 
		 Iterator it = distHours.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           DayDistributionHoursDTO dayDistributionHoursDTO = new DayDistributionHoursDTO();
	           
	           if(row[0] == null){
	        	   dayDistributionHoursDTO.setWorkDate(null);
	        	   }else{
	        		   String newWorkDate = row[0].toString();
	    	           newWorkDate=newWorkDate.replace(".0", "");
	    	           
	    				try {
	    					workdtTmp = new SimpleDateFormat("yyyy-MM-dd").parse(newWorkDate);
	    				} catch (ParseException e) {
	    					
	    					e.printStackTrace();
	    				}
	        		   dayDistributionHoursDTO.setWorkDate(workdtTmp);
	        		   }
	           
	           if(row[1] == null){dayDistributionHoursDTO.setTotalHours("");}else{dayDistributionHoursDTO.setTotalHours(row[1].toString());}
	           if(row[1] == null){
	        	   dayDistributionHoursDTO.setPercentHours("0.0");
	        	   perHrs = "0.0";
	           }else{
	        	   perHrs = String.valueOf(Double.parseDouble(row[1].toString())/Double.parseDouble(totalHoursWorked.get(0).toString()));
	        	   dayDistributionHoursDTO.setPercentHours(String.valueOf(Double.parseDouble(perHrs)*100));
	           }
	           dayDistributionHoursDTO.setWeightShare(String.valueOf(Double.parseDouble(perHrs)*Double.parseDouble(actWgt)));
	           String revShr = String.valueOf(Double.parseDouble(perHrs)*Double.parseDouble(totRev));
	           dayDistributionHoursDTO.setRevenueShare(revShr);
	           if(row[1] == null){
	        	   dayDistributionHoursDTO.setPerHour("");
	           }else{
	        	   dayDistributionHoursDTO.setPerHour(String.valueOf(Double.parseDouble(revShr)/Double.parseDouble(row[1].toString())));
	           }
	           list.add(dayDistributionHoursDTO);
	       }
		 return list;*/
		 return getHibernateTemplate().find("Select workDate,SUM(regularHours) , SUM(overTime) from TimeSheet where ticket = "+ticket+"  GROUP BY workDate");
	 }
	 
	 public String findExpression(String calc,String sessionCorpID,String compDivision){
		 //List comDivision=getHibernateTemplate().find("select companyDivision from Sharing where corpId='"+sessionCorpID+"' AND code=?",calc );
		 List ls=new ArrayList();
		 ls = getHibernateTemplate().find("select descriptionArea from Sharing where corpId='"+sessionCorpID+"' AND ( companyDivision is null or companyDivision = '' or companyDivision = '"+compDivision+"' ) AND code=?", calc); 
		 		 
		 String exp = "";
		 if(ls!=null && (!(ls.isEmpty()))){
			 exp = ls.get(0).toString();
		 }
		return exp;
	}
	 
	 public List findWorkDays(Long ticket){
		 return getHibernateTemplate().find("from TimeSheet where ticket="+ticket+" GROUP BY workDate");
	    }
	 
	 public List findNormalDist(Long ticket, Date workDt, String job, String service,String compDivision){
		 String rateType="";
		 String countDR = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='DR' and workdate=?",workDt).get(0).toString();
		 String countHE = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='HE' and workdate=?",workDt).get(0).toString();
		 String countSU = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='SU' and workdate=?",workDt).get(0).toString();
		 String countCH = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='CH' and workdate=?",workDt).get(0).toString();
		 String countNC = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='NC' and workdate=?",workDt).get(0).toString();
		 String countCD = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='CD' and workdate=?",workDt).get(0).toString();
		 String countWH = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='WH' and workdate=?",workDt).get(0).toString();
		 String countFL = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='FL' and workdate=?",workDt).get(0).toString();
		 String countTotal = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and workdate=?",workDt).get(0).toString();
		 
		 
		 List rateTypeList=getHibernateTemplate().find("select rateType from PayrollAllocation where job = '"+job+"' and service = '"+service+"' and ( companyDivision is null or companyDivision = '' or companyDivision = '"+compDivision+"' ) ") ;
		 if(!rateTypeList.isEmpty())
		 {
			 rateType=rateTypeList.get(0).toString();
		 }
		 return getHibernateTemplate().find("FROM CategoryRevenue where totalCrew='"+countTotal+"' and numberOfCrew1='"+countDR+"' and numberOfCrew2='"+countCD+"' and numberOfCrew3='"+countHE+"' and numberOfCrew4='"+countCH+"' and numberOfCrew5='"+countWH+"' and secondSet='"+rateType+"' and ( companyDivision is null or companyDivision = '' or companyDivision = '"+compDivision+"' ) ");
	 }
	 
	 public Boolean totalRevenueShare(Long ticket){
		 List totRevShr = getHibernateTemplate().find("select sum(paidRevenueShare) from TimeSheet where ticket="+ticket+"");
		 if(totRevShr == null || totRevShr.isEmpty() || totRevShr.get(0) == null || totRevShr.get(0).toString().equalsIgnoreCase("0") || totRevShr.get(0).toString().equalsIgnoreCase("")){
	    	return true;
	    }else{
	    	return false;
	    }
	 }
	 
	 public TimeSheet findTimeSheetDetail(){
		List<TimeSheet> timeSheetList = getHibernateTemplate().find("from TimeSheet where id =(select max(id) from TimeSheet)");
		return  timeSheetList.get(0);
	 }
	 
	 public void updateWorkTicketData(Long ticket, String totalRevenue, String calcCode){
		 List listCnt = getHibernateTemplate().find("Select count(*) from TimeSheet where action not in ('L') AND ticket=?", ticket);
		 String cnt = listCnt.get(0).toString();
		 getHibernateTemplate().bulkUpdate("Update WorkTicket set crewRevenue='"+totalRevenue+"', crewRevenue='"+totalRevenue+"', revenueCalculation='"+calcCode+"', crews="+cnt+" where ticket=?", ticket);
	    }
	 
	 String beginDtNew;
	 String endDtNew;
	 
	 public List findTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 	String otCrewQuery;
		 	otCrewQuery = "select max(t.id),t.username,t.workdate,t.calculationcode,t.action,sum(round(regularhours, 1)),sum(IF(STRCMP(lunch, 'No lunch'),1,0)) as lnc, t.regularPayPerHour as rph, sum(t.paidRevenueShare) as prs from timesheet as t left outer join crew as c on t.userName=c.userName where (t.workDate >='"+beginDtNew+"' and t.workDate<='"+endDtNew+"') and t.calculationCode = 'H' and t.action='c' AND t.warehouse = '"+wareHse+"' and c.overTimeType in ('S', 'W', 'U') and t.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' group by t.workDate,t.userName";
		 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
	    	return listOTCrew;
	    }
	 
	 public List findTimeSheetsForOTPreview(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 	String otCrewQuery;
		 	otCrewQuery = "select max(t.id),t.username,t.workdate,t.calculationcode,t.action,sum(round(regularhours, 1)),sum(IF(STRCMP(lunch, 'No lunch'),1,0)) as lnc, t.regularPayPerHour as rph, sum(t.paidRevenueShare) as prs,sum(round(overtime, 1)) from timesheet as t left outer join crew as c on t.userName=c.userName where (t.workDate >='"+beginDtNew+"' and t.workDate<='"+endDtNew+"') and t.calculationCode = 'H' and t.action='c' AND t.warehouse = '"+wareHse+"' and c.overTimeType in ('S', 'W', 'U') and t.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' group by t.workDate,t.userName";
		 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
	    	return listOTCrew;
	    }
	 
	 	public List findTimeSheetsForNonUnionOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 	String otCrewQuery;
		 	otCrewQuery = "select max(t.id),t.username,t.workdate,t.calculationcode,t.action, sum(round(regularhours, 1)) ,sum(IF(STRCMP(lunch, 'No lunch'),1,0)) as lnc, t.regularPayPerHour as rph, sum(t.paidRevenueShare) as prs, t.crewName, t.crewType , max(t.workDate) from timesheet as t left outer join crew as c on t.userName=c.userName where (t.workDate >='"+beginDtNew+"' and t.workDate<='"+endDtNew+"') and t.action='c' and t.calculationcode='H' AND t.warehouse = '"+wareHse+"' and c.overTimeType not in ('S','W','U') and t.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' group by t.userName";
		 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
	    	return listOTCrew;
	    }
	 
	 public List checkRevenueSharing(String crewNm, String workDt, String wareHse,String sessionCorpID, String endDt){
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 String qq = "select id from timesheet where userName='"+crewNm+"' AND (workDate >='"+beginDtNew+"' and workDate<='"+endDtNew+"') AND action = 'C' AND warehouse = '"+wareHse+"' AND calculationCode <> 'H' and corpID='"+sessionCorpID+"'";
		 
		 	return this.getSession().createSQLQuery("select id from timesheet where userName='"+crewNm+"' AND (workDate >='"+beginDtNew+"' and workDate<='"+endDtNew+"') AND action = 'C' AND warehouse = '"+wareHse+"' AND calculationCode <> 'H' and corpID='"+sessionCorpID+"' ").list();
	    }
	 
	 public List findSummerTimeSheetsForOT(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 	String otCrewQuery;
		 	otCrewQuery = "select max(t.id),t.username,t.workdate,t.calculationcode,t.action,sum(t.regularHours),sum(IF(STRCMP(lunch, 'No lunch'),1,0)) as lnc, t.regularPayPerHour as rph, sum(t.paidRevenueShare) as prs, t.crewName, t.crewType from timesheet as t left outer join crew as c on t.userName=c.userName where (t.workDate >='"+beginDtNew+"' and t.workDate<='"+endDtNew+"') and t.action='c' AND t.warehouse = '"+wareHse+"' and c.overTimeType in ('S','W','U') and t.corpID='"+sessionCorpID+"' and c.corpID='"+sessionCorpID+"' group by t.workDate,t.userName";
		 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
	    	return listOTCrew;
	 }
	 
	 
	 public List findNonRevenueHrs(String crewNm, String workDt, String wareHse){
		 getHibernateTemplate().bulkUpdate("update TimeSheet set regularHours='' where regularHours is null");
		 workDt=workDt.replace(".0", "");
		 //System.out.println("\n\n\n\n\n select sum(regularHours) from TimeSheet where userName='"+crewNm+"' AND workDate='"+workDt+"' AND action = 'C' AND warehouse='"+wareHse+"' AND calculationCode = 'H' group by workDate");
		 return getHibernateTemplate().find("select sum(regularHours) from TimeSheet where userName='"+crewNm+"' AND workDate='"+workDt+"' AND action = 'C' AND warehouse = '"+wareHse+"' AND calculationCode = 'H' group by workDate");
	    }
	 
	 public List findOTHrs(String crewNm, String workDt, String wareHse){
		 getHibernateTemplate().bulkUpdate("update TimeSheet set overTime='0' where overTime is null");
		 workDt=workDt.replace(".0", "");
		 return getHibernateTemplate().find("select sum(overTime) from TimeSheet where userName='"+crewNm+"' AND workDate='"+workDt+"' AND action = 'C' AND warehouse = '"+wareHse+"' AND calculationCode = 'H' group by workDate");
	    }
	 
	 public List findAllTimeSheets(String beginDt, String endDt, String wareHse, String sessionCorpID){
		 try {
			 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
		        beginDtNew = nowYYYYMMDD.toString();
		        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
		        endDtNew = nowYYYYMMDD1.toString();
		 	}catch (ParseException e) {
				
				e.printStackTrace();
			}
		 	return getHibernateTemplate().find("Select count(*) from TimeSheet where (workDate >='"+beginDtNew+"' and workDate<='"+endDtNew+"') AND action = 'C' AND warehouse = '"+wareHse+"' AND corpID='"+sessionCorpID+"'");
	    }
	 public List checkExtraPaidTimeSheet(String workDt,String userName){
	    	return getHibernateTemplate().find("from TimeSheet where workDate='"+workDt+"' AND userName = '"+userName+"' AND action='E'");
	    }
	 private String endDtNew1;
	 public List checkActionOTimeSheet(String workDt,String userName){
		 try{
		 	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(workDt) ) );
	        endDtNew1 = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			e.printStackTrace();
		}
	    	return getHibernateTemplate().find("from TimeSheet where workDate='"+endDtNew1+"' AND userName = '"+userName+"' AND action='O'");
	    }

	public List getTimeFromEndHours(Long ticket, String crewName, Date date1) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1 ) );
		return getHibernateTemplate().find("select max(endHours) from TimeSheet where ticket in(select ticket from TimeSheet where crewName in('"+crewName.replaceAll("'","")+"') and workdate='"+formatedDate1+"' and action='C') and crewName in ('"+crewName.replaceAll("'", "")+"') and workdate='"+formatedDate1+"' and action='C' group by crewName");
	}

	public List getBiginHoursTime(Long ticket, String crewName, Date workDate) {
		return getHibernateTemplate().find("select max(endHours) from TimeSheet where ticket in (select ticket from TimeSheet where crewName='"+crewName.replaceAll("'","")+"'and workdate='"+workDate+"' group by ticket) and crewName='"+crewName.replaceAll("'","")+"' and workdate='"+workDate+"' and action='C'" );
	}

	public List findRegularHours(Long ticket, String crewName, Date workDate) {
		return getHibernateTemplate().find("select regularHours from TimeSheet where ticket='"+ticket+"' and crewName='"+crewName.replaceAll("'","")+"' and workDate='"+workDate+"' group by regularHours");
	}

	public void updateDoneForTheDay(String crewName, Date workDate, String DFD) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( workDate ) );
		//System.out.println("\n\n\n\nupdate TimeSheet set doneForTheDay='true' where crewName='"+crewName+"' and workDate='"+formatedDate1+"'");
		getHibernateTemplate().bulkUpdate("update TimeSheet set doneForTheDay='"+DFD+"' where crewName='"+crewName+"' and workDate='"+formatedDate1+"'");
		
	}
	Date beginDate;
	Date endDate;
	public List findOverTimeList(String beginDt, String endDt, String wareHse) {
		List overTime = new ArrayList();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
			
			endDate = dateformatYYYYMMDD1.parse( endDt );
		} catch (ParseException e) {
	      e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( endDate ) );
		//System.out.println("\n\n\n\n\nfrom TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and warehouse = '"+wareHse+"' and corpID = 'SSCW' order by userName, workDate, beginHours ");
		String overTimeQuery="select if(c.overTimeType not in ('S','W','U'),crewName, concat(crewName,' (U)')) Crew,	workDate, t.warehouse, ticket, shipper, action,	crewType, calculationCode, distributionCode, beginHours, endHours,round(paidRevenueShare,2), regularHours, overTime, doubleOverTime, regularPayPerHour	from timesheet as t, crew as c where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and t.warehouse = '"+wareHse+"' and t.corpID = 'SSCW'  and action in ('C','E','O') and c.userName = t.userName order by crewName, workDate, action,beginHours";
		List overTimeList= this.getSession().createSQLQuery(overTimeQuery).list();
		Iterator it=overTimeList.iterator();
		while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew=new AvailableCrew();
	           if(row[0] == null){availableCrew.setCrewName("");}else{availableCrew.setCrewName(row[0].toString());}
	           if(row[1] == null){
	        	   try {
					availableCrew.setWorkDate(dateformatYYYYMMDD2.parse(""));
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
	        	}else{
	        		try {
						availableCrew.setWorkDate(dateformatYYYYMMDD2.parse(row[1].toString()));
					} catch (ParseException e) {
						
						e.printStackTrace();
					}
	        	}
	           if(row[2] == null){availableCrew.setWarehouse("");}else{availableCrew.setWarehouse(row[2].toString());}
	           if(row[3] == null){availableCrew.setTicket("");}else{availableCrew.setTicket(row[3].toString());}
	           if(row[4] == null){availableCrew.setShipper("");}else{availableCrew.setShipper(row[4].toString());}
	           if(row[5] == null){availableCrew.setAction("");}else{availableCrew.setAction(row[5].toString());}
	           if(row[6] == null){availableCrew.setCrewType("");}else{availableCrew.setCrewType(row[6].toString());}
	           if(row[7] == null){availableCrew.setCalculationCode("");}else{availableCrew.setCalculationCode(row[7].toString());}
	           if(row[8] == null){availableCrew.setDistributionCode("");}else{availableCrew.setDistributionCode(row[8].toString());}
	           if(row[9] == null){availableCrew.setBeginHours("");}else{availableCrew.setBeginHours(row[9].toString());}
	           if(row[10] == null){availableCrew.setEndHours("");}else{availableCrew.setEndHours(row[10].toString());}
	           if(row[11] == null){availableCrew.setPaidRevenueShare("");}else{availableCrew.setPaidRevenueShare(row[11].toString());}
	           if(row[12] == null){availableCrew.setRegularHours("");}else{availableCrew.setRegularHours(row[12].toString());}
	           if(row[13] == null){availableCrew.setOverTime("");}else{availableCrew.setOverTime(row[13].toString());}
	           if(row[14] == null){availableCrew.setDoubleOverTime("");}else{availableCrew.setDoubleOverTime(row[14].toString());}
	           if(row[15] == null){availableCrew.setRegularPayPerHour("");}else{availableCrew.setRegularPayPerHour(row[15].toString());}
	           overTime.add(availableCrew);
	       }
		/*List overtimeList= getHibernateTemplate().find("from TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and warehouse = '"+wareHse+"' and corpID = 'SSCW' order by userName, workDate, beginHours");
		System.out.println("\n\n\n\n overtimeList-->"+overtimeList);*/
		if(overTime.isEmpty()){
			//System.out.println("\n\n list is empty-->>");
			return null;
		}else{
			//System.out.println("\n\n list is full-->>");
			return overTime;
		}
		
	}

	public List findPreviewOverTimeList(String beginDt, String endDt, String wareHse) {
		List overTime = new ArrayList();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
			
			endDate = dateformatYYYYMMDD1.parse( endDt );
		} catch (ParseException e) {
	      e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( endDate ) );
		//System.out.println("\n\n\n\n\nfrom TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and warehouse = '"+wareHse+"' and corpID = 'SSCW' order by userName, workDate, beginHours ");
		String overTimeQuery="select if(c.overTimeType not in ('S','W','U'),crewName, concat(crewName,' (U)')) Crew,	workDate, t.warehouse, ticket, shipper, action,	crewType, calculationCode, distributionCode, beginHours, endHours,round(paidRevenueShare,2), previewRegularHours, previewOverTime, doubleOverTime, regularPayPerHour	from timesheet as t, crew as c where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and t.warehouse = '"+wareHse+"' and t.corpID = 'SSCW'  and action in ('C','E','O') and c.userName = t.userName order by crewName, workDate, action,beginHours";
		List overTimeList= this.getSession().createSQLQuery(overTimeQuery).list();
		Iterator it=overTimeList.iterator();
		while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew=new AvailableCrew();
	           if(row[0] == null){availableCrew.setCrewName("");}else{availableCrew.setCrewName(row[0].toString());}
	           if(row[1] == null){
	        	   try {
					availableCrew.setWorkDate(dateformatYYYYMMDD2.parse(""));
				} catch (ParseException e) {
					
					e.printStackTrace();
				}
	        	}else{
	        		try {
						availableCrew.setWorkDate(dateformatYYYYMMDD2.parse(row[1].toString()));
					} catch (ParseException e) {
						
						e.printStackTrace();
					}
	        	}
	           if(row[2] == null){availableCrew.setWarehouse("");}else{availableCrew.setWarehouse(row[2].toString());}
	           if(row[3] == null){availableCrew.setTicket("");}else{availableCrew.setTicket(row[3].toString());}
	           if(row[4] == null){availableCrew.setShipper("");}else{availableCrew.setShipper(row[4].toString());}
	           if(row[5] == null){availableCrew.setAction("");}else{availableCrew.setAction(row[5].toString());}
	           if(row[6] == null){availableCrew.setCrewType("");}else{availableCrew.setCrewType(row[6].toString());}
	           if(row[7] == null){availableCrew.setCalculationCode("");}else{availableCrew.setCalculationCode(row[7].toString());}
	           if(row[8] == null){availableCrew.setDistributionCode("");}else{availableCrew.setDistributionCode(row[8].toString());}
	           if(row[9] == null){availableCrew.setBeginHours("");}else{availableCrew.setBeginHours(row[9].toString());}
	           if(row[10] == null){availableCrew.setEndHours("");}else{availableCrew.setEndHours(row[10].toString());}
	           if(row[11] == null){availableCrew.setPaidRevenueShare("");}else{availableCrew.setPaidRevenueShare(row[11].toString());}
	           if(row[12] == null){availableCrew.setRegularHours("");}else{availableCrew.setRegularHours(row[12].toString());}
	           if(row[13] == null){availableCrew.setOverTime("");}else{availableCrew.setOverTime(row[13].toString());}
	           if(row[14] == null){availableCrew.setDoubleOverTime("");}else{availableCrew.setDoubleOverTime(row[14].toString());}
	           if(row[15] == null){availableCrew.setRegularPayPerHour("");}else{availableCrew.setRegularPayPerHour(row[15].toString());}
	           overTime.add(availableCrew);
	       }
		/*List overtimeList= getHibernateTemplate().find("from TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and warehouse = '"+wareHse+"' and corpID = 'SSCW' order by userName, workDate, beginHours");
		System.out.println("\n\n\n\n overtimeList-->"+overtimeList);*/
		if(overTime.isEmpty()){
			//System.out.println("\n\n list is empty-->>");
			return null;
		}else{
			//System.out.println("\n\n list is full-->>");
			return overTime;
		}
		
	}
	
	public void updateCrewType(String ticket) {
		
		getHibernateTemplate().bulkUpdate("update TimeSheet set calculationCode='H' , paidRevenueShare='0.00' where ticket='"+ticket+"'");
		getHibernateTemplate().bulkUpdate("update WorkTicket set revenueCalculation='H' where ticket='"+ticket+"'");
		
	}

	public void deleteFromTimeSheet(String beginDt, String endDt, String wareHse, String sessionCorpID) {
		
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
			
			endDate = dateformatYYYYMMDD1.parse( endDt );
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( endDate ) );
		getHibernateTemplate().bulkUpdate("delete from TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and   warehouse = '"+wareHse+"' and action in ('E','O') and corpID='"+sessionCorpID+"'");
		
	}

	public List getNumberOfCrew(String beginDt, String endDt, String wareHse, String sessionCorpID) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
			
			endDate = dateformatYYYYMMDD1.parse( endDt );
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( endDate ) );
		return getHibernateTemplate().find("select distinct crewName from TimeSheet where workDate between '"+formatedDate1+"' and '"+formatedDate2+"' and warehouse = '"+wareHse+"' and corpID='"+sessionCorpID+"'");
	}

	public List getMaxId(String userName) {
		return getHibernateTemplate().find("select max(id) from TimeSheet where userName='"+userName+"'");
	}

	

	public List doneForDay(String wareHse, String newWrkDt, String tkt, String crewNm) {
		wareHse = wareHse.replaceAll("'", "''");
		crewNm = crewNm.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		/*if(!tkt.equalsIgnoreCase("")){
				 if(date1.equalsIgnoreCase("")){
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+" ");
				 }else{
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND workDate ='"+date1+"' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+"");
				 }
		 	}else{*/
		 		 if(newWrkDt.equalsIgnoreCase("")){
		 			 if(tkt == null || tkt.equalsIgnoreCase("")){
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"'  AND crewName like '%"+crewNm+"%' and doneForTheDay='true' ORDER BY crewName, beginHours");
		 			 }else{
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and doneForTheDay='true' ORDER BY crewName, beginHours");
			 		 }
				 }else{
					 if(tkt == null || tkt.equalsIgnoreCase("")){
					 	 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' and doneForTheDay='true' ORDER BY crewName, beginHours");
					 }else{
						 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and doneForTheDay='true' ORDER BY crewName, beginHours");
					 }
				 }
		 	/*}*/
		 
		 
		 return timeSheets;
	}

	public List excludeEearningOppertunity(String wareHse, String newWrkDt, String tkt, String crewNm) {
		wareHse = wareHse.replaceAll("'", "''");
		crewNm = crewNm.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		/*if(!tkt.equalsIgnoreCase("")){
				 if(date1.equalsIgnoreCase("")){
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+" ");
				 }else{
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND workDate ='"+date1+"' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+"");
				 }
		 	}else{*/
		 		 if(newWrkDt.equalsIgnoreCase("")){
		 			 if(tkt == null || tkt.equalsIgnoreCase("")){
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"'  AND crewName like '%"+crewNm+"%' AND action<>'E' ORDER BY crewName, beginHours");
		 			 }else{
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' AND action<>'E' ORDER BY crewName, beginHours");
			 		 }
				 }else{
					 if(tkt == null || tkt.equalsIgnoreCase("")){
					 	 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND action<>'E' ORDER BY crewName, beginHours");
					 }else{
						 //System.out.println("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' AND action<>'E' ORDER BY crewName, beginHours");
						 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' AND action<>'E' ORDER BY crewName, beginHours");
					 }
				 }
		 	/*}*/
		 
		 
		 return timeSheets;
	}

	public List filterLunch(String wareHse, String newWrkDt, String tkt, String crewNm) {
		wareHse = wareHse.replaceAll("'", "''");
		crewNm = crewNm.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		/*if(!tkt.equalsIgnoreCase("")){
				 if(date1.equalsIgnoreCase("")){
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+" ");
				 }else{
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND workDate ='"+date1+"' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+"");
				 }
		 	}else{*/
		 		 if(newWrkDt.equalsIgnoreCase("")){
		 			 if(tkt == null || tkt.equalsIgnoreCase("")){
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"'  AND crewName like '%"+crewNm+"%' and action<>'L' ORDER BY crewName, beginHours");
		 			 }else{
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and action<>'L' ORDER BY crewName, beginHours");
			 		 }
				 }else{
					 if(tkt == null || tkt.equalsIgnoreCase("")){
					 	 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' and action<>'L' ORDER BY crewName, beginHours");
					 }else{
						 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and action<>'L' ORDER BY crewName, beginHours");
					 }
				 }
		 	/*}*/
		 
		 
		 return timeSheets;
	}

	public void updateOtherTimeSheet(String bhours, String eHours, String regHours, String doneForDay, String pRevShare, String id, String action, String lunch, String reasonForAdjustment, String overTime, String doubleOverTime, String differentPayBasis, String adjustmentToRevenue, String adjustedBy,String calcCode, String distCode) {
				
			if(differentPayBasis.equalsIgnoreCase("B"))
			{
				getHibernateTemplate().bulkUpdate("update TimeSheet set beginHours='"+bhours+"', endHours='"+eHours+"',regularHours='"+regHours+"' , doneForTheDay='"+doneForDay+"', paidRevenueShare='"+pRevShare+"' , action='"+action+"', lunch='"+lunch+"',reasonForAdjustment='"+reasonForAdjustment+"', overTime='"+overTime+"', doubleOverTime='"+doubleOverTime+"', adjustmentToRevenue='"+adjustmentToRevenue+"', adjustedBy='"+adjustedBy+"', calculationCode='"+calcCode+"', distributionCode='"+distCode+"'  where id in ("+id+")");
			}
			if(differentPayBasis.equalsIgnoreCase("A") || differentPayBasis.equalsIgnoreCase(""))
			{
				getHibernateTemplate().bulkUpdate("update TimeSheet set beginHours='"+bhours+"', endHours='"+eHours+"',regularHours='"+regHours+"' , doneForTheDay='"+doneForDay+"', paidRevenueShare='"+pRevShare+"' , action='"+action+"', lunch='"+lunch+"',reasonForAdjustment='"+reasonForAdjustment+"', overTime='"+overTime+"', doubleOverTime='"+doubleOverTime+"', adjustmentToRevenue='"+adjustmentToRevenue+"', adjustedBy='"+adjustedBy+"', calculationCode='"+calcCode+"' , differentPayBasis='"+differentPayBasis+"' , distributionCode='"+distCode+"' where id in ("+id+")");
			}
			
			
		}

	public List getParentTimeSheet(String userName, Date workDate,String lunch) {
		String check="from TimeSheet where crewName='"+userName+"' AND workDate='"+workDate+"' and lunch='"+lunch+"'";
		List p= getHibernateTemplate().find("from TimeSheet where crewName='"+userName+"' AND workDate='"+workDate+"' and lunch='"+lunch+"'");
		return p;
		
	}

	public void updateRegHours(String regHours, String userName, Date workDate, String lunch) {
		//System.out.println("\n\n\n\n update TimeSheet set regularHours='"+regHours+"' where crewName='"+userName+"' AND workDate='"+workDate+"' and lunch='"+lunch+"'");
		getHibernateTemplate().bulkUpdate("update TimeSheet set regularHours='"+regHours+"', lunch='No Lunch' where crewName='"+userName+"' AND workDate='"+workDate+"' and lunch='"+lunch+"'");		
	}

	public List openTimeSheets(String wareHse, String newWrkDt, String tkt, String crewNm) {
		wareHse = wareHse.replaceAll("'", "''");
		crewNm = crewNm.replaceAll("'", "''");
		 tkt = tkt.replaceAll("'", "");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		/*if(!tkt.equalsIgnoreCase("")){
				 if(date1.equalsIgnoreCase("")){
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+" ");
				 }else{
					 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse like '" +warehouse+"%' AND workDate ='"+date1+"' AND shipper like '"+shipper+"%' AND crewName like '"+crewNm+"%' AND ticket = "+tkt+"");
				 }
		 	}else{*/
		 		 if(newWrkDt.equalsIgnoreCase("")){
		 			 if(tkt == null || tkt.equalsIgnoreCase("")){
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"'  AND crewName like '%"+crewNm+"%' and doneForTheDay='false'  ORDER BY crewName, beginHours");
		 			 }else{
		 				 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and doneForTheDay='false' ORDER BY crewName, beginHours");
			 		 }
				 }else{
					 if(tkt == null || tkt.equalsIgnoreCase("")){
						 //System.out.println("\n\n\n\nfrom TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' and doneForTheDay='false' ORDER BY crewName, beginHours");
					 	 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' and doneForTheDay='false' ORDER BY crewName, beginHours");
					 }else{
						 timeSheets = getHibernateTemplate().find("from TimeSheet where warehouse = '" +wareHse+"' AND workDate like '"+newWrkDt+"%'  AND crewName like '%"+crewNm+"%' AND ticket like '"+tkt+"%' and doneForTheDay='false' ORDER BY crewName, beginHours");
					 }
				 }
		 	/*}*/
		 
		 
		 return timeSheets;
	}

	public List getWorkTicket(Long ticket, String sessionCorpID) {
		return getHibernateTemplate().find("from WorkTicket where ticket='"+ticket+"' and corpID='"+sessionCorpID+"'");
	}

	public void deleteLunchTicket(Date date, String name, String house) {
		getHibernateTemplate().bulkUpdate("delete from TimeSheet where userName='"+name+"' and warehouse='"+house+"' and action='L' and workDate=?", date);
		
	}

	public List checkRegularHours(String beginDt, String endDt, String wareHse) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
			
			endDate = dateformatYYYYMMDD1.parse( endDt );
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		StringBuilder formatedDate2 = new StringBuilder( dateformatYYYYMMDD2.format( endDate ) );
		return getHibernateTemplate().find("select workDate,ticket,crewName from TimeSheet where workDate BETWEEN '"+formatedDate1+"' AND '"+formatedDate2+"' and regularHours is null and warehouse ='"+wareHse+"' and action='C' ORDER BY workDate,ticket,regularHours");
	}

	public List getBillingDiscount(String shipNumber, String sessionCorpID) {
		return getHibernateTemplate().find("select discount from Billing where shipNumber='"+shipNumber+"' and corpID='"+sessionCorpID+"'");
	}

	public List getCompanyHolidays(String sessionCorpID, String workDate) {
		return this.getSession().createSQLQuery("select count(*) from calendarfile where userName = '"+sessionCorpID+"' and corpId = '"+sessionCorpID+"' and (toTime-fromTime) >= 8 and surveyDate = '"+workDate+"'").list();
		
		
	}

	public List findDistributionCode(String actionCode, String sessionCorpID) {
		return getHibernateTemplate().find("select bucket2 from RefMaster where parameter='TIMETYPE' and code='"+actionCode+"'");
	}

	public void updateRefmasterStopDate(String beginDt, String sessionCorpID, String wareHse) {
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		try {
			beginDate = dateformatYYYYMMDD1.parse( beginDt );
		
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedBeginDate = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		getHibernateTemplate().bulkUpdate("update RefMaster set stopDate='"+formatedBeginDate+"' where  code='"+wareHse+"' and parameter='HOUSE'");
		
	}

	public List checkStopDate(String workDt, String sessionCorpID, String wareHse) {
		List getDiff=new ArrayList();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		try {
			beginDate = dateformatYYYYMMDD1.parse( workDt );
		
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedWorkDate = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );
		List stopDateOfRefMaster=getHibernateTemplate().find("select stopDate from RefMaster where code = '"+wareHse+"' and parameter='HOUSE' and stopDate is not null ");
		if(!(stopDateOfRefMaster.isEmpty()) && !(stopDateOfRefMaster==null))
		{
			getDiff= this.getSession().createSQLQuery("select datediff('"+formatedWorkDate+"', '"+stopDateOfRefMaster.get(0) +"')").list();
			//System.out.println("\n\n\n\n\n getDiff--->>>"+getDiff);
			
		}
		
		return getDiff;
	}

	public List getLockPayrollList(String sessionCorpID) {
		return getHibernateTemplate().find("from RefMaster where  parameter='HOUSE'");
	}

	public List getcompanyDivision(String calcCode, String sessionCorpID) {
		return getHibernateTemplate().find("select companyDivision from Sharing where corpId='"+sessionCorpID+"' AND code=?",calcCode);
	}

	public List getWarehouse(String id) {
		return getHibernateTemplate().find("select warehouse from TimeSheet where id='"+id+"'");
	}

	public List checkStopDateForCrew(Date date1, String sessionCorpID, String warehouse) {
		List getDiff=new ArrayList();
		/*SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("dd-MMM-yy");
		try {
			beginDate = dateformatYYYYMMDD1.parse( workDt );
		
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedWorkDate = new StringBuilder( dateformatYYYYMMDD2.format( beginDate ) );*/
		List stopDateOfRefMaster=getHibernateTemplate().find("select stopDate from RefMaster where  code='"+warehouse+"' and parameter='HOUSE'");
		if(!(stopDateOfRefMaster.isEmpty()) && !(stopDateOfRefMaster==null))
		{
			getDiff= this.getSession().createSQLQuery("select datediff('"+date1+"', '"+stopDateOfRefMaster.get(0) +"')").list();
			//System.out.println("\n\n\n\n\n getDiff--->>>"+getDiff);
			
		}
		
		return getDiff;
	}

	public List findTimeSheetsForNonUnionOTPost(String beginDt, String endDt, String wareHse, String sessionCorpID, String userName) {
		
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
	        beginDtNew = nowYYYYMMDD.toString();
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
	        endDtNew = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			
			e.printStackTrace();
		}
	 	String otCrewQuery;
	 	otCrewQuery = "select round(t.regularHours,1), t.id  from timesheet as t where (t.workDate >='"+beginDtNew+"' and t.workDate<='"+endDtNew+"') and t.action='c' AND t.warehouse='"+wareHse+"' and t.corpID='"+sessionCorpID+"' and t.calculationcode='H' and t.userName='"+userName+"' order by id desc";
	 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
    	return listOTCrew;
	}

	public List findTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName) {
		/*try {
		 	
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(workDate) ) );
	        endDtNew = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			
			e.printStackTrace();
		}*/
	 	String otCrewQuery;
	 	otCrewQuery = "select round(t.regularHours,1), t.id  from timesheet as t where (t.workDate ='"+workDate+"') and t.action='c' AND t.warehouse='"+wareHse2+"' and t.corpID='"+sessionCorpID+"' and t.userName='"+userName+"' order by id desc";
	 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
    	return listOTCrew;
	}

	public List findOTTimeSheetsForUnionOTPost(String workDate, String wareHse2, String sessionCorpID, String userName) {
		String otCrewQuery;
	 	otCrewQuery = "select sum(round(regularhours, 1)) from timesheet as t where (t.workDate ='"+workDate+"') and t.action='c' AND t.warehouse='"+wareHse2+"' and t.corpID='"+sessionCorpID+"' and t.userName='"+userName+"' group by userName";
	 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
    	return listOTCrew;
	}

	public List findWorkdate(String beginDt, String endDt, String wareHse2, String userName, String sessionCorpID2) {
		try {
		 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format( new SimpleDateFormat("dd-MMM-yy").parse(beginDt) ) );
	        beginDtNew = nowYYYYMMDD.toString();
	        SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder nowYYYYMMDD1 = new StringBuilder( dateformatYYYYMMDD1.format( new SimpleDateFormat("dd-MMM-yy").parse(endDt) ) );
	        endDtNew = nowYYYYMMDD1.toString();
	 	}catch (ParseException e) {
			
			e.printStackTrace();
		}
 	String otCrewQuery;
 	otCrewQuery = "select workDate from timesheet  t where (workDate between '"+beginDtNew+"' and '"+endDtNew+"') and t.action='c' AND t.warehouse='"+wareHse2+"' and t.corpID='"+sessionCorpID2+"'  and t.userName='"+userName+"' group by workDate";
 	List listOTCrew= this.getSession().createSQLQuery(otCrewQuery).list();
	return listOTCrew;
	}

	public List searchAvailableTrucks(String wareHse, String newWrkDt, String sessionCorpID,Boolean visibilityForCorpId) {
		 wareHse = wareHse.replaceAll("'", "''");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		 	List availableTrucks = new ArrayList();
		 	String query="";
		 	if(visibilityForCorpId){
		 	     query="select distinct t.localnumber,t.description, tr.isTruckUsed, t.id as truckId,DATEDIFF(t.dotInspDue,now()) as inspectionDue " +
		 			"from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
		 			"and tr.corpid='"+sessionCorpID+"' and workDate='"+newWrkDt+"' where t.corpid='"+sessionCorpID+"' " +
		 			"and t.warehouse='"+wareHse+"' and truckstatus = 'A' and ((t.docVehicle='Y'  and t.dotInspDue is not null) or t.dotInspDue is not null ) ";
		 	}else{
		 		 query="select distinct t.localnumber,t.description, tr.isTruckUsed, t.id as truckId,'70' as inspectionDue " +
			 			"from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
			 			"and tr.corpid='"+sessionCorpID+"' and workDate='"+newWrkDt+"' where t.corpid='"+sessionCorpID+"' " +
			 			"and t.warehouse='"+wareHse+"' and truckstatus = 'A' ";
		 	}
		 	List list= this.getSession().createSQLQuery(query).list();
			Iterator it=list.iterator();
			while(it.hasNext()){
				 Object []row= (Object [])it.next();
				 SeceduleTrucks seceduleTrucks= new SeceduleTrucks();
				 seceduleTrucks.setTruckNumber(row[0].toString());
				 seceduleTrucks.setDescription(row[1].toString());
				 if(row[2] == null || row[2].equals("")){seceduleTrucks.setIsTruckUsed(false);}else{seceduleTrucks.setIsTruckUsed(true);}
				 seceduleTrucks.setId(row[3].toString());
				 seceduleTrucks.setInspectionDueDiff(row[4].toString());
				 availableTrucks.add(seceduleTrucks);
			}
			return availableTrucks;
	}

	public List findSecheDuledResource(String wareHse, String newWrkDt,	String string, String tkt, String string2, String sessionCorpID) {
		List scheduleResourceList= new ArrayList();
		String query="";
		String columns = "s.id, s.ticket, s.shipper, s.estimatedCubicFeet, s.service,s.warehouse, " + //0,1,2,3,4,5
		"s.crewName, s.localtruckNumber,s.description,n.id as notesId, timesheetId as timesheetId, " + //6,7,8,9,10
		"s.truckingOpsId as truckingOpsId, s.shipNumber , s.workTicketId,s.originCity,s.destinationCity,s.actualWeight,s.actualVolume, s.isDriverUsed, s.unit1, s.unit2,s.estimatedWeight,w.scheduledArrive as aTime,w.timeFrom as dTime,s.integrationTool as integrationTool,w.serviceOrderId as serviceOrderId "; //11,12,13,14,15,16,17,18,19,20,21,22,23,24
        
		query="select " + columns +" from scheduleresourceoperation s " +
        "left outer join notes n on s.workTicketId=n.notesKeyId  and n.notesubtype='Scheduling' " +
        "left outer join workticket w on s.ticket=w.ticket and s.workTicketId=w.id and s.corpID=w.corpID "+
       	"where s.corpID='"+sessionCorpID+"' and s.workDate='"+newWrkDt+"' and s.warehouse='"+wareHse+"' ";
        
        List list= this.getSession().createSQLQuery(query).list();
        Iterator it=list.iterator();
        
        while(it.hasNext()){
        
        	Object []row= (Object [])it.next();
        	ScheduleResource scheduleResource= new ScheduleResource();
        	scheduleResource.setId(row[0].toString());
        	scheduleResource.setTicket(row[1].toString());
        	
        	String ars[]=row[2].toString().split(",");
        	
        	if(ars.length==1){
        		scheduleResource.setShipper(ars[0].toString());
        	}else{
        		scheduleResource.setShipper(row[2].toString());
        	}
        	
        	scheduleResource.setEstimatedCubicFeet(row[3]==null?"":row[3].toString());
        	scheduleResource.setService(row[4]==null?"":row[4].toString());
        	scheduleResource.setWarehouse(row[5]==null?"":row[5].toString());
        	scheduleResource.setCrewName(row[6]==null?"":row[6].toString());
        	
        	scheduleResource.setLocaltruckNumber(row[7]==null?"":row[7].toString());
        	scheduleResource.setDescription(row[8]==null?"":row[8].toString());
        	if(row[9] == null){scheduleResource.setNotesId("");}else{scheduleResource.setNotesId(row[9].toString());}
        	scheduleResource.setTimesheetId(row[10]==null?"":row[10].toString());
        	scheduleResource.setTruckingOpsId(row[11]==null?"":row[11].toString());
        	if(row[12]==null){
        		scheduleResource.setShipNumber("");	
        	}else{
        		scheduleResource.setShipNumber(row[12].toString());
        	}
        	if(row[13]==null){
        		scheduleResource.setWorkTicketId("");
        	}else{
        		scheduleResource.setWorkTicketId(row[13].toString());
        	}
        	if(row[14]==null){scheduleResource.setOriginCity("");}else{scheduleResource.setOriginCity(row[14].toString());}
        	if(row[15]==null){scheduleResource.setDestinationCity("");}else{scheduleResource.setDestinationCity(row[15].toString());}
        	if(row[16]==null){scheduleResource.setActualWeight("0.0");}else{scheduleResource.setActualWeight(row[16].toString());}
        	if(row[17]==null){scheduleResource.setActualVolume("0.0");}else{scheduleResource.setActualVolume(row[17].toString());}
        	if(row[18]==null){scheduleResource.setIsDriverUsed("");}else{scheduleResource.setIsDriverUsed(row[18].toString());}
        	if(row[19]==null){scheduleResource.setUnit1("");}else{scheduleResource.setUnit1(row[19].toString());}
        	if(row[20]==null){scheduleResource.setUnit2("");}else{scheduleResource.setUnit2(row[20].toString());}
        	if(row[21]==null){scheduleResource.setEstimatedWeight("0.0");}else{scheduleResource.setEstimatedWeight(row[21].toString());}
        	if(row[22]==null){scheduleResource.setaTime("00:00");}else{scheduleResource.setaTime(row[22].toString());}
        	if(row[23]==null){scheduleResource.setdTime("00:00");}else{scheduleResource.setdTime(row[23].toString());}
        	if(row[24]==null){scheduleResource.setIntegrationTool("");}else{scheduleResource.setIntegrationTool(row[24].toString());}
        	scheduleResource.setServiceOrderId(Long.parseLong(row[25].toString()));
        	scheduleResourceList.add(scheduleResource);
        }
		
		//return getHibernateTemplate().find("from ScheduleResourceOperation where corpID='"+sessionCorpID+"' and workDate='"+newWrkDt+"'");
		return scheduleResourceList;
		//return getHibernateTemplate().find("from ScheduleResourceOperation where corpID='"+sessionCorpID+"' and workDate='"+newWrkDt+"' and warehouse='"+wareHse+"'");
		
	}
	
	public Map<Map, Map> findSecheDuledResourceMap(String wareHse, String newWrkDt,String sessionCorpID,String whereClauseSecheDuledResourceMap,String whereClauseSecheDuledResourceMap1){
		Map<Map, Map> finalMap= new LinkedHashMap<Map, Map>();
		
		if(whereClauseSecheDuledResourceMap ==null || whereClauseSecheDuledResourceMap.equalsIgnoreCase("")){
			whereClauseSecheDuledResourceMap="";
		}
		if(whereClauseSecheDuledResourceMap1 ==null || whereClauseSecheDuledResourceMap1.equalsIgnoreCase("")){
			whereClauseSecheDuledResourceMap1 ="";
		}
        	
        String query="SELECT a.* FROM (select distinct w.ticket, s.shipper,s.estimatedCubicFeet,s.service,s.shipNumber,s.workTicketId,s.originCity,s.destinationCity,"
        		+ " s.actualWeight,s.actualVolume,s.unit1, s.unit2,s.estimatedWeight,w.scheduledArrive as aTime,w.timeFrom as dTime,s.integrationTool as integrationTool,n.id as notesId,"
        		+ " (select sum(it.qty) from itemsjbkequip it where it.ticket= w.ticket and it.corpid=w.corpid ) as quantitySum,w.projectLeadName,w.projectLeadNumber,if(w.acceptByDriver,'true','false')"
        		+ " from scheduleresourceoperation s left outer join workticket w on s.ticket=w.ticket and s.workTicketId=w.id and s.corpID=w.corpID"
        		+ " left outer join notes n on s.workTicketId=n.notesKeyId  and n.notesubtype='Scheduling'"
        		+ " where s.corpID='"+sessionCorpID+"' and s.workDate='"+newWrkDt+"' and s.warehouse='"+wareHse+"' "+whereClauseSecheDuledResourceMap+" )a"
        		+ " union"
        		+ " SELECT b.* FROM (select distinct w.ticket, s.shipper,s.estimatedCubicFeet,s.service,s.shipNumber,s.workTicketId,s.originCity,s.destinationCity,"
        		+ " s.actualWeight,s.actualVolume,s.unit1, s.unit2,s.estimatedWeight,w.scheduledArrive as aTime,w.timeFrom as dTime,s.integrationTool as integrationTool,n.id as notesId,"
        		+ " (select sum(it.qty) from itemsjbkequip it where it.ticket= w.ticket and it.corpid=w.corpid ) as quantitySum,w.projectLeadName,w.projectLeadNumber,if(w.acceptByDriver,'true','false')"
        		+ " from scheduleresourceoperation s left outer join workticket w on s.ticket=w.ticket and s.workTicketId=w.id and s.corpID=w.corpID"
        		+ " left outer join notes n on s.workTicketId=n.notesKeyId  and n.notesubtype='Scheduling'"
        		+ " where s.corpID='"+sessionCorpID+"' and s.workDate='"+newWrkDt+"' and s.warehouse='"+wareHse+"' "+whereClauseSecheDuledResourceMap1+" )b";
        	
        
        List list= this.getSession().createSQLQuery(query).list();
        Iterator it=list.iterator();
        
        while(it.hasNext()){
        	ScheduleResource scheduleResource= new ScheduleResource();
        	Object []row= (Object [])it.next();
        	scheduleResource.setTicket(row[0].toString());
        	
        	String ars[]=row[1].toString().split(",");
        	
        	if(ars.length==1){
        		scheduleResource.setShipper(ars[0].toString());
        	}else{
        		scheduleResource.setShipper(row[1].toString());
        	}
        	
        	scheduleResource.setEstimatedCubicFeet(row[2]==null?"":row[2].toString());
        	scheduleResource.setService(row[3]==null?"":row[3].toString());
        	
        	if(row[4]==null){
        		scheduleResource.setShipNumber("");	
        	}else{
        		scheduleResource.setShipNumber(row[4].toString());
        	}
        	if(row[5]==null){
        		scheduleResource.setWorkTicketId("");
        	}else{
        		scheduleResource.setWorkTicketId(row[5].toString());
        	}
        	if(row[6]==null){scheduleResource.setOriginCity("");}else{scheduleResource.setOriginCity(row[6].toString());}
        	if(row[7]==null){scheduleResource.setDestinationCity("");}else{scheduleResource.setDestinationCity(row[7].toString());}
        	if(row[8]==null){scheduleResource.setActualWeight("0.0");}else{scheduleResource.setActualWeight(row[8].toString());}
        	if(row[9]==null){scheduleResource.setActualVolume("0.0");}else{scheduleResource.setActualVolume(row[9].toString());}
        	if(row[10]==null){scheduleResource.setUnit1("");}else{scheduleResource.setUnit1(row[10].toString());}
        	if(row[11]==null){scheduleResource.setUnit2("");}else{scheduleResource.setUnit2(row[11].toString());}
        	if(row[12]==null){scheduleResource.setEstimatedWeight("0.0");}else{scheduleResource.setEstimatedWeight(row[12].toString());}
        	if(row[13]==null){scheduleResource.setaTime("00:00");}else{scheduleResource.setaTime(row[13].toString());}
        	if(row[14]==null){scheduleResource.setdTime("00:00");}else{scheduleResource.setdTime(row[14].toString());}
        	if(row[15]==null){scheduleResource.setIntegrationTool("");}else{scheduleResource.setIntegrationTool(row[15].toString());}
        	if(row[16] == null){scheduleResource.setNotesId("");}else{scheduleResource.setNotesId(row[16].toString());}
        	if(row[17] == null){scheduleResource.setQuantitySum("");}else{scheduleResource.setQuantitySum(row[17].toString());}
        	if(row[18] == null){scheduleResource.setProjectLeadName("");}else{scheduleResource.setProjectLeadName(row[18].toString());}
        	if(row[19] == null){scheduleResource.setProjectLeadNumber("");}else{scheduleResource.setProjectLeadNumber(row[19].toString());}
        	scheduleResource.setAcceptByDriver(row[20].toString());
        	List scheduleResourceList= new ArrayList();
        	scheduleResourceList.add(scheduleResource);
        	Map<String, List> scheduleResourceMap= new LinkedHashMap<String, List>();
        	scheduleResourceMap.put(row[0].toString(), scheduleResourceList);
        	List crewListAll = findScheduleResourceSeparateCrewList(wareHse, newWrkDt,row[0].toString(), sessionCorpID,scheduleResource.getProjectLeadName(),scheduleResource.getProjectLeadNumber());
        	List truckListAll = findScheduleResourceSeparateTruckList(wareHse, newWrkDt,row[0].toString(), sessionCorpID,scheduleResource.getProjectLeadName(),scheduleResource.getProjectLeadNumber());
        	Map<List, List> crewTruckMap= new LinkedHashMap<List, List>();
        	crewTruckMap.put(crewListAll, truckListAll);
        	finalMap.put(scheduleResourceMap,crewTruckMap);
        }		
		return finalMap;
	}
	
	public class SeceduleTrucks{
		private String id;
		private String description;
		private String truckNumber;
		private Boolean isTruckUsed;
		private String inspectionDueDiff;
		
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getTruckNumber() {
			return truckNumber;
		}
		public void setTruckNumber(String truckNumber) {
			this.truckNumber = truckNumber;
		}
		public Boolean getIsTruckUsed() {
			return isTruckUsed;
		}
		public void setIsTruckUsed(Boolean isTruckUsed) {
			this.isTruckUsed = isTruckUsed;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getInspectionDueDiff() {
			return inspectionDueDiff;
		}
		public void setInspectionDueDiff(String inspectionDueDiff) {
			this.inspectionDueDiff = inspectionDueDiff;
		}
	}
	
	/*public class SecheduleResource{
		private String crewName;
		private String workDate;
		private String ticket;
		private String shipper;
		private String beginDate;
		private String estimatedWeight;
		private String service;
		private String warehouse;
		private String truckDescription;
		private String truckLocalNumber;
		private String timeSheetId;
		private String truckOpsId;
		
		public String getCrewName() {
			return crewName;
		}
		public void setCrewName(String crewName) {
			this.crewName = crewName;
		}
		public String getWorkDate() {
			return workDate;
		}
		public void setWorkDate(String workDate) {
			this.workDate = workDate;
		}
		public String getTicket() {
			return ticket;
		}
		public void beginDate(String ticket) {
			this.ticket = ticket;
		}
		public String getShipper() {
			return shipper;
		}
		public void setShipper(String shipper) {
			this.shipper = shipper;
		}
		public String getBeginDate() {
			return beginDate;
		}
		public void setBeginDate(String beginDate) {
			this.beginDate = beginDate;
		}
		public String getEstimatedWeight() {
			return estimatedWeight;
		}
		public void setEstimatedWeight(String estimatedWeight) {
			this.estimatedWeight = estimatedWeight;
		}
		public String getService() {
			return service;
		}
		public void setService(String service) {
			this.service = service;
		}
		public String getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(String warehouse) {
			this.warehouse = warehouse;
		}
		public String getTruckDescription() {
			return truckDescription;
		}
		public void setTruckDescription(String truckDescription) {
			this.truckDescription = truckDescription;
		}
		public String getTruckLocalNumber() {
			return truckLocalNumber;
		}
		public void setTruckLocalNumber(String truckLocalNumber) {
			this.truckLocalNumber = truckLocalNumber;
		}
		public void setTicket(String ticket) {
			this.ticket = ticket;
		}
		public String getTimeSheetId() {
			return timeSheetId;
		}
		public void setTimeSheetId(String timeSheetId) {
			this.timeSheetId = timeSheetId;
		}
		public String getTruckOpsId() {
			return truckOpsId;
		}
		public void setTruckOpsId(String truckOpsId) {
			this.truckOpsId = truckOpsId;
		}
		
		
		
	}*/
	
	public class ResourceWorkTickets{
		private String id;
		private String ticket;
		private String shipNumber;
		private String lastName;
		private String firstName;
		private Date date1;
		private Date date2;
		private String service;
		private String warehouse;
		private String estimatedWeight;
		private String estimatedCubicFeet;
		private Boolean isCrewUsed;
		private String jobType;
		private String city;
		private String destinationCity;
		private String notesID;
		private Boolean weekendFlag;
		private String actualWeight;
		private String actualVolume;
		private String unit1;
		private String unit2;
		private Boolean resourceReqdAm;
		private Boolean resourceReqdPm;		
		private String state;
		private String phone;
		private String destinationPhone;
		private String destinationState;
		private String C;
		private String V;
		private String M;
		private String E;
		private String targetActual;
		private String parkingS;
		private String farInMeters;
		private String origMeters;
		private String parkingD;
		private String dest_farInMeters;
		private String destMeters;
		private String scheduledArrive;
		private String billToName;
		
		public String getScheduledArrive() {
			return scheduledArrive;
		}
		public void setScheduledArrive(String scheduledArrive) {
			this.scheduledArrive = scheduledArrive;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTicket() {
			return ticket;
		}
		public void setTicket(String ticket) {
			this.ticket = ticket;
		}
		public String getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public Date getDate1() {
			return date1;
		}
		public void setDate1(Date date1) {
			this.date1 = date1;
		}
		public Date getDate2() {
			return date2;
		}
		public void setDate2(Date date2) {
			this.date2 = date2;
		}
		public String getService() {
			return service;
		}
		public void setService(String service) {
			this.service = service;
		}
		public String getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(String warehouse) {
			this.warehouse = warehouse;
		}
		public String getEstimatedWeight() {
			return estimatedWeight;
		}
		public void setEstimatedWeight(String estimatedWeight) {
			this.estimatedWeight = estimatedWeight;
		}
		public Boolean getIsCrewUsed() {
			return isCrewUsed;
		}
		public void setIsCrewUsed(Boolean isCrewUsed) {
			this.isCrewUsed = isCrewUsed;
		}
		public void setEstimatedCubicfeet(String estimatedCubicFeet) {
			this.estimatedCubicFeet=estimatedCubicFeet;
			
		}
		public String getEstimatedCubicFeet() {
			return estimatedCubicFeet;
		}
		public void setEstimatedCubicFeet(String estimatedCubicFeet) {
			this.estimatedCubicFeet = estimatedCubicFeet;
		}
		public String getJobType() {
			return jobType;
		}
		public void setJobType(String jobType) {
			this.jobType = jobType;
		}
		public String getCity() {
			return city;
		}
		public void setCity(String city) {
			this.city = city;
		}
		public String getDestinationCity() {
			return destinationCity;
		}
		public void setDestinationCity(String destinationCity) {
			this.destinationCity = destinationCity;
		}
		public String getNotesID() {
			return notesID;
		}
		public void setNotesID(String notesID) {
			this.notesID = notesID;
		}
		public Boolean getWeekendFlag() {
			return weekendFlag;
		}
		public void setWeekendFlag(Boolean weekendFlag) {
			this.weekendFlag = weekendFlag;
		}
		public String getActualWeight() {
			return actualWeight;
		}
		public void setActualWeight(String actualWeight) {
			this.actualWeight = actualWeight;
		}
		public String getActualVolume() {
			return actualVolume;
		}
		public void setActualVolume(String actualVolume) {
			this.actualVolume = actualVolume;
		}
		public String getUnit2() {
			return unit2;
		}
		public void setUnit2(String unit2) {
			this.unit2 = unit2;
		}
		public String getUnit1() {
			return unit1;
		}
		public void setUnit1(String unit1) {
			this.unit1 = unit1;
		}
		public Boolean getResourceReqdAm() {
			return resourceReqdAm;
		}
		public void setResourceReqdAm(Boolean resourceReqdAm) {
			this.resourceReqdAm = resourceReqdAm;
		}
		public Boolean getResourceReqdPm() {
			return resourceReqdPm;
		}
		public void setResourceReqdPm(Boolean resourceReqdPm) {
			this.resourceReqdPm = resourceReqdPm;
		}
		
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getDestinationPhone() {
			return destinationPhone;
		}
		public void setDestinationPhone(String destinationPhone) {
			this.destinationPhone = destinationPhone;
		}
		public String getDestinationState() {
			return destinationState;
		}
		public void setDestinationState(String destinationState) {
			this.destinationState = destinationState;
		}
		private String cQty;
		private String vQty;
		private String mQty;
		private String eQty;
		private String registrationNumber;
		public String getcQty() {
			return cQty;
		}
		public void setcQty(String cQty) {
			this.cQty = cQty;
		}
		public String getvQty() {
			return vQty;
		}
		public void setvQty(String vQty) {
			this.vQty = vQty;
		}
		public String getmQty() {
			return mQty;
		}
		public void setmQty(String mQty) {
			this.mQty = mQty;
		}
		public String geteQty() {
			return eQty;
		}
		public void seteQty(String eQty) {
			this.eQty = eQty;
		}
		public String getTargetActual() {
			return targetActual;
		}
		public void setTargetActual(String targetActual) {
			this.targetActual = targetActual;
		}
		public String getRegistrationNumber() {
			return registrationNumber;
		}
		public void setRegistrationNumber(String registrationNumber) {
			this.registrationNumber = registrationNumber;
		}
		public String getParkingS() {
			return parkingS;
		}
		public void setParkingS(String parkingS) {
			this.parkingS = parkingS;
		}
		public String getFarInMeters() {
			return farInMeters;
		}
		public void setFarInMeters(String farInMeters) {
			this.farInMeters = farInMeters;
		}
		public String getOrigMeters() {
			return origMeters;
		}
		public void setOrigMeters(String origMeters) {
			this.origMeters = origMeters;
		}
		public String getParkingD() {
			return parkingD;
		}
		public void setParkingD(String parkingD) {
			this.parkingD = parkingD;
		}
		public String getDest_farInMeters() {
			return dest_farInMeters;
		}
		public void setDest_farInMeters(String dest_farInMeters) {
			this.dest_farInMeters = dest_farInMeters;
		}
		public String getDestMeters() {
			return destMeters;
		}
		public void setDestMeters(String destMeters) {
			this.destMeters = destMeters;
		}
		public String getBillToName() {
			return billToName;
		}
		public void setBillToName(String billToName) {
			this.billToName = billToName;
		}
	}

	public List findResourceTickets(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus) {
		tktWareHse = tktWareHse.replaceAll("'", "''");
		newWrkDt = newWrkDt.replaceAll("'", "");
		if(workTicketStatus==null){ workTicketStatus = "";}
	 	List workTickets = new ArrayList();
		List workMgt=new ArrayList();
	 	String query=null;
        String columns = "w.id,w.ticket, w.shipNumber,w.lastName,w.firstName,w.date1,w.date2,w.service,w.warehouse,w.estimatedWeight,t.isCrewUsed,w.estimatedCubicFeet, w.jobType, w.city, w.destinationCity, n.id as notesID, w.weekendFlag,w.actualWeight,w.actualVolume,w.unit1,w.unit2, w.resourceReqdAm, w.resourceReqdPm, w.state, w.phone, w.destinationPhone, w.destinationState,w.targetActual,w.parkingS,w.farInMeters,w.origMeters,w.parkingD,w.dest_farInMeters,w.destMeters,w.scheduledArrive";
        if((workticketService!=null ) && (!(workticketService.equalsIgnoreCase("''")))){
        query="select " + columns +"," +
        		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='C') as cQty ,"+
        		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='V') as vQty ,"+
        		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='M') as mQty ,"+
        		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='E') as eQty ,"+
        		"(select s.registrationNumber from serviceorder s where w.serviceOrderId=s.id) as registrationNumber, "+
        		"(select b.billToName from billing b where w.serviceOrderId=b.id) as billToName "+
        		"from workticket w " +        		
        		"left outer join notes n on w.id=n.notesKeyId  and n.notesubtype='Scheduling' " +
        		"left outer join timesheet t on t.ticket = w.ticket  " +
        		"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
        		"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"') and w.service in ("+workticketService+") " +
        		"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
        		"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' group by w.ticket";
        }else{
        query="select " + columns +"," +
    		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='C') as cQty ,"+
    		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='V') as vQty ,"+
    		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='M') as mQty ,"+
    		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='E') as eQty ,"+
    		"(select s.registrationNumber from serviceorder s where w.serviceOrderId=s.id) as registrationNumber, "+
    		"(select b.billToName from billing b where w.serviceOrderId=b.id) as billToName "+
    		"from workticket w " + 
        		"left outer join notes n on w.id=n.notesKeyId  and n.notesubtype='Scheduling' " +
        		"left outer join timesheet t on t.ticket = w.ticket  " +
        		"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
        		"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"')" +
        		"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
        		"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' group by w.ticket";
        }
        List list= this.getSession().createSQLQuery(query).list();	
        //System.out.println("Query Needs to put "+query);
        Iterator it=list.iterator();
        while(it.hasNext()){
        	Object []row= (Object [])it.next();
        	ResourceWorkTickets resourceWorkTickets= new ResourceWorkTickets();
        	resourceWorkTickets.setId(row[0].toString());
        	resourceWorkTickets.setTicket(row[1].toString());
        	resourceWorkTickets.setShipNumber(row[2].toString());
        	resourceWorkTickets.setLastName(row[3].toString());
        	resourceWorkTickets.setFirstName(row[4].toString());
        	Date wrkDtTmp1 = null;
        	Date wrkDtTmp2 = null;
        	try{
	        	   wrkDtTmp1 = new SimpleDateFormat("yyyy-MM-dd").parse(row[5].toString());
	        	   wrkDtTmp2 = new SimpleDateFormat("yyyy-MM-dd").parse(row[6].toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
        	resourceWorkTickets.setDate1(wrkDtTmp1);
        	resourceWorkTickets.setDate2(wrkDtTmp2);
        	resourceWorkTickets.setService(row[7].toString());
        	resourceWorkTickets.setWarehouse(row[8].toString());
        	if(row[9] == null || row[9].equals("")){resourceWorkTickets.setEstimatedWeight("0.00");}else{resourceWorkTickets.setEstimatedWeight(row[9].toString());} 
        	if(row[10] == null || row[10].equals("")){resourceWorkTickets.setIsCrewUsed(false);}else{resourceWorkTickets.setIsCrewUsed(true);}
        	if(row[11] == null || row[11].equals("")){resourceWorkTickets.setEstimatedCubicfeet("0.00");}else{resourceWorkTickets.setEstimatedCubicfeet(row[11].toString());}
        	resourceWorkTickets.setJobType(row[12].toString());
        	resourceWorkTickets.setCity(row[13].toString());
        	resourceWorkTickets.setDestinationCity(row[14].toString());
        	if(row[15]==null){resourceWorkTickets.setNotesID("");}else{resourceWorkTickets.setNotesID(row[15].toString());}
        	if(row[16] == null || row[16].equals("") || row[16].equals(false)){resourceWorkTickets.setWeekendFlag(false);}else{resourceWorkTickets.setWeekendFlag(true);}
        	if(row[17]==null){resourceWorkTickets.setActualWeight("0.0");}else{resourceWorkTickets.setActualWeight(row[17].toString());}
        	if(row[18]==null){resourceWorkTickets.setActualVolume("0.0");}else{resourceWorkTickets.setActualVolume(row[18].toString());}
        	if(row[19]==null){resourceWorkTickets.setUnit1("");}else{resourceWorkTickets.setUnit1(row[19].toString());}
        	if(row[20]==null){resourceWorkTickets.setUnit2("");}else{resourceWorkTickets.setUnit2(row[20].toString());}
        	if(row[21] == null || row[21].equals("") || row[21].equals(false)){resourceWorkTickets.setResourceReqdAm(false);}else{resourceWorkTickets.setResourceReqdAm(true);}
        	if(row[22] == null || row[22].equals("") || row[22].equals(false)){resourceWorkTickets.setResourceReqdPm(false);}else{resourceWorkTickets.setResourceReqdPm(true);}
        	if(row[23]==null){resourceWorkTickets.setState("");}else{resourceWorkTickets.setState(row[23].toString());}
        	if(row[24]==null){resourceWorkTickets.setPhone("");}else{resourceWorkTickets.setPhone(row[24].toString());}
        	if(row[25]==null){resourceWorkTickets.setDestinationPhone("");}else{resourceWorkTickets.setDestinationPhone(row[25].toString());}
        	if(row[26]==null){resourceWorkTickets.setDestinationState("");}else{resourceWorkTickets.setDestinationState(row[26].toString());}
        	String query1="select description from refmaster where code='"+row[27]+"' and parameter='TCKTACTN' and language='en'  and corpID in ('TSFT','"+sessionCorpID+"')";
        	workMgt = this.getSession().createSQLQuery(query1).list();
        	if(workMgt!=null && !workMgt.isEmpty() && workMgt.get(0)!=null){
        		resourceWorkTickets.setTargetActual(workMgt.get(0).toString());
        	}else{
        	if(row[27]==null){resourceWorkTickets.setTargetActual("");}else{resourceWorkTickets.setTargetActual(row[27].toString());}	
        	}
        	if(row[35]==null){resourceWorkTickets.setcQty("");}else{resourceWorkTickets.setcQty(row[35].toString());}
        	if(row[36]==null){resourceWorkTickets.setvQty("");}else{resourceWorkTickets.setvQty(row[36].toString());}
        	if(row[37]==null){resourceWorkTickets.setmQty("");}else{resourceWorkTickets.setmQty(row[37].toString());}
        	if(row[38]==null){resourceWorkTickets.seteQty("");}else{resourceWorkTickets.seteQty(row[38].toString());}        	
        	if(row[39]==null){resourceWorkTickets.setRegistrationNumber("");}else{resourceWorkTickets.setRegistrationNumber(row[39].toString());}
        	if(row[40]==null){resourceWorkTickets.setBillToName("");}else{resourceWorkTickets.setBillToName(row[40].toString());}
        	if(row[28]==null){resourceWorkTickets.setParkingS("");}else{resourceWorkTickets.setParkingS(row[28].toString());}
        	if(row[29]==null){resourceWorkTickets.setFarInMeters("");}else{resourceWorkTickets.setFarInMeters(row[29].toString());}
        	if(row[30]==null){resourceWorkTickets.setOrigMeters("");}else{resourceWorkTickets.setOrigMeters(row[30].toString());}
        	if(row[31]==null){resourceWorkTickets.setParkingD("");}else{resourceWorkTickets.setParkingD(row[31].toString());}
        	if(row[32]==null){resourceWorkTickets.setDest_farInMeters("");}else{resourceWorkTickets.setDest_farInMeters(row[32].toString());}
        	if(row[33]==null){resourceWorkTickets.setDestMeters("");}else{resourceWorkTickets.setDestMeters(row[33].toString());}
        	if(row[34]==null){resourceWorkTickets.setScheduledArrive("");}else{resourceWorkTickets.setScheduledArrive(row[34].toString());}
        	
        	workTickets.add(resourceWorkTickets);
        }
	        
		return workTickets; 
	}

	public List searchAvailableResourceCrews(String warehouse, String date1, String sessionCorpID, String showAbsent,String defaultSortValueFlag) {
		 List list1 = new ArrayList();
		 List listAvailCrew = new ArrayList();
		 String availCrewQuery; 
		 warehouse = warehouse.replaceAll("'", "''");
		 date1 = date1.replaceAll("'", "");
		 if(showAbsent.equalsIgnoreCase("Absent")){   
			 if(date1.equalsIgnoreCase("")){
			 	availCrewQuery = "select t.id, c.firstName, c.lastName , c.warehouse, c.typeofWork, c.unionName,'',  c.overTimeType,'',c.userName,c.integrationTool,c.deviceNumber,c.drivingClass from crew c, timesheet t where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"' and c.username=t.username and t.warehouse = c.warehouse and c.corpID='"+sessionCorpID+"' and (t.isCrewUsed is false or t.isCrewUsed is null) group By c.username, c.id order by "+defaultSortValueFlag+"";
			 	listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
			 }else{
				availCrewQuery = "select t.id, c.firstName, c.lastName , c.warehouse, c.typeofWork, c.unionName,'',  c.overTimeType,'',c.userName,c.integrationTool,c.deviceNumber,c.drivingClass from crew c, timesheet t where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"' and c.username=t.username and t.workDate= :date1 and t.warehouse = c.warehouse and c.corpID='"+sessionCorpID+"' and (t.isCrewUsed is false or t.isCrewUsed is null) group By c.username, c.id order by "+defaultSortValueFlag+"";
				listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).setString("date1", date1).list();
			} 
		 }else{
		     if(date1.equalsIgnoreCase("")){
			 	availCrewQuery = "select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, sum(distinct t.regularHours),t.doneForTheDay, c.userName,c.integrationTool,c.deviceNumber,c.drivingClass from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' AND c.warehouse = '" +warehouse+"' and c.ignoreForTimeSheet is false  group By c.username";
			 	listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
			  }else{
				 availCrewQuery = "(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, c.unionName,t.doneForTheDay, c.overTimeType, t.isCrewUsed,c.userName,c.integrationTool,c.deviceNumber,c.drivingClass  from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"'  and t.workDate= :date1 and c.corpID='"+sessionCorpID+"' and t.isCrewUsed is not null group By c.username, t.workDate) UNION "+
				 "(select c.id , c.firstName, c.lastName , c.warehouse, c.typeofWork, c.unionName,'',  c.overTimeType,'',c.userName,c.integrationTool,c.deviceNumber,c.drivingClass from crew as c where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +warehouse+"'  and c.username not in (select username from timesheet where workDate= :date1 and warehouse = c.warehouse) and c.corpID='"+sessionCorpID+"' group By c.username, c.id) order by "+defaultSortValueFlag+"";
				 // 0 c.id  1 c.firstName 2 c.lastName 3 c.warehouse     4 c.typeofWork
				 //          5 c.unionName              6 t.doneForTheDay     7 c.overTimeType  8 t.isCrewUsed	
				 listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).setString("date1", date1).list();
			 } 
		 }
		 String wrkDt;
		 
		 Iterator it=listAvailCrew.iterator();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           AvailableCrew availableCrew = new AvailableCrew();
	           availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[1] == null){availableCrew.setFirstName("");}else{availableCrew.setFirstName(row[1].toString());}
	           if(row[2] == null){availableCrew.setLastName("");}else{availableCrew.setLastName(row[2].toString());}
	           if(row[3] == null){availableCrew.setWarehouse("");}else{availableCrew.setWarehouse(row[3].toString());}
	           if(row[4] == null){availableCrew.setTypeofWork("");}else{availableCrew.setTypeofWork(row[4].toString());}
	           if(!date1.equalsIgnoreCase("")){
		           if(row[5] == null){availableCrew.setIsUnioun("N");}else{
					if(row[5].toString().contains("S") || row[5].toString().contains("W") || row[5].toString().contains("U"))
					{
						availableCrew.setIsUnioun("U");
					}else{
						availableCrew.setIsUnioun("N");
					}
		           }
	           }
	           if(row[6] == null){availableCrew.setDoneForTheDay("");}else{availableCrew.setDoneForTheDay(row[6].toString());}
	           if(row[8] == null || row[8].equals("")){availableCrew.setIsCrewUsed(false);}else{availableCrew.setIsCrewUsed(true);}
	           if(row[9] == null || row[9].equals("")){availableCrew.setUserName("");}else{availableCrew.setUserName(row[9].toString());}
	           if(row[10] == null || row[10].equals("")){availableCrew.setIntegrationTool("");}else{availableCrew.setIntegrationTool(row[10].toString());}
	           if(row[11] == null || row[11].equals("")){availableCrew.setDeviceNumber("");}else{availableCrew.setDeviceNumber(row[11].toString());}
	           if(row[12] == null || row[12].equals("")){availableCrew.setDrivingClass("");}else{availableCrew.setDrivingClass(row[12].toString());}
	           list1.add(availableCrew);
	}
		 return list1;
	}

	public List getCountFromTimeSheet(String ticket, String date11, String wareHse,	String sessionCorpID) {
		
		String query="select count(*) from scheduleresourceoperation where warehouse='"+wareHse+"' and ticket='"+ticket+"'  and workDate='"+date11+"' and corpID='"+sessionCorpID+"'"  ; 
		return this.getSession().createSQLQuery(query).list();
	}

	public List findSecheDuledResourceOperation(String wareHse,String newWrkDt, String string, String tkt, String string2,String sessionCorpID) {
		return getHibernateTemplate().find("from ScheduleResourceOperation where corpID='"+sessionCorpID+"' and workDate='"+newWrkDt+"'");

	}

	public List getCountForTruckOpsId(String ticket,String date11,String wareHse, String sessionCorpID, Long truckOpsId) {
		
		String query="select count(*) from ScheduleResourceOperation where ticket='"+ticket+"' and truckingOpsId='"+truckOpsId+"'"  ; 
		return getHibernateTemplate().find(query);
	}

	public List getCountForTimeSheetId(String ticket, String date11,String wareHse, String sessionCorpID, Long timeSheetId) {
		
		String query="select count(*) from ScheduleResourceOperation where ticket='"+ticket+"' and timesheetId='"+timeSheetId+"'" ; 
		return getHibernateTemplate().find(query);
	}

	public void updateWorkTicketCrew(Long ticket, String updatedBy,String sessionCorpID) {
		List numList=this.getSession().createSQLQuery("select count(distinct username) from timesheet  where ticket ="+ticket+" and corpID='"+sessionCorpID+"'").list();
		int numberCrew=Integer.parseInt(numList.get(0).toString());
		getHibernateTemplate().bulkUpdate("update WorkTicket set crews="+numberCrew+",ticketAssignedStatus='Assigned' , updatedBy='"+updatedBy+"', updatedOn=now() where ticket="+ticket+"  and corpID='"+sessionCorpID+"'");
	}
	
	public List findFullDestinationCityAdd(String shipNumberForCity,String dcityForCity,String sessionCorpID){
		String query="select destinationCity,destinationZip,destinationState,destinationCountry from workticket where ticket='"+shipNumberForCity+"'and destinationCity='"+dcityForCity+"' and corpId='"+sessionCorpID+"'";
		 List list= this.getSession().createSQLQuery(query).list();
		 Iterator it= list.iterator();
		 List list1=new ArrayList();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           DestinationCityDTO availableCrew = new DestinationCityDTO();
	           //availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[0] == null){availableCrew.setDestinationCity("");}else{availableCrew.setDestinationCity(row[0].toString());}
	           if(row[3] == null){availableCrew.setDestinationCountry("");}else{availableCrew.setDestinationCountry(row[3].toString());}
	           if(row[2] == null){availableCrew.setDestinationState("");}else{availableCrew.setDestinationState(row[2].toString());}
	           if(row[1] == null){availableCrew.setDestinationZip("");}else{availableCrew.setDestinationZip(row[1].toString());}      
	           
	           list1.add(availableCrew);
	}
		 return list1;
		 //return list;
	}
	
	
	public List findFullCityAdd(String shipNumberForCity,String ocityForCity,String sessionCorpID){
		String query="select city,zip,state,originCountry from workticket where ticket='"+shipNumberForCity+"'and city='"+ocityForCity+"' and corpId='"+sessionCorpID+"'";
		 List list= this.getSession().createSQLQuery(query).list();
		 Iterator it= list.iterator();
		 List list1=new ArrayList();
		 while(it.hasNext())
	       {
	           Object []row= (Object [])it.next();
	           OriginCityDTO availableCrew = new OriginCityDTO();
	           //availableCrew.setId(Long.parseLong(row[0].toString()));
	           
	           if(row[0] == null){availableCrew.setOriginCity("");}else{availableCrew.setOriginCity(row[0].toString());}
	           if(row[3] == null){availableCrew.setOriginCountry("");}else{availableCrew.setOriginCountry(row[3].toString());}
	           if(row[2] == null){availableCrew.setOriginState("");}else{availableCrew.setOriginState(row[2].toString());}
	           if(row[1] == null){availableCrew.setOriginZip("");}else{availableCrew.setOriginZip(row[1].toString());}      
	           
	           list1.add(availableCrew);
	}
		 return list1;
		 //return list;
	}

	public Integer selectedWorkTicketSize(String tktWareHse, String newWrkDt,String sessionCorpID, String workticketService, String workTicketStatus) {
		tktWareHse = tktWareHse.replaceAll("'", "''");
		newWrkDt = newWrkDt.replaceAll("'", "");
		if(workTicketStatus==null){ workTicketStatus = "";}
	 	List workTickets = new ArrayList(); 
	 	String query=null;
        String columns = "w.id,w.ticket, w.shipNumber,w.lastName,w.firstName,w.date1,w.date2,w.service,w.warehouse,w.estimatedWeight,t.isCrewUsed,w.estimatedCubicFeet";
        if((workticketService!=null ) && (!(workticketService.equalsIgnoreCase("''")))){
        query="select " + columns +" from workticket w " +
        		"left outer join timesheet t on t.ticket = w.ticket  " +
        		"left outer join itemsjbkequip it on it.ticket= w.ticket and it.corpid=w.corpid "+
        		"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
        		"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"') and w.service in ("+workticketService+") " +
        		"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
        		"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' and t.isCrewUsed is true group by w.ticket";
        }else{
        	 query="select " + columns +" from workticket w " +
        		"left outer join timesheet t on t.ticket = w.ticket  " +
        		"left outer join itemsjbkequip it on it.ticket= w.ticket and it.corpid=w.corpid "+
        		"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
        		"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"')" +
        		"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
        		"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' and t.isCrewUsed is true group by w.ticket";	
        }
        List list= this.getSession().createSQLQuery(query).list();	
        Integer size=list.size();
        //System.out.println(size+"////////////selectedWorkTicketSize");
		return size;
	}

	public Integer selectedAvailableCrewSize(String wareHse, String newWrkDt,String sessionCorpID) {
		wareHse = wareHse.replaceAll("'", "''");
		newWrkDt = newWrkDt.replaceAll("'", "");
		 String availCrewQuery;
		 List listAvailCrew;
		 Integer size;
		 availCrewQuery = "select * from crew as c LEFT OUTER JOIN timesheet as t on c.username = t.username where c.active='true' and c.ignoreForTimeSheet is false AND c.warehouse = '" +wareHse+"'  and t.workDate= '"+newWrkDt+"' and c.corpID='"+sessionCorpID+"' and t.isCrewUsed is true group by c.username ";
		 listAvailCrew= this.getSession().createSQLQuery(availCrewQuery).list();
			size=listAvailCrew.size();
			//System.out.println(size+"////////////selectedAvailableCrewSize");

		return size;
	}

	public Integer selectedAvailableTrucksSize(String truckWareHse,	String newWrkDt, String sessionCorpID) {
		truckWareHse = truckWareHse.replaceAll("'", "''");
		 newWrkDt = newWrkDt.replaceAll("'", "");
		 	List availableTrucks = new ArrayList();
		 	String query="select distinct t.localnumber,t.description, tr.isTruckUsed, t.id as truckId " +
		 			"from truck t left outer join truckingoperations tr on  tr.localtrucknumber=t.localnumber " +
		 			"and tr.corpid='"+sessionCorpID+"' and workDate='"+newWrkDt+"' where t.corpid='"+sessionCorpID+"' " +
		 			"and t.warehouse='"+truckWareHse+"' and truckstatus = 'A' and tr.isTruckUsed is true";
		 	List list= this.getSession().createSQLQuery(query).list();
		 	Integer size=list.size();
		 	//System.out.println(size+"////////////selectedAvailableTrucksSize");
		return size;
	}

	public List findNonSelectedResourceTickets(String tktWareHse,String newWrkDt, String sessionCorpID, String workticketService, String workTicketStatus) {
		tktWareHse = tktWareHse.replaceAll("'", "''");
		newWrkDt = newWrkDt.replaceAll("'", "");
		if(workTicketStatus==null){ workTicketStatus = "";}
	 	List workTickets = new ArrayList();
	 	List workMgt=new ArrayList();
	 	String query= null;
        String columns = "w.id,w.ticket, w.shipNumber,w.lastName,w.firstName,w.date1,w.date2,w.service,w.warehouse,w.estimatedWeight,t.isCrewUsed,w.estimatedCubicFeet, w.jobType, w.city, w.destinationCity, n.id as notesID, w.weekendFlag,w.actualWeight,w.actualVolume,w.unit1,w.unit2, w.resourceReqdAm, w.resourceReqdPm, w.state, w.phone, w.destinationPhone, w.destinationState,w.targetActual,w.parkingS,w.farInMeters,w.origMeters,w.parkingD,w.dest_farInMeters,w.destMeters,w.scheduledArrive";
        if((workticketService!=null ) && (!(workticketService.equalsIgnoreCase("''")))){
        query="select " + columns +"," +
        "(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='C') as cQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='V') as vQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='M') as mQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='E') as eQty ,"+
		"(select s.registrationNumber from serviceorder s where w.serviceOrderId=s.id) as registrationNumber, "+
		"(select b.billToName from billing b where w.serviceOrderId=b.id) as billToName "+
		"from workticket w " + 
        			"left outer join notes n on w.id=n.notesKeyId  and n.notesubtype='Scheduling'" +		
        			"left outer join timesheet t on t.ticket = w.ticket  " +
        			"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
        			"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"') and w.service in ("+workticketService+")" +
        			"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
        			"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' and (t.isCrewUsed is null) group by w.ticket";
        }else{
        query="select " + columns +"," +
        "(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='C') as cQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='V') as vQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='M') as mQty ,"+
		"(select sum(it.qty) from itemsjbkequip it where it.workTicketID= w.id and type='E') as eQty , "+
		"(select s.registrationNumber from serviceorder s where and w.serviceOrderId=s.id) as registrationNumber, "+
		"(select b.billToName from billing b where w.serviceOrderId=b.id) as billToName "+
		"from workticket w " + 
	        		"left outer join notes n on w.id=n.notesKeyId  and n.notesubtype='Scheduling'" +		
	        		"left outer join timesheet t on t.ticket = w.ticket  " +
	        		"and t.warehouse='"+tktWareHse+"' and t.corpID='"+sessionCorpID+"' and t.workDate='"+newWrkDt+"'  " +
	        		"where (w.date1<='"+newWrkDt+"' AND w.date2>='"+newWrkDt+"')" +
	        		"and w.targetActual not in ('C','R') and  w.targetActual like '%"+workTicketStatus+"%' " +
	        		"and w.warehouse='"+tktWareHse+"' and w.corpID='"+sessionCorpID+"' and (t.isCrewUsed is null) group by w.ticket";	
        }
        List list= this.getSession().createSQLQuery(query).list();	
        
        Iterator it=list.iterator();
        while(it.hasNext()){
        	Object []row= (Object [])it.next();
        	ResourceWorkTickets resourceWorkTickets= new ResourceWorkTickets();
        	resourceWorkTickets.setId(row[0].toString());
        	resourceWorkTickets.setTicket(row[1].toString());
        	resourceWorkTickets.setShipNumber(row[2].toString());
        	resourceWorkTickets.setLastName(row[3].toString());
        	resourceWorkTickets.setFirstName(row[4].toString());
        	Date wrkDtTmp1 = null;
        	Date wrkDtTmp2 = null;
        	try{
	        	   wrkDtTmp1 = new SimpleDateFormat("yyyy-MM-dd").parse(row[5].toString());
	        	   wrkDtTmp2 = new SimpleDateFormat("yyyy-MM-dd").parse(row[6].toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
        	resourceWorkTickets.setDate1(wrkDtTmp1);
        	resourceWorkTickets.setDate2(wrkDtTmp2);
        	resourceWorkTickets.setService(row[7].toString());
        	resourceWorkTickets.setWarehouse(row[8].toString());
        	if(row[9] == null){resourceWorkTickets.setEstimatedWeight("0.00");}else{resourceWorkTickets.setEstimatedWeight(row[9].toString());}
        	if(row[10] == null || row[10].equals("")){resourceWorkTickets.setIsCrewUsed(false);}else{resourceWorkTickets.setIsCrewUsed(true);}
        	if(row[11] == null){resourceWorkTickets.setEstimatedCubicfeet("0.00");}else{resourceWorkTickets.setEstimatedCubicfeet(row[11].toString());}
         	resourceWorkTickets.setJobType(row[12].toString());
         	resourceWorkTickets.setCity(row[13].toString());
        	resourceWorkTickets.setDestinationCity(row[14].toString());
         	if(row[15]==null){resourceWorkTickets.setNotesID("");}else{resourceWorkTickets.setNotesID(row[15].toString());}
         	if(row[16] == null || row[16].equals("") || row[16].equals(false)){resourceWorkTickets.setWeekendFlag(false);}else{resourceWorkTickets.setWeekendFlag(true);}
         	if(row[17]==null){resourceWorkTickets.setActualWeight("0.00");}else{resourceWorkTickets.setActualWeight(row[17].toString());}
        	if(row[18]==null){resourceWorkTickets.setActualVolume("0.0");}else{resourceWorkTickets.setActualVolume(row[18].toString());}
        	if(row[19]==null){resourceWorkTickets.setUnit1("");}else{resourceWorkTickets.setUnit1(row[19].toString());}
        	if(row[20]==null){resourceWorkTickets.setUnit2("");}else{resourceWorkTickets.setUnit2(row[20].toString());}
        	if(row[21] == null || row[21].equals("") || row[21].equals(false)){resourceWorkTickets.setResourceReqdAm(false);}else{resourceWorkTickets.setResourceReqdAm(true);}
        	if(row[22] == null || row[22].equals("") || row[22].equals(false)){resourceWorkTickets.setResourceReqdPm(false);}else{resourceWorkTickets.setResourceReqdPm(true);}
        	if(row[23]==null){resourceWorkTickets.setState("");}else{resourceWorkTickets.setState(row[23].toString());}
        	if(row[24]==null){resourceWorkTickets.setPhone("");}else{resourceWorkTickets.setPhone(row[24].toString());}
        	if(row[25]==null){resourceWorkTickets.setDestinationPhone("");}else{resourceWorkTickets.setDestinationPhone(row[25].toString());}
        	if(row[26]==null){resourceWorkTickets.setDestinationState("");}else{resourceWorkTickets.setDestinationState(row[26].toString());}
        	String query1="select description from refmaster where code='"+row[27]+"' and parameter='TCKTACTN' and language='en'  and corpID in ('TSFT','"+sessionCorpID+"')";
        	workMgt = this.getSession().createSQLQuery(query1).list();
        	if(workMgt!=null && !workMgt.isEmpty() && workMgt.get(0)!=null){
        		resourceWorkTickets.setTargetActual(workMgt.get(0).toString());
        	}else{
        	if(row[27]==null){resourceWorkTickets.setTargetActual("");}else{resourceWorkTickets.setTargetActual(row[27].toString());}	
        	}
        	if(row[35]==null){resourceWorkTickets.setcQty("");}else{resourceWorkTickets.setcQty(row[35].toString());}
        	if(row[36]==null){resourceWorkTickets.setvQty("");}else{resourceWorkTickets.setvQty(row[36].toString());}
        	if(row[37]==null){resourceWorkTickets.setmQty("");}else{resourceWorkTickets.setmQty(row[37].toString());}
        	if(row[38]==null){resourceWorkTickets.seteQty("");}else{resourceWorkTickets.seteQty(row[38].toString());}        	
        	if(row[39]==null){resourceWorkTickets.setRegistrationNumber("");}else{resourceWorkTickets.setRegistrationNumber(row[39].toString());}
        	if(row[40]==null){resourceWorkTickets.setBillToName("");}else{resourceWorkTickets.setBillToName(row[40].toString());}
        	if(row[28]==null){resourceWorkTickets.setParkingS("");}else{resourceWorkTickets.setParkingS(row[28].toString());}
        	if(row[29]==null){resourceWorkTickets.setFarInMeters("");}else{resourceWorkTickets.setFarInMeters(row[29].toString());}
        	if(row[30]==null){resourceWorkTickets.setOrigMeters("");}else{resourceWorkTickets.setOrigMeters(row[30].toString());}
        	if(row[31]==null){resourceWorkTickets.setParkingD("");}else{resourceWorkTickets.setParkingD(row[31].toString());}
        	if(row[32]==null){resourceWorkTickets.setDest_farInMeters("");}else{resourceWorkTickets.setDest_farInMeters(row[32].toString());}
        	if(row[33]==null){resourceWorkTickets.setDestMeters("");}else{resourceWorkTickets.setDestMeters(row[33].toString());}
        	if(row[34]==null){resourceWorkTickets.setScheduledArrive("");}else{resourceWorkTickets.setScheduledArrive(row[34].toString());}
        	
        	workTickets.add(resourceWorkTickets);
        }			        
	  return workTickets; 
	}
	
	public void updateDriver(String driverId, Boolean driverChecked) {
		if(driverChecked.booleanValue()==true){
			getHibernateTemplate().bulkUpdate("update ScheduleResourceOperation set isDriverUsed=true where id='"+driverId+"'");
		}else{
			getHibernateTemplate().bulkUpdate("update ScheduleResourceOperation set isDriverUsed=false where id='"+driverId+"'");
		}
	}
	
	public List findResourceItemsList(String tktWareHse, String newWrkDt, String sessionCorpID){
		List resourceList = new ArrayList();
		Boolean warehouse = getCheckedWareHouse(tktWareHse, sessionCorpID);
		if(warehouse==true){
			OperationsResourceLimits operationsResourceLimits = new OperationsResourceLimits();
			List hubResourcelist =  this.getSession().createSQLQuery("select flex1 from refmaster where corpid in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'HOUSE' and code = '"+tktWareHse+"'").list();
			if(hubResourcelist!=null && (!(hubResourcelist.isEmpty()))){
				String hub = hubResourcelist.get(0).toString();
				List opsResourceList = getHibernateTemplate().find("from OperationsResourceLimits where hubID = '"+hub+"'");
				if(opsResourceList!=null && (!(opsResourceList.isEmpty()))){
					operationsResourceLimits = (OperationsResourceLimits)opsResourceList.get(0);
				}				
				Double totalUsedResourceLimit = 0.00;
				Double totalRequiredResourceLimit = 0.00;
				ResourceDTO resourceDTO = null;
				String query = " select distinct a.resource, a.category ,if(b.resourceLimit>0,b.resourceLimit,a.resourceLimit) from operationsresourcelimits a " +
						"left outer join resourcegrid b on a.hubid=b.hubid and b.corpID = '"+sessionCorpID+"' and b.workDate='"+newWrkDt+"' and a.category=b.category and a.resource=b.resource where a.corpID = '"+sessionCorpID+"' and  a.hubID = '"+operationsResourceLimits.getHubId()+"' " +
						"union select distinct a.resource, a.category ,if(a.resourceLimit>0,a.resourceLimit,b.resourceLimit) " +
						"from resourcegrid a left outer join operationsresourcelimits b on a.hubid=b.hubid " +
						"and b.corpID = '"+sessionCorpID+"' and a.resource=b.resource and a.category=b.category where a.corpID = '"+sessionCorpID+"' " +
						"and  a.hubID = '"+operationsResourceLimits.getHubId()+"' and a.workDate='"+newWrkDt+"' ";
				List totalResourceLimitList = this.getSession().createSQLQuery(query).list();
				/*if(totalResourceLimitList==null || totalResourceLimitList.isEmpty()){
					String queryString = "select resource, category, resourceLimit from  operationsresourcelimits where corpID = '"+sessionCorpID+"' and hubID = '"+operationsResourceLimits.getHubId()+"' ";
					totalResourceLimitList = this.getSession().createSQLQuery(queryString).list();
				}*/
				if(totalResourceLimitList !=null && (!(totalResourceLimitList.isEmpty())) && totalResourceLimitList.get(0)!=null){
                	 Iterator itr=totalResourceLimitList.iterator();
                     while(itr.hasNext()){
                    	Object [] row=(Object[])itr.next();
                    	String resource = row[0].toString();
                    	String category = row[1].toString();
                    	Double dailyResourceLimit = Double.parseDouble(row[2].toString());
                    	String availString = "select if(sum(qty) is null,'0',sum(qty)) from itemsjbkequip i where i.ticket in ( Select t.ticket from workticket t, refmaster r where t.targetActual <> 'C' and t.targetActual = 'T' " +
                 			 "and t.service in ('PK', 'LD', 'DU', 'DL','UP','PL') and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code " +
                 			 "and r.bucket = 'Y' and r.flex1 = '"+operationsResourceLimits.getHubId()+"' and ((t.date1  between '"+newWrkDt+"' and  '"+newWrkDt+"') or (t.date2 between '"+newWrkDt+"' and  '"+newWrkDt+"') " +
                 			 "or  (t.date1 <  '"+newWrkDt+"' and t.date2 > '"+newWrkDt+"')) and r.parameter = 'HOUSE') and corpid='"+sessionCorpID+"' and type like '"+category+"' and descript like '%"+resource+"%'";
                    	List availList = this.getSession().createSQLQuery(availString).list();
                    	if(availList !=null && (!(availList.isEmpty()))){
                    		totalUsedResourceLimit = Double.parseDouble(availList.get(0).toString());
                    	}
                    	String requiredString = "select if(sum(qty) is null,'0',sum(qty)) from itemsjbkequip i where i.ticket in ( Select t.ticket from workticket t, refmaster r where t.targetActual <> 'C' and t.targetActual in ('T','P') " +
                 			 "and t.service in ('PK', 'LD', 'DU', 'DL','UP','PL') and t.corpID = '"+sessionCorpID+"' and  r.corpID = t.corpID and t.warehouse = r.code " +
                 			 "and r.bucket = 'Y' and r.flex1 = '"+operationsResourceLimits.getHubId()+"' and ((t.date1  between '"+newWrkDt+"' and  '"+newWrkDt+"') or (t.date2 between '"+newWrkDt+"' and  '"+newWrkDt+"') " +
                 			 "or  (t.date1 <  '"+newWrkDt+"' and t.date2 > '"+newWrkDt+"')) and r.parameter = 'HOUSE') and corpid='"+sessionCorpID+"' and type like '"+category+"' and descript like '%"+resource+"%'";
                    	List requiredList = this.getSession().createSQLQuery(requiredString).list();
                    	if(requiredList !=null && (!(requiredList.isEmpty()))){
                    		totalRequiredResourceLimit = Double.parseDouble(requiredList.get(0).toString());
                    	}
         				 
                    	Double availLimit =0.00;
                        availLimit = dailyResourceLimit.doubleValue() - totalUsedResourceLimit.doubleValue();
                        resourceDTO= new ResourceDTO();
                        resourceDTO.setCategory(category);
                        resourceDTO.setResource(resource);
                        resourceDTO.setAvailLimit(availLimit);
                        resourceDTO.setRequiredLimit(totalRequiredResourceLimit);
                        resourceList.add(resourceDTO);		        
                     }
                }				       
			}			
		}			
		return resourceList;
	}
	public Boolean getCheckedWareHouse(String wareHouse, String corpId){
		Boolean check = false;		
		List bucketCheck = getHibernateTemplate().find("select bucket from RefMaster where code='"+wareHouse+"' and parameter = 'HOUSE' and (flex1 is not null and length(trim(flex1))>0) ");		
		if(!bucketCheck.isEmpty() && bucketCheck.get(0).toString().equalsIgnoreCase("Y")){
			check = true;
		}else{
			check = false;
		}
		
		return check;
	}
	
	public class ResourceDTO {
		
		private Object category;
		private Object resource;
		private Object availLimit;
		private Object requiredLimit;
		
		public Object getCategory() {
			return category;
		}
		public void setCategory(Object category) {
			this.category = category;
		}
		public Object getResource() {
			return resource;
		}
		public void setResource(Object resource) {
			this.resource = resource;
		}
		public Object getAvailLimit() {
			return availLimit;
		}
		public void setAvailLimit(Object availLimit) {
			this.availLimit = availLimit;
		}
		public Object getRequiredLimit() {
			return requiredLimit;
		}
		public void setRequiredLimit(Object requiredLimit) {
			this.requiredLimit = requiredLimit;
		}
	}
	
	//mydtoDestination
public class DestinationCityDTO {
		
		private Object destinationCity;
		private Object destinationZip;
		private Object destinationState;
		private Object destinationCountry;
		public Object getDestinationCity() {
			return destinationCity;
		}
		public Object getDestinationZip() {
			return destinationZip;
		}
		public Object getDestinationState() {
			return destinationState;
		}
		public Object getDestinationCountry() {
			return destinationCountry;
		}
		public void setDestinationCity(Object destinationCity) {
			this.destinationCity = destinationCity;
		}
		public void setDestinationZip(Object destinationZip) {
			this.destinationZip = destinationZip;
		}
		public void setDestinationState(Object destinationState) {
			this.destinationState = destinationState;
		}
		public void setDestinationCountry(Object destinationCountry) {
			this.destinationCountry = destinationCountry;
		}
		
		
	}
	
	
public class OriginCityDTO {
		
		private Object originCity;
		private Object originZip;
		private Object originState;
		private Object originCountry;
		public Object getOriginCity() {
			return originCity;
		}
		public Object getOriginZip() {
			return originZip;
		}
		public Object getOriginState() {
			return originState;
		}
		public Object getOriginCountry() {
			return originCountry;
		}
		public void setOriginCity(Object originCity) {
			this.originCity = originCity;
		}
		public void setOriginZip(Object originZip) {
			this.originZip = originZip;
		}
		public void setOriginState(Object originState) {
			this.originState = originState;
		}
		public void setOriginCountry(Object originCountry) {
			this.originCountry = originCountry;
		}
		
		
	}
	//mydto
	
	public class ScheduleResource{
		private String id;
		private String ticket;
		private String shipper;
		private String estimatedCubicFeet;
		private String service;
		private String warehouse;
		private String crewName;
		private String localtruckNumber;
		private String description;
		private String notesId;
		private String timesheetId;
		private String truckingOpsId;
		private String workTicketId;
		private String shipNumber;
		private String originCity;
		private String destinationCity;
		private String actualWeight;
		private String actualVolume;
		private String isDriverUsed;
		private String unit1;
		private String unit2;
		private Boolean resourceReqdAm;
		private Boolean resourceReqdPm;
		private String state;
		private String phone;
		private String destinationPhone;
		private String destinationState;		
		private String estimatedWeight;
		private String aTime;
		private String dTime;
		private String integrationTool;
		private Object crewNameList;
		private String assignedCrewNameForTruck;
		private String quantitySum;
		private Long serviceOrderId;
		private String projectLeadName;
		private String projectLeadNumber;
		private String acceptByDriver;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTicket() {
			return ticket;
		}
		public void setTicket(String ticket) {
			this.ticket = ticket;
		}
		public String getShipper() {
			return shipper;
		}
		public void setShipper(String shipper) {
			this.shipper = shipper;
		}
		public String getEstimatedCubicFeet() {
			return estimatedCubicFeet;
		}
		public void setEstimatedCubicFeet(String estimatedCubicFeet) {
			this.estimatedCubicFeet = estimatedCubicFeet;
		}
		public String getService() {
			return service;
		}
		public void setService(String service) {
			this.service = service;
		}
		public String getWarehouse() {
			return warehouse;
		}
		public void setWarehouse(String warehouse) {
			this.warehouse = warehouse;
		}
		public String getCrewName() {
			return crewName;
		}
		public void setCrewName(String crewName) {
			this.crewName = crewName;
		}
		public String getLocaltruckNumber() {
			return localtruckNumber;
		}
		public void setLocaltruckNumber(String localtruckNumber) {
			this.localtruckNumber = localtruckNumber;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		
		public String getTimesheetId() {
			return timesheetId;
		}
		public void setTimesheetId(String timesheetId) {
			this.timesheetId = timesheetId;
		}
		public String getTruckingOpsId() {
			return truckingOpsId;
		}
		public void setTruckingOpsId(String truckingOpsId) {
			this.truckingOpsId = truckingOpsId;
		}
		public String getWorkTicketId() {
			return workTicketId;
		}
		public void setWorkTicketId(String workTicketId) {
			this.workTicketId = workTicketId;
		}
		public String getShipNumber() {
			return shipNumber;
		}
		public void setShipNumber(String shipNumber) {
			this.shipNumber = shipNumber;
		}
		public String getNotesId() {
			return notesId;
		}
		public void setNotesId(String notesId) {
			this.notesId = notesId;
		}
		
		public String getOriginCity() {
			return originCity;
		}
		public void setOriginCity(String originCity) {
			this.originCity = originCity;
		}
		public String getDestinationCity() {
			return destinationCity;
		}
		public void setDestinationCity(String destinationCity) {
			this.destinationCity = destinationCity;
		}
		public String getActualWeight() {
			return actualWeight;
		}
		public void setActualWeight(String actualWeight) {
			this.actualWeight = actualWeight;
		}
		public String getActualVolume() {
			return actualVolume;
		}
		public void setActualVolume(String actualVolume) {
			this.actualVolume = actualVolume;
		}
		public String getIsDriverUsed() {
			return isDriverUsed;
		}
		public void setIsDriverUsed(String isDriverUsed) {
			this.isDriverUsed = isDriverUsed;
		}
		public String getUnit1() {
			return unit1;
		}
		public void setUnit1(String unit1) {
			this.unit1 = unit1;
		}
		public String getUnit2() {
			return unit2;
		}
		public void setUnit2(String unit2) {
			this.unit2 = unit2;
		}
		public Boolean getResourceReqdAm() {
			return resourceReqdAm;
		}
		public void setResourceReqdAm(Boolean resourceReqdAm) {
			this.resourceReqdAm = resourceReqdAm;
		}
		public Boolean getResourceReqdPm() {
			return resourceReqdPm;
		}
		public void setResourceReqdPm(Boolean resourceReqdPm) {
			this.resourceReqdPm = resourceReqdPm;
		}
		
		public String getState() {
			return state;
		}
		public void setState(String state) {
			this.state = state;
		}
		public String getPhone() {
			return phone;
		}
		public void setPhone(String phone) {
			this.phone = phone;
		}
		public String getDestinationPhone() {
			return destinationPhone;
		}
		public void setDestinationPhone(String destinationPhone) {
			this.destinationPhone = destinationPhone;
		}
		public String getDestinationState() {
			return destinationState;
		}
		public void setDestinationState(String destinationState) {
			this.destinationState = destinationState;
		}
		public String getEstimatedWeight() {
			return estimatedWeight;
		}
		public void setEstimatedWeight(String estimatedWeight) {
			this.estimatedWeight = estimatedWeight;
		}
		public String getaTime() {
			return aTime;
		}
		public void setaTime(String aTime) {
			this.aTime = aTime;
		}
		public String getdTime() {
			return dTime;
		}
		public void setdTime(String dTime) {
			this.dTime = dTime;
		}
		public String getIntegrationTool() {
			return integrationTool;
		}
		public void setIntegrationTool(String integrationTool) {
			this.integrationTool = integrationTool;
		}
		public Object getCrewNameList() {
			return crewNameList;
		}
		public void setCrewNameList(Object crewNameList) {
			this.crewNameList = crewNameList;
		}
		public String getAssignedCrewNameForTruck() {
			return assignedCrewNameForTruck;
		}
		public void setAssignedCrewNameForTruck(String assignedCrewNameForTruck) {
			this.assignedCrewNameForTruck = assignedCrewNameForTruck;
		}
		public String getQuantitySum() {
			return quantitySum;
		}
		public void setQuantitySum(String quantitySum) {
			this.quantitySum = quantitySum;
		}
		public Long getServiceOrderId() {
			return serviceOrderId;
		}
		public void setServiceOrderId(Long serviceOrderId) {
			this.serviceOrderId = serviceOrderId;
		}
		/**
		 * @return the projectLeadName
		 */
		public String getProjectLeadName() {
			return projectLeadName;
		}
		/**
		 * @param projectLeadName the projectLeadName to set
		 */
		public void setProjectLeadName(String projectLeadName) {
			this.projectLeadName = projectLeadName;
		}
		/**
		 * @return the projectLeadNumber
		 */
		public String getProjectLeadNumber() {
			return projectLeadNumber;
		}
		/**
		 * @param projectLeadNumber the projectLeadNumber to set
		 */
		public void setProjectLeadNumber(String projectLeadNumber) {
			this.projectLeadNumber = projectLeadNumber;
		}
		/**
		 * @return the acceptByDriver
		 */
		public String getAcceptByDriver() {
			return acceptByDriver;
		}
		/**
		 * @param acceptByDriver the acceptByDriver to set
		 */
		public void setAcceptByDriver(String acceptByDriver) {
			this.acceptByDriver = acceptByDriver;
		}
	}

	public ItemsJbkEquip getItemsJbkEquip() {
		return itemsJbkEquip;
	}

	public void setItemsJbkEquip(ItemsJbkEquip itemsJbkEquip) {
		this.itemsJbkEquip = itemsJbkEquip;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}
	
	public List findToolTipResourceDetail(String sessionCorpID,String ticket, String type){
		List temp=new ArrayList();
		List resourceDetailList = new ArrayList();
		String query =("Select descript,qty,comments from itemsjbkequip where corpID='"+sessionCorpID+"' and ticket='"+ticket+"' and type like '"+type+"%' group by descript");		
		temp = this.getSession().createSQLQuery(query)
			.addScalar("descript",Hibernate.STRING)
			.addScalar("qty", Hibernate.STRING)
		.addScalar("comments", Hibernate.STRING).list();
		Iterator it=temp.iterator();
		DTOForRDeatil dto=null;
		while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  dto=new DTOForRDeatil(); 
		  if( (	row[1]!=null ) && ( !row[1].equals("") ) &&( ( Integer.parseInt(row[1].toString())>0)||( Integer.parseInt(row[1].toString())<0 ))){		  
			  dto.setDescript(row[0]);
			  dto.setQty(row[1]);		  
			  dto.setComment(row[2]);
			  resourceDetailList.add(dto);
		  }
			  
	  } 
		return resourceDetailList;
	}
	
	public List findCrewResourceDetail(String sessionCorpID,String ticket){
		List temp=new ArrayList();
		List crewDetailList = new ArrayList();
		String query ="";
		query ="Select descript,qty from itemsjbkequip where corpID='"+sessionCorpID+"' and ticket='"+ticket+"' and type='C' order by type";
		temp = this.getSession().createSQLQuery(query).list();
		Iterator it=temp.iterator();
		DTOForRDeatil dto=null;
		while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  	dto=new DTOForRDeatil(); 
			dto.setDescript(row[0]);
			dto.setQty(row[1]);
			crewDetailList.add(dto);
		} 
		return crewDetailList;
	}
	public List findVehicleResourceDetail(String sessionCorpID,String ticket){
		List temp=new ArrayList();
		List vehicleDetailList = new ArrayList();
		String query ="";
		query ="Select descript,qty from itemsjbkequip where corpID='"+sessionCorpID+"' and ticket='"+ticket+"' and type='V' order by type";
		temp = this.getSession().createSQLQuery(query).list();
		Iterator it=temp.iterator();
		DTOForRDeatil dto=null;
		while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  	dto=new DTOForRDeatil(); 
			dto.setDescript(row[0]);
			dto.setQty(row[1]);
			vehicleDetailList.add(dto);
		} 
		return vehicleDetailList;
	}
	public List findEquipmentResourceDetail(String sessionCorpID,String ticket){
		List temp=new ArrayList();
		List equipmentDetailList = new ArrayList();
		String query ="";
		query ="Select descript,qty from itemsjbkequip where corpID='"+sessionCorpID+"' and ticket='"+ticket+"' and type='E' order by type";
		temp = this.getSession().createSQLQuery(query).list();
		Iterator it=temp.iterator();
		DTOForRDeatil dto=null;
		while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  	dto=new DTOForRDeatil(); 
			dto.setDescript(row[0]);
			dto.setQty(row[1]);
			equipmentDetailList.add(dto);
		} 
		return equipmentDetailList;
	}
	public List findMaterialResourceDetail(String sessionCorpID,String ticket){
		List temp=new ArrayList();
		List materialDetailList = new ArrayList();
		String query ="";
		query ="Select descript,qty from itemsjbkequip where corpID='"+sessionCorpID+"' and ticket='"+ticket+"' and type='M' order by type";
		temp = this.getSession().createSQLQuery(query).list();
		Iterator it=temp.iterator();
		DTOForRDeatil dto=null;
		while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  	dto=new DTOForRDeatil(); 
			dto.setDescript(row[0]);
			dto.setQty(row[1]);
			materialDetailList.add(dto);
		} 
		return materialDetailList;
	}
	
		public  class DTOForRDeatil{
			private Object descript;
			private Object qty;
			private Object comment;
			private Object type;
			public Object getDescript() {
				return descript;
			}
			public void setDescript(Object descript) {
				this.descript = descript;
			}
			public Object getQty() {
				return qty;
			}
			public void setQty(Object qty) {
				this.qty = qty;
			}
			public Object getComment() {
				return comment;
			}
			public void setComment(Object comment) {
				this.comment = comment;
			}
			public Object getType() {
				return type;
			}
			public void setType(Object type) {
				this.type = type;
			}
			
		}
		
		public List findScheduleResourceSeparateCrewList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber){
			List scheduleResourceCrewList= new ArrayList();
			String query="";
			query="select crewName,ticket,group_concat(timesheetId),group_concat(truckingOpsId),group_concat(id),'"+projectName+"','"+projectNumber+"'  " +
			        " from scheduleresourceoperation "+
			       	"where corpID='"+sessionCorpID+"' and workDate='"+newWrkDt+"' and warehouse='"+wareHse+"' and ticket="+tkt+" group by crewName order by crewName";
				List list= this.getSession().createSQLQuery(query).list();
				Iterator it=list.iterator();
			    while(it.hasNext()){
			    Object []row= (Object [])it.next();
				ScheduleResource scheduleResource= new ScheduleResource();
				if(row[0]!=null && !row[0].equals("")){
		        	scheduleResource.setCrewName(row[0].toString());
		        	scheduleResource.setTicket(row[1].toString());
		        	scheduleResource.setTimesheetId(row[2].toString());
		        	scheduleResource.setTruckingOpsId(row[3].toString());
		        	scheduleResource.setId(row[4].toString());
		        	scheduleResource.setProjectLeadName(row[5].toString());
		        	scheduleResource.setProjectLeadNumber(row[6].toString());
		        	scheduleResourceCrewList.add(scheduleResource);
				}
			}
			return scheduleResourceCrewList;
		}
		
		public List findScheduleResourceSeparateTruckList(String wareHse, String newWrkDt,String tkt, String sessionCorpID,String projectName,String projectNumber){
			List scheduleResourceTruckList= new ArrayList();
			String query1="";
			query1="select localtruckNumber,ticket,group_concat(timesheetId),group_concat(truckingOpsId),group_concat(id),assignedCrewForTruck,'"+projectName+"','"+projectNumber+"'  " +
			        " from scheduleresourceoperation "+
			       	" where corpID='"+sessionCorpID+"' and workDate='"+newWrkDt+"' and warehouse='"+wareHse+"' and ticket="+tkt+" group by localtruckNumber order by localtruckNumber";
				List list1= this.getSession().createSQLQuery(query1).list();
				Iterator itr=list1.iterator();
			    while(itr.hasNext()){
			    Object []row= (Object [])itr.next();
				ScheduleResource scheduleResource= new ScheduleResource();
				if(row[0]!=null && !row[0].equals("")){
		        	scheduleResource.setLocaltruckNumber(row[0].toString());
		        	scheduleResource.setTicket(row[1].toString());
		        	scheduleResource.setTimesheetId(row[2].toString());
		        	scheduleResource.setTruckingOpsId(row[3].toString());
		        	scheduleResource.setId(row[4].toString());
		        	if(row[5]!=null && !row[5].equals("")){
		        		scheduleResource.setAssignedCrewNameForTruck(row[5].toString());
		        	}else{
		        		scheduleResource.setAssignedCrewNameForTruck("");
		        	}
		        	String query = "select distinct crewName from scheduleresourceoperation  where ticket ='"+tkt+"' and warehouse='"+wareHse+"' and corpID='"+sessionCorpID+"' order by crewName ";
		        	List list = this.getSession().createSQLQuery(query).list();
		        	Iterator iter1=list.iterator();
		        	String tempCrewName="";
		        	while(iter1.hasNext()){
		        		if(tempCrewName.equals("")){
		        			tempCrewName=iter1.next().toString().replace(",", " ");
		    			}else{
		    				tempCrewName=tempCrewName+","+iter1.next().toString().replace(",", " ");
		    			} 
		        	}
		        	scheduleResource.setCrewNameList(tempCrewName);
		        	if(row[6] == null){scheduleResource.setProjectLeadName("");}else{scheduleResource.setProjectLeadName(row[6].toString());}
		        	if(row[7] == null){scheduleResource.setProjectLeadNumber("");}else{scheduleResource.setProjectLeadNumber(row[7].toString());}
		        	scheduleResourceTruckList.add(scheduleResource);
				}
			}
	        
			return scheduleResourceTruckList;
		}
		
		public void updateAssignedCrewForTruck(String updateQuery){
			getHibernateTemplate().bulkUpdate(updateQuery);
		}
		
		public List findDistinctFieldValue(String wareHse,String newWrkDt,String fieldName,String sessionCorpID){
			try{
					String query = " SELECT GROUP_CONCAT(DISTINCT("+fieldName+") SEPARATOR '~') from scheduleresourceoperation where workDate='"+newWrkDt+"' and warehouse='"+wareHse+"' and ("+fieldName+" is not null and "+fieldName+"!='') order by "+fieldName+" asc"; 	
					List list1= this.getSession().createSQLQuery(query).list();
					return list1;
			}catch (Exception e) {
					logger.error("Error executing query"+ e,e);
			}
			return null;
		}
		public Boolean getPlannedMove(String code,String parameter,String corpId){
			Boolean plannedMove=false;
			  try {
			   List isPlaanedMoveList = this
			     .getSession()
			     .createSQLQuery(
			       "select count(*) from refmaster where corpID='"+corpId+"' and parameter='"+parameter+"' and code='"+code+"' and flex1='PM' and language='en' ").list();
			   if (Integer.parseInt(isPlaanedMoveList.get(0).toString()) > 0) {
				   plannedMove = true;
			   }
		}catch (Exception e) {
			e.printStackTrace();
		}
			  return plannedMove;
		}
		
		public String findIssuedDriverCheck(String sessionCorpID,Long ticket){
			String check="";
			List countDR = getHibernateTemplate().find("select count(*) from TimeSheet where ticket="+ticket+" and crewtype='DR' and corpID ='"+sessionCorpID+"' ");
			if(Integer.parseInt(countDR.get(0).toString())>0){
				check="DR";
			}
			return check;
		}
	}

