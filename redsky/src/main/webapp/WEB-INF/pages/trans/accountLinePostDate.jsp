<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="accountLineList.title"/></title>   
    <meta name="heading" content="<fmt:message key='accountLineList.heading'/>"/> 
    <style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
			var cal = new CalendarPopup(); 
			cal.showYearNavigation(); 
			cal.showYearNavigationInput();  
	</script>
	
	
<script type="text/javascript">
  function saveData()
   {
     var idArray = new Array();
     var temlateIdList = new String();
     temlateIdList = "${invoicePost}";
     idArray = temlateIdList.split(",");
     for(var i=0;i<idArray.length;i++)
      {
        var tempId = idArray[i];
        var tempTextBox=document.forms['accountpostdateList'].elements['a'+tempId].value
	    var followUpStatusCheck = document.forms['accountpostdateList'].elements['postDateCheck'].value;
        if(followUpStatusCheck == '')
         {
          if(tempTextBox.length > 5)
           {
	         document.forms['accountpostdateList'].elements['postDateCheck'].value =tempId+'#'+tempTextBox;
           }
         }
        else
         {
           if(tempTextBox.length > 5)
            {
              var followUpStatusCheck=document.forms['accountpostdateList'].elements['postDateCheck'].value = followUpStatusCheck + ',' + tempId+'#'+tempTextBox;
            }
         }
       }
    }
    
    function updateStatus()
   { 
    document.forms['accountpostdateList'].action = 'saveInvoicePostDate.html?&btntype=yes&decorator=popup&popup=true';
    document.forms['accountpostdateList'].submit();
    }
    function pick() {
  		parent.window.opener.document.location.reload();
  		window.close();
	}
  </script>
  <script type="text/javascript">
  function calcDate()
  {
  var idArray = new Array();
     var temlateIdList = new String();
     temlateIdList = "${invoicePost}";
     idArray = temlateIdList.split(",");
     for(var i=0;i<idArray.length;i++)
      {
        var tempId = idArray[i];
        var tempTextBox=document.forms['accountpostdateList'].elements['a'+tempId].value  
       if(document.forms['accountpostdateList'].elements['a'+tempId].value=='')
       { 
       }
      else
      { 
        var date2 = document.forms['accountpostdateList'].elements['recAccDateToFormat'].value; 
        var date1 = document.forms['accountpostdateList'].elements['a'+tempId].value; 
        var date3 = document.forms['accountpostdateList'].elements['recAccDateToFormat'].value;
        var date4 = document.forms['accountpostdateList'].elements['a'+tempId].value; 
        var mySplitResult = date1.split("-");
        var day = mySplitResult[0];
        var month = mySplitResult[1];
        var year = mySplitResult[2];
        if(month == 'Jan')
        {
          month = "01";
        }
       else if(month == 'Feb')
       {
         month = "02";
       }
      else if(month == 'Mar')
       {
         month = "03"
       }
      else if(month == 'Apr')
       {
          month = "04"
       }
      else if(month == 'May')
       {
         month = "05"
       }
      else if(month == 'Jun')
       {
          month = "06"
       }
      else if(month == 'Jul')
       {
          month = "07"
       }
      else if(month == 'Aug')
       {
          month = "08"
       }
      else if(month == 'Sep')
       {
           month = "09"
       }
      else if(month == 'Oct')
       {
          month = "10"
       }
      else if(month == 'Nov')
       {
          month = "11"
       }
      else if(month == 'Dec')
       {
          month = "12";
       }
      var finalDate = month+"-"+day+"-"+year;
      var mySplitResult2 = date2.split("-");
      var day2 = mySplitResult2[0];
      var month2 = mySplitResult2[1];
      var year2 = mySplitResult2[2];
      if(month2 == 'Jan')
       {
         month2 = "01";
       }
      else if(month2 == 'Feb')
       {
          month2 = "02";
       }
      else if(month2 == 'Mar')
       {
          month2 = "03"
       }
      else if(month2 == 'Apr')
       {
          month2 = "04"
       }
      else if(month2 == 'May')
       {
          month2 = "05"
       }
      else if(month2 == 'Jun')
       {
         month2 = "06"
       }
      else if(month2 == 'Jul')
       {
         month2 = "07"
       }
      else if(month2 == 'Aug')
       {
         month2 = "08"
       }
      else if(month2 == 'Sep')
       {
          month2 = "09"
       }
      else if(month2 == 'Oct')
       {
         month2 = "10"
       }
      else if(month2 == 'Nov')
       {
         month2 = "11"
       }
      else if(month2 == 'Dec')
       {
          month2 = "12";
       }
     var finalDate2 = month2+"-"+day2+"-"+year2;
     date1 = finalDate.split("-");
     date2 = finalDate2.split("-");
     var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
     var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
     var daysApart = Math.round((sDate-eDate)/86400000); 
     if(daysApart<0)
      {
        alert("You can not enter posting date("+date4+") less than system default posting date("+date3+") on invoice #: "+tempId+"\n"); 
        document.forms['accountpostdateList'].elements['a'+tempId].value=date3;
        document.forms['accountpostdateList'].elements['a'+tempId].focus();
      } 
   }
  }
}
</script>
</head>
<s:form id="accountpostdateList" action="updateSOfromaccountLines" method="post">  
<s:hidden name="btntype" value="<%=request.getParameter("btntype") %>"/>
<input id="postDateCheck" type="hidden"  name="postDateCheck" style="width:600px"/>
<input id="shipNumberPost" type="hidden"  name="shipNumberPost" style="width:600px" value="${shipNumberPost}"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:set name="accountpostdateList" value="accountpostdateList" scope="request"/> 
<display:table name="accountpostdateList" class="table" requestURI="" id="accountpostdateList" style="width:700px" defaultsort="2" pagesize="100" >   
 		<display:column   sortable="true"  title="Total of Actual Amount" style="width:70px">
	        <div align="left"><fmt:formatNumber type="number" maxFractionDigits="0"  groupingUsed="true" value="${accountpostdateList.actualRevenueTolalInv}" /></div>
        </display:column> 
		<display:column property="recInvoiceNumber" sortable="true" title="Invoice#" style="width:70px" />
		<display:column property="billToName" sortable="true" title="Bill To" maxLength="15"  style="width:120px"/> 
	    <display:column title="Posting Date"   style="width:120px"> 
 <input id="a${accountpostdateList.recInvoiceNumber}" type="text"  name="a${accountpostdateList.recInvoiceNumber}" style="width:70px" readonly="readonly"  value='<fmt:formatDate value="${accountpostdateList.recPostDate}" type="date" dateStyle="medium" pattern="dd-NNN-yy" />' />
  <img id="c${accountpostdateList.recInvoiceNumber}" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 align="absmiddle" 
    onclick="cal.select(document.forms['accountpostdateList'].a${accountpostdateList.recInvoiceNumber},'c${accountpostdateList.recInvoiceNumber}',document.forms['accountpostdateList'].dateFormat.value);return false;" /> 
   </display:column>   
	     
</display:table> 
<table> 
<tr>
<td>
<input type="button" name="Submit" onclick="saveData(),updateStatus();" value="Apply To Invoice" class="cssbuttonA" style="width:120px; height:25px" onmouseover="calcDate();">
</td>
<td>
<input type="button" name="Submit"  value="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();">
</td>
</tr>
</table>
<s:text id="recAccDateToFormat" name="${FormDateValue}"><s:param name="value" value="recAccDateToFormat"/></s:text>
<s:hidden  id="recAccDateToFormat"  name="recAccDateToFormat" value="%{recAccDateToFormat}"  />
</s:form>
<script type="text/javascript">   

try{
if(document.forms['accountpostdateList'].elements['btntype'].value=='yes'){
pick();
}  
}
catch(e){} 
</script>  
		  	