/**
 * @Class Name	EmailSetUpUtil.java
 * @Author      Ashis Kumar Mohanty
 * @Version     V01.0
 * @Date        22-Feb-2013
 */

package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import javax.mail.Address;
import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.trilasoft.app.init.AppInitServlet;

/**
 * @author  <a>Ashis Mohanty</a>
 */
public class EmailSetUpUtil {
	static final Logger logger = Logger.getLogger(EmailSetUpUtil.class);
	public static Multipart multipleAttachment(Multipart multipart,Map<String,String> fileNameList,String mimeType){
		try {
			for (Map.Entry<String,String> map : fileNameList.entrySet()) {
				MimeBodyPart mbp = new MimeBodyPart();
				FileDataSource fds = new FileDataSource(map.getKey());
				mbp.setDataHandler(new DataHandler(fds));
				mbp.setFileName(fds.getName());
				mbp.setHeader("Content-Type", map.getValue());
				multipart.addBodyPart(mbp);
			}
		} catch (MessagingException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return multipart;
	}
	
	public static MimeMessage addAddress(MimeMessage msg,String mailAddress,RecipientType type){
		try {
			if( mailAddress !=null && (!"".contains(mailAddress.trim()))){
				String mailList[] = mailAddress.split(",");
				InternetAddress[] address = new InternetAddress[mailList.length];
				for (int i = 0; i < mailList.length; i++){
					address[i] = new InternetAddress(mailList[i]);
				}
				msg.setRecipients(Message.RecipientType.TO, mailAddress);
			}
		} catch (AddressException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}catch (Exception e) {
			e.printStackTrace();
		}
        
		return msg;
	}
	
	public static Boolean sendMail(String toTemp,String bccTemp,String ccTemp,String subject,String body,String location,String from){
		Boolean sendMailStatus=false;
		   try {
			   
			   if((getEmailOut()!=null)&&(!getEmailOut().equalsIgnoreCase(""))&&(getEmailOut().equalsIgnoreCase("YES"))){
			        String smtpHost = "localhost";
			        Properties props = System.getProperties();
			        props.put("mail.transport.protocol", "smtp");
			        props.put("mail.smtp.host", smtpHost);
			        Session session = Session.getInstance(props, null);
			        session.setDebug(true);
			        MimeMessage msg = new MimeMessage(session);
			        Multipart mp = new MimeMultipart();
			        MimeBodyPart mbp1 = new MimeBodyPart();
			        logger.warn("For content type>>>>>>>>>>");
			        body = body.replaceAll("\n", "<br>");
			        mbp1.setContent(body,"text/html; charset=UTF-8");
			        mp.addBodyPart(mbp1);
			        MimeBodyPart mbp2=null;
			        FileDataSource fds=null;
			        logger.warn("For attachment>>>>>>>>>>");
			        if ((location != null) && (!location.trim().equalsIgnoreCase(""))) {
			        	for(String str:location.split(",")){
					          try {
								mbp2 = new MimeBodyPart();
								  fds = new FileDataSource(str);
								  mbp2.setDataHandler(new DataHandler(fds));
								  mbp2.setFileName(fds.getName());
								  mp.addBodyPart(mbp2);
							} catch (Exception e) {
								e.printStackTrace();
							}
			        	}
			        }
			        logger.warn("toAddress>>>>>>>>>>");
			        if ((toTemp != null) && (!toTemp.trim().equalsIgnoreCase(""))) {
			            String strtemp=toTemp;
			  								if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))&&(getEmail().equalsIgnoreCase("")||getEmail().indexOf(strtemp)<0)){
			  									 String chk[]= strtemp.split(",");
			  									 strtemp="";
			  									 for(String str:chk){
			  										 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
			  												if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
			  												strtemp += str;
			  										 }
			  									 }
			  									}
				  		if ((strtemp != null) && (!strtemp.trim().equalsIgnoreCase(""))) {
				          String[] to = strtemp.split(",");
				          InternetAddress[] addressTo = new InternetAddress[to.length];
				          for (int i = 0; i < to.length; i++)
				            addressTo[i] = new InternetAddress(to[i]);
				          msg.setRecipients(Message.RecipientType.TO, addressTo);
				  		}
			        }
			        logger.warn("bccAddress>>>>>>>>>>");
			        if ((bccTemp != null) && (!bccTemp.trim().equalsIgnoreCase(""))) {
			            String strtemp=bccTemp;
							if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))&&(getEmail().equalsIgnoreCase("")||getEmail().indexOf(strtemp)<0)){
								 String chk[]= strtemp.split(",");
								 strtemp="";
								 for(String str:chk){
									 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
											if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
											strtemp += str;
									 }
								 }
								}
							if ((strtemp != null) && (!strtemp.trim().equalsIgnoreCase(""))) {
					          String[] bcc = strtemp.split(",");
					          InternetAddress[] addressBcc = new InternetAddress[bcc.length];
					          for (int i = 0; i < bcc.length; i++)
					            addressBcc[i] = new InternetAddress(bcc[i]);
					          msg.setRecipients(Message.RecipientType.BCC, addressBcc);
							}
			        } 
			        logger.warn("ccAddress>>>>>>>>>>");
			        if ((ccTemp != null) && (!ccTemp.trim().equalsIgnoreCase(""))) {
			            String strtemp=ccTemp;
						if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))&&(getEmail().equalsIgnoreCase("")||getEmail().indexOf(strtemp)<0)){
							 String chk[]= strtemp.split(",");
							 strtemp="";
							 for(String str:chk){
								 if((getEmailDomain().indexOf(str.substring(str.indexOf("@")+1, str.length()))>-1)){
										if (!strtemp.equalsIgnoreCase("")) strtemp += ",";
										strtemp += str;
								 }
							 }
							}
						if ((strtemp != null) && (!strtemp.trim().equalsIgnoreCase(""))) {
				          String[] cc = strtemp.split(",");
				          InternetAddress[] addressCc = new InternetAddress[cc.length];
				          for (int i = 0; i < cc.length; i++)
				            addressCc[i] = new InternetAddress(cc[i]);
				          msg.setRecipients(Message.RecipientType.CC, addressCc);
						}
			        }
			        logger.warn("Signature>>>>>>>>>>");
			        if ((from != null) && (!from.trim().equalsIgnoreCase("")) && (from.indexOf(",")<0)) {
			            String strtemp=from;
			  								if((getEmailDomain()!=null)&&(!getEmailDomain().equalsIgnoreCase(""))){
			  									if((getEmailDomain().indexOf(strtemp.substring(strtemp.indexOf("@")+1, strtemp.length()))>-1))
			  									{
			  										
			  									}
			  									else{
			  										strtemp="";
			  									}
			  						}
			  		if ((strtemp != null) && (!strtemp.trim().equalsIgnoreCase(""))) {
			            msg.setFrom(new InternetAddress(strtemp));
			          }
			        }	
			        
			        logger.warn("Subject>>>>>>>>>>");
			        msg.setSubject(subject);
			        msg.setContent(mp);
			        msg.setSentDate(new Date());
			        try
			        {
			        logger.warn("For send mail>>>>>>>>>>");  //
			        if((msg.getRecipients(Message.RecipientType.TO)!=null)&&(msg.getRecipients(Message.RecipientType.TO).length>0)&&(msg.getFrom()!=null)&&(msg.getFrom().length>0)){			        	
			          Transport.send(msg);
			          sendMailStatus=true;
			          System.out.println("Report E-mail has been sent.");
			        }
			        } catch (Exception e) {
			          e.printStackTrace();
			          sendMailStatus=false;
			        }
			   	 }
		      } catch (MessagingException mex) {
		    	  sendMailStatus=false;
		        mex.printStackTrace();
		      }
		      return sendMailStatus;
		   
	   }
	
	public static boolean emailSetUp(String bodyText,String subjcet,String from,String toAddress,String ccAddress,String bccAddress,boolean isAttachmentRequired,Map<String,String> attachmentMap,String attachmentMimeType){
		Map<String,Object> map = emailParameter(subjcet,from);
		MimeMessage msg;
			try {
				Session session = Session.getInstance((Properties)map.get("properties"), null);
				session.setDebug(true);
				
				msg = new MimeMessage(session);
				msg.setFrom(new InternetAddress(map.get("from").toString()));
				msg.setSubject(map.get("subject").toString());
				
				MimeBodyPart mbp = new MimeBodyPart();
				mbp.setContent(bodyText,"text/html");
				
				Multipart mp = new MimeMultipart();
				mp.addBodyPart(mbp);
				if(isAttachmentRequired){
					mp = multipleAttachment(mp,attachmentMap,attachmentMimeType);
				}
				msg = addAddress(msg,  toAddress,Message.RecipientType.TO);
				if(ccAddress != null && ccAddress.trim().indexOf("@") > 0){
					msg = addAddress(msg,  ccAddress,Message.RecipientType.CC);
				}
				if(bccAddress != null && bccAddress.trim().indexOf("@") > 0){
					msg = addAddress(msg,  bccAddress,Message.RecipientType.BCC);
				}
				msg.setContent(mp); 
				Transport.send(msg);
				return true;
			} catch (AddressException e) {
				e.printStackTrace();
			} catch (MessagingException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		return false;
	}
	
	public static Map<String,Object> emailParameter(String subject,String from){
		
		Map<String,Object> mailValueMap = new HashMap<String, Object>();
		
		mailValueMap.put("smtpHost","localhost");
		mailValueMap.put("from",from);
		mailValueMap.put("subject",subject);
		
		Properties props = System.getProperties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.host", mailValueMap.get("smtpHost"));
		
		mailValueMap.put("properties", props);
        
		return mailValueMap;
	}
	public static String getEmailOut() {
		return (String)AppInitServlet.redskyConfigMap.get("email.out");
	}
	public static String getEmailDomain() {
		return (String)AppInitServlet.redskyConfigMap.get("email.domainfilter");
	}
	public static String getEmail() {
		return (String)AppInitServlet.redskyConfigMap.get("email.email");
	}	
}
