<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="closedclaim_loss_summ"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="720"
		 pageHeight="842"
		 columnWidth="660"
		 columnSpacing="0"
		 leftMargin="30"
		 rightMargin="30"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="AllSectionsNoDetail"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Begin_Date" isForPrompting="true" class="java.util.Date"/>
	<parameter name="End_Date" isForPrompting="true" class="java.util.Date"/>
	<queryString><![CDATA[SELECT
     "closedclaim_loss_summ.jrxml",
     loss.`lossType` AS loss_lossType,
     sum(loss.`paidCompensationCustomer`) AS loss_paidCompensationCustomer,
     sum(loss.`paidCompensation3Party`) AS loss_paidCompensation3Party,
     loss.`lossAction` AS loss_lossAction,
     claim.`claimNumber` AS claim_claimNumber,
     refmaster.`description` AS refmaster_description
FROM
     `serviceorder` serviceorder LEFT OUTER JOIN `claim` claim  ON serviceorder.`id` = claim.`serviceOrderId`
LEFT OUTER JOIN `loss` loss ON claim.`claimNumber` = loss.`claimNumber`
LEFT OUTER JOIN `refmaster` refmaster ON loss.`lossType`=refmaster.`code` AND refmaster.`corpId`='SSCW'  AND refmaster.`parameter` IN('LOSS','Loss','loss')
WHERE serviceorder.`corpID`=$P{Corporate ID} 
AND loss.`lossAction` NOT IN('w','W') 
AND claim.`closeDate` BETWEEN $P{Begin_Date} AND $P{End_Date} 
AND serviceorder.`status` NOT IN('cancelled','closed','CNCL','CLSD')
AND loss.`lossType` NOT IN('','NULL')
GROUP BY loss.`lossType`
ORDER BY loss.`lossType`]]></queryString>

	<field name="closedclaim_loss_summ.jrxml" class="java.lang.String"/>
	<field name="loss_lossType" class="java.lang.String"/>
	<field name="loss_paidCompensationCustomer" class="java.math.BigDecimal"/>
	<field name="loss_paidCompensation3Party" class="java.math.BigDecimal"/>
	<field name="loss_lossAction" class="java.lang.String"/>
	<field name="claim_claimNumber" class="java.lang.Long"/>
	<field name="refmaster_description" class="java.lang.String"/>

	<variable name="cugrand" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{loss_paidCompensationCustomer}]]></variableExpression>
	</variable>
	<variable name="tdgrand" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$F{loss_paidCompensation3Party}]]></variableExpression>
	</variable>
	<variable name="grandtotal" class="java.math.BigDecimal" resetType="Report" calculation="Sum">
		<variableExpression><![CDATA[$V{cugrand}.add($V{tdgrand})]]></variableExpression>
	</variable>
	<variable name="totalcompcust" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$V{cugrand}.add($V{tdgrand})]]></variableExpression>
	</variable>
	<variable name="pdcust" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{loss_paidCompensationCustomer}]]></variableExpression>
	</variable>
	<variable name="pdthirdcust" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$F{loss_paidCompensation3Party}]]></variableExpression>
	</variable>
	<variable name="total" class="java.math.BigDecimal" resetType="Report" calculation="Nothing">
		<variableExpression><![CDATA[$V{pdcust}.add($V{pdthirdcust})]]></variableExpression>
	</variable>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="73"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="1"
						y="2"
						width="659"
						height="21"
						key="staticText-1"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Closed Claims Summary by Loss
]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="258"
						y="23"
						width="80"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{Begin_Date}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="dd/MM/yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="356"
						y="23"
						width="76"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{End_Date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="109"
						y="25"
						width="3"
						height="0"
						key="staticText-2"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font/>
					</textElement>
				<text><![CDATA[Static Text]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="244"
						y="23"
						width="26"
						height="18"
						key="staticText-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<text><![CDATA[from]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="338"
						y="23"
						width="18"
						height="18"
						key="staticText-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font pdfFontName="Helvetica-BoldOblique" isBold="true" isItalic="true"/>
					</textElement>
				<text><![CDATA[to]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="1"
						y="55"
						width="81"
						height="18"
						key="staticText-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Loss Type]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="240"
						y="55"
						width="140"
						height="18"
						key="staticText-7"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Paid Cust]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="380"
						y="55"
						width="140"
						height="18"
						key="staticText-9"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Paid 3rd]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="520"
						y="55"
						width="140"
						height="18"
						key="staticText-10"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Total]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="235"
						y="18"
						width="189"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="1.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<staticText>
					<reportElement
						x="82"
						y="55"
						width="158"
						height="18"
						key="staticText-14"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font pdfFontName="Helvetica-Bold" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Description]]></text>
				</staticText>
			</band>
		</columnHeader>
		<detail>
			<band height="20"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="240"
						y="2"
						width="140"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{loss_paidCompensationCustomer}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="380"
						y="2"
						width="140"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{loss_paidCompensation3Party}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="2"
						width="81"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{loss_lossType}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="82"
						y="2"
						width="158"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{refmaster_description}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="520"
						y="2"
						width="140"
						height="18"
						key="textField-7"/>
					<box></box>
					<textElement textAlignment="Right">
						<font/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{loss_paidCompensationCustomer}==null ? $F{loss_paidCompensation3Party} : ( $F{loss_paidCompensation3Party}==null ? $F{loss_paidCompensationCustomer} :  $F{loss_paidCompensationCustomer}.add($F{loss_paidCompensation3Party}))]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="21"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="240"
						y="3"
						width="140"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{cugrand}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="380"
						y="3"
						width="140"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{tdgrand}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="281"
						y="1"
						width="379"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Solid"/>
</graphicElement>
				</line>
				<textField isStretchWithOverflow="true" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="520"
						y="3"
						width="140"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{cugrand}.add($V{tdgrand})]]></textFieldExpression>
				</textField>
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<lastPageFooter>
			<band height="124"  isSplitAllowed="true" >
				<rectangle>
					<reportElement
						mode="Transparent"
						x="196"
						y="24"
						width="284"
						height="39"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch"/>
				</rectangle>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="2"
						y="105"
						width="187"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="483"
						y="105"
						width="153"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + "  of "]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="596"
						y="105"
						width="64"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right" verticalAlignment="Top">
						<font fontName="Arial" size="8"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="189"
						y="105"
						width="294"
						height="19"
						key="staticText-12"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Arial" size="8"/>
					</textElement>
				<text><![CDATA[ClosedClaim_loss_summ]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="226"
						y="35"
						width="101"
						height="18"
						key="staticText-13"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Grand Total : 

]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="¤ #,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="302"
						y="35"
						width="146"
						height="18"
						key="textField-6"
						isPrintInFirstWholeBand="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{cugrand}.add($V{tdgrand})]]></textFieldExpression>
				</textField>
			</band>
		</lastPageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
