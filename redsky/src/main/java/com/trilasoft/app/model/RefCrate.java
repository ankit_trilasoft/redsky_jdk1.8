package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "refcrate")
public class RefCrate extends BaseObject implements ListLinkData {

	private Long id;

	private String corpID;

	private String cartonType;

	private BigDecimal emptyContWeight;

	private BigDecimal length;

	private BigDecimal width;

	private BigDecimal volume;

	private BigDecimal height;

	private Integer pieces;
	private String unit1;
	private String unit2;
	private String unit3;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("cartonType", cartonType).append(
				"emptyContWeight", emptyContWeight).append("length", length)
				.append("width", width).append("volume", volume).append(
						"height", height).append("pieces", pieces).append(
						"unit1", unit1).append("unit2", unit2).append("unit3",
						unit3).append("createdBy", createdBy).append(
						"updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefCrate))
			return false;
		RefCrate castOther = (RefCrate) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(cartonType, castOther.cartonType)
				.append(emptyContWeight, castOther.emptyContWeight).append(
						length, castOther.length)
				.append(width, castOther.width)
				.append(volume, castOther.volume).append(height,
						castOther.height).append(pieces, castOther.pieces)
				.append(unit1, castOther.unit1).append(unit2, castOther.unit2)
				.append(unit3, castOther.unit3).append(createdBy,
						castOther.createdBy).append(updatedBy,
						castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(
				cartonType).append(emptyContWeight).append(length)
				.append(width).append(volume).append(height).append(pieces)
				.append(unit1).append(unit2).append(unit3).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}

	@Column(length = 20)
	public Integer getPieces() {
		return pieces;
	}

	public void setPieces(Integer pieces) {
		this.pieces = pieces;
	}

	@Column(length = 20)
	public String getCartonType() {
		return cartonType;
	}

	public void setCartonType(String cartonType) {
		this.cartonType = cartonType;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 10)
	public String getUnit1() {
		return unit1;
	}

	public void setUnit1(String unit1) {
		this.unit1 = unit1;
	}
	@Column(length = 10)
	public String getUnit2() {
		return unit2;
	}

	public void setUnit2(String unit2) {
		this.unit2 = unit2;
	}
	@Column(length = 10)
	public String getUnit3() {
		return unit3;
	}

	public void setUnit3(String unit3) {
		this.unit3 = unit3;
	}
	

	/**
	 * @return the emptyContWeight
	 */
	@Column(length = 20)
	public BigDecimal getEmptyContWeight() {
		return emptyContWeight;
	}

	/**
	 * @param emptyContWeight the emptyContWeight to set
	 */
	public void setEmptyContWeight(BigDecimal emptyContWeight) {
		this.emptyContWeight = emptyContWeight;
	}

	/**
	 * @return the height
	 */
	@Column(length = 20)
	public BigDecimal getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(BigDecimal height) {
		this.height = height;
	}

	/**
	 * @return the length
	 */
	@Column(length = 20)
	public BigDecimal getLength() {
		return length;
	}

	/**
	 * @param length the length to set
	 */
	public void setLength(BigDecimal length) {
		this.length = length;
	}

	/**
	 * @return the volume
	 */
	@Column(length = 20)
	public BigDecimal getVolume() {
		return volume;
	}

	/**
	 * @param volume the volume to set
	 */
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}

	/**
	 * @return the width
	 */
	@Column(length = 20)
	public BigDecimal getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(BigDecimal width) {
		this.width = width;
	}

	@Transient
	public String getListCode() {
		return cartonType;
	}

	@Transient
	public String getListDescription() {
		String s = String.valueOf(emptyContWeight);
		return s;
	}

	@Transient
	public String getListSecondDescription() {
		String s = String.valueOf(length);
		return s;
	}

	@Transient
	public String getListThirdDescription() {
		String s = String.valueOf(width);
		return s;
	}

	@Transient
	public String getListFourthDescription() {
		String s = String.valueOf(height);
		return s;
	}

	@Transient
	public String getListFifthDescription() {
		String s = String.valueOf(volume);
		if (s == null) {
			s = "";
		}
		return s;
	}

	@Transient
	public String getListSixthDescription() {
		String s = String.valueOf(unit2);
		if (s == null) {
			s = "";
		}
		return s;
	}
	
	@Transient 
	public String getListSeventhDescription() {
		String s = String.valueOf(unit3);
		if (s == null) {
			s = "";
		}
		return s;
	}

	/**
	 * @return the createdBy
	 */
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}

	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Transient
	public String getListEigthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListNinthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	@Transient
	public String getListTenthDescription() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
