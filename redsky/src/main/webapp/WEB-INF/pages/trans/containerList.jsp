<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>  

<head>   
    <title><fmt:message key="containerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='containerList.heading'/>"/> 
    <style type="text/css">
/* collapse */

span.pagelinks 
{display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:-1px;
margin-top:-31px;
!margin-top:-28px;
padding:2px 0px;
width:100%;
}

@media screen and (-webkit-min-device-pixel-ratio:0) {
.table td, .table th, .tableHeaderTable td{ height:5px }
}
} 
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;} 
</style> 
<script>
 <sec-auth:authComponent componentId="module.script.form.agentScript">
	window.onload = function() { 
	 trap();
		<sec-auth:authScript tableList="serviceOrder" formNameList="serviceForm1" transIdList='${serviceOrder.shipNumber}'> 
		</sec-auth:authScript> 
		}
 </sec-auth:authComponent> 
<sec-auth:authComponent componentId="module.script.form.corpAccountScript"> 
	window.onload = function() {  
		//trap(); 
	}
</sec-auth:authComponent> 
 function trap() { 
		  if(document.images)   { 
		    	for(i=0;i<document.images.length;i++)
		      { 
		      	if(document.images[i].src.indexOf('nav')>0)
						{
							document.images[i].onclick= right; 
		        			document.images[i].src = 'images/navarrow.gif';  
						}    }    }   }
		  

</script>

</head>   
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:hidden name="noteFor" id="noteFor" value="ServiceOrder"></s:hidden>
<s:form id="serviceForm1" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:100%; ">
<div id="newmnav" style="float: left;margin-bottom:0px;">
  <ul>
  <li>
  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
  <a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" /><span>S/O Details</span></a>
  </sec-auth:authComponent>
  </li>
  <sec-auth:authComponent componentId="module.tab.container.billingTab">
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  	<li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
  </sec-auth:authComponent>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.accountingTab">
  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
  <c:choose>
	<%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
	   <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
	</c:when> --%>
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose>
  </c:if> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
  <c:choose> 
	<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 <li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
	</c:when>
	<c:otherwise> 
		<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	</c:otherwise>
  </c:choose>
  </c:if> 
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">
  	<c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	  <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	  </c:if>
	  </sec-auth:authComponent>	
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}">   
    	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
  <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
       <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.forwardingTab">
  <li id="newmnav1" style="background:#FFF "><a href="containers.html?id=${serviceOrder.id}" class="current" ><span>Forwarding</span></a></li>
   </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.domesticTab">
  <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
     <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
      </c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.statusTab">
   <c:if test="${serviceOrder.job =='RLO'}"> 
	 <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	</c:if>
	<c:if test="${serviceOrder.job !='RLO'}"> 
			<li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	</c:if>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.summary.summaryTab">
		<li><a href="findSummaryList.html?id=${serviceOrder.id}"><span>Summary</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.ticketTab">
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
  </sec-auth:authComponent>
   <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
  	  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
   </sec-auth:authComponent>
   </configByCorp:fieldVisibility>
 <%--   <sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		 <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		 <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent> --%>
	<c:set var="checkTab" value="Agent" />
    <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
    <c:set var="checkTab" value="NotAgent" />
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.container.serviceOrderTab">
			      <c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			</c:if>
			</sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.container.customerFileTab">
  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
  </sec-auth:authComponent>
    <sec-auth:authComponent componentId="module.tab.serviceorder.soCriticalDate">
  		<li><a href="soAdditionalDateDetails.html?sid=${serviceOrder.id}"><span>Critical Dates</span></a></li>
	</sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.reportTab">
  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Container&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
  </sec-auth:authComponent>
   <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
    <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
</sec-auth:authComponent>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;width: 60px; float: left;"><tr>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="vertical-align:top;">
  		<a><img align="middle" src="images/navdisable_05.png"/></a>
  		</td>
  		</c:if>		
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
 <div id="Layer3" style="width:100%; !width:100%;">
<div id="newmnav">   
 <ul>
 <sec-auth:authComponent componentId="module.tab.container.sSContainertTab">
  <li id="newmnav1" style="background:#FFF "><a  href="containers.html?id=${serviceOrder.id}" class="current" ><span>SS Container</span></a></li>
 </sec-auth:authComponent>
 <sec-auth:authComponent componentId="module.tab.container.pieceCountTab">
  <li><a  href="cartons.html?id=${serviceOrder.id}" ><span>Piece Count</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.vehicleTab">
  <li><a  href="vehicles.html?id=${serviceOrder.id}" ><span>Vehicle</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.routingTab">
  <li><a href="servicePartnerss.html?id=${serviceOrder.id}"><span>Routing</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.container.consigneeInstructionsTab">
  <li><a href="editConsignee.html?sid=${serviceOrder.id}"><span>Consignee Instructions</span></a></li>
 </sec-auth:authComponent> 
 <c:if test="${countBondedGoods >= 0}" >
  <li><a href="customs.html?id=${serviceOrder.id}"><span>Customs</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.container.auditTab">
  <li>
    <a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=container&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
    <span>Audit</span></a></li>
    </sec-auth:authComponent>
  </ul> 
</div> 
<div class="spn spnSF">&nbsp;</div> 
 </div> 
  <s:hidden name="shipNumber" id ="fileID" value="%{serviceOrder.shipNumber}" />
  <s:hidden name="regNumber" id ="fileID" value="%{serviceOrder.registrationNumber}" />
  <s:hidden name="packingMode" id ="fileID" value="%{serviceOrder.packingMode}" />
  <c:set var="shipNumber" value="${serviceOrder.shipNumber}" scope="session"/>
  <c:set var="packingMode" value="${serviceOrder.packingMode}" scope="session"/>
  <c:set var="ServiceOrderID" value="${serviceOrder.id}" scope="session"/>
  <c:set var="buttons"> 
     <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editContainer.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/> 
          
     <input id="refresh" type="button" class="cssbuttonA" style="width:110px; height:25px" 
	      onclick="refreshWeights();" value="<fmt:message key="button.refreshWeights"/>"/>  
</c:set> 
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden name="customerFile.id" />
<s:hidden name="serviceOrder.id" />

<s:set name="containers" value="containers" scope="request"/>

<display:table name="containers" class="table" requestURI="" id="containerList" export="false" defaultsort="1" pagesize="100" style="width:100%;margin-top:2px;">   
 <display:column sortable="true" titleKey="container.idNumber">
 <sec-auth:authComponent componentId="module.link.container.id">
 <a onclick="goToCustomerDetail(${containerList.id});" style="cursor:pointer">
 </sec-auth:authComponent>
 <c:out value="${containerList.idNumber}" /></a></display:column> 
 <display:column  property="containerNumber" sortable="true" titleKey="container.containerNumber"/>
 <display:column headerClass="containeralign" property="sealNumber" sortable="true" titleKey="container.sealNumber" style="text-align: right"/>
 <display:column  property="size" sortable="true" titleKey="container.size"/>
 
 <c:if test="${UnitType==true}">
  <display:column headerClass="containeralign"  sortable="true" title="Gross Weight" style="width:80px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.grossWeight}" /></div>
 </display:column>
 
 <display:column headerClass="containeralign" sortable="true" titleKey="container.emptyContWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.emptyContWeight}" /></div>
                  </display:column>
 <display:column headerClass="containeralign" sortable="true" titleKey="container.netWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.netWeight}" /></div>
                  </display:column>
  </c:if>
 <c:if test="${UnitType==false}">
  <display:column headerClass="containeralign"  sortable="true" title="Gross Weight" style="width:80px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.grossWeightKilo}" /></div>
 </display:column>
 <display:column headerClass="containeralign" sortable="true" titleKey="container.emptyContWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.emptyContWeightKilo}" /></div>
                  </display:column>
 <display:column headerClass="containeralign" sortable="true" titleKey="container.netWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.netWeightKilo}" /></div>
                  </display:column>
  </c:if>
  <c:if test="${VolumeType==true}">
 <display:column  headerClass="containeralign" sortable="true" titleKey="container.volume" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.volume}" /></div>
                  </display:column>
<display:column property="density" headerClass="containeralign" sortable="true" titleKey="container.density" style="text-align: right"/>
 </c:if>
 <c:if test="${VolumeType==false}">
 <display:column  headerClass="containeralign" sortable="true" titleKey="container.volume" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${containerList.volumeCbm}" /></div>
                  </display:column>
 <display:column property="densityMetric" headerClass="containeralign" sortable="true" titleKey="container.density" style="text-align: right"/>
 </c:if>
 
 <display:column   sortable="true" titleKey="container.pieces" headerClass="containeralign" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${containerList.pieces}" /></div>
                  </display:column> 
  <c:if test="${usertype!='USER'}"> 
     <display:column title="Status" style="width:45px; text-align: center;">
     <c:if test="${containerList.status}">
      <img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
     </c:if>
     <c:if test="${containerList.status==false}">
     <img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
      
    </c:if>
    </display:column>
     </c:if>
    <display:setProperty name="paging.banner.item_name" value="container"/>  
    <display:setProperty name="paging.banner.items_name" value="container"/>
  
    <display:setProperty name="export.excel.filename" value="Container List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Container List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Container List.pdf"/>   
<display:footer >
            <tr > 
 	  	          <td align="right"  colspan="4"><b><div align="right"><fmt:message key="container.total"/></div></b></td>
		          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.grossWeightTotal}" /></div></td>
                  <td></td>
 	  	          <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.totalNetWeight}" /></div></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="2"
                  groupingUsed="true" value="${container.totalVolume}" /></div></td>
		          <td></td>
		  	      <td align="right" width="70px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${container.totalPieces}" /></div></td> 
		  	     <c:if test="${usertype!='USER'}">
		  	      <td></td> 
                  </c:if>
           </tr>
</display:footer>
</display:table>  
	<s:hidden name="id"></s:hidden>	 
	<sec-auth:authComponent componentId="module.button.container.addButton"> 
   <c:out value="${buttons}" escapeXml="false" /> 
     </sec-auth:authComponent>
     </div>
     <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
    <c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
     <c:set var="tableName" value="serviceorder" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if> 
<s:hidden name="serviceOrder.grpID"/>  
<s:hidden name="serviceOrder.grpStatus"/>
</s:form>

<script language="javascript" type="text/javascript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
</script> 

<script> 
		  function right(e) {  
		if (navigator.appName == 'Netscape' && e.which == 1) { 
		return false;
		} 
		if (navigator.appName == 'Microsoft Internet Explorer' && event.button==1) { 
		return false;
		} 
		else return true;
		}
function goToCustomerDetail(targetValue){
        document.forms['serviceForm1'].elements['id'].value = targetValue;
        document.forms['serviceForm1'].action = 'editContainer.html?from=list';
        document.forms['serviceForm1'].submit();
} 
function refreshContainer() {
	 var shipNum= document.forms['serviceForm1'].elements['shipNumber'].value;
	 url="refreshContainer.html?ajax=1&decorator=simple&popup=true&containerNos="+encodeURI(containerNos)+"&shipNum="+encodeURI(shipNum);
	 http5.open("GET", url, true);
     http5.onreadystatechange = handleHttpResponse5;
     http5.send(null);
}
function refreshWeights1() {
		var packingMode= document.forms['serviceForm1'].elements['packingMode'].value; 
		if(packingMode !='LVCO'){ 
			 } else {
			 refreshWeights("onload");
			 }
} 
function refreshWeights(check) {

	 var shipNum= document.forms['serviceForm1'].elements['shipNumber'].value;
	 var packingMode= document.forms['serviceForm1'].elements['packingMode'].value;

		if((packingMode !='LVCO') && (check != 'onload')){
		 alert("No Weights to be refreshed");
			 } else {
	 var url="refreshWeights.html?ajax=1&decorator=simple&popup=true&shipNum="+encodeURI(shipNum)+"&packingMode="+encodeURI(packingMode); 
	 http6.open("GET", url, true);
     http6.onreadystatechange = function() {handleHttpResponse4444(check)};
     http6.send(null);
}
}	
function handleHttpResponse4444(check) { 
           if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim(); 
                var  res1 = results.replace("[",'');
                var res2 = res1.replace("]",'');
                var res = res2.split(","); 
                if(res.length >=1 && res !=''){ 
					if(check != 'onload')
					{
                	var agree=confirm("Are you sure you want to overwrite container info?");
					if (agree){
					for(i=0;i<res.length;i++){
 			 		 containerNos = res[i];
 					 refreshContainer(containerNos);
 					}
					}else{ 
					return false;
					}
					}
                    } else { 
                 document.getElementById('refresh').disabled = true;
			}  } } 
function handleHttpResponse4444rrrrrrrrrrrrrrrrrrrrrrrrrrrrrr(check)
        { 
           if (http6.readyState == 4)
             {
                var results = http6.responseText
                results = results.trim(); 
                var  res1 = results.replace("[",'');
                var res2 = res1.replace("]",'');
                var res = res2.split(","); 
                if((res.length >=1) && (check != 'onload')){
                	var agree=confirm("Are you sure you want to overwrite container info?");
					if (agree){
					for(i=0;i<res.length;i++){
 			 		 containerNos = res[i];
 					 refreshContainer(containerNos);
 					}
					}else{
					return false;
					}
                    } else {
                    if((res.length-1 < 1) && (check == 'onload')){
                 document.getElementById('refresh').disabled = true;
                 }   } } } 

function handleHttpResponse5()    {
           if (http6.readyState == 4) {
                var results = http5.responseText
                results = results.trim(); 
                window.location = self.location;
        }
}
function confirmSubmit(targetElement)
	{
	var agree=confirm("Are you sure you wish to remove this row?");
	if (agree){
	var sid = document.forms['serviceForm1'].elements['serviceOrder.id'].value;	
	     location.href = "updateDeleteStatus.html?id="+encodeURI(targetElement)+"&sid="+sid;
     }else{
		return false;
	}
}  
function handleHttpResponse()
{
    var results = http6.responseText
    results = results.trim();
    window.location.reload(true);
} 
var http6 = getHTTPObject();
var http5 = getHTTPObject();

function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
} 
  function goPrev() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   } 
 function goNext() {
	progressBarAutoSave('1');
	var soIdNum =document.forms['serviceForm1'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['serviceForm1'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
  function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               location.href = 'containers.html?id='+results;
             }
       }     
function findCustomerOtherSO(position) {
 var sid=document.forms['serviceForm1'].elements['customerFile.id'].value;
 var soIdNum=document.forms['serviceForm1'].elements['serviceOrder.id'].value;
 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
  function goToUrl(id)
	{
		location.href = "containers.html?id="+id;
	} 
</script>

<script type="text/javascript">   
	try{
	 refreshWeights1();
	 }
	 catch(e){}
</script> 
<script type="text/javascript">   
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/containers.html?id=${serviceOrder.id}" ></c:redirect>
		</c:if>
		}
		catch(e){}
		function navigationVal(){
			if (window.addEventListener)
				 window.addEventListener("load", function(){
					 var imgList = document.getElementsByTagName('img');
					 for(var i = 0 ; i < imgList.length ; i++){
						if(imgList[i].src.indexOf('images/nav') > 1){
							imgList[i].style.display = 'none';
						}
					 }
				 }, false)
		}
		  <sec-auth:authComponent componentId="module.script.form.corpSalesScript">
		   navigationVal();
		 	window.onload = function() { 	 		
		 			var elementsLen=document.forms['serviceForm2'].elements.length;
		 			for(i=0;i<=elementsLen-1;i++){
		 				if(document.forms['serviceForm2'].elements[i].type=='text'){
		 						document.forms['serviceForm2'].elements[i].readOnly =true;
		 						document.forms['serviceForm2'].elements[i].className = 'input-textUpper';
		 					}else{
		 						document.forms['serviceForm2'].elements[i].disabled=true;
		 					}
		 			}
		 	}
		 	</sec-auth:authComponent>
</script>   
