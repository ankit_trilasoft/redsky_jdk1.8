<%@ include file="/common/taglibs.jsp"%>

<head>
 <title><fmt:message key="mainMenu.title"/></title>
 <meta name="heading" content="<fmt:message key='mainMenu.heading'/>"/>
 <meta name="menu" content="MainMenu"/>
 <style>
.clear{ clear:both;}
.urClass{ height:50px; width:149px; }
#menucontainer{ width:925px; text-align:center; height:350px; border:0px solid; background-image:url(images/mainmenu-bg.gif); background-repeat:no-repeat; margin:0px auto -1px auto;}
#maincontent{ width:710px; text-align:center; border:0px solid;  margin:0px auto -1px auto; padding-top:20px;}
/*#updatecontent{ width:510px; text-align:right; border:0px solid;  margin:0px auto -1px auto; padding-top:50px;}*/
.splashmargin{margin-top:45px;}
div.error, span.error, li.error, div.message {
background:#FFDFDF none repeat scroll 0 50%;
border:1px solid #DF5353;
color:#000000;
font-family:Arial,Helvetica,sans-serif;
font-size:1em;
font-weight:normal;
margin-left:800px;
padding:3px;
position:absolute;
text-align:center;
vertical-align:bottom;
width:600px;
z-index:1000;
}
#updatecontent {
    border: 0 solid;
    margin: 0 auto;
    padding-top: 90px;
    width: 100%;
}
 </style>
</head>
<s:hidden name="fileID"  id= "fileID" value=""/>

<s:form id="mainMenuForm">

<c:set var="pwdreset" value="false" scope="session"/>
<!-- 
<p><fmt:message key="mainMenu.message"/></p>
 -->
<div class="separator"></div>
<!--  
<li> 
    <a href="<c:url value="/storages.html"/>"><fmt:message key="menu.viewStorage"/></a> 
</li>
<li> 
    <a href="<c:url value="/bookStorages.html"/>"><fmt:message key="menu.viewBookStorage"/></a> 
</li>
<li> 
    <a href="<c:url value="/locations.html"/>"><fmt:message key="menu.viewLocation"/></a> 
</li> 

<s:textfield name="message" value="Welcome to RedSky Move Management System" size="100" readonly="true"/>   

<s:select name="whereToGo" list="{'Customer File','Service Order','WorkTicket','Partner'}"/>
-->


<div id="menucontainer">
<c:choose>
<c:when test="${user.userType=='AGENT'}">
  <c:if test="${vanLineAffiliation=='United' || utsNumber=='Y'}">
	<div style="width:710px; text-align:center; border:0px solid;margin:0px auto -1px auto; padding-top:60px; "><img src="images/unigroup_logo.png"  style="cursor: default;"/></div>
	<div id="maincontent" style="font: 22px arial bold ; color: #015083;">Welcome to the RedSky Agent Portal</div>
   </c:if>
   <c:if test="${vanLineAffiliation!='United' && utsNumber!='Y' }">
   <div style="width:710px; text-align:center; border:0px solid;margin:0px auto -1px auto; padding-top:60px; "><img src="images/AGENT_logo.png"  style="cursor: default;"/></div>
	<div id="maincontent" style="font: 22px arial bold ; color: #015083;">Welcome to the RedSky Agent Portal</div>
   </c:if>
</c:when>


<c:when test="${user.userType=='ACCOUNT'}">
	<div style="width:710px; text-align:center; border:0px solid;margin:0px auto -1px auto; padding-top:60px; ">
	<c:if test="${(partnerDetailLocation1!=null)}">
	<img id="userImage2" class="urClass" src="UserImage?location=${partnerDetailLocation1}" onload="secImage1()" alt="" style="border:none;" />
	</c:if>
	<c:if test="${cportalAccessCompanyDivisionLevel==true && partnerCompanyDivision!=''}">
	<img src="images/${user.corpID}_${partnerCompanyDivision}_logo.png"  style="cursor: default;"/>
	</c:if>
	<c:if test="${cportalAccessCompanyDivisionLevel==true && partnerCompanyDivision=='' }">
	<img src="images/${user.corpID}_logo.png"  style="cursor: default;"/>
	</c:if>	
	<c:if test="${cportalAccessCompanyDivisionLevel==false && partnerCompanyDivision=='' }">
	<img src="images/${user.corpID}_logo.png"  style="cursor: default;"/>
	</c:if>
	<c:if test="${cportalAccessCompanyDivisionLevel==false && partnerCompanyDivision!='' }">
	<img src="images/${user.corpID}_logo.png"  style="cursor: default;"/>
	</c:if>	
	</div>
	<c:if test="${empty landingPageWelcomeMassage}">
	<div id="maincontent" style="font: 22px arial bold ; color: #015083;">Welcome to your Move Management System.</div>
	</c:if>
	<c:if test="${not empty landingPageWelcomeMassage}">
    <div id="maincontent" style="font: 22px arial bold ; color: #015083;">${landingPageWelcomeMassage}</div>
    </c:if>
</c:when>

<c:otherwise>
	<div style="width:710px; text-align:center; border:0px solid;margin:0px auto -1px auto; padding-top:60px; "><img src="images/${user.corpID}_logo.png"  style="cursor: default;"/></div>
	<c:if test="${empty landingPageWelcomeMassage}">
	<div id="maincontent" style="font: 22px arial bold ; color: #015083;">Welcome to ${companyNameString} Move Management System.</div>
    </c:if>
    <c:if test="${not empty landingPageWelcomeMassage}">
    <div id="maincontent" style="font: 22px arial bold ; color: #015083;">${landingPageWelcomeMassage}</div>
    </c:if>
</c:otherwise>
</c:choose>

<%-- 
<configByCorp:fieldVisibility componentId="component.button.mainMenu.newsUpdate">
<div id="updatecontent">
<c:if test="${imageSelected == 'Normal' && user.corpID=='TSFT'}">
<a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes');">

 <img src="<c:url value='/images/btnupdate-ble.gif'/>" class="splashmargin" /> 
</c:if>
<c:if test="${imageSelected == 'Updated' && user.corpID=='TSFT'}">
<a onclick="window.open('newsUpdates.html?decorator=popup&popup=true&corpID=${user.corpID}','newsUpdateList','height=600,width=860,top=20, left=375, scrollbars=yes,resizable=yes');">
 <img src="<c:url value='/images/btnupdate-red.gif'/>" class="splashmargin" /> 
</a>
</c:if>
</div>
</configByCorp:fieldVisibility>
--%>
<div class="clear"></div>
</div>
</s:form>
<c:if test="${sessionExp=='true'}">
<c:redirect url="/login.jsp"/>
</c:if>
<c:if test="${not empty user.defaultURL}">
<c:redirect url="${user.defaultURL}"/>
</c:if>
<script language="javascript">
function secImage1(){
if(document.getElementById("userImage1").height > 100)
{
 document.getElementById("userImage1").style.height = "50px";
 document.getElementById("userImage1").style.width = "auto";
}
}</script>
<script type="text/javascript">
try{
secImage1();
}
catch(e){}
</script>