<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>


<head>

<title><fmt:message key="storageList.title" /></title>
<meta name="heading" content="<fmt:message key='storageList.heading'/>" />
<style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
</head>

<s:form id="storageForm" action="saveStorage" method="post"
	validate="true">
	<s:hidden name="storage.id" value="%{storage.id}" />
	<div id="Layer1">
	<table class="detailTabLabel" cellspacing="0" cellpadding="0"
		border="0">
		<tbody>
			<tr>
			<td width="0" align="right"><img width="2" height="20"
					src="<c:url value='/images/head-left.jpg'/>" /></td>
					<td width="100" class="content-tab"><a
					href="storages.html">Work On Ticket</a></td>
				
				<td width="0" align="left"><img width="2" height="20"
					src="<c:url value='/images/head-right.jpg'/>" /></td>

				<td width="4"></td>
				<td width="1" align="right"><img width="2" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="90" class="detailActiveTabLabel content-tab"><a href="editStorage.html">Detail</a></td>
				<td width="1" align="left"><img width="2" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>
				
				
				<td width="1" align="right"><img width="7" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="130" class="content-tab"><a href="locations.html">Add
				Item To Location</a></td>
				<td width="1" align="left"><img width="7" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>

				<td width="4"></td>
				<td width="1" align="right"><img width="7" height="20"
					src="<c:url value='/images/tab-left.jpg'/>" /></td>
				<td width="160" class="content-tab"><a href="bookStorages.html">Access,Release,Move
				Items </a></td>
				<td width="1" align="left"><img width="7" height="20"
					src="<c:url value='/images/tab-right.jpg'/>" /></td>
				<td width="4"></td>

			</tr>
		</tbody>
	</table>

	<table class="mainDetailTable" cellspacing="1" cellpadding="0"
		border="0">
		<tbody>
			<tr>
				<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td class="listwhite">Number</td>
							<td class="listwhite"><s:textfield name="bookStorage.idNum"
								required="true" cssClass="text medium" /></td>
							<!--  readonly="true"-->
						</tr>

						<tr>
							<td class="listwhite">Location</td>
							<td class="listwhite"><s:textfield name="storage.locationId"
								required="true" cssClass="text medium" /></td>
							<td class="listwhite">Type of Work</td>
							<td class="listwhite"><s:textfield name="storage.type"
								required="true" cssClass="text small" /></td>

						</tr>
						<tr>
							<td width="4" align="left" class="listwhite"></td>
							<td align="left" class="listwhite"></td>
							<td align="left" class="listwhite"><fmt:message
								key='storage.description' /></td>
						</tr>

						<tr>
							<td width="4" align="left" class="listwhite"></td>
							<td width="4" align="left" class="listwhite"></td>
							<td align="left" class="listwhite"><s:textarea rows="4"
								cols="20" key="storage.description" required="true"
								cssClass="text medium" /></td>
						</tr>


						<tr>
							<td></td>
							<td align="right" class="listwhite">Container ID</td>
							<td class="listwhite"><s:textfield
								name="storage.containerId" required="true"
								cssClass="text medium" /></td>
							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite">Model</td>
							<td class="listwhite"><s:textfield name="storage.model"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite">Serial</td>
							<td class="listwhite"><s:textfield name="storage.serial"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite">Weight</td>
							<td class="listwhite"><s:textfield
								name="storage.measQuantity" required="true"
								cssClass="text medium" /></td>
							<td></td>


						</tr>

						<tr>
							<td></td>
							<td align="right" class="listwhite">Item Tag</td>
							<td class="listwhite"><s:textfield name="storage.itemTag"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite">Pieces</td>
							<td class="listwhite"><s:textfield name="storage.pieces"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
						<tr>
							<td></td>
							<td align="right" class="listwhite">Price</td>
							<td class="listwhite"><s:textfield name="storage.price"
								required="true" cssClass="text medium" /></td>
							<td></td>


						</tr>
					</tbody>
				</table>
				</td>
			</tr>
		</tbody>
	</table>
	</div>

	<div align="center">
	<li class="buttonBar bottom"><s:submit cssClass="button"
		method="save" key="button.save" /> <c:if
		test="${not empty storage.id}">
		<s:submit cssClass="button" method="delete" key="button.delete"
			onclick="return confirmDelete('storage')" />
	</c:if> <s:submit cssClass="button" method="cancel" key="button.cancel" /></li>
	</div>
</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("storageForm")); 
</script>
