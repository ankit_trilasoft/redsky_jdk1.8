package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.KeySurveyQuestionMapDao;
import com.trilasoft.app.model.KeySurveyQuestionMap;

public class KeySurveyQuestionMapDaoHibernate extends GenericDaoHibernate<KeySurveyQuestionMap, Long> implements KeySurveyQuestionMapDao{

	public KeySurveyQuestionMapDaoHibernate() {
		super(KeySurveyQuestionMap.class);
	}

}
