package com.trilasoft.app.webapp.action;

 import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsMoveMgmt;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsMoveMgmtManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;




public class DsMoveMgmtAction extends BaseAction implements Preparable {
	
	private Long id;
	private String sessionCorpID;
	private  DsMoveMgmt dsMoveMgmt;
	private DsMoveMgmtManager dsMoveMgmtManager;
	private Long serviceOrderId;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  Miscellaneous  miscellaneous;
	private MiscellaneousManager miscellaneousManager ;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder;
	private TrackingStatus trackingStatus;
	private TrackingStatusManager trackingStatusManager;
	private String countDSMoveMgmtNotes;	
	private NotesManager notesManager;
	private String gotoPageString;
	private String validateFormNav;

	
	public void prepare() throws Exception { 
		
	}
	
	
    public DsMoveMgmtAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}    
    
	private String shipSize;
	private String minShip;
	private String countShip;
	private Map<String, String> relocationServices;
	private RefMasterManager refMasterManager;
    
   	public String edit() { 
		if (id != null) {
			trackingStatus=trackingStatusManager.get(id);
			serviceOrder=serviceOrderManager.get(id);
	    	customerFile = serviceOrder.getCustomerFile();
	    	miscellaneous = miscellaneousManager.get(id); 
	    	dsMoveMgmt = dsMoveMgmtManager.get(id);
		} else {
			dsMoveMgmt = new DsMoveMgmt(); 
			dsMoveMgmt.setCreatedOn(new Date());
			dsMoveMgmt.setUpdatedOn(new Date());
			dsMoveMgmt.setServiceOrderId(serviceOrderId);
		} 
		shipSize = customerFileManager.findMaximumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		minShip =  customerFileManager.findMinimumShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();
		countShip =  customerFileManager.findCountShip(serviceOrder.getSequenceNumber(),"",serviceOrder.getCorpID()).get(0).toString();		
		relocationServices=refMasterManager.findByRelocationServices(sessionCorpID, "RELOCATIONSERVICES");
		
		getNotesForIconChange();
		return SUCCESS;
	}
   	
   	
	public String save() throws Exception {
		
		boolean isNew = (dsMoveMgmt.getId() == null);  
				if (isNew) { 
					dsMoveMgmt.setCreatedOn(new Date());
		        }
				trackingStatus=trackingStatusManager.get(id);
				serviceOrder=serviceOrderManager.get(id);
		    	customerFile = serviceOrder.getCustomerFile();
		    	miscellaneous = miscellaneousManager.get(id); 
		    	dsMoveMgmt.setCorpID(sessionCorpID);
		    	dsMoveMgmt.setUpdatedOn(new Date());
		    	dsMoveMgmt.setUpdatedBy(getRequest().getRemoteUser()); 
		    	dsMoveMgmtManager.save(dsMoveMgmt);  
		    	if(!(validateFormNav=="OK"))
		    	{
		    	String key = (isNew) ? "dsMoveMgmt.added" : "dsMoveMgmt.updated";
			    saveMessage(getText(key));
		    	}
			    getNotesForIconChange();
				return SUCCESS;
				
			
	}
	@SkipValidation
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
		String s = save();
		return SUCCESS;
	}
	
	@SkipValidation
	public String getNotesForIconChange() {
		
		List noteDS = notesManager.countForDSMoveMgmtNotes(serviceOrder.getShipNumber());
		if (noteDS.isEmpty()) {
			countDSMoveMgmtNotes = "0";
		} else {
			countDSMoveMgmtNotes = ((noteDS).get(0)).toString();
		}

		return SUCCESS;
	}
	public String getCountDSMoveMgmtNotes() {
		return countDSMoveMgmtNotes;
	}
	public void setCountDSMoveMgmtNotes(String countDSMoveMgmtNotes) {
		this.countDSMoveMgmtNotes = countDSMoveMgmtNotes;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public DsMoveMgmt getDsMoveMgmt() {
		return dsMoveMgmt;
	}
	public void setDsMoveMgmt(DsMoveMgmt dsMoveMgmt) {
		this.dsMoveMgmt = dsMoveMgmt;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public Long getServiceOrderId() {
		return serviceOrderId;
	}

	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}
	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setDsMoveMgmtManager(DsMoveMgmtManager dsMoveMgmtManager) {
		this.dsMoveMgmtManager = dsMoveMgmtManager;
	}


	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}


	/**
	 * @return the countShip
	 */
	public String getCountShip() {
		return countShip;
	}


	/**
	 * @param countShip the countShip to set
	 */
	public void setCountShip(String countShip) {
		this.countShip = countShip;
	}


	/**
	 * @return the minShip
	 */
	public String getMinShip() {
		return minShip;
	}


	/**
	 * @param minShip the minShip to set
	 */
	public void setMinShip(String minShip) {
		this.minShip = minShip;
	}


	/**
	 * @return the relocationServices
	 */
	public Map<String, String> getRelocationServices() {
		return relocationServices;
	}


	/**
	 * @param relocationServices the relocationServices to set
	 */
	public void setRelocationServices(Map<String, String> relocationServices) {
		this.relocationServices = relocationServices;
	}


	/**
	 * @return the shipSize
	 */
	public String getShipSize() {
		return shipSize;
	}


	/**
	 * @param shipSize the shipSize to set
	 */
	public void setShipSize(String shipSize) {
		this.shipSize = shipSize;
	}


	/**
	 * @param refMasterManager the refMasterManager to set
	 */
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}	

}

