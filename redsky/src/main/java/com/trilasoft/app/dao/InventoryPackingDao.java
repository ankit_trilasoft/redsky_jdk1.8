package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.InventoryPacking;

public interface InventoryPackingDao extends GenericDao<InventoryPacking, Long>{
 List searchCriteria(Long sid,String  room,String  article,String sessionCorpID);
 List seachInventoryThroughPieceId(Long sid,String pieceID,String sessionCorpID);
 public List findArticleByPieceId(Long sid, String pieceID,String sessionCorpID);
 
}
