<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
    <%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Page Item List</title>   
    <meta name="heading" content="Page Item List"/>
<style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

</style>
<style type="text/css">
#overlay111 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
#loader {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<script language="javascript" type="text/javascript">
function requestedMenuItems(targetElement, evt, moduleC,url,role){
	  if((evt.checked==true)){
	    var agree = confirm(" Click Ok to update related page permissions\n[Clicking cancel will not update related page permissions\n you can choose individual pages to grant access to]");
		 if(agree) {
		//	 document.getElementById("loader").style.display = "block"
				<c:forEach items="${pageItems}" var="list1">
	  			var module='${list1.parentMenu}';
	  			var sid="${list1.id}";
	  			 if(module.trim()==moduleC.trim()){		
		   			document.getElementById(sid).checked=true;
	  			 }
	   			</c:forEach>
		 }else{
		//	 document.getElementById("loader").style.display = "block"
		 }
   		}
	  if((evt.checked==false) ){
    	  var agree = confirm(" Click Ok to update related page permissions\n[Clicking cancel will not update related page permissions\n you can choose individual pages to grant access to]");
	       if(agree) {
	   // 	   document.getElementById("loader").style.display = "block"
			  <c:forEach items="${pageItems}" var="list1">
  			    var module='${list1.parentMenu}';
	  			var sid="${list1.id}";
	  			 if(module.trim()==moduleC.trim()){		
	 			   document.getElementById(sid).checked=false;
		  		}
   			</c:forEach>
		 }else{
		//	 document.getElementById("loader").style.display = "block"	 
		 }
    }
	  populateListData();
		 showOrHide(1);
	  var requestedMI = document.forms['form2'].elements['requestedPIP'].value;
	  var menuId = document.forms['form2'].elements['menuId'].value;

	  var url = "updatePageSecurities.html";
	  var parameters = "decorator=simple&popup=true&requestedPIP="+encodeURIComponent(requestedMI)+"&menuId="+encodeURIComponent(menuId);
	  http2.open("POST", url, true); 
	  http2.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	  http2.setRequestHeader("Content-length", parameters .length);
	  http2.setRequestHeader("Connection", "close");
	  http2.onreadystatechange = handleHttpResponse2;
	  http2.send(parameters);

}
function handleHttpResponse2(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();     	  
     	  showOrHide(0);
	}

}
function getXMLHttpRequestObject()
{
  var xmlhttp;
  if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
    try {
      xmlhttp = new XMLHttpRequest();
    } catch (e) {
      xmlhttp = false;
    }
  }
  return xmlhttp;
}
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
var http2 = getXMLHttpRequestObject();
function clear_fields(){
	document.forms['form2'].elements['pageMenu'].value = '';
	document.forms['form2'].elements['pageDesc'].value = '';
	document.forms['form2'].elements['pageUrl'].value = '';
}
function findPageItemList(){	
	document.getElementById("loader").style.display = "block"
    document.forms['form2'].action ="pageSecurities.html?perRec=${perRec}&decorator=popup&popup=true";
	document.forms['form2'].submit();
	
}
function backMenuItemList(){
	document.getElementById("loader").style.display = "block"
    document.forms['form2'].action ="menuItems.html?perRec=${perRec}&decorator=popup&popup=true";
	document.forms['form2'].submit();
	 //showOrHide(0);
}
function populateListData(){
	try{
		  var role='${perRec}';
			var permission=0;
			 var newIds="";
				<c:forEach items="${pageItems}" var="list1">
					var sid="${list1.id}";
					var url="${list1.url}";	
					if(document.getElementById(sid).checked==true){permission=2;}else{permission=0;}
						 if(newIds==''){   
						     newIds=sid+"#"+permission+"#"+role+"#"+url;
						 }else{
						     newIds=newIds + ',' + sid+"#"+permission+"#"+role+"#"+url;
						 }
				</c:forEach>
				document.forms['form2'].elements['requestedPIP'].value=newIds;
		}catch(e){}	
}
function showOrHide(value) {
    if (value == 0) {
       	if (document.layers)
           document.layers["overlay111"].visibility='hide';
        else
           document.getElementById("overlay111").style.visibility='hidden';
   	}else if (value == 1) {
   		if (document.layers)
          document.layers["overlay111"].visibility='show';
       	else
          document.getElementById("overlay111").style.visibility='visible';
   	}
}

</script>
<s:form id="form2" action="assignPageItem.html?perRec=${perRec}&decorator=popup&popup=true" method="post">
<div id="overlay111">

<div id="layerLoading">

<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
  
   </div>
   </div>

<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
    <input type="button" class="cssbutton" value="Search" style="width:55px; height:25px;" onclick="findPageItemList();"/>
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Parent Menu Name</th>
		<th>Description</th>
		<th>URL</th>		
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="pageMenu" required="true" cssClass="input-text" size="30"/>
					</td>
					<td align="left">
					    <s:textfield name="pageDesc" required="true" cssClass="input-text" size="30"/>
					</td>					
  				    <td align="left">
					    <s:textfield name="pageUrl" required="true" cssClass="input-text" size="30"/>
					</td>
				</tr>		
				<tr><td></td><td></td><td align="right" colspan="0"><c:out value="${searchbuttons}" escapeXml="false"/></td></tr>			
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<s:hidden name="menuId"/>
<s:hidden name="menuUrl"/>
<s:hidden name="menuName"/>
<s:hidden name="requestedPIP" value=""/>
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
<tbody>
<tr>
  <td align="left">
  <ul id="newmnav">
  <li id="newmnav1"><a class="current"><span>Page List</span></a></li>
  <li><a onclick="backMenuItemList();"><span>Menu List</span></a></li>  
  </ul>
  <td style="padding-top:10px;">
<span style="font-weight: bold; font-size: 13px; padding: 20px 0pt 0pt 20px;">${perRec}</span>
</td>
</tr>
</td></tr>
</tbody></table>
<s:set name="pageItems" value="pageItems" scope="request"/> 
<display:table name="pageItems" class="table" requestURI="" id="pageItemList" defaultsort="4" export="false">
<display:column title="Assign" sortable="true">
	<c:set var="selectPermission" value="false" />
	<c:forEach var="mp" begin="0" end="${pipCount}" step="1">
	<c:set var="temp" value="${pageItemList.id}" />
	<c:set var="temp1" value="${pps[mp][0]}" />
		<script language="javascript" type="text/javascript">
</script>
	<c:if test="${temp1 == temp}" >
	<c:if test="${pps[mp][1] == '2'}" >
		<c:set var="selectPermission" value="true" />
		</c:if>
	<c:if test="${pps[mp][1] != '2'}" >
		<c:set var="selectPermission" value="false" />
		</c:if>
	</c:if>
	<c:if test="${temp1 != temp && selectPermission==false}" >
		<c:set var="selectPermission" value="false" />
	</c:if>

	</c:forEach> 

<s:checkbox id="${pageItemList.id}" name="permision11" value="${selectPermission}" onclick="requestedMenuItems('${pageItemList.id}',this,'${pageItemList.parentMenu}','${pageItemList.url}','${perRec}')" required="true" />
</display:column>
<display:column property="parentMenu" sortable="true" style="width:120px" title="Parent Menu Name" />
<display:column property="description" sortable="true" style="width:190px" titleKey="menuItem.description" />
	<c:if test="${securityChecked == 'Y'}">
		<display:column title="Assign&nbsp;ROLE" style="width:120px" sortable="true">
				<c:out value="${pageItemList.roleList}"/>
		</display:column>
	</c:if>
<display:column property="url" sortable="true" titleKey="menuItem.url" />
</display:table>
<table><tr><td align="left">

<input type="button" class="cssbutton" value="Back To Menu List" style="width:125px; height:25px;" onclick="backMenuItemList();"/>
</td></tr>
</table>
<div id="loader" style="text-align:center; display:none">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>
<tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
</div>
</s:form>
 <script>
setTimeout("showOrHide(0)",2000);
</script>
<script language="javascript" type="text/javascript">
populateListData();
</script>