<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>Vanline Integration Log</title>
    <meta name="heading" content="Vanline Integration Log"/>
   
   <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
   </script>
   <!-- Modification closed here -->
    
    <script language="javascript" type="text/javascript">
		function clear_fields(){
			var i;
					for(i=0;i<=4;i++)
					{
						document.forms['integrationLogForm'].elements['integrationLog.fileName'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.processedOn'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.recordID'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.message'].value = "";
						document.forms['integrationLogForm'].elements['integrationLog.transactionID'].value = "";
						document.forms['integrationLogForm'].elements['intType'].value = "";
						document.forms['integrationLogForm'].elements[i].value = "";
					}
		}
</script>
   
   <style> 
   span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
!margin-bottom:2px;
margin-top:-22px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
   
   </style>
</head>

<s:form id="integrationLogForm" action="searchCustomCenterIntegrationLog" method="post" validate="true"> 
 <!-- <p style="font:bold 12px/1.25em arial,verdana;margin-bottom:10px">Integration XML Summary</p> -->
 <p style="font: bold 12px/2.5em arial,verdana; margin-bottom: 10px;" class="bgblue">Integration Centre</p>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="newmnav">
		  <ul>
		<c:if test="${vanlineEnabled==true }">
		  <li><a href="integrationCenterList.html"><span>Vanline / Docs Downloads</span></a></li>
		     <li><a href="centreVanlineUpload.html"><span>Vanline / Docs Uploads</span></a></li>
		      <li style="background:#FFF " id="newmnav1"><a class="current"><span>Vanline Integration Log</span></a></li>
		       </c:if>
		        <c:if test="${enableMSS==true }">
		       <li><a href="mssLogList.html"><span>MSS Orders</span></a></li>
		       </c:if>
		        <li><a href="UGWWList.html"><span>UGWW</span></a></li>
		        <li><a href="centerintegrationLogList.html"><span>UGWW Log</span></a></li>
		         <c:if test="${voxmeIntegration==true }">
		         <li><a href="voxmeEstimatorList.html"><span>Voxme&nbsp;Estimator</span></a></li>
		         </c:if>
		          <c:if test="${enablePricePoint==true}">
		          <li><a href="pricePointList.html"><span>Price&nbsp;Point</span></a></li>
		          </c:if>
		           <c:if test="${enableMoveForYou==true }">
		        <li><a href="moveForUList.html"><span>Move&nbsp;4&nbsp;u</span></a></li>
		        </c:if>
		   		  </ul>
		</div>
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:34px;"><span></span></div>
    <div class="center-content">
<div id="otabs" style="margin-bottom:24px;">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
<table class="table" width="100%" >
<thead>
<tr>

<th><fmt:message key="integrationLog.fileName" /></th>
<th><fmt:message key="integrationLog.processedOn" /></th>
<th><fmt:message key="integrationLog.recordID" /></th>
<th><fmt:message key="integrationLog.message" /></th>
<th><fmt:message key="integrationLog.transactionID" /></th>
<th>Type</th>
<th></th>

</tr></thead>	
		<tbody>
		<tr>
			
			<td><s:textfield name="integrationLog.fileName" required="true" size="40" cssClass="input-text" onkeypress="return checkIt(event)" onfocus="onFormLoad();" cssStyle="width:130px"/></td>
			<!--<td><s:textfield name="integrationLog.processedOn"  required="true" cssClass="text medium"/></td>-->
			<c:if test="${not empty integrationLog.processedOn}">
				<s:text id="integrationLogprocessedOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="integrationLog.processedOn" /></s:text>
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="integrationLog.processedOn" value="%{integrationLogprocessedOnFormattedValue}" required="true" cssStyle="width:60px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)"/><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty integrationLog.processedOn}">
				<td align="left"><s:textfield cssClass="input-text" id="processedOn" name="integrationLog.processedOn" required="true" cssStyle="width:60px" maxlength="11" readonly="true"   onkeydown="return onlyDel(event,this)" /><img id="processedOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
			<td><s:textfield name="integrationLog.recordID" size="30" required="true" cssClass="input-text" cssStyle="width:90px"/></td>
			<td><s:textfield name="integrationLog.message" size="30" required="true" cssClass="input-text" cssStyle="width:90px"/></td>
			<td><s:textfield name="integrationLog.transactionID" size="30" required="true" cssClass="input-text" cssStyle="width:90px"/></td>
				<td><s:select name="intType"  cssClass="list-menu" headerKey="" headerValue="" cssStyle="width:130px"  list="%{integrationType}"/></td>
			<td colspan="2">
			<s:submit cssClass="cssbutton" cssStyle="width:60px;" key="button.search"  />
			    <input type="button" class="cssbutton" value="Clear" style="width:60px;" onclick="clear_fields();"/>  
			</td>
			</tr>			
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
		<div id="newmnav">
		  <ul>

		   
		      <li><a class=""><span>Vanline Integration Log List</span></a></li>
		       
		   		  </ul>
		</div>
		<div class="spn" style="line-height:2px;!margin-bottom:3px;">&nbsp;</div>		
<s:set name="integrationLogs" value="integrationLogs" scope="request"/>
<display:table name="integrationLogs" class="table" requestURI="" id="integrationLogList" export="true" pagesize="50">
    <display:column property="id" sortable="true" paramId="id" paramProperty="id" titleKey="integrationLog.id"/>
    <display:column property="message" sortable="true" titleKey="integrationLog.message"/>
    <display:column property="fileName" sortable="true" titleKey="integrationLog.fileName"/>
 	<display:column property="batchID" style="text-align: right" sortable="true" titleKey="integrationLog.batchID"/>
    <display:column property="processedOn" sortable="true" title="Processed On" style="width:150px; text-align:right;" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="recordID" sortable="true" titleKey="integrationLog.recordID"/>
     <display:column  sortable="true" titleKey="integrationLog.transactionID">    
    	<c:set var="sign" value="${integrationLogList.fileName}"/>    
	    <c:choose>
			<c:when test="${fn:startsWith(sign, 'st')}">
				<a href="editVanLine.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:when>
			<c:when test="${integrationLogList.controlFlag=='Q'}">
			<a href="editQuotationServiceOrderUpdate.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:when>
			<c:when test="${integrationLogList.fileName=='Quotes-2-Go'}">
			<a href="editCustomerFile.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:when>
			<c:when test="${integrationLogList.fileName=='Trans Docs'}">
			<c:out value="${integrationLogList.transactionID}" />
			</c:when>
	    	<c:otherwise>
	    		<a href="editServiceOrderUpdate.html?id=${integrationLogList.serviceOrderId}"><c:out value="${integrationLogList.transactionID}" /></a>
			</c:otherwise>
		</c:choose>
    </display:column>
     <display:column   sortable="true" title="Type"><c:forEach var="trmp" items="${integrationType}"><c:if test="${trmp.key==(fn:substring(integrationLogList.fileName,0, 2))}"><c:out value="${trmp.value}" /></c:if></c:forEach></display:column>    
    <display:setProperty name="paging.banner.item_name" value="integrationLog"/>
    <display:setProperty name="paging.banner.items_name" value="people"/>

    <display:setProperty name="export.excel.filename" value="IntegrationLog List.xls"/>
    <display:setProperty name="export.csv.filename" value="IntegrationLog List.csv"/>
    <display:setProperty name="export.pdf.filename" value="IntegrationLog List.pdf"/>
</display:table>
</s:form>
<script type="text/javascript">
    //highlightTableRows("integrationLogList");
    //Form.focusFirstElement($("integrationLogForm")); 
    function setData(temp){
    	document.forms['integrationLogForm'].action="searchCustomCenterIntegrationLog.html";
    	/* if(temp.value=='mu' || temp.value=='tu' || temp.value=='td' || temp.value=='qt'){
    		//alert("action in");
    		document.forms['integrationLogForm'].action="searchCustomCenterIntegrationLog.html";
    		
    	}else{
    		//alert("action out");
    		document.forms['integrationLogForm'].action="searchCenterIntegrationLog.html";
    		
    	} */
    }
   	
   	RANGE_CAL_1 = new Calendar({
           inputField: "processedOn",
           dateFormat: "%d-%b-%y",
           trigger: "processedOn_trigger",
           bottomBar: true,
           animation:true,
           onSelect: function() {                             
               this.hide();
       }
        
   });
  
</script>