package com.trilasoft.app.webapp.action;


import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

import java.util.*;

import org.appfuse.webapp.action.BaseAction;   
import com.trilasoft.app.model.Agent;

import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.AgentManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;

public class AgentAction extends BaseAction {   
  
	private List ls;//for combo
	private Set agents;
   // private List agents;  
    private List serviceOrders;
    private List servicePartners;
    private AgentManager agentManager;
    private ServicePartnerManager servicePartnerManager;
    private ServiceOrderManager serviceOrderManager;
    private Agent agent;
    private ServiceOrder serviceOrder;
    private Long id;
    private String shipNum;
    private String carrierNumber;
    private Long autoCarrier;
    private List maxCarrierNumber;
    private long IdMax;
    private List refMasters;
    private ServicePartner servicePartner;
    
    private RefMasterManager refMasterManager;//
    private static Map<String, String> yesno;//for combo
    private static Map<String, String> SITOUTTA;
    private static Map<String, String> omni;
    private static Map<String, String> partnerType;
    Date currentdate = new Date();

    static final Logger logger = Logger.getLogger(AgentAction.class);


 /*   
    public void setAgentManager(AgentManager agentManager) {
        this.agentManager = agentManager;
    }
    
    public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
        this.servicePartnerManager = servicePartnerManager;
    }
    
    public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
        this.serviceOrderManager = serviceOrderManager;
    }
    
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }//for combo
    
    public List getServiceOrders() {   
        return serviceOrders;   
    }   
    public Set getAgents() {   
        return agents;   
    }   
   
    public List getRefMasters(){
    	return refMasters;//for combo
    }
    public String list() {   
    	ls = agentManager.getAll();
    	agents=new HashSet(ls);
    	//getComboList("SSCW");
        return SUCCESS;   
    }//for combo
    
    public String getComboList(String corpID){
 	   yesno = refMasterManager.findByParameter(corpID, "YESNO");
 	  SITOUTTA = refMasterManager.findByParameter(corpID, "SITOUTTA");
 	 omni = refMasterManager.findByParameter(corpID, "omni");
 	partnerType = refMasterManager.findByParameter(corpID, "partnerType");
 	   return SUCCESS;
    }
    //for combo
    
    
    public  Map<String, String> getYesno() {
		return yesno;
	}
      //for combo
    public  Map<String, String> getSITOUTTA() {
		return SITOUTTA;
	}   
    
    public  Map<String, String> getOmni() {
		return omni;
	} 
    
    public  Map<String, String> getPartnerType() {
		return partnerType;
	}
    
    
    public void setId(Long id) {   
        this.id = id;   
    }   
      
    public Agent getAgent() {   
        return agent;   
    }   
      
    public void setAgent(Agent agent) {   
        this.agent = agent;   
    }   
    public ServiceOrder getServiceOrder() {   
        return serviceOrder;   
    }   
      
    public void setServiceOrder(ServiceOrder serviceOrder) {   
        this.serviceOrder = serviceOrder;   
    } 
    
    public ServicePartner getServicePartner() { 
        return servicePartner; 
    } 
     
    public void setServicePartner(ServicePartner servicePartner) { 
        this.servicePartner = servicePartner; 
    } 
    
    public List getServicePartners() {
		return servicePartners;
	}

	public String servicePartnerlist() {  
		System.out.println(id);
    	agent = agentManager.get(id);
    	serviceOrder = agent.getServiceOrder();
    	System.out.println("Before array\n\n\n");
    	//servicePartners = new ArrayList(agent.getServicePartners());
    	System.out.println(servicePartners);
    	return SUCCESS;   
    }
    
    
    
    public String delete() {   
    	agentManager.remove(agent.getId());   
        saveMessage(getText("agent.deleted"));   
      
        return SUCCESS;   
    }   
      
    public String edit() {
    	//IdMax = Long.parseLong((agentManager.findBillingMaxId(id)).get(0).toString())+1;
    //getComboList("SSCW");
    	if (id != null)
	     {  
		      serviceOrder=serviceOrderManager.get(id);
    		  if((agentManager.checkById(id)).isEmpty())
		    	{
		    		System.out.println("Congrat");
		    		agent = new Agent();
			    	//agent.setId(id);
					agent.setShipNumber(serviceOrder.getShipNumber());
					agent.setCreatedOn(new Date());
			   		agent.setUpdatedOn(new Date());
		    	}
		    	else
		    	{
		    		agent = agentManager.get(id);
		    		//agent.setCreatedOn(new Date());
		    		agent.setUpdatedOn(new Date());
		    	}
		}
    	return SUCCESS;
    }   
      
    public String editNewCarrier() {
    	//getComboList("SSCW");
    	if (id != null) {
    		agent = agentManager.get(id);
    	    maxCarrierNumber = agentManager.findMaximumCarrierNumber(agent.getShipNumber());
            if ( maxCarrierNumber.get(0) == null ) {          
            	carrierNumber = "01";
            }else {
	            autoCarrier = Long.parseLong((maxCarrierNumber).get(0).toString()) + 1;
	            System.out.println(autoCarrier);
	            if((autoCarrier.toString()).length() == 1) {
	            	carrierNumber = "0"+(autoCarrier.toString());
	            }else {
	            	carrierNumber=autoCarrier.toString();
	            }
            }
	      
            servicePartner = new ServicePartner();  
            servicePartner.setCarrierNumber(carrierNumber);
            servicePartner.setCreatedOn(new Date());
            servicePartner.setUpdatedOn(new Date());
    
    	
    }
    	return SUCCESS;
  }
    
    public String save() throws Exception {   
        if (cancel != null) {   
            return "cancel";   
        }   
      
        if (delete != null) {   
            return delete();   
        }
        /*
        String email1=agent.getBookingEmail();
        String email2=agent.getDestinationAgentEmail();
        String email3=agent.getOriginAgentEmail();
        String email4=agent.getFreightForworderEmail();
        email1=email1.trim();
        email2=email2.trim();
        email3=email3.trim();
        //email4=email4.trim();
        if(email1.indexOf('@')==-1 || email1.indexOf('.')==-1 || email1.indexOf('@')>email1.lastIndexOf('.'))
        {
          
        String key = "email.notvalid";
        saveMessage(getText(key));


        return INPUT;

        }
        if(email2.indexOf('@')==-1 || email2.indexOf('.')==-1 || email2.indexOf('@')>email2.lastIndexOf('.'))
        {
          
        String key = "email.notvalid";
        saveMessage(getText(key));


        return INPUT;

        }
        if(email3.indexOf('@')==-1 || email3.indexOf('.')==-1 || email3.indexOf('@')>email3.lastIndexOf('.'))
        {
          
        String key = "email.notvalid";
        saveMessage(getText(key));


        return INPUT;

        }
        if(email4.indexOf('@')==-1 || email4.indexOf('.')==-1 || email4.indexOf('@')>email4.lastIndexOf('.'))
        {
          
        String key = "email.notvalid";
        saveMessage(getText(key));


        return INPUT;

        }*/
 /*     //  agent.setServiceOrder(serviceOrder);
        boolean isNew = (agent.getId() == null);   
        agent.setUpdatedOn(new Date());
        agent.setShipNumber(serviceOrder.getShipNumber());
        
        agentManager.save(agent);  
        
        //serviceOrderManager.save(serviceOrder);
      
        String key = (isNew) ? "agent.added" : "agent.updated";   
        saveMessage(getText(key));   
      
        if (!isNew) {   
            return INPUT;   
        } else {   
            return SUCCESS;   
        }   
    }  

    //By Madhu
    private Long sid;
	public String carrierList() {  
    	serviceOrder = serviceOrderManager.get(sid); 
    	agent=agentManager.get(sid);
    	System.out.println("/n/n/n/n After agent /n"+agent);
    	//servicePartners = new ArrayList( agent.getServicePartners());
    	System.out.println("/n/n/n After Carrier /n/n"+servicePartners);
    	//System.out.println(payables);
    	return SUCCESS;   
    }

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	}
    
    */
    
}
    
 


