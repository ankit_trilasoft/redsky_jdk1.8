package com.trilasoft.app.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "history")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class AuditTrail extends BaseObject {




	private Long id;

	private String corpID;

	private String xtranId;

	private String tableName;

	private String fieldName;

	private String description;

	private String oldValue;

	private String newValue;

	private String user;

	private Date dated;
	private String alertRole;
	private String alertUser;
	private Boolean isViewed;
	private String alertFileNumber;
	private String alertShipperName;
	

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("xtranId", xtranId)
				.append("tableName", tableName).append("fieldName", fieldName)
				.append("description", description)
				.append("oldValue", oldValue).append("newValue", newValue)
				.append("user", user).append("dated", dated)
				.append("alertRole", alertRole).append("alertUser", alertUser)
				.append("isViewed", isViewed)
				.append("alertFileNumber", alertFileNumber)
				.append("alertShipperName", alertShipperName).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AuditTrail))
			return false;
		AuditTrail castOther = (AuditTrail) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(xtranId, castOther.xtranId)
				.append(tableName, castOther.tableName)
				.append(fieldName, castOther.fieldName)
				.append(description, castOther.description)
				.append(oldValue, castOther.oldValue)
				.append(newValue, castOther.newValue)
				.append(user, castOther.user).append(dated, castOther.dated)
				.append(alertRole, castOther.alertRole)
				.append(alertUser, castOther.alertUser)
				.append(isViewed, castOther.isViewed)
				.append(alertFileNumber, castOther.alertFileNumber)
				.append(alertShipperName, castOther.alertShipperName)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID).append(xtranId)
				.append(tableName).append(fieldName).append(description)
				.append(oldValue).append(newValue).append(user).append(dated)
				.append(alertRole).append(alertUser).append(isViewed)
				.append(alertFileNumber).append(alertShipperName).toHashCode();
	}

	//private String note;
	@Column(length = 25)
	public String getAlertRole() {
		return alertRole;
	}

	public void setAlertRole(String alertRole) {
		this.alertRole = alertRole;
	}


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column
	public Date getDated() {
		return dated;
	}

	public void setDated(Date dated) {
		this.dated = dated;
	}

	@Column(length = 255)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(length = 255)
	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	@Column(length = 255)
	public String getNewValue() {
		return newValue;
	}

	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/*@Column(length = 5000)
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
*/
	@Column(length = 255)
	public String getOldValue() {
		return oldValue;
	}

	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	@Column(length = 255)
	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Column(length = 255)
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	@Column
	public String getXtranId() {
		return xtranId;
	}

	public void setXtranId(String xtranId) {
		this.xtranId = xtranId;
	}
	@Column(length = 255)
	public String getAlertUser() {
		return alertUser;
	}

	public void setAlertUser(String alertUser) {
		this.alertUser = alertUser;
	}

	public Boolean getIsViewed() {
		return isViewed;
	}

	public void setIsViewed(Boolean isViewed) {
		this.isViewed = isViewed;
	}
	@Column(length = 15)
	public String getAlertFileNumber() {
		return alertFileNumber;
	}

	public void setAlertFileNumber(String alertFileNumber) {
		this.alertFileNumber = alertFileNumber;
	}
	@Column(length = 116)
	public String getAlertShipperName() {
		return alertShipperName;
	}

	public void setAlertShipperName(String alertShipperName) {
		this.alertShipperName = alertShipperName;
	}

}
