package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.dao.GenericDao;
import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.UserDataSecurityDao;
import com.trilasoft.app.model.UserDataSecurity;
import com.trilasoft.app.service.UserDataSecurityManager;

public class UserDataSecurityManagerImpl extends GenericManagerImpl<UserDataSecurity, Long> implements UserDataSecurityManager{

	UserDataSecurityDao userDataSecurityDao;
	public UserDataSecurityManagerImpl(UserDataSecurityDao userDataSecurityDao) {
		super(userDataSecurityDao);
		this.userDataSecurityDao=userDataSecurityDao;
	}
	public List findById(Long id) {
		return userDataSecurityDao.findById(id);
	}

	

}
