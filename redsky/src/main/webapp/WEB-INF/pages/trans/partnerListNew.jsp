<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerListForm'].elements['stateSearch'].value = '';
		document.forms['partnerListForm'].elements['countrySearch'].value = '';
		document.forms['partnerListForm'].elements['partner.status'].value = '';
		document.forms['partnerListForm'].elements['partner.isPrivateParty'].checked = false;
		document.forms['partnerListForm'].elements['partner.isAccount'].checked = false;
		document.forms['partnerListForm'].elements['partner.isAgent'].checked = false;
		document.forms['partnerListForm'].elements['partner.isVendor'].checked = false;
		document.forms['partnerListForm'].elements['partner.isCarrier'].checked = false;
		document.forms['partnerListForm'].elements['partner.isOwnerOp'].checked = false;
}
function activBtn(){
	document.forms['addPartner'].elements['addBtn'].disabled = false;
}

function seachValidate(){
		var pp =  document.forms['partnerListForm'].elements['partner.isPrivateParty'].checked;
		var ac =  document.forms['partnerListForm'].elements['partner.isAccount'].checked;
		var ag =  document.forms['partnerListForm'].elements['partner.isAgent'].checked;
		var vd =  document.forms['partnerListForm'].elements['partner.isVendor'].checked;
		var cr =  document.forms['partnerListForm'].elements['partner.isCarrier'].checked;
		var oo =  document.forms['partnerListForm'].elements['partner.isOwnerOp'].checked;
		if (pp == false && ac == false && ag ==false && vd == false && cr == false && oo == false){
			alert('Please select atleast one partner type.');
			return false;
		} 
}
</script>
<style>
span.pagelinks {
display:block;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:99%;
!width:98%;
font-size:0.85em;
}



.radiobtn {
margin:0px 0px 2px 0px;
!padding-bottom:5px;
!margin-bottom:5px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;

}
</style>
</head>
<c:set var="buttons">   
	<c:if test="${not empty param.popup && partnerType == 'PP'}">  
		<input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddFormPopup.html?partnerType=${partnerType}&decorator=popup&popup=true"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
	<c:if test="${empty param.popup}">  
    <input type="button" class="cssbutton" style="width:55px; height:25px" 
        onclick="location.href='<c:url value="/editPartnerAddForm.html?partnerType=${partnerType}"/>'"  
        value="<fmt:message key="button.add"/>"/>
    </c:if>          
</c:set> 
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px; !margin-bottom:10px;" align="top" method="" key="button.search" onclick="return seachValidate();"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerListForm" action='${empty param.popup?"searchPartnerAdmin.html":"searchPartner.html?decorator=popup&popup=true"}' method="post" >  
<div id="layer5" style="width:100%">
<s:hidden name="findFor" value="<%= request.getParameter("findFor")%>" />
<s:hidden name="popupFrom" value="<%= request.getParameter("popupFrom")%>" />
<s:hidden name="accountLine.id" value="%{accountLine.id}"/>   
<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
<s:hidden name="sid" value="<%=request.getParameter("sid")%>"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />

<s:hidden name="accountLine.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
<s:hidden name="serviceOrder.sequenceNumber"/>
<s:hidden name="serviceOrder.ship"/>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden  name="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" /> 
    <c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
</c:if>
<div id="otabs">
		<ul>
			<li><a class="current"><span>Search</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 
	<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top" style="margin-top:10px;!margin-top:-5px;  "><span></span></div>
<div class="center-content">
<table class="table" border="0" style="width:99%;">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Last Name/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>
			<th><fmt:message key="partner.status"/></th>
			
			<c:set var="ServiceOrderID" value="<%=request.getParameter("sid")%>" scope="session"/>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partner.firstName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="20" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:100px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isPrivateParty}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px">Private party<s:checkbox key="partner.isPrivateParty" value="${ischecked}" fieldValue="true" onclick="changeStatus();" /></td>				
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAccount}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border-left:hidden;"><fmt:message key='partner.accounts'/><s:checkbox key="partner.isAccount" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="12"/></td>
			
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isAgent}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="right" class="listwhitetext" width="100px" style="border-left:hidden;"><fmt:message key='partner.agents'/><s:checkbox key="partner.isAgent" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="13"/></td>
										
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isVendor}">
			<c:set var="ischecked" value="true"/>
			</c:if>					
			<td align="center" class="listwhitetext" width="100px" style="padding-left:20px ;border-left:hidden;"><fmt:message key='partner.vendors'/><s:checkbox key="partner.isVendor" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="14"/></td>
											
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isCarrier}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border-left:hidden;"><fmt:message key='partner.carriers'/><s:checkbox key="partner.isCarrier" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="15"/></td>
					
			<c:set var="ischecked" value="false"/>
			<c:if test="${partner.isOwnerOp}">
			<c:set var="ischecked" value="true"/>
			</c:if>						
			<td align="center" class="listwhitetext" width="100px" style="border-left:hidden;"><fmt:message key='partner.owner'/><s:checkbox key="partner.isOwnerOp" value="${ischecked}" fieldValue="true" onclick="changeStatus();" tabindex="16"/></td>
		
			<td width="130px" style="border-left:hidden;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
</s:form>
<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
		<ul>
			<li><a class="current"><span>Partner List</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partners" value="partners" scope="request"/>  
<display:table name="partners" class="table" requestURI="" id="partnerList" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}' >   
	<c:if test="${empty param.popup}">  
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" paramId="id" paramProperty="id" />   
	</c:if>		
	<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" titleKey="partner.partnerCode"/>   
    </c:if>	
	
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
	
	<display:column title="PParty" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isPrivateParty == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=PP"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Account" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isAccount == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=AC"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Agent" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isAgent == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=AG"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Carrier" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isCarrier == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=CR"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Vendor" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isVendor == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=VN"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>
	<display:column title="Driver" style="width:45px;font-size: 9px;" >
		<c:if test="${partnerList.isOwnerOp == true}">  
    		<a href="editPartnerAddForm.html?id=${partnerList.id}&partnerType=OO"><img align="left" style="margin: 0px 0px 0px 2px;" src="images/edit.gif"/>Edit</a>
    	</c:if>
	</display:column>			     
	
	<display:column property="countryName" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
	<display:column property="stateName" sortable="true" titleKey="partner.billingState" style="width:65px"/>
	<display:column property="cityName" sortable="true" titleKey="partner.billingCity" style="width:150px"/>
    <display:column property="status" sortable="true" titleKey="partner.status" style="width:100px"/>
    
	
    <c:if test="${param.popup}">
    	<display:column style="width:105px;cursor:pointer;"><A onclick="location.href='<c:url value="/viewPartner.html?id=${partnerList.id}&partnerType=${partnerType}&type=ZZ&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"/>'">View Detail</A></display:column>
    </c:if>
    
    <display:setProperty name="paging.banner.item_name" value="partner"/>   
    <display:setProperty name="paging.banner.items_name" value="partners"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
</div>
<table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="padding:1px;width: 100%;">
	<tbody>
		<s:form id="addPartner" action="editPartnerAddForm.html" method="post" >  
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">Add New Partner :
					<s:radio name="partnerType" list="%{actionType}" onclick="activBtn();"  />
				   	<s:submit name="addBtn" cssClass="cssbutton" cssStyle="width:85px; height:25px; margin-left:50px;" align="top" value=" Add Partner"/>   
				</td>
			</tr>	
		</s:form>
	</tbody>
</table>
<c:if test="${not empty param.popup && partnerType == 'PP' && (flag==0 || flag==1 || flag==2 || flag==3 || flag==4)}">  
<td>&nbsp;&nbsp;&nbsp;&nbsp;</td><c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:set var="isTrue" value="false" scope="session"/>
<script type="text/javascript">
	try{
	document.forms['addPartner'].elements['addBtn'].disabled = true;
	}
	catch(e){}
</script>