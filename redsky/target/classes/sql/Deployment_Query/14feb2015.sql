// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`redskybilling` 
ADD COLUMN `routing` VARCHAR(5) NULL DEFAULT NULL  AFTER `billingRate` ;

ALTER TABLE `redsky`.`container` 
ADD COLUMN `pickupPier` VARCHAR(100) NULL DEFAULT NULL  AFTER `ugwIntId` , 
ADD COLUMN `redeliveryPier` VARCHAR(100) NULL DEFAULT NULL  AFTER `pickupPier` ;

// As per Kundan and giten sir
select concat("update redskybilling set routing='",s.routing,"' where shipnumber='",r.shipnumber,"';")
from serviceorder s,
redskybilling r where s.shipnumber=r.shipnumber;
//

ALTER TABLE `redsky`.`dspdetails`
 ADD COLUMN `resendEmailServiceName` VARCHAR(250) AFTER `WOP_emailSent`;

//#10203
ALTER TABLE `redsky`.`sqlextract` MODIFY COLUMN `groupBy` VARCHAR(100) DEFAULT NULL;
//

ALTER TABLE `redsky`.`claim` ADD COLUMN `assistanceRequired` BIt(1) DEFAULT b'0';

//move to prod as per sangeeta mam
ALTER TABLE `redsky`.`accessinfo` 
ADD COLUMN `inventory` BIT(1) NULL DEFAULT b'0'  AFTER `customerFileId` , 
ADD COLUMN `labels` BIT(1) NULL DEFAULT b'0'  AFTER `inventory` , 
ADD COLUMN `typeofPacking` BIT(1) NULL DEFAULT b'0'  AFTER `labels` , 
ADD COLUMN `packMaterials` BIT(1) NULL DEFAULT b'0'  AFTER `typeofPacking` , 
ADD COLUMN `tools` BIT(1) NULL DEFAULT b'0'  AFTER `packMaterials` , 
ADD COLUMN `piano` BIT(1) NULL DEFAULT b'0'  AFTER `tools` , 
ADD COLUMN `carMotorbike` BIT(1) NULL DEFAULT b'0'  AFTER `piano` , 
ADD COLUMN `crating` BIT(1) NULL DEFAULT b'0'  AFTER `carMotorbike` , 
ADD COLUMN `remarks` MEDIUMTEXT NULL DEFAULT NULL  AFTER `crating` ;
//

ALTER TABLE `redsky`.`container` 
CHANGE COLUMN `pickupPier` `pickupPier` MEDIUMTEXT NULL DEFAULT NULL  , 
CHANGE COLUMN `redeliveryPier` `redeliveryPier` MEDIUMTEXT NULL DEFAULT NULL  ;

ALTER TABLE `redsky`.`trackingstatus` 
ADD COLUMN `tmss` VARCHAR(45) NULL DEFAULT NULL  AFTER `subDestinationAgentPhoneNumber` ;

Insert into refmaster (corpid,code,description,fieldlength,parameter,bucket2,status,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'BrokerAgent', 'Broker Agent', 20, 'NOTESUBTYPE','Service Order','Active','en',now(),'kkunal',now(),'kkunal');

Insert into refmaster (corpid,code,description,fieldlength,parameter,bucket2,status,language,createdon,createdby,updatedon,updatedby)values
('TSFT', 'ForwarderAgent', 'Forwarder Agent', 20, 'NOTESUBTYPE','Service Order','Active','en',now(),'kkunal',now(),'kkunal');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('TrackingStatus','serviceCompleteDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM/DMM');





