package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Partner;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.model.PartnerRateGridDetails;
import com.trilasoft.app.model.PartnerRateGridTemplate;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.PartnerRateGridDetailsManager;
import com.trilasoft.app.service.PartnerRateGridManager;
import com.trilasoft.app.service.RefMasterManager;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.HttpServletResponse;
import java.util.Properties;

public class PartnerRateGridAction extends BaseAction implements Preparable{ 
	 
	 private Long id;
	 private Long partnerId;
	 private String sessionCorpID;
	 private String userType;
	 private PartnerRateGrid partnerRateGrid;
	 //private PartnerPublic partner;
	 private PartnerPublicManager partnerPublicManager;
	 private Partner partner;
	 private PartnerManager partnerManager;
	 private PartnerPublic partnerPublic;
	 private PartnerRateGridManager  partnerRateGridManager;
	 private RefMasterManager refMasterManager;
	 private List partnerRatesGrids= new ArrayList();
	 private List  partnerRateGridTemplates;
	 private PartnerRateGridTemplate partnerRateGridTemplate;
	 private PartnerRateGridDetails partnerRateGridDetails;
	 private List  partnerRateGridDetailsList;
	 private List partnerRateGridlabelList = new ArrayList();
	 private  List multipleTariffScope= new ArrayList();
	 private String showRateGrig;
	 private PartnerRateGridDetailsManager  partnerRateGridDetailsManager;
	 private HashMap <String, Set> partnerRateGridWeightMap = new LinkedHashMap<String, Set>();
	 private HashMap <String, Set> partnerRateGridChargesMap = new LinkedHashMap<String, Set>();
	 private HashMap <String, Set> partnerRateGridHaulingMap = new LinkedHashMap<String, Set>();
	 private  Map<String,String>currencyList;
	 private  Map<String,String>basisList;
	 private  Map<String,String> tariffApplicabilityList;
	 private  Map<String,String> sit_storage_periodList;
	 private  Map<String,String> stateList;
	 private  Map<String,String> tariffScopeList;
	 private String standardOriginNote;
	 private String standardDestinationNote;
	 private String rateGridRules;
	 private String status;
	 private  String baseCurrency;
	 private int originSubmitCount;
	 private int destinationSubmitCount;
	 private int uniqueCount;
	 private String publishTariff;
	 private String  standardCountryHauling;
	 private String  standardCountryCharges;
	 private String userEmail;
	 private String emailMessage;
	 private String requestWithdrawalButton;
	 private String hitFlag;
	 private EmailSetupManager  emailSetupManager;
	
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	 public String getComboList(String corpID){
		 currencyList=refMasterManager.findCodeOnleByParameter(corpID, "CURRENCY");
		 basisList=refMasterManager.findByParameter(corpID, "RATEGRID_BASIS");
		 tariffApplicabilityList=refMasterManager.findByParameter(corpID, "RATEGRID_TARIFFAPP");
		 sit_storage_periodList=refMasterManager.findByParameter(corpID, "STORAGE_PERIOD");
		 tariffScopeList=refMasterManager.findByParameter(corpID, "TARIFFSCOPE");
		 stateList = new HashMap<String, String>();
		 List standardOriginNoteList=partnerRateGridManager.getStandardInclusionsNote(corpID);
		 if(standardOriginNoteList!=null && (!standardOriginNoteList.isEmpty())&& (standardOriginNoteList.get(0)!=null)){
			 standardOriginNote= standardOriginNoteList.get(0).toString();
		 }
		 List standardDestinationNoteList=partnerRateGridManager.getStandardDestinationNote(corpID);
		 if(standardDestinationNoteList!=null && (!standardDestinationNoteList.isEmpty()) && (standardDestinationNoteList.get(0)!=null)){
			 standardDestinationNote= standardDestinationNoteList.get(0).toString();
		 }
		 List standardCountryHaulingList=partnerRateGridManager.getStandardCountryHauling(corpID);
		 if(standardCountryHaulingList!=null && (!standardCountryHaulingList.isEmpty()) && (standardCountryHaulingList.get(0)!=null)){
			 standardCountryHauling= standardCountryHaulingList.get(0).toString();
		 }
		 List standardCountryChargesList=partnerRateGridManager.getStandardCountryCharges(corpID);
		 if(standardCountryChargesList!=null && (!standardCountryChargesList.isEmpty()) && (standardCountryChargesList.get(0)!=null)){
			 standardCountryCharges= standardCountryChargesList.get(0).toString();
		 }
		 return SUCCESS;
	 }
	 
	 	public String rateGridRules(){
		 List rateGridRulesList=partnerRateGridManager.getRateGridRules(sessionCorpID);
		 if(rateGridRulesList!=null && (!rateGridRulesList.isEmpty())&& (rateGridRulesList.get(0)!=null)){
			 rateGridRules= rateGridRulesList.get(0).toString();
		 }
		 return SUCCESS; 
	 }
	 	
	 	public String requestWithdrawal(){
	 		partnerRateGrid = partnerRateGridManager.get(id);
	 		return SUCCESS; 
	 	}
	 	public String sendRequestWithdrawal() {
	 		partnerRateGrid = partnerRateGridManager.get(id);
	 		try{
	 		
	 		String from = userEmail;

	 		String subject="";
	 		String mailTo="redsky@sscw.com";
			String tempRecipient="";
			String tempRecipientArr[]=mailTo.split(",");
			for(String str1:tempRecipientArr){
				if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
					if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
					tempRecipient += str1;
				}
			}
			mailTo=tempRecipient;
			subject="Agent " + getRequest().getRemoteUser() + " wishes to Withdraw their tariff (Tariff # "+partnerRateGrid.getId()+" )";
	 		
			SimpleDateFormat format1 = new SimpleDateFormat("dd-MMM-yyyy");
		    StringBuilder systemDate1 = new StringBuilder(format1.format(partnerRateGrid.getEffectiveDate()));
			String messageText1 = "Agent " + getRequest().getRemoteUser() + " wishes to Withdraw their tariff # "+partnerRateGrid.getId()+" for ";
	 		messageText1=messageText1+ "\n\n Tariff Applicability: "+partnerRateGrid.getTariffApplicability()+"\n Metro City:            "+partnerRateGrid.getMetroCity()+"\n Effective Date:       "+systemDate1.toString();
	 		messageText1=messageText1+ "\n\n Reason: "+"\n\n "+emailMessage;
	 		messageText1=messageText1+ "\n\n\n\n Regards,"+"\n\n "+partnerRateGrid.getPartnerName();
	 
	 		emailSetupManager.globalEmailSetupProcess(from, mailTo, "", "", "", messageText1, subject, sessionCorpID,"","","");
	 		requestWithdrawalButton="OK";
	 		}catch (Exception ex) {
	 			ex.printStackTrace();
	 			requestWithdrawalButton="NOTOK";
			}
	 		return SUCCESS;	
	 	}
	public void prepare() throws Exception {
		getComboList(sessionCorpID);
	}
	public PartnerRateGridAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		userEmail=user.getEmail();
		this.userType =user.getUserType();
		this.sessionCorpID = user.getCorpID(); 
		
	}
	public String partnerRateMatrixList(){
		//getComboList(sessionCorpID);
		String  partnerCode="";
		String partnerName="";
		String terminalCountry="";
		String tariffApplicability="";
		String status="";
		boolean publishTariff=false;
		partnerRateGrid =new PartnerRateGrid();
		partnerRateGrid.setPublishTariff(true);
		partnerRatesGrids=new ArrayList();
		String key = "Please enter your search criteria below";
		saveMessage(getText(key));
		//partnerRatesGrids=partnerRateGridManager.findPartnerRateMatrix(sessionCorpID,partnerCode,partnerName,terminalCountry,tariffApplicability,status,publishTariff);
		return SUCCESS;	
	}
	
	public String searchPartnerRateMatrix(){
		String  partnerCode="";
		String partnerName="";
		String terminalCountry="";
		String tariffApplicability="";
		String status="";
		boolean publishTariff=false;
		if (partnerRateGrid != null) {
		if (partnerRateGrid.getPartnerCode() == null) {
			partnerCode = "";
		} else {
			partnerCode = partnerRateGrid.getPartnerCode();
		}
		if (partnerRateGrid.getPartnerName() == null) {
			partnerName = "";
		} else {
			partnerName = partnerRateGrid.getPartnerName();
		}
		if (partnerRateGrid.getTerminalCountry() == null) {
			terminalCountry = "";
		} else {
			terminalCountry = partnerRateGrid.getTerminalCountry();
		} 
		if (partnerRateGrid.getTariffApplicability() == null) {
			tariffApplicability = "";
		} else {
			tariffApplicability = partnerRateGrid.getTariffApplicability();
		} 
		if (partnerRateGrid.getStatus() == null) {
			status = "";
		} else {
			status = partnerRateGrid.getStatus();
		}
		// partners = new HashSet(partnerManager.getPartnerPopupList(partnerType,sessionCorpID,lastName,partnerCode,billingCountryCode,billingStateCode));
		
	
		partnerRatesGrids=partnerRateGridManager.findPartnerRateMatrix(sessionCorpID,partnerCode,partnerName,terminalCountry,tariffApplicability,status,partnerRateGrid.isPublishTariff());
		}else{
		partnerRatesGrids=partnerRateGridManager.findPartnerRateMatrix(sessionCorpID,"","","","","",publishTariff);
		}
		return SUCCESS;	
	}
	
	public void updatePublishStatus(){
	int i=	partnerRateGridManager.updatePublisStatus(id,publishTariff);
	}
	
	public String list(){ 
		//getComboList(sessionCorpID);
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID); 
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		partnerRatesGrids=partnerRateGridManager.getPartnerRateGridList(sessionCorpID,partnerPublic.getPartnerCode());  
		return SUCCESS;	
	}
	
	public String listView(){
		//getComboList(sessionCorpID);
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID); 
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		partnerRatesGrids=partnerRateGridManager.getPartnerRateGridListView(sessionCorpID,partnerPublic.getPartnerCode());  
		return SUCCESS;	
	}
	
	public String edit() {
		//getComboList(sessionCorpID);
		baseCurrency=partnerRateGridManager.searchBaseCurrency(sessionCorpID).get(0).toString();
		if(partnerId==null || partnerId.toString().equals(""))
		{   
			partnerRateGrid = partnerRateGridManager.get(id);
		    partnerId=	Long.parseLong(partnerPublicManager.findPartnerId(partnerRateGrid.getPartnerCode(),sessionCorpID).get(0).toString());
		}
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID); 
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		if (id != null)
	     { 
			partnerRateGrid = partnerRateGridManager.get(id);
			multipleTariffScope=new ArrayList(); 
			String TariffScope=partnerRateGrid.getTariffScope();
			if(TariffScope==null){
				TariffScope="";
			}
			String[] arrayTariffScope = TariffScope.split(",");
			int arrayLength = arrayTariffScope.length;
			for (int i = 0; i < arrayLength; i++) { 
				multipleTariffScope.add(arrayTariffScope[i].trim()); 
			}
			 partnerRateGridDetailsList= partnerRateGridDetailsManager.getRateGridDetais(id,sessionCorpID);  
			   String label=new String();
			   String Key=new String ();
			   String mapValue=new String(); 
			   Iterator it=partnerRateGridDetailsList.iterator();   
				   while(it.hasNext()){
					   partnerRateGridDetails= (PartnerRateGridDetails) it.next(); 
						   Key= partnerRateGridDetails.getSortRateGrid()+"#"+partnerRateGridDetails.getGridSection()+"#"+partnerRateGridDetails.getLabel()+"#"+partnerRateGridDetails.getParameter();
						   String basis=partnerRateGridDetails.getBasis();
						   if(partnerRateGridDetails.getBasis().toString().equals("")){
							   basis="blank";
						   }
						   if(!partnerRateGridDetails.getValue().toString().equals("")){
						   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+partnerRateGridDetails.getValue()+"#"+basis;
						   }else if(partnerRateGridDetails.getValue().toString().equals("")){
							   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+"0"+"#"+basis;
						   }
						   Set displayList=new TreeSet(); 
						   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
						   {
						      if(partnerRateGridWeightMap.containsKey(Key)){
							     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
							     displayList.add(mapValue);
						      }
						      else {
							   displayList  = new TreeSet();   
							   displayList.add(mapValue);
						      } 
						      partnerRateGridWeightMap.put(Key,displayList);
						   }
						   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("CHARGES"))
						   {
						      if(partnerRateGridChargesMap.containsKey(Key)){
							     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
							     displayList.add(mapValue);
						      }
						      else {
							   displayList  = new TreeSet();   
							   displayList.add(mapValue);
						      } 
						      partnerRateGridChargesMap.put(Key,displayList);
						   }
						   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("HAULING"))
						   {
						      if(partnerRateGridHaulingMap.containsKey(Key)){
							     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
							     displayList.add(mapValue);
						      }
						      else {
							   displayList  = new TreeSet();   
							   displayList.add(mapValue);
						      } 
						      partnerRateGridHaulingMap.put(Key,displayList);
						   }
				   }
	     }else {
	    	 multipleTariffScope=new ArrayList();
	    	 partnerRateGrid=new PartnerRateGrid(); 
	    	 partnerRateGrid.setCreatedOn(new Date());
	    	 partnerRateGrid.setUpdatedOn(new Date());
	    	 partnerRateGrid.setCurrency(baseCurrency);
	    	 if(partnerPublic.getTerminalCountryCode()!=null){
	    	 if(partnerPublic.getTerminalCountryCode().toString().equalsIgnoreCase("USA") || partnerPublic.getTerminalCountryCode().toString().equalsIgnoreCase("IND")  || partnerPublic.getTerminalCountryCode().toString().equalsIgnoreCase("CAN")){
	    		 partnerRateGrid.setState(partnerPublic.getTerminalState()); 
	    	 }
	    	 }
	    	 partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
	 		 java.util.Iterator<PartnerRateGridTemplate> idIterator =  partnerRateGridTemplates.iterator();
	 		 String label=new String();
	 		 String Key=new String ();
	 		 String mapValue=new String();  
	 	     while(idIterator.hasNext()){
	 				 partnerRateGridTemplate= (PartnerRateGridTemplate) idIterator.next(); 
	 			     Key= partnerRateGridTemplate.getSortRateGrid()+"#"+partnerRateGridTemplate.getGridSection()+"#"+partnerRateGridTemplate.getLabel()+"#"+partnerRateGridTemplate.getParameter();
	 			     String basis=partnerRateGridTemplate.getBasis();
					   if(partnerRateGridTemplate.getBasis().toString().equals("")){
						   basis="blank";
					   }
	 			     if(!partnerRateGridTemplate.getDisplay().toString().equals("")){
	 					   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+partnerRateGridTemplate.getDisplay()+"#"+basis;
	 				}else if(partnerRateGridTemplate.getDisplay().toString().equals("")){
	 						   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+"0"+"#"+basis;
	 				}
	 		         Set displayList=new TreeSet();
	 		        if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
					   {
					      if(partnerRateGridWeightMap.containsKey(Key)){
						     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
						     displayList.add(mapValue);
					      }
					      else {
						   displayList  = new TreeSet();   
						   displayList.add(mapValue);
					      } 
					      partnerRateGridWeightMap.put(Key,displayList);
					   }
					   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("CHARGES"))
					   {
					      if(partnerRateGridChargesMap.containsKey(Key)){
						     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
						     displayList.add(mapValue);
					      }
					      else {
						   displayList  = new TreeSet();   
						   displayList.add(mapValue);
					      } 
					      partnerRateGridChargesMap.put(Key,displayList);
					   }
					   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("HAULING"))
					   {
					      if(partnerRateGridHaulingMap.containsKey(Key)){
						     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
						     displayList.add(mapValue);
					      }
					      else {
						   displayList  = new TreeSet();   
						   displayList.add(mapValue);
					      } 
					      partnerRateGridHaulingMap.put(Key,displayList);
					   }
	 				 
	 	    }
	     } 
		return SUCCESS;
	}
	
	public String save(){
		//getComboList(sessionCorpID);
		if(partnerId==null || partnerId.toString().equals(""))
		{   
			partnerRateGrid = partnerRateGridManager.get(id);
			partnerId=	Long .parseLong(partnerPublicManager.findPartnerId(partnerRateGrid.getPartnerCode(),sessionCorpID).get(0).toString());
		}
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID); 
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		if (cancel != null) {
			return "cancel";
		} 
		boolean isNew = (partnerRateGrid.getId() == null);
		if (isNew) {
			try{
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
				String S= df.format(partnerRateGrid.getEffectiveDate()); 
	            Date du = new Date();
	            du = df.parse(S);
	            df = new SimpleDateFormat("yyyy-MM-dd");
	            String effectiveDate = df.format(du); 
			   uniqueCount=partnerRateGridManager.findUniqueCount(sessionCorpID,partnerRateGrid.getPartnerCode(),partnerRateGrid.getTariffApplicability(),partnerRateGrid.getMetroCity(),partnerRateGrid.getServiceRangeMiles(),effectiveDate);
			  if(uniqueCount > 0){
				  multipleTariffScope=new ArrayList(); 
					String TariffScope=partnerRateGrid.getTariffScope();
					if(TariffScope==null){
						TariffScope="";
					}
					String[] arrayTariffScope = TariffScope.split(",");
					int arrayLength = arrayTariffScope.length;
					for (int i = 0; i < arrayLength; i++) { 
						multipleTariffScope.add(arrayTariffScope[i].trim()); 
					}
			    	 partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
			 		 java.util.Iterator<PartnerRateGridTemplate> idIterator =  partnerRateGridTemplates.iterator();
			 		 String label=new String();
			 		 String Key=new String ();
			 		 String mapValue=new String();  
			 	     while(idIterator.hasNext()){
			 				 partnerRateGridTemplate= (PartnerRateGridTemplate) idIterator.next(); 
			 				 String labelMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label");
			 			     Key= partnerRateGridTemplate.getSortRateGrid()+"#"+partnerRateGridTemplate.getGridSection()+"#"+labelMapValue+"#"+partnerRateGridTemplate.getParameter();
			 			     String display=getRequest().getParameter(partnerRateGridTemplate.getGrid() + "_" +partnerRateGridTemplate.getEditable()+"_"+ partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value");
			 			     String basisMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis");
			 			     
			 			     if(!display.toString().equals("")){
			 					   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+display+"#"+basisMapValue;
			 				}else if(partnerRateGridTemplate.getDisplay().toString().equals("")){
			 						   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+"0"+"#"+basisMapValue;
			 				}
			 		         Set displayList=new TreeSet();
			 		        if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
							   {
							      if(partnerRateGridWeightMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridWeightMap.put(Key,displayList);
							   }
							   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("CHARGES"))
							   {
							      if(partnerRateGridChargesMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridChargesMap.put(Key,displayList);
							   }
							   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("HAULING"))
							   {
							      if(partnerRateGridHaulingMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridHaulingMap.put(Key,displayList);
							   } 
			 	    } 
				   String key="This tariff cannot be saved as an existing tariff exists for the effective date and "+partnerRateGrid.getTariffApplicability()+" combination.";
				   errorMessage(getText(key));
				   return SUCCESS;
			  }
			  else{
				    multipleTariffScope=new ArrayList(); 
					String TariffScope=partnerRateGrid.getTariffScope();
					if(TariffScope==null){
						TariffScope="";
					}
					String[] arrayTariffScope = TariffScope.split(",");
					int arrayLength = arrayTariffScope.length;
					for (int i = 0; i < arrayLength; i++) { 
						multipleTariffScope.add(arrayTariffScope[i].trim()); 
					}
				  partnerRateGrid.setCreatedOn(new Date());
				  partnerRateGrid.setUpdatedOn(new Date());
				  partnerRateGrid.setCorpID(partnerPublic.getCorpID());
			      partnerRateGrid.setStatus("New");
			      partnerRateGrid = partnerRateGridManager.save(partnerRateGrid); 
			  }
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		}else{
			multipleTariffScope=new ArrayList(); 
			String TariffScope=partnerRateGrid.getTariffScope();
			if(TariffScope==null){
				TariffScope="";
			}
			String[] arrayTariffScope = TariffScope.split(",");
			int arrayLength = arrayTariffScope.length;
			for (int i = 0; i < arrayLength; i++) { 
				multipleTariffScope.add(arrayTariffScope[i].trim()); 
			}
			partnerRateGrid.setUpdatedOn(new Date());
			partnerRateGrid.setCorpID(partnerPublic.getCorpID());
			partnerRateGrid.setStatus("New");
			partnerRateGrid = partnerRateGridManager.save(partnerRateGrid); 
			
		} 
		long newid=partnerRateGrid.getId();
		List partnerRateGridIdList=partnerRateGridDetailsManager.checkPartnerRateGridId(partnerRateGrid.getId(),sessionCorpID);
		
		if(partnerRateGridIdList==null || partnerRateGridIdList.isEmpty()){
			partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
			java.util.Iterator<PartnerRateGridTemplate> idIterator =  partnerRateGridTemplates.iterator();
			partnerRateGridDetails=new PartnerRateGridDetails();
			   while (idIterator.hasNext()) 
			     {
				   partnerRateGridTemplate = (PartnerRateGridTemplate) idIterator.next();
				   partnerRateGridDetails.setCorpID(partnerRateGridTemplate.getCorpID());  
				   partnerRateGridDetails.setPartnerRateGridID(partnerRateGrid.getId());
				   partnerRateGridDetails.setBasis(partnerRateGridTemplate.getBasis());
				   if(partnerRateGridTemplate.getDisplay().toString().equals("")){
					   partnerRateGridDetails.setValue(new BigDecimal(0.00)); 
				   }else{
				   partnerRateGridDetails.setValue(new BigDecimal(partnerRateGridTemplate.getDisplay()));
				   }
				   partnerRateGridDetails.setGrid(partnerRateGridTemplate.getGrid());
				   partnerRateGridDetails.setGridSection(partnerRateGridTemplate.getGridSection());
				   partnerRateGridDetails.setLabel(partnerRateGridTemplate.getLabel());
				   partnerRateGridDetails.setParameter(partnerRateGridTemplate.getParameter());
				   partnerRateGridDetails.setWeight1(partnerRateGridTemplate.getWeight1());
				   partnerRateGridDetails.setWeight2(partnerRateGridTemplate.getWeight2());
				   partnerRateGridDetails.setSortRateGrid(partnerRateGridTemplate.getSortRateGrid());
				   partnerRateGridDetails.setSortGrid(partnerRateGridTemplate.getSortGrid());
				   partnerRateGridDetails.setEditable(partnerRateGridTemplate.getEditable());
				   partnerRateGridDetailsManager.save(partnerRateGridDetails);
				 } 
		}
		
		HashMap <String, String> saveDataDetailsMap = new LinkedHashMap<String, String>(); 
		HashMap <String, String> saveBasisMap = new LinkedHashMap<String, String>(); 
		HashMap <String, String> saveLabelMap = new LinkedHashMap<String, String>();
		partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
		java.util.Iterator<PartnerRateGridTemplate> templateIterator =  partnerRateGridTemplates.iterator();
		 while(templateIterator.hasNext()){
			 partnerRateGridTemplate= (PartnerRateGridTemplate) templateIterator.next();
			 String key=partnerRateGridTemplate.getGrid() + "_" + partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value";
			 String display=getRequest().getParameter(partnerRateGridTemplate.getGrid() + "_" +partnerRateGridTemplate.getEditable()+"_"+ partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value");
			 saveDataDetailsMap.put(key, display);
			 String basisMapKey=partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis";
			 String basisMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis");
			 saveBasisMap.put(basisMapKey, basisMapValue);
			 String labelMapKey=partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label";
			 String labelMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label");
			 saveLabelMap.put(labelMapKey, labelMapValue);
		 }
		
		partnerRateGridDetailsManager.updateRateGridData(newid,sessionCorpID,saveDataDetailsMap);
		partnerRateGridDetailsManager.updateRateGridBasis(newid,sessionCorpID,saveBasisMap);
		partnerRateGridDetailsManager.updateRateGridLabel(newid,sessionCorpID,saveLabelMap);
		partnerRateGridDetailsList= partnerRateGridDetailsManager.getRateGridDetaisData(newid,sessionCorpID);  
		   String label=new String();
		   String Key=new String ();
		   String mapValue=new String(); 
		   java.util.Iterator<PartnerRateGridDetails> it =  partnerRateGridDetailsList.iterator();
		   //Iterator it=partnerRateGridDetailsList.iterator();   
			   while(it.hasNext()){
				   partnerRateGridDetails= (PartnerRateGridDetails) it.next(); 
				   Key= partnerRateGridDetails.getSortRateGrid()+"#"+partnerRateGridDetails.getGridSection()+"#"+partnerRateGridDetails.getLabel()+"#"+partnerRateGridDetails.getParameter();
                   if(partnerRateGridDetails.getBasis().toString().equals("")){
                	   partnerRateGridDetails.setBasis("blank");
                   }
				   if(!(partnerRateGridDetails.getValue().toString().equals(""))){
				   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+partnerRateGridDetails.getValue()+"#"+partnerRateGridDetails.getBasis();
				   }else if(partnerRateGridDetails.getValue().toString().equals("")){
					   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+"0"+"#"+partnerRateGridDetails.getBasis();
				   }
				   Set displayList=new TreeSet();  
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
				   {
				      if(partnerRateGridWeightMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridWeightMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("CHARGES"))
				   {
				      if(partnerRateGridChargesMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridChargesMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("HAULING"))
				   {
				      if(partnerRateGridHaulingMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridHaulingMap.put(Key,displayList);
				   }
		   }  
		String key = (isNew) ? "partnerRateGrid.added" : "partnerRateGrid.updated";  
		saveMessage(getText(key)); 
		return SUCCESS;
	} 
	
	public String submitRateGrid(){ 
		//getComboList(sessionCorpID);
		if(partnerId==null || partnerId.toString().equals(""))
		{   
			partnerRateGrid = partnerRateGridManager.get(id);
			partnerId=	Long .parseLong(partnerPublicManager.findPartnerId(partnerRateGrid.getPartnerCode(),sessionCorpID).get(0).toString());
		}
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID); 
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		if (cancel != null) {
			return "cancel";
		} 
		boolean isNew = (partnerRateGrid.getId() == null);
		if (isNew) {
			try{
				SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yy");
				String S= df.format(partnerRateGrid.getEffectiveDate()); 
	            Date du = new Date();
	            du = df.parse(S);
	            df = new SimpleDateFormat("yyyy-MM-dd");
	            String effectiveDate = df.format(du); 
			   uniqueCount=partnerRateGridManager.findUniqueCount(sessionCorpID,partnerRateGrid.getPartnerCode(),partnerRateGrid.getTariffApplicability(),partnerRateGrid.getMetroCity(),partnerRateGrid.getServiceRangeMiles(),effectiveDate);
			  if(uniqueCount > 0){
				    multipleTariffScope=new ArrayList(); 
					String TariffScope=partnerRateGrid.getTariffScope();
					if(TariffScope==null){
						TariffScope="";
					}
					String[] arrayTariffScope = TariffScope.split(",");
					int arrayLength = arrayTariffScope.length;
					for (int i = 0; i < arrayLength; i++) { 
						multipleTariffScope.add(arrayTariffScope[i].trim()); 
					}
			    	 partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
			 		 java.util.Iterator<PartnerRateGridTemplate> idIterator =  partnerRateGridTemplates.iterator();
			 		 String label=new String();
			 		 String Key=new String ();
			 		 String mapValue=new String();  
			 	     while(idIterator.hasNext()){
			 				 partnerRateGridTemplate= (PartnerRateGridTemplate) idIterator.next(); 
			 				 String labelMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label");
			 			     Key= partnerRateGridTemplate.getSortRateGrid()+"#"+partnerRateGridTemplate.getGridSection()+"#"+labelMapValue+"#"+partnerRateGridTemplate.getParameter();
			 			     String display=getRequest().getParameter(partnerRateGridTemplate.getGrid() + "_" +partnerRateGridTemplate.getEditable()+"_"+ partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value");
			 			     String basisMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis");
			 			     
			 			     if(!display.toString().equals("")){
			 					   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+display+"#"+basisMapValue;
			 				}else if(partnerRateGridTemplate.getDisplay().toString().equals("")){
			 						   mapValue=partnerRateGridTemplate.getSortGrid()+"#"+partnerRateGridTemplate.getGrid()+"#"+partnerRateGridTemplate.getEditable()+"#"+"0"+"#"+basisMapValue;
			 				}
			 		         Set displayList=new TreeSet();
			 		        if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
							   {
							      if(partnerRateGridWeightMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridWeightMap.put(Key,displayList);
							   }
							   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("CHARGES"))
							   {
							      if(partnerRateGridChargesMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridChargesMap.put(Key,displayList);
							   }
							   if(partnerRateGridTemplate.getGridSection().toString().equalsIgnoreCase("HAULING"))
							   {
							      if(partnerRateGridHaulingMap.containsKey(Key)){
								     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
								     displayList.add(mapValue);
							      }
							      else {
								   displayList  = new TreeSet();   
								   displayList.add(mapValue);
							      } 
							      partnerRateGridHaulingMap.put(Key,displayList);
							   } 
			 	    } 
				   String key="This tariff cannot be saved as an existing tariff exists for the effective date and "+partnerRateGrid.getTariffApplicability()+" combination.";
				   errorMessage(getText(key));
				   partnerRateGrid.setStatus("");
				   return SUCCESS;
			  }
			  else{
				  multipleTariffScope=new ArrayList(); 
					String TariffScope=partnerRateGrid.getTariffScope();
					if(TariffScope==null){
						TariffScope="";
					}
					String[] arrayTariffScope = TariffScope.split(",");
					int arrayLength = arrayTariffScope.length;
					for (int i = 0; i < arrayLength; i++) { 
						multipleTariffScope.add(arrayTariffScope[i].trim()); 
					}
				  partnerRateGrid.setCreatedOn(new Date());
				  partnerRateGrid.setUpdatedOn(new Date());
				  partnerRateGrid.setCorpID(partnerPublic.getCorpID()); 
				  String user = getRequest().getRemoteUser();
				  partnerRateGrid.setConfirmationDate(new Date());
				  partnerRateGrid.setUserConfirming(user);
			      partnerRateGrid = partnerRateGridManager.save(partnerRateGrid); 
			  }
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
		}else{
			multipleTariffScope=new ArrayList(); 
			String TariffScope=partnerRateGrid.getTariffScope();
			if(TariffScope==null){
				TariffScope="";
			}
			String[] arrayTariffScope = TariffScope.split(",");
			int arrayLength = arrayTariffScope.length;
			for (int i = 0; i < arrayLength; i++) { 
				multipleTariffScope.add(arrayTariffScope[i].trim()); 
			}
			partnerRateGrid.setUpdatedOn(new Date());
			partnerRateGrid.setCorpID(partnerPublic.getCorpID()); 
			String user = getRequest().getRemoteUser();
			partnerRateGrid.setConfirmationDate(new Date());
			partnerRateGrid.setUserConfirming(user);
			partnerRateGrid = partnerRateGridManager.save(partnerRateGrid); 
			
		} 
		long newid=partnerRateGrid.getId();
		List partnerRateGridIdList=partnerRateGridDetailsManager.checkPartnerRateGridId(partnerRateGrid.getId(),sessionCorpID);
		
		if(partnerRateGridIdList==null || partnerRateGridIdList.isEmpty()){
			partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
			java.util.Iterator<PartnerRateGridTemplate> idIterator =  partnerRateGridTemplates.iterator();
			partnerRateGridDetails=new PartnerRateGridDetails();
			   while (idIterator.hasNext()) 
			     {
				   partnerRateGridTemplate = (PartnerRateGridTemplate) idIterator.next();
				   partnerRateGridDetails.setCorpID(partnerRateGridTemplate.getCorpID());  
				   partnerRateGridDetails.setPartnerRateGridID(partnerRateGrid.getId());
				   partnerRateGridDetails.setBasis(partnerRateGridTemplate.getBasis());
				   if(partnerRateGridTemplate.getDisplay().toString().equals("")){
					   partnerRateGridDetails.setValue(new BigDecimal(0.00)); 
				   }else{
				   partnerRateGridDetails.setValue(new BigDecimal(partnerRateGridTemplate.getDisplay()));
				   }
				   partnerRateGridDetails.setGrid(partnerRateGridTemplate.getGrid());
				   partnerRateGridDetails.setGridSection(partnerRateGridTemplate.getGridSection());
				   partnerRateGridDetails.setLabel(partnerRateGridTemplate.getLabel());
				   partnerRateGridDetails.setParameter(partnerRateGridTemplate.getParameter());
				   partnerRateGridDetails.setWeight1(partnerRateGridTemplate.getWeight1());
				   partnerRateGridDetails.setWeight2(partnerRateGridTemplate.getWeight2());
				   partnerRateGridDetails.setSortRateGrid(partnerRateGridTemplate.getSortRateGrid());
				   partnerRateGridDetails.setSortGrid(partnerRateGridTemplate.getSortGrid());
				   partnerRateGridDetails.setEditable(partnerRateGridTemplate.getEditable());
				   partnerRateGridDetailsManager.save(partnerRateGridDetails);
				 } 
		}
		
		HashMap <String, String> saveDataDetailsMap = new LinkedHashMap<String, String>(); 
		HashMap <String, String> saveBasisMap = new LinkedHashMap<String, String>(); 
		HashMap <String, String> saveLabelMap = new LinkedHashMap<String, String>();
		partnerRateGridTemplates=partnerRateGridManager.getRateGridTemplates(sessionCorpID);
		java.util.Iterator<PartnerRateGridTemplate> templateIterator =  partnerRateGridTemplates.iterator();
		 while(templateIterator.hasNext()){
			 partnerRateGridTemplate= (PartnerRateGridTemplate) templateIterator.next();
			 String key=partnerRateGridTemplate.getGrid() + "_" + partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value";
			 String display=getRequest().getParameter(partnerRateGridTemplate.getGrid() + "_" +partnerRateGridTemplate.getEditable()+"_"+ partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Value");
			 saveDataDetailsMap.put(key, display);
			 String basisMapKey=partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis";
			 String basisMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Basis");
			 saveBasisMap.put(basisMapKey, basisMapValue);
			 String labelMapKey=partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label";
			 String labelMapValue=getRequest().getParameter(partnerRateGridTemplate.getGridSection() + "_" +partnerRateGridTemplate.getParameter()+ "_Label");
			 saveLabelMap.put(labelMapKey, labelMapValue);
		 }
		
		partnerRateGridDetailsManager.updateRateGridData(newid,sessionCorpID,saveDataDetailsMap);
		partnerRateGridDetailsManager.updateRateGridBasis(newid,sessionCorpID,saveBasisMap);
		partnerRateGridDetailsManager.updateRateGridLabel(newid,sessionCorpID,saveLabelMap);
		partnerRateGridDetailsList= partnerRateGridDetailsManager.getRateGridDetaisData(newid,sessionCorpID);  
		   String label=new String();
		   String Key=new String ();
		   String mapValue=new String(); 
		   java.util.Iterator<PartnerRateGridDetails> it =  partnerRateGridDetailsList.iterator();
		   //Iterator it=partnerRateGridDetailsList.iterator();   
			   while(it.hasNext()){
				   partnerRateGridDetails= (PartnerRateGridDetails) it.next(); 
				   Key= partnerRateGridDetails.getSortRateGrid()+"#"+partnerRateGridDetails.getGridSection()+"#"+partnerRateGridDetails.getLabel()+"#"+partnerRateGridDetails.getParameter();
                   if(partnerRateGridDetails.getBasis().toString().equals("")){
                	   partnerRateGridDetails.setBasis("blank");
                   }
				   if(!(partnerRateGridDetails.getValue().toString().equals(""))){
				   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+partnerRateGridDetails.getValue()+"#"+partnerRateGridDetails.getBasis();
				   }else if(partnerRateGridDetails.getValue().toString().equals("")){
					   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+"0"+"#"+partnerRateGridDetails.getBasis();
				   }
				   Set displayList=new TreeSet();  
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
				   {
				      if(partnerRateGridWeightMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridWeightMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("CHARGES"))
				   {
				      if(partnerRateGridChargesMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridChargesMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("HAULING"))
				   {
				      if(partnerRateGridHaulingMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridHaulingMap.put(Key,displayList);
				   }
		   }
			  
		String key = "Partner Rate Grid has been submit successfully";  
		saveMessage(getText(key)); 
		return SUCCESS;
	
	}
	
	public String  updateRateGridStatus(){
		//getComboList(sessionCorpID);
		if(partnerId==null || partnerId.toString().equals(""))
		{   
			partnerRateGrid = partnerRateGridManager.get(id);
			partnerId=	Long .parseLong(partnerPublicManager.findPartnerId(partnerRateGrid.getPartnerCode(),sessionCorpID).get(0).toString());
		}
		partnerPublic=partnerPublicManager.get(partnerId);
		stateList = partnerRateGridManager.findStateList(partnerPublic.getTerminalCountryCode(), sessionCorpID);  
		String user = getRequest().getRemoteUser();
		int i=partnerRateGridManager.updateRateGridStatus(id,status,sessionCorpID,user);
		partnerRateGrid = partnerRateGridManager.get(id);
		multipleTariffScope=new ArrayList(); 
		String TariffScope=partnerRateGrid.getTariffScope();
		if(TariffScope==null){
			TariffScope="";
		}
		String[] arrayTariffScope = TariffScope.split(",");
		int arrayLength = arrayTariffScope.length;
		for (int j = 0; j < arrayLength; j++) { 
			multipleTariffScope.add(arrayTariffScope[j].trim()); 
		}
		originSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Origin");
		destinationSubmitCount=partnerRateGridManager.findSubmitCount(partnerPublic.getPartnerCode(),sessionCorpID,"Destination");
		partnerRateGrid.setStatus(status);
		partnerRateGridDetailsList= partnerRateGridDetailsManager.getRateGridDetaisData(id,sessionCorpID);  
		   String label=new String();
		   String Key=new String ();
		   String mapValue=new String(); 
		   java.util.Iterator<PartnerRateGridDetails> it =  partnerRateGridDetailsList.iterator();
		   //Iterator it=partnerRateGridDetailsList.iterator();   
			   while(it.hasNext()){
				   partnerRateGridDetails= (PartnerRateGridDetails) it.next(); 
				   Key= partnerRateGridDetails.getSortRateGrid()+"#"+partnerRateGridDetails.getGridSection()+"#"+partnerRateGridDetails.getLabel()+"#"+partnerRateGridDetails.getParameter();
				   if(partnerRateGridDetails.getBasis().toString().equals("")){
					   partnerRateGridDetails.setBasis("blank");
				   }
				   if(!(partnerRateGridDetails.getValue().toString().equals(""))){
				   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+partnerRateGridDetails.getValue()+"#"+partnerRateGridDetails.getBasis();
				   }else if(partnerRateGridDetails.getValue().toString().equals("")){
					   mapValue=partnerRateGridDetails.getSortGrid()+"#"+partnerRateGridDetails.getGrid()+"#"+partnerRateGridDetails.getEditable()+"#"+"0"+"#"+partnerRateGridDetails.getBasis();
				   }
				   Set displayList=new TreeSet();  
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("WEIGHT"))
				   {
				      if(partnerRateGridWeightMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridWeightMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridWeightMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("CHARGES"))
				   {
				      if(partnerRateGridChargesMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridChargesMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridChargesMap.put(Key,displayList);
				   }
				   if(partnerRateGridDetails.getGridSection().toString().equalsIgnoreCase("HAULING"))
				   {
				      if(partnerRateGridHaulingMap.containsKey(Key)){
					     displayList  = (Set)partnerRateGridHaulingMap.get(Key); 
					     displayList.add(mapValue);
				      }
				      else {
					   displayList  = new TreeSet();   
					   displayList.add(mapValue);
				      } 
				      partnerRateGridHaulingMap.put(Key,displayList);
				   }
		   }
		return SUCCESS;
	}
	
	public String deleteRateGrid(){
		partnerPublic=partnerPublicManager.get(partnerId);
		try{
			partnerRateGridManager.remove(id);
			
			hitFlag="1";
			//list();
			}catch(Exception ex)
			{
				ex.printStackTrace();
				//list();
			}
		return SUCCESS;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public PartnerRateGrid getPartnerRateGrid() {
		return partnerRateGrid;
	}


	public void setPartnerRateGrid(PartnerRateGrid partnerRateGrid) {
		this.partnerRateGrid = partnerRateGrid;
	}


	public void setPartnerRateGridManager(
			PartnerRateGridManager partnerRateGridManager) {
		this.partnerRateGridManager = partnerRateGridManager;
	}


	public Long getPartnerId() {
		return partnerId;
	}


	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}

	public Partner getPartner() {
		return partner;
	}
	
	public void setPartner(Partner partner) {
		this.partner = partner;
	}

	public List getPartnerRatesGrids() {
		return partnerRatesGrids;
	}
	public void setPartnerRatesGrids(List partnerRatesGrids) {
		this.partnerRatesGrids = partnerRatesGrids;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getPartnerRateGridTemplates() {
		return partnerRateGridTemplates;
	}
	public void setPartnerRateGridTemplates(List partnerRateGridTemplates) {
		this.partnerRateGridTemplates = partnerRateGridTemplates;
	}
	public void setPartnerRateGridDetailsManager(
			PartnerRateGridDetailsManager partnerRateGridDetailsManager) {
		this.partnerRateGridDetailsManager = partnerRateGridDetailsManager;
	}
	public List getPartnerRateGridDetailsList() {
		return partnerRateGridDetailsList;
	}
	public void setPartnerRateGridDetailsList(List partnerRateGridDetailsList) {
		this.partnerRateGridDetailsList = partnerRateGridDetailsList;
	}
	public List getPartnerRateGridlabelList() {
		return partnerRateGridlabelList;
	}
	public void setPartnerRateGridlabelList(List partnerRateGridlabelList) {
		this.partnerRateGridlabelList = partnerRateGridlabelList;
	}
	public String getShowRateGrig() {
		return showRateGrig;
	}
	public void setShowRateGrig(String showRateGrig) {
		this.showRateGrig = showRateGrig;
	} 
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getCurrencyList() {
		return currencyList;
	}
	public void setCurrencyList(Map<String, String> currencyList) {
		this.currencyList = currencyList;
	}
	public Map<String, String> getBasisList() {
		return basisList;
	}
	public void setBasisList(Map<String, String> basisList) {
		this.basisList = basisList;
	}
	public Map<String, String> getTariffApplicabilityList() {
		return tariffApplicabilityList;
	}
	public void setTariffApplicabilityList(
			Map<String, String> tariffApplicabilityList) {
		this.tariffApplicabilityList = tariffApplicabilityList;
	}
	public Map<String, String> getSit_storage_periodList() {
		return sit_storage_periodList;
	}
	public void setSit_storage_periodList(Map<String, String> sit_storage_periodList) {
		this.sit_storage_periodList = sit_storage_periodList;
	}
	public Map<String, String> getStateList() {
		return stateList;
	}
	public void setStateList(Map<String, String> stateList) {
		this.stateList = stateList;
	}
	public HashMap<String, Set> getPartnerRateGridWeightMap() {
		return partnerRateGridWeightMap;
	}
	public void setPartnerRateGridWeightMap(
			HashMap<String, Set> partnerRateGridWeightMap) {
		this.partnerRateGridWeightMap = partnerRateGridWeightMap;
	}
	public HashMap<String, Set> getPartnerRateGridChargesMap() {
		return partnerRateGridChargesMap;
	}
	public void setPartnerRateGridChargesMap(
			HashMap<String, Set> partnerRateGridChargesMap) {
		this.partnerRateGridChargesMap = partnerRateGridChargesMap;
	}
	public HashMap<String, Set> getPartnerRateGridHaulingMap() {
		return partnerRateGridHaulingMap;
	}
	public void setPartnerRateGridHaulingMap(
			HashMap<String, Set> partnerRateGridHaulingMap) {
		this.partnerRateGridHaulingMap = partnerRateGridHaulingMap;
	}
	public String getStandardOriginNote() {
		return standardOriginNote;
	}
	public void setStandardOriginNote(String standardOriginNote) {
		this.standardOriginNote = standardOriginNote;
	}
	public String getStandardDestinationNote() {
		return standardDestinationNote;
	}
	public void setStandardDestinationNote(String standardDestinationNote) {
		this.standardDestinationNote = standardDestinationNote;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	} 
	public int getOriginSubmitCount() {
		return originSubmitCount;
	}
	public void setOriginSubmitCount(int originSubmitCount) {
		this.originSubmitCount = originSubmitCount;
	}
	public int getDestinationSubmitCount() {
		return destinationSubmitCount;
	}
	public void setDestinationSubmitCount(int destinationSubmitCount) {
		this.destinationSubmitCount = destinationSubmitCount;
	}
	public int getUniqueCount() {
		return uniqueCount;
	}
	public void setUniqueCount(int uniqueCount) {
		this.uniqueCount = uniqueCount;
	}
	public String getRateGridRules() {
		return rateGridRules;
	}
	public void setRateGridRules(String rateGridRules) {
		this.rateGridRules = rateGridRules;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getPublishTariff() {
		return publishTariff;
	}

	public void setPublishTariff(String publishTariff) {
		this.publishTariff = publishTariff;
	}

	public String getStandardCountryHauling() {
		return standardCountryHauling;
	}

	public void setStandardCountryHauling(String standardCountryHauling) {
		this.standardCountryHauling = standardCountryHauling;
	}

	public String getStandardCountryCharges() {
		return standardCountryCharges;
	}

	public void setStandardCountryCharges(String standardCountryCharges) {
		this.standardCountryCharges = standardCountryCharges;
	}

	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}
	
	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public Map<String, String> getTariffScopeList() {
		return tariffScopeList;
	}

	public void setTariffScopeList(Map<String, String> tariffScopeList) {
		this.tariffScopeList = tariffScopeList;
	} 

	public List getMultipleTariffScope() {
		return multipleTariffScope;
	}

	public void setMultipleTariffScope(List multipleTariffScope) {
		this.multipleTariffScope = multipleTariffScope;
	}

	public String getEmailMessage() {
		return emailMessage;
	}

	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	public String getRequestWithdrawalButton() {
		return requestWithdrawalButton;
	}

	public void setRequestWithdrawalButton(String requestWithdrawalButton) {
		this.requestWithdrawalButton = requestWithdrawalButton;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public PartnerPublic getPartnerPublic() {
		return partnerPublic;
	}

	public void setPartnerPublic(PartnerPublic partnerPublic) {
		this.partnerPublic = partnerPublic;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}
	 
	
}


