package com.trilasoft.app.service;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.NetworkDataFields;
public interface NetworkDataFieldsManager extends GenericManager<NetworkDataFields,Long>
{	public List findForCombo1Map();
	public List search(String modelName,String fieldName,String types,String tranType);
	public List getFieldList(String modelName);
	public boolean checkDuplicateRecord(String modelName, String fieldName, String transactionType);
}
