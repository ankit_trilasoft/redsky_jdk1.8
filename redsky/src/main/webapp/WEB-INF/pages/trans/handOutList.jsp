<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@page import="java.util.*"%>
<head>
<title>HandOut List</title>
<meta name="heading" content="HandOut List" />

<script language="javascript" type="text/javascript">

function check(targetElement) {
	      if(targetElement.checked){
			    var userCheckStatus = document.forms['assignItemsForm'].elements['idCheck'].value;
			    if(userCheckStatus == ''){
				  	document.forms['assignItemsForm'].elements['idCheck'].value =targetElement.value ;
		         }else{
			     var userCheckStatus=document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus+','+targetElement.value;
			     document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			      }
			  }
		  
		   if(targetElement.checked==false){ 
			     var userCheckStatus = document.forms['assignItemsForm'].elements['idCheck'].value;
			     var userCheckStatus=document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( targetElement.value , '' );
			     document.forms['assignItemsForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			}
			
			if(document.forms['assignItemsForm'].elements['idCheck'].value==',' || document.forms['assignItemsForm'].elements['idCheck'].value==''){
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = true ;
			}else{
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = false ;
			}	

			if(targetElement.checked){
			      var userCheckStatus = document.forms['assignItemsreForm'].elements['idCheck'].value;
			      if(userCheckStatus == ''){
				  	document.forms['assignItemsreForm'].elements['idCheck'].value =targetElement.value ;
			      }else{
			       	var userCheckStatus=document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus+','+targetElement.value;
			      	document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			      }
			   }
			   
			if(targetElement.checked==false){ 
			     var userCheckStatus = document.forms['assignItemsreForm'].elements['idCheck'].value;
			     var userCheckStatus=document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( targetElement.value , '' );
			     document.forms['assignItemsreForm'].elements['idCheck'].value = userCheckStatus.replace( ',,' , ',' );
			  }
			if(document.forms['assignItemsreForm'].elements['idCheck'].value==',' || document.forms['assignItemsreForm'].elements['idCheck'].value==''){
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = true ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = true ;
			}else{
				document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
			  	document.forms['assignItemsreForm'].elements['forwardBtn1'].disabled = false ;
			}
  }

</script>
</head>

<s:form id="handOutForm" action="" method="post">
<div id="Layer1" style="width:100%">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="wtktServiceId" value="${wtktServiceId}"/>	
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			<td>
       <div id="otabs">
		  <ul>
		    <li><a class="current"><span>HandOut List</span></a></li>
		  </ul>
		</div>
        <s:set name="storages" value="storages" scope="request" />
			<display:table name="storages" class="table" requestURI="" id="storageList" pagesize="10">				
				<display:column><div style="text-align:center"><s:checkbox id="dd" name="dd" fieldValue="${storageList.id}" onclick="check(this);" /></div></display:column>
				<display:column property="ticket" sortable="true" titleKey="storage.ticket" />
				<display:column property="description" sortable="true" titleKey="storage.description" maxLength="15"/>
		 		<display:column property="locationId" sortable="true" titleKey="storage.locationId" />
				<display:column property="storageId" sortable="true" title="Storage Id" />				
				<display:column property="releaseDate" sortable="true" titleKey="storage.releaseDate" style="width:100px"  format="{0,date,dd-MMM-yyyy}"/>
				<display:column property="containerId" sortable="true" titleKey="storage.containerId" />
				<display:column property="itemTag" sortable="true" titleKey="storage.itemTag"  style="width:20px"/>
				<display:column property="model" sortable="true" titleKey="storage.model" style="width:20px" />
				<display:column property="serial" sortable="true" titleKey="storage.serial" style="width:20px" />
				<display:column property="pieces" sortable="true" titleKey="storage.pieces" style="width:20px" />
				<display:column property="volume" sortable="true" title="Volume" style="width:20px" />
				<display:column property="volUnit" sortable="true" title="VolUnit" style="width:20px"/>
												
				<display:setProperty name="paging.banner.item_name" value="storage" />
				<display:setProperty name="paging.banner.items_name" value="storage" />				
				<display:setProperty name="export.excel.filename" value="Storage List.xls" />
				<display:setProperty name="export.csv.filename" value="Storage List.csv" />
				<display:setProperty name="export.pdf.filename" value="Storage List.pdf" />
	       </display:table>

			</td>
		</tr>
	</tbody>
</table>
</div>   	

</s:form>
<table>
<tr>
	<td>
		<s:form id="assignItemsForm" action="handOutRelease.html?actual=''&wtktServiceId=${wtktServiceId}&ticket=${ticket}" method="post">
			<s:hidden name="idCheck" /> 
			<input type="submit" class="cssbutton1" name="forwardBtn"  disabled value="Release" style="width:55px; height:25px"  />			
			<c:if test="${hitFlag == 1}" >
				<c:redirect url="/handOut.html"  />
			</c:if>
			<c:if test="${hitFlag == 0}" >
				<c:redirect url="/handOutList.html?ticket=${ticket}&wtktServiceId=${wtktServiceId}"  />
			</c:if>	
		</s:form>
	</td>
	<td>
		<s:form id="assignItemsreForm" action="handOutRelease.html?actual=actualise&wtktServiceId=${wtktServiceId}&ticket=${ticket}" method="post">
			<s:hidden name="idCheck" /> 
			<input type="submit" class="cssbutton1" name="forwardBtn1"  disabled value="Release with actualize" style="width:155px; height:25px"  />			
			<c:if test="${hitFlag == 1}" >
				<c:redirect url="/handOut.html"  />
			</c:if>
			<c:if test="${hitFlag == 0}" >
				<c:redirect url="/handOutList.html?ticket=${ticket}&wtktServiceId=${wtktServiceId}"  />
			</c:if>				
		</s:form>
	</td>
</tr>
</table>
