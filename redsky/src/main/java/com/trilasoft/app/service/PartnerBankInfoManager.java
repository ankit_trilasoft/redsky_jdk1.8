package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PartnerBankInformation;

public interface PartnerBankInfoManager extends GenericManager<PartnerBankInformation,Long>{
	
	public List findBankData(String paetnercode, String sessionCorpID);
	

}
