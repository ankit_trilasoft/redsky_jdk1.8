<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Find Client Auth#s</title>   
    <meta name="heading" content="Find Client Auth#s"/> 
 <script language="javascript" type="text/javascript">
  function clear_fields(){
  			    document.forms['searchForm'].elements['billing.billToAuthority'].value = "";
			    document.forms['searchForm'].elements['billing.billToReference'].value = "";
		        document.forms['searchForm'].elements['billing.billingId'].value = "";
		        document.forms['searchForm'].elements['billing.billingIdOut'].value = "";
				
				}
</script>     
</head>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-20px;
padding:2px;
text-align:right;
width:100%;
}

div.error, span.error, li.error, div.message {
width:450px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;
!margin:0px;

}
</style>   
<s:form id="searchForm" name="searchForm" action="searchClientAuthNosList" method="post" validate="true" >
<c:set var="newAccountline" value="N" />
<sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
<c:set var="newAccountline" value="Y" />
</sec-auth:authComponent>
<div id="layer4" style="width:100%;">
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="searchClientAuthNosList" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/> 
</c:set>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 10px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
		<table class="table" style="width:100%"  >
		<thead>
		<tr>
		<th>Auth#</th>
		<th>A/C Ref#</th>
		<th><fmt:message key="billing.billingId"/></th>
		<th>Billing ID Out</th>
		</tr></thead>	
				<tbody>
				<tr>
				<td width="" align="left">
					    <s:textfield name="billing.billToAuthority" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="billing.billToReference" required="true" cssClass="input-text" size="30"/>
					</td>
					
					<td width="" align="left">
					    <s:textfield name="billing.billingId" required="true" cssClass="input-text" size="30"/>
					</td>
					<td width="" align="left">
					    <s:textfield name="billing.billingIdOut" required="true" cssClass="input-text" size="30"/>
					</td>
					</tr>
					<tr>
					<td colspan="3"></td>
					<td width="" align="center" style="border-left: hidden;">
					    <c:out value="${searchbuttons}" escapeXml="false" />   
					</td>
				</tr>
				</tbody>
			</table>
			</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
			
			
<div id="Layer1" style="width:100%" >
	<div id="otabs" style="margin-top:-15px;">
		  <ul>
		    <li><a class="current"><span>Find Client Auth#s List</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div> 
<s:set name="findClientAuthNos" value="findClientAuthNos" scope="request"/>
<display:table name="findClientAuthNos" class="table" requestURI="" id="findClientAuthNoList" style="width:100%;" defaultsort="1"  pagesize="10" >
    <c:choose>
<c:when test="${newAccountline=='Y'}">
<display:column sortable="true" title="Ship&nbsp;Number" style="width:3%">
	   <a href="pricingList.html?sid=${findClientAuthNoList.id}"><c:out value="${findClientAuthNoList.shipNumber}" /></a>
     </display:column>
</c:when>
<c:otherwise>
     <display:column sortable="true" title="Ship&nbsp;Number" style="width:3%">
	   <a href="accountLineList.html?sid=${findClientAuthNoList.id}"><c:out value="${findClientAuthNoList.shipNumber}" /></a>
     </display:column>
     </c:otherwise>
     </c:choose>
     <display:column property="billToAuthority"  sortable="true" title="Auth#"  style="width:5%"/>
     <display:column property="billToReference" sortable="true" title="A/C&nbsp;Ref#"   style="width:5%"/> 
     <display:column property="billingId" sortable="true" title="Billing&nbsp;ID"  style="width:3%"/>
	 <display:column property="billingIdOut" sortable="true" title="Billing&nbsp;ID&nbsp;Out"  style="width:3%"/>
	 <display:column property="billToCode" sortable="true" title="Bill&nbsp;To&nbsp;Code"  style="width:3%"/>
	 <display:column property="billToName" sortable="true" title="Bill&nbsp;To&nbsp;Name"  style="width:15%"/>
</display:table>
</div>
</div>
</s:form>

<script type="text/javascript"> 
highlightTableRows("findClientAuthNos");  
</script>  
		  	