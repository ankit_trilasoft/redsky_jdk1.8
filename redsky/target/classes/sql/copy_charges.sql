DELIMITER $$

DROP PROCEDURE IF EXISTS `redsky`.`copy_charges` $$
CREATE PROCEDURE `copy_charges`(
from_contract varchar(500),
to_contract varchar(500),
lcorpid varchar(4),
l_createdby varchar(100),
copyAgentAccount varchar(3))
BEGIN
declare msg varchar(50);
set msg='Copy charges Successfully!!!';

delete from charges where contract =to_contract and corpid=lcorpid;
DROP TABLE IF EXISTS charges_temp;
create table charges_temp (select * from charges where contract =from_contract and corpid=lcorpid and status=true);
ALTER TABLE `redsky`.`charges_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update charges_temp set id=null,contract=to_contract,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into charges select * from charges_temp;
drop table charges_temp;

delete from rategrid where contractname=to_contract and corpid=lcorpid;
DROP TABLE IF EXISTS rategrid_temp;
create table rategrid_temp (select * from rategrid where contractname =from_contract and corpid=lcorpid
and charge  in (select id from charges where contract =from_contract and corpid=lcorpid and status=true));
ALTER TABLE `redsky`.`rategrid_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update rategrid_temp set id=null,contractname =to_contract,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into rategrid select * from rategrid_temp;
drop table rategrid_temp;

delete from countrydeviation where contractname =to_contract and corpid=lcorpid ;
DROP TABLE IF EXISTS countrydeviation_temp;
create table countrydeviation_temp (select * from countrydeviation where contractname =from_contract and corpid=lcorpid
and charge  in (select id from charges where contract =from_contract and corpid=lcorpid and status=true));
ALTER TABLE `redsky`.`countrydeviation_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update countrydeviation_temp set id=null,contractname =to_contract,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into countrydeviation select * from countrydeviation_temp;
drop table countrydeviation_temp;

if(copyAgentAccount='Yes') then
delete from contractaccount where contract =to_contract and corpid=lcorpid ;
DROP TABLE IF EXISTS contractaccount_temp;
create table contractaccount_temp (select * from contractaccount where contract =from_contract and corpid=lcorpid);
ALTER TABLE `redsky`.`contractaccount_temp` CHANGE COLUMN `id` `id` BIGINT(20) NULL DEFAULT '0'  ;
update contractaccount_temp set id=null,published=NULL,contract=to_contract,createdby=l_createdby,createdon=now(),updatedby=l_createdby,updatedon=now();
insert into contractaccount select * from contractaccount_temp;
drop table contractaccount_temp;
end if;
call copy_countrydeviation(from_contract, to_contract, lcorpid);

call copy_rategrid(from_contract, to_contract, lcorpid);

select msg;

END $$

DELIMITER ;