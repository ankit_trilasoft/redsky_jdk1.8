<%@ include file="/common/taglibs.jsp"%>

<head>
<title>Survey Details</title>
<STYLE type=text/css>
#bigimageid {font-size: 0.75em;font-family:Arial, Helvetica, sans-serif;position: absolute;visibility: hidden;left: 0px;top: 0px;width: 400px;height: 0px;z-index: 100;
}
input[type=checkbox] {margin:0px;}
.subcontenttabChild {margin:0px;padding:5px 0 0 10px; height:20px;background: url("images/greylinebg.gif") repeat scroll 0 0 #DCDCDC;}
#tt {position:absolute; display:block; background:url(images/tt_left.gif) top left no-repeat; }
#tttop {display:block; height:5px; margin-left:5px; background:url(images/tt_top.gif) top right no-repeat; overflow:hidden}
#ttcont {display:block; padding:2px 12px 3px 7px; margin-left:5px; background:#dcf4fe; color:#000}
#ttbot {display:block; height:5px; margin-left:5px; background:url(images/tt_bottom.gif) top right no-repeat; overflow:hidden}
</STYLE>
<meta name="heading"
	content="Survey Details" />

<script language="javascript" type="text/javascript">
/*
Simple Image Trail script- By JavaScriptKit.com
Visit http://www.javascriptkit.com for this script and more
This notice must stay intact
*/
var offsetfrommouse=[15,15]; //image x,y offsets from cursor position in pixels. Enter 0,0 for no offset
var displayduration=0; //duration in seconds image should remain visible. 0 for always.
var currentimageheight = 240; // maximum image size.
//function linkToImages(id,cid){
//window.open('/imageListUpload.html?cid='+cid+'&id='+id,'forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
//}
<%-- JavaScript code like methods are shifted to Bottom from here, only global variables are left here if found --%>
</script>
	
</head>
<div id="Layer5" style="width:100%">
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<c:if test="${Quote!='y'}">
<s:hidden id="forQuotation" name="forQuotation" value=""/>
</c:if>
<c:if test="${Quote=='y'}">
<s:hidden id="forQuotation" name="forQuotation" value="QC"/>
</c:if>	 
<s:hidden name="fileID"  id= "fileID" value="%{customerFile.id}"/>  
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
 <c:set var="idOfTasks" value="${customerFile.id}" scope="session"/>
  <c:set var="quantitysum" value="0" />
  <c:set var="volumesum" value="0.00" />
  <c:set var="totalsum" value="0.00" />
  <c:set var="tableName" value="customerfile" scope="session"/>
<s:hidden name="SOflag" value="${SOflag}"/>
<c:set var="ppType" value=""/>
<s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <c:set var="custID" value="${customerFile.sequenceNumber}" scope="session"/>
		<div id="newmnav">		  
		  <ul>
		  <c:if test="${Quote!='y'}">
		    <li><a href="editCustomerFile.html?id=<%=request.getParameter("cid") %>" ><span>Customer File</span></a></li>
		    <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
				<li><a href="customerServiceOrders.html?id=<%=request.getParameter("cid") %> " ><span>Service Orders</span></a></li>
			</c:if>
			<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
				<li><a href="customerServiceOrders.html?id=<%=request.getParameter("cid") %> " ><span>Quotes</span></a></li>
			</c:if>
			 <c:if test="${ usertype!='AGENT'}"> <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}&cid=${customerFile.id}" ><span>Account Policy</span></a></li></c:if>
			 <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey Data<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			 <c:if test="${ usertype!='AGENT'}"><li><a onclick="window.open('subModuleReports.html?id=<%=request.getParameter("cid")%>&custID=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=Rate Request&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li></c:if>			
		   </c:if>
		   <c:if test="${Quote=='y'}">
		    <li ><a href="QuotationFileForm.html?from=list&id=<%=request.getParameter("cid") %>"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  	<li><a href="quotationServiceOrders.html?id=<%=request.getParameter("cid") %>&forQuotation=QC"><span>Quotes</span></a></li>		    
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Survey Details</span></a></li>
		     <c:if test="${ usertype!='AGENT'}"><li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=quotation&reportSubModule=quotation&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li></c:if>
             <c:if test="${ usertype!='AGENT'}"><li><a onclick="window.open('auditList.html?id=${customerFile.id}&tableName=customerfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>  </c:if>           
		   </c:if>
		   <sec-auth:authComponent componentId="module.tab.customerFile.auditTab">
		   	    <c:if test="${not empty accessInfo.id}"> 
		    <li>
              <a onclick="window.open('auditList.html?id=${accessInfo.id}&tableName=accessinfo&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')">
              <span>Audit</span></a></li>
		    </c:if>
		    <c:if test="${empty accessInfo.id}">
		    	<li><a><span>Audit</span></a></li>
		    </c:if>
		    </sec-auth:authComponent>
		   	<c:if test="${usertype=='USER'}">
			<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
			<li><a href="findEmailSetupTemplateByModuleNameCF.html?cid=${customerFile.id}"><span>View Emails</span></a></li>
			</configByCorp:fieldVisibility>
		  	</c:if>
		</ul>
		</div><div class="spn">&nbsp;</div> 
			<div style="padding-bottom:0px;"></div>		
	<div id="content" align="center" >
  <div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
	<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:100%">
		<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="5" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" onfocus="onLoad();"/></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" cssStyle="width:150px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.originCountryCode" required="true" cssStyle="width:60px;" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" cssStyle="width:150px;" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" cssStyle="width:120px;" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox"></td>
						<td align="right" class="listwhitebox"></td>
						<td align="right" class="listwhitebox"></td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	</tbody>
	</table>
	</div>
	
<div class="bottom-header"><span></span></div>


</div>
</div> </div>
<div style="clear:both;"></div>
       <div onClick="javascript:animatedcollapse.toggle('accessinfo')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Access Info</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div> 
        <div id="accessinfo">
        <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Origin</b></font></div>
        <s:form method="post" action="saveAccessInfo.html?&Quote=${Quote}"  name="accessInfoForm">
        
          	  <c:if test="${empty customerFile.id}">
				<c:set var="isTrue" value="false" scope="request" />
			    </c:if> 
			    <c:if test="${not empty customerFile.id}">
				<c:set var="isTrue" value="true" scope="request" />
			    </c:if>        
        <s:hidden name="countdown"  value="480"/>
        <s:hidden name="cid" value="${cid}"/>
         <s:hidden name="accessInfo.customerFileId" value="${cid}"/>         
         <s:hidden name="accessInfo.corpID" value="${customerFile.corpID}"/>
         <s:hidden name="aid" value="${accessInfo.id}"/>
         <s:hidden name="accessInfo.id" value="${accessInfo.id}"/>
         <s:hidden id="countSurveyNotes" name="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>"/>
         <c:set var="countSurveyNotes" value="<%=request.getParameter("countSurveyNotes") %>" />
         <s:hidden name="accessInfo.networkSynchedId" />
         <s:hidden name="accessInfo.createdBy" />
         <fmt:formatDate var="accessInfoCreatedOnFormattedValue" value="${accessInfo.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
         <s:hidden name="accessInfo.createdOn"  value="${accessInfoCreatedOnFormattedValue}" />
         
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
           <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:100px;"  ><s:select name="accessInfo.originResidenceType"  value="%{accessInfo.originResidenceType}" list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right"  width="37px" >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.originResidenceSize"   cssStyle="width:100px;" /></td>	  	      
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>
	  	  <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   
	  	   <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.originLongCarry" value="${accessInfo.originLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px" key="accessInfo.originCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="240px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.originStairCarry" value="${accessInfo.originStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px"  key="accessInfo.originStairDistance"/></td>
	  	          
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','origin')"/></td>
 	        
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right" >Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.originStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" colspan="2" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originNeedCrane" value="${accessInfo.originNeedCrane}" fieldValue="true"/></td>
	  	 		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.originComment" cssClass="input-text" cssStyle="width:240px;height:70px;" onkeydown="limitText(this,this.form.countdown,1950);" 
                     onkeyup="limitText(this,this.form.countdown,1950);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	 
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originElevator" value="${accessInfo.originElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.originExternalElevator" value="${accessInfo.originExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originElevatorType"  list="%{elevatorType}"  cssStyle="width:155px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.originReserveParking" value="${accessInfo.originReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.originParkingType"  list="%{parkingType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="145px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originDistanceToParking"/></td>
	  	  </tr>
	  	  <tr><td colspan="10"></td></tr> 
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="110px;">Shuttle Required</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.originShuttleRequired" value="${accessInfo.originShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.originShuttleDistance"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.originAdditionalStopRequired" value="${accessInfo.originAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.originAdditionalStopDetails"/></td>
	  	   </tr>
	  	    <tr><td colspan="10"></td></tr>
	  	   </table>
      
        <table width="100%" cellspacing="0" cellpadding="0" class="detailTabLabel" style="margin:0px;padding:0px;">
			<tbody>
			<tr></tr><tr><td align="center" class="vertlinedata" colspan="10"></td></tr>
			<tr><td>
			<div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild"><font color="black"><b>&nbsp;Destination</b></font></div>
			</td></tr>
			
			<tr><td class="listwhitetext"></td></tr>
			</tbody>
		</table>
       
        <table width="45%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="2" >
            <tr><td colspan="10" style="height:4px;"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:100px;"  ><s:select name="accessInfo.destinationResidenceType"  list="%{residenseType}"  cssStyle="width:90px; !width:50px;" cssClass="list-menu"  headerKey="" headerValue=""/></td>
	  	   	     <td   colspan="2"></td>
	  	   	     <td  class="listwhitetext" align="right" width="37px" >Size</td>
	  			 <td  width="2px"></td>
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" key="accessInfo.destinationResidenceSize" cssStyle="width:100px;" /></td>
	  	  </tr>
	  	    <tr><td colspan="10" style="height:4px;"></td></tr>
	  	  </table>	
	  	    <div style="width:99%" background="images/greyband.gif" height="20px" class="subcontenttabChild">	
	  	  <div style="width:49%;float:left">Carry Details</div>
	  	  <div style="width:48%;float:left;padding-left:28px;">Stair Details</div>
	  	   </div>
	  	   <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	      <tr><td colspan="10"></td></tr>
          <tr>
                 <td  class="listwhitetext" align="right"  width="110px;">Long Carry</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"  ><s:checkbox name="accessInfo.destinationLongCarry" value="${accessInfo.destinationLongCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;" >Carry Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.destinationCarryDistance"/></td>
	  			  <td  class="listwhitetext" align="right"  width="240px;">Stair Carry</td>
	  			 	  			
	  			 <td align="left" class="listwhitetext" style="width:10px;"><s:checkbox name="accessInfo.destinationStairCarry" value="${accessInfo.destinationStairCarry}" fieldValue="true"/></td>
	  	   	     <td  class="listwhitetext" align="right"  style="width:90px;">Stair Distance</td>
	  			 
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:135px;" key="accessInfo.destinationStairDistance"/></td>
	  	        	  	        	  		     		  
	  		     <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		     <td  width="2px"><img  src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages('${accessInfo.id}','dest')"/></td>
	  	       
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	   <tr>
                 <td  class="listwhitetext" align="right">Carry Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="3"><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationCarrydetails"/></td>
	  			 
	  			 <td class="listwhitetext" align="right">Stair Details</td>	
	  			 <td align="left" class="listwhitetext" colspan="3" ><s:textfield cssClass="input-text" cssStyle="width:244px" key="accessInfo.destinationStairdetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	  
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Elevator</div>
	  	  
	  	  <table border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Floor</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationfloor"/></td>
	  			 <td  class="listwhitetext" align="right"  width="">Need Crane</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;" colspan="2" ><s:checkbox name="accessInfo.destinationNeedCrane" value="${accessInfo.destinationNeedCrane}" fieldValue="true"/></td>
	  	  		 <td class="listwhitetext" width="60" align="right" rowspan="5">Comment</td>	
	  	 		 <td rowspan="8" align="left" class="listwhitetext">
	  	 		 <s:textarea name="accessInfo.destinationComment" cssClass="input-text" cssStyle="width:240px;height:70px;" onkeydown="limitText(this,this.form.countdown,1950);" 
                     onkeyup="limitText(this,this.form.countdown,1950);" onchange="return wordCount(this);" />
	  	 		 </td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationElevator" value="${accessInfo.destinationElevator}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Elevator Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationElevatorDetails"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">External Elevator</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"><s:checkbox name="accessInfo.destinationExternalElevator" value="${accessInfo.destinationExternalElevator}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Elevator Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationElevatorType"  list="%{elevatorType}"  cssStyle="width:100px; !width:50px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table >
	  	  <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Parking</div>
	  	  
	  	  <table width="" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	     <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right"  width="110px;">Reserve Parking</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:checkbox name="accessInfo.destinationReserveParking" value="${accessInfo.destinationReserveParking}" fieldValue="true"/></td>
	  	  <td  class="listwhitetext" align="right"  width="">Parking&nbsp;Type</td>
	  			 <td  width="2px"></td>	  			
	  			 <td align="left" class="listwhitetext" style="width:120px;"  ><s:select name="accessInfo.destinationParkingType"  list="%{parkingType}"  cssStyle="width:155px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  <tr>
	  	  <td  class="listwhitetext" align="right">Parking Lot Size</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkinglotsize"/></td>
	  	  <td  class="listwhitetext" align="right"  width="110px;" >Number Of Spots</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationParkingSpots"/></td>
	  	  <td  class="listwhitetext" align="right"  width="145px;" >Distance To Parking</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="1"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationDistanceToParking"/></td>
	  	  </tr>
	  	   <tr><td colspan="10"></td></tr>
	  	  </table>
	  	   <div style="width:99%;float:left" background="images/greyband.gif"  class="subcontenttabChild">Stop Details</div>
	  	   
	  	   <table width="100%" border="0" cellpadding="0" class="detailTabLabel" cellspacing="3">
	  	   <tr>
	  	   <td>
	  	     <div style="float:left;">
	  	     <table>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right" width="110px;">Shuttle Required</td>
	  			  			
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.destinationShuttleRequired" value="${accessInfo.destinationShuttleRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Shuttle Distance</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"><s:textfield cssClass="input-textRight" cssStyle="width:150px" key="accessInfo.destinationShuttleDistance"/></td>
	  			
	  	   </tr>
	  	 	<tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Additional Stop</td>
	  						
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.destinationAdditionalStopRequired" value="${accessInfo.destinationAdditionalStopRequired}" fieldValue="true"/></td>
	  			 <td  class="listwhitetext" align="right"  width="110px;" >Details</td>
	  			 <td  width="2px"></td>	
	  			 <td align="left" class="listwhitetext" colspan="2"  ><s:textfield cssClass="input-text" cssStyle="width:150px" key="accessInfo.destinationAdditionalStopDetails"/></td>
	  	   </tr>
	  	   </table>
	  	   </div>
	  	   <div style="float:right;">
	  	   <c:if test="${inventoryPath.path!=null && inventoryPath.path!=''}">
	  	    <img id="userImage" src="UserImage?location=${inventoryPath.path}" alt="" width="150" height="150"  style="border:thin solid #219DD1;" />
	  	    </c:if>
	  	   </div>
	  	   </td>
	  	   </tr>	  	  
	  	   </table>	  	   
	  	
        </div>
        <configByCorp:fieldVisibility componentId="component.field.accessInfo.surveyDetailsForPUTT">
         <div onClick="javascript:animatedcollapse.toggle('surveydetails')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Survey Details</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div>  
        <div style="width:100%; float:left;" id="surveydetails">
         <table>
         <tr><td colspan="10"></td></tr>
         <tr><td colspan="10"></td></tr> 
	  	   <tr>
	  	   <td  class="listwhitetext" align="right" width="110px;">Inventory</td>
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.inventory" value="${accessInfo.inventory}" fieldValue="true"/></td>
	  			  <td  class="listwhitetext" align="right" width="110px;">Labels</td>
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.labels" value="${accessInfo.labels}" fieldValue="true"/></td>
	  			   <td  class="listwhitetext" align="right" width="110px;">Export&nbsp;Wrap</td>
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.typeofPacking" value="${accessInfo.typeofPacking}" fieldValue="true"/></td>
	  			   <td  class="listwhitetext" align="right" width="110px;">Pack Materials</td>
	  			 <td align="left" class="listwhitetext" style="width:150px;"  ><s:checkbox name="accessInfo.packMaterials" value="${accessInfo.packMaterials}" fieldValue="true"/></td>
	  			 
	  	   </tr>
	  	 	<tr><td colspan="10"></td></tr>
	  	 	 <tr><td colspan="10"></td></tr>
	  	   <tr>
	  	   <td  class="listwhitetext" align="right"  width="">Tools</td>
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.tools" value="${accessInfo.tools}" fieldValue="true"/></td>
	  		<td  class="listwhitetext" align="right"  width="">Piano</td>
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.piano" value="${accessInfo.piano}" fieldValue="true"/></td>
	  		
	  		<td  class="listwhitetext" align="right"  width="">Car/ Motorbike</td>
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.carMotorbike" value="${accessInfo.carMotorbike}" fieldValue="true"/></td>
	  		
	  		<td  class="listwhitetext" align="right"  width="">Crating</td>
	  			 <td align="left" class="listwhitetext" style="width:120px;" ><s:checkbox name="accessInfo.crating" value="${accessInfo.crating}" fieldValue="true"/></td>
	  			
	  			</tr>
	  			 <tr><td colspan="10"></td></tr>
	  			  <tr><td colspan="10"></td></tr>
	  			<tr><td  class="listwhitetext" align="right"  width="">Remarks</td>
	  			<input readonly type="hidden" id="countdown" size="3" value="100"> 
	  			<td align="left" colspan="6" class="listwhitetext"><s:textarea name="accessInfo.remarks" id="remarks" rows="1" cols="50" onkeydown="limitText(this.form.remarks,this.form.countdown,100);" onkeyup="limitText(this.form.remarks,this.form.countdown,100);" cssClass="textarea" /></td>
	  			<c:if test="${empty customerFile.id}">
	  										<td width="90px" align="left"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
			</c:if>
				<c:if test="${empty customerFile.id}">
							<td align="right" colspan="20" ><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
							</c:if>
							<c:if test="${not empty customerFile.id}">
							<c:choose>
								<c:when test="${countSurveyNotes == '0' || countSurveyNotes == '' || countSurveyNotes == null}">
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
								<td align="right" colspan="20" ><img id="countSurveyNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${customerFile.id }&notesId=${customerFile.sequenceNumber }&noteFor=CustomerFile&subType=Survey&imageId=countSurveyNotesImage&fieldId=countSurveyNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
							</c:if>	
	  			</tr>
 
		<input readonly type="hidden" id="countdown" size="3" value="99"> 
	  	   </table>
        </div>
      </configByCorp:fieldVisibility>
         </s:form>
        <div style="clear:both;"></div>
        <div onClick="javascript:animatedcollapse.toggle('inventory')" style="margin:0px">
                   <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px"> 
                     <tr>
                     <td class="headtab_left"> </td>
                     <td NOWRAP class="headtab_center">Survey Data</td>
                     <td width="28" valign="top" class="headtab_bg"></td>
                     <td class="headtab_bg_center">&nbsp; </td>
                     <td class="headtab_right"> </td>
                     </tr> 
                   </table>
        </div>  
	
		<div style="width:100%; float:left;" id="inventory">
	     <div id="otabs" style="margin-top: 10px; margin-bottom:0px;">
		  <ul>
		    <li><a class="current"><span>Survey Data</span></a></li>
		    <li><a href="surveyDetailsMaterials.html?cid=${customerFile.id}&Quote=${Quote}"><span>Materials</span></a></li>
		  </ul>
		</div>
       <display:table name="articlesList" class="table" requestURI="" id="articlesList"  pagesize="25" style="width:100%;margin-bottom:5px;" > 
       <display:column property="atricle" style="width:140px;" title="Article" sortable="true" href="editSurveyDetails.html?cid=${customerFile.id}&SOflag=CF&Quote=${Quote}" paramId="id" paramProperty="id" />
          <display:column property="mode" style="width:50px;" sortable="true"/> 
          <display:column property="location" style="width:50px;" sortable="true" title="Location"/>         
           <display:column property="room" style="width:120px;"/>
            <c:if test="${invListflag ne null && invListflag=='1'}">
             <c:set var="quantitysum" value="${articlesList.quantity+quantitysum }" /> 
             </c:if>  
             <display:column property="quantity" style="width:30px;text-align:right;"/>
              <c:if test="${invListflag ne null && invListflag=='1'}">
               <c:set var="volumesum" value="${articlesList.totalWeight+volumesum }" /> 
               </c:if> 
           <display:column property="cft" style="width:30px;text-align:right;" title="Volume"/>
           <c:if test="${invListflag ne null && invListflag=='1'}">
           <c:set var="totalsum" value="${articlesList.total+totalsum }" /> 
           </c:if> 
            <display:column property="total" style="width:120px;text-align:right;" title="Total Volume"/>
            <display:column property="weight" style="width:30px;text-align:right;" title="Weight"/>
             <display:column property="totalWeight"  style="width:120px;text-align:right;" title="Total Weight"/>
            <display:column title="Valuation"  style="width:80px;text-align:right;">
            <c:if test="${articlesList.valuation!='' && articlesList.valuation!=null && articlesList.valuation!='0'}">
            <img  src="${pageContext.request.contextPath}/images/${accCorpID}.gif" style="vertical-align:top;" />${articlesList.valuation}
            </c:if>
            </display:column>
             <display:column title="NTBS"  style="width:30px;text-align:right;">
                   <c:if test="${articlesList.ntbsFlag==true}">         
                   <input type="checkbox"  checked="checked"  disabled="disabled" readonly="readonly"/>
                   </c:if>
                    <c:if test="${articlesList.ntbsFlag==false||articlesList.ntbsFlag==null}">         
                   <input type="checkbox" id="${findSummryViewDocList.id}" disabled="disabled" readonly="readonly"/>
                   </c:if>
             </display:column>
            <display:column property="comment" style="width:150px;" maxLength="25">
             <!--  <c:if test="${articlesList.comment!=''&& articlesList.comment!= null}" >
             <img src="${pageContext.request.contextPath}/images/commentplus.png" alt="" width="20" height="20" onmouseover="tooltip.show('${articlesList.comment} ', 200);" onmouseout="tooltip.hide();"  />
            </c:if>
          <c:if test="${articlesList.comment=='' || articlesList.comment == null}" >
             <img src="${pageContext.request.contextPath}/images/comment.png" alt="" width="20" height="20"  />
            </c:if> -->
             </display:column>
            <display:column  title="Images" style="width:50px;text-align:center;" > 
               <c:if test="${articlesList.imagesAvailablityFlag!=null && articlesList.imagesAvailablityFlag!=''}" >
             <img id="userImage" src="${pageContext.request.contextPath}/images/camera24.png" alt="" width="24" height="24" onclick="imageList('${customerFile.id}','${articlesList.id}',this)" />                             
               </c:if>
           </display:column>
            <display:column title="Remove" style="width: 15px;">
            <c:if test="${usertype == 'AGENT' || (not empty articlesList.id && not empty articlesList.networkSynchedId && articlesList.id != articlesList.networkSynchedId)}">
		 	 	<img  align="middle" title="" onclick="" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/recyle-disable.png"/>
		 	 </c:if> 
		 	  <c:if test="${usertype != 'AGENT' && ( empty articlesList.networkSynchedId || articlesList.id == articlesList.networkSynchedId)}"> 
		 	 	<a>
		 	   		<img  align="middle" title="" onclick="confirmSubmit('${customerFile.id}','${articlesList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/>
	          	</a>
	          </c:if>
	        </display:column>
	         <display:footer>
	           <tr>                     
                 <td colspan="4" align="left"  class="tdheight-footer"></td>                      
                  <td align="right">${quantitysum}</td>
                   <td align="right"> </td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalsum}"  /></td>
                     <td align="right"> </td>
                      <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum}"  /> </td><td></td><td></td><td></td><td></td><td></td>
               </tr> 
                <tr>                     
                 <td colspan="3" align="left"  class="tdheight-footer"></td>
                 <td align="right">Materials&nbsp;Total:</td>                      
                  <td align="right">${quatSum}</td>
                   <td align="right">  </td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totSum}"  /></td>
                     <td align="right"> </td>
                     <td align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volSum}"  /> </td><td></td><td></td><td></td><td></td><td></td>
               </tr> 
                 <tr>                     
                 <td colspan="3" align="left"  class="tdheight-footer"></td>
                 <td align="right">Grand&nbsp;Total:</td>                      
                  <td align="right">${quantitysum+quatSum}</td>
                   <td align="right"> </td>
                   <td  align="right"> <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${totalsum+totSum}"  /></td>
                  <td align="right"> </td>
                   <td align="right">   <fmt:formatNumber type="number"  maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${volumesum+volSum}"  /></td><td></td><td></td><td></td><td></td><td></td>
               </tr>
	         </display:footer> 
       </display:table>     
       
       
       </div>		
</div>

<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="accessInfoCreatedOnFormattedValue" value="${accessInfo.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="accessInfo.createdOn" value="${accessInfoCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${accessInfo.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty accessInfo.id}">
								<s:hidden name="accessInfo.createdBy"/>
								<td><s:label name="createdBy" value="%{accessInfo.createdBy}"/></td>
							</c:if>
							<c:if test="${empty accessInfo.id}">
								<s:hidden name="accessInfo.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="accessInfoUpdatedOnFormattedValue" value="${accessInfo.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="accessInfo.updatedOn" value="${accessInfoUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${accessInfo.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty accessInfo.id}">
							<s:hidden name="accessInfo.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{accessInfo.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty accessInfo.id}">
							<s:hidden name="accessInfo.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
			
			<input id="addInv" type="button"  class="cssbutton" style="width:55px; height:25px;margin-left:25px;"  value="Add" onclick="add();"  />
      <c:if test="${inventoryTransferflag !='N'}">
      <c:if test="${Quote!='y'}">
      <input id="trans" type="button"  Class="cssbutton" Style="width:190px; height:25px;margin-left:25px;"  value="Transfer Survey Data To SO" onclick="InventryTransfer();"  />		
      </c:if>
      <c:if test="${Quote=='y'}">
      <input id="trans1" type="button"  Class="cssbutton" Style="width:190px; height:25px;margin-left:25px;"  value="Transfer Survey Data To Quotes" onclick="InventryTransfer();"  />		
      </c:if>      
      </c:if>
      <!--   <input type="button"  Class="cssbutton" Style="width:175px; height:25px;margin-left:25px;"  value="Read XML's" onclick="ReadXML();"  />	-->
       <input id="saveAcc"  type="button"  Class="cssbutton" Style="width:130px; height:25px;margin-left:20px;"   value="Save Access Info" onclick="saveAccessInfo();"/> 

<s:if test="">
    <div class="error" id="errorMessages">    
      <s:iterator value="actionErrors">
        <img src="<c:url value="/images/iconWarning.gif"/>"
            alt="<fmt:message key="icon.warning"/>" class="icon" />
        <s:property escape="false"/><br />
      </s:iterator>
   </div>
</s:if>

<%-- Script Shifted from Top to Botton on 06-Sep-2012 By Kunal --%>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">  
	try{
		<c:if test="${message=='popup'}">
			window.open('inventryTransferPopUp.html?cid=${customerFile.id}&decorator=popup&popup=true&Quote=${Quote}&modeMessage=${modeMessage}','forms','height=300,width=550,top=1, left=200, scrollbars=yes,resizable=yes');
		</c:if>
	}
	catch(e){} 
	function add(){
		window.location ="editSurveyDetails.html?cid=${customerFile.id}&id=&SOflag=CF&Quote=${Quote}";
	}
	function InventryTransfer(){	
		window.location="inventryTransfer.html?cid=${customerFile.id}&Quote=${Quote}";
	}
	function ReadXML(){
		window.location ="readXml.html?cid=${customerFile.id}";
	}
	function saveAccessInfo(){
	  document.forms['accessInfoForm'].submit();
	}	
</script>
<script type="text/javascript">
	if (document.getElementById || document.all){
		document.write('<div id="bigimageid">');
		document.write('</div>');
	}
	
	function gettrailobj(){
		if (document.getElementById)
			return document.getElementById("bigimageid").style
		else if (document.all)
			return document.all.trailimagid.style
	}
	
	function gettrailobjnostyle(){
		if (document.getElementById)
			return document.getElementById("bigimageid")
		else if (document.all)
			return document.all.trailimagid
	}
	function truebody(){
		return (!window.opera && document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
	}
	
	function showtrail(imagename,title,description,showthumb,height){
		if (height > 0){
			currentimageheight = height;
		}
		document.onmousemove=followmouse;
		newHTML = '<div style="padding: 5px; background-color: #FFFFFF; border: 1px solid #888888;">';
		if (showthumb > 0){
			newHTML = newHTML + '<div align="center" style="padding: 2px 0px 2px 0px;">';
			newHTML = newHTML + '<img src="' + imagename + '" border="0"></div>';
		}
		gettrailobjnostyle().innerHTML = newHTML;
		gettrailobj().visibility="visible";
	}
	
	function hidetrail(){
		gettrailobj().visibility="hidden"
		document.onmousemove=""
		gettrailobj().left="-500px"
	}
	
	function followmouse(e){
		var xcoord=offsetfrommouse[0]
		var ycoord=offsetfrommouse[1]
		var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
		var docheight=document.all? Math.min(truebody().scrollHeight, truebody().clientHeight) : Math.min(window.innerHeight)
	
	//if (document.all){
	// gettrailobjnostyle().innerHTML = 'A = ' + truebody().scrollHeight + '<br>B = ' + truebody().clientHeight;
	//} else {
	// gettrailobjnostyle().innerHTML = 'C = ' + document.body.offsetHeight + '<br>D = ' + window.innerHeight;
	//}
	
		if (typeof e != "undefined"){
			if (docwidth - e.pageX < 380){
				xcoord = e.pageX - xcoord - 400; // Move to the left side of the cursor
			} else {
				xcoord += e.pageX;
			}
			if (docheight - e.pageY < (currentimageheight + 110)){
				ycoord += e.pageY - Math.max(0,(110 + currentimageheight + e.pageY - docheight - truebody().scrollTop));
			} else {
				ycoord += e.pageY;
			}
		
		} else if (typeof window.event != "undefined"){
			if (docwidth - event.clientX < 380){
				xcoord = event.clientX + truebody().scrollLeft - xcoord - 400; // Move to the left side of the cursor
			} else {
				xcoord += truebody().scrollLeft+event.clientX
			}
			if (docheight - event.clientY < (currentimageheight + 110)){
				ycoord += event.clientY + truebody().scrollTop - Math.max(0,(110 + currentimageheight + event.clientY - docheight));
			} else {
				ycoord += truebody().scrollTop + event.clientY;
			}
		}
		
		var docwidth=document.all? truebody().scrollLeft+truebody().clientWidth : pageXOffset+window.innerWidth-15
		var docheight=document.all? Math.max(truebody().scrollHeight, truebody().clientHeight) : Math.max(document.body.offsetHeight, window.innerHeight)
		if(ycoord < 0) { ycoord = ycoord*-1; }
		gettrailobj().left=xcoord+"px"
		gettrailobj().top=ycoord+"px"
	}

	var tooltip=function(){
	 var id = 'tt';
	 var top = 3;
	 var left = 3;
	 var maxw = 300;
	 var speed = 10;
	 var timer = 20;
	 var endalpha = 95;
	 var alpha = 0;
	 var tt,t,c,b,h;
	 var ie = document.all ? true : false;
	 return{
	  show:function(v,w){
	   if(tt == null){
	    tt = document.createElement('div');
	    tt.setAttribute('id',id);
	    t = document.createElement('div');
	    t.setAttribute('id',id + 'top');
	    c = document.createElement('div');
	    c.setAttribute('id',id + 'cont');
	    b = document.createElement('div');
	    b.setAttribute('id',id + 'bot');
	    tt.appendChild(t);
	    tt.appendChild(c);
	    tt.appendChild(b);
	    document.body.appendChild(tt);
	    tt.style.opacity = 0;
	    tt.style.filter = 'alpha(opacity=0)';
	    document.onmousemove = this.pos;
	   }
	   tt.style.display = 'block';
	   c.innerHTML = v;
	   tt.style.width = w ? w + 'px' : 'auto';
	   if(!w && ie){
	    t.style.display = 'none';
	    b.style.display = 'none';
	    tt.style.width = tt.offsetWidth;
	    t.style.display = 'block';
	    b.style.display = 'block';
	   }
	  if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
	  h = parseInt(tt.offsetHeight) + top;
	  clearInterval(tt.timer);
	  tt.timer = setInterval(function(){tooltip.fade(1)},timer);
	  },
	  pos:function(e){
	   var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
	   var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
	   tt.style.top = (u - h) + 'px';
	   tt.style.left = (l + left) + 'px';
	  },
	  fade:function(d){
	   var a = alpha;
	   if((a != endalpha && d == 1) || (a != 0 && d == -1)){
	    var i = speed;
	   if(endalpha - a < speed && d == 1){
	    i = endalpha - a;
	   }else if(alpha < speed && d == -1){
	     i = a;
	   }
	   alpha = a + (i * d);
	   tt.style.opacity = alpha * .01;
	   tt.style.filter = 'alpha(opacity=' + alpha + ')';
	  }else{
	    clearInterval(tt.timer);
	     if(d == -1){tt.style.display = 'none'}
	  }
	 },
	 hide:function(){
	  clearInterval(tt.timer);
	   tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
	  }
	 };
	}();

	function imageList(cid,id,position){
		var url="toolTipImageList.html?cid="+cid+"&id="+id+"&decorator=simple&popup=true";
		ajax_SoTooltip(url,position);	
	}
	function confirmSubmit(targetElement, targetElement2){
		var agree=confirm("Please confirm that you want to delete this survey data permanently?");
		var cid = targetElement;
		var id=targetElement2;
		if (agree){
	        window.location="deleteItem.html?SOflag=CF&Quote=${Quote}&cid="+cid+"&id="+id;
		}
	}

	function linkToImages(id,type){
	    if(id!=''){
	         window.open('accessInfoImageList.html?id=${accessInfo.id}&cid=${customerFile.id}&accessInfoType='+type,'forms','height=650,width=950,top=1, left=50, scrollbars=yes,resizable=yes');
	     }else{
	        alert("No Data is uploaded from Voxme");
	      }
	}

	function limitText(limitField, limitCount, limitNum) {
		if (limitField.value.length > limitNum) {
			limitField.value = limitField.value.substring(0,limitNum);
		} else {
			limitCount.value = limitNum - limitField.value.length;
		}
	}

	function wordCount(comment){
		commentLenght = comment.value.length;
		if(commentLenght>1950){
			alert("Please enter comment less than 2000 characters.");
			return false;
		}
	}
</script>
	
<script type="text/javascript">
  animatedcollapse.addDiv('inventory', 'fade=1,hide=0,show=1')
  animatedcollapse.addDiv('accessinfo', 'fade=1,hide=1,show=0')
  animatedcollapse.addDiv('destinationinfo', 'fade=1,hide=1,show=0')
  animatedcollapse.addDiv('surveydetails', 'fade=1,hide=1,show=0')	
  animatedcollapse.init()
</script>
<%-- Shifting Closed Here --%>

<script language="javascript" type="text/javascript">
<c:if test="${Quote!='y'}">
<c:if test="${hitflag=='1'}">
 <c:redirect url="/surveyDetails.html?cid=${customerFile.id}"/>
</c:if>
</c:if>
<c:if test="${Quote=='y'}">
<c:if test="${hitflag=='1'}">
 <c:redirect url="/surveyDetails.html?cid=${customerFile.id}&Quote=y"/>
</c:if>
</c:if>
try{
	<c:if test="${ usertype == 'AGENT' || ( not empty accessInfo.id && not empty accessInfo.networkSynchedId && accessInfo.id != accessInfo.networkSynchedId )}">
	var elementsLen=document.forms['accessInfoForm'].elements.length;
	for(i=0;i<=elementsLen-1;i++){
	if(document.forms['accessInfoForm'].elements[i].type=='text'){
				document.forms['accessInfoForm'].elements[i].readOnly =true;
				document.forms['accessInfoForm'].elements[i].className = 'input-textUpper';
		}else{
				document.forms['accessInfoForm'].elements[i].disabled=true;
		}
	}
	
	document.getElementById('addInv').disabled=true;
    document.getElementById('saveAcc').disabled=true;	
</c:if>
}catch(e){
	
}
function notExists(){
	alert("The surey details information has not been saved yet, please save survey information to continue");
}
</script>