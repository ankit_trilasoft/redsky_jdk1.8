/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "ImfEntitlements" object in Redsky .
 * @Class Name	ImfEntitlementsAction
 * @Author      Ravi Mishra
 * @Version     V01.0
 * @Since       1.0
 * @Date        08-Dec-2008
 */
package com.trilasoft.app.webapp.action;

import static java.lang.System.out;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.io.BufferedWriter;
import java.io.EOFException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.ImfEntitlements;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ImfEntitlementsManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.ServiceOrderManager;

public class ImfEntitlementsAction extends BaseAction{
	
	//static Variables
	//List Data type Variables
	private List imfEstimate;
	private List imfEstimateList;
	//String Data type Variables
	private String sessionCorpID;
	//Long Data type Variables	
	private Long id;
	private Long cid;
	private Long maxId;
	//Class Variables of Managers
	private ImfEntitlementsManager imfEntitlementsManager;
	private ImfEntitlements imfEntitlements;	
	private CustomerFileManager customerFileManager;	
	//Class Variables of Models	
	private CustomerFile customerFile;
	private ServiceOrder serviceOrder;
	private String mailStatus;
	private List userEmail;
	private String emailAdd;
	private Date beginDate;
	private Date endDate;
	private String prefix;
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	//A Method to Authenticate User, calling from main menu to set CorpID.
	public ImfEntitlementsAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	 //Method for editing  record.
	@SkipValidation
	public String edit(){
		customerFile=customerFileManager.get(cid);
		List imfEntitlementsList=imfEntitlementsManager.getImfEntitlements(customerFile.getSequenceNumber());
		if (!imfEntitlementsList.isEmpty())
		{
			
			imfEntitlements=(ImfEntitlements)imfEntitlementsManager.getImfEntitlements(customerFile.getSequenceNumber()).get(0);
		}
		else
		{
			imfEntitlements=new ImfEntitlements();
			imfEntitlements.setCreatedOn(new Date());
			imfEntitlements.setUpdatedOn(new Date());
		
		}
		imfEntitlements.setCorpID(sessionCorpID);
		getNotesForIconChange();
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String imfEstimateExtract(){		
		return SUCCESS;
	}

	@SkipValidation
	public String imfFileActual(){		
		return SUCCESS;
	}
	
	@SkipValidation
	public String imfFileAgent(){		
		return SUCCESS;
	}
	@SkipValidation
	public String imfFileDown(){		
		return SUCCESS;
	}
	
	
//	ssDown extraction
	@SkipValidation
	public void imfEstimateDown(){
		String transShip="";
		imfEstimate=imfEntitlementsManager.getImfEstimateDown();		
		try
		{
			File file1 = new File("ssdown");			
			String text= new String();
			String shipnumber="";
			if(!imfEstimate.isEmpty())
			{		
			Iterator it=imfEstimate.iterator();		
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();	
				if(row[6]!=null)
				{
					String originAddressLine1 = row[6].toString();					
					originAddressLine1 = originAddressLine1.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
				    row[6]=(String)originAddressLine1;				  
				}
				
				if(row[7]!=null)
				{
					String originAddressLine2 = row[7].toString();					
					originAddressLine2 = originAddressLine2.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[7]=(String)originAddressLine2;
				  
				}
				
				if(row[8]!=null)
				{
					String originAddressLine3 = row[8].toString();					
					originAddressLine3 = originAddressLine3.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[8]=(String)originAddressLine3;
				   
				}
				
				if(row[9]!=null)
				{
					String originCity = row[9].toString();					
					originCity = originCity.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[9]=(String)originCity;				   
				}
				
				if(row[15]!=null)
				{
					String destinAddressLine1 = row[15].toString();					
					destinAddressLine1 = destinAddressLine1.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[15]=(String)destinAddressLine1;				   
				}
				
				if(row[16]!=null)
				{
					String destinAddressLine2 = row[16].toString();					
					destinAddressLine2 = destinAddressLine2.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[16]=(String)destinAddressLine2;				   
				}
				
				if(row[17]!=null)
				{
					String destinAddressLine3 = row[17].toString();					
					destinAddressLine3 = destinAddressLine3.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[17]=(String)destinAddressLine3;				   
				}
				
				if(row[18]!=null)
				{
					String destinCity = row[18].toString();					
					destinCity = destinCity.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				   
				    row[18]=(String)destinCity;				   
				}
				
				
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))				
				{
					row[0]="            ";
				}
				if(row[0]!=null)
				{
					shipnumber=row[0].toString();
					int countWeight=shipnumber.length();
					int diff = (12-countWeight);
					String temp="";
					for(int i=1;i<=diff;i++)
					{
						temp=temp+" ";						
					}
					shipnumber=shipnumber+temp;					
				}
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{
					row[1]="      ";
				}
								
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{
					row[2]="  ";
				}
				
				//billToAuthority begin
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{
					row[3]="        ";
				}
							
				
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{
					row[4]="        ";
				}
				
				if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
				{
					row[5]=" ";
				}				
				
				//originAddress1 begin
				if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))				
				{
					row[6]="                                        ";
				}
				
				if(row[7]==null||row[7].toString().trim().equals("null")||row[7].toString().trim().equals(""))				
				{
					row[7]="                                        ";
				}
				
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals(""))				
				{
					row[8]="                                        ";
				}
				
				if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))				
				{
					row[9]="                         ";
				}
				
				if(row[10]==null||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))				
				{
					row[10]="  ";
				}
				if(row[11]==null||row[11].toString().trim().equals("null")||row[11].toString().trim().equals(""))				
				{
					row[11]="         ";
				}
				if(row[12]==null||row[12].toString().trim().equals("null")||row[12].toString().trim().equals(""))				
				{
					row[12]="              ";
				}
				
				if(row[13]==null||row[13].toString().trim().equals("null")||row[13].toString().trim().equals(""))				
				{
					row[13]="              ";
				}
								
				if(row[14]==null||row[14].toString().trim().equals("null")||row[14].toString().trim().equals(""))				
				{
					row[14]="   ";
				}
				if(row[14]!=null)
				{					
					String country=row[14].toString();
					if(country.trim().equals("ROU"))
					{
						country="ROM";
					}
					if(country.trim().equals("MNE"))
					{
						country="SCG";
					}
					if(country.trim().equals("TLS"))
					{
						country="TMP";
					}
									
					row[14]=country.toString();
				}
				
				if(row[15]==null||row[15].toString().trim().equals("null")||row[15].toString().trim().equals(""))				
				{
					row[15]="                                        ";
				}
				
				if(row[16]==null||row[16].toString().trim().equals("null")||row[16].toString().trim().equals(""))				
				{
					row[16]="                                        ";
				}
				
				if(row[17]==null||row[17].toString().trim().equals("null")||row[17].toString().trim().equals(""))				
				{
					row[17]="                                        ";
										
				}
				//destinationAddress3 END
				if(row[18]==null||row[18].toString().trim().equals("null")||row[18].toString().trim().equals(""))				
				{
					row[18]="                         ";
				}
				
				if(row[19]==null||row[19].toString().trim().equals("null")||row[19].toString().trim().equals(""))				
				{
					row[19]="  ";
				}
				
				if(row[20]==null||row[20].toString().trim().equals("null")||row[20].toString().trim().equals(""))				
				{
					row[20]="         ";
				}
				
				if(row[21]==null||row[21]==null||row[21].toString().trim().equals("null")||row[21].toString().trim().equals(""))				
				{
					row[21]="   ";
				}
				if(row[21]!=null)
				{					
					String country=row[21].toString();
					if(country.trim().equals("ROU"))
					{
						country="ROM";
					}
					if(country.trim().equals("MNE"))
					{
						country="SCG";
					}
					if(country.trim().equals("TLS"))
					{
						country="TMP";
					}
					row[21]=country.toString();
				}
				
				if(row[22]==null||row[22].toString().trim().equals("null")||row[22].toString().trim().equals(""))				
				{
					row[22]="               ";
				}
				
				if(row[23]==null||row[23].toString().trim().equals("null")||row[23].toString().trim().equals(""))				
				{
					row[23]="              ";
				}
				//destinationCountryPhone END
				if(row[24]==null||row[24].toString().trim().equals("null")||row[24].toString().trim().equals(""))				
				{
					row[24]="        ";
				}
				if(row[25]==null||row[25].toString().trim().equals("null")||row[25].toString().trim().equals(""))				
				{
					row[25]="        ";
				}
				if(row[26]==null||row[26].toString().trim().equals("null")||row[26].toString().trim().equals(""))				
				{
					row[26]="        ";
				}							
								
				if(row[27]==null||row[27].toString().trim().equals("null")||row[27].toString().trim().equals(""))				
				{
					row[27]=" ";
				}
				
				if(row[28]==null||row[28].toString().trim().equals("null")||row[28].toString().trim().equals(""))				
				{
					row[28]="      ";
				}
				
				if(row[29]==null||row[29].toString().trim().equals("null")||row[29].toString().trim().equals(""))				
				{
					row[29]=" ";
				}
				
				if(row[30]==null||row[30].toString().trim().equals("null")||row[30].toString().trim().equals(""))				
				{
					row[30]="   ";
										
				}
				//estimator END
				if(row[31]==null||row[31].toString().trim().equals("null")||row[31].toString().trim().equals(""))				
				{
					row[31]="     ";
				}
				
				if(row[32]==null||row[32].toString().trim().equals("null")||row[32].toString().trim().equals(""))				
				{
					row[32]="        ";
				}
				
				if(row[33]==null||row[33].toString().trim().equals("null")||row[33].toString().trim().equals(""))				
				{
					row[33]="        ";
				}
				
				if(row[34]==null||row[34].toString().trim().equals("null")||row[34].toString().trim().equals(""))				
				{
					row[34]=" ";
				}
				
				if(row[35]==null||row[35].toString().trim().equals("null")||row[35].toString().trim().equals(""))				
				{
					row[35]="      ";
				}
				
				if(row[36]==null||row[36].toString().trim().equals("null")||row[36].toString().trim().equals(""))				
				{
					row[36]=" ";
				}
				//pieces begin
				if(row[37]==null||row[37].toString().trim().equals("null")||row[37].toString().trim().equals(""))				
				{
					row[37]="      ";
				}
				if(row[38]==null||row[38].toString().trim().equals("null")||row[38].toString().trim().equals(""))				
				{
					row[38]="  ";
				}
				if(row[39]==null||row[39].toString().trim().equals("null")||row[39].toString().trim().equals(""))				
				{
					row[39]="  ";
				}
				
				
				if(row[40]==null||row[40].toString().trim().equals("null")||row[40].toString().trim().equals(""))				
				{
					row[40]=" ";
				}
							
				if(row[41]==null||row[41].toString().trim().equals("null")||row[41].toString().trim().equals(""))				
				{
					row[41]="        ";
				}
				
				if(row[42]==null||row[42].toString().trim().equals("null")||row[42].toString().trim().equals(""))				
				{
					row[42]="        ";
				}
				
				if(row[43]==null||row[43].toString().trim().equals("null")||row[43].toString().trim().equals(""))				
				{
					row[43]="        ";
				}
				
				if(row[44]==null||row[44].toString().trim().equals("null")||row[44].toString().trim().equals(""))				
				{
					row[44]=" ";
										
				}
				
				if(row[45]==null||row[45].toString().trim().equals("null")||row[45].toString().trim().equals(""))				
				{
					row[45]="        ";
				}
				
				if(row[46]==null||row[46].toString().trim().equals("null")||row[46].toString().trim().equals(""))				
				{
					row[46]="                 ";
				}
				
				if(row[47]==null||row[47].toString().trim().equals("null")||row[47].toString().trim().equals(""))				
				{
					row[47]="        ";
				}
				
				if(row[48]==null||row[48].toString().trim().equals("null")||row[48].toString().trim().equals(""))				
				{
					row[48]=" ";
				}
				transShip=row[48].toString();
				
				if(transShip.trim().equals("true"))
				{
					transShip="Y";
					
				}
				else
				{
					transShip=" ";
					
				}
				if(row[49]==null||row[49].toString().trim().equals("null")||row[49].toString().trim().equals(""))				
				{
					row[49]="        ";
				}
				
				if(row[50]==null||row[50].toString().trim().equals("null")||row[50].toString().trim().equals(""))				
				{
					row[50]="                              ";
				}
				
				if(row[51]==null||row[51].toString().trim().equals("null")||row[51].toString().trim().equals(""))				
				{
					row[51]="        ";
				}
				if(row[52]==null||row[52].toString().trim().equals("null")||row[52].toString().trim().equals(""))				
				{
					row[52]=" ";
				}
				if(row[53]==null||row[53].toString().trim().equals("null")||row[53].toString().trim().equals(""))				
				{
					row[53]="                              ";
				}
				
				if(row[54]==null||row[54].toString().trim().equals("null")||row[54].toString().trim().equals(""))				
				{
					row[54]="                              ";
				}
								
				if(row[55]==null||row[55].toString().trim().equals("null")||row[55].toString().trim().equals(""))				
				{
					row[55]="                            ";
				}
				
				if(row[56]==null||row[56].toString().trim().equals("null")||row[56].toString().trim().equals(""))				
				{
					row[56]="        ";
				}
				
				if(row[57]==null||row[57].toString().trim().equals("null")||row[57].toString().trim().equals(""))				
				{
					row[57]=" ";
				}
				
				if(row[58]==null||row[58].toString().trim().equals("null")||row[58].toString().trim().equals(""))				
				{
					row[58]="        ";
										
				}
				
				if(row[59]==null||row[59].toString().trim().equals("null")||row[59].toString().trim().equals(""))				
				{
					row[59]=" ";
				}
				
				if(row[60]==null||row[60].toString().trim().equals("null")||row[60].toString().trim().equals(""))				
				{
					row[60]=" ";
				}
				
				if(row[61]==null||row[61].toString().trim().equals("null")||row[61].toString().trim().equals(""))				
				{
					row[61]=" ";
				}
				
				if(row[62]==null||row[62].toString().trim().equals("null")||row[62].toString().trim().equals(""))				
				{
					row[62]="        ";
				}
				
				if(row[63]==null||row[63].toString().trim().equals("null")||row[63].toString().trim().equals(""))				
				{
					row[63]="        ";
				}
				
				if(row[64]==null||row[64].toString().trim().equals("null")||row[64].toString().trim().equals(""))				
				{
					row[64]="        ";
				}
				
				if(row[65]==null||row[65].toString().trim().equals("null")||row[65].toString().trim().equals(""))				
				{
					row[65]="        ";
				}
				if(row[66]==null||row[66].toString().trim().equals("null")||row[66].toString().trim().equals(""))				
				{
					row[66]="               ";
				}
				if(row[67]==null||row[67].toString().trim().equals("null")||row[67].toString().trim().equals(""))				
				{
					row[67]="        ";
				}
				
				if(row[67]!=null)
				{
					row[67]="        ";
				}
				
				if(row[68]==null||row[68].toString().trim().equals("null")||row[68].toString().trim().equals(""))				
				{
					row[68]="        ";
				}
				
				if(row[68]!=null)
				{
					row[68]="        ";
				}
								
				if(row[69]==null||row[69].toString().trim().equals("null")||row[69].toString().trim().equals(""))				
				{
					row[69]="                    ";
				}
				if(row[78]==null||row[78].toString().trim().equals("null")||row[78].toString().trim().equals(""))				
				{
					row[78]="         ";
				}
				if(row[70]==null||row[70].toString().trim().equals("null")||row[70].toString().trim().equals(""))				
				{
					row[70]="   ";
				}
				
				if(row[71]==null||row[71].toString().trim().equals("null")||row[71].toString().trim().equals(""))				
				{
					row[71]="        ";       
				}
				if(row[72]==null||row[72].toString().trim().equals("null")||row[72].toString().trim().equals(""))				
				{
					row[72]="    ";       
				}
				if(row[72]!=null)
				{					
					String commodity=row[72].toString();
					if(commodity.trim().equals("ART"))
					{
						commodity="FINE";
					}
					if(commodity.trim().equals("HHGD"))
					{
						commodity="HHG";
					}
					if(commodity.trim().equals("HIVA"))
					{
						commodity="HIVL";
					}
					if(commodity.trim().equals("MTRC"))
					{
						commodity="MTRC";
					}
					if(commodity.trim().equals("PIAN"))
					{
						commodity="PNO ";
					}					
					row[72]=commodity.toString();
				}
				
				if(row[73]==null||row[73].toString().trim().equals("null")||row[73].toString().trim().equals(""))				
				{
					row[73]="                                 ";       
				}
				if(row[74]==null||row[74].toString().trim().equals("null")||row[74].toString().trim().equals(""))				
				{
					row[74]="      ";       
				}	
				
				//System.out.println("\nrow[74]"+row[74]);
				
				if(row[75]==null||row[75].toString().trim().equals("null")||row[75].toString().trim().equals(""))				
				{
					row[75]="      ";       
				}
				
				if(row[76]==null||row[76].toString().trim().equals("null")||row[76].toString().trim().equals(""))				
				{
					row[76]=" ";       
				}
				
				if(row[77]==null||row[77].toString().trim().equals("null")||row[77].toString().trim().equals(""))				
				{
					row[77]=" ";       
				}
				
				// System.out.println("\nrow[76]"+row[76]);
				//System.out.println("\nrow[77]"+row[77]);
				
	    		String nowYYYYMMDD="         ";
	    		if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD="        ";
	    		}
	    		if(!row[4].toString().trim().equals("null") && !row[4].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[4];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
			         nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		
	    		String nowYYYYMMDD1="         ";
	    		if(row[26]==null||row[26].toString().trim().equals("null")||row[26].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD1="        ";
	    		}
	    		if(!row[26].toString().trim().equals("null") && !row[26].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[26];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
			         nowYYYYMMDD1 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		String nowYYYYMMDD2="          ";
	    		if(row[32]==null||row[32].toString().trim().equals("null")||row[32].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD2="         ";
	    		}
	    		if(!row[32].toString().trim().equals("null") && !row[32].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[32];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
			         nowYYYYMMDD2 = new String( dateformatYYYYMMDD.format( date ) );
			         nowYYYYMMDD2=nowYYYYMMDD2+row[76].toString();
			    } 
	    		
	    		String nowYYYYMMDD3="          ";
	    		if(row[33]==null||row[33].toString().trim().equals("null")||row[33].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD3="         ";
	    		}
	    		if(!row[33].toString().trim().equals("null") && !row[33].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[33];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
			         nowYYYYMMDD3 = new String( dateformatYYYYMMDD.format( date ) );
			         nowYYYYMMDD3=nowYYYYMMDD3+row[77].toString();
			    } 
	    		
	    		String nowYYYYMMDD4="         ";
	    		if(row[41]==null||row[41].toString().trim().equals("null")||row[41].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD4="        ";
	    		}
	    		if(!row[41].toString().trim().equals("null") && !row[41].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[41];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD4 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		String nowYYYYMMDD5="         ";
	    		if(row[42]==null||row[42].toString().trim().equals("null")||row[42].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD5="        ";
	    		}
	    		if(!row[42].toString().trim().equals("null") && !row[42].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[42];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD5 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		String nowYYYYMMDD6="         ";
	    		if(row[43]==null||row[43].toString().trim().equals("null")||row[43].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD6="        ";
	    		}
	    		if(!row[43].toString().trim().equals("null") && !row[43].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[43];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD6 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		String nowYYYYMMDD7="          ";
	    		if(row[47]==null||row[47].toString().trim().equals("null")||row[47].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD7="         ";
	    		}
	    		if(!row[47].toString().trim().equals("null") && !row[47].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[47];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD7 = new String( dateformatYYYYMMDD.format( date ) );	
					nowYYYYMMDD7=nowYYYYMMDD7+row[76].toString();
			    } 
	    		
	    		
	    		String nowYYYYMMDD56="          ";
	    		if(row[56]==null||row[56].toString().trim().equals("null")||row[56].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD56="         ";
	    		}
	    		if(!row[56].toString().trim().equals("null") && !row[56].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[56];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD56 = new String( dateformatYYYYMMDD.format( date ) );
					nowYYYYMMDD56=nowYYYYMMDD56+row[77].toString();
			    } 
	    		
	    		String nowYYYYMMDD58="         ";
	    		if(row[58]==null||row[58].toString().trim().equals("null")||row[58].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD58="        ";
	    		}
	    		if(row[58]!=null)
	    		{	    			
	    			nowYYYYMMDD58="        ";
	    		}
	    		
	    		String nowYYYYMMDD62="         ";
	    		if(row[62]==null||row[62].toString().trim().equals("null")||row[62].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD62="        ";
	    		}
	    		if(!row[62].toString().trim().equals("null") && !row[62].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[62];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD62 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		String nowYYYYMMDD63="         ";
	    		if(row[63]==null||row[63].toString().trim().equals("null")||row[63].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD63="        ";
	    		}
	    		if(!row[63].toString().trim().equals("null") && !row[63].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[63];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD63 = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		String nowYYYYMMDD64="          ";
	    		
	    		if(row[64]==null||row[64].toString().trim().equals("null")||row[64].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD64="         ";
	    		}
	    		if(!row[64].toString().trim().equals("null") && !row[64].toString().trim().equals(""))
	    		{				
	    			/*Date date=(Date)row[64];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD64 = new String( dateformatYYYYMMDD.format( date ) );
					nowYYYYMMDD64=nowYYYYMMDD64+row[79].toString();*/
	    			nowYYYYMMDD64=row[64]+row[79].toString();
			    } 
	    		
	    		
	    		String nowYYYYMMDD65="         ";
	    		if(row[65]==null||row[65].toString().trim().equals("null")||row[65].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD65="        ";
	    		}
	    		if(!row[65].toString().trim().equals("null") && !row[65].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[65];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD65 = new String( dateformatYYYYMMDD.format( date ) );	
			    }
	    		
	    		
	    		String nowYYYYMMDD51="          ";
	    		if(row[51]==null||row[51].toString().trim().equals("null")||row[51].toString().trim().equals(""))
	    		{	    			
	    			nowYYYYMMDD51="         ";
	    		}
	    		if(!row[51].toString().trim().equals("null") && !row[51].toString().trim().equals(""))
	    		{				
	    			Date date=(Date)row[51];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
					nowYYYYMMDD51 = new String( dateformatYYYYMMDD.format( date ) );	
					nowYYYYMMDD51=nowYYYYMMDD51+row[77].toString();
			    } 
	    		
	    		
	    		if(row[29].toString().trim().equals("A"))
	    		{
	    			row[29]="G";
	    		}
	    		else
	    		{
	    			row[29]="N";
	    		}
	    		
	    		if(row[36].toString().trim().equals("A"))
	    		{
	    			row[36]="G";
	    		}
	    		else
	    		{
	    			row[36]="N";
	    		}
	    		
	    		
	    		
	    		
	    		if(row[36].toString().trim().equals("S"))
	    		{
	    			text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"N"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                          "+row[78].toString()+row[71].toString()+row[72].toString()+"\n";
	    			//text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"N"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+row[78].toString()+"                                         "+row[72].toString()+"\n";
	    		}
	    		if(row[36].toString().trim().equals("A"))
	    		{
	    			text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"G"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                          "+row[78].toString()+row[71].toString()+row[72].toString()+"\n";
	    			//text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+"G"+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+row[78].toString()+"                                         "+row[72].toString()+"\n";
	    		}
	    			    		
	    		if(!row[36].toString().trim().equals("A") && !row[36].toString().trim().equals("S"))
	    		{
	    			text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+row[36].toString()+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+"                                         "+row[78].toString()+row[71].toString()+row[72].toString()+"\n";
	    			//text=text+shipnumber+row[1].toString()+row[2].toString()+row[3].toString()+nowYYYYMMDD+row[5].toString()+row[6].toString()+row[7].toString()+row[8].toString()+row[9].toString()+row[10].toString()+row[11].toString()+row[12].toString()+row[13].toString()+row[14].toString()+row[15].toString()+row[16].toString()+row[17].toString()+row[18].toString()+row[19].toString()+row[20].toString()+row[21].toString()+row[22].toString()+row[23].toString()+"237"+row[24].toString()+row[25].toString()+nowYYYYMMDD1+row[27].toString()+row[28].toString()+row[29].toString()+row[30].toString()+row[31].toString()+nowYYYYMMDD2+nowYYYYMMDD3+row[35].toString()+row[36].toString()+row[37].toString()+row[38].toString()+row[39].toString()+"                                                            "+row[40].toString()+nowYYYYMMDD4+nowYYYYMMDD5+nowYYYYMMDD6+transShip+row[45].toString()+row[46].toString()+nowYYYYMMDD7+row[49].toString()+row[53].toString()+nowYYYYMMDD51+row[50].toString()+row[73].toString()+row[55].toString()+nowYYYYMMDD56+nowYYYYMMDD58+" "+row[61].toString()+nowYYYYMMDD62+nowYYYYMMDD63+nowYYYYMMDD64+nowYYYYMMDD65+"                               "+row[69].toString()+row[70].toString()+row[78].toString()+"                                        "+row[72].toString()+"\n";
	    		}
				 
			}		
			
			HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();			
			response.setContentType("text");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");	
			outputStream.write(text.getBytes());
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
		
	}
//	Tsto extraction
	@SkipValidation
	public void imfEstimateTsto(){
		
		imfEstimate=imfEntitlementsManager.getImfEstimateTsto();				
		try
		{
			File file1 = new File("tsto");			
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();				
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();						
				
				//shipid  
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
				{					
					outputStream.write("      ".getBytes());
				}
				else
				{
					outputStream.write(row[0].toString().getBytes()) ;
				}
				
				//shipnbr
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{					
					outputStream.write("  ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
						
				//lastname
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{					
					outputStream.write("                                                       ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				//descr
				
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{					
					outputStream.write("                                                                                                                                                                                                                                                              ".getBytes());
				}
				else
				{
					
					String descr = row[3].toString();					
					descr = descr.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
					outputStream.write(descr.getBytes()) ;				  
					
				}
								
				//containerid
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				//price
				if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
				{				
					outputStream.write("        ".getBytes());
				}
				else
				{
					outputStream.write(row[5].toString().getBytes()) ;
				}
					
				// location
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[6].toString().getBytes()) ;
			    } 
	    		
				//torelease
				if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[7].toString().getBytes()) ;
				}	
				
				//ticket	
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[8].toString().getBytes()) ;
				}	
				
	    		//model	
	    		if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[9].toString().getBytes()) ;
			    }
	    		
//	    		measureqty	
	    		if(row[10]==null||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("         ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[10].toString().getBytes()) ;
			    }
	    		
//	    		itemtag	
	    		if(row[11]==null||row[11].toString().trim().equals("null")||row[11].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[11].toString().getBytes()) ;
			    }
	    		
//	    		serial
	    		if(row[12]==null||row[12].toString().trim().equals("null")||row[12].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[12].toString().getBytes()) ;
			    }
	    		
//	    		vendorrefnbr	
	    		if(row[13]==null||row[13].toString().trim().equals("null")||row[13].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("         ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[13].toString().getBytes()) ;
			    }
	    		
//	    		idnbr	
	    		if(row[14]==null||row[14].toString().trim().equals("null")||row[14].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[14].toString().getBytes()) ;
			    }
	    		
//	    		loctype		
	    		if(row[15]==null||row[15].toString().trim().equals("null")||row[15].toString().trim().equals(""))
	    		{	    
	    			outputStream.write(" ".getBytes());
	    		}
	    		else
	    		{	
	    			row[15]=" ";
	    			outputStream.write(row[15].toString().getBytes()) ;
			    }
	    		
//	    		closed	
	    		String closed="        ";
	    		if(row[16]==null||row[16].toString().trim().equals("null")||row[16].toString().trim().equals(""))
	    		{	    
	    			closed="        ";
	    			outputStream.write(closed.getBytes());
	    		}
	    		else
	    		{				
	    			Date date=(Date)row[16];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
					closed = new String( dateformatYYYYMMDD.format( date ) );	
			        outputStream.write(closed.getBytes()) ;
			    } 
	    		
	    		
//	    		canceled
	    		String canceled="        ";
	    		if(row[17]==null||row[17].toString().trim().equals("null")||row[17].toString().trim().equals(""))
	    		{	    
	    			canceled="        ";
	    			outputStream.write(canceled.getBytes());
	    		}
	    		else
	    		{				
	    			/*Date date=(Date)row[17];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
					canceled = new String( dateformatYYYYMMDD.format( date ) );	
			       */ outputStream.write(canceled.getBytes()) ;
			    } 
	    		
	    		
//	    		opend	
	    		String opend="        ";
	    		if(row[18]==null||row[18].toString().trim().equals("null")||row[18].toString().trim().equals(""))
	    		{	    
	    			opend="        ";
	    			outputStream.write(opend.getBytes());
	    		}
	    		else
	    		{				
	    			Date date=(Date)row[18];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
					opend = new String( dateformatYYYYMMDD.format( date ) );	
			        outputStream.write(opend.getBytes()) ;
			    } 
	    		
	    		
//	    		warehouse	
	    		if(row[19]==null||row[19].toString().trim().equals("null")||row[19].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[19].toString().getBytes()) ;
			    }
	    		
//	    		onhand		
	    		if(row[20]==null||row[20].toString().trim().equals("null")||row[20].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("       ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[20].toString().getBytes()) ;
			    }
	    		
//	    		insvalue	
	    		if(row[21]==null||row[21].toString().trim().equals("null")||row[21].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[21].toString().getBytes()) ;
			    }
	    		
//	    		measure	
	    		if(row[22]==null||row[22].toString().trim().equals("null")||row[22].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[22].toString().getBytes()) ;
			    }
	    		
//	    		released dt 	
	    		String released="        ";
	    		if(row[23]==null||row[23].toString().trim().equals("null")||row[23].toString().trim().equals(""))
	    		{	    
	    			released="        ";
	    			outputStream.write(released.getBytes());
	    		}
	    		else
	    		{				
	    			Date date=(Date)row[23];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
					released = new String( dateformatYYYYMMDD.format( date ) );	
			        outputStream.write(released.getBytes()) ;
			    } 
	    		
//	    	Commodity	
	    		if(row[24]==null||row[24].toString().trim().equals("null")||row[24].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("    ".getBytes());
	    		}
	    		else
	    		{			
	    								
						String commodity=row[24].toString();
						if(commodity.trim().equals("ART"))
						{
							commodity="FINE";
						}
						if(commodity.trim().equals("HIVA"))
						{
							commodity="HIVL";
						}
						if(commodity.trim().equals("MTRC"))
						{
							commodity="MTRC";
						}
						if(commodity.trim().equals("PIAN"))
						{
							commodity="PNO ";
						}					
						row[24]=commodity.toString();
					outputStream.write(row[24].toString().getBytes()) ;
					
			    }
 		
	    		outputStream.write("\n".getBytes());    		
	    		
		       }
			
					
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
		
	}
	
//	tstghist extraction
	@SkipValidation
	public void imfEstimateTstg(){
		imfEstimate=imfEntitlementsManager.getImfEstimateTstghist();				
		try
		{
			File file1 = new File("tstghist");		
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();				
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();	
				
			
				//SHIPID		
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
				{					
					outputStream.write("      ".getBytes());
				}
				else
				{
					outputStream.write(row[0].toString().getBytes()) ;
				}
				
				//FIELD	
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{					
					outputStream.write("                    ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
						
				//OLD		
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				//NEW			
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[3].toString().getBytes()) ;
				}
								
				//KEYER		
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				//DATE	
				String dated="        ";
	    		if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))
	    		{	    
	    			dated="        ";
	    			outputStream.write(dated.getBytes());
	    		}
	    		else
	    		{			
	    			Date date=(Date)row[5];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
					dated = new String( dateformatYYYYMMDD.format( date ) );	
			        outputStream.write(dated.getBytes()) ;
			    } 
	    		
					
				// TIME
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("        ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[6].toString().getBytes()) ;
			    } 
	    		
				//COMMENTS		
				if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
				{					
					outputStream.write("                                                                 ".getBytes());
				}
				else
				{
					String description = row[7].toString();					
					description = description.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
				    row[7]=(String)description;	
					outputStream.write(row[7].toString().getBytes()) ;
				}	
				
				//SHIPID		
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
				{					
					outputStream.write("  ".getBytes());
				}
				else
				{
					outputStream.write(row[8].toString().getBytes()) ;
				}
	    		
	    		outputStream.write("\n".getBytes());    		
	    		
		       }
			
					
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
	
	}
	
//	sshist extraction
	@SkipValidation
	public void imfEstimateSshist(){
		imfEstimate=imfEntitlementsManager.getImfEstimateSshist();				
		try
		{
			File file1 = new File("sshist");		
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();				
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();	
				
			
				//SHIPID		
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
				{					
					outputStream.write("      ".getBytes());
				}
				else
				{
					outputStream.write(row[0].toString().getBytes()) ;
				}
				
				//FIELD	
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{					
					outputStream.write("                    ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
						
				//OLD		
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				//NEW			
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[3].toString().getBytes()) ;
				}
								
				//KEYER		
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{					
					outputStream.write("          ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				//DATE	
				String dated="        ";
	    		if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))
	    		{	    
	    			dated="        ";
	    			outputStream.write(dated.getBytes());
	    		}
	    		else
	    		{		
	    			Date date=(Date)row[5];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
					dated = new String( dateformatYYYYMMDD.format( date ) );	
			        outputStream.write(dated.getBytes()) ;
			    } 
	    		
					
				// TIME
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("        ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[6].toString().getBytes()) ;
			    } 
	    		
				//COMMENTS		
				if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
				{					
					outputStream.write("                                                                 ".getBytes());
				}
				else
				{
					String description = row[7].toString();					
					description = description.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
					row[7]=(String)description;	
					outputStream.write(row[7].toString().getBytes()) ;
				}	
				
				//SHIPID		
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
				{					
					outputStream.write("  ".getBytes());
				}
				else
				{
					outputStream.write(row[8].toString().getBytes()) ;
				}
	    		
	    		outputStream.write("\n".getBytes());    		
	    		
		       }
			
					
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
	}
	
	
//	tbkstg extraction
	@SkipValidation
	public void imfEstimateTbkstg(){
	imfEstimate=imfEntitlementsManager.getImfEstimateTbkstg();			
	try
	{
		File file1 = new File("tbkstg");		
		if(!imfEstimate.isEmpty())
		{		
			HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();			
			response.setContentType("text");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
		Iterator it=imfEstimate.iterator();				
		while(it.hasNext())
	       {
			Object []row= (Object [])it.next();				
						
			//TICKET	
			if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
			{					
				outputStream.write("          ".getBytes());
			}
			else
			{
				outputStream.write(row[0].toString().getBytes()) ;
			}
			
			//LOCATION	
			if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
			{					
				outputStream.write("          ".getBytes());
			}
			else
			{
				outputStream.write(row[1].toString().getBytes()) ;
			}
					
			//VENDOR_REF_NO  
			if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
			{					
				outputStream.write("         ".getBytes());
			}
			else
			{
				outputStream.write(row[2].toString().getBytes()) ;
			}
			
			//WHAT		
			if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
			{					
				outputStream.write(" ".getBytes());
			}
			else
			{
				outputStream.write(row[3].toString().getBytes()) ;
			}
							
			//BY	
			if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
			{					
				outputStream.write("               ".getBytes());
			}
			else
			{
				outputStream.write(row[4].toString().getBytes()) ;
			}
			
			//DATED
			String dated="        ";
    		if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))
    		{	    
    			dated="        ";
    			outputStream.write(dated.getBytes());
    		}
    		else
    		{				
    			Date date=(Date)row[5];
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyyMMdd");
				dated = new String( dateformatYYYYMMDD.format( date ) );	
		        outputStream.write(dated.getBytes()) ;
		    } 
    		
				
			// STG_AMOUNT
    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
    		{	    
    			outputStream.write("         ".getBytes());
    		}
    		else
    		{				
    			outputStream.write(row[6].toString().getBytes()) ;
		    } 
    		
			//STG_HOW		
			if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
			{					
				outputStream.write(" ".getBytes());
			}
			else
			{
				outputStream.write(row[7].toString().getBytes()) ;
			}	
			
			//SHIPID		
			if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
			{					
				outputStream.write("      ".getBytes());
			}
			else
			{
				outputStream.write(row[8].toString().getBytes()) ;
			}	
			
    		//SHIP_LN_NO	
    		if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))
    		{	    
    			outputStream.write("  ".getBytes());
    		}
    		else
    		{				
    			outputStream.write(row[9].toString().getBytes()) ;
		    }
    		
    		
    		outputStream.write("\n".getBytes());    		
    		
	       }
		
				
		}
		else
		{
			String noRecord="No matching record found";
			HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();			
			response.setContentType("text");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			outputStream.write(noRecord.getBytes());
		}
	}
	catch(Exception e)
	{
		System.out.println("ERROR:"+e);
	} 
	
}
	
//	sscar extraction
	@SkipValidation
	public void imfEstimateSScar(){						
		imfEstimate=imfEntitlementsManager.getImfEstimateSscar();				
		try
		{
			File file1 = new File("sscarr");				
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();				
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();						
				
				//partner code
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
				{					
					outputStream.write("        ".getBytes());
				}
				else
				{
					outputStream.write(row[0].toString().getBytes()) ;
				}
				
				//name
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{					
					outputStream.write("                                        ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
						
				//address1
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{					
					outputStream.write("                              ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				//address2
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{					
					outputStream.write("                              ".getBytes());
				}
				else
				{
					outputStream.write(row[3].toString().getBytes()) ;
				}
								
				//address3
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{					
					outputStream.write("                                                             ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				//country
				if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
				{				
					outputStream.write("                    ".getBytes());
				}
				else
				{
					outputStream.write(row[5].toString().getBytes()) ;
				}
					
				// country code
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("   ".getBytes());
	    		}
	    		else if(row[6]!=null)
					{					
						String country=row[6].toString();
						if(country.trim().equals("ROU"))
						{
							country="ROM";
						}
						if(country.trim().equals("MNE"))
						{
							country="SCG";
						}
						if(country.trim().equals("TLS"))
						{
							country="TMP";
						}
						row[6]=country.toString();
						outputStream.write(row[6].toString().getBytes()) ;
			    } 
	    		
				//phone
				if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
				{					
					outputStream.write("                    ".getBytes());
				}
				else
				{
					outputStream.write(row[7].toString().getBytes()) ;
				}	
				
				//fax
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
				{					
					outputStream.write("                    ".getBytes());
				}
				else
				{
					outputStream.write(row[8].toString().getBytes()) ;
				}	
				
	    		//telex
	    		if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[9].toString().getBytes()) ;
			    }
	    		
	    		
	    		outputStream.write("\n".getBytes());    		
	    		
		       }
			
					
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
			
	}
	
	//ssagents extraction
	@SkipValidation
	public void imfEstimateFileAgent(){						
		imfEstimate=imfEntitlementsManager.getImfEstimateAgent();				
		try
		{
			File file1 = new File("ssagents");			
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();				
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();						
				
				//partner code
				if(row[0]==null||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))							
				{					
					outputStream.write("        ".getBytes());
				}
				else
				{
					outputStream.write(row[0].toString().getBytes()) ;
				}
				
				//terminal
				if(row[1]==null||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{					
					outputStream.write("                                                                           ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
						
				//lastname
				if(row[2]==null||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{					
					outputStream.write("                                                  ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				//address1
				if(row[3]==null||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{					
					outputStream.write("                              ".getBytes());
				}
				else
				{
					outputStream.write(row[3].toString().getBytes()) ;
				}
								
				//address2
				if(row[4]==null||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))				
				{					
					outputStream.write("                              ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				//address3
				if(row[5]==null||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
				{				
					outputStream.write("                                                             ".getBytes());
				}
				else
				{
					outputStream.write(row[5].toString().getBytes()) ;
				}
					
				//terminal country
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[6].toString().getBytes()) ;
			    } 
	    		
				//terminal country code
				if(row[7]==null||row[7].toString().trim().equals("")||row[7].toString().trim().equals("null"))				
				{					
					outputStream.write("         ".getBytes());
				}
				else if(row[7]!=null)
					{					
						String country=row[7].toString();
						if(country.trim().equals("ROU"))
						{
							country="ROM";
						}
						if(country.trim().equals("MNE"))
						{
							country="SCG";
						}
						if(country.trim().equals("TLS"))
						{
							country="TMP";
						}
					row[7]=country.toString()+"      ";
					outputStream.write(row[7].toString().getBytes()) ;
				}	
				
				//terminal city
				if(row[8]==null||row[8].toString().trim().equals("null")||row[8].toString().trim().equals("")||row[8]==null)				
				{					
					outputStream.write("               ".getBytes());
				}
				else
				{
					outputStream.write(row[8].toString().getBytes()) ;
				}	
				
	    		//terminal state
	    		if(row[9]==null||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("     ".getBytes());
	    		}
	    		else
	    		{		
	    			row[9]=row[9].toString()+"   ";
	    			outputStream.write(row[9].toString().getBytes()) ;
			    }
	    		
	    		//terminal zip
	    		if(row[10]==null||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[10].toString().getBytes()) ;
			    }
//	    		terminal phone
	    		if(row[11]==null||row[11].toString().trim().equals("null")||row[11].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[11].toString().getBytes()) ;
			    }
//	    		terminal fax
	    		if(row[12]==null||row[12].toString().trim().equals("null")||row[12].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("                    ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[12].toString().getBytes()) ;
			    }
//	    		updated on
	    		if(row[13]==null||row[13].toString().trim().equals("null")||row[13].toString().trim().equals(""))
	    		{	    
	    			outputStream.write("          ".getBytes());
	    		}
	    		else
	    		{				
	    			outputStream.write(row[13].toString().getBytes()) ;
			    }
	    		
	    		outputStream.write("\n".getBytes());    		
	    		
		       }
			
					
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
	}
	
	@SkipValidation
	public void imfEstimateFileActual() throws IOException {

		Double totalWeight = new Double(0);
		SimpleDateFormat df = new SimpleDateFormat("ddMMyyyy");
		imfEstimate = imfEntitlementsManager.getImfEstimateActual();

		File file1 = new File("sscosts");
		HttpServletResponse response = getResponse();
		ServletOutputStream outputStream = response.getOutputStream();
		response.setContentType("text");
		//response.setHeader("Cache-Control", "no-cache");
		response.setHeader("Content-Disposition", "attachment; filename="
				+ file1.getName() + ".txt" );
		response.setHeader("Pragma", "public");
		response.setHeader("Cache-Control", "max-age=0");
		if (imfEstimate.isEmpty()) {
			String noRecord = "No matching record found";
			outputStream.write(noRecord.getBytes());
		} else {
			StringBuilder sb = new StringBuilder("");
			Iterator it = imfEstimate.iterator();
			int lineNumber = 0;
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();

				sb.append("0");

				sb.append(isEmpty(row[0]) ? "      ": processDescription(row[0]));
				sb.append(isEmpty(row[1]) ? "  " : row[1].toString());
				sb.append(isEmpty(row[2]) ? "       " : row[2].toString());
				sb.append(isEmpty(row[3]) ? "    " : processCommodity(row[3]));
				sb.append(isEmpty(row[4]) ? "               " : row[4].toString());
				sb.append(isEmpty(row[5]) ? "         " : row[5].toString());
				sb.append(isEmpty(row[6]) ? "        " : df.format((Date) row[6]));
				sb.append(isEmpty(row[7]) ? "               " : row[7].toString());
				sb.append(isEmpty(row[8]) ? " " : row[8].toString());
				sb.append(isEmpty(row[9]) ? "        " : df.format((Date) row[9]));
				sb.append(isEmpty(row[10]) ? "        " : df.format((Date) row[10]));
				sb.append(isEmpty(row[13]) ? " " : row[13].toString()).append("    ");

				sb.append("    ").append("\n");

				outputStream.write(sb.toString().getBytes());
				
				//System.out.println("IMF sscosts extract - line " + lineNumber + " : " + sb.toString());
				if (row[12] != null) {
					//System.out.println("IMF sscosts extract - actualUpdated for id " + lineNumber + " : " + row[12].toString());
					imfEntitlementsManager.getImfEstimateActualUpdate(row[12]
							.toString());
				}

				String str = (String) row[4].toString();
				if (!str.equals("               ")) {
					totalWeight = totalWeight
							+ (double) Double.parseDouble(str.trim());
				}
				
				lineNumber++;
				sb=new StringBuilder("");

			}

			String totalFileNumber = computeTotalFileNumber(imfEstimate.size());
			String we = computeTotalWeight(totalWeight);

			outputStream.write(totalFileNumber.getBytes());
			outputStream.write(we.getBytes());
		}

	}
	
	private boolean isEmpty(Object field) {
		return field == null || field.toString().trim().equals("null")
				|| field.toString().trim().equals("");
	}
	
	private String processDescription(Object field) {
		String target1;
		int l = 48;
		String strDescription1 = (String) field;
		target1 = strDescription1;

		/*
		 * 
		   if (target1.charAt(0) == l) {
			if (l == 48) {
				char e = (char) l;

				int i = strDescription1.indexOf(e);

				if (i <= 0) {
					target1 = strDescription1;
				} else if (i > 0) {
					target1 = strDescription1.substring(0, i);
					strDescription1 = target1;
				}

				target1 = target1.replaceFirst("0", " ");
			}
		}*/
		if (target1.equals("")) {
			target1 = "       ";
		}

		return target1;
	}
	
	private String processCommodity(Object field) {
		String commodity = field.toString();
		if (commodity.trim().equals("ART")) {
			commodity = "FINE";
		}
		if (commodity.trim().equals("HIVA")) {
			commodity = "HIVL";
		}
		if (commodity.trim().equals("MTRC")) {
			commodity = "MTRC";
		}
		if (commodity.trim().equals("PIAN")) {
			commodity = "PNO ";
		}
		// System.out.println("\n\nCOMMODITY:"+commodity);
		return commodity;
	}
	
	private String computeTotalWeight(Double totalWeight) {
		String we;
		DecimalFormat df1 = new DecimalFormat("0.00");
		we = df1.format(totalWeight);

		// total weight

		int countFreight1 = we.length();
		int diffFreight1 = (15 - countFreight1);
		String temp2 = "";
		for (int i = 1; i <= diffFreight1; i++) {
			temp2 = " " + temp2;

		}
		we = temp2 + we;
		return we;
	}

	private String computeTotalFileNumber(int totalNumberofRecord) {
		Integer integ1 = (Integer) totalNumberofRecord;
		String freightOutPut = (String) integ1.toString();

		int countFreight = freightOutPut.length();
		int diffFreight = (5 - countFreight);
		String temp1 = "";
		for (int i = 1; i <= diffFreight; i++) {
			temp1 = " " + temp1;

		}
		freightOutPut = temp1 + freightOutPut;

		String totalFileNumber = "1INV" + freightOutPut;
		return totalFileNumber;
	} 
	
	@SkipValidation
	public void imfEstimateFileExtract(){
		
		
		Double  weight1=new Double(0);
		Double  weight2=new Double(0);
		Double  weight3=new Double(0);
		Double  weight4=new Double(0);
		Double  weight5=new Double(0);
		Double  weight6=new Double(0);
		Double  weight7=new Double(0);
		Double  weight8=new Double(0);
		Double  weight9=new Double(0);
		//FOR OFFICE
		Double  weight10=new Double(0);
		Double  weight11=new Double(0);
		Double  weight12=new Double(0);
		
		Double  totalWeight1=new Double(0);		
		Double  totalWeight2=new Double(0);		
		Double  totalWeight3=new Double(0);
		Double  totalWeight4=new Double(0);		
		Double  totalWeight5=new Double(0);		
		Double  totalWeight6=new Double(0);
		Double  totalWeight7=new Double(0);		
		Double  totalWeight8=new Double(0);		
		Double  totalWeight9=new Double(0);
		//FOR OFFICE
		Double  totalWeight10=new Double(0);		
		Double  totalWeight11=new Double(0);		
		Double  totalWeight12=new Double(0);
		
		Double allWeight =new Double(0);
		
		Double freight1=new Double(0.00);
		Double freight2=new Double(0.00);
		Double freight3=new Double(0.00);
		Double freight4=new Double(0.00);
		Double freight5=new Double(0.00);
		Double freight6=new Double(0.00);
		Double freight7=new Double(0.00);
		Double freight8=new Double(0.00);
		Double freight9=new Double(0.00);
		//FOR OFFICE
		Double freight10=new Double(0.00);
		Double freight11=new Double(0.00);
		Double freight12=new Double(0.00);
		
		Double totalFreight1=new Double(0.00);
		Double totalFreight2=new Double(0.00);
		Double totalFreight3=new Double(0.00);
		Double totalFreight4=new Double(0.00);
		Double totalFreight5=new Double(0.00);
		Double totalFreight6=new Double(0.00);
		Double totalFreight7=new Double(0.00);
		Double totalFreight8=new Double(0.00);
		Double totalFreight9=new Double(0.00);
		//FOR OFFICE
		Double totalFreight10=new Double(0.00);
		Double totalFreight11=new Double(0.00);
		Double totalFreight12=new Double(0.00);
		
		Double allFreight=new Double(0.00);	
		
		String we="";
		String fr="";
		imfEstimateList=imfEntitlementsManager.getImfEstimateList();		
		int totalNumberofRecord=0;
		String sequenceNumber="";
		
		
		try
		{
			File file1 = new File("ENTI237");
			
			String text= new String();
			String text1= new String();
			String text2= new String();
			String text3= new String();
			String text4= new String();
			String text5= new String();
			String text6= new String();
			String text7= new String();
			String text8= new String();
			//FOR OFFICE
			String text9= new String();
			String text10= new String();
			String text11= new String();
			
			HttpServletResponse response = getResponse();
			ServletOutputStream outputStream = response.getOutputStream();			
			response.setContentType("text");
			//response.setHeader("Cache-Control", "no-cache");			
			response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			if(!imfEstimateList.isEmpty())
			{		
			Iterator it=imfEstimateList.iterator();
			while(it.hasNext())
		       {			
				
				Object []row= (Object [])it.next();	
				
				
				
				if(row[0]==null)				
				{
					row[0]="      ";
				}
				String billToReference="       ";
	    		if(row[0]==null ||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))
				{					
	    			billToReference="       ";
				}
				else
				{
					String original=row[0].toString().trim();					
					//String strSub= original.substring(1, original.length());
					//char strFirst=original.charAt(0);
					billToReference="0"+""+original;
					int countWeight=billToReference.length();
					int diff = (7-countWeight);
					String temp="";
					for(int i=1;i<=diff;i++)
					{
						temp=temp+" ";
						
					}
					billToReference=billToReference+temp;
					
				}
	    		
				
								
				if(row[14]==null)				
				{
					row[14]="                ";
				}
				
				
								
				String nowYYYYMMDD="      ";
	    		if(row[13]==null||row[13].toString().trim().equals("null")||row[13].toString().trim().equals(""))
	    		{
	    			
	    			nowYYYYMMDD="        ";
	    		}
	    		if(row[13]!=null)
	    		{				
	    			Date date=(Date)row[13];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
					nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );	
			    } 
	    		
	    		/*String billToReference="       ";
	    		if(row[0]==null ||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))
				{					
	    			billToReference="       ";
				}
				else
				{
					String original=row[0].toString().trim();					
					String strSub= original.substring(1, original.length());
					char strFirst=original.charAt(0);
					billToReference=strFirst+" "+strSub;
					int countWeight=billToReference.length();
					int diff = (7-countWeight);
					String temp="";
					for(int i=1;i<=diff;i++)
					{
						temp=temp+" ";
						
					}
					billToReference=billToReference+temp;
					
				}
	    		*/
		        
//		      replacing value (0) of billToReference
	    		/*int l=48;
	    		String target1=new String("");	    		
	    		String strDescription1 =(String)row[0];
	    			    		
	    		if(l==48){
	    			char e=(char)l;
	    		
	    		int i=	strDescription1.indexOf(e);
	    		
	    				if(i<=0)
	    				{
	    					target1=strDescription1;	    					
	    				}else if(i>0)
	    				{
	    					target1=strDescription1.substring(0,i);
	    					strDescription1=target1;
	    				} 
	    				
	    			
	    		target1=target1.replaceFirst("0"," ");
	    		}
	    		if(target1.equals(""))
	    		{
	    			target1="       ";
	    		}
	    		    	*/
		       // System.out.println("\n\nrow[1]"+row[1]);
		       // System.out.println("\n\nrow[2]"+row[2]);
		        
		        if(row[1]!=null&&row[2]!=null)
		        {
		        	if(!row[1].toString().trim().equals("0.00")&&!row[2].toString().trim().equals("0.00"))
		        	{
		        		if(!row[1].toString().trim().equals("")&&!row[2].toString().trim().equals(""))
			        	{
			    		text=text+billToReference+"FREIGHTPERS"+row[1].toString()+row[2].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              A"+"\n";
			    		outputStream.write(text.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[1]!=null)
								{
									String str=row[1].toString();
									if(!str.equalsIgnoreCase("null"))
									{
										weight1 =Double.parseDouble(str);  
										totalWeight1=totalWeight1+weight1;
									}
								}			    		
					    		
					    		if(row[2]!=null)
								{
						    		String str3=(String)row[2].toString(); 
						    		if(!str3.equalsIgnoreCase("null"))
									{
						    			freight1= Double.parseDouble(str3); 
							    		totalFreight1=totalFreight1+freight1; 
									}
								}
			    		
			    		
			        	}
		        	}
		        }
		        
		        if(row[1]!=null&&row[3]!=null)
		        {
		        	if(!row[1].toString().trim().equals("0.00")&&!row[3].toString().trim().equals("0.00"))
		        	{
		        		if(!row[1].toString().trim().equals("")&&!row[3].toString().trim().equals(""))
			        	{
			    		text1=text1+billToReference+"ORIGIN PERS"+row[1].toString()+row[3].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              A"+"\n";
			    		outputStream.write(text1.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[1]!=null)
								{
									String str=row[1].toString();
									if(!str.equalsIgnoreCase("null"))
									{
										weight2 =Double.parseDouble(str);  
										totalWeight2=totalWeight2+weight2;
									}
								}	
					    		
					    		if(row[3]!=null)
								{
						    		String str31=(String)row[3].toString();
						    		if(!str31.trim().equalsIgnoreCase("null"))
									{
						    			freight2=Double.parseDouble(str31.trim());
							    		totalFreight2=totalFreight2+freight2;
							    		
									}
								}
			    		
			        	}
		        	}
	    			
	    			
		        }
		        
		        if(row[1]!=null&&row[4]!=null)
		        {
		        	if(!row[1].toString().trim().equals("0.00")&&!row[4].toString().trim().equals("0.00"))
		        	{
		        		if(!row[1].toString().trim().equals("")&&!row[4].toString().trim().equals(""))
			        	{
			    		text2=text2+billToReference+"DESTIN PERS"+row[1].toString()+row[4].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              A"+"\n";
			    		outputStream.write(text2.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
				    		if(row[1]!=null)
							{
								String str=row[1].toString();
								if(!str.equalsIgnoreCase("null"))
								{
									weight3 =Double.parseDouble(str);  
									totalWeight3=totalWeight3+weight3;
								}
							}	
				    		
				    		if(row[4]!=null)
							{
					    		String str32=(String)row[4].toString();
					    		if(!str32.trim().equalsIgnoreCase("null"))
								{
					    			freight3=Double.parseDouble(str32.trim());
						    		totalFreight4=totalFreight4+freight3;		    		
								}
							}
				    		
			    		
					    }
		        	}
		        }
		        
		        if(row[5]!=null&&row[6]!=null)
		        {
		        	if(!row[5].toString().trim().equals("0.00")&&!row[6].toString().trim().equals("0.00"))
		        	{
		        		if(!row[5].toString().trim().equals("")&&!row[6].toString().trim().equals(""))
			        	{
			    		text3=text3+billToReference+"FREIGHTHHG"+" "+row[5].toString()+row[6].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text3.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[5]!=null)
								{
									String str1=(String)row[5].toString();				
									if(!str1.equalsIgnoreCase("null"))
									{
										weight4 =(double) Double.parseDouble(str1.trim());
							    		totalWeight4=totalWeight4+weight4;	   
									}
								}
					    		
					    		if(row[6]!=null)
								{
						    		String str4=(String)row[6].toString();
						    		if(!str4.trim().equalsIgnoreCase("null"))
									{
						    			freight4=Double.parseDouble(str4.trim());
							    		totalFreight4=totalFreight4+freight4;			    		
									}
								}
			    		
			        	}
		        	}
		        }
		        
		        if(row[5]!=null && row[7]!=null)
		        {
		        	
		        	if(!row[5].toString().trim().equals("0.00") && !row[7].toString().trim().equals("0.00"))
		        	{		        		
		        		if(!row[5].toString().trim().equals("") && !row[7].toString().trim().equals(""))
		        		{				        			
		        		text4=text4+billToReference+"ORIGIN HHG"+" "+row[5].toString()+row[7].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text4.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
					    		if(row[5]!=null)
								{
									String str1=(String)row[5].toString();				
									if(!str1.equalsIgnoreCase("null"))
									{
										weight5 =(double) Double.parseDouble(str1.trim());
							    		totalWeight5=totalWeight5+weight5;	   
									}
								}
					    		
					    		if(row[7]!=null)
								{
						    		String str41=(String)row[7].toString();
						    		if(!str41.trim().equalsIgnoreCase("null"))
									{
						    			freight5=Double.parseDouble(str41.trim());
							    		totalFreight5=totalFreight5+freight5;
							    		
									}
								}
			        	}
		        	}
	    		
		        }
		        
		        if(row[5]!=null&&row[8]!=null)
		        {
		        	if(!row[5].toString().trim().equals("0.00")&&!row[8].toString().trim().equals("0.00"))
		        	{
		        		if(!row[5].toString().trim().equals("")&&!row[8].toString().trim().equals(""))
			        	{
			    		text5=text5+billToReference+"DESTIN HHG"+" "+row[5].toString()+row[8].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text5.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
					    		if(row[5]!=null)
								{
									String str1=(String)row[5].toString();				
									if(!str1.equalsIgnoreCase("null"))
									{
										weight6 =(double) Double.parseDouble(str1.trim());
							    		totalWeight6=totalWeight6+weight6;	   
									}
								}
					    		
					    		if(row[8]!=null)
								{
						    		String str42=(String)row[8].toString();
						    		if(!str42.trim().equalsIgnoreCase("null"))
									{
						    			freight6=Double.parseDouble(str42.trim());
							    		totalFreight6=totalFreight6+freight6;
							    		
									}
								}
			        	}
		        	}
		        }
		        
		        if(row[9]!=null&&row[10]!=null)
		        {
		        	if(!row[9].toString().trim().equals("0.00")&&!row[10].toString().trim().equals("0.00"))
		        	{
		        		if(!row[9].toString().trim().equals("")&&!row[10].toString().trim().equals(""))
			        	{
			    		text6=text6+billToReference+"FREIGHTAUTO"+row[9].toString()+row[10].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text6.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[9]!=null)
								{
									String str2=(String)row[9].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight7 =(double) Double.parseDouble(str2.trim());
							    		totalWeight7=totalWeight7+weight7;
									}
								}
					    		
					    		if(row[10]!=null)
								{
						    		String str5=(String)row[10].toString();
						    		if(!str5.trim().equalsIgnoreCase("null"))
									{
						    			freight7=Double.parseDouble(str5.trim());
						    			totalFreight7=totalFreight7+freight7;
							    		
									}
								}
			    		
			        	}
		        	}
		        }
		        
		        if(row[9]!=null&&row[11]!=null)
		        {
		        	if(!row[9].toString().trim().equals("0.00")&&!row[11].toString().trim().equals("0.00"))
		        	{
		        		if(!row[9].toString().trim().equals("")&&!row[11].toString().trim().equals(""))
			        	{
			    		text7=text7+billToReference+"ORIGIN AUTO"+row[9].toString()+row[11].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text7.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[9]!=null)
								{
									String str2=(String)row[9].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight8 =(double) Double.parseDouble(str2.trim());
							    		totalWeight8=totalWeight8+weight8;
									}
								}
					    		
					    		if(row[11]!=null)
								{
							    		String str51=(String)row[11].toString();
							    		if(!str51.trim().equalsIgnoreCase("null"))
										{
							    			freight8=Double.parseDouble(str51.trim());
							    			totalFreight8=totalFreight8+freight8;
								    		
										}
								}
			        	}
		        	}
		        }
		        
		        if(row[9]!=null&&row[12]!=null)
		        {
		        	if(!row[9].toString().trim().equals("0.00")&&!row[12].toString().trim().equals("0.00"))
		        	{
		        		if(!row[9].toString().trim().equals("")&&!row[12].toString().trim().equals(""))
			        	{
			    		text8=text8+billToReference+"DESTIN AUTO"+row[9].toString()+row[12].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text8.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[9]!=null)
								{
									String str2=(String)row[9].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight9 =(double) Double.parseDouble(str2.trim());
							    		totalWeight9=totalWeight9+weight9;
									}
								}
					    		
					    		if(row[12]!=null)
								{
						    		String str52=(String)row[12].toString();
						    		if(!str52.trim().equalsIgnoreCase("null"))
									{
						    			freight9=Double.parseDouble(str52.trim());
							    		totalFreight9=totalFreight9+freight9;		    		
									}
								}
			        	}
		        	}
		        }
		       
		        
		        if(row[16]!=null&&row[17]!=null)
		        {
		        	if(!row[16].toString().trim().equals("0.00")&&!row[17].toString().trim().equals("0.00"))
		        	{
		        		if(!row[16].toString().trim().equals("")&&!row[17].toString().trim().equals(""))
			        	{
			    		text9=text9+billToReference+"FREIGHTOFC "+row[16].toString()+row[17].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text9.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[16]!=null)
								{
									String str2=(String)row[16].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight10 =(double) Double.parseDouble(str2.trim());
							    		totalWeight10=totalWeight10+weight10;
									}
								}
					    		
					    		if(row[17]!=null)
								{
						    		String str5=(String)row[17].toString();
						    		if(!str5.trim().equalsIgnoreCase("null"))
									{
						    			freight10=Double.parseDouble(str5.trim());
						    			totalFreight10=totalFreight10+freight10;
							    		
									}
								}
			    		
			        	}
		        	}
		        }
		        
		        if(row[16]!=null&&row[18]!=null)
		        {
		        	if(!row[16].toString().trim().equals("0.00")&&!row[18].toString().trim().equals("0.00"))
		        	{
		        		if(!row[16].toString().trim().equals("")&&!row[18].toString().trim().equals(""))
			        	{
			    		text10=text10+billToReference+"ORIGIN OFC "+row[16].toString()+row[18].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text10.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[16]!=null)
								{
									String str2=(String)row[16].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight11 =(double) Double.parseDouble(str2.trim());
							    		totalWeight11=totalWeight11+weight11;
									}
								}
					    		
					    		if(row[18]!=null)
								{
							    		String str51=(String)row[18].toString();
							    		if(!str51.trim().equalsIgnoreCase("null"))
										{
							    			freight11=Double.parseDouble(str51.trim());
							    			totalFreight11=totalFreight11+freight11;
								    		
										}
								}
			        	}
		        	}
		        }
		        
		        if(row[16]!=null&&row[19]!=null)
		        {
		        	if(!row[16].toString().trim().equals("0.00")&&!row[19].toString().trim().equals("0.00"))
		        	{
		        		if(!row[16].toString().trim().equals("")&&!row[19].toString().trim().equals(""))
			        	{
			    		text11=text11+billToReference+"DESTIN OFC "+row[16].toString()+row[19].toString()+nowYYYYMMDD+"                                                          "+row[14].toString()+"              S"+"\n";
			    		outputStream.write(text11.getBytes());
			    		totalNumberofRecord=totalNumberofRecord+1;
			    		
					    		if(row[16]!=null)
								{
									String str2=(String)row[16].toString();				
									if(!str2.equalsIgnoreCase("null"))
									{
										weight12 =(double) Double.parseDouble(str2.trim());
							    		totalWeight12=totalWeight12+weight12;
									}
								}
					    		
					    		if(row[19]!=null)
								{
						    		String str52=(String)row[19].toString();
						    		if(!str52.trim().equalsIgnoreCase("null"))
									{
						    			freight12=Double.parseDouble(str52.trim());
							    		totalFreight12=totalFreight12+freight12;		    		
									}
								}
			        	}
		        	}
		        }
		        
		        
		        
		        //office entitlement end
		        
				text="";
				text1="";
				text2="";
				text3="";
				text4=""; 
				text5="";
				text6="";
				text7="";
				text8="";
				// for office
				text9="";
				text10="";
				text11="";
				
				
				// used for updation
				if(row[15]!=null)
				{	
				imfEntitlementsManager.getImfEstimateListUpdate(row[15].toString());
				}
				
			}
			
			//allWeight = (totalWeight2)+(totalWeight3);
			allWeight = (totalWeight1)+(totalWeight2)+(totalWeight3)+(totalWeight4)+(totalWeight5)+(totalWeight6)+(totalWeight7)+(totalWeight8)+(totalWeight9)+(totalWeight10)+(totalWeight11)+(totalWeight12);
			allFreight=(totalFreight1)+(totalFreight2)+(totalFreight3)+(totalFreight4)+(totalFreight5)+(totalFreight6)+(totalFreight7)+(totalFreight8)+(totalFreight9)+(totalFreight10)+(totalFreight11)+(totalFreight12);
			
			
			//total number of record calculation
			DecimalFormat df3 = new DecimalFormat("00000");
			String totalRecord=df3.format(totalNumberofRecord);
			
			Integer integ3=Integer.parseInt(totalRecord);			
			String target1=(String)integ3.toString();
			
			int countRecord=target1.length();
			int diffRecord = (5-countRecord);
			String temp2="";
			for(int i=1;i<=diffRecord;i++)
			{
				temp2=temp2+" ";
				
			}
			target1=temp2+target1; 			
    		String totalFileNumber="1ENT"+target1;
    		//total number of record calculation end
    		
    		// weight calculation	
    		DecimalFormat df4 = new DecimalFormat("00000");
			we=df4.format(allWeight);
			int countFreight1=we.length();
			int diffFreight1 = (15-countFreight1);
			String temp3="";
			for(int i=1;i<=diffFreight1;i++)
			{
				temp3=" "+temp3;
				
			}
			we=temp3+we;
			
			//freight calculation
			DecimalFormat df5 = new DecimalFormat("00000.00");
			
			fr=df5.format(allFreight);
			
			int countFreight2=fr.length();
			int diffFreight2 = (15-countFreight2);
			String temp4="";
			for(int i=1;i<=diffFreight2;i++)
			{
				temp4=" "+temp4;
				
			}
			fr=temp4+fr;
			
			
			
	        outputStream.write(totalFileNumber.getBytes());	
			outputStream.write(we.getBytes());			
			outputStream.write(fr.getBytes());
			
		 }
			else
			{
				String noRecord="No matching record found";							
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
				
			}
			
			
			
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
		
	}
	
	@SkipValidation
	public String imfEstimate(){		
		return SUCCESS;
	}
	@SkipValidation
	public void imfEstimateFile(){		
		Double  weight=new Double(0);
		Double  totalWeight=new Double(0);
		String we="";
		String fr="";
		Double freight=new Double(0);
		Double totalFreight=new Double(0);	
		
		imfEstimate=imfEntitlementsManager.getImfEstimate();		
		int totalNumberofRecord=imfEstimate.size();	
		
		try
		{
			File file1 = new File("esti237");			
			if(!imfEstimate.isEmpty())
			{		
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
			Iterator it=imfEstimate.iterator();		
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
				
				if(row[0]==null ||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))
				{					
					outputStream.write("       ".getBytes());
				}
				else
				{
					String str=row[0].toString().trim();					
					//String strSub= str.substring(1, str.length());
					//char strFirst=str.charAt(0);
					String billToReference="0"+""+str;
					int countWeight=billToReference.length();
					int diff = (7-countWeight);
					String temp="";
					for(int i=1;i<=diff;i++)
					{
						temp=temp+" ";
						
					}
					billToReference=billToReference+temp;
					outputStream.write(billToReference.getBytes());
				}
				//outputStream.write("0".getBytes()) ;
				
				//replacing value (0) of billToReference
				/*String target1="      ";
				if(row[0]==null ||row[0].toString().trim().equals("null")||row[0].toString().trim().equals(""))
				{
					outputStream.write(target1.getBytes());
				}
				else
						{
			    		int l=48;			    				
			    		String strDescription1 =(String)row[0];
			    		target1=strDescription1;
			    		if(target1.charAt(0)==l)
			    		{
			    		if(l==48){
			    			char e=(char)l;
			    		
			    		int i=	strDescription1.indexOf(e);
			    		
			    				if(i<=0)
			    				{
			    					target1=strDescription1;	    					
			    				}else if(i>0)
			    				{
			    					target1=strDescription1.substring(0,i);
			    					strDescription1=target1;
			    				} 
			    				
			    			
			    		target1=target1.replaceFirst("0"," ");
			    		}
			    		}
			    		if(target1.equals(""))
			    		{
			    			target1="       ";
			    		}
						outputStream.write(target1.getBytes());
						
						
						}
				*/
				if(row[1]==null ||row[1].toString().trim().equals("null")||row[1].toString().trim().equals(""))				
				{
					
					outputStream.write("  ".getBytes());
				}
				else
				{
					outputStream.write(row[1].toString().getBytes()) ;
				}
				
			
				
				if(row[2]==null ||row[2].toString().trim().equals("null")||row[2].toString().trim().equals(""))				
				{
					
					outputStream.write("       ".getBytes());
				}
				else
				{
					outputStream.write(row[2].toString().getBytes()) ;
				}
				
				
				if(row[3]==null ||row[3].toString().trim().equals("null")||row[3].toString().trim().equals(""))				
				{
					
					outputStream.write("    ".getBytes());
				}
				else
				{
					String commodity=row[3].toString();
					if(commodity.trim().equals("ART"))
					{
						commodity="FINE";
					}
					if(commodity.trim().equals("HIVA"))
					{
						commodity="HIVL";
					}
					if(commodity.trim().equals("MTRC"))
					{
						commodity="MTRC";
					}
					if(commodity.trim().equals("PIAN"))
					{
						commodity="PNO ";
					}
					//System.out.println("\n\nCOMMODITY:"+commodity);
					outputStream.write(commodity.getBytes()) ;
				}
				
				
				
				if(row[4]==null ||row[4].toString().trim().equals("null")||row[4].toString().trim().equals(""))			
					
				{
					
					outputStream.write("               ".getBytes());
				}
				else
				{
					outputStream.write(row[4].toString().getBytes()) ;
				}
				
				
				
				if(row[5]==null ||row[5].toString().trim().equals("null")||row[5].toString().trim().equals(""))				
				{
					
					outputStream.write("               ".getBytes());
				}
				else
				{
					outputStream.write(row[5].toString().getBytes()) ;
				}			
				
				
				
				String nowYYYYMMDD="      ";
	    		if(row[6]==null||row[6].toString().trim().equals("null")||row[6].toString().trim().equals(""))
	    		{
	    			
	    			nowYYYYMMDD="        ";
	    			
	    		}
	    		if(row[6]!=null)
	    		{				
	    			Date date=(Date)row[6];
					SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("ddMMyyyy");
			        nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );	
			        
			    }
	    		outputStream.write(nowYYYYMMDD.getBytes()) ;
	    		
	    		
	    		
				if(row[7]==null ||row[7].toString().trim().equals("null")||row[7].toString().trim().equals(""))				
				{
					
					outputStream.write("   ".getBytes());
				}
				else
				{
					outputStream.write(row[7].toString().getBytes()) ;
				}			
				// description
				
				if(row[8]!=null)
				{
					String description = row[8].toString();					
					description = description.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
				    row[8]=(String)description;				  
				}
				//System.out.println("\n\n row[8]===========>"+row[8]);
				//replacing value of description 
	    		
	    		int d=48;
	    		String target="                                                       ";	    		
	    		if(row[8]!=null)
	    		{
		    		String strDescription =(String)row[8];
		    			    		
		    		while(d<=64){
		    			char e=(char)d;
		    		int i=	strDescription.indexOf(e);	    		 
		    				if(i<=0)
		    				{
		    					target=strDescription;	    					
		    				}else if(i>0)
		    				{
		    					target=strDescription.substring(0,i);
		    					strDescription=target;
		    				} 
		    				d++;
		    			
		    		target=target.replaceAll("0", " ").replaceAll("1", " ").replaceAll("2"," ").replaceAll("3", " ").replaceAll("4", " ").replaceAll("5", " ").replaceAll("6", " ").replaceAll("7", " ").replaceAll("8", " ").replaceAll("9", " ").replaceAll(":", " ").replaceAll("@", " ").replaceAll(";", " ").replaceAll("<", " ").replaceAll("=", " ").replaceAll(">", " ").replaceAll(" ", " ");
		    		int x=target.length();
		    		int y=(55-x);
		    		while(y!=0)
		    		{
		    			target=target+" ";
		    			y--;
		    		}
		    			    		
		    		}
		    		if(target.equals(""))
		    		{
		    			target="                                                       ";
		    			
		    		}
		    		outputStream.write(target.getBytes()) ;
	    		}
	    		else
	    		{
	    			outputStream.write(target.getBytes()) ;
	    		}	    		
	    		
	    	
	    		
				
				if(row[9]==null ||row[9].toString().trim().equals("null")||row[9].toString().trim().equals(""))				
				{
					
					outputStream.write("                              ".getBytes());
				}
				else
				{
					outputStream.write(row[9].toString().getBytes()) ;
				}
				
				
				
				if(row[10]==null ||row[10].toString().trim().equals("null")||row[10].toString().trim().equals(""))				
				{
					
					outputStream.write(" ".getBytes());
				}
				else
				{
					outputStream.write(row[10].toString().getBytes()) ;
				}
				
				
				outputStream.write("\n".getBytes());				
				
				//used for updation
				if(row[12]!=null)
				{
				imfEntitlementsManager.getImfEstimateUpdate(row[12].toString());
				
				}
				
				if(row[4]!=null)
				{
					String str=(String)row[4].toString();					
					weight =(double) Double.parseDouble(str.trim());  
					totalWeight=totalWeight+weight;
					
				}
	    		
				if(row[5]!=null)
				{
					String str1=(String)row[5].toString();						
					freight =(double) Double.parseDouble(str1.trim());						
					totalFreight=totalFreight+freight;	
					
				}
	    		
		       }
			
			//weight calculation
			DecimalFormat df1 = new DecimalFormat("0000000000000000");
			we=df1.format(totalWeight);
			
			Integer integ=Integer.parseInt(we);			
			String weightOutPut=(String)integ.toString();
			
			int countWeight=weightOutPut.length();
			int diff = (15-countWeight);
			String temp="";
			for(int i=1;i<=diff;i++)
			{
				temp=temp+" ";
				
			}
			weightOutPut=temp+weightOutPut;
		    
			//freight calculation
			DecimalFormat df2 = new DecimalFormat("00000000000.00");
			fr=df2.format(totalFreight);
			
			Double integ1=Double.parseDouble(fr);			
			//String freightOutPut=(String)integ1.toString();
			//totalFreight=(double)Math.round(totalFreight*100)/100;
			//String freightOutPut = totalFreight.toString();
			String freightOutPut = Double.toString(integ1);
			
			int countFreight=freightOutPut.length();
			int diffFreight = (15-countFreight);
			String temp1="";
			for(int i=1;i<=diffFreight;i++)
			{
				temp1=temp1+" ";
				
			}
			freightOutPut=temp1+freightOutPut;
		    
			
			//total number of record calculation
			DecimalFormat df3 = new DecimalFormat("00000");
			String totalRecord=df3.format(totalNumberofRecord);
			
			Integer integ3=Integer.parseInt(totalRecord);			
			String target1=(String)integ3.toString();
			
			int countRecord=target1.length();
			int diffRecord = (5-countRecord);
			String temp2="";
			for(int i=1;i<=diffRecord;i++)
			{
				temp2=temp2+" ";
				
			}
			target1=temp2+target1;
    		String totalFileNumber="1EST"+target1;    		
    		
			outputStream.write(totalFileNumber.getBytes());			
			outputStream.write(weightOutPut.getBytes());			
			outputStream.write(freightOutPut.getBytes());
			}
			else
			{
				String noRecord="No matching record found";
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();			
				response.setContentType("text");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".txt");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				outputStream.write(noRecord.getBytes());
				
			}
			
			
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);
		} 
	}
	
	
	
	@SkipValidation
	public void officeDocMoveNew()	{
		StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();	
		String header=new String();
		List officeDocMoveList=imfEntitlementsManager.officeDocMoveNew(sessionCorpID);
		//System.out.println("\n\nSize::"+officeDocMoveList.size());
		try		{
			if(!officeDocMoveList.isEmpty()){	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OfficeDocMove");
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");	
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				out.println("filename="+file1.getName()+".csv"+"\t The number of line  \t"+officeDocMoveList.size()+"\t is extracted");
		Iterator it=officeDocMoveList.iterator();
		outputStream.write("SHIPNUM,".getBytes());
		outputStream.write("SHIP_LN_NBR,".getBytes());
		outputStream.write("VNDR_REF#,".getBytes());
		outputStream.write("AIRWAYBILL#,".getBytes());
		outputStream.write("D_AIR_CODE,".getBytes());
		outputStream.write("D_AIRLINE,".getBytes());
		outputStream.write("D_FLGHT_NO,".getBytes());
		outputStream.write("D_FLGHT_TO,".getBytes()); 
		outputStream.write("D_FLGHTETA,".getBytes());
		outputStream.write("A_AIR_CODE,".getBytes());
		outputStream.write("A_AIRLINE,".getBytes());
		outputStream.write("A_FLGHT_NO,".getBytes());
		outputStream.write("A_FLGHT_TO,".getBytes());
		outputStream.write("A_FLGHTETA,".getBytes());
		outputStream.write("ITNUM,".getBytes());
		outputStream.write("PIECES,".getBytes());
		outputStream.write("KILOS,".getBytes());
		outputStream.write("NOTIF_RECP,".getBytes()); 
		outputStream.write("SHIP_CLEAR,".getBytes());
		outputStream.write("DELIV_RECP,".getBytes());
		outputStream.write("SIGN_LNAME,".getBytes());
		outputStream.write("SIGN_FNAME,".getBytes());
		outputStream.write("DELV_ROOM,".getBytes());
		outputStream.write("INVNUM,".getBytes());
		outputStream.write("CHRG_1DESC,".getBytes());
		outputStream.write("CHRG_1AMT,".getBytes());
		outputStream.write("CHRG_2DESC,".getBytes());
		outputStream.write("CHRG_2AMT,".getBytes());
		outputStream.write("CHRG_3DESC,".getBytes());
		outputStream.write("CHRG_3AMT,".getBytes());
		outputStream.write("CHRG_4DESC,".getBytes());
		outputStream.write("CHRG_4AMT,".getBytes());
		outputStream.write("CHRG_5DESC,".getBytes());
		outputStream.write("CHRG_5AMT,".getBytes());
		outputStream.write("CHRG_6DESC,".getBytes());
		outputStream.write("CHRG_6AMT,".getBytes());
		outputStream.write("CHRG_7DESC,".getBytes());
		outputStream.write("CHRG_7AMT,".getBytes());
		outputStream.write("CHRG_8DESC,".getBytes());
		outputStream.write("CHRG_8AMT,".getBytes());
		outputStream.write("CHRG_9DESC,".getBytes());
		outputStream.write("CHRG_9AMT,".getBytes());
		outputStream.write("CHRG_10DESC,".getBytes());
		outputStream.write("CHRG_10AMT,".getBytes());
		outputStream.write("CHRG_11DESC,".getBytes());
		outputStream.write("CHRG_11AMT,".getBytes());
		outputStream.write("CHRG_12DESC,".getBytes());
		outputStream.write("CHRG_12AMT,".getBytes());
		outputStream.write("Invoice_date".getBytes()); 
		outputStream.write("\r\n".getBytes());
		while(it.hasNext())
	       {
			Object []row= (Object [])it.next();
				if(row[0]!=null){
					outputStream.write((row[0].toString()+",").getBytes()) ;
				}			
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[1]!=null)				
				{
					//String shipLineNo= row[1].toString();
					//String shipLineNoResult="'"+shipLineNo;
					outputStream.write((row[1]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[2]!=null)				
				{
					//String shipNo= row[2].toString();
					//String shipNoResult="'"+shipNo;
					outputStream.write((row[2]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[3]!=null)				
				{
					//String airWayBillNo= row[3].toString();
					//String airWayBillNoResult="'"+airWayBillNo;
					outputStream.write((row[3]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[5]!=null)				
				{
					//String dAOMNINo= row[5].toString();
					//String dAOMNINoResult="'"+dAOMNINo;
					outputStream.write((row[5]+",").getBytes()) ;
					
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					//String netWtLbs= row[6].toString();
					//String netWtLbsResult="'"+netWtLbs;
					outputStream.write((row[6]+",").getBytes()) ;
					
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					//String shipmnetNo= row[7].toString();
					//String shipmnetNoResult="'"+shipmnetNo;
					outputStream.write((row[7]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
								
				}
				if(row[9]!=null)				
				{
					outputStream.write((row[9].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[10]!=null)				
				{
					outputStream.write((row[10].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[11]!=null)				
				{
					outputStream.write((row[11].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[12]!=null)				
				{
					outputStream.write((row[12].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[13]!=null)				
				{
					outputStream.write((row[13].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[14]!=null)				
				{
					outputStream.write((row[14].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[15]!=null)				
				{
					outputStream.write((row[15].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[16]!=null)				
				{
					outputStream.write((row[16].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[17]!=null)				
				{
					outputStream.write((row[17].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[18]!=null)				
				{
					outputStream.write((row[18].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[19]!=null)				
				{
					outputStream.write((row[19].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[20]!=null)				
				{
					outputStream.write((row[20].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[21]!=null)				
				{
					outputStream.write((row[21].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[22]!=null)				
				{
					outputStream.write((row[22].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[23]!=null)				
				{
					outputStream.write((row[23].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[24]!=null)				
				{
					String desAmtTemp= row[24].toString();
					String[] desAmt = desAmtTemp.split("\\|");
					for (int x=0; x<desAmt.length; x++)
					{
					outputStream.write((desAmt[x].toString()+",").getBytes()) ;
				
					}
					for (int y=desAmt.length; y<24; y++)
						{
						outputStream.write((",").getBytes()) ;	
						}
				}
				else
				{
					for (int y=1; y<24; y++)
					{
					outputStream.write((",").getBytes()) ;	
					}
					}
				if(row[25]!=null)				
				{
					outputStream.write((row[25].toString()).getBytes()) ;
				}
				else
				{
					String s="";
					outputStream.write(s.getBytes());
				}
				outputStream.write("\r\n".getBytes());		
      			if(row[26]!=null)				
				{
					String accIdTemp= row[26].toString();
					String[] accId = accIdTemp.split("\\|");
					for (int x=0; x<accId.length; x++)
					{
					imfEntitlementsManager.getImfEstimateActualUpdate(accId[x]);
					}
				}
				
	       }
          }			
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OfficeDocMove");
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);			
		} 

		}
	
	@SkipValidation
	public void officeDocMove()	{
		StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();	
		String header=new String();
		List officeDocMoveList=imfEntitlementsManager.officeDocMove(sessionCorpID);
		//System.out.println("\n\nSize::"+officeDocMoveList.size());
		try		{
			if(!officeDocMoveList.isEmpty()){	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OfficeDocMove");
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");	
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				out.println("filename="+file1.getName()+".csv"+"\t The number of line  \t"+officeDocMoveList.size()+"\t is extracted");
		Iterator it=officeDocMoveList.iterator();
		outputStream.write("SHIPNUM,".getBytes());
		outputStream.write("SHIP_LN_NBR,".getBytes());
		outputStream.write("VNDR_REF#,".getBytes());
		outputStream.write("AIRWAYBILL#,".getBytes());
		outputStream.write("D_AIR_CODE,".getBytes());
		outputStream.write("D_AIRLINE,".getBytes());
		outputStream.write("D_FLGHT_NO,".getBytes());
		outputStream.write("D_FLGHT_TO,".getBytes()); 
		outputStream.write("D_FLGHTETA,".getBytes());
		outputStream.write("A_AIR_CODE,".getBytes());
		outputStream.write("A_AIRLINE,".getBytes());
		outputStream.write("A_FLGHT_NO,".getBytes());
		outputStream.write("A_FLGHT_TO,".getBytes());
		outputStream.write("A_FLGHTETA,".getBytes());
		outputStream.write("ITNUM,".getBytes());
		outputStream.write("PIECES,".getBytes());
		outputStream.write("KILOS,".getBytes());
		outputStream.write("NOTIF_RECP,".getBytes()); 
		outputStream.write("SHIP_CLEAR,".getBytes());
		outputStream.write("DELIV_RECP,".getBytes());
		outputStream.write("SIGN_LNAME,".getBytes());
		outputStream.write("SIGN_FNAME,".getBytes());
		outputStream.write("DELV_ROOM,".getBytes());
		outputStream.write("INVNUM,".getBytes());
		outputStream.write("CHRG_1DESC,".getBytes());
		outputStream.write("CHRG_1AMT,".getBytes());
		outputStream.write("CHRG_2DESC,".getBytes());
		outputStream.write("CHRG_2AMT,".getBytes());
		outputStream.write("CHRG_3DESC,".getBytes());
		outputStream.write("CHRG_3AMT,".getBytes());
		outputStream.write("CHRG_4DESC,".getBytes());
		outputStream.write("CHRG_4AMT,".getBytes());
		outputStream.write("CHRG_5DESC,".getBytes());
		outputStream.write("CHRG_5AMT,".getBytes());
		outputStream.write("CHRG_6DESC,".getBytes());
		outputStream.write("CHRG_6AMT,".getBytes());
		outputStream.write("CHRG_7DESC,".getBytes());
		outputStream.write("CHRG_7AMT,".getBytes());
		outputStream.write("CHRG_8DESC,".getBytes());
		outputStream.write("CHRG_8AMT,".getBytes());
		outputStream.write("CHRG_9DESC,".getBytes());
		outputStream.write("CHRG_9AMT,".getBytes());
		outputStream.write("CHRG_10DESC,".getBytes());
		outputStream.write("CHRG_10AMT,".getBytes());
		outputStream.write("CHRG_11DESC,".getBytes());
		outputStream.write("CHRG_11AMT,".getBytes());
		outputStream.write("CHRG_12DESC,".getBytes());
		outputStream.write("CHRG_12AMT,".getBytes());
		outputStream.write("Invoice_date".getBytes()); 
		outputStream.write("\r\n".getBytes());
		while(it.hasNext())
	       {
			Object []row= (Object [])it.next();
				if(row[0]!=null){
					outputStream.write((row[0].toString()+",").getBytes()) ;
				}			
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[1]!=null)				
				{
					//String shipLineNo= row[1].toString();
					//String shipLineNoResult="'"+shipLineNo;
					outputStream.write((row[1]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[2]!=null)				
				{
					//String shipNo= row[2].toString();
					//String shipNoResult="'"+shipNo;
					outputStream.write((row[2]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[3]!=null)				
				{
					//String airWayBillNo= row[3].toString();
					//String airWayBillNoResult="'"+airWayBillNo;
					outputStream.write((row[3]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[5]!=null)				
				{
					//String dAOMNINo= row[5].toString();
					//String dAOMNINoResult="'"+dAOMNINo;
					outputStream.write((row[5]+",").getBytes()) ;
					
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					//String netWtLbs= row[6].toString();
					//String netWtLbsResult="'"+netWtLbs;
					outputStream.write((row[6]+",").getBytes()) ;
					
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					//String shipmnetNo= row[7].toString();
					//String shipmnetNoResult="'"+shipmnetNo;
					outputStream.write((row[7]+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
								
				}
				if(row[9]!=null)				
				{
					outputStream.write((row[9].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[10]!=null)				
				{
					outputStream.write((row[10].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[11]!=null)				
				{
					outputStream.write((row[11].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[12]!=null)				
				{
					outputStream.write((row[12].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[13]!=null)				
				{
					outputStream.write((row[13].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[14]!=null)				
				{
					outputStream.write((row[14].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[15]!=null)				
				{
					outputStream.write((row[15].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[16]!=null)				
				{
					outputStream.write((row[16].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[17]!=null)				
				{
					outputStream.write((row[17].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[18]!=null)				
				{
					outputStream.write((row[18].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[19]!=null)				
				{
					outputStream.write((row[19].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[20]!=null)				
				{
					outputStream.write((row[20].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[21]!=null)				
				{
					outputStream.write((row[21].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[22]!=null)				
				{
					outputStream.write((row[22].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[23]!=null)				
				{
					outputStream.write((row[23].toString()+",").getBytes()) ;
				}
				else
				{
					String s=",";
					outputStream.write(s.getBytes());
				}
				if(row[24]!=null)				
				{
					String desAmtTemp= row[24].toString();
					String[] desAmt = desAmtTemp.split("\\|");
					for (int x=0; x<desAmt.length; x++)
					{
					outputStream.write((desAmt[x].toString()+",").getBytes()) ;
				
					}
					for (int y=desAmt.length; y<24; y++)
						{
						outputStream.write((",").getBytes()) ;	
						}
				}
				else
				{
					for (int y=1; y<24; y++)
					{
					outputStream.write((",").getBytes()) ;	
					}
					}
				if(row[25]!=null)				
				{
					outputStream.write((row[25].toString()).getBytes()) ;
				}
				else
				{
					String s="";
					outputStream.write(s.getBytes());
				}
				outputStream.write("\r\n".getBytes());		
      			if(row[26]!=null)				
				{
					String accIdTemp= row[26].toString();
					String[] accId = accIdTemp.split("\\|");
					for (int x=0; x<accId.length; x++)
					{
					imfEntitlementsManager.getImfEstimateActualUpdate(accId[x]);
					}
				}
				
	       }
          }			
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("OfficeDocMove");
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);			
		} 

		}
	
	@SkipValidation
	public String docExtractActual(){		
		return SUCCESS;
	}
	
	@SkipValidation
	public void docExtract() {
		StringBuffer query= new StringBuffer();	
		StringBuffer fileName= new StringBuffer();	
		String header=new String();
		List docExtractList=imfEntitlementsManager.docExtract(sessionCorpID);
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
	    StringBuilder systemDate = new StringBuilder(format.format(new Date()));
	    try		{
			if(!docExtractList.isEmpty()){	
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				List sysDefaultDetail = imfEntitlementsManager.findDosSCAC(sessionCorpID);
				
				if(!sysDefaultDetail.isEmpty()){
					prefix=sysDefaultDetail.get(0).toString();
				}else {
					prefix="";
				}
		
				File file1=new File(prefix+"-"+systemDate);
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");	
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				out.println("filename="+file1.getName()+".csv"+"\t The number of line  \t"+docExtractList.size()+"\t is extracted");
		Iterator it=docExtractList.iterator();
		while(it.hasNext())
	       {
			Object []row= (Object [])it.next();
				if(row[0]!=null){outputStream.write((row[0].toString()+",").getBytes());
				} else {
					String s=",";
					outputStream.write(s.getBytes());
				}
				
				if(row[1]!=null){outputStream.write((row[1]+",").getBytes()) ;
				} else {
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				
				if(row[2]!=null) { outputStream.write((row[2]+",").getBytes()) ;
				} else {
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[3]!=null)				
				{
					outputStream.write((row[3]+",").getBytes()) ;
				}
				else
				{
					String s="      ,";
					outputStream.write(s.getBytes());
				}
				
				if(row[4]!=null)				
				{
					outputStream.write((row[4].toString()+",").getBytes()) ;
				}
				else
				{
					String s="       ,";
					outputStream.write(s.getBytes());
				}
				
				if(row[5]!=null)				
				{
					outputStream.write((row[5]+",").getBytes()) ;
					
				}
				else
				{
					String s="      ,";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[6]!=null)				
				{
					outputStream.write((row[6]+",").getBytes()) ;
					
				}
				else
				{
					String s="      ,";
					outputStream.write(s.getBytes());
				}
				
				
				if(row[7]!=null)				
				{
					outputStream.write((row[7]+",").getBytes()) ;
				}
				else
				{
					String s="              ,";
					outputStream.write(s.getBytes());
				}
				
				if(row[8]!=null)
				{							
					outputStream.write((row[8].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
								
				}
				if(row[9]!=null)				
				{
					outputStream.write((row[9].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[10]!=null)				
				{
					outputStream.write((row[10].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[11]!=null)				
				{
					outputStream.write((row[11].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[12]!=null)				
				{
					outputStream.write((row[12].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                              ,";
					outputStream.write(s.getBytes());
				}
				if(row[13]!=null)				
				{
					outputStream.write((row[13].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[14]!=null)				
				{
					outputStream.write((row[14].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[15]!=null)				
				{
					outputStream.write((row[15].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                              ,";
					outputStream.write(s.getBytes());
				}
				if(row[16]!=null)				
				{
					outputStream.write((row[16].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[17]!=null)				
				{
					outputStream.write((row[17].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[18]!=null)				
				{
					outputStream.write((row[18].toString()+",").getBytes()) ;
				}
				else
				{
					String s="    ,";
					outputStream.write(s.getBytes());
				}
				if(row[19]!=null)				
				{
					outputStream.write((row[19].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[20]!=null)				
				{
					outputStream.write((row[20].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[21]!=null)				
				{
					outputStream.write((row[21].toString()+",").getBytes()) ;
				}
				else
				{
					String s="    ,";
					outputStream.write(s.getBytes());
				}
				if(row[22]!=null)				
				{
					outputStream.write((row[22].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[23]!=null)				
				{
					outputStream.write((row[23].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[24]!=null)				
				{
					outputStream.write((row[24].toString()+",").getBytes()) ;
				}
				else
				{
					String s="    ,";
					outputStream.write(s.getBytes());
				}
				if(row[25]!=null)				
				{
					outputStream.write((row[25].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[26]!=null)				
				{
					outputStream.write((row[26].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[27]!=null)				
				{
					outputStream.write((row[27].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[28]!=null)				
				{
					outputStream.write((row[28].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[29]!=null)				
				{
					outputStream.write((row[29].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                              ,";
					outputStream.write(s.getBytes());
				}
				if(row[30]!=null)				
				{
					outputStream.write((row[30].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[31]!=null)				
				{
					outputStream.write((row[31].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[32]!=null)				
				{
					outputStream.write((row[32].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[33]!=null)				
				{
					outputStream.write((row[33].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[34]!=null)				
				{
					outputStream.write((row[34].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[35]!=null)				
				{
					outputStream.write((row[35].toString()+",").getBytes()) ;
				}
				else
				{
					String s="               ,";
					outputStream.write(s.getBytes());
				}
				if(row[36]!=null)				
				{
					outputStream.write((row[36].toString()+",").getBytes()) ;
				}
				else
				{
					String s="            ,";
					outputStream.write(s.getBytes());
				}
				if(row[37]!=null)				
				{
					outputStream.write((row[37].toString()+",").getBytes()) ;
				}
				else
				{
					String s="   ,";
					outputStream.write(s.getBytes());
				}
				if(row[38]!=null)				
				{
					outputStream.write((row[38].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[39]!=null)				
				{
					outputStream.write((row[39].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[40]!=null)				
				{
					outputStream.write((row[40].toString()+",").getBytes()) ;
				}
				else
				{
					String s=" ,";
					outputStream.write(s.getBytes());
				}
				if(row[41]!=null)				
				{
					outputStream.write((row[41].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                                                   ,";
					outputStream.write(s.getBytes());
				}
				if(row[42]!=null)				
				{
					outputStream.write((row[42].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if(row[43]!=null)				
				{
					outputStream.write((row[43].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                         ,";
					outputStream.write(s.getBytes());
				}
				if(row[44]!=null)				
				{
					outputStream.write((row[44].toString()+",").getBytes()) ;
				}
				else
				{
					String s=" ,";
					outputStream.write(s.getBytes());
				}
				if(row[45]!=null)				
				{
					outputStream.write((row[45].toString()+",").getBytes()) ;
				}
				else
				{
					String s="                                                   ,";
					outputStream.write(s.getBytes());
				}
				if (row[46] != null && sessionCorpID.equals("CWMS")) {
					outputStream.write((row[46].toString() + ",")
							.getBytes());
				} else {
					String s = "    ,";
					outputStream.write(s.getBytes());
				}
				if (row[47] != null && sessionCorpID.equals("CWMS")) {
					outputStream.write((row[47].toString() + ",")
							.getBytes());
				} else {
					String s = "               ,";
					outputStream.write(s.getBytes());
				}
				if(row[48]!=null)				
				{
					outputStream.write((row[48].toString()+",").getBytes()) ;
				}
				else
				{
					String s="       ,";
					outputStream.write(s.getBytes());
				}
				if(row[49]!=null)				
				{
					outputStream.write((row[49].toString()+",").getBytes()) ;
				}
				else
				{
					String s="      ,";
					outputStream.write(s.getBytes());
				}
				if(row[50]!=null)				
				{
					outputStream.write((row[50].toString()+",").getBytes()) ;
				}
				else
				{
					String s="          ,";
					outputStream.write(s.getBytes());
				}
				if (row[51] != null && sessionCorpID.equals("CWMS")) {
					outputStream.write((row[51].toString() + ",")
							.getBytes());
				} else {
					String s = "               ,";
					outputStream.write(s.getBytes());
				}
				if (row[52] != null && sessionCorpID.equals("CWMS")) {
					outputStream.write((row[52].toString()).getBytes());
				} else {
					String s = "          ";
					outputStream.write(s.getBytes());
				}
				outputStream.write("\r\n".getBytes());
								
	       		}
          }			
			else
			{
				HttpServletResponse response = getResponse();
				ServletOutputStream outputStream = response.getOutputStream();
				File file1=new File("SSOW-"+systemDate);
				response.setContentType("text/csv");
				//response.setHeader("Cache-Control", "no-cache");			
				response.setHeader("Content-Disposition", "attachment; filename="+file1.getName()+".csv");			 
				response.setHeader("Pragma", "public");
				response.setHeader("Cache-Control", "max-age=0");
				header="NO Record Found , thanks";
				outputStream.write(header.getBytes());
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR:"+e);			
		} 

		}

	@SkipValidation
	public String docExtractSendMail() throws IOException, ParseException{
		String header = new String();
		List docExtractList=imfEntitlementsManager.docExtract(sessionCorpID);
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
	    StringBuilder systemDate = new StringBuilder(format.format(new Date()));
				String uploadDir =("/resources");
				uploadDir = uploadDir.replace(uploadDir, "/" + "usr" + "/" + "local" + "/" + "/dataExtract" + "/");
				File file = new File("SSOW-"+systemDate);
				String file_name = "SSOW-"+systemDate;
				String fileName="SSOW-"+systemDate+".csv";
				file_name=uploadDir+file_name+".csv";
		        FileWriter file1 = new FileWriter(file_name);
		        BufferedWriter out = new BufferedWriter (file1); 					
			if (!docExtractList.isEmpty()) {
		    Iterator it=docExtractList.iterator();
			while(it.hasNext())
		       {
				Object []row= (Object [])it.next();
					if(row[0]!=null){out.write((row[0].toString()+","));
					} else {
						String s=",";
						out.write(s);
					}
					
					if(row[1]!=null){out.write((row[1]+",")) ;
					} else {
						String s="          ,";
						out.write(s);
					}
					
					if(row[2]!=null) { out.write((row[2]+",")) ;
					} else {
						String s="          ,";
						out.write(s);
					}
					if(row[3]!=null)				
					{
						out.write((row[3]+",")) ;
					}
					else
					{
						String s="      ,";
						out.write(s);
					}
					
					if(row[4]!=null)				
					{
						out.write((row[4].toString()+",")) ;
					}
					else
					{
						String s="       ,";
						out.write(s);
					}
					
					if(row[5]!=null)				
					{
						out.write((row[5]+",")) ;
						
					}
					else
					{
						String s="      ,";
						out.write(s);
					}
					
					
					if(row[6]!=null)				
					{
						out.write((row[6]+",")) ;
						
					}
					else
					{
						String s="      ,";
						out.write(s);
					}
					
					
					if(row[7]!=null)				
					{
						out.write((row[7]+",")) ;
					}
					else
					{
						String s="              ,";
						out.write(s);
					}
					
					if(row[8]!=null)
					{							
						out.write((row[8].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
									
					}
					if(row[9]!=null)				
					{
						out.write((row[9].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[10]!=null)				
					{
						out.write((row[10].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[11]!=null)				
					{
						out.write((row[11].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[12]!=null)				
					{
						out.write((row[12].toString()+",")) ;
					}
					else
					{
						String s="                              ,";
						out.write(s);
					}
					if(row[13]!=null)				
					{
						out.write((row[13].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[14]!=null)				
					{
						out.write((row[14].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[15]!=null)				
					{
						out.write((row[15].toString()+",")) ;
					}
					else
					{
						String s="                              ,";
						out.write(s);
					}
					if(row[16]!=null)				
					{
						out.write((row[16].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[17]!=null)				
					{
						out.write((row[17].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[18]!=null)				
					{
						out.write((row[18].toString()+",")) ;
					}
					else
					{
						String s="    ,";
						out.write(s);
					}
					if(row[19]!=null)				
					{
						out.write((row[19].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[20]!=null)				
					{
						out.write((row[20].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[21]!=null)				
					{
						out.write((row[21].toString()+",")) ;
					}
					else
					{
						String s="    ,";
						out.write(s);
					}
					if(row[22]!=null)				
					{
						out.write((row[22].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[23]!=null)				
					{
						out.write((row[23].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[24]!=null)				
					{
						out.write((row[24].toString()+",")) ;
					}
					else
					{
						String s="    ,";
						out.write(s);
					}
					if(row[25]!=null)				
					{
						out.write((row[25].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[26]!=null)				
					{
						out.write((row[26].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[27]!=null)				
					{
						out.write((row[27].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[28]!=null)				
					{
						out.write((row[28].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[29]!=null)				
					{
						out.write((row[29].toString()+",")) ;
					}
					else
					{
						String s="                              ,";
						out.write(s);
					}
					if(row[30]!=null)				
					{
						out.write((row[30].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[31]!=null)				
					{
						out.write((row[31].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[32]!=null)				
					{
						out.write((row[32].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[33]!=null)				
					{
						out.write((row[33].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[34]!=null)				
					{
						out.write((row[34].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[35]!=null)				
					{
						out.write((row[35].toString()+",")) ;
					}
					else
					{
						String s="               ,";
						out.write(s);
					}
					if(row[36]!=null)				
					{
						out.write((row[36].toString()+",")) ;
					}
					else
					{
						String s="            ,";
						out.write(s);
					}
					if(row[37]!=null)				
					{
						out.write((row[37].toString()+",")) ;
					}
					else
					{
						String s="   ,";
						out.write(s);
					}
					if(row[38]!=null)				
					{
						out.write((row[38].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[39]!=null)				
					{
						out.write((row[39].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[40]!=null)				
					{
						out.write((row[40].toString()+",")) ;
					}
					else
					{
						String s=" ,";
						out.write(s);
					}
					if(row[41]!=null)				
					{
						out.write((row[41].toString()+",")) ;
					}
					else
					{
						String s="                                                   ,";
						out.write(s);
					}
					if(row[42]!=null)				
					{
						out.write((row[42].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[43]!=null)				
					{
						out.write((row[43].toString()+",")) ;
					}
					else
					{
						String s="                         ,";
						out.write(s);
					}
					if(row[44]!=null)				
					{
						out.write((row[44].toString()+",")) ;
					}
					else
					{
						String s=" ,";
						out.write(s);
					}
					if(row[45]!=null)				
					{
						out.write((row[45].toString()+",")) ;
					}
					else
					{
						String s="                                                   ,";
						out.write(s);
					}
					if(row[46]!=null)				
					{
						out.write((row[46].toString()+",")) ;
					}
					else
					{
						String s="    ,";
						out.write(s);
					}
					if(row[47]!=null)				
					{
						out.write((row[47].toString()+",")) ;
					}
					else
					{
						String s="               ,";
						out.write(s);
					}
					if(row[48]!=null)				
					{
						out.write((row[48].toString()+",")) ;
					}
					else
					{
						String s="       ,";
						out.write(s);
					}
					if(row[49]!=null)				
					{
						out.write((row[49].toString()+",")) ;
					}
					else
					{
						String s="      ,";
						out.write(s);
					}
					if(row[50]!=null)				
					{
						out.write((row[50].toString()+",")) ;
					}
					else
					{
						String s="          ,";
						out.write(s);
					}
					if(row[51]!=null)				
					{
						out.write((row[51].toString()+",")) ;
					}
					else
					{
						String s="               ,";
						out.write(s);
					}
					if(row[52]!=null)				
					{
						out.write((row[52].toString())) ;
					}
					else
					{
						String s="          ";
						out.write(s);
					}
					out.write("\r\n");		
	       } 
							} else{ 
							header = "NO Record Found , thanks";
							out.write(header);	
							}
				out.close(); 
				try {
				   
				   String from = "support@redskymobility.com";
				   userEmail = customerFileManager.findCoordEmailAddress(getRequest().getRemoteUser());
					if (!(userEmail == null) && (!userEmail.isEmpty())) {
						emailAdd = userEmail.get(0).toString().trim();
					}
				   String emailTo=emailAdd;
					String tempRecipient="";
					String tempRecipientArr[]=emailTo.split(",");
					for(String str1:tempRecipientArr){
						if(!userManager.doNotEmailFlag(str1).equalsIgnoreCase("YES")){
							if (!tempRecipient.equalsIgnoreCase("")) tempRecipient += ",";
							tempRecipient += str1;
						}
					}
					emailTo=tempRecipient;
				
				   String msgText1 = "Hi "+",\n\nPlease check the attached auto generated doc extract file.\n\nSchedule Details\nFile Name:"+fileName;
				  
				   try{
					
					   EmailSetUpUtil.sendMail(emailTo, "", "", "DOC Extract", msgText1, file_name, from);
					  
				       mailStatus = "sent";
				   }
				   catch (Exception e) {
						e.printStackTrace();
						mailStatus = "notSent";
				 }
				  }catch (Exception e) {
						e.printStackTrace();
						mailStatus = "notSent";
				 }
				  return SUCCESS; 
			}
	
	
	/*@SkipValidation
	public String   missingInvoiceExtracts()
	{  
		return SUCCESS;
	}
	
	
	
	
	@SkipValidation
	public void missingInvoiceExtract() throws Exception, EOFException{
		SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder beginDates = new StringBuilder(formats.format( beginDate ));
        StringBuilder endDates = new StringBuilder(formats.format( endDate ));
        List minSequence=imfEntitlementsManager.minSequence(beginDates.toString(), endDates.toString(), sessionCorpID);
        List maxSequence=imfEntitlementsManager.maxSequence(beginDates.toString(), endDates.toString(), sessionCorpID);
        int minSeq=Integer.parseInt(minSequence.get(0).toString());
        int maxSeq=Integer.parseInt(maxSequence.get(0).toString());
		String []columnList = new String[]{"Invoice #","Revenue","SO#"};
		SimpleDateFormat format = new SimpleDateFormat("MMddyyyy");
	    StringBuilder systemDate = new StringBuilder(format.format(new Date())); 
	    MissedInvoiceDTO missedInvoice = null;
	    List mainList = new ArrayList();
	    try{
	 		for (int seq = minSeq; seq <= maxSeq; seq++) {
	    	List missingInvoices=imfEntitlementsManager.missingInvoices(beginDates.toString(), endDates.toString(), sessionCorpID,seq);
	    	if(!(missingInvoices.isEmpty())){
	 			Iterator it = missingInvoices.iterator();	
		 		while(it.hasNext())
		        {
		 			Object []row= (Object [])it.next();
		 			missedInvoice = new MissedInvoiceDTO();
		 			missedInvoice.setInvoice(row[0].toString());
		 			missedInvoice.setRevenue(row[1].toString());
		 			missedInvoice.setSoNumber(row[2].toString());
		        }
	    	}
	    	else{
	    		missedInvoice = new MissedInvoiceDTO(); 
	 			missedInvoice.setInvoice(String.valueOf(seq));
	 			missedInvoice.setRevenue("Missed Invoice");
	 			missedInvoice.setSoNumber("");
			}
	    	mainList.add(missedInvoice);
		}
	 	ExcelCreator ec = ExcelCreator.getExcelCreator();
	 	ec.setHeaderRowBackgroundColor(ExcelCreatorColor.AQUA);
	 	ec.setConditionalDataAndIndex("Missed Invoice",1);
	 	ec.rowBeforeHeader(new String[]{"From "+beginDates+" To "+endDates}, true, true);
	 	ec.mergeCells(0, 0, columnList.length-1);
	 	ec.createExcelForAttachment(getResponse(), "Missed_Invoice-From#"+beginDates+"#To#"+endDates,columnList , mainList, "Missed_Invoice", ",");
	    } catch(Exception e)
		{
			System.out.println("ERROR:"+e);			
		}

		}
	
	*/
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public String save() throws Exception{
		
		boolean isNew = (imfEntitlements.getId() == null);
		
		if(isNew)
	    {
			imfEntitlements.setCreatedOn(new Date());
	    }
	      else
	        {
	    	  imfEntitlements.setCreatedOn(imfEntitlements.getCreatedOn());
	        }
		
		imfEntitlements.setCorpID(sessionCorpID);				
		imfEntitlements.setUpdatedOn(new Date());
		imfEntitlements.setUpdatedBy(getRequest().getRemoteUser());
		imfEntitlements= imfEntitlementsManager.save(imfEntitlements);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Entitlement has been added successfully" : "Entitlement has been updated successfully";
		saveMessage(getText(key));
		}
		getNotesForIconChange();
		if (!isNew) {   
        	return INPUT;   
        } else {   
        	maxId = Long.parseLong(imfEntitlementsManager.findMaximumId().get(0).toString());
        	imfEntitlements = imfEntitlementsManager.get(maxId);
          return SUCCESS;
        }
		
	}
	
	private String countImfEntitlementNotes;
	private NotesManager notesManager;
	
	@SkipValidation
	public String getNotesForIconChange() {
		
		List m = notesManager.countForIMFEntitlementNotes(customerFile.getSequenceNumber());
		if (m.isEmpty()) {
			countImfEntitlementNotes = "0"; 	
		} else {
			countImfEntitlementNotes = ((m).get(0)).toString();
		}
		return SUCCESS;
	}
	
	private String gotoPageString;
	public String saveOnTabChange() throws Exception {
    	String s = save();
    	return gotoPageString;
    }
	
	
	
	
	
	
	
	public ImfEntitlements getImfEntitlements() {
		return imfEntitlements;
	}
	public void setImfEntitlements(ImfEntitlements imfEntitlements) {
		this.imfEntitlements = imfEntitlements;
	}
	public Long getId() {
		return id;
	}
	public void setImfEntitlementsManager(
			ImfEntitlementsManager imfEntitlementsManager) {
		this.imfEntitlementsManager = imfEntitlementsManager;
	}
	public Long getMaxId() {
		return maxId;
	}
	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public CustomerFile getCustomerFile() {
		return customerFile;
	}

	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Long getCid() {
		return cid;
	}

	public void setCid(Long cid) {
		this.cid = cid;
	}
	
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public String getCountImfEntitlementNotes() {
		return countImfEntitlementNotes;
	}
	public void setCountImfEntitlementNotes(String countImfEntitlementNotes) {
		this.countImfEntitlementNotes = countImfEntitlementNotes;
	}
	public String getGotoPageString() {
		return gotoPageString;
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}
	public List getImfEstimate() {
		return imfEstimate;
	}
	public void setImfEstimate(List imfEstimate) {
		this.imfEstimate = imfEstimate;
	}
	public List getImfEstimateList() {
		return imfEstimateList;
	}
	public void setImfEstimateList(List imfEstimateList) {
		this.imfEstimateList = imfEstimateList;
	}
	public String getMailStatus() {
		return mailStatus;
	}
	public void setMailStatus(String mailStatus) {
		this.mailStatus = mailStatus;
	}
	public List getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(List userEmail) {
		this.userEmail = userEmail;
	}
	public String getEmailAdd() {
		return emailAdd;
	}
	public void setEmailAdd(String emailAdd) {
		this.emailAdd = emailAdd;
	}
	

}

/*class MissedInvoiceDTO{
	private String invoice;
	private String revenue;
	private String soNumber;
	public String getInvoice() {
		return invoice;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public String getRevenue() {
		return revenue;
	}
	public void setRevenue(String revenue) {
		this.revenue = revenue;
	}
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	@Override
	public String toString() {
		return invoice + ", " + revenue + ", " + soNumber;
	}
	
}	
	*/
