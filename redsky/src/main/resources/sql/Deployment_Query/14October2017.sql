
// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `payQuantity` DECIMAL(19,2) NULL , ADD COLUMN `payRate` DECIMAL(19,4) NULL  AFTER `payQuantity` ;
ALTER TABLE `redsky`.`customerfile` ADD COLUMN `originAgentContact` VARCHAR(200) DEFAULT '';
ALTER TABLE `redsky`.`customerfile` ADD COLUMN `originAgentPhoneNumber` VARCHAR(30) DEFAULT '';
ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `status` bit(1) DEFAULT b'1';
update operationsintelligence set status=true;
alter table partnerpublic modify column bankcode varchar(45);

CREATE  TABLE `redsky`.`genericsurveyquestion` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `sequenceNumber` INT(2) NULL ,
  `question` VARCHAR(300) NULL ,
  `answerType` VARCHAR(25) NULL ,
  `jobType` VARCHAR(3) NULL ,
  `routing` VARCHAR(3) NULL ,
  `bookingAgent` BIT(1) NULL ,
  `originAgent` BIT(1) NULL ,
  `destinationAgent` BIT(1) NULL ,
  `language` VARCHAR(2) NULL ,
  `status` BIT(1) NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  `createdOn` DATETIME NULL ,
  `updatedOn` DATETIME NULL ,
  PRIMARY KEY (`id`) );

CREATE  TABLE `redsky`.`genericsurveyanswer` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `corpId` VARCHAR(10) NULL ,
  `sequenceNumber` INT(2) NULL ,
  `answer` VARCHAR(200) NULL ,
  `status` BIT(1) NULL ,
  `surveyQuestionId` BIGINT(20) NULL,
  `createdBy` VARCHAR(80) NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  `createdOn` DATETIME NULL ,
  `updatedOn` DATETIME NULL ,
  PRIMARY KEY (`id`) );

CREATE  TABLE `redsky`.`surveyanswerbyuser` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `serviceOrderId` BIGINT(20) NULL ,
  `questionId` BIGINT(20) NULL ,
  `answerId`  VARCHAR(1000) NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  `createdOn` DATETIME NULL ,
  `updatedOn` DATETIME NULL ,
  PRIMARY KEY (`id`) );

ALTER TABLE `redsky`.`company` ADD COLUMN `customerSurveyByEmail` BIT(1) NULL DEFAULT b'0'  AFTER `restrictedMassage` ,
ADD COLUMN `customerSurveyOnCustomerPortal` BIT(1) NULL DEFAULT b'0'  AFTER `customerSurveyByEmail` ;
ALTER TABLE `redsky`.`customerfile` ADD COLUMN surveyObservation varchar(30) DEFAULT '';
ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `targetActual` VARCHAR(1) NULL  AFTER `status` ;
ALTER TABLE `redsky`.`operationsintelligence`
ADD COLUMN `revisionScopeOfWorkOrder` VARCHAR(500) NULL  AFTER `targetActual` ,
ADD COLUMN `revisionScopeOfNumberOfComputer` VARCHAR(45) NULL  AFTER `revisionScopeOfWorkOrder` ,
ADD COLUMN `revisionScopeOfNumberOfEmployee` VARCHAR(45) NULL  AFTER `revisionScopeOfNumberOfComputer` ,
ADD COLUMN `revisionScopeOfSalesTax` VARCHAR(45) NULL  AFTER `revisionScopeOfNumberOfEmployee` ,
ADD COLUMN `revisionScopeOfConsumables` DECIMAL(19,2) NULL  AFTER `revisionScopeOfSalesTax` ;
