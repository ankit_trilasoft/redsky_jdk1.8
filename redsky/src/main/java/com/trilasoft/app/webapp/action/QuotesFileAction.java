package com.trilasoft.app.webapp.action;

import org.appfuse.webapp.action.BaseAction;

import java.util.List;


import com.trilasoft.app.model.QuotesFile;
import com.trilasoft.app.service.QuotesFileManager;

public class QuotesFileAction extends BaseAction {
	
	private QuotesFileManager quotesFileManager;
  private List quotesFiles;   
 
  private Long id;
  private QuotesFile quotesFile;
  
  
  public void setQuotesFileManager(QuotesFileManager quotesFileManager) {
      this.quotesFileManager = quotesFileManager;
  }
  
  public List getQuotesFiles() {   
      return quotesFiles;   
  }   

  public String list() {   
      quotesFiles = quotesFileManager.getAll();  
      return SUCCESS;   
  }
  
  
  
  public void setId(Long id) {   
      this.id = id;   
  }   
    
  public QuotesFile getQuotesFile() {   
      return quotesFile;   
  }   
    
  public void setQuotesFile(QuotesFile quotesFile) {   
      this.quotesFile = quotesFile;   
  }   
    
  public String delete() {   
      quotesFileManager.remove(quotesFile.getId());   
      saveMessage(getText("quotesFile.deleted"));   
    
      return SUCCESS;   
  }   
  public String edit() {   
      if (id != null) {   
          quotesFile = quotesFileManager.get(id);   
      } else {   
          quotesFile = new QuotesFile();   
      }   
    
      return SUCCESS;   
  }   
    
  public String save() throws Exception {   
      if (cancel != null) {   
          return "cancel";   
      }   
    
      if (delete != null) {   
          return delete();   
      }   
    
      boolean isNew = (quotesFile.getId() == null);   
    
      quotesFileManager.save(quotesFile);   
    
      String key = (isNew) ? "quotesFile.added" : "quotesFile.updated";   
      saveMessage(getText(key));   
    
      if (!isNew) {   
          return INPUT;   
      } else {   
          return SUCCESS;   
      }   
  }  

  public String search() {  
  	   	
      quotesFiles = quotesFileManager.findByLastName(quotesFile.getLastName());   
      return SUCCESS;   
  }  


}