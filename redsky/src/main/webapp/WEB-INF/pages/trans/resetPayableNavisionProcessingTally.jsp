<%@ include file="/common/taglibs.jsp"%> 

<head>

	<title>Reset Purchase Invoice</title>   
    <meta name="heading" content="Reset Purchase Invoice"/> 
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">

function validatingField()
{
var fileName=document.forms['tallyPayableResetNewForm'].elements['invoiceFileName'].value;
	fileName=fileName.trim();
	if(fileName=='')
	{
	alert('Please enter the file name');
	return false;
	}
	if(fileName.length>30)
	{
	alert('Please enter valid file name');
		document.forms['tallyPayableResetNewForm'].elements['invoiceFileName'].value='';
	return false;
	}
	if(fileName.indexOf("'")>-1)
	{
	alert('Please enter valid file name');
	document.forms['tallyPayableResetNewForm'].elements['invoiceFileName'].value='';
	return false;
	}	
	if(fileName!='')
	{
       var agree = confirm("Press OK to continue for update or Press Cancel to Continue without update ");
						       if(agree)
						        {
						           location.href = 'saveTallyResetPayableProcessing.html?fileName='+encodeURIComponent(fileName);
						        }
						       else
						        {
						        return false;
								}
	}
}
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">

</script>
	<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 
</head>
<body style="background-color:#444444;">
<s:form id="tallyPayableResetNewForm" name="tallyPayableResetNewForm" action="" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="Layer1" style="width:85% " >
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table cellspacing="1" cellpadding="1" border="0" > <tr>
<td class="listwhitetext">Enter The Extract File Name</td>
<td><s:textfield cssClass="input-text" name="invoiceFileName" value="" cssStyle="width:200px" /></td><td></td></tr>
<tr><td class=""></td>
	<td align="left" colspan="2">
<input type="button" class="cssbutton" value="Reset Purchase Invoice" align="right" onclick="return validatingField();"></td><td></td></tr>
</table>
</div>
<div class="bottom-header"><span></span></div> 

</div></div></div></s:form></body>