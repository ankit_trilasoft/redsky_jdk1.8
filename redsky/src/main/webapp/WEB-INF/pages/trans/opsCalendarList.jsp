<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<head> 
<title>Operations List</title> 
<meta name="heading" content="Operations List"/> 
</head>
<s:form id="searchForm" name="searchForm" action="" method="post" validate="true">   
<s:set name="workList" value="workList" scope="request"/>
<div id="Layer1" style="width:620px">
	<div id="newmnav">
		<ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Operations List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		</ul>
	</div>
	<div class="listwhitetext">
		<strong><font>&nbsp;&nbsp;&nbsp;Hub ID : ${hubID}</font></strong>
	</div>
	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:3px;"></div>	
	<display:table name="workList" class="table" requestURI="" id="workList"  pagesize="10">
	   <display:column property="date" title="Date" sortable="true" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
	    <display:column property="day" sortable="true"  title="Day" style="width:150px"/>
	    <display:column property="status" sortable="true" title="Status"/>
	   
		<display:setProperty name="export.excel.filename" value="Reports List.xls"/> 
	    <display:setProperty name="export.csv.filename" value="Reports List.csv"/> 
	    <display:setProperty name="export.pdf.filename" value="Reports List.pdf"/> 
	</display:table>
	
	<table height="30px" class="mainDetailTable" cellpadding="0" cellspacing="0" style="width: 620px">
		<tbody>  
			<tr class="colored_bg" height="30px">
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="addBtn" class="cssbutton" style="width:105px; height:25px; margin-left:5px;" align="top" value="Previous Week"/>   
				</td>
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="addBtn" class="cssbutton" style="width:105px; height:25px; margin-left:5px;" align="top" value="Current Week"/>   
				</td>
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="addBtn" class="cssbutton" style="width:105px; height:25px; margin-left:5px;" align="top" value="Next Week"/>   
				</td>
				<td class="listwhitetext-hcrew" style="padding-left:20px;!padding-left:5px;font-size:12px; !padding-bottom:8px;">
					<input type="button" name="addBtn" class="cssbutton" style="width:105px; height:25px; margin-left:5px;" align="top" value="Close" onclick="window.close();"/>   
				</td>
			</tr>	
		</tbody>
	</table>
</div> 

</s:form>
 