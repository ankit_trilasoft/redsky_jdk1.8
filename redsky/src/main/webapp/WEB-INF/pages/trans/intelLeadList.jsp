<%@ include file="/common/taglibs.jsp"%>
<head>
<title>IntelLead List</title>
 <meta name="heading" content="IntelLead List"/>
<style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:0px; !margin-bottom:2px; margin-top:-18px; padding:2px 0px; text-align:right;width:100%;}
.table-price thead th {height:22px;}
#otabs{margin-bottom:0px;}
</style>
</head>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script language="javascript" type="text/javascript">
function openIntelLeadForm(id){	
	var url = 'intelLeadForm.html?decorator=popup&popup=true&id='+id;
   window.open(url,'intelLeadForm','height=600,width=1000,top=50,left=150, scrollbars=yes,resizable=yes'); 
}

function orderCreated(target,id,authType){
	if(target.value=='Accept'){
        var agree= confirm("Are you sure you want to Accept this Lead?");
        if(agree){
            // document.getElementById("overlayDetails").style.display = "block";
           // var linkNet="orderCreatedFromIntel.html?id="+id+"&authType="+authType;
           
             $.get("orderCreatedFromIntel.html?ajax=1&decorator=simple&popup=true",
            	        {id:id,authType:authType},
            	        function(data){
            	        	alert("Order is created successfuly. ")
            	        	window.location.reload();	
            	        	
            	    });
           
        }else{
           // obj.value='';
        }
    }
}

</script>
<s:form action="" >
<s:set name="intelLeadList" value="intelLeadList" scope="request"/>
<div id="otabs">
	<ul>
		<li><a class="current"><span>IntelLead List</span></a></li>
	</ul>
</div>
<div class="spn"></div>
<display:table name="intelLeadList" class="table-price" requestURI="" id="intelLeadList"  style="width:100%;margin-top:1px;margin-bottom:0px;" >
 <%-- <display:column><s:checkbox name="dd" value="false" /></display:column> --%>
 <display:column property="employeeName" title="Employee Name"></display:column>
 <display:column property="assignmentName" title="Assign. Name"></display:column>
 <display:column property="authID" title="Auth ID"></display:column>
 <display:column property="authType" title="Type"></display:column>
 <display:column property="authDate" title="Date" sortable="true" format="{0,date,dd-MMM-yyyy}"></display:column>
 <display:column property="accountName" title="Account Name"></display:column>
 <display:column property="assignmentType" title="Assignment Type"></display:column>
 <display:column title="Origin">${intelLeadList.fromState} ${intelLeadList.fromCountry}</display:column>
 <display:column title="Destination">${intelLeadList.toCityToState} ${intelLeadList.toCountry}</display:column>
 <display:column property="serviceOrderNumber" title="Reg# / moves ID" ></display:column>
 <display:column title="" style="text-align:center;"><input type="button" class="skybtn" value="View" name="view" onclick="openIntelLeadForm('${intelLeadList.id}')" /></display:column>    
   <display:column title="Action" style="width:5px" >
   <c:if test="${intelLeadList.action eq 'Completed' && intelLeadList.authType eq 'NEW'}">
    <s:select list="{'Accept'}" name="action"  onchange="orderCreated(this,'${intelLeadList.id }','${intelLeadList.authType}');" disabled="true" cssClass="list-menu" cssStyle="width:65px;"/> 
   </c:if>
   
   <c:if test="${intelLeadList.action ne 'Completed' || intelLeadList.authType ne 'NEW' }">
    <s:select list="{'','Accept','Hold'}" name="action"  onchange="orderCreated(this,'${intelLeadList.id }','${intelLeadList.authType}');"  cssClass="list-menu" cssStyle="width:65px;"></s:select> 
   </c:if>
    </display:column>
</display:table>
</s:form>