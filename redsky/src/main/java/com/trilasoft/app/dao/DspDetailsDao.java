package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.DspDetails;

public interface DspDetailsDao  extends GenericDao<DspDetails, Long> {
	public List getListById(Long id);
	public List checkById(Long id);
	public String getListRecord(String job,String corpId);
	public void updateServiceOrderStatus(Long id,String shipNumber,String serviceOrderStatus,String corpId,String userName,int StatusNumber);
	public void updateServiceCompleteDate(Long id,String shipNumber,String latestDate,String corpId );
	public String getRloVenderCode(Long uId,String corpId);
	public List findMaximumShip(String seqNum, String job,String CorpID);
	public List findMinimumShip(String seqNum, String job,String CorpID);
	public String checkNetworkPartnerServiceType(String sessionCorpID,String networkVendorCode,String preFixCode,String moveTypeCheck);
	public String checkMoveTypeAgent(String sessionCorpID,String networkVendorCode);
	public List findAllServicesForRelo(String corpId);
	//public List findCountShip(String seqNum, String job,String CorpID);
	 public String getCustomerSurveyDetails(String ShipNumber,Long soId,String corpId);
	 public List getFeebackDetails(String shipnumber,String DSserviceOrderId,String serviceName,String sessionCorpID);
	 public void resendEmailUpdateAjax(String updateQuery,String sessionCorpID,Long soId);
	 public void saveTenancyMgmtUtilities(String sessionCorpID,Long serviceOrderId, String fieldName, boolean fieldValue,String updatedBy);
	 public List findUtilityServiceDetail(String sessionCorpID,Long serviceOrderId,String fieldName);
	 public void updateUtilityService(String updateQuery);
	 public String findUtilitiesCheckBoxValues(String sessionCorpID,Long serviceOrderId);
	 public List tenancyBillingPreviewList(Date billThroughFrom, Date billThroughTo,String billToCode, String sessionCorpID, String tenancyBillingGrpList);
	 public void updateBillThroughDate(String tableName,String fieldName,String billThroughDate, Long id, String userName);
}
