package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
@Entity
@Table(name="t20VisionLogInfo")
public class T20VisionLogInfo extends BaseObject{
	private String corpID;
	private Long id;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String entryNumber;
    private String orderNumberLine;
    private String status;
	private String error;
	private String invoiceNumber;
	private String orderNumber;
	private Long serviceOrderId;
	private String checkLHF;
	private String notProcess;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("serviceOrderId",serviceOrderId).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("entryNumber", entryNumber)
				.append("orderNumberLine", orderNumberLine)
				.append("status", status).append("error", error)
				.append("invoiceNumber", invoiceNumber)
				.append("orderNumber", orderNumber).append("checkLHF",checkLHF).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof T20VisionLogInfo))
			return false;
		T20VisionLogInfo castOther = (T20VisionLogInfo) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(serviceOrderId,castOther.serviceOrderId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(entryNumber, castOther.entryNumber)
				.append(orderNumberLine, castOther.orderNumberLine)
				.append(status, castOther.status)
				.append(error, castOther.error)
				.append(invoiceNumber, castOther.invoiceNumber)
				.append(orderNumber, castOther.orderNumber).append(checkLHF, castOther.checkLHF).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id).append(serviceOrderId)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(entryNumber).append(orderNumberLine)
				.append(status).append(error).append(invoiceNumber)
				.append(orderNumber).append(checkLHF).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getEntryNumber() {
		return entryNumber;
	}
	public void setEntryNumber(String entryNumber) {
		this.entryNumber = entryNumber;
	}
	@Column
	public String getOrderNumberLine() {
		return orderNumberLine;
	}
	public void setOrderNumberLine(String orderNumberLine) {
		this.orderNumberLine = orderNumberLine;
	}
	@Column
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	@Column
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Column
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	@Column
	public Long getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Long serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getCheckLHF() {
		return checkLHF;
	}
	public void setCheckLHF(String checkLHF) {
		this.checkLHF = checkLHF;
	}
	@Column
	public String getNotProcess() {
		return notProcess;
	}
	public void setNotProcess(String notProcess) {
		this.notProcess = notProcess;
	}
	
	

}
