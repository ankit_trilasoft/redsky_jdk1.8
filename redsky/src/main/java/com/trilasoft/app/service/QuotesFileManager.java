package com.trilasoft.app.service;

import java.util.List;
import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.QuotesFile;


public interface QuotesFileManager extends GenericManager<QuotesFile, Long> {

	public List<QuotesFile> findByLastName(String lastName);
}