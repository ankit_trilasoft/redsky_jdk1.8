package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.IntelLeadDao;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.IntelLead;


public class IntelLeadDaoHibernate extends GenericDaoHibernate<IntelLead, Long> implements IntelLeadDao {

	public IntelLeadDaoHibernate() {
		super(IntelLead.class);
	}
	
	public List<IntelLead> getIntelLeadLists(String corpId){
		String query = "from IntelLead where corpId='" + corpId +"' ";
		try{
			return getHibernateTemplate().find(query);
			}catch(Exception e){
				e.printStackTrace();
			}
			return null;
	}
	
	public void updateActionValue(Long id, String sessionCorpID,String shipnumber, String seqNum){
		this.getHibernateTemplate().bulkUpdate("update IntelLead set action='Completed', serviceOrderNumber ='" + shipnumber +"' , customerFileNumber = '" + seqNum +"' where id = '"+id+"' and  corpId='" + sessionCorpID +"' ");
	}

}
