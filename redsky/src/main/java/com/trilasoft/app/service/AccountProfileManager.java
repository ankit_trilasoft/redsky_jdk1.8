package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AccountProfile;

public interface AccountProfileManager extends GenericManager<AccountProfile, Long> {
	
	public List<AccountProfile> findAccountProfileByPC(String partnerCode, String corpID);
	
	public List findMaximumId();
	public List getRelatedNotesForProfile(String corpID, String partnerCode);
	public List findAccountProfile(String corpID, String partnerCode);
	public List getAccountOwner(String corpID);

}
