package com.trilasoft.app.model;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

@Entity
@Table(name="company")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class Company extends BaseObject{
	private String corpID;
	private Long id;
	private String parentCorpId;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private Boolean networkFlag;
	private String companyName;
	private String address1;
    private String currencySign;
	private String address2;
	private String city;
	private String postal;
	private String reportValidationFlag;
	private String countryID;
	private String contactNumber;
	private File companyLOGO;
	private String timeZone;
	private String autoSavePrompt; 
	private String companyDivisionFlag; 
	private String lastInvoiceNumber;
	private Long lastTicketNumber;
	private Long lastClaimNumber;
	private String toDoRuleExecutionTime;
	private Date lastRunDate;
	private String multiCurrency;
	private Boolean flagDefaultVendorAccountLines;
	private Long userPasswordExpiryDuration;
	private String userActionOnExpiry;
	private Long partnerPasswordExpiryDuration;
	private String partnerActionOnExpiry;
	private String lastCreditInvoice;
	private Boolean tracInternalUser;
	private String localeLanguage;
	private String localeCountry;
	private Date clientBillingDate;
	private Date goLiveDate;
	private String jobType1;
	private String jobType2;
	private String jobType3;
	private String distanceFrom1;
	private String distanceFrom2;
	private String distanceFrom3;
	private String grpDefault;
	private Boolean UTSI;
	private Boolean voxmeIntegration;
	private String estimateVATFlag; 
	private Boolean weeklyBilling ;
	private String customerFeedback;
	private String autoGenerateAccRef;
	private Boolean transfereeInfopackage;
	private Boolean justSayYes;
	private Boolean qualitySurvey;
	private Boolean workticketQueue;
	private Boolean useCostTransferred;
	private String lastTripNumber;
	private Boolean creditInvoiceSequence;
	private Boolean postingDateStop;
	private Boolean printInsuranceCertificate;
	private Long certificateAdditionalNumber;
	private Boolean securityChecked;
	private String ftpDir;
	private String surveyEmailSign;
	private Boolean automaticLinkup = new Boolean(false);
	private Boolean payablesXferWithApprovalOnly;
	private String RSBillingInstructions;
	private Boolean vanlineEnabled ;
	private Boolean postingDateFlexibility ;
	private Boolean cmmdmmAgent; 
	private Boolean autoFxUpdater;
	private String rskyBillGroup;
	private BigDecimal defaultBillingRate;
	private BigDecimal rate2;
	private String email;
	private Boolean rulesRunning;
	private Boolean allowFutureActualDateEntry;
	private Boolean accessRedSkyCportalapp ;
	private String scaling;
	private Boolean enablePricePoint;
	private Date priceStartDate;
	private Date priceEndDate;
	private BigDecimal subscriptionAmt;
	private String clientID;
	private String ccEmail;
	private Boolean cportalAccessCompanyDivisionLevel;
	private Boolean chargeDiscountSetting;
	private Boolean accessQuotationFromCustomerFile;
	private String cportalBrandingURL;
	private Date mmStartDate;
	private Date mmEndDate;
	private Boolean enableMobileMover;
	private BigDecimal mmSubscriptionAmt;
	private String mmClientID;
	private BigDecimal rsAmount;
	private Long numberOfPricePointUser; 
	private String quoteServices;
	private Boolean surveyLinking = false;
    private String landingPageWelcomeMsg;
    private Boolean agentTDREmail;
    private Boolean singleCompanyDivisionInvoicing;
    private String privateCustomerTCode;
    private BigDecimal minimumBillAmount;
    private Boolean UGRNNetting;
    private Boolean storageBillingStatus=false;
    private String accountLineNonEditable;
    private Boolean oAdAWeightVolumeMandatoryValidation;
    private Boolean subOADASoftValidation;
    private Boolean historyFx = false;
    private Boolean enableMoveForYou = false;
    private Boolean enableMSS = false;
    private Boolean agentInvoiceSeed = false;
    private Boolean insurancePremiumTax=false;
	private BigDecimal insurancePremiumTaxValue;
	private String vatInsurancePremiumTax;
	private Boolean forceSoDashboard= false;
	private Boolean forceDocCenter= false;
	private Boolean fullAudit = false;
	private Boolean displayAPortalRevision = true;
	private Boolean recInvoicePerSO  = false;
	private String recInvoicePerSOJob;
	private Boolean activityMgmtVersion2 = false;
	private Boolean acctRefSameParent = false;
	private String integrationUser;
	private String integrationPassword;
	private String generateInvoiceBy;
	private Boolean status=new Boolean(true);
	private Boolean restrictAccess;
	private Boolean extractedFileAudit  = false;
	private String restrictedMassage;
	private Boolean customerSurveyByEmail = false;
	private Boolean customerSurveyOnCustomerPortal = false;
	private String genericSurveyOnCfOrSo;
	private String oiJob;
	private String corporateId;
	private Date billToExtractDate;
	private Date vendorExtractDate;
	private String dashBoardHideJobs;
	private String licenseType;
	private Boolean allowAgentInvoiceUpload;
     private Date emailUploadStart;

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Company))
			return false;
		Company castOther = (Company) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(parentCorpId, castOther.parentCorpId)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(networkFlag, castOther.networkFlag)
				.append(companyName, castOther.companyName)
				.append(address1, castOther.address1)
				.append(currencySign, castOther.currencySign)
				.append(address2, castOther.address2)
				.append(city, castOther.city)
				.append(postal, castOther.postal)
				.append(reportValidationFlag, castOther.reportValidationFlag)
				.append(countryID, castOther.countryID)
				.append(contactNumber, castOther.contactNumber)
				.append(companyLOGO, castOther.companyLOGO)
				.append(timeZone, castOther.timeZone)
				.append(autoSavePrompt, castOther.autoSavePrompt)
				.append(companyDivisionFlag, castOther.companyDivisionFlag)
				.append(lastInvoiceNumber, castOther.lastInvoiceNumber)
				.append(lastTicketNumber, castOther.lastTicketNumber)
				.append(lastClaimNumber, castOther.lastClaimNumber)
				.append(toDoRuleExecutionTime, castOther.toDoRuleExecutionTime)
				.append(lastRunDate, castOther.lastRunDate)
				.append(multiCurrency, castOther.multiCurrency)
				.append(flagDefaultVendorAccountLines,
						castOther.flagDefaultVendorAccountLines)
				.append(userPasswordExpiryDuration,
						castOther.userPasswordExpiryDuration)
				.append(userActionOnExpiry, castOther.userActionOnExpiry)
				.append(partnerPasswordExpiryDuration,
						castOther.partnerPasswordExpiryDuration)
				.append(partnerActionOnExpiry, castOther.partnerActionOnExpiry)
				.append(lastCreditInvoice, castOther.lastCreditInvoice)
				.append(tracInternalUser, castOther.tracInternalUser)
				.append(localeLanguage, castOther.localeLanguage)
				.append(localeCountry, castOther.localeCountry)
				.append(clientBillingDate, castOther.clientBillingDate)
				.append(goLiveDate, castOther.goLiveDate)
				.append(jobType1, castOther.jobType1)
				.append(jobType2, castOther.jobType2)
				.append(jobType3, castOther.jobType3)
				.append(distanceFrom1, castOther.distanceFrom1)
				.append(distanceFrom2, castOther.distanceFrom2)
				.append(distanceFrom3, castOther.distanceFrom3)
				.append(grpDefault, castOther.grpDefault)
				.append(UTSI, castOther.UTSI)
				.append(voxmeIntegration, castOther.voxmeIntegration)
				.append(estimateVATFlag, castOther.estimateVATFlag)
				.append(weeklyBilling, castOther.weeklyBilling)
				.append(customerFeedback, castOther.customerFeedback)
				.append(autoGenerateAccRef, castOther.autoGenerateAccRef)
				.append(transfereeInfopackage, castOther.transfereeInfopackage)
				.append(justSayYes, castOther.justSayYes)
				.append(qualitySurvey, castOther.qualitySurvey)
				.append(workticketQueue, castOther.workticketQueue)
				.append(useCostTransferred, castOther.useCostTransferred)
				.append(lastTripNumber, castOther.lastTripNumber)
				.append(creditInvoiceSequence, castOther.creditInvoiceSequence)
				.append(postingDateStop, castOther.postingDateStop)
				.append(printInsuranceCertificate,
						castOther.printInsuranceCertificate)
				.append(certificateAdditionalNumber,
						castOther.certificateAdditionalNumber)
				.append(securityChecked, castOther.securityChecked)
				.append(ftpDir, castOther.ftpDir)
				.append(surveyEmailSign, castOther.surveyEmailSign)
				.append(automaticLinkup, castOther.automaticLinkup)
				.append(payablesXferWithApprovalOnly,
						castOther.payablesXferWithApprovalOnly)
				.append(RSBillingInstructions, castOther.RSBillingInstructions)
				.append(vanlineEnabled, castOther.vanlineEnabled)
				.append(postingDateFlexibility,
						castOther.postingDateFlexibility)
				.append(cmmdmmAgent, castOther.cmmdmmAgent)
				.append(autoFxUpdater, castOther.autoFxUpdater)
				.append(rskyBillGroup, castOther.rskyBillGroup)
				.append(defaultBillingRate, castOther.defaultBillingRate)
				.append(rate2, castOther.rate2)
				.append(email, castOther.email)
				.append(rulesRunning, castOther.rulesRunning)
				.append(allowFutureActualDateEntry,
						castOther.allowFutureActualDateEntry)
				.append(accessRedSkyCportalapp,
						castOther.accessRedSkyCportalapp)
				.append(scaling, castOther.scaling)
				.append(enablePricePoint, castOther.enablePricePoint)
				.append(priceStartDate, castOther.priceStartDate)
				.append(priceEndDate, castOther.priceEndDate)
				.append(subscriptionAmt, castOther.subscriptionAmt)
				.append(clientID, castOther.clientID)
				.append(ccEmail, castOther.ccEmail)
				.append(cportalAccessCompanyDivisionLevel,
						castOther.cportalAccessCompanyDivisionLevel)
				.append(chargeDiscountSetting, castOther.chargeDiscountSetting)
				.append(accessQuotationFromCustomerFile,
						castOther.accessQuotationFromCustomerFile)
				.append(cportalBrandingURL, castOther.cportalBrandingURL)
				.append(mmStartDate, castOther.mmStartDate)
				.append(mmEndDate, castOther.mmEndDate)
				.append(enableMobileMover, castOther.enableMobileMover)
				.append(mmSubscriptionAmt, castOther.mmSubscriptionAmt)
				.append(mmClientID, castOther.mmClientID)
				.append(rsAmount, castOther.rsAmount)
		        .append(numberOfPricePointUser, castOther.numberOfPricePointUser)
		        .append(quoteServices, castOther.quoteServices)
		        .append(surveyLinking,castOther.surveyLinking)
		        .append(landingPageWelcomeMsg,castOther.landingPageWelcomeMsg)
		        .append(agentTDREmail,castOther.agentTDREmail)
		        .append(singleCompanyDivisionInvoicing,castOther.singleCompanyDivisionInvoicing)
		        .append(privateCustomerTCode,castOther.privateCustomerTCode)
		        .append(minimumBillAmount,castOther.minimumBillAmount).append(UGRNNetting,castOther.UGRNNetting)
		       .append(storageBillingStatus,castOther.storageBillingStatus)
		       .append(accountLineNonEditable,castOther.accountLineNonEditable) 
		       .append(oAdAWeightVolumeMandatoryValidation,castOther.oAdAWeightVolumeMandatoryValidation)
		       .append(subOADASoftValidation,castOther.subOADASoftValidation)
		       .append(historyFx,castOther.historyFx)
		       .append(enableMoveForYou,castOther.enableMoveForYou)
		       .append(enableMSS,castOther.enableMSS)
		       .append(agentInvoiceSeed,castOther.agentInvoiceSeed)
		       .append(insurancePremiumTax,castOther.insurancePremiumTax)
		       .append(insurancePremiumTaxValue,castOther.insurancePremiumTaxValue)
		       .append(vatInsurancePremiumTax,castOther.vatInsurancePremiumTax)
		       .append(forceSoDashboard,castOther.forceSoDashboard)
		       .append(forceDocCenter,castOther.forceDocCenter)
		       .append(fullAudit,castOther.fullAudit)
		       .append(displayAPortalRevision,castOther.displayAPortalRevision)
		       .append(recInvoicePerSO, castOther.recInvoicePerSO)
		       .append(recInvoicePerSOJob, castOther.recInvoicePerSOJob)
		       .append(activityMgmtVersion2, castOther.activityMgmtVersion2)
		       .append(acctRefSameParent, castOther.acctRefSameParent)
		       .append(integrationUser, castOther.integrationUser)
		       .append(integrationPassword, castOther.integrationPassword)
		       .append(generateInvoiceBy, castOther.generateInvoiceBy)
		       .append(status, castOther.status)
		       .append(restrictAccess, castOther.restrictAccess)
		       .append(extractedFileAudit, castOther.extractedFileAudit) 
		       .append(restrictedMassage, castOther.restrictedMassage)
		       .append(customerSurveyByEmail, castOther.customerSurveyByEmail)
		       .append(customerSurveyOnCustomerPortal, castOther.customerSurveyOnCustomerPortal)
		       .append(genericSurveyOnCfOrSo, castOther.genericSurveyOnCfOrSo)
		       .append(oiJob, castOther.oiJob)
               .append(corporateId, castOther.corporateId)
               .append(vendorExtractDate, castOther.vendorExtractDate)
               .append(billToExtractDate, castOther.billToExtractDate)
               .append(dashBoardHideJobs, castOther.dashBoardHideJobs)
               .append(licenseType, castOther.licenseType) 
                 .append(allowAgentInvoiceUpload, castOther.allowAgentInvoiceUpload) 
                 .append(emailUploadStart, castOther.emailUploadStart) 
                .isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(parentCorpId).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(networkFlag)
				.append(companyName).append(address1).append(currencySign)
				.append(address2).append(city).append(postal)
				.append(reportValidationFlag).append(countryID)
				.append(contactNumber).append(companyLOGO).append(timeZone)
				.append(autoSavePrompt).append(companyDivisionFlag)
				.append(lastInvoiceNumber).append(lastTicketNumber)
				.append(lastClaimNumber).append(toDoRuleExecutionTime)
				.append(lastRunDate).append(multiCurrency)
				.append(flagDefaultVendorAccountLines)
				.append(userPasswordExpiryDuration).append(userActionOnExpiry)
				.append(partnerPasswordExpiryDuration)
				.append(partnerActionOnExpiry).append(lastCreditInvoice)
				.append(tracInternalUser).append(localeLanguage)
				.append(localeCountry).append(clientBillingDate)
				.append(goLiveDate).append(jobType1).append(jobType2)
				.append(jobType3).append(distanceFrom1).append(distanceFrom2)
				.append(distanceFrom3).append(grpDefault).append(UTSI)
				.append(voxmeIntegration).append(estimateVATFlag)
				.append(weeklyBilling).append(customerFeedback)
				.append(autoGenerateAccRef).append(transfereeInfopackage)
				.append(justSayYes).append(qualitySurvey)
				.append(workticketQueue).append(useCostTransferred)
				.append(lastTripNumber).append(creditInvoiceSequence)
				.append(postingDateStop).append(printInsuranceCertificate)
				.append(certificateAdditionalNumber).append(securityChecked)
				.append(ftpDir).append(surveyEmailSign).append(automaticLinkup)
				.append(payablesXferWithApprovalOnly)
				.append(RSBillingInstructions).append(vanlineEnabled)
				.append(postingDateFlexibility).append(cmmdmmAgent)
				.append(autoFxUpdater).append(rskyBillGroup)
				.append(defaultBillingRate).append(rate2).append(email)
				.append(rulesRunning).append(allowFutureActualDateEntry)
				.append(accessRedSkyCportalapp).append(scaling)
				.append(enablePricePoint).append(priceStartDate)
				.append(priceEndDate).append(subscriptionAmt).append(clientID)
				.append(ccEmail).append(cportalAccessCompanyDivisionLevel)
				.append(chargeDiscountSetting)
				.append(accessQuotationFromCustomerFile)
				.append(cportalBrandingURL).append(mmStartDate)
				.append(mmEndDate).append(enableMobileMover)
				.append(mmSubscriptionAmt).append(mmClientID)
				.append(rsAmount)
		        .append(numberOfPricePointUser)
		        .append(quoteServices)
		        .append(surveyLinking)
		        .append(landingPageWelcomeMsg).append(agentTDREmail).append(singleCompanyDivisionInvoicing)
		        .append(privateCustomerTCode).append(minimumBillAmount).append(UGRNNetting)
		        .append(storageBillingStatus).append(accountLineNonEditable)
		        .append(oAdAWeightVolumeMandatoryValidation).append(subOADASoftValidation)
		        .append(historyFx).append(enableMoveForYou).append(enableMSS)
		        .append(agentInvoiceSeed)
		        .append(insurancePremiumTax)
		        .append(insurancePremiumTaxValue)
		        .append(vatInsurancePremiumTax)
		        .append(forceSoDashboard)
		        .append(forceDocCenter)
		        .append(fullAudit)
		        .append(displayAPortalRevision)
		        .append(recInvoicePerSO)
		        .append(recInvoicePerSOJob)
		        .append(activityMgmtVersion2)
		        .append(acctRefSameParent)
		        .append(integrationUser)
		        .append(integrationPassword)
		        .append(generateInvoiceBy)
		        .append(status).append(restrictAccess)
		        .append(extractedFileAudit).append(restrictedMassage)
		        .append(customerSurveyByEmail).append(customerSurveyOnCustomerPortal)
		        .append(genericSurveyOnCfOrSo)
		        .append(oiJob)
                .append(corporateId)
                .append(vendorExtractDate)
                .append(billToExtractDate)
                .append(dashBoardHideJobs)
                .append(licenseType) 
                .append(allowAgentInvoiceUpload) 
                .append(emailUploadStart) 
                .toHashCode();
	}
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("corpID", corpID)
				.append("id", id)
				.append("parentCorpId", parentCorpId)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("networkFlag", networkFlag)
				.append("companyName", companyName)
				.append("address1", address1)
				.append("currencySign", currencySign)
				.append("address2", address2)
				.append("city", city)
				.append("postal", postal)
				.append("reportValidationFlag", reportValidationFlag)
				.append("countryID", countryID)
				.append("contactNumber", contactNumber)
				.append("companyLOGO", companyLOGO)
				.append("timeZone", timeZone)
				.append("autoSavePrompt", autoSavePrompt)
				.append("companyDivisionFlag", companyDivisionFlag)
				.append("lastInvoiceNumber", lastInvoiceNumber)
				.append("lastTicketNumber", lastTicketNumber)
				.append("lastClaimNumber", lastClaimNumber)
				.append("toDoRuleExecutionTime", toDoRuleExecutionTime)
				.append("lastRunDate", lastRunDate)
				.append("multiCurrency", multiCurrency)
				.append("flagDefaultVendorAccountLines",
						flagDefaultVendorAccountLines)
				.append("userPasswordExpiryDuration",
						userPasswordExpiryDuration)
				.append("userActionOnExpiry", userActionOnExpiry)
				.append("partnerPasswordExpiryDuration",
						partnerPasswordExpiryDuration)
				.append("partnerActionOnExpiry", partnerActionOnExpiry)
				.append("lastCreditInvoice", lastCreditInvoice)
				.append("tracInternalUser", tracInternalUser)
				.append("localeLanguage", localeLanguage)
				.append("localeCountry", localeCountry)
				.append("clientBillingDate", clientBillingDate)
				.append("goLiveDate", goLiveDate)
				.append("jobType1", jobType1)
				.append("jobType2", jobType2)
				.append("jobType3", jobType3)
				.append("distanceFrom1", distanceFrom1)
				.append("distanceFrom2", distanceFrom2)
				.append("distanceFrom3", distanceFrom3)
				.append("grpDefault", grpDefault)
				.append("UTSI", UTSI)
				.append("voxmeIntegration", voxmeIntegration)
				.append("estimateVATFlag", estimateVATFlag)
				.append("weeklyBilling", weeklyBilling)
				.append("customerFeedback", customerFeedback)
				.append("autoGenerateAccRef", autoGenerateAccRef)
				.append("transfereeInfopackage", transfereeInfopackage)
				.append("justSayYes", justSayYes)
				.append("qualitySurvey", qualitySurvey)
				.append("workticketQueue", workticketQueue)
				.append("useCostTransferred", useCostTransferred)
				.append("lastTripNumber", lastTripNumber)
				.append("creditInvoiceSequence", creditInvoiceSequence)
				.append("postingDateStop", postingDateStop)
				.append("printInsuranceCertificate", printInsuranceCertificate)
				.append("certificateAdditionalNumber",
						certificateAdditionalNumber)
				.append("securityChecked", securityChecked)
				.append("ftpDir", ftpDir)
				.append("surveyEmailSign", surveyEmailSign)
				.append("automaticLinkup", automaticLinkup)
				.append("payablesXferWithApprovalOnly",
						payablesXferWithApprovalOnly)
				.append("RSBillingInstructions", RSBillingInstructions)
				.append("vanlineEnabled", vanlineEnabled)
				.append("postingDateFlexibility", postingDateFlexibility)
				.append("cmmdmmAgent", cmmdmmAgent)
				.append("autoFxUpdater", autoFxUpdater)
				.append("rskyBillGroup", rskyBillGroup)
				.append("defaultBillingRate", defaultBillingRate)
				.append("rate2", rate2)
				.append("email", email)
				.append("rulesRunning", rulesRunning)
				.append("allowFutureActualDateEntry",
						allowFutureActualDateEntry)
				.append("accessRedSkyCportalapp", accessRedSkyCportalapp)
				.append("scaling", scaling)
				.append("enablePricePoint", enablePricePoint)
				.append("priceStartDate", priceStartDate)
				.append("priceEndDate", priceEndDate)
				.append("subscriptionAmt", subscriptionAmt)
				.append("clientID", clientID).append("ccEmail", ccEmail)
				.append("cportalAccessCompanyDivisionLevel", cportalAccessCompanyDivisionLevel)
				.append("chargeDiscountSetting", chargeDiscountSetting)
				.append("accessQuotationFromCustomerFile", accessQuotationFromCustomerFile)
				.append("mmStartDate",mmStartDate)
				.append("mmEndDate",mmEndDate)
				.append("enableMobileMover",enableMobileMover)
				.append("mmSubscriptionAmt",mmSubscriptionAmt)
				.append("mmClientID",mmClientID)
				.append("rsAmount",rsAmount)
				.append("numberOfPricePointUser",numberOfPricePointUser)
				.append("quoteServices",quoteServices)
				.append("surveyLinking",surveyLinking)
				.append("landingPageWelcomeMsg",landingPageWelcomeMsg)
				.append("agentTDREmail",agentTDREmail)
				.append("singleCompanyDivisionInvoicing",singleCompanyDivisionInvoicing)
				.append("privateCustomerTCode",privateCustomerTCode)
				.append("minimumBillAmount",minimumBillAmount).append("UGRNNetting",UGRNNetting)
				.append("storageBillingStatus",storageBillingStatus).append("accountLineNonEditable",accountLineNonEditable)
				.append("oAdAWeightVolumeMandatoryValidation", oAdAWeightVolumeMandatoryValidation)
				.append("subOADASoftValidation", subOADASoftValidation)
				.append("historyFx",historyFx)
				.append("enableMoveForYou",enableMoveForYou)
				.append("enableMSS",enableMSS)
				.append("agentInvoiceSeed",agentInvoiceSeed) 
				.append("insurancePremiumTax",insurancePremiumTax)
				.append("insurancePremiumTaxValue",insurancePremiumTaxValue)
				.append("vatInsurancePremiumTax",vatInsurancePremiumTax)
				.append("forceSoDashboard",forceSoDashboard) 
				.append("forceDocCenter",forceDocCenter)
				.append("fullAudit",fullAudit)
				.append("displayAPortalRevision",displayAPortalRevision)
				.append("recInvoicePerSO",recInvoicePerSO)
				.append("recInvoicePerSOJob",recInvoicePerSOJob)
				.append("activityMgmtVersion2",activityMgmtVersion2)
				.append("acctRefSameParent",acctRefSameParent)
				.append("integrationUser",integrationUser)
				.append("integrationPassword",integrationPassword)
				.append("generateInvoiceBy",generateInvoiceBy)
				.append("status",status)
				.append("restrictAccess",restrictAccess)
				.append("extractedFileAudit",extractedFileAudit).append("restrictedMassage",restrictedMassage)
				.append("customerSurveyByEmail",customerSurveyByEmail).append("customerSurveyOnCustomerPortal",customerSurveyOnCustomerPortal)
				.append("genericSurveyOnCfOrSo",genericSurveyOnCfOrSo)
				.append("oiJob", oiJob)
				.append("corporateId", corporateId)
                .append("vendorExtractDate", vendorExtractDate)
                .append("billToExtractDate", billToExtractDate)
                .append("dashBoardHideJobs", dashBoardHideJobs)
                .append("licenseType", licenseType) 
                 .append("allowAgentInvoiceUpload", allowAgentInvoiceUpload) 
                 .append("emailUploadStart", emailUploadStart) 
				.toString();
	}
	
	@Column
    public Boolean getInsurancePremiumTax() {
		return insurancePremiumTax;
	}
	public void setInsurancePremiumTax(Boolean insurancePremiumTax) {
		this.insurancePremiumTax = insurancePremiumTax;
	}
	@Column
	public BigDecimal getinsurancePremiumTaxValue() {
		return insurancePremiumTaxValue;
	}
	public void setinsurancePremiumTaxValue(BigDecimal insurancePremiumTaxValue) {
		this.insurancePremiumTaxValue = insurancePremiumTaxValue;
	}
	
	@Column
	public Boolean getChargeDiscountSetting() {
		return chargeDiscountSetting;
	}
	public void setChargeDiscountSetting(Boolean chargeDiscountSetting) {
		this.chargeDiscountSetting = chargeDiscountSetting;
	}
	//rskyBillGroup
	@Column(length=20)
	public Boolean getAccessRedSkyCportalapp() {
		return accessRedSkyCportalapp;
	}
	public void setAccessRedSkyCportalapp(Boolean accessRedSkyCportalapp) {
		this.accessRedSkyCportalapp = accessRedSkyCportalapp;
	}
	@Column(length=20)
	public Long getLastTicketNumber() {
		return lastTicketNumber;
	}
	public void setLastTicketNumber(Long lastTicketNumber) {
		this.lastTicketNumber = lastTicketNumber;
	}
	@Column(length=30)
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	
	@Column(length=30)
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	@Column(length=25)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(length=30)
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	@Column(length=20)
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	
	@Column(length=20)
	public String getCountryID() {
		return countryID;
	}
	public void setCountryID(String countryID) {
		this.countryID = countryID;
	}
	
	@Column(length=15)	
	public String getParentCorpId() {
		return parentCorpId;
	}
	public void setParentCorpId(String parentCorpId) {
		this.parentCorpId = parentCorpId;
	}
	
	@Column(length=15)
	public String getPostal() {
		return postal;
	}
	public void setPostal(String postal) {
		this.postal = postal;
	}
	
	@Column(length=15)
	//@Type(type = "String")
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	public File getCompanyLOGO() {
		return companyLOGO;
	}
	public void setCompanyLOGO(File companyLOGO) {
		this.companyLOGO = companyLOGO;
	}
	@Column(length=200)
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	@Column(length=5)
	public String getAutoSavePrompt() {
		return autoSavePrompt;
	}
	public void setAutoSavePrompt(String autoSavePrompt) {
		this.autoSavePrompt = autoSavePrompt;
	} 
	@Column(length=3)
	public String getCompanyDivisionFlag() {
		return companyDivisionFlag;
	}
	public void setCompanyDivisionFlag(String companyDivisionFlag) {
		this.companyDivisionFlag = companyDivisionFlag;
	}
	/**
	 * @return the reportValidationFlag
	 */
	@Column(length=3)
	public String getReportValidationFlag() {
		return reportValidationFlag;
	}
	/**
	 * @param reportValidationFlag the reportValidationFlag to set
	 */
	public void setReportValidationFlag(String reportValidationFlag) {
		this.reportValidationFlag = reportValidationFlag;
	}
	@Column(length=20)
	public String getLastInvoiceNumber() {
		return lastInvoiceNumber;
	}
	public void setLastInvoiceNumber(String lastInvoiceNumber) {
		this.lastInvoiceNumber = lastInvoiceNumber;
	}
	@Column(length=20)
	public Long getLastClaimNumber() {
		return lastClaimNumber;
	}
	public void setLastClaimNumber(Long lastClaimNumber) {
		this.lastClaimNumber = lastClaimNumber;
	}
	@Column
	public Date getLastRunDate() {
		return lastRunDate;
	}
	public void setLastRunDate(Date lastRunDate) {
		this.lastRunDate = lastRunDate;
	}
	@Column(length=20)
	public String getToDoRuleExecutionTime() {
		return toDoRuleExecutionTime;
	}
	public void setToDoRuleExecutionTime(String toDoRuleExecutionTime) {
		this.toDoRuleExecutionTime = toDoRuleExecutionTime;
	}
	@Column(length = 2)
	public String getMultiCurrency() {
		return multiCurrency;
	}
	public void setMultiCurrency(String multiCurrency) {
		this.multiCurrency = multiCurrency;
	}
	@Column
	public Boolean getFlagDefaultVendorAccountLines() {
		return flagDefaultVendorAccountLines;
	}
	public void setFlagDefaultVendorAccountLines(
			Boolean flagDefaultVendorAccountLines) {
		this.flagDefaultVendorAccountLines = flagDefaultVendorAccountLines;
	}
	@Column(length = 20)
	public String getPartnerActionOnExpiry() {
		return partnerActionOnExpiry;
	}
	public void setPartnerActionOnExpiry(String partnerActionOnExpiry) {
		this.partnerActionOnExpiry = partnerActionOnExpiry;
	}
	@Column(length = 5)
	public Long getPartnerPasswordExpiryDuration() {
		return partnerPasswordExpiryDuration;
	}
	public void setPartnerPasswordExpiryDuration(Long partnerPasswordExpiryDuration) {
		this.partnerPasswordExpiryDuration = partnerPasswordExpiryDuration;
	}
	@Column(length = 20)
	public String getUserActionOnExpiry() {
		return userActionOnExpiry;
	}
	public void setUserActionOnExpiry(String userActionOnExpiry) {
		this.userActionOnExpiry = userActionOnExpiry;
	}
	@Column(length = 5)
	public Long getUserPasswordExpiryDuration() {
		return userPasswordExpiryDuration;
	}
	public void setUserPasswordExpiryDuration(Long userPasswordExpiryDuration) {
		this.userPasswordExpiryDuration = userPasswordExpiryDuration;
	}
	@Column(length=20)
	public String getLastCreditInvoice() {
		return lastCreditInvoice;
	}
	public void setLastCreditInvoice(String lastCreditInvoice) {
		this.lastCreditInvoice = lastCreditInvoice;
	}
	@Column
	public Boolean getNetworkFlag() {
		return networkFlag;
	}
	public void setNetworkFlag(Boolean networkFlag) {
		this.networkFlag = networkFlag;
	}
	@Column
	public Boolean getTracInternalUser() {
		return tracInternalUser;
	}
	public void setTracInternalUser(Boolean tracInternalUser) {
		this.tracInternalUser = tracInternalUser;
	}
	
	@Column(length=10)
	public String getCurrencySign() {
		return currencySign;
	}

	public void setCurrencySign(String currencySign) {
		this.currencySign = currencySign;
	}
	@Column(length=2)
	public String getLocaleCountry() {
		return localeCountry;
	}
	public void setLocaleCountry(String localeCountry) {
		this.localeCountry = localeCountry;
	}
	@Column(length=2)
	public String getLocaleLanguage() {
		return localeLanguage;
	}
	public void setLocaleLanguage(String localeLanguage) {
		this.localeLanguage = localeLanguage;
	}
	
	@Column
	public Date getClientBillingDate() {
		return clientBillingDate;
	}
	public void setClientBillingDate(Date clientBillingDate) {
		this.clientBillingDate = clientBillingDate;
	}

	@Column
	public Date getGoLiveDate() {
		return goLiveDate;
	}
	public void setGoLiveDate(Date goLiveDate) {
		this.goLiveDate = goLiveDate;
	}
	@Column(length=15)
	public String getDistanceFrom1() {
		return distanceFrom1;
	}
	public void setDistanceFrom1(String distanceFrom1) {
		this.distanceFrom1 = distanceFrom1;
	}
	@Column(length=15)
	public String getDistanceFrom2() {
		return distanceFrom2;
	}
	public void setDistanceFrom2(String distanceFrom2) {
		this.distanceFrom2 = distanceFrom2;
	}
	@Column(length=15)
	public String getDistanceFrom3() {
		return distanceFrom3;
	}
	public void setDistanceFrom3(String distanceFrom3) {
		this.distanceFrom3 = distanceFrom3;
	}
	@Column(length=5)
	public String getGrpDefault() {
		return grpDefault;
	}
	public void setGrpDefault(String grpDefault) {
		this.grpDefault = grpDefault;
	}
	@Column(length=100)
	public String getJobType1() {
		return jobType1;
	}
	public void setJobType1(String jobType1) {
		this.jobType1 = jobType1;
	}
	@Column(length=100)
	public String getJobType2() {
		return jobType2;
	}
	public void setJobType2(String jobType2) {
		this.jobType2 = jobType2;
	}
	@Column(length=100)
	public String getJobType3() {
		return jobType3;
	}
	public void setJobType3(String jobType3) {
		this.jobType3 = jobType3;
	}
	@Column
	public Boolean getUTSI() {
		return UTSI;
	}
	public void setUTSI(Boolean uTSI) {
		UTSI = uTSI;
	}
	@Column
	public Boolean getVoxmeIntegration() {
		return voxmeIntegration;
	}
	public void setVoxmeIntegration(Boolean voxmeIntegration) {
		this.voxmeIntegration = voxmeIntegration;
	}
	@Column(length = 2)
	public String getEstimateVATFlag() {
		return estimateVATFlag;
	}
	public void setEstimateVATFlag(String estimateVATFlag) {
		this.estimateVATFlag = estimateVATFlag;
	}
	@Column
	public Boolean getWeeklyBilling() {
		return weeklyBilling;
	}
	public void setWeeklyBilling(Boolean weeklyBilling) {
		this.weeklyBilling = weeklyBilling;
	}
	@Column(length = 25)
	public String getCustomerFeedback() {
		return customerFeedback;
	}	
	public void setCustomerFeedback(String customerFeedback) {
		this.customerFeedback = customerFeedback;
	}
	@Column(length = 1)
	public String getAutoGenerateAccRef() {
		return autoGenerateAccRef;
	}
	public void setAutoGenerateAccRef(String autoGenerateAccRef) {
		this.autoGenerateAccRef = autoGenerateAccRef;
	}
	@Column
	public Boolean getTransfereeInfopackage() {
		return transfereeInfopackage;
	}
	public void setTransfereeInfopackage(Boolean transfereeInfopackage) {
		this.transfereeInfopackage = transfereeInfopackage;
	}
	@Column
	public Boolean getJustSayYes() {
		return justSayYes;
	}
	public void setJustSayYes(Boolean justSayYes) {
		this.justSayYes = justSayYes;
	}
	@Column
	public Boolean getQualitySurvey() {
		return qualitySurvey;
	}
	public void setQualitySurvey(Boolean qualitySurvey) {
		this.qualitySurvey = qualitySurvey;
	}
	@Column
	public Boolean getWorkticketQueue() {
		return workticketQueue;
	}
	public void setWorkticketQueue(Boolean workticketQueue) {
		this.workticketQueue = workticketQueue;
	}
	@Column
	public Boolean getUseCostTransferred() {
		return useCostTransferred;
	}
	public void setUseCostTransferred(Boolean useCostTransferred) {
		this.useCostTransferred = useCostTransferred;
	}
	public String getLastTripNumber() {
		return lastTripNumber;
	}
	public void setLastTripNumber(String lastTripNumber) {
		this.lastTripNumber = lastTripNumber;
	}
	@Column
	public Boolean getCreditInvoiceSequence() {
		return creditInvoiceSequence;
	}
	public void setCreditInvoiceSequence(Boolean creditInvoiceSequence) {
		this.creditInvoiceSequence = creditInvoiceSequence;
	}
	public Boolean getPostingDateStop() {
		return postingDateStop;
	}
	public void setPostingDateStop(Boolean postingDateStop) {
		this.postingDateStop = postingDateStop;
	}
	public Boolean getPrintInsuranceCertificate() {
		return printInsuranceCertificate;
	}
	public void setPrintInsuranceCertificate(Boolean printInsuranceCertificate) {
		this.printInsuranceCertificate = printInsuranceCertificate;
	}
	public Long getCertificateAdditionalNumber() {
		return certificateAdditionalNumber;
	}
	public void setCertificateAdditionalNumber(Long certificateAdditionalNumber) {
		this.certificateAdditionalNumber = certificateAdditionalNumber;
	}
	@Column
	public Boolean getSecurityChecked() {
		return securityChecked;
	}
	public void setSecurityChecked(Boolean securityChecked) {
		this.securityChecked = securityChecked;
	}
	@Column
	public String getSurveyEmailSign() {
		return surveyEmailSign;
	}
	public void setSurveyEmailSign(String surveyEmailSign) {
		this.surveyEmailSign = surveyEmailSign;
	}
	@Column
	public Boolean getAutomaticLinkup() {
		return automaticLinkup;
	}
	public void setAutomaticLinkup(Boolean automaticLinkup) {
		this.automaticLinkup = automaticLinkup;
	}
	public String getFtpDir() {
		return ftpDir;
	}
	public void setFtpDir(String ftpDir) {
		this.ftpDir = ftpDir;
	}
	public Boolean getPayablesXferWithApprovalOnly() {
		return payablesXferWithApprovalOnly;
	}
	public void setPayablesXferWithApprovalOnly(Boolean payablesXferWithApprovalOnly) {
		this.payablesXferWithApprovalOnly = payablesXferWithApprovalOnly;
	}
	public String getRSBillingInstructions() {
		return RSBillingInstructions;
	}
	public void setRSBillingInstructions(String rSBillingInstructions) {
		RSBillingInstructions = rSBillingInstructions;
	}
	public Boolean getVanlineEnabled() {
		return vanlineEnabled;
	}
	public void setVanlineEnabled(Boolean vanlineEnabled) {
		this.vanlineEnabled = vanlineEnabled;
	}
	public Boolean getPostingDateFlexibility() {
		return postingDateFlexibility;
	}
	public void setPostingDateFlexibility(Boolean postingDateFlexibility) {
		this.postingDateFlexibility = postingDateFlexibility;
	}
	@Column
	public Boolean getCmmdmmAgent() {
		return cmmdmmAgent;
	}
	public void setCmmdmmAgent(Boolean cmmdmmAgent) {
		this.cmmdmmAgent = cmmdmmAgent;
	}
	@Column
	public Boolean getAutoFxUpdater() {
		return autoFxUpdater;
	}
	public void setAutoFxUpdater(Boolean autoFxUpdater) {
		this.autoFxUpdater = autoFxUpdater;
	}

	@Column
	public String getRskyBillGroup() {
		return rskyBillGroup;
	}
	public void setRskyBillGroup(String rskyBillGroup) {
		this.rskyBillGroup = rskyBillGroup;
	}
	@Column
	public BigDecimal getDefaultBillingRate() {
		return defaultBillingRate;
	}
	public void setDefaultBillingRate(BigDecimal defaultBillingRate) {
		this.defaultBillingRate = defaultBillingRate;
	}
	@Column
	public BigDecimal getRate2() {
		return rate2;
	}
	public void setRate2(BigDecimal rate2) {
		this.rate2 = rate2;
	}
	@Column
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Column
	public Boolean getRulesRunning() {
		return rulesRunning;
	}
	public void setRulesRunning(Boolean rulesRunning) {
		this.rulesRunning = rulesRunning;
	}
	public Boolean getAllowFutureActualDateEntry() {
		return allowFutureActualDateEntry;
	}
	@Column
	public void setAllowFutureActualDateEntry(Boolean allowFutureActualDateEntry) {
		this.allowFutureActualDateEntry = allowFutureActualDateEntry;
	}
	@Column(length=10)
	public String getScaling() {
		return scaling;
	}
	public void setScaling(String scaling) {
		this.scaling = scaling;
	}
	@Column
	public Boolean getEnablePricePoint() {
		return enablePricePoint;
	}
	public void setEnablePricePoint(Boolean enablePricePoint) {
		this.enablePricePoint = enablePricePoint;
	}
	@Column
	public Date getPriceStartDate() {
		return priceStartDate;
	}
	public void setPriceStartDate(Date priceStartDate) {
		this.priceStartDate = priceStartDate;
	}
	@Column
	public Date getPriceEndDate() {
		return priceEndDate;
	}
	public void setPriceEndDate(Date priceEndDate) {
		this.priceEndDate = priceEndDate;
	}
	@Column
	public BigDecimal getSubscriptionAmt() {
		return subscriptionAmt;
	}
	public void setSubscriptionAmt(BigDecimal subscriptionAmt) {
		this.subscriptionAmt = subscriptionAmt;
	}
	@Column
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	@Column
	public String getCcEmail() {
		return ccEmail;
	}
	public void setCcEmail(String ccEmail) {
		this.ccEmail = ccEmail;
	}
	@Column
	public Boolean getCportalAccessCompanyDivisionLevel() {
		return cportalAccessCompanyDivisionLevel;
	}
	public void setCportalAccessCompanyDivisionLevel(
			Boolean cportalAccessCompanyDivisionLevel) {
		this.cportalAccessCompanyDivisionLevel = cportalAccessCompanyDivisionLevel;
	}
	@Column
	public Boolean getAccessQuotationFromCustomerFile() {
		return accessQuotationFromCustomerFile;
	}
	public void setAccessQuotationFromCustomerFile(
			Boolean accessQuotationFromCustomerFile) {
		this.accessQuotationFromCustomerFile = accessQuotationFromCustomerFile;
	}
	public String getCportalBrandingURL() {
		return cportalBrandingURL;
	}
	public void setCportalBrandingURL(String cportalBrandingURL) {
		this.cportalBrandingURL = cportalBrandingURL;
	}
	@Column
	public Date getMmStartDate() {
		return mmStartDate;
	}
	public void setMmStartDate(Date mmStartDate) {
		this.mmStartDate = mmStartDate;
	}
	@Column
	public Date getMmEndDate() {
		return mmEndDate;
	}
	public void setMmEndDate(Date mmEndDate) {
		this.mmEndDate = mmEndDate;
	}
	@Column
	public Boolean getEnableMobileMover() {
		return enableMobileMover;
	}
	public void setEnableMobileMover(Boolean enableMobileMover) {
		this.enableMobileMover = enableMobileMover;
	}
	@Column
	public BigDecimal getMmSubscriptionAmt() {
		return mmSubscriptionAmt;
	}
	public void setMmSubscriptionAmt(BigDecimal mmSubscriptionAmt) {
		this.mmSubscriptionAmt = mmSubscriptionAmt;
	}
	@Column
	public String getMmClientID() {
		return mmClientID;
	}
	public void setMmClientID(String mmClientID) {
		this.mmClientID = mmClientID;
	}
	@Column
	public BigDecimal getRsAmount() {
		return rsAmount;
	}
	public void setRsAmount(BigDecimal rsAmount) {
		this.rsAmount = rsAmount;
	}
	@Column
	public Long getNumberOfPricePointUser() {
		return numberOfPricePointUser;
	}
	public void setNumberOfPricePointUser(Long numberOfPricePointUser) {
		this.numberOfPricePointUser = numberOfPricePointUser;
	}
	@Column
	public String getQuoteServices() {
		return quoteServices;
	}
	public void setQuoteServices(String quoteServices) {
		this.quoteServices = quoteServices;
	}
	@Column
	public Boolean getSurveyLinking() {
		return surveyLinking;
	}
	public void setSurveyLinking(Boolean surveyLinking) {
		this.surveyLinking = surveyLinking;
	}
	@Column
	public String getLandingPageWelcomeMsg() {
		return landingPageWelcomeMsg;
	}
	public void setLandingPageWelcomeMsg(String landingPageWelcomeMsg) {
		this.landingPageWelcomeMsg = landingPageWelcomeMsg;
	}
	@Column
	public Boolean getAgentTDREmail() {
		return agentTDREmail;
	}
	public void setAgentTDREmail(Boolean agentTDREmail) {
		this.agentTDREmail = agentTDREmail;
	}
	@Column
	public Boolean getSingleCompanyDivisionInvoicing() {
		return singleCompanyDivisionInvoicing;
	}
	public void setSingleCompanyDivisionInvoicing(
			Boolean singleCompanyDivisionInvoicing) {
		this.singleCompanyDivisionInvoicing = singleCompanyDivisionInvoicing;
	}
	@Column
	public String getPrivateCustomerTCode() {
		return privateCustomerTCode;
	}
	public void setPrivateCustomerTCode(String privateCustomerTCode) {
		this.privateCustomerTCode = privateCustomerTCode;
	}
	@Column
	public BigDecimal getMinimumBillAmount() {
		return minimumBillAmount;
	}
	public void setMinimumBillAmount(BigDecimal minimumBillAmount) {
		this.minimumBillAmount = minimumBillAmount;
	}
	@Column
	public Boolean getUGRNNetting() {
		return UGRNNetting;
	}
	public void setUGRNNetting(Boolean uGRNNetting) {
		UGRNNetting = uGRNNetting;
	}
	@Column
	public Boolean getStorageBillingStatus() {
		return storageBillingStatus;
	}
	public void setStorageBillingStatus(Boolean storageBillingStatus) {
		this.storageBillingStatus = storageBillingStatus;
	}
	@Column
	public String getAccountLineNonEditable() {
		return accountLineNonEditable;
	}
	public void setAccountLineNonEditable(String accountLineNonEditable) {
		this.accountLineNonEditable = accountLineNonEditable;
	}
	@Column
	public Boolean getsubOADASoftValidation() {
		return subOADASoftValidation;
	}
	public void setsubOADASoftValidation(Boolean subOADASoftValidation) {
		this.subOADASoftValidation = subOADASoftValidation;
	}
	@Column
	public Boolean getoAdAWeightVolumeMandatoryValidation() {
		return oAdAWeightVolumeMandatoryValidation;
	}
	public void setoAdAWeightVolumeMandatoryValidation(Boolean oAdAWeightVolumeMandatoryValidation) {
		this.oAdAWeightVolumeMandatoryValidation = oAdAWeightVolumeMandatoryValidation;
	}
	@Column
	public Boolean getHistoryFx() {
		return historyFx;
	}
	public void setHistoryFx(Boolean historyFx) {
		this.historyFx = historyFx;
	}
	@Column
	public Boolean getEnableMoveForYou() {
		return enableMoveForYou;
	}
	public void setEnableMoveForYou(Boolean enableMoveForYou) {
		this.enableMoveForYou = enableMoveForYou;
	}
	@Column
	public Boolean getEnableMSS() {
		return enableMSS;
	}
	public void setEnableMSS(Boolean enableMSS) {
		this.enableMSS = enableMSS;
	}
	@Column
	public Boolean getAgentInvoiceSeed() {
		return agentInvoiceSeed;
	}
	public void setAgentInvoiceSeed(Boolean agentInvoiceSeed) {
		this.agentInvoiceSeed = agentInvoiceSeed;
	}
	
	
	
	@Column
	public String getVatInsurancePremiumTax() {
		return vatInsurancePremiumTax;
	}
	public void setVatInsurancePremiumTax(String vatInsurancePremiumTax) {
		this.vatInsurancePremiumTax = vatInsurancePremiumTax;
	}
	@Column
	public Boolean getForceSoDashboard() {
		return forceSoDashboard;
	}
	public void setForceSoDashboard(Boolean forceSoDashboard) {
		this.forceSoDashboard = forceSoDashboard;
	}
	@Column
	public Boolean getForceDocCenter() {
		return forceDocCenter;
	}
	public void setForceDocCenter(Boolean forceDocCenter) {
		this.forceDocCenter = forceDocCenter;
	}
	
	@Column
	public Boolean getFullAudit() {
		return fullAudit;
	}
	
	public void setFullAudit(Boolean fullAudit) {
		this.fullAudit = fullAudit;
	}
	@Column
	public Boolean getDisplayAPortalRevision() {
		return displayAPortalRevision;
	}
	public void setDisplayAPortalRevision(Boolean displayAPortalRevision) {
		this.displayAPortalRevision = displayAPortalRevision;
	}
	
	
	@Column
	public Boolean getRecInvoicePerSO() {
		return recInvoicePerSO;
	}
	public void setRecInvoicePerSO(Boolean recInvoicePerSO) {
		this.recInvoicePerSO = recInvoicePerSO;
	}
	@Column
	public String getRecInvoicePerSOJob() {
		return recInvoicePerSOJob;
	}
	public void setRecInvoicePerSOJob(String recInvoicePerSOJob) {
		this.recInvoicePerSOJob = recInvoicePerSOJob;
	}
	@Column
	public Boolean getActivityMgmtVersion2() {
		return activityMgmtVersion2;
	}
	public void setActivityMgmtVersion2(Boolean activityMgmtVersion2) {
		this.activityMgmtVersion2 = activityMgmtVersion2;
	}
	public Boolean getAcctRefSameParent() {
		return acctRefSameParent;
	}
	public void setAcctRefSameParent(Boolean acctRefSameParent) {
		this.acctRefSameParent = acctRefSameParent;
	}
	@Column
	public String getIntegrationUser() {
		return integrationUser;
	}
	public void setIntegrationUser(String integrationUser) {
		this.integrationUser = integrationUser;
	}
	@Column
	public String getIntegrationPassword() {
		return integrationPassword;
	}
	public void setIntegrationPassword(String integrationPassword) {
		this.integrationPassword = integrationPassword;
	}
	@Column
	public String getGenerateInvoiceBy() {
		return generateInvoiceBy;
	}
	public void setGenerateInvoiceBy(String generateInvoiceBy) {
		this.generateInvoiceBy = generateInvoiceBy;
	}
	@Column
	public Boolean getStatus() {
		return status;
	}
	public void setStatus(Boolean status) {
		this.status = status;
	}
	@Column
	public Boolean getRestrictAccess() {
		return restrictAccess;
	}
	public void setRestrictAccess(Boolean restrictAccess) {
		this.restrictAccess = restrictAccess;
	}
	@Column
	public Boolean getExtractedFileAudit() {
		return extractedFileAudit;
	}
	public void setExtractedFileAudit(Boolean extractedFileAudit) {
		this.extractedFileAudit = extractedFileAudit;
	}
	@Column
	public String getRestrictedMassage() {
		return restrictedMassage;
	}
	public void setRestrictedMassage(String restrictedMassage) {
		this.restrictedMassage = restrictedMassage;
	}
	public Boolean getCustomerSurveyByEmail() {
		return customerSurveyByEmail;
	}
	public void setCustomerSurveyByEmail(Boolean customerSurveyByEmail) {
		this.customerSurveyByEmail = customerSurveyByEmail;
	}
	public Boolean getCustomerSurveyOnCustomerPortal() {
		return customerSurveyOnCustomerPortal;
	}
	public void setCustomerSurveyOnCustomerPortal(
			Boolean customerSurveyOnCustomerPortal) {
		this.customerSurveyOnCustomerPortal = customerSurveyOnCustomerPortal;
	}
	public String getGenericSurveyOnCfOrSo() {
		return genericSurveyOnCfOrSo;
	}
	public void setGenericSurveyOnCfOrSo(String genericSurveyOnCfOrSo) {
		this.genericSurveyOnCfOrSo = genericSurveyOnCfOrSo;
	}
	@Column
    public String getOiJob() {
		return oiJob;
	}
	public void setOiJob(String oiJob) {
		this.oiJob = oiJob;
	}
	@Column
	public String getCorporateId() {
		return corporateId;
	}
	public void setCorporateId(String corporateId) {
		this.corporateId = corporateId;
	}
	@Column
	public Date getBillToExtractDate() {
		return billToExtractDate;
	}
	public void setBillToExtractDate(Date billToExtractDate) {
		this.billToExtractDate = billToExtractDate;
	}
	@Column
	public Date getVendorExtractDate() {
		return vendorExtractDate;
	}
	public void setVendorExtractDate(Date vendorExtractDate) {
		this.vendorExtractDate = vendorExtractDate;
	}
	@Column
	public String getDashBoardHideJobs() {
		return dashBoardHideJobs;
	}
	public void setDashBoardHideJobs(String dashBoardHideJobs) {
		this.dashBoardHideJobs = dashBoardHideJobs;
	}
	@Column
	public String getLicenseType() {
		return licenseType;
	}
	public void setLicenseType(String licenseType) {
		this.licenseType = licenseType;
	}
	@Column
	public Boolean getAllowAgentInvoiceUpload() {
		return allowAgentInvoiceUpload;
	}
	public void setAllowAgentInvoiceUpload(Boolean allowAgentInvoiceUpload) {
		this.allowAgentInvoiceUpload = allowAgentInvoiceUpload;
	}
	
	@Column
	public Date getEmailUploadStart() {
		return emailUploadStart;
	}
	public void setEmailUploadStart(Date emailUploadStart) {
		this.emailUploadStart = emailUploadStart;
	}
	
}
