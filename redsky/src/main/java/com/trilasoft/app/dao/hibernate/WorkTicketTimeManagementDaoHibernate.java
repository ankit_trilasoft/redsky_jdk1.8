package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.WorkTicketTimeManagementDao;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.WorkTicketTimeManagement;

public class WorkTicketTimeManagementDaoHibernate extends GenericDaoHibernate<WorkTicketTimeManagement, Long>implements WorkTicketTimeManagementDao{
	public WorkTicketTimeManagementDaoHibernate() {
		super(WorkTicketTimeManagement.class);
	}
	public List getTimeManagementManagerDetails(Long Id, Long ticket, String sessionCorpID){
		String query="from WorkTicketTimeManagement where workTicketId='"+Id+"' and ticket='"+ticket+"' and corpId='"+sessionCorpID+"'";
		List list = getHibernateTemplate().find(query);
		return list;	
	}
}
