<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>   
    <title>Task Planning</title>   
    <meta name="heading" content="Task Planning"/>   
    <script language="javascript" type="text/javascript">
</script> 
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
!margin-bottom:3px;
margin-top:-18px;
!margin-top:-15px;
padding:2px 0px;
text-align:right;
width:99%;
}
form {
margin-top:-12px;
}

div#main {
margin:-5px 0 0;

}
</style> 

</head>
<%-- <c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="editTaskPlanning.html"/>'" value="<fmt:message key="button.add"/>"/> 
</c:set>  --%> 
<s:form id="searchForm" action='' method="post" validate="true" >
<div id="layer1" style="width:100%; ">
<c:set var="cid" value="<%=request.getParameter("cid") %>"/>
<s:hidden name="cid" id ="cid" value="<%=request.getParameter("cid") %>" />
<s:set name="taskPlanningList" value="taskPlanningList" scope="request"/>  
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
    <c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %> 
<div id="otabs" style="margin-top: -15px;">
		  <ul>
		    <li><a class="current"><span>Task Planning</span></a></li>
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		
<display:table name="taskPlanningList" class="table" requestURI="" id="taskPlanningList"  defaultsort="2" pagesize="10" style="width:99%; margin-left:5px;margin-top: 2px;!margin-top:-5px;" >   
  <display:column sortable="true" title="External?"><a href="editTaskPlanning.html?id=${taskPlanningList.id}&cid=${cid}" style="cursor:pointer"/><c:out value="${taskPlanningList.taskVia}" /></display:column>
        <display:column sortable="true" title="Company"><c:out value="${taskPlanningList.taskCompanyDesc}" /></display:column>
          <display:column sortable="true" title="Planner"><c:out value="${taskPlanningList.taskPlannerDesc}" /></display:column>
            <display:column sortable="true" title="Assignee"><c:out value="${taskPlanningList.taskAssigneeDesc}" /></display:column>
              <display:column sortable="true" title="Location"><c:out value="${taskPlanningList.taskLocation}" /></display:column>
                <display:column sortable="true" title="Type"><c:out value="${taskPlanningList.taskType}" /></display:column>
                 <display:column sortable="true" property="taskDate" title="Date" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
                  <display:column sortable="true" title="From"><c:out value="${taskPlanningList.taskTimeFrom}" /></display:column>
                     <display:column sortable="true" title="To"><c:out value="${taskPlanningList.taskTimeTo}" /></display:column>
                      <display:column sortable="true" property="taskCompleteBefore" title="Complete Before" style="width:100px" format="{0,date,dd-MMM-yyyy}"/>
                       <display:column sortable="true" title="Comment"><c:out value="${taskPlanningList.taskComment}" /></display:column>
                      
 
</display:table>   
 
<input id="addInv" type="button"  class="cssbutton" style="width:55px; height:25px;margin-left:25px;"  value="Add" onclick="add();"  />
</s:form>
<script type="text/javascript">   
function add(){
	window.location ="editTaskPlanning.html?cid=${customerFile.id}&id=";
}
</script>  
