package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.QualitySurveySettings;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.SurveyEmailAudit;
import com.trilasoft.app.service.AccountContactManager;
import com.trilasoft.app.service.EmailSetupManager;
import com.trilasoft.app.service.QualitySurveySettingsManager;
import com.trilasoft.app.service.RefMasterManager;

public class QualitySurveySettingsAction extends BaseAction implements Preparable{
	private QualitySurveySettingsManager qualitySurveySettingsManager;
	private QualitySurveySettings qualitySurveySettings;
	private String sessionCorpID;
	private List <QualitySurveySettings>qualitySurveySettingsList;
    private Long id;
    static final Logger logger = Logger.getLogger(QualitySurveySettings.class);
    private RefMasterManager refMasterManager;
    private Map<String, String> sendLimits;
    private Map<String, String> delayDays;
    private Map<String, String> surveyFromList;
    private String surveyFrom;
    public QualitySurveySettingsAction()
    {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();
    }
    private Map<String, String> parameterRecipient = new LinkedHashMap<String, String>();
    private Map<String, String> parameterBody = new LinkedHashMap<String, String>();
    private Map<String, String> parameterSubject = new LinkedHashMap<String, String>();
    private Map<String, String> parameterSignature = new LinkedHashMap<String, String>();
    private Map<String, String> surveyIdMap = new LinkedHashMap<String, String>();
    private Map<String, String> answerIdMap = new LinkedHashMap<String, String>();

	private String configType;
    private String emailCode;
    private String emailDesription;
    Date currentdate = new Date();
    private Map<String, String> jobList;
    public void prepare() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
	    	sendLimits = refMasterManager.findByParameter(sessionCorpID, "SENDLIMITS");
	        delayDays = refMasterManager.findByParameter(sessionCorpID, "DELAYDAYS");    
	        surveyFromList = refMasterManager.findByRelocationServices(sessionCorpID, "QUALITYSURVEYURL");
	        surveyIdMap=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QUALITYSURVEYURL");
	        answerIdMap=refMasterManager.findByAnswerParamSurveyQualityParameter(sessionCorpID, "ANSWERIDPARAM");
	        jobList=refMasterManager.findByParameter(sessionCorpID, "JOB");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    }
    private AccountContactManager accountContactManager;
	private EmailSetupManager emailSetupManager;
	public void setEmailSetupManager(EmailSetupManager emailSetupManager) {
		this.emailSetupManager = emailSetupManager;
	}
	private Boolean excludeFPU;	
    public String getEmailConfigrationOfPartner()
    {
		parameterRecipient=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYRECIPIENT");
		parameterBody=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYBODY");
		parameterSignature=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYSIGNATURE");
		parameterSubject=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYSUBJECT");
		qualitySurveySettingsList=qualitySurveySettingsManager.getQualitySurveySettingsDate(accountId,sessionCorpID,mode);
		String accountCodeTemp=accountId+"~"+mode;
		excludeFPU=qualitySurveySettingsManager.getExcludeFPU(accountCodeTemp,sessionCorpID);
    	if((qualitySurveySettingsList!=null)&&(!qualitySurveySettingsList.isEmpty()))
    	{
    		int i=0;
    		for (QualitySurveySettings qualitySurveySettings : qualitySurveySettingsList) {
    			
    			if(i==0)
    			{
    			accountId=qualitySurveySettings.getAccountId();
    			delayInterval=qualitySurveySettings.getDelayInterval()+"";
    			sendLimit=qualitySurveySettings.getSendLimit()+"";
    			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
    			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
        	    	recipient0=qualitySurveySettings.getSurveyEmailRecipientId()+"";
        	    	}
        	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
        	    	subject0=qualitySurveySettings.getSurveyEmailSubjectId()+"";
        	    	}
        	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
        	    	body0=qualitySurveySettings.getSurveyEmailBodyId()+"";
        	    	}
        	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
        	    	signature0=qualitySurveySettings.getSurveyEmailSignatureId()+"";
        	    	} 
    			}else if(i==1){
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient1=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject1=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body1=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
            	    	signature1=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}else if(i==2){
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient2=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject2=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body2=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
            	    	signature2=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}else if(i==3){
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient3=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject3=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body3=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
            	    	signature3=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}else if(i==4){
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient4=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject4=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body4=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
            	    	signature4=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}else if(i==5){
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient5=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject5=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body5=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{
            	    	signature5=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}else{
        			accountId=qualitySurveySettings.getAccountId();
        			delayInterval=qualitySurveySettings.getDelayInterval()+"";
        			sendLimit=qualitySurveySettings.getSendLimit()+"";
        			surveyFrom=qualitySurveySettings.getSurveyFrom()+"";
        			if(qualitySurveySettings.getSurveyEmailRecipientId()!=null)    	    	{
            	    	recipient6=qualitySurveySettings.getSurveyEmailRecipientId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSubjectId()!=null)    	    	{
            	    	subject6=qualitySurveySettings.getSurveyEmailSubjectId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailBodyId()!=null)    	    	{
            	    	body6=qualitySurveySettings.getSurveyEmailBodyId()+"";
            	    	}
            	    	if(qualitySurveySettings.getSurveyEmailSignatureId()!=null)    	    	{            	    	
            	    	signature6=qualitySurveySettings.getSurveyEmailSignatureId()+"";
            	    	} 
    				
    			}
    	    	i++;
    		}
    	}		
    	return SUCCESS;
    }
    private String surveyId;	
    private String surveyName;
    private String surveyUrl;
    private String jobId;
    private String transferre;
    private String recommend;
    private String destination;
    private String origin;
    private String overall;
    private String premove;
    private String nps;
    
    private String jobIdType;
    private String transferreType;
    private String recommendType;
    private String destinationType;
    private String originType;
    private String overallType;
    private String premoveType;
    private String npsType;
    
    private String jobIdSize;
    private String transferreSize;
    private String recommendSize;
    private String destinationSize;
    private String originSize;
    private String overallSize;
    private String premoveSize;
    private String npsSize;
    
    private String coordinator;
    private String coordinatorType;
    private String coordinatorSize;
    
    
    private String refmasterUrlId;
    private String refmasterJobId;
    private String refmasterTransferreId;
    private String refmasterRecommendId;
    private String refmasterDestinationId;
    private String refmasterCoordinatorId;
    private String refmasterOriginId;
    private String refmasterOverallId;
    private String refmasterPremoveId;
    private String refmasterNpsId;
    private String type; 
    public String emailConfigrationPopop()
    {
    	RefMaster refmasterTemp=null;
    	if(type!=null && type.equalsIgnoreCase("edit")){
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterUrlId));
    		surveyId=refmasterTemp.getCode();
    		surveyName=refmasterTemp.getflex1();
    		surveyUrl=refmasterTemp.getDescription();
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterJobId));
    		jobId=refmasterTemp.getflex4();
    		jobIdType=refmasterTemp.getflex2();
    		jobIdSize=refmasterTemp.getflex3();
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterTransferreId));
    		transferre=refmasterTemp.getflex4();
    		transferreType=refmasterTemp.getflex2();
    		transferreSize=refmasterTemp.getflex3();    		
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterRecommendId));
    		recommend=refmasterTemp.getflex4();
    		recommendType=refmasterTemp.getflex2();
    		recommendSize=refmasterTemp.getflex3();    		
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterDestinationId));
    		destination=refmasterTemp.getflex4();
    		destinationType=refmasterTemp.getflex2();
    		destinationSize=refmasterTemp.getflex3();
    		}catch(Exception e){}
    		try{
        		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterCoordinatorId));
        		coordinator=refmasterTemp.getflex4();
        		coordinatorType=refmasterTemp.getflex2();
        		coordinatorSize=refmasterTemp.getflex3();
        		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterOriginId));
    		origin=refmasterTemp.getflex4();
    		originType=refmasterTemp.getflex2();
    		originSize=refmasterTemp.getflex3();
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterOverallId));
    		overall=refmasterTemp.getflex4();
    		overallType=refmasterTemp.getflex2();
    		overallSize=refmasterTemp.getflex3();
    		}catch(Exception e){}
    		try{
    		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterPremoveId));
    		premove=refmasterTemp.getflex4();
    		premoveType=refmasterTemp.getflex2();
    		premoveSize=refmasterTemp.getflex3();
    		}catch(Exception e){}
    		try{
        		refmasterTemp=refMasterManager.get(Long.parseLong(refmasterNpsId));
        		nps=refmasterTemp.getflex4();
        		npsType=refmasterTemp.getflex2();
        		npsSize=refmasterTemp.getflex3();
        		}catch(Exception e){}
    	}
    	return SUCCESS;
    }
    String hitFlag="0";
    public String saveEmailPopup(){
    	if(configType.equalsIgnoreCase("surveyfrom")){
    		RefMaster refmaster=null;
            if((refmasterUrlId!=null)&&(!refmasterUrlId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterUrlId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription(surveyUrl);
        		refmaster.setFieldLength(200);
        		refmaster.setParameter("QUALITYSURVEYURL");
        		refmaster.setLanguage("en");
        		refmaster.setflex1(surveyName);
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription(surveyUrl);
        		refmaster.setFieldLength(200);
        		refmaster.setParameter("QUALITYSURVEYURL");
        		refmaster.setLanguage("en");
        		refmaster.setflex1(surveyName);
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
            }

            if((refmasterJobId!=null)&&(!refmasterJobId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterJobId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("JOBID");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(jobId);        		
        		refmaster.setflex2(jobIdType);
        		refmaster.setflex3(jobIdSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);

            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("JOBID");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(jobId);
        		refmaster.setflex2(jobIdType);
        		refmaster.setflex3(jobIdSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);            	
            }
    		
            if((refmasterTransferreId!=null)&&(!refmasterTransferreId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterTransferreId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("TRANSFERRE");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(transferre);
        		refmaster.setflex2(transferreType);
        		refmaster.setflex3(transferreSize);  
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
        		
/*        			if((transferreSize!=null)&&(!transferreSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        		     if((transferreType!=null)&&((transferreType.trim().equalsIgnoreCase("RADIO"))||(transferreType.trim().equalsIgnoreCase("DROPDOWN")))){
        				
        				

        				Integer t=Integer.parseInt(transferreSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(transferreType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(transferre);
        		        		refmaster.setDescription("TRANSFERRE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");  
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(transferre.split(",")[k]);
        		        		refmaster.setDescription("TRANSFERRE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");    
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		}*/
            	
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("TRANSFERRE");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(transferre);
        		refmaster.setflex2(transferreType);
        		refmaster.setflex3(transferreSize);
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
/*        		if((transferreType!=null)&&((transferreType.trim().equalsIgnoreCase("RADIO"))||(transferreType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((transferreSize!=null)&&(!transferreSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(transferreSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(transferreType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(transferre);
        		        		refmaster.setDescription("TRANSFERRE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(transferre.split(",")[k]);
        		        		refmaster.setDescription("TRANSFERRE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");     
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}
        		}*/
            }
    		
            if((refmasterRecommendId!=null)&&(!refmasterRecommendId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterRecommendId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("RECOMMEND");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(recommend);
        		refmaster.setflex2(recommendType);
        		refmaster.setflex3(recommendSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
        		
/*        			if((recommendSize!=null)&&(!recommendSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        				if((recommendType!=null)&&((recommendType.trim().equalsIgnoreCase("RADIO"))||(recommendType.trim().equalsIgnoreCase("DROPDOWN")))){	
        				
        				

        				Integer t=Integer.parseInt(recommendSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(recommendType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(recommend);
        		        		refmaster.setDescription("RECOMMEND");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");       
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(recommend.split(",")[k]);
        		        		refmaster.setDescription("RECOMMEND");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");     
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		}*/        		
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("RECOMMEND");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(recommend);
        		refmaster.setflex2(recommendType);
        		refmaster.setflex3(recommendSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);        		
/*        		if((recommendType!=null)&&((recommendType.trim().equalsIgnoreCase("RADIO"))||(recommendType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((recommendSize!=null)&&(!recommendSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(recommendSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(recommendType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(recommend);
        		        		refmaster.setDescription("RECOMMEND");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(recommend.split(",")[k]);
        		        		refmaster.setDescription("RECOMMEND");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");  
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}
        		}*/        		
            	
            }
    		
            if((refmasterDestinationId!=null)&&(!refmasterDestinationId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterDestinationId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("DESTINATION");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(destination);
        		refmaster.setflex2(destinationType);
        		refmaster.setflex3(destinationSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
        	
/*        			if((destinationSize!=null)&&(!destinationSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        				
        				if((destinationType!=null)&&((destinationType.trim().equalsIgnoreCase("RADIO"))||(destinationType.trim().equalsIgnoreCase("DROPDOWN")))){

        				Integer t=Integer.parseInt(destinationSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(destinationType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(destination);
        		        		refmaster.setDescription("DESTINATION");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(destination.split(",")[k]);
        		        		refmaster.setDescription("DESTINATION");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");    
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		}*/        		
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("DESTINATION");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(destination);
        		refmaster.setflex2(destinationType);
        		refmaster.setflex3(destinationSize);   
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);        		
/*        		if((destinationType!=null)&&((destinationType.trim().equalsIgnoreCase("RADIO"))||(destinationType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((destinationSize!=null)&&(!destinationSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(destinationSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(destinationType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(destination);
        		        		refmaster.setDescription("DESTINATION");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");      
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(destination.split(",")[k]);
        		        		refmaster.setDescription("DESTINATION");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}
        		}*/              		
            	
            }
    		
            if((refmasterCoordinatorId!=null)&&(!refmasterCoordinatorId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterCoordinatorId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("COORDINATOR");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(coordinator);        		
        		refmaster.setflex2(coordinatorType);
        		refmaster.setflex3(coordinatorSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);

            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("COORDINATOR");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(coordinator);
        		refmaster.setflex2(coordinatorType);
        		refmaster.setflex3(coordinatorSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);            	
            }
            
            if((refmasterNpsId!=null)&&(!refmasterNpsId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterNpsId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("NPS");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(nps);        		
        		refmaster.setflex2(npsType);
        		refmaster.setflex3(npsSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);

            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("NPS");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(nps);
        		refmaster.setflex2(npsType);
        		refmaster.setflex3(npsSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);            	
            }
            
            if((refmasterOriginId!=null)&&(!refmasterOriginId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterOriginId)); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("ORIGIN");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(origin);
        		refmaster.setflex2(originType);
        		refmaster.setflex3(originSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
        		
/*        			if((originSize!=null)&&(!originSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        				if((originType!=null)&&((originType.trim().equalsIgnoreCase("RADIO"))||(originType.trim().equalsIgnoreCase("DROPDOWN")))){        				
        				Integer t=Integer.parseInt(originSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(originType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(origin);
        		        		refmaster.setDescription("ORIGIN");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(origin.split(",")[k]);
        		        		refmaster.setDescription("ORIGIN");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		}*/        		
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("ORIGIN");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(origin);
        		refmaster.setflex2(originType);
        		refmaster.setflex3(originSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
/*        		if((originType!=null)&&((originType.trim().equalsIgnoreCase("RADIO"))||(originType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((originSize!=null)&&(!originSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(originSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(originType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(origin);
        		        		refmaster.setDescription("ORIGIN");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");  
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(origin.split(",")[k]);
        		        		refmaster.setDescription("ORIGIN");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");    
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}            	
            }*/
            }
    		
            if((refmasterOverallId!=null)&&(!refmasterOverallId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterOverallId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("OVERALL");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(overall);
        		refmaster.setflex2(overallType);
        		refmaster.setflex3(overallSize);  
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
        		
/*        			if((overallSize!=null)&&(!overallSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        				
        				if((overallType!=null)&&((overallType.trim().equalsIgnoreCase("RADIO"))||(overallType.trim().equalsIgnoreCase("DROPDOWN")))){

        				Integer t=Integer.parseInt(overallSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(overallType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(overall);
        		        		refmaster.setDescription("OVERALL");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");   
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(overall.split(",")[k]);
        		        		refmaster.setDescription("OVERALL");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");  
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		}*/        		
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("OVERALL");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(overall);
        		refmaster.setflex2(overallType);
        		refmaster.setflex3(overallSize); 
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
/*        		if((overallType!=null)&&((overallType.trim().equalsIgnoreCase("RADIO"))||(overallType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((overallSize!=null)&&(!overallSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(overallSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(overallType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(overall);
        		        		refmaster.setDescription("OVERALL");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");    
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(overall.split(",")[k]);
        		        		refmaster.setDescription("OVERALL");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");   
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}            	
            }*/        		
            	
            }

            if((refmasterPremoveId!=null)&&(!refmasterPremoveId.equalsIgnoreCase(""))){
            	refmaster=refMasterManager.get(Long.parseLong(refmasterPremoveId));
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("PREMOVE");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		String ss=refmaster.getflex4();
        		refmaster.setflex4(premove);
        		refmaster.setflex2(premoveType);
        		refmaster.setflex3(premoveSize);  
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);        		
        		
/*        			if((premoveSize!=null)&&(!premoveSize.trim().equalsIgnoreCase(""))&&(ss!=null)&&(!ss.trim().equalsIgnoreCase(""))){
        				String str="";
        				String tempArr[]=ss.split(",");
        				for(String sk:tempArr){
        					if(str.equalsIgnoreCase("")){
        						str="'"+sk+"'";
        					}else{
        						str=str+",'"+sk+"'";
        					}
        				}
        				refMasterManager.deletedByParameterAndCodeList("ANSWERVALUE", str, sessionCorpID);
        				
        				if((premoveType!=null)&&((premoveType.trim().equalsIgnoreCase("RADIO"))||(premoveType.trim().equalsIgnoreCase("DROPDOWN")))){

        				Integer t=Integer.parseInt(premoveSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(premoveType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(premove);
        		        		refmaster.setDescription("PREMOVE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1"); 
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(premove.split(",")[k]);
        		        		refmaster.setDescription("PREMOVE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");    
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			        				
        			}
        		} */       		
            	
            }else{
        		refmaster=new RefMaster(); 
        		refmaster.setCorpID(sessionCorpID);
        		refmaster.setCreatedOn(new Date());
        		refmaster.setUpdatedOn(new Date());
        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		refmaster.setCode(surveyId);
        		refmaster.setDescription("PREMOVE");
        		refmaster.setFieldLength(20);
        		refmaster.setParameter("ANSWERIDPARAM");
        		refmaster.setLanguage("en");
        		refmaster.setflex4(premove);
        		refmaster.setflex2(premoveType);
        		refmaster.setflex3(premoveSize);   
        		refmaster.setStatus("Active");
        		refMasterManager.save(refmaster);
/*        		if((premoveType!=null)&&((premoveType.trim().equalsIgnoreCase("RADIO"))||(premoveType.trim().equalsIgnoreCase("DROPDOWN")))){
        			if((premoveSize!=null)&&(!premoveSize.trim().equalsIgnoreCase(""))){
        				Integer t=Integer.parseInt(premoveSize);
        				int r=1;
        				int d=t;
        				for(int k=0;k<t;k++){
        					if(premoveType.trim().equalsIgnoreCase("RADIO")){
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(premove);
        		        		refmaster.setDescription("PREMOVE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(r+"");
        		        		refmaster.setflex2(r+"0");
        		        		refmaster.setflex3(r+"1");   
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}else{
        		        		refmaster=new RefMaster(); 
        		        		refmaster.setCorpID(sessionCorpID);
        		        		refmaster.setCreatedOn(new Date());
        		        		refmaster.setUpdatedOn(new Date());
        		        		refmaster.setCreatedBy(getRequest().getRemoteUser());
        		        		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
        		        		refmaster.setCode(premove.split(",")[k]);
        		        		refmaster.setDescription("PREMOVE");
        		        		refmaster.setFieldLength(20);
        		        		refmaster.setParameter("ANSWERVALUE");
        		        		refmaster.setLanguage("en");
        		        		refmaster.setflex1(d+"");
        		        		refmaster.setflex2(d+"0");
        		        		refmaster.setflex3(d+"1");     
        		        		refmaster.setStatus("Active");
        		        		refMasterManager.save(refmaster);
        					}
        					r++;
        					d--;
        				}
        			}            	
            }*/              		
            	
            }
    		hitFlag="1";
    	}else if(configType.equalsIgnoreCase("recipient")){
            String surveyEmailName=getRequest().getParameter("surveyEmailName");
            String surveyEmailDesription=getRequest().getParameter("surveyEmailDesription");

    		RefMaster refmaster=new RefMaster(); 
    		refmaster.setCorpID(sessionCorpID);
    		refmaster.setCreatedOn(new Date());
    		refmaster.setUpdatedOn(new Date());
    		refmaster.setCreatedBy(getRequest().getRemoteUser());
    		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
    		refmaster.setCode(surveyEmailName);
    		refmaster.setDescription(surveyEmailDesription);
    		refmaster.setFieldLength(200);
    		refmaster.setParameter("QSURVEYRECIPIENT");
    		refmaster.setLanguage("en");
    		refmaster.setflex1(surveyEmailName);
    		refmaster.setStatus("Active");
    		refMasterManager.save(refmaster);
    		hitFlag="1";
    	}else if(configType.equalsIgnoreCase("subject")){
            String surveyEmailName=getRequest().getParameter("surveyEmailName");
            String surveyEmailDesription=getRequest().getParameter("surveyEmailDesription");

    		RefMaster refmaster=new RefMaster(); 
    		refmaster.setCorpID(sessionCorpID);
    		refmaster.setCreatedOn(new Date());
    		refmaster.setUpdatedOn(new Date());
    		refmaster.setCreatedBy(getRequest().getRemoteUser());
    		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
    		refmaster.setCode(surveyEmailName);
    		refmaster.setDescription(surveyEmailDesription);
    		refmaster.setFieldLength(200);
    		refmaster.setParameter("QSURVEYSUBJECT");
    		refmaster.setLanguage("en");
    		refmaster.setflex1(surveyEmailName);
    		refmaster.setStatus("Active");
    		refMasterManager.save(refmaster);
    		hitFlag="1";
    	}else if(configType.equalsIgnoreCase("body")){
            String surveyEmailName=getRequest().getParameter("surveyEmailName");
            String surveyEmailDesription=getRequest().getParameter("surveyEmailDesription");

    		RefMaster refmaster=new RefMaster(); 
    		refmaster.setCorpID(sessionCorpID);
    		refmaster.setCreatedOn(new Date());
    		refmaster.setUpdatedOn(new Date());
    		refmaster.setCreatedBy(getRequest().getRemoteUser());
    		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
    		refmaster.setCode(surveyEmailName);
    		refmaster.setDescription(surveyEmailDesription);
    		refmaster.setFieldLength(200);
    		refmaster.setParameter("QSURVEYBODY");
    		refmaster.setLanguage("en");
    		refmaster.setflex1(surveyEmailName);
    		refmaster.setStatus("Active");
    		refMasterManager.save(refmaster);
    		hitFlag="1";
    	}else if(configType.equalsIgnoreCase("signature")){
            String surveyEmailName=getRequest().getParameter("surveyEmailName");
            String surveyEmailDesription=getRequest().getParameter("surveyEmailDesription");

    		RefMaster refmaster=new RefMaster(); 
    		refmaster.setCorpID(sessionCorpID);
    		refmaster.setCreatedOn(new Date());
    		refmaster.setUpdatedOn(new Date());
    		refmaster.setCreatedBy(getRequest().getRemoteUser());
    		refmaster.setUpdatedBy(getRequest().getRemoteUser());      		
    		refmaster.setCode(surveyEmailName);
    		refmaster.setDescription(surveyEmailDesription);
    		refmaster.setFieldLength(200);
    		refmaster.setParameter("QSURVEYSIGNATURE");
    		refmaster.setLanguage("en");
    		refmaster.setflex1(surveyEmailName);
    		refmaster.setStatus("Active");
    		refMasterManager.save(refmaster);
    		hitFlag="1";
    	}else{
    	}
    	return SUCCESS;
    }
    private String accountId;
    private String accountName;
    private String accountCode;
    private String delayInterval;
    private String sendLimit;    

    private String recipient0;
    private String subject0;
    private String body0;
    private String signature0;   

    private String recipient1;
    private String subject1;
    private String body1;
    private String signature1;  

    private String recipient2;
    private String subject2;
    private String body2;
    private String signature2;      
    
    private String recipient3;
    private String subject3;
    private String body3;
    private String signature3;  

    private String recipient4;
    private String subject4;
    private String body4;
    private String signature4;  
    
    private String recipient5;
    private String subject5;
    private String body5;
    private String signature5;  
    
    private String recipient6;
    private String subject6;
    private String body6;
    private String signature6;  
    private String mode; 
    private String shipNumber;
    private SurveyEmailAudit surveyEmailAudit;
    private List qualityList;
    public String qualitySurveyConfig()
    {
    	qualityList=qualitySurveySettingsManager.getEmailConfigrationSoList(sessionCorpID,"","");
    	return SUCCESS;
    }
    public String searchQualitySurveyList()
    {
    	qualityList=qualitySurveySettingsManager.getEmailConfigrationSoList(sessionCorpID,accountCode,accountName);
    	return SUCCESS;
    }    
    public String save()
    {
		parameterRecipient=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYRECIPIENT");
		parameterBody=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYBODY");
		parameterSignature=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYSIGNATURE");
		parameterSubject=refMasterManager.findBySurveyQualityParameter(sessionCorpID, "QSURVEYSUBJECT");
    	if(delayInterval.equalsIgnoreCase(""))
    	{
    		delayInterval="0";
    	}
    	if(sendLimit.equalsIgnoreCase(""))
    	{
    		sendLimit="0";
    	}   
    	if(surveyFrom.equalsIgnoreCase(""))
    	{
    		surveyFrom="0";
    	}   
    	
    	qualitySurveySettingsList=qualitySurveySettingsManager.getQualitySurveySettingsDate(accountId,sessionCorpID,mode);
    	String accountCodeTemp=accountId+"~"+mode;
    	excludeFPU=qualitySurveySettingsManager.getExcludeFPU(accountCodeTemp,sessionCorpID);
    	if((qualitySurveySettingsList!=null)&&(!qualitySurveySettingsList.isEmpty()))
    	{
    		int i=0;
    		for (QualitySurveySettings qualitySurveySettings : qualitySurveySettingsList) {
    			qualitySurveySettings.setAccountId(accountId);
    	    	qualitySurveySettings.setDelayInterval(Integer.parseInt(delayInterval));
    	    	qualitySurveySettings.setSendLimit(Integer.parseInt(sendLimit)); 
    	    	qualitySurveySettings.setSurveyFrom(surveyFrom);
    	    	qualitySurveySettings.setMode(mode);
	    	    if(i==0)
	    	    {
	    	    		if(recipient0!=null && (!recipient0.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient0));
	        	    	}
	        	    	if(subject0!=null && (!subject0.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject0));
	        	    	}
	        	    	if(body0!=null && (!body0.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body0));
	        	    	}
	        	    	if(signature0!=null && (!signature0.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature0));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }else if(i==1){
    	    		if(recipient1!=null && (!recipient1.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient1));
	        	    	}
	        	    	if(subject1!=null && (!subject1.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject1));
	        	    	}
	        	    	if(body1!=null && (!body1.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body1));
	        	    	}
	        	    	if(signature1!=null && (!signature1.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature1));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    	
	    	    }else if(i==2){
    	    		if(recipient2!=null && (!recipient2.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient2));
	        	    	}
	        	    	if(subject2!=null && (!subject2.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject2));
	        	    	}
	        	    	if(body2!=null && (!body2.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body2));
	        	    	}
	        	    	if(signature2!=null && (!signature2.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature2));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }else if(i==3){
    	    		if(recipient3!=null && (!recipient3.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient3));
	        	    	}
	        	    	if(subject3!=null && (!subject3.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject3));
	        	    	}
	        	    	if(body3!=null && (!body3.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body3));
	        	    	}
	        	    	if(signature3!=null && (!signature3.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature3));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }else if(i==4){
    	    		if(recipient4!=null && (!recipient4.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient4));
	        	    	}
	        	    	if(subject4!=null && (!subject4.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject4));
	        	    	}
	        	    	if(body4!=null && (!body4.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body4));
	        	    	}
	        	    	if(signature4!=null && (!signature4.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature4));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }else if(i==5){
    	    		if(recipient5!=null && (!recipient5.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient5));
	        	    	}
	        	    	if(subject5!=null && (!subject5.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject5));
	        	    	}
	        	    	if(body5!=null && (!body5.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body5));
	        	    	}
	        	    	if(signature5!=null && (!signature5.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature5));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }else {
    	    		if(recipient6!=null && (!recipient6.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient6));
	        	    	}
	        	    	if(subject6!=null && (!subject6.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject6));
	        	    	}
	        	    	if(body6!=null && (!body6.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body6));
	        	    	}
	        	    	if(signature6!=null && (!signature6.equalsIgnoreCase("")))    	    	{
	        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature6));
	        	    	
	        	    	qualitySurveySettings.setTimeSent(i);
	        	    	}
	    	    }   
    	    	qualitySurveySettings.setCorpId(sessionCorpID);
    	    	qualitySurveySettings.setUpdatedOn(new Date());
    	    	qualitySurveySettings.setUpdatedBy(getRequest().getRemoteUser()); 
    	    	qualitySurveySettingsManager.save(qualitySurveySettings);
    	    	i++;
    		}
    	}else{
    		for(int j=0;j<7;j++)
    		{
    	    	qualitySurveySettings=new QualitySurveySettings();
    	    	qualitySurveySettings.setAccountId(accountId);
    	    	qualitySurveySettings.setDelayInterval(Integer.parseInt(delayInterval));
    	    	qualitySurveySettings.setSendLimit(Integer.parseInt(sendLimit));
    	    	qualitySurveySettings.setSurveyFrom(surveyFrom);
    	    	qualitySurveySettings.setMode(mode);
    	    	    if(j==0)
    	    	    {
		    	    		if(recipient0!=null && (!recipient0.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient0));
		        	    	}
		        	    	if(subject0!=null && (!subject0.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject0));
		        	    	}
		        	    	if(body0!=null && (!body0.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body0));
		        	    	}
		        	    	if(signature0!=null && (!signature0.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature0));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }else if(j==1){
	    	    		if(recipient1!=null && (!recipient1.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient1));
		        	    	}
		        	    	if(subject1!=null && (!subject1.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject1));
		        	    	}
		        	    	if(body1!=null && (!body1.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body1));
		        	    	}
		        	    	if(signature1!=null && (!signature1.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature1));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    	
    	    	    }else if(j==2){
	    	    		if(recipient2!=null && (!recipient2.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient2));
		        	    	}
		        	    	if(subject2!=null && (!subject2.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject2));
		        	    	}
		        	    	if(body2!=null && (!body2.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body2));
		        	    	}
		        	    	if(signature2!=null && (!signature2.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature2));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }else if(j==3){
	    	    		if(recipient3!=null && (!recipient3.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient3));
		        	    	}
		        	    	if(subject3!=null && (!subject3.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject3));
		        	    	}
		        	    	if(body3!=null && (!body3.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body3));
		        	    	}
		        	    	if(signature3!=null && (!signature3.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature3));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }else if(j==4){
	    	    		if(recipient4!=null && (!recipient4.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient4));
		        	    	}
		        	    	if(subject4!=null && (!subject4.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject4));
		        	    	}
		        	    	if(body4!=null && (!body4.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body4));
		        	    	}
		        	    	if(signature4!=null && (!signature4.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature4));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }else if(j==5){
	    	    		if(recipient5!=null && (!recipient5.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient5));
		        	    	}
		        	    	if(subject5!=null && (!subject5.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject5));
		        	    	}
		        	    	if(body5!=null && (!body5.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body5));
		        	    	}
		        	    	if(signature5!=null && (!signature5.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature5));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }else {
	    	    		if(recipient6!=null && (!recipient6.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailRecipientId(Long.parseLong(recipient6));
		        	    	}
		        	    	if(subject6!=null && (!subject6.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSubjectId(Long.parseLong(subject6));
		        	    	}
		        	    	if(body6!=null && (!body6.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailBodyId(Long.parseLong(body6));
		        	    	}
		        	    	if(signature6!=null && (!signature6.equalsIgnoreCase("")))    	    	{
		        	    	qualitySurveySettings.setSurveyEmailSignatureId(Long.parseLong(signature6));
		        	    	
		        	    	qualitySurveySettings.setTimeSent(j);
		        	    	}
    	    	    }   
    	    	    qualitySurveySettings.setCorpId(sessionCorpID);
        	    	qualitySurveySettings.setCreatedOn(new Date());
        	    	qualitySurveySettings.setUpdatedOn(new Date());
        	    	qualitySurveySettings.setCreatedBy(getRequest().getRemoteUser());
        	    	qualitySurveySettings.setUpdatedBy(getRequest().getRemoteUser()); 
    	    	    qualitySurveySettingsManager.save(qualitySurveySettings);
    		}
    	}

    	return SUCCESS;
    }
	public QualitySurveySettings getQualitySurveySettings() {
		return qualitySurveySettings;
	}
	public void setQualitySurveySettings(QualitySurveySettings qualitySurveySettings) {
		this.qualitySurveySettings = qualitySurveySettings;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public List getQualitySurveySettingsList() {
		return qualitySurveySettingsList;
	}
	public void setQualitySurveySettingsList(List qualitySurveySettingsList) {
		this.qualitySurveySettingsList = qualitySurveySettingsList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setQualitySurveySettingsManager(
			QualitySurveySettingsManager qualitySurveySettingsManager) {
		this.qualitySurveySettingsManager = qualitySurveySettingsManager;
	}
	public Map<String, String> getParameterRecipient() {
		return parameterRecipient;
	}
	public void setParameterRecipient(Map<String, String> parameterRecipient) {
		this.parameterRecipient = parameterRecipient;
	}
	public Map<String, String> getParameterBody() {
		return parameterBody;
	}
	public void setParameterBody(Map<String, String> parameterBody) {
		this.parameterBody = parameterBody;
	}
	public Map<String, String> getParameterSubject() {
		return parameterSubject;
	}
	public void setParameterSubject(Map<String, String> parameterSubject) {
		this.parameterSubject = parameterSubject;
	}
	public Map<String, String> getParameterSignature() {
		return parameterSignature;
	}
	public void setParameterSignature(Map<String, String> parameterSignature) {
		this.parameterSignature = parameterSignature;
	}
	public String getConfigType() {
		return configType;
	}
	public void setConfigType(String configType) {
		this.configType = configType;
	}

	public String getEmailCode() {
		return emailCode;
	}

	public void setEmailCode(String emailCode) {
		this.emailCode = emailCode;
	}

	public String getEmailDesription() {
		return emailDesription;
	}

	public void setEmailDesription(String emailDesription) {
		this.emailDesription = emailDesription;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Map<String, String> getSendLimits() {
		return sendLimits;
	}

	public void setSendLimits(Map<String, String> sendLimits) {
		this.sendLimits = sendLimits;
	}

	public Map<String, String> getDelayDays() {
		return delayDays;
	}

	public void setDelayDays(Map<String, String> delayDays) {
		this.delayDays = delayDays;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getDelayInterval() {
		return delayInterval;
	}
	public void setDelayInterval(String delayInterval) {
		this.delayInterval = delayInterval;
	}
	public String getSendLimit() {
		return sendLimit;
	}
	public void setSendLimit(String sendLimit) {
		this.sendLimit = sendLimit;
	}
	public String getRecipient0() {
		return recipient0;
	}
	public void setRecipient0(String recipient0) {
		this.recipient0 = recipient0;
	}
	public String getSubject0() {
		return subject0;
	}
	public void setSubject0(String subject0) {
		this.subject0 = subject0;
	}
	public String getBody0() {
		return body0;
	}
	public void setBody0(String body0) {
		this.body0 = body0;
	}
	public String getSignature0() {
		return signature0;
	}
	public void setSignature0(String signature0) {
		this.signature0 = signature0;
	}
	public String getRecipient1() {
		return recipient1;
	}
	public void setRecipient1(String recipient1) {
		this.recipient1 = recipient1;
	}
	public String getSubject1() {
		return subject1;
	}
	public void setSubject1(String subject1) {
		this.subject1 = subject1;
	}
	public String getBody1() {
		return body1;
	}
	public void setBody1(String body1) {
		this.body1 = body1;
	}
	public String getSignature1() {
		return signature1;
	}
	public void setSignature1(String signature1) {
		this.signature1 = signature1;
	}
	public String getRecipient2() {
		return recipient2;
	}
	public void setRecipient2(String recipient2) {
		this.recipient2 = recipient2;
	}
	public String getSubject2() {
		return subject2;
	}
	public void setSubject2(String subject2) {
		this.subject2 = subject2;
	}
	public String getBody2() {
		return body2;
	}
	public void setBody2(String body2) {
		this.body2 = body2;
	}
	public String getSignature2() {
		return signature2;
	}
	public void setSignature2(String signature2) {
		this.signature2 = signature2;
	}
	public String getRecipient3() {
		return recipient3;
	}
	public void setRecipient3(String recipient3) {
		this.recipient3 = recipient3;
	}
	public String getSubject3() {
		return subject3;
	}
	public void setSubject3(String subject3) {
		this.subject3 = subject3;
	}
	public String getBody3() {
		return body3;
	}
	public void setBody3(String body3) {
		this.body3 = body3;
	}
	public String getSignature3() {
		return signature3;
	}
	public void setSignature3(String signature3) {
		this.signature3 = signature3;
	}
	public String getRecipient4() {
		return recipient4;
	}
	public void setRecipient4(String recipient4) {
		this.recipient4 = recipient4;
	}
	public String getSubject4() {
		return subject4;
	}
	public void setSubject4(String subject4) {
		this.subject4 = subject4;
	}
	public String getBody4() {
		return body4;
	}
	public void setBody4(String body4) {
		this.body4 = body4;
	}
	public String getSignature4() {
		return signature4;
	}
	public void setSignature4(String signature4) {
		this.signature4 = signature4;
	}
	public String getRecipient5() {
		return recipient5;
	}
	public void setRecipient5(String recipient5) {
		this.recipient5 = recipient5;
	}
	public String getSubject5() {
		return subject5;
	}
	public void setSubject5(String subject5) {
		this.subject5 = subject5;
	}
	public String getBody5() {
		return body5;
	}
	public void setBody5(String body5) {
		this.body5 = body5;
	}
	public String getSignature5() {
		return signature5;
	}
	public void setSignature5(String signature5) {
		this.signature5 = signature5;
	}
	public String getRecipient6() {
		return recipient6;
	}
	public void setRecipient6(String recipient6) {
		this.recipient6 = recipient6;
	}
	public String getSubject6() {
		return subject6;
	}
	public void setSubject6(String subject6) {
		this.subject6 = subject6;
	}
	public String getBody6() {
		return body6;
	}
	public void setBody6(String body6) {
		this.body6 = body6;
	}
	public String getSignature6() {
		return signature6;
	}
	public void setSignature6(String signature6) {
		this.signature6 = signature6;
	}
	public SurveyEmailAudit getSurveyEmailAudit() {
		return surveyEmailAudit;
	}
	public void setSurveyEmailAudit(SurveyEmailAudit surveyEmailAudit) {
		this.surveyEmailAudit = surveyEmailAudit;
	}
	public List getQualityList() {
		return qualityList;
	}
	public void setQualityList(List qualityList) {
		this.qualityList = qualityList;
	}
	public String getAccountCode() {
		return accountCode;
	}
	public void setAccountCode(String accountCode) {
		this.accountCode = accountCode;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	public Map<String, String> getSurveyFromList() {
		return surveyFromList;
	}
	public void setSurveyFromList(Map<String, String> surveyFromList) {
		this.surveyFromList = surveyFromList;
	}
	public String getSurveyFrom() {
		return surveyFrom;
	}
	public void setSurveyFrom(String surveyFrom) {
		this.surveyFrom = surveyFrom;
	}
	public void setAccountContactManager(AccountContactManager accountContactManager) {
		this.accountContactManager = accountContactManager;
	}
	public Boolean getExcludeFPU() {
		return excludeFPU;
	}
	public void setExcludeFPU(Boolean excludeFPU) {
		this.excludeFPU = excludeFPU;
	}
	public Map<String, String> getSurveyIdMap() {
		return surveyIdMap;
	}
	public void setSurveyIdMap(Map<String, String> surveyIdMap) {
		this.surveyIdMap = surveyIdMap;
	}
	public Map<String, String> getAnswerIdMap() {
		return answerIdMap;
	}
	public void setAnswerIdMap(Map<String, String> answerIdMap) {
		this.answerIdMap = answerIdMap;
	}
	public String getSurveyId() {
		return surveyId;
	}
	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}
	public String getSurveyName() {
		return surveyName;
	}
	public void setSurveyName(String surveyName) {
		this.surveyName = surveyName;
	}
	public String getSurveyUrl() {
		return surveyUrl;
	}
	public void setSurveyUrl(String surveyUrl) {
		this.surveyUrl = surveyUrl;
	}
	public String getJobId() {
		return jobId;
	}
	public void setJobId(String jobId) {
		this.jobId = jobId;
	}
	public String getTransferre() {
		return transferre;
	}
	public void setTransferre(String transferre) {
		this.transferre = transferre;
	}
	public String getRecommend() {
		return recommend;
	}
	public void setRecommend(String recommend) {
		this.recommend = recommend;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public String getOverall() {
		return overall;
	}
	public void setOverall(String overall) {
		this.overall = overall;
	}
	public String getPremove() {
		return premove;
	}
	public void setPremove(String premove) {
		this.premove = premove;
	}
	public String getRefmasterUrlId() {
		return refmasterUrlId;
	}
	public void setRefmasterUrlId(String refmasterUrlId) {
		this.refmasterUrlId = refmasterUrlId;
	}
	public String getRefmasterJobId() {
		return refmasterJobId;
	}
	public void setRefmasterJobId(String refmasterJobId) {
		this.refmasterJobId = refmasterJobId;
	}
	public String getRefmasterTransferreId() {
		return refmasterTransferreId;
	}
	public void setRefmasterTransferreId(String refmasterTransferreId) {
		this.refmasterTransferreId = refmasterTransferreId;
	}
	public String getRefmasterRecommendId() {
		return refmasterRecommendId;
	}
	public void setRefmasterRecommendId(String refmasterRecommendId) {
		this.refmasterRecommendId = refmasterRecommendId;
	}
	public String getRefmasterDestinationId() {
		return refmasterDestinationId;
	}
	public void setRefmasterDestinationId(String refmasterDestinationId) {
		this.refmasterDestinationId = refmasterDestinationId;
	}
	public String getRefmasterOriginId() {
		return refmasterOriginId;
	}
	public void setRefmasterOriginId(String refmasterOriginId) {
		this.refmasterOriginId = refmasterOriginId;
	}
	public String getRefmasterOverallId() {
		return refmasterOverallId;
	}
	public void setRefmasterOverallId(String refmasterOverallId) {
		this.refmasterOverallId = refmasterOverallId;
	}
	public String getRefmasterPremoveId() {
		return refmasterPremoveId;
	}
	public void setRefmasterPremoveId(String refmasterPremoveId) {
		this.refmasterPremoveId = refmasterPremoveId;
	}
	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getJobIdType() {
		return jobIdType;
	}
	public void setJobIdType(String jobIdType) {
		this.jobIdType = jobIdType;
	}
	public String getTransferreType() {
		return transferreType;
	}
	public void setTransferreType(String transferreType) {
		this.transferreType = transferreType;
	}
	public String getRecommendType() {
		return recommendType;
	}
	public void setRecommendType(String recommendType) {
		this.recommendType = recommendType;
	}
	public String getDestinationType() {
		return destinationType;
	}
	public void setDestinationType(String destinationType) {
		this.destinationType = destinationType;
	}
	public String getOriginType() {
		return originType;
	}
	public void setOriginType(String originType) {
		this.originType = originType;
	}
	public String getOverallType() {
		return overallType;
	}
	public void setOverallType(String overallType) {
		this.overallType = overallType;
	}
	public String getPremoveType() {
		return premoveType;
	}
	public void setPremoveType(String premoveType) {
		this.premoveType = premoveType;
	}
	public String getJobIdSize() {
		return jobIdSize;
	}
	public void setJobIdSize(String jobIdSize) {
		this.jobIdSize = jobIdSize;
	}
	public String getTransferreSize() {
		return transferreSize;
	}
	public void setTransferreSize(String transferreSize) {
		this.transferreSize = transferreSize;
	}
	public String getRecommendSize() {
		return recommendSize;
	}
	public void setRecommendSize(String recommendSize) {
		this.recommendSize = recommendSize;
	}
	public String getDestinationSize() {
		return destinationSize;
	}
	public void setDestinationSize(String destinationSize) {
		this.destinationSize = destinationSize;
	}
	public String getOriginSize() {
		return originSize;
	}
	public void setOriginSize(String originSize) {
		this.originSize = originSize;
	}
	public String getOverallSize() {
		return overallSize;
	}
	public void setOverallSize(String overallSize) {
		this.overallSize = overallSize;
	}
	public String getPremoveSize() {
		return premoveSize;
	}
	public void setPremoveSize(String premoveSize) {
		this.premoveSize = premoveSize;
	}
	public Map<String, String> getJobList() {
		return jobList;
	}
	public void setJobList(Map<String, String> jobList) {
		this.jobList = jobList;
	}
	public String getRefmasterCoordinatorId() {
		return refmasterCoordinatorId;
	}
	public void setRefmasterCoordinatorId(String refmasterCoordinatorId) {
		this.refmasterCoordinatorId = refmasterCoordinatorId;
	}
	public String getCoordinator() {
		return coordinator;
	}
	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}
	public String getCoordinatorType() {
		return coordinatorType;
	}
	public void setCoordinatorType(String coordinatorType) {
		this.coordinatorType = coordinatorType;
	}
	public String getCoordinatorSize() {
		return coordinatorSize;
	}
	public void setCoordinatorSize(String coordinatorSize) {
		this.coordinatorSize = coordinatorSize;
	}
	public String getNps() {
		return nps;
	}
	public void setNps(String nps) {
		this.nps = nps;
	}
	public String getNpsType() {
		return npsType;
	}
	public void setNpsType(String npsType) {
		this.npsType = npsType;
	}
	public String getNpsSize() {
		return npsSize;
	}
	public void setNpsSize(String npsSize) {
		this.npsSize = npsSize;
	}
	public String getRefmasterNpsId() {
		return refmasterNpsId;
	}
	public void setRefmasterNpsId(String refmasterNpsId) {
		this.refmasterNpsId = refmasterNpsId;
	}


}
