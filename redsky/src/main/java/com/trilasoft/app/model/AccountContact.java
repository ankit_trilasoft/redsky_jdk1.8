package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "accountcontact")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class AccountContact extends BaseObject {

	private Long id;

	private String corpID;

	private Date createdOn;

	private String createdBy;

	private Date updatedOn;

	private String updatedBy;

	private String jobType;

	private String jobTypeOwner;

	private String contractType;

	private String contactName;

	private String department;

	private String primaryPhone;

	private String primaryPhoneType;

	private String secondaryPhone;

	private String secondaryPhoneType;

	private String thirdPhone;

	private String thirdPhoneType;

	private String contactEmail;

	private String partnerCode;

	private String contactFrequency;

	private String title;

	private Date dateOfBirth;

	private Boolean doNotCall;

	private String salutation;

	private String contactLastName;

	private String leadSource;

	private String reportTo;

	private String assistant;

	private String assistantPhone;

	private String email2;

	private String emailType;

	private Boolean optedOut1;

	private Boolean invalid1;

	private Boolean optedOut2;

	private Boolean invalid2;

	private String address;

	private String country;

	private String state;

	private String city;

	private String zip;
	private Date visitedOn;
	
	private String leadSourceOther;
	
	private String userName;
	private String basedAt;
    private String basedAtName;
	
	private Boolean status;

	//private Partner partner;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpID", corpID).append("createdOn", createdOn)
				.append("createdBy", createdBy).append("updatedOn", updatedOn)
				.append("updatedBy", updatedBy).append("jobType", jobType)
				.append("jobTypeOwner", jobTypeOwner)
				.append("contractType", contractType)
				.append("contactName", contactName)
				.append("department", department)
				.append("primaryPhone", primaryPhone)
				.append("primaryPhoneType", primaryPhoneType)
				.append("secondaryPhone", secondaryPhone)
				.append("secondaryPhoneType", secondaryPhoneType)
				.append("thirdPhone", thirdPhone)
				.append("thirdPhoneType", thirdPhoneType)
				.append("contactEmail", contactEmail)
				.append("partnerCode", partnerCode)
				.append("contactFrequency", contactFrequency)
				.append("title", title).append("dateOfBirth", dateOfBirth)
				.append("doNotCall", doNotCall)
				.append("salutation", salutation)
				.append("contactLastName", contactLastName)
				.append("leadSource", leadSource).append("reportTo", reportTo)
				.append("assistant", assistant)
				.append("assistantPhone", assistantPhone)
				.append("email2", email2).append("emailType", emailType)
				.append("optedOut1", optedOut1).append("invalid1", invalid1)
				.append("optedOut2", optedOut2).append("invalid2", invalid2)
				.append("address", address).append("country", country)
				.append("state", state).append("city", city).append("zip", zip)
				.append("visitedOn", visitedOn)
				.append("leadSourceOther", leadSourceOther)
				.append("userName", userName).append("basedAt", basedAt)
				.append("basedAtName", basedAtName)
				.append("status",status).toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof AccountContact))
			return false;
		AccountContact castOther = (AccountContact) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpID, castOther.corpID)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy)
				.append(jobType, castOther.jobType)
				.append(jobTypeOwner, castOther.jobTypeOwner)
				.append(contractType, castOther.contractType)
				.append(contactName, castOther.contactName)
				.append(department, castOther.department)
				.append(primaryPhone, castOther.primaryPhone)
				.append(primaryPhoneType, castOther.primaryPhoneType)
				.append(secondaryPhone, castOther.secondaryPhone)
				.append(secondaryPhoneType, castOther.secondaryPhoneType)
				.append(thirdPhone, castOther.thirdPhone)
				.append(thirdPhoneType, castOther.thirdPhoneType)
				.append(contactEmail, castOther.contactEmail)
				.append(partnerCode, castOther.partnerCode)
				.append(contactFrequency, castOther.contactFrequency)
				.append(title, castOther.title)
				.append(dateOfBirth, castOther.dateOfBirth)
				.append(doNotCall, castOther.doNotCall)
				.append(salutation, castOther.salutation)
				.append(contactLastName, castOther.contactLastName)
				.append(leadSource, castOther.leadSource)
				.append(reportTo, castOther.reportTo)
				.append(assistant, castOther.assistant)
				.append(assistantPhone, castOther.assistantPhone)
				.append(email2, castOther.email2)
				.append(emailType, castOther.emailType)
				.append(optedOut1, castOther.optedOut1)
				.append(invalid1, castOther.invalid1)
				.append(optedOut2, castOther.optedOut2)
				.append(invalid2, castOther.invalid2)
				.append(address, castOther.address)
				.append(country, castOther.country)
				.append(state, castOther.state).append(city, castOther.city)
				.append(zip, castOther.zip)
				.append(visitedOn, castOther.visitedOn)
				.append(leadSourceOther, castOther.leadSourceOther)
				.append(userName, castOther.userName)
				.append(basedAt, castOther.basedAt)
				.append(basedAtName, castOther.basedAtName)
				.append(status, castOther.status)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).append(jobType).append(jobTypeOwner)
				.append(contractType).append(contactName).append(department)
				.append(primaryPhone).append(primaryPhoneType)
				.append(secondaryPhone).append(secondaryPhoneType)
				.append(thirdPhone).append(thirdPhoneType).append(contactEmail)
				.append(partnerCode).append(contactFrequency).append(title)
				.append(dateOfBirth).append(doNotCall).append(salutation)
				.append(contactLastName).append(leadSource).append(reportTo)
				.append(assistant).append(assistantPhone).append(email2)
				.append(emailType).append(optedOut1).append(invalid1)
				.append(optedOut2).append(invalid2).append(address)
				.append(country).append(state).append(city).append(zip)
				.append(visitedOn).append(leadSourceOther).append(userName)
				.append(basedAt).append(basedAtName).append(status).toHashCode();
	}

	/*@ManyToOne
	 @JoinColumn(name="partnerCode", nullable=false, updatable=false,
	 referencedColumnName="partnerCode")
	 public Partner getPartner() {
	 return partner;
	 }

	 public void setPartner(Partner partner) {
	 this.partner = partner;
	 }*/
	@Column(length = 65)
	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	@Column(length = 50)
	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	@Column(length = 20)
	public String getContractType() {
		return contractType;
	}

	public void setContractType(String contractType) {
		this.contractType = contractType;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 20)
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 20)
	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	@Column(length = 20)
	public String getJobTypeOwner() {
		return jobTypeOwner;
	}

	public void setJobTypeOwner(String jobTypeOwner) {
		this.jobTypeOwner = jobTypeOwner;
	}

	@Column(length = 25)
	public String getPrimaryPhone() {
		return primaryPhone;
	}

	public void setPrimaryPhone(String primaryPhone) {
		this.primaryPhone = primaryPhone;
	}

	@Column(length = 25)
	public String getPrimaryPhoneType() {
		return primaryPhoneType;
	}

	public void setPrimaryPhoneType(String primaryPhoneType) {
		this.primaryPhoneType = primaryPhoneType;
	}

	@Column(length = 15)
	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}

	@Column(length = 25)
	public String getSecondaryPhone() {
		return secondaryPhone;
	}

	public void setSecondaryPhone(String secondaryPhone) {
		this.secondaryPhone = secondaryPhone;
	}

	@Column(length = 25)
	public String getSecondaryPhoneType() {
		return secondaryPhoneType;
	}

	public void setSecondaryPhoneType(String secondaryPhoneType) {
		this.secondaryPhoneType = secondaryPhoneType;
	}

	@Column(length = 25)
	public String getThirdPhone() {
		return thirdPhone;
	}

	public void setThirdPhone(String thirdPhone) {
		this.thirdPhone = thirdPhone;
	}

	@Column(length = 25)
	public String getThirdPhoneType() {
		return thirdPhoneType;
	}

	public void setThirdPhoneType(String thirdPhoneType) {
		this.thirdPhoneType = thirdPhoneType;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	@Column(length = 25)
	public String getContactFrequency() {
		return contactFrequency;
	}

	public void setContactFrequency(String contactFrequency) {
		this.contactFrequency = contactFrequency;
	}

	@Column(length = 25)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@Column
	public Boolean getDoNotCall() {
		return doNotCall;
	}

	public void setDoNotCall(Boolean doNotCall) {
		this.doNotCall = doNotCall;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAssistant() {
		return assistant;
	}

	public void setAssistant(String assistant) {
		this.assistant = assistant;
	}

	public String getAssistantPhone() {
		return assistantPhone;
	}

	public void setAssistantPhone(String assistantPhone) {
		this.assistantPhone = assistantPhone;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getEmailType() {
		return emailType;
	}

	public void setEmailType(String emailType) {
		this.emailType = emailType;
	}

	public Boolean getInvalid1() {
		return invalid1;
	}

	public void setInvalid1(Boolean invalid1) {
		this.invalid1 = invalid1;
	}

	public Boolean getInvalid2() {
		return invalid2;
	}

	public void setInvalid2(Boolean invalid2) {
		this.invalid2 = invalid2;
	}

	public String getLeadSource() {
		return leadSource;
	}

	public void setLeadSource(String leadSource) {
		this.leadSource = leadSource;
	}

	public Boolean getOptedOut1() {
		return optedOut1;
	}

	public void setOptedOut1(Boolean optedOut1) {
		this.optedOut1 = optedOut1;
	}

	public Boolean getOptedOut2() {
		return optedOut2;
	}

	public void setOptedOut2(Boolean optedOut2) {
		this.optedOut2 = optedOut2;
	}

	public String getReportTo() {
		return reportTo;
	}

	public void setReportTo(String reportTo) {
		this.reportTo = reportTo;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getContactLastName() {
		return contactLastName;
	}

	public void setContactLastName(String contactLastName) {
		this.contactLastName = contactLastName;
	}
	@Column
	public Date getVisitedOn() {
		return visitedOn;
	}

	public void setVisitedOn(Date visitedOn) {
		this.visitedOn = visitedOn;
	}

	public String getLeadSourceOther() {
		return leadSourceOther;
	}

	public void setLeadSourceOther(String leadSourceOther) {
		this.leadSourceOther = leadSourceOther;
	}
	@Column
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column
	public String getBasedAt() {
		return basedAt;
	}

	public void setBasedAt(String basedAt) {
		this.basedAt = basedAt;
	}
	@Column
	public String getBasedAtName() {
		return basedAtName;
	}

	public void setBasedAtName(String basedAtName) {
		this.basedAtName = basedAtName;
	}
	@Column
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

}
