


package com.trilasoft.app.model; 
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id; 
import javax.persistence.Table; 
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "passwordOTP")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")


public class PasswordOTP extends BaseObject { 

	private Long id;
	private String otpCode;
	private String corpID;
	private String createdBy; 
	private Date createdOn;
	private String updatedBy; 
	private Date updatedOn;  
    private Date expiryDate; 
	private String status; 
	private String verificationCodeReason; 
	private String verifiedOTPCode; 
	private Long userId;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id",id)
				.append("otpCode",otpCode)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("createdOn", createdOn).append("updatedBy", updatedBy)
				.append("updatedOn", updatedOn)
				.append("expiryDate", expiryDate)
				.append("status",
						status)
				.append("verificationCodeReason", verificationCodeReason)
				.append("verifiedOTPCode", verifiedOTPCode)
				.append("userId", userId)
				.toString();
						
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PasswordOTP))
			return false;
		PasswordOTP castOther = (PasswordOTP) other;
		return new EqualsBuilder().append(id,castOther.id)
				.append(otpCode, castOther.otpCode)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(createdOn,castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn,castOther.updatedOn)
				.append(expiryDate,castOther.expiryDate)
				.append(status,castOther.status)
				.append(verificationCodeReason,castOther.verificationCodeReason)
				.append(verifiedOTPCode,castOther.verifiedOTPCode)
				.append(userId,castOther.userId).isEquals();

	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder()
		        .append(id)
				.append(otpCode)
				.append(corpID)
				.append(createdBy)
				.append(createdOn)
				.append(updatedBy)
				.append(updatedOn)
				.append(expiryDate)
				.append(status)
				.append(verificationCodeReason)
				.append(verifiedOTPCode)
				.append(userId).toHashCode();
	} 
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() { 
		return id;
	} 
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Column
	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	@Column
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	

	
	@Column
	public String getVerificationCodeReason() {
		return verificationCodeReason;
	}

	public void setVerificationCodeReason(String verificationCodeReason) {
		this.verificationCodeReason = verificationCodeReason;
	}
	@Column
	public String getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(String otpCode) {
		this.otpCode = otpCode;
	}
	@Column
	public String getVerifiedOTPCode() {
		return verifiedOTPCode;
	}

	public void setVerifiedOTPCode(String verifiedOTPCode) {
		this.verifiedOTPCode = verifiedOTPCode;
	}

	
}
