<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<c:set var="FormDateValue2" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat2" name="dateFormat2" value="dd-NNN-yy"/>
    
<style type="text/css"> 
    /* Desktops and laptops  */
@media only screen
and (min-width : 1224px)
and (max-width : 1366px) {
/* Styles */
#container {width:95%}
}
</style>
<table class="detailTabLabel" border="0" width="100%">
<c:if test="${accountInterface!='Y'}"> 
 <c:if test="${not empty accountLine.payPostDate}">
 <s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.payPostDate" /></s:text>
    <s:hidden  name="accountLine.payPostDate" value="%{accountLineFormattedValue}" /> 
 </c:if>
<c:if test="${empty accountLine.payPostDate}">
 <s:hidden   name="accountLine.payPostDate"/> 
</c:if>
</c:if> 
<c:if test="${accountInterface!='Y'}">
<c:if test="${not empty accountLine.payAccDate}">
<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.payAccDate" /></s:text>
<s:hidden  name="accountLine.payAccDate" value="%{accountLineFormattedValue}" /> 
</c:if>
<c:if test="${empty accountLine.payAccDate}">
<s:hidden   name="accountLine.payAccDate"/> 
</c:if>
</c:if>
<sec-auth:authComponent componentId="module.accountLine.section.payableDetail.edit" >
	<%;
	String sectionPayableDetail="true";
	int permissionPayable  = (Integer)request.getAttribute("module.accountLine.section.payableDetail.edit" + "Permission");
 	if (permissionPayable > 2 ){
 		sectionPayableDetail = "false";
 	}
  %>
															
  					<div class="subcontenttabChild" style="height:20px;padding:0 3px 4px;" background="images/greyband.gif" style="width:99%"><fmt:message key="accountLine.PaybleDetail" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
																<input disabled="<%=sectionPayableDetail%>" type="button" value="Compute" style="width:65px" name="accountLine.compute5" onclick="findRevisedQuantityLocalAmounts();"></div>
															 <c:if test="${contractType}"> 
															<tr>
															<td align="right" class="listwhitetext" width="12%">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
															<c:if test="${empty accountLine.payAccDate}">
															 <td align="left" colspan="1" >
															 
															 <configByCorp:customDropDown 	  listType="map" list="${country}"   fieldValue="${accountLine.payableContractCurrency}"
																     attribute="id=payableContractCurrency class=list-menu name=accountLine.payableContractCurrency style=width:55px  headerKey='' headerValue=''  tabindex='18' onchange=changeStatus();findPayableContractCurrencyExchangeRate(); tabindex='12'"/>	</td>
															
															 </c:if>
															 <c:if test="${not empty accountLine.payAccDate}">
															 <td align="left" colspan="1" >
															 
															 <configByCorp:customDropDown 	  listType="map" list="${country}"   fieldValue="${accountLine.payableContractCurrency}"
																     attribute="id=payableContractCurrency class=list-menu name=accountLine.payableContractCurrency style=width:55px  headerKey='' headerValue=''  tabindex='18' onclick='checkPayAccPayableContractCurrency()' onfocus='checkPayAccPayableContractCurrency()' tabindex='12'"/>	</td>
															
															 
															 </c:if>
															 <td align="right"   class="listwhitetext">Value&nbsp;Date</td>
															 <c:if test="${not empty accountLine.payableContractValueDate}"> 
					                                          <s:text id="accountLineFormattedPayableContractValueDate" name="${FormDateValue2}"><s:param name="value" value="accountLine.payableContractValueDate"/></s:text>
				                                              <td width="97"><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" value="%{accountLineFormattedPayableContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="7"/>
				                                              <c:if test="${empty accountLine.payAccDate}">
				                                              <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
				                                              </c:if> 
                                                             <c:if test="${not empty accountLine.payAccDate}">
                                                             <img id="payableContractValueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')"/>
                                                             </c:if>
				                                              </td>
				                                             </c:if>
				                                             <c:if test="${empty accountLine.payableContractValueDate}">
					                                          <td width="97"><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
					                                          <c:if test="${empty accountLine.payAccDate}">
					                                          <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					                                          </c:if>
					                                          <c:if test="${not empty accountLine.payAccDate}">
					                                          <img id="payableContractValueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')"/>
					                                          </c:if>
					                                          </td>
				                                             </c:if>
															
															
															 				                                           
				                                             <td  align="right" class="listwhitetext" width="">Contract&nbsp;Exchange&nbsp;Rate</td>
															 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.payableContractExchangeRate" size="6" maxlength="10"   onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateActualExpenseByContractRate('none');" onblur="chkSelect('1');"/></td>   			
				                                              
				                                             <td align="right" class="listwhitetext" width="">&nbsp;Contract&nbsp;Amount</td>
														     <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.payableContractRateAmmount" size="8" maxlength="12"  onkeydown="return onlyRateAllowed(event)" onchange="calculateActualExpenseByContractRate('accountLine.payableContractRateAmmount');" onblur="chkSelect('1');"/></td>	 
															 
															<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>
															<td align="left"></td>
															<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>
															<td width="35px"></td>
															<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>  
															</tr>
															<tr>
															<td colspan="8" class="vertlinedata"></td>
															</tr> 
															</c:if> 
															
															<tr>
																<td width="105" align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceNumber" /></td>
																<td align="left" class="listwhitetext"><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" key="accountLine.invoiceNumber" onkeydown="return onlyforInvoice(event)"
																	size="16" maxlength="25" onchange="checkVendorInvoice('INV');assignInvoiceDate();" tabindex="38" /></td>
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.invoiceDate" /></td> 
																<c:if test="${not empty accountLine.invoiceDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.invoiceDate" /></s:text>
																	<c:if test="${empty accountLine.payAccDate}">
																	<td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown="return onlyDelForInvoiceDate(event,this)" readonly="true" />
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
																    <%} %>
																    </c:if>
																    <c:if test="${not empty accountLine.payAccDate}">
																    <td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" cssStyle="width:78px;" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown="" readonly="true"/>
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="invoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	</c:if>
																    </td>
																</c:if>
																<c:if test="${empty accountLine.invoiceDate}">
																	<td><s:textfield tabindex="" cssClass="input-text" id="invoiceDate" name="accountLine.invoiceDate" cssStyle="width:78px;" required="true" size="7" maxlength="12" onkeydown="return onlyDelForInvoiceDate(event,this)" readonly="true"  onfocus="cal.select(document.forms['accountLineForms'].invoiceDate,'calender6',document.forms['accountLineForms'].dateFormat2.value); return false;"/>
																		<c:if test="${empty accountLine.payAccDate}">
																		<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																		<img id="invoiceDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="checkAcctStatus(this.id);"/>
																	<%} %>
																	</c:if>
																	 <c:if test="${not empty accountLine.payAccDate}">
																	 <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="invoiceDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	 </c:if>
																	</td>
																</c:if> 
																<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.serviceTaxInput">Service&nbsp;Tax&nbsp;Input</configByCorp:fieldVisibility></td> 
      															<c:set var="serviceTaxInput" value="false" />
															    <c:if test="${accountLine.serviceTaxInput}">
																	<c:set var="serviceTaxInput" value="true" />
																</c:if>
																<td align="left" class="listwhitetext">
																<configByCorp:fieldVisibility componentId="accountLine.serviceTaxInput"><s:checkbox name="accountLine.serviceTaxInput" cssStyle="margin:0px;" value="${serviceTaxInput}" fieldValue="true" onchange="changeStatus();" /></configByCorp:fieldVisibility>
																</td>   															
																<td width="" align="right" class="listwhitetext"><fmt:message key="accountLine.exchangeRate" /></td>
																<td><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;" key="accountLine.exchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)" onchange="convertedAmountbyExchangeRate();" tabindex="39"/></td> 
																<c:if test="${!contractType}"> 
																<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>
																<td align="left"></td>
																<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>
																<td width="35px"></td>
																<td valign="middle" rowspan="9" class="vertlinedata_vert"></td>
																</c:if>
																<c:if test="${empty accountLine.id}">
																	<td width="77px" align="right">
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
																<%} %>
																</c:if>
																<c:if test="${not empty accountLine.id}">
																<c:choose>
																	<c:when test="${countPayableDetailNotes == '0' || countPayableDetailNotes == '' || countPayableDetailNotes == null}">
																	<td width="77px" align="right">
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="countPayableDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=PayableDetail&imageId=countPayableDetailNotesImage&fieldId=countPayableDetailNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=PayableDetail&imageId=countPayableDetailNotesImage&fieldId=countPayableDetailNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	<%} %>
																	</c:when>
																	<c:otherwise>
																	<td width="77px" align="right">
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="countPayableDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=PayableDetail&imageId=countPayableDetailNotesImage&fieldId=countPayableDetailNotes&decorator=popup&popup=true',755,500);"/><a onclick="javascript:openWindow('notess.html?id=${accountLine.id}&notesId=${accountLine.id}&noteFor=AccountLine&subType=PayableDetail&imageId=countPayableDetailNotesImage&fieldId=countPayableDetailNotes&decorator=popup&popup=true',755,500);" ></a></td>
																	<%} %>
																	</c:otherwise>
																</c:choose> 
																</c:if>
											                 </tr>
											                
															  <tr>
															   <c:choose>
															   <c:when test="${contractType}">
															   <td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /><font color="red" size="2">*</font></td>
															   </c:when>
															    <c:otherwise>
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.country" /></td>
																</c:otherwise></c:choose>
																<c:if test="${empty accountLine.payAccDate}">
																<td align="left" >
																 <configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.country}"
																     attribute="id=country tabindex='40'class=list-menu name=accountLine.country style=width:60px  headerKey='' headerValue='' onchange=checkPurchaseInvoiceProcessing(this,'CUR'); tabindex='40'"/>	
 										                        </c:if>
 										                        <c:if test="${not empty accountLine.payAccDate}">
 										                        <td align="left" >
 										                        
 										                        <configByCorp:customDropDown 	 listType="map" list="${country}"   fieldValue="${accountLine.country}"
																     attribute="id=country tabindex='40'class=list-menu name=accountLine.country style=width:60px  headerKey='' headerValue='' onchange='changeStatus()' onclick='checkPayAccCountry()' onfocus='checkPayAccCountry()' tabindex='40'"/>	
 										                     
 										                        </c:if>
 										                         <td align="right" class="listwhitetext"><fmt:message key="accountLine.valueDate" /></td> 
																<c:if test="${not empty accountLine.valueDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.valueDate" /> </s:text>
																	<c:if test="${empty accountLine.payAccDate}">	
																	<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" cssStyle="width:78px;" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
													                   <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																		<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>
																	</c:if>
																	<c:if test="${not empty accountLine.payAccDate}">
																	<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" cssStyle="width:78px;" size="7" maxlength="12" onkeydown=""readonly="true" />
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	 <img id="valueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	</c:if>
																	</td>
																</c:if>
																<c:if test="${empty accountLine.valueDate}">
																	<td><s:textfield  cssClass="input-text" id="valueDate" name="accountLine.valueDate" required="true" size="7" cssStyle="width:78px;" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true"/>
																	<c:if test="${empty accountLine.payAccDate}">	
																	 <% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	 <img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>
																	</c:if>
																	<c:if test="${not empty accountLine.payAccDate}">
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="valueDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	</c:if>
																</td>
																</c:if>																
																<td width="63" align="right" class="listwhitetext">Document</td>
																<c:if test="${empty accountLine.payAccDate}">
																<td align="left"><s:select  cssClass="list-menu" name="accountLine.myFileFileName" cssStyle="width:150px" list="%{myfileDocumentList}" headerKey="" headerValue="" tabindex="" />
																&nbsp;<input id="ViewBtn" type="button" class="cssbuttonA" style="width:50px; height: 22px;" name="ViewBtn" value="View" onclick="detailsView();"  /></td>
																</c:if>	
																<c:if test="${not empty accountLine.payAccDate}">
																<td align="left"><s:select  cssClass="list-menu" name="accountLine.myFileFileName" cssStyle="width:150px" list="%{myfileDocumentList}" headerKey="" headerValue="" tabindex="" onclick="javascript:alert('You can not change the document as Sent To Acc has been already filled')" />
																&nbsp;<input id="ViewBtn" type="button" class="cssbuttonA" style="width:50px; height: 22px;" name="ViewBtn" value="View" onclick="detailsView();"  /></td>
																</c:if>															
																<c:if test="${empty accountLine.id}">
																<td colspan="2"></td>
																</c:if>
																<td align="right" class="listwhitetext" >Curr&nbsp;Amount</td>
																<td><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;" key="accountLine.localAmount" size="8" maxlength="12" onkeydown="return onlyRateAllowed(event)" onchange="convertedAmount('accountLine.localAmount');calculatePayRate();" tabindex="41"/></td>
																<td align="left" class="listwhitetext"><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;" key="accountLine.actualExpense" size="8" maxlength="15" onkeydown="return onlyRateAllowed(event)"  onchange="convertLocalAmount('none');calculatePayRate();" tabindex="42"/></td>
															</tr>
															
															 <tr> 
                                                           		<td align="right"  class="listwhitetext"><fmt:message key="accountLine.receivedDate" /></td>
																<c:if test="${not empty accountLine.receivedDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.receivedDate" /></s:text>
																	<c:if test="${empty accountLine.payAccDate}">
																	<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" />
																		
																		<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																		<img id="receivedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																	<%} %>
																	</c:if>
																	<c:if test="${not empty accountLine.payAccDate}">
																	<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown=""readonly="true" />
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="receivedDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	</c:if>
																	</td>
																</c:if>
																<c:if test="${empty accountLine.receivedDate}">
																	<td width="130px"><s:textfield  cssClass="input-text" id="receivedDate" name="accountLine.receivedDate" required="true" size="7" maxlength="12" onkeydown="return onlyDel(event,this)" readonly="true" />
																		<c:if test="${empty accountLine.payAccDate}">
																		<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																		<img id="receivedDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																		<%} %>
																		</c:if>
																		<c:if test="${not empty accountLine.payAccDate}">
																		<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																		<img id="receivedDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" />
																	 <%} %>
																	 </c:if>
																	</td> 
																</c:if> 
																<td width="63" align="right" class="listwhitetext"><fmt:message key="accountLine.payingStatus" /></td>
																<c:if test="${empty accountLine.payAccDate}">
																<td align="left"> 
																<configByCorp:customDropDown 	listType="map" list="${payingStatus}"   fieldValue="${accountLine.payingStatus}"
																     attribute="id=payingStatus tabindex='43'class=list-menu name=accountLine.payingStatus style=width:102px  headerKey='' headerValue='' onchange=checkVendorInvoice('PAY'),changeStatus();"/>
															    </c:if>
															    <c:if test="${not empty accountLine.payAccDate}">
															    <td align="left">
															    <configByCorp:customDropDown 	listType="map" list="${payingStatus}"   fieldValue="${accountLine.payingStatus}"
																     attribute="id=payingStatus tabindex='43'class=list-menu name=accountLine.payingStatus style=width:90px  headerKey='' headerValue='' onchange=changeStatus() onclick='checkPayAcc()'"/>
															    </c:if>
															    <td colspan="3">
																<div id="payableDeviationShow" > 
                                                                   <table class="detailTabLabel" style="margin:0px;padding:0px;"><tr>
                                                                   <td width="71"> </td>
                                                                   <td align="right" class="listwhitetext" >Buy&nbsp;Deviation</td>
                                                                   <c:if test="${empty accountLine.payAccDate}">
                                                                   <td align="left" class="listwhitetext" ><s:textfield cssClass="input-text" readonly="<%=sectionPayableDetail%>" cssStyle="text-align:right" key="accountLine.payDeviation" size="5" maxlength="10" onkeydown="return onlyRateAllowed(event)"onblur="chkSelect('1');" onchange="convertedDeviationAmount();" tabindex="17" />%</td>
                                                                   </c:if>
                                                                    <c:if test="${not empty accountLine.payAccDate}">
                                                                   		<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" readonly="true" cssStyle="text-align:right" key="accountLine.payDeviation" size="5" tabindex="17" />%</td>
                                                                    </c:if>
                                                                   <s:hidden name="oldPayDeviation" value="${accountLine.payDeviation}"/>
                                                                   </tr></table>
                                                                </div>
																</td>
															    
															  </tr> 
															  <configByCorp:fieldVisibility componentId="component.field.AccountLine.payRate">
															  <tr>
															   <td align="right"  class="listwhitetext">Quantity</td>
															   <td align="left"  class="listwhitetext"><s:textfield readonly="<%= sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right; width:60px" key="accountLine.payQuantity"  maxlength="12" onkeydown="return onlyFloatNumsAllowed(event)" onchange="calculateActualExpense();"  /></td>
															   <td align="right"  class="listwhitetext">Buy Rate</td>
															   <td align="left" class="listwhitetext"><s:textfield readonly="<%=sectionPayableDetail%>" cssClass="input-text" cssStyle="text-align:right;" key="accountLine.payRate" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="calculateActualExpense();" /></td>
															  </tr>
															  </configByCorp:fieldVisibility>
                                                         	<tr>
																 <td align="right" class="listwhitetext"><fmt:message key="accountLine.note" /></td>
																<td colspan="3" align="left" class="listwhitetext"><s:textarea readonly="<%=sectionPayableDetail%>" name="accountLine.note" rows="1" cols="45" tabindex="46" cssStyle="width:297px; height:35px;" cssClass="textarea"/></td>
																<c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)>=0||fn1:indexOf(systemDefaultCommissionJob,serviceOrder.job)>=0}"> 
																  <td align="right" class="listwhitetext"><fmt:message key="accountLine.commission" /></td>
																   <td align="left" ><s:select  disabled="<%=sectionPayableDetail%>" cssClass="list-menu" name="accountLine.commission" cssStyle="width:107px" list="%{getCommissionList}" tabindex="44" onchange="changeStatus()"/></td> 
																   <td><input id="commissionBtn" type="button" class="cssbuttonA" style="width:80px; height: 25px;" name="commissionBtn" value="Commission" onclick="getCommissionAmtFromBCode();" tabindex="45" /></td>
															     </c:if>
															     <c:if test="${fn1:indexOf(systemDefaultmiscVl,serviceOrder.job)<0}"> 
																	<td align="left" class="listwhitetext"></td> 
																 </c:if> 
																</tr>     
															<c:if test="${systemDefaultVatCalculation=='true'}">
															<tr>
															<td class="listwhitetext" align="right" ></td>
															<td align="left" class="listwhitetext" ></td>
														    <td class="listwhitetext" align="right" ></td>
														    <td width="100px" align="left" class="listwhitetext" colspan="2">
                        									</td>
													        <td>
													        
													        </td>
															
                        									<td align="right" class="listwhitetext" colspan="4"></td>
                        									<td align="right" class="listwhitetext">VAT</td>
											
										<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.payVatAmt" id="accountLine.payVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" tabindex="49"/></td> 
															</tr></c:if>
															<tr>
															 <td align="right" class="listwhitetext" ><fmt:message key="accountLine.payPayableStatus" /></td>
															 <td align="left"> 
															<configByCorp:customDropDown 	 listType="map" list="${paymentStatus}"   fieldValue="${accountLine.payPayableStatus}"
																     attribute="id=paymentStatus class=list-menu name=accountLine.payPayableStatus style=width:110px  headerKey='' headerValue=''   onchange=changeStatus(),autoPayPayableDate(this) tabindex='50'"/>
															 <td align="right"  class="listwhitetext"><fmt:message key="accountLine.payPayableDate" /></td>
																<c:if test="${not empty accountLine.payPayableDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.payPayableDate" /></s:text>
																	<td width=""><s:textfield  cssClass="input-text" id="payPayableDate" name="accountLine.payPayableDate" value="%{accountLineFormattedValue}" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true" cssStyle="width:78px;" />
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="payPayableDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																  <%} %>
																  </c:if>
																<c:if test="${empty accountLine.payPayableDate}">
																	<td width=""><s:textfield  cssClass="input-text" id="payPayableDate" name="accountLine.payPayableDate" required="true" cssStyle="width:78px;" size="7" maxlength="12" onkeydown="return onlyDel(event,this)"readonly="true"  />
																	<% if (sectionPayableDetail.equalsIgnoreCase("false")){%>
																	<img id="payPayableDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																<%} %>
																</c:if>  
																<td align="right" class="listwhitetext"><fmt:message key="accountLine.payPayableVia" /></td>
																<td><s:textfield cssClass="input-text" key="accountLine.payPayableVia" size="6" maxlength="25" tabindex="52" cssStyle="width:74px; margin-left:11px;"/></td>
																<c:if test="${accountInterface=='Y'}"> 
																<td width="" align=right class="listwhitetext" ><fmt:message key="accountLine.payPayableAmount" /></td>
																<td><s:textfield cssClass="input-text" cssStyle="text-align:right;" key="accountLine.payPayableAmount" size="8" maxlength="17" tabindex="51"/></td>		
													            </c:if>
													            <c:if test="${accountInterface!='Y'}">
													            <td width="" align="right" class="listwhitetext" ><fmt:message key="accountLine.payPayableAmount" /></td>
																<td><s:textfield cssClass="input-text" key="accountLine.payPayableAmount" size="8" maxlength="17" tabindex="52" readonly="true" /></td>		 
													            </c:if>
													            <td></td>
													            <c:if test="${systemDefaultVatCalculation!='true'}">
													            <td></td>
													            <td></td> 
													            </c:if>
													            <c:if test="${systemDefaultVatCalculation=='true'}">
													<td align="right" class="listwhitetext"><div id="qstPayVatAmtGlLabel">VAT</div></td>
															<td align="left" class="listwhitetext"><div id="qstPayVatAmtGlId"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.qstPayVatAmt" id="accountLine.qstPayVatAmt" size="8" maxlength="15" onkeydown="return onlyFloatNumsAllowed(event)"  readonly="true" /></div></td>
															</c:if> 
													            
															</tr>
	</sec-auth:authComponent>		
													</table>
													<sec-auth:authComponent componentId="module.accountLine.section.payableDetailDate.edit">
	<%;
	String PayableDetailDate="true";
	int permissionPayableDate  = (Integer)request.getAttribute("module.accountLine.section.payableDetailDate.edit" + "Permission");
 	if (permissionPayableDate > 2 ){
 		PayableDetailDate = "false";
 		
 	}
 	
  %>
														<table width="100%" class="colored_bg" style="margin-bottom:1px;border-radius:3px;" border="0">
														<tr>
															<td height="20" width="64px"align="right" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payGl"><fmt:message key="charges.glCode"/></configByCorp:fieldVisibility></td>  
                                                            <td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payGl"><s:textfield cssClass="input-text" key="accountLine.payGl" size="7"  maxlength="7" tabindex="52" readonly="true"/></configByCorp:fieldVisibility></td> 
															<c:if test="${accountInterface=='Y'}">
															<td align="right" class="listwhitetextAcct" width="50px"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><fmt:message key="accountLine.recPostDate"/></configByCorp:fieldVisibility></td>  
                                                            <c:if test="${not empty accountLine.payPostDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.payPostDate" /></s:text>
																	<c:if test="${empty accountLine.payAccDate}">
																	<td class="listwhitetextAcct" width="90px"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" name="accountLine.payPostDate" value="%{accountLineFormattedValue}" size="7"
																		maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/>
																		<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																		
																	<img id="payPostDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																	<%} %>
																	</configByCorp:fieldVisibility></td>
																	</c:if>
																	<c:if test="${not empty accountLine.payAccDate}">
																	<td class="listwhitetextAcct" width="90px"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" cssStyle="width:60px;" name="accountLine.payPostDate" value="%{accountLineFormattedValue}" size="7"
																		maxlength="12"  onkeydown="" readonly="true"/>
																		<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="payPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" /></td>
																	<%} %>
																	</configByCorp:fieldVisibility></td>
																	</c:if>
																 
																	
																 </c:if>
																 <c:if test="${empty accountLine.payPostDate}">
																	<td class="listwhitetextAcct" width="90px"><configByCorp:fieldVisibility componentId="accountLine.payPostDate"><s:textfield cssClass="input-text" id="payPostDate" cssStyle="width:60px;" name="accountLine.payPostDate" required="true" size="7" maxlength="12"
																		 onkeydown="return onlyDel(event,this)"readonly="true"/> 
																		<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<c:if test="${empty accountLine.payAccDate}">	
																	<img id="payPostDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																	</c:if>
																	<c:if test="${not empty accountLine.payAccDate}">
																	<img id="payPostDate" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="javascript:alert('You can not change the date as Sent To Acc has been already filled')" /></td>
																	</c:if>
																<%} %> 
																	  </configByCorp:fieldVisibility></td>
																</c:if> 
																</c:if>  
															<td align="right" class="listwhitetextAcct" style="width:316px;" ><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><fmt:message key="accountLine.accruePayable" /></configByCorp:fieldVisibility></td> 
																	<c:if test="${not empty accountLine.accruePayable}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.accruePayable" /></s:text>
																	<td class="listwhitetextAcct" width="100px"><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><s:textfield cssClass="input-text" id="accruePayable"   name="accountLine.accruePayable" value="%{accountLineFormattedValue}" size="8"
																		maxlength="12"  onkeydown="" readonly="true"/>
																	<%--<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="accruePayable_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																<%} %> 	 
																	--%></configByCorp:fieldVisibility></td>
																 </c:if>
																 <c:if test="${empty accountLine.accruePayable}">
																	<td class="listwhitetextAcct" width="100px"><configByCorp:fieldVisibility componentId="accountLine.accruePayable"><s:textfield cssClass="input-text" id="accruePayable" name="accountLine.accruePayable" required="true" size="8" maxlength="12"
																		 onkeydown=""readonly="true"/> 
																	<%--<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="accruePayable_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																<%} %> 	 
																	  --%></configByCorp:fieldVisibility></td>
																</c:if>  
                                                              <c:if test="${accountInterface=='Y'}">
    <sec-auth:authComponent componentId="module.accountLine.section.payableDetailAccrual.edit">
	<%;
	String payableDetailAccrual="true";
	int permissionPayAccural  = (Integer)request.getAttribute("module.accountLine.section.payableDetailAccrual.edit" + "Permission");
 	if (permissionPayAccural > 2 ){
 		payableDetailAccrual = "false";
 		
 	}
 	
  %>
                                                              <td align="right"  class="listwhitetextAcct"><fmt:message key="accountLine.accruePayableManual" /></td>
                                                              <c:choose>
                                                            <c:when test="${empty accountLine.accruePayable }">
                                                           <td align="left" ><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange="changeStatus()"/></td>
                                                            </c:when>
                                                            <c:when test="${not empty accountLine.accruePayable && empty accountLine.accruePayableReverse}">
                                                             <td align="left" ><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="<%=payableDetailAccrual%>" onchange="changeStatus()"/></td>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <td align="left" ><s:checkbox name="accountLine.accruePayableManual" value="${accountLine.accruePayableManual}" fieldValue="true" cssStyle="margin:0px;" disabled="true" onchange="changeStatus()"/></td>
                                                            </c:otherwise>
                                                            </c:choose> 
  </sec-auth:authComponent> 
                                                              <td align="right"  class="listwhitetextAcct"><fmt:message key="accountLine.accruePayableReverse" /></td>
	                                                           <c:if test="${not empty accountLine.accruePayable }"> 
	                                                            <c:if test="${not empty accountLine.accruePayableReverse}"> 
		                                                           <s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.accruePayableReverse"/></s:text>
		                                                           <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" value="%{accountLineFormattedValue}" onkeydown="" readonly="true"  cssClass="input-text" size="8"/>
	                                                            <%--<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="accruePayableReverse_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																<%} %>--%> </td>
	                                                            </c:if>
	                                                             <c:if test="${empty accountLine.accruePayableReverse}">
	                                                                <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" onkeydown="" readonly="true" cssClass="input-text"  size="8"/>
	                                                            <%-- <% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="accruePayableReverse_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																<%} %>--%> </td>
	                                                             </c:if>
	                                                             </c:if>
	                                                             <c:if test="${empty accountLine.accruePayable }"> 
	                                                             <c:if test="${not empty accountLine.accruePayableReverse}"> 
		                                                           <s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.accruePayableReverse"/></s:text>
		                                                           <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse" value="%{accountLineFormattedValue}"  readonly="true" cssClass="input-text"  size="8"/>
	                                                            </td>
	                                                            </c:if>
	                                                             <c:if test="${empty accountLine.accruePayableReverse}">
	                                                                <td><s:textfield id="accruePayableReverse" name="accountLine.accruePayableReverse"  readonly="true" cssClass="input-text"  size="8"/>
	                                                             </td>
	                                                             </c:if>
	                                                             </c:if> 
	                                                             </c:if> 
															</tr>
															<tr>
															<c:if test="${accountInterface=='Y'}">
																<td height="20" align="right" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><fmt:message key="accountLine.recAccDate"/></configByCorp:fieldVisibility></td>  
	                                                            <c:if test="${not empty accountLine.payAccDate}">
																	<s:text id="accountLineFormattedValue" name="${FormDateValue2}"><s:param name="value" value="accountLine.payAccDate" /></s:text>
																	<% if (!(PayableDetailDate.equalsIgnoreCase("false"))){%>
																	<td class="listwhitetextAcct" width="90px"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" value="%{accountLineFormattedValue}" size="7"
																		 maxlength="12"   readonly="true"/> 
																		 </configByCorp:fieldVisibility></td>
																<%} %>
																<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																<td class="listwhitetextAcct" width="95px"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" value="%{accountLineFormattedValue}" size="7"
																		 maxlength="12"  onkeydown="return onlyDel(event,this)" readonly="true"/> 
																	<img id="payAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
																</configByCorp:fieldVisibility></td>
																<%} %>
																
																 </c:if>
																 <c:if test="${empty accountLine.payAccDate}">
																	<td class="listwhitetextAcct" width="90px"><configByCorp:fieldVisibility componentId="accountLine.payAccDate"><s:textfield cssClass="input-text" id="payAccDate" name="accountLine.payAccDate" required="true" size="7" maxlength="12"
																		 onkeydown="return onlyDel(event,this)"readonly="true"/> 
																	<% if (PayableDetailDate.equalsIgnoreCase("false")){%>
																	<img id="payAccDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
																<%} %>
																	  </configByCorp:fieldVisibility></td>
																</c:if> 
																</c:if> 
																<td align="right"  class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payXfer"><fmt:message key="accountLine.recXfer"/></configByCorp:fieldVisibility></td>  
	                                                            <td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payXfer"><s:textfield cssClass="input-text" tabindex="53" key="accountLine.payXfer" size="15" cssStyle="width:82px;" maxlength="20" readonly="<%=PayableDetailDate%>" /></configByCorp:fieldVisibility></td> 	
																
                                                            <c:if test="${accountInterface=='Y'}">
                                                             <td height="20" align="right" class="listwhitetextAcct"><fmt:message key="accountLine.payXferUser"/></td>  
                                                             <td align="left" class="listwhitetextAcct"><s:textfield cssClass="input-text" key="accountLine.payXferUser" tabindex="54" size="8"  maxlength="7" readonly="<%=PayableDetailDate%>" /></td> 	 	 
															</c:if>
															<script type="text/javascript"> 
                                                               disableBYPayAcc()    
                                                            </script>
																	<td align="right" class="listwhitetext"><configByCorp:fieldVisibility componentId="accountLine.settledDate">Settled Date</configByCorp:fieldVisibility></td>
							<c:if test="${not empty accountLine.settledDate}">
								<s:text id="accountLineSettledDateValue2" name="${FormDateValue2}">
									<s:param name="value" value="accountLine.settledDate" />
								</s:text>
								<td width="50px"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><s:textfield cssClass="input-text" id="settledDate" readonly="true" name="accountLine.settledDate" value="%{accountLineSettledDateValue2}" size="7" maxlength="11" required="true"/></configByCorp:fieldVisibility></td>
								<td align="left" style="padding-top:1px;"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
							</c:if>
							<c:if test="${empty accountLine.settledDate}">
								<td width="50px"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><s:textfield cssClass="input-text" id="settledDate" readonly="true" name="accountLine.settledDate" size="7" maxlength="11" required="true" /></configByCorp:fieldVisibility></td>
								<td align="left" style="padding-top:1px;"><configByCorp:fieldVisibility componentId="accountLine.settledDate"><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></configByCorp:fieldVisibility></td>
							</c:if>
															<td width="113px">
															<s:hidden name="accountLine.costTransferred" />
															<configByCorp:fieldVisibility componentId="component.accountLine.costTransferred">
																<c:if test="${company.useCostTransferred == true }">
																	Cost Xfer&nbsp;&nbsp;<s:checkbox name="costTransferred" value="${accountLine.costTransferred}" disabled="true"  />
																</c:if>
															</configByCorp:fieldVisibility>
															</td>
															
															 <c:choose>
															<c:when test="${(serviceOrder.grpStatus=='Finalized' || serviceOrder.grpStatus=='finalized')  && (serviceOrder.controlFlag=='G') && (accountLine.status)}"> 
															<tr>
															<td align="right" colspan="9" style="padding-right:60px;"><input type="button" class="cssbuttonA"   style="width:155px; height:25px" name="distributePayableStatus" value="Distribute groupage cost" onclick="distributeOrdersAmount('UnderPayableStatus');"></td>
															</tr>
															</c:when>
															<c:otherwise>
															<tr></tr>
															</c:otherwise>
															</c:choose>
															</tr> 
															<tr>
															<c:if test="${systemDefaultVatCalculation=='true'}">
															<td align="right" width=""  class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payVatGl">VAT&nbsp;GL&nbsp;Code</configByCorp:fieldVisibility></td>
															<td align="left" class="listwhitetextAcct"><configByCorp:fieldVisibility componentId="accountLine.payVatGl"><s:textfield cssClass="input-text" key="accountLine.payVatGl" size="7"  maxlength="7" readonly="true" tabindex="35" /></configByCorp:fieldVisibility></td>
															<td align="right" width=""  class="listwhitetextAcct"><div id="qstPayVatGlLabel">Qst&nbsp;GL&nbsp;Code</div></td>
																<td align="left"><div id="qstPayVatGlId"><s:textfield cssClass="input-text" key="accountLine.qstPayVatGl" size="7"  maxlength="7" readonly="true" tabindex="35" /></div></td>
															</c:if>
															</tr>
														</table>	
	</sec-auth:authComponent>
													