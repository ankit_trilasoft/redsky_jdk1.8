<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="extractColumnMgmtDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='extractColumnMgmtDetail.heading'/>"/>  
    
<style>
.tab{
	border:1px solid #74B3DC;
}
span.pagelinks {
    display: block;
    font-size: 0.9em;
    margin-bottom: 1px;
    margin-top: -14px;        
    text-align: right;
    width:100%;
}

</style> 
</head>
<script language="javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this job?");
	var did = targetElement;
	if (agree){
		location.href="deleteColumnManagementList.html?id="+did;
	}else{
		return false;
	}
}

function clear_fields(){
	document.forms['columnManagementList'].elements['reportName'].value = "";
	document.forms['columnManagementList'].elements['tableName'].value = "";
	document.forms['columnManagementList'].elements['fieldNameSearch'].value = "";
	document.forms['columnManagementList'].elements['displayName'].value = "";
	document.forms['columnManagementList'].elements['corp_id'].value = "";
}


</script>
<s:form id="columnManagementList" action="searchColumnManagementList" method="post" validate="true">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yyyy}"/>
<c:set var="corp_Id" value="${sessionCorpID}" />
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:55px;" align="top" method="" key="button.search"/> 
	
    <input type="button" class="cssbutton1" value="Clear" style="width:55px;" onclick="clear_fields();"/> 
   
</c:set>
<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

<div id="Layer1" style="width: 100%;">	
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
 <div class="center-content">
<table class="table" style="width:98%;">
<thead>
<tr>
<th>Report Name</th>
<th>Table Name</th>
<th>Field Name</th>
<th>Display Name</th>
<c:if test="${corp_Id == 'TSFT'}">
	<th>Corp Id</th>
</c:if>
<th>&nbsp;</th>
</tr></thead>	
		<tbody>
			<tr>
			<td><s:select cssClass="list-menu" name="reportName" list="%{reportNameList}" cssStyle="width:150px" headerKey="" headerValue="" /></td>
			<td><s:select cssClass="list-menu" name="tableName" list="%{tableNameList}" cssStyle="width:150px" headerKey="" headerValue="" /></td>
			<td><s:select cssClass="list-menu" name="fieldNameSearch" list="%{fieldNameList}" cssStyle="width:150px" headerKey="" headerValue=""/></td>
			<td><s:select cssClass="list-menu" name="displayName" cssStyle="width:150px" headerKey="" headerValue="" list="%{displayNameList}"></s:select></td>
			<c:if test="${corp_Id == 'TSFT'}">
				<td align="right"><s:select	name="corpIdSearch" cssClass="list-menu" cssStyle="width:140px" headerKey="" headerValue="" list="%{corpIdList}"></s:select></td>
			</c:if>
			<%-- <td><s:textfield cssClass="input-text" name="createdOn"  id="begin" size="7" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)"/>
			<img id="begin_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td> --%>
			<td width="230px" style="border-left:hidden;" align="right">
			       <c:out value="${searchbuttons}" escapeXml="false" /></td>
			</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>

<div id="newmnav">
	  <ul>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Field Details List</span></a></li>  	
	  </ul>
</div>
<div class="spn">&nbsp;</div>
<s:set name="extractColumnPropertiesList" value="extractColumnPropertiesList" scope="request"/>
<display:table name="extractColumnPropertiesList" class="table" requestURI="" id="extractColumnPropertiesList" export="true" defaultsort="1" pagesize="20" style="width:100%; !margin-top:5px;">
		<c:if test="${not empty param.popup}">
			<display:column property="id" sortable="true" style="width: 15px;" href="editColumnManagement.html" paramId="id" paramProperty="id" title="Id"></display:column>
		</c:if>
		<c:if test="${empty param.popup}">
		
			<display:column property="id" sortable="true" style="width: 15px;" href="editColumnManagement.html?&decorator=popup&popup=false" paramId="id" paramProperty="id" title="Id"></display:column>
		</c:if>
		<display:column property="reportName" sortable="true" style="width: 100px;" title="Report Name"></display:column>
		<display:column property="tableName" sortable="true" style="width: 100px;" title="Table Name"></display:column>
		<display:column property="fieldName" sortable="true" style="width: 150px;" title="Field Name"></display:column>
		<display:column property="displayName" sortable="true" style="width: 100px;" title="Display Name"></display:column>
		<display:column property="corpID" sortable="true" style="width: 100px;" title="Corp Id"></display:column>
		<display:column property="createdOn" sortable="true" style="width: 100px;" title="Created On"></display:column>
		<display:column title="Remove" style="width: 15px;">
		<c:if test="${corp_Id == 'TSFT'}">
				<a><img align="middle" title="" onclick="confirmSubmit('${extractColumnPropertiesList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a>
		</c:if>

		<c:if test="${corp_Id != 'TSFT' && extractColumnPropertiesList.corpID != 'TSFT'}">
				<a><img align="middle" title="" onclick="confirmSubmit('${extractColumnPropertiesList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/></a>
		</c:if>
		</display:column>
		<display:setProperty name="paging.banner.item_name" value="Report Field Type"/>   
		<display:setProperty name="paging.banner.items_name" value="Report Field Type"/>   
				  
		<display:setProperty name="export.excel.filename" value="Report Field Type List.xls"/>   
		<display:setProperty name="export.csv.filename" value="Report Field Type List.csv"/>   
		<display:setProperty name="export.pdf.filename" value="Report Field Type List.pdf"/>
	</display:table>

<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editColumnManagement.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>
<table> 
<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>

</s:form>
<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>