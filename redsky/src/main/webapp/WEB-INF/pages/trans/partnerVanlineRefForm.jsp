<%@ include file="/common/taglibs.jsp"%>   
<head>   
<title><fmt:message key="partnerVanLineRefDetail.title"/></title>   
<meta name="heading" content="<fmt:message key='partnerVanLineRefDetail.heading'/>"/>   
<script type="text/javascript">

function disabledAll(){
	if(!(document.forms['partnerVanlineRefForm'].elements['partnerVanLineRef.id'].value == '')){
	var i;
		for(i=0;i<=100;i++){
		document.forms['partnerVanlineRefForm'].elements[i].disabled = true;
		}
	}
}

function enableAll(){
	if(!(document.forms['partnerVanlineRefForm'].elements['partnerVanLineRef.id'].value == '')){
	var i;
		for(i=0;i<=100;i++){
		document.forms['partnerVanlineRefForm'].elements[i].disabled = false;
		}
	}
}
</script>
</head>   
 <s:hidden name="fileNameFor"  id= "fileNameFor" value="PO"/>
<s:hidden name="fileID" id ="fileID" value="%{partner.id}" />
<c:set var="fileID" value="%{partner.id}"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="ppType" value="<%= request.getParameter("partnerType")%>"/>

<s:form id="partnerVanlineRefForm" action="savePartnerVanlineRef" method="post" validate="true">   
<s:hidden name="partnerVanLineRef.corpID"/>   
<s:hidden name="partnerVanLineRef.id"/>   
<s:hidden name="partnerVanLineRef.partnerCode"/>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="partner.id"/>
<div id="Layer1" style="width:100%">
	<div id="newmnav">
		  <ul>
				<c:if test="${partnerType == 'AG'}">
						 <li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}" ><span>Agent Detail</span></a></li>
						 <li><a href="findPartnerProfileList.html?code=${partner.partnerCode}&partnerType=${partnerType}&id=${partner.id}"><span>Agent Profile</span></a></li>
						<c:if test="${sessionCorpID!='TSFT' }">
						 <li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
						 </c:if>
						 <li><a href="partnerAccountRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Acct Ref</span></a></li>
				 </c:if>
				 <li><a href="partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"><span>Vanline Ref List</span></a></li>           
				<c:if test="${partnerType == 'VN'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}"><span>Vendor Detail</span></a></li>
						<c:if test="${sessionCorpID!='TSFT' }">
						<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=${partnerType}&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
						</c:if>
				</c:if>
				<c:if test="${partnerType == 'CR'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}"><span>Carrier Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType == 'AG'}">
				        <li><a href="baseList.html?id=${partner.id}"><span>Base</span></a></li>
				        	<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
				        <!--<c:if test='${partner.latitude == ""  || partner.longitude == "" || partner.latitude == null  || partner.longitude == null}'>
			              <li><a onclick="javascript:alert('Please confirm and update your Agent Profile with the Geographic Coordinates before you update your tariff.  Whilst you are in the profile, please ensure that your information about FIDI, IAM membership, Service Lines, Quality Certifications, etc. is updated?')"><span>Rate Matrix</span></a></li>	
			            </c:if>
			            <c:if test='${partner.latitude != "" && partner.longitude != "" && partner.latitude != null && partner.longitude != null}'>
			             <li><a href="partnerRatess.html?id=${partner.id}"><span>Rate Matrix</span></a></li>
			            </c:if>
				        
				--></c:if>
				<c:if test="${partnerType == 'OO'}">
						<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=${partnerType}"><span>Owner Ops Detail</span></a></li>
				</c:if>
				<c:if test="${partnerType != 'AG'}">
					<c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
					<c:if test='${paramValue == "View"}'>
						<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
					</c:if>
					<c:if test='${paramValue != "View"}'>
						<li><a href="partnerPublics.html?partnerType=${partnerType}"><span>Partner List</span></a></li>
					</c:if>
				</c:if>
				<c:if test="${partnerType == 'AG'}">
					<li><a href="partnerReloSvcs.html?id=${partner.id}&partnerType=${partnerType}"><span>Services</span></a></li>
					<c:if test="${partner.partnerPortalActive == true  && partnerType == 'AG'}">
					       	<li><a href="partnerUsersList.html?id=${partner.id}&partnerType=${partnerType}"><span>Portal Users & contacts</span></a></li>
					       	<configByCorp:fieldVisibility componentId="component.field.partnerUser.showForTsftOnly">
					       	<li><a onclick="window.open('getAssignedSecurity.html?partnerId=${partner.id}&partnerType=${partnerType}&decorator=popup&popup=true','forms','height=400,width=900,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Filter Sets</span></a></li>
					       </configByCorp:fieldVisibility>
					</c:if>
				</c:if>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Vanline Ref Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		 </ul>
	</div><div class="spn">&nbsp;</div>
	
</div>
<div id="Layer1" style="width:100%"> 
<div id="content" align="center">
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
 	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:650px">
  		<tbody>
			<tr>
				<td align="left" class="listwhitetext">
					<table class="detailTabLabel" border="0" cellspacing="1" cellpadding="1">
  						<tbody>  	
						  	<tr>
						  		<td align="left" height="3px"></td>
						  	</tr>
						  	<tr>
  								<td align="right" width="100px" >Code</td>
								<td><s:textfield key="partner.partnerCode" required="true" readonly="true" cssClass="input-text" size="10"/></td>
								<td align="right" width="80px">Name</td>
								<td><s:textfield key="partner.lastName" required="true" size="35" readonly="true" cssClass="input-text"/></td>
							</tr>
							<tr>
								<td height="5px"></td>
							</tr>
						    <tr>
						    	<td align="right"><fmt:message key="partnerVanLineRef.vanlineCode"/><font color="red" size="2">*</font></td>
								<td><s:textfield name="partnerVanLineRef.vanLineCode" maxlength="8" required="true" cssClass="input-text" size="10"/></td>
								<td align="right"><fmt:message key="partnerVanLineRef.jobType"/><font color="red" size="2">*</font></td>
						    	<td><s:select cssClass="list-menu" name="partnerVanLineRef.jobType" list="{'UVL','MVL','UGW','PPI'}" cssStyle="width:210px" headerKey="" headerValue=""/></td>
							</tr>
							<tr>
								<td height="5px"></td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
		<table style="width:700px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partnerVanLineRef.createdOn}" pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerVanLineRef.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${partnerVanLineRef.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty partnerVanLineRef.id}">
								<s:hidden name="partnerVanLineRef.createdBy"/>
								<td><s:label name="createdBy" value="%{partnerVanLineRef.createdBy}"/></td>
						</c:if>
						<c:if test="${empty partnerVanLineRef.id}">
								<s:hidden name="partnerVanLineRef.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partnerVanLineRef.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerVanLineRef.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${partnerVanLineRef.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						
						<c:if test="${not empty partnerVanLineRef.id}">
							<s:hidden name="partnerVanLineRef.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{partnerVanLineRef.updatedBy}"/></td>
						</c:if>
						<c:if test="${empty partnerVanLineRef.id}">
							<s:hidden name="partnerVanLineRef.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
						</c:if>
					</tr>
				</tbody>
			</table>
		</div>								 
 
        <s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:70px; height:25px" /> 
        <input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}"/>'" />
        <s:reset cssClass="cssbutton" key="Reset" cssStyle="width:70px; height:25px "/> 
 
</s:form>   
<script type="text/javascript"> 		
		try{
		<c:if test="${hitFlag == 1}" >
			<c:redirect url="/partnerVanlineRefs.html?partnerCodeForRef=${partner.partnerCode}&partnerType=${partnerType}" ></c:redirect>
		</c:if>
		}
		catch(e){}
</script>    
<script type="text/javascript">   
    Form.focusFirstElement($("partnerVanlineRefForm"));  
    var checkRoleAG = 0; 
    <sec-auth:authComponent componentId="module.tab.partner.AgentDEV">
	checkRoleAG  = 14;
</sec-auth:authComponent>
if(checkRoleAG < 14){
	 disabledAll();
   }
</script>
