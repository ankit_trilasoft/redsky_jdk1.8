package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.hibernate3.SessionFactoryUtils;

import com.trilasoft.app.dao.CorpComponentPermissionDao;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.CorpComponentPermission;

public class CorpComponentPermissionDaoHibernate extends GenericDaoHibernate<CorpComponentPermission, Long> implements CorpComponentPermissionDao {   
	Logger logger = Logger.getLogger(CorpComponentPermissionDaoHibernate.class);
    public CorpComponentPermissionDaoHibernate() {   
        super(CorpComponentPermission.class);   
    }
    private static Map pageFieldVisibilityMap = AppInitServlet.pageFieldVisibilityComponentMap;
    
	public boolean getFieldVisibilityForComponent(String userCorpID, String componentId) {
		Boolean temp = new Boolean(false);
		//List searchResults = this.getSession().createSQLQuery("SELECT ccp.mask from corp_comp_permission ccp where ccp.corp_id = '"+userCorpID+"' and " +
			//	"ccp.componentId = '" + componentId +"'").list();
		
		/*try {
			Session session = SessionFactoryUtils.getSession(this.getSession()
					.getSessionFactory(), true);
			List<CorpComponentPermission> compPerms = session
					.createCriteria(CorpComponentPermission.class)
					.add(Restrictions.eq("corpID", userCorpID))
					.add(Restrictions.eq("componentId", componentId))
					.list();
			List<Integer> searchResults = new ArrayList();
			for (CorpComponentPermission cp : compPerms) {
				searchResults.add(cp.getMask());
			}
			if (searchResults.isEmpty() || ((Integer) searchResults.get(0)) < 2) {
				temp =  false;
			} else {
				temp =  true;
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e);
		}*/
		try {
		if(pageFieldVisibilityMap.containsKey(userCorpID +"-"+componentId)) {
			Integer i= (Integer) pageFieldVisibilityMap.get(userCorpID +"-"+componentId) ;
			if(i>=2){
			temp=true;
			}
		} 
		} catch (Exception e) {
			log.error("Error in CorpComponentPermission the  Field Visibility cache",e);
		}
		return temp;
	}

	public List findMaximumId() {
		return getHibernateTemplate().find("select max(id) from CorpComponentPermission");
	}

	public List searchcompanyPermission(String componentId, String description,String corpId) {
		List temp = new ArrayList();
		try {
			if (corpId.equalsIgnoreCase("TSFT")) {
				temp = getHibernateTemplate().find(
						"from CorpComponentPermission where componentId like '%"
								+ componentId.replaceAll("'", "''")
								+ "%' and description like '%"
								+ description.replaceAll("'", "''") + "%'");
			} else {
				temp = getHibernateTemplate().find(
						"from CorpComponentPermission where componentId like '%"
								+ componentId.replaceAll("'", "''")
								+ "%' and description like '%"
								+ description.replaceAll("'", "''")
								+ "%' and corpID='" + corpId + "'");
			}
		} catch (Exception e) {
			logger.error("Error executing query"+ e);
		}
		return temp;
	}
	public List findPermissionsByCorpId(String corpId)
	{
		List temp = new ArrayList();
		try {
			temp = getHibernateTemplate().find(
					"from CorpComponentPermission where corpID='" + corpId
							+ "'");
		} catch (Exception e) {
			logger.error("Error executing query"+ e);
		}
		return temp; 
	}
	public List findCorpID(){
		List temp = new ArrayList();
		try {
			temp = this.getSession()
					.createSQLQuery("select corpID from company").list();
		} catch (Exception e) {
			logger.error("Error executing query"+ e);
		}
		return temp ;
	}

	public Map<String, Integer> getComponentVisibilityAttrbuteDetail(String userCorpID, String componentId) {
		List<CorpComponentPermission> list;
		Map<String, Integer> pageFieldVisibilityComponentMap = new LinkedHashMap<String, Integer>();
		try {
			list = getHibernateTemplate().find(
					"from CorpComponentPermission where componentId  in ("+componentId+") and corpID='" + userCorpID + "' and mask is not null  ");
			for (CorpComponentPermission corpComponentPermission : list) {
				pageFieldVisibilityComponentMap.put(corpComponentPermission.getComponentId(), corpComponentPermission.getMask());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		//System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n in DAO pageFieldVisibilityComponentMap     >>>>>>>>     "+pageFieldVisibilityComponentMap+"\n\n\n\n  Corpid of pageFieldVisibilityComponentMap "+userCorpID );
		return pageFieldVisibilityComponentMap;
		 
	}

	public void updateCurrentFieldVisibilityMap() {
		try {
			String query="select corp_id,componentId,mask from corp_comp_permission rb where componentId is not null and componentId !='' and mask is not null and mask !='' and corp_id in (select corpID from company where status is true)";
			List rolePermissions= this.getSession().createSQLQuery(query).list();
			Map permissionMap = new HashMap();
			Iterator it=rolePermissions.iterator();
			while(it.hasNext()){
				 Object [] row=(Object[])it.next();
				String permKey = row[0] +"-"+row[1];
				 permissionMap.put(permKey, row[2]);
				
			}
			pageFieldVisibilityMap.clear();
			pageFieldVisibilityMap.putAll(permissionMap);			
		} catch (Exception e) {
			log.error("Error creating the  Field Visibility cache",e);
		} 
	}
}