/**
 * WSQuestionSettings.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSQuestionSettings  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private int displayLayout;

    private boolean hideQuestion;

    private boolean newPageNumber;

    private long questionId;

    public WSQuestionSettings() {
    }

    public WSQuestionSettings(
           long versionId,
           int displayLayout,
           boolean hideQuestion,
           boolean newPageNumber,
           long questionId) {
        super(
            versionId);
        this.displayLayout = displayLayout;
        this.hideQuestion = hideQuestion;
        this.newPageNumber = newPageNumber;
        this.questionId = questionId;
    }


    /**
     * Gets the displayLayout value for this WSQuestionSettings.
     * 
     * @return displayLayout
     */
    public int getDisplayLayout() {
        return displayLayout;
    }


    /**
     * Sets the displayLayout value for this WSQuestionSettings.
     * 
     * @param displayLayout
     */
    public void setDisplayLayout(int displayLayout) {
        this.displayLayout = displayLayout;
    }


    /**
     * Gets the hideQuestion value for this WSQuestionSettings.
     * 
     * @return hideQuestion
     */
    public boolean isHideQuestion() {
        return hideQuestion;
    }


    /**
     * Sets the hideQuestion value for this WSQuestionSettings.
     * 
     * @param hideQuestion
     */
    public void setHideQuestion(boolean hideQuestion) {
        this.hideQuestion = hideQuestion;
    }


    /**
     * Gets the newPageNumber value for this WSQuestionSettings.
     * 
     * @return newPageNumber
     */
    public boolean isNewPageNumber() {
        return newPageNumber;
    }


    /**
     * Sets the newPageNumber value for this WSQuestionSettings.
     * 
     * @param newPageNumber
     */
    public void setNewPageNumber(boolean newPageNumber) {
        this.newPageNumber = newPageNumber;
    }


    /**
     * Gets the questionId value for this WSQuestionSettings.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSQuestionSettings.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSQuestionSettings)) return false;
        WSQuestionSettings other = (WSQuestionSettings) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.displayLayout == other.getDisplayLayout() &&
            this.hideQuestion == other.isHideQuestion() &&
            this.newPageNumber == other.isNewPageNumber() &&
            this.questionId == other.getQuestionId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += getDisplayLayout();
        _hashCode += (isHideQuestion() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isNewPageNumber() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += new Long(getQuestionId()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSQuestionSettings.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestionSettings"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("displayLayout");
        elemField.setXmlName(new javax.xml.namespace.QName("", "displayLayout"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("hideQuestion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "hideQuestion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("newPageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("", "newPageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
