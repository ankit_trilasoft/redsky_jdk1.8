<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="truckList.title"/></title>   
    <meta name="heading" content="<fmt:message key='truckList.heading'/>"/>   
    
 
 <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['searchForm'].elements['truck.localNumber'].value = "";
	document.forms['searchForm'].elements['truck.warehouse'].value = "";
	document.forms['searchForm'].elements['truck.description'].value = "";	
	document.forms['searchForm'].elements['truck.type'].value = "";
	document.forms['searchForm'].elements['truck.state'].value = "";
	document.forms['searchForm'].elements['truck.tagNumber'].value = "";
	document.forms['searchForm'].elements['truck.ownerPayTo'].value = "";
}
</script>
	
<style>	
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-17px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
font-size:.85em;
}
</style>
</head>
<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />
  
<s:form id="searchForm" action='${empty param.popup?"searchTruckTrailer.html":"searchTruckTrailer.html?decorator=popup&popup=true"}' method="post" validate="true"> 
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="/editTruck.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;!margin-bottom: 10px;" align="top" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set> 
  
<div id="otabs">
<ul>
<li><a class="current"><span>Search</span></a></li>
</ul>
</div>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:99%"  >
	<thead>
		<tr>
		    <th align="center"><fmt:message key="truck.localNumber"/>
		    <th align="center"><fmt:message key="truck.type"/></th>
		     <th align="center">Owner</th>		
			<th align="center"><fmt:message key="truck.description"/>				
			<th align="center">State&nbsp;Code</th>	
			<th align="center">License&nbsp;#&nbsp;/Tag&nbsp;#</th>
			<th align="center"><fmt:message key="truck.warehouse"/>
			
		</tr>
	</thead>	
	<tbody>
		<tr>
		    <td width="" align="left"><s:textfield name="truck.localNumber" required="true" cssClass="input-text" size="20"/></td>
            <td width="" align="left"><s:select cssClass="list-menu" name="truck.type" list="%{trailerTactorType}" headerKey="" headerValue="" cssStyle="width:125px"/></td>
            <td width="" align="left"><s:textfield name="truck.ownerPayTo" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.description" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.state" required="true" cssClass="input-text" size="4"/></td>
			<td width="" align="left"><s:textfield name="truck.tagNumber" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.warehouse" required="true" cssClass="input-text" size="20"/></td>
		</tr>
		<tr>
		    <td colspan="6"></td>	
			
			<td width="" align="center" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false" /></td>
		</tr>
	</tbody>
</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</s:form>
<c:out value="${searchresults}" escapeXml="false" />  
<s:set name="truckList" value="truckList" scope="request"/>
<div id="otabs">
	<ul>
		<li><a class="current"><span>Trailer List</span></a></li>
	</ul>
	
</div>
<div class="spnblk">&nbsp;</div>   
<display:table name="truckList" class="table" requestURI="" id="truckList" export="${empty param.popup}" pagesize="10" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id"/>   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type"/>
    <display:column property="ownerPayTo" sortable="true" title="Owner"/>    
    <display:column property="description" sortable="true" titleKey="truck.description"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>   
</display:table>   
  </tr>
  </tbody>
  </table>
  </div>
  <c:if test="${empty param.popup}"> 
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
  
<script type="text/javascript">   
    document.forms['searchForm'].elements['truck.localNumber'].focus(); 
    highlightTableRows('truckList');   
</script>  
