package com.trilasoft.app.service;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.DsMoveMgmt;


public interface DsMoveMgmtManager extends GenericManager<DsMoveMgmt, Long>{
	List getListById(Long id);
}


