delimiter $$

CREATE trigger redsky.trigger_add_history_datasecuritypermission

after insert on redsky.datasecuritypermission

for each row

BEGIN

  DECLARE fieldNameList LONGTEXT;

  DECLARE oldValueList LONGTEXT;

  DECLARE newValueList LONGTEXT;

  DECLARE oldCorpid LONGTEXT;

  DECLARE newUser LONGTEXT;

  DECLARE newRoleName LONGTEXT;

 

  SET fieldNameList = " "; 

  SET oldValueList = " ";

  SET newValueList = " ";

  SET oldCorpid = " ";

  SET newUser = " ";

  SET newRoleName = " ";

 

select corpid into oldCorpid from datasecurityset where id=NEW.datasecurityset_id;



IF (NEW.datasecurityfilter_id <> '' or NEW.datasecurityfilter_id is not null) THEN

    select name,updatedby into newRoleName,newUser from datasecurityfilter where id=NEW.datasecurityfilter_id;

       select CONCAT(fieldNameList,'datasecuritypermission.datasecurityfilter_id~') into fieldNameList;

       select CONCAT(newValueList,newRoleName,'~') into newValueList;

   END IF;

 



CALL add_tblHistory (NEW.datasecurityset_id,"datasecuritypermission", fieldNameList, oldValueList, newValueList, newUser, oldCorpid, now());



END $$
delimiter;

 

 

