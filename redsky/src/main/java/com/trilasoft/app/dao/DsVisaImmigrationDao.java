package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao; 

import com.trilasoft.app.model.DsVisaImmigration;

public interface DsVisaImmigrationDao extends GenericDao<DsVisaImmigration, Long>{
	
	List getListById(Long id);

}
