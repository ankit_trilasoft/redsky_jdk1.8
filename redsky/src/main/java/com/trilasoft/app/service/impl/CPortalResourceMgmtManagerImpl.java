package com.trilasoft.app.service.impl;

import java.util.List;

import com.trilasoft.app.dao.CPortalResourceMgmtDao;
import com.trilasoft.app.model.CPortalResourceMgmt;   
import com.trilasoft.app.service.CPortalResourceMgmtManager;

import org.appfuse.service.impl.GenericManagerImpl;

public class CPortalResourceMgmtManagerImpl extends GenericManagerImpl<CPortalResourceMgmt, Long> implements CPortalResourceMgmtManager{
	CPortalResourceMgmtDao cPortalResourceMgmtDao;
	public CPortalResourceMgmtManagerImpl(CPortalResourceMgmtDao cPortalResourceMgmtDao) {   
        super(cPortalResourceMgmtDao);   
        this.cPortalResourceMgmtDao = cPortalResourceMgmtDao;   
    }   
  
	public List cPortalResourceMgmtList(String corpId) {   
       return cPortalResourceMgmtDao.cPortalResourceMgmtList(corpId);
    }   
	public List getMaxId(){
		return cPortalResourceMgmtDao.getMaxId();
	}
	public List getListByDesc(Long id){
		return cPortalResourceMgmtDao.getListByDesc(id);
	}
	public List vendorName(String partnerCode,String corpId){
		return cPortalResourceMgmtDao.vendorName(partnerCode, corpId);
	}

	public List getBysearchCriteria(String documentType, String documentName, String fileFileName, String originCountry, String destinationCountry, String billToCode, String billToName,String corpID ) {
		// TODO Auto-generated method stub
		return cPortalResourceMgmtDao.getBysearchCriteria(documentType,documentName,fileFileName,originCountry,destinationCountry,billToCode,billToName,corpID);
	}
	public List findCPortalDocument(String partnerCode,String documentType ,String documentName,String fileFileName,String language,String infoPackage,String corpId ){
		return cPortalResourceMgmtDao.findCPortalDocument(partnerCode,documentType,documentName,fileFileName,language,infoPackage,corpId);
	}
	 public List accountportalResourceMgmtList(String partnerCode,String corpId){
		 return cPortalResourceMgmtDao.accountportalResourceMgmtList(partnerCode,corpId) ;
	 }
	 public List FindsearchCriteria(String documentType,String documentName,String fileFileName,String originCountry,String destinationCountry,String billToCode,String corpID){
		 return cPortalResourceMgmtDao.FindsearchCriteria(documentType,documentName,fileFileName,originCountry,destinationCountry,billToCode,corpID); 
	 }

	public List <CPortalResourceMgmt>resourceExtracts(String documentType, String sessionCorpID) {
		return cPortalResourceMgmtDao.resourceExtracts(documentType, sessionCorpID);
	}
	public List getChildList(String partnerCode,String corpId){
		return cPortalResourceMgmtDao.getChildList(partnerCode,corpId);
	}
	public List getDocumentLocation(String corpID,String language,String jobType){
		return cPortalResourceMgmtDao.getDocumentLocation(corpID,language,jobType);
	}
	
}