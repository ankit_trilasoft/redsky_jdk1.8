<%@ include file="/common/taglibs.jsp"%>  

<head>   
    <title><fmt:message key="costingList.title"/></title>   
    <meta name="heading" content="<fmt:message key='costingList.heading'/>"/>   
</head>   
  <!-- By Madhu -->
  <s:hidden name="ServiceOrderID" value="<%=request.getParameter("id")%>"/>
  <c:set var="ServiceOrderID" value="<%=request.getParameter("id")%>" scope="session"/>
  <!--  -->  
<c:set var="buttons">  

    <c:if test="${billingFlag==1}">
	     <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editCosting.html?sid=${ServiceOrderID}"/>'"  
        value="<fmt:message key="button.add"/>"/>   
	</c:if>
    <c:if test="${billingFlag==0}">
	     <input type="button" class="cssbuttonA" style="width:55px; height:25px" 
	      onclick="showMessage();" value="<fmt:message key="button.add"/>"/>   
	</c:if>
	
</c:set>   

      
<s:form id="costingList1" name="costingList1" method="post" validate="true"> 
<div id="newmnav">
  <ul>
  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
  </sec-auth:authComponent>
  <li><a href="servicePartners.html?id=${serviceOrder.id}"><span>Partner</span></a></li>
  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
  </sec-auth:authComponent>
  <li><a href="containers.html?id=${serviceOrder.id}"><span>Container</span></a></li>
  
  <c:if test="${serviceOrder.job !='INT'}">
  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
  </c:if>
  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
    <c:if test="${serviceOrder.job =='INT'}">
     <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
    </c:if>
    </sec-auth:authComponent>
  <c:if test="${serviceOrder.job =='RLO'}">
  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
  </c:if>
  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
  <li id="newmnav1" style="background:#FFF" ><a class="current"><span>Invoice<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  <configByCorp:fieldVisibility componentId="component.standard.claimTab">
   <c:if test="${serviceOrder.job !='RLO'}">
  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
  </c:if>
  </configByCorp:fieldVisibility>
  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
</ul>
		</div><div class="spn">&nbsp;</div><br>
 
<table class="mainDetailTable" cellspacing="1" cellpadding="0" border="0">
	<tbody>
	<tr><td align="left" class="listwhitebox">
		<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr><td align="left" height="5px"></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.shipper"/></td><td align="left" colspan="2"><s:textfield name="serviceOrder.firstName" size="15"  cssClass="input-textUpper" readonly="true"/><td align="left" ><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper" size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.originCountry"/></td><td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper" size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.originCountry" cssClass="input-textUpper"  size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.Type"/></td><td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.commodity"/></td><td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"  size="3" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.routing"/></td><td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper" size="1" readonly="true"/></td></tr>
		  	<tr><td align="right">&nbsp;&nbsp;<fmt:message key="billing.jobNo"/></td><td align="left"><s:textfield name="customerFileNumber" cssClass="input-textUpper" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="9" readonly="true"/></td><td align="right"><fmt:message key="billing.registrationNo"/></td><td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.destination"/></td><td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"  size="11" readonly="true"/></td><td align="left"><s:textfield name="serviceOrder.destinationCountry" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.mode"/></td><td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper" size="1" readonly="true"/></td><td align="right">&nbsp;&nbsp;<fmt:message key="billing.AccName"/></td><td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper" size="21" readonly="true"/></td></tr>
		  	<tr><td align="left" height="5px"></td></tr>
   		  </tbody>
	  </table>
	  </td></tr>
	</tbody>
 </table>

 
<table cellspacing="0" cellpadding="0" border="0" width="700px">
	<tbody>
	<tr><td height="10px"></td></tr>
	<tr><td>
   <display:table name="costings" class="table" requestURI="" id="costingList" defaultsort="2" export="true" pagesize="10">   
   
    <display:column property="billToCode" sortable="true" titleKey="costing.billToCode" url="/editCosting.html" paramId="id" paramProperty="id"/>
    <display:column property="invoiceNumber" sortable="true" titleKey="costing.invoiceNumber"/>
    <display:column property="description" sortable="true" titleKey="costing.description" maxLength="20"/>
    <display:column property="expPayeeType" sortable="true" titleKey="receivable.type"/>
    <display:column property="invoiceDate" sortable="true" titleKey="costing.invoiceDate" format="{0,date,dd-MMM-yyyy}"/> 
    <display:column property="amount" sortable="true" titleKey="costing.ActAmount"/>
    <display:column property="expTotalInvoice" sortable="true" titleKey="costing.expActAmount"/>
    
    <display:setProperty name="paging.banner.item_name" value="costing"/>   
    <display:setProperty name="paging.banner.items_name" value="Invoice"/>   
  
    <display:setProperty name="export.excel.filename" value="Receivable List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Receivable List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Receivable List.pdf"/>   
</display:table>   
  </td></tr></tbody></table>
  
<c:out value="${buttons}" escapeXml="false" />   
</s:form>  

<script type="text/javascript">
function showMessage()
{
	alert("Billing not created.");
}
</script>

<script type="text/javascript">   
    highlightTableRows("costingList");   
</script>  
