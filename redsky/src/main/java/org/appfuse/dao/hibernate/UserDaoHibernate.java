package org.appfuse.dao.hibernate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.struts2.ServletActionContext;
import org.appfuse.dao.UserDao;
import org.appfuse.model.User;
import org.hibernate.Query;
import org.springframework.orm.ObjectRetrievalFailureException;

import com.trilasoft.app.dao.hibernate.DataSecuritySetDaoHibernate.UserDTO;

/**
 * This class interacts with Spring's HibernateTemplate to save/delete and
 * retrieve User objects.
 *
 * <p><a href="UserDaoHibernate.java.html"><i>View Source</i></a></p>
 *
 * @author <a href="mailto:matt@raibledesigns.com">Matt Raible</a>
 *   Modified by <a href="mailto:dan@getrolling.com">Dan Kibler</a>
 *   Extended to implement Acegi UserDetailsService interface by David Carter david@carter.net
*/
public class UserDaoHibernate extends BaseDaoHibernate implements UserDao, UserDetailsService {
    /**
     * @see org.appfuse.dao.UserDao#getUser(Long)
     */
    public User getUser(Long userId) {
    	
        User user = (User) getHibernateTemplate().get(User.class, userId);
        if(user!=null){
        getHibernateTemplate().getSessionFactory().getCurrentSession().refresh(user);
        }

        if (user == null) {
            log.warn("uh oh, user '" + userId + "' not found...");
            throw new ObjectRetrievalFailureException(User.class, userId);
        }

        return user;
    }
    protected HttpServletRequest getRequest() {
        return ServletActionContext.getRequest();  
    }
    /**
     * @see org.appfuse.dao.UserDao#getUsers(org.appfuse.model.User)
     */
    public List getUsers(User user) {
        return getHibernateTemplate().find("from User u where u.userType='User' and enabled=true order by upper(u.username) ");
    }

    /**
     * @see org.appfuse.dao.UserDao#saveUser(org.appfuse.model.User)
     */
    public void saveUser(final User user) {
        if (log.isDebugEnabled()) {
            log.debug("user's id: " + user.getId());
        }
        
        getHibernateTemplate().saveOrUpdate(user);
        // necessary to throw a DataIntegrityViolation and catch it in UserManager
        getHibernateTemplate().flush();
    }

    /**
     * @see org.appfuse.dao.UserDao#removeUser(Long)
     */
    public void removeUser(Long userId) {
        getHibernateTemplate().delete(getUser(userId));
    }

    /** 
    * @see org.acegisecurity.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
    */
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        List users = getHibernateTemplate().find("from User where username=?", username);
        if (users == null || users.isEmpty()) {
        	Long userId=null;
        	List list= this.getSession().createSQLQuery("select id from app_user where username='"+username+"' ").list();
    		if(list!=null && (!(list.isEmpty())) && list.get(0) !=null && (!(list.get(0).toString().trim().equals("")))) { 
    			userId=Long.parseLong(list.get(0).toString().trim());
    			if(userId!=null) {
    				return (UserDetails) getUser(userId);
    			}else {
    				System.out.println("user name "+username);
    	            throw new UsernameNotFoundException("user '" + username + "' not found...");
    			}
    		}else {
        	System.out.println("user name "+username);
            throw new UsernameNotFoundException("user '" + username + "' not found...");
    		}
        } else {
            return (UserDetails) users.get(0);
        }
    }
    public List findUserRoles(String corpID){
    	return getHibernateTemplate().find("select name from Role where corpID=? order by name",corpID);
    }
    
    public List findUserRoles(String corpID,String category){
    	List returnList = this.getSession().createSQLQuery("select name from role where corpid='" + corpID + "' and (category like '%" + category +"%' or category ='' or category is null) order by name").list();
    		return returnList;
    }
    
    public List findUserPassword(String corpID){
    return getHibernateTemplate().find("select password FROM User a where username='sangeeta' and password_hint='development'and email='sdwivedi@trilasoft.com'",corpID);
    }
    public List checkById(Long id) {
		return getHibernateTemplate().find("select id from Claim where id=?",id);
}
public List findUserDetails(String userName){
	return getHibernateTemplate().find("from User where username=?",userName);
}
public void resetPassword(String passwordNew,String confirmPasswordNew,String pwdHintQues,String pwdHint,String userName){
	getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() , password = '"+ passwordNew +"', confirmPassword = '"+ confirmPasswordNew +"', passwordHintQues = '"+ pwdHintQues +"', passwordHint = '"+ pwdHint +"',passwordReset = false where username=?",userName);
}
public void findPwordHintQ(String pwdHintQues,String userName){
	getHibernateTemplate().find("from User where  passwordHintQues = '"+ pwdHintQues +"' and username=?",userName);

}
public void forgotPassword(String pwdHintQues,String pwdHint,String userName){
 getHibernateTemplate().find("from User where passwordHintQues = '"+ pwdHintQues +"', passwordHint = '"+ pwdHint +"'  username=?",userName);
		}

public void removeUserPhotograph(String location,String userName){
	getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() , location ='' where username=?",userName);
	}

public List getDateSecuritySet() {
	return getHibernateTemplate().find("from DataSecuritySet");
}

public List getPermissionList() {
	return getHibernateTemplate().find("select name from DataSecuritySet order by name");
}

public List getPermission(String permissionName) {
	return getHibernateTemplate().find("from DataSecuritySet where name='"+permissionName+"'");
}

public List getSuperVisor(String corpID) {
	return this.getSession().createSQLQuery("SELECT upper(username) FROM app_user where corpID='"+corpID+"' and userType='USER' and account_enabled=true order by alias").list();
}

public List getPartnerExpirationDate(String sessionCorpID, String username) {
	List partnerExpiryDuration=getHibernateTemplate().find("select partnerPasswordExpiryDuration from Company where corpID='"+sessionCorpID+"'");
	return partnerExpiryDuration;
}

public void updatePasswordExpiryDate(String username, String sessionCorpID, Date compareWithDate) {
	String beginDtNew="";
	try {
	 	SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
        StringBuilder nowYYYYMMDD = new StringBuilder( dateformatYYYYMMDD.format(compareWithDate) );
        beginDtNew = nowYYYYMMDD.toString();
        
 	}catch (Exception e) {
		
		e.printStackTrace();
	}
	getHibernateTemplate().bulkUpdate("update User set pwdexpiryDate='"+beginDtNew+"' where username='"+username+"' and corpID='"+sessionCorpID+"'");
}

public void setExpirePassword(String username, String sessionCorpID) {
	getHibernateTemplate().bulkUpdate("update User set credentialsExpired=true where username='"+username+"' and corpID='"+sessionCorpID+"'");
}

public void updatePassword(String passwordNew, Long id) {
	getHibernateTemplate().bulkUpdate("Update User set updatedBy='"+getRequest().getRemoteUser()+"',updatedOn = now() , password = '"+ passwordNew +"', confirmPassword = '"+ passwordNew +"' ,passwordReset = true where id=?",id);	
}

public void updateUserNewsUpdateFlag(String corpid,Boolean flag,String user) {
	if(!flag){ 
		Query query=this.getSession().createSQLQuery("update app_user set newsUpdateFlag=false where corpID='"+corpid+"' and username='"+user+"'");
		query.executeUpdate();
	}
	if(flag){
		try{ 
			if(corpid.equals("TSFT")){
				Query query=this.getSession().createSQLQuery("update app_user set newsUpdateFlag=true" );
				query.executeUpdate();	
			}
			else{
				Query query=this.getSession().createSQLQuery("update app_user set newsUpdateFlag=true where corpID='"+corpid+"'");
				query.executeUpdate();
				
			}
			//Query query=this.getSession().createSQLQuery("update app_user set newsUpdateFlag=true where corpID='"+corpid+"'");
			//query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}

public List findDateDiff(Date pwdexpiryDate, Date compareWithDate) {
	Date date1Str = pwdexpiryDate;
	Date date2Str = compareWithDate;
	SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
	StringBuilder formatedDate1 = new StringBuilder(dateformatYYYYMMDD1.format(date1Str));
	SimpleDateFormat dateformatYYYYMMDD2 = new SimpleDateFormat("yyyy-MM-dd");
	StringBuilder formatedDate2 = new StringBuilder(dateformatYYYYMMDD2.format(date2Str));
 	
 	List getDiff= this.getSession().createSQLQuery("select datediff('"+formatedDate1+"', '"+formatedDate2+"')").list();
	//System.out.println("\n\n\n\n\n getDiff--->>>"+getDiff);
		return getDiff;
}

public List getFromEmailAddress(String corpID) {
	return getHibernateTemplate().find("select email from SystemDefault where corpID='"+corpID+"' ");
}

public List findUserByUsername(String coordinator) {
	return this.getSession().createSQLQuery("select alias from app_user where username='"+coordinator+"' and username not like 'admin%' order by alias").list();
}
public List getAllUserList(String corpID){
	return this.getSession().createSQLQuery("select distinct upper(a.username) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=true and r.name in ('ROLE_BILLING','ROLE_SALE','ROLE_AUDITOR','ROLE_COORD','ROLE_PAYABLE','ROLE_PRICING','ROLE_EXECUTIVE','ROLE_RECEIVABLE','ROLE_CLAIM_HANDLER','ROLE_FORWARDER','ROLE_INTERNAL_BILLING','ROLE_OPS') and a.userType='USER' and a.corpID='"+corpID+"' order by upper(a.username) ASC").list();
}
public List getAllUserRole(String userList,String sessionCorpID,String activeStatus){
	String usrList="";
	if(userList!=null && !userList.equalsIgnoreCase(""))
	{
		if(userList.indexOf(",")>-1)
		{
		String []userArr=userList.split(",");
		for(int i=0;i<userArr.length;i++)
		{
			if(usrList.equalsIgnoreCase(""))
			{
				usrList="'"+userArr[i]+"'";
			}else{
				usrList=usrList+",'"+userArr[i]+"'";
			}
		}
		}else{
			usrList="'"+userList+"'";
		}
	}
	List al=new ArrayList();
	if(activeStatus.equalsIgnoreCase("false"))
	{
	if(!usrList.equalsIgnoreCase(""))
	{
	al=this.getSession().createSQLQuery("select distinct upper(r.name) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=true and r.name in ('ROLE_BILLING','ROLE_SALE','ROLE_AUDITOR','ROLE_COORD','ROLE_PAYABLE','ROLE_PRICING','ROLE_EXECUTIVE','ROLE_RECEIVABLE','ROLE_CLAIM_HANDLER','ROLE_FORWARDER','ROLE_INTERNAL_BILLING','ROLE_OPS') and a.corpID='"+sessionCorpID+"' and a.userType='USER' and a.username in ("+usrList+") order by upper(r.name) ASC").list();
	}
}
	else
	{if(!usrList.equalsIgnoreCase(""))
	{
	al=this.getSession().createSQLQuery("select distinct upper(r.name) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=false and r.name in ('ROLE_BILLING','ROLE_SALE','ROLE_AUDITOR','ROLE_COORD','ROLE_PAYABLE','ROLE_PRICING','ROLE_EXECUTIVE','ROLE_RECEIVABLE','ROLE_CLAIM_HANDLER','ROLE_FORWARDER','ROLE_INTERNAL_BILLING','ROLE_OPS') and a.corpID='"+sessionCorpID+"' and a.userType='USER' and a.username in ("+usrList+") order by upper(r.name) ASC").list();
	}}
	return al;
}
public List pickAllUser(String roleList,String sessionCorpID,String userList,String activeStatus){
	String rolList="";
	if(roleList!=null && !roleList.equalsIgnoreCase(""))
	{
		if(roleList.indexOf(",")>-1)
		{
		String []roleArr=roleList.split(",");
		for(int i=0;i<roleArr.length;i++)
		{
			if(rolList.equalsIgnoreCase(""))
			{
				rolList="'"+roleArr[i]+"'";
			}else{
				rolList=rolList+",'"+roleArr[i]+"'";
			}
		}
		}else{
			rolList="'"+roleList+"'";
		}
	}
	String usrList="";
	if(userList!=null && !userList.equalsIgnoreCase(""))
	{
		if(userList.indexOf(",")>-1)
		{
		String []userArr=userList.split(",");
		for(int i=0;i<userArr.length;i++)
		{
			if(usrList.equalsIgnoreCase(""))
			{
				usrList="'"+userArr[i]+"'";
			}else{
				usrList=usrList+",'"+userArr[i]+"'";
			}
		}
		}else{
			usrList="'"+userList+"'";
		}
	}	
	List al=new ArrayList();
	
	if(!rolList.equalsIgnoreCase(""))
	{
	al=this.getSession().createSQLQuery("select distinct upper(a.username) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=true and a.corpID='"+sessionCorpID+"' and a.userName NOT IN ("+usrList+") and a.userType='USER' order by upper(a.username) ASC").list();
	}
	
	
	return al;
}
public int updateUserWithRoles(String transferFrom,String roleTransfer,String transferTo,String sessionCorpID){
	String roleToAlert="";
	String code=""; 
	if(!roleTransfer.equalsIgnoreCase(""))
	{
		if(roleTransfer.equalsIgnoreCase("ROLE_BILLING"))
		{
			roleToAlert="Billing";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set personBilling='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where personBilling='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.personBilling='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){ 
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set personBilling='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and personBilling='"+transferFrom+"'").executeUpdate();
            }
			List partnerId=getSession().createSQLQuery("select id from partnerprivate where billingUser='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
			if(partnerId!=null && (!(partnerId.isEmpty()))){ 
            	code= partnerId.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set billingUser='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and billingUser='"+transferFrom+"'");
            }
            //getSession().createSQLQuery("Update billing b,serviceorder s set b.personBilling='"+transferTo+"' where                                                            ='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_AUDITOR"))
		{
			roleToAlert="Auditor";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set auditor='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where auditor='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.auditor='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set auditor='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and auditor='"+transferFrom+"'").executeUpdate();
            }
            List partnerId=getSession().createSQLQuery("select id from partnerprivate where auditor='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
			if(partnerId!=null && (!(partnerId.isEmpty()))){ 
            	code= partnerId.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set auditor='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and auditor='"+transferFrom+"'");
            }

            //getSession().createSQLQuery("Update billing b,serviceorder s set b.auditor='"+transferTo+"' where b.auditor='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_PAYABLE"))
		{
			roleToAlert="Payable";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set personPayable='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where personPayable='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b,serviceorder s	where b.id=s.id and b.personPayable='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set personPayable='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and personPayable='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPayable='"+transferTo+"' where b.personPayable='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
			}
            List partnerId=getSession().createSQLQuery("select id from partnerprivate where payableUser='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
            if(partnerId!=null && (!(partnerId.isEmpty()))){ 
            	code= partnerId.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set payableUser='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and payableUser='"+transferFrom+"'");
            }

			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_PRICING"))
		{
			roleToAlert="Pricing";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set personPricing='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where personPricing='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.personPricing='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set personPricing='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and personPricing='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }
            List partnerId=getSession().createSQLQuery("select id from partnerprivate where pricingUser='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
			if(partnerId!=null && (!(partnerId.isEmpty()))){ 
            	code= partnerId.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set pricingUser='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and pricingUser='"+transferFrom+"'");
            }

			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_COORD"))
		{
			roleToAlert="Coordinator";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set coordinator='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where coordinator='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			getHibernateTemplate().bulkUpdate("Update ServiceOrder set coordinator='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where coordinator='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD')");			
			List partnerId1=getSession().createSQLQuery("select id from partnerprivate where coordinator='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
			if(partnerId1!=null && (!(partnerId1.isEmpty()))){ 
            	String code1= partnerId1.toString().replace("[","").replace("]", "");
            	
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set coordinator='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code1+") and coordinator='"+transferFrom+"'");
            }
			List partnerId2=getSession().createSQLQuery("select id from partnerprivate where accountManager='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();
			if(partnerId2!=null && (!(partnerId2.isEmpty()))){ 
            	String code2= partnerId2.toString().replace("[","").replace("]", "");
            	
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set accountManager='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code2+") and accountManager='"+transferFrom+"'");
            }

			//getSession().createSQLQuery("update partnerprivate set coordinator='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where corpid='"+sessionCorpID+"' and coordinator='"+transferFrom+"'").executeUpdate();
			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_SALE"))
		{
			roleToAlert="Salesman";
			try{
			getHibernateTemplate().bulkUpdate("Update ServiceOrder set salesMan='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where salesMan='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD')");			
			List partnerId2=getSession().createSQLQuery("select id from partnerprivate where accountHolder='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();

			if(partnerId2!=null && (!(partnerId2.isEmpty()))){ 
            	code= partnerId2.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set accountHolder='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and accountHolder='"+transferFrom+"'");
            }

			}catch(Exception e){
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_EXECUTIVE"))
		{
			roleToAlert="Pricing";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set approvedBy='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where approvedBy='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.approvedBy='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set approvedBy='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and approvedBy='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_RECEIVABLE"))
		{
			roleToAlert="Pricing";
			try{
			//getHibernateTemplate().bulkUpdate("Update CustomerFile set approvedBy='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where approvedBy='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','HOLD')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.personReceivable='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set personReceivable='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and personReceivable='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_CLAIM_HANDLER"))
		{
			roleToAlert="Pricing";
			try{
			//getHibernateTemplate().bulkUpdate("Update CustomerFile set approvedBy='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where approvedBy='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','HOLD')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.claimHandler='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set claimHandler='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and claimHandler='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }
            List partnerId2=getSession().createSQLQuery("select id from partnerprivate where claimsUser='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();

			if(partnerId2!=null && (!(partnerId2.isEmpty()))){ 
            	code= partnerId2.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set claimsUser='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and claimsUser='"+transferFrom+"'");
            }
			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_FORWARDER"))
		{
			roleToAlert="Pricing";
			try{
			//getHibernateTemplate().bulkUpdate("Update CustomerFile set approvedBy='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where approvedBy='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','HOLD')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.personForwarder='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set personForwarder='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and personForwarder='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_INTERNAL_BILLING"))
		{
			roleToAlert="Pricing";
			try{
			getHibernateTemplate().bulkUpdate("Update CustomerFile set internalBillingPerson='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where internalBillingPerson='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD','CLOSED')");
			List generateId=getSession().createSQLQuery("select b.id from billing b, serviceorder s	where b.id=s.id and b.internalBillingPerson='"+transferFrom+"' and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD')").list();
            if(generateId!=null && (!(generateId.isEmpty()))){
            	code= generateId.toString().replace("[","").replace("]", "");
            	getSession().createSQLQuery("update billing set internalBillingPerson='"+transferTo+"', updatedby='"+getRequest().getRemoteUser()+"',updatedon=now() where id in("+code+") and internalBillingPerson='"+transferFrom+"'").executeUpdate();
            
			
			//getSession().createSQLQuery("Update billing b,serviceorder s set b.personPricing='"+transferTo+"' where b.personPricing='"+transferFrom+"' and b.id=s.id and b.corpId='"+sessionCorpID+"' and s.status not in ('CNCL','CLSD','HOLD')").executeUpdate();
            }
            List partnerId2=getSession().createSQLQuery("select id from partnerprivate where internalBillingPerson='"+transferFrom+"' and  corpid='"+sessionCorpID+"'").list();

			if(partnerId2!=null && (!(partnerId2.isEmpty()))){ 
            	code= partnerId2.toString().replace("[","").replace("]", "");
            	getHibernateTemplate().bulkUpdate("Update PartnerPrivate set internalBillingPerson='"+transferTo+"' , updatedBy='"+getRequest().getRemoteUser()+"',updatedOn=now()  where id in ("+code+") and internalBillingPerson='"+transferFrom+"'");
            }

			}catch(Exception e){
				
			}
		}
		if(roleTransfer.equalsIgnoreCase("ROLE_OPS"))
		{
			roleToAlert="Salesman";
			try{
			getHibernateTemplate().bulkUpdate("Update ServiceOrder set opsPerson='"+transferTo+"' , updatedby='"+getRequest().getRemoteUser()+"',updatedon=now()  where opsPerson='"+transferFrom+"' and corpId='"+sessionCorpID+"' and status not in ('CNCL','CLSD')");			
			}catch(Exception e){
			}
		}
		
		
		try{
			getHibernateTemplate().bulkUpdate("Update ToDoResult set owner='"+transferTo+"' where owner='"+transferFrom+"' and corpID='"+sessionCorpID+"'");
			if(!roleToAlert.equals(""))
			getHibernateTemplate().bulkUpdate("Update AuditTrail set isViewed=true where alertUser='"+transferFrom+"' and alertRole ='"+roleToAlert+"' and UPPER(user)<>UPPER(alertUser) and corpID='"+sessionCorpID+"'");
			else getHibernateTemplate().bulkUpdate("Update AuditTrail set isViewed=true where alertUser='"+transferFrom+"' and UPPER(user)<>UPPER(alertUser) and corpID='"+sessionCorpID+"'");
			}catch(Exception e){				
			}
	}	
	return 0;
}

// query changed as per # 6581
public List getAgentsUsersList(String partnerCode, String sessionCorpID) {
	String condition=" and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	String condition1=" and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	List newTempParent=new ArrayList();	
	String tempPartnerCode="";
	List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid in ('TSFT','"+sessionCorpID+"')").list();
	
	if(tempParent.size()>0){
		Iterator it=tempParent.iterator();
		while(it.hasNext()){
			if(tempPartnerCode.equals("")){
				tempPartnerCode = "'"+it.next().toString()+"'";
			}else{
				tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
			}
		}
	}
	String partnerCodes="";
	if(tempPartnerCode.equals("")){
		partnerCodes ="'"+partnerCode+"'";
	}else{
		partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
	}
	String query = "Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact " +
			" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
			"datasecurityfilter dsf, datasecuritypermission dsp " +
			"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
			""+condition+" "+ 
			"and dsf.id=dsp.datasecurityfilter_id " +
			"and dss.id=dsp.datasecurityset_id  " +
			"and dsf.filtervalues in("+partnerCodes+")" +
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			
			"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact " +
			"from app_user ap, user_role u, role r, companydivision c " +
			"where ap.corpid=c.corpid " +
			"and ap.id= u.user_id " +
			""+condition1+" "+ 
			"and u.role_id= r.id " +
			"and c.bookingagentcode in("+partnerCodes+")" +
			"and (r.name='ROLE_COORD')" +
			
			"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled ,ap.userType,ap.NPSscore,ap.relocationContact  " +
			"from app_user ap where ap.basedAt in("+partnerCodes+")"+
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			"AND ap.contact is true AND ap.account_enabled is false order by 1 ";
			List agentUsers= this.getSession().createSQLQuery(query).list();
		List result= new ArrayList(); 
	
	if(agentUsers != null && agentUsers.size()>0){
		return getConvertObjectToModel(agentUsers);
	}
	
	return result;
}
	public List getContactListByPartnerCode(String partnerCode,String corpID){
		String condition=" and ( userType='PARTNER' or userType='AGENT') and relocationContact is true  and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		String condition1=" and account_enabled is true and relocationContact is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		List newTempParent=new ArrayList();	
		String tempPartnerCode="";
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid='"+corpID+"'").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+partnerCode+"'";
		}else{
			partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
		}
		String query = "Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore ,ap.relocationContact " +
				" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
				"datasecurityfilter dsf, datasecuritypermission dsp " +
				"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
				""+condition+" "+ 
				"and dsf.id=dsp.datasecurityfilter_id " +
				"and dss.id=dsp.datasecurityset_id and ap.parentAgent =  dsf.filtervalues " +
				"and dsf.filtervalues in("+partnerCodes+")" +
				"and ap.corpID in ('TSFT','"+corpID+"') " +
				"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore ,ap.relocationContact " +
				"from app_user ap, user_role u, role r, companydivision c " +
				"where ap.corpid=c.corpid " +
				"and ap.id= u.user_id " +
				""+condition1+" "+ 
				"and u.role_id= r.id " +
				"and c.bookingagentcode in("+partnerCodes+")" +
				"and (r.name='ROLE_COORD')" +
				"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled ,ap.userType,ap.NPSscore ,ap.relocationContact " +
				"from app_user ap where ap.basedAt in("+partnerCodes+")"+
				"and ap.corpID in ('TSFT','"+corpID+"') " +
				"AND ap.contact is true and ap.relocationContact is true AND ap.account_enabled is false order by 1 ";
				List agentUsers= this.getSession().createSQLQuery(query).list();
			List result= new ArrayList(); 
		
		if(agentUsers != null && agentUsers.size()>0){
			return getConvertObjectToModel(agentUsers);
		}
		
		return result;
 }
	public List getSearchContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId){
		String condition=" and ( userType='PARTNER' or userType='AGENT') and relocationContact is true and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		String condition1=" and account_enabled is true and relocationContact is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		List newTempParent=new ArrayList();	
		String tempPartnerCode="";
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid='"+corpID+"'").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+partnerCode+"'";
		}else{
			partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
		}
		List agentUsers= this.getSession().createSQLQuery("Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact " +
				" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
				"datasecurityfilter dsf, datasecuritypermission dsp " +
				"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
				""+condition+" "+ 
				"and dsf.id=dsp.datasecurityfilter_id " +
				"and dss.id=dsp.datasecurityset_id and ap.parentAgent =  dsf.filtervalues " +
				"and dsf.filtervalues in("+partnerCodes+")" +
				"and ap.corpID in ('TSFT','"+corpID+"') " +
				"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
				"and email like '" + emailId + "%'" +
				"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled ,ap.userType,ap.NPSscore,ap.relocationContact " +
				" from app_user ap, user_role u, role r, companydivision c " +
				"where ap.corpid=c.corpid " +
				"and ap.id= u.user_id " +
				"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
				"and email like '" + emailId + "%'" +
				""+condition1+" "+ 
				"and u.role_id= r.id " +
				"and c.bookingagentcode in("+partnerCodes+")" +
				"and (r.name='ROLE_COORD')" +
				"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType ,ap.NPSscore,ap.relocationContact " +
				" from app_user ap where ap.basedAt in("+partnerCodes+")"+
				"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
				"and email like '" + emailId + "%'" +
				"and ap.corpID in ('TSFT','"+corpID+"') " +
				"AND ap.contact is true and ap.relocationContact is true AND ap.account_enabled is false order by 1 ").list();
		
			List result= new ArrayList(); 
		
		if(agentUsers != null && agentUsers.size()>0){
			return getConvertObjectToModel(agentUsers);
		}
		
		return result;
  }
public List getAllUserContactListByPartnerCode(String partnerCode,String corpID) {
	return getAgentsUsersList(partnerCode, corpID);
}

/* Modified by Kunal Kumar Sharma for ticket number: 6838 */
public List getAllUserContactListByPartnerCode(String partnerCode,String corpID,String name, String emailId) {
	List list = getAgentsUsersList(partnerCode, corpID, name, emailId);
	return list;
}

public List getAgentsUsersList(String partnerCode, String sessionCorpID,String name, String emailId) {
	String condition=" and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	String condition1=" and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	List newTempParent=new ArrayList();	
	String tempPartnerCode="";
	List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid in ('TSFT','"+sessionCorpID+"')").list();
	if(tempParent.size()>0){
		Iterator it=tempParent.iterator();
		while(it.hasNext()){
			if(tempPartnerCode.equals("")){
				tempPartnerCode = "'"+it.next().toString()+"'";
			}else{
				tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
			}
		}
	}
	String partnerCodes="";
	if(tempPartnerCode.equals("")){
		partnerCodes ="'"+partnerCode+"'";
	}else{
		partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
	}
	List agentUsers= this.getSession().createSQLQuery("Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact " +
			" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
			"datasecurityfilter dsf, datasecuritypermission dsp " +
			"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
			""+condition+" "+ 
			"and dsf.id=dsp.datasecurityfilter_id " +
			"and dss.id=dsp.datasecurityset_id " +
			"and dsf.filtervalues in("+partnerCodes+")" +
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
			"and email like '" + emailId + "%'" +
			
		"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled ,ap.userType,ap.NPSscore,ap.relocationContact " +
			" from app_user ap, user_role u, role r, companydivision c " +
			"where ap.corpid=c.corpid " +
			"and ap.id= u.user_id " +
			"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
			"and email like '" + emailId + "%'" +
			""+condition1+" "+ 
			"and u.role_id= r.id " +
			"and c.bookingagentcode in("+partnerCodes+")" +
			"and (r.name='ROLE_COORD')" +
			"union Select distinct ap.id,  ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact " +
			" from app_user ap where ap.basedAt in("+partnerCodes+")"+
			"and concat(ap.first_name,' ',ap.last_name) like '%" + name + "%'" +
			"and email like '" + emailId + "%'" +
			"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
			"AND ap.contact is true AND ap.account_enabled is false order by 1 ").list();
	
		List result= new ArrayList(); 
	
	if(agentUsers != null && agentUsers.size()>0){
		return getConvertObjectToModel(agentUsers);
	}
	
	return result;
}

/* Modification Ends Here for ticket number: 6838 */


	public List getAllUserContactListByPartnerCode1(String partnerCode,String corpID) {
//		String query ="from User where parentAgent='"+partnerCode+"' and corpID in ('TSFT','"+corpID+"') and userType='AGENT'";
//		List user =  getHibernateTemplate().find(query);
		
		String query1= "select id,  first_Name,last_Name,jobFunction,userTitle,email,phone_number from app_user where parentAgent='"+partnerCode+"' and corpID in ('TSFT','"+corpID+"') and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		List user1 = getSession().createSQLQuery(query1).list();
		return getConvertObjectToModel(user1);
	}
	
	private List<User> getConvertObjectToModel(List list1){
		List<User> itemList = new ArrayList<User>();
		Iterator it=list1.iterator();
		while(it.hasNext()){
			User u = new User();
	           Object []row= (Object [])it.next();
	           Long id = Long.parseLong(row[0].toString());
	           u.setId(id);
	           if(row[1] != null){
	        	   u.setFirstName(row[1].toString());
	           }
	           if(row[2] != null){
	        	   u.setLastName(row[2].toString());
	           }
	           if(row[3] != null){
	        	   u.setJobFunction(row[3].toString());
	           }
	           if(row[4] != null){
	        	   u.setUserTitle(row[4].toString());
	           }
	           if(row[5] != null){
	        	   u.setEmail(row[5].toString());
	           }
	           if(row[6] != null){
	        	   u.setPhoneNumber(row[6].toString());
	           }
	           if(row[7] != null){
	        	   u.setEnabled((Boolean) row[7]);
	           }
	           if(row[8] != null){
	        	   u.setUserType( row[8].toString());
	           }
	           if(row[9] != null){
	        	   u.setNPSscore((BigDecimal)row[9]);
	           }
	           if(row[10] != null){
	        	   u.setRelocationContact((Boolean) row[10]);
	           }
	           itemList.add(u);
		}
		return itemList;
	}

	public List findCoordinatorByUsername(String coordinator,String sessionCorpID) {
		return this.getSession().createSQLQuery("select networkCoordinator from app_user where username='"+coordinator+"' and corpID ='"+sessionCorpID+"' ").list();
	}

	public List findCoordinatorByNetworkCoordinator(String coordinator, String corpID) {
		return this.getSession().createSQLQuery("select username from app_user where networkCoordinator='"+coordinator+"' and corpID ='"+corpID+"' ").list();
	}
	
	
	public String doNotEmailFlag(String email) {
		try {
			List al = getHibernateTemplate()
					.find("SELECT doNotEmail from User where doNotEmail is true and email=?",
							email);
			if (al != null && !al.isEmpty() && (Boolean) al.get(0)) {
				return "YES";
			}
		} catch (Exception e) {
			logger.error("Error getting doNotEmail, returning false");
			e.printStackTrace();
		}
		logger.error("Returning doNotEmail: NO");
		return "NO";
	}
	public String findToolTip(String username, String corpID){
		String str="";
		List list= this.getSession().createSQLQuery("select loginFailureReason from app_user where username='"+username+"'").list();
		if(list!=null && !(list.isEmpty())&& (list.get(0)!=null)){
			str=list.get(0).toString();
		}
		return str;
	}
	
	public void updateUserLoginFailureReason(String userName,String chng,String chng1){
		System.out.println("\n\n\n\n\nupdating user  "+userName);
		
		if(chng.equals("")){
			String str[]=chng1.split("  ");		
			//String dtt=fm.format(curr);
			String dt=str[0];
			SimpleDateFormat fm=new SimpleDateFormat("yyyy-mm-dd HH:mm:ss") ;
			Date dttt;
			try {
				dttt = fm.parse(dt);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				dttt=new Date();
				e.printStackTrace();
			}
			String dt1=str[1];
			getSession().createSQLQuery("update app_user  set login_time='"+dt+"',login_status='"+dt1+"' ,updatedBy='"+userName+"',updatedOn = now() where username='"+userName+"'").executeUpdate();	
		}else if(chng.indexOf("Password Expired")>-1){
			String str[]=chng1.split("  ");
			String dt=str[0];
			String dt1=str[1];
			getSession().createSQLQuery("update app_user  set login_time='"+dt+"',login_status='"+dt1+"' ,updatedBy='"+userName+"',updatedOn = now() , loginFailureReason=if(loginFailureReason is null or loginFailureReason='','"+chng+"' ,concat(SUBSTRING_INDEX(loginFailureReason, ',' , -9),',','"+chng+"')) where username='"+userName+"'").executeUpdate();		
		}
		else if(chng.indexOf("Account Expired")>-1){
			String str[]=chng1.split("  ");
			String dt=str[0];
			String dt1=str[1];
			getSession().createSQLQuery("update app_user  set login_time='"+dt+"',login_status='"+dt1+"' ,updatedBy='"+userName+"',updatedOn = now() ,loginFailureReason=if(loginFailureReason is null or loginFailureReason='','"+chng+"' ,concat(SUBSTRING_INDEX(loginFailureReason, ',' , -9),',','"+chng+"')) where username='"+userName+"'").executeUpdate();		
		}
		else if(chng.indexOf("Account Locked")>-1){
			String str[]=chng1.split("  ");
			String dt=str[0];
			String dt1=str[1];
			getSession().createSQLQuery("update app_user  set login_time='"+dt+"',login_status='"+dt1+"' ,updatedBy='"+userName+"',updatedOn = now() ,loginFailureReason=if(loginFailureReason is null or loginFailureReason='','"+chng+"' ,concat(SUBSTRING_INDEX(loginFailureReason, ',' , -9),',','"+chng+"')) where username='"+userName+"'").executeUpdate();		
		}
		else{
			String str[]=chng1.split("  ");
			String dt=str[0];
			String dt1=str[1];
		    getSession().createSQLQuery("update app_user  set login_time='"+dt+"',login_status='"+dt1+"' ,updatedBy='"+userName+"',updatedOn = now() ,loginFailureReason=if(loginFailureReason is null or loginFailureReason='','"+chng+"' ,concat(SUBSTRING_INDEX(loginFailureReason, ',' , -9),',','"+chng+"'))  where username='"+userName+"'").executeUpdate();
		}
		
	}

	public List findUserDetail(String coordinator, String sessionCorpID) {
		return this.getSession().createSQLQuery("select concat((if(first_Name is null or first_Name = '','',first_Name)),'~',(if(last_Name is null or last_Name = '','',last_Name)),'~',(if(email is null or email = '','',email)),'~',(if(phone_number is null or phone_number = '','NO',phone_number))  ) from app_user where username='"+coordinator+"' ").list();
	}

	public void updateUserInfo(String updateBy, String userID,String datasecurityID) {
		// TODO Auto-generated method stub
		try{			
			 getSession().createSQLQuery("update userdatasecurity set updatedby='"+updateBy+"',updatedon=now() where user_id='"+userID+"' and userdatasecurity_id in ("+datasecurityID+")").executeUpdate();
			 }
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void updateUserRoleInfo(String updateBy, String userID,String roleID) {
		try{
			 getSession().createSQLQuery("update user_role set updatedby='"+updateBy+"',updatedon=now()  where user_id='"+userID+"' and role_id in ("+roleID+")").executeUpdate();
			 }
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getDefaultContactPersonCount(String partnerCode, String corpID){
		int getCount=0;
		try{
		List list= this.getSession().createSQLQuery("select count(*) from app_user where parentAgent='"+partnerCode+"' and isDefaultContactPerson is true and corpid='"+corpID+"'").list();
		if(list!=null && !(list.isEmpty())&& (list.get(0)!=null)){
			getCount=Integer.parseInt(list.get(0).toString());
			return getCount;
		}
		}catch(Exception e){
			e.printStackTrace();
			return getCount;
		}
		return getCount;
			}
	
	public List getUsersByParentAgent(String parentAgent) { 
		  return getHibernateTemplate().find("from User where userType='User' and enabled=true and  parentAgent ='"+parentAgent+"' order by upper(username) ");
	}
	public List getAllInactiveUserList(String corpID){
		return this.getSession().createSQLQuery("select distinct upper(a.username) from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=false and r.name in ('ROLE_BILLING','ROLE_SALE','ROLE_AUDITOR','ROLE_COORD','ROLE_PAYABLE','ROLE_PRICING','ROLE_EXECUTIVE','ROLE_RECEIVABLE','ROLE_CLAIM_HANDLER','ROLE_FORWARDER','ROLE_INTERNAL_BILLING','ROLE_OPS') and a.userType='USER' and a.corpID='"+corpID+"' order by upper(a.username) ASC").list();
	}
	public List getContactList(String partnerCode, String corpID, String name, String emailId, String phone_number) {
		List list = getContactSearchList(partnerCode, corpID, name, emailId,phone_number);
		return list;
	}
	public List getContactSearchList(String partnerCode, String sessionCorpID,String name, String emailId,String phone_number) {
		String condition=" and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		String condition1=" and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
		List newTempParent=new ArrayList();	
		String tempPartnerCode="";
		List tempParent= this.getSession().createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpid='"+sessionCorpID+"'").list();
		if(tempParent.size()>0){
			Iterator it=tempParent.iterator();
			while(it.hasNext()){
				if(tempPartnerCode.equals("")){
					tempPartnerCode = "'"+it.next().toString()+"'";
				}else{
					tempPartnerCode =tempPartnerCode+ ",'"+it.next().toString()+"'";
				}
			}
		}
		String partnerCodes="";
		if(tempPartnerCode.equals("")){
			partnerCodes ="'"+partnerCode+"'";
		}else{
			partnerCodes = "'"+partnerCode+"',"+tempPartnerCode;
		}
		String Query="";
		if(partnerCode !=null && (!(partnerCode.trim().equals("")))){
		Query="Select distinct ap.id,ap.username,ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact ,ap.basedAt" +
				" from app_user ap, userdatasecurity uds, datasecurityset  dss, " +
				"datasecurityfilter dsf, datasecuritypermission dsp " +
				"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "+
				""+condition+" "+ 
				"and dsf.id=dsp.datasecurityfilter_id " +
				"and dss.id=dsp.datasecurityset_id and ap.parentAgent =  dsf.filtervalues " +
				"and dsf.filtervalues in("+partnerCodes+")" +
				"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
				"and username like '%" + name + "%'" +
				"and email like '%" + emailId + "%'" +
				"and phone_number like '%"+ phone_number + "%'" +
				"union Select distinct ap.id,ap.username,ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled ,ap.userType,ap.NPSscore,ap.relocationContact ,ap.basedAt" +
				" from app_user ap, user_role u, role r, companydivision c " +
				"where ap.corpid=c.corpid " +
				"and ap.id= u.user_id " +
				"and username like '%" + name + "%'" +
				"and email like '%" + emailId + "%'" +
				"and phone_number like '%"+ phone_number + "%'" +
				""+condition1+" "+ 
				"and u.role_id= r.id " +
				"and c.bookingagentcode in("+partnerCodes+")" +
				"and (r.name='ROLE_COORD')" +
				"union Select distinct ap.id,ap.username,ap.first_Name,ap.last_Name,ap.functionalArea,ap.userTitle,ap.email,ap.phone_number,ap.account_enabled,ap.userType,ap.NPSscore,ap.relocationContact ,ap.basedAt" +
				" from app_user ap where ap.basedAt in("+partnerCodes+")"+
				"and username like '%" + name + "%'" +
				"and email like '%" + emailId + "%'" +
				"and phone_number like '%"+ phone_number + "%'" +
				"and ap.corpID in ('TSFT','"+sessionCorpID+"') " +
				"AND ap.contact is true AND ap.account_enabled is false order by 1 "; 
	
		}else{
			
			Query= "select distinct id,username,first_Name,last_Name,functionalArea,userTitle,email,phone_number,account_enabled,userType,NPSscore,relocationContact,basedAt from app_user where  corpID in ('TSFT','"+sessionCorpID+"')and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%'and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";

		}
			
			
		List agentUsers= this.getSession().createSQLQuery(Query).list();
		
			List result= new ArrayList(); 
		
		if(agentUsers != null && agentUsers.size()>0){
			return getConvertObjectToModel1(agentUsers);
		}
		
		return result;
	}
	private List<User> getConvertObjectToModel1(List list1){
		List<User> itemList = new ArrayList<User>();
		Iterator it=list1.iterator();
		while(it.hasNext()){
			User u = new User();
	           Object []row= (Object [])it.next();
	           Long id = Long.parseLong(row[0].toString());
	           u.setId(id);
	           if(row[1] != null){
	        	   u.setUsername(row[1].toString());;
	           }
	           if(row[2] != null){
	        	   u.setFirstName(row[2].toString());
	           }
	           if(row[3] != null){
	        	   u.setLastName(row[3].toString());
	           }
	           if(row[4] != null){
	        	   u.setJobFunction(row[4].toString());
	           }
	           if(row[5] != null){
	        	   u.setUserTitle(row[5].toString());
	           }
	           if(row[6] != null){
	        	   u.setEmail(row[6].toString());
	           }
	           if(row[7] != null){
	        	   u.setPhoneNumber(row[7].toString());
	           }
	           if(row[8] != null){
	        	   u.setEnabled((Boolean) row[8]);
	           }
	           if(row[9] != null){
	        	   u.setUserType( row[9].toString());
	           }
	           if(row[10] != null){
	        	   u.setNPSscore((BigDecimal)row[10]);
	           }
	           if(row[11] != null){
	        	   u.setRelocationContact((Boolean) row[11]);
	           }
	         if(row[12] != null){
	        	   u.setBasedAt(row[12].toString());;
	           }
	           itemList.add(u);
		}
		return itemList;
	}
	


	public List getagentRequestList(String partnerCode, String sessionCorpID,String name, String emailId,String phone_number,String status) {
     String Query="";
     if(status!=null && status.equalsIgnoreCase("Rejected"))
     {
     if(partnerCode !=null && (!(partnerCode.trim().equals("")))){ 
       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"')and parentAgent='"+partnerCode+"' and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=False and createdby='Partner Request' and createdBy!=updatedBy and updatedOn >= createdOn order by id desc";
     }else{
       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"') and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=false and createdBy='Partner Request' and createdBy!=updatedBy and updatedOn >= createdOn order by id desc";
     }}
     else if(status!=null && status.equalsIgnoreCase("Approved"))
     {
    	 if(partnerCode !=null && (!(partnerCode.trim().equals("")))){ 
    	       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"')and parentAgent='"+partnerCode+"' and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=true and createdby='Partner Request' and createdBy!=updatedby and updatedOn >= createdOn order by id desc";
    	     }else{
    	       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"') and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=true and createdBy='Partner Request' and createdBy!=updatedby and updatedOn >= createdOn order by id desc";
    	     }
     }
     else 
     {
    	 if(partnerCode !=null && (!(partnerCode.trim().equals("")))){ 
    	       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"')and parentAgent='"+partnerCode+"' and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=False and createdby='Partner Request' and createdBy=updatedBy and updatedOn <= createdOn order by id desc";
    	     }else{
    	       Query= "from User where corpID in ('TSFT','"+sessionCorpID+"') and username like'%"+name+"%' and email like'%"+emailId+"%' and phone_number like'%"+phone_number+"%' and  usertype='AGENT' and account_enabled=False and createdBy='Partner Request' and createdBy=updatedby and updatedOn <= createdOn order by id desc";
    	     }
     }
   
     System.out.println("query----"+Query);
     return getHibernateTemplate().find(Query);
	}
	
	public List getUserRequestDetailList(String email) {
		
	String query1= "from User where email='"+email+"' and userType='AGENT' and account_enabled is true and account_expired is not true and account_locked is not true and credentials_expired is not true";
	return getHibernateTemplate().find(query1);
}
	public List findUserUsername(String coordinator) {
		// TODO Auto-generated method stub
		return this.getSession().createSQLQuery("select username from app_user where username='"+coordinator+"' and username not like 'admin%' order by alias").list();
	}
}