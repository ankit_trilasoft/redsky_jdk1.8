package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.RoleBasedComponentPermissionManager;

public class RoleBasedPermissionAction extends BaseAction{
	
	private Long id;
	private Long maxId;
	private String sessionCorpID;
	private RoleBasedComponentPermission roleBasedComponentPermission;
	private RoleBasedComponentPermissionManager roleBasedComponentPermissionManager;
	private List roleBasedPermissionList;
	private RefMasterManager refMasterManager;
	private static Map<String, String> roleAccess;
	private static Map<String, String> display;
	private static List role;
	
	
	public RoleBasedPermissionAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		roleAccess = refMasterManager.findByParameter(corpId, "ROLEACCESS");
		role=roleBasedComponentPermissionManager.getRole(corpId);
		role.add("ROLE_BOOKING_AGENT");
		role.add("ROLE_BROKER_AGENT");
		role.add("ROLE_ORIGIN_AGENT");
		role.add("ROLE_ORIGINSUB_AGENT");
		role.add("ROLE_DESTINATION_AGENT");
		role.add("ROLE_DESTINATIONSUB_AGENT");
		
		return SUCCESS;
	 }
	@SkipValidation
	public String roleBasedPermissionList(){
			
		roleBasedPermissionList=roleBasedComponentPermissionManager.getAll();
		getComboList(sessionCorpID);
		return SUCCESS;

	}
	@SkipValidation
	public String delete() {
		roleBasedComponentPermissionManager.remove(id);
		try{
		roleBasedComponentPermissionManager.updateCurrentRolePermission(sessionCorpID);
		}catch (Exception e) {}
		saveMessage(getText("Role Permission is deleted sucessfully"));
		
		return SUCCESS;
	}
	@SkipValidation
	public String edit(){
		
		if (id != null)
		{
			roleBasedComponentPermission=roleBasedComponentPermissionManager.get(id);
		}
		else
		{
			roleBasedComponentPermission=new RoleBasedComponentPermission();
			roleBasedComponentPermission.setCreatedOn(new Date());
			roleBasedComponentPermission.setUpdatedOn(new Date());
		
		}
		roleBasedComponentPermission.setCorpID(sessionCorpID);
		getComboList(sessionCorpID);
		return SUCCESS;
	}
	
	public String save() throws Exception{
		getComboList(sessionCorpID);
		boolean isNew = (roleBasedComponentPermission.getId() == null);
		
		if(isNew)
	    {
			roleBasedComponentPermission.setCreatedOn(new Date());
	    }
	      else
	        {
	    	  roleBasedComponentPermission.setCreatedOn(roleBasedComponentPermission.getCreatedOn());
	        }
		
		roleBasedComponentPermission.setCorpID(sessionCorpID);	
		roleBasedComponentPermission.setUpdatedBy(getRequest().getRemoteUser());
		roleBasedComponentPermission.setUpdatedOn(new Date());
		roleBasedComponentPermission= roleBasedComponentPermissionManager.save(roleBasedComponentPermission);
		try{
			roleBasedComponentPermissionManager.updateCurrentRolePermission(sessionCorpID);
			}catch (Exception e) {}		
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Corp Component Permission has been added successfully" : "Corp Component Permission has been updated successfully";
		saveMessage(getText(key));
		}
		  return SUCCESS;
        }
	
	@SkipValidation
	public String search(){
		boolean componenetId=roleBasedComponentPermission.getComponentId()==null;
		boolean description=roleBasedComponentPermission.getDescription()==null;
		boolean role=roleBasedComponentPermission.getRole()==null;
		if(!componenetId || !description ||!role) {
			roleBasedPermissionList=roleBasedComponentPermissionManager.searchcompanyPermission(roleBasedComponentPermission.getComponentId(), roleBasedComponentPermission.getDescription(),roleBasedComponentPermission.getRole());
			
		}
		
		return SUCCESS;
		
	}
	
	
	private String gotoPageString;
	private String validateFormNav;
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = save();
    	return gotoPageString;
    }
	
	
	

	public Long getMaxId() {
		return maxId;
	}
	public void setMaxId(Long maxId) {
		this.maxId = maxId;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getDisplay() {
		return display;
	}

	public void setDisplay(Map<String, String> display) {
		this.display = display;
	}

	

	public List getRole() {
		return role;
	}

	public void setRole(List role) {
		this.role = role;
	}

	

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public List getRoleBasedPermissionList() {
		return roleBasedPermissionList;
	}

	public void setRoleBasedPermissionList(List roleBasedPermissionList) {
		this.roleBasedPermissionList = roleBasedPermissionList;
	}

	public RoleBasedComponentPermission getRoleBasedComponentPermission() {
		return roleBasedComponentPermission;
	}

	public void setRoleBasedComponentPermission(
			RoleBasedComponentPermission roleBasedComponentPermission) {
		this.roleBasedComponentPermission = roleBasedComponentPermission;
	}

	public void setRoleBasedComponentPermissionManager(
			RoleBasedComponentPermissionManager roleBasedComponentPermissionManager) {
		this.roleBasedComponentPermissionManager = roleBasedComponentPermissionManager;
	}

	public static Map<String, String> getRoleAccess() {
		return roleAccess;
	}

	public static void setRoleAccess(Map<String, String> roleAccess) {
		RoleBasedPermissionAction.roleAccess = roleAccess;
	}

}
