package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.DriverReconciliationManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VanLineManager;

public class DriverReconciliationAction  extends BaseAction implements Preparable {
	private List vanLineCodeList=new ArrayList();
	private List weekendingDateList=new ArrayList();
	private String agent="";
	
	private String sessionCorpID;
	
	private DriverReconciliationManager driverReconciliationManager;
	private VanLineManager vanLineManager;
	private VanLine vanLine;
	private Date weekEnding;
	private String vanRegNum;
	private String controlFlag;
	private String orderNumber;
	private String ownerCode="";
	private  ExchangeRateManager exchangeRateManager;
	private ServiceOrderManager serviceOrderManager;
	private CompanyDivisionManager companyDivisionManager;
	private PartnerManager partnerManager;
	private ChargesManager chargesManager;
	private RefMasterManager refMasterManager;
	private ServiceOrder serviceOrder; 
	private AccountLineManager accountLineManager;
	private AccountLine accountLine;
	
	private BillingManager billingManager;
	private Billing billing;
	private List maxLineNumber;
	private String accountLineNumber;
	private Long autoLineNumber;
	private SystemDefault systemDefault;
	private  TrackingStatus trackingStatus;
	private  Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	
	
	public DriverReconciliationAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
		
	}
	
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
	}
   List driverPreviewDetails=new ArrayList();
	@SkipValidation
	public String driverReconciliationDetails() {
		vanLineCodeList = driverReconciliationManager.getVanLineCodeCompDiv(sessionCorpID);
		weekendingDateList = driverReconciliationManager.getWeekendingList(agent,ownerCode,sessionCorpID);
		return SUCCESS;
	}
	@SkipValidation
	public String driverWithReconcile() {
		
		
		
		
		driverPreviewDetails=driverReconciliationManager.getDriverPreview(agent,ownerCode, weekEnding,sessionCorpID);
		
	return SUCCESS;
	}
  List driverPreviewAccDetails=new ArrayList();
	@SkipValidation
	public String driverRegAccNum(){
		 
		driverPreviewAccDetails=driverReconciliationManager.getDriverAccPreview(vanRegNum,agent,ownerCode,weekEnding,sessionCorpID); 
	 return SUCCESS;  
	  }
    private String driverDetailsPay="";
	private List<SystemDefault> sysDefaultDetail;
	private String companyDivisionAcctgCodeUnique;
	private String actCode="";
	private String baseCurrency="";
	private String divisionFlag;
	private Boolean costElementFlag;
	private String baseCurrencyCompanyDivision="";
	@SkipValidation
	public String updateAccDriver(){
		sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				baseCurrency="";
				if(systemDefault.getBaseCurrency()!=null){
					baseCurrency=systemDefault.getBaseCurrency();	
				}
				companyDivisionAcctgCodeUnique="";
				if(systemDefault.getCompanyDivisionAcctgCodeUnique()!=null){
					companyDivisionAcctgCodeUnique=systemDefault.getCompanyDivisionAcctgCodeUnique();	
				}
			}
		}
		String loginUser = getRequest().getRemoteUser();
		Map<String , String> accFileDriver =new HashMap<String, String>();
		if(driverDetailsPay!=null && !driverDetailsPay.equals("")){
			String abc[]=driverDetailsPay.split("`");
			for (String string : abc) {
				String cda[]=string.split("~");
				accFileDriver.put(cda[0].trim(), cda[1]);
			}
		}
	
		String chargeCode="";
		String sid="";
		String aid="";
		String amount="";
		String vendorCode="";
		String estimateVendorName="";
		String glType="";
		String action="";
		for (Map.Entry<String, String> Entry1 : accFileDriver.entrySet()) {
			//aid=Entry1.getKey();
		if(Entry1.getValue()!=null && !Entry1.getValue().toString().equals("") && !Entry1.getValue().toString().equals("B") ){
		String accData[]=Entry1.getValue().toString().split("@");
			chargeCode=accData[0].trim().toString();
			sid=accData[1].trim().toString();
			amount=accData[2].trim().toString();
			vendorCode=accData[3].trim().toString();
			estimateVendorName=accData[4].trim().toString();
			glType=accData[5].trim().toString();
			action=accData[6].trim().toString();
			if(amount!=null && !amount.equals("")){
		 Long  SoId = Long.parseLong(sid)  ; 
		  serviceOrder = serviceOrderManager.get(SoId);		  
		  billing=billingManager.get(SoId);
		  trackingStatus=trackingStatusManager.get(SoId);
		  miscellaneous = miscellaneousManager.get(SoId);
		  String actCode="";
		  accountLine = new AccountLine();
		  accountLine.setActivateAccPortal(true); 
		  boolean activateAccPortal =true;
		
		  accountLine.setBasis("");
		  accountLine.setCategory("Driver");
		  accountLine.setChargeCode(chargeCode);

		  											  
		    accountLine.setContract(billing.getContract());
		    accountLine.setVendorCode(vendorCode);
			accountLine.setEstimateVendorName(estimateVendorName);
			accountLine.setCompanyDivision(serviceOrder.getCompanyDivision()); 
			accountLine.setDownload(true);
			if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
	 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),accountLine.getCompanyDivision(),sessionCorpID);
	 		}else{
	 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
	 		}
	         accountLine.setActgCode(actCode);	
			
			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrency);
			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
				accountLine.setLocalAmount(new BigDecimal(Double.parseDouble(amount)).multiply(new BigDecimal(accExchangeRateList.get(0).toString())));
			}else{
				accountLine.setLocalAmount(new BigDecimal("0.00"));
			}
			if(actCode !=null && (!(actCode.trim().equals("")))){
				baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
        		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
	    		{

		    		accountLine.setEstCurrency(baseCurrency);
	    			accountLine.setRevisionCurrency(baseCurrency);	
	    			accountLine.setCountry(baseCurrency);
	    			accountLine.setEstValueDate(new Date());
	    			accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
	    			accountLine.setRevisionValueDate(new Date());
	    			accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
	    			accountLine.setValueDate(new Date());
	    			accountLine.setExchangeRate(new Double(1.0));
		    		
        			
	    		}else{

		    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
	    			accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
	    			accountLine.setCountry(baseCurrencyCompanyDivision);
	    			accountLine.setEstValueDate(new Date());
	    			accountLine.setRevisionValueDate(new Date());
	    			accountLine.setValueDate(new Date());
	    			//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
	    			//List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
	    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
	    				try{
	    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
	    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
	    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
	    				}catch(Exception e){
	    					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
		    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
		    				accountLine.setExchangeRate(new Double(1.0));
		    				e.printStackTrace();
	    				}
	    			}else{
	    				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
	    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
	    				accountLine.setExchangeRate(new Double(1.0));
	    			}
		    		
	    		}
			}
	
      	accountLine.setActualExpense(new BigDecimal(Double.parseDouble(amount)));
   
      	String  invDate ="";
          try{
			 if(weekEnding!=null) {
	        	 SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
	        	 invDate	= new StringBuilder(formats.format(weekEnding)).toString();
			 }}catch(Exception e){
				 e.printStackTrace();
			 }
			 accountLine.setInvoiceNumber(agent+invDate);
			 accountLine.setInvoiceDate(new Date());
			 accountLine.setReceivedDate(weekEnding);
			 accountLine.setPayingStatus("A");
			 if(action.equalsIgnoreCase("Approved")){
			 accountLine.setPayPayableStatus("Fully Paid");
			 }else if(action.equalsIgnoreCase("Dispute")){
			  accountLine.setPayPayableStatus("Dispute"); 
			 }
			 accountLine.setPayPayableDate(weekEnding); 
			 accountLine.setPayPayableVia("UVL "+invDate);  
			 accountLine.setPayPayableAmount(new BigDecimal(Double.parseDouble(amount)).multiply(new BigDecimal(accExchangeRateList.get(0).toString()))); 
			 accountLine.setCorpID(sessionCorpID);
			 accountLine.setStatus(true);
			 accountLine.setServiceOrder(serviceOrder);
			 accountLine.setServiceOrderId(serviceOrder.getId());
			 accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
			 accountLine.setShipNumber(serviceOrder.getShipNumber());
			 accountLine.setCreatedOn(new Date());
			 //accountLine.setPayPostDate(new Date());
		/*	 if(action.equalsIgnoreCase("")){
				 accountLine.setPaymentStatus("Fully Paid"); 
			 }else if(){
				 accountLine.setPaymentStatus("Fully Paid"); 
			 }*/
			 accountLine.setCreatedBy("DriverReconcilition: "+loginUser);
			 accountLine.setUpdatedOn(new Date());
			 accountLine.setUpdatedBy("DriverReconcilition : "+loginUser);
			 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
	         if ( maxLineNumber.get(0) == null ) {          
	         	accountLineNumber = "001";
	         }else {
	         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
	             if((autoLineNumber.toString()).length() == 2) {
	             	accountLineNumber = "0"+(autoLineNumber.toString());
	             }
	             else if((autoLineNumber.toString()).length() == 1) {
	             	accountLineNumber = "00"+(autoLineNumber.toString());
	             } 
	             else {
	             	accountLineNumber=autoLineNumber.toString();
	             }
	         }
	         accountLine.setAccountLineNumber(accountLineNumber);	   
	         
	    		accountLine.setDivision("03");
	    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
	    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
	    		try{  
	    			String roles[] = agentRoleValue.split("~");
	    		  if(roles[3].toString().equals("hauler")){
	    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
	    				   accountLine.setDivision("01");
	    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
	    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
	    					   accountLine.setDivision("01");
	    				   }else{ 
	    					   String divisionTemp="";
	    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
	    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
	    							}
	    							if(!divisionTemp.equalsIgnoreCase("")){
	    								accountLine.setDivision(divisionTemp);
	    							} 
	    				   }
	    			   }else{
	    				   String divisionTemp="";
	    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
	    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
	    						}
	    						if(!divisionTemp.equalsIgnoreCase("")){
	    							accountLine.setDivision(divisionTemp);
	    						}
	    			   }
	    		  }else{
	    			  String divisionTemp="";
	    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
	    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
	    					}
	    					if(!divisionTemp.equalsIgnoreCase("")){
	    						accountLine.setDivision(divisionTemp);
	    					}
	    		  }
	    		}catch (Exception e1) {
	    			// TODO Auto-generated catch block
	    			e1.printStackTrace();
	    		}
    		
	       // List chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),glType,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
			  List chargeid=new ArrayList();
			  if(!costElementFlag){
			  chargeid=chargesManager.findChargeId(glType,billing.getContract());
			  }else{
			   chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),glType,sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());  
			  }
	        try{
			    
			  if(!costElementFlag){
				Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));  
					accountLine.setRecGl(charge.getGl());
					accountLine.setPayGl(charge.getExpGl());
					accountLine.setNote(charge.getDescription());
				}else{
					String chargeStr="";
					   
					if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
						  chargeStr= chargeid.get(0).toString();
					  }
					   if(!chargeStr.equalsIgnoreCase("")){
						  String [] chrageDetailArr = chargeStr.split("#");
						  accountLine.setRecGl(chrageDetailArr[1]);
						  accountLine.setPayGl(chrageDetailArr[2]);
						}else{
						  accountLine.setRecGl("");
						  accountLine.setPayGl("");
					  }
				}
		  } catch(Exception e){
			  e.printStackTrace();
		  }
		  accountLine= accountLineManager.save(accountLine); 
		}
			
		}
		}
		
		//driverWithReconcile();
		return SUCCESS;  
	}
	
	private List partnerDetailsAutoComplete =new ArrayList();
	private String partnerCodeAutoCopmlete="";
	private String paertnerCodeId;
	private String autocompleteDivId;
	@SkipValidation
	public String getownerDetailsForAutoComplete(){
		partnerDetailsAutoComplete=driverReconciliationManager.findPartnerDetailsForAutoComplete(partnerCodeAutoCopmlete,sessionCorpID);
		
	return SUCCESS; 		
	}
	public void setDriverReconciliationManager(
			DriverReconciliationManager driverReconciliationManager) {
		this.driverReconciliationManager = driverReconciliationManager;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}



	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}



	public List getVanLineCodeList() {
		return vanLineCodeList;
	}



	public void setVanLineCodeList(List vanLineCodeList) {
		this.vanLineCodeList = vanLineCodeList;
	}



	public String getAgent() {
		return agent;
	}



	public void setAgent(String agent) {
		this.agent = agent;
	}
	public List getWeekendingDateList() {
		return weekendingDateList;
	}
	public void setWeekendingDateList(List weekendingDateList) {
		this.weekendingDateList = weekendingDateList;
	}
	public VanLine getVanLine() {
		return vanLine;
	}
	public void setVanLine(VanLine vanLine) {
		this.vanLine = vanLine;
	}
	public void setVanLineManager(VanLineManager vanLineManager) {
		this.vanLineManager = vanLineManager;
	}

	public Date getWeekEnding() {
		return weekEnding;
	}

	public void setWeekEnding(Date weekEnding) {
		this.weekEnding = weekEnding;
	}

	public List getDriverPreviewDetails() {
		return driverPreviewDetails;
	}

	public void setDriverPreviewDetails(List driverPreviewDetails) {
		this.driverPreviewDetails = driverPreviewDetails;
	}

	public List getDriverPreviewAccDetails() {
		return driverPreviewAccDetails;
	}

	public void setDriverPreviewAccDetails(List driverPreviewAccDetails) {
		this.driverPreviewAccDetails = driverPreviewAccDetails;
	}

	public String getVanRegNum() {
		return vanRegNum;
	}

	public void setVanRegNum(String vanRegNum) {
		this.vanRegNum = vanRegNum;
	}

	public String getControlFlag() {
		return controlFlag;
	}

	public void setControlFlag(String controlFlag) {
		this.controlFlag = controlFlag;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOwnerCode() {
		return ownerCode;
	}

	public void setOwnerCode(String ownerCode) {
		this.ownerCode = ownerCode;
	}

	public String getDriverDetailsPay() {
		return driverDetailsPay;
	}

	public void setDriverDetailsPay(String driverDetailsPay) {
		this.driverDetailsPay = driverDetailsPay;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public List getMaxLineNumber() {
		return maxLineNumber;
	}

	public void setMaxLineNumber(List maxLineNumber) {
		this.maxLineNumber = maxLineNumber;
	}

	public String getAccountLineNumber() {
		return accountLineNumber;
	}

	public void setAccountLineNumber(String accountLineNumber) {
		this.accountLineNumber = accountLineNumber;
	}

	public Long getAutoLineNumber() {
		return autoLineNumber;
	}

	public void setAutoLineNumber(Long autoLineNumber) {
		this.autoLineNumber = autoLineNumber;
	}

	public String getPartnerCodeAutoCopmlete() {
		return partnerCodeAutoCopmlete;
	}

	public void setPartnerCodeAutoCopmlete(String partnerCodeAutoCopmlete) {
		this.partnerCodeAutoCopmlete = partnerCodeAutoCopmlete;
	}

	public String getPaertnerCodeId() {
		return paertnerCodeId;
	}

	public void setPaertnerCodeId(String paertnerCodeId) {
		this.paertnerCodeId = paertnerCodeId;
	}

	public List getPartnerDetailsAutoComplete() {
		return partnerDetailsAutoComplete;
	}

	public void setPartnerDetailsAutoComplete(List partnerDetailsAutoComplete) {
		this.partnerDetailsAutoComplete = partnerDetailsAutoComplete;
	}

	public String getAutocompleteDivId() {
		return autocompleteDivId;
	}

	public void setAutocompleteDivId(String autocompleteDivId) {
		this.autocompleteDivId = autocompleteDivId;
	}

	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}

	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}

	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}

	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}



}
