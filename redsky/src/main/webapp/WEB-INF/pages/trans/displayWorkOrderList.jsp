<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="operationResource.title"/></title>   
    <meta name="heading" content="<fmt:message key='O&I.heading' />"/>   
    <style> #ajax_tooltipObj .ajax_tooltip_content {background-color:#FFF !important;}</style>
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	 <td align="left"><b>Work Order List </b></td>
	<td align="right"  style="width:30px;">
	<img  valign="top"align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="findDistinctWorkOrder" class="table" requestURI="" id="findDistinctWorkOrder" style="margin:0px;margin-bottom:8px">
		  <display:column title=" " style="width:3px;"><input type="checkbox" id="checkboxId${findDistinctWorkOrder.id}" name="DD" value="${findDistinctWorkOrder.id}" /></display:column>
		 <display:column  title="WO#"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${findDistinctWorkOrder.id}" id="workOrderOI${findDistinctWorkOrder.id}"  value="${findDistinctWorkOrder.workorder}" style="width:40px" disabled="true"/> 
    </display:column>
	 <display:column  title="Ticket"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${findDistinctWorkOrder.id}" id="ticket${findDistinctWorkOrder.id}"  value="${findDistinctWorkOrder.ticket}" style="width:40px" disabled="true"/> 
    </display:column>
    <display:column  title="Ticket Status"  style="width:7px">
    <%-- <c:if test="${findDistinctWorkOrder.targetActual == 'T'}"><c:set var="status" value="Target"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'A'}"><c:set var="status" value="Actual"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'C'}"><c:set var="status" value="Cancelled"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'D'}"><c:set var="status" value="Dispatching"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'F'}"><c:set var="status" value="Forecasting"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'P'}"><c:set var="status" value="Pending"></c:set> </c:if>
   <c:if test="${findDistinctWorkOrder.targetActual == 'R'}"><c:set var="status" value="Rejected"></c:set> </c:if> --%>
   <input type="text" class="input-text pr-f11" name="workOrderOI${findDistinctWorkOrder.id}" id="ticket${findDistinctWorkOrder.id}"  value="${findDistinctWorkOrder.targetActual}" style="width:57px" disabled="true"/> 
    </display:column>
	
	<display:column  title="Billing Status"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="workOrderOI${findDistinctWorkOrder.id}" id="ticket${findDistinctWorkOrder.id}"  value="${findDistinctWorkOrder.reviewStatus}" style="width:40px" disabled="true"/> 
    </display:column>
    
    <display:column  title="Invoice#"  style="width:7px">
   <input type="text" class="input-text pr-f11" name="invoiceNumber${findDistinctWorkOrder.id}" id="invoiceNumber${findDistinctWorkOrder.id}"  value="${findDistinctWorkOrder.invoiceNumber}" style="width:40px" disabled="true"/> 
    </display:column>
    
	</display:table>
	
	  <c:if test="${findDistinctWorkOrder!='[]'}"> 
  <input type="button"  Class="cssbutton" align="center" style="width: 90px; font-size: 10px; height: 21px; margin: 0px 70px 5px;" id="okButton"  value="Open WorkOrder" onclick="findAllResource('flag');"/>
	</c:if>
	<c:if test="${findDistinctWorkOrder=='[]'}"> 
  <input type="button"  Class="cssbutton" align="center" style="width: 90px; font-size: 10px; height: 21px; margin: 0px 70px 5px;" id="okButton" readonly="readonly" disabled="true" value="Open WorkOrder" onclick="findAllResource('flag');"/>
	</c:if>