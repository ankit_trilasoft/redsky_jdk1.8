/**
 * Implementation of <strong>ActionSupport</strong> that contains convenience methods.
 * This class represents the basic actions on "TaskPlanning" object in Redsky that allows for task management.
 * @Class Name	TaskPlanningAction
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        24-OCT-2019
 */


package com.trilasoft.app.webapp.action;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.IntegrationLogInfo;
import com.trilasoft.app.model.TaskPlanning;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.IntegrationLogInfoManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.TaskPlanningManager;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;

import com.opensymphony.xwork2.Preparable;


public class TaskPlanningAction extends BaseAction implements Preparable{
	private String usertype; 
	private String sessionCorpID;
	private TaskPlanning taskPlanning;
    private TaskPlanningManager taskPlanningManager;
    private RefMasterManager refMasterManager;
    private List taskPlanningList;
    private Map<String, String> estimatorList;
    private Map<String, String> extCompany;
    private Map<String, String> planner;
    
    static final Logger logger = Logger.getLogger(TaskPlanningAction.class);
    Date currentdate = new Date();
    public List getTaskPlanningList() {
		return taskPlanningList;
	}
	public void setTaskPlanningList(List taskPlanningList) {
		this.taskPlanningList = taskPlanningList;
	}
 	public TaskPlanningAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        usertype=user.getUserType();
 	}
    public void setTaskPlanningManager(TaskPlanningManager taskPlanningManager) {
		this.taskPlanningManager = taskPlanningManager;
	}
	public void prepare() throws Exception {
		try {} catch (Exception e) {
			 e.printStackTrace();
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		}
	
		
		
	}
	private Long id;
	private Long cid;
	private CustomerFile customerFile;
	private CustomerFileManager customerFileManager;
	private  IntegrationLogInfoManager  integrationLogInfoManager;
	private int moveCount;
	private String emailSetupValue;
	private String redirectflag;
	public String list() {
    	try{
        customerFile = customerFileManager.get(cid);
      	taskPlanningList = new ArrayList(taskPlanningManager.findTaskPlanningList(customerFile.getCorpID(), cid));
    	}
      	catch (Exception e) {
			 e.printStackTrace();
    	}
      	return SUCCESS;   
      } 
	private String uuidCompany;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		customerFile = customerFileManager.get(cid);
		estimatorList=refMasterManager.findCordinaor("","ROLE_SALE",sessionCorpID);
		extCompany = refMasterManager.findByCategoryCode("TSFT", "EXTERNALCOMPANY");
		planner = refMasterManager.findByCategoryCodeByFlex("TSFT","","EXTERNAPLANNER");
		uuidCompany = refMasterManager.getCompanyUUID(customerFile.getCorpID(), "EXTERNALCOMPANY");
		if (id != null) {
			taskPlanning = taskPlanningManager.get(id);
			if((taskPlanning.getTaskCompany()!=null && !taskPlanning.getTaskCompany().equalsIgnoreCase(""))){
			planner = refMasterManager.findByCategoryCodeByFlex("TSFT", taskPlanning.getTaskCompany(),"EXTERNAPLANNER");
			}
			emailSetupValue=integrationLogInfoManager.findSendMoveCloudGlobalStatus(customerFile.getCorpID(), customerFile.getSequenceNumber(),taskPlanning.getId().toString());
			if(emailSetupValue!= null && !emailSetupValue.equals("")){
			 moveCount=1;
			}
		} else {
			taskPlanning = new TaskPlanning();
			taskPlanning.setCustomerFileId(cid);
			if(customerFile.getSurvey()!=null && !customerFile.getSurvey().equals(""))
			{
				taskPlanning.setTaskDate(customerFile.getSurvey());
			}
			if(customerFile.getSurveyTime()!=null && !customerFile.getSurveyTime().equals(""))
			{
				taskPlanning.setTaskTimeFrom(customerFile.getSurveyTime());
			}
			if(customerFile.getSurveyTime2()!=null && !customerFile.getSurveyTime2().equals(""))
			{
				taskPlanning.setTaskTimeTo(customerFile.getSurveyTime2());
			}
			if(customerFile.getSurveyType()!=null && !customerFile.getSurveyType().equals(""))
			{
				taskPlanning.setTaskType(customerFile.getSurveyType());
			}
			taskPlanning.setServiceOrderId(0L);
			taskPlanning.setCreatedBy(getRequest().getRemoteUser());
			taskPlanning.setUpdatedBy(getRequest().getRemoteUser());;
			taskPlanning.setCreatedOn(new Date());
			taskPlanning.setUpdatedOn(new Date());
		}		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		estimatorList=refMasterManager.findCordinaor("","ROLE_SALE",sessionCorpID);
		extCompany = refMasterManager.findByCategoryCode("TSFT", "EXTERNALCOMPANY");
		planner = refMasterManager.findByCategoryCode("TSFT", "EXTERNAPLANNER");
		redirectflag="1";
		boolean isNew = (taskPlanning.getId() == null);
		if (isNew) { 
			taskPlanning.setCreatedOn(new Date());
		        }
		taskPlanning.setUpdatedOn(new Date());
		taskPlanning.setUpdatedBy(getRequest().getRemoteUser());
		customerFile = customerFileManager.get(taskPlanning.getCustomerFileId());
		List taskCompanyList = refMasterManager.getDescription(taskPlanning.getTaskCompany(), "EXTERNALCOMPANY");
		if(taskCompanyList!=null && !taskCompanyList.isEmpty() && taskCompanyList.get(0)!=null)
		taskPlanning.setTaskCompanyDesc(taskCompanyList.get(0).toString());	
		List taskPlannerList = refMasterManager.getDescription(taskPlanning.getTaskPlanner(), "EXTERNAPLANNER");
		if(taskPlannerList!=null && !taskPlannerList.isEmpty() && taskPlannerList.get(0)!=null)
		taskPlanning.setTaskPlannerDesc(taskPlannerList.get(0).toString());
		
		String taskAssigneeDesc = refMasterManager.findDescriptionByFlex5(taskPlanning.getTaskAssignee(), "EXTERNAPLANNER");
		if(taskAssigneeDesc!=null && !taskAssigneeDesc.equals(""))
		taskPlanning.setTaskAssigneeDesc(taskAssigneeDesc);
		
		taskPlanning.setCorpID(customerFile.getCorpID());
		taskPlanning = taskPlanningManager.save(taskPlanning);
		String key = (isNew) ? "Task has been added successfully." : "Task has been updated successfully.";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  			return SUCCESS;
			}

	private List AjaxParameterValuesList;
	private List AjaxParameterAssignList;
	private String extCompanyId;
	private String assignee;
	private String taskType;
	@SkipValidation
	public String getTaskPlannerListM(){
		try {
			AjaxParameterValuesList=refMasterManager.getTaskPlannerListAjax("EXTERNAPLANNER",extCompanyId);
		    }catch (Exception e) {
			logger.warn("Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString());
			logger.warn(e.getStackTrace()[0].getMethodName().toString());
	    	 e.printStackTrace();
	    	 return CANCEL;
		}

		return SUCCESS;	
	}
	private String taskCompanyuuid;
	@SkipValidation
	public String getAssigneeList(){
		try {
			
			
			if(taskType.equals("Survey"))
			{
				if((!taskCompanyuuid.equals(null) && !taskCompanyuuid.equals("")))
				{
				AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("SURVEYOR",taskCompanyuuid);
			    }
				else
				{
					AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("SURVEYOR",uuidCompany);

				}
			}
			else if(taskType.equals("VideoSurvey"))
			{
				if((!taskCompanyuuid.equals(null) &&  !taskCompanyuuid.equals("")))
				{
				AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("VIDEOSURVEYOR",taskCompanyuuid);
			    }
				else
				{
					AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("VIDEOSURVEYOR",uuidCompany);

				}
				
				
			}
			else if(taskType.equals("Packing") || taskType.equals("Delivery"))
			{
				if((!taskCompanyuuid.equals(null) && !taskCompanyuuid.equals("")))
				{
				AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("EXTERNALCREW",taskCompanyuuid);
			    }
				else
				{
					AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("EXTERNALCREW",uuidCompany);

				}
				
				
			}
			else 
			{
			AjaxParameterAssignList=refMasterManager.getAssigneeListAjax("EXTERNALASSIGNEE",assignee);
			}
		    }catch (Exception e) {
			logger.warn("Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString());
			logger.warn(e.getStackTrace()[0].getMethodName().toString());
	    	 e.printStackTrace();
	    	 return CANCEL;
		}

		return SUCCESS;	
	}
	
	//	  End of Method.
	
	
	@SkipValidation
	public String moveCloudIntegrationLogInfo(){
		        customerFile = customerFileManager.get(cid);
		        taskPlanning = taskPlanningManager.get(id);
				IntegrationLogInfo inloginfo= new IntegrationLogInfo();
			    inloginfo.setSource("RedSky");
			    inloginfo.setDestination("MoveCloud");
			    inloginfo.setIntegrationId(cid.toString());
			    inloginfo.setSourceOrderNum(customerFile.getSequenceNumber());
			    inloginfo.setDestOrderNum(taskPlanning.getTaskType());
			    inloginfo.setOperation("CREATE");
			    inloginfo.setOrderComplete("N");
			    inloginfo.setStatusCode("S");
			    inloginfo.setEffectiveDate(new Date());
			    inloginfo.setDetailedStatus("Ready For Sending");
			    inloginfo.setUgwwOA(id.toString());
			    inloginfo.setCreatedBy(getRequest().getRemoteUser());
			    inloginfo.setCreatedOn(new Date());
			    inloginfo.setUpdatedBy(getRequest().getRemoteUser());
			    inloginfo.setUpdatedOn(new Date());
			    inloginfo.setCorpID(customerFile.getCorpID());
			    inloginfo =  integrationLogInfoManager.save(inloginfo);
			    taskPlanning.setUpdatedBy(getRequest().getRemoteUser());
			    taskPlanning.setUpdatedOn(new Date());
			    taskPlanning.setTaskStatus("Pending");
			    taskPlanning = taskPlanningManager.save(taskPlanning);
			    return SUCCESS;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public TaskPlanning getTaskPlanning() {
		return taskPlanning;
	}
	public void setTaskPlanning(TaskPlanning taskPlanning) {
		this.taskPlanning = taskPlanning;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getEstimatorList() {
		return estimatorList;
	}
	public void setEstimatorList(Map<String, String> estimatorList) {
		this.estimatorList = estimatorList;
	}
	public Long getCid() {
		return cid;
	}
	public void setCid(Long cid) {
		this.cid = cid;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public Map<String, String> getExtCompany() {
		return extCompany;
	}
	public void setExtCompany(Map<String, String> extCompany) {
		this.extCompany = extCompany;
	}
	public Map<String, String> getPlanner() {
		return planner;
	}
	public void setPlanner(Map<String, String> planner) {
		this.planner = planner;
	}
	public String getEmailSetupValue() {
		return emailSetupValue;
	}
	public void setEmailSetupValue(String emailSetupValue) {
		this.emailSetupValue = emailSetupValue;
	}
	public int getMoveCount() {
		return moveCount;
	}
	public void setMoveCount(int moveCount) {
		this.moveCount = moveCount;
	}
	public void setIntegrationLogInfoManager(
			IntegrationLogInfoManager integrationLogInfoManager) {
		this.integrationLogInfoManager = integrationLogInfoManager;
	}
	public String getRedirectflag() {
		return redirectflag;
	}
	public void setRedirectflag(String redirectflag) {
		this.redirectflag = redirectflag;
	}
	public List getAjaxParameterValuesList() {
		return AjaxParameterValuesList;
	}
	public void setAjaxParameterValuesList(List ajaxParameterValuesList) {
		AjaxParameterValuesList = ajaxParameterValuesList;
	}
	public String getExtCompanyId() {
		return extCompanyId;
	}
	public void setExtCompanyId(String extCompanyId) {
		this.extCompanyId = extCompanyId;
	}
	public List getAjaxParameterAssignList() {
		return AjaxParameterAssignList;
	}
	public void setAjaxParameterAssignList(List ajaxParameterAssignList) {
		AjaxParameterAssignList = ajaxParameterAssignList;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public String getUuidCompany() {
		return uuidCompany;
	}
	public void setUuidCompany(String uuidCompany) {
		this.uuidCompany = uuidCompany;
	}
	public String getTaskType() {
		return taskType;
	}
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}
	public String getTaskCompanyuuid() {
		return taskCompanyuuid;
	}
	public void setTaskCompanyuuid(String taskCompanyuuid) {
		this.taskCompanyuuid = taskCompanyuuid;
	}


}  


