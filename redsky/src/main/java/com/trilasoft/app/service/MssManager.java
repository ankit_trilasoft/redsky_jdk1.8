package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.Mss;

public interface MssManager extends GenericManager<Mss, Long> {
	
	public List findListByShipNumber(String shipNumber,String sessionCorpID);
	public List checkById(Long wid);
	public List findRecord(Long id,String sessionCorpID);
	public Map<String, String> findParentRecord(String sessionCorpID,String parameter);
	public Map<String, String> findChildRecord(String sessionCorpID,String parameter,String parentCode);
	public List findAllOriServiceRecord(Long id,String sessionCorpID);
	public List findAllDestinationServiceList(Long id,String sessionCorpID);
	public Map<String, String> findDestinationParentRecord(String sessionCorpID,String parameter);
	public Map<String, String> findDestinationChildRecord(String sessionCorpID,String parameter,String parentCode);
	public List findValuesForQuotePDF(Long id,String TransportTypeRadio,String PurchaseOrderNumber,Long workTicketId, String sessionCorpID);
	public List findCratingValue(Long id,String sessionCorpID);
	public List findOriginValue(Long id,String sessionCorpID);
	public List findDestinationValue(Long id,String sessionCorpID);
	public List getMssNotes(Long id,String PurchaseOrderNumber,String noteType,String sessionCorpID);
	public List getOriginPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID);
	public List getdestinationPhone(Long workTicketId,String PurchaseOrderNumber,String sessionCorpID);
	public void updateplaceOrderNumber(Long id, String soNumber,String workTicketNumber,String placeOrderNumber,String sessionCorpID);
	public void updatefilePath(Long id, String soNumber,String workTicketNumber,String filePath,String sessionCorpID);
	public void updateRefmaster(String submitAs,String userName,String sessionCorpID);
	public List getdefaultSubmitAs(String userName,String sessionCorpID);
	public List getMsscustomerID(String companyDivision,String sessionCorpID);
	public List getSoDetails(String PurchaseOrderNumber,String sessionCorpID);
	
}
