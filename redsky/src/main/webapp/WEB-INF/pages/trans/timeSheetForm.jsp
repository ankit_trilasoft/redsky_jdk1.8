<%@ include file="/common/taglibs.jsp"%> 

<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<head>
<meta name="heading" content="Timesheet Detail"/> 
<title>Timesheet Detail</title> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="JavaScript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
	<%
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    	User user = (User)auth.getPrincipal();
    	String userName=user.getUsername();
		%>
<SCRIPT LANGUAGE="JavaScript">
	function completeTimeString() {
	
		stime1 = document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
		stime2 = document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = "0" + stime2;
			}
		}
		
	}

</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
//document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.beginHours'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Begin Hours' time must between 00:01 to 23:59 (Hrs)");
//document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.beginHours'].select();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Begin Hours' time must between 00:01 to 23:59 (Hrs)");
//document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.beginHours'].select();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Begin Hours' time must between 0 to 59 (Sec)");
//document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.beginHours'].select();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
//document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.endHours'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'End Hours' time must between 00:01 to 23:59 (Hrs)");
//document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.endHours'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'End Hours' time must between 00:01 to 23:59 (Hrs)");
//document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.endHours'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'End Hours' time must between 0 to 59 (Sec)");
//document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = '00:00';
document.forms['timeSheetForm'].elements['timeSheet.endHours'].focus();
return false;
}

return checkTime();

}
function checkBEHours(){
   
	var tim1=document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
	var tim2=document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
	if(tim1=='' || tim2=='' || tim1=='00' || tim1=='00:00' || tim2=='00' || tim2=='00:00' || tim1=='0' || tim2=='0')
	{
		alert('Please fill the Begin and End Hours');
		return false;
	}
	
	return checkDistributionCode();
}
function checkTime()
{
	var tim1=document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
	var tim2=document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("Begin hours cannot be less than end hours");
		//document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = '00:00';
		document.forms['timeSheetForm'].elements['timeSheet.beginHours'].select();
		return false;
	}else{
		return fillAction();
	}
}

</script>
<script>
function calcTimeHours(){

	var action = document.forms['timeSheetForm'].elements['timeSheet.action'].value;
	var tim1=document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
	var tim2=document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
	
	if(action == 'C'){
	if((tim1 != '00' && tim1 != '00:00' ) && (tim2 != '00' && tim2 != '00:00' )){
	time1 = ((tim1.substring(0,tim1.indexOf(":"))*60)*1 + (tim1.substring(tim1.indexOf(":")+1, tim1.length))*1)/60;
	time2 = ((tim2.substring(0,tim2.indexOf(":"))*60)*1 + (tim2.substring(tim2.indexOf(":")+1, tim2.length))*1)/60;
	
	//document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = time2*1 - time1*1;
	
	var tobeRound=time2*1 - time1*1;
	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
	
	if(document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 <= .5){
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].disabled = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].disabled = true;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].disabled = true;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked = true;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].checked = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].checked = false;
	}else if(document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 > .5 && document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 <= 1){
	    document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].disabled = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].disabled = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].disabled = true;  
	}else{
		if(!document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked && !document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].checked && !document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].checked){
			document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked = true;
		}
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].disabled = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].disabled = false;
		document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].disabled = false;
	if(!document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked && document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].checked){
		
		//document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = time2*1 - time1*1 - .5;
		
		var tobeRound=time2*1 - time1*1 - .5;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
		
	} else if(!document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked && ( ! (document.forms['timeSheetForm'].elements['timeSheet.lunch'][1].checked) )&& (document.forms['timeSheetForm'].elements['timeSheet.lunch'][2].checked) )
	    {
	    var tobeRound=time2*1 - time1*1 - 1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
	}
	} 
	}
	}else{
		if((tim1 != '00' && tim1 != '00:00' ) && (tim2 != '00' && tim2 != '00:00' )){
		time1 = ((tim1.substring(0,tim1.indexOf(":"))*60)*1 + (tim1.substring(tim1.indexOf(":")+1, tim1.length))*1)/60;
		time2 = ((tim2.substring(0,tim2.indexOf(":"))*60)*1 + (tim2.substring(tim2.indexOf(":")+1, tim2.length))*1)/60;
		
		//document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = time2*1 - time1*1;
		var tobeRound=time2*1 - time1*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
		
		}
	}
	
	if(!(document.forms['timeSheetForm'].elements['timeSheet.overTime'].value == '')){
		var tobeRound=document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 - document.forms['timeSheetForm'].elements['timeSheet.overTime'].value*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value =Math.round(tobeRound*100)/100;
		
	}
	if(!(document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value == '')){
		var tobeRound= document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 - document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value =Math.round(tobeRound*100)/100;
	}
	findDefaultTimeHours();
}

function revisedRegularHours(targetElement){
	if(targetElement.checked){
	var action = document.forms['timeSheetForm'].elements['timeSheet.action'].value;
	var tim1=document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
	var tim2=document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
	if(action == 'C'){
	if(tim1 != '' && tim2 != ''){
	time1 = ((tim1.substring(0,tim1.indexOf(":"))*60)*1 + (tim1.substring(tim1.indexOf(":")+1, tim1.length))*1)/60;
	time2 = ((tim2.substring(0,tim2.indexOf(":"))*60)*1 + (tim2.substring(tim2.indexOf(":")+1, tim2.length))*1)/60;
	//document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = time2*1 - time1*1 - .5;
	if(targetElement.value=='Lunch Included'){
	var tobeRound=time2*1 - time1*1 - .5; 
	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
	}
	if(targetElement.value=='Lunch Included @ 1hr'){
	var tobeRound=time2*1 - time1*1 - 1; 
	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
	
	}
	}
	}
	if(targetElement.value=='No lunch')
	{
		var tobeAdded=time2*1 - time1*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeAdded*100)/100;
	}
	}
	if(!targetElement.checked){
		if(document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value != ''){
			var tobeRound= document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 + .5;
			document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value =Math.round(tobeRound*100)/100;
		}
	}
	if(!(document.forms['timeSheetForm'].elements['timeSheet.overTime'].value == '')){
		var tobeRound= document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 - document.forms['timeSheetForm'].elements['timeSheet.overTime'].value*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value =Math.round(tobeRound*100)/100;
	}
	if(!(document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value == '')){
		var tobeRound= document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1 - document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value*1;
		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value =Math.round(tobeRound*100)/100;
	}
}
</script>
<SCRIPT LANGUAGE="JavaScript">

function findDefaultTimeHours(){
	   var action=document.forms['timeSheetForm'].elements['timeSheet.action'].value;
	   if(action == 'C'){
			document.getElementById("lunchDiv").style.visibility = "visible";
	   }else{
			document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked = true;
			document.getElementById("lunchDiv").style.visibility = "hidden";
	   }
	   var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
		var url="getTimetypeFlex.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&actionCode=' + encodeURI(action);
		http21.open("GET", url, true);
		http21.onreadystatechange = function(){handleHttpResponseGetFlex1Value(action);};
		http21.send(null);
}
function handleHttpResponseGetFlex1Value(actionType){
	var action = actionType;
	var typeSubmit='';
	document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
	document.forms['timeSheetForm'].elements['timeSheet.overTime'].className ='input-text';
	document.forms['timeSheetForm'].elements['timeSheet.overTime'].disabled=false;
	
	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].className ='input-text';
	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].disabled=false;
	try{
		typeSubmit=document.forms['timeSheetForm'].elements['typeSubmit'].value;
	}catch(e){}
	if (http21.readyState == 4){
       var results = http21.responseText;
       results = results.trim();
       results = results.replace('[','');
       results = results.replace(']','');
       if(results!='0'){
	   if(action=='S'){
			var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
			var userName=document.forms['timeSheetForm'].elements['timeSheet.userName'].value;
			var url="getSickHours.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&userName='+userName;
			http2.open("GET", url, true);
			http2.onreadystatechange = handleHttpResponseGetSickHours;
			http2.send(null);
		}else if(action=='P'){
			var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
			var userName=document.forms['timeSheetForm'].elements['timeSheet.userName'].value;
			var url="getPersonalHours.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&userName='+userName;
			http2.open("GET", url, true);
			http2.onreadystatechange = handleHttpResponseGetPersonalHours;
			http2.send(null);
		}else if(action=='V'){
			var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
			var userName=document.forms['timeSheetForm'].elements['timeSheet.userName'].value;
			var url="getVactionHours.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&userName='+userName;
			http2.open("GET", url, true);
			http2.onreadystatechange = handleHttpResponseGetVacationHours;
			http2.send(null);
		}else if(action=='R'){
			document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';	
		}else if((action=='U')&&(typeSubmit=='A')){
        	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
			document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
        	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';	
		}else{			
		if( action != 'C' && action != 'L' ){
		    var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
		    var url="sysDefaultData.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID);
		    http2.open("GET", url, true);
		    http2.onreadystatechange = function(){handleHttpResponse2(action);};
		    http2.send(null);
 		}
      }
     }else{
    	 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value=results;
    	 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-textUpper';
    	 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=true;    	 
     }
	}
  findDistributionCode(action);	
}


function handleHttpResponseGetSickHours(){
	if (http2.readyState == 4)
         {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
                var compDiv=res[1];
                var hrs=res[0];
                if(res[1].trim()=='SSC')
                {
                 if(res[0].trim() *1 >=8)
                {
                	//alert('Can not assign sick as sick Hours are already Used');
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	//document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value='${timeSheet.distributionCode}';
                }
                if(res[0].trim()* 1 < 8)
                {
                	alert('Sick hours left '+ res[0]+' Hrs');
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                  	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].disabled=true;
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].className ='input-textUpper';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].disabled=true;
                  	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-text';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=true;
                	
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].disabled=true;
                	
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].disabled=true; 
                
                }
                }
                
         }     

}

function handleHttpResponseGetPersonalHours(){
	if (http2.readyState == 4)
         {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
                var compDiv=res[1];
                var hrs=res[0];
               	if(res[1].trim()=='SSC'){
                  if(res[0].trim()>=8)
                {
                	alert('Personal Day already Used');
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value='${timeSheet.distributionCode}';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].disabled=true;
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].className ='input-textUpper';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].disabled=true;
                  	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-text';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=true; 
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
                	
                 	document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].disabled=true;
                	
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].disabled=true; 
                }else{
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='07:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='15:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='8';
                  	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='nput-text';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=false; 
                }
                }
                
         }     

}

function handleHttpResponseGetVacationHours(){
	if (http2.readyState == 4)
         {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
               	if(res[1].trim()=='SSC'){
                  if(res[0].trim()>=8)
                {
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='07:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='15:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='8';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-text';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=false;
                	
                }else{
                	alert('Vacation hours left '+ res[0]+' Hrs');
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	//document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value='${timeSheet.distributionCode}';
                }
                }
                
         }     

}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2(ac)
        {
	
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
                document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value = res[0];
                document.forms['timeSheetForm'].elements['timeSheet.endHours'].value = res[1];
                var tim1=document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
				var tim2=document.forms['timeSheetForm'].elements['timeSheet.endHours'].value;
				if(tim1 != '' && tim2 != ''){
				time1 = ((tim1.substring(0,tim1.indexOf(":"))*60)*1 + (tim1.substring(tim1.indexOf(":")+1, tim1.length))*1)/60;
				time2 = ((tim2.substring(0,tim2.indexOf(":"))*60)*1 + (tim2.substring(tim2.indexOf(":")+1, tim2.length))*1)/60;
				 var tobeRound=time2*1 - time1*1;
				 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value = Math.round(tobeRound*100)/100;
				 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-text';
             	 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=false;
				}
				if(ac=='Y')
				{
					document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
				}
				 var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;				
				 if(corpID=='SSCW' && ac=='K' ){
					 document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0'; 
             		}
        		}
        }
  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var http2 = getHTTPObject();

function getHTTPObject1()
    {
        var xmlhttp;
        if(window.XMLHttpRequest)
        {
            xmlhttp = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            if (!xmlhttp)
            {
                xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        return xmlhttp;
    }
var http21 = getHTTPObject1();
</script>
<script>
function fillAction(){
	var typeSubmit=document.forms['timeSheetForm'].elements['typeSubmit'].value;
	var ticket=document.forms['timeSheetForm'].elements['timeSheet.ticket'].value;
	if(document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value != '' || document.forms['timeSheetForm'].elements['timeSheet.endHours'].value != ''){
		if(document.forms['timeSheetForm'].elements['timeSheet.action'].value == '' && ticket=='0'){
			alert("Please select the appropriate timesheet action for ABSENT");
			return false;
		}else if(document.forms['timeSheetForm'].elements['timeSheet.action'].value == ''){
			alert("Please select the appropiate timesheet action");
			return false;
		}
		else{
			if(document.forms['timeSheetForm'].elements['timeSheet.action'].value == 'L' && (document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value*1) >.5 ){
				var agree = confirm("Lunch Hrs more than 1/2 Hrs, press OK to continue");
				if(!agree){
					return false;
				}
			}
		}
	}	
	if(typeSubmit!='A')
		return checkBEHours();
	else
		return true;
}

function increasePayRate(){
if(document.forms['timeSheetForm'].elements['timeSheet.crewType'].value == 'HE'){
	if(!('${timeSheet.differentPayBasis}' == 'B')){
	if(document.forms['timeSheetForm'].elements['timeSheet.differentPayBasis'].value == 'B'){
		var tobeRound=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value * 1 + 0.45;
		document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value = Math.round(tobeRound*1000)/1000;
		var tobeRound1=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value * 1.5;
		document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value = Math.round(tobeRound1*1000)/1000;
		var tobeRound2=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value * 2;
		document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value =Math.round(tobeRound2*1000)/1000;
	}else{
	
		document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value = Math.round('${timeSheet.regularPayPerHour}'*1000)/1000;
		document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value =Math.round('${timeSheet.overTimePayPerHour}'*1000)/1000;
		document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value =Math.round('${timeSheet.doubleOverTimePayPerHour}'*1000)/1000;
	}
	}else{
	if(document.forms['timeSheetForm'].elements['timeSheet.differentPayBasis'].value == 'B'){
		document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value =Math.round('${timeSheet.regularPayPerHour}'*1000)/1000;
		document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value =Math.round('${timeSheet.overTimePayPerHour}'*1000)/1000;
		document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value =Math.round('${timeSheet.doubleOverTimePayPerHour}'*1000)/1000;
	}else{
		var tobeRound=${timeSheet.regularPayPerHour} * 1 - 0.45;
		document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value = Math.round(tobeRound*1000)/1000;
		var tobeRound1=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value * 1.5;
		document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value = Math.round(tobeRound1*1000)/1000;
		var tobeRound2=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value * 2;
		document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value = Math.round(tobeRound2*1000)/1000;
	}
	}
}
}

function checkBeginHours()
{
	var id1=document.forms['timeSheetForm'].elements['timeSheet.id'].value;
    var url="checkBeginHours.html?ajax=1&decorator=simple&popup=true&id1=" + encodeURI(id1);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponseForCheckBeginHours;
    http2.send(null);

}

function handleHttpResponseForCheckBeginHours()
{
	
	var beginHours = document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value;
	if (http2.readyState == 4)
 	{
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                //alert(results);
                if(beginHours<results)
                {
                	alert('The timesheet hours have already been assigned.');
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='';
                	document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].focus();
                	return false;
                }
	}
}

function adjustmentField()
{
	if(document.forms['timeSheetForm'].elements['timeSheet.adjustmentToRevenue'].value!='')
	{
		document.forms['timeSheetForm'].elements['timeSheet.adjustedBy'].value='<%=userName%>';
		
	}
	if(document.forms['timeSheetForm'].elements['timeSheet.adjustmentToRevenue'].value=='')
	{
		document.forms['timeSheetForm'].elements['timeSheet.adjustedBy'].value='';
	}
}



</script>
<SCRIPT LANGUAGE="JavaScript">

function trimTextField(){
	var paidRevenue=document.forms['timeSheetForm'].elements['timeSheet.paidRevenueShare'].value;
	var paidRevenue1 = Math.round(paidRevenue*1000)/1000;
	document.forms['timeSheetForm'].elements['timeSheet.paidRevenueShare'].value=paidRevenue1;
	var regularPayPerHour=document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value;
	var regularPayPerHour1=Math.round(regularPayPerHour*1000)/1000;
	document.forms['timeSheetForm'].elements['timeSheet.regularPayPerHour'].value=regularPayPerHour1;
	var overTimePayPerHour=document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value;
	var overTimePayPerHour1=Math.round(overTimePayPerHour*1000)/1000;
	document.forms['timeSheetForm'].elements['timeSheet.overTimePayPerHour'].value=overTimePayPerHour1;
	var doubleOverTimePayPerHour=document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value;
	var doubleOverTimePayPerHour1=Math.round(doubleOverTimePayPerHour*1000)/1000;
	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTimePayPerHour'].value=doubleOverTimePayPerHour1;
}

function findDistributionCode(actionType){
	var action = actionType;
	var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
    var url="getDistributionCode.html?ajax=1&decorator=simple&popup=true&actionCode="+ encodeURI(action);
    http3.open("GET", url, true);
    http3.onreadystatechange = function(){handleHttpResponseDistributionCode(action);};
    http3.send(null);
}

function handleHttpResponseDistributionCode(action){
	if (http3.readyState == 4)
 	{
                var results = http3.responseText;              
                results = results.trim();
              	results = results.replace('[','');
                results = results.replace(']','');
                //alert(results)
                if(results!=''){
                var distCode=document.getElementById("distributionCode"); 
				for(var i = 0; i < distCode.length; i++) {
					if(action!='C'){
						if(results == document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].options[i].value.trim()) {
							document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].options[i].selected="true";
						}
					}else{			
					if(document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].options[i].value.trim()=='') {
							document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].options[0].selected="true";
						}
					}
				}
                } 
    }
}

function checkDistributionCode()
{
	var action=document.forms['timeSheetForm'].elements['timeSheet.action'].value;
	
	var distributionCode= document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value;
	if(action!='C' && distributionCode=='')
	{
		alert('Distribution Code is required field');
		document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].focus();
		return false;
	}
	
}

function getHTTPObject1()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http3 = getHTTPObject1();
    
    function checkTicket()
    {
    	
    	var action=document.forms['timeSheetForm'].elements['timeSheet.action'].value;
    	var ticket=document.forms['timeSheetForm'].elements['timeSheet.ticket'].value;
    	
    	if(action=='C' && ticket=='0')
    	
    	{
    		alert('This timesheet action cannot be selected for ABSENT');
    		document.forms['timeSheetForm'].elements['timeSheet.action'].value='';
    		document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
    		document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked = true;
    		document.forms['timeSheetForm'].elements['timeSheet.calculationCode'].value='';
		    document.getElementById("lunchDiv").style.visibility = "hidden";
    		return false;
    	}
    
    }
    
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==109)|| (keyCode==190)|| (keyCode==110); 
	  
}
function onlyFloatWithSign(targetElement)
{   var i;
	var s = targetElement.value;
	var count = 0;
	var count1=0;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
         if(c == '.')
        {
        	count = count+1
        }
        if(c == '-')
        {
        	count1 = count1+1
        }
        if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) && ((c != '-') || (count1>'1'))) 
        {
        	alert("Enter valid Number");
	        document.getElementById(targetElement.id).value=0;
            document.getElementById(targetElement.id).select();
	        return false;
        }
    }
    return true;
}

function checkVacationHours(){
			var corpID = document.forms['timeSheetForm'].elements['timeSheet.corpID'].value;
			var userName=document.forms['timeSheetForm'].elements['timeSheet.userName'].value;
			var requestedRegHours=document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value;
			var action=document.forms['timeSheetForm'].elements['timeSheet.action'].value;
			if(action=='V'){
			var url="getVactionHours.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&userName='+userName;
			http2.open("GET", url, true);
			http2.onreadystatechange = function(){handleHttpResponseChecckRequestedVacationHours(requestedRegHours);};
			http2.send(null);
			findDistributionCode(action);
			}
			if(action=='S'){
			var url="getSickHours.html?ajax=1&decorator=simple&popup=true&crewCorpId=" + encodeURI(corpID)+'&userName='+userName;
			http2.open("GET", url, true);
			http2.onreadystatechange = function(){handleHttpResponseCheckRequestedSickHours(requestedRegHours);};
			http2.send(null);
			findDistributionCode(action);
			}
}
function handleHttpResponseChecckRequestedVacationHours(requestedRegHours){
	if (http2.readyState == 4)
         {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
               	if(res[1].trim()=='SSC'){
                  if(res[0].trim()>= requestedRegHours*1)
                {
                	
                	
                }else{
                	alert('Vacation hours left '+ res[0]+' Hrs');
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
                	return false;
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	//document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value='${timeSheet.distributionCode}';
                }
                }
                
         }     

}

function handleHttpResponseCheckRequestedSickHours(requestedRegHours){
	if (http2.readyState == 4)
         {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                res = results.split("#");
               	if(res[1].trim()=='SSC'){
                  if(res[0].trim()>= requestedRegHours*1)
                {
                	
                	
                }else{
                	alert('Sick hours left '+ res[0]+' Hrs');
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].value='00:00';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].value='00:00';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value='0';
                  	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.beginHours'].disabled=true;
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].className ='input-textUpper';
					document.forms['timeSheetForm'].elements['timeSheet.endHours'].disabled=true;
                  	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].className ='input-text';
                	document.forms['timeSheetForm'].elements['timeSheet.regularHours'].disabled=true;
                	
                 	document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.overTime'].disabled=true;
                	
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].className ='input-textUpper';
                	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].disabled=true; 
                	return false;
                	//document.forms['timeSheetForm'].elements['timeSheet.action'].value='${timeSheet.action}';
                	//document.forms['timeSheetForm'].elements['timeSheet.distributionCode'].value='${timeSheet.distributionCode}';
                }
                }
                
         }     

}
function checkoverTimeAnddoubleOverTime (){
	var requestedRegHours=document.forms['timeSheetForm'].elements['timeSheet.regularHours'].value;
	var action=document.forms['timeSheetForm'].elements['timeSheet.action'].value;
	if(action=='V' && requestedRegHours=='0'){
		document.forms['timeSheetForm'].elements['timeSheet.overTime'].value='0';
    	document.forms['timeSheetForm'].elements['timeSheet.doubleOverTime'].value='0';
	}
}
</script>

</head>

<s:form name="timeSheetForm" action="saveTimeSheet.html?filter=ShowAll" onsubmit="return submit_form()"  method="post">
<s:hidden name="editable" value="${editable}"/>
<s:hidden name="idList" />
<s:hidden name="timeSheet.id" />
<s:hidden name="timeSheet.corpID" />
<s:hidden name="timeSheet.warehouse" />
<s:hidden name="timeSheet.isCrewUsed" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="whoticket" value="<%=request.getParameter("whoticket") %>"/>
<s:hidden  name="whoticket" value="<%=request.getParameter("whoticket") %>"/>
<c:set var="whid" value="<%=request.getParameter("whid") %>"/>
<s:hidden  name="whid" value="<%=request.getParameter("whid") %>"/>
<s:hidden name="workDt" />
<s:hidden name="serv" />
<s:hidden name="wareHse" />
<s:hidden name="tktWareHse" />
<s:hidden name="shipper" />
<s:hidden name="crewNm" />
<s:hidden name="tkt" />
<s:hidden name="typeSubmit"/>
<s:hidden name="timeSheet.dailySheetNotes" />
<s:hidden name="timeSheet.crewEmail" />
<s:hidden name="timeSheet.integrationTool" />
<s:hidden name="timeSheet.integrationStatus" />
<div id="layer1" style="width:90%">
<div id="newmnav">
	<ul>
		<li id="newmnav1"><a class="current"><span>Time&nbsp;Sheet&nbsp;Details</span></a></li>
		<li><a onclick="window.open('auditList.html?id=${timeSheet.id}&tableName=timesheet&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
		</ul>
	</div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%">
		<tbody>
			<tr>
				<td style="width:100%">
				
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border='0' style="width:100%">
					<tbody>
						<tr><td height="2px"></td></tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Working&nbsp;On:&nbsp;</td>
							<c:if test="${not empty timeSheet.workDate}">
							<s:text id="timeSheetWorkDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="timeSheet.workDate"/></s:text>
							<td colspan="2"><s:textfield cssClass="input-textUpper" id="workDate" name="timeSheet.workDate" value="%{timeSheetWorkDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
							</c:if>
							<c:if test="${empty timeSheet.workDate}">
							<td colspan="2"><s:textfield cssClass="input-textUpper" id="workDate" name="timeSheet.workDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true" /></td>
							</c:if>
							<td class="listwhitetext" align="right" style="width:50px;">Done&nbsp;For&nbsp;The&nbsp;Day</td>
							<c:set var="doneForTheDay" value="false"/>
							<c:if test="${timeSheet.doneForTheDay}">
							<c:set var="doneForTheDay" value="true"/>
						</c:if>
							<td class="listwhitetext"><s:checkbox key="timeSheet.doneForTheDay" value="${doneForTheDay}" fieldValue="true" /></td>
							<td rowspan="6"  class="listwhitetext" >
							<div id="lunchDiv"  >
								<c:choose>
								<c:when test="${timeSheet.lunch == 'Lunch Included'}">
									<table><tr><td>
									<input id="saveTimeSheet_timeSheet_lunchNo lunch" type="radio" onclick="revisedRegularHours(this);" value="No lunch" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchNo lunch">No lunch</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ beginning" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included" name="timeSheet.lunch" checked="checked"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ beginning">Lunch Included (30 mins)</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ 1hr" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included @ 1hr" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ end">Lunch Included (1hr)</label></td></tr>
									
									</table>
								</c:when>
								<c:when test="${timeSheet.lunch == 'Lunch Included @ 1hr'}">
									<table><tr><td>
									<input id="saveTimeSheet_timeSheet_lunchNo lunch" type="radio" onclick="revisedRegularHours(this);" value="No lunch" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchNo lunch">No lunch</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ beginning" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included" name="timeSheet.lunch" />
									<label for="saveTimeSheet_timeSheet_lunchLunch @ beginning">Lunch Included (30 mins)</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ 1hr" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included @ 1hr" name="timeSheet.lunch" checked="checked"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ end">Lunch Included (1hr)</label></td></tr>
									</table>
								</c:when>
								<%--<c:when test="${timeSheet.lunch == 'Lunch @ end'}">
									<table><tr><td>
									<input id="saveTimeSheet_timeSheet_lunchNo lunch" type="radio" onclick="revisedRegularHours(this);" value="No lunch" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchNo lunch">No lunch</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ beginning" type="radio" onclick="revisedRegularHours(this);" value="Lunch @ beginning" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ beginning">Lunch @ beginning</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ end" type="radio" onclick="revisedRegularHours(this);" value="Lunch @ end" name="timeSheet.lunch" checked="checked"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ end">Lunch @ end</label></td></tr></table>
								</c:when>
								--%>
								<c:otherwise>
									<table><tr><td>
									<input id="saveTimeSheet_timeSheet_lunchNo lunch" type="radio" onclick="revisedRegularHours(this);" value="No lunch" name="timeSheet.lunch" checked="checked"/>
									<label for="saveTimeSheet_timeSheet_lunchNo lunch">No lunch</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ beginning" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ beginning">Lunch Included (30 mins)</label></td></tr>
									<tr><td>
									<input id="saveTimeSheet_timeSheet_lunchLunch @ 1hr" type="radio" onclick="revisedRegularHours(this);" value="Lunch Included @ 1hr" name="timeSheet.lunch"/>
									<label for="saveTimeSheet_timeSheet_lunchLunch @ end">Lunch Included (1hr)</label></td></tr>
									</table>
								</c:otherwise>
								</c:choose>
							</div>
							</td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Crew:&nbsp;</td>
							<td class="listwhitetext" colspan="2" width="27%"><s:textfield cssClass="input-textUpper"  cssStyle="width:246px" name="timeSheet.crewName" readonly="true"/></td>
							<td class="listwhitetext" align="right">UserName</td>
							<td class="listwhitetext" style="padding-left:6px"><s:textfield cssClass="input-text"  name="timeSheet.userName" readonly="true"/></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Time&nbsp;Sheet&nbsp;Action:&nbsp;<font color="red" size="2">*</font></td>
							<c:if test="${empty timeSheet.paidRevenueShare}" >
							<td class="listwhitetext" colspan="4"><s:select name="timeSheet.action" list="%{timetype}"  cssClass="list-menu" headerKey="" headerValue="" onchange="findDefaultTimeHours(); return checkTicket();"/></td>
							</c:if>
							<c:if test="${not empty timeSheet.paidRevenueShare}" >
							<td class="listwhitetext" colspan="4"><s:select name="timeSheet.action" cssClass="list-menu" list="%{timetype}" cssStyle="width:250px" onchange="findDefaultTimeHours(); return checkTicket();"/></td>
							</c:if>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Begin&nbsp;Hours:&nbsp;</td>
							<td colspan="6">
							<table cellspacing="0" cellpadding="0" style="margin:0px;padding:0px;">
							<tr>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.beginHours" size="5" maxlength="5" onchange = "completeTimeString();calcTimeHours();checkVacationHours();return checkBeginHours();" cssStyle="width:71px;"/><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default; ></td>
							<td class="listwhitetext" style="width:10px;"></td>
							<td class="listwhitetext" align="right">End&nbsp;Hours:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.endHours" size="5" maxlength="5" onchange = "completeTimeString();calcTimeHours();checkVacationHours();" cssStyle="width:71px;"/><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" ></td>
							<td class="listwhitetext"></td>
							</tr>
							</table>
							</td>
							
							<%-- <td><s:radio name="timeSheet.lunch" list="%{lunchOptions}" onclick="revisedRegularHours(this);"/></td>
							<td><s:checkbox key="timeSheet.lunch" value="${lunchFlag}" fieldValue="true" onclick="revisedRegularHours(this);"/></td> --%>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Crew&nbsp;Type:&nbsp;</td>
							<td class="listwhitetext" colspan="4"><s:select name="timeSheet.crewType" list="%{crewtype}" cssStyle="width:250px" cssClass="list-menu" headerKey="" headerValue="" /></td>
						
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; height:5px;"></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="center" colspan="15" class="vertlinedata" width="100%">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border='0'>
					<tbody>
						<tr><td height="2px"></td></tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Current&nbsp;Ticket#:&nbsp;</td>
							<td class="listwhitetext" colspan="2"><s:textfield cssClass="input-textUpper" cssStyle="width:130px" name="timeSheet.ticket" readonly="true"/></td>
							<td class="listwhitetext" align="right" >Shipper:&nbsp;</td>
							<td class="listwhitetext" colspan="2"><s:textfield cssClass="input-textUpper" cssStyle="width:245px"  name="timeSheet.shipper" readonly="true"/></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;">Contract:&nbsp;</td>
							<td class="listwhitetext" colspan="2"><s:textfield cssClass="input-textUpper" cssStyle="width:130px" name="timeSheet.contract" readonly="true"/></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; "><fmt:message key='timeSheet.distributionCode'/>:&nbsp;</td>
							
							<td class="listwhitetext" colspan="2">
							<div id="lunchDiv"  >
								<s:select cssClass="list-menu" id="distributionCode"  name="timeSheet.distributionCode" list="%{distcode}" cssStyle="width:134px" headerKey="" headerValue=""/>
							</div>
							
							</td>
							
							<td class="listwhitetext" align="right" style="width:130px; "><fmt:message key='timeSheet.calculationCode'/>:&nbsp;</td>
							<%-- <td class="listwhitetext" colspan="3"><s:select cssClass="list-menu"  name="timeSheet.calculationCode" list="%{revcalc}" headerKey="" headerValue=""/></td> --%>
							<td class="listwhitetext" colspan="3"><s:select cssClass="list-menu"  name="timeSheet.calculationCode" list="%{revcalc}" cssStyle="width:250px" headerKey="" headerValue=""/></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; ">&nbsp;</td>
							<td class="listwhitetext">Regular</td>
							<td class="listwhitetext">Overtime</td>
							<td class="listwhitetext">Double&nbsp;Overtime</td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; ">Hours:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.regularHours" id="regularHours" size="5" maxlength="5" onkeydown="return onlyRateAllowed(event)" onkeyup="valid(this,'special')" onchange="checkVacationHours();onlyFloatWithSign(this);" /></td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.overTime" id="overTime" size="5" maxlength="5" onkeydown="return onlyRateAllowed(event)" onkeyup="valid(this,'special')" onchange="checkoverTimeAnddoubleOverTime();onlyFloatWithSign(this);" /></td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.doubleOverTime" id="doubleOverTime" size="5" maxlength="5" onkeydown="return onlyRateAllowed(event)" onkeyup="valid(this,'special')" onchange="checkoverTimeAnddoubleOverTime();onlyFloatWithSign(this);" /></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; ">Pay&nbsp;Per&nbsp;Hour:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-textUpper"  name="timeSheet.regularPayPerHour" size="5" maxlength="5" readonly="true"/></td>
							<td class="listwhitetext"><s:textfield cssClass="input-textUpper"  name="timeSheet.overTimePayPerHour" size="5" maxlength="5" readonly="true"/></td>
							<td class="listwhitetext"><s:textfield cssClass="input-textUpper"  name="timeSheet.doubleOverTimePayPerHour" size="5" maxlength="5" readonly="true"/></td>
						</tr>
						
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="0" width="100%">
					<tbody>
						<tr><td class="listwhitetext" style="width:50px; "></td></tr>
						<tr></tr>
						<tr></tr>
						<tr>
							<td align="center" colspan="15" class="vertlinedata" width="100%">&nbsp;</td>
						</tr>
					</tbody>
				</table>
				<table class="detailTabLabel" cellspacing="0" cellpadding="2" border='0'>
					<tbody>
						<tr><td height="2px"></td></tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; ">Paid&nbsp;Revenue:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.paidRevenueShare" size="10" onchange="onlyFloatWithSign(this);" onkeyup="valid(this,'special')" onkeydown="return onlyRateAllowed(event)" cssStyle="width:100px;"/></td>
							<td class="listwhitetext" align="right" style="width:130px; ">Different&nbsp;Pay&nbsp;Basis:&nbsp;</td>
							<td class="listwhitetext"><s:select cssClass="list-menu" cssStyle="width:102px;" name="timeSheet.differentPayBasis" list="%{pay_ab}" headerKey="" headerValue="" onchange="increasePayRate();"/></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px; ">Adjustments:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.adjustmentToRevenue" maxlength="25" size="10" onchange="onlyFloatWithSign(this);adjustmentField();"  onkeyup="valid(this,'special')" onkeydown="return onlyRateAllowed(event)" cssStyle="width:100px;"/></td>
							<td class="listwhitetext" align="right" style="width:130px;">Adjusted&nbsp;By:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="width:100px;"  name="timeSheet.adjustedBy" /></td>
						</tr>
						<tr>
							<td class="listwhitetext" align="right" style="width:150px;"><fmt:message key='timeSheet.reasonForAdjustment'/>:&nbsp;</td>
							<td class="listwhitetext" colspan="3"><s:textfield cssClass="input-text"  cssStyle="width:340px;" size="35" name="timeSheet.reasonForAdjustment" /></td>
						</tr>
						<tr><td height="2px"></td></tr>
						<!--<tr>
							<td class="listwhitetext" align="right" style="width:50px; height:5px;"><fmt:message key='timeSheet.notes'/>:&nbsp;</td>
							<td class="listwhitetext"><s:textfield cssClass="input-text"  name="timeSheet.notes"/></td>
						</tr>
					
						--><tr>
							
						</tr>
					</tbody>
				</table>
				
			</td>
		</tr>
	</tbody>
</table>

</div>
<div class="bottom-header"><span></span></div>
</div>
</div>

 		  
   <tr><td>
 		  <table class="detailTabLabel" border="0" >
				<tbody>
					<tr>
					   <td align="left" class="listwhitetext" width="30px"></td>
						<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
							<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:120px">
							<fmt:formatDate var="timeSheetCreatedOnFormattedValue" value="${timeSheet.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="timeSheet.createdOn" value="${timeSheetCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${timeSheet.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty timeSheet.id}">
								<s:hidden name="timeSheet.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{timeSheet.createdBy}"/></td>
							</c:if>
							<c:if test="${empty timeSheet.id}">
								<s:hidden name="timeSheet.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="timeSheetupdatedOnFormattedValue" value="${timeSheet.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="timeSheet.updatedOn" value="${timeSheetupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${timeSheet.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty timeSheet.id}">
								<s:hidden name="timeSheet.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{timeSheet.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty timeSheet.id}">
								<s:hidden name="timeSheet.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					<tr><td align="left" height="5px"></td></tr>
				</tbody>
			</table></td></tr>
					<table class="detailTabLabel" style="width:200px">
					<tbody>
						<tr>
							<td align="left"><s:submit name="save" cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="button.save" onmouseover="completeTimeString();" onclick="return checkVacationHours(),IsValidTime();"/></td>
							<c:if test="${whoticket=='ticketCrews'}">
							<td align="left"><input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/workTicketCrews.html?id=${whid}"/>'" /></td>
							</c:if>
							<c:if test="${whoticket!='ticketCrews'}">
							<td align="left"><input type="button" class="cssbutton1" value="Cancel" size="20" style="width:55px; height:25px" onclick="location.href='<c:url value="/searchCrewTickets.html?workDt=${workDt }&serv=${serv }&wareHse=${wareHse }&shipper=${shipper }&crewNm=${crewNm }&tkt=${tkt }&tktWareHse=${tktWareHse}&absentId=${absentId}&filter=ShowAll"/>'" /></td>
							</c:if>
							<td align="left"><s:reset cssClass="cssbutton1" cssStyle="width:55px; height:25px" key="Reset" /></td>
						</tr>
					</tbody>
				</table>

</s:form>
<script>
<c:if test="${hitFlag=='1' && whoticket=='ticketCrews'}">
<c:redirect url="/workTicketCrews.html?id=${whid}"/>
</c:if>
try{
if(document.forms['timeSheetForm'].elements['timeSheet.action'].value == 'C'){}else{
	document.forms['timeSheetForm'].elements['timeSheet.lunch'][0].checked = true;
	document.getElementById("lunchDiv").style.visibility = "hidden";
}
}
catch(e){}
try{
trimTextField();
}
catch(e){}
try{
<c:if test="${editable=='No'}">
	for(i=0;i<=100;i++)
			{
					document.forms['timeSheetForm'].elements[i].disabled = true;
			}
</c:if> 
}
catch(e){}
</script>
<script>
//try{
	//findDefaultTimeHours();
//}catch(e){
//}
</script>
<script type="text/javascript">
document.getElementById('regularHours').addEventListener('keypress', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});
document.getElementById('overTime').addEventListener('keypress', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});
document.getElementById('doubleOverTime').addEventListener('keypress', function(event) {
    if (event.keyCode == 13) {
        event.preventDefault();
    }
});
</script>