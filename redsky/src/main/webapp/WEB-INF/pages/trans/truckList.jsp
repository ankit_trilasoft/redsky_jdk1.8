<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="truckList.title"/></title>   
    <meta name="heading" content="<fmt:message key='truckList.heading'/>"/>   
    
 
 <script language="javascript" type="text/javascript">

function clear_fields(){
	document.forms['searchForm'].elements['truck.localNumber'].value = "";
	document.forms['searchForm'].elements['truck.warehouse'].value = "";
	document.forms['searchForm'].elements['truck.description'].value = "";
	document.forms['searchForm'].elements['truck.type'].value = "";
	document.forms['searchForm'].elements['truck.state'].value = "";
	document.forms['searchForm'].elements['truck.tagNumber'].value = "";
	document.forms['searchForm'].elements['truck.ownerPayTo'].value = "";
	document.forms['searchForm'].elements['truck.truckStatus'].checked = false;
	document.forms['searchForm'].elements['dotInspFlag'].checked = false;
}

function searchTruckList(type)
{
	 document.forms['searchForm'].elements['myListFor'].value=parseInt(type);
	document.forms['searchForm'].submit();
	}
function setListFor(){
	document.forms['searchForm'].elements['myListFor'].value=0;
}

</script>
 <script language="javascript" type="text/javascript">
 <c:set value="false" var="dueInspFlag"/>
<configByCorp:fieldVisibility componentId="component.truckingOps.OnBasisOfDotInspDue">
<c:set value="true" var="dueInspFlag"/>
 </configByCorp:fieldVisibility>
</script>	
<style>	
span.pagelinks {
display:block;
font-size:0.90em;
margin-bottom:2px;
!margin-bottom:2px;
margin-top:-6px;
!margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
font-size:.85em;
}
a.resetLine:hover {color:#003366;}
</style>
</head>
<c:set var="fld_code" value="<%=request.getParameter("fld_code") %>" />
<c:set var="fld_description" value="<%=request.getParameter("fld_description") %>" />
  
<s:form cssClass="form_magn" id="searchForm" action='${empty param.popup?"searchTrucks.html":"searchTrucks.html?decorator=popup&popup=true"}' method="post" validate="true"> 
<s:hidden name="myListFor" />
<c:set var="buttons">   
    <input type="button" class="cssbuttonA" style="width:55px; height:25px" onclick="location.href='<c:url value="/editTruck.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" onclick="setListFor();" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:58px;" onclick="clear_fields();"/> 
</c:set> 
  
<div id="otabs">
<ul>
<li><a class="current"><span>Search</span></a></li>
</ul>
</div>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden name="truck.truckStatus" value="true"/>
	<s:hidden name="dotInspFlag" />
	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	
</c:if>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table class="table" style="width:100%;"><!-- sandeep -->
	<thead>
		<tr>
		    <th align="center"><fmt:message key="truck.localNumber"/>
		    <th align="center"><fmt:message key="truck.type"/></th>
		    <th align="center">Owner</th>		
			<th align="center"><fmt:message key="truck.description"/>				
			<th align="center">State&nbsp;Code</th>	
			<th align="center">License&nbsp;#&nbsp;/Tag&nbsp;#</th>
			<th align="center"><fmt:message key="truck.warehouse"/>			
		</tr>		
	</thead>	
	<tbody>
		<tr>
		    <td width="" align="left"><s:textfield name="truck.localNumber" required="true" cssClass="input-text" size="20"/></td>
            <td width="" align="left"><s:select cssClass="list-menu" name="truck.type" list="%{type}" headerKey="" headerValue="" cssStyle="width:148px"/></td>
            <td width="" align="left"><s:textfield name="truck.ownerPayTo" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.description" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.state" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.tagNumber" required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="truck.warehouse" required="true" cssClass="input-text" size="20"/></td>
		 	
		</tr>
		<tr>
			<!-- <td><a onclick="dueSixtyDay();"><font color="green">Due 60 Days (N)</font></font></a></td>  -->
			<td colspan="5" style="margin:0;padding:0px;border:none;text-align:left;">			
			<table style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0">
			 <tr>
			<configByCorp:fieldVisibility componentId="component.truckingOps.OnBasisOfDotInspDue">
			 <td style="border:none;text-align:left;padding-right:60px;"><font color="green"><a class="resetLine" href="#" onclick="searchTruckList('60');" oncontextmenu="return false;"><font color="#green">Due 60 Days (${counterSixty})</font></a></font></td> 
			 <td style="border:none;text-align:left;padding-right:60px;"><font color="green"><a class="resetLine" href="#" onclick="searchTruckList('30');" oncontextmenu="return false;"><font color="A9AE35">Due 30 Days (${counterThirty})</font></a></font></td>
			 <td style="border:none;text-align:left;padding-right:60px;"><font color="green"><a class="resetLine" href="#" onclick="searchTruckList('1');" oncontextmenu="return false;"searchTruckList('60');><font color="red">Over Due (${counterDueDate})</font></a></font></td>
			</configByCorp:fieldVisibility>
			</tr>
			</table>
			</td>
			<td colspan="2" style="margin:0;padding:0px 0px 0px 50px;border:none;text-align:right;">	
			<div style="float:right;">		
			<table style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0">
			 <tr>
			 <c:if test="${empty param.popup}">
		 	 <td align="right" style="text-align:right;border:hidden;" class="listwhitetext">Active</td>		    
					<c:set var="isActive" value="false"/>
					<c:if test="${truckStatus == 'A'}">
						<c:set var="isActive" value="true"/>
					</c:if>
					<td style="border:hidden;"><s:checkbox key="truck.truckStatus" value="${isActive}" fieldValue="true"  /></td>
					 <c:if test="${dueInspFlag eq true }">
					 <td align="right" style="text-align:right;border:hidden;" class="listwhitetext">DOT Inspection</td>		    
					<td style="border:hidden;"><s:checkbox id="dotInspFlag1" name="dotInspFlag"  required="true" /></td>
					</c:if>
					</c:if>
			<td align="center" style="text-align:right;border:hidden;"><c:out value="${searchbuttons}" escapeXml="false" /></td>
			</tr>
			</table>
			</div>
		</td>
		</tr>
	</tbody>
</table>
	</div>
<div class="bottom-header" style="!margin-top:50px;"><span></span></div>
</div>
</div>
</s:form>
<c:out value="${searchresults}" escapeXml="false" />  
<s:set name="truckList" value="truckList" scope="request"/>
<div id="newmnav">
	<ul>
	<li id="newmnav1" style="background:#FFF"><a class="current" style="margin-bottom:0px;"><span>Truck List</span></a></li>
	<c:if test="${fld_code!='accountLine.truckNumber' && fld_code!='subcontractorCharges.truckNo' && fld_code!='partnerPrivate.truckID' && fld_code!='miscellaneous.vanID' && fld_code!='truckingOperations.localTruckNumber'}">  
	<c:if test="${param.popup}">
	<li><a href="miscellaneousListPopUpForCarrier.html?partnerType=CR&decorator=popup&popup=true&fld_sixthDescription=${param.fld_sixthDescription}&fld_fifthDescription=${param.fld_fifthDescription}&fld_fourthDescription=${param.fld_fourthDescription}&fld_thirdDescription=${param.fld_thirdDescription}&fld_secondDescription=${param.fld_secondDescription}&fld_description=${param.fld_description}&fld_code=${param.fld_code}"><span>Carrier List</span></a></li>
	</ul>
	</c:if>
	</c:if>
</div>
<div class="spnblk">&nbsp;</div>
 <jsp:useBean id="now" class="java.util.Date" scope="request"/> 
 <fmt:parseNumber
    value="${ now.time /(1000 * 60 * 60 * 24)}"
    integerOnly="true" var="nowDays" scope="request"/>
<display:table name="truckList" class="table" requestURI="" id="truckList" export="${empty param.popup}" pagesize="25" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'>   
        <c:if test="${not empty truckList}">
      <c:if test="${not empty truckList.dotInspDue}">
      <fmt:parseNumber
    value="${ truckList.dotInspDue.time /(1000 * 60 * 60 * 24)}"
    integerOnly="true" var="otherDays" scope="page"/>
   <c:set value="${otherDays-nowDays}" var="dateDiff"/> 
   </c:if>
    <c:if test="${empty truckList.dotInspDue}">
     <c:set value="70" var="dateDiff"/> 
    </c:if>
     <c:choose>
    <c:when test="${dueInspFlag eq true && dateDiff ne null && dateDiff gt 29  && dateDiff lt 60 }">
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id" style="width:100px;background:#BFF6C2;"/>   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;background:#BFF6C2;"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type"  style="width:25px;background:#BFF6C2;"/>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;background:#BFF6C2;">
				    <c:if test="${truckList.truckStatus == 'A'}">
				    <img id="target" src="${pageContext.request.contextPath}/images/tick-icon.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				    </c:if>
					<c:if test="${truckList.truckStatus == 'I'}">
					<img id="target" src="${pageContext.request.contextPath}/images/C.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					</c:if>
   </display:column>
   </c:if>
    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;background:#BFF6C2;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;background:#BFF6C2;"/>
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" style="width:95px;background:#BFF6C2;" format="{0,date,dd-MMM-yyyy}"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;background:#BFF6C2;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;background:#BFF6C2;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse"  style="width:25px;background:#BFF6C2;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>   
    
    
    
    </c:when>
     <c:when test="${ dueInspFlag eq true && dateDiff ne null  && dateDiff lt 30  && dateDiff ge -1 }">
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id" style="width:100px;background:#E4D970;" />   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;background:#E4D970;"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type" style="width:25px;background:#E4D970;"/>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;background:#E4D970;">
				    <c:if test="${truckList.truckStatus == 'A'}">
				    <img id="target" src="${pageContext.request.contextPath}/images/tick-icon.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				    </c:if>
					<c:if test="${truckList.truckStatus == 'I'}">
					<img id="target" src="${pageContext.request.contextPath}/images/C.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					</c:if>
   </display:column>
   </c:if>
    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;background:#E4D970;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;background:#E4D970;"/>
    
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" format="{0,date,dd-MMM-yyyy}" style="width:95px;background:#E4D970;"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;background:#E4D970;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;background:#E4D970;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse" style="width:25px;background:#E4D970;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>   
    
    
    
    </c:when>
         <c:when test="${dueInspFlag ne null && dueInspFlag eq true && dateDiff lt -1 }">
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id" style="width:100px;background:#F59696;"/>   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;background:#F89696;"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type" style="width:25px;background:#F59696;"/>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;background:#F59696;">
				    <c:if test="${truckList.truckStatus == 'A'}">
				    <img id="target" src="${pageContext.request.contextPath}/images/tick-icon.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				    </c:if>
					<c:if test="${truckList.truckStatus == 'I'}">
					<img id="target" src="${pageContext.request.contextPath}/images/C.gif"  ALIGN=TOP />
					</c:if>
   </display:column>
   </c:if>
    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;background:#F59696;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;background:#F59696;"/>
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" format="{0,date,dd-MMM-yyyy}" style="width:95px;background:#F59696;"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;background:#F59696;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;background:#F59696;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse" style="width:25px;background:#F59696;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>   
    </c:when>
  
    <c:otherwise>
     <c:if test="${dueInspFlag == true}">
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id" style="width:100px;background:#FFFAFA;"/>   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;background:#FFFAFA;"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type" style="width:25px;background:#FFFAFA;"/>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;background:#FFFAFA;">
				    <c:if test="${truckList.truckStatus == 'A'}">
				    <img id="target" src="${pageContext.request.contextPath}/images/tick-icon.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				    </c:if>
					<c:if test="${truckList.truckStatus == 'I'}">
					<img id="target" src="${pageContext.request.contextPath}/images/C.gif"  ALIGN=TOP />
					</c:if>
   </display:column>
   </c:if>
    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;background:#FFFAFA;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;background:#FFFAFA;"/>
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" format="{0,date,dd-MMM-yyyy}" style="width:95px;background:#FFFAFA;"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;background:#FFFAFA;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;background:#FFFAFA;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse" style="width:25px;background:#FFFAFA;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>
    </c:if>
    <c:if test="${dueInspFlag == false}">
    <c:if test="${empty param.popup}">
    <display:column property="localNumber" sortable="true" titleKey="truck.localNumber" href="editTruck.html" paramId="id" paramProperty="id" style="width:100px;"/>   
    </c:if>
    <c:if test="${param.popup}">
    <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;"/>
    </c:if>
    <display:column property="type" sortable="true" titleKey="truck.type" style="width:25px;"/>
    <c:if test="${empty param.popup}">
      <display:column title="Active" style="width:25px;">
				    <c:if test="${truckList.truckStatus == 'A'}">
				    <img id="target" src="${pageContext.request.contextPath}/images/tick-icon.png" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				    </c:if>
					<c:if test="${truckList.truckStatus == 'I'}">
					<img id="target" src="${pageContext.request.contextPath}/images/C.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
					</c:if>
   </display:column>
   </c:if>
    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;"/>
     <c:if test="${dueInspFlag eq true }">
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" format="{0,date,dd-MMM-yyyy}" style="width:95px;"/>    
    </c:if>
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse" style="width:25px;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/> 
    </c:if>
     
</c:otherwise>
</c:choose>
</c:if>
 <c:if test="${empty truckList}">
  <display:column property="listLinkParams" sortable="true" titleKey="truck.localNumber" style="width:100px;background:#BFF6C2;"/>

    <display:column property="type" sortable="true" titleKey="truck.type"  style="width:25px;background:#BFF6C2;"/>
      <display:column title="Active" style="width:25px;background:#BFF6C2;">

   </display:column>

    <display:column property="ownerPayTo" sortable="true" title="Owner" style="width:25px;background:#BFF6C2;"/>    
    <display:column property="description" sortable="true" titleKey="truck.description" style="width:180px;background:#BFF6C2;"/>
    <display:column property="dotInspDue" sortable="true" title="DOT&nbsp;Inspection&nbsp;Due" style="width:85px;background:#BFF6C2;" format="{0,date,dd-MMM-yyyy}"/>    
    <display:column property="state" sortable="true" title="State&nbsp;Code" style="width:20px;background:#BFF6C2;"/>
    <display:column property="tagNumber" sortable="true" title="License&nbsp;#&nbsp;/Tag&nbsp;#" style="width:80px;background:#BFF6C2;"/>
    <display:column property="warehouse" sortable="true" titleKey="truck.warehouse"  style="width:25px;background:#BFF6C2;"/>    
    <display:setProperty name="paging.banner.item_name" value="truckList"/>  
    <display:setProperty name="paging.banner.items_name" value="truckList"/>
  
    <display:setProperty name="export.excel.filename" value="Trucking Ops List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Trucking Ops List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Trucking Ops List.pdf"/>
 </c:if>
</display:table>   
  </tr>
  </tbody>
  </table>
 <table style="margin:0px; padding:0px; border:none">
  <tr>
    <td> <c:if test="${empty param.popup}"> 
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
 </td>
   
  </tr>
 
</table>
 
  
  </div>
  
<script type="text/javascript">   
    document.forms['searchForm'].elements['truck.localNumber'].focus(); 
    highlightTableRows('truckList');   
</script>  