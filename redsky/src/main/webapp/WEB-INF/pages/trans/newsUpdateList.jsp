<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<head>
<style>
span.pagelinks {
	display: block;
	font-size: 0.95em;
	margin-bottom: 5px;
	margin-top: -18px;
	padding: 1px 0;
	text-align: right;
	width: 100%;
}

.table th.sorted,.table th.order2 {
	color: #15428B !important;
}
</style>
<script language="javascript" type="text/javascript">
function findToolTipDescriptionForNewsUpdate(id,position){	
	//var url="findToolTipNewsUpdate.html?id="+id+"&decorator=simple&popup=true";
   var url="findToolTipNewsUpdate.html?ajax=1&decorator=simple&popup=true&id="+id;
   ajax_showTooltip(url,position);	
		}
</script>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
<script language="JavaScript" type="text/javascript"
	SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/jscal2.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/border-radius.css'/>" />
<link rel="stylesheet" type="text/css"
	href="<c:url value='/styles/redsky/steel.css'/>" />
<title><fmt:message key="newsUpdateList.title" />
</title>
<meta name="heading"
	content="<fmt:message key='newsUpdateList.heading'/>" />
</head>

<%-- <s:form id="serviceForm3" action="newsUpdates.html?true&corpID=${sessionCorpID}" method="post"> --%>
<s:form id="serviceForm3" name="newsUpdateList"
	action='${empty param.popup?"newsUpdates.html":"newsUpdates.html?decorator=popup&popup=true"}'
	method="post">


	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden name="id" value="<%=request.getParameter("id")%>" />
	<s:hidden name="id1" value="<%=request.getParameter("id")%>" />
	<s:hidden name="newsUpdate.id" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
    <s:hidden name="addValues" />
	<s:hidden name="corpID" value="<%= request.getParameter("corpID") %>" />
	<c:set var="corpID" value="<%=request.getParameter("corpID") %>" />

	<c:set var="buttons">
		<input type="button" class="cssbuttonA" align="left"
			onclick="location.href='<c:url value="/editNewsUpdate.html?true&corpID=true','height=400,width=850,top=20, left=100, scrollbars=yes,resizable=yes"/>'"
			value="<fmt:message key="button.add"/>" />
<td> <input type="button" class="cssbutton" style="width:82px; height:25px" value="Upload" onclick="uploadData();"/> </td>
	</c:set>

	<div id="Layer1" style="width: 100%">
		<div id="otabs">
			<ul>
				<li><a class="current"><span>Search</span>
				</a>
				</li>
			</ul>
		</div>
		<div class="spnblk">&nbsp;</div>
		<div id="content" align="center">
			<div id="liquid-round-top">
				<div class="top" style="margin-top: 10px; ! margin-top: -4px;">
					<span></span>
				</div>
				<div class="center-content">
					<table class="table" width="99%">
						<thead>
							<tr>
								<th><fmt:message key="newsUpdate.module" />
								</th>
								<configByCorp:fieldVisibility
									componentId="component.field.newsUpdate.corpId">
									<th><fmt:message key="newsUpdate.corpId" />
									</th>
								</configByCorp:fieldVisibility>
								<th>Release&nbsp;Date</th>
								<configByCorp:fieldVisibility componentId="component.field.newsUpdate.visible">
								<th>Visible</th>
								</configByCorp:fieldVisibility>
								<th></th>
						</thead>
		</tr>
						<tr>
							<td><s:select cssClass="list-menu" name="newsUpdate.module"
									list="%{moduleReport}" headerKey="" headerValue=""
									cssStyle="width:150px" /></td>
							<configByCorp:fieldVisibility
								componentId="component.field.newsUpdate.corpId">
								<td><s:select cssClass="list-menu"
										cssStyle="width:100px;height:45px;overflow:auto;" id="corpid"
										name="newsUpdate.corpId" list="%{distinctCorpId}"
										value="%{selectedCorpIdList}" multiple="true" headerKey=""
										headerValue="" />
							<span style="position: relative; top: 15px; vertical-align: top;" class="listblacktext"> 
							        * Use Control + mouse for multiple selections</span>
								</td>
								<%-- <s:textfield name="newsUpdate.corpId" size="8" required="true" cssClass="input-text" /> --%>
							</configByCorp:fieldVisibility>
							<td><c:if test="${not empty newsUpdate.publishedDate}">
									<s:text id="customerFileMoveDateFormattedValue"
										name="${FormDateValue}">
										<s:param name="value" value="newsUpdate.publishedDate" />
									</s:text>
									<s:textfield cssClass="input-text" id="publishedDate"
										name="newsUpdate.publishedDate"
										value="%{customerFileMoveDateFormattedValue}" size="7"
										maxlength="11" onkeydown="return onlyDel(event,this)"/>
									<img id="publishedDate_trigger" style="vertical-align: bottom"
										src="${pageContext.request.contextPath}/images/calender.png"
										HEIGHT=20 WIDTH=20 />

								</c:if> <c:if test="${empty newsUpdate.publishedDate}">
									<s:textfield cssClass="input-text" id="publishedDate"
										name="newsUpdate.publishedDate" size="7" maxlength="11"
										onkeydown="return onlyDel(event,this)" />
									<img id="publishedDate_trigger" style="vertical-align: bottom"
										src="${pageContext.request.contextPath}/images/calender.png"
										HEIGHT=20 WIDTH=20 />

								</c:if></td>
								<configByCorp:fieldVisibility
								componentId="component.field.newsUpdate.visible">
								<td><s:checkbox key="activeStatus" value="true" cssStyle="vertical-align:middle; margin:5px 3px 3px 0px;"   /></td>
								</configByCorp:fieldVisibility>
							<td width="200px"><s:submit
									cssClass="cssbutton" cssStyle="width:57px; height:25px;"
									method="searchNewsUpdate" key="button.search" /> <input
								type="button" class="cssbutton" value="Clear"
								style="width: 57px; height: 25px;" onclick="clear_fields();" />
							</td>
						</tr>
					</table>
				</div>
				<div class="bottom-header">
					<span></span>
				</div>
			</div>
		</div>
	</div>
	<div id="newmnav" style="width: 100%">
	<c:if test="${addValues=='true' }">
		<ul>
			<li><a href="newsUpdates.html?decorator=popup&popup=true" ><span>Release&nbsp;Updates&nbsp;List</span>
			 <li style="background:#FFF " id="newmnav1"><a class="current" href="newsUpdates.html?addValues=true&decorator=popup&popup=true" ><span>All&nbsp;Updates&nbsp; </span>
			</a>
			</li>
		</ul>
		</c:if>
		<c:if test="${addValues=='false' }">
		<ul>
			<li style="background:#FFF " id="newmnav1"><a class="current" href="newsUpdates.html?decorator=popup&popup=true" ><span>Release&nbsp;Updates&nbsp;List</span>
			 <li ><a href="newsUpdates.html?addValues=true&decorator=popup&popup=true" ><span>All&nbsp;Updates&nbsp; </span>
			</a>
			</li>
		</ul>
		</c:if>
	</div>
	<div class="spn">&nbsp;</div>

	<s:set name="newsUpdates" value="newsUpdates" scope="request" />
	<display:table name="newsUpdates" class="table" requestURI=""
		id="newsUpdateList" export="false" defaultsort="1"
		defaultorder="descending" pagesize="25"
		style="width:100%;margin-top:0px;!margin-top:8px; ">

		<c:if test="${corpID=='TSFT' }">
			<display:column property="publishedDate" style="width:50px;"
				title="Release Date"
				href="editNewsUpdate.html?true&corpID=${corpID}" paramId="id"
				paramProperty="id" format="{0,date,dd-MMM-yyyy}" />
		</c:if>
		<c:if test="${corpID!='TSFT' }">
			<display:column property="publishedDate" style="width:50px;"
				title="Release Date" paramId="id" paramProperty="id"
				format="{0,date,dd-MMM-yyyy}" />
		</c:if>
		<c:if test="${sessionCorpID=='TSFT' }">
			<display:column property="corpId" title="Corp Id" paramId="id"
				paramProperty="id" style="width:50px;" />
		</c:if>
		<display:column title="Module" style="width:70px;">
			<c:forEach var="entry" items="${moduleReport}">
				<c:if test="${newsUpdateList.module==entry.key}">
					<c:out value="${entry.value}" />
				</c:if>
			</c:forEach>
		</display:column>
		
		<display:column title="Category" style="width:70px;">
			<c:forEach var="entry" items="${categoryReport}">
				<c:if test="${newsUpdateList.category==entry.key}">
					<c:out value="${entry.value}" />
				</c:if>
			</c:forEach>
		</display:column>

		<c:if test="${sessionCorpID=='TSFT' }">
			<display:column property="ticketNo" sortable="true" title="Ticket #"
				style="width:50px;" paramId="id" paramProperty="id" />
			<display:column property="functionality"
				titleKey="newsUpdate.functionality" style="width:150px;"
				url="/editNewsUpdate.html?from=list&id1=${newsUpdate.id }&imageId=${imageId}&fieldId=${fieldId}&true"
				paramId="id" paramProperty="id" />
		</c:if>
		<c:if test="${sessionCorpID!='TSFT' }">
			<display:column property="functionality"
				titleKey="newsUpdate.functionality" style="width:150px;"
				paramId="id" paramProperty="id" />
		</c:if>
		<c:if test="${sessionCorpID=='TSFT' }">
			<display:column sortable="true" title="Description">
				<c:if test="${newsUpdateList.details != ''}">
					<c:if test="${fn:length(newsUpdateList.details) > 100}">
						<c:set var="abc"
							value="${fn:substring(newsUpdateList.details, 0, 100)}" />
						<div align="left"
							onmouseover="findToolTipDescriptionForNewsUpdate('${newsUpdateList.id}',this);"
							onmouseout="ajax_hideTooltip();">
							<c:out value="${abc}" />
							&nbsp;.....
						</div>
					</c:if>
					<c:if test="${fn:length(newsUpdateList.details) <= 100}">
						<c:set var="abc"
							value="${fn:substring(newsUpdateList.details, 0, 100)}" />
						<div align="left">
							<c:out value="${abc}" />
						</div>
					</c:if>
				</c:if>
			</display:column>
		</c:if>
		<c:if test="${sessionCorpID!='TSFT' }">
			<display:column property="details" title="Description"
				style="width:200px;" maxLength="150" />
		</c:if>

		<%--   <display:column property="fileName" title="File Name" style="width:150px;" maxLength="55" /> --%>
		<display:column title="Release Note" style="width:150px;"
			maxLength="55">
			<a
				onclick="javascript:openWindow('NewsUpdateResourceMgmtImageServletAction.html?id=${newsUpdateList.id}&decorator=popup&popup=true',900,600);">
				<c:out value="${newsUpdateList.fileName}" escapeXml="false" />
			</a>
		</display:column>
		<configByCorp:fieldVisibility componentId="component.field.newsUpdate.visible">
		<display:column title="Visible"   style="margin: 0px 0px 0px 8px;text-align:center;">
	   <c:if test="${newsUpdateList.visible==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${newsUpdateList.visible==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
	</configByCorp:fieldVisibility>
		<display:column title="Partial"   style="width:50px;padding:0px;">
	<c:if test="${newsUpdateList.status}">
        <input type="checkbox"   checked="checked" disabled="disabled"   />
        </c:if>
        </display:column>

		<c:if test="${sessionCorpID=='TSFT' }">
			<display:column title="Remove" style="width:45px;">
				<a><img align="middle"
					onclick="confirmSubmit(${newsUpdateList.id});"
					style="margin: 0px 0px 0px 8px;" src="images/recycle.gif" />
				</a>
			</display:column>
		</c:if>
	</display:table>

	<c:if test="${sessionCorpID=='TSFT' }">
		<tr>
			<td align="left" style="margin-left: 50px;"><c:out
					value="${buttons}" escapeXml="false" />
			</td>
		</tr>
	</c:if>
	<c:if test="${sessionCorpID!='TSFT' }">
		<tr>
			<td>
			<c:if test="${updateFlag == 'true'}">
			<input type="button" class="cssbuttonA"
				style="margin-right: 5px; height: 25px; width: 55px; font-size: 15"
				value="Close" onclick="opener.location.reload();window.close();" />
				</c:if>
				<c:if test="${updateFlag == 'false'}">
				<input type="button" class="cssbuttonA"
				style="margin-right: 5px; height: 25px; width: 55px; font-size: 15"
				value="Close" onclick="window.close();" />
				</c:if>
			</td>
		</tr>
	</c:if>
</s:form>
<script type="text/javascript"> 
function uploadData(){

	openWindow('uploadXlsFileNewsUpdateList.html?&decorator=popup&popup=true',800,500);
	
}
function clear_fields(){
	document.forms['serviceForm3'].elements['newsUpdate.module'].value = "";
	document.forms['serviceForm3'].elements['newsUpdate.publishedDate'].value = "";
	if(document.forms['serviceForm3'].elements['newsUpdate.corpId'] != null && document.forms['serviceForm3'].elements['newsUpdate.corpId'] !=undefined){
		document.forms['serviceForm3'].elements['newsUpdate.corpId'].value = "";
	}
}

function confirmSubmit(targetElement){
	var agree=confirm("Are you sure to remove this row? ");
	var did = targetElement;
	if (agree){
		location.href="deleteUpdateDoc.html?id="+did;
		}
	else{
		return false;
	}
}
    //highlightTableRows("newsUpdateList"); 
</script>
<script type="text/javascript">
	setCalendarFunctionality();
</script>
<script type="text/javascript">
<c:if test="${updateFlag == 'true'}">
function RefreshParent() {
    if (window.opener != null && !window.opener.closed) {
        window.opener.location.reload();
    }
}
window.onbeforeunload = RefreshParent;
</c:if>
</script>
