<%@ include file="/common/taglibs.jsp"%> 
<%@page import="java.math.BigDecimal"%>

<script type="text/javascript">
function feedback(targetElement){
	var year = targetElement; 
	window.open('ratingFeedback.html?partnerCode=${partnerCode}&id=${id}&year='+year+'&decorator=popup&popup=true','','height=500,width=850,top=100, left=80, scrollbars=yes,resizable=no') ;
}

function changeimage(url,year){
	var imageelement = null;
	imageelement = window.document.getElementById("image_"+year);
	imageelement.src = url;
}
</script>
<% 
	BigDecimal pos6m = (BigDecimal)request.getAttribute("pos6");
	BigDecimal neg6m = (BigDecimal)request.getAttribute("neg6");
		
	BigDecimal pos1 = (BigDecimal)request.getAttribute("pos1");
	BigDecimal neg1 = (BigDecimal)request.getAttribute("neg1");
		
	BigDecimal pos2 = (BigDecimal)request.getAttribute("pos2");
	BigDecimal neg2 = (BigDecimal)request.getAttribute("neg2");
%>	
<c:set var="partnerCode" value="<%= request.getParameter("partnerCode")%>" />
<s:hidden name="partnerCode" value="<%= request.getParameter("partnerCode")%>" />
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="margin: 0px; padding: 0px;">
	<tr style="margin: 0px; padding: 0px;">
		<td colspan="5" style="font-family: arial,verdana; font-size: 14px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>One Year Rating : ${rating}%</b></td>
	</tr>
	<tr style="margin: 0px; padding: 0px; height: 5px;">
		<td colspan="5"></td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em; " >
		<td width="70px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Period</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Positive</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Negative</b></td>
		<td align="center" width="80px" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Feedback</b></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><b>Rating</b></td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">Six Months</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=pos6m%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=neg6m%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_<%=pos6m%>" width="17" height="20" onclick="feedback(183)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>" onmouseover="changeimage('images/feedback-hover.png',<%=pos6m%>);" onmouseout="changeimage('images/feedback.png',<%=pos6m%>);" alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${rating6m}%</td>
	
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">One Year</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=pos1%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=neg1%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_<%=pos1%>" width="17" height="20" onclick="feedback(365)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>" onmouseover="changeimage('images/feedback-hover.png',<%=pos1%>);" onmouseout="changeimage('images/feedback.png',<%=pos1%>);" alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${rating}%</td>
	</tr>
	<tr style="margin: 0px; padding: 0px;line-height:1.5em ;">	
		<td style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">Two Years</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=pos2%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;"><%=neg2%></td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">
			<img class="openpopup" id="image_<%=pos2%>" width="17" height="20" onclick="feedback(730)" style="cursor: pointer;" src="<c:url value='images/feedback.png'/>" onmouseover="changeimage('images/feedback-hover.png',<%=pos2%>);" onmouseout="changeimage('images/feedback.png',<%=pos2%>);" alt="Show Feedback" title="Show Feedback"/>
		</td>
		<td align="center" style="font-family: arial,verdana; font-size: 11px; color: #003366; text-decoration: none; background-color: none; font-weight: normal;">${rating2y}%</td>
	</tr>	
</table>

