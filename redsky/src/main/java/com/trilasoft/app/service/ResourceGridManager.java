package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.ResourceGrid;

public interface ResourceGridManager extends GenericManager<ResourceGrid, Long>{
	
	public List findResource(String hub, Date workDate,String sessionCorpID);	
	public List getListById(Long id, String sessionCorpID);
}
