package com.trilasoft.app.dao.hibernate;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.springframework.dao.DataAccessException;

import com.trilasoft.app.dao.InventoryDataDao;
import com.trilasoft.app.dao.hibernate.ToDoRuleDaoHibernate.ToDoRuleDTO;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.InventoryData;
import com.trilasoft.app.model.ItemsJbkEquip;
import com.trilasoft.app.model.MaterialDto;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.WorkTicket; 
public class InventoryDataDaoHibernate extends GenericDaoHibernate<InventoryData, Long> implements InventoryDataDao  {
	public InventoryDataDaoHibernate() {
		super(InventoryData.class);
	}

	public List getListByType(String type,Long id) {
		if(!type.equals("")){
		return getHibernateTemplate().find("from InventoryData where customerFileID='"+id+"' and type='"+type+"' order by mode");
		}else{
		return getHibernateTemplate().find("from InventoryData where customerFileID='"+id+"' order by mode");
		}
	}
	public  class InvDTO {	
		private String quantitySum;
	    private String volSum;
	  	private String TotalSum;
		public String getQuantitySum() {
			return quantitySum;
		}
		public void setQuantitySum(String quantitySum) {
			this.quantitySum = quantitySum;
		}
		public String getVolSum() {
			return volSum;
		}
		public void setVolSum(String volSum) {
			this.volSum = volSum;
		}
		public String getTotalSum() {
			return TotalSum;
		}
		public void setTotalSum(String totalSum) {
			TotalSum = totalSum;
		}		
	  	
		
	}
	
	public InventoryData getLinkedInventoryData(Long invId, Long cId, Long sId){
		InventoryData inv = new InventoryData();
		List <InventoryData> l  = new ArrayList<InventoryData>();
		if(sId== null){
			l = getHibernateTemplate().find("from InventoryData  where networkSynchedId = '"+invId+"' and customerfileid=  '"+cId+"' and id != '"+invId+"' ");
		}else{
			l = getHibernateTemplate().find("from InventoryData  where networkSynchedId = '"+invId+"' and customerfileid=  '"+cId+"' and serviceorderid='"+sId+"' and id != '"+invId+"' ");
		}
		if(l!=null && !l.isEmpty()){
		for (InventoryData inventoryData : l) {
			inv = inventoryData;
		}
		}
		return inv;
	}
	
	
	public List getSumOfListByType(String type,Long id,Long soid) {
		String jbkEquipList="select sum(quantity) as quantity,sum(totalweight) as cft,sum(total) as total from inventorydata where customerFileID='"+id+"' order by mode";
		List listMasterList = new ArrayList();
		List list1 = new ArrayList();
		if(!type.equals("")){
			if(soid!=null && !(String.valueOf(soid).equals(""))){
				jbkEquipList="select sum(quantity) as quantity,sum(totalweight) as cft,sum(total) as total from inventorydata where customerFileID='"+id+"' and type='"+type+"' and serviceOrderID='"+soid+"' order by mode";
			}else{
			jbkEquipList="select sum(quantity) as quantity,sum(totalweight) as cft,sum(total) as total from inventorydata where customerFileID='"+id+"' and type='"+type+"' order by mode";
			}
		}else{
			if(soid!=null && !(String.valueOf(soid).equals(""))){
			jbkEquipList="select sum(quantity) as quantity,sum(totalweight) as cft,sum(total) as total from inventorydata where customerFileID='"+id+"' and serviceOrderID='"+soid+"' order by mode";
			}else{
				jbkEquipList="select sum(quantity) as quantity,sum(totalweight) as cft,sum(total) as total from inventorydata where customerFileID='"+id+"' order by mode";	
			}
		}
		listMasterList=this.getSession().createSQLQuery(jbkEquipList).list();	
		Iterator it=listMasterList.iterator();
		while(it.hasNext()){
	           Object []row= (Object [])it.next();
	           InvDTO materialDto=new InvDTO();	           
	           if(row[0] == null){materialDto.setQuantitySum("0.0");}else{materialDto.setQuantitySum(row[0].toString());}
	           if(row[1] == null){materialDto.setVolSum("0.00");}else{materialDto.setVolSum(row[1].toString());
	           }
	           if(row[2] == null){materialDto.setTotalSum("0.00");}else{materialDto.setTotalSum(row[2].toString());
	           }
	           list1.add(materialDto);
	           }
		if(list1.isEmpty()){
			return null;
		}else{
			return list1;
		}
	}
	public List getInventoryListForSO(Long sid,Long cid){
		return getHibernateTemplate().find("from InventoryData where customerFileID='"+cid+"' and serviceOrderID='"+sid+"' order by mode");
	}
	public List getInventoryListForSOByType(Long id,Long cid,String type){
		return getHibernateTemplate().find("from InventoryData where customerFileID='"+cid+"' and serviceOrderID='"+id+"' and type='"+type+"' order by mode");
	}
	public String updateEstimatedVolumeCft(int volumeAir,int volumeSea,int volumeRoad,int volumeStorage,Long cid){
	List list=getHibernateTemplate().find("from ServiceOrder where customerFileId='"+cid+"'");
	String Air="";
	String Sea="";
	String Truck="";
	String Storage="";
	String checkModeForServiceOrder="";
	if(!(list.isEmpty()) && !(list.get(0)==null)){
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			ServiceOrder serviceorder = (ServiceOrder) itr.next();
			if(serviceorder.getMode().equalsIgnoreCase("Air")){
			 int i=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+volumeAir+"' where id="+serviceorder.getId());
			 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+serviceorder.getId()+"' where customerFileID='"+cid+"' and mode in ('Air','AIR')");
			 Air="Air"; 
			}
			if(serviceorder.getMode().equalsIgnoreCase("Sea")){
				 int i=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+volumeSea+"' where id="+serviceorder.getId());
				 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+serviceorder.getId()+"' where customerFileID='"+cid+"' and mode in ('Sea','SEA')");
				 Sea="Sea";  	
			}
			if(serviceorder.getMode().equalsIgnoreCase("TRU")){
				 int i=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+volumeRoad+"' where id="+serviceorder.getId());
				 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+serviceorder.getId()+"' where customerFileID='"+cid+"' and mode='TRU'");
				 Truck="Truck"; 	
			}
			if(serviceorder.getMode().equalsIgnoreCase("STO")){
				 int i=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+volumeStorage+"' where id="+serviceorder.getId());
				 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+serviceorder.getId()+"' where customerFileID='"+cid+"' and mode='STO'");
				 Storage="Storage"; 	
			}
		}
		if(volumeAir!=0 && Air.equals("")){
			checkModeForServiceOrder="Air";
		}
		if(volumeSea!=0 && Sea.equals("")){
			if(!(checkModeForServiceOrder.equals(""))){
			checkModeForServiceOrder=checkModeForServiceOrder+","+"Sea";
			}else{checkModeForServiceOrder="Sea";}
		}
		if(volumeRoad!=0 && Truck.equals("")){
			if(!(checkModeForServiceOrder.equals(""))){
			checkModeForServiceOrder=checkModeForServiceOrder+","+"Road";}
			else{
				checkModeForServiceOrder="Road";
			}
		}
		if(volumeStorage!=0 && Storage.equals("")){
			if(!(checkModeForServiceOrder.equals(""))){
			checkModeForServiceOrder=checkModeForServiceOrder+","+"Storage";}
			else{checkModeForServiceOrder="Storage";}
		}
		return  checkModeForServiceOrder;
	}else{return "false";}
	
	}
	public Boolean getCountForSameModeTypeSO(Long cid){
		List list = this.getSession().createSQLQuery("select (if(mode!='OTHER',mode,other)) from inventorydata where customerFileID='"+cid+"' and (serviceOrderID is  null or  serviceOrderID ='') and ntbsFlag is false").list();
		String mode="";
		String storageJob="";
		for (int i=0;i<list.size();i++){
		  if(i==0){
			  if(list.get(0).toString().substring(0, 3).equalsIgnoreCase("sto")){
				  storageJob=list.get(0).toString().substring(0, 3);
			  }else{
				  if(list.get(0).toString().length()>6){
					  mode = "'"+list.get(0).toString().substring(0, 3)+"'";
				  }else{
				  mode = "'"+list.get(0).toString()+"'";
				  }
			   }
		  }else{
			  if(list.get(i).toString().substring(0, 3).equalsIgnoreCase("sto")){
				  storageJob=list.get(i).toString().substring(0, 3);
			  }else{
				  if(mode.equalsIgnoreCase("")){
					  if(list.get(i).toString().length()>6){
						  mode = "'"+list.get(i).toString().substring(0, 3)+"'";
					  }else{
					  mode = "'"+list.get(i).toString()+"'";
					  }
				  }else{
					  if(list.get(i).toString().length()>6){
						  mode=mode +",'"+ list.get(i).toString().substring(0, 3)+"'";
					  }else{
						  mode=mode +",'"+ list.get(i).toString()+"'";
					  }
				  }
			  }
		  }
		}
		List list2;
		if(!storageJob.equalsIgnoreCase("")){
			if(mode.equals("")){
				list2 =this.getSession().createSQLQuery("select count(*) from serviceorder where  customerFileId ='"+cid+"' and  job='"+storageJob+"' group by mode").list();
			}else{
				list2 =this.getSession().createSQLQuery("select count(*) from serviceorder where  customerFileId ='"+cid+"' and (mode in("+mode+") or job='"+storageJob+"') group by mode").list();
			}
		}else{
			if(mode.equals("")){list2 = new ArrayList();}
			else{
				list2 =this.getSession().createSQLQuery("select count(*) from serviceorder where  customerFileId ='"+cid+"' and mode in("+mode+") group by mode").list();
			}
		}
		if(!list2.isEmpty()&& list2.get(0)!= null){
		for (int i=0;i<list2.size();i++){
			  if(Integer.parseInt(list2.get(i).toString())>=1){
				 return false;
			  }
			}
		}
		 return true;
	}
	public List getSOListbyCF(Long cid){
		return getHibernateTemplate().find("from ServiceOrder where customerFileId='"+cid+"' order by mode");
	}
	 public List getDistinctMode(Long cid){
		 return this.getSession().createSQLQuery("select distinct(if(mode!='OTHER',mode,other)) from inventorydata where customerFileID='"+cid+"' and (serviceOrderID is  null or  serviceOrderID ='') and ntbsFlag is false order by mode").list();
	 }
	 public int countByIndividualMode(Long cid,String mode){
		 List list2 =this.getSession().createSQLQuery("select count(*) from serviceorder where  customerFileId ='"+cid+"'    and mode like '"+mode+"%'").list();
		 if(!list2.isEmpty() && list2.get(0)!=null ){
			 int count =Integer.parseInt(list2.get(0).toString());
			 return count;
		 }else{
			 return 0;
		 }
	 }
	 public void InventorySOMapping(Long cid,String modeSORelation,String modeWithOneSO,String sessionCorpID, String userName){
		 String unit = getHibernateTemplate().find("select volumeUnit from SystemDefault where corpID='"+sessionCorpID+"'").get(0).toString();
		 String[] modeSo= new String[99];
		 String[] modeSo2= new String[99];
		 if(modeSORelation!=null && !modeSORelation.equals("")){
		 modeSo=modeSORelation.split(",");
		 for(int i=0;i<modeSo.length;i++){
			 String SQLQurry="";
			 String[] shipMode= new String[3]; 
			 shipMode=modeSo[i].split(":");
			 List list;
			 if(shipMode[0].substring(0,3).equalsIgnoreCase("sto")){
				    list =this.getSession().createSQLQuery("select id from serviceorder where  customerFileId ='"+cid+"' and job ='"+shipMode[0].substring(0, 3)+"' and shipNumber='"+shipMode[1]+"'").list();
			   }else{
			    	list =this.getSession().createSQLQuery("select id from serviceorder where  customerFileId ='"+cid+"' and mode  like '"+shipMode[0].substring(0, 3)+"%' and shipNumber='"+shipMode[1]+"'").list();
			 }               
			 if(!(list.isEmpty()) && !(list.get(0)==null)){
				 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+list.get(0).toString()+"',updatedBy='"+userName+"',updatedOn=now()   where customerFileID='"+cid+"'  and (serviceOrderID is  null or  serviceOrderID ='')  and mode ='"+shipMode[0]+"' and  ntbsFlag is false");
				 if(i2==0){
					 getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+list.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where customerFileID='"+cid+"'  and (serviceOrderID is null or  serviceOrderID ='')  and mode ='other' and  other='"+shipMode[0]+"' and  ntbsFlag is false");
					 List listSum1=this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and mode ='"+shipMode[0].substring(0,3)+"' and  ntbsFlag is false").list();
					 List listsum =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and mode ='other' and  other='"+shipMode[0]+"' and  ntbsFlag is false").list();
					 					 
					 if(!(listsum.isEmpty()) && !(listsum.get(0)==null)){
						 if(listSum1.get(0)== null){
							 listSum1.set(0,0);
						 }
						String totalSum=  (new Float(Float.parseFloat(listsum.get(0).toString()) + Float.parseFloat(listSum1.get(0).toString()))).toString() ;
						if(unit.equalsIgnoreCase("Cft")){
							//int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+totalSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
							double temp = (Float.parseFloat(totalSum)) * 0.0283;
				    		temp= Math.round(temp*10000)/10000;
							//int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+totalSum+"' , estimateCubicMtr='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
							SQLQurry=SQLQurry+"estimateCubicFeet='"+totalSum+"' , estimateCubicMtr='"+temp+"' , ";
						}else{
						 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+totalSum+"',updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
						 double temp = (Float.parseFloat(totalSum)) * 35.3147;
				    	 temp= Math.round(temp*10000)/10000;
						 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+totalSum+"', estimateCubicFeet='"+temp+"',updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
						 SQLQurry=SQLQurry+"estimateCubicMtr='"+totalSum+"', estimateCubicFeet='"+temp+"', ";
						}
					}
					 List listWeightsum1 =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and mode ='"+shipMode[0].substring(0,3)+"' and  ntbsFlag is false").list();
					 List listWeightsum =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and mode ='other' and  other='"+shipMode[0]+"' and  ntbsFlag is false").list();
					    if(!(listWeightsum.isEmpty()) && !(listWeightsum.get(0)==null)){
					    	if(listWeightsum1.get(0)== null){
					    		listWeightsum1.set(0,0);
					    	}
					    	 String totalWeightSum=  (new Float(Float.parseFloat(listWeightsum1.get(0).toString()) + Float.parseFloat(listWeightsum.get(0).toString()))).toString() ;
					    	 if(unit.equalsIgnoreCase("Cft")){
					    		 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
					    		 double temp = (Float.parseFloat(totalWeightSum)) * 0.4536;
					    		 temp= Math.round(temp*10000)/10000;
					    		 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set  estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
					    		 SQLQurry=SQLQurry+"estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' , ";
					    	 }else{
					    		 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
					    		 double temp = (Float.parseFloat(totalWeightSum)) * 2.2046;
					    		 temp= Math.round(temp*10000)/10000;
					    		 //int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
					    		 SQLQurry=SQLQurry+"estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' , ";
					    	 }
					      }					 					 
				      }else{
					 List listSum1 =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and mode ='other' and  other like '"+shipMode[0]+"%' and  ntbsFlag is false").list();
					 List listsum =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and mode='"+shipMode[0]+"' and  ntbsFlag is false").list();
					 if(!(listsum.isEmpty()) && !(listsum.get(0)==null)){
						 if(listSum1.get(0)== null){
							 listSum1.set(0,0);
						 }
						 String totalSum=  (new Float(Float.parseFloat(listsum.get(0).toString()) + Float.parseFloat(listSum1.get(0).toString()))).toString() ;
						 if(unit.equalsIgnoreCase("Cft")){
							 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+totalSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
							 double temp = (Float.parseFloat(totalSum)) * 0.0283;
					    		temp= Math.round(temp*10000)/10000;
								//int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+totalSum+"' , estimateCubicMtr='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
					    		SQLQurry=SQLQurry+"estimateCubicFeet='"+totalSum+"' , estimateCubicMtr='"+temp+"' , ";
						 }else{
							 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+totalSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
							 double temp = (Float.parseFloat(totalSum)) * 35.3147;
					    	 temp= Math.round(temp*10000)/10000;
							 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+totalSum+"' , estimateCubicFeet='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
							 SQLQurry=SQLQurry+"estimateCubicMtr='"+totalSum+"' , estimateCubicFeet='"+temp+"' , ";
						 }
					}
					 List listWeightsum1 =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and mode ='other' and  other like '"+shipMode[0]+"%' and  ntbsFlag is false").list();
					 List listWeightsum =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and mode='"+shipMode[0]+"' and  ntbsFlag is false").list();
				     if(!(listWeightsum.isEmpty()) && !(listWeightsum.get(0)==null)){
				    	 if(listWeightsum1.get(0)== null){
					    		listWeightsum1.set(0,0);
					    }
				    	 String totalWeightSum=  (new Float(Float.parseFloat(listWeightsum1.get(0).toString()) + Float.parseFloat(listWeightsum.get(0).toString()))).toString() ;
				    	 if(unit.equalsIgnoreCase("Cft")){
				    		 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
				    		 double temp = (Float.parseFloat(totalWeightSum)) * 0.4536;
				    		 temp= Math.round(temp*10000)/10000;
				    		 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set  estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
				    		 SQLQurry=SQLQurry+"estimateGrossWeight='"+totalWeightSum+"', estimateTareWeight='0.00',estimatedNetWeight='"+totalWeightSum+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' , ";
				    	 }else{
				    		 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
				    		 double temp = (Float.parseFloat(totalWeightSum)) * 2.2046;
				    		 temp= Math.round(temp*10000)/10000;
				    		 //int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
				    		 SQLQurry=SQLQurry+"estimateGrossWeightKilo='"+totalWeightSum+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+totalWeightSum+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' , ";
				    	 }
				     }
				 }
				 int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set "+SQLQurry+"  updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
			 }
		 }}
		 if(modeWithOneSO!=null && !modeWithOneSO.equals("")){
		 modeSo2=modeWithOneSO.split(",");
		 for(int i=0;i<modeSo2.length;i++){	
			 String SQLQurry="";
			 List list =this.getSession().createSQLQuery("select id from serviceorder where  customerFileId ='"+cid+"' and mode ='"+modeSo2[i]+"'").list();
			 if(!(list.isEmpty()) && !(list.get(0)==null)){
				 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+list.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where customerFileID='"+cid+"'  and (serviceOrderID is  null or  serviceOrderID ='')  and mode ='"+modeSo2[i]+"'"); 
				 List listsum =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and mode='"+modeSo2[i]+"'").list();				 
				 if(!(listsum.isEmpty()) && !(listsum.get(0)==null)){
					 if(unit.equalsIgnoreCase("Cft")){
			    		//int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+listsum.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
			    		double temp = (Float.parseFloat(listsum.get(0).toString())) * 0.0283;
			    		temp= Math.round(temp*10000)/10000;
						//int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+listsum.get(0).toString()+"' , estimateCubicMtr='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
						SQLQurry=SQLQurry+"estimateCubicFeet='"+listsum.get(0).toString()+"' , estimateCubicMtr='"+temp+"' , ";
					 }else{
						 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+listsum.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
						 double temp = (Float.parseFloat(listsum.get(0).toString())) * 35.3147;
				    	 temp= Math.round(temp*10000)/10000;
						 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous  set estimateCubicMtr='"+listsum.get(0).toString()+"' , set estimateCubicFeet='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'"); 
						 SQLQurry=SQLQurry+"estimateCubicMtr='"+listsum.get(0).toString()+"' , set estimateCubicFeet='"+temp+"' , ";
					 }
			    }
			    List listWeightsum =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and mode='"+modeSo2[i]+"'").list();
			    if(!(listWeightsum.isEmpty()) && !(listWeightsum.get(0)==null)){
			    	if(unit.equalsIgnoreCase("Cft")){
			    		//int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
			    		 double temp = (Float.parseFloat(listWeightsum.get(0).toString())) * 0.4536;
			    		 temp= Math.round(temp*10000)/10000;
			    		 //int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
			    		 SQLQurry=SQLQurry+"estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' , estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' , ";
			    	}else{
			    		 //int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+listWeightsum.get(0).toString()+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+listWeightsum.get(0).toString()+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
			    		 double temp = (Float.parseFloat(listWeightsum.get(0).toString())) * 2.2046;
			    		 temp= Math.round(temp*10000)/10000;
			    		 //int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set  estimateGrossWeightKilo='"+listWeightsum.get(0).toString()+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+listWeightsum.get(0).toString()+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' ,updatedBy='"+userName+"',updatedOn=now() where id='"+list.get(0).toString()+"'");
			    		 SQLQurry=SQLQurry+"estimateGrossWeightKilo='"+listWeightsum.get(0).toString()+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+listWeightsum.get(0).toString()+"' , estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' , ";	
			    	}
			    }
			    int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set  "+SQLQurry+"   updatedBy='"+userName+"', updatedOn=now() where id='"+list.get(0).toString()+"'");
			 }
		 }
		 
	 }}
	 
	 public void updateOtherModeList(Long cid,String otherModeItem){
		 List list =this.getSession().createSQLQuery("select id from serviceorder where  customerFileId ='"+cid+"' and mode ='"+otherModeItem.trim().substring(0,3)+"'").list();
		 if(!(list.isEmpty()) && !(list.get(0)==null)){	 
			 int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+list.get(0).toString()+"' where customerFileID='"+cid+"' and  mode='other' and other ='"+otherModeItem+"'");
		 }
	 }
	 public int workTicketCount(Long id,String sessionCorpID){
		 List list2 =this.getSession().createSQLQuery("select count(*) from workticket where  serviceOrderId ='"+id+"' and service in ('PK','PL') and corpID ='"+sessionCorpID+"'").list();
		 if(!list2.isEmpty() && list2.get(0)!=null ){
			 int count =Integer.parseInt(list2.get(0).toString());
			 return count;
		 }else{
			 return 0;
		 }
	 }
	 public void tranferInventoryMaterial(Long id,Long cid,String sessionCorpID){
		 List list= getHibernateTemplate().find("from WorkTicket where serviceOrderId='"+cid+"' and service='PK'");
		 if(!list.isEmpty() && list.get(0)!=null ){
			 WorkTicket workTicket=(WorkTicket) list.get(0);
		 List inventoryList= getHibernateTemplate().find("from InventoryData where serviceOrderID='"+id+"' and customerFileID='"+cid+"' and type='M'");
		 Iterator itr= inventoryList.listIterator();
		 while(itr.hasNext()){
			 InventoryData inventoryData=(InventoryData) itr.next();
			 ItemsJbkEquip itemsJbkEquip = new ItemsJbkEquip();
			 itemsJbkEquip.setDescript(inventoryData.getAtricle());
			 itemsJbkEquip.setQty(Integer.parseInt(inventoryData.getQuantity()));
			 itemsJbkEquip.setTicket(workTicket.getTicket());
			 itemsJbkEquip.setWorkTicketID(workTicket.getId());
			 
		 }
		/* AccessInfo accInfo  = getAccessinfo( cid,  sessionCorpID,id);
		 if(accInfo !=null ){
			 workTicket.setSiteS(accInfo.getOriginResidenceType());
			 workTicket.setOrigMeters(accInfo.getOriginfloor());
			 workTicket.setOriginElevator(accInfo.getOriginElevatorType());
			 workTicket.setParkingS(accInfo.getOriginParkingType());
			 workTicket.setOriginShuttle(accInfo.getOriginShuttleRequired());
			 workTicket.setDest_farInMeters(accInfo.getOriginShuttleDistance());
		 }*/
		}
		 
	 }
	 
	 public WorkTicket getWorkTicketByTicket(String ticket, String sessionCorpID){
		 WorkTicket wkt = null;
		 try{
			 List l1 =  getHibernateTemplate().find(" from WorkTicket where ticket='"+ticket+"' and corpID='"+sessionCorpID+"' ");
			 if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
				 wkt = (WorkTicket) l1.get(0);
			 }
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 return wkt;
	 }
	 
	 public List getWorkTicket(Long id,String sessionCorpID){
		 return getHibernateTemplate().find("from WorkTicket where serviceOrderId='"+id+"' and service in ('PK','PL')");
	 }
	 public List getInventoryListByType(Long id,Long cid,String type){
		return getHibernateTemplate().find("from InventoryData where serviceOrderID='"+id+"' and customerFileID='"+cid+"' and type='"+type+"'");
	 }
	 public List getDistinctArticleInventoryListByType(Long id,Long cid,String type){
		// getHibernateTemplate().bulkUpdate("delete from InventoryData where serviceOrderID='"+id+"' and customerFileID='"+cid+"' and type='"+type+"'");
		//  return getHibernateTemplate().find("select atricle,count(atricle)  from InventoryData where serviceOrderID='"+id+"' and customerFileID='"+cid+"' and type='"+type+"' group by  atricle");
		 return getHibernateTemplate().find("select atricle,sum(quantity)  from InventoryData where serviceOrderID='"+id+"' and customerFileID='"+cid+"' and type='"+type+"' group by  atricle");
			
	 }
	 
	 public AccessInfo getAccessinfo(Long cid, String sessionCorpID,Long sid){
		 AccessInfo accInfo = null;
		 try {
			List l1 =  getHibernateTemplate().find("from AccessInfo where serviceOrderId='"+sid+"'");
			 if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
				 accInfo = (AccessInfo) l1.get(0);
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return accInfo;
	 }
	 
	 public AccessInfo getAccessinfoByCid(Long cid, String sessionCorpID){
		 AccessInfo accInfo = null;
		 try {
			List l1 =  getHibernateTemplate().find("from AccessInfo where customerFileId='"+cid+"'");
			 if(l1!=null && !l1.isEmpty() && l1.get(0)!=null ){
				 accInfo = (AccessInfo) l1.get(0);
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return accInfo;
	 }
	 public List getWorkTicketList(Long id,String sessionCorpID){
		 List list= new ArrayList();
		 return list;
	 }
	 public void setOtherModeInventorySOid(Long cid,String otherModeItemList){
		 String[] otherMode= otherModeItemList.split(",");
			for(int k=0;k<otherMode.length;k++){
				String modeItems=otherMode[k];
				 List list =this.getSession().createSQLQuery("select id from serviceorder where  customerFileId ='"+cid+"' and mode ='"+modeItems.substring(0, 3)+"'").list();
				 if(!(list.isEmpty()) && !(list.get(0)==null)){
					// int i2=getHibernateTemplate().bulkUpdate("update InventoryData set serviceOrderID='"+list.get(0).toString()+"' where customerFileID='"+cid+"' and mode ='other' and other='"+modeItems+"'"); 
					 List listsum =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and (mode='"+modeItems.substring(0, 3)+"' or (mode='other' and other='"+modeItems+"'))").list();				 
					    if(!(listsum.isEmpty()) && !(listsum.get(0)==null)){
					      int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+listsum.get(0).toString()+"' where id='"+list.get(0).toString()+"'");
						 }
					    List listWeightsum =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and (mode='"+modeItems.substring(0, 3)+"' or (mode='other' and other='"+modeItems+"'))").list();
					    if(!(listWeightsum.isEmpty()) && !(listWeightsum.get(0)==null)){
						      int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' where id='"+list.get(0).toString()+"'");
							 }
				 }
			}
	 }
	 
	 public InventoryData getSyncInventoryData(Long id, String corpID,Long networkSynchedId){
		 InventoryData invdata =  new InventoryData();
		 List l =  getHibernateTemplate().find("from InventoryData where   customerFileID='"+id+"' and corpId='"+corpID+"' and networkSynchedId='"+networkSynchedId+"'");
		 if(l!=null && !l.isEmpty() && l.get(0)!=null){
			 invdata = (InventoryData) l.get(0) ;
		 }
		 return invdata;
		}
	 
	   public String getSOTransferStatus(String type,Long cid,String sessionCorpID){
		   List list2 =this.getSession().createSQLQuery("select count(*) from inventorydata where  customerFileID ='"+cid+"' and type='"+type+"' and serviceOrderID is null and corpID ='"+sessionCorpID+"' and ntbsFlag is false").list();
			 if(!list2.isEmpty() && list2.get(0)!=null ){
				 int count =Integer.parseInt(list2.get(0).toString());
				 if(count<1){
				 return "N";
				 }else{
					return "Y";
				 }
			 }else{
				 return "Y";
			 }
	   }
	   public void updateWeghtAndVol(Long cid, Long sid,String sessionCorpID){
		   String unit = getHibernateTemplate().find("select volumeUnit from SystemDefault where corpID='"+sessionCorpID+"'").get(0).toString();
		   List listsum =this.getSession().createSQLQuery("select sum(total) from inventorydata where customerFileID='"+cid+"' and serviceOrderId='"+sid+"' and  ntbsFlag is false").list();				 
		    if(!(listsum.isEmpty()) && !(listsum.get(0)==null)){
		    	if(unit.equalsIgnoreCase("Cft")){
		  	      int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+listsum.get(0).toString()+"' where id='"+sid+"'");
		  	      double temp = (Float.parseFloat(listsum.get(0).toString())) * 0.0283;
		  	      temp= Math.round(temp*10000)/10000;
		  	      int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+temp+"' where id='"+sid+"'"); 
			   	} 
		    	else{
		    		 int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicMtr='"+listsum.get(0).toString()+"' where id='"+sid+"'");
					 double temp = (Float.parseFloat(listsum.get(0).toString())) * 35.3147;
			    	 temp= Math.round(temp*10000)/10000;
					 int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='"+temp+"' where id='"+sid+"'"); 
		    		
		    	}
		    	}else{
				 int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateCubicFeet='0.00',estimateCubicMtr='0.00' where id='"+sid+"'");
			 }
		    
		    List listWeightsum =this.getSession().createSQLQuery("select sum(totalWeight) from inventorydata where customerFileID='"+cid+"' and serviceOrderId='"+sid+"' and  ntbsFlag is false").list();
		    if(!(listWeightsum.isEmpty()) && !(listWeightsum.get(0)==null)){
			     
		    	if(unit.equalsIgnoreCase("Cft")){
		    		int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' where id='"+sid+"'");
		    		 double temp = (Float.parseFloat(listWeightsum.get(0).toString())) * 0.4536;
		    		 temp= Math.round(temp*10000)/10000;
		    		 int i4 = getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+temp+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+temp+"' where id='"+sid+"'");
				
		    	}else{
		    		 int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeightKilo='"+listWeightsum.get(0).toString()+"', estimateTareWeightKilo='0.00',estimatedNetWeightKilo='"+listWeightsum.get(0).toString()+"' where id='"+sid+"'");
		    		 double temp = (Float.parseFloat(listWeightsum.get(0).toString())) * 2.2046;
		    		 temp= Math.round(temp*10000)/10000;
		    		 int i4=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+temp+"',estimateTareWeight='0.00',estimatedNetWeight='"+temp+"' where id='"+sid+"'");
			    		
		    	}
		    	//int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='"+listWeightsum.get(0).toString()+"',estimateTareWeight='0.00',estimatedNetWeight='"+listWeightsum.get(0).toString()+"' where id='"+sid+"'");
			}else{
				int i3=getHibernateTemplate().bulkUpdate("update Miscellaneous set estimateGrossWeight='0.00',estimateTareWeight='0.00',estimatedNetWeight='0.00',estimateGrossWeightKilo='0.00',estimateTareWeightKilo='0.00',estimatedNetWeightKilo='0.00' where id='"+sid+"'");
			}
	 }
	
	   public String findServiceOrderId(String tempOrgId, String tempEmfId){
		   List tempIds;
		   getHibernateTemplate().setMaxResults(1);
		  // String query = "select id from ServiceOrder where shipNumber like '"+tempEmfId+"%' and corpID='"+tempOrgId+"'";
		 tempIds = getHibernateTemplate().find("select id from ServiceOrder where shipNumber like '"+tempEmfId+"%' and corpID='"+tempOrgId+"'");
		 if(tempIds.isEmpty()){
			 return "" ;
		 }else{
			 return tempIds.get(0).toString();
		 }
	}
	   public List getItemList(Long sid,String sessionCorpID){
		   return getHibernateTemplate().find("from InventoryData where  serviceOrderID='"+sid+"'  and corpID='"+sessionCorpID+"'");  
	   }
	   public List getVehicle(Long sid,String sessionCorpID){
	   return getHibernateTemplate().find("from Vehicle where serviceOrderId='"+sid+"'");
		   
	   }
	   public void updateInsurance(String newField,String fieldValue,Long id,Long sid,Long cid,String sessionCorpID){
		/*if(newField.equalsIgnoreCase("newAtricle")){*/
			 getHibernateTemplate().bulkUpdate("update InventoryData set "+newField+"='"+fieldValue+"' where id='"+id+"' and serviceOrderID='"+sid+"' and customerFileID='"+cid+"' and corpID='"+sessionCorpID+"'");
				//queryParameter=queryParameter+"atricle="+fieldValue+" where id="+id+"and serviceOrderID="+sid+" and customerFileID="+serviceOrder.getCustomerFileId()+" and corpID="+sessionCorpID+"";
			/*}else if(newField.equalsIgnoreCase("newValuation")){
				getHibernateTemplate().bulkUpdate("update InventoryData set valuation='"+fieldValue+"' where id='"+id+"' and serviceOrderID='"+sid+"' and customerFileID='"+cid+"' and corpID='"+sessionCorpID+"'");
				
			}*/
	   }
	   public void updateVehicle(String newField,String fieldValue,Long id,Long sid,String vehicleIdNumber,String sessionCorpID){
		   getHibernateTemplate().bulkUpdate("update Vehicle set "+newField+"='"+fieldValue+"' where id='"+id+"' and serviceOrderId='"+sid+"' and idNumber='"+vehicleIdNumber+"' and corpID='"+sessionCorpID+"'");
	   }

	public List<Long> getWorkTicketIdList(String shipno, String sessionCorpID) {
		return getHibernateTemplate().find("select id from WorkTicket where shipNumber = '"+shipno+"' and (orderId is not null or orderId<>'') ");
	}

	public void deleteInventoryData(Long sid, Long networkSynchedId) { 
			this.getSession().createSQLQuery("delete from inventorydata where networkSynchedId='"+networkSynchedId+"' and serviceOrderID="+sid+"").executeUpdate();			
		}

	public void updateLinkedInsurance(String newField, String fieldValue, Long networkSynchedId, Long sid, Long cid) {
		this.getSession().createSQLQuery("update inventorydata set "+newField+"='"+fieldValue+"' where networkSynchedId='"+networkSynchedId+"' and serviceOrderID='"+sid+"' and customerFileID='"+cid+"' ").executeUpdate();		
		
	}

	public void updateLinkedVehicleData(String newField, String fieldValue, Long sid, String idNumber) {
		this.getSession().createSQLQuery("update vehicle set "+newField+"='"+fieldValue+"' where  serviceOrderId='"+sid+"' and idNumber='"+idNumber+"' ").executeUpdate();		
		
	}
	public AccessInfo getAccessinfoByNetworkSyncId(Long id, String corpID){
		AccessInfo  acc  = new AccessInfo();
		List <AccessInfo> l = getHibernateTemplate().find(" from AccessInfo where networkSynchedId = '"+id+"' and id != '"+id+"' ");
		if(l!=null && !l.isEmpty() && l.get(0)!=null){
			acc = l.get(0);
		}
		return acc;
	}

	public void removeLinkedInventories(Long id) {
		this.getSession().createSQLQuery("delete from inventorydata where networkSynchedId='"+id+"'").executeUpdate();			
	}
	 public List getInventoryItemForMss(Long sid, Long cid, String sessionCorpID){
		 return getHibernateTemplate().find("from InventoryData where (serviceOrderID='"+sid+"' or customerFileID='"+cid+"') and type='A' and (width<>0 or width>0) and (height<>0 or height>0) and (length<>0 or length>0) and corpID='"+sessionCorpID+"'");
	 }
}
