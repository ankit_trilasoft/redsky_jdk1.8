<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="quoteList.title"/></title>   
    <meta name="heading" content="<fmt:message key='quoteList.heading'/>"/>   
	
</head>

<c:set var="buttons">
	
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editQuote.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>
<s:form id="quoteListForm">
<div id="Layer1">
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td width="4" align="right"><img width="4" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab"><a href="editCustomerFile.html?id=<%=request.getParameter("id") %> " >Customer
			File</a></td>
			<td width="4" align="left"><img width="4" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Service Orders</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="7" height="20"
				src="<c:url value='/images/head-left.jpg'/>" /></td>
			<td width="100" class="detailActiveTabLabel content-tab">Quotations</td>
			<td width="7" align="left"><img width="7" height="20"
				src="<c:url value='/images/head-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Surveys</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Notes</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Reports</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td width="4"></td>
			<td width="7" align="right"><img width="6" height="20"
				src="<c:url value='/images/tab-left.jpg'/>" /></td>
			<td width="100" class="content-tab">Account Policy</td>
			<td width="7" align="left"><img width="6" height="20"
				src="<c:url value='/images/tab-right.jpg'/>" /></td>
			<td align="left"></td>
		</tr>
	</tbody>
</table>
<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
				    <tbody><tr>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
				      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">QuoteFor</div></td>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
				      <td></td>
				    </tr>
				</tbody></table>
				<table>
					<tbody>
						<tr>
							<td>
							    <s:textfield name="customerFile.lastName" required="true" cssClass="text medium"/>
							</td>
							<td>
							    <s:textfield name="customerFile.firstName" required="true" cssClass="text medium"/>
							</td>
						</tr>
					</tbody>
				</table>
				
				<s:set name="quotes" value="quotes" scope="request"/>  
				<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">
				    <tbody><tr>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-left.jpg'/>"/></td>
				      <td id="searchLabelCenter" width="90"><div align="center" class="content-head">List</div></td>
				      <td width="7"><img width="7" height="20" src="<c:url value='/images/head-right.jpg'/>"/></td>
				      <td></td>
				    </tr>
				</tbody></table>
				<display:table name="quotes" class="table" requestURI="" id="quoteList" export="true" defaultsort="2" pagesize="10" decorator="org.displaytag.decorator.TotalTableDecorator" >   
				    <display:column property="id" sortable="true" href="editQuote.html"    
				        paramId="id" paramProperty="id" titleKey="quote.id" />
				    <display:column property="quoteNumber" sortable="true" titleKey="quote.quoteNumber"/>
				    <display:column property="description" sortable="true" titleKey="quote.description"/>
				    <display:column property="amount" sortable="true" titleKey="quote.amount" total="true"/>
				    <display:column property="billed" sortable="true" titleKey="quote.billed"/>
				    <display:column property="type" sortable="true" titleKey="quote.type" />
				    <display:column property="billToCode" sortable="true" titleKey="quote.billToCode"/>
				    
				    <display:setProperty name="paging.banner.item_name" value="quote"/>   
				    <display:setProperty name="paging.banner.items_name" value="quotes"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Quote List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Quote List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Quote List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table>
</div>
<a href="editNewQuote.html?id=<%=request.getParameter("id") %> "><input type="button" value="Add" /></a>

</s:form>

<script type="text/javascript">   
    highlightTableRows("quoteList");   
</script>
