package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DsFamilyDetails;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DsFamilyDetailsManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.webapp.util.GetSortedMap;

public class DsFamilyDetailsAction extends BaseAction implements Preparable{
	private Long id;
	private String sessionCorpID;
	private DsFamilyDetails dsFamilyDetails;
	private DsFamilyDetailsManager dsFamilyDetailsManager;
	private NotesManager notesManager; 
	private Long customerFileId;
	private String lastName;
	private RefMasterManager refMasterManager;
	private Map<String,String> country=new HashMap<String, String>();
	private Map<String,String> country_isactive=new HashMap<String, String>();
	private Map<String,String> faimaly=new HashMap<String, String>();
	private Map<String,String> faimaly_isactive=new HashMap<String, String>();
	private CustomerFileManager customerFileManager;
	private CustomerFile customerFile;
	private String usertype;
	//private String isSOExtract;
	private String userName;
	private String email;
	private String cellNumber;
	private String custPartnerEmail;
	private String countFamilyDetailsNotes;
	private Map<String,String> preffix_isactive;
	private Map<String,String> benefitsEndReasonList;
	private int selfValueCount;


	/*public String getIsSOExtract() {
		return isSOExtract;
	}
	public void setIsSOExtract(String isSOExtract) {
		this.isSOExtract = isSOExtract;
	}*/
	public void prepare() throws Exception { 
		userName=getRequest().getRemoteUser();
	}
	private Map<String, String> relocatingWithCoWorkerList; 
	public String getComboList(String corpID){ 
		String parameters="'RELOWITHCOWORKER','PREFFIX','BENEFITS_END_REASON'";
		preffix_isactive =  new LinkedHashMap<String,String>();
		benefitsEndReasonList =  new LinkedHashMap<String,String>();
		List<RefMaster> list=refMasterManager.findByParameterlist("COUNTRY", corpID);
		relocatingWithCoWorkerList = new LinkedHashMap<String, String>();
		List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(corpID,parameters);

		for (RefMasterDTO refObj : allParamValue) {
			if(refObj.getParameter().trim().equals("RELOWITHCOWORKER")){
				relocatingWithCoWorkerList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
			}
			if (refObj.getParameter().trim().equals("PREFFIX")){
	 			preffix_isactive.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 	}
			if (refObj.getParameter().trim().equals("BENEFITS_END_REASON")){
				benefitsEndReasonList.put(refObj.getCode().trim()+"~"+refObj.getStatus().trim(),refObj.getDescription().trim());
		 	}
		}
		for(RefMaster refmaster:list){
			country_isactive.put(refmaster.getCode()+"~"+refmaster.getStatus(), refmaster.getDescription());
		}
		country_isactive=GetSortedMap.sortByValues(country_isactive);
	 	country = refMasterManager.findByParameter(corpID, "COUNTRY");
	 	list=refMasterManager.findByParameterlist("FAMILY", corpID);
		for(RefMaster refmaster:list){
			faimaly_isactive.put(refmaster.getCode()+"~"+refmaster.getStatus(), refmaster.getDescription());
		}	 	
		faimaly_isactive=GetSortedMap.sortByValues(faimaly_isactive);
	 	faimaly= refMasterManager.findByParameter(corpID, "FAMILY");
	 	 return SUCCESS;
	}
	public DsFamilyDetailsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID();
		this.usertype=user.getUserType();
		
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	private Boolean visibilityForSalesRole=false;
	 private Boolean visibilityForSalesPortla=false;
	 private Set<Role> roles;
	public String edit() {
		String permKey = sessionCorpID +"-"+"module.script.form.corpSalesScript";
		visibilityForSalesRole=AppInitServlet.roleBasedComponentPerms.containsKey(permKey);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		roles=user.getRoles();
		if(roles!=null && visibilityForSalesRole){
			for (Role role : roles){				
				if(role.getName().toString().equals("ROLE_SALES_PORTAL")){
				visibilityForSalesPortla=true;
				}
		}
		}
		getComboList(sessionCorpID);
		customerFile=customerFileManager.get(customerFileId);
		lastName=customerFile.getLastName();
		email=customerFile.getEmail();
		cellNumber=customerFile.getOriginMobile();
		custPartnerEmail=customerFile.getCustPartnerEmail();
		String familySituationVal = customerFile.getFamilySitiation();
		List familyRelationshipList = dsFamilyDetailsManager.findRelationshipList(customerFile.getId(),sessionCorpID);
		if (id != null) {
			dsFamilyDetails = dsFamilyDetailsManager.get(id);
			selfValueCount= dsFamilyDetailsManager.findRelationshipList1(customerFileId,sessionCorpID,dsFamilyDetails.getId());
			getRequest().setAttribute("soLastName", dsFamilyDetails.getLastName());
			Map<String, String> soDetailNotes = new LinkedHashMap<String, String>();
			soDetailNotes = notesManager.countSoDetailNotes(dsFamilyDetails.getId().toString()+dsFamilyDetails.getCustomerFileId(),"'FamilyDetails'",sessionCorpID);
			if (soDetailNotes.containsKey("FamilyDetails")) {
				countFamilyDetailsNotes = soDetailNotes.get("FamilyDetails");
			} else {
				countFamilyDetailsNotes = "0";
			}
		} else {
			dsFamilyDetails = new DsFamilyDetails(); 
			dsFamilyDetails.setCreatedOn(new Date());
			dsFamilyDetails.setUpdatedOn(new Date());
			dsFamilyDetails.setCustomerFileId(customerFileId);
	
			selfValueCount= dsFamilyDetailsManager.findRelationshipList2(customerFileId,sessionCorpID);
			if((familySituationVal!=null && !familySituationVal.equalsIgnoreCase("")) && (familySituationVal.equalsIgnoreCase("Married with children") || familySituationVal.equalsIgnoreCase("Single with children"))
					&& (!familyRelationshipList.isEmpty() && familyRelationshipList.contains("Spouse"))){
				dsFamilyDetails.setRelationship("Child");
			}else if((familySituationVal!=null && !familySituationVal.equalsIgnoreCase("")) && (familySituationVal.equalsIgnoreCase("Married") || familySituationVal.equalsIgnoreCase("Couple with children") || familySituationVal.equalsIgnoreCase("Married with children"))){
				dsFamilyDetails.setRelationship("Spouse");
			}else{
				dsFamilyDetails.setRelationship("Self");
			}
		} 
		return SUCCESS;
	}
	
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (dsFamilyDetails.getId() == null);  
				if (isNew) { 
					dsFamilyDetails.setCreatedOn(new Date());
		        } 
				dsFamilyDetails.setCorpID(sessionCorpID);
				dsFamilyDetails.setUpdatedOn(new Date());
				dsFamilyDetails.setUpdatedBy(getRequest().getRemoteUser()); 
				dsFamilyDetails=dsFamilyDetailsManager.save(dsFamilyDetails);
				customerFile = customerFileManager.get(dsFamilyDetails.getCustomerFileId());
				if (isNew) {
					if(dsFamilyDetails.getRelationship()!=null && (dsFamilyDetails.getRelationship().equals("Spouse"))){
						customerFile.setCustPartnerPrefix(dsFamilyDetails.getPrefix());
					}
				}
				if(!isNew){
					if(dsFamilyDetails.getRelationship()!=null && (dsFamilyDetails.getRelationship().equals("Spouse"))){
					/*if(isSOExtract!=null && isSOExtract.equals("yes")){
					//customerFileManager.updateSOExtractFieldOnCFSave(customerFile.getId());
					}*/
					}
				}
				getRequest().setAttribute("soLastName", customerFile.getLastName());
				dsFamilyDetailsManager.setCFileFamilySize(dsFamilyDetails.getCustomerFileId(), sessionCorpID);
			    String key = (isNew) ? "dsFamilyDetails.added" : "dsFamilyDetails.updated";
			    try{
			    	if(customerFile.getIsNetworkRecord() !=null && customerFile.getIsNetworkRecord()){
						List linkedSequenceNumber=findLinkerSequenceNumber(customerFile);
						Set set = new HashSet(linkedSequenceNumber);
						List linkedSeqNum = new ArrayList(set);
						List<Object> records=findRecords(linkedSeqNum,customerFile);
						synchornizeFamilyDetail(records,dsFamilyDetails,isNew,customerFile);
					}
			    }catch(Exception ex){
						ex.printStackTrace();
				}
			    List dsFamilyDetailsList = dsFamilyDetailsManager.getDsFamilyDetailsList(dsFamilyDetails.getCustomerFileId(),customerFile.getCorpID());
			    if(dsFamilyDetailsList != null && dsFamilyDetailsList.size() > 0){
			    	customerFile.setFamilysize(dsFamilyDetailsList.size());
			    }else{
			    	customerFile.setFamilysize(0);
			    }
			    if(dsFamilyDetails.getRelationship()!=null && (dsFamilyDetails.getRelationship().equals("Spouse"))){
					customerFile.setCustPartnerFirstName(dsFamilyDetails.getFirstName());
					customerFile.setCustPartnerLastName(dsFamilyDetails.getLastName());
					customerFile.setCustPartnerContact(dsFamilyDetails.getCellNumber()); 
					customerFile.setCustPartnerEmail(dsFamilyDetails.getEmail());
					customerFile.setCustPartnerPrefix(dsFamilyDetails.getPrefix());
				}else{
					try{
					List SpouseDetails = dsFamilyDetailsManager.findSpouseDetails(customerFile.getId(),sessionCorpID);
					if(SpouseDetails !=null && (!(SpouseDetails.isEmpty())) && SpouseDetails.get(0)!=null && (!(SpouseDetails.get(0).toString().trim().equals("")))){
						String SpouseDetailsData = SpouseDetails.get(0).toString().trim();
						String	SpouseDetail[] = SpouseDetailsData.split("~"); 
						if(SpouseDetail[0] !=null && (!(SpouseDetail[0].toString().trim().equalsIgnoreCase("NOData")))){
						customerFile.setCustPartnerFirstName(SpouseDetail[0]);
						}else{
							customerFile.setCustPartnerFirstName("");	
						}
						if(SpouseDetail[1] !=null && (!(SpouseDetail[1].toString().trim().equalsIgnoreCase("NOData")))){
						customerFile.setCustPartnerLastName(SpouseDetail[1]);
						}else{
							customerFile.setCustPartnerLastName("");	
						}
						if(SpouseDetail[2] !=null && (!(SpouseDetail[2].toString().trim().equalsIgnoreCase("NOData")))){
						customerFile.setCustPartnerContact(SpouseDetail[2]); 
						}else{
							customerFile.setCustPartnerContact(""); 	
						}
						if(SpouseDetail[3] !=null && (!(SpouseDetail[3].toString().trim().equalsIgnoreCase("NOData")))){
							customerFile.setCustPartnerEmail(SpouseDetail[3]); 
							}else{
								customerFile.setCustPartnerEmail(""); 	
							}
						if(SpouseDetail[4] !=null && (!(SpouseDetail[4].toString().trim().equalsIgnoreCase("NOData")))){
							customerFile.setCustPartnerPrefix(SpouseDetail[4]);; 
							}else{
								customerFile.setCustPartnerPrefix("");	
							}
						
					} 
					}catch(Exception e){
						e.printStackTrace();
					}
					}
			    customerFile= customerFileManager.save(customerFile);
			    
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	
	private List findLinkerSequenceNumber(CustomerFile customerFile) {
		List linkedSequnceNumberList= new ArrayList();
		if(customerFile.getBookingAgentSequenceNumber()==null || customerFile.getBookingAgentSequenceNumber().equals("") ){
			linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getSequenceNumber(), "Primary");
		}else{
			linkedSequnceNumberList=customerFileManager.getLinkedSequenceNumber(customerFile.getBookingAgentSequenceNumber(), "Secondary");
		}
		return linkedSequnceNumberList;
	}
	private List<Object> findRecords(List linkedSequenceNumber,CustomerFile customerFileLocal) {
		List<Object> recordList= new ArrayList();
		Iterator it =linkedSequenceNumber.iterator();
		while(it.hasNext()){
			String sequenceNumber= it.next().toString();
			CustomerFile customerFileRemote=customerFileManager.getForOtherCorpid(Long.parseLong(customerFileManager.findRemoteCustomerFile(sequenceNumber).toString()));
			recordList.add(customerFileRemote);
		}
		return recordList;
	}
	
	private void synchornizeFamilyDetail(List<Object> records, DsFamilyDetails dsFamilyDetails, boolean isNew,CustomerFile customerFile) {
		Iterator  it=records.iterator();
		while(it.hasNext()){
			CustomerFile customerFileToRecods=(CustomerFile)it.next();
			if(!(customerFile.getSequenceNumber().equals(customerFileToRecods.getSequenceNumber()))){
			try{
				if(isNew){
					DsFamilyDetails dsFamilyDetailsNew = new DsFamilyDetails();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(dsFamilyDetailsNew, dsFamilyDetails);
    				
    				dsFamilyDetailsNew.setCreatedBy("Networking");
    				dsFamilyDetailsNew.setUpdatedBy("Networking");
    				dsFamilyDetailsNew.setCreatedOn(new Date());
    				dsFamilyDetailsNew.setUpdatedOn(new Date());
    				dsFamilyDetailsNew.setCustomerFileId(customerFileToRecods.getId());
    				dsFamilyDetailsNew.setCorpID(customerFileToRecods.getCorpID());
    				dsFamilyDetailsNew.setNetworkId(dsFamilyDetails.getId());
    				dsFamilyDetailsNew.setId(null);
    				dsFamilyDetailsNew = dsFamilyDetailsManager.save(dsFamilyDetailsNew);
    				
    				//dsFamilyDetails.setNetworkId(dsFamilyDetails.getId());
    				//dsFamilyDetails=dsFamilyDetailsManager.save(dsFamilyDetails);
    				int i=dsFamilyDetailsManager.updateFamilyDetailsNetworkId(dsFamilyDetails.getId());
    				dsFamilyDetailsManager.setCFileFamilySize(customerFileToRecods.getId(), customerFileToRecods.getCorpID());
				}else{
					DsFamilyDetails dsFamilyDetailsTo = dsFamilyDetailsManager.getForOtherCorpid(dsFamilyDetailsManager.findRemoteDsFamilyDetails(dsFamilyDetails.getNetworkId(),customerFileToRecods.getId()));
					Long id = dsFamilyDetailsTo.getId();
					String createdBy=dsFamilyDetailsTo.getCreatedBy();
					Date createdOn=dsFamilyDetailsTo.getCreatedOn();
					BeanUtilsBean beanUtilsBean = BeanUtilsBean.getInstance();
    				beanUtilsBean.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
    				beanUtilsBean.copyProperties(dsFamilyDetailsTo, dsFamilyDetails);
    				dsFamilyDetailsTo.setUpdatedBy(dsFamilyDetails.getCorpID()+":"+getRequest().getRemoteUser());
    				dsFamilyDetailsTo.setUpdatedOn(new Date());
    				dsFamilyDetailsTo.setCustomerFileId(customerFileToRecods.getId());
    				dsFamilyDetailsTo.setCorpID(customerFileToRecods.getCorpID()); 
    				dsFamilyDetailsTo.setCreatedBy(createdBy);
    				dsFamilyDetailsTo.setCreatedOn(createdOn);
    				dsFamilyDetailsTo.setId(id);
    				dsFamilyDetailsTo = dsFamilyDetailsManager.save(dsFamilyDetailsTo);
    				dsFamilyDetailsManager.setCFileFamilySize(customerFileToRecods.getId(), customerFileToRecods.getCorpID());
				}
					
			}catch(Exception ex){
				ex.printStackTrace();
			}
				
		
		}
		}
		
	}
	
	public DsFamilyDetails getDsFamilyDetails() {
		return dsFamilyDetails;
	}
	public void setDsFamilyDetails(DsFamilyDetails dsFamilyDetails) {
		this.dsFamilyDetails = dsFamilyDetails;
	}
	public void setDsFamilyDetailsManager(
			DsFamilyDetailsManager dsFamilyDetailsManager) {
		this.dsFamilyDetailsManager = dsFamilyDetailsManager;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getCountry() {
		return country;
	}
	public void setCountry(Map<String, String> country) {
		this.country = country;
	}
	public Map<String, String> getFaimaly() {
		return faimaly;
	}
	public void setFaimaly(Map<String, String> faimaly) {
		this.faimaly = faimaly;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}
	public Boolean getVisibilityForSalesPortla() {
		return visibilityForSalesPortla;
	}
	public void setVisibilityForSalesPortla(Boolean visibilityForSalesPortla) {
		this.visibilityForSalesPortla = visibilityForSalesPortla;
	}
	public Map<String, String> getCountry_isactive() {
		return country_isactive;
	}
	public void setCountry_isactive(Map<String, String> country_isactive) {
		this.country_isactive = country_isactive;
	}
	public Map<String, String> getFaimaly_isactive() {
		return faimaly_isactive;
	}
	public void setFaimaly_isactive(Map<String, String> faimaly_isactive) {
		this.faimaly_isactive = faimaly_isactive;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Map<String, String> getRelocatingWithCoWorkerList() {
		return relocatingWithCoWorkerList;
	}
	public void setRelocatingWithCoWorkerList(
			Map<String, String> relocatingWithCoWorkerList) {
		this.relocatingWithCoWorkerList = relocatingWithCoWorkerList;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCellNumber() {
		return cellNumber;
	}
	public void setCellNumber(String cellNumber) {
		this.cellNumber = cellNumber;
	}
	public String getCustPartnerEmail() {
		return custPartnerEmail;
	}
	public void setCustPartnerEmail(String custPartnerEmail) {
		this.custPartnerEmail = custPartnerEmail;
	}

	public Map<String, String> getPreffix_isactive() {
		return preffix_isactive;
	}
	public void setPreffix_isactive(Map<String, String> preffix_isactive) {
		this.preffix_isactive = preffix_isactive;
	}

	public Map<String, String> getBenefitsEndReasonList() {
		return benefitsEndReasonList;
	}
	public void setBenefitsEndReasonList(Map<String, String> benefitsEndReasonList) {
		this.benefitsEndReasonList = benefitsEndReasonList;
	}
	public int getSelfValueCount() {
		return selfValueCount;
	}
	public void setSelfValueCount(int selfValueCount) {
		this.selfValueCount = selfValueCount;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public String getCountFamilyDetailsNotes() {
		return countFamilyDetailsNotes;
	}
	public void setCountFamilyDetailsNotes(String countFamilyDetailsNotes) {
		this.countFamilyDetailsNotes = countFamilyDetailsNotes;
	}
	 
}
