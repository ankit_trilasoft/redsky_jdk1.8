package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.RateGrid;

public interface RateGridManager extends GenericManager<RateGrid,Long>{
	
	public List findMaximumId();

	public List findGridIds(String charge, String contractName,String sessionCorpID);

	public List getRates(String chid, String contractName,String sessionCorpID);

	public int findTwoDNumber(String chargeID, String sessionCorpID);
	public List findSaleCommissionRateGrid(String corpId,String contract,String compDiv,String job);
	public String findSaleCommissionContractAndCharge(String compDiv,String bucketJobType, String sessionCorpID);

}
