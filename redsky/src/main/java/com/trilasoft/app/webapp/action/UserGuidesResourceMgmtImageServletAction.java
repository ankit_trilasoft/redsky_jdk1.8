package com.trilasoft.app.webapp.action;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.UserGuides;
import com.trilasoft.app.service.UserGuidesManager;

public class UserGuidesResourceMgmtImageServletAction extends BaseAction implements Preparable {
	private String sessionCorpID;
	   private String imageURL;
	   private String contentType;
	   private String fileFileName;
	   private UserGuides userGuides;
	   private UserGuidesManager userGuidesManager;
	   static final Logger logger = Logger.getLogger(UserGuidesResourceMgmtImageServletAction.class);
	   Date currentdate = new Date();
	   InputStream fileInputStream;
	   public InputStream getFileInputStream() {
				return fileInputStream;
			}	   
		public UserGuidesResourceMgmtImageServletAction(){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal(); 
			this.sessionCorpID = user.getCorpID(); 
		}
		public void prepare() throws Exception { 
			
		}
		@SkipValidation
		public String extractFileImageServlet() throws ServletException,IOException{
			 String id = getRequest().getParameter("id");
			 HttpServletResponse response = getResponse();
			 ServletOutputStream out = response.getOutputStream();
			 String resultType="success";
				try {
					 if((id!=null)&&(!id.toString().equalsIgnoreCase(""))){
						 logger.warn("ID: "+id+" Start");
						 userGuides=userGuidesManager.get(Long.parseLong((id.toString())));
							imageURL = userGuides.getLocation();							
						    contentType = "application/octet-stream";
						    fileFileName = userGuides.getFileName();
						    fileFileName=fileFileName.replaceAll(" ", "");
						    logger.warn("ID: "+id+" Start");
					 }else{
						 resultType="error";
					 }
				} catch (Exception e) {
					 resultType="error";
				}
				try {
					logger.warn("imageURL: "+imageURL+" Start");
					fileInputStream = new FileInputStream(new File(imageURL));
					logger.warn("imageURL: "+imageURL+" Start");
					return SUCCESS;	
					}
					catch (Exception e) {
						resultType="error";
					}
					return (resultType.equalsIgnoreCase("error")?ERROR:SUCCESS);
		}
	
		public String getSessionCorpID() {
			return sessionCorpID;
		}
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		}
		public String getImageURL() {
			return imageURL;
		}
		public void setImageURL(String imageURL) {
			this.imageURL = imageURL;
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public String getFileFileName() {
			return fileFileName;
		}
		public void setFileFileName(String fileFileName) {
			this.fileFileName = fileFileName;
		}
		public UserGuides getUserGuides() {
			return userGuides;
		}
		public void setUserGuides(UserGuides userGuides) {
			this.userGuides = userGuides;
		}
		public void setUserGuidesManager(
				UserGuidesManager userGuidesManager) {
			this.userGuidesManager = userGuidesManager;
		}	
	   
}
