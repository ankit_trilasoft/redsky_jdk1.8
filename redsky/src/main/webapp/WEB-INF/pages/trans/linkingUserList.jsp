<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>

<table style="margin:0px;padding:0px;">
<s:hidden name="tempRec" value="${fn:length(userListName)}"/>
<c:forEach var="accLine" items="${userListName}" varStatus="loopStatus">
<tr class="listwhitetext">
  <td width="90"></td> 
  <td>
  <s:checkbox name="rep${accLine.id}" id="rep${accLine.id}" value="false" fieldValue="${accLine.email}" onclick="fillUserEmail(this)" required="true" /> 
  </td>
  <td width="170">
  <c:out value="${accLine.firstName}"/>&nbsp;&nbsp;<c:out value="${accLine.lastName}"/>
  </td><td width="5" >&nbsp;</td>
  <td>
  <s:checkbox name="res${accLine.id}" id="res${accLine.id}" value="false" fieldValue="${accLine.email}" onclick="fillUserEmail1(this)" required="true" />
  </td>
  <td>
  <c:out value="${accLine.firstName}"/>&nbsp;&nbsp;<c:out value="${accLine.lastName}"/>
  </td> 
</tr>
</c:forEach>
</table>


