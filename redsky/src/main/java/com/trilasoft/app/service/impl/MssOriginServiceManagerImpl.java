package com.trilasoft.app.service.impl;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MssOriginServiceDao;
import com.trilasoft.app.model.MssOriginService;
import com.trilasoft.app.service.MssOriginServiceManager;

public class MssOriginServiceManagerImpl extends GenericManagerImpl<MssOriginService, Long> implements MssOriginServiceManager{
	MssOriginServiceDao mssOriginServiceDao;
	public MssOriginServiceManagerImpl(MssOriginServiceDao mssOriginServiceDao) {
		super(mssOriginServiceDao);
		this.mssOriginServiceDao = mssOriginServiceDao;
	}
}
