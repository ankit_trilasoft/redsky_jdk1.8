<%@ include file="/common/taglibs.jsp" %>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
<title>Category Resource List</title>
<meta name="heading" content="Resource List"/> 
<style>

</style>

</head>

<s:form id="resourceList" name="resourceList" action="assignResources.html?decorator=popup&popup=true" method="post">
<s:hidden name="category" value="<%=request.getParameter("category") %>"/>
<s:hidden name="flagType" value="<%=request.getParameter("flagType") %>"/>
<s:hidden  name="rowIndex"/>
<div id="content" align="center" style="width: 450px" >
<div id="liquid-round">
   	<div class="top"><span></span></div>
   	<div class="center-content">
		<table class="" cellspacing="1" cellpadding="1" border="0" style="width:100%">
	  		<tbody>
		  		<tr>
				  	<td align="left" class="listwhitetext">
					  	<table class="detailTabLabel" border="0" cellpadding="2"  style="width:500px">
							  <tbody>  	
								<tr>
			                        <td align="right" width="15px">Resource</td>
							        <td width="120px" ><s:textfield name="resource"  cssClass="input-text" cssStyle="width:100px;" /></td>
									<td><s:submit cssClass="cssbutton" method="findResourceList" cssStyle="width:60px; height:25px;!margin-bottom: 10px;" key="button.search"/>
									<input type="button" class="cssbutton" value="Clear" style="width:60px; height:25px;!margin-bottom: 10px;" onclick="clear_fields();"/></td> 
								</tr>
							  </tbody>
						</table>
					 </td>
		    	</tr>		  	
		 	</tbody>
		</table>
	</div>
	<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="Layer1" style="width: 100%">
<div class="spn">&nbsp;</div>
<div id="newmnav">
		<ul>
		 	<li id="newmnav1" style="background:#FFF "><a class="current" style="margin:0px;"><span>Resource List</span></a></li>	  	
		</ul>
</div>

<s:set name="itemList" value="itemList" scope="itemList"/>
   
<display:table name="itemList" class="table" requestURI="" id="itemssList" defaultsort="1" pagesize="10" >
	<display:column title="Resource" sortable="true" sortProperty="descript" ><a href="#" onclick="return setCategoryResource('${itemssList.descript}')"/><c:out value="${itemssList.descript}"/></a></display:column>
    
    <display:setProperty name="export.excel.filename" value="ItemsJEquip List.xls"/>   
    <display:setProperty name="export.csv.filename" value="ItemsJEquip List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="ItemsJEquip List.pdf"/>   
</display:table>   
  
 <c:out value="${buttons}" escapeXml="false" />

 </div>
 </s:form>
 
 <script language="javascript" type="text/javascript">
function setCategoryResource(target){
	var flagType = document.forms['resourceList'].elements['flagType'].value;
	var rowIndex = document.forms['resourceList'].elements['rowIndex'].value;
	if(flagType=='All'){
		window.opener.document.forms['operationsResourceLimits'].elements['equipMaterialsLimits.resource'].value=target;
	}else if(flagType=='Daily'){
		try{
		window.opener.document.forms['dailyLimitControl'].elements['equipMaterialsLimits.resource'].value=target;
		}catch(e){
			window.opener.document.forms['dailyLimitControl'].elements['res'+rowIndex].value=target;
		}
	}else{
		window.opener.document.forms['dailyLimitListForm'].elements['resource'].value=target;	
	}
	window.close();
	return true;
}
function clear_fields(){
	document.forms['resourceList'].elements['resource'].value='';
}	
</script>  

<script type="text/javascript">   
    highlightTableRows("itemList");
</script>  
