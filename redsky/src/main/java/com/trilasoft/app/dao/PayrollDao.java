package com.trilasoft.app.dao;

import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.GenralDTO;
import com.trilasoft.app.model.Payroll;

public interface PayrollDao extends GenericDao<Payroll, Long>{
		public List findById(Long id); 
		public List<Payroll> searchPayroll(String firstName, String lastName, String warehouse, String active, String userName,String union,String sessionCorpID); 
		public List checkUserName(String userName); 
		public List findMaximumId(); 
		public Map<String, String> getCompnayDevisionCode(String corpId);
		public List<GenralDTO> findPayrollExtractList(String beginDates,String endDates,String corpId, String hub);
		public List findPayrollInternalList(String beginDates, String endDates, String corpId);
		public List findsStorageExtracts(String warehouseS, String typeS, String corpID);
		public List findsClaimsExtracts(String query,String corpId, String recievedFormCheckFlag);
		public List<Payroll> getActiveList();
		public int updateSickPersonal(String fromDate, String toDate, String sessionCorpID);
		public List getSickHours(String userName, String sessionCorpID);
		public List getPersonalHours(String userName, String sessionCorpID);
		
		public List getHealthWelfareExtract(String beginDate, String endDate, String sessionCorpID);
		public int updateVacation(String fromDate, String toDate, String sessionCorpID);
		public List getVacationHours(String userName, String sessionCorpID);
		public List findStorageLibraryExtract(String locWarehouse, String typeSto, String sessionCorpID);
		public List findPaychexExtractList(String beginDates,String endDates,String corpId, String hub);
		public List getPkCodeList(String vlCode, String sessionCorpID);
		public List findsComplaintsExtracts(String query,String corpId);
		public void updateSickPersonalDay();
		public List getAllCrewName(String sessionCorpID);
		public List<GenralDTO> findPayrollWorkDayExtractList(String beginDates,String endDates,String corpId, String hub);
		public Map<String, String> getOpenCompnayDevisionCode(String corpId);
	
}
