<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="locationList.title" /></title>
<meta name="heading" content="<fmt:message key='locationList.heading'/>" />

<script type="text/javascript">
		function setFlagValueMo(){
		  	document.forms['assignItemsForm'].elements['hitFlag'].value = '1';
		}
		
		
		function clear_fields(){
		var i;
			for(i=0;i<=2;i++){
				document.forms['locationList1'].elements['locationId'].value = "";
				document.forms['locationList1'].elements['warehouse'].value = "";
				document.forms['locationList1'].elements['type'].value = "";
				document.forms['locationList1'].elements['occupied'].checked = false;
			}
		}
</script>

<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:1px;
margin-top:-5px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>

<s:form id="locationformList" name="locationList1" action="searchLocatesUnitMove" method="post">
<s:hidden name="id1" value="<%=request.getParameter("id1") %>"/> 
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<div id="Layer1" style="width: 100%">
	<s:hidden name="serviceOrder.id" />
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
			<div id="newmnav">
		    <ul>
			  <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
			  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			  <li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
			  </sec-auth:authComponent>
			  <li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
			  
			  <c:if test="${serviceOrder.job !='INT'}">
			  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
			  </c:if>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
			  		    <c:if test="${serviceOrder.job =='RLO'}">
		  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  </c:if>
		    <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  </c:if>
			  <li id="newmnav1" style="background:#FFF"><a class="current"><span>Ticket</span></a></li>
			 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
			  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
			  </configByCorp:fieldVisibility>
			  <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			</sec-auth:authComponent>
			  <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div>
	
	<%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
		<div id="newmnav">
		   			 <ul>
					  	<li><a href="editWorkTicketUpdate.html?id=<%=request.getParameter("id") %>"><span>Work Ticket</span></a></li>
					  	<li><a href="bookStorageLibraries.html?id=<%=request.getParameter("id") %>"><span>Storage List</span></a></li>
					  	<li><a href="searchUnitLocates.html?locationId=&type=&occupied=&warehouse=${workTicket.warehouse}&id=<%=request.getParameter("id") %>"><span>Add Storage</span></a></li>
					  	<li><a href="storageUnit.html?id=<%=request.getParameter("id") %>"><span>Access/Release Storage</span></a></li>
					  	<li id="newmnav1" style="background:#FFF"><a class="current"><span>Move Storage Location</span></a></li>
					  	<li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=workTicket&reportSubModule=workTicket&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
					</ul>
		</div><div class="spn">&nbsp;</div>
		
		


<table class="" width="100%"  cellspacing="1" cellpadding="0" border="0">
	<tbody>
		<tr>
			<td>
				<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
				<table>
					<tbody>
						<tr>
							<td width="10px"></td>
							<td class="listwhitetext">Ticket Number</td>
							<td><s:textfield name="workTicket.ticket" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
							<td width="10px"></td>
							<td class="listwhitetext">Warehouse</td>
							<td><s:textfield name="workTicket.warehouse" cssClass="input-textUpper" size="10" readonly="true" required="true" /></td>
						</tr>
					</tbody>
				</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbuttonA" cssStyle="width:58px; height:25px" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Search</span></a></li>
		  </ul>
		</div>
		<div class="spnblk">&nbsp;</div>

	<table class="table" width="90%">
		<thead>
			<tr>
				<th><fmt:message key="location.locationId" /></th>
				<th><fmt:message key="location.warehouse" /></th>
				<th><fmt:message key="location.type" /></th>
				<th ></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><s:textfield name="locationId" cssClass="text medium" cssStyle="width:262px" /></td>
				<td><s:textfield name="warehouse" maxlength="3" cssClass="text medium" /></td>
				<td>
				<configByCorp:customDropDown listType="map" list="${loctype_isactive}" fieldValue="${type}" 
   									attribute="id=type class=list-menu name=type style=width:230px headerKey='' headerValue='' "/>
							
				<%-- <s:select name="type" cssClass="list-menu" cssStyle="width:162px" list="%{locTYPE}" headerKey=""  headerValue=""/> --%>
				</td>
				<s:hidden key="occupied" cssClass="text medium" value="false"/>	
				<td width="130px" align="center"><c:out value="${searchbuttons}" escapeXml="false" /></td>
					
					
			</tr>
		</tbody>
	</table>
	

	<s:set name="locations" value="locations" scope="request" />
	<div id="otabs" style="margin-bottom: 0px;">
				  <ul>
				    <li><a class="current"><span>List</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
	<display:table name="locations" class="table" requestURI="" id="locationList" defaultsort="2" export="true" pagesize="10">
		<display:column> <input type="radio" name="dd" onclick="check(this)" value="${locationList.locationId}" /></display:column>
		<display:column property="locationId" sortable="true" titleKey="location.locationId" />
	 	<display:column property="capacity" headerClass="containeralign" style="text-align: right;" sortable="true"	title="Holding Capacity" />
		<display:column property="cubicFeet" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Capacity(Cft)" />
		<display:column property="cubicMeter" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Capacity(Cbm)" />
	    <display:column property="utilizedVolCft" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Used(Cft)" />
	    <display:column property="utilizedVolCbm" headerClass="containeralign" style="text-align: right;"  sortable="true" title="Used(Cbm)" />
	    <display:column headerClass="containeralign" style="text-align: right;"  sortable="true" title="Available(Cft)">
	    <c:out value="${locationList.cubicFeet-locationList.utilizedVolCft}"></c:out>
	    </display:column>
	   <display:column headerClass="containeralign" style="text-align: right;"  sortable="true" title="Available(Cbm)">
	    <c:out value="${locationList.cubicMeter-locationList.utilizedVolCbm}"></c:out>
	    </display:column>
		<display:column property="type" sortable="true" titleKey="location.type"/>
		
		<display:setProperty name="paging.banner.item_name" value="location" />
		<display:setProperty name="paging.banner.items_name" value="location" />
		<display:setProperty name="export.excel.filename" value="Location List.xls" />
		<display:setProperty name="export.csv.filename" value="Location List.csv" />
		<display:setProperty name="export.pdf.filename" value="Location List.pdf" />
	</display:table>
	</td>
   </tr>
  </tbody>
</table>
</div> 
 
	<table>
		<tr>
			<td>
				<c:out value="${buttons}" escapeXml="false" />
	
</s:form>
<s:form id="assignItemsForm" action="editStorageUnitMo" method="post">
	<s:hidden name="id" value="<%=request.getParameter("id1") %>"/> 
	<s:hidden name="id1" value="<%=request.getParameter("id") %>" />
	<s:hidden name="locationId" />
	<s:hidden name="ticket" value="%{workTicket.ticket}"  required="true" cssClass="text medium"/>
	
	<input type="submit" name="forwardBtn2" class="cssbutton1" disabled value="Move" style="width:67px; height:25px" onclick="setFlagValueMo();" />
	
	<c:set var="hitFlag" value="<%=request.getParameter("hitFlag") %>" />
	<s:hidden name="hitFlag" />
	<c:if test="${hitFlag == 1}" >
		<c:redirect url="/bookStorageLibraries.html?id=${workTicket.ticket}"  />
	</c:if>
	<c:if test="${hitFlag == 0}" >
		<c:redirect url="/storageUnitMove.html?id=${workTicket.id}"  />
	</c:if>
	<c:if test="${hitFlag == 2}" >
		<c:redirect url="/storagesMove.html?id=${workTicket.id}"  />
	</c:if>
</s:form>  
<td>
<td>

</td>
</tr>
	</table>

<script type="text/javascript"> 
    highlightTableRows("locationList"); 
</script>
<script type="text/javascript">
		function check(targetElement)
		  {
		  try{
		  document.forms['assignItemsForm'].elements['locationId'].value=targetElement.value;
		  document.forms['assignItemsForm'].elements['forwardBtn2'].disabled = false ;
	
		  //document.forms['assignItemsForm'].elements['id1'].value=targetElement.value;
		  }
		  catch(e){}
		  }
	</script>

