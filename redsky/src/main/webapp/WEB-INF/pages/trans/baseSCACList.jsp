<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="agentBaseSCAC.title"/></title>   
    <meta name="heading" content="<fmt:message key='agentBaseSCAC.heading'/>"/>  
    
<style>
.tab{
border:1px solid #74B3DC;

}
</style> 

</head>
<s:form id="agentBaseListForm" action="" method="post" validate="true">	
<div id="newmnav">
	  <ul>
	  	<li><a href="editPartnerPublic.html?id=${partner.id}&partnerType=AG" ><span>Agent Detail</span></a></li>
	  	<c:if test="${sessionCorpID!='TSFT' }">
	  	<li><a href="editPartnerPrivate.html?partnerId=${partner.id}&partnerType=AG&partnerCode=${partner.partnerCode}"><span>Additional Info</span></a></li>
	  	</c:if>
	  	<li><a href="baseList.html?id=${partner.id}"><span>Base List</span></a></li>
	  	<li><a href="editAgentBase.html?partnerId=${partner.id}&id=${agentBase.id}"><span>Base Details</span></a></li>
	    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Agent Base SCAC List</span></a></li>
	    <c:set var="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<s:hidden name="paramValue" value="<%= (String)session.getAttribute("paramView")%>" />
		<c:if test='${paramValue == "View"}'>
			<li><a href="searchPartnerView.html"><span>Partner List</span></a></li>
		</c:if>
		<c:if test='${paramValue != "View"}'>
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</c:if>
	  </ul>
</div>
<div style="width: 100%" >
<div class="spn">&nbsp;</div>
</div>
<div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table style="margin-bottom: 3px">
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
		  		<td align="right" class="listwhitetext">Partner Code</td>
				<td><s:textfield cssClass="input-text" key="partner.partnerCode" readonly="true"/></td>
				<td align="right" class="listwhitetext" width="15px"></td>
				<td align="right" class="listwhitetext" width="70px">Partner Name</td>
				<td><s:textfield key="partner.lastName" cssClass="input-text" size="50" readonly="true"/></td>
			</tr>
			<tr>
				<td align="left" colspan="6"></td>
			</tr>
			<tr>
				<td align="right" class="listwhitetext" width="15px"></td>
				<td align="right" class="listwhitetext"><fmt:message key='agentBase.baseCode'/></td>
				<td><s:textfield cssClass="input-text" key="agentBase.baseCode" required="true" readonly="true"/></td>
				<td align="right" class="listwhitetext" width="15px"></td>
				<td align="right" class="listwhitetext"><fmt:message key='agentBase.description'/></td>
				<td><s:textfield cssClass="input-text" name="agentBase.description"  size="50" readonly="true"/></td>
			</tr>	
			<tr>
			<td align="left" height="10"></td>
			</tr>
		</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<div id="Layer1" style="width: 100%;">
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editAgentBaseSCAC.html?baseId=${agentBase.id}"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="baseAgentSCAC" value="baseAgentSCAC" scope="request"/>  
				<display:table name="baseAgentSCAC" class="table" requestURI="" id="baseAgentSCACList" export="true" defaultsort="1" pagesize="10" style="width: 100%; " >   
					<display:column property="SCACCode" sortable="true" style="width: 150px;" href="editAgentBaseSCAC.html?baseId=${agentBase.id}" paramId="id" paramProperty="id" titleKey="agentBaseSCAC.SCACCode" />
					
					<display:column  sortable="true" titleKey="agentBaseSCAC.internationalLOIflag" >
				        <c:if test="${baseAgentSCACList.internationalLOIflag == true}">
				         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				        </c:if>
				        <c:if test="${baseAgentSCACList.internationalLOIflag == false}">
				        	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				        </c:if>
			        </display:column>
			        
			        <display:column  sortable="true" titleKey="agentBaseSCAC.domesticLOIflag">
				        <c:if test="${baseAgentSCACList.domesticLOIflag == true}">
				         	<img id="active" src="${pageContext.request.contextPath}/images/tick01.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				        </c:if>
				        <c:if test="${baseAgentSCACList.domesticLOIflag == false}">
				        	<img id="target" src="${pageContext.request.contextPath}/images/cancel001.gif" HEIGHT=14 WIDTH=14 ALIGN=TOP />
				        </c:if>
			        </display:column>
					
					<display:column title="Remove" style="width: 15px;">
						<a>
							<img align="middle" title="" onclick="confirmSubmit('${baseAgentSCACList.id}');" style="margin: 0px 0px 0px 8px;" src="images/recyle-trans.gif"/>
						</a>
					</display:column>	 
				    
				    <display:setProperty name="paging.banner.item_name" value="Base Agent"/>   
				    <display:setProperty name="paging.banner.items_name" value="Base Agent"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Base Agent List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Base Agent List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Base Agent List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr>
</table>
</div>
</s:form> 

<script language="javascript" type="text/javascript">
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Base SCAC?");
	var did = targetElement;
	if (agree){
		location.href="deleteBaseSCAC.html?baseId=${agentBase.id}&id="+did;
	}else{
		return false;
	}
}
		
</script>
<script type="text/javascript">   
		try{
			<c:if test="${hitflag == 1}" >
				<c:redirect url="/baseSCACList.html?id=${agentBase.id}" ></c:redirect>
			</c:if>
		}
		catch(e){}
</script> 
