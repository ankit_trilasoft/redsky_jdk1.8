<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>So Date Spreads List</title>   
    <meta name="heading" content="So Date Spreads List"/>  
<style>
span.pagelinks {display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;
margin-top:-18px;!margin-top:-19px;padding:2px 0px;text-align:right;width:100%;!width:100%;
}
div.error, span.error, li.error, div.message {
width:450px;
margin-top:0px; 
}
form {
margin-top:-40px;
!margin-top:-10px;
}
div#main {
margin:-5px 0 0;

}
 div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
 
</style>
</head>   
<s:form id="trackingInfoListByShipNumberForm" method="post" validate="true">  

<div class="spnblk">&nbsp;</div>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<td align="left"  style="padding-left:5px;min-width:120px;">
		<b>SO Date Spreads List</b>
	</td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="hideTooltip();" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<s:set name="trackingInfoListByShipNumber" value="trackingInfoListByShipNumber" scope="request"/>   
<display:table name="trackingInfoListByShipNumber" class="table" requestURI="" id="trackingInfoListByShipNumber" pagesize="1" style="width:100%;" partialList="true" size="1"> 
  <display:column   title="Packing" style="width:190px"/> 
   	 <display:column   title="Loading" style="width:35%"/>
	 <display:column   title="Delivery"  style="width:40%"/>
<display:caption style="margin-top: -11px; !margin-top: -10px;  ">
<tr>
<c:if test="${trackingInfoListByShipNumber.PACKA==''}">
<td>
<c:choose>
<c:when test='${trackingInfoListByShipNumber.ENDPACKA == ""}'>
Pck.Spread:&nbsp;${trackingInfoListByShipNumber.BEGPACKA}</td>
</c:when>
<c:otherwise>
Pck.Spread:&nbsp;${trackingInfoListByShipNumber.BEGPACKA}&nbsp;-&nbsp;${trackingInfoListByShipNumber.ENDPACKA}</td>
</c:otherwise></c:choose>
</c:if>
<c:if test="${trackingInfoListByShipNumber.PACKA!=''}">
<td>Packed:&nbsp;${trackingInfoListByShipNumber.PACKA}</td>
</c:if>

<c:if test="${trackingInfoListByShipNumber.LOADA==''}">
<td>
<c:choose>
<c:when test='${trackingInfoListByShipNumber.ENDLOAD == ""}'>
Ld.Spread:&nbsp;${trackingInfoListByShipNumber.BEGLOAD}</td>
</c:when>
<c:otherwise>
Ld.Spread:&nbsp;${trackingInfoListByShipNumber.BEGLOAD}&nbsp;-&nbsp;${trackingInfoListByShipNumber.ENDLOAD}</td>
</c:otherwise></c:choose>
</c:if>
<c:if test="${trackingInfoListByShipNumber.LOADA!=''}">
<td>Loaded:&nbsp;${trackingInfoListByShipNumber.LOADA}</td>
</c:if>

<c:if test="${trackingInfoListByShipNumber.ETA==''}">
<td>
<c:choose>
<c:when test='${trackingInfoListByShipNumber.DELLASTDAY == ""}'>
Del.Spread:&nbsp;${trackingInfoListByShipNumber.DELSHIP}</td>
</c:when>
<c:otherwise>
Del.Spread:&nbsp;${trackingInfoListByShipNumber.DELSHIP}&nbsp;-&nbsp;${trackingInfoListByShipNumber.DELLASTDAY}</td>
</c:otherwise></c:choose>
</c:if>
<c:if test="${trackingInfoListByShipNumber.ETA!=''}">
<td>ETA:&nbsp;${trackingInfoListByShipNumber.ETA}</td>
</c:if>
<%-- 
  <display:column property="beginPacking" title="Begin&nbsp;Packing" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>
  <display:column property="packA" title="Packing(A)" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>    
  <display:column property="beginLoad" title="Load(T)" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  
  <display:column property="loadA" title="Load(A)" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>  
  <display:column property="deliveryShipper" title="Delivery(T)" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>
  <display:column property="deliveryTA" title="ETA" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>    
  <display:column property="deliveryA" title="Delivery(A)" format="{0,date,dd-MMM-yyyy}" style="width:100px"/>
  --%>
   
</display:caption>
</display:table>
</s:form>

<script type="text/javascript">   
    highlightTableRows("trackingInfoListByShipNumber");  
</script> 