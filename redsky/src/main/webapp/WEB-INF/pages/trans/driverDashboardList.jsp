<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<head>
<title>Driver Dashboard</title>   
    <meta name="heading" content="Driver Dashboard"/> 
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> 
h2 {background-color: #CCCCCC}

 </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/jquery-ui-themes-1.10.4/themes/redmond/jquery-ui.css'/>" />
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script>
  	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-ui.js"></script>
  
<style>
 .ui-autocomplete {
    max-height: 250px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
  * html .ui-autocomplete {
    height: 100px;
  }
  .table tr.even {
    background-color: #DBEEF3;   
}
 span.pagelinks {margin-top:-15px;} 
div#main {margin-top:-30px;}

.total { 
font-weight:bold ; 
background-color: #eee; 
}
.table tr.total td { 
text-align:right !important;
}
#overlay11 {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>

<script language="javascript" type="text/javascript">
function fieldValidate(){
   var date1 = document.forms['driverDashboardForm'].elements['beginDate'].value;	 
   var date2 = document.forms['driverDashboardForm'].elements['endDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);  
  if(daysApart<0){
    alert("End Date should be greater than  or equal to Begin Date");
    document.forms['driverDashboardForm'].elements['endDate'].value='';
    return false;
  }	else if(daysApart>6){
	  alert("Date range should not be more than 7 days.");
	  document.forms['driverDashboardForm'].elements['endDate'].value='';
	  return false;          
 	}
}  

function validateDriverId(){
    var driverId = document.forms['driverDashboardForm'].elements['driverId'].value; 
    var data = 'autocompleteDriverIdAjax.html?ajax=1&decorator=simple&popup=true&driverId ='+driverId+'';
	$( "#driverIdAutoComplete" ).autocomplete({				 
	      source: data		      
	    });
}
function findRevCodeDetails(driverId,position){
	var date1 = document.forms['driverDashboardForm'].elements['beginDate'].value;
	var date2 = document.forms['driverDashboardForm'].elements['endDate'].value;		
	var url='driverDashBoardChildAjax.html?ajax=1&beginDate='+date1+'&endDate='+date2+'&driverId='+driverId+'&decorator=simple&popup=true';
	ajax_SoTooltip(url,position);
}

function findSoDetails(driverId,position){
	var date1 = document.forms['driverDashboardForm'].elements['beginDate'].value;
	var date2 = document.forms['driverDashboardForm'].elements['endDate'].value;		
	var url='driverDashBoardSoDetailsAjax.html?ajax=1&beginDate='+date1+'&endDate='+date2+'&driverId='+driverId+'&decorator=simple&popup=true';
	ajax_SoTooltip(url,position);
}

function checkDate(){
	var date1 = document.forms['driverDashboardForm'].elements['beginDate'].value;
	var date2 = document.forms['driverDashboardForm'].elements['endDate'].value;
    if(date1==''){
    	alert("Please enter the Begin Date"); 
    	return false;
    }else if(date2==''){
    	alert("Please enter the End Date "); 
    	return false;
    }else{
		return true
        }	
}
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["overlay11"].visibility='hide';
        else
           document.getElementById("overlay11").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["overlay11"].visibility='show';
       else
          document.getElementById("overlay11").style.visibility='visible';
   }
}
function goToSearch(){	
	var selectedSearch= checkDate();
	if(selectedSearch){
		    document.forms['driverDashboardForm'].action = 'driverDashboardSearch.html';
		    document.forms['driverDashboardForm'].submit();
		    showOrHide(1);	
		    hideTooltip();			
	}else{
		return false;
	}
}

function checkDriverId(){
    var driverId = document.forms['driverDashboardForm'].elements['driverId'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(driverId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);     
}

function handleHttpResponse4(){
             if (http2.readyState == 4){             
                var results = http2.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){ 		
 				  }
                 else{
                 alert("Driver ID is Incorrect");
                 document.forms['driverDashboardForm'].elements['driverId'].value="";
                 document.forms['driverDashboardForm'].elements['driverId'].select();
                 }                 
                 }
       }
                 
 function getHTTPObject()
         {
                var xmlhttp;
               if(window.XMLHttpRequest){
              xmlhttp = new XMLHttpRequest();
             }
               else if (window.ActiveXObject) {
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)       {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
var http2 = getHTTPObject(); 
</script>
</head>

<s:form name="driverDashboardForm" id="driverDashboardForm" action="" method="post" onsubmit="return goToSearch();">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<div id="Layer1" style="width:100%;">
<div id="otabs">
	  <ul>
	    <li><a class="current"><span>Driver DashBoard</span></a></li>
	  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top: -2px"><span></span></div>
   <div class="center-content">
<table  border="0" class="" style="" cellspacing="0" cellpadding="2">	
	<tr><td height="5px"></td></tr>
	  		
  			<tr> 
  			<td class="listwhitetext" align="right">Begin&nbsp;Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">											
					<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
					<td><s:textfield cssClass="input-text" id="date1" name="beginDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
					<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">					
					<td><s:textfield cssClass="input-text" id="date1" name="beginDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
					<img id="date1_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td width="60px"></td>
				<td class="listwhitetext" align="right">End&nbsp;Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="customerFiledate1FormattedValue" name="${FormDateValue}"><s:param name="value" value="endDate" /></s:text>
					<td><s:textfield cssClass="input-text" id="date2" name="endDate" value="%{customerFiledate1FormattedValue}" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this);" readonly="true"/>
					<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
					<td><s:textfield cssClass="input-text" id="date2" name="endDate" required="true" size="8" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this);"/>
					<img id="date2_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td width="60px"></td>
					<td class="listwhitetext" align="right">Driver&nbsp;ID</td>
                	<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="driverId" id="driverIdAutoComplete" required="true"  size="10" maxlength="8" onchange="checkDriverId();" onkeyup="validateDriverId()"/></td>
			 		<td width="60px"></td>
			 		 <td align="right" class="listwhitebox"><fmt:message key="vanLine.agencya"/></td>
		  			<td align="left"><s:select cssClass="list-menu" name="driverAgency" headerKey="" headerValue="" list="%{vanLineCodeList}" cssStyle="width:90px" onchange="" tabindex="1"/></td> 			 	
				
	  			<td width="60px"></td>		
				<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:65px;" align="top"  method="" value="Submit" onclick=""/></td>
	    	</tr>
	    					
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div> 

 <div id="otabs" >
	<ul>
		<li><a class="current"><span>Driver DashBoard List</span></a></li>
	</ul>
</div> 
<div class="spnblk">&nbsp;</div> 
<s:set name="driverDashboardList" value="driverDashboardList" scope="request"/>

<display:table name="driverDashboardList" class="table"  requestURI="" id="driverDashboardList" defaultsort="1" defaultorder="ascending">

<display:column property="venderCode" sortable="true" title="Driver ID" />
<display:column property="driverName" sortable="true" title="Name"/>
<display:column property="agencyName" sortable="true" title="Agency #"/>
<display:column sortable="true" title="Balance As Of" >
	<fmt:parseDate value="${driverDashboardList.balanceAsOfDate}" pattern="yyyy-MM-dd HH:mm:ss" var="date"/>
	<fmt:formatDate value="${date}" pattern="dd-MMM-yyyy" />
</display:column>

<c:if test="${driverDashboardList!='[]'}">
<c:if test="${driverDashboardList.priorBalance != '0.00'}">
<display:column sortable="true" title="Prior Balance" style="width:8%;">
	<div align="right">
		<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverDashboardList.priorBalance}" />                
     </div>
     </display:column>
     </c:if> 
     <c:if test="${driverDashboardList.priorBalance == '0.00'}">
     <display:column sortable="true" title="Prior Balance" style="width:8%;background:#90EE90; text-align:right">
	<div align="right">
		<c:out value="0.00" />
	</div>
</display:column>
</c:if> 
</c:if>
<c:if test="${driverDashboardList=='[]'}">
<display:column property="priorBalance" sortable="true" title="Prior Balance" style="width:8%;" />
</c:if>
<display:column sortable="true" title="Advance / Charge Back" style="width:11%;" >
<c:if test="${driverDashboardList.advanceChargeBack != '0.00'}">
<a><div align="right" " onclick ="findRevCodeDetails('${driverDashboardList.venderCode}',this);" onmouseout="">	
	<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverDashboardList.advanceChargeBack}" /> 
     </div> </a>  
</c:if> 

<c:if test="${driverDashboardList.advanceChargeBack == '0.00'}">
<div align="right">
	<c:out value="0.00" />
</div>
</c:if> 
</display:column>

<display:column sortable="true" title="# of Jobs" >
<div align="right">
	<c:out value="${driverDashboardList.shipNumber}" />
</div>
</display:column>
<display:column sortable="true" title="Payables" >
<c:if test="${driverDashboardList.actualExpense != '0.00'}">
<a>	<div align="right"  onclick="findSoDetails('${driverDashboardList.venderCode}',this);">
			<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverDashboardList.actualExpense}" />          
     </div></a>
     </c:if>
  <c:if test="${driverDashboardList.actualExpense == '0.00'}">
<div align="right">
	<c:out value="0.00" />
</div>
</c:if>   
 </display:column>
<c:if test="${driverDashboardList!='[]'}">
	<c:set var="value" value="${driverDashboardList.newBalance}"/>
	 <c:choose> 
	<c:when test="${fn:contains(value, '-')}">
	<display:column sortable="true" title="New Balance" style="background:#FF3232; text-align:right" >
			<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverDashboardList.newBalance}" />           
     </display:column>
	</c:when> 
	<c:when test="${driverDashboardList.newBalance == '0.00'}">
	<display:column sortable="true" title="New Balance"  style="text-align:right">
	<div align="right">
		<c:out value="0.00" />
	</div>
	</display:column>
	</c:when>
	<c:otherwise>
	<display:column sortable="true" title="New Balance"  style="text-align:right">
			<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2"
                  groupingUsed="true" value="${driverDashboardList.newBalance}" />              
     </display:column>
	</c:otherwise>
	</c:choose>
</c:if>
<c:if test="${driverDashboardList=='[]'}">
<display:column property="newBalance" sortable="true" title="New Balance"  style="text-align:right"/>
</c:if>
</display:table>  
<div id="overlay11">
		 	<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
			<td align="center">
			<table cellspacing="0" cellpadding="3" align="center">
			<tr>
			<td height="200px"></td>
			</tr>
			<tr>
	       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
	           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait<br>Driver settlement results are being computed!...</font>
	       </td>
	       </tr>
       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           <img src="<c:url value='/images/ajax-loader.gif'/>" />     
       
       </td>
       </tr>
       </table>
       </td>
       </tr>
       </table>
			</div>
</s:form>
<script type="text/javascript">
	setOnSelectBasedMethods(["fieldValidate()"]);
	setCalendarFunctionality();
	showOrHide(0);
</script>
