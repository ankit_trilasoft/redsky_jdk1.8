package com.trilasoft.app.dao.hibernate;

import java.util.List;   
import org.appfuse.dao.hibernate.GenericDaoHibernate;   
import com.trilasoft.app.dao.ConsigneeInstructionDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.ConsigneeInstruction;   
    
public class ConsigneeInstructionDaoHibernate extends GenericDaoHibernate<ConsigneeInstruction, Long> implements ConsigneeInstructionDao {   
      public ConsigneeInstructionDaoHibernate()
      {   
        super(ConsigneeInstruction.class);   
    }   
      private HibernateUtil hibernateUtil;
  	
  	public void setHibernateUtil(HibernateUtil hibernateUtil){
          this.hibernateUtil = hibernateUtil;
  	}
      public List<ConsigneeInstruction> findBySequenceNumber(String sequenceNumber) {   
        return getHibernateTemplate().find("from ConsigneeInstruction where sequenceNumber=?", sequenceNumber);   
    } 
      public List findMaximumId() {
      	return getHibernateTemplate().find("select max(id) from ConsigneeInstruction");
      }
      public List checkById(Long id) {
      	return getHibernateTemplate().find("select id from ConsigneeInstruction where id=?",id);
      }
	public List getByShipNumber(String shipNumber) {
		return getHibernateTemplate().find("from ConsigneeInstruction where shipNumber=?",shipNumber);
	}
	public List findConsigneeCode(String shipNumber) {
		return getHibernateTemplate().find("select destinationAgentCode from TrackingStatus where shipNumber='"+shipNumber+"'");
	}
	public List findConsigneeAddress(String DaCode) {
		return this.getSession().createSQLQuery("select concat(if(lastName is null || lastName ='',1, lastName),'~',if(billingAddress1 is null || billingAddress1 ='',1, billingAddress1),'~',if(billingAddress2 is null || billingAddress2 ='',1, billingAddress2),'~',if(billingAddress3 is null || billingAddress3 ='',1, billingAddress3),'~',if(billingAddress4 is null || billingAddress4 ='',1, billingAddress4),'~',if(billingPhone is null || billingPhone ='',1, billingPhone))from partner where partnerCode = '"+DaCode+"' ").list() ;
	}
	 public List findConsigneeNCAddress(String shipNum){
		 return this.getSession().createSQLQuery("select concat(if(s.lastname is null || s.lastname='',1,s.lastname),'~',if(s.firstname is null || s.firstname ='',1,s.firstname),'~',if(s.destinationAddressLine1 is null || s.destinationAddressLine1 ='',1,s.destinationAddressLine1),'~',if(s.destinationAddressLine2 is null || s.destinationAddressLine2 ='',1,s.destinationAddressLine2),'~',if(s.destinationAddressLine3 is null || s.destinationAddressLine3 ='',1,s.destinationAddressLine3),'~',if(s.destinationDayPhone is null || s.destinationDayPhone ='',1,s.destinationDayPhone),'~',if(s.destinationHomePhone is null || s.destinationHomePhone ='',1,s.destinationHomePhone),'~',if(s.destinationMobile is null || s.destinationMobile ='',1,s.destinationMobile),'~',if(s.destinationCountry is null || s.destinationCountry ='',1,s.destinationCountry),'~',if(s.destinationCity is null || s.destinationCity ='',1,s.destinationCity),'~',if(s.destinationState is null || s.destinationState ='',1,s.destinationState),'~',if(s.destinationZip is null || s.destinationZip ='',1, s.destinationZip)) from serviceorder s  where s.shipNumber ='"+shipNum+"' ").list(); 
	 }
	
	public List findConsigneeJobAddress(String DaCode, String shipNum) {
		return this.getSession().createSQLQuery("select concat(if(p.lastName is null || p.lastName ='',1, p.lastName),'~',if(p.terminalAddress1 is null || p.terminalAddress1 ='',1, p.terminalAddress1),'~',if(p.terminalAddress2 is null || p.terminalAddress2 ='',1, p.terminalAddress2),'~',if(p.terminalAddress3 is null || p.terminalAddress3 ='',1, p.terminalAddress3),'~',if(p.terminalAddress4 is null || p.terminalAddress4 ='',1, p.terminalAddress4),'~',if(p.terminalPhone is null || p.terminalPhone ='',1, p.terminalPhone),'~',if(s.lastname is null || s.lastname='',1,s.lastname),'~',if(s.firstname is null || s.firstname ='',1,s.firstname),'~',if(s.destinationAddressLine1 is null || s.destinationAddressLine1 ='',1,s.destinationAddressLine1),'~',if(s.destinationAddressLine2 is null || s.destinationAddressLine2 ='',1,s.destinationAddressLine2),'~',if(s.destinationAddressLine3 is null || s.destinationAddressLine3 ='',1,s.destinationAddressLine3),'~',if(s.destinationDayPhone is null || s.destinationDayPhone ='',1,s.destinationDayPhone),'~',if(s.destinationHomePhone is null || s.destinationHomePhone ='',1,s.destinationHomePhone),'~',if(s.destinationMobile is null || s.destinationMobile ='',1,s.destinationMobile),'~',if(s.destinationCountry is null || s.destinationCountry ='',1,s.destinationCountry),'~',if(s.destinationCity is null || s.destinationCity ='',1,s.destinationCity),'~',if(p.terminalCountry is null || p.terminalCountry ='',1,p.terminalCountry),'~',if(p.terminalCity is null || p.terminalCity ='',1,p.terminalCity),'~',if(p.terminalState is null || p.terminalState ='',1,p.terminalState),'~',if(s.destinationState is null || s.destinationState ='',1,s.destinationState),'~',if(p.terminalZip is null || p.terminalZip ='',1, p.terminalZip),'~',if(s.destinationZip is null || s.destinationZip ='',1, s.destinationZip)) from partner p, serviceorder s where p.partnerCode = '"+DaCode+"' and s.shipNumber ='"+shipNum+"' ").list();
	}
	public List findShipmentLocation(String consigneeInstructionCode,String corpID) {
		return this.getSession().createSQLQuery("select concat(if(lastName is null || lastName ='',1, lastName),'~',if(terminalAddress1 is null || terminalAddress1 ='',1, terminalAddress1),'~',if(terminalAddress2 is null || terminalAddress2 ='',1, terminalAddress2),'~',if(terminalAddress3 is null || terminalAddress3 ='',1, terminalAddress3),'~',if(terminalAddress4 is null || terminalAddress4 ='',1, terminalAddress4),'~',if(terminalCity is null || terminalCity ='',1, terminalCity),'~',if(terminalState is null || terminalState ='',1, terminalState),'~',if(terminalCountry is null || terminalCountry ='',1, terminalCountry),'~',if(terminalPhone is null || terminalPhone ='',1, terminalPhone),'~',if(terminalZip is null || terminalZip ='',1, terminalZip)) from partnerpublic where partnerCode = '"+consigneeInstructionCode+"' and corpID in ('"+corpID+"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"')").list() ;
	}
	public List findConsigneeSubAgentAddress(String sACode, String shipNum) {
		return this.getSession().createSQLQuery("select concat(if(p.lastName is null || p.lastName ='',1, p.lastName),'~',if(p.terminalAddress1 is null || p.terminalAddress1 ='',1, p.terminalAddress1),'~',if(p.terminalAddress2 is null || p.terminalAddress2 ='',1, p.terminalAddress2),'~',if(p.terminalAddress3 is null || p.terminalAddress3 ='',1, p.terminalAddress3),'~',if(p.terminalAddress4 is null || p.terminalAddress4 ='',1, p.terminalAddress4),'~',if(p.terminalCity is null || p.terminalCity ='',1, p.terminalCity),'~',if(p.terminalState is null || p.terminalState ='',1, p.terminalState),'~',if(p.terminalCountry is null || p.terminalCountry ='',1, p.terminalCountry),'~',if(p.terminalPhone is null || p.terminalPhone ='',1, p.terminalPhone),'~',if(s.lastname is null || s.lastname='',1,s.lastname),'~',if(s.firstname is null || s.firstname ='',1,s.firstname),'~',if(p.terminalZip is null || p.terminalZip ='',1, p.terminalZip)) from partner p, serviceorder s where p.partnerCode = '"+sACode+"' and s.shipNumber ='"+shipNum+"' ").list() ;
	}
	public List consigneeInstructionList(Long serviceOrderId) {
		return  getHibernateTemplate().find("from ConsigneeInstruction where serviceOrderId=?",serviceOrderId);
	}
	public Long findRemoteConsigneeInstruction(Long serviceOrderId) {
		return Long.parseLong(this.getSession().createSQLQuery("select id from consigneeInstruction where serviceOrderId="+serviceOrderId+"").list().get(0).toString());
	}
	public List getShiperAddress(String corpID,String shipNum){
		return this.getSession().createSQLQuery("select concat(if(s.bookingAgentName is null || s.bookingAgentName='',1,s.bookingAgentName),'~',if(s.firstName is null || s.firstName ='',1,s.firstName),'~',if(s.lastName is null || s.lastName='',1,s.lastName),'~',if(s.originAddressLine1 is null || s.originAddressLine1 ='',1,s.originAddressLine1),'~',if(s.originAddressLine2 is null || s.originAddressLine2 ='',1,s.originAddressLine2),'~',if(s.originAddressLine3 is null || s.originAddressLine3 ='',1,s.originAddressLine3),'~',if(s.originCity is null || s.originCity ='',1,s.originCity),'~',if(s.originState is null || s.originState='',1,s.originState),'~',if(s.originZip is null || s.originZip ='',1,s.originZip),'~',if(s.originCountry is null || s.originCountry ='',1,s.originCountry),'~',if(s.originDayPhone is null || s.originDayPhone ='',1,s.originDayPhone)) from serviceorder s  where s.shipNumber ='"+shipNum+"' and s.corpID='"+corpID+"' ").list();
	}
	public List consigneeInstructionListOtherCorpId(Long sid){
		return this.getSession().createSQLQuery("select id from consigneeInstruction where serviceOrderId="+sid+"").list();
	}
}