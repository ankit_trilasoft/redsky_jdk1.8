package com.trilasoft.app;

public class CalendarDTO implements Comparable<CalendarDTO>{
	public int id;
    public String title;
    public String start;
    public String end;
    public String color;
    public String fromtime;
    public boolean allDay;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public boolean isAllDay() {
		return allDay;
	}
	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}
	public int compareTo(CalendarDTO o) {
		// TODO Auto-generated method stub
		String compareQuantity = ((CalendarDTO) o).getFromtime();
		String ss[]=null;
		Integer i1=0;
		Integer i2=0;
		if(compareQuantity!=null){
		 ss=compareQuantity.split(":");
		
		if(ss!=null && ss[0]!=null){
			i1=Integer.parseInt(ss[0]);
			
		}
		}
		String compareQuantity1 = this.getFromtime();
		String ss1[]=null;
		if(compareQuantity1!=null){
		ss1=compareQuantity1.split(":");		
		if(ss1!=null && ss1[0]!=null){
			i2=Integer.parseInt(ss1[0]);
			
		}
		}
		
		 
		//ascending order
		return i1 - i2;
		//return 0;
	}
	public String getFromtime() {
		return fromtime;
	}
	public void setFromtime(String fromtime) {
		this.fromtime = fromtime;
	}
    

}
