
package com.trilasoft.app.model;

import org.appfuse.model.BaseObject;   

import java.math.BigDecimal;
import java.util.*;

import javax.persistence.Entity;
import javax.persistence.GenerationType;   
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Column;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name="location")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
//@Table(appliesTo="location", indexes = { @Index(name="locOcc",columnNames={"occupied","location"} ) } )

public class Location extends BaseObject implements ListLinkData {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String capacity;
	private BigDecimal cubicFeet;
	private BigDecimal cubicMeter;
	private BigDecimal utilizedVolCft;
	private BigDecimal utilizedVolCbm;
	private String locationId;
	private String warehouse;
	private String occupied;
	private String corpID;
	private String type;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	/*private Set<Storage> storages;
	private Set<BookStorage> bookStorages;
	private WorkTicket workTicket;*/

//private Set storage;
	

	//private BookStorage bookStorage;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("capacity", capacity).append("cubicFeet", cubicFeet)
				.append("cubicMeter", cubicMeter)
				.append("utilizedVolCft", utilizedVolCft)
				.append("utilizedVolCbm", utilizedVolCbm)
				.append("locationId", locationId)
				.append("warehouse", warehouse).append("occupied", occupied)
				.append("corpID", corpID).append("type", type)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof Location))
			return false;
		Location castOther = (Location) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(capacity, castOther.capacity)
				.append(cubicFeet, castOther.cubicFeet)
				.append(cubicMeter, castOther.cubicMeter)
				.append(utilizedVolCft, castOther.utilizedVolCft)
				.append(utilizedVolCbm, castOther.utilizedVolCbm)
				.append(locationId, castOther.locationId)
				.append(warehouse, castOther.warehouse)
				.append(occupied, castOther.occupied)
				.append(corpID, castOther.corpID).append(type, castOther.type)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(capacity)
				.append(cubicFeet).append(cubicMeter).append(utilizedVolCft)
				.append(utilizedVolCbm).append(locationId).append(warehouse)
				.append(occupied).append(corpID).append(type).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.toHashCode();
	}
	@Column(length=1)
	public String getCapacity() {
		return capacity;
	}
	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=40)
	//@Index(name = "location")
	public String getLocationId() {
		return locationId;
	}
	
	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}
	@Column(length=1)
	public String getOccupied() {
		return occupied;
	}
	public void setOccupied(String occupied) {
		this.occupied = occupied;
	}
	@Column(length=3)
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Column(length=15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length=82)  
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=10)
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	
	@Column(length=15)
	public BigDecimal getCubicMeter() {
		return cubicMeter;
	}
	public void setCubicMeter(BigDecimal cubicMeter) {
		this.cubicMeter = cubicMeter;
	}
	@Column(length=15)
	public BigDecimal getUtilizedVolCft() {
		return utilizedVolCft;
	}

	public void setUtilizedVolCft(BigDecimal utilizedVolCft) {
		this.utilizedVolCft = utilizedVolCft;
	}
	@Column(length=15)
	public BigDecimal getUtilizedVolCbm() {
		return utilizedVolCbm;
	}

	public void setUtilizedVolCbm(BigDecimal utilizedVolCbm) {
		this.utilizedVolCbm = utilizedVolCbm;
	}
	
	@Column(length=15)
	public BigDecimal getCubicFeet() {
		return cubicFeet;
	}
	
	public void setCubicFeet(BigDecimal cubicFeet) {
		this.cubicFeet = cubicFeet;
	}
	
	@Transient
	public String getListCode() {
		if (this.locationId == null){
			locationId="";
		}
		return locationId;
	}
	@Transient
	public String getListDescription() {
		return "";
	}
	@Transient
	public String getListSecondDescription() {
		return "";
	}
	@Transient
	public String getListThirdDescription() {
		return "";
	}
	@Transient
	public String getListFourthDescription() {
		return "";
	}
	@Transient
	public String getListFifthDescription() {
		return "";
	}
	@Transient
	public String getListSixthDescription() {
		return "";
	}
	@Transient
	public String getListSeventhDescription() {
		return "";
	}
	@Transient
	public String getListEigthDescription() {
	    return "";
	}
	@Transient
	public String getListNinthDescription() {
		return "";
	}
	@Transient
	public String getListTenthDescription() {
		return "";
	}
}