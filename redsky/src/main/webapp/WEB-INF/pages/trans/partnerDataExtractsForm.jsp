<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title><fmt:message key="dataExtracts.title"/></title>
    <meta name="heading" content="<fmt:message key='reportsDataExtact.heading'/>"/>
    <style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
	
	<style type="text/css">                
 .input-textUpper {
background-color:#E3F1FE;
border:1px solid #ABADB3;
color:#000000;
font-family:arial,verdana;
font-size:12px;
height:15px;
text-decoration:none;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
	<script language="javascript" type="text/javascript"><!--
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject(); 
        
 
 function valButton(btn) {
    var cnt = -1;  
    var len = btn.length;  
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
        document.forms['reportDataExtractForm'].elements['radiobilling'].checked=true;
    	return document.forms['reportDataExtractForm'].elements['radiobilling'].value; 
    } 
  }
    function findPreviewBillingReport(){ 
      		var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);  
      		var check=formValidate('none');
		  if (btn == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else if(btn ==1)
	       {
           		  if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
           		    document.forms['reportDataExtractForm'].action ='partnerShipmentExtract.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    return true;
           		 } 
        	}
        	
	   	
   }
   
    function formValidate(clickType)
    { 
    animatedcollapse.hide('billing'); 
    if(clickType!='none'){
    	 valButton(document.forms['reportDataExtractForm'].elements['radiobilling']); 
    	 }
    if(document.forms['reportDataExtractForm'].elements['conditionC14'].value != "" || document.forms['reportDataExtractForm'].elements['routingTypes'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC16'].value != "" || document.forms['reportDataExtractForm'].elements['packModeType'].value != ""
       || document.forms['reportDataExtractForm'].elements['conditionC120'].value != "" || document.forms['reportDataExtractForm'].elements['oACountryType'].value != ""
       || document.forms['reportDataExtractForm'].elements['conditionC15'].value != "" || document.forms['reportDataExtractForm'].elements['modeType'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC17'].value != "" || document.forms['reportDataExtractForm'].elements['commodityType'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" || document.forms['reportDataExtractForm'].elements['dACountryType'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC200'].value != "" || document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value != ""
       || document.forms['reportDataExtractForm'].elements['conditionC201'].value != "" || document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value != ""  
       || document.forms['reportDataExtractForm'].elements['conditionC21'].value != "" || document.forms['reportDataExtractForm'].elements['beginDate'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC22'].value != "" || document.forms['reportDataExtractForm'].elements['endDate'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC211'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value != ""
       || document.forms['reportDataExtractForm'].elements['conditionC222'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value != ""    
       || document.forms['reportDataExtractForm'].elements['conditionC233'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value != "" 
       || document.forms['reportDataExtractForm'].elements['conditionC244'].value != "" || document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value != ""  )
    {
     if(document.forms['reportDataExtractForm'].elements['conditionC14'].value != "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value == "")
    	{
    		alert("Routing type value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC14'].value == "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value != "")
    	{
    		alert("Routing types condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value != "" && document.forms['reportDataExtractForm'].elements['packModeType'].value == "")
    	{
    		alert("PackMode value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value == "" && document.forms['reportDataExtractForm'].elements['packModeType'].value != "")
    	{
    		alert("PackMode condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value != "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value == "")
    	{
    		alert("OACountry type value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value == "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value != "")
    	{
    		alert("OACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value != "" && document.forms['reportDataExtractForm'].elements['modeType'].value == "")
    	{
    		alert("Mode type value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value == "" && document.forms['reportDataExtractForm'].elements['modeType'].value != "")
    	{
    		alert("Mode type condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value != "" && document.forms['reportDataExtractForm'].elements['commodityType'].value == "")
    	{
    		alert("Commodity  type value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value == "" && document.forms['reportDataExtractForm'].elements['commodityType'].value != "")
    	{
    		alert("Commodity type condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value == "")
    	{
    		alert("DACountry type value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value == "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value != "")
    	{
    		alert("DACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['conditionC200'].value != "" && document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value == "")
    	{
    		alert("Begin Load Target Date value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC200'].value == "" && document.forms['reportDataExtractForm'].elements['loadTgtBeginDate'].value != "")
    	{
    		alert("Begin Load Target Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC201'].value != "" && document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value == "")
    	{
    		alert("End Load Target Date value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC201'].value == "" && document.forms['reportDataExtractForm'].elements['loadTgtEndDate'].value != "")
    	{
    		alert("End Load Target Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC21'].value != "" && document.forms['reportDataExtractForm'].elements['beginDate'].value == "")
    	{
    		alert("Begin load date value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC21'].value == "" && document.forms['reportDataExtractForm'].elements['beginDate'].value != "")
    	{
    		alert("Begin load date  condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC22'].value != "" && document.forms['reportDataExtractForm'].elements['endDate'].value == "")
    	{
    		alert("End load date value is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC22'].value == "" && document.forms['reportDataExtractForm'].elements['endDate'].value != "")
    	{
    		alert("End load date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC211'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value == "")
    	{
    		alert("Begin Delivery Target Date value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC211'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryTBeginDate'].value != "")
    	{
    		alert("Begin Delivery Target Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC222'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value == "")
    	{
    		alert("End Delivery Target Date  value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC222'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryTEndDate'].value != "")
    	{
    		alert("End Delivery Target Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC233'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value == "")
    	{
    		alert("Begin Delivery Actual Date  value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC233'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryActBeginDate'].value != "")
    	{
    		alert("Begin Delivery Actual Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC244'].value != "" && document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value == "")
    	{
    		alert("End Delivery Actual Date value is Blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC244'].value == "" && document.forms['reportDataExtractForm'].elements['deliveryActEndDate'].value != "")
    	{
    		alert("End Delivery Actual Date condition is blank, Please select.");
    		document.getElementById("hid").style.display="none"
    		return false;
    	}
    	else
    	{
    	    document.getElementById("hid").style.display="inline";  
          		if(clickType == "none")
          		{
          			 document.getElementById("hid").style.display="none";
          			return true; 
          		}
          		else
          		{
          			return false;
          		}
    	}
    	
    }
    
    else {
    	    alert("Please select at least one  extract parameter before selecting report.");
    		document.getElementById("hid").style.display="none"
    		return false; 
     }
     
 }
 
 
function buttonDisable()
  {       
  		document.getElementById("hid").style.display="none";
  		  //document.getElementById("button1").style.visibility="collapse";
         
          //document.getElementById("button2").style.visibility="collapse";
         
          //document.getElementById("button3").style.visibility="collapse";
       
  }
  
  function progress()
{
		setTimeout("alert('hello')",1250);
		var tend = "</tr></table>";
		var tstrt = "<table><tr>";
		scell = scell + "<td style='width:10;height:10' bgcolor=red>";
		document.getElementById("cell1").innerHTML = sbase + tstrt +  scell + tend;     
		 if( i < 50) // total 50 cell will be created with red color.
		 {                         i = i + 1;
			     timerID = setTimeout("progress()",1000); // recursive call
		 }
		 else
		 {
		    if(timerID)
			  {
			    clearTimeout(timerID);
			  }
		}
}

/*function checkFile11()
{
	if( document.forms['reportDataExtractForm'].elements['file'].value!="" )
   		{
  			toggleBox('demodiv',1);
  			event.returnValue = true;
  		} 
  	else 
  	   {
  	     event.returnValue = false;
	   }
    
}*/

 function openPopWindow(){
		
		javascript:openWindow('extractPartnersPopup.html?partnerType=DE&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCodeType');
}
function openBookingAgentPopWindow(){
		
		javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=bookingCodeType');
}  
--></script>
<style type="text/css">
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px;width:99%; border:1px solid #99BBE8; border-right:1px solid #99BBE8; border-left:1px solid #99BBE8; border-bottom: none;} 
a.dsphead{text-decoration:none;color:#000000;padding-top:5px;}
.dspchar2{padding-left:0px;}

.listblackText {
color:#000;
font-family:arial,verdana;
font-size:12px;
font-weight:bold;
text-decoration:none;
}

</style>

<style type="text/css">


/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
.subcontent-tab:hover{background:url(images/collapsebg_hover.gif) #f7efd8;}

a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:0px;}

</style>

<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('billing', 'fade=0,persist=1,hide=1')



animatedcollapse.init()

</script>
</head>

<s:form id="reportDataExtractForm" name="reportDataExtractForm" action="partnerDataExtracts" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="reports.id" value="%{reports.id}"/> 
<div id="Layer1" style="width:100%" >
<div id="content" align="center">
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" style="width:100%; margin-bottom:5px" cellspacing="0" cellpadding="0" border="0" >
<tr><td>
<table style="margin-bottom:0px"  border="0">
<tr><td height="5px"></td></tr>	  
<tr>
<td  class="bgblue" >Extract Parameters</td></tr>
<tr><td height="2px"></td></tr>
<!--<tr><td width="10px"> </td>
<td style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #888787; padding-left: 40px;">Please select the corresponding conditions and values to view reports.</td></tr>
	--></table></td></tr>
	
	 <tr><td>
	  	   <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:0px">
	  		<tr><td height="0px"></td>	  		
	  		</tr>
	  	
	  		<tr>
	  			<td  class="heading" colspan="3">
	  			<span style="padding:0 8% 0 15%;!padding:0 50px 0 60px;">Field</span><span style="padding:0px 100px 0px 0px;">Condition</span><span style="padding:0px 40px 0px 0px;">Value</span>
	  			</td>
	  			
	  			<td  class="heading" colspan="4">
	  			<span style="padding:0 8% 0 12%;!padding:0 50px 0 60px;">Field</span><span style="padding:0px 100px 0px 0px;">Condition</span><span style="padding:0px 40px 0px 0px;">Value</span>
	  			</td>
	  	   </tr>
	  	  
	  	  </table></td></tr>
	  	  <tr><td>
	  	  <table style="margin-bottom:0px" cellpadding="1" cellspacing="0" width="100%" border="0">
	  	   
	  	   <!--<tr>
	  	   
	  	   <td width="150px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:110px" id="c1" name="conditionC1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px" colspan="3"><s:select cssClass="list-menu" cssStyle="width:310px;height:50px"  name="jobTypes" list="%{jobtype}"  multiple="true"/></td>
	  	    <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Job Type</b>
             </td>
	  	   
	  	   </tr>-->
	  	    <tr><td ></td>
	  	   <td ></td></tr>
	  	   <tr>
	  	   
	  	   <td width="150px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:103px" id="c1" name="jobTypesCondition" list="{'Equal to','Not Equal to'}" value="%{jobTypesCondition}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px" colspan="2"><s:select cssClass="list-menu" cssStyle="width:240px;height:50px"  name="jobTypes" list="%{jobtype}"   value="%{multiplejobType}" multiple="true"  /></td>
	  			
	  			<td width="20px" align="right" class="vertlinedata_vert" rowspan="7" style="height:95px;!height:105px;background-position:top right;width:5px;padding-left:13px;" valign="top"></td>
	  			
	  	    <td class="listwhitetext" align="left" colspan="4" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Job Type</b>
             </td>
	  	   
	  	   </tr>
	  	   <tr><td ></td>
	  	   <td ></td></tr>
	  	   <tr><td ></td>
	  	   <td ></td>
	  	   <td width="113px"></td>
	  	   <td  width=""></td>
	  	   
	  	   
	  	  <td></td>
	  	   
	  	
	  	    <td width="63px"></td>
	  	   <td></td>
	  	   <td width="113px"></td>
	  	   <td width="120px"></td>
	  	   </tr>
	  	   
	  		<!--<tr>
	  		<c:if test="${companyDivisionFlag=='Yes'}"> 
	  		<td class="listwhitetext" align="right">Company Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="conditionC115" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="companyCode" list="%{companyCodeList}"  headerKey="" headerValue="" /></td>
	  			
	  		</c:if>	
	  		<td width="150px" class="listwhitetext" align="right" height="">Job Status</td>
			<td  width="5px"></td>
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:110px" id="activeStatus" name="activeStatus" list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
	  		 </tr>
	  		 
	  		 <tr>
	  			<td class="listwhitetext" align="right">Bill To Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c4" name="conditionC12" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td valign="top"><s:textfield cssClass="input-text" name="billToCodeType" size="11" maxlength="6" onchange="findBillToCode()"/>
	  			<img class="openpopup" width="17" align="top" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		
	  			<td class="listwhitetext" align="right">Booking Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c5" name="conditionC13" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td valign="top"><s:textfield  cssClass="input-text" name="bookingCodeType" size="11" maxlength="6" onchange="findBookingAgentCode()"/>
	  			<img class="openpopup" width="17" height="20" align="top" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		</tr>
	  		-->
	  		<tr>
	  	
	  		<td  class="listwhitetext" align="right" height="">Job Status</td>
			<td  width="1px"></td>
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:100px" id="activeStatus" name="activeStatus" list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
	  		 <td></td>
	  		 <td></td>
	  		 <td></td>
	  		 <td></td>
	  		 <td></td>
	  		 </tr>
	  		<tr>
	  			<td class="listwhitetext" align="right" width="11%">Routing</td>
	  			<td></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="c6" name="conditionC14" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  			<td colspan="2"><s:select cssClass="list-menu" cssStyle="width:240px"  name="routingTypes" list="%{routing}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right" >Mode</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="c7" name="conditionC15" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:240px"  name="modeType" list="%{mode}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Pack Mode</td>
	  			<td></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="c8" name="conditionC16" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td colspan="2"><s:select cssClass="list-menu" cssStyle="width:240px"  name="packModeType" list="%{pkmode}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">Commodity</td>
	  			<td></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px"  id="c9" name="conditionC17" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		   
	  			<td ><s:select cssClass="list-menu"  cssStyle="width:240px"  name="commodityType" list="%{commodit}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<!--<tr>
	  			<td class="listwhitetext" align="right">Coordinator</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c10" name="conditionC18" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="coordinatorType" list="%{coord}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">Salesman</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c11" name="conditionC19" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="salesmanType" list="%{sale}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		--><tr>
	  			<td class="listwhitetext" align="right">OA Country</td>
	  			<td></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="c12"  name="conditionC120" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td colspan="2"><s:select cssClass="list-menu" cssStyle="width:240px"  name="oACountryType" list="%{ocountry}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">DA Country</td>
	  			<td></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:100px" id="c13"  name="conditionC121" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  			<td ><s:select cssClass="list-menu" cssStyle="width:240px"  name="dACountryType" list="%{dcountry}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<!--<tr>
	  			<td class="listwhitetext" align="right">Actual Wgt</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c15" name="conditionC3" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td><s:textfield cssClass="input-text" id="actualWgtType" name="actualWgtType"  size="16" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td>
	  		
	  			<td class="listwhitetext" align="right">Actual Volume</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c16" name="conditionC31" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td><s:textfield cssClass="input-text" id="actualVolumeType" name="actualVolumeType" value="%{actualVolumeType}" size="16" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/>
	  			</td>
	  			
	  		</tr>
	  		 
	  		 --><tr><td  height="13" class="listwhitetext" colspan="20" style="text-align:left;" width="100%">
	  		<div  onClick="javascript:animatedcollapse.toggle('billing')" style="cursor: pointer;">
     
          <table cellpadding="0" cellspacing="0" width="100%"  style="margin: 0px;">
<tr>
<td class="headtab_left">
</td>
<td NOWRAP class="headtab_center" >&nbsp;Please enter the date ranges for the Activity Dates
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special">&nbsp;
</td>
<td class="headtab_right">
</td>
</tr>
</table>
</font></div>
	  		 
	  		 
	  		
				<div class="dspcont" id="billing" >
				
	  		<!--<tr>
	  		
	  			<td width="150px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:110px" id="c1" name="conditionC1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:210px;height:50px"  name="jobTypes" list="%{jobtype}"   headerKey="" headerValue="" multiple="true"/></td>
	  		
	  		  
	  			<td class="listwhitetext" align="right">Created On</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c14" name="conditionC122" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  		<c:if test="${not empty createdOnType}">
						<s:text id="createdOnType" name="${FormDateValue}"> <s:param name="value" value="createdOnType" /></s:text>
						<td><s:textfield cssClass="input-text" id="createdOnType" name="createdOnType" value="%{createdOnType}" size="11" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="calender6" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['reportDataExtractForm'].createdOnType,'calender6',document.forms['reportDataExtractForm'].dateFormat.value); return false;" /></td>
			</c:if>
			<c:if test="${empty createdOnType}" >
						<td><s:textfield cssClass="input-text" id="createdOnType" name="createdOnType" required="true" size="11" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="calender6" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['reportDataExtractForm'].createdOnType,'calender6',document.forms['reportDataExtractForm'].dateFormat.value); return false;" /></td>
			</c:if>	
			
			<td width="150px" class="listwhitetext" align="right" height="">Job Status</td>
			<td  width="5px"></td>
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:110px" id="activeStatus" name="activeStatus" list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
			<%-- <td align="left"><s:checkbox name="activeStatus" fieldValue="true" value="true" onclick=""onchange="" /></td>--%>
	  		<td></td><td width="150"></td><td colspan="2"></td>
	  		</tr>-->
	  		<table class="detailTabLabel" cellspacing="2" cellpadding="1" border="0" width="">
			<tbody>
	  		<tr>
	  			
	  			<td class="listwhitetext" align="right" width="">Load Target Date</td>
	  			
	  			<td width="120px"><s:select cssClass="list-menu" cssStyle="width:110px"  id="conditionC200" name="conditionC200" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<c:if test="${not empty loadTgtBeginDate}">
						<s:text id="loadTgtBeginDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtBeginDate" /></s:text>
						<td width="143px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" value="%{loadTgtBeginDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty loadTgtBeginDate}" >
						<td width="143px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		<td width="5px"></td>
	  			<td class="listwhitetext" align="right" width="166px">Load Target Date</td>
	  			
	  			<td width="120px">
	  			<s:select cssClass="list-menu" cssStyle="width:110px" id="conditionC201" name="conditionC201" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  	
	  		<c:if test="${not empty loadTgtEndDate}">
						<s:text id="loadTgtEndDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtEndDate" /></s:text>
						<td width="157px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" value="%{loadTgtEndDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty loadTgtEndDate}" >
						<td width="157px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Load Actual Date</td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c2" name="conditionC21" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"> <s:param name="value" value="beginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty beginDate}" >
						<td><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		<td></td>
	  			<td class="listwhitetext" align="right">Load Actual Date</td>
	
	  			<td>
	  			<s:select cssClass="list-menu" cssStyle="width:110px" id="c3" name="conditionC22" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  	
	  		<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty endDate}" >
						<td><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Delivery Target Date</td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="conditionC211" name="conditionC211" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<c:if test="${not empty deliveryTBeginDate}">
						<s:text id="deliveryTBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" value="%{deliveryTBeginDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryTBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		<td></td>
	  			<td class="listwhitetext" align="right">Delivery Target Date</td>
	  		
	  			<td>
	  			<s:select cssClass="list-menu" cssStyle="width:110px" id="conditionC222" name="conditionC222" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  	
	  		<c:if test="${not empty deliveryTEndDate}">
						<s:text id="deliveryTEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" value="%{deliveryTEndDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryTEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Delivery Actual Date</td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="conditionC233" name="conditionC233" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<c:if test="${not empty deliveryActBeginDate}">
						<s:text id="deliveryActBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" value="%{deliveryActBeginDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryActBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>
	  		<td></td>
	  			<td class="listwhitetext" align="right">Delivery Actual Date</td>
	
	  			<td>
	  			<s:select cssClass="list-menu" cssStyle="width:110px" id="conditionC244" name="conditionC244" list="{'Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  	
	  		<c:if test="${not empty deliveryActEndDate}">
						<s:text id="deliveryActEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" value="%{deliveryActEndDate}" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryActEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" required="true" size="8" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr> 
	  		</tbody>
	  		</table>
	  		</div>	  		
	  		</td>
	  		</tr>
	  		<!--  
	  		  <c:if test="${companyDivisionFlag=='Yes'}"> 
	  		<tr>
	  		<td class="listwhitetext" align="right">Company Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="conditionC115" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="companyCode" list="%{companyCodeList}"  headerKey="" headerValue="" /></td>
	  			
	  			
	  		 </tr>		 
	  		
	  		</c:if> --> 
	  		 <tr>
	  			<td height="5px" colspan="4"></td>
	  		</tr>
	  		
	  		<tr>
   	          
   	           <td align="left" colspan="15" class="vertlinedata"></td>
   	        
   	        </tr>
   	        
   	        <tr><td height="5px"></td></tr>
   	        
   	        <tr>
   	        
   	          <td align="center" colspan="10" width="100%"><s:submit cssClass="cssbutton" cssStyle="width:94px; height:27px" align="absmiddle"  value="View Reports" onclick = "return formValidate();"/>
   	          <s:reset cssClass="cssbutton" cssStyle="width:55px; height:27px" key="Reset" /></td>
   	          </td>
   	          <s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:choose>
							<c:when test="${submitType == 'job'}">
							<td align="center" colspan="7"><s:reset type="button" cssClass="cssbutton" align="top" cssStyle="width:100px; height:25px" key="Reset" /></td>
							</c:when>
							
	 					</c:choose>
   	         </tr>
			<tr>
	  			<td height="20px" colspan="4"></td>
	  		</tr>
	  		</table></td></tr>
     </table> 
     <!-- Arvind  -->
     <div id="hid">
    <table class="" style="width:100%; margin-bottom:0px" cellspacing="0" cellpadding="0" border="0">
    <tr><td height="10px"> </td></tr>
    <tr><td width="15px"> </td>
	<td  class="bgblue" >Select Reports</td></tr>
	<tr><td width="10px"></td><td><table style="margin-bottom:0px">
	  		
	  			  			  		
	  	   <tr>
	  	   <td height="25px" width="200px" class="listblackText" align="left" style="padding-left:10px; !width:180px;">
	  	   <label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i1" value="1"/><b>Partner Shipment Extract</b></label></td>
	  	   <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"><a href="#"><img src="<c:url value='/images/info.gif'/>" width="23" height="23" border="0" /></a></td>
   	       <td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract Info</td>
   	      </tr> 
	  	   	<tr>
   	         <td align="left" style="padding-left:18px;padding-top:10px;">
   	           <input type="button" class="cssbutton" style="width:110px; height:32px" align="top"  value="Show Reports" onclick="return findPreviewBillingReport();"/>
   	         </td>
   	          <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"></td>
   	          </tr>
   	          <tr>
   	          <td height="20px"></td>
   	          	  	   
	  	   </tr>
	  		</table>
	  		</td>
	  		</tr>
	  		</table>
	  		
	  	</div>	
	  	</div>
	  <div class="bottom-header"><span></span></div>
</div>
	 	
	  	</div>
	  
	  		
</div>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />



</s:form>
 
<script type="text/javascript"> 
     try{
     buttonDisable(); 
    }
    catch(e){}
   try{
    document.forms['reportDataExtractForm'].elements['radiobilling'].checked=true;
	}
	catch(e){}
</script> 

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>