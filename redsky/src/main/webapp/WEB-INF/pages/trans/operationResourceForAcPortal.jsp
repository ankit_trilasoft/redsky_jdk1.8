<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<head>


<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<%-- <s:head theme="ajax" /> --%>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<style>
table-fc tbody th, .table tbody td {white-space:nowrap;}
.table td, .table th, .tableHeaderTable td {padding: 0.3em;}
.pr-f11{font-family:arial,verdana;font-size:11px;}
.header-rt {border-right:medium solid #003366 !important;} 
</style>

<script language="javascript" type="text/javascript">
        <%@ include file="/common/formCalender.js"%>
    </script> 
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <title><fmt:message key="operationResource.title" /></title>
  <meta name="heading" content="<fmt:message key='operationResource.heading'/>" />
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
<%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}"> 
<configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
    animatedcollapse.addDiv('resources', 'fade=0,persist=0,hide=1')
    animatedcollapse.addDiv('operationsDetails', 'fade=0,persist=0,hide=1')
</configByCorp:fieldVisibility>
</c:if> 
animatedcollapse.init();
</script>
<script language="javascript" type="text/javascript">

var httpTemplate = getHTTPObject();
var httpResource = getHTTPObject();
function getDayTemplate(){
    var id = '${serviceOrder.id}';
    var contract = '${customerFile.contract}';
    if(id.length == 0){
        alert('Please Save Quote first before adding Resource Template.');
    }else {
        showOrHide(1);
        var url ="resouceTemplateForAccPortal.html?ajax=1&shipNumber=${serviceOrder.shipNumber}&contract=${customerFile.contract}&tempId=${serviceOrder.id}";
        httpTemplate.open("GET", url, true);
        httpTemplate.onreadystatechange = handleHttpResponseForTemplate;
        httpTemplate.send(null);
    }
}
function handleHttpResponseForTemplate(){
    if (httpTemplate.readyState == 4){
        var results= httpTemplate.responseText;
        findAllResource('A');
    }
}


function findAllResource(target){
    var resourceDiv = document.getElementById("resourcMapAjax");
    var shipNumber='${serviceOrder.shipNumber}';
    $.get("findAllResourcesAjaxForPortal.html?ajax=1&decorator=simple&popup=true", 
            {shipNumber:shipNumber,resouceTemplate: target},
            function(data){
            	showOrHide(0);
                $("#resourcMapAjax").html(data);
        });
} 


function showOrHide(value) {
    if (value == 0) {
        if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
    }else if (value == 1) {
        if (document.layers)
          document.layers["overlay"].visibility='show';
        else
          document.getElementById("overlay").style.visibility='visible';
    }
}


function findAllOperationsDetails(currentId)
{
    var resourceDiv = document.getElementById("operationsDetailsAjax");
    var shipNumber='${serviceOrder.shipNumber}';
    $.get("findAllDistinctResources.html?ajax=1&decorator=simple&popup=true", 
            {shipNumber:shipNumber},  
            function(data){
                $("#operationsDetailsAjax").html(data);
                if(currentId!=null && currentId!=undefined && currentId!=''){
                document.getElementById("show-"+currentId).click();
                }
        });
        
}

</script>
</head>
<s:form name="operationResourceForm" id="operationResourceForm" action="saveOperationResource" method="post" validate="true">
    <s:hidden name="tempId1"></s:hidden>
    <s:hidden name="tempId2"></s:hidden>    
  <c:set var="idOfWhom" value="${serviceOrder.id}" scope="session" /> 
                <c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session" /> 
                <c:set var="noteFor" value="ServiceOrder" scope="session" /> 
                <c:if test="${empty serviceOrder.id}">
                <c:set var="isTrue" value="false" scope="request" />
                </c:if> 
                <c:if test="${not empty serviceOrder.id}">
                <c:set var="isTrue" value="true" scope="request" />
                </c:if>     
       
       <div id="newmnav" style="float:none;">
          <ul>
                 <ul>
            <li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
           <%--   <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
             <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
             </sec-auth:authComponent>
             <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
             <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
             </sec-auth:authComponent> --%>
              <li id="newmnav1" style="background:#FFF "><a class="current" ><span>O&I<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
              <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
                 <li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
                 </sec-auth:authComponent>
                <li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
          <%-- <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=OI&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
           <li><a onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder&decorator=popup&popup=true','audit','height=400,width=770,top=100, left=120, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>  --%>   
         
         <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
         <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
    </sec-auth:authComponent>
         
         </ul>
        </ul>
        </div>
        
        <div class="spn">&nbsp;</div>
      <div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">             
    <table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%" >
        <tbody>
        <tr>
            <td>
            <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="">
                <tbody>
                <tr><td height="5px"></td></tr>
                    <tr>
                    &nbsp;
                        <td align="right" class="listwhitetext" style="width:85px"></td>
                        <td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.prefix' /></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="left" class="listwhitetext" valign="bottom">Title / Rank</td>
                        <td align="left" class="listwhitetext" width="5px"></td>
                        <td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.firstName' /></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="center" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.mi' /></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="left" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.lastName' /></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="left" class="listwhitetext" valign="bottom"><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber"><fmt:message key='serviceOrder.socialSecurityNumber'/></configByCorp:fieldVisibility></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="right" class="listwhitetext" style="width:5px"></td>
                        <td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
                        <%-- Added by kunal for ticket number: 6092 and 6218(later) --%>
                        <c:set var="isVipFlag" value="false" />
                        <c:if test="${serviceOrder.vip}">
                            <c:set var="isVipFlag" value="true" />
                        </c:if>
                        </td>
                        </tr>
                        <tr>
                        <td align="left" style="width:85px"></td>
                        <td align="left" style="width:5px"><s:textfield cssClass="input-textUpper" id="tabindexFlag" name="serviceOrder.prefix" cssStyle="width:25px" maxlength="15" onfocus="myDate();" readonly="true" tabindex=""/></td>
                        <td align="left" style="width:3px">
                        <td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="serviceOrder.rank" cssStyle="width:55px" maxlength="7" readonly="true" tabindex=""/> </td>
                        <td align="left" style="width:3px">
                        <td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.firstName" cssStyle="width:155px" maxlength="80" readonly="true" tabindex=""  /></td>
                        <td align="left" style="width:3px"></td>
                        <td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.mi" cssStyle="width:13px" maxlength="1" readonly="true" tabindex="" /></td>
                        <td align="left" style="width:17px"></td>
                        <td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.lastName" cssStyle="width:183px" maxlength="80" required="true" readonly="true" tabindex="" /></td>
                        <td align="left" style="width:3px"></td>
                        <td align="left" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber">
                        <s:textfield cssClass="input-textUpper" key="serviceOrder.socialSecurityNumber" size="7" maxlength="9" readonly="true" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="" cssStyle="width:88px"/>
                        </configByCorp:fieldVisibility></td>
                      <%--   <td align="right"><s:checkbox id="serviceOrder.vip" name="serviceOrder.vip" value="${isVipFlag}" fieldValue="true" disabled="true" onchange="changeStatus();" onclick="setVipImage();" tabindex="" /></td>
                        <td align="left" class="listwhitebox" valign="middle"><fmt:message key='serviceOrder.vip' /></td> --%>
                        <td width="85px"></td>                      
                    </tr>
                    </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                     <td>
                       <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                        <tr>
                        <td height="5px"></td>
                        </tr>
                            <tr>
                                <td style="width:85px"></td>
                                <td align="left" class="listwhitetext">S/O#</td>
                                <td align="right" class="listwhitetext" style="width:7px"></td>                             
                                <c:choose>
                                <c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
                                <td></td>
                                </c:when>
                                <c:otherwise>
                                <td align="left" class="listwhitetext">Status</td>
                                </c:otherwise>
                                </c:choose>
                                <c:if test="${checkAccessQuotation}">
                                <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
                                <td align="left" class="listwhitetext" valign="bottom"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteStatus',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.quoteStatus'/></a></td>                               
                                </c:if>
                                </c:if>
                                <td align="right" class="listwhitetext" style="width:7px"></td>
                                <td align="left" class="listwhitetext">Status Date</td>
                                <td align="left" class="listwhitetext" style="width:7px"></td>                              
                            </tr>
                            </tr>
                            <tr>
                                <td align="left" style="width:85px"></td>
                                <td align="left" style="width:27px">
                                <s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" cssStyle="width:276px" size="22" maxlength="15" readonly="true" tabindex="" /></td>
                                <td align="left" style="width:17px"></td>
                                <c:choose>
                                <c:when test="${checkAccessQuotation && (not empty serviceOrder.moveType && serviceOrder.moveType=='Quote') && serviceOrder.controlFlag=='C'}">
                                <td><s:hidden name="serviceOrder.status" /></td>
                                </c:when>
                                <c:otherwise>
                               <td><s:select cssClass="list-menuUpper" disabled="true" name="serviceOrder.status" list="%{JOB_STATUS}" required="false"  onchange="checkStatus();updateOperationIntilligence('${serviceOrder.id}','status',this,'ServiceOrder','${serviceOrder.shipNumber}');" cssStyle="width:185px" tabindex="" /></td>
                                </c:otherwise>
                                </c:choose>
                                <c:if test="${checkAccessQuotation}">                           
                                <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
                                <td align="left" style=""><s:select cssClass="list-menuUpper" disabled="true" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:168px" onchange="checkQuotation();changeStatus();" tabindex="" /></td>
                                </c:if>
                                
                                </c:if>
                                <c:if test="${not empty serviceOrder.statusDate}">
                                    <s:text id="statusDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.statusDate"  />
                                    </s:text>
                                    <td align="left" style="width:7px"></td>
                                    <td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:87px" maxlength="11" readonly="true" tabindex="" /></td>
                                </c:if>
                                <c:if test="${empty serviceOrder.statusDate}">
                                    <td align="left" style="width:7px"></td>
                                    <td><s:textfield cssClass="input-textUpper" name="serviceOrder.statusDate" cssStyle="width:87px" maxlength="11" readonly="true" tabindex="" /></td>
                                </c:if>
                                <td align="left" class="listwhitetext"></td>
                                
                                <script type="text/javascript">
                                 /* cheackStatusReasonLoad() */;
                                </script>                              
                            </tr>
                        </tbody>
                    </table>
                    </td>
                    </tr>
                    <tr>
                        <td height="5px"></td>
                        </tr>
                        <tr>
                        <td>
                        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="right" class="listwhitetext" style="width:85px"></td>
                                    <td align="left" class="listwhitetext">Origin&nbsp;City/State</td>
                                    <td width="7px"></td>
                                    <td align="left" class="listwhitetext" style="">Country</td>
                                    <td align="left" class="listwhitetext" style="width:7px"></td>
                                    <td align="left" class="listwhitetext">Destination&nbsp;City/State</td>
                                    <td width="7px"></td>
                                    <td align="left" class="listwhitetext" style="">Country</td>
                                </tr>
                                <tr>
                                    <td align="right" class="listwhitetext" style="width:85px"></td>
                                    <td align="left" class="listwhitetext">
                                    <s:textfield cssClass="input-textUpper" cssStyle="width:220px" name="serviceOrder.originCityCode" size="22" maxlength="30" readonly="true" tabindex="" /></td>
                                    <td width="7px"></td>
                                    <td align="left" class="listwhitetext">
                                    <s:textfield cssClass="input-textUpper" name="serviceOrder.originCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
                                    <td align="right" class="listwhitetext" style="width:17px"></td>
                                    <td align="left" class="listwhitetext" >
                                    <s:textfield cssClass="input-textUpper" key="serviceOrder.destinationCityCode" cssStyle="width:220px" size="22" maxlength="30" readonly="true" tabindex="" /></td>
                                    <td width="9px"></td>
                                    <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCountryCode" cssStyle="width:45px" maxlength="30" readonly="true" tabindex="" /></td>
                                    <td width="55px"></td>                                  
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>                   
                        <tr>
                        <td>
                        <table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
                            <tbody>
                            <tr>
                            <td style="width:85px; !width: 85px"></td>
                            <td align="left" class="listwhitetext">Code</td>
                            <td></td>
                            <td align="left" class="listwhitetext" style="width:10px">Name</td>
                            <td></td>
                            
                            <td align="left" class="listwhitetext" style="width:10px"><c:if  test="${companies == 'Yes'}">Company&nbsp;Division</c:if></td>
                            </tr>
                                <tr>
                                    <td align="right" class="listwhitetext" style="">Booking&nbsp;Agent:&nbsp;</td>
                                    <td><s:textfield cssClass="input-textUpper" disabled="true" id="soBookingAgentCodeId" name="serviceOrder.bookingAgentCode" cssStyle="width:135px;" maxlength="8" onchange="valid(this,'special');findBookingAgentName()"  tabindex=""/></td>
                                    <td style="width:20px"><%-- <img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="openBookingAgentPopWindow(); document.forms['operationResourceForm'].elements['serviceOrder.bookingAgentCode'].select();" src="<c:url value='/images/open-popup.gif'/>" /> --%></td>
                                    <td align="left"><s:textfield cssClass="input-textUpper" disabled="true" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('soBookingAgentNameId','soBookingAgentCodeId','soBookingAgentNameDivId',' and (isAccount=true or isAgent=true or isVendor=true )','',event);" onchange="findPartnerDetailsByName('soBookingAgentCodeId','soBookingAgentNameId');"  size="44" maxlength="250" cssStyle="width:247px" tabindex="" />
                                   <!--  <div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div> -->
                                    </td>
                                    
                                    <td width="24px"></td>
                                    <c:if  test="${companies == 'Yes'}">
                                        <td align="left">
                                        <c:if test="${not empty serviceOrder.id}">
                                        <s:select  cssClass="list-menuUpper" id="companyDivision" disabled="true" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:145px" onchange="updateOperationIntilligence('${serviceOrder.id}','companyDivision',this,'ServiceOrder','${serviceOrder.shipNumber}');" headerKey="" headerValue="" tabindex="" />
                                        </c:if>
                                        <c:if test="${empty serviceOrder.id}">
                                        <s:select  cssClass="list-menuUpper" id="companyDivision" disabled="true" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:145px" onchange="populateInclusionExclusionData('default','1','F');getJobList('unload');changeStatus();checkTransferDate(); changebAgentCheckedBy();" headerKey="" headerValue="" tabindex="" />
                                        </c:if>
                                        </td>
                                    </c:if>
                                    <c:if  test="${companies != 'Yes'}">
                                        <s:hidden name="serviceOrder.companyDivision"/>
                                    </c:if>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                        <table class="detailTabLabel" border="0 " cellpadding="0" cellspacing="0" width="">
                            <tbody>
                            <tr><td height="5px"></td></tr>
                                    <tr>                                     
                                    <td align="right" class="listwhitetext" style="width:85px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td align="left" class="listwhitetext" valign="bottom" style=""><div id="registrationHideLabel">
                                    <fmt:message key='serviceOrder.registrationNumber' /></div></td>
                                    <td align="right" class="listwhitetext" style="width:15px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom" style="">
                                    <fmt:message key='serviceOrder.job' /></td>
                                    <td align="left" class="listwhitetext" style="padding-left:10px;" valign="bottom"><fmt:message key='serviceOrder.projectManager' /></td>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <td align="right" class="listwhitetext" style="width:8px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom">
                                    <div id="labelEstimator"><fmt:message key='serviceOrder.estimator' />
                                    </div></td>
                                </tr>
                                <tr>
                                    <td align="right">                           
                                    </td> 
                                    <td align="left" style=""><div id="registrationHide"><s:textfield cssClass="input-textUpper" disabled="true" name="serviceOrder.registrationNumber" id="regist" onchange="updateOperationIntilligence('${serviceOrder.id}','registrationNumber',this,'ServiceOrder','${serviceOrder.shipNumber}');" onkeyup="valid(this,'special')"
                                        cssStyle="width:135px;" maxlength="20" required="true" tabindex="" /></div></td>
                                    <td align="right" class="listwhitetext" style="width:20px"></td>                                    
                                    <td align="left" style="width:100px">
                                    <c:if test="${not empty serviceOrder.id}">
                                    <s:select id="jobService" cssClass="list-menuUpper" key="serviceOrder.job" list="%{job}"
                                                                            cssStyle="width:121px" headerKey="" headerValue=""
                                                                            onchange="updateOperationIntilligence('${serviceOrder.id}','job',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" disabled="true"/>
                                    </c:if>     
                                    <c:if test="${empty serviceOrder.id}">
                                    <s:select id="jobService" cssClass="list-menuUpper" key="serviceOrder.job" list="%{job}"
                                                                            cssStyle="width:121px" headerKey="" headerValue=""
                                                                            onchange="updateOperationIntilligence('${serviceOrder.id}','job',this,'ServiceOrder','${serviceOrder.shipNumber}');"  tabindex="" disabled="true"/>
                                    </c:if>                                 
                                        </td>
                                        
                                        <td style="padding-left:10px;"><s:select cssClass="list-menuUpper" disabled="true"
                                        name="serviceOrder.projectManager" id="projectManager" list="%{projectManagerList}"
                                        cssStyle="width:119px" headerKey="" headerValue=""
                                        onchange="updateOperationIntilligence('${serviceOrder.id}','projectManager',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></td>
                                    <td align="right" class="listwhitetext" style="width:10px"></td>
                                    <script type="text/javascript">
                                    try{
                                     checkJobTypeOnLoad();
                                    }catch(e){}
                                    </script>
                                    <td align="left" style="width:5px"></td>
                                    <td align="left" style="width:10px">
                                    <div id="EstimatorService1">
                                    <s:select cssClass="list-menuUpper" name="serviceOrder.estimator" disabled="true"
                                        list="%{sale}" id="estimator" cssStyle="width:145px" headerKey=""
                                        headerValue="" onchange="updateOperationIntilligence('${serviceOrder.id}','estimator',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
                                    </div>
                                        </td>
                                <td width="5px"></td>
                                </tr>
                                <tr><td height="5px"></td></tr>
                                <tr>
                                    
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom">
                                </tr>
                                <tr>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                </tr>
                                <tr><td height="2px"></td></tr>
                            
                                <tr><td height="4px"></td></tr>
                                <tr>
                                    <td align="right" class="listwhitetext" style="width:77px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom" ><div id="hidStatusSalesLabel"><fmt:message
                                        key='serviceOrder.salesMan' /></div></td>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom" colspan="2">
                                    <div id="hidStatusCommoditLabel"><fmt:message key='serviceOrder.commodity' /></div>
                                    </td>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <td align="right" class="listwhitetext" style="width:13px"></td>
                                    <td align="left" class="listwhitetext" valign="bottom"><fmt:message
                                        key='serviceOrder.coordinator' /></td>                                   
                                </tr>
                                <tr>
                                    <td align="left" style="width:25px"></td>
                                    <td ><div id="hidStatusSalesText"><s:select cssClass="list-menuUpper" disabled="true"
                                        name="serviceOrder.salesMan" id="salesMan" list="%{estimatorList}"
                                        cssStyle="width:140px" headerKey="" headerValue=""
                                        onchange="updateOperationIntilligence('${serviceOrder.id}','salesMan',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></div></td>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <c:choose>
                                        <c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='STL' ||serviceOrder.job =='STF' ||serviceOrder.job =='TPS'}">
                                            <td align="left" colspan="2"><div id="hidStatusCommoditText">
                                            <c:if test="${not empty serviceOrder.id}">
                                            <s:select
                                                cssClass="list-menuUpper" name="serviceOrder.commodity" disabled="true"
                                                list="%{commodits}" cssStyle="width:251px"
                                                onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
                                                </c:if>
                                                <c:if test="${empty serviceOrder.id}">
                                            <s:select
                                                cssClass="list-menuUpper" name="serviceOrder.commodity" disabled="true"
                                                list="%{commodits}" cssStyle="width:251px"
                                                onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
                                                </c:if>
                                                </div></td>
                                        </c:when>
                                        <c:otherwise>
                                            <td align="left" colspan="2"><div id="hidStatusCommoditText">
                                            <c:if test="${not empty serviceOrder.id}">
                                            <s:select
                                                cssClass="list-menuUpper" name="serviceOrder.commodity" disabled="true"
                                                list="%{commodit}" cssStyle="width:251px"
                                                onchange="updateOperationIntilligence('${serviceOrder.id}','commodity',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" />
                                                </c:if>
                                        <c:if test="${empty serviceOrder.id}">
                                            <s:select
                                                cssClass="list-menuUpper" name="serviceOrder.commodity" disabled="true"
                                                list="%{commodit}" cssStyle="width:251px"
                                                onchange="validAutoBoat();populateInclusionExclusionData('default','1','F');changeStatus();" tabindex="" />
                                                </c:if>     
                                                
                                                </div></td>
                                        </c:otherwise>
                                    </c:choose>
                                    <td align="right" class="listwhitetext" style="width:5px"></td>
                                    <td align="right" style="width:5px"></td>
                                    <td><s:select cssClass="list-menuUpper" disabled="true"
                                        name="serviceOrder.coordinator" id="coordinator" list="%{coordinatorList}"
                                        cssStyle="width:145px" headerKey="" headerValue=""
                                        onchange="updateOperationIntilligence('${serviceOrder.id}','coordinator',this,'ServiceOrder','${serviceOrder.shipNumber}');" tabindex="" /></td>
                                        
                                </tr>                               
                                   <tr><td height="5px"></td></tr>
                                                           
                            </tbody>
                        </table>
                    </tr>
                    </tbody>
                    </table>
                    
                  
                  
                    <table cellspacing="0" cellpadding="0" border="0" width="100%" style="margin:0px;" class="detailTabLabel">
                   <%--  <c:if test="${serviceOrder.job =='OFF'}">	--%>
<c:if test="${fn:indexOf(oiJobList,serviceOrder.job)>=0}"> 
    <configByCorp:fieldVisibility componentId="component.field.Resource.Visibility">
  <tr>
    <td height="10" width="100%" align="left" style="margin:0px">
        <div id="resourcefar" onClick="javascript:animatedcollapse.toggle('resources'); findAllResource('');" style="margin:0px">
             <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
                <tr>
                    <td class="headtab_left"></td>
                    <td NOWRAP class="headtab_center">&nbsp;&nbsp;Resources</td>
                    <td width="28" valign="top" class="headtab_bg"></td>
                    <td class="headtab_bg_center">&nbsp;</td>
                    <td class="headtab_right"></td>
                </tr>
            </table>
         </div>
         <div id="resources" class="switchgroup1">
                <table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;" >
                   
                </table>
                <div id="resourcMapAjax">
                </div>
        </div>
        
</td>
</tr>
  <tr>
    <td height="10" width="100%" align="left" style="margin:0px">
        <div id="resourcefar1" onClick="javascript:animatedcollapse.toggle('operationsDetails'); findAllOperationsDetails('');" style="margin:0px">
             <table cellpadding="0" cellspacing="0"  border="0" width="100%" style="margin: 0px">
                <tr>
                    <td class="headtab_left"></td>
                    <td NOWRAP class="headtab_center">&nbsp;&nbsp;Operations&nbsp;Details</td>
                    <td width="28" valign="top" class="headtab_bg"></td>
                    <td class="headtab_bg_center">&nbsp;</td>
                    <td class="headtab_right"></td>
                </tr>
            </table>
         </div>
         <div id="operationsDetails" class="switchgroup1">
                <table cellpadding="0" cellspacing="0"  class="detailTabLabel" border="0" style="margin:0px;padding:0px;" >
                   
                </table>
                <div id="operationsDetailsAjax">
                </div>
        </div>
</td>
</tr>



</configByCorp:fieldVisibility>
</c:if>
    </table>
    </div>
<div class="bottom-header" style="margin-top:50px;z-index:999;"><span></span></div>
</div>
</div>      
</div>  
</s:form>
<script language="javascript" type="text/javascript">
function ajax_hideTooltip(){if(ajax_tooltipObj){ajax_tooltipObj.style.display="none"}if(ajax_tooltipObjBig){ajax_tooltipObjBig.style.display="none"}}

function showCheckDetails(position,workOrder,ticket){
	  var shipNumber='${serviceOrder.shipNumber}';
	     var url="showTaskDetails.html?ajax=1&decorator=simple&popup=true&shipNumber=" + encodeURI(shipNumber)+"&ticket="+encodeURI(ticket);
	      ajax_showTooltip(url,position);   
}

var flag = false;
function saveInTaskDetails(target,fieldName,corpId,id,workorder,shipNumber,currentId){
	var status = document.getElementById ("status"+id).value;
	var selectedWO = document.getElementById ("wo"+id).value;
	if(fieldName=='status'){
	    var selectedWO = document.getElementById ("wo"+id).value;
		if(selectedWO != null && selectedWO !='' ){
			flag = true;
			saveInTaskDetailsForStatus(target,fieldName,corpId,id,workorder,currentId);
		}else{
			flag = false;
		}
	}else{
		flag = false;	
	}
	if(fieldName=='workorder'){
		flag = false;
	    if((selectedWO == workorder)){
	        alert("Task Is Already In "+workorder+"." );
	        document.getElementById ("wo"+id).value='';
	        return false;
	    }  if(status !='Transfer'){
        	alert("Please Change The Status to Transfer.")
            flag = true;
        	document.getElementById ("wo"+id).value='';
        	return false;
        }else{
        var datails= document.getElementById ("details"+id).value;
        var date= document.getElementById ("date"+id).value;
        var time = document.getElementById ("startTime"+id).value;
        if((datails == null || datails == '')){
        alert("For Transferring the task to another work order, Please fill task details.");
        		document.getElementById ("wo"+id).value='';
                return false;
        }
        	
        }
    }
	if(!flag){
	$.ajax({
        type: "POST",
        url: "autoSaveTaskDetails.html?ajax=1&decorator=simple&popup=true",
        data: {id:id,fieldName:fieldName,fieldValue:target.value,sessionCorpID:corpId,selectedWO:selectedWO,shipNumber:shipNumber},
        success: function (data, textStatus, jqXHR) { 
        	if(document.getElementById ("status"+id).value == 'Transfer' && document.getElementById ("wo"+id).value != ''){
        		document.getElementById ("status"+id).disabled=true;
                document.getElementById ("wo"+id).disabled=true;
        	}
        	findAllOperationsDetails(currentId);
        },
      error: function (data, textStatus, jqXHR) {
              }
      });
	}
}

function checkMsg(target)
{
    document.getElementById(target.id).style.height = "17px";

}
function checkMsg1(target)
{
    document.getElementById(target.id).style.height = "45px";
    document.getElementById(target.id).style.width = "200px;";

}

function saveInTaskDetailsForStatus(target,fieldName,corpId,id,workOrder,currentId){
	var status = document.getElementById ("status"+id).value;
	var selectedWO = document.getElementById ("wo"+id).value;
		$.ajax({
	        type: "POST",
	        url: "autoSaveTaskDetails.html?ajax=1&decorator=simple&popup=true",
	        data: {id:id,fieldName:fieldName,fieldValue:target.value,sessionCorpID:corpId,workOrder:selectedWO},
	        success: function (data, textStatus, jqXHR) { 
	        	 findAllOperationsDetails(currentId);
	        },
	      error: function (data, textStatus, jqXHR) {
	              }
	      });
	}

var currentClickId;
var corpIdForTask;
function setFieldId(id,corpid){
    currentClickId = id;
	corpIdForTask=corpid;
}

function saveDateInTaskDetails(){
    if(currentClickId!=undefined && currentClickId!=null && currentClickId!=''){
    	var id=currentClickId;
        var fieldName = "date";
         var date = document.getElementById ('date'+id).value;
         $.ajax({
             type: "POST",
             url: "autoSaveTaskDetails.html?ajax=1&decorator=simple&popup=true",
             data: {id:id,fieldName:fieldName,fieldValue:date,sessionCorpID:corpIdForTask},
             success: function (data, textStatus, jqXHR) {     
             },
           error: function (data, textStatus, jqXHR) {
             }
           });
    }   
}


window.onload=function(){
    findAllResource('');
    findAllOperationsDetails('');
//animatedcollapse.show(['resources']);              
animatedcollapse.show(['operationsDetails']); 
}

function onlyTimeFormatAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
}

function completeTimeString(id)
{
var stime3=document.getElementById ('startTime'+id).value;

if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
    if(stime3.length==1 || stime3.length==2){
        if(stime3.length==2){
            document.getElementById ('startTime'+id).value = stime3 + ":00";
        }
        if(stime3.length==1){
            document.getElementById ('startTime'+id).value = "0" + stime3 + ":00";
        }
    }else{
        document.getElementById ('startTime'+id).value = stime3 + "00";
    }
}else{
    if(stime3.indexOf(":") == -1 && stime3.length==3){
        document.getElementById ('startTime'+id).value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
    }
    if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
        document.getElementById ('startTime'+id).value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
    }
    if(stime3.indexOf(":") == 1){
        document.getElementById ('startTime'+id).value = "0" + stime3;
    }
}
var startTlmeValue=document.getElementById ('startTime'+id).value;
var t = startTlmeValue.split(':');

  if( t[0] >= 0 && t[0] < 24 && t[1] >= 0 && t[1] < 60)
    {
    document.getElementById ('startTime'+id).value=startTlmeValue;;
    }else{
    if(t[0] < 0 || t[0] >= 24)
    {
    alert("Hour must be between 0 and 23");
    }else
    if(t[1] < 0 || t[1] >= 60){
    alert("Minute must be between 0 and 59");
    }
    document.getElementById ('startTime'+id).value='00:00';
    }
  }
  
function auditSetUp(id){
    window.open('auditList.html?decorator=popup&popup=true&id='+id+'&tableName=taskdetails&decorator=popup&popup=true','audit','height=400,width=790,top=100,left=150, scrollbars=yes,resizable=yes').focus();
}

function transferResourceToWorkTicket(shipNumber){
    var id="${serviceOrder.id}"
    window.open('transferResourceForOIAccPortal.html?decorator=popup&popup=true&shipNumber='+shipNumber+'&id='+id+'&emailFlag=true&decorator=popup&popup=true','accountProfileForm','height=500,width=1080,top=0,scrollbars=yes,resizable=yes').focus();
    }

function contractForOI(target){
    var contractCheck = '${contractForOI}';
    if(contractCheck == null || contractCheck == '')
      {
        var agree=confirm("No contract has been selected for the rates to be displayed for resources");
      /*   alert("No contract has been selected for the rates to be displayed for resources"); */
          if (agree){ 
              return false;
          }else{
              if(target.id == 'resourceTemplate')
                  {
                  getDayTemplate();
                  }else{
              addResources();
          }}
        }else{
            if(target.id == 'resourceTemplate')
              {
                var conf = 'Continue with Billing Contract : '+contractCheck;
              if(confirm(conf)){
              getDayTemplate();
              }}else{
          addResources();
        }
}}

function addResources(){
    var ship='${serviceOrder.shipNumber}';
    $.get("addResourcesAjaxInPortal.html?ajax=1&decorator=simple&popup=true", 
            { shipNumber:ship},
            function(data){             
            $("#resourcMapAjax").html(data);
        });
}


function validateWorkOrder(id){
    var workOrder=document.getElementById("workOrderOI"+id).value;
    if(workOrder==''){
        alert("Please enter WO# "); 
    }
}
</script>