<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 

    <title><fmt:message key="dataExtracts.title"/></title>
    <meta name="subcontenttabChild" content="<fmt:message key='reportsDataExtact.subcontenttabChild'/>"/>
   
	
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==190) || (keyCode==110) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==35) || (keyCode==36)|| (keyCode==110) ; 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
	}
	
	function onlyAlphaNumericAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36); 
	}	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	function onlyPhoneNumsAllowed(evt)
	{
		
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  //alert(keyCode);
	  //return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109) || (keyCode==35) || (keyCode==36) || (keyCode==32) || (keyCode==190) || (keyCode==189) ; 
	}
</script>
	<script language="javascript" type="text/javascript"><!--
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
function findBillToCode()
{
     var billToCode = document.forms['reportDataExtractForm'].elements['billToCodeType'].value; 
     billToCode=billToCode.trim();
     if(billToCode!=''){
     var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode="+encodeURI(billToCode); 
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
     }
}
function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
                 }
                 else
                 {
                     alert("Invalid Bill To code, please select another");
                     document.forms['reportDataExtractForm'].elements['billToCodeType'].value = "";
					
                 }
             }
        }
        
 function findBookingAgentCode(){
         var bookingAgentCode = document.forms['reportDataExtractForm'].elements['bookingCodeType'].value;
         bookingAgentCode=bookingAgentCode.trim();
         if(bookingAgentCode!=''){
         var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
         http2.open("GET", url, true);
         http2.onreadystatechange = handleHttpResponse4;
         http2.send(null);
         }
  }
  
  function createTextAreaSave(){
  
   var answer = prompt ("Enter Query Name","");
  
   return answer;
    
  }
  

  
  
  
  
  
  function createTextAreaSave1()
  {
  var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);
      		var check=formValidate('none');
		  if (btn == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       }
	   else{
           window.open('extractSupport.html?decorator=popup&popup=true&from=file','','top=300,left=400,width=500,height=250');
           return true;
           }
  }
  
  
  
  function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                if(results.length>=1)
                {
 					
 				}
                 else
                 {
                     alert("Invalid Booking Agent code, please select another");
                     document.forms['reportDataExtractForm'].elements['bookingCodeType'].value="";
					 
                 }
             }
    } 
 function valButton(btn) {
    var cnt = -1;
    for (var i=btn.length-1; i > -1; i--) {
        if (btn[i].checked) {cnt = i; i = -1;}
    }
    if (cnt > -1) return btn[cnt].value;
    else return null;
} 
    function findPreviewBillingReport(){
	         
      		var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);
      		var check=formValidate('none');
		  if (btn == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       }
     
	       else if(btn ==1)
	       {
           		  if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
           		    document.forms['reportDataExtractForm'].action ='activeShip.html';
           		    document.forms['reportDataExtractForm'].submit();
           		    return true;
           		 } 
        	}
        	else if(btn ==4)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicFinancialSummary.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
        	else if(btn ==7)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicFinancialDetails.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}  
        	else if(btn ==8)
        	{
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {           		   
        	        document.forms['reportDataExtractForm'].action ='dynamicDomesticAnalysis.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	} 
        	
        	else if(btn ==9){
        	     if(check==false){
           		    return false;
           		 }else{           		   
        	        document.forms['reportDataExtractForm'].action ='storageAnalysis.html';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
        	else{
        	  alert("Under Processing");
        	  document.getElementById("hid").style.visibility="collapse";
        	  return false;
        	}   	
	   	
   }
   
   
   
  
   
    function findPreviewBillingReportNew(){
	        
	     
      		var btn = valButton(document.forms['reportDataExtractForm'].elements['radiobilling']);
      	
      		 
      		
      		var check=formValidate('none');
		  if (btn == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       }
     
	       else if(btn ==1)
	       {
	      
               
	            
           		  if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
           		    
           		     
           		    document.forms['reportDataExtractForm'].action ='activeShip.html?conditionC79=trueeee';
           		    document.forms['reportDataExtractForm'].submit();
           		    return true;
           		 } 
        	}
        	else if(btn ==4)
        	{
        	
        	
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicFinancialSummary.html?conditionC79=trueeee';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
        	else if(btn ==7)
        	{
        	  
        	
        	
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {
        	        document.forms['reportDataExtractForm'].action ='dynamicFinancialDetails.html?conditionC79=trueeee';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}  
        	else if(btn ==8)
        	{
        	  
        	
        	
        	     if(check==false)
           		  {
           		    return false;
           		  }
           		  else
           		  {           		   
        	        document.forms['reportDataExtractForm'].action ='dynamicDomesticAnalysis.html?conditionC79=trueeee';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	} 
        	
        	else if(btn ==9){
        	
        	
        	
        	     if(check==false){
        	      alert("Please enter Query Name");
           		    return false;
           		 }else{           		   
        	        document.forms['reportDataExtractForm'].action ='storageAnalysis.html?conditionC79=trueeee';
           		    document.forms['reportDataExtractForm'].submit();
        	   	    return true; 
        	   	 }
        	}
        	else{
        	  alert("Under Processing");
        	  document.getElementById("hid").style.visibility="collapse";
        	  return false;
        	}   	
	   	
   }
   
    function formValidate(clickType)
    {
    
    animatedcollapse.hide('billing'); 
      <c:if test="${companyDivisionFlag=='Yes'}">  
    	if(document.forms['reportDataExtractForm'].elements['conditionC1'].value != ""       
    	      || document.forms['reportDataExtractForm'].elements['conditionC12'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC13'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC14'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC15'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC16'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC17'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC18'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC19'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC120'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['conditionC3'].value != ""   || document.forms['reportDataExtractForm'].elements['conditionC31'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['jobTypes'].value != ""     
    	    || document.forms['reportDataExtractForm'].elements['billToCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != ""|| document.forms['reportDataExtractForm'].elements['routingTypes'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeType'].value != ""      || document.forms['reportDataExtractForm'].elements['packModeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityType'].value != "" || document.forms['reportDataExtractForm'].elements['coordinatorType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['oACountryType'].value != "" || document.forms['reportDataExtractForm'].elements['salesmanType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryType'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "" || document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['conditionC115'].value != "" || document.forms['reportDataExtractForm'].elements['companyCode'].value != "" )
    	    
    {	    
    	    
    	if(document.forms['reportDataExtractForm'].elements['conditionC1'].value != "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value == ""){
    	
    		alert("Job type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse";
    		//document.getElementById("hid").style.visibility="collapse";
    		//document.getElementById("button1").disabled= true;
          	//document.getElementById("button2").disabled= true;
          	//document.getElementById("button3").disabled= true;
          	return false;
    		
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC1'].value == "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value != "")
    	{
    		    alert("Job type condition is blank, Please select.");
    		    document.getElementById("hid").style.visibility="collapse"
          	    return false;
    		
    	}  
    	else if(document.forms['reportDataExtractForm'].elements['conditionC12'].value != "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value == "")
    	{
    		alert("BillToCode type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC12'].value == "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value != "")
    	{
    		alert("BillToCode type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC13'].value != "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value == "")
    	{
    		alert("BookingCode value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC13'].value == "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != "")
    	{
    		alert("BookingCode condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC14'].value != "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value == "")
    	{
    		alert("Routing type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC14'].value == "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value != "")
    	{
    		alert("Routing types condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value != "" && document.forms['reportDataExtractForm'].elements['modeType'].value == "")
    	{
    		alert("Mode type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value == "" && document.forms['reportDataExtractForm'].elements['modeType'].value != "")
    	{
    		alert("Mode type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['conditionC115'].value != "" && document.forms['reportDataExtractForm'].elements['companyCode'].value == "")
    	{
    		alert("Company Code value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC115'].value == "" && document.forms['reportDataExtractForm'].elements['companyCode'].value != "")
    	{
    		alert("Company Code condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}   
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value != "" && document.forms['reportDataExtractForm'].elements['packModeType'].value == "")
    	{
    		alert("PackMode value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value == "" && document.forms['reportDataExtractForm'].elements['packModeType'].value != "")
    	{
    		alert("PackMode condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value != "" && document.forms['reportDataExtractForm'].elements['commodityType'].value == "")
    	{
    		alert("Commodity  type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value == "" && document.forms['reportDataExtractForm'].elements['commodityType'].value != "")
    	{
    		alert("Commodity type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC18'].value != "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value == "")
    	{
    		alert("Coordinator type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC18'].value == "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value != "")
    	{
    		alert("Coordinator types condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC19'].value != "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value == "")
    	{
    		alert("Salesman type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC19'].value == "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value != "")
    	{
    		alert("Salesman type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value != "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value == "")
    	{
    		alert("OACountry type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value == "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value != "")
    	{
    		alert("OACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value == "")
    	{
    		alert("DACountry type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value == "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value != "")
    	{
    		alert("DACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	/*    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC122'].value != "" && document.forms['reportDataExtractForm'].elements['createdOnType'].value == "")
    	{
    		alert("CreatedOn type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC122'].value == "" && document.forms['reportDataExtractForm'].elements['createdOnType'].value != "")
    	{
    		alert("CreatedOn type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}*/
    	else if(document.forms['reportDataExtractForm'].elements['conditionC3'].value != "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value == "")
    	{
    		alert("ActualWgt type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC3'].value == "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "")
    	{
    		alert("ActualWgt type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC31'].value != "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value == "")
    	{
    		alert("ActualVolume type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC31'].value == "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "")
    	{
    		alert("ActualVolume type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else
    	{
    	    document.getElementById("hid").style.visibility="visible";
    	    
    			//document.getElementById("button1").disabled= false;
          		//document.getElementById("button2").disabled= false;
          		//document.getElementById("button3").disabled= false;
          		if(clickType == "none")
          		{
          			 document.getElementById("hid").style.visibility="collapse";
          			return true;
          			
          		}
          		else
          		{
          			return false;
          		}
    	}
    }	
   	else
   	{
    	    alert("Please select at least one  extract parameter before selecting report.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	
   	}
    
 </c:if>
 <c:if test="${companyDivisionFlag!='Yes'}">
 if(document.forms['reportDataExtractForm'].elements['conditionC1'].value != "" || document.forms['reportDataExtractForm'].elements['conditionC12'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC13'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC14'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC15'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC16'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC17'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC18'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC19'].value != ""  || document.forms['reportDataExtractForm'].elements['conditionC120'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" 
    	    || document.forms['reportDataExtractForm'].elements['conditionC3'].value != ""   || document.forms['reportDataExtractForm'].elements['conditionC31'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['jobTypes'].value != ""      
    	    || document.forms['reportDataExtractForm'].elements['billToCodeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != ""|| document.forms['reportDataExtractForm'].elements['routingTypes'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['modeType'].value != ""      || document.forms['reportDataExtractForm'].elements['packModeType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['commodityType'].value != "" || document.forms['reportDataExtractForm'].elements['coordinatorType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['oACountryType'].value != "" || document.forms['reportDataExtractForm'].elements['salesmanType'].value != ""
    	    || document.forms['reportDataExtractForm'].elements['dACountryType'].value != "" 
            || document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "" || document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "")
    	    
    { 	
    if(document.forms['reportDataExtractForm'].elements['conditionC1'].value != "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value == ""){
    	
    		alert("Job type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse";
    		//document.getElementById("hid").style.visibility="collapse";
    		//document.getElementById("button1").disabled= true;
          	//document.getElementById("button2").disabled= true;
          	//document.getElementById("button3").disabled= true;
          	return false;
    		
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC1'].value == "" && document.forms['reportDataExtractForm'].elements['jobTypes'].value != "")
    	{
    		    alert("Job type condition is blank, Please select.");
    		    document.getElementById("hid").style.visibility="collapse"
          	    return false;
    		
    	}  
    	else if(document.forms['reportDataExtractForm'].elements['conditionC12'].value != "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value == "")
    	{
    		alert("BillToCode type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC12'].value == "" && document.forms['reportDataExtractForm'].elements['billToCodeType'].value != "")
    	{
    		alert("BillToCode type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC13'].value != "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value == "")
    	{
    		alert("BookingCode value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC13'].value == "" && document.forms['reportDataExtractForm'].elements['bookingCodeType'].value != "")
    	{
    		alert("BookingCode condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC14'].value != "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value == "")
    	{
    		alert("Routing type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC14'].value == "" && document.forms['reportDataExtractForm'].elements['routingTypes'].value != "")
    	{
    		alert("Routing types condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value != "" && document.forms['reportDataExtractForm'].elements['modeType'].value == "")
    	{
    		alert("Mode type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC15'].value == "" && document.forms['reportDataExtractForm'].elements['modeType'].value != "")
    	{
    		alert("Mode type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	} 
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value != "" && document.forms['reportDataExtractForm'].elements['packModeType'].value == "")
    	{
    		alert("PackMode value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC16'].value == "" && document.forms['reportDataExtractForm'].elements['packModeType'].value != "")
    	{
    		alert("PackMode condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value != "" && document.forms['reportDataExtractForm'].elements['commodityType'].value == "")
    	{
    		alert("Commodity  type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC17'].value == "" && document.forms['reportDataExtractForm'].elements['commodityType'].value != "")
    	{
    		alert("Commodity type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC18'].value != "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value == "")
    	{
    		alert("Coordinator type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC18'].value == "" && document.forms['reportDataExtractForm'].elements['coordinatorType'].value != "")
    	{
    		alert("Coordinator types condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC19'].value != "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value == "")
    	{
    		alert("Salesman type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC19'].value == "" && document.forms['reportDataExtractForm'].elements['salesmanType'].value != "")
    	{
    		alert("Salesman type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value != "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value == "")
    	{
    		alert("OACountry type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC120'].value == "" && document.forms['reportDataExtractForm'].elements['oACountryType'].value != "")
    	{
    		alert("OACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value != "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value == "")
    	{
    		alert("DACountry type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC121'].value == "" && document.forms['reportDataExtractForm'].elements['dACountryType'].value != "")
    	{
    		alert("DACountry type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	/*    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC122'].value != "" && document.forms['reportDataExtractForm'].elements['createdOnType'].value == "")
    	{
    		alert("CreatedOn type value is Blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC122'].value == "" && document.forms['reportDataExtractForm'].elements['createdOnType'].value != "")
    	{
    		alert("CreatedOn type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}*/
    	else if(document.forms['reportDataExtractForm'].elements['conditionC3'].value != "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value == "")
    	{
    		alert("ActualWgt type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else if(document.forms['reportDataExtractForm'].elements['conditionC3'].value == "" && document.forms['reportDataExtractForm'].elements['actualWgtType'].value != "")
    	{
    		alert("ActualWgt type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC31'].value != "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value == "")
    	{
    		alert("ActualVolume type value is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	
    	else if(document.forms['reportDataExtractForm'].elements['conditionC31'].value == "" && document.forms['reportDataExtractForm'].elements['actualVolumeType'].value != "")
    	{
    		alert("ActualVolume type condition is blank, Please select.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	}
    	else
    	{
    	    document.getElementById("hid").style.visibility="visible";
    	    
    			//document.getElementById("button1").disabled= false;
          		//document.getElementById("button2").disabled= false;
          		//document.getElementById("button3").disabled= false;
          		if(clickType == "none")
          		{
          			 document.getElementById("hid").style.visibility="collapse";
          			return true;
          			
          		}
          		else
          		{
          			return false;
          		}
    	}
    }	
   	else
   	{
    	    alert("Please select at least one  extract parameter before selecting report.");
    		document.getElementById("hid").style.visibility="collapse"
    		return false;
    	
   	} 
  </c:if>
 }
 
 
function buttonDisable()
  {       
  		document.getElementById("hid").style.visibility="collapse";
  		  //document.getElementById("button1").style.visibility="collapse";
         
          //document.getElementById("button2").style.visibility="collapse";
         
          //document.getElementById("button3").style.visibility="collapse";
       
  }
  
  function progress()
{
		setTimeout("alert('hello')",1250);
		var tend = "</tr></table>";
		var tstrt = "<table><tr>";
		scell = scell + "<td style='width:10;height:10' bgcolor=red>";
		document.getElementById("cell1").innerHTML = sbase + tstrt +  scell + tend;     
		 if( i < 50) // total 50 cell will be created with red color.
		 {                         i = i + 1;
			     timerID = setTimeout("progress()",1000); // recursive call
		 }
		 else
		 {
		    if(timerID)
			  {
			    clearTimeout(timerID);
			  }
		}
}

/*function checkFile11()
{
	if( document.forms['reportDataExtractForm'].elements['file'].value!="" )
   		{
  			toggleBox('demodiv',1);
  			event.returnValue = true;
  		} 
  	else 
  	   {
  	     event.returnValue = false;
	   }
    
}*/

 function openPopWindow(){
		
		javascript:openWindow('extractPartnersPopup.html?partnerType=DE&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=billToCodeType');
}
function openBookingAgentPopWindow(){
		
		javascript:openWindow('bookingAgentPopup.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=firstDescription&fld_code=bookingCodeType');
}  
--></script>




<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">

animatedcollapse.addDiv('billing', 'fade=0,persist=1,hide=1,speed=70')



animatedcollapse.init()

</script>
</head>

<s:form id="reportDataExtractForm" name="reportDataExtractForm" action="dataExtracts" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>



<s:hidden id="publicPrivateFlag" name="publicPrivateFlag"/>

<s:hidden id="queryName" name="queryName"/>
<div id="Layer1" style="width:100%">
<s:hidden name="reports.id" value="%{reports.id}"/> 
<div id="newmnav">
		  <ul>
		  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Data Extracts<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		  		<li><a href="findOldQueries.html"><span>Find Old Queries</span></a></li>
		  </ul>
	</div><div class="spn">&nbsp;</div>
	<div style="padding-bottom:3px;"></div>
	<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:-5px;!margin-top:0px;" ><span></span></div>
   <div class="center-content">
<table class="" style="width:868px; margin-bottom:5px" cellspacing="0" cellpadding="0" border="0" >
<tr><td>
<table style="margin-bottom:0px">
<tr><td height="5px"></td></tr>	  
<tr><td width="10px"> </td>
<td  class="bgblue" >Extract Parameters</td></tr>
<tr><td height="2px"></td></tr>
<!--<tr><td width="10px"> </td>
<td style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #888787; padding-left: 40px;">Please select the corresponding conditions and values to view reports.</td></tr>
	--></table></td></tr>
	
	 <tr><td>
	  	   <table border="0" width="100%" cellpadding="0" cellspacing="0" style="margin-bottom:0px;margin: 0px;">
	  		<tr><td height="0px"></td>	  		
	  		</tr>
	  	
	  		<tr>
	  			<td  class="subcontenttabChild" colspan="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			
	  			Condition&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			Value</td><td class="subcontenttabChild" colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Field &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			
	  			Condition&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	  			Value</td>
	  	   </tr>
	  	  </table></td></tr>
	  	  <tr><td style="margin: 0px;">
	  	  <table style="margin-bottom:0px; margin:0px;" cellpadding="1" cellspacing="1" border="0" width="100%">
	  	   
	  	   <tr>
	  	   
	  	   <td width="150px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:110px" id="c1" name="conditionC1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px" colspan="3"><s:select cssClass="list-menu" cssStyle="width:310px;height:50px"  name="jobTypes" list="%{jobtype}"  multiple="true"/></td>
	  	    <td class="listwhitetext" align="left" colspan="3" style="font-size:10px; font-style: italic;">
            <b>* Use Control + mouse to select multiple Job Type</b>
             </td>
	  	   
	  	   </tr>
	  	   <tr><td height="10px" width="100px"></td>
	  	   <td height="10px"></td>
	  	   <td height="10px"></td>
	  	   <td height="10px" width="160px"></td>
	  	   <td height="10px" rowspan="8" class="vertlinedata_vert"></td>	
	  	    <td height="10px"></td>
	  	   <td height="10px"></td>
	  	   </tr>
	  	   
	  		<tr>
	  		<c:if test="${companyDivisionFlag=='Yes'}"> 
	  		<td class="listwhitetext" align="right">Company Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="conditionC115" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="companyCode" list="%{companyCodeList}"  headerKey="" headerValue="" /></td>
	  			
	  		</c:if>	
	  		<td width="150px" class="listwhitetext" align="right" height="">Job Status</td>
			<td  width="5px"></td>
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:110px" id="activeStatus" name="activeStatus" list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
	  		 </tr>
	  		 
	  		 <tr>
	  			<td class="listwhitetext" align="right">Bill To Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c4" name="conditionC12" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td valign="top"><s:textfield cssClass="input-text" name="billToCodeType" size="11" maxlength="6" onchange="findBillToCode()"/>
	  			<img class="openpopup" width="17" align="top" height="20" onclick="openPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		
	  			<td class="listwhitetext" align="right">Booking Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c5" name="conditionC13" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td valign="top"><s:textfield  cssClass="input-text" name="bookingCodeType" size="11" maxlength="6" onchange="findBookingAgentCode()"/>
	  			<img class="openpopup" width="17" height="20" align="top" onclick="openBookingAgentPopWindow();" src="<c:url value='/images/open-popup.gif'/>" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Routing</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c6" name="conditionC14" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="routingTypes" list="%{routing}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">Mode</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="conditionC15" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="modeType" list="%{mode}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Pack Mode</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c8" name="conditionC16" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="packModeType" list="%{pkmode}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">Commodity</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c9" name="conditionC17" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		   
	  			<td ><s:select cssClass="list-menu"  cssStyle="width:110px"  name="commodityType" list="%{commodit}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Coordinator</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c10" name="conditionC18" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="coordinatorType" list="%{coord}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">Salesman</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c11" name="conditionC19" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="salesmanType" list="%{sale}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">OA Country</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c12"  name="conditionC120" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="oACountryType" list="%{ocountry}"  headerKey="" headerValue="" /></td>
	  		
	  			<td class="listwhitetext" align="right">DA Country</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c13"  name="conditionC121" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="dACountryType" list="%{dcountry}"  headerKey="" headerValue="" /></td>
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Actual Wgt</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c15" name="conditionC3" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td><s:textfield cssClass="input-text" id="actualWgtType" name="actualWgtType"  size="16" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td>
	  		
	  			<td class="listwhitetext" align="right">Actual Volume</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  id="c16" name="conditionC31" list="{'Equal to','Not Equal to','Greater than','Less than','Greater than Equal to','Less than Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td><s:textfield cssClass="input-text" id="actualVolumeType" name="actualVolumeType" value="%{actualVolumeType}" size="16" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/>
	  			</td>
	  			
	  		</tr>
	  		 
	  		 <tr><td  height="13" class="listwhitetext" colspan="13" style="text-align:left; margin: 0px;">
	  		 <div class="" onClick="javascript:animatedcollapse.toggle('billing')" style="cursor: pointer;">
	  		 <table cellpadding="0" cellspacing="0" width="100%" border="0"  
style="margin: 0px;" >
<tr>
<td class="headtab_left">
</td>
<td class="headtab_center"  >&nbsp;<a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" border="0" align="absmiddle" /></span>&nbsp;Please enter the date ranges for the Activity Dates</a>
</td>
<td width="28" valign="top" class="headtab_bg"></td>
<td class="headtab_bg_special" >&nbsp;
</td>
<td class="headtab_right"></td>
</tr>
</table>
 </div>
				
				<div class="dspcont" id="billing" >
				
	  		<!--<tr>
	  		
	  			<td width="150px" class="listwhitetext" align="right" height="">Job Type</td>
	  			<td  width="5px"></td>
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:110px" id="c1" name="conditionC1" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			
	  			<td width="130px"><s:select cssClass="list-menu" cssStyle="width:210px;height:50px"  name="jobTypes" list="%{jobtype}"   headerKey="" headerValue="" multiple="true"/></td>
	  		
	  		  
	  			<td class="listwhitetext" align="right">Created On</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c14" name="conditionC122" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  		
	  		<c:if test="${not empty createdOnType}">
						<s:text id="createdOnType" name="${FormDateValue}"> <s:param name="value" value="createdOnType" /></s:text>
						<td><s:textfield cssClass="input-text" id="createdOnType" name="createdOnType" value="%{createdOnType}" size="11" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="calender6" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['reportDataExtractForm'].createdOnType,'calender6',document.forms['reportDataExtractForm'].dateFormat.value); return false;" /></td>
			</c:if>
			<c:if test="${empty createdOnType}" >
						<td><s:textfield cssClass="input-text" id="createdOnType" name="createdOnType" required="true" size="11" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="calender6" align="top" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="cal.select(document.forms['reportDataExtractForm'].createdOnType,'calender6',document.forms['reportDataExtractForm'].dateFormat.value); return false;" /></td>
			</c:if>	
			
			<td width="150px" class="listwhitetext" align="right" height="">Job Status</td>
			<td  width="5px"></td>
			<td align="left"><s:select cssClass="list-menu" cssStyle="width:110px" id="activeStatus" name="activeStatus" list="{'Active','Inactive','All','All (Excl. Cancel)'}"/></td>
			<%-- <td align="left"><s:checkbox name="activeStatus" fieldValue="true" value="true" onclick=""onchange="" /></td>--%>
	  		<td></td><td width="150"></td><td colspan="2"></td>
	  		</tr>-->
	  		<table class="detailTabLabel" cellspacing="1" cellpadding="1" border="0" width="100%" align="left">
			<tbody>
			
			<tr>
			<td width="126"></td>
			<td></td>
			<td class="listwhitetext" align="left" width="100"><b>From</b></td>
			
			<td class="listwhitetext" align="left" width="100"><b>To</b></td>
			<c:if test="${accountInterface=='Y'}"> 
			<td height="10px" rowspan="8" width="2px" class="vertlinedata_vert"></td>	
			</c:if>
			<c:if test="${accountInterface!='Y'}">
			<td height="10px" rowspan="8" width="2px" class="vertlinedata_vert"></td>	 
			</c:if>
			<td width="132"></td>
			
			<td></td>
			<td class="listwhitetext" align="left"><b>From</b></td>
			
			<td class="listwhitetext" align="left" width="104"><b>To</b></td>
			</tr>
	  		<tr>
	  			
	  			<td class="listwhitetext" align="right" >Load Target Date</td>
	  			<td  width="0px"></td> 
	  			<s:hidden   id="conditionC200" name="conditionC200" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty loadTgtBeginDate}">
						<s:text id="loadTgtBeginDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtBeginDate" /></s:text>
						<td width="90px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" value="%{loadTgtBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty loadTgtBeginDate}" >
						<td width="90px"><s:textfield cssClass="input-text" id="loadTgtBeginDate" name="loadTgtBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden id="conditionC201" name="conditionC201" value="Less than Equal to" /> 
	  		  <c:if test="${not empty loadTgtEndDate}">
						<s:text id="loadTgtEndDate" name="${FormDateValue}"> <s:param name="value" value="loadTgtEndDate" /></s:text>
						<td width="150px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" value="%{loadTgtEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty loadTgtEndDate}" >
						<td width="150px"><s:textfield cssClass="input-text" id="loadTgtEndDate" name="loadTgtEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="loadTgtEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	 
	  			<td class="listwhitetext" align="right" >Load Actual Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="c2" name="conditionC21" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"> <s:param name="value" value="beginDate" /></s:text>
						<td width="90px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty beginDate}" >
						<td width="90px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden id="c3" name="conditionC22" value="Less than Equal to"  /> 
	  		<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td width="96px"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty endDate}" >
			  <td width="96px"><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<tr>
	  			<td class="listwhitetext" align="right">Delivery Target Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="conditionC211" name="conditionC211" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty deliveryTBeginDate}">
						<s:text id="deliveryTBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" value="%{deliveryTBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryTBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTBeginDate" name="deliveryTBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden id="conditionC222" name="conditionC222" value ="Less than Equal to"  /> 
	  		<c:if test="${not empty deliveryTEndDate}">
						<s:text id="deliveryTEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryTEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" value="%{deliveryTEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryTEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryTEndDate" name="deliveryTEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryTEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	 
	  			<td class="listwhitetext" align="right">Delivery Actual Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="conditionC233" name="conditionC233" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty deliveryActBeginDate}">
						<s:text id="deliveryActBeginDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" value="%{deliveryActBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty deliveryActBeginDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActBeginDate" name="deliveryActBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="conditionC244" name="conditionC244" value="Less than Equal to" /> 
	  		<c:if test="${not empty deliveryActEndDate}">
						<s:text id="deliveryActEndDate" name="${FormDateValue}"> <s:param name="value" value="deliveryActEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" value="%{deliveryActEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty deliveryActEndDate}" >
						<td><s:textfield cssClass="input-text" id="deliveryActEndDate" name="deliveryActEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="deliveryActEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		</tr>
	  		<c:if test="${accountInterface=='Y'}">
	  		<tr>
	  			<td class="listwhitetext" align="right">Invoice Posting Date</td>
	  			<td  width="5px"></td>
	  			
	  			<s:hidden  id="conditionC255" name="conditionC255" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty invPostingBeginDate}">
						<s:text id="invPostingBeginDate" name="${FormDateValue}"> <s:param name="value" value="invPostingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invPostingBeginDate" name="invPostingBeginDate" value="%{invPostingBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invPostingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="invPostingBeginDate" name="invPostingBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="conditionC266" name="conditionC266" value="Less than Equal to" /> 
	  		<c:if test="${not empty invPostingEndDate}">
						<s:text id="invPostingEndDate" name="${FormDateValue}"> <s:param name="value" value="invPostingEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invPostingEndDate" name="invPostingEndDate" value="%{invPostingEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty invPostingEndDate}" >
						<td><s:textfield cssClass="input-text" id="invPostingEndDate" name="invPostingEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if> 
	  		</c:if>
	  		<c:if test="${accountInterface!='Y'}">
	  		<s:hidden id="conditionC255" name="conditionC255" />
	  		 <s:hidden id="invPostingBeginDate" name="invPostingBeginDate" /> 
	  		 <s:hidden id="conditionC266" name="conditionC266" />
	  		 <s:hidden id="invPostingEndDate" name="invPostingEndDate" /> 
	  		</c:if>
	  		<c:if test="${accountInterface=='Y'}"> 
	  			<td class="listwhitetext" align="right">Payable Posting Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC300" name="conditionC300" value="Greater than Equal to" /> 
	  			<c:if test="${not empty payPostingBeginDate}">
						<s:text id="payPostingBeginDate" name="${FormDateValue}"> <s:param name="value" value="payPostingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="payPostingBeginDate" name="payPostingBeginDate" value="%{payPostingBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="payPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty payPostingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="payPostingBeginDate" name="payPostingBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="payPostingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if> 
	  			<s:hidden  id="conditionC301" name="conditionC301" value="Less than Equal to"   /> 
	  		<c:if test="${not empty payPostingEndDate}">
						<s:text id="payPostingEndDate" name="${FormDateValue}"> <s:param name="value" value="payPostingEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="payPostingEndDate" name="payPostingEndDate" value="%{payPostingEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="payPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty payPostingEndDate}" >
						<td><s:textfield cssClass="input-text" id="payPostingEndDate" name="payPostingEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="payPostingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		 </tr>
	  		</c:if>
	  		<c:if test="${accountInterface!='Y'}">
	  		<s:hidden id="conditionC300" name="conditionC300" />
	  		 <s:hidden id="payPostingBeginDate" name="payPostingBeginDate" /> 
	  		 <s:hidden id="conditionC301" name="conditionC301" />
	  		 <s:hidden id="payPostingEndDate" name="payPostingEndDate" /> 
	  		</c:if> 
	  			<td class="listwhitetext" align="right">Revenue Recognition</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC333" name="conditionC333" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty revenueRecognitionBeginDate}">
						<s:text id="revenueRecognitionBeginDate" name="${FormDateValue}"> <s:param name="value" value="revenueRecognitionBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="revenueRecognitionBeginDate" name="revenueRecognitionBeginDate" value="%{revenueRecognitionBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="revenueRecognitionBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty revenueRecognitionBeginDate}" >
						<td><s:textfield cssClass="input-text" id="revenueRecognitionBeginDate" name="revenueRecognitionBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="revenueRecognitionBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if>  
	  			<s:hidden id="conditionC344" name="conditionC344" value="Less than Equal to"  /> 
		  		<c:if test="${not empty revenueRecognitionEndDate}">
							<s:text id="revenueRecognitionEndDate" name="${FormDateValue}"> <s:param name="value" value="revenueRecognitionEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="revenueRecognitionEndDate" name="revenueRecognitionEndDate" value="%{revenueRecognitionEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="revenueRecognitionEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty revenueRecognitionEndDate}" >
						<td><s:textfield cssClass="input-text" id="revenueRecognitionEndDate" name="revenueRecognitionEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="revenueRecognitionEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	 
	  			<td class="listwhitetext" align="right">Invoicing date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC277" name="conditionC277" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty invoicingBeginDate}">
						<s:text id="invoicingBeginDate" name="${FormDateValue}"> <s:param name="value" value="invoicingBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="invoicingBeginDate" name="invoicingBeginDate" value="%{invoicingBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoicingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoicingBeginDate}" >
						<td><s:textfield cssClass="input-text" id="invoicingBeginDate" name="invoicingBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoicingBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if> 
	  			<s:hidden  id="conditionC288" name="conditionC288" value="Less than Equal to"   />
	  	
		  		<c:if test="${not empty invoicingEndDate}">
							<s:text id="invoicingEndDate" name="${FormDateValue}"> <s:param name="value" value="invoicingEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="invoicingEndDate" name="invoicingEndDate" value="%{invoicingEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoicingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty invoicingEndDate}" >
						<td><s:textfield cssClass="input-text" id="invoicingEndDate" name="invoicingEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="invoicingEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>	
	  		</tr> 
	  		<tr>
	  			<td class="listwhitetext" align="right">Storage Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC311" name="conditionC311" value="Greater than Equal to"  /> 
	  			<c:if test="${not empty storageBeginDate}">
						<s:text id="storageBeginDate" name="${FormDateValue}"> <s:param name="value" value="storageBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="storageBeginDate" name="storageBeginDate" value="%{storageBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="storageBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty storageBeginDate}" >
						<td><s:textfield cssClass="input-text" id="storageBeginDate" name="storageBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="storageBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   	</c:if> 
	  			<s:hidden id="conditionC322" name="conditionC322" value="Less than Equal to"   /> 
		  		<c:if test="${not empty storageEndDate}">
							<s:text id="storageEndDate" name="${FormDateValue}"> <s:param name="value" value="storageEndDate" /></s:text>
							<td><s:textfield cssClass="input-text" id="storageEndDate" name="storageEndDate" value="%{storageEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="storageEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty storageEndDate}" >
						<td><s:textfield cssClass="input-text" id="storageEndDate" name="storageEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="storageEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if> 
	  		<td class="listwhitetext" align="right">Create Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC77" name="conditionC77" value="Greater than Equal to"   />
	  			<c:if test="${not empty createDate}">
	  			<s:text id="createDate" name="${FormDateValue}"> <s:param name="value" value="createDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="createDate" name="createDate" value="%{createDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="createDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if>
	  		    <c:if test="${empty createDate}" >
	  		    <td><s:textfield cssClass="input-text" id="createDate" name="createDate" value="%{createDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="createDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		    </c:if> 
	  			<s:hidden id="conditionC78" name="conditionC78" value="Less than Equal to"  />
	  			<c:if test="${not empty toCreateDate}">
	  			<s:text id="toCreateDate" name="${FormDateValue}"> <s:param name="value" value="toCreateDate" /></s:text>
	  			<td><s:textfield cssClass="input-text" id="toCreateDate" name="toCreateDate" value="%{toCreateDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="toCreateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>
	  		   <c:if test="${empty toCreateDate}">
	  		   <td><s:textfield cssClass="input-text" id="toCreateDate" name="toCreateDate" value="%{toCreateDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="toCreateDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	  		   </c:if>	  		   
	  		</tr>
	  		<tr>
	  		 <c:if test="${accountInterface=='Y'}"> 
	  			<td class="listwhitetext" align="right">Posting Period Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden  id="conditionC304" name="conditionC304" value="Greater than Equal to "/> 
	  			<c:if test="${not empty postingPeriodBeginDate}">
						<s:text id="postingPeriodBeginDate" name="${FormDateValue}"> <s:param name="value" value="postingPeriodBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="postingPeriodBeginDate" name="postingPeriodBeginDate" value="%{postingPeriodBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postingPeriodBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty postingPeriodBeginDate}" >
						<td><s:textfield cssClass="input-text" id="postingPeriodBeginDate" name="postingPeriodBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postingPeriodBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden  id="conditionC305" name="conditionC305" value="Less than Equal to"  /> 
	  		<c:if test="${not empty postingPeriodEndDate}">
						<s:text id="postingPeriodEndDate" name="${FormDateValue}"> <s:param name="value" value="postingPeriodEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="postingPeriodEndDate" name="postingPeriodEndDate" value="%{postingPeriodEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postingPeriodEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty payPostingEndDate}" >
						<td><s:textfield cssClass="input-text" id="postingPeriodEndDate" name="postingPeriodEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="postingPeriodEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	 
	  		 </c:if>
	  		 <c:if test="${accountInterface!='Y'}">
	  		 <s:hidden id="conditionC304" name="conditionC304" />
	  		 <s:hidden id="postingPeriodBeginDate" name="postingPeriodBeginDate" /> 
	  		 <s:hidden id="conditionC305" name="conditionC305" />
	  		 <s:hidden id="postingPeriodEndDate" name="postingPeriodEndDate" /> 
	  		 </c:if> 
	  			<td class="listwhitetext" align="right">Received Date</td>
	  			<td  width="5px"></td> 
	  			<s:hidden   id="conditionC302" name="conditionC302" value="Greater than Equal to"   /> 
	  			<c:if test="${not empty receivedBeginDate}">
						<s:text id="receivedBeginDate" name="${FormDateValue}"> <s:param name="value" value="receivedBeginDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="receivedBeginDate" name="receivedBeginDate" value="%{receivedBeginDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="receivedBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
				</c:if>
				<c:if test="${empty receivedBeginDate}" >
						<td><s:textfield cssClass="input-text" id="receivedBeginDate" name="receivedBeginDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="receivedBeginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			   </c:if>  
	  			<s:hidden id="conditionC303" name="conditionC303" value="Less than Equal to"   /> 
	  		<c:if test="${not empty receivedEndDate}">
						<s:text id="receivedEndDate" name="${FormDateValue}"> <s:param name="value" value="receivedEndDate" /></s:text>
						<td><s:textfield cssClass="input-text" id="receivedEndDate" name="receivedEndDate" value="%{receivedEndDate}" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="receivedEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty receivedEndDate}" >
						<td><s:textfield cssClass="input-text" id="receivedEndDate" name="receivedEndDate" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/> <img id="receivedEndDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>	
	  		 </tr> 
	  		<tr>
	  		</tbody>
	  		</table>
	  		</div>	  		
	  		</td>
	  		</tr>
	  		<!--  
	  		  <c:if test="${companyDivisionFlag=='Yes'}"> 
	  		<tr>
	  		<td class="listwhitetext" align="right">Company Code</td>
	  			<td  width="5px"></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px" id="c7" name="conditionC115" list="{'Equal to','Not Equal to'}"  headerKey="" headerValue="" /></td>
	  			<td ><s:select cssClass="list-menu" cssStyle="width:110px"  name="companyCode" list="%{companyCodeList}"  headerKey="" headerValue="" /></td>
	  			
	  			
	  		 </tr>		 
	  		
	  		</c:if> -->
	  		 <c:if test="${companyDivisionFlag!='Yes'}">
	  		  <s:hidden id="conditionC115" name="conditionC115" />
	  		 <s:hidden id="companyCode" name="companyCode" /> 
	  		 </c:if>
	  		 <tr>
	  			<td height="5px" colspan="4"></td>
	  		</tr>
	  		
	  		<tr>
   	           <td colspan="15" align="left" class="vertlinedata"></td>
   	        </tr>
   	        
   	        <tr><td height="5px"></td></tr>
   	        <tr>
   	       <td class="listwhitetext" align="right" colspan="3">            
             </td>
   	          <td align="center" colspan="3"><s:submit cssClass="cssbutton" cssStyle="width:94px; height:27px" align="absmiddle"  value="View Reports" onclick = "return formValidate();"/>
   	          <s:reset cssClass="cssbutton" cssStyle="width:55px; height:27px" key="Reset" />
   	                
   	       
   	         </td>
   	          
   	          
   	          
   	          <s:hidden name="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:set var="submitType" value="<%=request.getParameter("submitType") %>"/>
						<c:choose>
							<c:when test="${submitType == 'job'}">
							<td align="center" colspan="7"><s:reset type="button" cssClass="cssbutton" align="top" cssStyle="width:100px; height:25px" key="Reset" /></td>
							</c:when>
							
	 					</c:choose>
   	         </tr>
			<tr>
	  			<td height="1px" colspan="4"></td>
	  		</tr>
	  		</table></td></tr>
     </table> 
      </div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
     <!-- Arvind  -->
     <div id="hid">
    <table class="tablegrey" style="width:100%; margin-bottom:0px" cellspacing="0" cellpadding="0" border="0">
    <tr><td height="10px"> </td></tr>
    <tr><td width="15px"> </td>
<td  class="bgblue" >Select Reports</td></tr>
	<tr><td width="10px"></td><td><table style="margin-bottom:0px; width:45%;">
	  		
	  			  			  		
	  	   <tr>
	  	   <td height="25px" width="200px" class="listblackText" align="left" style="padding-left:10px; !width:180px;">
	  	   <label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i1" value="1"/><b>Shipment Analysis</b></label></td>
	  	   <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"><a href="#"><img src="<c:url value='/images/info.gif'/>" width="23" height="23" border="0" /></a></td>
   	       <td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract&nbsp;Info</td>
   	      </tr>
   	      
   	       <tr>
	  	   <td height="25px" width="110px" class="listblackText" align="left" style="padding-left:10px; !width:180px;">
	  	   <label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i8" value="8"/><b>Domestic Analysis</b></label></td>
	  	   <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"><a href="#"><img src="<c:url value='/images/info.gif'/>" width="23" height="23" border="0" /></a></td>
   	       <td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract&nbsp;Info</td>
   	      </tr>
   	      
	  	   <tr><td height="25px" width="110px" class="listblackText" style="padding-left:10px;!width:180px;"><label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i4" value="4"/><b>Dynamic&nbsp;Financial&nbsp;Summary</b></label></td>
    	   <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px; ">
    	   <a href="#">
			<img border="0" onclick="window.open('resources/extracts/DynamicFinancialSummary_info.pdf?decorator=popup&popup=true','DynamicFinancialSummary_info','height=800,width=750,top=0, left=610, scrollbars=no,resizable=yes');" src="<c:url value='/images/info.gif'/>"/>
			</a>
    	   
    	   </td>
    	   <td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract&nbsp;Info</td>
	  	   </tr>
	  	   <tr><td height="25px" width="110px" class="listblackText" style="padding-left:10px;!width:180px;"><label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i7" value="7"/><b>Dynamic&nbsp;Financial&nbsp;Details</b></label></td>
 	   		<td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"><a href="#"><img src="<c:url value='/images/info.gif'/>" width="23" height="23" border="0" /></a></td>
 	   		<td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract&nbsp;Info</td>
	  	   </tr>
	  	   
	  	    <tr>
		  	    <td height="25px" width="110px" class="listblackText" style="padding-left:10px;!width:180px;"><label for="r1"><input style="vertical-align:bottom;" type="radio" name="radiobilling" id="i9" value="9"/><b>Storage&nbsp;Billing&nbsp;Analysis</b></label></td>
	 	   		<td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"><a href="#"><img src="<c:url value='/images/info.gif'/>" width="23" height="23" border="0" /></a></td>
	 	   		<td height="25px" class="listblackText" align="left" style="padding-left:10px;">Extract&nbsp;Info</td>
	  	   	</tr>
	  
	  	   	<tr>
   	         <td align="left" style="padding-left:18px;padding-top:10px;">
   	           <input type="button" class="cssbutton" style="width:110px; height:28px" align="top"  value="Show Reports" onclick="return findPreviewBillingReport();"/>
   	         </td> 	 
   	         
   	         
   	         <td align="left" style="padding-left:18px;padding-top:10px;">
   	           <input type="button" class="cssbutton" style="width:110px; height:28px" align="top"  value="Save Queries" onclick="createTextAreaSave1();"/>
   	         </td>
   	         
   	         
   	         
   	                   
   	         
   	        
   	         
   	          <td height="25px" width="110px" class="listblackText" align="right" style="padding-left:10px;"></td>
   	          </tr>
   	          <tr>
   	          <td height="1px"></td>
   	          	  	   
	  	   </tr>
	  		</table>
	  		</td>
	  		</tr>
	  		</table>
	  		
	  	</div>	
	  	
	  		<!-- Arvind
	  		<s:submit cssClass="cssbutton" cssStyle="width:110px; height:32px" align="absmiddle"  value="Show Reports" onclick = "return formValidate();"/> 
   	     <div id="hid">
   	     	<table class="mainDetailTable" style="width:100%" cellspacing="0" cellpadding="0" border="0" >
   	     	<tr><td height="10px"></td></tr>
   	       	<tr>
   	       		<td align="center" colspan="3"><s:submit id="button3" cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="activeShip" value="Active Shipments" name="activeShipments"  onclick = "return formValidate('none');"/></td>   
			    <td align="center" colspan="4"><s:submit id="button1" cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="dynamicFinancialSummary" value="Dynamic  Financial Summary" name="dynamicFinancialSummary" onclick = "return formValidate('none');"/></td>
	    		<%--<td align="center" colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="reciprocityReport" value="Reciprocity Reports" onclick="return fieldValidate();"/></td>--%>
	    		<td align="center" colspan="3"><s:submit id="button2" cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="dynamicFinancialDetails" value="Dynamic  Financial Details" name="dynamicFinancialDetails" onclick = "return formValidate('none');"/></td>
	    <%--	
	    	</tr>
	    	<tr>
	  			<td height="10px" colspan="4"></td>
	  		</tr>
			<tr>	
	  			
	    		 <td align="center" colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="" value="Active Shipments - Dom" onclick=" alert('Under Implementation.'); return false;;"/></td> --%>
	    	
	  			
	  			
	  			
	    		<%--<td align="center" colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="" value="Active Shipments-GST" onclick=" alert('Under Implementation.'); return false;"/></td>--%>
	    	</tr>
	
	  
	    	<tr>
	  			<td height="10px" colspan="4"></td>
	  		</tr>
	    	<tr>	
	  			<%--<td align="center" colspan="2"><s:submit cssClass="cssbutton" cssStyle="width:200px; height:25px" align="top" method="" value="Active Shipments-STO" onclick=" alert('Under Implementation.'); return false;"/></td>--%>
	    	</tr>
	    		 				
	</table>
 	</div>
 -->
</div>
<s:hidden name="firstDescription" />
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />




</s:form>
 
<script type="text/javascript"> 
   try{
     buttonDisable();
     }
     catch(e){}
    
    Form.focusFirstElement($("reportForm")); 
    
</script> 

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>