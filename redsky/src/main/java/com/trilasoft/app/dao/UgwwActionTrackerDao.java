package com.trilasoft.app.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.IntegrationLogInfoDTO;
import com.trilasoft.app.model.UgwwActionTracker;

public interface UgwwActionTrackerDao  extends GenericDao<UgwwActionTracker, Long>{
	
	  public List getAllugwwactiontrackeList();
	  
	  public List findId(String sequenceNumber,String corpId);
	  public List findsoId(String sequenceNumber,String corpId);
	  public List findRateRequestId(String sequenceNumber,String corpId);
	  public List findAccountPolicyUrlId(String sequenceNumber,String corpId);
	  public List findFormPageUrlId(String sequenceNumber,String corpId);
	  public List findsodId(String shipNumber,String corpId);
	  public List findAccountLineId(String shipNumber,String corpId,Boolean accessQFromCF);
	  public List findFormPageId(String shipNumber,String corpId);

	public List findServiceAuditPageId(String shipNumber,String corpId);
	public List findCustID(String subStr,String corpId);

	public List getTrackerList(String soNumber, String action, String userName,
			Date actionTime, String corpID);

	public List findQuotesID(String shipNumber, String corpId);

	public List findServiceQuotesID(String shipNumber, String corpId);

	public List findPartnerID(String searchstr, String sessionCorpID);

	public List getPartner(String sessionCorpID, String partnerCode);

	public List findTicketDetailsId(String searchstr, String sessionCorpID);

	public List findClaimDetailsId(String searchstr, String sessionCorpID);

	public List findQuotesList(String searchstr, String sessionCorpID);

	public List findQuotesListId(String searchstr, String sessionCorpID);

	public List findOrderId(String searchstr, String sessionCorpID);

	public List<IntegrationLogInfoDTO> getErrorLogList(String integrationId,
			String operation, String orderComplete, Date statusCode,
			String effectiveDate,String so,String corpID);
	
	public List getWeeklyIntegrationCenterXML(String corpId);
	
	public List getMssLogList(String corpId);
	
	public List getCentreVanlineList(String corpId);
	public List findDriverDashboardList(Date beginDate,Date endDate,String driverId,String driverAgency,String corpId);
	public List findDriverDBChildList(Date beginDate,Date endDate,String driverId,String corpId);
	public List findDriverDBSoDetails(Date beginDate,Date endDate,String driverId,String corpId);
	 public List findDriverDetails(String driverId,String corpId);
	public List findSoDashboardList(String corpId);
	public List soDashboardSearchList(String shipNumber,String registrationNumber,String job1,String coordinator,String status,String corpId,String role,String lastName,String firstName,String cmpdiv,String billtoName,String originCity,String destinCity,Boolean actstatus);
	 public Map<String,String> getFieldsNameToDisplay();
	 public List getMoveForYouList(String corpId);
	 public List getVoxmeEstimatList(String corpID);
	 public List getPricePointListForIntegration(String corpId);
	 public List UGWWListForIntegration(String corpId);
	 public List<IntegrationLogInfoDTO> getLisaLogList(String integrationId,String operation, String orderComplete, Date effectiveDate,String statusCode,String so,String corpID);
	 public List findsoFullId(String shipNumber,String corpId);
}
