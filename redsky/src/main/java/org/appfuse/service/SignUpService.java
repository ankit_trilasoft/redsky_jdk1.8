package org.appfuse.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.appfuse.model.Role;
import org.appfuse.model.User;

import com.opensymphony.xwork2.ActionContext;
import com.trilasoft.app.model.AccessInfo;
import com.trilasoft.app.model.CustomerFile;

public interface SignUpService {
	public List customerEmail(String email,String fName,String lName,String target);
	public void resetPassword(String passwordNew,String confirmPasswordNew,String name);
	public List customerHintQues(String username);
	public List customerCorpID(String username);
	public List supportEmail(String corpId);
	public List customerForgotEmail(String username);
	public List maxAttempt(String username);
	public void resetAccount(String name);
	public String saveUser(User user);
	public Map<String, String> findCountry(String parameter);
	public List getAgentPopupList(String partnerType, String sessionCorpID, String userCountry, String string, String string2, String string3, String string4);
	public List checkUser(String userEmail, String sessionCorpID);
	public List getJobFunction(String parameter);
	public List getPartner(String partnerCode, String sessionCorpID);
	public Role getRole(String rolename);
	public List findPortalId(String oldPortalId);
	public boolean findUser(String portalId);
	public List findCustomerPortalListDetails(String oldPortalId);
	public Map<String, String> findState(String country, String sessionCorpID);
	public List searchAgents(String partnerCode, String partnerLastName, String partnerTerminalCountryCode, String partnerTerminalCountry, String partnerTerminalState, String sessionCorpID);
	public CustomerFile getCustomer(Long id);
	public AccessInfo getAccessinfo(Long id);
	public String serviceStateList(String sessionCorpID);
	public List findStateService(String country, String sessionCorpID);
	public List findUserDetails(String userName);
	public List updateSoCustomerFeedback(String soId,String rtNumber,String customerFeedback,String services,String surveyJob,String sessionCorpID);
	public List getUserEmail(String rtNumber,String sessionCorpID);
	public List getCrewName(String shipNumber,String sessionCorpID);
	public List getUserDetails(String userName,String sessionCorpID);
	public List getSurveyEmail(String sessionCorpID);
	public List checkCustomerFeedback(String soId,String sessionCorpID);
	public List getReloDetailsList(String sessionCorpID);
	public List checkRLOCustomerFeedback(String soId,String sessionCorpID);
	public String getDspDetalsData(String services,String dspDetailsValue,String soId,String sessionCorpID);
	public void getNPSCalculation(String services,String dspDetailsValue,String soId,String sessionCorpID);
	public String getVenderDspDetails(String services,String soId,String shipNumber,String sessionCorpID);
	public List getPostMoveOrderDetails(String id,String sessionCorpID);
	public void updateEmailReveived(String id,String contactPhone,String contactEmail,String sessionCorpID,String response);
	
}
