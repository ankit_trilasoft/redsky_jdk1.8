<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>Quotation Quotes List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
	<display:table name="customerSO" class="table" requestURI="" id="serviceOrderList" export="false" defaultsort="1" >
	<display:column title="Ship" ><a href="#" onclick="goToUrl(${serviceOrderList.id});" ><c:out value="${serviceOrderList.ship}" /></a></display:column>   
		<display:column property="status" titleKey="serviceOrder.status" style="width:10px" />
		<display:column property="routing" title="Route" style="width:10px"/>  
		<display:column property="commodity"  title="Comm." style="width:18px"/>  
		<display:column property="mode"  titleKey="serviceOrder.mode" style="width:18px"/>   
	</display:table>
</div>
