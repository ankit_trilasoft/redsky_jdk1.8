package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.CommissionLocking;

public interface CommissionLockingManager extends GenericManager<CommissionLocking, Long> {
	public List getCommissionLineId(String cid,String corpId,String commisionGiven,Long aid); 
	public String getCommisionLineId(String corpId,Long aid,String commisionGiven);
	public String getSentToCommisionLineId(String corpId,String aidList,String commisionGiven,Long aid);
}
