<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title><fmt:message key="subcontrCompanyCodeForm.title" /></title>
<meta name="heading" content="<fmt:message key='subcontrCompanyCodeForm.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:488px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}

</style>
<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">
function checkCompanyCode()
{ 
 var companyCode = document.forms['subcontrCompanyCodeForm'].elements['subcontrCompanyCode'].value; 
 companyCode=companyCode.trim();
// alert(companyCode);
 if(companyCode=='')
 {
  document.forms['subcontrCompanyCodeForm'].elements['Extract'].disabled=true;
 }
else
{
document.forms['subcontrCompanyCodeForm'].elements['Extract'].disabled=false;
}
}

</script>
</head>
<s:form id="subcontrCompanyCodeForm" name="subcontrCompanyCodeForm" action="subcontractorExtractHoll" method="post">
<div id="Layer1" style="width:85%;">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Subcontractor Pay Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
<table  border="0" class="" style="width:80%;" cellspacing="0" cellpadding="2">
	
	<tr><td height="5"></td></tr>
		<tr>
	    <td width="5px"></td>
		<td align="left"  class="listwhitebox" width="200px">Please enter Company Group Code</td> 
		<td align="left" width="500px"><s:select list="%{subcontrCompanyCodeList}" cssClass="list-menu" cssStyle="width:100px" headerKey=" " headerValue=" " id="subcontrCompanyCode"  name="subcontrCompanyCode"  onchange="checkCompanyCode();"/></td>
		</tr> 
		<tr><td height="10"></td></tr>
		<tr>
		<td width="5px"></td>
		<td></td> 
        <td align="left"><s:submit cssClass="cssbutton1" type="button" value="Extract" name="Extract"  cssStyle="width:70px;"  />  
        </td>
		</tr> 
		<tr><td height="15"></td></tr>
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>
</s:form>
<script type="text/javascript"> 
try{
checkCompanyCode(); 
}
catch(e){}
</script>