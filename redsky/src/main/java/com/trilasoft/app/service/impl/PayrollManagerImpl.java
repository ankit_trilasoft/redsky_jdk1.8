//---Created By Bibhash

package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PayrollDao;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.GenralDTO;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.model.Payroll;

public class PayrollManagerImpl extends GenericManagerImpl<Payroll, Long> implements PayrollManager{

	PayrollDao payrollDao; 
    public PayrollManagerImpl(PayrollDao payrollDao) { 
        super(payrollDao); 
        this.payrollDao = payrollDao; 
    } 
	public List findById(Long id) {
		return payrollDao.findById(id);
	} 
	
    public List findMaximumId(){
	    	return payrollDao.findMaximumId(); 
	} 
	public List<Payroll> searchPayroll(String firstName, String lastName, String warehouse, String active, String userName,String union,String sessionCorpID) {
			return payrollDao.searchPayroll(firstName, lastName, warehouse, active, userName,union,sessionCorpID);
	} 
	public List checkUserName(String userName) {
			return payrollDao.checkUserName(userName);
	} 

	public Map<String, String> getCompnayDevisionCode(String corpId) {
			return payrollDao.getCompnayDevisionCode(corpId);
	}
	public List<GenralDTO> findPayrollExtractList(String beginDates, String endDates,String corpId, String hub) {
		return payrollDao.findPayrollExtractList(beginDates, endDates,corpId, hub);
	}
	public List findPayrollInternalList(String beginDates, String endDates, String corpId) {
		
		return payrollDao.findPayrollInternalList(beginDates, endDates, corpId);
	} 
	public List findsStorageExtracts(String warehouseS, String typeS, String corpID){
		return payrollDao.findsStorageExtracts(warehouseS, typeS, corpID);
	}
	public List findsClaimsExtracts(String query,String corpId, String recievedFormCheckFlag){
		return payrollDao.findsClaimsExtracts(query, corpId,recievedFormCheckFlag);
	}
	public List<Payroll> getActiveList() {
		return payrollDao.getActiveList();
	}
	public int updateSickPersonal(String fromDate, String toDate, String sessionCorpID) {
		return payrollDao.updateSickPersonal(fromDate, toDate, sessionCorpID)	;
	}
	public List getSickHours(String userName, String sessionCorpID) {
		return payrollDao.getSickHours(userName, sessionCorpID);
	}
	public List getPersonalHours(String userName, String sessionCorpID) {
		return payrollDao.getPersonalHours(userName, sessionCorpID);
	}
	
	public List getHealthWelfareExtract(String beginDate, String endDate, String sessionCorpID){
		return payrollDao.getHealthWelfareExtract(beginDate, endDate, sessionCorpID);
	}
	public int updateVacation(String fromDate, String toDate, String sessionCorpID) {
		return payrollDao.updateVacation(fromDate, toDate, sessionCorpID);
	}
	public List getVacationHours(String userName, String sessionCorpID) {
		return payrollDao.getVacationHours(userName, sessionCorpID);
	}
	public List findStorageLibraryExtract(String locWarehouse, String typeSto, String sessionCorpID){
		return payrollDao.findStorageLibraryExtract(locWarehouse, typeSto, sessionCorpID);
	}
	public List findPaychexExtractList(String beginDates, String endDates, String corpId, String hub) {
		return payrollDao.findPaychexExtractList(beginDates, endDates, corpId, hub) ;
	}
	public List getPkCodeList(String vlCode, String sessionCorpID){
		return payrollDao.getPkCodeList(vlCode, sessionCorpID);
	}
	public List findsComplaintsExtracts(String query,String corpId){
		return payrollDao.findsComplaintsExtracts(query,corpId);
	}
	public void updateSickPersonalDay(){
		payrollDao.updateSickPersonalDay();	
	}
	public List getAllCrewName(String sessionCorpID){
		return payrollDao.getAllCrewName(sessionCorpID);
	}
	public List<GenralDTO> findPayrollWorkDayExtractList(String beginDates,String endDates,String corpId, String hub){
		return payrollDao.findPayrollWorkDayExtractList(beginDates, endDates, corpId, hub);
	}
	public Map<String, String> getOpenCompnayDevisionCode(String corpId) {
		return payrollDao.getOpenCompnayDevisionCode(corpId);
	}
}
