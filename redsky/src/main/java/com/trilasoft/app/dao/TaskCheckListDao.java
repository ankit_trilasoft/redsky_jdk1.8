package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.TaskCheckList;

public interface TaskCheckListDao extends GenericDao<TaskCheckList, Long>{
	public List getById();
	public TaskCheckList getTaskCheckListData(String shipnumber, Long id);
	
}
