<%@ include file="/common/taglibs.jsp"%> 
<head> 
<title>Role List</title> 
<meta name="heading" content="User Role List"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}
</style>

</head>
<c:set var="buttons">  

     <input type="button" class="cssbutton1" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editRoleCorpManagement.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>
	<s:form id="userRole" name="userRole" action="searchRoleCorpManagementForm" method="post" validate="true">   
	<div id="Layer1" style="width:100%;">
	<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Search</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center">
	<div id="liquid-round-top">
	   <div class="top" style="margin-top:10px;!margin-top:0px;"><span></span></div>
	   <div class="center-content">		
		<table class="table">
		<thead>
		<tr>
		<th>Corp ID</th>
		<th>Role</th>
		<th>Category</th>
		<th>&nbsp;</th>
		</tr>
		</thead>	
		<tbody>
			<tr>	
				<td width="" align="left"><s:select id="corpID" name="corpID" list="%{companyCorpId}"  cssClass="list-menu"  cssStyle="width:200px"  headerKey="" headerValue=""/></td>
				<td width="" align="left"><s:select id="name" name="name" list="%{searchList}"  cssClass="list-menu"  cssStyle="width:200px"  headerKey="" headerValue=""/></td>		
			    <td width="" align="left"><s:select id="category" name="category" list="%{usertype}"  cssClass="list-menu"  cssStyle="width:200px"  headerKey="" headerValue=""/></td>
				<td>
	       		<s:submit cssClass="cssbutton1" cssStyle="width:55px; height:25px;!margin-bottom:10px;" key="button.search"/>  
	       		<input type="button" class="cssbutton1" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/>       
	   			</td>
				 </tr>			 
			</tbody>
		</table>
		</div>
		<div class="bottom-header"><span></span></div>
	</div>
</div>
	
	<c:out value="${searchresults}" escapeXml="false" /> 
	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Role List</span></a></li>
		  </ul>
	</div>
	<div class="spnblk">&nbsp;</div>
</div>
<display:table name="roleList" class="table" requestURI="" id="roleListId" defaultsort="1" export="true" style="width:100%" pagesize="25" >   
	<display:column property="corpID"  style="width:10%" href="editRoleCorpManagement.html" paramId="id" paramProperty="id" sortable="true" title="Corp ID"/>
   	<display:column property="name"  sortable="true" style="width:45%" title="Role Name"/>
   	<display:column property="category"  sortable="true" title="Category"/>
   	<display:column  sortable="true" title="Remove" style="width:8%">
 		<a><img title="Remove" align="middle" onclick="confirmSubmit(${roleListId.id},'${roleListId.category }');" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
	</display:column>
   	</display:table>
	
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
	<c:out value="${buttons}" escapeXml="false" /> 
</s:form>

<script language="javascript" type="text/javascript">

function clear_fields(){
	document.getElementById('corpID').value='';
	document.getElementById('name').value='';
	document.getElementById('category').value='';
}

function quotesNotAllowed(evt){
  var keyCode = evt.which ? evt.which : evt.keyCode;
  if(keyCode==222){
  return false;
  }
  else{
  return true;
  }
}

function confirmSubmit(rowId,category){
	if(category == '' || category == undefined || category == null){
		var agree = confirm("Are you sure want to remove it?");
		if(agree){
			location.href = 'removeRole.html?id=' + rowId;
		}else{
			return false;
		}
	}else{
		alert("You cannot remove role if it is associated with any Category.");
		return false;
	}
}
</script>