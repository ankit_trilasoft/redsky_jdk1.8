<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>  

<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr>
	<td align="left"  style="padding-left:5px;min-width:120px;">
		<b>Crew Truck List</b>
	</td>
	<td align="right"  style="padding-right:5px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  

	<table style="width:250px" cellpadding="0" cellspacing="0">
		<tr>
				<c:if test="${separateCrewList!='[]'}">
					<td valign="top" style="border:none;padding-right:0px;">
					<table class="table">
						 <thead>
						 <tr>
							<th width="">Crew Name</th>
						 </tr>
						 </thead>
						 <tbody>
						 <c:forEach var="individualItem" items="${separateCrewList}" >	
							 <tr>
							 <td class="listwhitetext" align="right" style="width:300px;">${individualItem.crewName}</td>
							 </tr>		 
						</c:forEach>
						
						 </tbody>		 		  
						 </table>
						</td>
				</c:if>
				<c:if test="${separateTruckList!='[]'}">
					<td valign="top" style="border:none;padding-left:2px;">
					<table class="table" >
					 <thead>
					 <tr>
						 <th width="">Truck#</th>
					 </tr>
					 </thead>
					 <tbody>
					 <c:forEach var="individualItem" items="${separateTruckList}" >	
						 <tr>
						 <td  class="listwhitetext" align="right" >${individualItem.localtruckNumber}</td>
						 </tr>		 
					</c:forEach>
					 </tbody>		 		  
					 </table>
					</td>
			</c:if>
		</tr>
</table>