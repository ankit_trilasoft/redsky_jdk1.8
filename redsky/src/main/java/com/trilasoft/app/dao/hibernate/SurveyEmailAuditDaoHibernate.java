package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.SurveyEmailAuditDao;
import com.trilasoft.app.model.SurveyEmailAudit;

public class SurveyEmailAuditDaoHibernate extends GenericDaoHibernate<SurveyEmailAudit, Long> implements SurveyEmailAuditDao{
public SurveyEmailAuditDaoHibernate()
{
	super(SurveyEmailAudit.class);
}
public Long getSurveyEmailAuditSession(String shipNumber,String accountId,String corpId){
	Long l=null;
	getHibernateTemplate().setMaxResults(1);
	List al= getHibernateTemplate().find("Select id from SurveyEmailAudit where shipNumber='"+shipNumber+"' and accountId='"+accountId+"' and corpId='"+corpId+"'");
	if((!al.isEmpty())&&(al!=null))
	{
		l=Long.parseLong(al.get(0).toString());
	}
	return l;
    }
}
