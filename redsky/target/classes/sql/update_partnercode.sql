DELIMITER $$
DROP PROCEDURE IF EXISTS `redsky`.`update_partnercode` $$

CREATE  PROCEDURE `update_partnercode`(oldcode varchar(50), newcode varchar(50))
BEGIN
DECLARE oldcode1 varchar(50) ;
DECLARE newcode1 varchar(50) ;
DECLARE newname1 varchar(50) ;
DECLARE oldname1 varchar(50) ;
DECLARE oldcode2 varchar(50) ;
DECLARE newcode2 varchar(50) ;
DECLARE oldname2 varchar(50) ;
DECLARE newname2 varchar(50) ;
DECLARE pid varchar(50) ;
DECLARE pcorpid varchar(500) ;
DECLARE pcorpid1 varchar(500) ;
declare Ncode int   ;
declare Ocode int   ;
declare msg varchar(50) ;
declare message varchar(50);
select oldcode into oldcode1;
set oldcode2=oldcode1;
select newcode into newcode1;
set newcode2=newcode1;
select id into pid from partnerpublic where partnercode=newcode2 and corpid='TSFT';
select lastname into newname1 from partnerpublic where corpid='TSFT' and partnercode=newcode2;
set newname2=newname1;
select lastname into oldname1 from partnerpublic where corpid='TSFT' and partnercode=oldcode2;
set oldname2=oldname1;
select replace(group_concat(corpid),',','\',\'') into pcorpid from partnerprivate where partnercode=oldcode2 and corpid not in (select corpid from partnerprivate where partnercode=newcode2);
set pcorpid1=pcorpid;
/*start this condition  for partner code is exist in system or not*/
select count(id) into  Ncode from partneraccountref where   partnercode=newcode2   ;
select count(id) into  Ocode from partneraccountref where   partnercode=oldcode2  ;
if ( Ncode>0 and  Ocode>0)  then
set msg='data is comming both sanario check list';
set message=msg;
select Corpid,partnercode,companyDivision,accountCrossReference,RefType from partneraccountref where partnercode=newcode2
union
select Corpid,partnercode,companyDivision,accountCrossReference,RefType from partneraccountref where partnercode=oldcode2
order by 2;
else
set msg='Merge Sussfully';
set message=msg;
update partneraccountref set partnercode=newcode2,updatedBy='MERG_WIZ', updatedOn= now() where  partnercode=oldcode2 ;
update partnervanlineref set partnerCode=newcode2,updatedBy='MERG_WIZ', updatedOn= now() where partnerCode =oldcode2;
update partnerrategrid set partnerCode = newcode2, partnerName = newname2, corpID='TSFT',updatedBy='MERG_WIZ', updatedOn= now() where partnerCode =oldcode2;
update partnerpublic set status='Inactive',updatedBy='MERG_WIZ', updatedOn= now() where corpid='TSFT' and partnercode=oldcode2;
update partnerprivate set partnercode=newcode2,status='Approved',partnerpublicid=pid,updatedBy='MERG_WIZ', updatedOn= now() where  partnercode=oldcode2 and corpid in (pcorpid1);
update customerfile set bookingAgentCode = newcode2, bookingAgentName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where bookingAgentCode =oldcode2;
update customerfile set billToCode = newcode2, billToName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where billToCode =oldcode2;
update customerfile set accountCode = newcode2, accountName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where accountCode =oldcode2;
update serviceorder set bookingAgentCode = newcode2, bookingAgentName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where bookingAgentCode =oldcode2;
update serviceorder set originAgentCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where originAgentCode =oldcode2;
update serviceorder set billtocode = newcode2, billToName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where billtocode =oldcode2;
update serviceorder set originSubAgentCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where originSubAgentCode =oldcode2;
update serviceorder set destinationAgentCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where destinationAgentCode =oldcode2;
update serviceorder set destinationSubAgentCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where destinationSubAgentCode =oldcode2;
update serviceorder set brokerCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where brokerCode =oldcode2;
update serviceorder set forwarderCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where forwarderCode =oldcode2;
update trackingstatus set originAgentCode = newcode2, originAgent = newname2,updatedBy='MERG_WIZ', updatedOn= now() where originAgentCode =oldcode2;
update trackingstatus set destinationAgentCode = newcode2, destinationAgent = newname2,updatedBy='MERG_WIZ', updatedOn= now() where destinationAgentCode =oldcode2;
update trackingstatus set originSubAgentCode = newcode2, originSubAgent = newname2,updatedBy='MERG_WIZ', updatedOn= now() where originSubAgentCode =oldcode2;
update trackingstatus set destinationSubAgentCode = newcode2, destinationSubAgent = newname2,updatedBy='MERG_WIZ', updatedOn= now() where destinationSubAgentCode =oldcode2;
update trackingstatus set brokerCode = newcode2, brokerName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where brokerCode =oldcode2;
update trackingstatus set forwarderCode = newcode2, forwarder = newname2,updatedBy='MERG_WIZ', updatedOn= now() where forwarderCode =oldcode2;
update billing set billToCode = newcode2, billToName = newname2 , updatedBy='MERG_WIZ', updatedOn= now() where billToCode =oldcode2;
update billing set billTo2Code = newcode2, billTo2Name = newname2, updatedBy='MERG_WIZ', updatedOn= now() where billTo2Code =oldcode2;
update billing set privatePartyBillingCode = newcode2, privatePartyBillingName = newname2 , updatedBy='MERG_WIZ', updatedOn= now() where privatePartyBillingCode =oldcode2;
update billing set billingId = newcode2 , updatedBy='MERG_WIZ', updatedOn= now() where billingId =oldcode2;
update billing set vendorCode = newcode2, vendorName = newname2 , updatedBy='MERG_WIZ', updatedOn= now() where vendorCode =oldcode2;
update accountline set oldBillToCode = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where oldBillToCode =oldcode2;
update accountline set billToCode = newcode2, billToName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where billToCode =oldcode2;
update accountline set vendorCode = newcode2, estimateVendorName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where vendorCode =oldcode2;
update workticket set account = newcode2, accountName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where account =oldcode2;
update workticket set vendorCode = newcode2, vendorName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where vendorCode =oldcode2;
update servicepartner set carrierCode = newcode2, carrierName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where carrierCode =oldcode2;
update claim set insurerCode = newcode2, insurer = newname2,updatedBy='MERG_WIZ', updatedOn= now() where insurerCode =oldcode2;
update loss set whoWorkCDE = newcode2, whoWorkName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where whoWorkCDE =oldcode2;
update miscellaneous set bookerSelfPacking = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where bookerSelfPacking =oldcode2;
update miscellaneous set haulingAgentCode = newcode2, haulerName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where haulingAgentCode =oldcode2;
update subcontractorcharges set personId = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where personId =oldcode2;
update cportalresourcemgmt set billToCode = newcode2, billToName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where billToCode =oldcode2;
update defaultaccountline set billToCode = newcode2, billToName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where billToCode =oldcode2;
update defaultaccountline set vendorCode = newcode2, vendorName = newname2,updatedBy='MERG_WIZ', updatedOn= now() where vendorCode =oldcode2;
update pricingcontrol set parentAgent = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where parentAgent =oldcode2;
update app_user set parentAgent = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where parentAgent =oldcode2;
update datasecurityfilter set name = REPLACE(name,oldcode2,newcode2), filterValues = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where filterValues =oldcode2;
update datasecurityset set name = REPLACE(name,oldcode2,newcode2), description = REPLACE(description,oldcode2,newcode2) where substring_index(name,'_',-1)=oldcode2;
update customerfile set originagentcode=newcode2,originagentname=newname2,updatedBy='MERG_WIZ', updatedOn= now() where originagentcode=oldcode2;
update app_user set basedAt=newcode2,basedAtName=newname2,updatedBy='MERG_WIZ', updatedOn= now() where basedAt=oldcode2;
update app_user set bookingAgent=newcode2,updatedBy='MERG_WIZ', updatedOn= now() where bookingAgent=oldcode2;
update contractaccount set accountcode=newcode2,accountName=newname2,updatedBy='MERG_WIZ', updatedOn= now() where accountcode=oldcode2;
update crew set partnercode=newcode2,partnerName=newname2,updatedBy='MERG_WIZ', updatedOn= now() where partnercode=oldcode2;
update claim set paidByName=newname2,updatedBy='MERG_WIZ', updatedOn= now() where paidByName=oldname2;
update companydivision set bookingagentcode=newcode2,updatedBy='MERG_WIZ', updatedOn= now() where bookingagentcode=oldcode2;
update app_user set basedAt = newcode2,updatedBy='MERG_WIZ', updatedOn= now() where basedAt =oldcode2;
update partnerprivate set status='Inactive',updatedBy='MERG_WIZ', updatedOn= now() where partnercode=oldcode2 ;
update partnerprivate set status='Approved',updatedBy='MERG_WIZ', updatedOn= now() where partnercode=newcode2 ;
select concat("update partnerprivate set partnercode='",newcode2,"',status='Approved',partnerpublicid='",pid,"',updatedBy='MERG_WIZ', updatedOn= now() where  partnercode='",oldcode2,"' and corpid in ('",pcorpid1,"');");
select concat("update partnerprivate set status='Inactive',updatedBy='MERG_WIZ', updatedOn= now() where partnercode='",oldcode2,"' and corpid not in ('",pcorpid1,"');");
select concat("update partnerpublic set utsnumber ='Y' where  partnercode='",(select partnercode from partnerprivate where partnercode='",newcode2,"' and corpid='UTSI' and status='Approved'),"';");
do sleep(2);
update partnerpublic set utsnumber ='Y' where  partnercode=(select partnercode from partnerprivate where partnercode=newcode2 and corpid='UTSI' and status='Approved');
end if;
select message;
END$$
DELIMITER ;