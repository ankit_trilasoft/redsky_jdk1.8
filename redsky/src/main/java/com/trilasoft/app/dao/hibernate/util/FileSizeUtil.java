package com.trilasoft.app.dao.hibernate.util;

import java.io.File;
import java.util.StringTokenizer;

public class FileSizeUtil {

	double fileSizeKB;

	public String FindFileSize(File f) {
		String fileLength = String.valueOf(f.length());
		int fileLengthDigitCount = fileLength.length();
		double fileLengthLong = f.length();
		double decimalVal = 0.0;
		String howBig = "";

		System.out.println("fileLengthDigitCount is..." + fileLengthDigitCount);

		if (f.length() > 0) {
			if (fileLengthDigitCount < 5) {
				fileSizeKB = Math.abs(fileLengthLong);
				howBig = "Byte(s)";
			} else if (fileLengthDigitCount >= 5 && fileLengthDigitCount <= 6) {
				fileSizeKB = Math.round(fileLengthLong / 1024);
				howBig = "KB";
			} else if (fileLengthDigitCount >= 7 && fileLengthDigitCount <= 9) {
				fileSizeKB = Math.round(fileLengthLong / (1024 * 1024));
				howBig = "MB";
			} else if (fileLengthDigitCount > 9) {
				fileSizeKB = Math.round(fileLengthLong / (1024 * 1024 * 1024));
				decimalVal = fileLengthLong % (1024 * 1024 * 1024);
				howBig = "GB";
			}
		}
		String finalResult = getRoundedValue(fileSizeKB);
		String fileSize = finalResult + " " + howBig;
		return fileSize;
	}

	private String getRoundedValue(double decimalVal) {
		
		long beforeDecimalValue = decimalTokenize(decimalVal, 1);
		long afterDecimalValue = decimalTokenize(decimalVal, 2);
		long decimalValueLength = String.valueOf(afterDecimalValue).length();
		long dividerVal = divider(decimalValueLength - 1);
		long dividedValue = afterDecimalValue / dividerVal;
		//String finalResult = String.valueOf(beforeDecimalValue) + "." + String.valueOf(dividedValue);
		String finalResult = String.valueOf(beforeDecimalValue);

		return finalResult;
	}

	private long divider(long argLength) {
		long varDivider = 1;

		for (int i = 0; i < (argLength - 1); i++) {
			varDivider = varDivider * 10;
		}

		return varDivider;
	}

	private long decimalTokenize(double decimalVal, int position) {

		long returnDecimalVal = 0;
		String strDecimalVal = "";

		if (decimalVal > 0)
			strDecimalVal = String.valueOf(decimalVal);

		if (strDecimalVal.length() > 0) {
			StringTokenizer decimalToken = new StringTokenizer(strDecimalVal, ".");
			if (position == 1) {
				returnDecimalVal = Long.parseLong(decimalToken.nextToken());
			} else if (position == 2) {
				decimalToken.nextToken();
				returnDecimalVal = Long.parseLong(decimalToken.nextToken());
			}
		}
		return returnDecimalVal;
	}
}
