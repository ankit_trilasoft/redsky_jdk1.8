<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="partnerPrivateList.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerPrivateList.heading'/>"/>   
    <c:if test="${param.popup}"> 
    	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerListForm'].elements['partnerPrivate.lastName'].value = '';
		document.forms['partnerListForm'].elements['partnerPrivate.firstName'].value = '';
		document.forms['partnerListForm'].elements['partnerPrivate.partnerCode'].value = '';
		document.forms['partnerListForm'].elements['partnerPrivate.status'].value = '';
</script>
<style>
span.pagelinks {
display:block;
margin-bottom:0px;
!margin-bottom:2px;
margin-top:-10px;
!margin-top:-17px;
padding:2px 0px;
text-align:right;
width:99%;
!width:98%;
font-size:0.85em;
}



.radiobtn {
margin:0px 0px 2px 0px;
!padding-bottom:5px;
!margin-bottom:5px;
}

form {
margin-top:-40px;
!margin-top:0px;
}

div#main {
margin:-5px 0 0;

}
</style>
</head>
<c:set var="buttons"> 
	<c:if test="${partnerType == 'AC'}">
    	<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editPartnerPrivateAcc.html?partnerType=${partnerType}"/>'" value="<fmt:message key="button.add"/>"/>         
    </c:if>
    <c:if test="${partnerType == 'AG' || partnerType == 'VN'}">
    	<input type="button" class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editPartnerPrivateAgent.html?partnerType=${partnerType}"/>'" value="<fmt:message key="button.add"/>"/> 
    </c:if>
</c:set> 
<div id="layer4" style="width:100%;">
	<div id="newmnav">
		<ul>
			<c:if test="${partnerType == 'AC'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AC"><span>Account Detail</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'AG'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=AG"><span>Agent Detail</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'VN'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=VN"><span>Vendor Detail</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'PP'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=PP"><span>Private Party Detail</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'CR'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=CR"><span>Carrier Detail</span></a></li>
			</c:if>
			<c:if test="${partnerType == 'OO'}">
				<li><a href="editPartnerPublic.html?id=${partnerId}&partnerType=OO"><span>Owner Ops</span></a></li>
			</c:if>
			<configByCorp:fieldVisibility componentId="component.button.partnverPublicScript">
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Additional Info</span></a></li>
			</configByCorp:fieldVisibility>
			<li><a href="partnerPublics.html"><span>Partner List</span></a></li>
		</ul>
	</div>
	<div class="spn">&nbsp;</div>
</div>
	
<div id="layer1" style="width:100%">
<div id="otabs" style="margin-top:-20px; ">
		<ul>
			<li><a class="current"><span>Partner Private List</span></a></li>
		</ul>
</div><div class="spnblk">&nbsp;</div> 


<s:set name="partnerPrivateList" value="partnerPrivateList" scope="request"/>  
<display:table name="partnerPrivateList" class="table" requestURI="" id="partnerPrivateLists" export="false" defaultsort="2" pagesize="10" style="width:99%;margin-left:5px;">  		
	<c:if test="${partnerType == 'AC'}">
		<display:column property="partnerCode" style="width:75px" sortable="true" titleKey="partner.partnerCode" url="/editPartnerPrivateAcc.html?partnerType=${partnerType}&partnerId=${partnerId}" paramId="id" paramProperty="id" />   
	</c:if>
	<c:if test="${partnerType == 'AG' || partnerType == 'VN'}">
		<display:column property="partnerCode" style="width:75px" sortable="true" titleKey="partner.partnerCode" url="/editPartnerPrivateAgent.html?partnerType=${partnerType}&partnerId=${partnerId}" paramId="id" paramProperty="id" />   
	</c:if>
	<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerPrivateLists.firstName} ${partnerPrivateLists.lastName}" /></display:column>
	<display:column property="status" sortable="true" titleKey="partner.status" style="width:100px"/>
    
    <display:setProperty name="paging.banner.item_name" value="Partner Private"/>   
    <display:setProperty name="paging.banner.items_name" value="Partner Private"/>
       
  	<display:setProperty name="export.excel.filename" value="Partner Private List.xls"/>   
    <display:setProperty name="export.csv.filename" value="Partner Private List.csv"/>   
    <display:setProperty name="export.pdf.filename" value="Partner Private List.pdf"/>   
</display:table>  
</div>
<c:out value="${buttons}" escapeXml="false" /> 
<c:set var="isTrue" value="false" scope="session"/>