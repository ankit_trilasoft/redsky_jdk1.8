<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
 <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/bootstrap.min.css'/>" />
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.1.1.min.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/bootstrap.min.js"></script>
   
<style>

div#content {left:0;margin:0 auto 0px 0px; padding:0;position:relative;text-align:left;}
</style>
<style type="text/css">
		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 5.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		a.tooltips {
  position: relative;
  display: inline;
}
a.tooltips span {
  position: absolute;
  width:130px;
  height:35px;
  line-height: 15px;
  padding-top:4px;
  visibility: hidden;
  font-size: 11px;
  text-align: center;
  color: #15428B;     
  border: 1px solid #56bcdd;
  border-radius: 5px;         
  box-shadow: rgba(0, 0, 0, 0.1) 1px 1px 2px 0px;
 background: rgb(254,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZlZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNkMmViZjkiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top, rgba(254,255,255,1) 0%, rgba(210,235,249,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(254,255,255,1)), color-stop(100%,rgba(210,235,249,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* IE10+ */
background: linear-gradient(to bottom, rgba(254,255,255,1) 0%,rgba(210,235,249,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffff', endColorstr='#d2ebf9',GradientType=0 ); /* IE6-8 */
}
a.tooltips span:after {
  content: '';
  position: absolute;
  top: 100%;
  left: 60%;
  margin-left: -8px;
  width: 0; height: 0;
  border-color: #56bcdd transparent transparent transparent;
  border-width: 10px;
  border-style: solid;
}
a:hover.tooltips span {
  visibility: visible;
  opacity: 1;
  bottom: 34px;
  left: 50%;
  margin-left: -66px;
  z-index: 999;
} 

.cshead {border-bottom:1px dashed #b7b5b5;padding-top:5px;padding-bottom:3px;max-width:250px;}
.qhead {padding-top:10px;padding-bottom:3px;}
		

		h4 {color: #444;font-size: 14px;line-height: 1.3em; margin: 0 0 0.25em;padding: 0 0 0 15px;font-weight:bold;}
		.modal-body {padding: 0 5px; max-height: calc(100vh - 140px); overflow-y: auto; margin-top: 10px }
		.modal-header { border-bottom: 1px solid #e5e5e5; min-height: 20.43px; padding: 10px;}		
		.modal-footer { border-top: 1px solid #e5e5e5; padding: 10px; text-align: right;}
		.modal-header .close {margin-right:5px;margin-top: -2px; text-align: right;}
		@media screen and (min-width: 768px) {
		    .custom-class {width: 70%;
		        /* either % (e.g. 60%) or px (400px) */
		    }
		}
		.hide{display:none;}
		.show{display:block;}
		.list-menu {height: 19px;!height: 20px;}
		.hidden{display:none;}
		
	</style>
	 <style>
  span.pagelinks {
 display:block;font-size:0.95em;margin-bottom:3px;!margin-bottom:2px;margin-top:-8px;padding:2px 0px;
 text-align:right;width:99%;
}
.btn-group-sm > .btn, .btn-sm {    
   box-shadow: 0 2px 5px 0 rgba(0,0,0,.16),0 2px 10px 0 rgba(0,0,0,.12) !important;
}
.btn.focus, .btn:focus, .btn:hover {outline:none !important;}

.alert { margin-bottom: 0px;}

#modalAlert{
    padding: 10px!important;
    overflow-y: inherit!important;
    margin-top: 0px!important;
}
.loading-indicator {position: absolute;background-color: #fff; padding:15px; border-radius:5px;left: 45%;top: 40%;}
   .key_so_dashboard { background: url("images/key_partner_list.jpg") no-repeat scroll 0 0 transparent; cursor: default; height: 22px; margin-left:22%; }
  
    </style>
</head>
<body>
<c:set var="buttons"> 
<c:if test="${ppType ==''}">  
	<input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=973797&myFileFor=SO&flagAgent=true&noteFor=ServiceOrder&active=true&secure=${secure}&forQuotation=${forQuotation}&partnerCode=${vendorCode}&docUpload=docCentre"/>'" value="<fmt:message key="button.upload"/>"/>
</c:if>
	<c:if test="${ppType !=''}"> 
	 <input type="button" class="cssbutton" style="margin-right: 5px;width:65px;" onclick="location.href='<c:url value="/uploadMyFile!start.html?id=973797&myFileFor=SO&flagAgent=true&noteFor=ServiceOrder&active=true&secure=${secure}&ppType=${ppType}&partnerCode=${vendorCode}&forQuotation=${forQuotation}&PPID=${PPID}&docUpload=docCentre"/>'" value="<fmt:message key="button.upload"/>"/>
	</c:if>
</c:set> 
		
<s:form id="" name="" action="" method="post" validate="true" enctype="multipart/form-data" >
	
	
    
<tr><td>		
<table class="table-fc" style="width:100%;margin-top:-3px; " id="table-File" >
<thead>
<tr style="height:25px">
		<td rowspan="2" class="newtablehead" style="width:1%"></td>
				<td rowspan="2" class="newtablehead" style="width:1%">Document&nbsp;Type&nbsp;&nbsp;</td>
		 <td rowspan="2" class="newtablehead" style="width:10%">Description&nbsp;&nbsp;</td>
			<td rowspan="2" class="newtablehead" style="width:3%">Uploaded&nbsp;On&nbsp;&nbsp;</td>
					<td rowspan="2" class="newtablehead" style="width:3%">Actions</td>
					
		
		<td rowspan="2" class="newtablehead" style="width:3%">Uploaded&nbsp;By</td>
		
		<td rowspan="2" class="newtablehead" style="width:3%">Document Category</td>
		
		
	
		
		
		
	</tr>
	
	
	</thead>
	<tbody>
	
		
		 
			 
		 <c:forEach var="individualItem" items="${filecabinetFiles}" >
		 <tr>
		
         <td  class="listwhitetext" align="right" > <input type="checkbox"  id="chk${individualItem.id}"  name="check" class="isha test" onclick="updateLinking('${individualItem.id}',this);" /></td>
      
		 <td  class="listwhitetext" align="right" >${individualItem.fileType}</td>
		 <td  class="listwhitetext" align="right" >${individualItem.description}</td>
		 <td style="text-align:center">
										<img src="${pageContext.request.contextPath}/images/edit.png"  alt="Edit" title="Edit" onclick="return performAction('Edit', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" `/>
										<img src="${pageContext.request.contextPath}/images/split.png" alt="Split" title="Split" onclick="return performAction('Split', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										<img src="${pageContext.request.contextPath}/images/delete.png" alt="Waste Basket" title="Waste Basket" onclick="return performAction('Remove', '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');"/>
										
									
								
									
									<%-- <s:select cssClass="list-menu" cssStyle="width:72px" name="action" list="{'','Edit','Split','Remove'}" onchange="return performAction(this, '${myFileFor}', '${myFileList.id}', '${fileId}', '${fileNameFor}', '${secure}','${forQuotation}','${ppType}','${myFileList.fileContentType}','${from}');" /> --%>
								</td>
		 <td  class="listwhitetext" align="right" >${individualItem.updatedOn}</td>
		  <td  class="listwhitetext" align="right" >${individualItem.updatedBy}</td>
		    <td  class="listwhitetext" align="right" >${individualItem.documentCategory}</td>
		    
		 <s:hidden name="vendorCode" value="${individualItem.vendorCode}"/>
		 
		</c:forEach>
	</tbody>
</table>
</td></tr>	
<table border="0" cellpadding="2"  style="margin:0px">
<tbody>
<tr>

	
					    </div>
				
				</tr>
				</tbody>
				</table>
				
	
</tr>
</tbody>
</table>

<c:out value="${buttons}" escapeXml="false" />

<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>

<input type="button" class="cssbutton" style="margin-right: 5px;width:70px;" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
</s:form>

</div>
</div> 
</div>

<script type="text/javascript">
  
</script>

 
</body>