/* tScroll v1.0 - Continuous thumbnail scrolling javascript */
/* Javascript by James Nisbet of bandit.co.nz and gooduse.co.nz */
/* With efficiency modifications by Hayden Smith (HGS) of silverstripe.com */

/* Last modified: 27/06/07 by James Nisbet */


/* Usage:
 *****************************************
 * Create new scroll instance:
 * --> var scroll = new tScroll();
 *
 * Initialize the instance as below:
 * myInstance.init('MyScrollDiv')
 * --> scroll.init('ScrollArea');
 *****************************************
*/

function tScroll() {	
	// should we automatically stop the scroll after 10mins?
	// this will help with excessive memory issues and cpu usage
	this.autoStop = false;
	
	// do we want to attempt to parse styles (for images with padding, margins etc)
	// this adds accuracy to the reset, recommended: ON
	this.parseStyles = true;
	
	// HGS - Number of milliseconds
	// (scroll speed in ms - less is faster)
	// Default: 50
	this.delay = 50;
	
	// define system vars
	var imgs = new Array();
	var links = Array();
	var loc = 0;
	var cnt = 0;
	
	// HGS - Total width of the images, not including spacing between them (should have it)
	var totalWidth = 0;
	
	// this scrolls the underlying div
	this.autoScroll = function() {
		loc++;
		
		// if location is further than totalWidth, reset position
		if(loc > totalWidth) loc = 0;
		
		// minus 1px from scrolling element's leftmost position
		ctent.style.left = -loc+'px';
		
		// stop after 600 seconds (10 minutes) if this.autoStop is set
		// this is to cut down on unnecessary memory consumption and cpu usage
		if(!this.autoStop||loc<(600000/this.delay)) {
			var self = this;
			// HGS - Set the scroll timer so we can clear it in the stop function
			this.scrollTimer = setTimeout(function(){self.autoScroll();},this.delay);
		}
	}
	
	// initializes all variables and reads dom
	this.init = function(what) {
		// grab the designated scrolling dom element
		ctent = document.getElementById(what);
		
		// populate arrays with elements from dom
		cnodes = ctent.getElementsByTagName('a');
		for(i=0;i<cnodes.length;i++) {
			links[i] = cnodes[i].href;
			imgs[i] = new Image;

			img = cnodes[i].getElementsByTagName('img')[0]; 
			imgs[i].src = img.src;
			
			// if set, get the post-computed styles from the element and add to total width
			if(this.parseStyles) {
				totalWidth += img.width;
				// styles to check values of
				var checkStyles = new Array('padding-left','padding-right','margin-left','margin-right','border-left-width','border-right-width');
				// because IE is frustrating
				var checkStylesIE = new Array('paddingLeft','paddingRight','marginLeft','marginRight','borderLeftWidth','borderRightWidth');
				for(x=0;x<checkStyles.length;x++) {
					if ((val = parseInt(this.getStyle(img,checkStyles[x],checkStylesIE[x]))) > 0) totalWidth += val;
				}
			}
			else {
				// offsetWidth should offer greater accuracy since we're not checking styles
				if(img.offsetWidth) totalWidth += img.offsetWidth;
				else totalWidth += img.width;
			}
		}
		
		var length = cnodes.length;
		var hiddenWidth = 0;
		
		// HGS - Add the images needed to the end of the row
		var repeat = totalWidth / ctent.offsetWidth;
		for(j=0; j < repeat; j++) {
			for(k=0; k < length; k++) {
				var clone = cnodes[k].cloneNode(true);
				ctent.appendChild(clone);
			}
			
			hiddenWidth += totalWidth;	
		}
	
		ctent.style.width = (hiddenWidth + totalWidth) + 'px' 
		
		// start scrollin'
		this.autoScroll();
	}
	
	// HGS - functions to stop and start the scroller 
	this.doStop = function() {
		if(this.scrollTimer) clearTimeout(this.scrollTimer);
	}
	
	this.doStart = function() {
		this.autoScroll();
	}
	
	// ppk's function to get a post-computed style of an element
	// quicksmode.org/dom/getstyles.html
	// (modified for use if this.parseStyles is set)
	this.getStyle = function(el,styleProp,stylePropIE) {
		var y = 0;
		if(el != 'undefined') {
			if (el.currentStyle) y = el.currentStyle[stylePropIE];
			else if (window.getComputedStyle) y = document.defaultView.getComputedStyle(el,null).getPropertyValue(styleProp);
		}
		return y;
	}
}

/* For more information on the usage of this script, please visit bandit.co.nz */