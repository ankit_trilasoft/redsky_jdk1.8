<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ include file="/common/tooltip.jsp"%>
<head>   
    <title>Quality Survey Configuration</title>   
    <meta name="heading" content="Quality Survey Configuration"/>   

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>	
<style>
span.pagelinks {
display:block;
font-size:0.9em;
margin-bottom:3px;
margin-top:0px;
!margin-top:-20px;
padding:2px 0px;
text-align:right;
width:100%;
}
/*jitendra 11-10-2018*/
span.pagelinks {top: 0px;
    position: absolute;}
/*end jitendra 11-10-2018*/
</style>
<script language="javascript">
function clear_fields(){
	document.forms['qualitySurveyListPage'].elements['accountCode'].value = '';
	document.forms['qualitySurveyListPage'].elements['accountName'].value = '';
 }
</script>
</head>
<s:form id="qualitySurveyListPage" action="searchQualitySurveyList" method="post" validate="true">
<div id="layer1" style="width:100%;">
<div id="newmnav" style="!padding-bottom:5px;">
	  <ul>
	  <li id="newmnav1" style="background:#FFF "><a class="current"><span>Search</span></a></li>	  	
	  </ul>
</div>
<div class="spn">&nbsp;</div> 	
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px;" align="top" method="" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;" onclick="clear_fields();"/>     
</c:set>
<div id="content" align="center">
<div id="liquid-round" style="margin:0px;">
    <div class="top"><span></span></div>
    <div class="center-content">
		<table class="table" border="0">
		<thead>
		<tr>
		<th>Partner Code</th>
		<th>Partner Name</th>		
		<th></th>	
		</tr>
		</thead>	
		<tbody>
				<tr>
  				    <td align="left">
					    <s:textfield name="accountCode" required="true" cssClass="input-text" size="45"/>
					</td>
					<td align="left">
					    <s:textfield name="accountName" required="true" cssClass="input-text" size="45"/>
					</td>
					<td><c:out value="${searchbuttons}" escapeXml="false"/></td>					
				</tr>	
				
		</tbody>
		</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div> 
	<div id="Layer1" style="width:100%;position:relative;">
  
			<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Quality Survey List</span></a></li>
		  </ul>
		</div>
		<div class="spn">&nbsp;</div>
				<display:table name="qualityList" class="table" requestURI="" id="qualityList" pagesize="10" export="true" style="width:100%;!margin-top:13px;">
			    <display:column  sortable="true" title="Partner Code" style="width: 15%">
<c:url value="getEmailConfigrationOfPartner.html" var="url1" >
<c:param name="accountId" value="${qualityList.partnerCode}"/>
<c:param name="accountName" value="${qualityList.lastName}"/>
<c:param name="mode" value="${qualityList.mode}"/>
</c:url>			    
			    	<a href="${url1}"><c:out value="${qualityList.partnerCode}"></c:out></a>
			   </display:column>			    
				<display:column property="lastName" sortable="true" title="Partner Name" />
				<display:column sortable="true" title="Mode" >
				<c:choose>
				<c:when test="${qualityList.mode=='Relocation'}">
					<c:set var="modeTemp" value="Relocation" />
				</c:when>
				<c:when test="${qualityList.mode=='Air/Sea/Truck'}">
					<c:set var="modeTemp" value="Air/Sea/Truck" />
				</c:when>
				<c:otherwise>
			    	<c:set var="modeTemp" value="" />
					<c:forEach var="entry" items="${jobList}">
		            <c:if test="${entry.key==qualityList.mode}">
		            <c:set var="modeTemp" value="${entry.value}" />
		            </c:if>
		            </c:forEach>
				</c:otherwise>
				</c:choose>
		            <c:out value="${modeTemp}"></c:out>
            	</display:column>
				<display:column  title="Email Config" style="width:75px;text-align:center;" >
				<a href="${url1}"><img title="Email Config" src="images/email_small.gif"/></a>
				</display:column>				
				</display:table> 
	   
</div>
</div>
</s:form>