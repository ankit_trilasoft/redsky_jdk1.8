package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;

import org.appfuse.webapp.action.BaseAction;
import com.trilasoft.app.model.VanLineCommissionType;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.VanLineCommissionTypeManager;

public class VanLineCommissionTypeAction extends BaseAction {
	private List vanLineCommissionTypeList;

	private Long id;

	private VanLineCommissionTypeManager vanLineCommissionTypeManager;

	private String sessionCorpID;

	private VanLineCommissionType vanLineCommissionType;

	private RefMasterManager refMasterManager;

	private Map<String, String> glCode;
	
	private String gotoPageString;
	
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(VanLineCommissionTypeAction.class);


	public String getGotoPageString() {

	   return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

	   this.gotoPageString = gotoPageString;

	}

	private String validateFormNav;
    
    public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}
	
	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		  //if (option enabled for this company in the system parameters table then call save) {
		validateFormNav = "OK";
	    String s = save();    // else simply navigate to the requested page)
	    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return gotoPageString;
	}

	public VanLineCommissionTypeAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineCommissionTypeList = vanLineCommissionTypeManager.vanLineCommissionTypeList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String  listCommission(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				//String key = "Please enter your search criteria below";
	   	//saveMessage(getText(key));
	   	vanLineCommissionTypeList=new ArrayList();
	   	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String  searchCommission(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(vanLineCommissionType!=null){
		boolean code = (vanLineCommissionType.getCode() == null);
		boolean type = (vanLineCommissionType.getType() == null);
	    boolean desc = (vanLineCommissionType.getDescription() == null);
	    boolean glCode = (vanLineCommissionType.getGlCode() == null);
	    
          if(!code ||!desc ||!glCode ) {
        	  vanLineCommissionTypeList =  vanLineCommissionTypeManager.searchVanLineCommissionPopup(vanLineCommissionType.getCode(), vanLineCommissionType.getType(), vanLineCommissionType.getDescription(), vanLineCommissionType.getGlCode());
        	  
          }else{
        	  vanLineCommissionTypeList =  vanLineCommissionTypeManager.searchVanLineCommissionPopup("", "", "", "");  
          }
          }else{
        	  vanLineCommissionTypeList =  vanLineCommissionTypeManager.searchVanLineCommissionPopup("", "", "", "");  
          }
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;     
	}

	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		glCode = refMasterManager.findByParameter(corpId, "GLCODES");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (id != null) {
			vanLineCommissionType = vanLineCommissionTypeManager.get(id);
		} else {
			vanLineCommissionType = new VanLineCommissionType();
			vanLineCommissionType.setCreatedOn(new Date());
			vanLineCommissionType.setUpdatedOn(new Date());
		}
		getComboList(sessionCorpID);
		vanLineCommissionType.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (vanLineCommissionType.getId() == null);
		getComboList(sessionCorpID);
		List isexisted=vanLineCommissionTypeManager.isExisted(vanLineCommissionType.getCode(),sessionCorpID);
		if(!isexisted.isEmpty() && isNew){
			errorMessage("Van Line Commission Code already exist.");
			return INPUT; 
		}else{ 
		if (isNew) {
			vanLineCommissionType.setCreatedOn(new Date());
		} 

		vanLineCommissionType.setUpdatedOn(new Date());
		vanLineCommissionType.setUpdatedBy(getRequest().getRemoteUser());
		vanLineCommissionType.setCorpID(sessionCorpID);
		
		if(vanLineCommissionType.getPc().doubleValue()>999.99 || vanLineCommissionType.getPc().doubleValue()<-999.99)
		{
			String keymassage="Percent value should be less than 1000";
			errorMessage(getText(keymassage));
		}
		else{
		vanLineCommissionType = vanLineCommissionTypeManager.save(vanLineCommissionType);
		if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
		String key = (isNew) ? "Commission Type has been added successfully" : "Commission Type has been updated successfully";
		saveMessage(getText(key));
		}
		}
			if (!isNew) {
				return INPUT;
			} else {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				return SUCCESS;
			}
		}	
	}
	
	@SkipValidation 
	  public String searchCommissionType(){  
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean code = (vanLineCommissionType.getCode() == null);
		boolean type = (vanLineCommissionType.getType() == null);
	    boolean desc = (vanLineCommissionType.getDescription() == null);
	    boolean glCode = (vanLineCommissionType.getGlCode() == null);
	    
          if(!code ||!desc ||!glCode ) {
        	  vanLineCommissionTypeList =  vanLineCommissionTypeManager.searchVanLineCommissionType(vanLineCommissionType.getCode(), vanLineCommissionType.getType(), vanLineCommissionType.getDescription(), vanLineCommissionType.getGlCode());
        	  
          }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;     
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setVanLineCommissionTypeManager(VanLineCommissionTypeManager vanLineCommissionTypeManager) {
		this.vanLineCommissionTypeManager = vanLineCommissionTypeManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public VanLineCommissionType getVanLineCommissionType() {
		return vanLineCommissionType;
	}

	public void setVanLineGLType(VanLineCommissionType vanLineCommissionType) {
		this.vanLineCommissionType = vanLineCommissionType;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public List getVanLineCommissionTypeList() {
		return vanLineCommissionTypeList;
	}

	public void setVanLineCommissionTypeList(List vanLineCommissionTypeList) {
		this.vanLineCommissionTypeList = vanLineCommissionTypeList;
	}

	public void setVanLineCommissionType(VanLineCommissionType vanLineCommissionType) {
		this.vanLineCommissionType = vanLineCommissionType;
	}

	

	
}
