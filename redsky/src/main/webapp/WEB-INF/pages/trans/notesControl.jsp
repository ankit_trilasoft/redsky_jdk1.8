<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<meta name="heading" content="<fmt:message key='notesControl.heading'/>"/> 
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/redskyditor/tiny_mce.js"></script>
<title><fmt:message key="notesControl.title"/></title> 
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</head>

<style type="text/css">h2 {background-color: #FBBFFF}</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>	
	
<script>
String.prototype.trim = function() {
	    return this.replace(/^\s+|\s+$/g,"");
	}
	String.prototype.ltrim = function() {
	    return this.replace(/^\s+/,"");
	}
	String.prototype.rtrim = function() {
	    return this.replace(/\s+$/,"");
	}
	String.prototype.lntrim = function() {
	    return this.replace(/^\s+/,"","\n");
	}
	 
	
	
	function getHTTPObject()
	{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
	}
    var http2 = getHTTPObject();


	function submitAction(){
		agree= confirm("Are you sure you want to dismiss this note?");
		if(agree){
		var url = "dismissReminder1.html?ajax=1&decorator=simple&popup=true";
		document.forms['notesControl'].action = url;
		document.forms['notesControl'].submit();
		}
		else{
			return false;
		}	
	}
	
	function handleHttpResponseCheckReminder()
        {

             if (http2.readyState == 4)
             {
                
                	alert('Your notes has been marked as completed, please refresh to do list to view refreshed list');
                	self.close();
             }
    
        }
	
	function refreshParent() {
			  window.opener.location.href = window.opener.location.href;
				if (window.opener.progressWindow)
					{
			    		window.opener.progressWindow.close()
			  		}
			  refreshParent1();
			  refreshParent1();
			   refreshParent1();
		}
		
			function refreshParent1() {
			  window.opener.location.href = window.opener.location.href;
				if (window.opener.progressWindow)
					{
			    		window.opener.progressWindow.close()
			  		}
			  
		}
			function buttonName(targerelement){
				//alert(targerelement.value);
				document.forms['notesControl'].elements['btntype'].value=targerelement.value;
		}
		
		
</script>	

<script type="text/javascript">
			// Use it to attach the editor to all textareas with full featured setup
			//WYSIWYG.attach('all', full);
			<configByCorp:fieldVisibility componentId="component.field.partnerUser.showEditor">	
			// Use it to attach the editor directly to a defined textarea
			 tinyMCE.init({
			
			 mode : "textareas",
			 theme : "advanced",
			 relative_urls : false,
			 remove_script_host : false,
			 plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave",
			 
			 theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,code,|,insertdate,inserttime|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft",
				 theme_advanced_toolbar_location : "top",
					theme_advanced_toolbar_align : "left",
					theme_advanced_statusbar_location : "bottom",
					theme_advanced_resizing : true,
					// Example content CSS (should be your site CSS)
					content_css : "css/content.css",

					// Drop lists for link/image/media/template dialogs
					template_external_list_url : "lists/template_list.js",
					external_link_list_url : "lists/link_list.js",
					external_image_list_url : "lists/image_list.js",
					media_external_list_url : "lists/media_list.js"
			 }) 
			  </configByCorp:fieldVisibility>
		</script> 

<script language="JavaScript">	

	function onlyTimeFormatAllowed(evt)
		{
		
		  var keyCode = evt.which ? evt.which : evt.keyCode;
		  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
		}
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<script language="javascript" type="text/javascript">
		function setTimeMask() {
		
		oTimeMask1 = new Mask("##:##", "remindTime");
		oTimeMask1.attach(document.forms['notesControl'].remindTime);
		
		}
</script>

<SCRIPT LANGUAGE="JavaScript">



function IsValidTime() {

var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['notesControl'].elements['remindTimeUpdate'].value;

var matchArray = timeStr.match(timePat);
if (document.forms['notesControl'].elements['forwardDateUpdate'].value == '' || document.forms

['notesControl'].elements['remindTimeUpdate'].value == '' || document.forms

['notesControl'].elements['remindTimeUpdate'].value == '00:00' || document.forms

['notesControl'].elements['notes.followUpFor'].value == '' || document.forms

['notesControl'].elements['notes.remindInterval'].value == '') {
	if(document.forms['notesControl'].elements['forwardDateUpdate'].value == '' && 

(document.forms['notesControl'].elements['remindTimeUpdate'].value == '' || document.forms

['notesControl'].elements['remindTimeUpdate'].value=='00:00')){
		alert ("Please select follow up date and time.");
		document.forms['notesControl'].elements['forwardDateUpdate'].focus();
		return false;
	}else {
		if(document.forms['notesControl'].elements['forwardDateUpdate'].value == ''){
			alert ("Please select follow up Date");
			document.forms['notesControl'].elements['forwardDateUpdate'].focus();
			return false;
		}
		if(document.forms['notesControl'].elements['remindTimeUpdate'].value == '' || 

document.forms['notesControl'].elements['remindTimeUpdate'].value == '00:00'){
			alert ("Please select follow up time");
			document.forms['notesControl'].elements['remindTimeUpdate'].focus();
			return false;
		}
		if(document.forms['notesControl'].elements['notes.followUpFor'].value == ''){
			alert ("Please select follow up For ");
			document.forms['notesControl'].elements['notes.followUpFor'].focus();
			return false;
		}
		if(document.forms['notesControl'].elements['notes.remindInterval'].value == ''){
			alert ("Please select reminder interval");
			document.forms['notesControl'].elements['notes.remindInterval'].focus();
			return false;
		}
	}
}
if (matchArray == null) {
alert("Time is not in a valid format.");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
//document.forms['notesControl'].elements['remindBtn'].disabled = true ;
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Follow up  time must be between 0 and 23(Hrs)");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
return false;
}
if (minute<0 || minute > 59) {
alert ("Follow up  time must be between 0 and 59(Mins)");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
return false;
}
return checkDate();
document.forms['notesControl'].submit();
return false;
}
function checkDate(){
		var date1 = document.forms['notesControl'].elements['forwardDateUpdate'].value; 
		var systenDate = new Date();
		var mySplitResult = date1.split("-");
	   	var day = mySplitResult[0];
	   	var month = mySplitResult[1];
	   	var year = mySplitResult[2];
	 	if(month == 'Jan'){
	       month = "01";
	   	}else if(month == 'Feb'){
	       month = "02";
	   	}else if(month == 'Mar'){
	       month = "03"
	   	}else if(month == 'Apr'){
	       month = "04"
	   	}else if(month == 'May'){
	       month = "05"
	   	}else if(month == 'Jun'){
	       month = "06"
	   	}else if(month == 'Jul'){
	       month = "07"
	   	}else if(month == 'Aug'){
	       month = "08"
	   	}else if(month == 'Sep'){
	       month = "09"
	   	}else if(month == 'Oct'){
	       month = "10"
	   	}else if(month == 'Nov'){
	       month = "11"
	   	}else if(month == 'Dec'){
	       month = "12";
	   	}
	   	var finalDate = month+"-"+day+"-"+year;
	   	date1 = finalDate.split("-");
	  	var enterDate = new Date(date1[0]+"/"+date1[1]+"/20"+date1[2]);
	  	var newSystenDate = new Date(systenDate.getMonth()+1+"/"+systenDate.getDate()

          +"/"+systenDate.getFullYear());
	  	var daysApart = Math.round((enterDate-newSystenDate)/86400000);
	  	
	  	if(daysApart < 0){
	    	alert("Reminder can not be set on the past follow up date or time");
	    	document.forms['notesControl'].elements['forwardDateUpdate'].value='';
	    	return false;
	  	}
	  	
	  	if(date1 != ''){
	  		if(daysApart == 0){
	  			var time = document.forms['notesControl'].elements['remindTimeUpdate'].value;
				var hour = time.substring(0, time.indexOf(":"))
				var min = time.substring(time.indexOf(":")+1, time.length);
	  			
	  			if (hour < 0  || hour > 23) {
					alert("Follow up  time must be between 0 and 23(Hrs)");
					document.forms['notesControl'].elements['remindTimeUpdate'].value = '00:00'
					document.forms['notesControl'].elements['remindTimeUpdate'].focus();
					return false;
				}
				if (min<0 || min > 59) {
					alert ("Follow up  time must be between 0 and 59(Mins)");
					document.forms['notesControl'].elements['remindTimeUpdate'].value = '00:00'
					document.forms['notesControl'].elements['remindTimeUpdate'].focus();
					return false;
				}
				
				if(systenDate.getHours() > hour){
					document.forms['notesControl'].elements['remindTimeUpdate'].value = '00:00'
					alert("Reminder can not be set on the past follow up date or time");
					return false;
				}else if(systenDate.getHours() == hour && systenDate.getMinutes() > min){
					document.forms['notesControl'].elements['remindTimeUpdate'].value = '00:00'
					alert("Reminder can not be set on the past follow up date or time");
					return false;
				}
				
			}
	  	}
}


 function completeTimeString() {

		stime1 = document.forms['notesControl'].elements['remindTimeUpdate'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['notesControl'].elements['remindTimeUpdate'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['notesControl'].elements['remindTimeUpdate'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['notesControl'].elements['remindTimeUpdate'].value = stime1 + "00";
			}
		}
		
	}
 
 /* function IsValidTime() {
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['notesControl'].elements['remindTimeUpdate'].value;

var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format.");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
//document.forms['notesControl'].elements['remindBtn'].disabled = true ;
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("Follow up  time must be between 0 and 23(Hrs)");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
//document.forms['notesControl'].elements['remindBtn'].disabled = true ;
return false;
}
if (minute<0 || minute > 59) {
alert ("Follow up  time must be between 0 and 59(Mins)");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
//document.forms['notesControl'].elements['remindBtn'].disabled = true ;
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("Second must be between 0 and 59.");
document.forms['notesControl'].elements['remindTimeUpdate'].value = '';
document.forms['notesControl'].elements['remindTimeUpdate'].focus();
//document.forms['notesControl'].elements['remindBtn'].disabled = true ;
return false;
}
//if(document.forms['notesControl'].elements['notes.forwardToUser'].value == ''){
//	alert("Since you has not chosen any forwardTo user, the reminder will be set to you");
//}

}

 */


function openURL(sURL) {
opener.document.location = sURL;
}

function openAccountContact(partnerId, accountContactId){
var url="editAccountContact.html?pID="+partnerId+"&partnerType=AC&id="+accountContactId;
var newWindow = window.open(url, '_blank');
newWindow.focus();
}

function openAccountProfile(partnerId, accountContactId){
var url="editNewAccountProfile.html?id="+partnerId+"&partnerType=AC";
var newWindow = window.open(url, '_blank');
newWindow.focus();

}

function openRespectiveRecords(noteType){
var url=""; 
if(noteType=='Customer File'){
// javascript:openURL('editCustomerFile.html?id=${customerFile.id}&decorator=popup','','width=850,height=500');self.close();
	url="editCustomerFile.html?id=${customerFile.id}";
}
if(noteType=='Quotation File'){
	// javascript:openURL('editCustomerFile.html?id=${customerFile.id}&decorator=popup','','width=850,height=500');self.close();
		url="QuotationFileForm.html?id=${customerFile.id}";
	}
if(noteType=='Service Order'){
// javascript:openURL('editServiceOrderUpdate.html?id=${serviceOrder.id}&decorator=popup','','width=850,height=500');self.close();
	url="editServiceOrderUpdate.html?id=${serviceOrder.id}";
}
if(noteType=='quote'){
	// javascript:openURL('editServiceOrderUpdate.html?id=${serviceOrder.id}&decorator=popup','','width=850,height=500');self.close();
		url="editQuotationServiceOrderUpdate.html?id=${serviceOrder.id}";
	}
if(noteType=='Claim'){
	url="editClaim.html?id=${notes.notesKeyId}";
}

if(noteType=='Work Tickets'){
	url="editWorkTicketUpdate.html?id=${notes.notesKeyId}";
}
if(noteType=='Account Line' || noteType=='Service Partner'){
	url="editCustomerFile.html?id=${customerFile.id}";
}
if(noteType=='Container'){
	url="editContainer.html?id=${notes.notesKeyId}";
	}
if(noteType=='Carton'){
	url="editCarton.html?id=${notes.notesKeyId}";
	}
if(noteType=='Vehicle'){
	url="editVehicle.html?id=${notes.notesKeyId}";
	}
if(noteType=='Partner'){
	url="editPartnerPublic.html?id=${partnerPublic.id}";
	}

var newWindow = window.open(url, '_blank');
newWindow.focus();


}

</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
  <!--
function changeStatus(){
	document.forms['notesControl'].elements['formStatus'].value = '1';
}
function addEvent( obj, type, fn )
{
	if (obj.addEventListener)
		obj.addEventListener( type, fn, false );
	else if (obj.attachEvent)
	{
		obj["e"+type+fn] = fn;
		obj[type+fn] = function() { obj["e"+type+fn]( window.event ); }
		obj.attachEvent( "on"+type, obj[type+fn] );
	}
}

function removeEvent( obj, type, fn )
{
	if (obj.removeEventListener)
		obj.removeEventListener( type, fn, false );
	else if (obj.detachEvent)
	{
		obj.detachEvent( "on"+type, obj[type+fn] );
		obj[type+fn] = null;
		obj["e"+type+fn] = null;
	}
}

/*
Create the new window
*/
function openInNewWindow() {
	// Change "_blank" to something like "newWindow" to load all links in the same new window
    var newWindow = window.open(this.getAttribute('goToUrl'), '_blank');
    newWindow.focus();
    return false;
}

/*
Add the openInNewWindow function to the onclick event of links with a class name of "new-window"
*/
function getNewWindowLinks() {
	// Check that the browser is DOM compliant
	if (document.getElementById && document.createElement && document.appendChild) {
		//Change this to the text you want to use to alert the user that a new window will be opened
		var strNewWindowAlert = "";
		// Find all links
		var links = document.getElementsByTagName('a');
		var objWarningText;
		var strWarningText;
		var link;
		for (var i = 0; i < links.length; i++) {
			link = links[i];
			// Find all links with a class name of "non-html"
			if (/\bnon\-html\b/.exec(link.className)) {
				link.onclick = openInNewWindow;
			}
		}
		objWarningText = null;
	}
}


function getSubType(target){
 	if(target.value==''){
    	document.forms['notesControl'].elements['notes.noteSubType'].options[0].text = '';
		document.forms['notesControl'].elements['notes.noteSubType'].options[0].value = '';
	}
    if(target.value != ''){
     	var url="getSubTypeList.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
     	http3.open("GET", url, true);
     	http3.onreadystatechange = handleHttpResponse;
     	http3.send(null);
   	}
}

function handleHttpResponse(){
            if (http3.readyState == 4) { 
              	var results = http3.responseText
             	results = results.trim();
              	var res = results.split("#");
              	if(res.length == 2)  {
	                var targetElement = document.forms['notesControl'].elements['notes.noteSubType'];
					targetElement.length = (res.length-1);
 					for(i=1;i<res.length;i++){ 	
 						document.forms['notesControl'].elements['notes.noteSubType'].options[0].text= res[i];
						document.forms['notesControl'].elements['notes.noteSubType'].options[0].value = res[i];
					}
				} else {
					var targetElement = document.forms['notesControl'].elements['notes.noteSubType'];
					targetElement.length = res.length;
 					for(i=0;i<res.length;i++){
 						document.forms['notesControl'].elements['notes.noteSubType'].options[i].text = res[i];
						document.forms['notesControl'].elements['notes.noteSubType'].options[i].value = res[i];
					}
				}
				document.forms['notesControl'].elements['notes.noteSubType'].value='${notes.noteSubType}';
			}
}


var http3 = getHTTPObject();

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function linkVisible(){
var chkedData = document.forms['notesControl'].elements['notes.noteType'].value;
var linkDiv = document.getElementById("linkto");
var linkDiv1 = document.getElementById("linkto1");
if(chkedData == 'Issue Resolution' || chkedData=='Exception Service'){
		//linkDiv.style.display = 'block';
		//linkDiv1.style.display = 'block';
	}
	else{
		//linkDiv.style.display = 'none';
		//linkDiv1.style.display = 'none';
	}

}
var http24=getHTTPObject();
var http23=getHTTPObject();
var http25=getHTTPObject();
var http2222 = getHTTPObject();
function findNewAgents(){
var spNumber=document.forms['notesControl'].elements['notes.linkedTo'].value;
	if(spNumber!=''){
	  var url = "findNewAgents.html?ajax=1&decorator=simple&popup=true&spNumber=" + encodeURI(spNumber)+"&cid=";
	  http2222.open("GET", url, true);
	  http2222.onreadystatechange = handleHttpResponseAgents;
	  http2222.send(null);
	}else{
	var url = "findNewAgents.html?ajax=1&decorator=simple&popup=true&spNumber=" + encodeURI(spNumber)+"&cid=${customerFile.id}";
	  http2222.open("GET", url, true);
	  http2222.onreadystatechange = handleHttpResponseAgents;
	  http2222.send(null);
	}
}


function handleHttpResponseAgents(){
            if (http2222.readyState == 4) { 
              	var results = http2222.responseText
              	res=results.split("~");
              	tarElem=document.getElementById("supplierId");
              	tarElem.length = res.length;
           		for(i=0;i<res.length;i++)
					{
					if(res[i] == ''){
					document.forms['notesControl'].elements['notes.supplier'].options[i].text = '';
					document.forms['notesControl'].elements['notes.supplier'].options[i].value = '';
					}else{
					codeName = res[i].split("#");
					document.forms['notesControl'].elements['notes.supplier'].options[i].text = codeName[1].trim();;
					document.forms['notesControl'].elements['notes.supplier'].options[i].value = codeName[0].trim(); 
					
					 } 
					 }
					 document.getElementById("supplierId").value='${notes.supplier}';
             	
			}
}
function getGradingDetails(){
	var notesFor='${noteFor}';
	var CFbillToCode='';
	target=document.forms['notesControl'].elements['notes.noteType'];	
	    if(target.value != '' && target.value=='Exception Service' ){
	    	var spNumber=document.forms['notesControl'].elements['notes.linkedTo'].value;
	    	if(spNumber!=''){
		    	//alert(spNumber)					    	
	    		var url="getNoteGradinglinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumber="+encodeURI(spNumber);
		     	http25.open("GET", url, true);
		     	http25.onreadystatechange = handleHttpResponse25;
		     	http25.send(null);
	    	}else{
		    	if(notesFor=='CustomerFile' || notesFor=='agentQuotes'){
		    		CFbillToCode='${customerFile.billToCode}';
		    	}else if(notesFor=='ServiceOrder' || notesFor=='agentQuotesSO'){
		    		CFbillToCode='${serviceOrder.billToCode}';
		    	}				    	
		    	//alert(CFbillToCode)
	    		var url="getNoteGrading.html?ajax=1&decorator=simple&popup=true&spNumber="+encodeURI(CFbillToCode)+"&noteFor=" + encodeURI(target.value);
		     	http25.open("GET", url, true);
		     	http25.onreadystatechange = handleHttpResponse25;
		     	http25.send(null);
	    	}			     	
	   	}else{
	   		var url="getNoteGrading.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
	     	http25.open("GET", url, true);
	     	http25.onreadystatechange = handleHttpResponse25;
	     	http25.send(null);
	   	}
	}
	function handleHttpResponse25(){
	            if (http25.readyState == 4) { 
	              	var results = http25.responseText
	             	results = results.trim();
	              	var res = results.split("#");
	              	//alert(res)
	              	var targetElement = document.forms['notesControl'].elements['notes.grading'];
	              	 targetElement.length= res.length;
	           	   for(i=0; i<res.length; i++){
	 	     		if(res[i] == ''){
	 	     		}else{
	 	     		document.forms['notesControl'].elements['notes.grading'].options[i].text =res[i].split('~')[1];
	 	     		document.forms['notesControl'].elements['notes.grading'].options[i].value=res[i].split('~')[0];		 	     	
	 		         }
	 	     		document.forms['notesControl'].elements['notes.grading'].value='${notes.grading}';
	                 }
	              
				}
	}
	function getSubCategoryDetails(){
		var notesFor='${noteFor}';	
		target=document.forms['notesControl'].elements['notes.noteType'];	
		    if(target.value != '' && target.value=='Exception Service' ){
			    if(notesFor=='ServiceOrder' || notesFor=='agentQuotesSO'){
			    	var spNumber='${serviceOrder.shipNumber}';
			    	//alert(spNumber)
			    	var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumber="+encodeURI(spNumber);
			    	http23.open("GET", url, true);
			     	http23.onreadystatechange = handleHttpResponse23;
			     	http23.send(null);
			    }else{
			    	var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
			     	http23.open("GET", url, true);
			     	http23.onreadystatechange = handleHttpResponse23;
			     	http23.send(null);
			    }		     
		   	}else{
		   		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
		     	http23.open("GET", url, true);
		     	http23.onreadystatechange = handleHttpResponse23;
		     	http23.send(null);
		   	}
		}
	function handleHttpResponse23(){
        if (http23.readyState == 4) { 
          	var results = http23.responseText
         	results = results.trim();
          	var res = results.split("#");
          	//alert(res)
          	var targetElement = document.forms['notesControl'].elements['notes.issueType'];
          	 targetElement.length= res.length;
       	   for(i=0; i<res.length; i++){
	     		if(res[i] == ''){
	     		}else{
	     		document.forms['notesControl'].elements['notes.issueType'].options[i].text =res[i].split('~')[1];
	     		document.forms['notesControl'].elements['notes.issueType'].options[i].value=res[i].split('~')[0];		 	     	
		         }
	     		document.forms['notesControl'].elements['notes.issueType'].value='${notes.issueType}';
             }
          
		}
}
	function getSubCategorySoDetails(){
		var notesFor='${noteFor}';		
		target=document.forms['notesControl'].elements['notes.noteType'];	
		    if(target.value != '' && target.value=='Exception Service' ){
		    	var spNumber=document.forms['notesControl'].elements['notes.linkedTo'].value;			    
		    	if(spNumber!=''){			    
		    		var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumber="+encodeURI(spNumber);
			     	http24.open("GET", url, true);
			     	http24.onreadystatechange = handleHttpResponse24;
			     	http24.send(null);
		    	}else if(spNumber=='' && (notesFor=='ServiceOrder' || notesFor=='agentQuotesSO')){	
		    		spNumber='${serviceOrder.shipNumber}';		    		
		    		var url="getNoteSubCategorylinkedTo.html?ajax=1&decorator=simple&popup=true&noteFor="+encodeURI(target.value)+"&spNumber="+encodeURI(spNumber);
			     	http24.open("GET", url, true);
			     	http24.onreadystatechange = handleHttpResponse24;
			     	http24.send(null);
		    	}else{				    	
		    		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
			     	http24.open("GET", url, true);
			     	http24.onreadystatechange = handleHttpResponse24;
			     	http24.send(null);
		    	}			     	
		   	}else{				 
		   		var url="getNoteSubCategory.html?ajax=1&decorator=simple&popup=true&noteFor=" + encodeURI(target.value);
		     	http24.open("GET", url, true);
		     	http24.onreadystatechange = handleHttpResponse24;
		     	http24.send(null);
		   	}
		}
		function handleHttpResponse24(){
		            if (http24.readyState == 4) { 
		              	var results = http24.responseText
		             	results = results.trim();
		              	var res = results.split("#");
		              	//alert(res)
		              	var targetElement = document.forms['notesControl'].elements['notes.issueType'];
		              	 targetElement.length= res.length;
		           	   for(i=0; i<res.length; i++){
		 	     		if(res[i] == ''){
		 	     		}else{
		 	     		document.forms['notesControl'].elements['notes.issueType'].options[i].text =res[i].split('~')[1];
		 	     		document.forms['notesControl'].elements['notes.issueType'].options[i].value=res[i].split('~')[0];		 	     	
		 		         }
		 	     		document.forms['notesControl'].elements['notes.issueType'].value='${notes.issueType}';
		                 }
		              
					}
		}
addEvent(window, 'load', getNewWindowLinks);
  //-->
  </SCRIPT>
  <script type="text/javascript">
  function checkType(){
	  var nateValue=document.forms['notesControl'].elements['notes.noteType'].value
	 // alert(nateValue);
	  if(nateValue=='Customer File' || nateValue=='Issue Resolution' || nateValue=='Exception Service'){
		  window.open('editNewNote.html?id1=${customerFile.id}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor=CustomerFile','','width=800,height=600');
	  }else if(nateValue=='Claim'){
		  window.open('editNewNoteForClaim.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	  }else if(nateValue=='Work Tickets'){
		  window.open('editNewNoteForWorkTicket.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	     }else if(nateValue=='Service Order'){
	     window.open('editNewNoteForServiceOrder.html?id=${serviceOrder.id}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	     }else if(nateValue=='Container'){
		  window.open('editNewNoteForContainer.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	     }else if(nateValue=='Carton'){
		     window.open('editNewNoteForCarton.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	     }else if(nateValue=='Vehicle'){
		     window.open('editNewNoteForVehicle.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
			 }else if(nateValue=='Partner'){
		     window.open('editNewNoteForPartnerPrivate.html?id=${notes.notesKeyId}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
			  }else{
	    window.open('editNewNote.html?id1=${customerFile.id}&noteId=${notes.id}&generatedfrom=activityManagement&decorator=popup&popup=true&noteFor'+nateValue,'','width=800,height=600');
	  }
}
</script>
<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="{0,date,MM/dd/yyyy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="MM/dd/yyyy"/>
<% } %>
<s:form id="notesControl" name="notesControl" action="saveNotesfromReminder.html?decorator=popup&popup=true" onsubmit="return IsValidTime();" method="post" validate="true" >
<s:hidden name="serviceOrder.controlFlag" />
<s:hidden name="customerFile.controlFlag" />
<s:hidden name="notes.id" />
<s:hidden name="idNote" value="${notes.id}"/>
<s:hidden name="customerFile.firstName" />
<s:hidden name="customerFile.lastName" />
<s:hidden name="customerFile.sequenceNumber" />
<s:hidden name="formStatus"/>
<s:hidden name="notes.noteStatus" />
<s:hidden name="customerFile.id" />
<s:hidden name="partnerPublic.id" />
<c:if test="${notes.noteType!='Issue Resolution' && notes.noteType!='Customer File' && notes.noteType!='Exception Service'}">
<s:hidden name="notes.noteType" value="${notes.noteType}" />
<s:hidden name="notes.noteSubType" value="${notes.noteSubType}" />
</c:if>

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="notes" value="<%=request.getParameter("id")%>" scope="session"/>
<div id="layer1"  style="width:100%;margin-bottom:1px; ">
 	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="!margin-top:5px;"><span></span></div>
   <div class="center-content">
 <table class="" cellspacing="1" cellpadding="1" border="0" style="" width="100%">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext" width="100%">
	  	<table class="detailTabLabel" border="0">
			<tbody>  	
				  	<tr>
				  		<td align="left" height="3px" colspan="6"></td>
				  	</tr>
				  	<tr>
					
					  	<c:if test="${notes.noteType=='AccountContact'||notes.noteType=='AccountProfile'||notes.noteType=='Service Order'||notes.noteType=='Customer File'||notes.noteType=='Claim'||notes.noteType=='Work Tickets'||notes.noteType=='Container'||notes.noteType=='Vehicle'||notes.noteType=='Account Line'||notes.noteType=='Service Partner'||notes.noteType=='Issue Resolution' || notes.noteType=='Exception Service' || notes.noteType=='Carton'}">
					  	<td align="right" width="75px"><fmt:message key="billing.shipper"/></td>
					   <td align="left"><s:textfield name="serviceOrder.firstName"  value="%{customerFile.firstName} %{customerFile.lastName}" size="20"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	
					  	  	<c:if test="${notes.noteType=='Activity'}">
					  	<td align="right" width="75px"><fmt:message key="billing.shipper"/></td>
					   <td align="left"><s:textfield name="serviceOrder.firstName"  value="%{customerFile.firstName} %{customerFile.lastName}" size="20"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if> 
					  	
					  	<c:if test="${notes.noteType=='Partner'}">
					<td align="right" width="75px"><fmt:message key="billing.shipper"/></td>
					  	<td align="left"><s:textfield name="serviceOrder.firstName"  value="%{notes.name}" size="20"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='AccountContact'}">
					  	<td align="right" width="100px">Partner Code</td>
				  		<td align="left" ><s:textfield name="notes.notesId" value="${notes.notesId}" cssStyle="cursor: pointer;" size="15"  cssClass="input-textUpper" readonly="true" onclick="openAccountContact('${partnerPublic.id}','${notes.notesKeyId}');"/></td>
					  	<td align="right" width="100px">Partner Name</td>
				  		<td align="left" ><s:textfield name="partnerPublic.lastName" value="${partnerPublic.lastName}" cssStyle="cursor: pointer;" size="25"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='AccountProfile'}">
					  	<td align="right" width="100px">Partner Code</td>
				  		<td align="left" ><s:textfield name="notes.notesId" value="${notes.notesId}" cssStyle="cursor: pointer;" size="15"  cssClass="input-textUpper" readonly="true" onclick="openAccountProfile('${partnerPublic.id}','${notes.notesKeyId}');"/></td>
					  	<td align="right" width="100px">Partner Name</td>
				  		<td align="left" ><s:textfield name="partnerPublic.lastName" value="${partnerPublic.lastName}" cssStyle="cursor: pointer;" size="25"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='Partner'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}"  size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
					    <c:if test="${not empty customerFile.sequenceNumber}">
					  	<c:if test="${notes.noteType=='Service Order'&& (serviceOrder.controlFlag=='Q')}">
					  	<td align="right" width="100px"><fmt:message key="serviceOrder.shipNumber"/></td>
					  	<td align="left" ><s:textfield name="serviceOrder.shipNumber" cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('quote')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='Service Order'&& serviceOrder.controlFlag=='C'}">
					  	<td align="right" width="100px"><fmt:message key="serviceOrder.shipNumber"/></td>
					  	<td align="left" ><s:textfield name="serviceOrder.shipNumber" cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Service Order')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='Customer File' && customerFile.controlFlag=='C'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield name="serviceOrder.sequenceNumber" cssStyle="cursor: pointer;" value="%{customerFile.sequenceNumber}" onclick="openRespectiveRecords('Customer File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Customer File' && customerFile.controlFlag=='Q'}">
					  	<td align="right" width="100px">Quotation File#</td>
				  		<td align="left" ><s:textfield name="serviceOrder.sequenceNumber" cssStyle="cursor: pointer;" value="%{customerFile.sequenceNumber}" onclick="openRespectiveRecords('Quotation File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Claim'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Claim')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Work Tickets'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Work Tickets')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Container'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Container')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Carton'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Carton')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Vehicle'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Vehicle')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Account Line'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.customerNumber}" onclick="openRespectiveRecords('Account Line')" size="15" cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Service Partner'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.customerNumber}" onclick="openRespectiveRecords('Service Partner')"  size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='Issue Resolution' || notes.noteType=='Exception Service'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield name="serviceOrder.sequenceNumber" cssStyle="cursor: pointer;" value="%{customerFile.sequenceNumber}" onclick="openRespectiveRecords('Customer File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		<c:if test="${notes.noteType=='FamilyDetails'}">
					  	<td align="right" width="100px"><fmt:message key="notes.notesId"/></td>
				  		<td align="left" ><s:textfield  cssStyle="cursor: pointer;" value="%{notes.customerNumber}" onclick="openRespectiveRecords('Customer File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		</c:if>
				  		</c:if> 
				  		<c:if test="${notes.noteType=='Issue Resolution' || notes.noteType=='Exception Service'}">
				  		 	<td id="linkto" align="left" class="listwhitetext" >
				  		 	<table class="detailTabLabel">
				  		 	<tr>
				  		 	<td align="right" width="60" class="listwhitetext">Linked&nbsp;To</td>
				  		 	<td align="left">
				  		 		  <s:select cssClass="list-menu" name="notes.linkedTo" list="%{linkedToList}" cssStyle="width:112px;"  headerKey="" headerValue="" onchange="changeStatus();getSubCategorySoDetails();getGradingDetails();findNewAgents();"/>
								<s:hidden name="noteFor" value="CustomerFile" />
				  		 	</td>				  		 	
				  		 	<td align="right" width="50" class="listwhitetext">
				  		 	 <c:if test="${notes.noteType!='Exception Service' }">
				  		 	Grading
				  		 	</c:if>
				  		 	 <c:if test="${notes.noteType=='Exception Service'}">
				  		      Approved&nbsp;By&nbsp;/&nbsp;Denied&nbsp;By
				  		     </c:if>
				  		 	</td>
				  		 		 <td align="left"><s:select cssClass="list-menu" name="notes.grading" list="%{grading}" cssStyle="width:105px;"  headerKey="" headerValue="" onchange="changeStatus();"/>
							</td>
							</tr>
							</table>
							</td>
						</c:if>
					
				  	  </tr>
				  	  <tr>
				  	<c:if test="${notes.noteType=='Customer File' || notes.noteType=='Issue Resolution' || notes.noteType=='Exception Service'}">
				  	<td align="right" class="listwhitetext" style="width:50px; !width:95px"><fmt:message key="notes.noteType"/></td>
					<td align="left" class="listwhitetext" width="75px"> <s:textfield cssClass="list-menu" id="noteTypeId" name="notes.noteType"  cssStyle="width:126px;height:15px"  readonly="true" onchange="linkVisible();changeStatus();getSubType(this);"/></td>
				   	<td align="right" class="listwhitetext" width="55px" style="!padding:0px">
				   	<c:if test="${notes.noteType!='Exception Service' }">
				   	<fmt:message key="notes.noteSubType"/>
				   	</c:if>
				   	<c:if test="${notes.noteType=='Exception Service'}">
				   	<fmt:message key="notes.noteCategory"/>
				   	</c:if>
				   	</td>
					<td align="left" class="listwhitetext" > <s:select cssClass="list-menu" name="notes.noteSubType" list="%{notesubtype}" cssStyle="width:105px;height:20px"  headerKey="" headerValue="" onchange="changeStatus();"/></td>
					<td id="linkto1" align="left" class="listwhitetext" >
				  		 	<table class="detailTabLabel">
				  		 <tr>
				  		 <td align="right" width="60"  class="listwhitetext">
				  		 <c:if test="${notes.noteType!='Exception Service'&& notes.noteType!='Customer File'}">
				  		 Issue&nbsp;Type
				  		 </c:if>
				  		 <c:if test="${notes.noteType=='Exception Service'}">
				  		 Sub-Category
				  		 </c:if>
				  		 </td>
				  		  <c:if test="${notes.noteType!='Customer File' }">
				  		 	<td align="left">
				  		 		<s:select cssClass="list-menu" name="notes.issueType" list="%{issueType}" cssStyle="width:113px;"  headerKey="" headerValue="" onchange="changeStatus();"/>
						 	</td>
						 	<td align="right" width="50" class="listwhitetext">Caused&nbsp;By</td>
						 	<td align="left">
						 	<s:select id="supplierId" cssClass="list-menu" name="notes.supplier" list="%{supplier}" cssStyle="width:105px;"  onchange="changeStatus();"/>
						 	</td>
						 	</c:if>
						 	</tr>
				  		</table>
							</td>
					</c:if>
					
				  	</tr>
				  	<tr>
					  	<td align="right"><fmt:message key="notes.remindTime"/></td>
					  	<td align="left"><s:textfield name="notes.remindTime"   size="20"  cssClass="input-textUpper" readonly="true" /></td>
					  	<td align="right"><fmt:message key="notes.remindDate"/></td>
					  	<s:text id="forwardDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.forwardDate"/></s:text> 
					  	<td align="left"><s:textfield name="notes.forwardDate"   value="%{forwardDateFormattedValue}" size="15"  cssClass="input-textUpper" readonly="true"  /></td>
					  	<c:if test="${notes.noteType=='Activity'&& (serviceOrder.controlFlag=='Q')}">
					  	<td align="right" ><fmt:message key="serviceOrder.shipNumber"/></td>
					  	<td align="left" ><s:textfield name="serviceOrder.shipNumber" cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('quote')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
					  	<c:if test="${notes.noteType=='Activity'&& serviceOrder.controlFlag=='C'}">
					  	<td align="right" ><fmt:message key="serviceOrder.shipNumber"/></td>
					  	<td align="left" ><s:textfield name="serviceOrder.shipNumber" cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Service Order')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
					  	</c:if>
						<c:if test="${notes.noteType=='Activity'&& customerFile.controlFlag=='Q'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield name="serviceOrder.sequenceNumber" cssStyle="cursor: pointer;" value="%{notes.notesId}" onclick="openRespectiveRecords('Quotation File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		
				  		</c:if>
					  	<c:if test="${notes.noteType=='Activity'&& customerFile.controlFlag=='C'}">
					  	<td align="right" width="100px"><fmt:message key="customerFile.sequenceNumber"/></td>
				  		<td align="left" ><s:textfield name="serviceOrder.sequenceNumber" cssStyle="cursor: pointer;" value="%{customerFile.sequenceNumber}" onclick="openRespectiveRecords('Customer File')" size="15"  cssClass="input-textUpper" readonly="true" /></td>
				  		
				  		</c:if>
				  	</tr>
				  	<c:if test="${not empty notes.followUpId}">
				  	<!--<tr>
				  		<td align="right">Original Note #</td>
					  	<td align="left"><s:textfield name="notes.followUpId"  onclick="window.open('notesControl.html?id=${notes.followUpId}&decorator=popup&popup=true','','width=550,height=345');" size="25"  cssClass="input-textUpper" readonly="true"/></td>
				  	</tr>
				  	
				  	
				  	--><tr>
				  	<tr>
				  		<td align="left" colspan="3" style="cursor: pointer;"  onclick="window.open('notesControl.html?id=${notes.followUpId}&decorator=popup&popup=true','','width=550,height=345');"><font color="Green" size="2">Click here to go to the original Note</font></td>
					 </tr>
					 </c:if>
					<c:if test="${notes.noteType=='Issue Resolution'}">
					<!--<tr>
				  		<td align="left" colspan="3" style="cursor: pointer;"  onclick="window.open('editNewNote.html?id=${notes.id}&id1=${notes.notesKeyId }&decorator=popup&popup=true','','width=750,height=500');"  ><font color="Green" size="2">Click here to go to the original Note</font></td>
					 </tr>
					--></c:if>
				</tbody>
			</table>
		  
		<table class="detailTabLabel" border="0" width="100%">
		<tbody> 
			  			   
			<tr>
			  		<td align="left" class="listwhitetext"><fmt:message key="notes.subject"/>
			  		<s:textfield cssClass="input-text"  cssStyle="height:16px" id="subject" name="notes.subject" size="107" maxlength="100" /></td>
			  	</tr>
			  <tr><td class="listwhitetext" style="font-size: 14px; font-weight: bold">Reminder Details</td></tr>
			  <tr><td class="vertlinedata"></td></tr>
			  	<tr>
			  		<td align="left" colspan="4"><s:textarea name="notes.note"  cols="122" rows="10" readonly="false" cssClass="textarea"/></td>
			  	</tr>
		</tbody>
  </table>
  	<table border="0" class="detailTabLabel">
		<tr>		
        	<td>
        	<table class="detailTabLabel">	
        	<tr>
        	<td>
        		<display:table name="myFileList" class="table" requestURI="" id="myFileList" export="true" defaultsort="2" defaultorder="ascending" pagesize="5" style="min-width:400px;" >   
					   <display:column property="description" sortable="true" title="Description" />
					   <display:column sortable="true" title="Document">
					   	<a onclick="javascript:openWindow('ImageServletAction.html?id=${myFileList.id}&decorator=popup&popup=true',900,600);"><c:out value="${myFileList.fileFileName}" escapeXml="false"/></a>
					   </display:column>
					   <display:column property="createdOn" sortable="true" title="Created On" />
					   <display:column property="updatedOn" sortable="true" title="Updated On" />
					  </display:table>
        	</td> 	
        	</tr> 	
        	</table>
        		</td>
        		<td align="left" valign="top" style="padding-top:3px;">
		<input type="button" class="cssbutton" style="margin-right: 0px;height: 25px;width:120px; font-size: 15;"  
        	onclick="javascript:openWindow('myAllFiles.html?fileId=${notes.notesId}&id=${notes.id}&decorator=popup&popup=true',755,500);"  
        	value="Link to File Cabinet"/></td>
		</tr>
		</table>
	
		
		<table class="detailTabLabel" border="0">
			<tbody>
				<tr>
							
							<td align="right" class="listwhitetext">Follow Up On</td>
							<s:text id="notesForwardDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="notes.forwardDate"/></s:text>
							<td align="left"><s:textfield cssClass="input-text"  id="forwardDate" name="forwardDateUpdate" value="%{notesForwardDateFormattedValue}" size="7" maxlength="11"  readonly="true"/></td>
							<td align="left"><img id="forwardDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
							<td align="left" class="listwhitetext"></td>
							<td align="center" class="listwhitetext"><b>@</b></td>
							<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" id="remindTime" name="remindTimeUpdate" value="%{notes.remindTime}" size="3" maxlength="5" onkeydown="setTimeMask();return onlyTimeFormatAllowed(event)" onchange="completeTimeString()" /></td>
							<td align="left" class="listwhitetext">remind&nbsp;@</td>
							<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="notes.remindInterval"  list="%{remindIntervals}" cssStyle="width:90px" headerKey="" headerValue="" /></td>
							
							<c:set var="forwardUser" value="${notes.followUpFor}"/>
							<c:set var="remoteUser" value="${pageContext.request.remoteUser}"/>
							<c:if test="${forwardUser==remoteUser}">
							<td align="center" class="listwhitetext">FollowUp To</td>
							<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="notes.followUpFor" list="%{all_user}" cssStyle="width:170px" headerKey="" headerValue="" /></td>
							</c:if>
							<c:if test="${forwardUser!=remoteUser}">
							<td align="center" class="listwhitetext">FollowUp To</td>
							<td align="left" class="listwhitetext"><s:select cssClass="list-menu" name="notes.followUpFor"  list="%{all_user}" cssStyle="width:170px" headerKey="" headerValue="" /></td>
							</c:if>
							
				</tr>
		</tbody>
		</table>
		</td>
		</tr>
		
		</tbody>
		
	</table>
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<table style="margin:0px; ">
					<tbody>
					
						<tr>
							<td align="right" class="listwhitetext" width="73px" style="font-size:1em"><b><fmt:message key='notes.createdOn'/></b></td>
							<td style="">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${notes.createdOn}" pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="notes.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${notes.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" width="73px"><b><fmt:message key='notes.createdBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.createdBy"/>
								<td width="60px" style=""><s:label name="createdBy" value="%{notes.createdBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style=""><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							<td align="right" class="listwhitetext" width="73px" style="font-size:1em"><b><fmt:message key='notes.updatedOn'/></b></td>
							<td style="">
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${notes.updatedOn}" pattern="${displayDateTimeFormat}"/>
							<s:hidden name="notes.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<fmt:formatDate value="${notes.updatedOn}" pattern="${displayDateTimeFormat}"/>
							</td>
							<td align="right" class="listwhitetext" width="73px"><b><fmt:message key='notes.updatedBy' /></b></td>
							<c:if test="${not empty notes.id}">
								<s:hidden name="notes.updatedBy"/>
								<td style="" ><s:label name="updatedBy" value="%{notes.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty notes.id}">
								<s:hidden name="notes.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style=""><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>	
	<table class="detailTabLabel" border="0">
				<tbody>
					
				<tr align="center">
				<td align="left"> 
					<input type="submit" class="cssbutton1" name="Dismiss" style="width: 120px" value="Keep Active" onclick="buttonName(this);"/>
					<input type="button" class="cssbutton1" name="Dismiss" style="width: 120px" value="Completed" onclick="return submitAction();"/>
					<!--<input type="button" class="cssbutton1" style="width: 120px" value="Set New Followup" onclick="refreshParent(); buttonName(this);" />  <a href="javascript:openWindow('notesControl.html?id=${notes2List.id}&decorator=popup&popup=true',550,345)">
					<input type="button" class="cssbutton1" style="width: 120px" value="Set New Followup" onclick="location.href='<c:url value="/editNewNote.html?id=${customerFile.id }"/>'" />-->
					<c:if test="${notes.noteType!='' && notes.noteType!='AccountContact' && notes.noteType!='AccountProfile' && notes.noteType!='Activity' && notes.noteSubType!='ActivityManagement' }">
						 <input type="button" class="cssbutton1" style="width: 120px" value="Set New Follow Up" onclick="checkType(form);"/>
					</c:if>
				</td>   
			</tr>
		</tbody>
	</table>
		
		
<s:hidden name="btntype"/>
<s:hidden name="ownerName" value="<%=request.getParameter("ownerName")%>"/>

<div id="mydiv" style="position:absolute"></div>
</s:form>
<script>
setOnSelectBasedMethods([]);
setCalendarFunctionality();
try{
<c:if test="${param.popup}"> 
var btn ="<%=request.getParameter("btntype") %>";
if(!(btn == 'null')){
//alert('Button Name---->>'+btn);
var type;
var agree
	if(btn == 'Set New Followup'){
		type = 'Reminder edited succesfully,';
		var agree = confirm(type+"\n"+"Click Ok to close this window,"+"\n"+" or click 'Cancel' to continue working in this reminder.");
		if(agree){
		self.close();
		}
		}
		
	if(btn == 'Keep Active'){
		type = 'Do you want to keep it active,';
		var agree = confirm(type+"\n"+"Click Ok to close this window,"+"\n"+" or click 'Cancel' to continue working in this reminder.");
		if(agree){
		//	window.opener.location.href ="toDos.html?fromUser=${pageContext.request.remoteUser}";
		// window.opener.location.href = window.opener.location.href;
		//window.opener.document.forms['toDo'].submit();
		//window.opener.document.forms['toDo'].reload();
		opener.location.reload();
		self.close();
		}
}
	
}
</c:if>
<c:if test="${hitFlag == 1}" >
	alert('Your notes has been completed, please refresh the to do list to get new set of notes');
	self.close();
</c:if>
}


catch(e){}
try {linkVisible();
getGradingDetails();
getSubCategoryDetails();
}
catch(e){}
try {getSubType(document.getElementById("noteTypeId"));}
catch(e){}
try {findNewAgents();}
catch(e){}

</script>
<script>
<c:if test="${notesComp =='Yes'}">
alert('Your notes has been completed, please refresh the to do list to get new set of notes');
self.close();
//window.refreshParent();
//window.self.close();
</c:if>
</script>