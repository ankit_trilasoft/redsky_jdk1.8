package com.trilasoft.app.webapp.action;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.sun.org.apache.bcel.internal.generic.NEW;
import com.trilasoft.app.model.DriverCommissionPlan;
import com.trilasoft.app.service.DriverCommissionManager;
import com.trilasoft.app.service.RefMasterManager;

public class DriverCommissionAction extends BaseAction implements Preparable{
   private DriverCommissionPlan driverCommissionPlan;
   private DriverCommissionManager driverCommissionManager;
   private RefMasterManager refMasterManager;
   private String sessionCorpID;
   private Long id;
   //private List planList;
   private  Map<String, String> planCommissionList;
   private String hitFlag;
   private List contractList;
   private List chargess;
   private List multiplContracts;
   private String planValue;
   private List planListValue;
   private String chargeValue;
   private List chargeListValue;
   private List commissionPlan;
   private List chargeList; 
   private String contractValue;
   private String percentValue;
   private String delCommissionId; 
   private String dummPlan;
   private String dummCharges;
   private String planAmount;
   private List chargesDescriptionValue;
   private String Amount1;
   private List amtList;
	static final Logger logger = Logger.getLogger(DriverCommissionAction.class);
	Date currentdate = new Date();

public void prepare() throws Exception {
	   try{
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			this.sessionCorpID = user.getCorpID();
			}catch(Exception ex)
			{
			ex.printStackTrace();	
			}
			 //planList=driverCommissionManager.getPlanList(sessionCorpID);
			contractList=driverCommissionManager.getcontractList(sessionCorpID);
			  chargeList=driverCommissionManager.getChargeList(sessionCorpID);
			  planCommissionList = refMasterManager.findByParameter(sessionCorpID, "COMMISSIONPLAN");
			  
			  amtList=new ArrayList();
			  amtList.add("ActualLineHaul");
			  amtList.add("DistributionAmount");
			  amtList.add("EstimatedRevenue");
			  amtList.add("GrossRevenue");
			  amtList.add("RevisionRevenue");
			 
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			  
   }
   @SkipValidation
	public String edit(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(id!=null){
			driverCommissionPlan=driverCommissionManager.get(id);
			 chargess = driverCommissionManager.getChargedriverList(driverCommissionPlan.getCharge(),sessionCorpID);
			 dummPlan=driverCommissionPlan.getPlan();
			 dummCharges=driverCommissionPlan.getCharge();
		      multiplContracts = new ArrayList();
			if((driverCommissionPlan.getContract()!=null) && (!driverCommissionPlan.getContract().equals(""))){
			String[] serviceType = driverCommissionPlan.getContract().split(",");
			for (String sType : serviceType) {
				multiplContracts.add(sType.trim());
			    }
			}
		}else{
			chargess=new ArrayList();
			driverCommissionPlan=new DriverCommissionPlan();
			driverCommissionPlan.setCreatedOn(new Date());
			driverCommissionPlan.setUpdatedOn(new Date());
			driverCommissionPlan.setCorpID(sessionCorpID);
			driverCommissionPlan.setUpdatedBy(getRequest().getRemoteUser());
			driverCommissionPlan.setCreatedBy(getRequest().getRemoteUser());
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 return SUCCESS;
	}
   @SkipValidation
     public String save() throws Exception{
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   boolean isNew = (driverCommissionPlan.getId() == null); 
	   if(isNew){
		   int i=driverCommissionManager.checkPlan(driverCommissionPlan.getPlan(),driverCommissionPlan.getCharge(),driverCommissionPlan.getContract(),sessionCorpID);
		   chargess = driverCommissionManager.getChargedriverList(driverCommissionPlan.getCharge(),sessionCorpID);
		   if(i>0 && isNew ){
		    	errorMessage("Commission Plan Charge Code and Contract already exist In Driver Commission.");
		    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    	return INPUT;
		    }
		   if(driverCommissionPlan.getContract()==null){
			  driverCommissionPlan.setContract("");
		   }
		    driverCommissionPlan.setUpdatedOn(new Date());
		    driverCommissionPlan.setCorpID(sessionCorpID);
		    driverCommissionPlan.setUpdatedBy(getRequest().getRemoteUser());
		    driverCommissionPlan.setCreatedOn(new Date());
		    driverCommissionPlan.setCreatedBy(getRequest().getRemoteUser());
		 
	   }else{
		   String plandumm=driverCommissionPlan.getPlan();
	        String Chargesdumm=driverCommissionPlan.getCharge();
		   int i=driverCommissionManager.checkPlan(driverCommissionPlan.getPlan(),driverCommissionPlan.getCharge(),driverCommissionPlan.getContract(),sessionCorpID);
		   chargess = driverCommissionManager.getChargedriverList(driverCommissionPlan.getCharge(),sessionCorpID);
		   if((!dummPlan.equalsIgnoreCase(plandumm))&&(!(dummCharges.equalsIgnoreCase(Chargesdumm)))){
		   if(i>0 ){
		    	errorMessage("Commission Plan Charge Code and Contract already exist In Driver Commission.");
		    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    	return INPUT;
		    }
		   }
		   if(driverCommissionPlan.getContract()==null){
				  driverCommissionPlan.setContract("");
			   }
		   driverCommissionPlan.setCorpID(sessionCorpID);
		   driverCommissionPlan.setUpdatedOn(new Date());
		   driverCommissionPlan.setUpdatedBy(getRequest().getRemoteUser());
	   }
	   driverCommissionPlan=driverCommissionManager.save(driverCommissionPlan);
	   
	   String key = (isNew) ? "Driver Commission has been added successfully." : "Driver Commission has been updated successfully.";   
       saveMessage(getText(key)); 
	   hitFlag="3";
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	 return SUCCESS; 
     }
   public String chargesList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	      try{
			chargess = driverCommissionManager.findChargeList(sessionCorpID);
			}catch(Exception e){
				e.printStackTrace();
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
   }
   public String searchChargesList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   chargess = driverCommissionManager.getChargeList(driverCommissionPlan.getCharge(),driverCommissionPlan.getDescription(),sessionCorpID);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;
	   
   }
   public String DriverContractList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   chargess = driverCommissionManager.getChargedriverList(chargeValue,sessionCorpID);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;
	   
   }
   public String planSearchList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	   planListValue=driverCommissionManager.getPlanListValue(planValue,sessionCorpID);
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS;
   }
  public String  chargeSearchList(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  chargeListValue=driverCommissionManager.getChargeListValue(planValue,chargeValue,sessionCorpID);
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  return SUCCESS;
   }
  public String driverCommissionSearchList(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  commissionPlan=driverCommissionManager.getCommissionPlanList(sessionCorpID);
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  return SUCCESS;
  }
  public String searchDriverCommission(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  commissionPlan=driverCommissionManager.getCommissionSearchPlanList(driverCommissionPlan.getPlan(),driverCommissionPlan.getCharge(),driverCommissionPlan.getDescription(),driverCommissionPlan.getContract(),sessionCorpID);
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  return SUCCESS;
  }
  public void updateCommissionPercent(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  BigDecimal percent=new BigDecimal(percentValue);
	  driverCommissionManager.updateCommissionPercent(percent,planAmount,planValue,contractValue,chargeValue,sessionCorpID);
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
  }
  public String delDriverCommissionId(){
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	  driverCommissionManager.delDriverCommission(delCommissionId);
	  String key =" Deleted successfully";
	  	saveMessage(getText(key));
	  hitFlag="2";
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	  return SUCCESS;
  }
	public DriverCommissionPlan getDriverCommissionPlan() {
		return driverCommissionPlan;
	}

	public void setDriverCommissionPlan(DriverCommissionPlan driverCommissionPlan) {
		this.driverCommissionPlan = driverCommissionPlan;
	}

	public void setDriverCommissionManager(
			DriverCommissionManager driverCommissionManager) {
		this.driverCommissionManager = driverCommissionManager;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	/*public List getPlanList() {
		return planList;
	}
	public void setPlanList(List planList) {
		this.planList = planList;
	}*/
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}
	public List getContractList() {
		return contractList;
	}
	public void setContractList(List contractList) {
		this.contractList = contractList;
	}
	public List getChargess() {
		return chargess;
	}
	public void setChargess(List chargess) {
		this.chargess = chargess;
	}
	public String getPlanValue() {
		return planValue;
	}
	public void setPlanValue(String planValue) {
		this.planValue = planValue;
	}
	public List getPlanListValue() {
		return planListValue;
	}
	public void setPlanListValue(List planListValue) {
		this.planListValue = planListValue;
	}
	public String getChargeValue() {
		return chargeValue;
	}
	public void setChargeValue(String chargeValue) {
		this.chargeValue = chargeValue;
	}
	public List getChargeListValue() {
		return chargeListValue;
	}
	public void setChargeListValue(List chargeListValue) {
		this.chargeListValue = chargeListValue;
	}
	public List getCommissionPlan() {
		return commissionPlan;
	}
	public void setCommissionPlan(List commissionPlan) {
		this.commissionPlan = commissionPlan;
	}
	public List getChargeList() {
		return chargeList;
	}
	public void setChargeList(List chargeList) {
		this.chargeList = chargeList;
	}
	public String getContractValue() {
		return contractValue;
	}
	public void setContractValue(String contractValue) {
		this.contractValue = contractValue;
	}
	public String getPercentValue() {
		return percentValue;
	}
	public void setPercentValue(String percentValue) {
		this.percentValue = percentValue;
	}
	public String getDelCommissionId() {
		return delCommissionId;
	}
	public void setDelCommissionId(String delCommissionId) {
		this.delCommissionId = delCommissionId;
	}
	public String getDummPlan() {
		return dummPlan;
	}
	public void setDummPlan(String dummPlan) {
		this.dummPlan = dummPlan;
	}
	public String getDummCharges() {
		return dummCharges;
	}
	public void setDummCharges(String dummCharges) {
		this.dummCharges = dummCharges;
	}
	public String getPlanAmount() {
		return planAmount;
	}
	public void setPlanAmount(String planAmount) {
		this.planAmount = planAmount;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Map<String, String> getPlanCommissionList() {
		return planCommissionList;
	}
	public void setPlanCommissionList(Map<String, String> planCommissionList) {
		this.planCommissionList = planCommissionList;
	}
	public List getChargesDescriptionValue() {
		return chargesDescriptionValue;
	}
	public void setChargesDescriptionValue(List chargesDescriptionValue) {
		this.chargesDescriptionValue = chargesDescriptionValue;
	}
	public List getMultiplContracts() {
		return multiplContracts;
	}
	public void setMultiplContracts(List multiplContracts) {
		this.multiplContracts = multiplContracts;
	}
	public String getAmount1() {
		return Amount1;
	}
	public void setAmount1(String amount1) {
		Amount1 = amount1;
	}
	public List getAmtList() {
		return amtList;
	}
	public void setAmtList(List amtList) {
		this.amtList = amtList;
	}
}
