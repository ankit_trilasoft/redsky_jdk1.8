package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.GenericSurveyQuestionDao;
import com.trilasoft.app.model.GenericSurveyQuestion;
import com.trilasoft.app.service.GenericSurveyQuestionManager;


public class GenericSurveyQuestionManagerImpl extends GenericManagerImpl<GenericSurveyQuestion, Long> implements GenericSurveyQuestionManager{
	GenericSurveyQuestionDao genericSurveyQuestionDao;
	
	public GenericSurveyQuestionManagerImpl(GenericSurveyQuestionDao genericSurveyQuestionDao) {
		super(genericSurveyQuestionDao);
		this.genericSurveyQuestionDao = genericSurveyQuestionDao;
	}
	
	public List findRecords(String sessionCorpID){
		return genericSurveyQuestionDao.findRecords(sessionCorpID);
	}
	
	public Map<List, Map> getSurveyUserList(String joType,String routingType,String sessionCorpID,Long sid,Long cid,boolean isBookingAgent,boolean isOriginAgent,boolean isdestinationAgent,String language){
		return genericSurveyQuestionDao.getSurveyUserList(joType, routingType, sessionCorpID, sid, cid,isBookingAgent,isOriginAgent,isdestinationAgent,language);
	}
	
	public String checkForBA(Long sid,String sessionCorpID){
		return genericSurveyQuestionDao.checkForBA(sid,sessionCorpID);
	}
	
	public String checkForOA(Long sid,String sessionCorpID){
		return genericSurveyQuestionDao.checkForOA(sid,sessionCorpID);
	}
	
	public String checkForDA(Long sid,String sessionCorpID){
		return genericSurveyQuestionDao.checkForDA(sid,sessionCorpID);
	}
}
