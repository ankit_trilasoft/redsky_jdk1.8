<%@ include file="/common/taglibs.jsp"%>   
 <%@ taglib prefix="s" uri="/struts-tags" %> 
<head>   
    <title><fmt:message key="inlandAgentDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='inlandAgentDetail.heading'/>"/>   
<style type="text/css">
/* collapse */
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}
</style>
</head> 
 <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
	<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<s:form id="inlandAgentForm" action="saveInlandAgentAjax.html?decorator=popup&popup=true" onsubmit="return submit_form()" method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
	<s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber"/>
	<s:hidden name="serviceOrder.ship"/>
	<s:hidden name="inlandAgent.idNumber" /> 
	<s:hidden name="id" value="<%=request.getParameter("id")%>"/>
	<s:hidden name="inlandAgent.id" value="%{inlandAgent.id}"/> 
	<s:hidden name="shipSize" />
    <s:hidden name="minShip" />
    <s:hidden name="countShip" />
    <s:hidden name="minChild" />
    <s:hidden name="maxChild" />
    <s:hidden name="countChild" />
	<s:hidden name="customerFile.id" />
	<s:hidden name="inlandAgent.corpID" />
	<s:hidden name="inlandAgent.serviceOrderId" value="%{serviceOrder.id}"/>
	<s:hidden name="serviceOrder.sid" value="%{serviceOrder.id}"/>
	<s:hidden name="soId" value="%{serviceOrder.id}"/>
	<s:hidden name="inlandAgent.shipNumber" value="%{serviceOrder.shipNumber}"/>
	<s:hidden name="secondDescription" />    
	<s:hidden name="thirdDescription" />
	<s:hidden name="fourthDescription" />
	<s:hidden name="fifthDescription" />
	<s:hidden name="sixthDescription" />
	<s:hidden name="firstDescription" />
	<s:hidden name="trackingUrl"/>

	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value=""/>
		<div id="Layer4" style="width:100%;">
 			<div id="newmnav" style="!float:left;"> 
 			<ul> 
  				<li id="newmnav1" style="background:#FFF "><a class="current"><span>InLandAgent</span></a></li>   
  			</ul>
			</div>
			<div class="spn">&nbsp;</div>
		</div>
		<div id="Layer1" style="width:100%">
			<div id="content" align="center">
			<div id="liquid-round-top">
    			<div class="top"><span></span></div>
    				<div class="center-content">
					<table class="" cellspacing="1" cellpadding="0" border="0"  width="100%">
						<tbody>
							<tr>
								<td>
									<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0"  width="">
 										<tbody>
 											<tr><td align="left" height="5px"></td></tr> 		  
 											<tr>
												<td align="right" width="126" class="listwhitetext">Vendor&nbsp;ID</td>
												<td align="left" colspan="2"><s:textfield cssClass="input-text"  name="inlandAgent.vendorId" required="true" cssStyle="width:70px" maxlength="10" tabindex="" onchange="checkVendorNameInLand()"/></td>
												<td align="left"width="36">
 													<img class="openpopup" width="17" height="20" onclick="javascript:openWindow('agentPartnersPopUp.html?id=${serviceOrder.id}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=trackingUrl&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=inlandAgent.vendorName&fld_code=inlandAgent.vendorId');" src="<c:url value='/images/open-popup.gif'/>" /> 
												</td>
												<td>&nbsp;</td>
												<td align="right" class="listwhitetext">Vendor&nbsp;Name</td>
												<td align="left">
													<s:textfield cssClass="input-text"  name="inlandAgent.vendorName" id="checkvendorName" required="true" cssStyle="width:235px" maxlength="20" tabindex="" onkeyup="autoCompleterAjaxCallAgentDetails('vendorDivId',event);" onchange="checkVendorNameInLand()"/>
													<div id="vendorDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
												</td>
												<td>&nbsp;</td>
												<td align="right" class="listwhitetext">Grade</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.grade" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onfocus="closeAutoDivId()"/></td>
											</tr>
 										</tbody>
									</table>
									<table width="100%" cellspacing="0" cellpadding="0" style="margin: 0px; padding: 0px;">
										<tr class="subcontenttabChild">
											<td style="border-bottom:1px solid #dfdfdf;"><b>&nbsp;Container&nbsp;and&nbsp;Warehouse</b></td>
										</tr>
									</table>
									<table class="detailTabLabel" border="0" width="">
 										<tbody>
											<tr>
												<td align="right" class="listwhitetext">Container&nbsp;Type</td>
												<td align="left"><s:select  id="cntnrNumber" cssClass="list-menu" key="inlandAgent.containerType" headerKey="" headerValue="" list="%{containerNumberList}"   cssStyle="width:110px"  tabindex="" /></td>
												<td align="right" class="listwhitetext">Last&nbsp;Free&nbsp;Date&nbsp;with&nbsp;Container</td>
												<c:if test="${not empty inlandAgent.freeDatewithContainer}">
												<s:text id="inlandfreeDatewithContainerFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.freeDatewithContainer"/></s:text>
												<td width="60"><s:textfield cssClass="input-text" id="freeDatewithContainer" name="inlandAgent.freeDatewithContainer" value="%{inlandfreeDatewithContainerFormattedValue}" tabindex=""  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="freeDatewithContainer-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.freeDatewithContainer}">
												<td width="60"><s:textfield cssClass="input-text" id="freeDatewithContainer" name="inlandAgent.freeDatewithContainer" required="true" cssStyle="width:65px" maxlength="11" tabindex="" onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="freeDatewithContainer-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<td align="right" colspan="2"class="listwhitetext">Chasis&nbsp;Provided</td>
												<td align="left" colspan="3">
													<c:set var="isCPFlag" value="false"/>
													<c:if test="${inlandAgent.chasisProvide}">
															<c:set var="isCPFlag" value="true"/>
													</c:if>
													<s:checkbox key="inlandAgent.chasisProvide" value="${isCPFlag}" fieldValue="true"   tabindex="0" />
												</td>
												</tr>
											<tr>
												<td align="right" class="listwhitetext">Container&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.containerDays" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onchange="onlyNumeric(this); isNumeric(this);"/></td>
												<td align="right" class="listwhitetext">Free&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.containerFreeDays" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onchange="onlyNumeric(this); isNumeric(this);"/></td>
												<td align="left" class="listwhitetext">*&nbsp;5&nbsp;Per&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.containerFreeDaysSubTotal" id="containerFreeDaysSubTotal" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter(this);totalSumWithInLandAgent()"/></td>
												<td align="center" class="listwhitetext">=</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.containerFreeDaysTotal" required="true" cssStyle="width:65px" maxlength="10" tabindex="" readonly="true"/></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Chasis&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.chasisDays" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onchange="onlyNumeric(this); isNumeric(this);"/></td>
												<td align="right" class="listwhitetext">Free&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.chasisfreeDays" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onchange="onlyNumeric(this); isNumeric(this);"/></td>
												<td align="left" class="listwhitetext">*&nbsp;5&nbsp;Per&nbsp;Days</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.chasisfreeDaysSubTotal" id="chasisfreeDaysSubTotal" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter(this);totalSumWithInLandAgent()"/></td>
												<td align="center" class="listwhitetext">=</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.chasisfreeDaysTotal" required="true" cssStyle="width:65px" maxlength="10" tabindex="" readonly="true"/></td>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Other&nbsp;Charges&nbsp;Description</td>
												<td align="left" colspan="5"><s:textarea cssClass="textarea"  name="inlandAgent.otherChargesDesc" required="true" cssStyle="width:422px;height:25px;" tabindex="" /></td>
												<td align="center" class="listwhitetext">=</td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.otherChargesAmount" required="true" cssStyle="width:65px" maxlength="10" tabindex="" onkeydown="return onlyFloatNumsAllowed(event)" onchange="onlyFloat2(this);ValidationSpecialCharcter(this);totalSumWithInLandAgent()"/></td>
											</tr>
											<tr>
												<td colspan="5"></td>
												<td colspan="3" align="right" class="listwhitetext"><div style="width:65%;border-top:1px solid #219dd1;" class="vertlinedata"></div></td>
											</tr>
											<tr>
												<td colspan="5"></td>
												<td align="right" colspan="2" class="listwhitetext"><b>Total</b></td>
												<td align="left"><s:textfield cssClass="input-text"  name="inlandAgent.inLandTotalAmount" required="true" cssStyle="width:65px" maxlength="10" tabindex="" readonly="true"/></td>
											</tr>
										</tbody>
										</table>
										<table class="detailTabLabel" border="0" width="100%">
										  <tbody>
											<tr>
												<td colspan="12" align="right" class="listwhitetext"><div style="width:100%;" class="vertlinedata"></div></td>
											</tr>
										</tbody>
										</table>
										<table class="detailTabLabel" border="0" width="">
										  <tbody>
											<tr>
												<td align="right" class="listwhitetext">Sent&nbsp;Trucking&nbsp;Order&nbsp;For&nbsp;Pickup</td>
												<c:if test="${not empty inlandAgent.sentTOFPickUp}">
													<s:text id="sentTOFPickUpDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.sentTOFPickUp"/></s:text>
													<td><s:textfield cssClass="input-text" id="sentTOFPickUp" name="inlandAgent.sentTOFPickUp" value="%{sentTOFPickUpDateFormattedValue}" tabindex=""  required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="sentTOFPickUp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.sentTOFPickUp}">
													<td><s:textfield cssClass="input-text" id="sentTOFPickUp" name="inlandAgent.sentTOFPickUp" required="true" cssStyle="width:72px" maxlength="11" tabindex="" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="sentTOFPickUp-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>		
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Last&nbsp;Free&nbsp;Date&nbsp;At&nbsp;Port</td>	
												<c:if test="${not empty inlandAgent.freeDateAtPort}">
													<s:text id="freeDateAtPortFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.freeDateAtPort"/></s:text>
													<td><s:textfield cssClass="input-text" id="freeDateAtPort" name="inlandAgent.freeDateAtPort" value="%{freeDateAtPortFormattedValue}" required="true" cssStyle="width:72px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="freeDateAtPort-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.freeDateAtPort}">
													<td><s:textfield cssClass="input-text" id="freeDateAtPort" name="inlandAgent.freeDateAtPort" required="true" cssStyle="width:72px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="freeDateAtPort-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>		
												<td align="right" class="listwhitetext" colspan="5" width="350">Out&nbsp;Of&nbsp;Port</td>
												<c:if test="${not empty inlandAgent.outOfPort}">
													<s:text id="outOfPortFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.outOfPort"/></s:text>
													<td width="60"><s:textfield cssClass="input-text" id="outOfPort" name="inlandAgent.outOfPort" value="%{outOfPortFormattedValue}" required="true" cssStyle="width:65px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="outOfPort-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.outOfPort}">
													<td width=""><s:textfield cssClass="input-text" id="outOfPort" name="inlandAgent.outOfPort" required="true" cssStyle="width:65px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="outOfPort-trigger"   style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
											</tr>
											<tr>
												<td align="right" class="listwhitetext">Requested&nbsp;Pickup&nbsp;Date</td>	
												<c:if test="${not empty inlandAgent.requestPickDate}">
													<s:text id="requestPickDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.requestPickDate"/></s:text>
													<td><s:textfield cssClass="input-text" id="requestPickDate" name="inlandAgent.requestPickDate" value="%{requestPickDateFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="requestPickDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.requestPickDate}">
													<td><s:textfield cssClass="input-text" id="requestPickDate" name="inlandAgent.requestPickDate" required="true" cssStyle="width:72px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="requestPickDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>		
												<td align="right" class="listwhitetext" colspan="5" width="">Actual&nbsp;Pickup&nbsp;Date</td>
												<c:if test="${not empty inlandAgent.actualPickDate}">
													<s:text id="actualPickDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.actualPickDate"/></s:text>
													<td width="60"><s:textfield cssClass="input-text" id="actualPickDate" name="inlandAgent.actualPickDate" value="%{actualPickDateFormattedValue}" required="true" tabindex=""  cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="actualPickDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.actualPickDate}">
													<td width=""><s:textfield cssClass="input-text" id="actualPickDate" name="inlandAgent.actualPickDate" required="true" cssStyle="width:65px" tabindex=""  maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="actualPickDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
											</tr>
										
											<tr>
												<td align="right" class="listwhitetext">Requested&nbsp;Drop&nbsp;Date</td>	
												<c:if test="${not empty inlandAgent.requestDropDate}">
													<s:text id="requestDropDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.requestDropDate"/></s:text>
													<td><s:textfield cssClass="input-text" id="requestDropDate" name="inlandAgent.requestDropDate" value="%{requestDropDateFormattedValue}" tabindex=""  required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="requestDropDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.requestDropDate}">
													<td><s:textfield cssClass="input-text" id="requestDropDate" name="inlandAgent.requestDropDate" required="true" cssStyle="width:72px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)"/></td>
													<td><img id="requestDropDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
												</c:if>		
												<td align="right" class="listwhitetext" colspan="5" width="">Actual&nbsp;Drop&nbsp;Date</td>
												<c:if test="${not empty inlandAgent.actualDropDate}">
													<s:text id="actualDropDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.actualDropDate"/></s:text>
													<td width="60"><s:textfield cssClass="input-text" id="actualDropDate" name="inlandAgent.actualDropDate" value="%{actualDropDateFormattedValue}" tabindex=""  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="actualDropDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
												<c:if test="${empty inlandAgent.actualDropDate}">
													<td width=""><s:textfield cssClass="input-text" id="actualDropDate" name="inlandAgent.actualDropDate" required="true" cssStyle="width:65px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)" /></td>
													<td><img id="actualDropDate-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
												</c:if>
										</tr>
										<tr>
											<td align="right" class="listwhitetext">Sent&nbsp;Trucking&nbsp;Order&nbsp;For&nbsp;Return</td>	
											<c:if test="${not empty inlandAgent.sentTruckingOrderforReturn}">
												<s:text id="sentTruckingOrderforReturnFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.sentTruckingOrderforReturn"/></s:text>
												<td><s:textfield cssClass="input-text" id="sentTruckingOrderforReturn" name="inlandAgent.sentTruckingOrderforReturn" value="%{sentTruckingOrderforReturnFormattedValue}" tabindex=""  required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="sentTruckingOrderforReturn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
											</c:if>
											<c:if test="${empty inlandAgent.sentTruckingOrderforReturn}">
												<td><s:textfield cssClass="input-text" id="sentTruckingOrderforReturn" name="inlandAgent.sentTruckingOrderforReturn" required="true" cssStyle="width:72px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="sentTruckingOrderforReturn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
											</c:if>		
											<td align="right" class="listwhitetext" colspan="5" width="">Return&nbsp;To&nbsp;Port&nbsp;Actual</td>
											<c:if test="${not empty inlandAgent.returntoPortActual}">
											<s:text id="returntoPortActualFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.returntoPortActual"/></s:text>
												<td width="60"><s:textfield cssClass="input-text" id="returntoPortActual" name="inlandAgent.returntoPortActual" value="%{returntoPortActualFormattedValue}" tabindex=""  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
												<td><img id="returntoPortActual-trigger"   style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											</c:if>
											<c:if test="${empty inlandAgent.returntoPortActual}">
												<td width=""><s:textfield cssClass="input-text" id="returntoPortActual" name="inlandAgent.returntoPortActual" required="true" cssStyle="width:65px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)" /></td>
												<td><img id="returntoPortActual-trigger"   style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											</c:if>
										</tr>
										<tr>
											<td align="right" class="listwhitetext">Live&nbsp;Load</td>	
											<c:if test="${not empty inlandAgent.liveLoadDate}">
												<s:text id="liveLoadFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.liveLoadDate"/></s:text>
												<td><s:textfield cssClass="input-text" id="liveLoad" name="inlandAgent.liveLoadDate" value="%{liveLoadFormattedValue}" required="true" cssStyle="width:72px" maxlength="11" tabindex=""  onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="liveLoad-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="trackingStatusForm_trackingStatus_liveLoad" HEIGHT=20 WIDTH=20/></td>
											</c:if>
											<c:if test="${empty inlandAgent.liveLoadDate}">
												<td><s:textfield cssClass="input-text" id="liveLoad" name="inlandAgent.liveLoadDate" tabindex=""  required="true" cssStyle="width:72px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td>
												<td><img id="liveLoad-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png"  HEIGHT=20 WIDTH=20/></td>
											</c:if>		
											<td align="right" class="listwhitetext" colspan="5" width="">Live&nbsp;Unload</td>
											<c:if test="${not empty inlandAgent.liveUnLoadDate}">
												<s:text id="liveUnLoadFormattedValue" name="${FormDateValue}"><s:param name="value" value="inlandAgent.liveUnLoadDate"/></s:text>
												<td width="60"><s:textfield cssClass="input-text" id="liveUnLoad" name="inlandAgent.liveUnLoadDate" value="%{liveUnLoadFormattedValue}" tabindex=""  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
												<td><img id="liveUnLoad-trigger"   style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											</c:if>
											<c:if test="${empty inlandAgent.liveUnLoadDate}">
												<td width=""><s:textfield cssClass="input-text" id="liveUnLoad" name="inlandAgent.liveUnLoadDate" tabindex=""  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" /></td>
												<td><img id="liveUnLoad-trigger"   style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
											</c:if>
										</tr>
									</table>
										<s:submit cssClass="cssbuttonA" key="button.save" method="save" cssStyle="width: 55px; margin-left: 144px; margin-top: 10px;" tabindex="9" />   
										<s:reset cssClass="cssbutton1" key="Reset" cssStyle="width:55px; height:25px" tabindex="10"/>
								</td>									
								</tr>
							</tbody>
						</table>	

				<table>
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${inlandAgent.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="inlandAgent.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td><fmt:formatDate value="${inlandAgent.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty inlandAgent.id}">
								<s:hidden name="inlandAgent.createdBy"/>
								<td><s:label name="createdBy" value="%{inlandAgent.createdBy}"/></td>
							</c:if>
							<c:if test="${empty inlandAgent.id}">
								<s:hidden name="inlandAgent.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${inlandAgent.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="inlandAgent.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${inlandAgent.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty inlandAgent.id}">
							<s:hidden name="inlandAgent.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{inlandAgent.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty inlandAgent.id}">
							<s:hidden name="inlandAgent.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>				
					
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
</div>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
 </s:form>
<script type="text/javascript">
   setCalendarFunctionality();
</script>
<script type="text/javascript">
function vendorCodeWinOpen(){
	 	var sid=document.forms['inlandAgentForm'].elements['serviceOrder.id'].value;
	 	 openWindow('agentPartners.html?&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=inlandAgent.vendorName&fld_code=inlandAgent.vendorId');
	 	  document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].select();	
	 }
	 
var http2 = getHTTPObject();
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)  {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)  {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }  }
    return xmlhttp;
}
	 
function checkVendorNameInLand(){ 
    var vendorId = document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value;
	vendorId=vendorId.trim();
	if(vendorId == ''){
	
		document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value="";
		 }
	if(vendorId != ''){ 
    document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value=vendorId;
    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2In;
     http2.send(null);
    }  }
function handleHttpResponse2In(){
	if (http2.readyState == 4){
            var results = http2.responseText
            results = results.trim();
            var res = results.split("#"); 
	           if(res.length >= 2){ 
	           		if(res[2] == 'Approved'){
	           			document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value = res[1];
	           			document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].select(); 
		                	          	
	           		}else{
	           			alert("Vendor code is not approved." ); 
					    document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value="";
					    document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value="";
					    document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].select();
	           		}
            }else{
                 alert("Vendor code not valid." );
                 document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value="";
			 	 document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value="";
			 	 document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].select();
		   } } }
	 
function onlyFloatNumsAllowed(evt)
{
	
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==35) || (keyCode==36); 
}

function onlyFloat2(e) {
    var t;
    var n = e.value;
    var r = 0;
    for (t = 0; t < n.length; t++) {
        var i = n.charAt(t);
        if (i == ".") {
            r = r + 1
        }
        if ((i < "0" || i > "9") && i != "." || r > "1") {
            document.getElementById(e.id).value ="";
            alert ("Special characters are not allowed.");
            document.getElementById(e.id).select();
            return false;
        }
    }
    return true;
}


function ValidationSpecialCharcter(e){
	var iChars = "!`@#$%^&*()+=-[]\\\';,/{}|\":<>?~_";   

	var data = e.value;

	for (var i = 0; i < data.length; i++)

	{      
	    if (iChars.indexOf(data.charAt(i)) != -1)
	    {    
	    alert ("Special characters are not allowed.");
	    document.getElementById(e.id).value ="";
	    return false; 
	    } 
	} 
	}


function totalSumWithInLandAgent(){
    var containerDaysTotal =document.forms['inlandAgentForm'].elements['inlandAgent.containerFreeDaysSubTotal'].value;
    var chasisDaysTotal =document.forms['inlandAgentForm'].elements['inlandAgent.chasisfreeDaysSubTotal'].value;
    var otherAmnt =document.forms['inlandAgentForm'].elements['inlandAgent.otherChargesAmount'].value;
    if (containerDaysTotal == ""){
    	containerDaysTotal = 0;
    }
    if (chasisDaysTotal == ""){
    	chasisDaysTotal = 0;
    }
    if (otherAmnt == ""){
    	otherAmnt = 0;
    }
    document.forms['inlandAgentForm'].elements['inlandAgent.containerFreeDaysTotal'].value=containerDaysTotal;
    document.forms['inlandAgentForm'].elements['inlandAgent.chasisfreeDaysTotal'].value=chasisDaysTotal;
    // Add them together and display
    var sum = parseFloat(containerDaysTotal) +parseFloat(chasisDaysTotal)+parseFloat(otherAmnt);
    document.forms['inlandAgentForm'].elements['inlandAgent.inLandTotalAmount'].value=parseFloat(sum);
}

function autoCompleterAjaxCallAgentDetails(divid,evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	if(evt.keyCode != 9 && evt.keyCode != 46) {
	var partnerName = document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value;
	var u=document.getElementById("checkvendorName").value;
	if(u.length<=1)
		{
		document.getElementById(divid).style.display = "none";
		document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value="";
		}
		var partnerCode ="";
		var idval="";
		if(partnerName.length>=3){
           $.get("agentDetailAutocompleteAjax.html?ajax=1&decorator=simple&popup=true", {
           	searchName:partnerName,
           	partnerType: "AG",
           	fieldType:"agentName",
           	partnerCode:partnerCode,
           	autocompleteDivId: divid
           }, function(idval) {
               document.getElementById(divid).style.display = "block";
               $("#" + divid).html(idval)
           })
       } else {
           document.getElementById(divid).style.display = "none"
       }
}}

function copyAgentDetails(partnerCode,partnerName,autocompleteDivId){
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value=partnerName;
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value=partnerCode;
	document.getElementById(autocompleteDivId).style.display = "none";
	
}

function closeMyAgentDiv(partnerCode,partnerName,autocompleteDivId){
	document.getElementById(autocompleteDivId).style.display = "none";
	if(partnerCode==""){
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value="";
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value="";
	}
	}
	
function closeAutoDivId(){
	document.getElementById("vendorDivId").style.display = "none";
	if(document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value==""){
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorName'].value="";
	document.forms['inlandAgentForm'].elements['inlandAgent.vendorId'].value="";
	}
}

function submit_form()
{
  if (form_submitted)
  {
    alert ("Your form has already been submitted. Please wait...");
    return false;
  }
  else
  {
    form_submitted = true;
    return true;
  }
}

<c:if test="${hitFlag=='1'}">
	window.close();
	try {
	window.opener.getInlandAgentDetails();
}catch(e){
	window.opener.document.forms[0].submit();
}
</c:if>
</script>
