
package com.trilasoft.app.service;


import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.AgentRequestReason;
import com.trilasoft.app.model.PartnerPublic;

public interface AgentRequestReasonManager extends GenericManager<AgentRequestReason, Long> {

	public Map<String, String> getCountryWithBranch(String parameter,String sessionCorpID);
	public List getPartnerPublicList(String corpID,String lastName,String aliasName, String countryCode, String country, String stateCode, String status, Boolean isAgt, Boolean isIgnoreInactive, String fidiNumber,String OMNINumber,String AMSANumber,String WERCNumber,String IAMNumber,String utsNumber ,String eurovanNetwork ,String PAIMA,String LACMA,String counter); 
	public List getAgentRequestDetailList(String sessionCorpID,
			String lastName, String aliasName, String billingCountry,
			String billingCity, String billingState, String status,
			String billingEmail,boolean isAgt,String billingZip,String billingAddress1);
	public String updateAgentRecord(String sessionCorpID,String lastName, String aliasName, String billingCountry,String billingCity,String billingState,String status,String billingEmail,boolean isAgt,String billingZip,String billingAddress1,String updatedBy,Long id,String createdBy);
	
	public String  updateAgentRejected(Long id,String updatedBy);
	 public List checkById(Long id) ;
public List getAgentRequestList(String partnerType, String corpID, String lastName,String aliasName, String partnerCode, String countryCode, String country, String stateCode, String status,Boolean isAgt, Boolean isIgnoreInactive);
public String getUser(String createdBy);
public AgentRequestReason getByID(Long id) ;
public List findAgentFeedbackList(Long id);
}
