<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>
	<authz:authorize ifAnyGranted="ROLE_SALE" >
    	<title><fmt:message key="myCalendarDetail.title"/></title>   
    	<meta name="heading" content="<fmt:message key='myCalendarDetail.heading'/>"/>  
    </authz:authorize>
    <authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
    <c:choose>
	<c:when test="${calendarFile.userName == calendarFile.corpID}">
    	<title>Company Event Detail</title>   
    	<meta name="heading" content="Company Event Detail"/>   
 	</c:when>
 	<c:otherwise>
 		<title><fmt:message key="myCalendarDetail.title"/></title>   
    	<meta name="heading" content="<fmt:message key='myCalendarDetail.heading'/>"/>  
    </c:otherwise>
 	</c:choose>
 	</authz:authorize>
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	</style>
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>

</head>
<s:form id="myCalendarForm" action="saveMyCalendars" method="post" validate="true" onsubmit="return calcDays();"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:choose>
<c:when test="${gotoPageString == 'gototab.calendarFile' }">
   <c:redirect url="/myCalendar.html"/>
</c:when>
<c:otherwise>
</c:otherwise>
</c:choose>
<s:hidden name="calendarFile.id" />
<s:hidden name="calendarFile.corpID" />
<s:hidden name="id1" value="<%=request.getParameter("id1") %>" />
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
 <s:hidden id="checkSurveyDaysClick" name="checkSurveyDaysClick" />
<authz:authorize ifAnyGranted="ROLE_SALE">
<s:hidden name="consult"/>
</authz:authorize>
<s:hidden name="surveyFrom" />
<s:hidden name="surveyTo" />
<s:hidden name="surveyCity" />
<s:hidden name="surveyJob" />
<s:hidden id="surDays" name="calendarFile.surveyDays" onselect="calcDays();"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<%-- 
<div id="newmnav">
		    <ul>
			  <li id="newmnav1" style="background:#FFF"><a class="current" href="editMyMessage.html" ><span>My Calendar Detail<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			  <li><a href="myCalendar.html"><span>My Calendar List</span></a></li>
			</ul>
		</div><div class="spn">&nbsp;</div><br> --%>
<div id="Layer1" onkeydown="changeStatus();"  style="width:100%">
<div id="newmnav">


<ul>
<authz:authorize ifAnyGranted="ROLE_SALE">
	<c:choose>
	<c:when test="${empty calendarFile.id}" >
  	 <li id="newmnav1"  style="cursor:pointer" style="background:#FFF "><a class="current"  ><span  style="cursor:pointer">Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
  	 <li><a onmouseover="completeTimeString(); " onclick="setReturnString('gototab.calendarFile');return IsValidTime('autoSave');"><span>Event List</span></a></li>
</c:when>
<c:otherwise>
  		 <li id="newmnav1"  style="cursor:pointer" style="background:#FFF "><a class="current"  ><span  style="cursor:pointer">Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    	 <li><a onmouseover="completeTimeString(); " onclick="setReturnString('gototab.calendarFile');return IsValidTime('autoSave');"><span>Event List</span></a></li>
</c:otherwise>
</c:choose>
</authz:authorize>
<authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
  <c:choose>
	  <c:when test="${empty calendarFile.id}" >
 		 <li id="newmnav1"  style="cursor:pointer" style="background:#FFF "><a class="current" ><span  style="cursor:pointer">Company Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
 		 <li><a onmouseover="completeTimeString(); " onclick="setReturnString('gototab.calendarFile');return IsValidTime('autoSave');"><span>Company Event List</span></a></li>
  </c:when>
  <c:otherwise>
 		 <li id="newmnav1"  style="cursor:pointer" style="background:#FFF "><a class="current"  ><span  style="cursor:pointer">Company Events<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
    	 <li><a onmouseover="completeTimeString(); " onclick="setReturnString('gototab.calendarFile');return IsValidTime('autoSave');"><span>Company Event List</span></a></li>
</c:otherwise>
</c:choose>
</authz:authorize>
<li><a onmouseover="checkHasValuation()" onclick="window.open('auditList.html?id=${calendarFile.id}&tableName=calendarfile&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
</ul>
</div><div class="spn">&nbsp;</div>
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0">
	<tbody>
		<tr>
		<td>
		
			<c:set var="fullDayFlag" value="false"/>
			<c:if test="${calendarFile.fullDay}">
				<c:set var="fullDayFlag" value="true"/>
			</c:if>
			<table><tbody>
			<tr>
				<td align="right" class="listwhitetext"><fmt:message key='calendarFile.userName'/></td>
				<authz:authorize ifAnyGranted="ROLE_SALE">
				<s:hidden name="calendarFile.category"/>
				<c:if test="${empty calendarFile.id}">
					<td class="listwhitetext"  colspan="5"><s:textfield cssClass="input-text"  name="calendarFile.userName" readonly="true" /></td>
				</c:if>
				<c:if test="${not empty calendarFile.id}">
					<td class="listwhitetext"  colspan="5"><s:textfield cssClass="input-text"  name="calendarFile.userName" readonly="true" /></td>
				</c:if>
				</authz:authorize>
				<authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
				<c:if test="${empty calendarFile.id}">
					<td class="listwhitetext" colspan="5"><s:textfield cssClass="input-text"  name="calendarFile.userName" value="%{calendarFile.corpID}" maxlength="82"/></td>
				</c:if>
				<c:if test="${not empty calendarFile.id}">
					<td class="listwhitetext" colspan="5"><s:textfield cssClass="input-text"  name="calendarFile.userName" readonly="true" /></td>
				</c:if>
				</authz:authorize>
				</tr>
				<tr>
<c:if test="${not empty calendarFile.id}">
<td align="right" class="listwhitetext">Date<font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyDate}">
<s:text id="calendarFileSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyDate"/></s:text>
<td><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" value="%{calendarFileSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td><td><img id="surveyDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyDate}">
<td><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onselect="calcDays()" /></td><td><img id="surveyDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
<s:hidden name="calendarFile.surveyTillDate" value="%{calendarFile.surveyTillDate}"/>
</c:if>



<c:if test="${empty calendarFile.id}">
<td align="right" class="listwhitetext"><fmt:message key='calendarFile.surveyDate'/><font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyDate}">
<s:text id="calendarFileSurveyDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyDate"/></s:text>
<td><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" value="%{calendarFileSurveyDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="fillSurveyToDate();calcDays();findSurveyDay();"/></td><td><img id="surveyDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyDate}">
<td><s:textfield cssClass="input-text" id="surveyDate" name="calendarFile.surveyDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onselect="fillSurveyToDate();calcDays();findSurveyDay();" /></td><td><img id="surveyDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
<td align="right" style="width:90px;" class="listwhitetext"><fmt:message key='calendarFile.surveyTillDate'/><font color="red" >*</font></td>
<c:if test="${not empty calendarFile.surveyTillDate}">
<s:text id="calendarFileSurveyTillDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="calendarFile.surveyTillDate"/></s:text>
<td><s:textfield cssClass="input-text" id="surveyTillDate" name="calendarFile.surveyTillDate" value="%{calendarFileSurveyTillDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays();findSurveyDay();"/></td>
<td><img id="surveyTillDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
<c:if test="${empty calendarFile.surveyTillDate}">
<td><s:textfield cssClass="input-text" id="surveyTillDate" name="calendarFile.surveyTillDate" required="true" cssStyle="width:65px" maxlength="11" readonly="true"  onkeydown="return onlyDel(event,this)" onchange="calcDays();findSurveyDay();"/></td><td><img id="surveyTillDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays();"/></td>
</c:if>
</c:if>
</tr>
<tr>
<td align="right" class="listwhitetext" >Full&nbsp;Day</td>
	<td colspan="5"><s:checkbox key="calendarFile.fullDay" value="${fullDayFlag}" cssStyle="margin-left:0px;" fieldValue="true" onclick="findDefaultTimeHours(this);"/></td>
			
			</tr>
			<tr>
				<td class="listwhitetext" align="right"><fmt:message key='calendarFile.fromTime'/></td>
				<td class="listwhitetext" width="60px"><s:textfield cssStyle="text-align:right;width:65px;" cssClass="input-text"  name="calendarFile.fromTime" size="7" maxlength="5" onchange = "completeTimeString();"/></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;"></td>
				<td class="listwhitetext" align="right" width="40px"><fmt:message key='calendarFile.toTime'/></td>
				<td class="listwhitetext" width="20px"><s:textfield cssStyle="text-align:right;width:65px;" cssClass="input-text"  name="calendarFile.toTime" size="7" maxlength="5" onchange = "completeTimeString();"/></td><td><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;"></td>
			</tr>
			<tr>
				<td class="listwhitetext" align="right" style="width:100px;"><fmt:message key='calendarFile.activityName'/><font color="red" >*</font></td>
				<td class="listwhitetext" colspan="4"><s:textfield cssClass="input-text"  name="calendarFile.activityName" size="20" maxlength="50" /></td>
			</tr>
		</tbody>
		</table>		
		</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>
<table>
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
							<fmt:formatDate var="calendarFileCreatedOnFormattedValue" value="${calendarFile.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="calendarFile.createdOn" value="${calendarFileCreatedOnFormattedValue}" />
							<td><fmt:formatDate value="${calendarFile.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdBy' /></b></td>
							<c:if test="${not empty calendarFile.id}">
								<s:hidden name="calendarFile.createdBy"/>
								<td style="width:85px"><s:label name="createdBy" value="%{calendarFile.createdBy}"/></td>
							</c:if>
							<c:if test="${empty calendarFile.id}">
								<s:hidden name="calendarFile.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
							<fmt:formatDate var="calendarFileUpdatedOnFormattedValue" value="${calendarFile.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="calendarFile.updatedOn" value="${calendarFileUpdatedOnFormattedValue}" />
						<td><fmt:formatDate value="${calendarFile.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
						
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedBy' /></b></td>
							<c:if test="${not empty calendarFile.id}">
								<s:hidden name="calendarFile.updatedBy"/>
								<td style="width:85px"><s:label name="updatedBy" value="%{calendarFile.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty calendarFile.id}">
								<s:hidden name="calendarFile.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>

						</tr>
					</tbody>
				</table> 
<authz:authorize ifAnyGranted="ROLE_SALE">
	<s:submit cssClass="cssbutton" id="once" cssStyle="width:55px; height:25px" method="saveCompanyEvent" key="button.save" onclick="fillSurveyToDate();completeTimeString(); return IsValidTime('noSave');javascript:document.getElementById('once').disabled=true"/>  
	<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px;margin-left:10px;" key="Reset"/>
</authz:authorize>
<authz:authorize ifAnyGranted="ROLE_ADMIN" ifNotGranted="ROLE_SALE" >
	<c:choose>
		<c:when test="${(calendarFile.userName == surveyCorpID)}">
		<s:submit cssClass="cssbutton" id="once" cssStyle="width:55px; height:25px" method="saveCompanyEvent" key="button.save" onclick="fillSurveyToDate();completeTimeString(); return IsValidTime('noSave');javascript:document.getElementById('once').disabled=true"/>  
			<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px;margin-left:10px;" key="Reset"/>
		</c:when>
		<c:otherwise>
		<s:submit cssClass="cssbutton" id="once" cssStyle="width:55px; height:25px" method="saveCompanyEvent" key="button.save" onclick="fillSurveyToDate();completeTimeString(); return IsValidTime('noSave');javascript:document.getElementById('once').disabled=true"/>  
	   	<s:reset type="button" cssClass="cssbutton1" cssStyle="width:55px; height:25px;margin-left:10px;" key="Reset"/>
	    </c:otherwise>
	</c:choose>
</authz:authorize>
</s:form>   
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
	
<SCRIPT LANGUAGE="JavaScript">
	function completeTimeString() {
	
		stime1 = document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value;
		stime2 = document.forms['myCalendarForm'].elements['calendarFile.toTime'].value;
		if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
			if(stime1.length==1 || stime1.length==2){
				if(stime1.length==2){
					document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = stime1 + ":00";
				}
				if(stime1.length==1){
					document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = "0" + stime1 + ":00";
				}
			}else{
				document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = stime1 + "00";
			}
		}else{
			if(stime1.indexOf(":") == -1 && stime1.length==3){
				document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
			}
			if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
				document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = stime1.substring(0,2) + ":" + stime1.substring(2,4);
			}
			if(stime1.indexOf(":") == 1){
				document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = "0" + stime1;
			}
		}
		if(stime2.substring(stime2.indexOf(":")+1,stime2.length) == "" || stime2.length==1 || stime2.length==2){
			if(stime2.length==1 || stime2.length==2){
				if(stime2.length==2){
					document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = stime2 + ":00";
				}
				if(stime2.length==1){
					document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = "0" + stime2 + ":00";
				}
			}else{
				document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = stime2 + "00";
			}
		}else{
			if(stime2.indexOf(":") == -1 && stime2.length==3){
				document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = "0" + stime2.substring(0,1) + ":" + stime2.substring(1,stime2.length);
			}
			if(stime2.indexOf(":") == -1 && (stime2.length==4 || stime2.length==5) ){
				document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = stime2.substring(0,2) + ":" + stime2.substring(2,4);
			}
			if(stime2.indexOf(":") == 1){
				document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = "0" + stime2;
			}
		}
	}

</SCRIPT>
<SCRIPT LANGUAGE="JavaScript">
function IsValidTime(temp) {
var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
var timeStr = document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value;
var matchArray = timeStr.match(timePat);
if (matchArray == null) {
alert("Time is not in a valid format. Please use HH:MM format");
//document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.fromTime'].focus();
return false;
}
hour = matchArray[1];
minute = matchArray[2];
second = matchArray[4];
ampm = matchArray[6];

if (second=="") { second = null; }
if (ampm=="") { ampm = null }

if (hour < 0  || hour > 23) {
alert("'Begin Hours' time must between 0 to 23 (Hrs)");
//document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.fromTime'].select();
return false;
}
if (minute<0 || minute > 59) {
alert ("'Begin Hours' time must between 0 to 59 (Min)");
//document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.fromTime'].select();
return false;
}
if (second != null && (second < 0 || second > 59)) {
alert ("'Begin Hours' time must between 0 to 59 (Sec)");
//document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.fromTime'].select();
return false;
}

// **************Check for Survey Time2*************************

var time2Str = document.forms['myCalendarForm'].elements['calendarFile.toTime'].value;
var matchTime2Array = time2Str.match(timePat);
if (matchTime2Array == null) {
alert("Time is not in a valid format. please Use HH:MM format");
//document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.toTime'].focus();
return false;
}
hourTime2 = matchTime2Array[1];
minuteTime2 = matchTime2Array[2];
secondTime2 = matchTime2Array[4];
ampmTime2 = matchTime2Array[6];

if (hourTime2 < 0  || hourTime2 > 23) {
alert("'End Hours' time must between 0 to 23 (Hrs)");
//document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.toTime'].focus();
return false;
}
if (minuteTime2<0 || minuteTime2 > 59) {
alert ("'End Hours' time must between 0 to 59 (Min)");
//document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.toTime'].focus();
return false;
}
if (secondTime2 != null && (secondTime2 < 0 || secondTime2 > 59)) {
alert ("'End Hours' time must between 0 to 59 (Sec)");
//document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = '00:00';
document.forms['myCalendarForm'].elements['calendarFile.toTime'].focus();
return false;
}

return checkTime(temp);
}

function checkTime(temp)
{
	var tim1=document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value;
	var tim2=document.forms['myCalendarForm'].elements['calendarFile.toTime'].value;
	time1=tim1.replace(":","");
	time2=tim2.replace(":","");
	if(time1 > time2){
		alert("Begin hours should be less than End hours");
		//document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = '00:00';
		document.forms['myCalendarForm'].elements['calendarFile.fromTime'].select();
		return false;
	}else{
		return checkActivityName(temp);
	}
}

function checkDateField(temp){
	if(document.forms['myCalendarForm'].elements['calendarFile.surveyDate'].value == ""){
 		alert("Select date to continue.....");
 		document.forms['myCalendarForm'].elements['calendarFile.surveyDate'].select();
 		return false;
 	}
 	if(temp=='autoSave')
 	{
	 	 var date2 = document.forms['myCalendarForm'].elements['calendarFile.surveyDate'].value;	 
	  	 var date1 = document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value; 
 		 var mySplitResult = date1.split("-");
		 var day = mySplitResult[0];
		 var month = mySplitResult[1];
         var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value = daysApart; 
  if(daysApart<0)
  {
    alert("From Date should be less than To Date");
    document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value='';
    document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value='';
    return false;
  }
  if(document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value=='NaN')
   {
     document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value = '';
   } 
    ////document.forms['myCalendarForm'].getElementById("surveyDays").focus();
 	document.forms['myCalendarForm'].elements['checkSurveyDaysClick'].value = '';
 }
 	if(temp=='autoSave'){
		autoSaveFunc('none');
	}
}
function checkActivityName(temp){
	if(document.forms['myCalendarForm'].elements['calendarFile.activityName'].value == ""){
 		alert("Enter Event Name to continue.....");
 		return false;
 		}else{
 		return checkDateField(temp);
 	}
}
function getEventListBySurvey(){
		
		document.forms['myCalendarForm'].action = "myCalendar.html";
		document.forms['myCalendarForm'].submit();
	}
function getEventList(){
		document.forms['myCalendarForm'].action = "myCalendar.html";
		document.forms['myCalendarForm'].submit();
	}

</script>
<SCRIPT LANGUAGE="JavaScript">

function findDefaultTimeHours(targetElement)
{
	if(targetElement.checked){
	var usrName = document.forms['myCalendarForm'].elements['calendarFile.userName'].value;
    var url="userDefaultData.html?ajax=1&decorator=simple&popup=true&usrName=" + encodeURI(usrName);
    http2.open("GET", url, true);
    http2.onreadystatechange = handleHttpResponse2;
    http2.send(null);
    }
}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                results = results.replace('[','');
                results = results.replace(']','');
                if(results == '' || results == 'null' || results == null){
	                completeTimeString();
                }else{
                res = results.split("#");
                document.forms['myCalendarForm'].elements['calendarFile.fromTime'].value = res[0];
                document.forms['myCalendarForm'].elements['calendarFile.toTime'].value = res[1];
                }
				}
				
             }
  
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    var http6 = getHTTPObject();







function fillSurveyToDate(){
////////////alert("sangeeeeeeeeeeeeeeeeeeeeeeeeee");
	var surveyFromDate = document.forms['myCalendarForm'].elements['calendarFile.surveyDate'].value;
	var surveyToDate = document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value;
	 if(surveyToDate =="")
	{
			document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value = surveyFromDate;
	}
} 




function calcDays()
{
 var date2 = document.forms['myCalendarForm'].elements['calendarFile.surveyDate'].value;	 
 var date1 = document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((sDate-eDate)/86400000);
  document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value = daysApart; 
  if(daysApart<0)
  {
    alert("From Date should be less than To Date");
    document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value='';
    document.forms['myCalendarForm'].elements['calendarFile.surveyTillDate'].value='';
    return false;
  }
  if(document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value=='NaN')
   {
     document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value = '';
   } 
    ////document.forms['myCalendarForm'].getElementById("surveyDays").focus();
 	document.forms['myCalendarForm'].elements['checkSurveyDaysClick'].value = '';
 } 

function findSurveyDay() {
	var survDay= document.forms['myCalendarForm'].elements['calendarFile.surveyDays'].value;
	} 

function autoSaveFunc(clickType){
	if ('${autoSavePrompt}' == 'No'){
		var noSaveAction = '<c:out value="${calendarFile.id}"/>';
      	     		
		if(document.forms['myCalendarForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 			noSaveAction = 'myCalendar.html';
        }
        processAutoSave(document.forms['myCalendarForm'], 'saveMyCalendars!saveOnTabChange.html', noSaveAction);
	
	
	}else{
			if (formIsDirty(document.forms['myCalendarForm']))
			{
			var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='myCalendarDetail.heading'/>");
       			if(agree){
		           if(document.forms['myCalendarForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 						noSaveAction = 'myCalendar.html';
       				 }
       				 processAutoSave(document.forms['myCalendarForm'], 'saveMyCalendars!saveOnTabChange.html', noSaveAction);
        		}else
        		{
        		if(document.forms['myCalendarForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 						noSaveAction = 'myCalendar.html';
       				 }
       				 processAutoSave(document.forms['myCalendarForm'], noSaveAction, noSaveAction);
        		
        		}
    		
  		}else{
  			if(document.forms['myCalendarForm'].elements['gotoPageString'].value == 'gototab.calendarFile'){
 				noSaveAction = 'myCalendar.html';
       		}
       			processAutoSave(document.forms['myCalendarForm'], noSaveAction, noSaveAction);
  		
  		}
  		}
  
}

function changeStatus()
{
   document.forms['myCalendarForm'].elements['formStatus'].value = '1';
}

function forDays(){
 document.forms['myCalendarForm'].elements['checkSurveyDaysClick'].value = '1';
}
</script>
<script type="text/javascript">   
    Form.focusFirstElement($("myCalendarForm"));   
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["fillSurveyToDate(),calcDays(),findSurveyDay()"]);
	setCalendarFunctionality();
</script>
