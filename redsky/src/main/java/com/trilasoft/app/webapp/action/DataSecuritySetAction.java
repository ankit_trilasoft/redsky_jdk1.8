package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.DataSecuritySet;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecuritySetManager;

public class DataSecuritySetAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private DataSecuritySet dataSecuritySet;
	private DataSecuritySetManager dataSecuritySetManager;
	private DataSecurityFilterManager dataSecurityFilterManager;
	private DataSecurityFilter dataSecurityFilter;
	private List dataSecuritySetList;
	private Long maxId;
	private List dataSecurityFilterList;
	private String name;
	private List userListSecuritySet; 
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DataSecuritySet getDataSecuritySet() {
		return dataSecuritySet;
	}
	public void setDataSecuritySet(DataSecuritySet dataSecuritySet) {
		this.dataSecuritySet = dataSecuritySet;
	}
	public List getDataSecuritySetList() {
		return dataSecuritySetList;
	}
	public void setDataSecuritySetList(List dataSecuritySetList) {
		this.dataSecuritySetList = dataSecuritySetList;
	}
	
	public void setDataSecuritySetManager(DataSecuritySetManager dataSecuritySetManager) {
		this.dataSecuritySetManager = dataSecuritySetManager;
	}
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(DataSecuritySetAction.class);

	 
	public DataSecuritySetAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataSecuritySetList=dataSecuritySetManager.getSetList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String findUserSecuritySet(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		userListSecuritySet=dataSecuritySetManager.getUserList(sessionCorpID,name);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataSecurityFilterList= dataSecuritySetManager.getDataSecurityFilterList();
		getSession().setAttribute("dataSecurityFilterList", dataSecurityFilterList);
		if (id != null)
		{
			dataSecuritySet=dataSecuritySetManager.get(id);
		}
		else
		{
			dataSecuritySet=new DataSecuritySet();
			dataSecuritySet.setCreatedOn(new Date());
			dataSecuritySet.setUpdatedOn(new Date());
			dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
			dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
		}
		
		dataSecuritySet.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean isNew = (dataSecuritySet.getId() == null);
		List filterList=dataSecuritySet.getFilterList();
		//System.out.println("\n\n\n\n\n filterList--->"+filterList);
		dataSecuritySet.getFilterList().clear();
		String[] filters = getRequest().getParameterValues("filters");

        for (int i = 0; filters != null && i < filters.length; i++) {
            String filterName = filters[i];
            dataSecurityFilter=(DataSecurityFilter)dataSecurityFilterManager.findFilters(filterName).get(0);
            dataSecuritySet.addFilters(dataSecurityFilter);
        }
		dataSecuritySet.setCorpID(sessionCorpID);
		/** #8123  Enable audit on security settings **/ 
		dataSecuritySet=dataSecuritySetManager.save(dataSecuritySet);
		// dataSecuritySetManager.saveDataSecuritySet(dataSecuritySet); 
		String key = (isNew) ? "dataSecuritySet has been added successfully" : "dataSecuritySet has been updated successfully";
		saveMessage(getText(key));
		
		if (!isNew) {  
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End"); 
			dataSecuritySet.setUpdatedOn(new Date());
			dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
        	return INPUT;   
        } else {   
        	dataSecuritySet.setCreatedOn(new Date());
        	dataSecuritySet.setCreatedBy(getRequest().getRemoteUser());
			dataSecuritySet.setUpdatedOn(new Date());
			dataSecuritySet.setUpdatedBy(getRequest().getRemoteUser());
        	maxId = Long.parseLong(dataSecuritySetManager.findMaximumId().get(0).toString());
        	dataSecuritySet = dataSecuritySetManager.get(maxId);
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
        }
		
	}
	@SkipValidation
	public String dataSecuritySetSearch()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(dataSecuritySet!=null){
		boolean name=(dataSecuritySet.getName() == null);
		boolean description = (dataSecuritySet.getDescription() == null);
		
		if(!name|| !description) {
			dataSecuritySetList=dataSecuritySetManager.search(dataSecuritySet.getName(), dataSecuritySet.getDescription());
		}
		}else{
			dataSecuritySetList=dataSecuritySetManager.getAll();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	private String userRoleListAction(){
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String roleName;
	private String userName;
	private List userRoleList;
	private List roleList;
	@SkipValidation
	public String userRoleListSearch(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		roleList=dataSecuritySetManager.getRoleList(sessionCorpID);
		boolean rName=(roleName == null);
		boolean uName = (userName == null);
		if(!rName|| !uName)
		{
			userRoleList=dataSecuritySetManager.getUsers(roleName, userName, sessionCorpID);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public List getDataSecurityFilterList() {
		return dataSecurityFilterList;
	}
	public void setDataSecurityFilterList(List dataSecurityFilterList) {
		this.dataSecurityFilterList = dataSecurityFilterList;
	}
	public void setDataSecurityFilterManager(DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}
	public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}
	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List getUserRoleList() {
		return userRoleList;
	}
	public void setUserRoleList(List userRoleList) {
		this.userRoleList = userRoleList;
	}
	public List getRoleList() {
		return roleList;
	}
	public void setRoleList(List roleList) {
		this.roleList = roleList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List getUserListSecuritySet() {
		return userListSecuritySet;
	}
	public void setUserListSecuritySet(List userListSecuritySet) {
		this.userListSecuritySet = userListSecuritySet;
	}
	
}
