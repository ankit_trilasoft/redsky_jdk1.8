<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ShippingConfirmation" pageWidth="595" pageHeight="842" columnWidth="535" leftMargin="30" rightMargin="30" topMargin="20" bottomMargin="20" uuid="7f7833a9-1ccb-4108-9af3-d05dd1ee9916">
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="12"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT
     "ShippingConfirmation.jrxml",
     concat(serviceorder.`prefix`," ",serviceorder.`firstName`," ",serviceorder.`lastName`)AS Shipper,
     serviceorder.`registrationNumber` AS serviceorder_registrationNumber,
     serviceorder.`shipNumber` AS serviceorder_shipNumber,
     DATE_FORMAT(now(),'%M %d, %Y') AS Date,
     servicepartner.`carrierDeparture` AS servicepartner_polCode,
     servicepartnerl.`carrierArrival` AS servicepartner_poeCode,
     DATE_FORMAT(servicepartner.`etArrival`,'%M %d, %Y') AS servicepartner_etArrival,
     servicepartner.`carrierVessels` AS servicepartner_carrierVessels,
     DATE_FORMAT(servicepartner.`etDepart`,'%M %d, %Y') AS servicepartner_etDepart,
     if(servicepartner.transhipped is true,'Transit Harbour','')AS aaaa,
     container.`size` AS container_size
FROM
     `serviceorder` serviceorder,
     `servicepartner` servicepartner,
     `servicepartner` servicepartnerl,
     `container` container
WHERE
     serviceorder.corpid = $P{Corporate ID}
     and servicepartnerl.corpid = $P{Corporate ID}
     and container.corpid = $P{Corporate ID}
     and servicepartner.corpid = $P{Corporate ID}
     and serviceorder.id = servicepartner.serviceorderid
     and serviceorder.id = servicepartnerl.serviceorderid
     and serviceorder.id = container.serviceorderid
 AND serviceorder.`shipNumber` = $P{Service Order Number}
 order by servicepartnerl.id DESC,servicepartner.id ASC]]>
	</queryString>
	<field name="ShippingConfirmation.jrxml" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="Date" class="java.lang.String"/>
	<field name="servicepartner_polCode" class="java.lang.String"/>
	<field name="servicepartner_poeCode" class="java.lang.String"/>
	<field name="servicepartner_etArrival" class="java.lang.String"/>
	<field name="servicepartner_carrierVessels" class="java.lang.String"/>
	<field name="servicepartner_etDepart" class="java.lang.String"/>
	<field name="aaaa" class="java.lang.String"/>
	<field name="container_size" class="java.lang.String"/>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="79" splitType="Stretch">
			<image scaleImage="FillFrame" hAlign="Center" vAlign="Middle" onErrorType="Icon">
				<reportElement key="image-1" x="98" y="1" width="339" height="77" uuid="8c9b41cc-8e38-477e-8e4d-14d538f1f86c"/>
				<imageExpression><![CDATA["../../images/transpacklogo.jpg"]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band height="33" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-1" x="0" y="1" width="535" height="32" uuid="b29ecffa-0c6a-4acc-b77d-2a824be942c4"/>
				<box leftPadding="84"/>
				<textElement textAlignment="Center">
					<font size="18" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[LOADING / SHIPPING CONFIRMATION]]></text>
			</staticText>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="422" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="7" y="5" width="142" height="22" uuid="e8653222-b9df-4ab2-b146-1b91149ae7d5"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[DATE
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-3" x="7" y="71" width="142" height="22" uuid="7c98485c-5217-4441-b205-9869a5b2f97d"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[YOUR REFERENCE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-4" x="7" y="49" width="142" height="22" uuid="93495d0b-9d73-4804-9fca-ae3c1a3eae47"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[TSS REFERENCE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-5" x="7" y="27" width="142" height="22" uuid="355e7de5-5736-4fbe-a4ee-1a4c02767828"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[CLIENTNAME]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-6" x="7" y="93" width="528" height="22" uuid="9a31dbfd-24e4-4cfe-aa84-2bf7717c886f"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-7" x="7" y="159" width="142" height="22" uuid="4c61e552-a807-4c56-9b0f-c7441b936440"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[P.O.D.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-8" x="7" y="137" width="142" height="22" uuid="fd7f64c5-f4f7-489c-9fe5-d1d057345afe"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[VIA]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-9" x="7" y="115" width="142" height="22" uuid="5ba06d68-ad56-41be-accd-f2c43b95cb5f"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[P.O.L.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-10" x="7" y="181" width="528" height="22" uuid="560ce6fd-2a2d-483a-a093-bb392aa3615b"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[ 
]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-11" x="7" y="247" width="142" height="22" uuid="09626064-8c8e-4b69-9f11-0b0323eb16e3"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[MOTOR VESSEL]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-12" x="7" y="225" width="528" height="22" uuid="0710f2de-6044-427a-9e72-7841d3ce2ba4"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-13" x="7" y="203" width="142" height="22" uuid="cc38c4eb-ec31-4287-89c0-d8f65219571c"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[EQUIPMENT]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-14" x="7" y="269" width="142" height="22" uuid="f1b609d9-1429-491c-8428-b7a23be03d33"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Sailing Date]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-15" x="7" y="335" width="142" height="22" uuid="3d4bd521-0f2e-4f55-8549-1b0d7ea2c25d"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[REMARKS]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-16" x="7" y="313" width="142" height="22" uuid="c89d4727-00f3-4bb1-9bc3-5646db4c274a"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[EXCHANGERATE]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-17" x="7" y="291" width="142" height="22" uuid="aaa0ce36-2c2d-439b-bc9c-ee47d8af25dc"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[ETA ]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="71" width="386" height="22" uuid="a79e8403-3bac-41d7-bb26-df0a5614de37"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="49" width="386" height="22" uuid="64d477f9-c1bf-4a20-8f40-99aade55972c"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="27" width="386" height="22" uuid="beb46b8e-1769-4210-b1eb-4629a3b69972"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Shipper}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="5" width="386" height="22" uuid="18238bd8-141d-4ab1-b8fb-009ab6fa5dff"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{Date}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="115" width="386" height="22" uuid="3fb5c8e1-382f-400b-812f-c7b107c73f85"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_polCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="159" width="386" height="22" uuid="0e4ad065-d58e-485a-9335-c12c17c0db48"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_poeCode}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="MMMMM dd, yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="291" width="386" height="22" uuid="4cd660ce-1746-4695-a77d-bfff08841aaf"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_etArrival}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" pattern="MMMMM dd, yyyy" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="269" width="386" height="22" uuid="89fead85-802f-42d5-b1e4-a43fd6253410"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_etDepart}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField" x="149" y="247" width="386" height="22" uuid="c6531c80-9d5f-476b-a7e9-5ee01e687c08"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{servicepartner_carrierVessels}]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-1" x="149" y="335" width="386" height="22" uuid="14bb272d-91a3-4626-92ea-c9afc4da907e"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-2" x="149" y="313" width="386" height="22" uuid="5ae080e9-9c76-4e7c-933b-66737c340f2b"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[]]></textFieldExpression>
			</textField>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-3" x="149" y="137" width="386" height="22" uuid="4458c3bc-c23d-4b62-a5c1-db580ef47a74"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement>
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{aaaa}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-19" x="7" y="5" width="528" height="417" uuid="559318f6-e279-4767-bdf1-3f50c2146f95"/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField isStretchWithOverflow="true" isBlankWhenNull="true">
				<reportElement key="textField-4" x="149" y="203" width="386" height="22" uuid="db4f532c-e6e3-4af8-a038-c987f250bc91"/>
				<box leftPadding="5">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{container_size}]]></textFieldExpression>
			</textField>
		</band>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
