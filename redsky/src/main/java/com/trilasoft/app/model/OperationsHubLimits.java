package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "operationshublimits")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")

public class OperationsHubLimits extends BaseObject {
	private Long id;

	private String hubID;

	private String dailyOpHubLimit;

	private String dailyOpHubLimitPC;

	private String minServiceDayHub;

	private String corpID;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	
	private BigDecimal dailyMaxEstWt;
	
	private BigDecimal dailyMaxEstVol;
	private String dailyCrewCapacityLimit;
	private String dailyCrewThreshHoldLimit;

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("hubID", hubID)
				.append("dailyOpHubLimit", dailyOpHubLimit)
				.append("dailyOpHubLimitPC", dailyOpHubLimitPC)
				.append("minServiceDayHub", minServiceDayHub)
				.append("corpID", corpID).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("dailyMaxEstWt", dailyMaxEstWt)
				.append("dailyMaxEstVol", dailyMaxEstVol)
				.append("dailyCrewCapacityLimit", dailyCrewCapacityLimit)
				.append("dailyCrewThreshHoldLimit", dailyCrewThreshHoldLimit)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof OperationsHubLimits))
			return false;
		OperationsHubLimits castOther = (OperationsHubLimits) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(hubID, castOther.hubID)
				.append(dailyOpHubLimit, castOther.dailyOpHubLimit)
				.append(dailyOpHubLimitPC, castOther.dailyOpHubLimitPC)
				.append(minServiceDayHub, castOther.minServiceDayHub)
				.append(corpID, castOther.corpID)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(dailyMaxEstWt, castOther.dailyMaxEstWt)
				.append(dailyMaxEstVol, castOther.dailyMaxEstVol)
				.append(dailyCrewCapacityLimit, castOther.dailyCrewCapacityLimit)
				.append(dailyCrewThreshHoldLimit, castOther.dailyCrewThreshHoldLimit)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(hubID)
				.append(dailyOpHubLimit).append(dailyOpHubLimitPC)
				.append(minServiceDayHub).append(corpID).append(createdBy)
				.append(updatedBy).append(createdOn).append(updatedOn)
				.append(dailyMaxEstWt).append(dailyMaxEstVol)
				.append(dailyCrewCapacityLimit).append(dailyCrewThreshHoldLimit)
				.toHashCode();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getDailyOpHubLimit() {
		return dailyOpHubLimit;
	}

	public void setDailyOpHubLimit(String dailyOpHubLimit) {
		this.dailyOpHubLimit = dailyOpHubLimit;
	}

	public String getDailyOpHubLimitPC() {
		return dailyOpHubLimitPC;
	}

	public void setDailyOpHubLimitPC(String dailyOpHubLimitPC) {
		this.dailyOpHubLimitPC = dailyOpHubLimitPC;
	}

	public String getHubID() {
		return hubID;
	}

	public void setHubID(String hubID) {
		this.hubID = hubID;
	}

	public String getMinServiceDayHub() {
		return minServiceDayHub;
	}

	public void setMinServiceDayHub(String minServiceDayHub) {
		this.minServiceDayHub = minServiceDayHub;
	}
	@Column
	public BigDecimal getDailyMaxEstWt() {
		return dailyMaxEstWt;
	}

	public void setDailyMaxEstWt(BigDecimal dailyMaxEstWt) {
		this.dailyMaxEstWt = dailyMaxEstWt;
	}
	@Column
	public BigDecimal getDailyMaxEstVol() {
		return dailyMaxEstVol;
	}

	public void setDailyMaxEstVol(BigDecimal dailyMaxEstVol) {
		this.dailyMaxEstVol = dailyMaxEstVol;
	}

	public String getDailyCrewCapacityLimit() {
		return dailyCrewCapacityLimit;
	}

	public void setDailyCrewCapacityLimit(String dailyCrewCapacityLimit) {
		this.dailyCrewCapacityLimit = dailyCrewCapacityLimit;
	}

	public String getDailyCrewThreshHoldLimit() {
		return dailyCrewThreshHoldLimit;
	}

	public void setDailyCrewThreshHoldLimit(String dailyCrewThreshHoldLimit) {
		this.dailyCrewThreshHoldLimit = dailyCrewThreshHoldLimit;
	}
}
