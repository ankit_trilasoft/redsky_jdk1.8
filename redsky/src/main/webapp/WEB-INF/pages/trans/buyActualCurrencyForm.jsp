<%@ include file="/common/taglibs.jsp"%>
<title>Pricing Currency Form</title>
<meta name="heading" content="Pricing Currency Form" />
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 

<style type="text/css">h2 {background-color: #FBBFFF}
.subcontent-tab{border-style: solid solid solid; border-color: rgb(116, 179, 220) rgb(116, 179, 220) -moz-use-text-color; border-width: 1px 1px 1px; height:20px;border-color:#99BBE8}
</style>
	<style>
		<%@ include file="/common/calenderStyle.css"%>
	</style>
	<script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script>
	<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js"></script>
	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup('mydiv'); 
		cal.showNavigationDropdowns()
	</script>
<style type="text/css">
 #overlay {
filter:alpha(opacity=70);
-moz-opacity:0.7;
-khtml-opacity: 0.7;
opacity: 0.7;
position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
background:url(images/over-load.png);
}
</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<SCRIPT LANGUAGE="JavaScript">
 
</script>


<s:form id="accountLineCurrencyPayForm" name="accountLineCurrencyPayForm" action="" onsubmit="" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="tempAccList"/>
 <s:hidden name="accountLine.actualExpense"  /> 
 <s:hidden name="accountLine.recQuantity"  />  
 <s:hidden name="accountLine.basis"  />
 <s:hidden name="accountLine.actualDiscount"/> 
 <s:hidden name="aid" value="${accountLine.id}" />  
<s:hidden name="formStatus"  />
<div id="layer1" style="width:100%">
  
 <div id="otabs">
				  <ul>
				    <li><a class="current"><span>Buy&nbsp;Rate&nbsp;Currency&nbsp;Data</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div> 
<div id="content" align="center">
<div id="liquid-round-top">
    <div class="top" style="margin-top:10px; "><span></span></div>
    <div class="center-content">    

<table border="0" style="margin:0px;padding:0px;">
										<c:if test="${contractType}">																											 
										 <tr>
										 <td colspan="9">															 											
										 <table class="detailTabLabel" style="margin:0px;">
										 <tr>															
										 <td width="1px"></td>
										 <td align="right" class="listwhitetext">Contract&nbsp;Currency<font color="red" size="2">*</font></td>
										 <td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.payableContractCurrency" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.payableContractExchangeRate');calculatePayCurrency('accountLine.payableContractRateAmmount');" headerValue="" tabindex="12"  /></td>
										 <td align="right" width="" class="listwhitetext">Value&nbsp;Date</td>

										 <c:if test="${not empty accountLine.payableContractValueDate}"> 
										  <s:text id="accountLineFormattedEstimateContractValueDate" name="${FormDateValue}"><s:param name="value" value="accountLine.payableContractValueDate"/></s:text>
										  <td ><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" value="%{accountLineFormattedEstimateContractValueDate}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
										  <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										 </td>
										 </c:if>
										     <c:if test="${empty accountLine.payableContractValueDate}">
										   <td><s:textfield id="payableContractValueDate" name="accountLine.payableContractValueDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="8"/>
										   <img id="payableContractValueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
										   </td>
										     </c:if>
                                            <td  align="right" class="listwhitetext" width="55px">Ex.&nbsp;Rate</td>
										 <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.payableContractExchangeRate"  size="8" maxlength="10" tabindex="12" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');" /></td>   			
                                            <td align="right" class="listwhitetext" width="85px">Contract&nbsp;Amount</td>
									     <td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.payableContractRateAmmount" size="8" maxlength="12" tabindex="12" onkeydown="return onlyRateAllowed(event)"  onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');" /></td>	 
										 </tr></table>															
										 </td>															
										 </tr>
										</c:if> 
										
</table>
<table style="margin:0px 0px 20px 0px; padding:0px;">

<tr>

													 <c:choose>
													 <c:when test="${contractType}">
													 <td align="right" class="listwhitetext" width="102">Billing&nbsp;Currency<font color="red" size="2">*</font></td>
													 </c:when>
													 <c:otherwise>
													 <td align="right" class="listwhitetext">Currency</td>
													 </c:otherwise>
													 </c:choose>
<td align="left" colspan="2" ><s:select  cssClass="list-menu"  key="accountLine.country" cssStyle="width:60px" list="%{country}" headerKey="" onchange="changeStatus();updateExchangeRate(this,'accountLine.exchangeRate');calculatePayCurrency('accountLine.payableContractRateAmmount');" headerValue="" tabindex="18"  /></td>
<td align="right"   class="listwhitetext">Value&nbsp;Date</td>
<c:if test="${not empty accountLine.valueDate}"> 
<s:text id="accountLineFormattedValue" name="${FormDateValue}"><s:param name="value" value="accountLine.valueDate"/></s:text>
<td width="110px" ><s:textfield id="valueDate" name="accountLine.valueDate" value="%{accountLineFormattedValue}" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<c:if test="${empty accountLine.valueDate}">
<td width="110px"><s:textfield id="valueDate" name="accountLine.valueDate" onkeydown="" readonly="true" cssClass="input-text" size="8"/>
<img id="valueDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
</td>
</c:if>
<td  align="right" class="listwhitetext">Ex.&nbsp;Rate</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" cssStyle="text-align:right" key="accountLine.exchangeRate" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculatePayCurrency('accountLine.payableContractRateAmmount');"/></td>
<td align="right" class="listwhitetext" width="86px">Curr&nbsp;Amount</td>
<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" cssStyle="text-align:right" key="accountLine.localAmount" size="8" maxlength="10" onkeydown="return onlyFloatNumsAllowed(event)"  onchange="changeStatus();calculatePayCurrency('accountLine.localAmount');"/></td>	 
</tr> 
</table>

</div>

<div class="bottom-header"><span></span></div>
</div>
</div> 
	</div>
<div id="mydiv" style="position:absolute;margin-top:-28px;"></div>
<table>
<tr>
<td><input type="button" class="cssbuttonA" style="width:55px; height:25px" name="pricingSave" value="Save" onclick="calculatePayRate();"/></td>
<td><input type="button"  value="Cancel" name="Cancel" class="cssbuttonA" style="width:70px; height:25px" onclick="window.close();"></td>
</tr>
</table>
</s:form>
<script type="text/javascript">
</script>
<script type="text/javascript">
<c:if test="${not empty accountLine.payAccDate}">
var elementsLen=document.forms['accountLineCurrencyPayForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyPayForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyPayForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyPayForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyPayForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${((accountLine.chargeCode=='MGMTFEE'|| accountLine.chargeCode=='DMMFEE'|| accountLine.chargeCode=='DMMFXFEE') || (!trackingStatus.accNetworkGroup  && trackingStatus.soNetworkGroup && accountLine.createdBy == 'Networking'))}">
var elementsLen=document.forms['accountLineCurrencyPayForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyPayForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyPayForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyPayForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyPayForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ serviceOrder.status == 'CNCL' || serviceOrder.status == 'DWND' || serviceOrder.status == 'DWNLD'}">
var elementsLen=document.forms['accountLineCurrencyPayForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyPayForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyPayForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyPayForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyPayForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
<c:if test="${ accountLine.status == false}">
var elementsLen=document.forms['accountLineCurrencyPayForm'].elements.length;
for(i=0;i<=elementsLen-1;i++)
	{
		if(document.forms['accountLineCurrencyPayForm'].elements[i].type=='text') {
				document.forms['accountLineCurrencyPayForm'].elements[i].readOnly =true;
				document.forms['accountLineCurrencyPayForm'].elements[i].className = 'input-textUpper'; 
			} else { 
				document.forms['accountLineCurrencyPayForm'].elements[i].disabled=true;
			}
	}
var totalImages = document.images.length;
for (var i=0;i<totalImages;i++){
		if(document.images[i].src.indexOf('calender.png')>0){
			var el = document.getElementById(document.images[i].id);  
				document.images[i].src = 'images/navarrow.gif'; 
				if((el.getAttribute("id")).indexOf('trigger')>0){
					var newId = el.getAttribute("id").substring(0,el.getAttribute("id").length-1);
					el.setAttribute("id",newId);
				}
		}
}
</c:if>
</script>
<script type="text/javascript">
setOnSelectBasedMethods([]);
setCalendarFunctionality();
</script>
<script type="text/javascript">

function checkFloat1(temp,fieldName) { 
	negativeCount = 0;
	var check='';  
	var i;
	var dotcheck=0;
	var s = temp;
	if(s.indexOf(".") == -1){
		dotcheck=eval(s.length)
	}else{
		dotcheck=eval(s.indexOf(".")-1)
	}
	var count = 0;
	var countArth = 0;
	for (i = 0; i < s.length; i++) {   
		var c = s.charAt(i); 
		if(c == '.') {
			count = count+1
		}
		if(c == '-') {
			countArth = countArth+1
		}
		if((i!=0)&&(c=='-')) {
			alert("Invalid data." ); 
			document.forms['accountLineCurrencyPayForm'].elements[fieldName].select();
			document.forms['accountLineCurrencyPayForm'].elements[fieldName].value='';
			return false;
		} 	
		if (((c < '0') || (c > '9')) && ((c != '.') || (count>'1')) &&((c != '-') || (countArth>'1')))  {
			check='Invalid'; 
		}
	}
	if(dotcheck>15){
		check='tolong';
	}
	if(check=='Invalid'){ 
		alert("Invalid data." ); 
		document.forms['accountLineCurrencyPayForm'].elements[fieldName].select();
		document.forms['accountLineCurrencyPayForm'].elements[fieldName].value='';
		return false;
	} else if(check=='tolong'){ 
		alert("Data Too Long." ); 
		document.forms['accountLineCurrencyPayForm'].elements[fieldName].select();
		document.forms['accountLineCurrencyPayForm'].elements[fieldName].value='';
		return false;
	} else{
		s=Math.round(s*100)/100;
		var value=""+s;
		if(value.indexOf(".") == -1){
			value=value+".00";
		} 
		if((value.indexOf(".")+3 != value.length)){
			value=value+"0";
		}
		value = document.forms['accountLineCurrencyPayForm'].elements[fieldName].value=value;
		return value;
	}
}

function calculatePayRate(){ 
	var aid=document.forms['accountLineCurrencyPayForm'].elements['aid'].value;
	var actualExpense = checkFloat1(document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value,'accountLine.actualExpense');
	 window.opener.setFieldValue('actualExpense'+aid,actualExpense);
	 window.opener.setFieldValue('recQuantity'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.recQuantity'].value);
    <c:if test="${contractType}">
	 window.opener.setFieldValue('payableContractCurrency'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractCurrency'].value);
	 window.opener.setFieldValue('payableContractExchangeRate'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractExchangeRate'].value);
	 window.opener.setFieldValue('payableContractRateAmmount'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractRateAmmount'].value);
	 window.opener.setFieldValue('payableContractValueDate'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractValueDate'].value);
    </c:if>  
	 window.opener.setFieldValue('country'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.country'].value);
	 window.opener.setFieldValue('exchangeRate'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.exchangeRate'].value);
	 window.opener.setFieldValue('localAmount'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value);
	 window.opener.setFieldValue('valueDate'+aid,document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value);
	 window.opener.getId(aid);
    if(document.forms['accountLineCurrencyPayForm'].elements['formStatus'].value=='2'){
    window.opener.calculateActualRevenue(aid,'','FX');
    window.opener.changeStatus();
    }
		window.close();
}




function changeStatus() {
	   document.forms['accountLineCurrencyPayForm'].elements['formStatus'].value = '2'; 
	}
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
}	
function onlyRateAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==109)|| (keyCode==190)||(keyCode==189)|| (keyCode==110); 
}
function findExchangeRateGlobal(currency){
	 var rec='1';
		<c:forEach var="entry" items="${currencyExchangeRate}">
			if('${entry.key}'==currency.trim()){
				rec='${entry.value}';
			}
		</c:forEach>
		return rec;
}
function updateExchangeRate(currency,field){
	document.forms['accountLineCurrencyPayForm'].elements[field].value=findExchangeRateGlobal(currency.value);
}
function currentDateGlobal(){
	 var mydate=new Date();
    var daym;
    var year=mydate.getFullYear()
    var y=""+year;
    if (year < 1000)
    year+=1900
    var day=mydate.getDay()
    var month=mydate.getMonth()+1
    if(month == 1)month="Jan";
    if(month == 2)month="Feb";
    if(month == 3)month="Mar";
	  if(month == 4)month="Apr";
	  if(month == 5)month="May";
	  if(month == 6)month="Jun";
	  if(month == 7)month="Jul";
	  if(month == 8)month="Aug";
	  if(month == 9)month="Sep";
	  if(month == 10)month="Oct";
	  if(month == 11)month="Nov";
	  if(month == 12)month="Dec";
	  var daym=mydate.getDate()
	  if (daym<10)
	  daym="0"+daym
	  var datam = daym+"-"+month+"-"+y.substring(2,4); 
	  return datam;
}
function calculatePayCurrency(target){
	 var basis=document.forms['accountLineCurrencyPayForm'].elements['accountLine.basis'].value;
	 var baseRateVal=1;
	var actualDiscount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualDiscount'].value;	
	var recQuantity=document.forms['accountLineCurrencyPayForm'].elements['accountLine.recQuantity'].value;	
    var actualDiscount=0.00;
    var actualExpense=0.00;
    try{
   	 actualDiscount=document.forms['pricingListForm'].elements['accountLine.actualDiscount'].value;
    }catch(e){}
	 if(recQuantity=='' ||recQuantity=='0' ||recQuantity=='0.0' ||recQuantity=='0.00') {
		 recQuantity = document.forms['accountLineCurrencyPayForm'].elements['accountLine.recQuantity'].value=1;
        } 
    if( basis=="cwt" || basis=="%age"){
      	 baseRateVal=100;
    }else if(basis=="per 1000"){
      	 baseRateVal=1000;
    } else {
      	 baseRateVal=1;  	
	 }		
<c:if test="${contractType}">
if(target=='accountLine.payableContractRateAmmount'){
	var payableContractCurrency=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractCurrency'].value;		
	var payableContractExchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractExchangeRate'].value;
	var payableContractRateAmmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractRateAmmount'].value;
	var payableContractValueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractValueDate'].value;
	if(payableContractCurrency!=''){
		document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
	   document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value=payableContractRateAmmount/payableContractExchangeRate;
	}
	   var country=document.forms['accountLineCurrencyPayForm'].elements['accountLine.country'].value;		
	   var exchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.exchangeRate'].value;
	   var localAmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value;
	   var valueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value;
	   actualExpense=document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value;
	   	if(country!=''){
	   		document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
	   		localAmount=actualExpense*exchangeRate;
	   	 document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value=localAmount;   	 		
	    }
}else{
	var country=document.forms['accountLineCurrencyPayForm'].elements['accountLine.country'].value;		
	var exchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.exchangeRate'].value;
	var localAmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value;
	var valueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value;
	if(country!=''){
		document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
		   document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
		}
	var payableContractCurrency=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractCurrency'].value;		
	var payableContractExchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractExchangeRate'].value;
	var payableContractRateAmmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractRateAmmount'].value;
	var payableContractValueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractValueDate'].value;
		   actualExpense=document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value;
		   	if(payableContractCurrency!=''){
		   		document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractValueDate'].value=currentDateGlobal();
		   		payableContractRateAmmount=actualExpense*payableContractExchangeRate;
		   	 document.forms['accountLineCurrencyPayForm'].elements['accountLine.payableContractRateAmmount'].value=payableContractRateAmmount;   	 		
		    }
}
	
</c:if>
<c:if test="${!contractType}">
if(target=='accountLine.localAmount'){
	var country=document.forms['accountLineCurrencyPayForm'].elements['accountLine.country'].value;		
	var exchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.exchangeRate'].value;
	var localAmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value;
	var valueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value;
	if(country!=''){
		document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
	   document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
	}
}else{
	var country=document.forms['accountLineCurrencyPayForm'].elements['accountLine.country'].value;		
	var exchangeRate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.exchangeRate'].value;
	var localAmount=document.forms['accountLineCurrencyPayForm'].elements['accountLine.localAmount'].value;
	var valueDate=document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value;
	if(country!=''){
		document.forms['accountLineCurrencyPayForm'].elements['accountLine.valueDate'].value=currentDateGlobal();
		   document.forms['accountLineCurrencyPayForm'].elements['accountLine.actualExpense'].value=localAmount/exchangeRate;
		}
}
</c:if>
}
</script>
