<script type="text/javascript">
function getCookie(name) {
	var prefix = name + "=" 
	var start = document.cookie.indexOf(prefix) 

	if (start==-1) {
		return null;
	}
	
	var end = document.cookie.indexOf(";", start+prefix.length) 
	if (end==-1) {
		end=document.cookie.length;
	}

	var value=document.cookie.substring(start+prefix.length, end) 
	return unescape(value);
}

    if (getCookie("username") != null) {
        document.forms['loginForm'].elements['j_username'].value = getCookie("username");
        document.forms['loginForm'].elements['j_password'].focus();
    } else {
        document.forms['loginForm'].elements['j_username'].focus();
    }
    
    function saveUsername(theForm) {
        var expires = new Date();
        expires.setTime(expires.getTime() + 24 * 30 * 60 * 60 * 1000); // sets it for approx 30 days.
        setCookie("username",theForm.j_username.value,expires,"<c:url value="/"/>");
        //var expires1 = new Date();
        //expires1.setTime(expires1.getTime() + 60 * 1000);
        //setCookie("currentPassword",theForm.j_password.value,expires1,"<c:url value="/"/>");
    }
    
    function validateForm(form) {                                                               
        return validateRequired(form); 
    } 
    
    function passwordHint() {
        if (document.forms['loginForm'].elements['j_username'].value.length == 0) {
            alert("<s:text name="errors.requiredField"><s:param><s:text name="label.username"/></s:param></s:text>");
            document.forms['loginForm'].elements['j_username'].focus();
        } else {
            location.href="<c:url value="/passwordHint.html"/>?username=" + document.forms['loginForm'].elements['j_username'].value;     
        }
    }
    
    function required () { 
        this.aa = new Array("j_username", "<s:text name="errors.requiredField"><s:param><s:text name="label.username"/></s:param></s:text>", new Function ("varName", " return this[varName];"));
        this.ab = new Array("j_password", "<s:text name="errors.requiredField"><s:param><s:text name="label.password"/></s:param></s:text>", new Function ("varName", " return this[varName];"));
    } 
</script>