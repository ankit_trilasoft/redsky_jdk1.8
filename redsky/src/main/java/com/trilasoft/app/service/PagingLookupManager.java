package com.trilasoft.app.service;

import java.util.List;

import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public interface PagingLookupManager {
    ExtendedPaginatedList getAllRecordsPage(Class clazz,
            ExtendedPaginatedList paginatedList, List<SearchCriterion> searchCriteria);
}
