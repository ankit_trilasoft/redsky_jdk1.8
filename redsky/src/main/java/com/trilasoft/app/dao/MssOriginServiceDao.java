package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.MssOriginService;

public interface MssOriginServiceDao extends GenericDao<MssOriginService, Long>{

}
