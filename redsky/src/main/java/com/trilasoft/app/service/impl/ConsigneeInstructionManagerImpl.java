package com.trilasoft.app.service.impl;
import java.util.List;
import com.trilasoft.app.dao.ConsigneeInstructionDao; 
import com.trilasoft.app.model.ConsigneeInstruction; 
import com.trilasoft.app.service.ConsigneeInstructionManager; 
import org.appfuse.service.impl.GenericManagerImpl;  
 
public class ConsigneeInstructionManagerImpl extends GenericManagerImpl<ConsigneeInstruction, Long> implements ConsigneeInstructionManager
{ 
	ConsigneeInstructionDao consigneeInstructionDao; 
    public ConsigneeInstructionManagerImpl(ConsigneeInstructionDao consigneeInstructionDao)
    { 
        super(consigneeInstructionDao); 
        this.consigneeInstructionDao = consigneeInstructionDao; 
    } 
     public List<ConsigneeInstruction> findBySequenceNumber(String sequenceNumber) 
     { 
        return consigneeInstructionDao.findBySequenceNumber(sequenceNumber); 
    }
     public List findMaximumId(){
     	return consigneeInstructionDao.findMaximumId(); 
     }
     public List checkById(Long id) { 
         return consigneeInstructionDao.checkById(id); 
     }
	public List getByShipNumber(String shipNumber) {
		return consigneeInstructionDao.getByShipNumber(shipNumber);
	}
	public List findConsigneeCode(String shipNumber) {
		return consigneeInstructionDao.findConsigneeCode(shipNumber);
	}
	public List findConsigneeAddress(String DaCode) {
		return consigneeInstructionDao.findConsigneeAddress(DaCode);
	}
	public List findConsigneeJobAddress(String DaCode, String shipNum) {
		return consigneeInstructionDao.findConsigneeJobAddress(DaCode, shipNum);
	}
	public List findShipmentLocation(String consigneeInstructionCode,String corpID){
		return consigneeInstructionDao.findShipmentLocation(consigneeInstructionCode,corpID);
	}
	public List findConsigneeSubAgentAddress(String sACode, String shipNum){
		return consigneeInstructionDao.findConsigneeSubAgentAddress(sACode,shipNum);
	}
	public List consigneeInstructionList(Long serviceOrderId) {
		return consigneeInstructionDao.consigneeInstructionList(serviceOrderId);
	}
	public Long findRemoteConsigneeInstruction(Long serviceOrderId) {
		return consigneeInstructionDao.findRemoteConsigneeInstruction(serviceOrderId);
	}
	public List findConsigneeNCAddress(String shipNum){
		return consigneeInstructionDao.findConsigneeNCAddress(shipNum);
	}
	public List getShiperAddress(String corpID,String shipNum){
		return consigneeInstructionDao.getShiperAddress(corpID,shipNum);
	}
	public List consigneeInstructionListOtherCorpId(Long sid){
		return  consigneeInstructionListOtherCorpId(sid);
	}
}