package com.trilasoft.app.service;

import java.util.List;
import java.util.Map;

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.PartnerAccountRef;

public interface PartnerAccountRefManager extends GenericManager<PartnerAccountRef, Long> {
	public List getPartnerAccountReferenceList(String corpId,String partnerCodeForRef);
	public List findCompanyDivisionByCorpId(String corpId);
	public List getPartnerList(String partnerCode, String sessionCorpID);
	public List checkCompayCode(String companyDivision, String corpID,String partnerCode);
	public List checkCompayCodeAccount(String companyDivision, String corpID,String partnerCode,String refType);
	public int updateDeleteStatus(Long ids);
	public String findAccountInterface(String sessionCorpID); 
	public Map<String, String> getPayableReffNumber(String sessionCorpID);
	public List checkAccountReference(String AccountReference, String corpID,String partnerCode,String refType);
	public Map<String, String> getActgCodeMap(String sessionCorpID);
	public List countChkAccountRef(String partnerCode, String refType,String sessionCorpID);
	public String getAccountingReffCode(String sessionCorpID,String billToCode, String currencyCode, String reftype);
	public List getAccrefSameParentForChild(String corpId);
	public void deletePartnerAccountRefFromChild(String accountcrossReference,String childCorpId,String reftype,String partnerCode);
	public List checkAccountReferenceForChild(String AccountReference, String corpID,String partnerCode,String refType);
	
}
