package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="subcontractorcharges")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class SubcontractorCharges extends BaseObject{
	
	private String corpID;
	private Long id;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String branch;
	private String regNumber;
	private String personType;
	private String personId;
	private String glCode;
	private BigDecimal amount= new BigDecimal(0);
	private String description;
	private Date approved;
	private String flag1099;
	private Date post;
	private Date accountSent;
	private String sentToAccountVia;
	private Long  parentId;
	private String personName;
	private String vlCode;
	private boolean balanceForward;
	private String truckNo;
	private String companyDivision;
	private Boolean deductTempFlag;
	private Boolean isSettled;
	private String source;
	private String payTo;
	private String serviceOrder;
	private Date advanceDate;
	private Boolean personalEscrow;
	private Boolean onAccount;
	private String job;
	private String division;
	private String vanLineNumber;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("branch", branch)
				.append("regNumber", regNumber)
				.append("personType", personType).append("personId", personId)
				.append("glCode", glCode).append("amount", amount)
				.append("description", description)
				.append("approved", approved).append("flag1099", flag1099)
				.append("post", post).append("accountSent", accountSent)
				.append("sentToAccountVia", sentToAccountVia)
				.append("parentId", parentId).append("personName", personName)
				.append("vlCode", vlCode)
				.append("balanceForward", balanceForward)
				.append("truckNo", truckNo)
				.append("companyDivision", companyDivision)
				.append("deductTempFlag", deductTempFlag)
				.append("isSettled", isSettled).append("source", source)
				.append("payTo", payTo).append("serviceOrder", serviceOrder)
				.append("advanceDate", advanceDate)
				.append("personalEscrow", personalEscrow)
				.append("onAccount", onAccount) 
			    .append("job", job)
		        .append("division", division)
		        .append("vanLineNumber", vanLineNumber).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SubcontractorCharges))
			return false;
		SubcontractorCharges castOther = (SubcontractorCharges) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(branch, castOther.branch)
				.append(regNumber, castOther.regNumber)
				.append(personType, castOther.personType)
				.append(personId, castOther.personId)
				.append(glCode, castOther.glCode)
				.append(amount, castOther.amount)
				.append(description, castOther.description)
				.append(approved, castOther.approved)
				.append(flag1099, castOther.flag1099)
				.append(post, castOther.post)
				.append(accountSent, castOther.accountSent)
				.append(sentToAccountVia, castOther.sentToAccountVia)
				.append(parentId, castOther.parentId)
				.append(personName, castOther.personName)
				.append(vlCode, castOther.vlCode)
				.append(balanceForward, castOther.balanceForward)
				.append(truckNo, castOther.truckNo)
				.append(companyDivision, castOther.companyDivision)
				.append(deductTempFlag, castOther.deductTempFlag)
				.append(isSettled, castOther.isSettled)
				.append(source, castOther.source)
				.append(payTo, castOther.payTo)
				.append(serviceOrder, castOther.serviceOrder)
				.append(advanceDate, castOther.advanceDate)
				.append(personalEscrow, castOther.personalEscrow)
				.append(onAccount, castOther.onAccount)
				.append(job, castOther.job)
		        .append(division, castOther.division)
		        .append(vanLineNumber, castOther.vanLineNumber).isEquals();
		
		
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(branch).append(regNumber)
				.append(personType).append(personId).append(glCode)
				.append(amount).append(description).append(approved)
				.append(flag1099).append(post).append(accountSent)
				.append(sentToAccountVia).append(parentId).append(personName)
				.append(vlCode).append(balanceForward).append(truckNo)
				.append(companyDivision).append(deductTempFlag)
				.append(isSettled).append(source).append(payTo)
				.append(serviceOrder).append(advanceDate)
				.append(personalEscrow).append(onAccount).append(job).append(division).append(vanLineNumber).toHashCode();
	}
	@Column(length=25)
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	@Column
	public Date getApproved() {
		return approved;
	}
	public void setApproved(Date approved) {
		this.approved = approved;
	}
	@Column(length = 25)
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length = 225) 
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(length = 20)
	public String getGlCode() {
		return glCode;
	}
	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	} 
	@Column(length = 25)
	public String getPersonId() {
		return personId;
	}
	public void setPersonId(String personId) {
		this.personId = personId;
	}
	@Column(length = 20)
	public String getPersonType() {
		return personType;
	}
	public void setPersonType(String personType) {
		this.personType = personType;
	}
	@Column
	public Date getPost() {
		return post;
	}
	public void setPost(Date post) {
		this.post = post;
	} 
	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	 
	@Column(length = 25)
	public String getRegNumber() {
		return regNumber;
	}
	public void setRegNumber(String regNumber) {
		this.regNumber = regNumber;
	}
	@Column(length = 5)
	public String getFlag1099() {
		return flag1099;
	}
	public void setFlag1099(String flag1099) {
		this.flag1099 = flag1099;
	}
	@Column(length = 100)
	public String getSentToAccountVia() {
		return sentToAccountVia;
	}
	public void setSentToAccountVia(String sentToAccountVia) {
		this.sentToAccountVia = sentToAccountVia;
	}
	@Column
	public Date getAccountSent() {
		return accountSent;
	}
	public void setAccountSent(Date accountSent) {
		this.accountSent = accountSent;
	}
	@Column(length = 20)
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	@Column(length = 50)
	public String getPersonName() {
		return personName;
	}
	public void setPersonName(String personName) {
		this.personName = personName;
	}
	@Column
	public boolean isBalanceForward() {
		return balanceForward;
	}
	public void setBalanceForward(boolean balanceForward) {
		this.balanceForward = balanceForward;
	}
	@Column(length = 20)
	public String getTruckNo() {
		return truckNo;
	}
	public void setTruckNo(String truckNo) {
		this.truckNo = truckNo;
	}
	
	@Column(length = 10)
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	public Boolean getDeductTempFlag() {
		return deductTempFlag;
	}
	public void setDeductTempFlag(Boolean deductTempFlag) {
		this.deductTempFlag = deductTempFlag;
	}
	public Boolean getIsSettled() {
		return isSettled;
	}
	public void setIsSettled(Boolean isSettled) {
		this.isSettled = isSettled;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	@Column(length=15)
	public String getPayTo() {
		return payTo;
	}
	public void setPayTo(String payTo) {
		this.payTo = payTo;
	}
	@Column(length=20)
	public String getVlCode() {
		return vlCode;
	}

	public void setVlCode(String vlCode) {
		this.vlCode = vlCode;
	}
	@Column(length = 15)
	public String getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(String serviceOrder) {
		this.serviceOrder = serviceOrder;
	}
	public Date getAdvanceDate() {
		return advanceDate;
	}
	public void setAdvanceDate(Date advanceDate) {
		this.advanceDate = advanceDate;
	}
	public Boolean getPersonalEscrow() {
		return personalEscrow;
	}
	public void setPersonalEscrow(Boolean personalEscrow) {
		this.personalEscrow = personalEscrow;
	}
	public Boolean getOnAccount() {
		return onAccount;
	}
	public void setOnAccount(Boolean onAccount) {
		this.onAccount = onAccount;
	}
	@Column(length = 3)
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	@Column
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	@Column(length = 15)
	public String getVanLineNumber() {
		return vanLineNumber;
	}
	public void setVanLineNumber(String vanLineNumber) {
		this.vanLineNumber = vanLineNumber;
	}
	
	
}


