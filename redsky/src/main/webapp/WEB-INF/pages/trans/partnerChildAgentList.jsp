<%@ include file="/common/taglibs.jsp"%> 

<head> 
<title><fmt:message key="dataSecurityFilterList.title"/></title> 
<meta name="heading" content="<fmt:message key='dataSecurityFilterList.heading'/>"/>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:3px;
margin-top:-18px;
padding:2px 0px;
text-align:right;
width:100%;
}</style>

<script language="javascript" type="text/javascript">


</script>



</head>


	<s:form id="partnerChildAgentList" name="partnerChildAgentList" action="" method="post" validate="true">   
	<div id="Layer1" style="width:85%;">
	<c:if test="${partnerType=='AC'}">	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Child Account List</span></a></li>
		  </ul>
		</div>
	</c:if>
	<c:if test="${partnerType=='AG'}">	
	<div id="otabs">
		  <ul>
		    <li><a class="current"><span>Child Agent List</span></a></li>
		  </ul>
		</div>
	</c:if>
		<div class="spnblk">&nbsp;</div>
	
	<display:table name="partnerChildAgentList" class="table" requestURI="" id="partnerChildAgentListId" export="true" defaultsort="1" style="width:100%" >   
	<display:column sortable="true" titleKey="partner.partnerCode"><a href="editPartnerPublic.html?id=${partnerChildAgentListId.id}&partnerType=${partnerType}" target="_blank" ><c:out value="${partnerChildAgentListId.partnerCode}"></c:out></a></display:column>
   	<display:column property="lastName"  sortable="true" title="Last Name"/>
   	<display:column property="terminalCity"  sortable="true" title="Terminal City"/>
   	<display:column  sortable="true" title="Portal Access">
   	<c:if test="${partnerChildAgentListId.partnerPortalActive==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${partnerChildAgentListId.partnerPortalActive==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
   	<display:column sortable="true" title="Child Access">
   	<c:if test="${partnerChildAgentListId.viewChild==true }">
	<img src="${pageContext.request.contextPath}/images/tick01.gif"  />
	</c:if>
	<c:if test="${partnerChildAgentListId.viewChild==false }">
	<img src="${pageContext.request.contextPath}/images/cancel001.gif"  /> 
	</c:if>
	</display:column>
   	</display:table>
	
	<c:out value="${buttons}" escapeXml="false" />   
	<c:set var="isTrue" value="false" scope="session"/>
	</div>
</s:form> 

<script type="text/javascript"> 

highlightTableRows("dataSecurityFilterListId"); 

</script> 