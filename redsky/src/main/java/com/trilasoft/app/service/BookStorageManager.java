

package com.trilasoft.app.service;

import java.util.List;
 

import org.appfuse.service.GenericManager;

import com.trilasoft.app.model.BookStorage;

public interface BookStorageManager extends GenericManager<BookStorage, Long> {  
	
    public List<BookStorage> findByLocation(String locationId);   
    
    public List findMaximum();
    
    public List getStorageList(String ticket, String corpID);

	public List getStorageLibraryList(String ticket, String corpID);
}  
