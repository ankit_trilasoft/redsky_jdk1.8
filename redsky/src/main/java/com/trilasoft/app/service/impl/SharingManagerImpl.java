package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;


import com.trilasoft.app.dao.SharingDao;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Sharing;
import com.trilasoft.app.service.DataCatalogManager;
import com.trilasoft.app.service.SharingManager;

public class SharingManagerImpl extends GenericManagerImpl<Sharing, Long> implements SharingManager{

	SharingDao sharingDao;
	

	 public SharingManagerImpl(SharingDao sharingDao) { 
	        super(sharingDao); 
	        this.sharingDao = sharingDao; 
	    }


	public List findById(Long id) {
		return sharingDao.findById(id);
	} 
	
	public List findMaximumId(){
	    return sharingDao.findMaximumId(); 
	}
	
	public List<Sharing> searchSharing(String code, String discription) 
	{   
        return sharingDao.searchSharing(code,discription);   
    }
	
}
