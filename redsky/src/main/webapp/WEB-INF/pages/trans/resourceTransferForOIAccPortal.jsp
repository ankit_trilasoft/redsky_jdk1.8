<%@ include file="/common/taglibs.jsp"%> 
<%@ include file="/common/tooltip.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title>Resource Transfer Details</title>   
    <meta name="heading" content="ResourceTransfer"/>  
    <c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
    </c:if>
    
    <script type="text/javascript">
   
    function closeWIN(){
        alert("Resources Transferred to Work Ticket Successfully");
         window.opener.location.reload(true);
         window.close();
    }
    </script>

</head>

<style type="text/css">
.pr-f11{font-family:arial,verdana;font-size:11px;}
.textarea-comment {border:1px solid #219dd1;color:#000;height:17px; width:100px}
.textarea-comment:focus{height:45px; width: 100px;}
.table th.sorted{color:#15428b;}
.hidden { display: none;}
#dialogoverlay{ display: none; opacity:.8; position: fixed; top: 0px;left: 0px; background: #FFF;z-index: 10;}
#dialogbox{ display: none;  position: fixed;background: #9ac2f7;border-radius:7px; width:350px; z-index: 10;}
#dialogbox > div{ background:#9ac2f7; margin:5px; }
#dialogbox > div > #dialogboxhead{ background:url("images/bg.png") repeat scroll 0 0 rgba(0, 0, 0, 0); font-size:19px; padding:14px; color:#000;border-bottom:2px solid #e1e1e1; }
#dialogbox > div > #dialogboxbody{ background:#f0f1f1; padding:20px; color:#000; }
#dialogbox > div > #dialogboxfoot{ background: #f0f1f1; padding:10px; text-align:center; }
.table tbody tr:hover, .table tr.over, .contribTable tr:hover div {background-color:transparent !important;}
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
        <%@ include file="/common/formCalender.js"%>
    </script>
   <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    <title><fmt:message key="operationResource.title" /></title>
  <meta name="heading" content="<fmt:message key='operationResource.heading'/>" />
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<c:set var="buttons"> 
    <input type="button" id="btnTransfer" name="btnTransfer" class="cssbutton1" onclick="createWorkTicket();" value="Create Work Ticket" style="width:160px; height:25px" tabindex="83"/> 
</c:set>
<script type="text/javascript">
 window.onload =function ModifyPlaceHolder () {     
     <c:forEach var="entry" items="${resourceTransferOI}">
     var id="${entry.id}";
     document.getElementById ('startTime'+id).value='00:00';
     var type = document.getElementById ('type'+id).value
     if(type=='T'){
         document.getElementById ('service'+id).value='TPS'; 
         document.getElementById ('warehouse'+id).value='04'; 
     var input = document.getElementById ('ban'+id);
     input.placeholder = "Vendor Search";
     }else
         {
         document.getElementById ('service'+id).value='${serviceFromSystemDefault}';
         document.getElementById ('warehouse'+id).value='${warehouseFromSystemDefault}';
         }
</c:forEach>
 } 
function getHTTPObject() {
    var xmlhttp;
    if(window.XMLHttpRequest)  {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp) {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        } }
    return xmlhttp;
}
function StatusCheckForResourceTransferOI(target)
{
    var userCheckStatus = target.value
    alert(userCheckStatus)
    }
</script>
<script type="text/javascript">
function getOIValue(id){
    var delimeter = "-,-";
      var values = id;
      values = values + delimeter + validateFieldValueForDate('date'+id);//01
      values = values + delimeter + validateFieldValueBoolean('am'+id);//2
      values = values + delimeter + validateFieldValueBoolean('pm'+id);//3
      values = values + delimeter + validateFieldValueString('service'+id); //4
      values = values + delimeter + validateFieldValueForDate('warehouse'+id);//05
      values = values + delimeter + validateFieldValueString('instructions'+id);//06
      values = values + delimeter + validateFieldValueString('workOrderOI'+id);//07
      values = values + delimeter + validateFieldValueString('type'+id);//08
      if(document.getElementById('type'+id).value =='T' && document.getElementById('service'+id).value == 'TPS'  )
          {
      values = values + delimeter + validateFieldValueString('ban'+id);//09
      values = values + delimeter + validateFieldValueString('bac'+id);//10
      values = values + delimeter + validateFieldValueString('startTime'+id);//11
      values = values + delimeter + validateFieldValueString('scope'+id);//12
          }else
              {
              values = values + delimeter + validateFieldValueString('ban'+id);//09
              values = values + delimeter + validateFieldValueString('bac'+id);//10
              values = values + delimeter + validateFieldValueString('checkLimit'+id);//11
              values = values + delimeter + validateFieldValueString('startTime'+id);//12
              values = values + delimeter + validateFieldValueString('scope'+id);//13
              }
      return values;
}

function validateFieldValueString(fieldId){
      var result1 =" ";
      try {
          var result = document.getElementById(fieldId);
          if(result == null || result ==  undefined  || result.value ==  ''){
                 return result1;
             }else{
                 return result.value;
             }
      }catch(e) {
          return result1;
      }
}
function validateFieldValueBoolean(fieldId){
      var result1 =" ";
      try {
          var result = document.getElementById(fieldId).checked;
          if(result==true)
            {
            return  result='true';
            }else{
            return  result='false';
            }

      }catch(e) {
          return result1;
      }
}

function validateFieldValueForDate(fieldId){
      var result1 =null;
      try {
          var result = document.getElementById(fieldId);
          if(result == null || result ==  undefined  || result.value ==  ''){
                 return result1;
             }else{
                 return result.value;
             }
      }catch(e) {
          return result1;
      }
}
function closeMyDiv(autocompleteDivId,partnerNameId,paertnerCodeId){    
    document.getElementById(autocompleteDivId).style.display = "none";
    if(document.getElementById(paertnerCodeId).value==''){
        document.getElementById(partnerNameId).value="";    
    }
    }
</script>

<script>
function checkMsg(target)
{
    document.getElementById(target.id).style.height = "17px";

}
function checkMsg1(target)
{
    document.getElementById(target.id).style.height = "45px";
    document.getElementById(target.id).style.width = "200px;";

}
</script>

<script type="text/javascript">
function CustomAlert(){
    this.render = function(dialog){
        var winW = window.innerWidth;
        var winH = window.innerHeight;
        var dialogoverlay = document.getElementById('dialogoverlay');
        var dialogbox = document.getElementById('dialogbox');
        dialogoverlay.style.display = "block";
        dialogoverlay.style.height = winH+"px";
        dialogbox.style.left = (winW/2) - (550 * .5)+"px";
        dialogbox.style.top = "200px";
        dialogbox.style.display = "block";
        document.getElementById('dialogboxhead').innerHTML = "";
        document.getElementById('dialogboxbody').innerHTML = dialog;
        document.getElementById('dialogboxfoot').innerHTML = '<button class="cssbutton"  style="border-radius:3px;width:55px;">OK</button>';
    }
    this.ok = function(){
        document.getElementById('dialogbox').style.display = "none";
        document.getElementById('dialogoverlay').style.display = "none";
    }
}
var Alert;
</script>

<script type="text/javascript">
function createWorkTicket(){
    var tempForStatus='N';
    var delimeter1 = "-@-";
     var delimeter = "-,-";
     var oiValue = '';
     var alertMsg = "";
     <c:forEach var="entry" items="${resourceTransferOI}">
     var id="${entry.id}";
     var workNo=document.getElementById('checkboxId'+id).checked;
     var wo = document.getElementById ('workOrderOI'+id).value;
     var type = document.getElementById ('type'+id).value;
     var ban = document.getElementById ('ban'+id).value;
     var date = document.getElementById ('date'+id).value;
     var service = document.getElementById ('service'+id).value;
     var warehouse = document.getElementById ('warehouse'+id).value;
     var flag = document.getElementById ('checkLimit'+id).value;
     if(flag =='P')
     {
         if(alertMsg == "")
             {
            // alertMsg = "Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit.";
             /* alert("Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit."); */
             }else{
                 //alertMsg=  alertMsg+"\n"+ "Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit.";
             }
             }
     if(flag =='F')
         {
            tempForStatus='Y';  
         }
     if(flag=='T')
        {
         if(alertMsg == "")
         {
         //alertMsg = "The hub limit for resource is exceeding the limit for "+wo+". Ticket for "+wo+" will be sent in Dispatch Queue for Approval.";
         /* alert("Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit."); */
         }else{
             //alertMsg=  alertMsg+"\n"+ "The hub limit for resource is exceeding the limit for "+wo+". Ticket for "+wo+" will be sent in Dispatch Queue for Approval.";
         }
         
         
        tempForStatus='Y';  
         /* alert("The hub limit for resource is exceeding the limit for "+wo+". Ticket for "+wo+" will be sent in Dispatch Queue for Approval."); */ 
        }
         if(flag=='Y')
        {
             if(alertMsg == "")
             {
             //alertMsg = "Ticket for "+wo+" is exceeding the limit. Ticket will be sent in Dispatch Queue for Approval.";
             /* alert("Ticket is being saved for "+wo+" but your date "+date+" are close to the operational service limit."); */
             }else{
                 //alertMsg=  alertMsg+"\n"+ "Ticket for "+wo+" is exceeding the limit. Ticket will be sent in Dispatch Queue for Approval.";
             }
             
             tempForStatus='Y'; 
             /* alert("Ticket for "+wo+" is exceeding the limit. Ticket will be sent in Dispatch Queue for Approval.");  */
        }
         if(flag !='T' && flag !='Y' && flag !='F')
             {
             var tempForStatus='N';
             }
        
         if(workNo == true)
         {
      if((date == null || date == ''))
     {
     alert("Date is Required Field. Please fill Date " +wo);
     return false;
     }else{ 
     if(type == 'T' && (ban == null || ban ==''))
         {
         alert("Please fill Third Party Vendor to create the work ticket for "+wo)
         return false;
         } }
         }
     if(workNo == true){
        if(oiValue == ''){
            oiValue  =  getOIValue(id.trim());
        }else{
            oiValue  =  oiValue +delimeter1+ getOIValue(id.trim());
        }}
    </c:forEach>
    if(alertMsg != "")
     {
   //  alert(alertMsg)
     }
    if(oiValue==null ||oiValue == '' )
        {
        alert("Please Select Any Work Order");
        }
   $.ajax({
          type: "POST",
          url: "createWorkTicketFromOI.html?ajax=1&decorator=simple&popup=true",
          data: { id: '${id}',shipNumber:'${shipNumber}', oiValue:oiValue, delimeter:delimeter, delimeter1:delimeter1 ,tempForStatus:tempForStatus,emailFlag:true },
          success: function (data, textStatus, jqXHR) {
          },
        error: function (data, textStatus, jqXHR) {
                }
        }); 
    if(oiValue!=null && oiValue != '' )
        {
        
        closeWIN();
        } 

}
</script>
<script type="text/javascript">
function onlyTimeFormatAllowed(evt)
    {
      var keyCode = evt.which ? evt.which : evt.keyCode;
      return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
    }
    
function completeTimeString(id)
{
    var stime3=document.getElementById ('startTime'+id).value;
    
    if(stime3.substring(stime3.indexOf(":")+1,stime3.length) == "" || stime3.length==1 || stime3.length==2){
        if(stime3.length==1 || stime3.length==2){
            if(stime3.length==2){
                document.getElementById ('startTime'+id).value = stime3 + ":00";
            }
            if(stime3.length==1){
                document.getElementById ('startTime'+id).value = "0" + stime3 + ":00";
            }
        }else{
            document.getElementById ('startTime'+id).value = stime3 + "00";
        }
    }else{
        if(stime3.indexOf(":") == -1 && stime3.length==3){
            document.getElementById ('startTime'+id).value = "0" + stime3.substring(0,1) + ":" + stime3.substring(1,stime3.length);
        }
        if(stime3.indexOf(":") == -1 && (stime3.length==4 || stime3.length==5) ){
            document.getElementById ('startTime'+id).value = stime3.substring(0,2) + ":" + stime3.substring(2,4);
        }
        if(stime3.indexOf(":") == 1){
            document.getElementById ('startTime'+id).value = "0" + stime3;
        }
    }
    var startTlmeValue=document.getElementById ('startTime'+id).value;
    var t = startTlmeValue.split(':');

      if( t[0] >= 0 && t[0] < 24 && t[1] >= 0 && t[1] < 60)
        {
        document.getElementById ('startTime'+id).value=startTlmeValue;;
        }else{
        if(t[0] < 0 || t[0] >= 24)
        {
        alert("Hour must be between 0 and 23");
        }else
        if(t[1] < 0 || t[1] >= 60){
        alert("Minute must be between 0 and 59");
        }
        document.getElementById ('startTime'+id).value='00:00';
        }
      }
</script>
<script type="text/javascript">
function checkLimitOfWorkTicket(id,wo,date,service,warehouse)
{
    var found='yes';
 var dataForCheckLimit = service+','+warehouse+","+date+","+wo;
 $.ajax({
      type: "POST",
      url: "checkLimitForOI.html?ajax=1&decorator=simple&popup=true",
      data: { id: '${id}',shipNumber:'${shipNumber}',dataForCheckLimit:dataForCheckLimit},
      success: function (data, textStatus, jqXHR) {
             data = data.trim(); 
             if(data=='thresholdMsg')
                 {
                 document.getElementById ('checkLimit'+id).value="P";
                 }else if(data.length>0)
                {
                var checkForMsg =data.split(",")[0];
                 if(data.trim() == "checkMinimumServiceDays" )
                     {
                     document.getElementById ('checkLimit'+id).value="F";
                     }else
                   if(checkForMsg.trim() == "exceedLimit" )
                    {
                if(parseInt(data.split(",")[1].trim()) < 0)
                {
                        document.getElementById ('checkLimit'+id).value="T";
                 }else if(parseInt(data.split(",")[1].trim()) == 0){
                     document.getElementById ('checkLimit'+id).value="A";
                 }
                else{
                     document.getElementById ('checkLimit'+id).value="F";
                 }  
                }
            else  if(parseInt(data) <= 0)
             {
                    document.getElementById ('checkLimit'+id).value="Y";
             }else{
                 document.getElementById ('checkLimit'+id).value="N";
             }
                }
      },
  error: function (data, textStatus, jqXHR) {
      document.getElementById ('checkLimit'+id).value="N";
            }
    }); 
}

 function fillCheckBox(id)
{
    setFieldId(id);
    document.getElementById('checkboxId'+id).checked = true;
} 
</script>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:form name="resourceTransferFormForOI" id="resourceTransferFormForOI" action="" method="post" validate="true">
<s:hidden name="userCheck"/> 
<div id="layer1" style="width:100%; margin-top: 10px;">
<div id="newmnav">
            <ul>
             <li id="newmnav1" style="background:#FFF"><a class="current"><span>Resource Transfer<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
           </ul>
       </div><div class="spn" >&nbsp;</div>
      

<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
<tbody>
<tr>
<td>
<div id="content" align="center" >
<div id="dialogoverlay"></div>
<div id="dialogbox">
  <div>
    <div id="dialogboxhead"></div>
    <div id="dialogboxbody"></div>
    <div id="dialogboxfoot"></div>
  </div>
</div>
<div id="liquid-round">
   <div class="top" style="margin-top: 0px;!margin-top: 0px;"><span></span></div>
   <div class="center-content">
    <table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="100%">
    <tbody>
    <tr>
    <td>
    <c:if test="${not empty resourceTransferOI}">
    <s:set name="resourceTransferOI" value="resourceTransferOI" scope="request"/>
    <display:table name="resourceTransferOI" class="table" requestURI="" id="resourceTransferOI" style="width:100%;">
    <display:column title=" " style="width:5px;"><input type="checkbox" id="checkboxId${resourceTransferOI.id}" name="DD" value="${resourceTransferOI.id}" /></display:column>
   <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="type${resourceTransferOI.id}" name="type${resourceTransferOI.id}" value="${resourceTransferOI.type}"> 
    </display:column>
     <display:column  title="WO#"  style="width:65px">
     <c:if test="${resourceTransferOI.type == 'T'}">
     <input type="text" class="input-text pr-f11" name="workOrderOI${resourceTransferOI.id}" id="workOrderOI${resourceTransferOI.id}"  value="${resourceTransferOI.workorder}" style="width:40px" disabled="true"/>
     <img  class="openpopup" title="<c:out value="${resourceTransferOI.description}"></c:out>"   style="text-align:right;vertical-align:inherit;" src="<c:url value='/images/TP-arrow-bg.png'/>" />
    </c:if>
    
     <c:if test="${resourceTransferOI.type != 'T'}">
     <input type="text" class="input-text pr-f11" name="workOrderOI${resourceTransferOI.id}" id="workOrderOI${resourceTransferOI.id}"  value="${resourceTransferOI.workorder}" style="width:40px" disabled="true"/>
    </c:if>
    </display:column>
    
    <display:column  title='Date' style="width:82px">
    <fmt:parseDate pattern="yyyy-MM-dd" value="${resourceTransferOI.date}" var="parsedAtDepartDate" />
    <fmt:formatDate pattern="dd-MMM-yy" value="${parsedAtDepartDate}" var="formattedAtDepartDate" /> 
    <s:textfield cssClass="input-textUpper pr-f11" id="date${resourceTransferOI.id}" name="date${resourceTransferOI.id}" value="${formattedAtDepartDate}" onkeydown="" required="true" cssStyle="width:55px" maxlength="11" readonly="true" tabindex="72"/>
    <img id="date${resourceTransferOI.id}-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" textelementname="date${resourceTransferOI.id}" HEIGHT=20 WIDTH=20 onclick="fillCheckBox('${resourceTransferOI.id}')"/>
    <s:hidden id="checkLimit${resourceTransferOI.id}" name="checkLimit${resourceTransferOI.id}"/>
    </display:column>
    
    <display:column  title="Schedule" style="width:95px;">
    AM <input type="checkbox" id="am${resourceTransferOI.id}" name="am${resourceTransferOI.id}"  value="" class="input-textUpper pr-f11" style="vertical-align:middle;">
  &nbsp;&nbsp;PM <input type="checkbox" id="pm${resourceTransferOI.id}" name="pm${resourceTransferOI.id}" class="checkSID" class="input-textUpper pr-f11" style="vertical-align:middle;">
    </display:column>
    
     <display:column  title='Start Time' style="width:83px">
     <%-- <input type="" class="input-textUpper pr-f11"  name="startTime" id="startTime${resourceTransferOI.id}"  size="8" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)"/><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" > --%>
   <s:textfield cssClass="input-text" cssStyle="text-align:right" name="startTime" id="startTime${resourceTransferOI.id}" size="3" maxlength="5" onkeydown="return onlyTimeFormatAllowed(event)" onchange="completeTimeString('${resourceTransferOI.id}');"/><img src="${pageContext.request.contextPath}/images/time.png" HEIGHT=20 WIDTH=40 align="top" style="cursor:default;" >
    </display:column>
    
    <c:if test="${resourceTransferOI.type == 'T'}">
    <display:column  title="Vendor" style="width:100px"> 
   <s:textfield cssClass="input-text pr-f11" id="ban${resourceTransferOI.id}" name="serviceOrder.bookingAgentName"  onkeyup="findPartnerDetails('ban${resourceTransferOI.id}','bac${resourceTransferOI.id}','soAShishBookingAgentNameDivId${resourceTransferOI.id}',' and (isAccount=true or isAgent=true or isVendor=true )','',event);" onchange="findPartnerDetailsByName('soAShishBookingAgentNameDivId${resourceTransferOI.id}','ban${resourceTransferOI.id}');"  maxlength="180"  cssStyle="width:150px"  tabindex="" />
    <div id="soAShishBookingAgentNameDivId${resourceTransferOI.id}" name ="soAShishBookingAgentNameDivId${resourceTransferOI.id}" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;font-size:0.8em;"></div>
    </display:column>
   </c:if>
   <c:if test="${resourceTransferOI.type != 'T'}">
    <display:column  title="Vendor" style="width:100px"> 
   <s:textfield cssClass="input-textUpper" id="ban${resourceTransferOI.id}" name="serviceOrder.bookingAgentName"    maxlength="180"  cssStyle="width:150px"  tabindex="" disabled="true"/>
    <!-- <div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div> -->
    </display:column>
    </c:if>
    
    <display:column title="Service" style="width:80px;"> 
    <select id="service${resourceTransferOI.id}" name ="service${resourceTransferOI.id}" style="width:120px" class="list-menu pr-f11" onchange="checkLimitFoTicketWoDate('${resourceTransferOI.id}');"> 
    <c:forEach var="chrms" items="${tcktservc}" varStatus="loopStatus">
    <option value="<c:out value='${chrms.key}' />" selected="selected">
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach> 
    </select>
    </display:column>
    <display:column title="Warehouse" style="width:60px;"> 
    <select id="warehouse${resourceTransferOI.id}" name ="warehouse${resourceTransferOI.id}" style="width:80px" class="list-menu pr-f11" onchange="checkLimitFoTicketWoDate('${resourceTransferOI.id}');"> 
    <c:forEach var="chrms" items="${house}" varStatus="loopStatus">
    <option value="<c:out value='${chrms.key}' />" selected="03">
    <c:out value="${chrms.value}"></c:out>
    </option>
    </c:forEach> 
    </select>
    </display:column>    
    <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="bac${resourceTransferOI.id}" name="serviceOrder.bookingAgentCode" "> 
    </display:column>
    <display:column title="" class="hidden" headerClass="hidden" > 
    <input type="hidden" id="id${resourceTransferOI.id}" name="id${resourceTransferOI.id}"  value="${resourceTransferOI.id}"> 
    </display:column>
    <display:column  title="Instructions" style="width:100px"> 
    <textarea  class="textarea-comment pr-f11" id="instructions${resourceTransferOI.id}" name="instructions${resourceTransferOI.id}"  onmouseover="" onmouseout="checkMsg(this);" onclick="checkMsg1(this)"></textarea>
   <%-- <textarea  id="instructions${resourceTransferOI.id}" name="instructions${resourceTransferOI.id}" value="" onselect="discriptionView('instructions${resourceTransferOI.id}')" onblur="discriptionView1('instructions${resourceTransferOI.id}')" cols="15" rows="1" class="input-text pr-f11" style="overflow:auto;width:140px;"  ></textarea> --%>
    </display:column>
    <%--<display:column property="date1"  title="Begin Date" maxLength="25" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="date2"  title="End Date" maxLength="25" format="{0,date,dd-MMM-yyyy}" /> --%>
     <display:column title=" " class="hidden" headerClass="hidden"><input type="text" id="scope${resourceTransferOI.id}" name="scope" value="${resourceTransferOI.scopeOfWorkOrder}" /></display:column>
</display:table>
</c:if>

<c:if test="${empty resourceTransferOI}">
<display:table name="resourceTransferOI" class="table" requestURI="" id="resourceTransferOI" defaultsort="1"  style="width:100%;">
    <display:column title=" " style="width:5px;"><input type="checkbox" id="checkboxId${resourceTransferOI.id}" name="DD" value="${resourceTransferOI.id}" /></display:column>
   <display:column title="" class="hidden" headerClass="hidden" > 
    </display:column>
    <display:column  title="WO#"  style="width:30px">
    </display:column>
    
    <display:column  title='Date' style="width:90px">
    </display:column>
    
    <display:column  title="Schedule" style="width:110px;"></display:column>
    <display:column  title="Vendor" style="width:100px"> 
    </display:column>
    
    <display:column title="Service" style="width:80px;"> 
    </display:column>
    <display:column title="Warehouse" style="width:60px;"> 
    </display:column>    
    <display:column title="" class="hidden" headerClass="hidden" > 
    </display:column>
    <display:column title="" class="hidden" headerClass="hidden" > 
    </display:column>
   
    <display:column  title="Instructions" style="width:100px"> 
    </display:column>
</display:table>
</c:if>


</td>
</tr>

<tr>
 <td align="center" class="listwhitetext" colspan="5" valign="bottom" style="">                     
<c:out value="${buttons}" escapeXml="false" />                  
</td>
</tr>   
</tbody>
</table>
</div>
<div class="bottom-header" style="margin-top:50px;"><span></span></div>
</div>
</div> 
</div>
</td>
</tr>
</tbody>
</table> 
 
</div>

</s:form> 
<script type="text/javascript"> 
setOnSelectBasedMethods(["checkLimitFoTicket()"]);
setCalendarFunctionality(); 
setCalendarFunctionality(); 
</script>
<script type="text/javascript">
 var currentClickId;
function setFieldId(id){
    currentClickId = id;
}
function checkLimitFoTicket(){
    if(currentClickId!=undefined && currentClickId!=null && currentClickId!=''){
        var typeService= "${typeService}";
        var id=currentClickId;
         var workNo=document.getElementById('checkboxId'+id).checked;
         var wo = document.getElementById ('workOrderOI'+id).value;
         var type = document.getElementById ('type'+id).value;
         var ban = document.getElementById ('ban'+id).value;
         var date = document.getElementById ('date'+id).value;
         var service = document.getElementById ('service'+id).value;
         var warehouse = document.getElementById ('warehouse'+id).value;
        if(typeService.indexOf(service) > -1)
        {
         checkLimitOfWorkTicket(id,wo,date,service,warehouse);
        }
    }   
}

function checkLimitFoTicketWoDate(id){
        var typeService= "${typeService}";
        var workNo=document.getElementById('checkboxId'+id).checked;
         var wo = document.getElementById ('workOrderOI'+id).value;
         var type = document.getElementById ('type'+id).value;
         var ban = document.getElementById ('ban'+id).value;
         var date = document.getElementById ('date'+id).value;
         var service = document.getElementById ('service'+id).value;
         var warehouse = document.getElementById ('warehouse'+id).value;
        if(date != null && date != ''){
        if(typeService.indexOf(service) > -1)
        {
         checkLimitOfWorkTicket(id,wo,date,service,warehouse);
        }}
}

document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentName'].value = ""
function findBookingAgentName(){
    var bookingAgentCode = document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].value;
    if(bookingAgentCode == ''){
        document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentName'].value = "";
    }
    if(bookingAgentCode!=''){
        try{        
    document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentVanlineCode'].value = "";
        }catch(e){}
    if(bookingAgentCode != ''){
    /* showOrHide(1) */
     var url="BookingAgentName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(bookingAgentCode);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
    }   } }
function handleHttpResponse4(){
        if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                    if(res[2] == 'Approved'){
                        document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentName'].value = res[1];
                        document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].select();
                    }else{
                        alert("Booking Agent code is not approved" ); 
                        document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentName'].value="";
                     document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].value="";
                     document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].select();
                    }  
                }else{
                    showOrHide(0);
                     alert("Booking Agent code not valid");
                     document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentName'].value="";
                     document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].value="";
                     document.forms['resourceTransferFormForOI'].elements['serviceOrder.bookingAgentCode'].select();
               } } 
        } 
        
function copyPartnerDetails(partnercode,lastName,partnerNameId,paertnerCodeId,autocompleteDivId){
    lastName=lastName.replace("~","'");
    document.getElementById(partnerNameId).value=lastName;
    document.getElementById(paertnerCodeId).value=partnercode;
    document.getElementById(autocompleteDivId).style.display = "none";  
     if(paertnerCodeId=='soBookingAgentCodeId'){
        findBookingAgentName();
    }  
}

function discriptionView(discription){ 
     document.getElementById(discription).style.height="50px"; 
     } 
      
     function discriptionView1(discription){ 
     document.getElementById(discription).style.height="15px"; 
     SetCursorToTextEnd(discription);
     }
     function SetCursorToTextEnd(textControlID) 
       { 
        var text = document.getElementById(textControlID); 
        text.scrollTop = 1;
       }
</script>