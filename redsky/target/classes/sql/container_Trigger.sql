DELIMITER $$
DROP trigger IF EXISTS `redsky`.`trigger_add_history_container` $$
create trigger redsky.trigger_add_history_container BEFORE UPDATE on redsky.container
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
      IF (OLD.sequenceNumber <> NEW.sequenceNumber ) THEN
       select CONCAT(fieldNameList,'container.sequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sequenceNumber,'~') into newValueList;
   END IF;

  

   IF (OLD.ship <> NEW.ship ) THEN
       select CONCAT(fieldNameList,'container.ship~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ship,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ship,'~') into newValueList;
   END IF;

   IF (OLD.shipNumber <> NEW.shipNumber ) THEN
       select CONCAT(fieldNameList,'container.shipNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.shipNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.shipNumber,'~') into newValueList;
   END IF;

   IF (OLD.cntnrNumber <> NEW.cntnrNumber ) THEN
       select CONCAT(fieldNameList,'container.cntnrNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.cntnrNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.cntnrNumber,'~') into newValueList;
   END IF;

   IF (OLD.numberOfContainer <> NEW.numberOfContainer ) THEN
       select CONCAT(fieldNameList,'container.numberOfContainer~') into fieldNameList;
       select CONCAT(oldValueList,OLD.numberOfContainer,'~') into oldValueList;
       select CONCAT(newValueList,NEW.numberOfContainer,'~') into newValueList;
   END IF;

   IF (OLD.containerNumber <> NEW.containerNumber ) THEN
       select CONCAT(fieldNameList,'container.containerNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.containerNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.containerNumber,'~') into newValueList;
   END IF;

   IF (OLD.sealNumber <> NEW.sealNumber ) THEN
       select CONCAT(fieldNameList,'container.sealNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.sealNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.sealNumber,'~') into newValueList;
   END IF;

   IF (OLD.customSeal <> NEW.customSeal ) THEN
       select CONCAT(fieldNameList,'container.customSeal~') into fieldNameList;
       select CONCAT(oldValueList,OLD.customSeal,'~') into oldValueList;
       select CONCAT(newValueList,NEW.customSeal,'~') into newValueList;
   END IF;

   IF (OLD.size <> NEW.size ) THEN
       select CONCAT(fieldNameList,'container.size~') into fieldNameList;
       select CONCAT(oldValueList,OLD.size,'~') into oldValueList;
       select CONCAT(newValueList,NEW.size,'~') into newValueList;
   END IF;

   IF ((OLD.grossWeight <> NEW.grossWeight) or (OLD.grossWeight is null and NEW.grossWeight is not null) 
	  or (OLD.grossWeight is not null and NEW.grossWeight is null)) THEN
		select CONCAT(fieldNameList,'container.grossWeight~') into fieldNameList;
		IF(OLD.grossWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.grossWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.grossWeight is not null) then
			select CONCAT(oldValueList,OLD.grossWeight,'~') into oldValueList;
		END IF;
		IF(NEW.grossWeight is not null) then
			select CONCAT(newValueList,NEW.grossWeight,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.emptyContWeight <> NEW.emptyContWeight) or (OLD.emptyContWeight is null and NEW.emptyContWeight is not null) 
	  or (OLD.emptyContWeight is not null and NEW.emptyContWeight is null)) THEN
		select CONCAT(fieldNameList,'container.emptyContWeight~') into fieldNameList;
		IF(OLD.emptyContWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.emptyContWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.emptyContWeight is not null) then
			select CONCAT(oldValueList,OLD.emptyContWeight,'~') into oldValueList;
		END IF;
		IF(NEW.emptyContWeight is not null) then
			select CONCAT(newValueList,NEW.emptyContWeight,'~') into newValueList;
		END IF; 
	END IF;
	
    IF ((OLD.netWeight <> NEW.netWeight) or (OLD.netWeight is null and NEW.netWeight is not null) 
	  or (OLD.netWeight is not null and NEW.netWeight is null)) THEN
		select CONCAT(fieldNameList,'container.netWeight~') into fieldNameList;
		IF(OLD.netWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.netWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.netWeight is not null) then
			select CONCAT(oldValueList,OLD.netWeight,'~') into oldValueList;
		END IF;
		IF(NEW.netWeight is not null) then
			select CONCAT(newValueList,NEW.netWeight,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.volume <> NEW.volume) or (OLD.volume is null and NEW.volume is not null) 
	  or (OLD.volume is not null and NEW.volume is null)) THEN
		select CONCAT(fieldNameList,'container.volume~') into fieldNameList;
		IF(OLD.volume is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.volume is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.volume is not null) then
			select CONCAT(oldValueList,OLD.volume,'~') into oldValueList;
		END IF;
		IF(NEW.volume is not null) then
			select CONCAT(newValueList,NEW.volume,'~') into newValueList;
		END IF; 
	END IF;
	
    IF ((OLD.pieces <> NEW.pieces) or (OLD.pieces is null and NEW.pieces is not null) 
	  or (OLD.pieces is not null and NEW.pieces is null)) THEN
		select CONCAT(fieldNameList,'container.pieces~') into fieldNameList;
		IF(OLD.pieces is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.pieces is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.pieces is not null) then
			select CONCAT(oldValueList,OLD.pieces,'~') into oldValueList;
		END IF;
		IF(NEW.pieces is not null) then
			select CONCAT(newValueList,NEW.pieces,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.useDsp <> NEW.useDsp) or (OLD.useDsp is null and NEW.useDsp is not null) 
	  or (OLD.useDsp is not null and NEW.useDsp is null)) THEN
		select CONCAT(fieldNameList,'container.useDsp~') into fieldNameList;
		IF(OLD.useDsp is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.useDsp is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.useDsp is not null) then
			select CONCAT(oldValueList,OLD.useDsp,'~') into oldValueList;
		END IF;
		IF(NEW.useDsp is not null) then
			select CONCAT(newValueList,NEW.useDsp,'~') into newValueList;
		END IF; 
	END IF;

	IF ((OLD.density <> NEW.density) or (OLD.density is null and NEW.density is not null) 
	  or (OLD.density is not null and NEW.density is null)) THEN
		select CONCAT(fieldNameList,'container.density~') into fieldNameList;
		IF(OLD.density is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.density is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.density is not null) then
			select CONCAT(oldValueList,OLD.density,'~') into oldValueList;
		END IF;
		IF(NEW.density is not null) then
			select CONCAT(newValueList,NEW.density,'~') into newValueList;
		END IF; 
	END IF;


   IF (OLD.totalLine <> NEW.totalLine ) THEN
       select CONCAT(fieldNameList,'container.totalLine~') into fieldNameList;
       select CONCAT(oldValueList,OLD.totalLine,'~') into oldValueList;
       select CONCAT(newValueList,NEW.totalLine,'~') into newValueList;
   END IF;

   IF (OLD.idNumber <> NEW.idNumber ) THEN
       select CONCAT(fieldNameList,'container.idNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.idNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.idNumber,'~') into newValueList;
   END IF;

   IF ((OLD.topier <> NEW.topier) or (OLD.topier is null and NEW.topier is not null)
	  or (OLD.topier is not null and NEW.topier is null)) THEN
       select CONCAT(fieldNameList,'container.topier~') into fieldNameList;
       IF(OLD.topier is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.topier is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.topier is not null) THEN
        	select CONCAT(oldValueList,OLD.topier,'~') into oldValueList;
     	END IF;
      	IF(NEW.topier is not null) THEN
        	select CONCAT(newValueList,NEW.topier,'~') into newValueList;
      	END IF;
   END IF;

   IF ((OLD.pickup <> NEW.pickup) or (OLD.pickup is null and NEW.pickup is not null)
	  or (OLD.pickup is not null and NEW.pickup is null)) THEN
       select CONCAT(fieldNameList,'container.pickup~') into fieldNameList;
       IF(OLD.pickup is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.pickup is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.pickup is not null) THEN
        	select CONCAT(oldValueList,OLD.pickup,'~') into oldValueList;
     	END IF;
      	IF(NEW.pickup is not null) THEN
        	select CONCAT(newValueList,NEW.pickup,'~') into newValueList;
      	END IF;
   END IF;

   IF ((OLD.deliver <> NEW.deliver) or (OLD.deliver is null and NEW.deliver is not null)
	  or (OLD.deliver is not null and NEW.deliver is null)) THEN
       select CONCAT(fieldNameList,'container.deliver~') into fieldNameList;
       IF(OLD.deliver is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.deliver is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.deliver is not null) THEN
        	select CONCAT(oldValueList,OLD.deliver,'~') into oldValueList;
     	END IF;
      	IF(NEW.deliver is not null) THEN
        	select CONCAT(newValueList,NEW.deliver,'~') into newValueList;
      	END IF;
   END IF;

   IF (OLD.tripNumber <> NEW.tripNumber ) THEN
       select CONCAT(fieldNameList,'container.tripNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.tripNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.tripNumber,'~') into newValueList;
   END IF;
      IF (OLD.unit1 <> NEW.unit1 ) THEN
       select CONCAT(fieldNameList,'container.unit1~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit1,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit1,'~') into newValueList;
   END IF;


   IF (OLD.unit2 <> NEW.unit2 ) THEN
       select CONCAT(fieldNameList,'container.unit2~') into fieldNameList;
       select CONCAT(oldValueList,OLD.unit2,'~') into oldValueList;
       select CONCAT(newValueList,NEW.unit2,'~') into newValueList;
   END IF;



  IF ((OLD.grossWeightTotal <> NEW.grossWeightTotal) or (OLD.grossWeightTotal is null and NEW.grossWeightTotal is not null) 
	  or (OLD.grossWeightTotal is not null and NEW.grossWeightTotal is null)) THEN
		select CONCAT(fieldNameList,'container.grossWeightTotal~') into fieldNameList;
		IF(OLD.grossWeightTotal is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.grossWeightTotal is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.grossWeightTotal is not null) then
			select CONCAT(oldValueList,OLD.grossWeightTotal,'~') into oldValueList;
		END IF;
		IF(NEW.grossWeightTotal is not null) then
			select CONCAT(newValueList,NEW.grossWeightTotal,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.totalNetWeight <> NEW.totalNetWeight) or (OLD.totalNetWeight is null and NEW.totalNetWeight is not null) 
	  or (OLD.totalNetWeight is not null and NEW.totalNetWeight is null)) THEN
		select CONCAT(fieldNameList,'container.totalNetWeight~') into fieldNameList;
		IF(OLD.totalNetWeight is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.totalNetWeight is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.totalNetWeight is not null) then
			select CONCAT(oldValueList,OLD.totalNetWeight,'~') into oldValueList;
		END IF;
		IF(NEW.totalNetWeight is not null) then
			select CONCAT(newValueList,NEW.totalNetWeight,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.totalVolume <> NEW.totalVolume) or (OLD.totalVolume is null and NEW.totalVolume is not null) 
	  or (OLD.totalVolume is not null and NEW.totalVolume is null)) THEN
		select CONCAT(fieldNameList,'container.totalVolume~') into fieldNameList;
		IF(OLD.totalVolume is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.totalVolume is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.totalVolume is not null) then
			select CONCAT(oldValueList,OLD.totalVolume,'~') into oldValueList;
		END IF;
		IF(NEW.totalVolume is not null) then
			select CONCAT(newValueList,NEW.totalVolume,'~') into newValueList;
		END IF; 
	END IF;

   IF ((OLD.totalPieces <> NEW.totalPieces) or (OLD.totalPieces is null and NEW.totalPieces is not null) 
	  or (OLD.totalPieces is not null and NEW.totalPieces is null)) THEN
		select CONCAT(fieldNameList,'container.totalPieces~') into fieldNameList;
		IF(OLD.totalPieces is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.totalPieces is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.totalPieces is not null) then
			select CONCAT(oldValueList,OLD.totalPieces,'~') into oldValueList;
		END IF;
		IF(NEW.totalPieces is not null) then
			select CONCAT(newValueList,NEW.totalPieces,'~') into newValueList;
		END IF; 
	END IF;

     IF (OLD.serviceOrderId <> NEW.serviceOrderId ) THEN
       select CONCAT(fieldNameList,'container.serviceOrderId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.serviceOrderId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.serviceOrderId,'~') into newValueList;
   END IF;
    IF ((OLD.grossWeightKilo <> NEW.grossWeightKilo) or (OLD.grossWeightKilo is null and NEW.grossWeightKilo is not null) 
	  or (OLD.grossWeightKilo is not null and NEW.grossWeightKilo is null)) THEN
		select CONCAT(fieldNameList,'container.grossWeightKilo~') into fieldNameList;
		IF(OLD.grossWeightKilo is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.grossWeightKilo is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.grossWeightKilo is not null) then
			select CONCAT(oldValueList,OLD.grossWeightKilo,'~') into oldValueList;
		END IF;
		IF(NEW.grossWeightKilo is not null) then
			select CONCAT(newValueList,NEW.grossWeightKilo,'~') into newValueList;
		END IF; 
	END IF;
	 IF ((OLD.emptyContWeightKilo <> NEW.emptyContWeightKilo) or (OLD.emptyContWeightKilo is null and NEW.emptyContWeightKilo is not null) 
	  or (OLD.emptyContWeightKilo is not null and NEW.useDsp is null)) THEN
		select CONCAT(fieldNameList,'container.emptyContWeightKilo~') into fieldNameList;
		IF(OLD.emptyContWeightKilo is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.emptyContWeightKilo is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.emptyContWeightKilo is not null) then
			select CONCAT(oldValueList,OLD.emptyContWeightKilo,'~') into oldValueList;
		END IF;
		IF(NEW.emptyContWeightKilo is not null) then
			select CONCAT(newValueList,NEW.emptyContWeightKilo,'~') into newValueList;
		END IF; 
	END IF;
	 IF ((OLD.netWeightKilo <> NEW.netWeightKilo) or (OLD.netWeightKilo is null and NEW.netWeightKilo is not null) 
	  or (OLD.netWeightKilo is not null and NEW.netWeightKilo is null)) THEN
		select CONCAT(fieldNameList,'container.netWeightKilo~') into fieldNameList;
		IF(OLD.netWeightKilo is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.netWeightKilo is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.netWeightKilo is not null) then
			select CONCAT(oldValueList,OLD.netWeightKilo,'~') into oldValueList;
		END IF;
		IF(NEW.netWeightKilo is not null) then
			select CONCAT(newValueList,NEW.netWeightKilo,'~') into newValueList;
		END IF; 
	END IF;
	
	IF ((OLD.volumeCbm <> NEW.volumeCbm) or (OLD.volumeCbm is null and NEW.volumeCbm is not null) 
	  or (OLD.volumeCbm is not null and NEW.volumeCbm is null)) THEN
		select CONCAT(fieldNameList,'container.volumeCbm~') into fieldNameList;
		IF(OLD.volumeCbm is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.volumeCbm is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.volumeCbm is not null) then
			select CONCAT(oldValueList,OLD.volumeCbm,'~') into oldValueList;
		END IF;
		IF(NEW.volumeCbm is not null) then
			select CONCAT(newValueList,NEW.volumeCbm,'~') into newValueList;
		END IF; 
	END IF;
	IF ((OLD.densityMetric <> NEW.densityMetric) or (OLD.densityMetric is null and NEW.densityMetric is not null) 
	  or (OLD.densityMetric is not null and NEW.densityMetric is null)) THEN
		select CONCAT(fieldNameList,'container.densityMetric~') into fieldNameList;
		IF(OLD.densityMetric is null) THEN
			select CONCAT(oldValueList,"0",'~') into oldValueList;
		END IF;
		IF(NEW.densityMetric is null) THEN
			select CONCAT(newValueList,"0",'~') into newValueList;
		END IF;

		IF(OLD.densityMetric is not null) then
			select CONCAT(oldValueList,OLD.densityMetric,'~') into oldValueList;
		END IF;
		IF(NEW.densityMetric is not null) then
			select CONCAT(newValueList,NEW.densityMetric,'~') into newValueList;
		END IF; 
		END IF; 
		IF (OLD.status <> NEW.status ) THEN
      select CONCAT(fieldNameList,'container.status~') into fieldNameList;
      IF(OLD.status = false) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(OLD.status = true) THEN
        select CONCAT(oldValueList,'TRUE','~') into oldValueList;
      END IF;
      IF(OLD.status is null) THEN
        select CONCAT(oldValueList,'FALSE','~') into oldValueList;
      END IF;
      IF(NEW.status = false) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
      IF(NEW.status = true) THEN
        select CONCAT(newValueList,'TRUE','~') into newValueList;
      END IF;
       IF(NEW.status is null) THEN
        select CONCAT(newValueList,'FALSE','~') into newValueList;
      END IF;
	END IF;
 
  IF ((OLD.sealDate <> NEW.sealDate) or (OLD.sealDate is null and NEW.sealDate is not null)
	  or (OLD.sealDate is not null and NEW.sealDate is null)) THEN
       select CONCAT(fieldNameList,'container.sealDate~') into fieldNameList;
       IF(OLD.sealDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.sealDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.sealDate is not null) THEN
        	select CONCAT(oldValueList,OLD.sealDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.sealDate is not null) THEN
        	select CONCAT(newValueList,NEW.sealDate,'~') into newValueList;
      	END IF;
   END IF;
   
   
   IF (OLD.containerId <> NEW.containerId ) THEN
       select CONCAT(fieldNameList,'container.containerId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.containerId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.containerId,'~') into newValueList;
   END IF;
   
   
IF (OLD.ugwIntId <> NEW.ugwIntId ) THEN
       select CONCAT(fieldNameList,'container.ugwIntId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.ugwIntId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.ugwIntId,'~') into newValueList;
   END IF;

  
IF (OLD.pickupPier <> NEW.pickupPier ) THEN
       select CONCAT(fieldNameList,'container.pickupPier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.pickupPier,'~') into oldValueList;
       select CONCAT(newValueList,NEW.pickupPier,'~') into newValueList;
   END IF;
  
  IF (OLD.redeliveryPier <> NEW.redeliveryPier ) THEN
       select CONCAT(fieldNameList,'container.redeliveryPier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.redeliveryPier,'~') into oldValueList;
       select CONCAT(newValueList,NEW.redeliveryPier,'~') into newValueList;
   END IF;

	
IF ((OLD.vgmCutoff <> NEW.vgmCutoff) or (OLD.vgmCutoff is null and NEW.vgmCutoff is not null)
	  or (OLD.vgmCutoff is not null and NEW.vgmCutoff is null)) THEN
       select CONCAT(fieldNameList,'container.vgmCutoff~') into fieldNameList;
       IF(OLD.vgmCutoff is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.vgmCutoff is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.vgmCutoff is not null) THEN
        	select CONCAT(oldValueList,OLD.vgmCutoff,'~') into oldValueList;
     	END IF;
      	IF(NEW.vgmCutoff is not null) THEN
        	select CONCAT(newValueList,NEW.vgmCutoff,'~') into newValueList;
      	END IF;
   END IF;
	
update serviceorder set isSOExtract=false,updatedBy = NEW.updatedBy,updatedOn = now() where id=NEW.id;	
CALL add_tblHistory (OLD.id,"container", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

   END 
   $$

delimiter;