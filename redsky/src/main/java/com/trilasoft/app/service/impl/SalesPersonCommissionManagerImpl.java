package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.SalesPersonCommissionDao;
import com.trilasoft.app.model.SalesPersonCommission;
import com.trilasoft.app.service.SalesPersonCommissionManager;

public class SalesPersonCommissionManagerImpl extends GenericManagerImpl<SalesPersonCommission, Long> implements SalesPersonCommissionManager{
	SalesPersonCommissionDao salesPersonCommissionDao;
	public SalesPersonCommissionManagerImpl(SalesPersonCommissionDao salesPersonCommissionDao){
		super(salesPersonCommissionDao);
		this.salesPersonCommissionDao=salesPersonCommissionDao;
		
	}
	public List getCommPersentAcRange(String corpId,String contractName,String chargeCode){
		return salesPersonCommissionDao.getCommPersentAcRange(corpId, contractName, chargeCode);
	}
	public Map<Double,BigDecimal > getCommPersentAcMap(String corpId,String contractName,String chargeCode,String companyDiv){
		return salesPersonCommissionDao.getCommPersentAcMap(corpId, contractName, chargeCode,companyDiv);
	}
}
