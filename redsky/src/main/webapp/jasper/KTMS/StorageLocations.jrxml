<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="StorageLocations" pageWidth="612" pageHeight="792" whenNoDataType="AllSectionsNoDetail" columnWidth="576" leftMargin="18" rightMargin="18" topMargin="18" bottomMargin="18" uuid="57db4d28-4c70-451d-8d8c-c6b1914e7e09">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Warehouse" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	"StorageLocations-R-PDF-KTMS.jrxml",
	DATE_FORMAT(now(),'%d/%m/%Y') AS currentdate,
	if(serviceorder.companyDivision is null || serviceorder.companyDivision='','',serviceorder.companyDivision) AS serviceorder_companyDivision,
	serviceorder.`shipNumber` AS serviceorder_shipNumber,
	trim(serviceorder.registrationNumber) AS serviceorder_registrationNumber,
	storage.locationId AS storage_locationId,
	trim(storage.description) AS storage_description,
	location.type AS location_type,
	location.occupied AS location_occupied,
	if((serviceorder.`firstName` is null || trim(serviceorder.`firstName`)=''),trim(serviceorder.`lastName`),concat(trim(serviceorder.`firstName`)," ",trim(serviceorder.`lastName`))) AS shipper
FROM	`serviceorder` serviceorder
	LEFT OUTER JOIN `storage` storage ON serviceorder.`shipNumber` = storage.`shipNumber` AND storage.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN `billing` billing ON serviceorder.`id` = billing.`id` AND billing.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN `location` location ON storage.`locationId` = location.`locationId` AND location.`corpID` = $P{Corporate ID}
WHERE
	serviceorder.`corpID` = $P{Corporate ID}
	AND serviceorder.`status` NOT IN('CNCL','CLSD')
	AND billing.`storageOut` IS NULL
	AND storage.`releaseDate` IS NULL
	AND (if(LEFT(storage.`locationId`,2) IN('12','14'),LEFT(storage.`locationId`,2),RIGHT(LEFT(storage.`locationId`,2),1)) LIKE $P{Warehouse})
GROUP BY storage.`locationId`
ORDER BY serviceorder.companyDivision,shipper,storage.`locationId`]]>
	</queryString>
	<field name="StorageLocations-R-PDF-KTMS.jrxml" class="java.lang.String"/>
	<field name="currentdate" class="java.lang.String"/>
	<field name="serviceorder_companyDivision" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="serviceorder_registrationNumber" class="java.lang.String"/>
	<field name="storage_locationId" class="java.lang.String"/>
	<field name="storage_description" class="java.lang.String"/>
	<field name="location_type" class="java.lang.String"/>
	<field name="location_occupied" class="java.lang.String"/>
	<field name="shipper" class="java.lang.String"/>
	<group name="CompDiv" isStartNewPage="true">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band height="18" splitType="Stretch">
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField" x="102" y="0" width="331" height="18" uuid="9ef4513d-9bc0-47d0-a282-f72db6ed82ed"/>
					<textElement>
						<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{serviceorder_companyDivision}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-29" x="0" y="0" width="102" height="18" uuid="9e060fcf-32f5-4c37-a06c-3b24784c6601"/>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
					</textElement>
					<text><![CDATA[Company Division: ]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="shipper" isReprintHeaderOnEachPage="true">
		<groupExpression><![CDATA[$F{shipper}]]></groupExpression>
		<groupHeader>
			<band height="25" splitType="Stretch">
				<textField isBlankWhenNull="true">
					<reportElement key="textField" x="0" y="8" width="576" height="16" forecolor="#800080" uuid="17ad7462-a45a-49a1-a9f8-44de4b2d6143"/>
					<textElement>
						<font fontName="Arial" isBold="true" isItalic="true" isUnderline="true" pdfFontName="Helvetica-BoldOblique"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{shipper}]]></textFieldExpression>
				</textField>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band height="33" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-2" x="0" y="0" width="576" height="18" forecolor="#0033CC" uuid="dfa512f0-db4a-4849-a686-97e632c13b80"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="12" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Storage Locations By Row # And Pallets]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-21" x="0" y="17" width="331" height="16" forecolor="#CC0033" uuid="03fdbda4-ad97-4e71-a867-5901f8f60530"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#663300"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#663300"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="10" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[For Warehouse #]]></text>
			</staticText>
			<textField isBlankWhenNull="false">
				<reportElement key="textField" x="336" y="17" width="240" height="16" forecolor="#CC0033" uuid="66a6098f-b5cb-40f8-9fd2-aeb0618c3be7"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$P{Warehouse}.equals("%") || $P{Warehouse}.equals("%%") ? "All" : $P{Warehouse}]]></textFieldExpression>
			</textField>
		</band>
	</pageHeader>
	<columnHeader>
		<band height="25" splitType="Stretch">
			<staticText>
				<reportElement key="staticText-22" x="0" y="7" width="76" height="18" uuid="619961ab-3d38-46c9-ab69-78a2e3c5ae31"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Location]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-23" x="76" y="7" width="90" height="18" uuid="b8a2e1f0-2fd4-4f22-8ded-1aa84d53e8b4"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Ship No.]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-24" x="166" y="7" width="87" height="18" uuid="9c4ea77c-9b4d-4a45-8397-4939e959ad71"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Reg#]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-26" x="252" y="7" width="151" height="18" uuid="cd42d90f-599f-47ac-b857-819cf3503ecb"/>
				<textElement textAlignment="Left">
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Description]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-27" x="403" y="7" width="38" height="18" uuid="ca5fba43-ba51-4ee4-b11b-0b12b6bd1450"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Type]]></text>
			</staticText>
			<staticText>
				<reportElement key="staticText-28" x="441" y="7" width="59" height="18" uuid="7201ab1e-cfbe-4a60-9dbb-ec0bb3179ec7"/>
				<textElement>
					<font fontName="Arial" isBold="true" isUnderline="false" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<text><![CDATA[Occupied]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="18" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="0" width="76" height="18" uuid="dce044dc-3244-48b5-8718-0b990da93283"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{storage_locationId}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="76" y="0" width="90" height="18" uuid="a05df2d5-7a66-4150-a769-42fe737efac6"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="166" y="0" width="87" height="18" uuid="57390b14-5194-406b-8b37-61cc4ad70574"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{serviceorder_registrationNumber}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="252" y="0" width="151" height="18" uuid="a4efe3bf-26cc-44bc-a29d-de73aae5fd9e"/>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{storage_description}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="403" y="0" width="23" height="18" forecolor="#CC0033" uuid="c83372c6-99ff-4033-a2a1-d7db2f4e40e4"/>
				<textElement textAlignment="Center">
					<font fontName="Arial" isBold="true" pdfFontName="Helvetica-Bold"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location_type}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement key="textField" x="441" y="0" width="59" height="18" uuid="a4820ef5-afe9-4838-a7d6-c7e2d737d594"/>
				<textElement textAlignment="Center">
					<font fontName="Arial"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{location_occupied}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="23" splitType="Stretch">
			<textField pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="368" y="6" width="170" height="17" forecolor="#000000" backcolor="#FFFFFF" uuid="299cf566-c5bb-4b97-ad72-f392c511966d"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Right">
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report" pattern="" isBlankWhenNull="false">
				<reportElement key="textField" x="540" y="6" width="36" height="17" forecolor="#000000" backcolor="#FFFFFF" uuid="490a34b7-37b7-4abf-bf97-08448023cdb5"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement key="textField" x="0" y="6" width="116" height="17" forecolor="#000000" backcolor="#FFFFFF" uuid="b8a9c873-907a-4491-beaa-f9e1a9cef7ce"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement>
					<font fontName="Arial" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{currentdate}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement key="staticText-20" x="198" y="6" width="180" height="17" uuid="5543f66a-1c40-420b-aca9-00b65092c982"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<textElement textAlignment="Center">
					<font fontName="Arial" size="8"/>
				</textElement>
				<text><![CDATA[StorageLocations.jrxml]]></text>
			</staticText>
			<line>
				<reportElement key="line-2" x="-1" y="2" width="574" height="1" uuid="b8b6fdc1-81de-4284-b0ec-2908174bd883"/>
				<graphicElement fill="Solid"/>
			</line>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
