// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1;  
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`contract` ADD INDEX `contract`(`contract`);

ALTER TABLE `redsky`.`workticket` ADD COLUMN `dailySheetNotes` TEXT NULL;

ALTER TABLE `redsky`.`crew` ADD COLUMN `licenceNumber` VARCHAR(25) NULL;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `serviceTaxInput` bit(1) DEFAULT b'0';

update accountline set serviceTaxInput =false;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `acctDisplayEntitled` bit(1) DEFAULT b'1' ,
ADD COLUMN `acctDisplayEstimate` bit(1) DEFAULT b'1',
ADD COLUMN `acctDisplayRevision` bit(1) DEFAULT b'1';

update app_user set acctDisplayEntitled = true , acctDisplayEstimate = true, acctDisplayRevision = true;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `CAR_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `COL_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TRG_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `HOM_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `RNT_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `SET_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `LAN_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `MMG_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `ONG_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `PRV_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `AIO_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `EXP_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `RPT_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `SCH_displyOtherSchoolSelected` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TAX_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TAC_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `VIS_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_displyOtherVendorCode` BIt(1) DEFAULT b'0';

ALTER TABLE `redsky`.`company` ADD COLUMN `ccEmail` VARCHAR(250);

ALTER TABLE `redsky`.`accountline` MODIFY COLUMN `payPayableVia` VARCHAR(200) DEFAULT '';

update refmaster set fieldLength=20 where parameter='EUVAT';

update refmaster set fieldLength=20 where parameter='PAYVATDESC';

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `surveyEmailLanguage` VARCHAR(200),ADD COLUMN `cportalEmailLanguage` VARCHAR(200);

ALTER TABLE `redsky`.`customerfile` MODIFY COLUMN `salesMan` VARCHAR(82) DEFAULT NULL;

ALTER TABLE `redsky`.`serviceorder` MODIFY COLUMN `salesMan` VARCHAR(82) DEFAULT NULL;

