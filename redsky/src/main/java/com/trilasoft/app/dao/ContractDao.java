package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Contract;

import java.util.Date;
import java.util.List;
import java.util.Map;

public interface ContractDao extends GenericDao<Contract, Long> {
	public List findMaximumId();
	
	public Contract getContactListByContarct(String corpid,String contact);

	public List<Contract> findByJobType(String jobType);

	public List<Contract> findByBillingInstructionCode(String billingInstructionCode);

	public Map<String, String> findForContract(String corpID, String jobType);

	public List findContracts(String contractType, String description, String jobType,String companyDivision, Boolean activeCheck, String corpId);

	public List findForJobContract(String corpID, String jobType);

	public List findAllContract(String corpID);

	public List calculateCommission(Date openfromdate, Date opentodate, String corpId);
	
	public List findComanyDivision(String corpID);
	public List agentContract(String sessionCorpID,  String contract);
	public List countRow(String contractName, String agentID);
	public List findCostElement(String sessionCorpID);
	public String getOwnerContract(Long id);
	public List countRowOwner(String owner, Long Orginid, String agentID);
	public int findContractCount(String corpId, String contract, String agentsCorpId, Long id);
	public List findAgentCodeList(String agentCorpId);
	public List checkContract(String sessionCorpID, String contract);
	public List getAllContractbyCorpId(String sessionCorpID);
	public void findDeleteContractOfOtherCorpId(String contractName, String agentCorpId);
	public String callPublishContractProcedure(String contractName,String sessionCorpID,String agentCorpId,String updatedBy);
	public List findCmmDmmAgentDromCompany();
	public Map<String, String> getContractAndContractTypeMap(String sessionCorpID);
}
