<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="ImportAdviceAIR"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="487"
		 columnSpacing="0"
		 leftMargin="56"
		 rightMargin="52"
		 topMargin="0"
		 bottomMargin="0"
		 whenNoDataType="AllSectionsNoDetail"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.zoom" value="1.0" />
	<property name="ireport.x" value="0" />
	<property name="ireport.y" value="96" />
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Service Order Number" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[SELECT	'ImportAdviceAIR-F-INTM.jrxml',
	if(companydivision.description is null || trim(companydivision.description)='','',trim(companydivision.description)) AS companydivision_description,
	if(companydivision.billingAddress1 is null || trim(companydivision.billingAddress1)='','',trim(companydivision.billingAddress1)) AS companydivision_billingAddress1,
	if(companydivision.billingAddress2 is null || trim(companydivision.billingAddress2)='','',trim(companydivision.billingAddress2)) AS companydivision_billingAddress2,
	if(companydivision.billingAddress3 is null || trim(companydivision.billingAddress3)='','',trim(companydivision.billingAddress3)) AS companydivision_billingAddress3,
	if(companydivision.billingAddress4 is null || trim(companydivision.billingAddress4)='','',trim(companydivision.billingAddress4)) AS companydivision_billingAddress4,
	Concat(if(companydivision.billingZip is null || trim(companydivision.billingZip)='',"",trim(companydivision.billingZip)),"",if(companydivision.billingCity is null || trim(companydivision.billingCity)='',"",if((companydivision.billingZip is null || trim(companydivision.billingZip)=''),trim(companydivision.billingCity),concat(", ",trim(companydivision.billingCity)))),"",if(companydivision.billingState is null || companydivision.billingState='',"",if((companydivision.billingZip is null || companydivision.billingZip='') AND (companydivision.billingCity is null || trim(companydivision.billingCity)=''),companydivision.billingState,concat(", ",companydivision.billingState))))AS companydivision_billing_ZipCityState,
	if(companydivision.billingCountry is null || trim(companydivision.billingCountry)='','',trim(companydivision.billingCountry)) AS companydivision_billingCountry,
	CONCAT(if(app_user.first_name is null || trim(app_user.first_name)="","",trim(app_user.first_name)),if(app_user.last_name is null || trim(app_user.last_name)="","",if((app_user.first_name is null || trim(app_user.first_name)=""),trim(app_user.last_name),concat(" ",trim(app_user.last_name))))) As Coordinator,
	TRIM(app_user.email) As Cordinator_Email,
	trim(app_user.phone_number) As Cordinator_phone,
	DATE_FORMAT(now(),'%d.%b.%Y') As currentDate,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	if((trim(serviceorder.prefix)="" || serviceorder.prefix is null),if((trim(serviceorder.firstName)="" || serviceorder.firstName is null),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))),concat(trim(serviceorder.prefix)," ", if((trim(serviceorder.firstName)="" || serviceorder.firstName is null),trim(serviceorder.lastName),concat(trim(serviceorder.firstName)," ",trim(serviceorder.lastName))))) AS ShipperName,
	trackingstatus.destinationAgent AS trackingstatus_destinationAgent,
	trackingstatus.billLading AS trackingstatus_billLading,
	if(miscellaneous.`unit2`='Cbm',If(serviceorder.`mode`='AIR',if(miscellaneous.actualCubicMtr is null || miscellaneous.actualCubicMtr='','',concat(miscellaneous.actualCubicMtr,' ','Cbm')),if(miscellaneous.netActualCubicMtr is null || miscellaneous.netActualCubicMtr='','',concat(miscellaneous.netActualCubicMtr,' ','Cbm'))),If(serviceorder.`mode`='AIR',if(miscellaneous.actualCubicFeet is null || miscellaneous.actualCubicFeet='','',concat(miscellaneous.actualCubicFeet,' ','Cft')),if(miscellaneous.netActualCubicFeet is null || miscellaneous.netActualCubicFeet='','',concat(miscellaneous.netActualCubicFeet,' ','Cft')))) AS miscellaneous_Actual_Volume,
	if(carton.idNumber is null || carton.idNumber='','',carton.idNumber) AS carton_idNumber,
	concat(IF(carton.netWeightKilo is null || carton.netWeightKilo='','',concat(carton.netWeightKilo," kg")),"",if(carton.grossWeightKilo is null || carton.grossWeightKilo='','',IF(carton.netWeightKilo is null || carton.netWeightKilo='',concat(carton.grossWeightKilo," kg"),concat(" / ",carton.grossWeightKilo,"kg")))) AS carton_grossWeightKilo
FROM	serviceorder serviceorder
	INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` AND trackingstatus.`corpID`=$P{Corporate ID}
	INNER JOIN miscellaneous miscellaneous ON serviceorder.`id` = miscellaneous.`id` AND miscellaneous.`corpID`=$P{Corporate ID}
	LEFT OUTER JOIN `companydivision` companydivision ON serviceorder.`companyDivision` = companydivision.`companyCode` AND companydivision.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN `app_user` app_user ON serviceorder.`coordinator` = app_user.`username` AND app_user.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN `carton` carton ON serviceorder.`id` = carton.`serviceOrderId`  AND carton.`corpID` = $P{Corporate ID}

WHERE   serviceorder.`corpID` = $P{Corporate ID}
	AND serviceorder.`shipNumber`= $P{Service Order Number}
ORDER BY carton.idNumber]]></queryString>

	<field name="ImportAdviceAIR-F-INTM.jrxml" class="java.lang.String"/>
	<field name="companydivision_description" class="java.lang.String"/>
	<field name="companydivision_billingAddress1" class="java.lang.String"/>
	<field name="companydivision_billingAddress2" class="java.lang.String"/>
	<field name="companydivision_billingAddress3" class="java.lang.String"/>
	<field name="companydivision_billingAddress4" class="java.lang.String"/>
	<field name="companydivision_billing_ZipCityState" class="java.lang.String"/>
	<field name="companydivision_billingCountry" class="java.lang.String"/>
	<field name="Coordinator" class="java.lang.String"/>
	<field name="Cordinator_Email" class="java.lang.String"/>
	<field name="Cordinator_phone" class="java.lang.String"/>
	<field name="currentDate" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="ShipperName" class="java.lang.String"/>
	<field name="trackingstatus_destinationAgent" class="java.lang.String"/>
	<field name="trackingstatus_billLading" class="java.lang.String"/>
	<field name="miscellaneous_Actual_Volume" class="java.lang.String"/>
	<field name="carton_idNumber" class="java.lang.String"/>
	<field name="carton_grossWeightKilo" class="java.lang.String"/>


		<group  name="Page1" isStartNewPage="true" >
			<groupExpression><![CDATA[]]></groupExpression>
			<groupHeader>
			<band height="262"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-1"
						y="2"
						width="276"
						height="85"
						key="textField-74"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" lineSpacing="Single">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="10" isBold="true" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[(	($F{companydivision_description}==null || $F{companydivision_description}.equals("") ? "" :($F{companydivision_description}+"\n"))+
	($F{companydivision_billingAddress1}==null || $F{companydivision_billingAddress1}.equals("") ? "" :($F{companydivision_billingAddress1}+"\n"))+
	($F{companydivision_billingAddress2}==null || $F{companydivision_billingAddress2}.equals("") ? "" :($F{companydivision_billingAddress2}+"\n"))+
	($F{companydivision_billingAddress3}==null || $F{companydivision_billingAddress3}.equals("") ? "" :($F{companydivision_billingAddress3}+"\n"))+
	($F{companydivision_billingAddress4}==null || $F{companydivision_billingAddress4}.equals("") ? "" :($F{companydivision_billingAddress4}+"\n"))+
	($F{companydivision_billing_ZipCityState}==null || $F{companydivision_billing_ZipCityState}.equals("") ? "" :($F{companydivision_billing_ZipCityState}+"\n"))+
	($F{companydivision_billingCountry}==null || $F{companydivision_billingCountry}.equals("") ? "" :($F{companydivision_billingCountry}))

)]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="337"
						y="2"
						width="150"
						height="15"
						key="textField-75"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Coordinator}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="337"
						y="17"
						width="150"
						height="14"
						key="textField-76"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Cordinator_Email}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="306"
						y="2"
						width="39"
						height="15"
						key="staticText-101"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[Name:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="306"
						y="17"
						width="39"
						height="14"
						key="staticText-102"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[E-mail:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="337"
						y="31"
						width="150"
						height="15"
						key="textField-77"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Cordinator_phone}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="337"
						y="46"
						width="150"
						height="14"
						key="textField-78"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{currentDate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="306"
						y="31"
						width="39"
						height="15"
						key="staticText-105"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[Tel]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="306"
						y="46"
						width="39"
						height="14"
						key="staticText-106"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Arial" pdfFontName="Helvetica" size="8" isBold="false" isItalic="false"/>
					</textElement>
				<text><![CDATA[Date]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="8"
						y="84"
						width="470"
						height="28"
						forecolor="#FF0000"
						key="textField-79"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center" verticalAlignment="Bottom">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["IMPORT-MITTEILUNG LUFTFRACHT"]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="-1"
						y="185"
						width="180"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-108"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Empfänger:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-1"
						y="202"
						width="180"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-110"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[AWB-Nr.:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-1"
						y="224"
						width="180"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-111"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Menge:]]></text>
				</staticText>
				<line direction="BottomUp">
					<reportElement
						x="0"
						y="86"
						width="487"
						height="0"
						key="line-1"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="BottomUp">
					<reportElement
						x="0"
						y="113"
						width="487"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="487"
						y="86"
						width="0"
						height="28"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<line direction="TopDown">
					<reportElement
						x="-1"
						y="86"
						width="0"
						height="28"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="241"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-80"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{miscellaneous_Actual_Volume}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="185"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-81"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_destinationAgent}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="202"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-82"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{trackingstatus_billLading}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="224"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-83"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[""]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="-1"
						y="241"
						width="180"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-112"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Volumen:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-1"
						y="122"
						width="488"
						height="31"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-108"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="10" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Bitte übernehmen Sie in unserem Auftrag die beschriebene Sendung und fertigen diese zolltechnisch ab."]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="-1"
						y="168"
						width="180"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-117"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Service order No:]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="168"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-111"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{serviceorder_shipNumber}]]></textFieldExpression>
				</textField>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="100"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-1"
						y="51"
						width="471"
						height="39"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-109"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Bei Rückfragen stehe ich gerne zu Ihrer Verfügung."]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-1"
						y="74"
						width="274"
						height="25"
						key="textField-110"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isItalic="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Coordinator}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="842"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="288"
						y="747"
						width="107"
						height="40"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="5"/>
					</textElement>
				<text><![CDATA[Sparkasse Miesbach – Tegernsee
8 547 978
711 525 70
DE 02 7115 2570 0008 5479 78
BYLADEM1MIB]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="218"
						y="747"
						width="87"
						height="40"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-100"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[Bankverbindung:
Kto.-Nr.:
BLZ:
IBAN:
SWIFT-/BIC - Code:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="218"
						y="786"
						width="87"
						height="40"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-100"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[Bankverbindung:
Kto.-Nr.:
BLZ:
IBAN:
SWIFT-/BIC - Code:
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="69"
						y="747"
						width="33"
						height="39"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-100"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[Fon:
Fax:
Email:
Internet :]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="162"
						y="747"
						width="63"
						height="14"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="staticText-100"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[Geschäftsführer:]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="288"
						y="786"
						width="107"
						height="40"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="5"/>
					</textElement>
				<text><![CDATA[Postbank Nürnberg
245 761 857
760 100 85
DE 12 7601 0085 0245 7618 57
PBNKDEFF
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="162"
						y="753"
						width="63"
						height="23"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="5"/>
					</textElement>
				<text><![CDATA[Florian Mannhardt
Axel Just
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="102"
						y="747"
						width="71"
						height="23"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="5"/>
					</textElement>
				<text><![CDATA[+49 (0)89 – 1893 86 0
+49 (0)89 – 1893 86 50]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="7"
						y="747"
						width="44"
						height="14"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[E GmbH]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-4"
						y="747"
						width="10"
						height="14"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[MO]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-19"
						y="753"
						width="88"
						height="23"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" size="5"/>
					</textElement>
				<text><![CDATA[Parsdorfer Weg 10
85551 Kirchheim b. München
AG München HRB 138 385
UID-Nr.: 216 789 748
]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="4"
						y="747"
						width="12"
						height="14"
						forecolor="#FF0000"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="5" isBold="true"/>
					</textElement>
				<text><![CDATA[V]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-19"
						y="747"
						width="32"
						height="14"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-BoldOblique" size="5" isBold="true" isItalic="true"/>
					</textElement>
				<text><![CDATA[INTER]]></text>
				</staticText>
				<staticText>
					<reportElement
						x="-19"
						y="779"
						width="222"
						height="50"
						key="staticText"/>
					<box>					<pen lineWidth="0.0"/>
					<topPen lineWidth="0.0"/>
					<leftPen lineWidth="0.0"/>
					<bottomPen lineWidth="0.0"/>
					<rightPen lineWidth="0.0"/>
</box>
					<textElement>
						<font fontName="Arial" size="6"/>
					</textElement>
				<text><![CDATA[Für die Beförderung von Umzugsgut gelten die gesetzlichen Regelungen im IV. Abschnitt des HGB und ergänzend unsere Allgemeinen Geschäftsbedingungen. Für Möbellagerungen sind die Allgemeinen Lagerbedingungen des deutschen Möbeltransports (ALB) vereinbart. In allen Fällen arbeiten wir ausschließlich aufgrund der Allgemeinen deutschen Spediteurbedingungen (ADSp), neueste Fassung. Erfüllungsort und Gerichtsstand ist ausschließlich München.]]></text>
				</staticText>
				<rectangle>
					<reportElement
						mode="Opaque"
						x="-57"
						y="823"
						width="597"
						height="19"
						forecolor="#FFFFFF"
						backcolor="#FF0000"
						key="rectangle"
						isPrintWhenDetailOverflows="true"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="0.0"/>
</graphicElement>
				</rectangle>
				<staticText>
					<reportElement
						x="102"
						y="758"
						width="71"
						height="26"
						forecolor="#0000FF"
						key="staticText"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Oblique" size="5" isUnderline="true"/>
					</textElement>
				<text><![CDATA[info@intermove.de
www.intermove.de
]]></text>
				</staticText>
				<image  scaleImage="FillFrame" vAlign="Top" hAlign="Left" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="366"
						y="761"
						width="111"
						height="42"
						key="image-3"/>
					<box leftPadding="1"></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/UniGroup_Relocation_Network.png"]]></imageExpression>
				</image>
				<image  scaleImage="RetainShape" vAlign="Bottom" hAlign="Left" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-2"
						y="29"
						width="117"
						height="40"
						key="image-4"/>
					<box bottomPadding="1"></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/intermove.png"]]></imageExpression>
				</image>
				<image  scaleImage="RetainShape" vAlign="Top" hAlign="Left" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="357"
						y="26"
						width="119"
						height="41"
						key="image-5"/>
					<box leftPadding="2"></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/UTS-Logo.png"]]></imageExpression>
				</image>
				<rectangle radius="0" >
					<reportElement
						mode="Opaque"
						x="466"
						y="26"
						width="5"
						height="5"
						forecolor="#FFFFFF"
						backcolor="#FFFFFF"
						key="rectangle-1"/>
					<graphicElement stretchType="NoStretch" fill="Solid" />
				</rectangle>
				<image  scaleImage="RetainShape" vAlign="Top" hAlign="Left" onErrorType="Blank" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						mode="Opaque"
						x="467"
						y="26"
						width="8"
						height="8"
						key="image-6"/>
					<box topPadding="1"></box>
					<graphicElement stretchType="NoStretch"/>
					<imageExpression class="java.lang.String"><![CDATA["../../images/regUTS.png"]]></imageExpression>
				</image>
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="100"  isSplitAllowed="true" >
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="25"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="-1"
						y="1"
						width="214"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-113"
						positionType="Float"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Gew.netto/brutto/ACW #"+($F{carton_idNumber}==null ||$F{carton_idNumber}.equals("") ? "01" :$F{carton_idNumber})+":"]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="179"
						y="1"
						width="302"
						height="17"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-114"
						isRemoveLineWhenBlank="true"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica" size="12" isBold="false"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{carton_grossWeightKilo}]]></textFieldExpression>
				</textField>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="93"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</summary>
</jasperReport>
