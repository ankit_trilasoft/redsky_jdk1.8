package com.trilasoft.app.service.impl;


import java.math.BigDecimal;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.T20VisionLogInfoDao;
import com.trilasoft.app.model.T20VisionLogInfo;
import com.trilasoft.app.service.T20VisionLogInfoManager;

public class T20VisionLogInfoManagerImpl extends GenericManagerImpl<T20VisionLogInfo, Long> implements T20VisionLogInfoManager
{
	T20VisionLogInfoDao t20VisionLogInfoDao;
	public T20VisionLogInfoManagerImpl(T20VisionLogInfoDao t20VisionLogInfoDao) {
		super(t20VisionLogInfoDao);
		this.t20VisionLogInfoDao=t20VisionLogInfoDao;
	}

	public List findT20VisionLogList(String sessionCorpID){
		return t20VisionLogInfoDao.findT20VisionLogList(sessionCorpID);
	}
	public List searcht20VisionLogInfoList(String entryNumber,String invoiceNumber,String orderNumber,String orderNumberLine,String status,String sessionCorpID){
		return t20VisionLogInfoDao.searcht20VisionLogInfoList(entryNumber,invoiceNumber,orderNumber,orderNumberLine,status,sessionCorpID);
	}

}
