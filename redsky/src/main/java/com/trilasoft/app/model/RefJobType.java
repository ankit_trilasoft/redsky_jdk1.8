package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name = "refjobtype")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")
public class RefJobType extends BaseObject {
	private String corpID;

	private Long id;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;
	
	private String job;  
	
	private String personBilling;
	
	private String personAuditor;
	private String personClaims;
	private String companyDivision;
	
	private String personPricing; 
	
	private String personPayable; 
	
	private String coordinator;	
	
	private String estimator;
	
	private String country;
	
	private String countryCode;
	
	private String surveyTool;
	
	private Boolean accAccountPortal=new Boolean(true);
	
	private String internalBillingPerson;
	
	private String opsPerson;
	
	private String personForwarder;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("createdBy", createdBy)
				.append("updatedBy", updatedBy).append("createdOn", createdOn)
				.append("updatedOn", updatedOn).append("job", job)
				.append("personBilling", personBilling)
				.append("personAuditor", personAuditor)
				.append("personClaims", personClaims)
				.append("companyDivision", companyDivision)
				.append("personPricing", personPricing)
				.append("personPayable", personPayable)
				.append("coordinator", coordinator)
				.append("estimator", estimator).append("country", country)
				.append("countryCode", countryCode)
				.append("surveyTool", surveyTool).append("accAccountPortal",accAccountPortal)
				.append("internalBillingPerson", internalBillingPerson)
				.append("opsPerson", opsPerson)
				.append("personForwarder", personForwarder)
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefJobType))
			return false;
		RefJobType castOther = (RefJobType) other;
		return new EqualsBuilder().append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(job, castOther.job)
				.append(personBilling, castOther.personBilling)
				.append(personAuditor, castOther.personAuditor)
				.append(personClaims, castOther.personClaims)
				.append(companyDivision, castOther.companyDivision)
				.append(personPricing, castOther.personPricing)
				.append(personPayable, castOther.personPayable)
				.append(coordinator, castOther.coordinator)
				.append(estimator, castOther.estimator)
				.append(country, castOther.country)
				.append(countryCode, castOther.countryCode)
				.append(surveyTool, castOther.surveyTool)
				.append(accAccountPortal, castOther.accAccountPortal)
				.append(internalBillingPerson, castOther.internalBillingPerson)
				.append(opsPerson, castOther.opsPerson)
				.append(personForwarder, castOther.personForwarder)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(job).append(personBilling)
				.append(personAuditor).append(personClaims)
				.append(companyDivision).append(personPricing)
				.append(personPayable).append(coordinator).append(estimator)
				.append(country).append(countryCode).append(surveyTool).append(accAccountPortal)
				.append(internalBillingPerson).append(opsPerson).append(personForwarder)
				.toHashCode();
	}

	@Column(length = 20)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	public String getEstimator() {
		return estimator;
	}

	public void setEstimator(String estimator) {
		this.estimator = estimator;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public String getPersonBilling() {
		return personBilling;
	}

	public void setPersonBilling(String personBilling) {
		this.personBilling = personBilling;
	}

	public String getPersonPayable() {
		return personPayable;
	}

	public void setPersonPayable(String personPayable) {
		this.personPayable = personPayable;
	}

	public String getPersonPricing() {
		return personPricing;
	}

	public void setPersonPricing(String personPricing) {
		this.personPricing = personPricing;
	}
    @Column
	public String getPersonAuditor() {
		return personAuditor;
	}

	public void setPersonAuditor(String personAuditor) {
		this.personAuditor = personAuditor;
	}

	@Column(length=10)
	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
    @Column(length=100)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
    @Column(length=3)
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getSurveyTool() {
		return surveyTool;
	}

	public void setSurveyTool(String surveyTool) {
		this.surveyTool = surveyTool;
	}

	public String getPersonClaims() {
		return personClaims;
	}

	public void setPersonClaims(String personClaims) {
		this.personClaims = personClaims;
	}
	@Column
	public Boolean getAccAccountPortal() {
		return accAccountPortal;
	}

	public void setAccAccountPortal(Boolean accAccountPortal) {
		this.accAccountPortal = accAccountPortal;
	}

	public String getInternalBillingPerson() {
		return internalBillingPerson;
	}

	public void setInternalBillingPerson(String internalBillingPerson) {
		this.internalBillingPerson = internalBillingPerson;
	}

	public String getOpsPerson() {
		return opsPerson;
	}

	public void setOpsPerson(String opsPerson) {
		this.opsPerson = opsPerson;
	}

	public String getPersonForwarder() {
		return personForwarder;
	}

	public void setPersonForwarder(String personForwarder) {
		this.personForwarder = personForwarder;
	}

}
