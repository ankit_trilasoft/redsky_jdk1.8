delimiter $$
DROP PROCEDURE IF EXISTS `redsky`.`ServiceOrder_UpdateStatus`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `ServiceOrder_UpdateStatus`(Nshipnumber varchar(20),NupdatedBy varchar(82))
begin
declare NStatus  varchar(6);
declare statusNo  int(11);
declare Packingdateactual  datetime;
declare Loadingdateactual  datetime;
declare sitOrigindate  datetime;
declare ActualDeparturedate  datetime;
declare Actualarrivaldate  datetime;
declare sitDestinationdate  datetime;
declare deliveryAdate  datetime;
declare OStatus  varchar(6);
declare l_StatusNumber bigint(20);
declare NetworkGroup bit(1);
declare UnetworkAgentExSO varchar(25);
declare UbookingAgentExSO varchar(25);
declare ClaimCreatedon datetime;
declare serviceorderNid bigint(20);
declare l_customerfileid bigint(20);
declare l_jobType varchar(20);
declare l_storage  bigint;
declare l_storageOut  datetime;
declare l_distinctStatus bigint;
declare l_billComplete datetime;



select t.packA,t.loadA,sitOriginOn,t.sitDestinationIn,
t.deliveryA,s.Status,s.statusnumber,t.soNetworkGroup,t.networkAgentExSO,t.bookingAgentExSO,s.id,s.customerfileid,s.job,b.billComplete
into Packingdateactual,Loadingdateactual,sitOrigindate,sitDestinationdate,
deliveryAdate,OStatus,l_StatusNumber,NetworkGroup,UnetworkAgentExSO,UbookingAgentExSO,serviceorderNid,l_customerfileid,l_jobType,l_billComplete
from serviceorder s, trackingstatus t, billing b
where s.shipnumber=Nshipnumber
and s.id=t.id and s.corpid=t.corpid
and s.id=b.id and s.corpid=b.corpid
group by s.shipnumber;


select atDepart into ActualDeparturedate from servicepartner
where shipnumber=Nshipnumber
and status=true
and atDepart is not null
group by shipnumber;

select  atArrival  into Actualarrivaldate from servicepartner
where shipnumber=Nshipnumber
and status=true
and atArrival is not null
group by shipnumber;

select createdon into ClaimCreatedon
from  claim
where serviceorderid=serviceorderNid  and status ='New'
group by serviceorderid;

if(OStatus not in('HOLD','CLSD','CNCL','DWNLD','TCNL') and l_jobType not in('STO','TPS','STF','STL','RLO')) then

if((l_billComplete is not null) and (DATEDIFF(now(),deliveryAdate)>=90)) then
set NStatus='2CLS';
set statusNo='500';
elseif(ClaimCreatedon is not null) then
set NStatus='CLMS';
set statusNo='70';
elseif(deliveryAdate is not null) then
set NStatus='DLVR';
set statusNo='60';
elseif(sitDestinationdate is not null) then
set NStatus='DSIT';
set statusNo='50';
elseif(Actualarrivaldate is not null) then
set NStatus='DSRV';
set statusNo='40';
elseif(ActualDeparturedate is not null) then
set NStatus='TRNS';
set statusNo='30';
elseif(sitOrigindate is not null) then
set NStatus='OSIT';
set statusNo='20';
elseif(Packingdateactual is not null  or Loadingdateactual is not null) then
set NStatus='OSRV';
set statusNo='10';
else
set NStatus='NEW';
set statusNo='1';
end if;

update serviceorder
set status= NStatus,
StatusNumber=statusNo ,statusDate=now(),
updatedby=NupdatedBy,updatedon=now()
where shipnumber=Nshipnumber and status !=NStatus ;

end if;

if(OStatus not in('HOLD','CLSD','CNCL','TCNL') and l_jobType in('STO','TPS','STF','STL')) then

select count(*) into l_storage from storage where shipNumber= Nshipnumber and releaseDate is null;
select storageOut into l_storageOut from billing where shipNumber= Nshipnumber;

if(l_storageOut is not null) then
  set NStatus='2CLS';
set statusNo='500';
elseif((l_storage >0) OR OStatus='STG') then
set NStatus='STG';
set statusNo='80';
else
set NStatus='NEW';
set statusNo='1';
end if;

update serviceorder
set status= NStatus,
StatusNumber=statusNo ,statusDate=now(),
updatedby=NupdatedBy,updatedon=now()
where shipnumber=Nshipnumber and status !=NStatus;

end if;

if(OStatus in('HOLD','CLSD','CNCL','TCNL')) then
  if(NStatus is null) then
    set NStatus = OStatus; 
    set statusNo = l_StatusNumber;

    update serviceorder set status= NStatus, StatusNumber=statusNo,statusDate=now(), updatedby=NupdatedBy,updatedon=now() where shipnumber=Nshipnumber and status !=NStatus;

  end if;
end if;





if(l_jobType not in('STO','TPS','STF','STL')) then
if(statusNo between 10 and 60) then
  update customerfile set status='ACTIVE',statusnumber='20',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='ACTIVE';
end if;

if(statusNo =70) then
  update customerfile set status='CLAIM',statusnumber='40',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='CLAIM';
end if;

if(NStatus ='HOLD') then
  update customerfile set status='HOLD',statusnumber='200',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='HOLD';
end if;
if(OStatus ='REOP') then
  update customerfile set status='REOPEN',statusnumber='1',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='REOPEN';
end if;

end if;

if(l_jobType in('STO','TPS','STF','STL')) then

if(l_storageOut is not null) then
  set NStatus='2CLS';
set statusNo='500';
elseif((l_storage >0)OR OStatus ='STG') then
set NStatus='STG';
set statusNo='80';
else
set NStatus='NEW';
set statusNo='1';
end if;

if(statusNo =500) then
  update customerfile set status='STORG',statusnumber='50',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='STORG';
end if;
if(statusNo between 10 and 60) then
  update customerfile set status='ACTIVE',statusnumber='20',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='ACTIVE';
end if;

if(statusNo =70) then
  update customerfile set status='CLAIM',statusnumber='40',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='CLAIM';
end if;

if(NStatus ='HOLD') then
  update customerfile set status='NEW',statusnumber='200',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='NEW';
end if;

if(OStatus ='REOP') then
  update customerfile set status='REOPEN',statusnumber='1',updatedby=NupdatedBy,updatedon=now(),statusDate=now() where id=l_customerfileid and status!='REOPEN';
end if;
end if;

 call CustomerFile_UpdateStatus(l_customerfileid,NupdatedBy);

end$$

