package com.trilasoft.app.dao;


import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.FrequentlyAskedQuestions;

public interface FrequentlyAskedQuestionsDao extends GenericDao<FrequentlyAskedQuestions, Long>{
public List getFaqByPartnerCode(String partnerCode,String corpId);
public List getNewFaqByPartnerCode(String partnerCode,String corpId);
public Boolean getExcludeFPU(String partnerCode,String corpId);
public int checkFrequentlyAskedQuestions(String question,String answer,String language,String partnerCode,String sessionCorpID);
public List getFaqReference(String partnerCode, String corpID,Long parentId);
public List findMaximum(String language,String partnerCode,String corpID);
public int checkFrequentlySequence(String language,String partnerCode,Long sequenceNumber,String sessionCorpID);
public List getFAQByPartnerCode(String partnerCode,String corpId);
public List getFAQByCMMDMMPartnerCode(String partnerCode,String corpId);
public List getFAQByAdmin(String corpId);
public List findMaximumNumber(String language,String corpID);
public void deleteagentParentPolicyFile(String oldAgentParent,String partnerCode,String sessionCorpID);
public void deleteChildDocument(Long id,String sessionCorpID);
public int deletedChildFAQ(String partnerCode, String sessionCorpID);
}
 
