package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.io.*;
import java.io.*;
import org.apache.struts2.ServletActionContext;

public final class login_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static java.util.Vector _jspx_dependants;

  static {
    _jspx_dependants = new java.util.Vector(7);
    _jspx_dependants.add("/common/taglibs.jsp");
    _jspx_dependants.add("/WEB-INF/appfuse.tld");
    _jspx_dependants.add("/WEB-INF/breadcrumbs.tld");
    _jspx_dependants.add("/WEB-INF/redsky-config-by-corp.tld");
    _jspx_dependants.add("/WEB-INF/redsky-security.tld");
    _jspx_dependants.add("/newclient.jsp");
    _jspx_dependants.add("/scripts/login.js");
  }

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_set_var;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_fmt_message_key_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_url_value_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_hidden_value_name_nobody;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_if_test;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_form_validate_name_method_action;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_text_name;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_param;
  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_s_text_name_nobody;

  private org.apache.jasper.runtime.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_set_var_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_set_var = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_fmt_message_key_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_url_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_hidden_value_name_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_c_if_test = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_form_validate_name_method_action = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_text_name = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_param = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
    _jspx_tagPool_s_text_name_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_set_var_value_nobody.release();
    _jspx_tagPool_c_set_var.release();
    _jspx_tagPool_fmt_message_key_nobody.release();
    _jspx_tagPool_c_url_value_nobody.release();
    _jspx_tagPool_s_hidden_value_name_nobody.release();
    _jspx_tagPool_c_if_test.release();
    _jspx_tagPool_s_form_validate_name_method_action.release();
    _jspx_tagPool_s_text_name.release();
    _jspx_tagPool_s_param.release();
    _jspx_tagPool_s_text_name_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    JspFactory _jspxFactory = null;
    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;


    try {
      _jspxFactory = JspFactory.getDefaultFactory();
      response.setContentType("text/html;charset=utf-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			"/error.htm", true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      String resourceInjectorClassName = config.getInitParameter("com.sun.appserv.jsp.resource.injector");
      if (resourceInjectorClassName != null) {
        _jspx_resourceInjector = (org.apache.jasper.runtime.ResourceInjector) Class.forName(resourceInjectorClassName).newInstance();
        _jspx_resourceInjector.setContext(application);
      }

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \r\n");
      out.write(" \r\n");
      out.write(" \n");
      if (_jspx_meth_c_set_0(_jspx_page_context))
        return;
      out.write('\n');
      if (_jspx_meth_c_set_1(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("    <title>");
      if (_jspx_meth_fmt_message_1(_jspx_page_context))
        return;
      out.write("</title>\r\n");
      out.write("    <meta name=\"heading\" content=\"");
      if (_jspx_meth_fmt_message_2(_jspx_page_context))
        return;
      out.write("\"/>\r\n");
      out.write("    <meta name=\"menu\" content=\"Login\"/>    \r\n");
      out.write("    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"");
      if (_jspx_meth_c_url_0(_jspx_page_context))
        return;
      out.write("\" />\r\n");
      out.write("    <link rel=\"shortcut icon\" href=\"");
      if (_jspx_meth_c_url_1(_jspx_page_context))
        return;
      out.write("\" type=\"image/x-icon\" />\r\n");
      out.write("   <style>\r\n");
      out.write("\tbody{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}   \r\n");
      out.write("\t\r\n");
      out.write("\t#leftcolumn{ margin-top:60px !important;}\r\n");
      out.write("\t    \r\n");
      out.write("\tdiv#branding {float: left; width: 100%; margin: 0;text-align: left;height:0px;} \r\n");
      out.write("\t\r\n");
      out.write("\t#wrapper {margin:0 auto; width:950px;}  \r\n");
      out.write("\t     \r\n");
      out.write("\tdiv#footer div#divider div {border-top:5px solid #EBF5FC; margin:1px 0 0; }\r\n");
      out.write("\t\r\n");
      out.write("\t#logincontainer{ width:915px; height:376px;!height:370px; background-image:url(images/loginbg-final.gif); background-repeat:no-repeat; margin:0px auto; margin-top:10px;}\r\n");
      out.write("\t\r\n");
      out.write("\tdiv.messageSussess {background: none repeat scroll 0 0 rgb(179, 218, 244) !important; border: 1px solid rgb(0, 0, 0) !important;border-radius: 0.3em !important;color: rgb(0, 0, 0);font-family: Arial, Helvetica, sans-serif;\r\n");
      out.write("\tfont-size: 11px;font-weight: normal;margin: 0 auto !important;margin-top:3px;padding: 5px;text-align: center; vertical-align: bottom;width: 400px !important;z-index: 1000;}\r\n");
      out.write("\t\r\n");
      out.write("\t#errormessage {width:300px; list-style:none;margin:0px auto;}\r\n");
      out.write("\r\n");
      out.write("\timg.validationWarning, div.error img.icon, div.message img.icon, li.error img.icon {\r\n");
      out.write("\t background:transparent none repeat scroll 0 0 !important; border:0 none !important; margin-left:3px; padding-bottom:1px;\r\n");
      out.write("\t vertical-align:middle; width:14px; height:14px;}\r\n");
      out.write(" \r\n");
      out.write("\t.cssbutton {width: 55px; height: 25px; padding:0px; font-family:'arial'; font-weight:normal; color:#000;font-size:12px;\r\n");
      out.write("\t background:url(/redsky/images/btn_bg.gif) repeat-x; background-color:#e7e7e7;border:1px solid #cccccc; }\t\r\n");
      out.write("\t.cssbutton:hover {color: black; background:url(/redsky/images/btn_hoverbg.gif) repeat-x;background-color: #c1c2bf;}\r\n");
      out.write("\t\r\n");
      out.write("\t.textbox_username {background-color:#E7E7E7;border:1px solid #B2B2B2; color:#616161;font-family:Arial,Helvetica,sans-serif;\r\n");
      out.write("     font-size:12px; width:135px;background: #E7E7E7 url('images/user.png') no-repeat; background-position: 1 1;padding-left: 19px; }\r\n");
      out.write("\t.textbox_password {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;\r\n");
      out.write("\tbackground: #E7E7E7 url('images/icon_password.png') no-repeat;background-position: 1 1;padding-left: 19px;}\r\n");
      out.write("\r\n");
      out.write("\tdiv.message{background: none repeat scroll 0 50% #F5DFDF !important; border: 1px solid #CE9E9E !important;\r\n");
      out.write("    border-radius: 0.3em 0.3em 0.3em 0.3em !important;color: #000000;font-family: Arial,Helvetica,sans-serif;\r\n");
      out.write("    font-size: 1em;font-weight: normal;padding: 5px;text-align: center;vertical-align: bottom; /* width: 22.5% !important;*/\r\n");
      out.write("    width: 287px !important; margin:0px auto !important; margin-top:3px; z-index: 1000; display:none;}\r\n");
      out.write("\r\n");
      out.write("\tdiv.error, span.error, li.error {color:#000000;font-family:Arial,Helvetica,sans-serif; font-weight:normal;text-align:center;\r\n");
      out.write("\tvertical-align:bottom; z-index: 1000;font-size: 1em;}\r\n");
      out.write("\r\n");
      out.write("\tform li.error{ background-color:  #F5DFDF !important; border: 1px solid #F5ACA6 !important;border-radius: 0.3em 0.3em 0.3em 0.3em;\r\n");
      out.write("    color: #000000; margin: 2px 0 !important;padding: 1.5px !important;width: 294px;text-align:center !important; }\r\n");
      out.write("\r\n");
      out.write("\t#inlogo {margin-right: 0;margin-top: -1px;padding: 0;text-align: right;width:87px;height:89px;float:right;background-image: url(\"images/IN-Icon.gif\");\r\n");
      out.write("    background-repeat: no-repeat; }\r\n");
      out.write(" </style>\r\n");
      out.write("  </head>  \r\n");
      out.write("\t<script>\r\n");
      out.write("    function clear_fileds(){\r\n");
      out.write("       \tdocument.forms['loginForm'].elements['j_username'].value = \"\";\r\n");
      out.write("    \tdocument.forms['loginForm'].elements['j_password'].value = \"\";\r\n");
      out.write("    }\r\n");
      out.write("    \r\n");
      out.write("    </script>\r\n");
      out.write("    <script language=\"javascript\" type=\"text/javascript\">\r\n");
      out.write(" \tfunction forgotPassword()\r\n");
      out.write(" \t{\r\n");
      out.write(" \tdocument.forms['userOTPForm'].elements['OTPusername'].value = document.forms['loginForm'].elements['j_username'].value\r\n");
      out.write(" \tlocation.href= \"findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true\";\r\n");
      out.write("\tdocument.forms['userOTPForm'].submit();\r\n");
      out.write(" \t\t\r\n");
      out.write(" \t}\r\n");
      out.write(" \t\r\n");
      out.write(" \tfunction openPartnerRequest(){\r\n");
      out.write(" \tvar sessionCorpID=document.forms['loginForm'].elements['corpID'].value;\r\n");
      out.write("\tlocation.href=\"partnerRequestAccess.html?decorator=popup&popup=true&bypass_sessionfilter=YES&sessionCorpID=\"+sessionCorpID;\t\r\n");
      out.write("}\r\n");
      out.write(" </script>\r\n");
      out.write(" \r\n");
      out.write(" <script>\r\n");
      out.write("window.onload = function() {\r\n");
      out.write("\tvar scrollOne = new tScroll();\r\n");
      out.write("\tscrollOne.init('Scroller');\r\n");
      out.write("\r\n");
      out.write("}\r\n");
      out.write("\r\n");
      out.write("function effectFunction(element)\r\n");
      out.write("{\r\n");
      out.write("   new Effect.Opacity(element, {from:0, to:1.0, duration:0.5});\r\n");
      out.write("}\r\n");
      out.write("</script>\r\n");
      out.write("<script language=\"javascript\" type=\"text/javascript\">\r\n");
      out.write("function getCookie(name) {\r\n");
      out.write("    var nameEQ = name + \"=\";\r\n");
      out.write("    var ca = document.cookie.split(';');\r\n");
      out.write("    for(var i=0;i < ca.length;i++) {\r\n");
      out.write("        var c = ca[i];\r\n");
      out.write("        while (c.charAt(0)==' ') c = c.substring(1,c.length);\r\n");
      out.write("        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);\r\n");
      out.write("    }\r\n");
      out.write("    return null;\r\n");
      out.write("}\r\n");
      out.write("var today = new Date(); \r\n");
      out.write("var expiry = new Date(today.getTime() + 505000); \r\n");
      out.write("function setCookie(name, value) { \r\n");
      out.write("\tdocument.cookie=name + \"=\" + escape(value) + \"; path=/; expires=\" + expiry.toGMTString(); \r\n");
      out.write("}\r\n");
      out.write("function loginStatusCheck(){\r\n");
      out.write("\tvar soDiv = document.getElementById(\"successMessages\");\r\n");
      out.write("\tsoDiv.style.display = 'none';\r\n");
      out.write("\tvar userNameStatus=getCookie('username');\r\n");
      out.write("\t//var currentPassword = getCookie('currentPassword');\r\n");
      out.write("\tvar url = \"updateSucFail.html?ajax=1&decorator=simple&popup=true&bypass_sessionfilter=YES&userNameStatus=\"+encodeURIComponent(userNameStatus);   \r\n");
      out.write("\thttp.open(\"GET\", url, true);\r\n");
      out.write("\thttp.onreadystatechange = function(){  handleHttpResponseUserAuthenticate(userNameStatus);};\r\n");
      out.write("\thttp.send(null);\r\n");
      out.write("}\r\n");
      out.write("function handleHttpResponseUserAuthenticate(userNameStatus){\r\n");
      out.write(" if (http.readyState == 4){\r\n");
      out.write("   var results = http.responseText\r\n");
      out.write("  \r\n");
      out.write("   results = results.trim();\r\n");
      out.write("   if(results.length>0){\r\n");
      out.write("\t  \r\n");
      out.write("\t   var soDiv = document.getElementById(\"successMessages\");\r\n");
      out.write("\t   soDiv.style.display = 'block';\r\n");
      out.write("\t   soDiv.innerHTML = results;\r\n");
      out.write("         } }  } \r\n");
      out.write("var http = getHTTPObject();\r\n");
      out.write("function getHTTPObject() {\r\n");
      out.write("var xmlhttp;\r\n");
      out.write("if(window.XMLHttpRequest) {\r\n");
      out.write("xmlhttp = new XMLHttpRequest();\r\n");
      out.write("}\r\n");
      out.write("else if (window.ActiveXObject) {\r\n");
      out.write("xmlhttp=new ActiveXObject(\"Microsoft.XMLHTTP\");\r\n");
      out.write("if (!xmlhttp) {\r\n");
      out.write("    xmlhttp=new ActiveXObject(\"Msxml2.XMLHTTP\");\r\n");
      out.write("}\r\n");
      out.write("}\r\n");
      out.write("return xmlhttp;\r\n");
      out.write("}\r\n");
      out.write(" </script>\r\n");
      out.write("\r\n");
      out.write("<body id=\"login\"/ style=\"text-align:center\">\r\n");
      out.write("\r\n");
      out.write(" \r\n");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<form method=\"post\" id=\"loginForm\" name=\"loginForm\" action=\"");
      if (_jspx_meth_c_url_2(_jspx_page_context))
        return;
      out.write("\"\r\n");
      out.write("    onsubmit=\"saveUsername(this); return validateForm(this),submit_form();\">\r\n");
      //  c:set
      org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_2 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
      _jspx_th_c_set_2.setPageContext(_jspx_page_context);
      _jspx_th_c_set_2.setParent(null);
      _jspx_th_c_set_2.setVar("corpID");
      _jspx_th_c_set_2.setValue(request.getParameter("corpID") );
      int _jspx_eval_c_set_2 = _jspx_th_c_set_2.doStartTag();
      if (_jspx_th_c_set_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
        return;
      }
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_2);
      out.write('\r');
      out.write('\n');
      //  s:hidden
      org.apache.struts2.views.jsp.ui.HiddenTag _jspx_th_s_hidden_0 = (org.apache.struts2.views.jsp.ui.HiddenTag) _jspx_tagPool_s_hidden_value_name_nobody.get(org.apache.struts2.views.jsp.ui.HiddenTag.class);
      _jspx_th_s_hidden_0.setPageContext(_jspx_page_context);
      _jspx_th_s_hidden_0.setParent(null);
      _jspx_th_s_hidden_0.setName("corpID");
      _jspx_th_s_hidden_0.setValue(request.getParameter("corpID") );
      int _jspx_eval_s_hidden_0 = _jspx_th_s_hidden_0.doStartTag();
      if (_jspx_th_s_hidden_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_s_hidden_value_name_nobody.reuse(_jspx_th_s_hidden_0);
        return;
      }
      _jspx_tagPool_s_hidden_value_name_nobody.reuse(_jspx_th_s_hidden_0);
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_if_0(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("  ");
      //  c:if
      org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_1 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
      _jspx_th_c_if_1.setPageContext(_jspx_page_context);
      _jspx_th_c_if_1.setParent(null);
      _jspx_th_c_if_1.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${globalMsg != null && globalMsg!=''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_if_1 = _jspx_th_c_if_1.doStartTag();
      if (_jspx_eval_c_if_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("   \r\n");
          out.write("   <div class=\"messageSussess\" id=\"successMessages\"><img class=\"icon\" style=\"vertical-align:top;\" alt=\"Information\" src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("/images/iconInformation.gif\">&nbsp;");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${globalMsg}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</div>\r\n");
          out.write("   \r\n");

    application.setAttribute("globalMsg", "");

          out.write("\r\n");
          out.write("   \r\n");
          out.write("  ");
          int evalDoAfterBody = _jspx_th_c_if_1.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_if_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
        return;
      }
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_1);
      out.write("\r\n");
      out.write("  <div id=\"errormessage\">\r\n");
      out.write("  ");
      //  c:if
      org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_2 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
      _jspx_th_c_if_2.setPageContext(_jspx_page_context);
      _jspx_th_c_if_2.setParent(null);
      _jspx_th_c_if_2.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${restrictLoginMsg != null && restrictLoginMsg!=''}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
      int _jspx_eval_c_if_2 = _jspx_th_c_if_2.doStartTag();
      if (_jspx_eval_c_if_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
        do {
          out.write("   \r\n");
          out.write("   <li class=\"error\" style =\"font-size:11px\"><img class=\"icon\" style=\"vertical-align:top;\" alt=\"");
          if (_jspx_meth_fmt_message_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_2, _jspx_page_context))
            return;
          out.write("\" src=\"");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ctx}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("/images/logerror.png\">&nbsp;");
          out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${restrictLoginMsg}", java.lang.String.class, (PageContext)_jspx_page_context, null));
          out.write("</li>\r\n");
          out.write("\t\t");

		    application.setAttribute("restrictLoginMsg", "");
		
          out.write("\r\n");
          out.write("  ");
          int evalDoAfterBody = _jspx_th_c_if_2.doAfterBody();
          if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
            break;
        } while (true);
      }
      if (_jspx_th_c_if_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
        _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
        return;
      }
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_2);
      out.write("\r\n");
      out.write("  \r\n");
      out.write("    \r\n");
      if (_jspx_meth_c_if_3(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_c_if_4(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("</div>\r\n");
      out.write("<div id=\"wrapper\" style=\"margin:0px auto; text-align:center;\">\r\n");
      out.write("\r\n");
      out.write("<div id=\"logincontainer\" style=\"margin-bottom:1px;\" >\r\n");
      out.write("<div style=\"margin: 0px; padding: 0px; width: 892px; float: left; position: relative; clear: both;\">\r\n");
      out.write("<div id=\"indexlogo\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/title.gif\"  style=\"cursor: default;\"/></div>\r\n");
      out.write("<div id=\"inlogo\">\r\n");
      out.write("<div id=\"inTiny\"><a href=\"https://www.linkedin.com/company/redsky-new-features?trk=biz-brand-tree-co-name\" target=\"_blank\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/in16.gif\" style=\"cursor: inherit;border:none;\"/></a></div>\r\n");
      out.write("</div>\r\n");
      out.write("</div>\r\n");
      out.write("<div id=\"marquee\">");

     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/clients.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }

      out.write("</div>\r\n");
      out.write("<div id=\"leftcolumn\">\r\n");
      out.write("  <table width=\"360\" border=\"0\" align=\"right\" cellpadding=\"0\" cellspacing=\"0\">  \r\n");
      out.write("  <td style=\"!padding-left:24px;\" align=\"left\" ></td>\r\n");
      out.write("    <tr>    \r\n");
      out.write("      ");
      out.write("</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("     <tr>\r\n");
      out.write("     <td>");

     try{
  FileInputStream data = new FileInputStream("/usr/local/redskydoc/RSuserMessage.txt");
  //FileInputStream data = new FileInputStream("C:/home/RSuserMessage.txt");
  BufferedInputStream file = new BufferedInputStream(data);
   int c = -1;
    while((c = file.read()) != -1) {
   out.write(c);
      } 	
   file.close();
    }catch(Exception e){
	
    }

      out.write("</td>\r\n");
      out.write("      <td align=\"left\" class=\"content\">&nbsp;</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("  </table>\r\n");
      out.write("  <table border=\"0\" align=\"left\" cellpadding=\"0\" cellspacing=\"0\" style=\"margin-left:65px\">\r\n");
      out.write("  <tr>\r\n");
      out.write("  <td style=\"\">&nbsp;</td>\r\n");
      out.write("  </tr>\r\n");
      out.write("  \r\n");
      out.write("  <tr>\r\n");
      out.write("  <td style=\"width:26px;\">&nbsp;</td>\r\n");
      out.write("  <td style=\"!padding-left:24px;\" align=\"left\">\r\n");
      out.write("  <!--<a href=\"//privacy.truste.com/privacy-seal/RedSky-Mobility-Solutions,-LLC/validation?rid=3470f34c-05bb-4946-96d8-a87d03b1cb29\" title=\"Validate TRUSTe privacy certification\" target=\"_blank\">\r\n");
      out.write("  <img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/TRUSTeSeal.png\" border=\"0\" alt=\"Validate TRUSTe privacy certification\" />\r\n");
      out.write("  </a>\r\n");
      out.write("  \r\n");
      out.write("  -->\r\n");
      out.write("  \r\n");
      out.write("  </td>\r\n");
      out.write("  </tr>\r\n");
      out.write("  </table>\r\n");
      out.write("</div>\r\n");
      out.write("\r\n");
      out.write("<div id=\"rightcolumn\" style=\"text-align:left;\">\r\n");
      out.write("  <table width=\"285\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td width=\"60\" class=\"content\">Username</td>\r\n");
      out.write("      <td colspan=\"2\"><label>\r\n");
      out.write("        <input type=\"text\" class=\"textbox_username\" name=\"j_username\" id=\"j_username\" tabindex=\"1\" />\r\n");
      out.write("      </label></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td height=\"15px\">&nbsp;</td>\r\n");
      out.write("      <td colspan=\"2\">&nbsp;</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td class=\"content\">Password</td>\r\n");
      out.write("      <td colspan=\"2\"><input type=\"password\" class=\"textbox_password\" name=\"j_password\" id=\"j_password\" tabindex=\"2\" />\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td colspan=\"3\" class=\"content\" height=\"5\"></td>\r\n");
      out.write("      </tr>\r\n");
      out.write("      ");
      if (_jspx_meth_c_if_5(_jspx_page_context))
        return;
      out.write(" \r\n");
      out.write("    <tr>\r\n");
      out.write("      <td class=\"content\" height=\"20px\">&nbsp;</td>\r\n");
      out.write("      <td colspan=\"2\">&nbsp;</td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td class=\"content\">&nbsp;</td>\r\n");
      out.write("      <td colspan=\"2\"><input type=\"submit\" class=\"cssbutton\" name=\"login\" value=\"");
      if (_jspx_meth_fmt_message_9(_jspx_page_context))
        return;
      out.write("\" tabindex=\"4\" style=\"\">&nbsp;<input type=\"button\" class=\"cssbutton\" name=\"Clear\" value=\"Clear\" onclick=\"clear_fileds();\" style=\"\"></td>\r\n");
      out.write("    </tr>    \r\n");
      out.write("   <tr>     \r\n");
      out.write("       <td colspan=\"2\" style=\"height:20px;!height:0px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>     \r\n");
      out.write("       <td colspan=\"2\" style=\"height:20px;!height:0px;\"></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr>\r\n");
      out.write("      <td class=\"content\">&nbsp;</td>\r\n");
      out.write("      <td colspan=\"2\"><table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n");
      out.write("         <tr>\r\n");
      out.write("        <td align=\"center\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/helpicon.gif\" width=\"26\" height=\"23\" /></td>\r\n");
      out.write("        <td class=\"formtext\" colspan=\"2\" align=\"left\" style=\"padding-left: 4px;color: #71b9c3;\">\r\n");
      out.write("          <script language=\"javascript\">\r\n");
      out.write("\r\n");
      out.write("           document.write('<a href=\"javascript:forgotPassword()\" class=\"otherlinks\" >Forgot Password</a>');\r\n");
      out.write("\r\n");
      out.write("                      </script>\r\n");
      out.write("                     \r\n");
      out.write("                     \r\n");
      out.write("                      \r\n");
      out.write("                      </td></tr>  \r\n");
      out.write("        ");
      out.write("\r\n");
      out.write("        <tr>\r\n");
      out.write("          <td align=\"center\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/helpicon.gif\" width=\"26\" height=\"23\" /></td>\r\n");
      out.write("          <td align=\"left\"><a href=\"partnerRequestAccess.html?bypass_sessionfilter=YES&sessionCorpID=TSFT&decorator=popup&popup=true\" class=\"otherlinks\">Request Agent Portal Access</a></td>\r\n");
      out.write("        </tr>\r\n");
      out.write("        <!--<tr>\r\n");
      out.write("        <td align=\"center\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/helpicon.gif\" width=\"26\" height=\"23\" /></td>\r\n");
      out.write("      <td align=\"left\" class=\"content\" style=\"font-size:11px;\"><a href=\"http://redskymobility.com/redskyforums/phpBB3/ucp.php?mode=login\" target=\"_blank\"><font color=\"#fe000c\">Red</font><font color=\"#1f1a17\">Sky Forums</font></a></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("        <tr>\r\n");
      out.write("        <td align=\"center\"><img src=\"");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("/images/helpicon.gif\" width=\"26\" height=\"23\" /></td>\r\n");
      out.write("        <td align=\"left\"><a href=\"javascript:alert('Your screen resolution is '+screen.width+'x'+screen.height);\" class=\"otherlinks\">Click for your screen resolution</a>\r\n");
      out.write("        </td>\r\n");
      out.write("        </tr>-->\r\n");
      out.write("      </table></td>\r\n");
      out.write("    </tr>\r\n");
      out.write("    <tr><!--\r\n");
      out.write("    ");
      if (_jspx_meth_c_if_6(_jspx_page_context))
        return;
      out.write("\r\n");
      out.write("    --></tr>\r\n");
      out.write("  </table>\r\n");
      out.write("  \r\n");
      out.write("     </div>\r\n");
      out.write("     </div>   \r\n");
      out.write("  <!-- Modified By Arbind at 16-Feb-2012 -->   \r\n");
      out.write("      \r\n");
      out.write(" \r\n");
      out.write(" ");
      out.write("\r\n");
      out.write("\r\n");
      out.write("<head>\r\n");
      out.write("<style media=\"all\" type=\"text/css\">\t\r\n");
      out.write("/* Thumb Scroll CSS */\r\n");
      out.write("#logoScroller { float: left; height: 72px; /* margin: 5px 20px 0;*/ margin:2px 0 0 10px; overflow: hidden;position: relative;width: 515px;z-index: 1;}\r\n");
      out.write("#Scroller {position: absolute;width: 600px;top: 0px;}\r\n");
      out.write("#Scroller img {\tfloat: left;margin-right: 25px;\tdisplay: inline;cursor:pointer;\tborder:none;}/* End */\r\n");
      out.write(".trust {float: left; margin-left: 151px; margin-top: 15px;}\t\r\n");
      out.write("#screen {font:11px normal; font-family:arial,verdana;width:660px; /* margin:0px auto; text-align:center; */\tmargin-bottom:0px;margin-left:0px;}\r\n");
      out.write("#privacy {width:868px; margin:0px auto;\tbackground-color:#FFF;height:90px;}\r\n");
      out.write("#privacyText {float: left; font: 12px/15px arial,verdana; margin-top:33px;padding-left:45px;text-align: left;width: 740px;}\t\r\n");
      out.write("#privacyText a { text-decoration: underline; color: #0428db}\t\r\n");
      out.write("#privacyLogo {border: 0 none; float: right;margin-left: 185px;padding-top: 3px;text-align: left;width: 162px;}\r\n");
      out.write(".PP_Bg { background-image: url(\"images/PricePoint.png\");background-repeat: no-repeat;height: 67px;margin-top: 11px; width: 323px;}\r\n");
      out.write("</style>\r\n");
      out.write("</head>\r\n");
      out.write("  ");
      out.write("\r\n");
      out.write("  \r\n");
      out.write("  <div id=\"privacy\">  \r\n");
      out.write("\t  <div id=\"privacyText\" style=\"    margin-top: 22px;\">\r\n");
      out.write("\t  <!-- RedSky complies with the US-EU & US-Swiss <a href=\"http://www.export.gov/safeharbor\" target=\"_blank\"><font color=\"#0428db\">Safe Harbor Frameworks</font></a> for Data Privacy. -->\r\n");
      out.write("\tView our <a href=\"https://redskymobility.com/privacy-policy\" target=\"_blank\" style=\"margin-top: 15px !important; display: inline-block;\">Privacy Policy</a>.\r\n");
      out.write("\t  \r\n");
      out.write("\t  <a href=\"//privacy.truste.com/privacy-seal/validation?rid=8aae3976-6093-4864-be7e-580286bfa0fe\" target=\"_blank\" style=\"float: right;\"><img style=\"border: none\" src=\"//privacy-policy.truste.com/privacy-seal/seal?rid=8aae3976-6093-4864-be7e-580286bfa0fe\" alt=\"TRUSTe\"/></a>\r\n");
      out.write("\t  \r\n");
      out.write("\t  \r\n");
      out.write("\t  </div>  \r\n");
      out.write("  <div style=\"float:left;border:none;\">  \r\n");
      out.write("  ");
      out.write("  \r\n");
      out.write(" </div>  \r\n");
      out.write(" </div>");
      out.write("\r\n");
      out.write("  <!-- end 16-Feb-2012 --> \r\n");
      out.write("     </div>\r\n");
      out.write(" </form>\r\n");
      out.write(" <script type=\"text/javascript\">\r\n");
      out.write("<!--\r\n");
      out.write("function popup(url) \r\n");
      out.write("{\r\n");
      out.write(" var width  = 850;\r\n");
      out.write(" var height = 800;\r\n");
      out.write(" var left   = (screen.width  - width)/2;\r\n");
      out.write(" var top    = (screen.height - height)/2;\r\n");
      out.write(" var params = 'width='+width+', height='+height;\r\n");
      out.write(" params += ', left='+left;\r\n");
      out.write(" params += ', directories=no';\r\n");
      out.write(" params += ', location=no';\r\n");
      out.write(" params += ', menubar=no';\r\n");
      out.write(" params += ', resizable=no';\r\n");
      out.write(" params += ', scrollbars=yes';\r\n");
      out.write(" params += ', status=no';\r\n");
      out.write(" params += ', toolbar=no';\r\n");
      out.write(" newwin=window.open(url,'windowname5', params);\r\n");
      out.write(" if (window.focus) {newwin.focus()}\r\n");
      out.write(" return false;\r\n");
      out.write("}\r\n");
      out.write("// -->\r\n");
      out.write("</script>\r\n");
      out.write("<script type=\"text/javascript\">   \r\n");
      out.write("    document.forms['loginForm'].elements['j_username'].focus();  \r\n");
      out.write("    \r\n");
      out.write("</script> \r\n");
      if (_jspx_meth_c_if_7(_jspx_page_context))
        return;
      out.write('\r');
      out.write('\n');
      if (_jspx_meth_s_form_0(_jspx_page_context))
        return;
      out.write("    \r\n");
      out.write("<script type=\"text/javascript\">\n");
      out.write("function getCookie(name) {\n");
      out.write("\tvar prefix = name + \"=\" \n");
      out.write("\tvar start = document.cookie.indexOf(prefix) \n");
      out.write("\n");
      out.write("\tif (start==-1) {\n");
      out.write("\t\treturn null;\n");
      out.write("\t}\n");
      out.write("\t\n");
      out.write("\tvar end = document.cookie.indexOf(\";\", start+prefix.length) \n");
      out.write("\tif (end==-1) {\n");
      out.write("\t\tend=document.cookie.length;\n");
      out.write("\t}\n");
      out.write("\n");
      out.write("\tvar value=document.cookie.substring(start+prefix.length, end) \n");
      out.write("\treturn unescape(value);\n");
      out.write("}\n");
      out.write("\n");
      out.write("    if (getCookie(\"username\") != null) {\n");
      out.write("        document.forms['loginForm'].elements['j_username'].value = getCookie(\"username\");\n");
      out.write("        document.forms['loginForm'].elements['j_password'].focus();\n");
      out.write("    } else {\n");
      out.write("        document.forms['loginForm'].elements['j_username'].focus();\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("    function saveUsername(theForm) {\n");
      out.write("        var expires = new Date();\n");
      out.write("        expires.setTime(expires.getTime() + 24 * 30 * 60 * 60 * 1000); // sets it for approx 30 days.\n");
      out.write("        setCookie(\"username\",theForm.j_username.value,expires,\"");
      if (_jspx_meth_c_url_3(_jspx_page_context))
        return;
      out.write("\");\n");
      out.write("        //var expires1 = new Date();\n");
      out.write("        //expires1.setTime(expires1.getTime() + 60 * 1000);\n");
      out.write("        //setCookie(\"currentPassword\",theForm.j_password.value,expires1,\"");
      if (_jspx_meth_c_url_4(_jspx_page_context))
        return;
      out.write("\");\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("    function validateForm(form) {                                                               \n");
      out.write("        return validateRequired(form); \n");
      out.write("    } \n");
      out.write("    \n");
      out.write("    function passwordHint() {\n");
      out.write("        if (document.forms['loginForm'].elements['j_username'].value.length == 0) {\n");
      out.write("            alert(\"");
      if (_jspx_meth_s_text_0(_jspx_page_context))
        return;
      out.write("\");\n");
      out.write("            document.forms['loginForm'].elements['j_username'].focus();\n");
      out.write("        } else {\n");
      out.write("            location.href=\"");
      if (_jspx_meth_c_url_5(_jspx_page_context))
        return;
      out.write("?username=\" + document.forms['loginForm'].elements['j_username'].value;     \n");
      out.write("        }\n");
      out.write("    }\n");
      out.write("    \n");
      out.write("    function required () { \n");
      out.write("        this.aa = new Array(\"j_username\", \"");
      if (_jspx_meth_s_text_2(_jspx_page_context))
        return;
      out.write("\", new Function (\"varName\", \" return this[varName];\"));\n");
      out.write("        this.ab = new Array(\"j_password\", \"");
      if (_jspx_meth_s_text_4(_jspx_page_context))
        return;
      out.write("\", new Function (\"varName\", \" return this[varName];\"));\n");
      out.write("    } \n");
      out.write("</script>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
      }
    } finally {
      if (_jspxFactory != null) _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_set_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_0 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_0.setPageContext(_jspx_page_context);
    _jspx_th_c_set_0.setParent(null);
    _jspx_th_c_set_0.setVar("ctx");
    _jspx_th_c_set_0.setValue((java.lang.Object) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${pageContext.request.contextPath}", java.lang.Object.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_set_0 = _jspx_th_c_set_0.doStartTag();
    if (_jspx_th_c_set_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
      return true;
    }
    _jspx_tagPool_c_set_var_value_nobody.reuse(_jspx_th_c_set_0);
    return false;
  }

  private boolean _jspx_meth_c_set_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:set
    org.apache.taglibs.standard.tag.rt.core.SetTag _jspx_th_c_set_1 = (org.apache.taglibs.standard.tag.rt.core.SetTag) _jspx_tagPool_c_set_var.get(org.apache.taglibs.standard.tag.rt.core.SetTag.class);
    _jspx_th_c_set_1.setPageContext(_jspx_page_context);
    _jspx_th_c_set_1.setParent(null);
    _jspx_th_c_set_1.setVar("datePattern");
    int _jspx_eval_c_set_1 = _jspx_th_c_set_1.doStartTag();
    if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_c_set_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_c_set_1.doInitBody();
      }
      do {
        if (_jspx_meth_fmt_message_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_set_1, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_c_set_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_c_set_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_c_set_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_set_var.reuse(_jspx_th_c_set_1);
      return true;
    }
    _jspx_tagPool_c_set_var.reuse(_jspx_th_c_set_1);
    return false;
  }

  private boolean _jspx_meth_fmt_message_0(javax.servlet.jsp.tagext.JspTag _jspx_th_c_set_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_0 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_0.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_set_1);
    _jspx_th_fmt_message_0.setKey("date.format");
    int _jspx_eval_fmt_message_0 = _jspx_th_fmt_message_0.doStartTag();
    if (_jspx_th_fmt_message_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_0);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_0);
    return false;
  }

  private boolean _jspx_meth_fmt_message_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_1 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_1.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_1.setParent(null);
    _jspx_th_fmt_message_1.setKey("login.title");
    int _jspx_eval_fmt_message_1 = _jspx_th_fmt_message_1.doStartTag();
    if (_jspx_th_fmt_message_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_1);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_1);
    return false;
  }

  private boolean _jspx_meth_fmt_message_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_2 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_2.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_2.setParent(null);
    _jspx_th_fmt_message_2.setKey("login.heading");
    int _jspx_eval_fmt_message_2 = _jspx_th_fmt_message_2.doStartTag();
    if (_jspx_th_fmt_message_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_2);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_2);
    return false;
  }

  private boolean _jspx_meth_c_url_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_0 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_0.setPageContext(_jspx_page_context);
    _jspx_th_c_url_0.setParent(null);
    _jspx_th_c_url_0.setValue((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("/styles/${appConfig[\"csstheme\"]}/login-layout.css", java.lang.String.class, (PageContext)_jspx_page_context, null));
    int _jspx_eval_c_url_0 = _jspx_th_c_url_0.doStartTag();
    if (_jspx_th_c_url_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
    return false;
  }

  private boolean _jspx_meth_c_url_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_1 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_1.setPageContext(_jspx_page_context);
    _jspx_th_c_url_1.setParent(null);
    _jspx_th_c_url_1.setValue("/images/favicon2.ico");
    int _jspx_eval_c_url_1 = _jspx_th_c_url_1.doStartTag();
    if (_jspx_th_c_url_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
    return false;
  }

  private boolean _jspx_meth_c_url_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_2 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_2.setPageContext(_jspx_page_context);
    _jspx_th_c_url_2.setParent(null);
    _jspx_th_c_url_2.setValue("/j_security_check");
    int _jspx_eval_c_url_2 = _jspx_th_c_url_2.doStartTag();
    if (_jspx_th_c_url_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
    return false;
  }

  private boolean _jspx_meth_c_if_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_0 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_0.setPageContext(_jspx_page_context);
    _jspx_th_c_if_0.setParent(null);
    _jspx_th_c_if_0.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${param.error != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_0 = _jspx_th_c_if_0.doStartTag();
    if (_jspx_eval_c_if_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("   <div class=\"message\" id=\"successMessages\"></div>\r\n");
        out.write("  ");
        int evalDoAfterBody = _jspx_th_c_if_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_0);
    return false;
  }

  private boolean _jspx_meth_fmt_message_3(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_3 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_3.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_2);
    _jspx_th_fmt_message_3.setKey("icon.warning");
    int _jspx_eval_fmt_message_3 = _jspx_th_fmt_message_3.doStartTag();
    if (_jspx_th_fmt_message_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_3);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_3);
    return false;
  }

  private boolean _jspx_meth_c_if_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_3 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_3.setPageContext(_jspx_page_context);
    _jspx_th_c_if_3.setParent(null);
    _jspx_th_c_if_3.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${param.error != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_3 = _jspx_th_c_if_3.doStartTag();
    if (_jspx_eval_c_if_3 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("    <li class=\"error\">\r\n");
        out.write("        <img src=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ctx}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/images/logerror.png\" alt=\"");
        if (_jspx_meth_fmt_message_4((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_3, _jspx_page_context))
          return true;
        out.write("\" class=\"icon\"/>\r\n");
        out.write("        ");
        if (_jspx_meth_fmt_message_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_3, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("        ");
        out.write("\r\n");
        out.write("    </li>\r\n");
        int evalDoAfterBody = _jspx_th_c_if_3.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_3);
    return false;
  }

  private boolean _jspx_meth_fmt_message_4(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_4 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_4.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_4.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_3);
    _jspx_th_fmt_message_4.setKey("icon.warning");
    int _jspx_eval_fmt_message_4 = _jspx_th_fmt_message_4.doStartTag();
    if (_jspx_th_fmt_message_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_4);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_4);
    return false;
  }

  private boolean _jspx_meth_fmt_message_5(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_3, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_5 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_5.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_3);
    _jspx_th_fmt_message_5.setKey("errors.password.mismatch");
    int _jspx_eval_fmt_message_5 = _jspx_th_fmt_message_5.doStartTag();
    if (_jspx_th_fmt_message_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_5);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_5);
    return false;
  }

  private boolean _jspx_meth_c_if_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_4 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_4.setPageContext(_jspx_page_context);
    _jspx_th_c_if_4.setParent(null);
    _jspx_th_c_if_4.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${param.companySystemDefaultError != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_4 = _jspx_th_c_if_4.doStartTag();
    if (_jspx_eval_c_if_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("    <li class=\"error\">\r\n");
        out.write("        <img src=\"");
        out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${ctx}", java.lang.String.class, (PageContext)_jspx_page_context, null));
        out.write("/images/logerror.png\" alt=\"");
        if (_jspx_meth_fmt_message_6((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_4, _jspx_page_context))
          return true;
        out.write("\" class=\"icon\"/>\r\n");
        out.write("        ");
        if (_jspx_meth_fmt_message_7((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_4, _jspx_page_context))
          return true;
        out.write("\r\n");
        out.write("        ");
        out.write("\r\n");
        out.write("    </li>\r\n");
        int evalDoAfterBody = _jspx_th_c_if_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_4);
    return false;
  }

  private boolean _jspx_meth_fmt_message_6(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_6 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_6.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_6.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_4);
    _jspx_th_fmt_message_6.setKey("icon.warning");
    int _jspx_eval_fmt_message_6 = _jspx_th_fmt_message_6.doStartTag();
    if (_jspx_th_fmt_message_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_6);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_6);
    return false;
  }

  private boolean _jspx_meth_fmt_message_7(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_7 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_7.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_7.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_4);
    _jspx_th_fmt_message_7.setKey("errors.companySystemDefault.mismatch");
    int _jspx_eval_fmt_message_7 = _jspx_th_fmt_message_7.doStartTag();
    if (_jspx_th_fmt_message_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_7);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_7);
    return false;
  }

  private boolean _jspx_meth_c_if_5(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_5 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_5.setPageContext(_jspx_page_context);
    _jspx_th_c_if_5.setParent(null);
    _jspx_th_c_if_5.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${appConfig['rememberMeEnabled']}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_5 = _jspx_th_c_if_5.doStartTag();
    if (_jspx_eval_c_if_5 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("    <tr>\r\n");
        out.write("      <td class=\"content\">&nbsp;</td>\r\n");
        out.write("      <td width=\"22\" class=\"content\"><input type=\"checkbox\" class=\"checkbox\" name=\"rememberMe\" id=\"rememberMe\" tabindex=\"3\"/></td>\r\n");
        out.write("      <td width=\"170\" class=\"content\" style=\"padding-top:0px; !padding-top:9px;\"><label for=\"rememberMe\">");
        if (_jspx_meth_fmt_message_8((javax.servlet.jsp.tagext.JspTag) _jspx_th_c_if_5, _jspx_page_context))
          return true;
        out.write("</label></td>\r\n");
        out.write("    \r\n");
        out.write("    </tr>\r\n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_c_if_5.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_5);
    return false;
  }

  private boolean _jspx_meth_fmt_message_8(javax.servlet.jsp.tagext.JspTag _jspx_th_c_if_5, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_8 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_8.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_8.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_c_if_5);
    _jspx_th_fmt_message_8.setKey("login.rememberMe");
    int _jspx_eval_fmt_message_8 = _jspx_th_fmt_message_8.doStartTag();
    if (_jspx_th_fmt_message_8.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_8);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_8);
    return false;
  }

  private boolean _jspx_meth_fmt_message_9(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  fmt:message
    org.apache.taglibs.standard.tag.rt.fmt.MessageTag _jspx_th_fmt_message_9 = (org.apache.taglibs.standard.tag.rt.fmt.MessageTag) _jspx_tagPool_fmt_message_key_nobody.get(org.apache.taglibs.standard.tag.rt.fmt.MessageTag.class);
    _jspx_th_fmt_message_9.setPageContext(_jspx_page_context);
    _jspx_th_fmt_message_9.setParent(null);
    _jspx_th_fmt_message_9.setKey("button.login");
    int _jspx_eval_fmt_message_9 = _jspx_th_fmt_message_9.doStartTag();
    if (_jspx_th_fmt_message_9.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_9);
      return true;
    }
    _jspx_tagPool_fmt_message_key_nobody.reuse(_jspx_th_fmt_message_9);
    return false;
  }

  private boolean _jspx_meth_c_if_6(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_6 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_6.setPageContext(_jspx_page_context);
    _jspx_th_c_if_6.setParent(null);
    _jspx_th_c_if_6.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${not empty corpID}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_6 = _jspx_th_c_if_6.doStartTag();
    if (_jspx_eval_c_if_6 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("    <td colspan=\"3\">\r\n");
        out.write("    \t<input type=\"button\" class=\"cssbutton1\" style=\"width: 150px\" value=\"Request Partner Access\" onclick=\"openPartnerRequest();\"/>\r\n");
        out.write("    </td>\r\n");
        out.write("    ");
        int evalDoAfterBody = _jspx_th_c_if_6.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_6.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_6);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_6);
    return false;
  }

  private boolean _jspx_meth_c_if_7(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:if
    org.apache.taglibs.standard.tag.rt.core.IfTag _jspx_th_c_if_7 = (org.apache.taglibs.standard.tag.rt.core.IfTag) _jspx_tagPool_c_if_test.get(org.apache.taglibs.standard.tag.rt.core.IfTag.class);
    _jspx_th_c_if_7.setPageContext(_jspx_page_context);
    _jspx_th_c_if_7.setParent(null);
    _jspx_th_c_if_7.setTest(((java.lang.Boolean) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${param.error != null}", java.lang.Boolean.class, (PageContext)_jspx_page_context, null)).booleanValue());
    int _jspx_eval_c_if_7 = _jspx_th_c_if_7.doStartTag();
    if (_jspx_eval_c_if_7 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      do {
        out.write("\r\n");
        out.write("<script language=\"javascript\" type=\"text/javascript\">\r\n");
        out.write("\r\n");
        out.write("</script>\r\n");
        int evalDoAfterBody = _jspx_th_c_if_7.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
    }
    if (_jspx_th_c_if_7.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_7);
      return true;
    }
    _jspx_tagPool_c_if_test.reuse(_jspx_th_c_if_7);
    return false;
  }

  private boolean _jspx_meth_s_form_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:form
    org.apache.struts2.views.jsp.ui.FormTag _jspx_th_s_form_0 = (org.apache.struts2.views.jsp.ui.FormTag) _jspx_tagPool_s_form_validate_name_method_action.get(org.apache.struts2.views.jsp.ui.FormTag.class);
    _jspx_th_s_form_0.setPageContext(_jspx_page_context);
    _jspx_th_s_form_0.setParent(null);
    _jspx_th_s_form_0.setName("userOTPForm");
    _jspx_th_s_form_0.setAction("findForgotPasswordOTP.html?decorator=otpResetPass&otpResetPass=true");
    _jspx_th_s_form_0.setMethod("post");
    _jspx_th_s_form_0.setValidate("true");
    int _jspx_eval_s_form_0 = _jspx_th_s_form_0.doStartTag();
    if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_form_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_form_0.doInitBody();
      }
      do {
        out.write("\r\n");
        out.write("<input type=\"hidden\" class=\"textbox\" id=\"OTPusername\" name=\"OTPusername\" tabindex=\"1\"  value=\"\"/>\r\n");
        out.write("<input type=\"hidden\" class=\"textbox\" id=\"bypass_sessionfilter\" name=\"bypass_sessionfilter\"   value=\"YES\"/>\r\n");
        int evalDoAfterBody = _jspx_th_s_form_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_form_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_form_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_form_validate_name_method_action.reuse(_jspx_th_s_form_0);
      return true;
    }
    _jspx_tagPool_s_form_validate_name_method_action.reuse(_jspx_th_s_form_0);
    return false;
  }

  private boolean _jspx_meth_c_url_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_3 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_3.setPageContext(_jspx_page_context);
    _jspx_th_c_url_3.setParent(null);
    _jspx_th_c_url_3.setValue("/");
    int _jspx_eval_c_url_3 = _jspx_th_c_url_3.doStartTag();
    if (_jspx_th_c_url_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
    return false;
  }

  private boolean _jspx_meth_c_url_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_4 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_4.setPageContext(_jspx_page_context);
    _jspx_th_c_url_4.setParent(null);
    _jspx_th_c_url_4.setValue("/");
    int _jspx_eval_c_url_4 = _jspx_th_c_url_4.doStartTag();
    if (_jspx_th_c_url_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
    return false;
  }

  private boolean _jspx_meth_s_text_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_0 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_0.setPageContext(_jspx_page_context);
    _jspx_th_s_text_0.setParent(null);
    _jspx_th_s_text_0.setName("errors.requiredField");
    int _jspx_eval_s_text_0 = _jspx_th_s_text_0.doStartTag();
    if (_jspx_eval_s_text_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_text_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_text_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_text_0.doInitBody();
      }
      do {
        if (_jspx_meth_s_param_0((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_text_0, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_text_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_text_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_text_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_0);
      return true;
    }
    _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_0);
    return false;
  }

  private boolean _jspx_meth_s_param_0(javax.servlet.jsp.tagext.JspTag _jspx_th_s_text_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:param
    org.apache.struts2.views.jsp.ParamTag _jspx_th_s_param_0 = (org.apache.struts2.views.jsp.ParamTag) _jspx_tagPool_s_param.get(org.apache.struts2.views.jsp.ParamTag.class);
    _jspx_th_s_param_0.setPageContext(_jspx_page_context);
    _jspx_th_s_param_0.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_text_0);
    int _jspx_eval_s_param_0 = _jspx_th_s_param_0.doStartTag();
    if (_jspx_eval_s_param_0 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_param_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_param_0.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_param_0.doInitBody();
      }
      do {
        if (_jspx_meth_s_text_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_param_0, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_param_0.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_param_0 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_param_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_param.reuse(_jspx_th_s_param_0);
      return true;
    }
    _jspx_tagPool_s_param.reuse(_jspx_th_s_param_0);
    return false;
  }

  private boolean _jspx_meth_s_text_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_param_0, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_1 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name_nobody.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_1.setPageContext(_jspx_page_context);
    _jspx_th_s_text_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_param_0);
    _jspx_th_s_text_1.setName("label.username");
    int _jspx_eval_s_text_1 = _jspx_th_s_text_1.doStartTag();
    if (_jspx_th_s_text_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_1);
      return true;
    }
    _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_1);
    return false;
  }

  private boolean _jspx_meth_c_url_5(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_5 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_5.setPageContext(_jspx_page_context);
    _jspx_th_c_url_5.setParent(null);
    _jspx_th_c_url_5.setValue("/passwordHint.html");
    int _jspx_eval_c_url_5 = _jspx_th_c_url_5.doStartTag();
    if (_jspx_th_c_url_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_5);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_5);
    return false;
  }

  private boolean _jspx_meth_s_text_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_2 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_2.setPageContext(_jspx_page_context);
    _jspx_th_s_text_2.setParent(null);
    _jspx_th_s_text_2.setName("errors.requiredField");
    int _jspx_eval_s_text_2 = _jspx_th_s_text_2.doStartTag();
    if (_jspx_eval_s_text_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_text_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_text_2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_text_2.doInitBody();
      }
      do {
        if (_jspx_meth_s_param_1((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_text_2, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_text_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_text_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_text_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_2);
      return true;
    }
    _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_2);
    return false;
  }

  private boolean _jspx_meth_s_param_1(javax.servlet.jsp.tagext.JspTag _jspx_th_s_text_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:param
    org.apache.struts2.views.jsp.ParamTag _jspx_th_s_param_1 = (org.apache.struts2.views.jsp.ParamTag) _jspx_tagPool_s_param.get(org.apache.struts2.views.jsp.ParamTag.class);
    _jspx_th_s_param_1.setPageContext(_jspx_page_context);
    _jspx_th_s_param_1.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_text_2);
    int _jspx_eval_s_param_1 = _jspx_th_s_param_1.doStartTag();
    if (_jspx_eval_s_param_1 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_param_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_param_1.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_param_1.doInitBody();
      }
      do {
        if (_jspx_meth_s_text_3((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_param_1, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_param_1.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_param_1 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_param_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_param.reuse(_jspx_th_s_param_1);
      return true;
    }
    _jspx_tagPool_s_param.reuse(_jspx_th_s_param_1);
    return false;
  }

  private boolean _jspx_meth_s_text_3(javax.servlet.jsp.tagext.JspTag _jspx_th_s_param_1, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_3 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name_nobody.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_3.setPageContext(_jspx_page_context);
    _jspx_th_s_text_3.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_param_1);
    _jspx_th_s_text_3.setName("label.username");
    int _jspx_eval_s_text_3 = _jspx_th_s_text_3.doStartTag();
    if (_jspx_th_s_text_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_3);
      return true;
    }
    _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_3);
    return false;
  }

  private boolean _jspx_meth_s_text_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_4 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_4.setPageContext(_jspx_page_context);
    _jspx_th_s_text_4.setParent(null);
    _jspx_th_s_text_4.setName("errors.requiredField");
    int _jspx_eval_s_text_4 = _jspx_th_s_text_4.doStartTag();
    if (_jspx_eval_s_text_4 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_text_4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_text_4.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_text_4.doInitBody();
      }
      do {
        if (_jspx_meth_s_param_2((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_text_4, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_text_4.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_text_4 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_text_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_4);
      return true;
    }
    _jspx_tagPool_s_text_name.reuse(_jspx_th_s_text_4);
    return false;
  }

  private boolean _jspx_meth_s_param_2(javax.servlet.jsp.tagext.JspTag _jspx_th_s_text_4, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:param
    org.apache.struts2.views.jsp.ParamTag _jspx_th_s_param_2 = (org.apache.struts2.views.jsp.ParamTag) _jspx_tagPool_s_param.get(org.apache.struts2.views.jsp.ParamTag.class);
    _jspx_th_s_param_2.setPageContext(_jspx_page_context);
    _jspx_th_s_param_2.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_text_4);
    int _jspx_eval_s_param_2 = _jspx_th_s_param_2.doStartTag();
    if (_jspx_eval_s_param_2 != javax.servlet.jsp.tagext.Tag.SKIP_BODY) {
      if (_jspx_eval_s_param_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE) {
        out = _jspx_page_context.pushBody();
        _jspx_th_s_param_2.setBodyContent((javax.servlet.jsp.tagext.BodyContent) out);
        _jspx_th_s_param_2.doInitBody();
      }
      do {
        if (_jspx_meth_s_text_5((javax.servlet.jsp.tagext.JspTag) _jspx_th_s_param_2, _jspx_page_context))
          return true;
        int evalDoAfterBody = _jspx_th_s_param_2.doAfterBody();
        if (evalDoAfterBody != javax.servlet.jsp.tagext.BodyTag.EVAL_BODY_AGAIN)
          break;
      } while (true);
      if (_jspx_eval_s_param_2 != javax.servlet.jsp.tagext.Tag.EVAL_BODY_INCLUDE)
        out = _jspx_page_context.popBody();
    }
    if (_jspx_th_s_param_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_param.reuse(_jspx_th_s_param_2);
      return true;
    }
    _jspx_tagPool_s_param.reuse(_jspx_th_s_param_2);
    return false;
  }

  private boolean _jspx_meth_s_text_5(javax.servlet.jsp.tagext.JspTag _jspx_th_s_param_2, PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  s:text
    org.apache.struts2.views.jsp.TextTag _jspx_th_s_text_5 = (org.apache.struts2.views.jsp.TextTag) _jspx_tagPool_s_text_name_nobody.get(org.apache.struts2.views.jsp.TextTag.class);
    _jspx_th_s_text_5.setPageContext(_jspx_page_context);
    _jspx_th_s_text_5.setParent((javax.servlet.jsp.tagext.Tag) _jspx_th_s_param_2);
    _jspx_th_s_text_5.setName("label.password");
    int _jspx_eval_s_text_5 = _jspx_th_s_text_5.doStartTag();
    if (_jspx_th_s_text_5.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_5);
      return true;
    }
    _jspx_tagPool_s_text_name_nobody.reuse(_jspx_th_s_text_5);
    return false;
  }
}
