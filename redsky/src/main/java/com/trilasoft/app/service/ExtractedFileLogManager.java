package com.trilasoft.app.service;

import java.util.Date;
import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.ExtractedFileLog;

public interface ExtractedFileLogManager extends GenericManager<ExtractedFileLog, Long> {

	List<ExtractedFileLog> searchExtractedFileLog(String sessionCorpID, String moduleName, String extractCreatedbyFirstName, String extractCreatedbyLastName, String fromExtractDate, String toExtractDate);
	public List getFileLogListByModule(String sessionCorpID,String moduleName);
}
