<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>   
    <title><fmt:message key="containerList.title"/></title>   
    <meta name="heading" content="<fmt:message key='containerList.heading'/>"/> 
    <style type="text/css">
/* collapse */
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}
</style>    
</head>   

<s:form id="serviceForm1" > 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
                       
<s:set name="auditContainers" value="auditContainers" scope="request"/>
<div id="Layer1" style="width:780px">
	<div id="otabs">
		<ul>
			<li><a class="current"><span>Audit List</span></a></li>
		</ul>
	</div>
	<div class="spnblk"></div>
<display:table name="auditContainers" class="table" requestURI="" id="auditContainerList" export="false" defaultsort="1" pagesize="100" style="width:721px">   
 <display:column property="idNumber" sortable="true" titleKey="container.idNumber"/>
 <display:column property="containerNumber" sortable="true" titleKey="container.containerNumber"/>
 <display:column property="sealNumber" sortable="true" titleKey="container.sealNumber"/>
 <display:column property="size" sortable="true" titleKey="container.size"/>
 <display:column  sortable="true" title="Gross Weight" style="width:80px"><div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${auditContainerList.grossWeight}" /></div>
 </display:column>
 <display:column sortable="true" titleKey="container.emptyContWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${auditContainerList.emptyContWeight}" /></div>
                  </display:column>
 <display:column sortable="true" titleKey="container.netWeight" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${auditContainerList.netWeight}" /></div>
                  </display:column>
 <display:column  sortable="true" titleKey="container.volume" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${auditContainerList.volume}" /></div>
                  </display:column>
 <display:column  sortable="true" titleKey="container.pieces" style="width:70px">
 <div align="right"><fmt:formatNumber type="number" maxFractionDigits="0"
                  groupingUsed="true" value="${auditContainerList.pieces}" /></div>
                  </display:column>
 <display:column property="density" sortable="true" titleKey="container.density"/>
 <display:column property="updatedOn" sortable="true" titleKey="container.updatedOns" format="{0,date,dd-MMM-yyyy}"/>
 <display:column title="Deleted&nbsp;By" style="width:55px;" > <c:out value="${auditContainerList.updatedBy}"></c:out> </display:column>
 
    <display:setProperty name="paging.banner.item_name" value="container"/>  
    <display:setProperty name="paging.banner.items_name" value="container"/>
  
   
</display:table>   

	<s:hidden name="id"></s:hidden>	 
     
</s:form>  


<script language="javascript" type="text/javascript">
	function myDate() {
		var mydate=new Date()
		var year=mydate.getYear()
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if (month<10)
		month="0"+month
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym
		var datam = month+"/"+daym+"/"+year;
	}
</script>