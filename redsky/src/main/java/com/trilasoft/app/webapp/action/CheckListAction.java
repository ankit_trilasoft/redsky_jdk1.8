package com.trilasoft.app.webapp.action;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.CheckList;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.service.CheckListManager;
import com.trilasoft.app.service.RefMasterManager;


public class CheckListAction extends BaseAction implements Preparable {
	private Long id;
	private String sessionCorpID;
	private CheckList checkList;
	private CheckListManager checkListManager;
	private List checksList;
	private RefMasterManager refMasterManager;
	private Map<String, String> job;
	private Map<String, String> docsList;
	private List controlDateList;
	private Map<String, String> ownerList;
	private Map<String, String> pentity;
	private Integer countNumberOfRules;
	private Map<String, String> routing;
	private List<RefMaster> mode;
	private Map<String, String> service;
	private Map<String, String> ocountry;
	private Map<String, String> dcountry;
	private String btn;
	private Integer countNumberOfResult;
	private String hitFlag;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(CheckListAction.class);

	 

	
	public CheckListAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 pentity = refMasterManager.findByParameter(corpId, "PENTITY");
		 pentity.remove("Vehicle");
		 pentity.remove("Claims");
		 pentity.remove("ServicePartner");
		 pentity.remove("AccountLine");
		 pentity.remove("Miscellaneous");
		 pentity.remove("WorkTicket");
		 pentity.remove("AccountLineList");
		 pentity.put("TrackingStatus", "TrackingStatus");
		 job = refMasterManager.findByParameter(corpId, "JOB");
		 docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
		 controlDateList=checkListManager.getControlDateList(sessionCorpID);
		 ownerList = refMasterManager.findByParameter(corpId, "PRULEROLE");
		 routing = refMasterManager.findByParameter(corpId, "ROUTING");
		 mode = refMasterManager.findByParameters(corpId, "MODE");
	 	 service = refMasterManager.findByParameter(corpId, "SERVICE");
	 	 ocountry = refMasterManager.findCountry(corpId, "COUNTRY");
	 	 dcountry = refMasterManager.findCountry(corpId, "COUNTRY");
	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	 }
	
	
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		checksList=checkListManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		
		if (id!= null)
		{
			checkList=checkListManager.get(id);
		}
		else
		{
			checkList=new CheckList();
			checkList.setCreatedBy(getRequest().getRemoteUser());
			checkList.setCreatedOn(new Date());
			checkList.setUpdatedBy(getRequest().getRemoteUser());
			checkList.setUpdatedOn(new Date());
		}
		checkList.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public synchronized String save() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		boolean isNew = (checkList.getId() == null);
			if(isNew)
			{
				checkList.setCreatedBy(getRequest().getRemoteUser());
				checkList.setCreatedOn(new Date());
			}
			checkList.setUpdatedBy(getRequest().getRemoteUser());
			checkList.setUpdatedOn(new Date());
			checkList.setCorpID(sessionCorpID);
			if(checkList.getTableRequired().equalsIgnoreCase("ServiceOrder")){
				
				checkList.setUrl("editServiceOrderUpdate");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("TrackingStatus")){
				
				checkList.setUrl("editTrackingStatus");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("CustomerFile")){
				
				checkList.setUrl("editCustomerFile");
			}
			
			if(checkList.getTableRequired().equalsIgnoreCase("ServicePartner")){
				
				checkList.setUrl("editServicePartner");
			}
			
			if(checkList.getTableRequired().equalsIgnoreCase("Vehicle")){
				
				checkList.setUrl("editVehicle");
			}
			
			if(checkList.getTableRequired().equalsIgnoreCase("AccountLine")){
				
				checkList.setUrl("editAccountLine");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("WorkTicket")){
				
				checkList.setUrl("editWorkTicketUpdate");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("AccountLineList")){
				
				checkList.setUrl("accountLineList");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("Billing")){
				
				checkList.setUrl("editBilling");
			}
			if(checkList.getTableRequired().equalsIgnoreCase("Miscellaneous")){
				
				checkList.setUrl("editMiscellaneous");
			}
            if(checkList.getTableRequired().equalsIgnoreCase("QuotationFile")){
				
				checkList.setUrl("QuotationFileForm");
			}
            if(checkList.getTableRequired().equalsIgnoreCase("Claim")){
				
				checkList.setUrl("editClaim");
			}
             if(checkList.getTableRequired().equalsIgnoreCase("DspDetails")){
            	 
				checkList.setUrl("editDspDetails");
			}
			
			checkList=checkListManager.save(checkList);
			
			String executeRules=checkListManager.executeTestRule(checkList, sessionCorpID);
		
			if(btn.equalsIgnoreCase("Test")){
				if(executeRules==null){
					checkList.setStatus(true);
					checkList=checkListManager.save(checkList);
					saveMessage("Rule has been executed successfully.");
					
				}else{
					String sqlError = "";
					try
					{
						sqlError = executeRules;
					}catch(Exception ex){
						StringBuffer buffer = new StringBuffer();
						buffer.append(executeRules);
						buffer.append(" ");
						sqlError = buffer.toString();
						 ex.getStackTrace();
					}
					checkList.setStatus(false);
					checkList=checkListManager.save(checkList);
					errorMessage("Rule cannot be executed. "+ sqlError);
				}
			}
			
			else if(btn.equalsIgnoreCase("Execute")){
				if(checkList.getEnable()){
					List executeThisRule= checkListManager.executeThisRule(checkList, sessionCorpID);
					List numberOfResult=checkListManager.findNumberOfResultEntered(sessionCorpID, checkList.getId());
					countNumberOfResult=numberOfResult.size();
					saveMessage("Rule has been executed successfully.  "   + countNumberOfResult + " Record Entered");
				}else{
					saveMessage("Execution failed: Rule is not enabled");
				}
			
		}
		else{
			if(executeRules==null){
				checkList.setStatus(true);
				checkList=checkListManager.save(checkList);
				String key = (isNew) ? "CheckList has been added successfully" : "CheckList has been updated successfully";
				saveMessage(getText(key));
				}
			else{
				checkList.setCreatedOn(new Date());
				checkList.setStatus(false);
				checkList=checkListManager.save(checkList);
				errorMessage("Entered rule is not executable. Record saved!");
			}
		
	}	
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS;
 	}
	
	public String delete(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			checkListManager.remove(id);
			
			}catch(Exception ex)
			{
				list();
				ex.getStackTrace();
			}
			hitFlag="1";
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public String search()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");	
			return SUCCESS;
	}
	
	public String executeCheckList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List executeRules=checkListManager.execute(sessionCorpID); 
		countNumberOfRules=executeRules.size();
		countNumberOfResult=checkListManager.findNumberOfResult(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public CheckList getCheckList() {
		return checkList;
	}

	public void setCheckList(CheckList checkList) {
		this.checkList = checkList;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public Long getId() {
		return id;
	}

	public void setCheckListManager(CheckListManager checkListManager) {
		this.checkListManager = checkListManager;
	}

	public List getChecksList() {
		return checksList;
	}

	public void setChecksList(List checksList) {
		this.checksList = checksList;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public void setJob(Map<String, String> job) {
		this.job = job;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getDocsList() {
		return docsList;
	}

	public void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	public List getControlDateList() {
		return controlDateList;
	}

	public void setControlDateList(List controlDateList) {
		this.controlDateList = controlDateList;
	}

	public Map<String, String> getOwnerList() {
		return ownerList;
	}

	public void setOwnerList(Map<String, String> ownerList) {
		this.ownerList = ownerList;
	}
	public Map<String, String> getPentity() {
		return pentity;
	}
	public void setPentity(Map<String, String> pentity) {
		this.pentity = pentity;
	}
	public Map<String, String> getDcountry() {
		return dcountry;
	}
	public void setDcountry(Map<String, String> dcountry) {
		this.dcountry = dcountry;
	}
	public List<RefMaster> getMode() {
		return mode;
	}
	public void setMode(List<RefMaster> mode) {
		this.mode = mode;
	}
	public Map<String, String> getOcountry() {
		return ocountry;
	}
	public void setOcountry(Map<String, String> ocountry) {
		this.ocountry = ocountry;
	}
	public Map<String, String> getRouting() {
		return routing;
	}
	public void setRouting(Map<String, String> routing) {
		this.routing = routing;
	}
	public Map<String, String> getService() {
		return service;
	}
	public void setService(Map<String, String> service) {
		this.service = service;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getBtn() {
		return btn;
	}
	public void setBtn(String btn) {
		this.btn = btn;
	}
	public Integer getCountNumberOfResult() {
		return countNumberOfResult;
	}
	public void setCountNumberOfResult(Integer countNumberOfResult) {
		this.countNumberOfResult = countNumberOfResult;
	}
	public Integer getCountNumberOfRules() {
		return countNumberOfRules;
	}
	public void setCountNumberOfRules(Integer countNumberOfRules) {
		this.countNumberOfRules = countNumberOfRules;
	}
	public String getHitFlag() {
		return hitFlag;
	}
	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	
	
	

}
