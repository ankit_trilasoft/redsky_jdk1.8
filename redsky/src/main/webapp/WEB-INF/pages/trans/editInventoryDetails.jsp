<%@ include file="/common/taglibs.jsp"%>
<head>
<title>Inventory Detail</title>
<meta name="heading"
	content="Survey Detail" />
<style type=text/css>

.input-textRight {
    border: 1px solid #219DD1;
    color: #000000;
    font-family: arial,verdana;
    font-size: 12px;
    text-align:right;
    height: 15px;
    text-decoration: none;
}
div.autocomplete {
      position:absolute;
      width:250px;
      background-color:white;
      border:1px solid #888;
      margin:0px;
      padding:0px;
      z-index:99999;
    }
    div.autocomplete ul {
      list-style-type:none;
      margin:0px;
      padding:0px;
    }
    div.autocomplete ul li.selected { background-color: #ffb;}
    div.autocomplete ul li {
      list-style-type:none;
      display:block;
      margin:0;
      padding:0px;
      padding-left:3px;   
      cursor:pointer;
    }
</style>	
<script type="text/javascript">

function onlyNumeric(targetElement)
{   
var i;
    var s = targetElement.value;
    for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) {
        alert("Enter valid Number");
        targetElement.value="";
        return false;
        }
    }
    return true;
}

function limitText(limitField, limitCount, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum).trim();
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
}
function wordCount(){
	var commentLenght = document.forms['cubeSheetForm'].elements['inventoryPacking.comment'].value.length;
	if(commentLenght>=250){
		alert("Please enter comment less than 250 charecters.");
		return false;
	}
}
<c:if test="${hitflag=='1'}">
 <c:redirect url="/editInventoryDetails.html?sid=${sid}&id=${inventoryPacking.id}"/>
</c:if>

function linkVisible(){
	var chkedData = document.forms['cubeSheetForm'].elements['inventoryPacking.conditon'].value;
	var yr = document.forms['cubeSheetForm'].elements['inventoryPacking.year'].value;
	var linkDiv = document.getElementById("linkto");
	var commonDiv = document.getElementById("common");
	var  all = document.getElementById("default");

		if(chkedData == 'Electronic' || chkedData == 'Vehicle' ){
			linkDiv.style.display = 'block';
			commonDiv.style.display = 'none';
			all.style.display='none';
			document.forms['cubeSheetForm'].elements['eleYear'].value = yr;
		}
		else{
			if(chkedData == 'Pictures'){
				linkDiv.style.display = 'none';
				commonDiv.style.display = 'block';
				all.style.display='none';
				document.forms['cubeSheetForm'].elements['picYear'].value = yr;
			}else{
				linkDiv.style.display = 'none';
				commonDiv.style.display = 'none';
				all.style.display='block';
			}
		}

	}

window.onload=function(){
	new Ajax.Autocompleter("autocomplete", "autocomplete_choices", "/redsky/articleAutocomplete.html?decorator=simple&popup=true", {afterUpdateElement : getSelectionId});
	}
function getSelectionId(text, li) {
	    alert (li.id);
	}
function checkValue(result){
	 document.getElementById('autocomplete').value=result.innerHTML;
}
function linkToImages(){
		   window.open('packingImageListUpload.html?cid=${cid}&id=${id}&sid=${sid}','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');
		}
function setVal(target){
	document.forms['cubeSheetForm'].elements['inventoryPacking.year'].value = target.value;
}
</script>
<meta name="heading"
	content="Inventory Details" />
</head>
<div id="otabs" style="margin-bottom:25px;">
		  <ul>
		    <li ><a href="inventoryDetails.html?sid=${sid}"><span>Inventory Data List</span></a></li>
		    <li ><a class="current"><span>Inventory Data</span></a></li>
		  </ul>
		</div>
			<div class="spn">&nbsp;</div>
<s:form id="cubeSheetForm" name="saveInventoryData" action="saveInventoryData.html" onsubmit="return wordCount();" method="post" >		
<div id="content" align="center" style="width:80%">
<div id="liquid-round-top">
    <div class="top" ><span></span></div>   
    <s:hidden name="countdown"  value="245"/>
    <s:hidden name="sid"  value="${sid}"/>
    <s:hidden name="inventoryPacking.location" />
    <s:hidden name="inventoryPacking.length"/>
    <s:hidden name="inventoryPacking.itemType" />
    <s:hidden name="inventoryPacking.mode" />
    <s:hidden name="inventoryPacking.quantity" />
    <s:hidden name="inventoryPacking.volume" />
    <s:hidden name="inventoryPacking.weight" />
    <s:hidden name="inventoryPacking.width" />
    <s:hidden name="inventoryPacking.height" />
    <s:hidden name="inventoryPacking.pieceID" />
    <s:hidden name="inventoryPacking.year" />
    <s:hidden name="inventoryPacking.boxName" />
    <s:hidden name="inventoryPacking.assembling" />
    <s:hidden name="inventoryPacking.specialContainer" />
    <s:hidden name="inventoryPacking.imagesAvailablityFlag" />
    <s:hidden name="inventoryPacking.serviceOrderID"  value="${sid}"/>
    <s:hidden name="inventoryPacking.id"  value="${inventoryPacking.id}"/>
    <div class="center-content" style="padding-left:15px;">
    <table class="detailTabLabel" cellspacing="2" cellpadding="0" border="0">
					<tbody>
					<tr>						
						<td align="right" class="listwhitebox" width="70px"><b>Item</b></td>
								<td  width="150px" ><s:textfield cssClass="input-text" id="autocomplete"  name="inventoryPacking.item"  size="25" maxlength="25"/>
				  			<span id="indicator1" style="display: none"><img src="/images/spinner.gif" alt="Working..." /></span>
			                <div id="autocomplete_choices" class="autocomplete" ></div> 
				  			</td>	                        	   	   
						
						<td align="right" class="listwhitebox" width="70px"><b>Condition</b></td>
						<td>
							<s:select cssClass="list-menu" cssStyle="width:100px; !width:50px;"  name="inventoryPacking.conditon" list="%{conditionType}"  onchange="linkVisible()"  />
						</td>						
					</tr>
					<tr><td colspan="4"></td></tr>
					<tr><td colspan="4"></td></tr>
					<tr><td colspan="4"></td></tr>
					<tr>						
						<td align="right" class="listwhitebox">Valuation</td>
						<td><s:textfield name="inventoryPacking.value" size="25" cssClass="input-textRight" onchange="onlyNumeric(this);" maxlength="5"/></td>
						<td align="right" class="listwhitebox">Quantity</td>
						<td><s:textfield name="inventoryPacking.itemQuantity" required="true" size="14"  cssClass="input-textRight" onchange="onlyNumeric(this);" maxlength="5" /></td>						
					</tr>
					<tr><td colspan="4"></td></tr>
					<tr><td colspan="4"></td></tr>
					<tr><td colspan="4"></td></tr>
					<tr>						
						<td align="right" class="listwhitebox">Comment</td>
						<td colspan="5"><s:textarea name="inventoryPacking.comment" rows="2" cols="57"  cssClass="textarea" onkeydown="limitText(this,this.form.countdown,245);" 
                     onkeyup="limitText(this,this.form.countdown,245);" onchange="limitText(this,this.form.countdown,245);"/></td>
                       <c:if test="${inventoryPacking.id!=null}">
	  		    <td  class="listwhitetext" align="right" width="55px">Images</td>	  		  
	  		    <td  width="2px"><img id="userImage" src="${pageContext.request.contextPath}/images/cameraupload.png" alt="" width="24" height="24" onclick="linkToImages()"/></td>
	  		    </c:if>
				</tr>
				<tr><td colspan="4"></td></tr>
				<tr><td colspan="4"></td></tr>
				<tr><td colspan="4"></td></tr>
				<tr>
				<td  class="listwhitetext" align="right" >Part</td>					
	  			 		    
	 		    <td colspan="6" align="left">
	 		    <table class="detailTabLabel">
	 		    <tr>
	 		    <td><s:checkbox name="inventoryPacking.part" value="${inventoryPacking.part}" fieldValue="true"/></td>	
	 		     <td  width="128px" class="listwhitetext" align="right" >Dismantling</td>	  	   		
	  			<td ><s:checkbox name="inventoryPacking.dismantling" value="${inventoryPacking.dismantling}" fieldValue="true"/></td>
	  			<td  width="128px" class="listwhitetext" align="right" >isValuable</td>	  			  					
	  			<td ><s:checkbox name="inventoryPacking.valuable" value="${inventoryPacking.valuable}" fieldValue="true"/></td>
	 		    </tr>	 		    
	 		    </table>
	 		    </td>  		    	  	   	    
	  	   	   
	  			</tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr>
	  			<td colspan="4">
	  			<table id="linkto" class="detailTabLabel">
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Make</td>
	  					<td> <s:textfield name="inventoryPacking.make" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Model</td>
	  					<td> <s:textfield name="inventoryPacking.model" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Year</td>
	  					<td> <s:textfield name="eleYear" cssClass="input-text" size="50"  onchange="setVal(this)" maxlength="30"/></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" width="67px" >Serial Number</td>
	  					<td> <s:textfield name="inventoryPacking.serialnumber" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				</table>
	  			</td>
	  			</tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr><td colspan="4"></td></tr>
	  			<tr>
	  			<td colspan="4">
	  				<table id="common" class="detailTabLabel">
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Title</td>
	  					<td> <s:textfield name="inventoryPacking.title" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Author</td>
	  					<td> <s:textfield name="inventoryPacking.author" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" >Year</td>
	  					<td> <s:textfield name="picYear" cssClass="input-text" size="50" onchange="setVal(this)" maxlength="30" /></td>
	  				</tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr><td colspan="2"></td></tr>
	  				<tr>
	  					<td  class="listwhitetext" align="right" width="67px">Materials</td>
	  					<td> <s:textfield name="inventoryPacking.materials" cssClass="input-text" size="50" maxlength="30" /></td>
	  				</tr>
	  				</table>
	  				</td>
	  				</tr>
	  				<tr>
	  				<td colspan="4" class="detailTabLabel">
	  				<table id="default" ></table>
	  				</td>
	  				</tr>
	  			</table>
	  			</td>
	  			</tr>
				</tbody>
			</table>
    </div>
<div class="bottom-header" style="margin-top:31px;!margin-top:49px;"><span></span></div>
</div>
</div> 
<table width="750px">
				<tbody>
					<tr><td align="left" rowspan="1"></td></tr>
					<tr><fmt:formatDate var="cartonCreatedOnFormattedValue" value="${inventoryPacking.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='carton.createdOn'/></td>
						<s:hidden name="inventoryPacking.createdOn" value="${cartonCreatedOnFormattedValue}" />
						<td style="width:120px"><fmt:formatDate value="${inventoryPacking.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.createdBy' /></td>
						
						
						<c:if test="${not empty inventoryPacking.id}">
								<s:hidden name="inventoryPacking.createdBy"/>
								<td style="width:120px"><s:label name="createdBy" value="%{inventoryPacking.createdBy}"/></td>
							</c:if>
							<c:if test="${empty inventoryPacking.id}">
								<s:hidden name="inventoryPacking.createdBy" value="${pageContext.request.remoteUser}"/>
								<td ><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedOn'/></td>
						<fmt:formatDate var="cartonUpdatedOnFormattedValue" value="${inventoryPacking.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="inventoryPacking.updatedOn" value="${cartonUpdatedOnFormattedValue}"/>
						<td style="width:120px"><fmt:formatDate value="${inventoryPacking.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='carton.updatedBy' /></td>
						<c:if test="${not empty inventoryPacking.id}">
							<s:hidden name="inventoryPacking.updatedBy"/>
							<td style="width:110px"><s:label name="updatedBy" value="%{inventoryPacking.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty inventoryPacking.id}">
							<s:hidden name="inventoryPacking.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
					</tr>
				</tbody>
			</table>
<s:submit  cssClass="cssbutton" cssStyle="width:55px; height:25px;margin-left:35px;" key="button.save" /> 
</s:form>
<script>
try {linkVisible();}
catch(e){}
</script>

