package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.commons.lang.StringEscapeUtils;
import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.NotesDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.TrackingStatusManager;

public class NotesDaoHibernate extends GenericDaoHibernate<Notes, Long> implements NotesDao {

	private List notess;
	private HibernateUtil hibernateUtil;
	private Object locationId;

	public NotesDaoHibernate() {
		super(Notes.class);
	}

	public TrackingStatusManager trackingStatusManager;

	public BillingManager billingManager;

	public MiscellaneousManager miscellaneousManager;

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public List findMaxId() {
		return this.getSession().createSQLQuery("select max(id) from notes").list();
	}

	public List getListByNotesId(String notesId, String subType) {
		return getHibernateTemplate().find("from Notes where noteStatus<>'CNCL' and  noteSubType like '" + subType.replaceAll("'", "''") + "%' AND notesId=? order by updatedOn desc", notesId);	}
	
	public List getALLListByNotesId(String notesId, String subType) {
		return getHibernateTemplate().find("from Notes where noteSubType like '" + subType.replaceAll("'", "''") + "%' AND notesId=? order by updatedOn desc", notesId);	}

	public List getListByCustomerNumber(String customerNumber, String subType) {
		return getHibernateTemplate().find("from Notes where customerNumber=? order by updatedOn desc", customerNumber);
	}

	public List<Notes> findByNotes(String noteStatus, String noteSubType, String notesId) {

		//System.out.println(notesId);
		if (noteStatus != "" && noteSubType != "") {
			notess = getHibernateTemplate().find(
					"from Notes where noteStatus<>'CNCL' AND noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' AND notesId='" + notesId + "' AND noteSubType like '"
					+ noteSubType.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus != "" && noteSubType.equals("")) {
			notess = getHibernateTemplate().find("from Notes where noteStatus<>'CNCL' AND  notesId='" + notesId + "' AND noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus.equals("") && noteSubType != "") {
			notess = getHibernateTemplate().find("from Notes where noteStatus<>'CNCL' AND notesId='" + notesId + "' AND noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus.equals("") && noteSubType.equals("")) {
			notess = getHibernateTemplate().find("from Notes where noteStatus<>'CNCL' AND notesId='" + notesId + "' order by updatedOn desc");
		}

		return notess;
	}

	public List<Notes> findByReleventNotes(String noteStatus, String noteSubType, String noteId) {
		if (noteSubType != "" && noteStatus != "" && noteId != "") {
			notess = getHibernateTemplate().find( "from Notes where noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' AND noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' AND notesId like '" + noteId + "%' order by updatedOn desc");
		}
		if (noteSubType != "" && noteStatus != "" && noteId.equals("")) {
			notess = getHibernateTemplate().find("from Notes where noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' AND noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteSubType != "" && noteStatus.equals("") && noteId != "") {
			notess = getHibernateTemplate().find("from Notes where noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' AND notesId like '" + noteId + "%' order by updatedOn desc");
		}
		if (noteSubType.equals("") && noteStatus != "" && noteId != "") {
			notess = getHibernateTemplate().find("from Notes where noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' AND notesId like '" + noteId + "%' order by updatedOn desc");
		}

		if (noteSubType.equals("") && noteStatus != "" && noteId.equals("")) {
			notess = getHibernateTemplate().find("from Notes where noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteSubType.equals("") && noteStatus.equals("") && noteId != "") {
			notess = getHibernateTemplate().find("from Notes where customerNumber like '" + noteId + "%' order by updatedOn desc");
		}
		if (noteSubType.equals("") && noteStatus.equals("") && noteId != "") {
			notess = getHibernateTemplate().find("from Notes where notesId like '" + noteId + "%' order by updatedOn desc");
		}
		if (noteSubType != "" && noteStatus.equals("") && noteId.equals("")) {
			notess = getHibernateTemplate().find("from Notes where noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		return notess;
	}

	public List countForNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Customer' AND notesId =?", seqNum);
	}

	public List countForOriginNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Origin' AND notesId =?", seqNum);
	}

	public List countForDestinationNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Destination' AND notesId =?", seqNum);
	}

	public List countForSurveyNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Survey' AND notesId =?", seqNum);
	}

	public List countForVipNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='VipReason' AND notesId =?", seqNum);
	}

	public List countForServiceNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='ServicePartners' AND notesId =?", spid);
	}

	public List countForCarrierNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Carrier' AND notesId =?", spid);
	}

	public List countForContainerNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Container' AND notesId =?", spid);
	}

	public List countForVehicleNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Vehicle' AND notesId =?", spid);
	}

	public List countForCartonNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Carton' AND notesId =?", spid);
	}

	public List countForVipDetailNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='VipPerson' AND notesId =?", shipNum);
	}

	public List countForOriginDetailNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Origin' AND notesId =?", shipNum);
	}

	public List countForDestinationDetailNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Destination' AND notesId =?", shipNum);
	}

	public List countForWeightDetailNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='WeightDetail' AND notesId =?", shipNum);
	}

	public List countForCategoryTypeNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='CategoryType' AND notesId =?", spid);
	}

	public List countForEntitleDetailNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='EntitleDetail' AND notesId =?", spid);
	}

	public List countForEstimateDetailNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='EstimateDetail' AND notesId =?", spid);
	}

	public List countForRevisionDetailNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='RevisionDetail' AND notesId =?", spid);
	}

	public List countForPayableDetailNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType in ('PayableDetail','Pending with note','rejected') AND notesId =?", spid);
	}

	public List countForReceivableDetailNotes(Long id) {
		String spid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='ReceivableDetail' AND notesId =?", spid);
	}

	public List countForBillingNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Billing' AND notesId =?", seqNum);
	}

	public List countForEntitlementNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Entitlement' AND notesId =?", seqNum);
	}

	public List countForContactNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='AccountContact' AND notesId =?", seqNum);
	}

	public List countForSpouseNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Spouse' AND notesId =?", seqNum);
	}

	public List countForCportalNotes(String seqNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='cportal' AND notesId =?", seqNum);
	}

	public List countForServiceOrderNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='ServiceOrder' AND notesId =?", shipNum);
	}

	public List countCustomerFeedbackNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Feedback' AND notesId =?", shipNum);
	}

	public List countOriginPackoutNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='OriginPackout' AND notesId =?", shipNum);
	}

	public List countSitOriginNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='SitOrigin' AND notesId =?", shipNum);
	}

	public List countOriginForwardingNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='OriginForwarding' AND notesId =?", shipNum);
	}

	public List countInterStateNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='InterState' AND notesId =?", shipNum);
	}

	public List countGSTForwardingNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='GSTForwarding' AND notesId =?", shipNum);
	}

	public List countDestinationStatusNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DestinationStatus' AND notesId =?", shipNum);
	}

	public List countSitDestinationNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='SitDestination' AND notesId =?", shipNum);
	}

	public List countDestinationImportNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DestinationImport' AND notesId =?", shipNum);
	}

	public List countServiceOrderNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='ServiceOrder' AND notesId =?", shipNum);
	}

	public List countForClaimNotes(Long claimNum) {
		String clmn = String.valueOf(claimNum);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Claim' AND notesId =?", clmn);
	}

	public List countClaimValuationNotes(Long claimNum) {
		String clmn = String.valueOf(claimNum);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='ClaimValuation' AND notesId =?", clmn);
	}

	public List countLossNotes(Long claimNum) {
		String clmn = String.valueOf(claimNum);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Loss' AND notesId =?", clmn);
	}

	public List countLossTicketNotes(Long claimNum) {
		String clmn = String.valueOf(claimNum);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='LossTicket' AND notesId =?", clmn);
	}

	public List countForTicketBillingNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketBilling' AND notesId =?", tkt);
	}

	public List countForDomesticNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Domestic' AND notesId =?", shipNum);
	}

	public List countForDomesticServiceNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DomService' AND notesId =?", shipNum);
	}

	public List countForDomesticStateCountryNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DomStateCountry' AND notesId =?", shipNum);
	}

	public List countForTicketStaffNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketStaff' AND notesId =?", tkt);
	}

	public List countForTicketSchedulingNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketScheduling' AND notesId =?", tkt);
	}

	public List countForTicketOriginNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketOrigin' AND notesId =?", tkt);
	}

	public List countForTicketDestinationNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketDestination' AND notesId =?", tkt);
	}

	public List countForTicketNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='JobTicket' AND notesId =?", tkt);
	}

	public List countTicketStorageNotes(Long ticket) {
		String tkt = ticket.toString();
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketStorage' AND notesId =?", tkt);
	}

	public List countTicketLocationNotes(Long ticket) {
		String tkt = "";
		if (ticket != null) {
			tkt = ticket.toString();
			return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketLocation' AND notesId =?", tkt);
		} else {
			tkt = "";
			return getHibernateTemplate().find("select count(*) from Notes where noteSubType='TicketLocation' AND notesId =?", tkt);
		}
	}

	public List countBillingAuthorizedNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingAuthorized' AND notesId =?", shipNum);
	}

	public List countBillingValuationNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingValuation' AND notesId =?", shipNum);
	}

	public List countBillingSecondaryNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingSecondary' AND notesId =?", shipNum);
	}

	public List countBillingPrivatePartyNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingPrivateParty' AND notesId =?", shipNum);
	}

	public List notesBillingCompleteStatus(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingStatus' AND notesId =?", shipNum);
	}

	public List countBillingPrimaryNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingPrimary' AND notesId =?", shipNum);
	}

	public List countBillingDomesticNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='BillingDomestic' AND notesId =?", shipNum);
	}

	public List countCreditCardNotes(Long creditCardId) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='CreditCard' AND notesId='" + creditCardId + "'");
	}

	public List<CustomerFile> getCustomerFilesDetails(String customerNumber) {
		return getHibernateTemplate().find("from CustomerFile where sequenceNumber=?", customerNumber);
	}
	public List<ServiceOrder> getServiceOrederDetils(String notesId) {
		return getHibernateTemplate().find("from ServiceOrder where shipNumber=?", notesId);
	}
	public void updateNotesFromRules(String note,String subject, Long id, String forwardUser, String updatedBy, String remindInterval,String noteType, String noteSubType, String linkedTo, String supplier, String grading, String issueType){
		getHibernateTemplate().bulkUpdate("update Notes set note='" + note.replaceAll("'", "''") + "',subject='" + subject.replaceAll("'", "''") + "',updatedOn=now(),followUpFor='" + forwardUser + "',updatedBy='" + updatedBy + "',remindInterval='" + remindInterval + "',noteType='"+noteType+"' ,noteSubType='"+noteSubType+"',linkedTo='"+linkedTo+"',supplier='"+supplier+"',grading='"+grading+"',issueType='"+issueType+"' where id=?", id);

	}

	public void dismissReminder(Long id) {
		getHibernateTemplate().bulkUpdate("update Notes set reminderStatus='OLD' where id=?", id);

	}

	public void updateForwardDateFromRules(String forwardDate, String remindTime, Long id) {
		getHibernateTemplate().bulkUpdate("update Notes set forwardDate='" + forwardDate + "',remindTime='" + remindTime + "' where id=?", id);

	}

	public List findByUserActivity(String userName, String timeZone) {
		TimeZone timeZones = TimeZone.getTimeZone(timeZone);
		SimpleDateFormat timeZoneFormate = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		timeZoneFormate.setTimeZone(timeZones);
		return getHibernateTemplate().find(
				"select CONCAT(id,'#',remindinterval,'#',subject,'#',TIME_TO_SEC(TIMEDIFF(CONCAT(date_format(forwardDate,'%Y-%m-%d '),str_to_date(remindTime,'%H:%i')),date_format('"
				+ timeZoneFormate.format(new Date())
				+ "','%Y-%m-%d %H:%i')))) from Notes where date_format(forwardDate,'%Y-%m-%d ') = date_format('"
				+ timeZoneFormate.format(new Date())
				+ "','%Y-%m-%d ') and noteStatus = 'NEW' and TIME_TO_SEC(TIMEDIFF(CONCAT(date_format(forwardDate,'%Y-%m-%d '),str_to_date(remindTime,'%H:%i')),date_format('"
				+ timeZoneFormate.format(new Date()) + "','%Y-%m-%d %H:%i')))>0 and followUpFor=?", userName);
	}

	public List countAccountProfileNotes(String partnerCode, String sessionCorpID) {
		return getHibernateTemplate().find("select count(*) from Notes where noteType ='Partner' AND corpID='" + sessionCorpID + "' AND customerNumber='XXXXXXXX' AND notesId =?", partnerCode);
	}

	public List countAccountContactNotes(String partnerCode, String sessionCorpID,Long kId) {
		return getHibernateTemplate().find("select count(*) from Notes where noteType ='Partner' AND corpID='" + sessionCorpID + "' AND customerNumber='YYYYYYYY'  AND notesId ='"+partnerCode+"' AND notesKeyId ='" + kId + "' ");
	}

	public List getListByAccountNotesId(String notesId, String subType, String type, Long kId) {
		//return getHibernateTemplate().find("from Notes where notesId='"+notesId+"' and noteSubType ='"+subType+"'  AND notesKeyId ='" + kId + "' ");
		return getHibernateTemplate().find("from Notes where notesId='"+notesId+"' and noteType ='Partner' AND notesKeyId ='" + kId + "' order by updatedOn desc ");
	}

	public List countForIMFEntitlementNotes(String shipNum) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='IMF Entitlement Notes' AND notesId =?", shipNum);
	}

	public List getRulesNotes(String fileNumber, String tdrID, String sessionCorpID) {
		return getHibernateTemplate().find("from Notes where notesKeyId='" + fileNumber + "' and toDoRuleId='" + tdrID + "' and corpID='" + sessionCorpID + "' and noteType='Activity' and  (noteSubType='ActivityManagement'  or noteSubtype = 'AgentNotes') order by updatedOn desc");
	}

	public void updateResult(String resultRecordId, String sessionCorpID) {
		getHibernateTemplate().bulkUpdate("update ToDoResult set isNotesAdded=true where id='" + resultRecordId + "' and corpID='" + sessionCorpID + "'");
	}
	
	public void updateCheckListResult(String resultRecordId, String sessionCorpID)
	
	{
		getHibernateTemplate().bulkUpdate("update CheckListResult set isNotesAdded=true where id='" + resultRecordId + "' and corpID='" + sessionCorpID + "'");
	}

	public List userSignatureNotes(String username) {
		return getHibernateTemplate().find("select signature from User where username='" + username + "'");
	}

	public List findMaximumId(String fileNumber) {
		return getHibernateTemplate().find("select max(id) from Notes where notesId='" + fileNumber + "'");
	}

	public List findMinimumId(String fileNumber) {
		return getHibernateTemplate().find("select min(id) from Notes where notesId='" + fileNumber + "'");
	}

	public List findCountId(String fileNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where notesId='" + fileNumber + "'");
	}

	public List findMaximumIdIsRal(String fileNumber) {
		return getHibernateTemplate().find("select max(id) from Notes where customerNumber='" + fileNumber + "'");
	}

	public List findMinimumIdIsRal(String fileNumber) {
		return getHibernateTemplate().find("select min(id) from Notes where customerNumber='" + fileNumber + "'");
	}

	public List findCountIdIsRal(String fileNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where customerNumber='" + fileNumber + "'");
	}

	public List goNotes(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("from  Notes where  notesId='" + fileNumber + "' and id !='" + id + "' and corpID='" + corpID + "'");
	}

	public List goNextNotes(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("select id from  Notes where  notesId='" + fileNumber + "' and id >'" + id + "' and corpID='" + corpID + "' order by 1 ");
	}

	public List goPrevNotes(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("select id from  Notes where  notesId='" + fileNumber + "' and id <'" + id + "' and corpID='" + corpID + "' order by 1 desc");
	}

	public List notesNextPrev(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("from  Notes where notesId='" + fileNumber + "' and id !='" + id + "' and corpID='" + corpID + "'");
	}

	public List goNextNotesIsRal(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("select id from  Notes where  customerNumber='" + fileNumber + "' and id >'" + id + "' and corpId in('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') order by 1 ");
	}

	public List goPrevNotesIsRal(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("select id from  Notes where  customerNumber='" + fileNumber + "' and id <'" + id + "' and corpId in('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') order by 1 desc");
	}

	public List notesNextPrevIsRal(String fileNumber, Long id, String corpID) {
		return getHibernateTemplate().find("from  Notes where customerNumber='" + fileNumber + "' and id !='" + id + "' and corpId in('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"')");
	}

	public List countForPartnerNotes(String partnerCode) {
		return getHibernateTemplate().find("select count(*) from Notes where notesId =?", partnerCode);
	}

	public List getPartnerNotesListByCustomerNumber(CustomerFile customerFile) {
		String notesID = "";

		if (customerFile.getBookingAgentCode() != null && customerFile.getBookingAgentCode().trim() != "") {
			notesID += "','" + customerFile.getBookingAgentCode();
		}
		if (customerFile.getBillToCode() != null && customerFile.getBillToCode().trim() != "") {
			notesID += "','" + customerFile.getBillToCode();
		}
		if (customerFile.getAccountCode() != null && customerFile.getAccountCode().trim() != "") {
			notesID += "','" + customerFile.getAccountCode();
		}

		Set soList = customerFile.getServiceOrders();

		Iterator it = soList.iterator();
		while (it.hasNext()) {
			ServiceOrder so = (ServiceOrder) it.next();
			if (so.getBillToCode() != null && so.getBillToCode().trim() != "") {
				notesID += "','" + so.getBillToCode();
			}
			if (so.getBrokerCode() != null && so.getBrokerCode().trim() != "") {
				notesID += "','" + so.getBrokerCode();
			}
			if (so.getBookingAgentCode() != null && so.getBookingAgentCode().trim() != "") {
				notesID += "','" + so.getBookingAgentCode();
			}

			Billing billing = billingManager.get(so.getId());
			if (billing.getBillToCode() != null && billing.getBillToCode().trim() != "") {
				notesID += "','" + billing.getBillToCode();
			}
			if (billing.getBillTo2Code() != null && billing.getBillTo2Code().trim() != "") {
				notesID += "','" + billing.getBillTo2Code();
			}
			if (billing.getPrivatePartyBillingCode() != null && billing.getPrivatePartyBillingCode().trim() != "") {
				notesID += "','" + billing.getPrivatePartyBillingCode();
			}
			if (billing.getVendorCode() != null && billing.getVendorCode().trim() != "") {
				notesID += "','" + billing.getVendorCode();
			}

			TrackingStatus trackingStatus = trackingStatusManager.get(so.getId());
			if (trackingStatus.getOriginAgentCode() != null && trackingStatus.getOriginAgentCode().trim() != "") {
				notesID += "','" + trackingStatus.getOriginAgentCode();
			}
			if (trackingStatus.getDestinationAgentCode() != null && trackingStatus.getDestinationAgentCode().trim() != "") {
				notesID += "','" + trackingStatus.getDestinationAgentCode();
			}
			if (trackingStatus.getOriginSubAgentCode() != null && trackingStatus.getOriginSubAgentCode().trim() != "") {
				notesID += "','" + trackingStatus.getOriginSubAgentCode();
			}
			if (trackingStatus.getDestinationSubAgentCode() != null && trackingStatus.getDestinationSubAgentCode().trim() != "") {
				notesID += "','" + trackingStatus.getDestinationSubAgentCode();
			}
			if (trackingStatus.getBrokerCode() != null && trackingStatus.getBrokerCode().trim() != "") {
				notesID += "','" + trackingStatus.getBrokerCode();
			}
			if (trackingStatus.getForwarderCode() != null && trackingStatus.getForwarderCode().trim() != "") {
				notesID += "','" + trackingStatus.getForwarderCode();
			}

			Miscellaneous miscellaneous = miscellaneousManager.get(so.getId());
			if (miscellaneous.getHaulingAgentCode() != null && miscellaneous.getHaulingAgentCode().trim() != "") {
				notesID += "','" + miscellaneous.getHaulingAgentCode();
			}
			if (miscellaneous.getBookerSelfPacking() != null && miscellaneous.getBookerSelfPacking().trim() != "") {
				notesID += "','" + miscellaneous.getBookerSelfPacking();
			}

		}
		return notess = getHibernateTemplate().find("from Notes where notesId in ('" + notesID + "') AND noteType = 'Partner' order by updatedOn desc");
	}

	public List findPartnerAlertList(String notesId) {
		return getHibernateTemplate().find("from Notes where notesId = '" +StringEscapeUtils.escapeSql(notesId) + "' AND noteType = 'Partner' order by updatedOn desc");
	}
	
	public List findPartnerAlertListForCf(String notesId) {
		return getHibernateTemplate().find("Select id from Notes where notesId = '" + notesId + "' AND noteType = 'Partner' order by updatedOn desc");
	}
	
	public List countForCrewNotes(Long id){
		String crid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Crew' AND notesId =?", crid);
	}

	public List countForDSNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DSBenefit' AND notesId =?", sequenceNumber);
	}
	

	public List countForDSTemporaryAccommodationNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dstempaccomodation' AND notesId =?", sequenceNumber);
	}
	public List countForDSRelocationExpenseManagementNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dsexpensemanagement' AND notesId =?", sequenceNumber);
	}
	
	public List countForDSColaServiceNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dscola' AND notesId =?", sequenceNumber);
	}
	public List countForDspDetailsNotes(String sequenceNumber,String dspNoteType) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='"+dspNoteType+"' AND notesId =?", sequenceNumber);
	}	
	public List countForDSAutomobileAssistanceNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DsAutomobileAssistance' AND notesId =?", sequenceNumber);
	}

	public List countForDSMoveMgmtNotes(String sequenceNumber){
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DsMoveMgmt' AND notesId =?", sequenceNumber);
	}
	
	public List countForDSTenancyManagementNotes(String sequenceNumber){
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DsTenancyManagement' AND notesId =?", sequenceNumber);
	}
	public List countForDSOngoingSupportNotes(String sequenceNumber){
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='DsOngoingSupport' AND notesId =?", sequenceNumber);
	}
	
	public List countForDSSchoolEducationalCounselingNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dsschooling' AND notesId =?", sequenceNumber);
	}
	
	public List countForDSHomeFindingAssistancePurchaseNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dshomepurchase' AND notesId =?", sequenceNumber);
	}	
	
	public List countForDSPreviewTripNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Preview' AND notesId =?", sequenceNumber);
	}

	public List countForDSVisaImmigrationNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='VisaImmigration' AND notesId =?", sequenceNumber);
	}

	public List countRelocationAreaInfoOrientationNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dsorientation' AND notesId =?", shipNumber);
	}

	public List countForDSTaxServicesNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dstaxes' AND notesId =?", shipNumber);
	}

	public List countForDSHomeRentalNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dshomerental' AND notesId =?", shipNumber);
	} 
	public List countForDSLanguageNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dslanguage' AND notesId =?", shipNumber);
	}
	public List countForDSCrossCulturalTrainingNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dscrossculturaltraining' AND notesId =?", shipNumber);
	}
	public List countForCountDSRepatriationNotes(String shipNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Dsrepatriation' AND notesId =?", shipNumber);
	}

	public List countForAccountPortalNotes(String sequenceNumber) {
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='AgentNotes' AND notesId =?", sequenceNumber);
	}
	public void updateNoteFileList(Long id ,String myFileIds, String sessionCorpID){
			getHibernateTemplate().bulkUpdate("update Notes set myFileId='"+myFileIds+"' where id='" +id+ "' and corpID='" + sessionCorpID + "'");
	}
	public List getListById(Long id, String sessionCorpID){
		return getHibernateTemplate().find("from Notes where id='"+id+"' and corpID='" + sessionCorpID + "' order by updatedOn desc");
	}
	public List getMyFileList(Long id){
			return getHibernateTemplate().find("select myFileId from Notes where id='"+id+"'");
	}
	public Map<String, String> findSupplier(Long id1){
		
			Map<String, String> supplierList = new LinkedHashMap<String, String>();
			List <CustomerFile> tempList = getHibernateTemplate().find("from CustomerFile where id='"+id1+"'");
			supplierList.put("","");
			String sequenceNumber=null;
			// CustomerFile customerFile= (CustomerFile)tempList.get(0);
			for (CustomerFile customerFile : tempList) {
				sequenceNumber=customerFile.getSequenceNumber();
				if((customerFile.getBookingAgentCode()!=null) && !(customerFile.getBookingAgentCode().equals(""))){
					supplierList.put(customerFile.getBookingAgentCode(), customerFile.getBookingAgentName());
				}
				if((customerFile.getAccountCode()!=null) && !(customerFile.getAccountCode().equals(""))){
					supplierList.put(customerFile.getAccountCode(), customerFile.getAccountName());
				}
				if((customerFile.getBillToCode()!=null) && !(customerFile.getBillToCode().equals(""))){
					supplierList.put(customerFile.getBillToCode(), customerFile.getBillToName());
				}
				if((customerFile.getOriginAgentCode()!=null && !(customerFile.getOriginAgentCode().equals("")))){
					supplierList.put(customerFile.getOriginAgentCode(), customerFile.getOriginAgentName());
				}
				if((customerFile.getEstimator()!=null && !(customerFile.getEstimator().equals("")))){
					supplierList.put(customerFile.getEstimator(), customerFile.getEstimator());
				}
			}
			List<TrackingStatus> tempList1 = getHibernateTemplate().find("from TrackingStatus where sequenceNumber='"+sequenceNumber+"'");
			for(TrackingStatus trackingStatus : tempList1){
				if((trackingStatus.getOriginAgentCode() != null) && !(trackingStatus.getOriginAgentCode().equals(""))){
					supplierList.put(trackingStatus.getOriginAgentCode(), trackingStatus.getOriginAgent());
				}
				if((trackingStatus.getDestinationAgentCode() != null) && !(trackingStatus.getDestinationAgentCode().equals(""))){
					supplierList.put(trackingStatus.getDestinationAgentCode(), trackingStatus.getDestinationAgent());
				}
				if((trackingStatus.getOriginSubAgentCode() != null) && !(trackingStatus.getOriginSubAgentCode().equals(""))){
					supplierList.put(trackingStatus.getOriginSubAgentCode(), trackingStatus.getOriginSubAgent());
				}
				if((trackingStatus.getDestinationSubAgentCode() != null) && !(trackingStatus.getDestinationSubAgentCode().equals(""))){
					supplierList.put(trackingStatus.getDestinationSubAgentCode(), trackingStatus.getDestinationSubAgent());
				}
				
			}
			return supplierList;
		}
	public String findDistinctAgentName(String corpId, String spNumber, String cid){
		List distinctAgents=new ArrayList();
		Map  distinctAgentsMap=new TreeMap<String,String>();
		distinctAgentsMap.put("", "");
		String sortedMapString="";
		String query="";
		String query1="";
		List destinAgents=new ArrayList();
		if(cid.equalsIgnoreCase("")){
			query="select distinct concat(concat(if(t.originAgent is null or t.originAgent='','1',t.originAgent),'#'," +
				" if(t.originAgentCode is null or t.originAgentCode='','1',t.originAgentCode)),'~'," +
				" concat(if(b.billtoname is null or b.billtoname='','1',b.billtoname),'#'," +
				" if(b.billToCode is null or b.billToCode='','1',b.billToCode)),'~'," +
				" concat(if(b.billto2name is null or b.billto2name='','1',b.billto2name),'#'," +
				" if(b.billTo2Code is null or b.billTo2Code='','1',b.billTo2Code)),'~'," +
				" concat(if(b.privatePartyBillingName is null or b.privatePartyBillingName='','1',b.privatePartyBillingName),'#'," +
				" if(b.privatePartyBillingCode is null or b.privatePartyBillingCode='','1',b.privatePartyBillingCode)),'~'," +
				" concat(if(t.destinationAgent is null or t.destinationAgent='','1',t.destinationAgent),'#'," +
				" if(t.destinationAgentcode is null or t.destinationAgentcode='','1',t.destinationAgentcode)),'~'," +
				" concat(if(m.haulerName is null or m.haulerName='','1',m.haulerName),'#'," +
				" if(m.haulingAgentCode is null or m.haulingAgentCode='','1',m.haulingAgentCode)),'~'," +
				" concat(if(t.brokerName is null or t.brokerName='','1',t.brokerName),'#'," +
				" if(t.brokerCode is null or t.brokerCode='','1',t.brokerCode)),'~'," +
				" concat(if(t.forwarder is null or t.forwarder='','1',t.forwarder),'#'," +
				" if(t.forwarderCode is null or t.forwarderCode='','1',t.forwarderCode)),'~'," +
				" concat(if(t.networkPartnerName is null or t.networkPartnerName='','1',t.networkPartnerName),'#'," +
				" if(t.networkPartnerCode is null or t.networkPartnerCode='','1',t.networkPartnerCode)),'~'," +
				" concat(if(s.bookingAgentName is null or s.bookingAgentName='','1',s.bookingAgentName),'#'," +
				" if(s.bookingAgentCode is null or s.bookingAgentCode='','1',s.bookingAgentCode)),'~'," +
				" concat(if(c.bookingAgentName is null or c.bookingAgentName='','1',c.bookingAgentName),'#'," +
				" if(c.bookingAgentCode is null or c.bookingAgentCode='','1',c.bookingAgentCode)),'~', " +
				" concat(if(c.accountName is null or c.accountName='','1',c.accountName),'#'," +
				" if(c.accountCode is null or c.accountCode='','1',c.accountCode)),'~'," +
				" concat(if(c.billToName is null or c.billToName='','1',c.billToName),'#'," +
				" if(c.billToCode is null or c.billToCode='','1',c.billToCode)),'~'," +
				" concat(if(c.originAgentName is null or c.originAgentName='','1',c.originAgentName),'#'," +
				" if(c.originAgentCode is null or c.originAgentCode='','1',c.originAgentCode)),'~'," +
				" concat(if(t.originSubAgent is null or t.originSubAgent='','1',t.originSubAgent),'#'," +
				" if(t.originSubAgentCode is null or t.originSubAgentCode='','1',t.originSubAgentCode)),'~'," +
				" concat(if(t.destinationSubAgent is null or t.destinationSubAgent='','1',t.destinationSubAgent),'#'," +
				" if(t.destinationSubAgentCode is null or t.destinationSubAgentCode='','1',t.destinationSubAgentCode)),'~'," +
				" concat(if(c.estimator is null or c.estimator='','1',c.estimator),'#'," +
				" if(c.estimator is null or c.estimator='','1',c.estimator))" +
				" )" +
				
				" from trackingstatus t,miscellaneous m,serviceorder s, customerfile c, billing b where t.id=m.id and t.corpid='"+corpId+"'" +
				" and m.corpid='"+corpId+"' and s.corpid='"+corpId+"' and b.corpid='"+corpId+"' and t.shipnumber='"+spNumber+"' and s.id=t.id and b.id=s.id" + 
			    " and c.id=s.customerFileId";
		}else{
			 query = "select distinct concat(concat(if(c.bookingAgentName is null or c.bookingAgentName='','1',c.bookingAgentName),'#'," +
				" if(c.bookingAgentCode is null or c.bookingAgentCode='','1',c.bookingAgentCode)),'~', " +
				" concat(if(c.accountName is null or c.accountName='','1',c.accountName),'#'," +
				" if(c.accountCode is null or c.accountCode='','1',c.accountCode)),'~'," +
				" concat(if(c.billToName is null or c.billToName='','1',c.billToName),'#'," +
				" if(c.billToCode is null or c.billToCode='','1',c.billToCode)),'~'," +
				" concat(if(c.originAgentName is null or c.originAgentName='','1',c.originAgentName),'#'," +
				" if(c.originAgentCode is null or c.originAgentCode='','1',c.originAgentCode)),'~'," +
				" concat(if(c.estimator is null or c.estimator='','1',c.estimator),'#'," +
				" if(c.estimator is null or c.estimator='','1',c.estimator))" +
				" )from customerfile c where c.id='"+cid+"'";
				
		}
		if((spNumber!=null && !spNumber.equals(""))){
			query1 = "select distinct concat(if(destinationAgent is null or destinationAgent='','1',destinationAgent),'#', " +
					"if(destinationAgentcode is null or destinationAgentcode='','1',destinationAgentcode)) " +
					"from trackingstatus where shipnumber='"+spNumber+"' ";
		}
		///////////////////////////////////////////////////////////////////////////////////////////
		String query2 = "select distinct concat(concat(if(originAgent is null or originAgent='','1',originAgent),'#',"+
				"if(originAgentCode is null or originAgentCode='','1',originAgentCode)),'~',"+
				"concat(if(destinationAgent is null or destinationAgent='','1',destinationAgent),'#',"+
				"if(destinationAgentCode is null or destinationAgentCode='','1',destinationAgentCode)),'~',"+
				"concat(if(originSubAgent is null or originSubAgent='','1',originSubAgent),'#',"+
				"if(originSubAgentCode is null or originSubAgentCode='','1',originSubAgentCode)),'~',"+
				"concat(if(destinationSubAgent is null or destinationSubAgent='','1',destinationSubAgent),'#',"+
				"if(destinationSubAgentCode is null or destinationSubAgentCode='','1',destinationSubAgentCode))) from trackingstatus where shipNumber='"+spNumber+"'";
		List destinAgent=new ArrayList();
		destinAgent=this.getSession().createSQLQuery(query2).list();
		////////////////////////////////////////////////////////////////////////////////////////////
		if(query1 != null && !query1.equals(""))
		{	
		destinAgents=this.getSession().createSQLQuery(query1).list();
		}
		distinctAgents=this.getSession().createSQLQuery(query).list();
		if(distinctAgents!=null && !(distinctAgents.isEmpty())){
			if(destinAgents!=null && !destinAgents.isEmpty()){
				distinctAgents.addAll(destinAgents);
			}
			/////////////////////////////////////////////////
			if(destinAgent!=null && !destinAgent.isEmpty()){
			distinctAgents.addAll(destinAgent);
			}
			//////////////////////////////////////////////////
			Iterator itt=distinctAgents.iterator();
			while(itt.hasNext()){
				String[] values=itt.next().toString().split("~");
				for(int k=0;k<values.length; k++){
					String[] str=values[k].split("#");
				if(!str[0].equalsIgnoreCase("1")){
					distinctAgentsMap.put(str[1],str[0]);
				}
				}
				
			}
			
		}
		List mapKeys=new ArrayList(distinctAgentsMap.keySet());
		List mapValues=new ArrayList(distinctAgentsMap.values());
		
		HashMap<String, String> sortedMap = new LinkedHashMap<String, String>();
		TreeSet<String> sortedSet = new TreeSet<String>(mapValues);
	    Object[] sortedArray = sortedSet.toArray();
	    int size = sortedArray.length;
	    for (int i=0; i<size; i++){
	        sortedMap.put((String)mapKeys.get(mapValues.indexOf(sortedArray[i])),(String)sortedArray[i]);
	    }
	    for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
	    	sortedMapString=sortedMapString+"~"+ entry.getKey() + "#" + entry.getValue();
	    }
	    sortedMapString= sortedMapString.substring(1,sortedMapString.length());
	    return sortedMapString;		
	}
	public List getServiceOrderNotesList(String shipNumber, String type, Boolean bookingAgent, Boolean networkAgent, Boolean originAgent, Boolean subOriginAgent, Boolean destAgent,	Boolean subDestAgent){
		String query="select concat(";
		if(networkAgent==true){query=query+" if(networkAgentExSO is null or networkAgentExSO='','',networkAgentExSO),'#', if(bookingAgentShipNumber is null or bookingAgentShipNumber='','',bookingAgentShipNumber),'#',";}
		if(originAgent==true){query=query+" if(originAgentExSO is null or originAgentExSO='','',originAgentExSO),'#',";}
		if(subOriginAgent==true){query=query+" if(originSubAgentExSO is null or originSubAgentExSO='','',originSubAgentExSO),'#',";}
		if(destAgent==true){query=query+" if(destinationAgentExSO is null or destinationAgentExSO='','',destinationAgentExSO),'#',";}
		if(subDestAgent==true){query=query+" if(destinationSubAgentExSO is null or destinationSubAgentExSO='','',destinationSubAgentExSO),'#',";}
		if(bookingAgent==true){query=query+" if(t.shipNumber is null or t.shipNumber='','',t.shipNumber) ,'#', if(bookingAgentExSO is null or bookingAgentExSO='','',bookingAgentExSO) ";}
		
		if(query.endsWith(",")==true){
			query = query.substring(0, query.lastIndexOf(",")-4);
		}
		List linkedShipNumberList= new ArrayList();
		List linkedList = new ArrayList();
		List linkedShipNumber= this.getSession().createSQLQuery("select if(bookingAgentShipNumber is null or bookingAgentShipNumber='','1',bookingAgentShipNumber) from serviceorder where shipNumber='"+shipNumber+"' and bookingAgentShipNumber not like 'UTSI%'  ").list();
		if(linkedShipNumber!=null && !linkedShipNumber.isEmpty() && (!(linkedShipNumber.get(0).toString().equalsIgnoreCase("1")))){
			query = query+" ) from trackingstatus t ,serviceorder s where t.shipNumber='"+linkedShipNumber.get(0).toString()+"' and s.id=t.id ";	
		}else{
			query = query+" ) from trackingstatus t ,serviceorder s where t.shipNumber='"+shipNumber+"' and s.id=t.id ";
		}
		linkedList=this.getSession().createSQLQuery(query).list();
		if(linkedList!=null && !linkedList.isEmpty()){
		String listString=linkedList.get(0).toString();
		String [] listArray=listString.split("#");
		for(String sNumber:listArray){
			if(!sNumber.trim().equalsIgnoreCase("")){
				linkedShipNumberList.add(sNumber);	
			}
		  }
	     }
		return linkedShipNumberList;
	}
	public void upDateNetworkLinkId(String networkLinkId, Long id){
		getHibernateTemplate().bulkUpdate("update Notes set networkLinkId='"+networkLinkId+"' where id='"+id+"'");
	}
	public List findlinkedIdList(String networkLinkId){
		List list= this.getSession().createSQLQuery("select id from notes where networkLinkId = '"+networkLinkId+"'").list();
		return list;	
	}
	public List getSurveyNotes(String sequenceNumber, Long cid, String corpID){
		return getHibernateTemplate().find("select id from Notes where noteType='Customer File' and noteSubType='Survey' and notesId='"+sequenceNumber+"' and notesKeyId = '"+cid+"' ");
	}
	public String findSubjectDescForNotes(Long id, String sessionCorpID){
		String subjectList="";
		List list = this.getSession().createSQLQuery("select subject from notes where id='"+id+"' and corpID='"+sessionCorpID+"'").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			subjectList=list.get(0).toString();
		}
		return subjectList;
	}
	public String findNoteDescForNotes(Long id, String sessionCorpID){
		String noteList="";
		List list = this.getSession().createSQLQuery("select note from notes where id='"+id+"'").list();
		if((list!=null)&&(!list.isEmpty())&&(list.get(0)!=null))
		{
			noteList=list.get(0).toString();
		}
		return noteList;
	}
	public List countForMssNotes(Long id){
		String crid = String.valueOf(id);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Mss' AND notesId =?", crid);
	}
	public List getListForMss(Long notesId, String subType) {
		return getHibernateTemplate().find("from Notes where noteType='MSS'  AND notesKeyId=? order by updatedOn desc", notesId);
	}
	public List<Notes> countNotesByShipNumAndSubType(String shipNum,String subType){
		return getHibernateTemplate().find("from Notes where noteStatus<>'CNCL' AND  noteSubType='"+subType+"' AND notesId =?", shipNum);
	} 

	public List<Notes> getNoteByNotesId(Long notesKeyId, String corpId,String notesSubType){
		return getHibernateTemplate().find("from Notes where notesKeyId="+notesKeyId+" and corpID = '"+corpId+"' and noteSubType='"+notesSubType+"'");
	}
	public List getLinkedId(String networkLinkId, String sessionCorpID){
		List list= this.getSession().createSQLQuery("select id from notes where networkLinkId = '"+networkLinkId+"' and corpid = '"+sessionCorpID+"'").list();
		return list;
	}

	public Map<String, String> countSoDetailNotes(String shipNumber, String noteSubType,String sessionCorpID) {
		Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
		try{
		List list = this .getSession() .createSQLQuery("select count(*),noteSubType from notes where noteSubType in (" + noteSubType + ") AND notesId ='"+shipNumber+"' group by noteSubType " ).list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			parameterMap.put(row[1].toString(), row[0].toString());
		}
		}catch(Exception e){
		e.printStackTrace();	
		}
		return parameterMap;
	}

	public HibernateUtil getHibernateUtil() {
		return hibernateUtil;
	}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}
	public Map<String, String> getSubCategoryRlo(String shipNumber,String sessionCorpID){
		Map<String, String> SubCategoryMap = new LinkedHashMap<String, String>(); 
		List list = this .getSession() .createSQLQuery("select serviceType from serviceorder where shipnumber='"+shipNumber+"' and corpid = '"+sessionCorpID+"' and job='RLO' " ).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("") ){
			String str=list.get(0).toString();
			String temp="";
			for(String rec:str.split(",")){
				if(temp.equalsIgnoreCase("")){
					temp="'"+rec+"'";
				}else{
					temp=temp+",'"+rec+"'";
				}
			}
			if(!temp.equalsIgnoreCase("")){
				List list1 = this .getSession() .createSQLQuery("select code,description from refmaster where parameter='SERVICE' and flex1='Relo' and code in ("+temp+") " ).list();
				Iterator itr=list1.iterator();
				while(itr.hasNext()){
					Object obj[]=(Object[])itr.next();
					SubCategoryMap.put(obj[0].toString(), obj[1].toString());
				}
				
			}
			
		}
		return SubCategoryMap;
	}
	public Map<String, String> getNoteGrading(String cfBillToCode,String sessionCorpID){
		Map<String, String> NoteGrading = new LinkedHashMap<String, String>(); 
		List list5 = this .getSession() .createSQLQuery("select  id,contactName,contactLastName from accountcontact where partnercode='"+cfBillToCode+"' and corpId='"+sessionCorpID+"' " ).list();
		Iterator itr=list5.iterator();
		while(itr.hasNext()){
			Object obj[]=(Object[])itr.next();
			
			NoteGrading.put(obj[0].toString(), (obj[1]!=null && !obj[1].toString().equals("")? obj[1].toString()+" "+obj[2].toString():obj[2].toString() ));
		}		
		return NoteGrading;
	}
	public Map<String, String> getNoteGradinglinkedTo(String spNumber,String sessionCorpID){
		Map<String, String> NoteGradinglinkedTo = new LinkedHashMap<String, String>();
		String partnerCode="";
		List list = this .getSession() .createSQLQuery("select billtocode from serviceorder where shipnumber='"+spNumber+"' and corpid = '"+sessionCorpID+"'" ).list();
		if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("") ){
			partnerCode=list.get(0).toString();
		}
		if(partnerCode!=null && !partnerCode.equals("")){
			List list5 = this .getSession() .createSQLQuery("select  id,contactName,contactLastName from accountcontact where partnercode='"+partnerCode+"' and corpId='"+sessionCorpID+"' " ).list();
			Iterator itr=list5.iterator();
			while(itr.hasNext()){
				Object obj[]=(Object[])itr.next();
				NoteGradinglinkedTo.put(obj[0].toString(), (obj[1]!=null && !obj[1].toString().equals("")? obj[1].toString()+" "+obj[2].toString():obj[2].toString() ));
			}	
		}			
		return NoteGradinglinkedTo;
	}
	
	public void disableNotesForGivenOrder(String corpId,String idList) {
		this.getSession().createSQLQuery("update notes set noteStatus='CNCL' where notesId in ("+idList+")").executeUpdate();
	}
	
	public List<Notes> findByNotesAll(String noteStatus, String noteSubType, String notesId) {

		//System.out.println(notesId);
		if (noteStatus != "" && noteSubType != "") {
			notess = getHibernateTemplate().find(
					"from Notes where  noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' AND notesId='" + notesId + "' AND noteSubType like '"
					+ noteSubType.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus != "" && noteSubType.equals("")) {
			notess = getHibernateTemplate().find("from Notes where   notesId='" + notesId + "' AND noteStatus like '" + noteStatus.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus.equals("") && noteSubType != "") {
			notess = getHibernateTemplate().find("from Notes where  notesId='" + notesId + "' AND noteSubType like '" + noteSubType.replaceAll("'", "''") + "%' order by updatedOn desc");
		}
		if (noteStatus.equals("") && noteSubType.equals("")) {
			notess = getHibernateTemplate().find("from Notes where  notesId='" + notesId + "' order by updatedOn desc");
		}

		return notess;
	}
	
	public List countForOINotes(Long sId){
		String clmn = String.valueOf(sId);
		return getHibernateTemplate().find("select count(*) from Notes where noteSubType='Claim' AND notesId =?", clmn);
	}
	
	public List getListForOI(Long id, String subType){
		return getHibernateTemplate().find("from Notes where noteType='OperationsIntelligence'  AND notesKeyId=? order by updatedOn desc", id);
	}	
	//13674 - Create dashboard
	public String getNotesCount(String sessionCorpID,String shipNumber,Long id) {
		String noteList="";
		List list = new ArrayList();
		
			 list = this.getSession().createQuery("select count(*)  from Notes where  noteStatus<>'CNCL' and corpId='" + sessionCorpID + "' and  notesId='" + shipNumber + "' and noteType='Service Order' and noteSubType='OriginAgentNotes'  and  noteskeyid='"+id+"'").list();
			
		if((list!=null)&&(!list.isEmpty()))
		{
			noteList=list.get(0).toString();
		}
		return noteList;
	}
	public String getNoteDestCount(String sessionCorpID,String shipNumber,Long id) {
		String noteList="";
		List list = new ArrayList();
		
			 list = this.getSession().createQuery("select count(*)  from Notes where noteStatus<>'CNCL' and corpId='" + sessionCorpID + "' and  notesId='" + shipNumber + "' and noteType='Service Order' and noteSubType IN ('DestinAgentNotes','DestinationStatus')  and  noteskeyid='"+id+"'").list();
			
		if((list!=null)&&(!list.isEmpty()))
		{
			noteList=list.get(0).toString();
		}
		return noteList;
	}
	public String getNoteForwardCount(String sessionCorpID,Long id) {
		String noteList="";
		List list = new ArrayList();
		
			 list = this.getSession().createQuery("select count(*)  from Notes where noteStatus<>'CNCL' and corpId='"+sessionCorpID+ "' and notesId='"+id+"' and noteType IN ('Container', 'Carton', 'Vehicle','Service Partner') and noteSubType  IN ('Container', 'Carton', 'Vehicle','Carrier') ").list();
			
		if((list!=null)&&(!list.isEmpty()))
		{
			noteList=list.get(0).toString();
		}
		return noteList;
	}
	// dashboard

	@Override
	public String accountLineNotesStatus(String accid,  String sessionCorpID) {

		String noteList="";
		List list = new ArrayList();

			 list = this.getSession().createQuery("select count(*)  from Notes where noteStatus<>'CNCL' and corpId='"+sessionCorpID+ "' and notesId IN ("+accid+") and noteType='Account Line' and noteSubType  IN ('Pending with note', 'rejected') ").list();
			
		if((list!=null)&&(!list.isEmpty()))
		{
			noteList=list.get(0).toString();
		}
		return noteList+"~"+accid;
		
	}
	
	
	public List getALLListByFamilyDetails(String notesId, String subType) {
		
		List pList=getSession().createSQLQuery("select * from notes where noteType='FamilyDetails' and  noteskeyId='"+notesId+"'").addEntity(Notes.class).list();
			
				return pList;
			
			
		
	
		//return getHibernateTemplate().find("from Notes where noteSubType like '" + subType.replaceAll("'", "''") + "%'  AND notesId=? order by updatedOn desc", notesId);
	}

	
}
	