package com.trilasoft.app.dao;

import java.util.Date;
import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.OperationsResourceLimits;

public interface OperationsResourceLimitsDao extends GenericDao<OperationsResourceLimits, Long>  {
	
	public List isExisted(Date workDate, String hub, String sessionCorpID);
	
	public List getResourceLimitsByHub(String hub, String category);
	
	public List getItemListByCategory(String category, String resource, String sessionCorpID);
	
	public List checkResource(String resource, String sessionCorpID);
	
}
