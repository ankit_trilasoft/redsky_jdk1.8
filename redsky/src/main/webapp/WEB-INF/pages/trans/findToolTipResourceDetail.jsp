<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
 <title>Resource : Quantity List</title>
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top">  
 <td align="left"><b></b></td>
 <td align="right"  style="width:30px;">
  <img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
 </td>
</tr>
</table>
<s:set name="tooltipList" value="tooltipList" scope="request"/>
<display:table name="tooltipList" class="table" requestURI="" id="tooltipList" export="false" defaultsort="1" >
<display:column  title="Resource" property="descript" style="width:60px"/>
<display:column  title="Quantity" property="qty" style="width:50px"/><configByCorp:fieldVisibility componentId="component.field.Alternative.showOnlyForICMG">
<display:column  title="Comment" property="comment" style="width:50px"/>
</configByCorp:fieldVisibility>
</display:table>