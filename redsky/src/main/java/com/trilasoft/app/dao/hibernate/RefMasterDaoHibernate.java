package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.trilasoft.app.dao.RefMasterDao;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTO1;
import com.trilasoft.app.dao.hibernate.AccountLineDaoHibernate.DTOvatCodeTotal;
import com.trilasoft.app.dao.hibernate.dto.BillingDTO;
import com.trilasoft.app.dao.hibernate.dto.HibernateDTO;
import com.trilasoft.app.dao.hibernate.dto.ReportEmailDTO;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.BillingEmailDTO;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.UserDTO;
import com.trilasoft.app.model.VanLineGLType;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;

public class RefMasterDaoHibernate extends GenericDaoHibernate<RefMaster, Long> implements RefMasterDao {

	private List users;
	private HibernateUtil hibernateUtil; 
	private Set name;

	private RefMaster refMaster = new RefMaster();
	Logger logger = Logger.getLogger(RefMasterDaoHibernate.class);
	public RefMasterDaoHibernate() {
		super(RefMaster.class);
	}

	List<RefMaster> search;

	public List<RefMaster> findByParameter(String parameter, String searchCode, String searchDescription, String corpId, String status) {
		try {
			String corp = corpId + "%";
			String c = searchCode + "%";
			String d = searchDescription + "%";
			String p = parameter ;
			Criteria crit = this.getSession().createCriteria(RefMaster.class);
			crit.add(Restrictions.ilike("code", c));
			crit.add(Restrictions.ilike("description", d));
			if(!status.equalsIgnoreCase("")){
				crit.add(Restrictions.ilike("status", status));
			}
			return crit.add(Restrictions.eq("parameter", p)).list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findClaimPerson(String corpID, String jobType){
		try {
			List list = getHibernateTemplate().find(
					"select distinct personClaims from RefJobType where corpID='"
							+ corpID + "' and job='" + jobType
							+ "' and personClaims is not null");
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List<RefMaster> findByParameterWithoutState(String parameter, String searchCode, String searchDescription, String corpId, String status) {

		try {
			String c = searchCode + "%";
			String d = searchDescription + "%";
			String p = parameter;
			Criteria crit = this.getSession().createCriteria(RefMaster.class);
			crit.add(Property.forName("parameter").ne("state"));
			crit.add(Property.forName("parameter").ne("country"));
			crit.add(Restrictions.ilike("code", c));
			crit.add(Restrictions.ilike("description", d));
			if(!status.equalsIgnoreCase("")){
				crit.add(Restrictions.ilike("status", status));
			}
			return crit.add(Restrictions.ilike("parameter", p)).list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List<RefMaster> findByParameterlist(String parameter, String corpId) {
		try {
			String query="from RefMaster where  parameter='"+ parameter+ "' and (language='en' or language='' or language is null ) order by parameter ,case when sequenceNo is not null then sequenceNo else 99999 end";
			return getHibernateTemplate().find(query);
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
		
	}
	public List<RefMaster> findByRefParameterlist(String parameter, String corpId) {
		try {
			return getHibernateTemplate().find(
					"from RefMaster where  parameter=" + "'" + parameter + "'"
							+ " order by parameter ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
		
	}

	public Map<String, String> findByParameterStatus(String parameter, String corpId, int statusNumber) {
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try {
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter=" + "'" + parameter + "'"
							+ " and (bucket2 >=" + statusNumber
							+ " or bucket2=0) group  by description");
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}

	public List<RefMaster> findByParameterlistWithoutState(String parameter, String corpId) {
		try {
			return getHibernateTemplate().find("from RefMaster where   parameter="+ "'"+ parameter+ "' AND parameter not in ('state','country') order by parameter ,case when sequenceNo is not null then sequenceNo else 99999 end");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findByParameterWithBucket2(String corpId, String parameter) {
		String[] params = { corpId, parameter };
		List<RefMaster> list;		
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try {
			list = getHibernateTemplate().find(
					"from RefMaster where parameter= '" + parameter
							+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			for (RefMaster refMaster : list) {
				if (refMaster.getBucket2() != null
						&& !refMaster.getBucket2().equals("")) {
					parameterMap.put(
							refMaster.getCode() + ":"
									+ refMaster.getDescription(),
							refMaster.getBucket2());
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}
	
	
	
	public Map<String, String> findByParameter(String corpId, String parameter) {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		
		try {
			if (parameter.equals("RMDINTRVL")
					|| parameter.equals("CONT_FREQNECY")
					|| parameter.equals("REVENUE")) {
				list = getHibernateTemplate().find(
						"from RefMaster where  parameter= '" + parameter
								+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,id");
			}else if ((parameter.equals("DPS_SERVICES"))||(parameter.equals("HSM_STATUS"))||(parameter.equals("MTS_STATUS"))||(parameter.equals("PDT_SERVICES"))||(parameter.equals("PAYMENTRESPONSIBILITY"))||(parameter.equals("IMMIGRATION_STATUS"))||(parameter.equals("VIEWSTATUS"))) {
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter
								+ "'");
			}else if (parameter.equals("MILITARY")) {
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter
								+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,code");
			} else if(parameter.equalsIgnoreCase("omni")){
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter
								+ "' and bucket !='Y' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description asc");
			} else if(parameter.equalsIgnoreCase("INVOICEFORMINPUTPARM")){
			
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter + "' and corpid= '" + corpId + "'  and status='Active' GROUP BY code Order BY code");
			
			}
			else if(parameter.equalsIgnoreCase("BILLCYCLE")){
				
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "') and status='Active'");
			System.out.print(list);
			}
	        else if(parameter.equalsIgnoreCase("vatBillingGroup")){
				
				list = getHibernateTemplate().find(
						"from RefMaster where parameter= '" + parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "') and status='Active' GROUP BY description ");
			System.out.print(list);
			}
			
			else {
			
				//			list = getHibernateTemplate().find("from RefMaster where parameter= '" + parameter + "' ORDER BY description");
				list = /*getHibernateTemplate().getSessionFactory()
						.getCurrentSession().createCriteria(RefMaster.class)
						.add(Restrictions.eq("parameter", parameter))
						.addOrder(Order.asc("description"))
						.add(Restrictions.eq("language", "en"))
						*/
						getHibernateTemplate().find("from RefMaster where parameter = '"
							+ parameter
							+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			}
			if (parameter.equals("SPECIAL")) {
				for (RefMaster refMaster : list) {
					parameterMap.put(
							refMaster.getCode() + " : "
									+ refMaster.getDescription(),
							refMaster.getCode() + " : "
									+ refMaster.getDescription());
				}
			} else if (parameter.equals("BILLINST")
					|| parameter.equals("INSOPT")) {
				for (RefMaster refMaster : list) {
					parameterMap.put(
							refMaster.getCode() + " : "
									+ refMaster.getDescription(),
							refMaster.getDescription());
				}
			} else if (parameter.equals("PAYROLL")
					|| parameter.equals("REVCALC")
					|| parameter.equals("GLCODES")) {
				for (RefMaster refMaster : list) {
					parameterMap.put(refMaster.getCode(), refMaster.getCode()
							+ " : " + refMaster.getDescription());
				}
			} else if (parameter.equals("MILITARY")) {
				for (RefMaster refMaster : list) {
					parameterMap.put(refMaster.getCode(), refMaster.getCode());
				}
			}  else if (parameter.equals("BILLCYCLE")) {
				for (RefMaster refMaster : list) {
					parameterMap.put(
							refMaster.getCode() + " : "
									+ refMaster.getDescription(),
							refMaster.getDescription());
				}
			}
			else {
				for (RefMaster refMaster : list) {
					parameterMap.put(refMaster.getCode(),
							refMaster.getDescription().trim());
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}
	
	
	public Map<String, Map<String, String>> findByMultipleParameter(String corpId, String parameters) { 

		List<RefMaster> list ; 
		Map<String, Map<String, String>> RefMasterMap = new LinkedHashMap<String, Map<String, String>>();
		try {
			//System.out.println("from RefMaster where  parameter in (" + parameters + ") and (language='en' or language='' or language is null ) ORDER BY parameter");
			list = getSession()
					.createQuery(
							"from RefMaster where  parameter in ("
									+ parameters
									+ ") and (language='en' or language='' or language is null ) ORDER BY parameter,case when sequenceNo is not null then sequenceNo else 99999 end,description ")
					.list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Map<String, String> parameterMapMILITARY = new TreeMap<String, String>();
			for (RefMaster refMaster : list) {
				if (RefMasterMap.containsKey(refMaster.getParameter())) {

					if (refMaster.getParameter() != null
							&& refMaster.getParameter().toString()
									.equals("SPECIAL")) {
						parameterMap = RefMasterMap.get(refMaster
								.getParameter());
						parameterMap.put(
								refMaster.getCode() + " : "
										+ refMaster.getDescription(),
								refMaster.getCode() + " : "
										+ refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("BILLINST"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString().equals("INSOPT"))) {
						parameterMap = RefMasterMap.get(refMaster
								.getParameter());
						parameterMap.put(refMaster.getCode() + " : "
								+ refMaster.getDescription(),
								refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("PAYROLL"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString()
									.equals("REVCALC"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString()
									.equals("GLCODES"))) {
						parameterMap = RefMasterMap.get(refMaster
								.getParameter());
						parameterMap.put(
								refMaster.getCode(),
								refMaster.getCode() + " : "
										+ refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("MILITARY"))) {
						parameterMapMILITARY = RefMasterMap.get(refMaster
								.getParameter());
						parameterMapMILITARY.put(refMaster.getCode(),
								refMaster.getCode());
						RefMasterMap.put(refMaster.getParameter(),
								parameterMapMILITARY);

					} else {
						parameterMap = RefMasterMap.get(refMaster
								.getParameter());
						parameterMap.put(refMaster.getCode(),
								refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					}

				} else {
					if (refMaster.getParameter() != null
							&& refMaster.getParameter().toString()
									.equals("SPECIAL")) {
						parameterMap = new LinkedHashMap<String, String>();
						parameterMap.put(
								refMaster.getCode() + " : "
										+ refMaster.getDescription(),
								refMaster.getCode() + " : "
										+ refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("BILLINST"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString().equals("INSOPT"))) {
						parameterMap = new LinkedHashMap<String, String>();
						parameterMap.put(refMaster.getCode() + " : "
								+ refMaster.getDescription(),
								refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("PAYROLL"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString()
									.equals("REVCALC"))
							|| (refMaster.getParameter() != null && refMaster
									.getParameter().toString()
									.equals("GLCODES"))) {
						parameterMap = new LinkedHashMap<String, String>();
						parameterMap.put(
								refMaster.getCode(),
								refMaster.getCode() + " : "
										+ refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					} else if ((refMaster.getParameter() != null && refMaster
							.getParameter().toString().equals("MILITARY"))) {
						parameterMapMILITARY = new TreeMap<String, String>();
						parameterMapMILITARY.put(refMaster.getCode(),
								refMaster.getCode());
						RefMasterMap.put(refMaster.getParameter(),
								parameterMapMILITARY);

					} else {
						parameterMap = new LinkedHashMap<String, String>();
						parameterMap.put(refMaster.getCode(),
								refMaster.getDescription());
						RefMasterMap
								.put(refMaster.getParameter(), parameterMap);

					}

				}

			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return RefMasterMap;
	}
	
	public Map<String, String> findUser(String corpId, String parameter) {

		List list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		int utsiCount=0;
		List utsiCountList=this .getSession() .createSQLQuery("select count(*) from company where UTSI is true and corpid='"+corpId+"'").list();
		if(utsiCountList!=null && (!(utsiCountList.isEmpty())) && utsiCountList.get(0)!=null && (!(utsiCountList.get(0).toString().trim().equals(""))) ){
			utsiCount=Integer.parseInt(utsiCountList.get(0).toString().trim());
		}
		if(utsiCount>0){
			try {
				if (parameter.equals("ALL_USER")) {
					list = this
							.getSession()
							.createSQLQuery(
									"SELECT distinct (username), alias FROM app_user, user_role, role "
											+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
											+ "and app_user.account_enabled=TRUE and (app_user.corpid in ('"+ corpId + "') or app_user.networkCoordinatorStatus=TRUE ) order by alias")
							.addScalar("username", Hibernate.STRING)
							.addScalar("alias", Hibernate.STRING)
							.list();
				} else if (parameter.equals("ROLE_SALE_COORD")) {
					list = this
							.getSession()
							.createSQLQuery(
									"SELECT distinct (username), alias FROM app_user, user_role, role "
											+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
											+ "and app_user.account_enabled=TRUE and role.name in ('ROLE_SALE','ROLE_COORD') "
											+ "and (app_user.corpid in ('" + corpId + "') or app_user.networkCoordinatorStatus=TRUE) order by alias")
							.addScalar("username", Hibernate.STRING)
							.addScalar("alias", Hibernate.STRING)
							.list();
				}else if (parameter.equals("ROLE_BILLING")) {
					list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role "
									+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
									+ "and app_user.account_enabled=TRUE and role.name in ('ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC') "
									+ "and (app_user.corpid in ('" + corpId + "') or app_user.networkCoordinatorStatus=TRUE) order by alias")
					.addScalar("username", Hibernate.STRING)
					.addScalar("alias", Hibernate.STRING)
					.list();
				} else {
					list = this
							.getSession()
							.createSQLQuery(
									"SELECT distinct (username), alias FROM app_user, user_role, role "
											+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
											+ "and app_user.account_enabled=TRUE "
											+ "and role.name='" + parameter
											+ "' and (app_user.corpid in ('" + corpId + "') or app_user.networkCoordinatorStatus=TRUE) order by alias")
							.addScalar("username", Hibernate.STRING)
							.addScalar("alias", Hibernate.STRING)
							.list();
				}
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					parameterMap.put(((String) row[0]).toUpperCase(),
							((String) row[1]).toUpperCase());
				}
			} catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
			}else{
		try {
			if (parameter.equals("ALL_USER")) {
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct (username), alias FROM app_user, user_role, role "
										+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
										+ "and app_user.account_enabled=TRUE and app_user.corpid in ('"
										+ corpId + "') order by alias")
						.addScalar("username", Hibernate.STRING)
						.addScalar("alias", Hibernate.STRING)
						.list();
			} else if (parameter.equals("ROLE_SALE_COORD")) {
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct (username), alias FROM app_user, user_role, role "
										+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
										+ "and app_user.account_enabled=TRUE and role.name in ('ROLE_SALE','ROLE_COORD') "
										+ "and app_user.corpid in ('" + corpId
										+ "') order by alias")
						.addScalar("username", Hibernate.STRING)
						.addScalar("alias", Hibernate.STRING)
						.list();
			}else if (parameter.equals("ROLE_BILLING")) {
				list = this
				.getSession()
				.createSQLQuery(
						"SELECT distinct (username), alias FROM app_user, user_role, role "
								+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
								+ "and app_user.account_enabled=TRUE and role.name in ('ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC') "
								+ "and app_user.corpid in ('" + corpId
								+ "') order by alias")
				.addScalar("username", Hibernate.STRING)
				.addScalar("alias", Hibernate.STRING)
				.list();
			} else {
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct (username), alias FROM app_user, user_role, role "
										+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
										+ "and app_user.account_enabled=TRUE "
										+ "and role.name='" + parameter
										+ "' and app_user.corpid in ('"
										+ corpId + "') order by alias")
						.addScalar("username", Hibernate.STRING)
						.addScalar("alias", Hibernate.STRING)
						.list();
			}
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(((String) row[0]).toUpperCase(),
						((String) row[1]).toUpperCase());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		}
		return parameterMap;

	}
	
	
	public Map<String, String> findUserUGCORP(String parameter) {

		List list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		try {
			list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role "
									+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
									+ "and app_user.account_enabled=TRUE "
									+ "and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('UGHK','UGSG','UGCN','UGJP') order by alias")
					.addScalar("username", Hibernate.STRING)
					.addScalar("alias", Hibernate.STRING)
					.list();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(((String) row[0]).toUpperCase(),
						((String) row[1]).toUpperCase());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;

	}
	
	
	public List findRole(String corpId, String parameter) {

		List<Role> list = null;

		try {
			if (parameter.equals("ALL_ROLE")) {
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT  * FROM  role where role.id = user_role.role_id")
						.addScalar("app_user.username", Hibernate.STRING)
						.list();
			} else {
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT  * FROM role where role.id = user_role.role_id role.name='"
										+ parameter + "'")
						.addScalar("app_user.username", Hibernate.STRING)
						.list();
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return list;
	}

	public List findByRoles(String corpID) {
		try {
			return getHibernateTemplate().find("from Role where corpID=?",
					corpID);
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List searchUser(String userType, String username, String firstName, String lastName, String email, String supervisor,String enableUser,String parentAgent) {
		try {
			if(parentAgent!=null && (!(parentAgent.trim().equals("")))) {

				if (enableUser.equals("Y")) {
					users = getHibernateTemplate().find(
							"from User where userType like '"
									+ userType
									+ "%' AND username like '%"
									+ username.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND  firstName like '%"
									+ firstName.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND  lastName like '%"
									+ lastName.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND email like '%"
									+ email.replaceAll("'", "''").replaceAll(":",
											"''")
									+ "%' AND supervisor like '"
									+ supervisor.replaceAll("'", "''").replaceAll(
											":", "''") + "%' AND enabled=true and parentAgent ='"+parentAgent+"'  ");
				} else {
					users = getHibernateTemplate().find(
							"from User where userType like '"
									+ userType
									+ "%' AND username like '%"
									+ username.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND  firstName like '%"
									+ firstName.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND  lastName like '%"
									+ lastName.replaceAll("'", "''").replaceAll(
											":", "''")
									+ "%' AND email like '%"
									+ email.replaceAll("'", "''").replaceAll(":",
											"''")
									+ "%' AND supervisor like '"
									+ supervisor.replaceAll("'", "''").replaceAll(
											":", "''") + "%' and parentAgent ='"+parentAgent+"' ");
				}
				
			}else {
			if (enableUser.equals("Y")) {
				users = getHibernateTemplate().find(
						"from User where userType like '"
								+ userType
								+ "%' AND username like '%"
								+ username.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  firstName like '%"
								+ firstName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  lastName like '%"
								+ lastName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND email like '%"
								+ email.replaceAll("'", "''").replaceAll(":",
										"''")
								+ "%' AND supervisor like '"
								+ supervisor.replaceAll("'", "''").replaceAll(
										":", "''") + "%' AND enabled=true ");
			} else {
				users = getHibernateTemplate().find(
						"from User where userType like '"
								+ userType
								+ "%' AND username like '%"
								+ username.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  firstName like '%"
								+ firstName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND  lastName like '%"
								+ lastName.replaceAll("'", "''").replaceAll(
										":", "''")
								+ "%' AND email like '%"
								+ email.replaceAll("'", "''").replaceAll(":",
										"''")
								+ "%' AND supervisor like '"
								+ supervisor.replaceAll("'", "''").replaceAll(
										":", "''") + "%'");
			}
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return users;
	}

	// By Vijay for use in cportal

	public List findUserDetails(String corpId, String parameter) {
		List list1;
		List userList= new ArrayList();
		String userId = "";
		try {
			String User_Detail_VALUES = " select app_user.id from app_user left outer join (user_role, role) "
					+ " on (user_role.user_id = app_user.id and "
					+ " role.id = user_role.role_id) where app_user.account_expired=FALSE and app_user.corpid='"
					+ corpId + "' and role.name='" + parameter + "'";
			list1 = this.getSession().createSQLQuery(User_Detail_VALUES).list();
			Iterator it = list1.iterator();
			while (it.hasNext()) {
				if (userId.equals("")) {
					userId = it.next().toString();
				} else {
					userId = userId + "," + it.next().toString();
				}
			}
			userList = getHibernateTemplate().find(
					"from User where id in (" + userId + ")");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return userList;
	}

	public List searchByCodeAndDescription(String searchCode, String searchDescription, String corpId) {
		try {
			return getHibernateTemplate().find(
					"from RefMaster where  code like '" + searchCode
							+ "%' OR  description like '" + searchDescription
							+ "%' order by parameter ,case when sequenceNo is not null then sequenceNo else 99999 end");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List<RefMaster> getList() {
		List result = new ArrayList();
		try {
			List ls = getHibernateTemplate().find(
					"select distinct UCASE(parameter) from RefMaster");
			Iterator it = ls.iterator();
			HibernateDTO dTO;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				dTO = new HibernateDTO();
				dTO.setDescription(obj[0]);
				dTO.setShipNumber(obj[1]);
				result.add(dTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return result;
	}

	public List<RefMaster> searchList(String parameter, String corpId,String nameonform) {
		List result = new ArrayList();
		try {
			/*List list = getSession().createSQLQuery(
						"select distinct UCASE(parameter),fieldLength from  refmaster  where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(corpId)+"','"+corpId+"') AND parameter like'" + parameter
						+ "%' group by parameter order by parameter asc").list();*/
			String query="select distinct UCASE(parameter),fieldLength, comments,description from  parametercontrol  where corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId)+ "','"+ corpId+ "') AND parameter like'"+ parameter+ "%' ";
					if(nameonform!=null && !nameonform.equalsIgnoreCase("")){
						query=query+ "and nameonform like'"+nameonform+ "%' ";
					}
					query=query+ " group by parameter order by parameter asc";
			List list = getSession().createSQLQuery(query)
					.list();
			Iterator it = list.iterator();
			HibernateDTO dTO;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				dTO = new HibernateDTO();
				dTO.setDescription(obj[0]);
				dTO.setShipNumber(obj[1]);
				if (obj[2] == null) {
					dTO.setComments("");
				} else {
					dTO.setComments(obj[2]);
				}
				if (obj[3] == null) {
					dTO.setAbbreviation("");
				} else {
					dTO.setAbbreviation(obj[3]);
				}

				result.add(dTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return result;
	}

	public List<RefMaster> searchListWithoutState(String parameter, String corpId,String nameonform) {
		List result = new ArrayList();
		try {
			/*List list = getSession().createSQLQuery(
						"select distinct UCASE(parameter),fieldLength from  refmaster  where parameter not in('state','country') AND parameter like'" + parameter
						+ "%' group by parameter order by parameter asc").list();*/
			String query="select distinct UCASE(parameter),fieldLength, comments,description from  parametercontrol  where parameter not in('state','country') AND parameter like'"+ parameter+ "%'";
			if(nameonform!=null && !nameonform.equalsIgnoreCase("")){
				query=query+ "and nameonform like'"+nameonform+ "%' ";
				}
				query=query+ " group by parameter order by parameter asc";
			List list = getSession().createSQLQuery(query).list();
			Iterator it = list.iterator();
			HibernateDTO dTO;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				dTO = new HibernateDTO();
				dTO.setDescription(obj[0]);
				dTO.setShipNumber(obj[1]);
				if (obj[2] == null) {
					dTO.setComments("");
				} else {
					dTO.setComments(obj[2]);
				}
				if (obj[3] == null) {
					dTO.setAbbreviation("");
				} else {
					dTO.setAbbreviation(obj[3]);
				}
				result.add(dTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return result;
	}

	public List findMaximumId() {
		try {
			return getHibernateTemplate().find("select max(id) from RefMaster");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List<RefMaster> deletedByParameterAndCode(String parameter, String code, String corpId) {
		try {
			return getHibernateTemplate().find(
					"delete from RefMaster where  code like '" + code
							+ "%' AND  parameter like '" + parameter + "%'");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	List<Object> s = new ArrayList();

	public List testJoin() {
		try {
			List results = new ArrayList();
			Object obj = getHibernateTemplate().execute(
					new HibernateCallback() {
						public Object doInHibernate(Session session) {
							List list = session
									.createQuery(
											"SELECT c.parameter ,s.sequenceNumber FROM RefMaster as c ,ServiceOrder as s where s.id=c.id")
									.list();
							String columns = "parameter, description ";
							Iterator it = list.iterator();
							RefMasterDaoHibernate.Test t = null;
							while (it.hasNext()) {
								Object[] row = (Object[]) it.next();
								Object parameter = row[0];
								Object firstname = row[1];
								t = new RefMasterDaoHibernate().new Test();
								t.setId(parameter);
								t.setSequenceNumber(firstname);
								s.add(t);
							}
							return s;
						}
					});
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return s;
	}

	public class Test {
		private Object id;

		private Object sequenceNumber;

		@Override
		public String toString() {
			return new ToStringBuilder(this).append("id", id).append("sequenceNumber", sequenceNumber).toString();
		}

		@Override
		public boolean equals(final Object other) {
			if (!(other instanceof Test))
				return false;
			Test castOther = (Test) other;
			return new EqualsBuilder().append(id, castOther.id).append(sequenceNumber, castOther.sequenceNumber).isEquals();
		}

		@Override
		public int hashCode() {
			return new HashCodeBuilder().append(id).append(sequenceNumber).toHashCode();
		}

		public Object getId() {
			return id;
		}

		Test() {
		}

		Test(Object id, Object sequenceNumber) {
			this.id = id;
			this.sequenceNumber = sequenceNumber;
		}

		public void setId(Object id) {
			this.id = id;
		}

		public Object getSequenceNumber() {
			return sequenceNumber;
		}

		public void setSequenceNumber(Object sequenceNumber) {
			this.sequenceNumber = sequenceNumber;
		}

	}

	public Map<String, String> findDocumentList(String corpID, String Param) {
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try {
			List<RefMaster> list = getHibernateTemplate()
					.find("from RefMaster where parameter = '"
							+ Param
							+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}
	
	public Map<String, String> findDocumentByJobList(String corpID, String Param,String job) {
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try {
			List<RefMaster> list = getHibernateTemplate()
					.find("from RefMaster where parameter = '"
							+ Param
							+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			for (RefMaster refMaster : list) {
				if(job==null || job.equalsIgnoreCase("") || refMaster.getflex2()==null || refMaster.getflex2().trim().equalsIgnoreCase("") || (job!=null && !job.equalsIgnoreCase("") && refMaster.getflex2()!=null && !refMaster.getflex2().trim().equalsIgnoreCase("") && refMaster.getflex2().indexOf(job)>-1)){
				parameterMap.put(refMaster.getCode(),refMaster.getDescription());
				}
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}
	public Map<String, String> findMapByJobList(String corpID, String Param,String job){
		try {
			List<RefMaster> list = getHibernateTemplate()
					.find("from RefMaster where  parameter = '"
							+ Param
							+ "' and (language='en' or language='' or language is null )");
			Map<String, String> parameterMap = new TreeMap<String, String>();
			for (RefMaster refMaster : list) {
				if(job==null || job.equalsIgnoreCase("") || refMaster.getflex2()==null || refMaster.getflex2().trim().equalsIgnoreCase("") || (job!=null && !job.equalsIgnoreCase("") && refMaster.getflex2()!=null && !refMaster.getflex2().trim().equalsIgnoreCase("") && refMaster.getflex2().indexOf(job)>-1)){
					parameterMap.put(refMaster.getBucket2(), refMaster.getBucket2());
				}
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;		
	}
	public Map<String, String> findDocumentListByCategory(String corpID, String Param,String cat) {
		Map<String, String> parameterMap = new TreeMap<String, String>();
		try {
			List<RefMaster> list = getHibernateTemplate()
					.find("from RefMaster where   parameter like '"
							+ Param
							+ "%' and (language='en' or language='' or language is null ) and flex4='"+cat+"'");
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return parameterMap;
	}

	public List findAll(String corpID) {

		List result = new ArrayList();
		try {
			List list = getSession()
					.createSQLQuery(
							"select distinct UCASE(parameter),fieldLength from  refmaster  where corpID in ('TSFT','"
									+ hibernateUtil.getParentCorpID(corpID)
									+ "','"
									+ corpID
									+ "') group by parameter order by parameter asc")
					.list();
			Iterator it = list.iterator();
			HibernateDTO dTO;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				dTO = new HibernateDTO();
				dTO.setDescription(obj[0]);
				dTO.setShipNumber(obj[1]);
				result.add(dTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return result;
	}

	public List findAccordingCorpId() {
		List result = new ArrayList();
		try {
			List list = getSession()
					.createSQLQuery(
							"select distinct UCASE(parameter),fieldLength from  refmaster  where parameter not in('state','country') group by parameter order by parameter asc")
					.list();
			Iterator it = list.iterator();
			HibernateDTO dTO;
			while (it.hasNext()) {
				Object[] obj = (Object[]) it.next();
				dTO = new HibernateDTO();
				dTO.setDescription(obj[0]);
				dTO.setShipNumber(obj[1]);
				result.add(dTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return result;
	}

	public List<RefMaster> findByParameters(String corpId, String parameter) {
		try {
			return getHibernateTemplate().find(
					"select distinct(description) from RefMaster where  parameter= '"
							+ parameter + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public Map<String,String> findByParameterCodeDesc(String corpId, String parameter) {
		try {
			List list = getHibernateTemplate().find("select distinct(description),code from RefMaster where  parameter= '" + parameter + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String,String> map = new HashMap<String,String>();
			if(!list.isEmpty()){
				Iterator it = list.iterator();
				while(it.hasNext()){
					Object ob[] = (Object[])it.next();
					map.put(ob[1].toString(),ob[0].toString());
				}
			}
			return map;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List<RefMaster> findByParameters_status(String corpId, String parameter) {
		try {
			return getHibernateTemplate().find(
					"select distinct(description) from RefMaster where parameter= '"
							+ parameter + "'");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
}
	public List test() {
		return this.getSession().createSQLQuery("").list();
	}

	public List getBucket(String code, String corpId) { 
		try {
			return getHibernateTemplate().find(
					"select bucket  from RefMaster where code='" + code
							+ "' and parameter='JOB' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findOnlyCode(String code, String corpId) {
		try {
			return getHibernateTemplate().find(
					"select code from RefMaster where parameter='" + code
							+ "'  order by case when sequenceNo is not null then sequenceNo else 99999 end,code");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findByParameterPortCountry(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getDescription(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List isExisted(String code, String parameter, String corpId,String language) {
		try {
			if (code != null) {
				code = code.replaceAll("'", "''");
			}
			return getHibernateTemplate().find(
					"from RefMaster where lower(code) = lower('" + code
							+ "') and parameter='" + parameter
							+ "' and language ='" + language + "' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List occupied(String parameter, String corpId) {
		try {
			return getHibernateTemplate().find(
					"select code from RefMaster where parameter='" + parameter
							+ "' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findCountryCode(String countryName, String corpId) {
		try {
			return getHibernateTemplate().find(
					"select code from RefMaster where description='"
							+ countryName
							+ "'  and parameter='COUNTRY' and language='en' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List findStateCode(String stateName, String corpId) {
		try {
			return getHibernateTemplate().find(
					"select code from RefMaster where description='"+ stateName+ "' and parameter='State' and language='en' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findCountry(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate()
					.find("from RefMaster where  parameter= '"
							+ parameter
							+ "' and  (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getDescription(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> findContinentList(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List list;
			list = getSession()
					.createSQLQuery(
							"select distinct flex4 from refmaster where  parameter= '"
									+ parameter
									+ "' and flex4<>''  and language='en' ORDER BY flex4")
					.list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			if ((list != null) && (!list.isEmpty()) && (list.get(0) != null)
					&& (!list.get(0).equals(""))) {
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object row = (Object) it.next();
					parameterMap.put(row.toString(), row.toString());
				}
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public List findCountryList(String countryName,String sessionCorpID){
		try {
			List list;
			list = getSession()
					.createSQLQuery(
							"select distinct description from refmaster where  parameter='COUNTRY' and flex4='"
									+ countryName
									+ "' and language='en' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
					.list();
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public List findContinent(String countryName,String sessionCorpID){
		try {
			List list;
			list = getSession()
					.createSQLQuery(
							"select distinct flex4 from refmaster where  parameter='COUNTRY' and description='"
									+ countryName
									+ "'  and language='en' ORDER BY flex4")
					.list();
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public List findCountryCodeList(String countryName,String sessionCorpID){
		try {
			List list;
			list = getSession().createSQLQuery(
					"select distinct code from refmaster where  parameter='COUNTRY' and flex4='"
							+ countryName
							+ "'  and language='en' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
					.list();
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> findCountryForPartner(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate()
					.find("from RefMaster where  parameter= '"
							+ parameter
							+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List militaryDesc(String code, String corpId) {
		try {
			return getHibernateTemplate().find(
					"select description  from RefMaster where code='" + code
							+ "' and parameter='MILITARY' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findByParameterOrderByCode(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' ORDER BY code");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(), refMaster.getCode()
						+ " : " + refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> findByParameterOrderByFlex1(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "'  and flex1='SC' ORDER BY description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query"+ e);
		}
		return null;
	}

	public Map<String, String> findState(String sessionCorpID) {
		try {
			List<RefMaster> list;
			list = getHibernateTemplate()
					.find("from RefMaster where  parameter= 'STATE' and bucket2='USA' order by case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public void updateRefmasterStopDate(Date stopDate, int fieldLength, String bucket2, String bucket, Double number, String address1, String address2, String city, String state,
		String zip, String branch, String sessionCorpID, String code, Date updatedOn, String updatedBy) {
		try {
			String formatedStopDate = "";
			SimpleDateFormat dateformat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			formatedStopDate = dateformat.format(stopDate);
			getHibernateTemplate().bulkUpdate(
					"update RefMaster set stopDate='" + formatedStopDate
							+ "', fieldLength=" + fieldLength + ",bucket2='"
							+ bucket2 + "',bucket='" + bucket + "', number="
							+ number + ",  address1='" + address1
							+ "',address2='" + address2 + "',city='" + city
							+ "', state='" + state + "', zip='" + zip
							+ "' , branch='" + branch
							+ "',updatedOn=now() , updatedBy='" + updatedBy
							+ "' where  code='" + code
							+ "' and parameter='HOUSE'");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
	}

	public Map<String, String> findByParameterWhareHouse(String corpId, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,code");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(), refMaster.getCode());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> findByParameterHubWithWarehouse(String corpId, String parameter,Map<String, String> hub) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,code");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			String str="";
			for (RefMaster refMaster : list) {
				if(refMaster.getflex1()!=null && !refMaster.getflex1().equalsIgnoreCase("") && hub.containsKey(refMaster.getflex1())){
				if(parameterMap!=null && !parameterMap.isEmpty() && parameterMap.containsKey(refMaster.getflex1())){
					str=parameterMap.get(refMaster.getflex1());
					str=str+",'"+refMaster.getCode()+"'";
					parameterMap.put(refMaster.getflex1(), str);
				}else{
					str="'"+refMaster.getCode()+"'";
					parameterMap.put(refMaster.getflex1(), str);
				}
				}
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> findGlType(String corpId) {
		try {
			String[] params = { corpId };
			List<VanLineGLType> list;
			list = getHibernateTemplate().find("from VanLineGLType where corpID in ('" + corpId+ "','XXXX','TSFT','NULL') ORDER BY glType");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (VanLineGLType vanLineGLType : list) {
				parameterMap.put(
						vanLineGLType.getGlType(),
						vanLineGLType.getGlType() + " : "
								+ vanLineGLType.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List getSuperVisor(String corpId) {
		try {
			return this
					.getSession()
					.createSQLQuery(
							"SELECT upper(username) FROM app_user where corpID='"
									+ corpId
									+ "' and userType='USER' and account_enabled=true order by alias")
					.list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findNoteSubType(String corpId, String noteType, String parameter) {
		try {
			String[] params = { corpId, parameter };
			List<RefMaster> list;
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			list = getHibernateTemplate().find(
					"from RefMaster where bucket2 = '" + noteType
							+ "'  AND parameter= '" + parameter
							+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findNoteSubTypeList(String corpId, String noteType, String parameter) {
		try {
			String query = "select code from refmaster where bucket2 = '"
					+ noteType + "' AND corpID in ('TSFT','"
					+ hibernateUtil.getParentCorpID(corpId) + "','" + corpId
					+ "') AND parameter= '" + parameter
					+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description";
			return this.getSession().createSQLQuery(query).list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public class DtoRoleList {
		private Object name;

		public Object getName() {
			return name;
		}

		public void setName(Object name) {
			this.name = name;
		}
	}

	public List findRolesUser(Long id, String sessionCorpID) {

		List list1 = new ArrayList();
		try {
			String query = "select name from role r left outer join user_role ur on ur.role_id=r.id left outer join app_user au on  ur.user_id = au.id where au.id='"
					+ id + "' and r.corpId='" + sessionCorpID + "' ";
			List lista = getSession().createSQLQuery(query).list();
			Iterator it = lista.iterator();
			while (it.hasNext()) {
				Object row = (Object) it.next();
				DtoRoleList roleListDTO = new DtoRoleList();
				roleListDTO.setName(row.toString());
				list1.add(roleListDTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return list1;

	}

	public class DtoUserList {
		private Object userName;

		public Object getUserName() {
			return userName;
		}

		public void setUserName(Object userName) {
			this.userName = userName;
		}

	}

	public List findUserRoles(String sessionCorpID, String name) {
		List list1 = new ArrayList();
		try {
			String query = "select au.username from app_user au left outer join user_role ur on  ur.user_id = au.id left outer join role r on ur.role_id=r.id where r.name='"
					+ name
					+ "' and au.account_enabled is true and au.corpID='"
					+ sessionCorpID + "'";
			List lista = getSession().createSQLQuery(query).list();
			Iterator it = lista.iterator();
			while (it.hasNext()) {
				Object row = (Object) it.next();
				DtoUserList userListDTO = new DtoUserList();
				userListDTO.setUserName(row.toString());
				list1.add(userListDTO);
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return list1;
	}

	public Map<String, String> findSaleConsultant(String corpId) {
		try {
			List<User> list;
			list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE and role.name in ('ROLE_SALE','ROLE_CONSULTANT') and app_user.corpid in ('"
									+ corpId + "') order by alias").list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(row[0].toString().toUpperCase(), row[1]
						.toString().toUpperCase());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findMapList(String corpID, String Param) {
		try {
			List<RefMaster> list = getHibernateTemplate()
					.find("from RefMaster where  parameter = '"
							+ Param
							+ "' and (language='en' or language='' or language is null )");
			Map<String, String> parameterMap = new TreeMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap
						.put(refMaster.getBucket2(), refMaster.getBucket2());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List getDescription(String originCountry, String parameter) {
		try {
			return getHibernateTemplate().find(
					"select description  from RefMaster where code='"
							+ originCountry + "' and parameter='" + parameter
							+ "'  and language='en' ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public List<String>  getCustomerServiceAnsList(String originCountry, String parameter) {
		try {
			List list = this
					.getSession()
					.createSQLQuery(
							"select description from refmaster where parameter in (select flex2 from refmaster where parameter='"
									+ parameter + "')").list();
			Iterator it = list.iterator();
			List<String> res = new ArrayList<String>();
			while (it.hasNext()) {
				String row = (String) it.next();
				//String rows[]=row.split("-");
				res.add(row);
			}
			return res;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public Map<String, String> getCustomerServiceQuesList(String corpid, String parameter){
		try {
			String str = "''";
			List list = getHibernateTemplate().find(
					"select distinct flex1 from RefMaster where  parameter='"
							+ parameter + "'  and corpID='" + corpid + "' ");
			List<String> distinctHeader = new ArrayList<String>();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				String row = (String) it.next();
				if (str.equalsIgnoreCase("")) {
					str = "'" + row + "'";
				} else {
					str = str + ",'" + row + "'";
				}
				distinctHeader.add(row);
			}
			list = getHibernateTemplate().find(
					"select distinct flex2 from RefMaster where  parameter='"
							+ parameter + "' and flex1 in (" + str
							+ ") and corpID='" + corpid + "' ");
			Map<String, String> distinctParameter = new LinkedHashMap<String, String>();
			it = list.iterator();
			while (it.hasNext()) {
				String row = (String) it.next();
				List list1 = getHibernateTemplate().find(
						"select description,id from RefMaster where  parameter='"
								+ row + "' 	and  corpID in ('TSFT','" + corpid
								+ "') ");
				String st = "<select>";
				Iterator it3 = list1.iterator();
				while (it3.hasNext()) {
					Object[] row2 = (Object[]) it3.next();
					String st9 = "<option value='" + row2[1] + "'>" + row2[0]
							+ "</option>";
					st += st9;
				}
				st += "</select>";

				distinctParameter.put(row, st);
			}
			//chaged to linked map
			Map<String, String> finalMap = new LinkedHashMap<String, String>();
			it = distinctHeader.iterator();
			while (it.hasNext()) {
				String row = (String) it.next();
				list = getHibernateTemplate().find(
						"select description,flex2,id from RefMaster where  parameter='"
								+ parameter + "' and flex1 = '" + row
								+ "' and corpID='" + corpid + "' ");
				Iterator it4 = list.iterator();
				//String ss="<table style='width:100%'><tr><td colspan='2' style='text-align:left; font-weight:bold;font-family:verdana;font-size:12px'>"+row+"</td></tr>";
				String ss = "<table style='width:100%'><tr><td colspan='2' style='text-align:left; font-weight:bold;font-family:verdana;font-size:12px'>"
						+ row + "";
				while (it4.hasNext()) {
					Object[] row4 = (Object[]) it4.next();
					String sd = distinctParameter.get(row4[1]) + "";
					String ss9 = "";
					sd = sd.replaceAll("<select>",
							"<select class='list-menu' style='width:100px' id='"
									+ row4[2] + "' onChange='pickData(this,"
									+ row4[2] + ")'>");
					if (row4[0] != null && !row4[0].equals("")) {
						ss9 = "</td></tr><tr><td class='listwhitetext' width='58%'>"
								+ row4[0] + "</td><td>" + sd + "</td></tr>";
					} else {
						ss9 = "&nbsp;&nbsp;" + sd + "</td></tr>";
					}
					ss += ss9;
				}
				ss += "</table>";

				finalMap.put(row, ss);
			}
			return finalMap;
			/*	
				List list= this.getSession().createSQLQuery("select flex1,description,id,flex2 from refmaster where  parameter='"+parameter+"'  and corpID='"+corpid+"' order by flex1").list();
				Map<String, String> tempMap = new HashMap<String, String>();
				Iterator it1 = list.iterator();
				while (it1.hasNext()) {
					Object[]  row = (Object[]) it1.next();
					tempMap.put(row[1]+""+row[3],row[0]+"~"+row[2]);
				}
				list= this.getSession().createSQLQuery("select id from refmaster where  parameter='"+parameter+"'  and corpID='"+corpid+"' order by flex1").list();
				List list1= this.getSession().createSQLQuery("select distinct flex1 from refmaster where  parameter='"+parameter+"'  and corpID='"+corpid+"' order by flex1").list();
				it1 = list.iterator();
				while (it1.hasNext()) {
					Object[]  row = (Object[]) it1.next();
					tempMap.put(row[1]+""+row[3],row[0]+"~"+row[2]);
				}
				
				
				
				
				
				
				
				
				
				//Map<String, String> parameterMap = new HashMap<String, String>();
				//String query = "select flex1,description,flex2 from refmaster where  parameter ='"+parameter+"'  and corpID='"+corpid+"' ";
				//List list= this.getSession().createSQLQuery("select concat(description,'-',flex1) from refmaster where  parameter='"+parameter+"'  and corpID='"+corpid+"' ").list();
				List list= this.getSession().createSQLQuery("select flex1,description,id,flex2 from refmaster where  parameter='"+parameter+"'  and corpID='"+corpid+"' order by flex1").list();
				Map<String, Map> outerMap = new HashMap<String, Map>();
				Map<String, String> quesiddescMap; 
				Map<String, String> ansiddescMap;
				List listAns;
				int no=1;
				Iterator it1 = list.iterator();
				while (it1.hasNext()) {
					quesiddescMap= new HashMap<String, String>();
					Object[]  row = (Object[]) it1.next();			
					 listAns=this.getSession().createSQLQuery("select distinct description, id from refmaster where parameter = '"+row[3].toString()+"'").list();
					Iterator it2 = listAns.iterator();
					Map<String, String> ansList= new HashMap<String, String>();
					while (it2.hasNext()) {
						Object [] row2 = (Object[]) it2.next();
						ansList.put(row2[1].toString(),row2[0].toString());
					}
					quesiddescMap.put(row[2].toString(),row[1].toString());
					Map<String, Map> innerMap = new HashMap<String, Map>();
					innerMap.put(row[2].toString()+"-"+row[1].toString(), ansList);
					outerMap.put(row[0].toString()+"-"+row[1].toString(), innerMap);
					//outerMap.put(row[0].toString(), innerMap);
				}
				
				return outerMap;
				
				Iterator it = list.iterator();
				while (it.hasNext()) {
					String  row = (String) it.next();
					//Object[] row = (Object[]) it.next();
					if(row!=null && !row.equals("")){
					String rows[]=row.split("-");
					parameterMap.put(rows[0], rows[1]);
					}
				}
				
				//return parameterMap;
			 */
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;	
		
	}
public List<String> getCustomerServiceQuesFlex2List(String corpid, String parameter) {
		
	try {
		List<String> parameterMap = new ArrayList<String>();
		String str = "''";
		List list = getHibernateTemplate().find(
				"select distinct flex1 from RefMaster where  parameter='"
						+ parameter + "'  and corpID='" + corpid + "' ");
		List<String> distinctHeader = new ArrayList<String>();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			String row = (String) it.next();
			if (str.equalsIgnoreCase("")) {
				str = "'" + row + "'";
			} else {
				str = str + ",'" + row + "'";
			}
			distinctHeader.add(row);
		}
		list = getHibernateTemplate().find(
				"select distinct flex2 from RefMaster where  parameter='"
						+ parameter + "' and flex1 in (" + str
						+ ") and corpID='" + corpid + "' ");
		Map<String, String> finalMap = new HashMap<String, String>();
		it = distinctHeader.iterator();
		while (it.hasNext()) {
			String row = (String) it.next();
			list = getHibernateTemplate().find(
					"select description,flex2,id from RefMaster where  parameter='"
							+ parameter + "' and flex1 = '" + row
							+ "' and corpID='" + corpid + "' ");
			Iterator it4 = list.iterator();
			//String ss="<table style='width:100%'><tr><td colspan='2' style='text-align:left; font-weight:bold;font-family:verdana;font-size:12px'>"+row+"</td></tr>";
			while (it4.hasNext()) {
				Object[] row4 = (Object[]) it4.next();
				parameterMap.add(row4[2].toString());
			}
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	
	}

	public Map<String, String> findCordinaor(String job, String parameter, String sessionCorpID) {
		try {
			List<User> list;
			int utsiCount=0;
			List utsiCountList=this .getSession() .createSQLQuery("select count(*) from company where UTSI is true and corpid='"+sessionCorpID+"'").list();
			if(utsiCountList!=null && (!(utsiCountList.isEmpty())) && utsiCountList.get(0)!=null && (!(utsiCountList.get(0).toString().trim().equals(""))) ){
				utsiCount=Integer.parseInt(utsiCountList.get(0).toString().trim());
			}
			if(utsiCount>0){
				if (parameter.equals("ROLE_BILLING")) {
					list = this
					.getSession()
					.createSQLQuery(
							" SELECT distinct (username), alias FROM app_user, user_role, role " +
							" where user_role.user_id = app_user.id and role.id = user_role.role_id " +
							" and app_user.account_expired=FALSE  and app_user.account_enabled=true " +
							" and role.name in ('ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC') " +
							" and (app_user.corpid in ('" +sessionCorpID+ "') or app_user.networkCoordinatorStatus=TRUE ) " +
							" and (jobtype is null or jobtype ='' or jobtype like '%"+ job + "%') " +
							" and app_user.username not like 'admin%'order by alias").list();
					
				}else{
				list = this
						.getSession()
						.createSQLQuery(
								"SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
										+ parameter
										+ "' and (app_user.corpid in ('" + sessionCorpID + "') or app_user.networkCoordinatorStatus=TRUE) and (jobtype is null or jobtype ='' or jobtype like '%"
										+ job + "%') and app_user.username not like 'admin%' order by alias").list();
				}
				}else{
			if (parameter.equals("ROLE_BILLING")) {
				list = this
				.getSession()
				.createSQLQuery(
						" SELECT distinct (username), alias FROM app_user, user_role, role " +
						" where user_role.user_id = app_user.id and role.id = user_role.role_id " +
						" and app_user.account_expired=FALSE  and app_user.account_enabled=true " +
						" and role.name in ('ROLE_BILLING','ROLE_BILLING_ARM','ROLE_BILLING_CAJ','ROLE_BILLING_SSC') " +
						" and app_user.corpid in ('" +sessionCorpID+ "') " +
						" and (jobtype is null or jobtype ='' or jobtype like '%"+ job + "%') " +
						"and app_user.username not like 'admin%' order by alias").list();
				
			}else{
			list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('"
									+ sessionCorpID
									+ "') and (jobtype is null or jobtype ='' or jobtype like '%"
									+ job + "%') and app_user.username not like 'admin%'order by alias").list();
			}
			}
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(row[0].toString().toUpperCase(), row[1]
						.toString().toUpperCase());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;

	}	
	
	public Map<String, String> findCordinaorForCmmDmm(String job, String parameter, String corpID){
		try {
			List<User> list;
			list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('"
									+ corpID
									+ "') and (jobtype is null or jobtype ='' or jobtype like '%"
									+ job + "%') and app_user.networkCoordinatorStatus=TRUE  and app_user.username not like 'admin%'  order by alias").list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(row[0].toString().toUpperCase(), row[1]
						.toString().toUpperCase());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;

	}
	
	public Map<String, String> findCordinaorForCorpId(String job, String parameter, String corpID,String checkCorpId){
		try {
			List<User> list;
			list = this
					.getSession()
					.createSQLQuery(
							"SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('"
									+ corpID
									+ "')  and app_user.username not like 'admin%'  and (jobtype is null or jobtype ='' or jobtype like '%"
									+ job + "%') UNION SELECT distinct (username), alias FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('"
									+ checkCorpId
									+ "') and app_user.networkCoordinatorStatus=TRUE and app_user.username not like 'admin%' order by alias").list();
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object[] row = (Object[]) it.next();
				parameterMap.put(row[0].toString().toUpperCase(), row[1]
						.toString().toUpperCase());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;

	}
	
	
	public List findCordinaorList(String job, String parameter, String sessionCorpID){
		
		int utsiCount=0;
		List utsiCountList=this .getSession() .createSQLQuery("select count(*) from company where UTSI is true and corpid='"+sessionCorpID+"'").list();
		if(utsiCountList!=null && (!(utsiCountList.isEmpty())) && utsiCountList.get(0)!=null && (!(utsiCountList.get(0).toString().trim().equals(""))) ){
			utsiCount=Integer.parseInt(utsiCountList.get(0).toString().trim());
		}
		if(utsiCount>0){
			try {
				return this
						.getSession()
						.createSQLQuery(
								"SELECT distinct CONCAT(UPPER(username),'#',UPPER(alias)) FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
										+ parameter
										+ "' and (app_user.corpid in ('" + sessionCorpID + "') or app_user.networkCoordinatorStatus=TRUE) and (jobtype is null or jobtype ='' or jobtype like '%"
										+ job + "%') and app_user.username not like 'admin%' order by alias").list();
			} catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
			}else{
		try {
			return this
					.getSession()
					.createSQLQuery(
							"SELECT distinct CONCAT(UPPER(username),'#',UPPER(alias)) FROM app_user, user_role, role where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE  and app_user.account_enabled=true and role.name='"
									+ parameter
									+ "' and app_user.corpid in ('"
									+ sessionCorpID
									+ "') and (jobtype is null or jobtype ='' or jobtype like '%"
									+ job + "%') and app_user.username not like 'admin%' order by alias").list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		}
		return null;
	}
	
	
	public List findCompanyList( String sessionCorpID){
		try {
			return this
					.getSession()
					.createSQLQuery(
							"SELECT distinct companyCode FROM companydivision where corpid in ('"
									+ sessionCorpID + "') ").list();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	public List findJobByCompanyDivision(String compDivision, String sessionCorpID){
		try {
			List list = new ArrayList();
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'JOB' AND (flex1 like '%"
							+ compDivision
							+ "%' or flex1 = '') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findUserNotes(String corpId, String parameter) {
			try {
				List<User> list;
				list = this
						.getSession()
						.createSQLQuery(
								"select distinct (username), alias from app_user where app_user.account_enabled=TRUE and app_user.corpid ='"
										+ corpId
										+ "' and app_user.userType='USER' and alias<>'' order by alias;")
						.list();
				Map<String, String> parameterMap = new LinkedHashMap<String, String>();
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					parameterMap.put(row[0].toString().toUpperCase(), row[1]
							.toString().toUpperCase());
				}
				return parameterMap;
			} catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
			return null;
      }
	
	public String findDocCategoryByDocType(String sessionCorpID, String docType){
		List list;
		String docat="";
		list = this
				.getSession()
				.createSQLQuery(
						"select flex4 from refmaster where code='"+docType+"' and parameter='DOCUMENT' and (language='en' or language='' or language is null ) "
						+ "and corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') ")
				.list();
		if(list!=null && (!(list.isEmpty())) && (list.get(0)!=null)){
			docat=list.get(0).toString();
		}
		return docat;
		
	}

	public Map<String, String> findByParameterRelo(String corpId, String parameter) {
		try {
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter=" + "'" + parameter + "'"
							+ " and flex2 ='RELO'");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findJobByCompanyDivisionRelo(String compDivision, String sessionCorpID){
		try {
			List list = new ArrayList();
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'JOB' AND (flex1 like '%"
							+ compDivision
							+ "%' or flex1 = '') AND flex2='CUST' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List findJobByCompanyDivisionReloServiceOrder(String compDivision, String sessionCorpID){
		try {
			List list = new ArrayList();
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'JOB' AND (flex1 like '%"
							+ compDivision
							+ "%' or flex1 = '') AND flex3='RELO' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
public Map<String, String> findByParameterJob(String corpId, String parameter) {
		
		try {
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' AND  flex2='CUST' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	
public Map<String, String> findByParameterJobRelo(String corpId, String parameter) {
		
		try {
			List<RefMaster> list;
			list = getHibernateTemplate().find(
					"from RefMaster where parameter= '" + parameter
							+ "' AND  flex3='RELO' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode(),
						refMaster.getDescription());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

public String findReloJob(String job, String sessionCorpID, String parameter) {
	try {
		List listReloJob = new ArrayList();
		String reloJobFlag = "";
		listReloJob = getHibernateTemplate().find(
				"select flex3 from RefMaster where  parameter= '" + parameter
						+ "' AND  code='" + job + "'");
		if (listReloJob != null && (!(listReloJob.isEmpty()))
				&& listReloJob.get(0) != null) {
			reloJobFlag = listReloJob.get(0).toString();
		}
		return reloJobFlag;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByflexValue(String corpId, String parameter){
	try {
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getflex1(), refMaster.getCode());

		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByRelocationServices(String corpId, String parameter) {
	try {
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getflex1());

		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByAnswerParamSurveyQualityParameter(String corpId, String parameter){
	try {
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getId().toString(),
					refMaster.getCode()+"~"+refMaster.getDescription());

		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findBySurveyQualityParameter(String corpId, String parameter){
	try {
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getId().toString(),
					refMaster.getCode());

		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findServiceByJob(String job, String sessionCorpID, String parameter) { 
	try {
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		if (!(job.equals("RLO"))) {
			list = getHibernateTemplate().find(
					"from RefMaster where parameter= '" + parameter
							+ "' and flex1 <>'Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		} else if (job.equals("RLO")) {
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' and flex1 ='Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");

		} else {
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' and flex1 <>'Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		}
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List findServiceByJob(String jobRelo, String sessionCorpID) {
	List list = new ArrayList();
	try {
		if (!(jobRelo.equals("RLO"))) {
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'SERVICE' AND flex1 <>'Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		} else if (jobRelo.equals("RLO")) {
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'SERVICE' AND flex1 ='Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		} else {
			list = getHibernateTemplate()
					.find("select concat(code,'#',description) from RefMaster where  parameter= 'SERVICE' AND flex1 <>'Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return list;
	
}


public void setHibernateUtil(HibernateUtil hibernateUtil) {
	this.hibernateUtil = hibernateUtil;
}

public Map<String, String> findVatPercentList(String corpID, String parameter) {
	try {
		String[] params = { corpID, parameter };
		List<RefMaster> list;
		list = getSession()
				.createSQLQuery(
						"select code,flex1 from refmaster where parameter= '"
								+ parameter + "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(corpID) + "','"
								+ corpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("flex1", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String flex1 = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				flex1 = row[1].toString();
			}

			parameterMap.put(code, flex1);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByParameteFlex2(String corpID, String parameter) {
	try {
		String[] params = { corpID, parameter };
		List<RefMaster> list;
		list = getSession()
				.createSQLQuery(
						"select code,flex2 from refmaster where parameter= '"
								+ parameter + "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(corpID) + "','"
								+ corpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("flex2", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String flex2 = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				flex2 = row[1].toString();
			}

			parameterMap.put(code, flex2);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public String findCodeByParameter(String sessionCorpID, String param,String storageLibrayType) {
		try {
			return getHibernateTemplate()
					.find("select code from RefMaster where corpID='"
							+ sessionCorpID + "' AND parameter='" + param
							+ "' AND description='" + storageLibrayType + "'")
					.get(0).toString();
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
     }

public Map<String, String> findByParameterCustomDoc(String corpId, String parameter, String county) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		//list = getHibernateTemplate().find("from RefMaster where parameter= '" + parameter + "' AND flex1= '" + county + "' ORDER BY description");
		list = getHibernateTemplate().find(
				"from RefMaster where parameter= '" + parameter
						+ "'  and language='en' ORDER BY sequenceNo,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put("", "");
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public String findCountryCodeEU(String originCountryCode, String destinationCountryCode, String sessionCorpID) {
	String result="notOk";
	try {
		List originCountryCodeEUList= getHibernateTemplate().find("select  count(*)  from RefMaster where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'COUNTRY'  and bucket='EU' and code='"+originCountryCode+"'  and language='en' ");
		List destinationCountryCodeEUList= getHibernateTemplate().find("select  count(*)  from RefMaster where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"') and parameter = 'COUNTRY'  and bucket='EU' and code='"+destinationCountryCode+"'  and language='en' ");
		try{
			if((Integer.parseInt(originCountryCodeEUList.get(0).toString())>0)&&(Integer.parseInt(destinationCountryCodeEUList.get(0).toString())>0)){
				result="Ok";	
			} 
		}catch (Exception e) {
			e.printStackTrace();
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return result;
}

public List findByParameterFlex3(String corpId, String parameter) {  
	try {
		return getHibernateTemplate().find(
				"select distinct flex3 from RefMaster where  parameter= '"
						+ parameter + "' ORDER BY flex3 ");
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;

}

public Map<String, String> findCountryIso2(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		list = getHibernateTemplate()
				.find("from RefMaster where  parameter= '"
						+ parameter
						+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getBucket2());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public String findDivisionInBucket2(String corpId, String jobType) {
	try {
		String bucket2Val = "";
		List al = getHibernateTemplate().find(
				"select distinct bucket2 from RefMaster where  parameter= 'JOB' and code='"
						+ jobType + "'");
		if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)
				&& (!al.get(0).toString().equalsIgnoreCase(""))) {
			bucket2Val = al.get(0).toString();
		}
		return bucket2Val;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterFlex4(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		list = getHibernateTemplate()
				.find("from RefMaster where  parameter= '"
						+ parameter
						+ "' and flex3 <> '' and flex3 is not null ORDER BY flex3");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getflex3(), refMaster.getflex4());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterService(String sessionCorpID, String parameter, String serviceGroup) {
	try {
		String[] params = { sessionCorpID, parameter };
		Map<String, String> parameterMap;
		List<RefMaster> list;
		if (serviceGroup == null) {
			serviceGroup = "";
		}
		list = getHibernateTemplate().find(
				"from RefMaster where parameter= '" + parameter
						+ "' AND flex1 like '%" + serviceGroup
						+ "%' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put("", "");
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByParameter(String corpId, String parameter, String job) {
	try {
		//String[] params = { corpId, parameter };
		List<RefMaster> list = new ArrayList<RefMaster>();
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' and (flex1 like '%" + job
						+ "%' or flex1='') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameter_isactive(String corpId, String parameter, String job) {
	try {
		//String[] params = { corpId, parameter };
		
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		List list = getSession()
				.createSQLQuery(
						
						"select code,description,status "
								+ "from refmaster "
								+ "where  parameter= '" + parameter
						+ "' and (flex1 like '%" + job
						+ "%' or flex1='') order by case when sequenceNo is not null then sequenceNo else 99999 end,description").list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			parameterMap.put(((String) row[0]).toUpperCase()+"~"+row[2],
					((String) row[1]).toUpperCase());
		}
		return parameterMap;
	} catch (Exception e) {
    	 e.printStackTrace();
	}
	return null;
}
public Map<String, String> findByParameterReloParentAgent(String sessionCorpID, String parameter, String parentAgent) {
	try {
		List<RefMaster> list;
		List listReloServices = new ArrayList();
		String reloServices = "";
		String relo = "";
		listReloServices = getHibernateTemplate().find(
				"select reloServices from PartnerPublic where partnerCode='"
						+ parentAgent + "'");
		if (listReloServices != null && (!(listReloServices.isEmpty()))
				&& listReloServices.get(0) != null) {
			reloServices = listReloServices.get(0).toString();
		}
		if (reloServices == null || reloServices.equals("")
				|| reloServices.equals(",")) {
		} else {
			reloServices = reloServices.trim();
			if (reloServices.indexOf(",") == 0) {
				reloServices = reloServices.substring(1);
			}
			if (reloServices.lastIndexOf(",") == reloServices.length() - 1) {
				reloServices = reloServices.substring(0,
						reloServices.length() - 1);
			}
			String[] arrayReloServices = reloServices.split("#");
			int arrayLength = arrayReloServices.length;
			for (int i = 0; i < arrayLength; i++) {
				if (!(relo.lastIndexOf("'") == relo.length() - 1))
					relo = relo + "','" + arrayReloServices[i] + "','";
				else if (relo.lastIndexOf("'") == relo.length() - 1)
					relo = relo + arrayReloServices[i] + "','";
				else
					relo = "'" + arrayReloServices[i] + "','";
			}
			if (relo.lastIndexOf("'") == relo.length() - 1) {
				relo = relo.substring(0, relo.length() - 1);
			}
			if (relo.lastIndexOf(",") == relo.length() - 1) {
				relo = relo.substring(0, relo.length() - 1);
			}
			if (relo.lastIndexOf("'") == relo.length() - 1) {
				relo = relo.substring(0, relo.length() - 1);
			}
		}
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter=" + "'" + parameter + "'"
						+ " and flex1 ='RELO' and code in ('" + relo + "')");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterASML(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		list = getHibernateTemplate()
				.find("from RefMaster where  parameter= '"
						+ parameter
						+ "' and flex1='ROLE_CORP_ACC_ASML' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	
}

public Map<String, String> findByParameterWithOutASML(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		list = getHibernateTemplate()
				.find("from RefMaster where  parameter= '"
						+ parameter
						+ "' and (flex1 !='ROLE_CORP_ACC_ASML' or flex1='' or flex1 is null) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription()); 
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	
}

public Map<String, String> findDocumentListAccountPortal(String sessionCorpID, String Param) {
	try {
		List<RefMaster> list = getHibernateTemplate().find(
				"from RefMaster where corpID in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')  and  parameter = '" + Param+ "' and  flex1 = 'accPortal'");
		Map<String, String> parameterMap = new TreeMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}


public Map<String, String> findByParameterWithoutParent(String corpID, String parameter) { 
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,description from refmaster where  parameter= '"
								+ parameter + "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(corpID) + "','"
								+ corpID + "')  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("description", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String description = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				description = row[1].toString();
			}

			parameterMap.put(code, description);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByParameterNotCF(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' and bucket2='' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public List findAvaliableVolume(String defaulVolUnit,String containerSize, String sessionCorpID){
	try {
		List emptyList = new ArrayList();
		List defaultUnit = getHibernateTemplate().find(
				"select volumeUnit from SystemDefault where corpId='"
						+ sessionCorpID + "'");
		if (defaultUnit != null && !(defaultUnit.isEmpty())) {
			if (defaultUnit.get(0).toString().equalsIgnoreCase("cbm")) {

				return getHibernateTemplate().find(
						"select flex1 from RefMaster where parameter = 'EQUIP'  and code='"
								+ containerSize + "'");
			} else {

				return getHibernateTemplate().find(
						"select flex2 from RefMaster where parameter = 'EQUIP'  and code='"
								+ containerSize + "'");
			}
		} else {
			return emptyList;
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public List typeOfWorkFlex1(String typeofWork, String parameter,String sessionCorpID){
	try {
		return getHibernateTemplate().find(
				"select flex1 from RefMaster where code='" + typeofWork
						+ "'and parameter = '" + parameter + "'");
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Boolean getCostElementFlag(String corpID) {
	try {
		List costElementList = getHibernateTemplate().find(
				"select costElement from SystemDefault where corpID = ?",
				corpID);
		if ((costElementList != null) && (!costElementList.isEmpty())) {
			if (costElementList.get(0).toString().equalsIgnoreCase("true"))
				return true;
			else
				return false;
		} else {
			return false;
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	}

public List findByParameterRadius(String sessionCorpID,String parameter){
	try {
		List<RefMaster> list;
		list = getHibernateTemplate().find(
				"select code from RefMaster where  parameter= '" + parameter
						+ "' ");
		return list;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findDescByParameter(String sessionCorpID,String parameter){
	try {
		List<RefMaster> list;
		Map<String, String> modes = new TreeMap<String, String>();
		list = getHibernateTemplate().find(
				"select description from RefMaster where  parameter= '"
						+ parameter + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		if (list != null && !(list.isEmpty())) {
			Iterator it = list.iterator();
			while (it.hasNext()) {
				String temp = it.next().toString();
				modes.put(temp, temp);
			}

		}
		return modes;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List getTimetypeFlex1List(String actionCode, String parameter, String sessionCorpID){
	try {
		List list = getHibernateTemplate().find(
				"select flex1 from RefMaster where  parameter= '" + parameter
						+ "' and code='" + actionCode + "' ");
		return list;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findWithDescriptionWithBucket2(String parameter, String sessionCorpID){
	try {
		String[] params = { sessionCorpID, parameter };
		List<RefMaster> list;
		list = getHibernateTemplate()
				.find("from RefMaster where  parameter= '"
						+ parameter
						+ "' and (language='en' or language='' or language is null ) ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap
					.put(refMaster.getBucket2(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findCodeOnleByParameter(String corpID, String parameter) {
	try {
		String[] params = { corpID, parameter };
		List<RefMaster> list;
		list = getSession()
				.createQuery(
						"from RefMaster where parameter= '" + parameter
								+ "' ORDER BY code,case when sequenceNo is not null then sequenceNo else 99999 end").list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getCode());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public String getVatDescrCode(String externalCorpID, String parameter, String VatPercent) {
	try {
		String vatCode = "";
		List vatCodeList = new ArrayList();
		String query = "select code from refmaster where flex1 = '"
				+ VatPercent + "' AND corpID in ('TSFT','"
				+ hibernateUtil.getParentCorpID(externalCorpID) + "','"
				+ externalCorpID + "') AND parameter= '" + parameter + "' ";
		vatCodeList = this.getSession().createSQLQuery(query).list();
		if (vatCodeList != null && (!(vatCodeList.isEmpty()))
				&& vatCodeList.get(0) != null) {
			vatCode = vatCodeList.get(0).toString();
		}
		return vatCode;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public String findByParameterHouse(String sessionCorpID, String warehouse, String parameter){
	try {
		String warehouseManager = "";
		List list = getHibernateTemplate().find(
				"select billCrew  from RefMaster where code='" + warehouse
						+ "' and parameter='" + parameter + "' ");
		if (list != null && (!(list.isEmpty())) && list.get(0) != null) {
			warehouseManager = list.get(0).toString();
		}
		return warehouseManager;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public String getBucket2(String code, String corpId) {
	 try {
		String Bucket2 = "";
		List Bucket2List = new ArrayList();
		Bucket2List = getHibernateTemplate().find(
				"select bucket2  from RefMaster where code='" + code
						+ "' and parameter='JOB' ");
		if (Bucket2List != null && (!(Bucket2List.isEmpty()))
				&& Bucket2List.get(0) != null) {
			Bucket2 = Bucket2List.get(0).toString();
		}
		return Bucket2;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public String getVatCodeDetail(String vatCode, String sessionCorpID,String vatParameter){
	try {
		List list = this
				.getSession()
				.createSQLQuery(
						"select concat(code,'~',flex1)  from refmaster where parameter='"
								+ vatParameter
								+ "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(sessionCorpID)
								+ "','"
								+ sessionCorpID
								+ "') and description='"
								+ vatCode
								+ "' and (language='en' or language='' or language is null ) ")
				.list();
		String vatDetail = "";
		if (list != null && !list.isEmpty() && list.get(0) != null) {
			vatDetail = list.get(0).toString();
		}
		return vatDetail;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List<String> findUserType(String sessionCorpID, String parameter){
	try {
		List<String> list = this
				.getSession()
				.createSQLQuery(
						"select description from refmaster where corpid='"
								+ sessionCorpID + "' and parameter='"
								+ parameter + "' order by description asc")
				.list();
		return list;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List<BillingEmailDTO> findBillingEmail(String partnerCode, String sessionCorpID) {
    
try {
	List<User> list;
	List<BillingEmailDTO> emailList = new ArrayList();
	BillingEmailDTO dto;
	/*list = this.getSession().createSQLQuery("Select distinct ap.id,ap.email,  ap.first_Name,ap.last_Name,ap.username "+
		"from app_user ap, userdatasecurity uds, datasecurityset  dss, datasecurityfilter dsf, datasecuritypermission dsp, "+
		"role r,user_role ur "+
		"where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id and account_enabled is true "+
		"and account_expired is not true and account_locked is not true and credentials_expired is not true "+
		"and dsf.id=dsp.datasecurityfilter_id and dss.id=dsp.datasecurityset_id and ap.parentAgent =  dsf.filtervalues "+
		"and dsf.filterValues in("+ partnerCodes +")and ap.corpID in ('TSFT','" + sessionCorpID + "') "+
		"and ap.id=ur.user_id and ur.role_id=r.id "+
		"and r.name in('ROLE_CORP_ACCT_FINAN','ROLE_CORP_ACCOUNT')").list();
	list = this.getSession().createSQLQuery("select distinct ap.id,ap.email,  ap.first_Name,ap.last_Name,ap.username,ap.functionalArea,ap.userTitle " +
			"from app_user ap, user_role ur, role r where ap.id=ur.user_id and r.id=ur.role_id "+
			"and ap.parentagent in("+ partnerCodes +") and ap.corpid in('TSFT','"+ sessionCorpID +"') "+
			"and r.name in('ROLE_CORP_ACCT_FINAN','ROLE_CORP_ACCOUNT')").list();*/
	list = this
			.getSession()
			.createSQLQuery(
					"Select distinct ap.id,ap.email,ap.first_Name,ap.last_Name,ap.username,ap.functionalArea,ap.userTitle "
							+ "from app_user ap, userdatasecurity uds, datasecurityset  dss, datasecurityfilter dsf, datasecuritypermission dsp, "
							+ "role r,user_role ur "
							+ "where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "
							+ "and (account_enabled is true or contact is true or contact is null) "
							+ "and account_expired is not true and account_locked is not true and credentials_expired is not true "
							+ " and dsf.id=dsp.datasecurityfilter_id and dss.id=dsp.datasecurityset_id "
							+ "and ap.corpID in ('TSFT','"
							+ sessionCorpID
							+ "') "
							+ "and ap.id=ur.user_id and ur.role_id=r.id "
							+ "and r.name in('ROLE_CORP_ACC_FINANCIAL','ROLE_CORP_ACCOUNT') "
							+ "and dsf.filtervalues='" + partnerCode + "'")
			.list();
	Iterator it = list.iterator();
	while (it.hasNext()) {
		dto = new BillingEmailDTO();
		Object[] row = (Object[]) it.next();
		dto.setEmail(row[1].toString());
		dto.setFirstName(row[2].toString());
		dto.setLastName(row[3].toString());
		dto.setUserName(row[4].toString());

		emailList.add(dto);
	}
	return emailList;
} catch (Exception e) {
	logger.error("Error executing query "+ e.getStackTrace()[0]);
}
return null;

}
public List findReportEmailViaCompanyDivision(String corpId,String bookingAgentCode){
	List emailList = null;
	ReportEmailDTO dto;
	String query="select corpId from companydivision where bookingAgentCode='"+bookingAgentCode+"'";
	List comDivCorpList = this.getSession().createSQLQuery(query).list();
	if((comDivCorpList!=null)&&(!comDivCorpList.isEmpty())&&(comDivCorpList.get(0)!=null)&&(!comDivCorpList.get(0).toString().equalsIgnoreCase(""))){
		emailList = new ArrayList();
		query="select a.email,a.first_Name,a.last_Name,a.username,a.id from app_user a , role r , user_role ur where ur.user_id=a.id and ur.role_id=r.id and a.account_enabled=true and r.name in ('ROLE_COORD') and a.corpID='"+comDivCorpList.get(0).toString()+"'";
		List list = this.getSession().createSQLQuery(query).list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			dto = new ReportEmailDTO();
			Object[] row = (Object[]) it.next();
			if ((row[0] != null) && (!row[0].toString().trim().equalsIgnoreCase(""))) {
				dto.setEmail(row[0].toString());
				if (row[1] != null) {
					dto.setFirstName(row[1].toString());
				} else {
					dto.setFirstName("");
				}
				;
				if (row[2] != null) {
					dto.setLastName(row[2].toString());
				} else {
					dto.setLastName("");
				}
				;
				if (row[3] != null) {
					dto.setUserName(row[3].toString());
				} else {
					dto.setUserName("");
				}
				dto.setId(row[4]);
				emailList.add(dto);
			}			
		}
	}
	return emailList;
}
public List<ReportEmailDTO> findReportEmailList(String partnerCode, String sessionCorpID) {
    
	try {
		List<User> list;
		List<ReportEmailDTO> emailList = new ArrayList();
		ReportEmailDTO dto;
		String tempPartnerCode = "";
		List tempParent = this
				.getSession()
				.createSQLQuery("select partnercode from partnerpublic where agentParent='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"') "
						+ "UNION select agentParent from partnerpublic where partnercode='"+partnerCode+"' and corpID in ('TSFT','"+sessionCorpID+"')").list();
		if (tempParent!=null && !tempParent.isEmpty() && tempParent.get(0)!=null && tempParent.size() > 0) {
			Iterator it = tempParent.iterator();
			while (it.hasNext()) {
				if (tempPartnerCode.equals("")) {
					tempPartnerCode = "'" + it.next().toString() + "'";
				} else {
					tempPartnerCode = tempPartnerCode + ",'"
							+ it.next().toString() + "'";
				}
			}
		}
		String partnerCodes = "";
		if (tempPartnerCode.equals("")) {
			partnerCodes = "'" + partnerCode + "'";
		} else {
			partnerCodes = "'" + partnerCode + "'," + tempPartnerCode;
		}
		String query = "Select distinct ap.email,ap.first_Name,ap.last_Name,ap.username,ap.id "
				+ "from app_user ap, userdatasecurity uds, datasecurityset  dss, "
				+ "datasecurityfilter dsf, datasecuritypermission dsp "
				+ "where uds.userdatasecurity_id=dss.id and ap.id = uds.user_id "
				+ "and dsf.id=dsp.datasecurityfilter_id "
				+ "and dss.id=dsp.datasecurityset_id "
				+ "and dsf.filtervalues in("
				+ partnerCodes
				+ ")"
				+ " and ap.corpID in ('TSFT','"
				+ sessionCorpID
				+ "')" 
				+" and ap.account_enabled is true"
				+ " union "
				+ "Select distinct email, first_name, last_name, username,id "
				+ "from app_user "
				+ "where basedAt in("
				+ partnerCodes
				+ ") "
				+ "and corpID in ('TSFT','"
				+ sessionCorpID
				+ "')"
				+" and account_enabled is true "
				+ "order by 1";
		list = this.getSession().createSQLQuery(query).list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			dto = new ReportEmailDTO();
			Object[] row = (Object[]) it.next();
			if ((row[0] != null)
					&& (!row[0].toString().trim().equalsIgnoreCase(""))) {
				dto.setEmail(row[0].toString());
				if (row[1] != null) {
					dto.setFirstName(row[1].toString());
				} else {
					dto.setFirstName("");
				}
				;
				if (row[2] != null) {
					dto.setLastName(row[2].toString());
				} else {
					dto.setLastName("");
				}
				;
				if (row[3] != null) {
					dto.setUserName(row[3].toString());
				} else {
					dto.setUserName("");
				}
				;
				dto.setId(row[4]);
				emailList.add(dto);
			}
		}
		return emailList;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	}
public Map<String, String> findByCategoryCode (String sessionCorpID, String parameter){
	try {
		List<RefMaster> list;
		list = getSession().createQuery(
				"from RefMaster where parameter= '" + parameter
						+ "' and corpID='" + sessionCorpID + "' and status='Active' ORDER BY description,case when sequenceNo is not null then sequenceNo else 99999 end,code")
				.list();
		Map<String, String> categoryMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			categoryMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return categoryMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByCategoryCodeByFlex (String sessionCorpID, String extTaskCode,String parameter){
	try {
		List<RefMaster> list;
		list = getSession().createQuery(
				"from RefMaster where parameter= '"+parameter+"' and flex1='"+extTaskCode
						+ "' and corpID='" + sessionCorpID + "' and status='Active' ORDER BY description")
				.list();
		Map<String, String> categoryMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			categoryMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return categoryMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, String> findByParameterUTSIPayVat(String sessionCorpID, String parameters, String externalCorpID) { 
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,bucket2,flex4 from refmaster where  parameter= '"
								+ parameters
								+ "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(externalCorpID)
								+ "','"
								+ externalCorpID
								+ "') and bucket='"
								+ sessionCorpID
								+ "' and bucket2!='' and bucket2 is not null  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("bucket2", Hibernate.STRING)
				.addScalar("flex4", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String bucket2 = "";
			String flex4 = "NO";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString().trim();
			}
			if (row[1] != null) {
				bucket2 = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				flex4 = row[2].toString().trim();
			}
			if(!(code.equals(""))){
				code=code+"~"+flex4;	
			}

			parameterMap.put(code, bucket2);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterAgetntPayVat(String sessionCorpID, String parameters, String externalCorpID) { 
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,bucket2,flex4 from refmaster where  parameter= '"
								+ parameters
								+ "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(externalCorpID)
								+ "','"
								+ externalCorpID
								+ "') and bucket='"
								+ sessionCorpID
								+ "' and bucket2!='' and bucket2 is not null  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("bucket2", Hibernate.STRING)
				.addScalar("flex4", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String bucket2 = "";
			String flex4 = "NO";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString().trim();
			}
			if (row[1] != null) {
				bucket2 = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				flex4 = row[2].toString().trim();
			}
			if(!(bucket2.equals(""))){
				bucket2=bucket2+"~"+flex4;	
			}
			parameterMap.put(bucket2, code);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public String findDivisionForReconcile(String corpID,String chargeCode,String RegNum){
	try {
		String division = "";
		List Bucket2List = new ArrayList();
		String query = "select a.division from serviceorder s,accountline a,partnerpublic p,partnerprivate pp "
				+ " where s.id=a.serviceOrderId  and a.vendorcode=p.partnercode and p.id=pp.partnerPublicId  "
				+ " and a.actualExpense is not null and a.vendorCode is not null and a.vendorCode!='' "
				+ " and a.actualExpense<>0  and p.isOwnerOp is true  and p.corpId in ('TSFT','"
				+ corpID
				+ "') "
				+ " and a.corpId='"
				+ corpID
				+ "' and pp.validNationalCode is not null  and pp.validNationalCode<>''  "
				+ " and pp.corpId='"
				+ corpID
				+ "' and p.status ='Approved' and a.chargeCode='"
				+ chargeCode
				+ "' "
				+ " and a.status=true and s.registrationNumber='"
				+ RegNum + "'";
		Bucket2List = this.getSession().createSQLQuery(query).list();
		if (Bucket2List != null && (!(Bucket2List.isEmpty()))
				&& Bucket2List.get(0) != null) {
			division = Bucket2List.get(0).toString();
		}
		return division;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public String checkInvoiceDateForWithReconcile(String corpID,String chargeCode,String RegNum){
	try {
		String division = "";
		List Bucket2List = new ArrayList();
		String query = "select a.id from serviceorder s,accountline a "
				+ " where s.id=a.serviceOrderId and a.actualRevenue is not null "
				+ " and a.actualRevenue<>0	and a.corpId='"
				+ corpID
				+ "' and a.chargeCode='"
				+ chargeCode
				+ "' "
				+ " and a.recPostDate is null and a.status=true and s.registrationNumber='"
				+ RegNum + "'";
		Bucket2List = this.getSession().createSQLQuery(query).list();
		if (Bucket2List != null && (!(Bucket2List.isEmpty()))
				&& Bucket2List.get(0) != null) {
			Iterator it = Bucket2List.iterator();
			while (it.hasNext()) {
				Object row = (Object) it.next();
				if (row != null) {
					if (division.equalsIgnoreCase("")) {
						division = row.toString();
					} else {
						division = division + "," + row.toString();
					}
				}
			}
		}
		return division;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterUserGuideModule(String corpId, String parameter) {
	try {
		String[] params = { corpId, parameter };
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY flex1");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByProperty(String corpId, String... parameter) {
	try {
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		List<RefMaster> list = getHibernateTemplate().find(	"from RefMaster where  parameter= '" + parameter[0] + "' and code='"+parameter[1]+"' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getflex2());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> findByParameterAjax(String sessionCorpID,String parameter) {
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	try {
		List list;
		list = getSession()
				.createSQLQuery(
			"select code,description from refmaster where  parameter= '"+ parameter +"' and flex1='AC' and corpID = '"+ sessionCorpID +"' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("description", Hibernate.STRING).list();
		
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String description = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				description = row[1].toString();
			}
			parameterMap.put(code, description);
			
		}
		return parameterMap;
		}catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;

	/*String[] params = { sessionCorpID, parameter };
	try {
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		list = getHibernateTemplate().find(	"from RefMaster where  parameter= '" + parameter + "' and flex1='AC'");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}*/
	

}
public Map<String, String> findServiceByJobForlinkUpNotes(String job, String sessionCorpID, String parameter) { 
	try {
		List<RefMaster> list;
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		if (!(job.equals("RLO"))) {
			list = getHibernateTemplate().find(
					"from RefMaster where parameter= '" + parameter
							+ "' and flex1 <>'Relo' and corpID='"+sessionCorpID+"' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		} else if (job.equals("RLO")) {
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' and flex1 ='Relo' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");

		} else {
			list = getHibernateTemplate().find(
					"from RefMaster where  parameter= '" + parameter
							+ "' and flex1 <>'Relo' and corpID='"+sessionCorpID+"' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		}
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}


public List<UserDTO> getAllUser(String corpId, String roles){
	List <UserDTO> allUser = new ArrayList<UserDTO>();
	List list;
	int utsiCount=0;
	List utsiCountList=this .getSession() .createSQLQuery("select count(*) from company where UTSI is true and corpid='"+corpId+"'").list();
	if(utsiCountList!=null && (!(utsiCountList.isEmpty())) && utsiCountList.get(0)!=null && (!(utsiCountList.get(0).toString().trim().equals(""))) ){
		utsiCount=Integer.parseInt(utsiCountList.get(0).toString().trim());
	}
	if(utsiCount>0){
		 list = getSession()
					.createSQLQuery(
							"SELECT distinct (app_user.username) as uname, app_user.alias ,app_user.first_Name,app_user.last_Name,role.name,app_user.jobType FROM app_user, user_role, role "
							+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
							+ "and app_user.account_enabled=TRUE and role.name in ("+roles+") "
							+ "and ( app_user.corpid in ('" + corpId + "') or app_user.networkCoordinatorStatus=TRUE) and app_user.username not like 'admin%' order by app_user.alias")
							.addScalar("uname", Hibernate.STRING)
							.addScalar("app_user.alias", Hibernate.STRING)
							.addScalar("app_user.first_Name", Hibernate.STRING)
							.addScalar("app_user.last_Name", Hibernate.STRING)
							.addScalar("role.name", Hibernate.STRING)
							.addScalar("app_user.jobType", Hibernate.STRING)
							.list();
					}else{
	 list = getSession()
	.createSQLQuery(
			"SELECT distinct (app_user.username) as uname, app_user.alias ,app_user.first_Name,app_user.last_Name,role.name,app_user.jobType FROM app_user, user_role, role "
			+ "where user_role.user_id = app_user.id and role.id = user_role.role_id and app_user.account_expired=FALSE "
			+ "and app_user.account_enabled=TRUE and role.name in ("+roles+") "
			+ "and app_user.corpid in ('" + corpId
			+ "') and app_user.username not like 'admin%' order by app_user.alias ")
			.addScalar("uname", Hibernate.STRING)
			.addScalar("app_user.alias", Hibernate.STRING)
			.addScalar("app_user.first_Name", Hibernate.STRING)
			.addScalar("app_user.last_Name", Hibernate.STRING)
			.addScalar("role.name", Hibernate.STRING)
			.addScalar("app_user.jobType", Hibernate.STRING)
			.list();
	}
	Iterator it=list.iterator();
	UserDTO dto=null;
	  while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  dto=new UserDTO(); 
		  if(row[0]!=null){
			  dto.setUserName(row[0].toString());
		  }else{
			  dto.setUserName("");
		  }
		  if(row[1]!=null){
			  dto.setAlias(row[1].toString());
		  }else{
			  dto.setAlias("");
		  }
		  if(row[2]!=null){
			  dto.setFirstName(row[2].toString());
		  }else{
			  dto.setFirstName("");
		  }
		  if(row[3]!=null){
			  dto.setLastName(row[3].toString());
		  }else{
			  dto.setLastName("");
		  }
		  if(row[4]!=null){
			  dto.setRoleName(row[4].toString());
		  }else{
			  dto.setRoleName("");
		  }
		  if(row[5]!=null){
			  dto.setJobType(row[5].toString());
		  }else{
			  dto.setJobType("");
		  }
		  allUser.add(dto);
	  }
	
	return allUser;
}


public  List<RefMasterDTO> getAllParameterValue(String corpId ,String parameters){
	List <RefMasterDTO> refValueList = new ArrayList<RefMasterDTO>(); 
	String query="select ref.parameter,ref.code,ref.description,ref.flex1,ref.flex2,ref.flex3,ref.flex4,ref.bucket2,ref.status from refmaster ref where  ref.parameter in ("+ parameters +")" +
		" and (ref.language='en' or ref.language='' or ref.language is null ) and " +
		"ref.corpID in('"+ corpId +"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"')" +
				"ORDER BY ref.parameter, if(ref.sequenceNo is not null,ref.sequenceNo,99999),ref.description";
	if(corpId!=null && (!(corpId.equalsIgnoreCase("TSFT")))){
		query="select ref.parameter,ref.code,ref.description,ref.flex1,ref.flex2,ref.flex3,ref.flex4,ref.bucket2,ref.status from refmaster ref where  ref.parameter in ("+ parameters +")" +
				" and (ref.language='en' or ref.language='' or ref.language is null ) and " +
				"ref.corpID in('"+ corpId +"','TSFT','"+hibernateUtil.getParentCorpIDForAgentAllParameter(corpId)+"')" +
						"ORDER BY ref.parameter,if(ref.sequenceNo is not null,ref.sequenceNo,99999),ref.description";	
	}
	List list = getSession()
	.createSQLQuery(query)
	.addScalar("ref.parameter", Hibernate.STRING)
	.addScalar("ref.code", Hibernate.STRING)
	.addScalar("ref.description", Hibernate.STRING)
	.addScalar("ref.flex1", Hibernate.STRING)
	.addScalar("ref.flex2", Hibernate.STRING)
	.addScalar("ref.flex3", Hibernate.STRING)
	.addScalar("ref.flex4", Hibernate.STRING)
	.addScalar("ref.bucket2", Hibernate.STRING)
	.addScalar("ref.status", Hibernate.STRING)
	.list();
	Iterator it=list.iterator();
	RefMasterDTO dto=null;
	  while(it.hasNext()) {
		  Object [] row=(Object[])it.next();
		  dto=new RefMasterDTO();
		  if(row[0]!=null){
			  dto.setParameter(row[0].toString());
		  }else{
			  dto.setParameter("");
		  }
		  if(row[1]!=null){
			  dto.setCode(row[1].toString());
		  }else{
			  dto.setCode("");
		  }
		  if(row[2]!=null){
			  dto.setDescription(row[2].toString());
		  }else{
			  dto.setDescription("");
		  }
		  if(row[3]!=null){
			  dto.setFlex1(row[3].toString());
		  }else{
			  dto.setFlex1("");
		  }
		  if(row[4]!=null){
			  dto.setFlex2(row[4].toString());
		  }else{
			  dto.setFlex2("");
		  }
		  if(row[5]!=null){
			  dto.setFlex3(row[5].toString());
		  }else{
			  dto.setFlex3("");
		  }
		  if(row[6]!=null){
			  dto.setFlex4(row[6].toString());
		  }else{
			  dto.setFlex4("");
		  }
		  if(row[7]!=null){
			  dto.setBucket2(row[7].toString());
		  }else{
			  dto.setBucket2("");
		  }
		  if(row[8]!=null && !row[8].toString().equalsIgnoreCase("")){
			  dto.setStatus(row[8].toString());
		  }else{
			  dto.setStatus("");
		  }
		  refValueList.add(dto);
	  }
	  return refValueList;
}

public Map<String, Map<String, String>> findByParameterQstVatAmtGL(String sessionCorpID) { 
	List<RefMaster> list;
	Map<String, Map<String, String>> finalMap = new LinkedHashMap<String, Map<String, String>>();
	Map<String, String> parameterQstRecMap = new LinkedHashMap<String, String>(); 
	Map<String, String> parameterQstPayMap = new LinkedHashMap<String, String>(); 
    try{	
	list = getSession() .createSQLQuery("select code,billCrew,parameter from refmaster where parameter in ('EUVAT','PAYVATDESC') and parameter is not null and language='en' and corpID in ('TSFT','" + hibernateUtil.getParentCorpID(sessionCorpID) + "','" + sessionCorpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
	.addScalar("code", Hibernate.STRING)
	.addScalar("billCrew", Hibernate.STRING).addScalar("parameter", Hibernate.STRING).list();
 
    Iterator iterator = list.iterator();
	    String code = "";
	    String billCrew = "";
	    String param = "";
     while (iterator.hasNext()) {
      Object[] row = (Object[]) iterator.next();
      if (row[0] != null) {
	     code = row[0].toString();
	     billCrew =(row[1]==null?"":row[1].toString());
	     param = row[2].toString();
	     if (param != null && param.equalsIgnoreCase("EUVAT")) {
	    	 parameterQstRecMap.put(code, billCrew);
	      }else{
	    	  parameterQstPayMap.put(code, billCrew);
	      }
      }
     } 
     if(parameterQstRecMap!=null && !parameterQstRecMap.isEmpty()){
    	 finalMap.put("EUVAT", parameterQstRecMap);
     }
     if(parameterQstPayMap!=null && !parameterQstPayMap.isEmpty()){
    	 finalMap.put("PAYVATDESC", parameterQstPayMap);
     }     
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return finalMap;
}
public Map<String, Map<String, String>> findByParameterQstVatGL(String sessionCorpID) { 
	List<RefMaster> list;
	Map<String, Map<String, String>> finalMap = new LinkedHashMap<String, Map<String, String>>();
	Map<String, String> parameterQstRecMap = new LinkedHashMap<String, String>(); 
	Map<String, String> parameterQstPayMap = new LinkedHashMap<String, String>(); 
    try{	
	list = getSession() .createSQLQuery("select code,flex7,flex8,parameter from refmaster where parameter in ('EUVAT','PAYVATDESC') and parameter is not null and language='en' and corpID in ('TSFT','" + hibernateUtil.getParentCorpID(sessionCorpID) + "','" + sessionCorpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
	.addScalar("code", Hibernate.STRING)
	.addScalar("flex7", Hibernate.STRING).addScalar("flex8", Hibernate.STRING).addScalar("parameter", Hibernate.STRING).list();
 
    Iterator iterator = list.iterator();
	    String code = "";
	    String flex7 = "";
	    String flex8 = "";
	    String param = "";
     while (iterator.hasNext()) {
      Object[] row = (Object[]) iterator.next();
      if (row[0] != null) {
	     code = row[0].toString();
	     flex7 =(row[1]==null?"":row[1].toString());
	     flex8 = (row[2]==null?"":row[2].toString());
	     param = row[3].toString();
	     if (param != null && param.equalsIgnoreCase("EUVAT")) {
	    	 parameterQstRecMap.put(code, flex7);
	      }else{
	    	  parameterQstPayMap.put(code, flex8);
	      }
      }
     } 
     if(parameterQstRecMap!=null && !parameterQstRecMap.isEmpty()){
    	 finalMap.put("EUVAT", parameterQstRecMap);
     }
     if(parameterQstPayMap!=null && !parameterQstPayMap.isEmpty()){
    	 finalMap.put("PAYVATDESC", parameterQstPayMap);
     }     
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return finalMap;
}
public Map<String, String> findByParameterVatRecGL(String sessionCorpID, String parameter) { 
	List<RefMaster> list;
	Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
    try{	
	list = getSession() .createSQLQuery("select code,flex5 from refmaster where parameter= '" + parameter + "' and language='en' and corpID in ('TSFT','" + hibernateUtil.getParentCorpID(sessionCorpID) + "','" + sessionCorpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
	.addScalar("code", Hibernate.STRING)
	.addScalar("flex5", Hibernate.STRING).list();
 
    Iterator iterator = list.iterator();
     while (iterator.hasNext()) {
      String code = "";
      String flex5 = "";
      Object[] row = (Object[]) iterator.next();
      if (row[0] != null) {
	     code = row[0].toString();
      }
      if (row[1] != null) {
	     flex5 = row[1].toString();
      } 
      parameterMap.put(code, flex5);
     } 
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return parameterMap;
}

public Map<String, String> findByParameterVatPayGL(String sessionCorpID, String parameter) { 
	List<RefMaster> list;
	Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
    try{	
	list = getSession() .createSQLQuery("select code,flex6 from refmaster where parameter= '" + parameter + "' and language='en' and corpID in ('TSFT','" + hibernateUtil.getParentCorpID(sessionCorpID) + "','" + sessionCorpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
	.addScalar("code", Hibernate.STRING)
	.addScalar("flex6", Hibernate.STRING).list();
 
    Iterator iterator = list.iterator();
     while (iterator.hasNext()) {
      String code = "";
      String flex6 = "";
      Object[] row = (Object[]) iterator.next();
      if (row[0] != null) {
	     code = row[0].toString();
      }
      if (row[1] != null) {
	     flex6 = row[1].toString();
      } 
      parameterMap.put(code, flex6);
     } 
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return parameterMap;
} 
public List findByParameterFlex5(String corpId, String parameter) {  
	try {
		return getHibernateTemplate().find(
				"select distinct flex5 from RefMaster where  parameter= '"
						+ parameter + "' ORDER BY flex5 ");
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;

}
public void deletedByParameterAndCodeList(String parameter, String code, String corpId) {
	try {
		this.getSession().createSQLQuery("delete from refmaster where  code in ("+code+") AND corpId='"+corpId+"' AND  parameter like '" + parameter + "%'").executeUpdate();
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
}
public Map<String, String> getFlex1MapByParameter(String corpId,String parameter){
	try {
		List<RefMaster> list;
		LinkedHashMap<String, String> parameterMap = new LinkedHashMap<String, String>();
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter
						+ "' ORDER BY parameter,case when sequenceNo is not null then sequenceNo else 99999 end");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getflex1(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, List> findReloServiceFieldMap(String serviceType, String corpId){
	Map<String, List> parameterMap = new LinkedHashMap<String, List>();
	List field=null;
	try {
		List list = getSession() .createSQLQuery("select fieldName,type from dspnetworkfield where corpId in ('TSFT','" + hibernateUtil.getParentCorpID(corpId) + "','" + corpId + "') ").list();
		if (list != null && !list.isEmpty() && list.get(0)!=null){
			Iterator itr=list.iterator();
			while(itr.hasNext()){
				Object obj[]=(Object[])itr.next();
				String serviceTypeTemp=obj[1]+"";
				serviceTypeTemp=serviceTypeTemp.trim();
				if(serviceType.indexOf(serviceTypeTemp)>-1){
					if(parameterMap==null || parameterMap.isEmpty()){
						field=new ArrayList();
						field.add(obj[0]+"".trim());
						parameterMap.put(obj[1]+"".trim(), field);
					}else{
						if(parameterMap.containsKey(obj[1]+"".trim())){
							field=parameterMap.get(obj[1]+"".trim());
							field.add(obj[0]+"".trim());
							parameterMap.put(obj[1]+"".trim(), field);														
						}else{
							field=new ArrayList();
							field.add(obj[0]+"".trim());
							parameterMap.put(obj[1]+"".trim(), field);							
						}
					}
					
				}
			}
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return parameterMap;
}
public List customsDocList(String sessionCorpID)
{
	return getHibernateTemplate().find(
			"from RefMaster where  parameter='COUNTRY' and language='en' ");
}
public List searchCustomDoc(String code , String description , String sessionCorpID)
{
try {
	String query = "from RefMaster where  parameter='COUNTRY' and language='en' and corpID in ('TSFT','" + hibernateUtil.getParentCorpID(sessionCorpID) + "','" + sessionCorpID + "')";
	if(code !=null && !code.equalsIgnoreCase(""))
	{
		if(   description==null || description.equalsIgnoreCase(""))
		{
		query = query + "and code like '%"+code+"%'";
	}}
	
	if(description !=null &&  code.equalsIgnoreCase("") && (code ==null || code.equalsIgnoreCase("")))
	{
		query = query + "and description like '%"+description+"%'";
	}
	
	if(!code.equalsIgnoreCase("") && !description.equalsIgnoreCase(""))
	{
		query = query + " and code like '%"+code+"%' and description like '%"+description+"%'";
	}
	return getHibernateTemplate().find(query);
} catch (Exception e) {
	logger.error("Error executing query "+ e.getStackTrace()[0]);
}
return null;
}
public List findCodeList(){
	return getHibernateTemplate().find(" SELECT distinct code FROM RefMaster where  parameter='COUNTRY'");
}

public Map<String, Map> findByParameterUTSIPayVatMap(String parameters, String sessionCorpID) { 
	List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
	Iterator it=corpidList.iterator();
	String corpID="";
	while(it.hasNext()){
		if(corpID.equals("")){
			corpID=it.next().toString();	
		}else{
			corpID=corpID+"','"+it.next().toString();	
		}
	}
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,bucket2,flex4,bucket from refmaster where  parameter= '"
								+ parameters
								+ "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(sessionCorpID)
								+ "','"
								+ sessionCorpID
								+ "') and bucket in ('"+corpID
								+"') and bucket2!='' and bucket2 is not null  ORDER BY bucket,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("bucket2", Hibernate.STRING)
				.addScalar("flex4", Hibernate.STRING)
				.addScalar("bucket", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Map<String, Map> parameterMapData = new LinkedHashMap<String, Map>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String bucket2 = "";
			String flex4 = "NO";
			String bucket = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString().trim();
			}
			if (row[1] != null) {
				bucket2 = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				flex4 = row[2].toString().trim();
			}
			if(row[3] != null && (!(row[3].toString().trim().equals("")))){
				bucket = row[3].toString().trim();
			}
			if(!(code.equals(""))){
				code=code+"~"+flex4;	
			}
			if(parameterMapData.containsKey(bucket)){
				parameterMap=parameterMapData.get(bucket);	
			}else{
				parameterMap = new LinkedHashMap<String, String>();
			}
			parameterMap.put(code, bucket2);
			parameterMapData.put(bucket, parameterMap);
		}
		return parameterMapData;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, Map> findByParameterAgetntPayVatMap(String parameters, String sessionCorpID) { 
	try {
		List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
		Iterator it=corpidList.iterator();
		String corpID="";
		while(it.hasNext()){
			if(corpID.equals("")){
				corpID=it.next().toString();	
			}else{
				corpID=corpID+"','"+it.next().toString();	
			}
		}
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,bucket2,flex4,bucket from refmaster where  parameter= '"
								+ parameters
								+ "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(sessionCorpID)
								+ "','"
								+ sessionCorpID
								+ "') and bucket in ('"+corpID 
								+ "' ) and bucket2!='' and bucket2 is not null  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("bucket2", Hibernate.STRING)
				.addScalar("flex4", Hibernate.STRING)
				.addScalar("bucket", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
		Map<String, Map> parameterMapData = new LinkedHashMap<String, Map>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String bucket2 = "";
			String flex4 = "NO";
			String bucket = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString().trim();
			}
			if (row[1] != null) {
				bucket2 = row[1].toString().trim();
			}
			if(row[2] != null && (!(row[2].toString().trim().equals("")))){
				flex4 = row[2].toString().trim();
			}
			if(row[3] != null && (!(row[3].toString().trim().equals("")))){
				bucket = row[3].toString().trim();
			}
			if(!(bucket2.equals(""))){
				bucket2=bucket2+"~"+flex4;	
			}
			if(parameterMapData.containsKey(bucket)){
				parameterMap=parameterMapData.get(bucket);	
			}else{
				parameterMap = new LinkedHashMap<String, String>();
			}
			parameterMap.put(bucket2, code);
			parameterMapData.put(bucket, parameterMap);
		}
		return parameterMapData;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, Map> findVatPercentListMap(String parameters) {
	try {
		Map<String, Map> parameterMapData = new LinkedHashMap<String, Map>();
		List corpidList=this.getSession().createSQLQuery("select corpid from company  where cmmdmmAgent is true").list();
		Iterator it=corpidList.iterator();
		String corpID="";
		while(it.hasNext()){
			 
				corpID=it.next().toString();	
			 
		//}
		//String[] params = { corpID, parameter };
		List<RefMaster> list;
		list = getSession()
				.createSQLQuery(
						"select code,flex1 from refmaster where parameter= '"
								+ parameters + "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(corpID) + "','"
								+ corpID + "') ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("flex1", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String flex1 = "";
			 
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				flex1 = row[1].toString();
			} 

			/*if(parameterMapData.containsKey(corpID)){
				parameterMap=parameterMapData.get(corpID);	
			}else{
				parameterMap = new LinkedHashMap<String, String>();
			}*/
			parameterMap.put(code, flex1);
			
		}
		parameterMapData.put(corpID, parameterMap);
		}
		return parameterMapData;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List serviceTypeList(String corpId, String parameter )
{
		List typeServiceList;
		typeServiceList = getHibernateTemplate().find(
				  "select code from RefMaster where  parameter= '" + parameter
					+ "'and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(corpId) + "','"
								+ corpId + "')  and bucket='Y' order by code ");
		return typeServiceList;
}

public String getSDHubWarehouseOperationalLimit(String sessionCorpID)
{
		String DHubWarehouseOperationalLimit = "";
		List al = getHibernateTemplate().find(
				"select hubWarehouseOperationalLimit from SystemDefault where corpId='"
				+ sessionCorpID + "'");
		if ((al != null) && (!al.isEmpty()) && (al.get(0) != null)
				&& (!al.get(0).toString().equalsIgnoreCase(""))) {
			DHubWarehouseOperationalLimit = al.get(0).toString();
		}
		return DHubWarehouseOperationalLimit;
}

public String findByCrTerms(String sessionCorpID, String jobNumber)
{
	String crList="";
	String query = "SELECT if(((partnerprivate.creditterms is null || trim(partnerprivate.creditterms)='') and serviceorder.commodity in ('EVT','ART')),'Credit for ARTIM & ICEFAT members per agreed terms. For others pre-payment is required.',(if(((partnerprivate.creditterms is null || trim(partnerprivate.creditterms)='') and serviceorder.commodity not in ('EVT','ART')),'30 days from the date of invoice of FIDI/OMNI/UNIGROUP Members and those agents/account on pre-approved creditor list. For others pre-payment will be required. Bank Charges would be on account of customer/agent remitting the payment; the Invoice must be paid in full.',refmaster_Cr.flex1))) FROM serviceorder serviceorder INNER JOIN `customerfile` customerfile ON serviceorder.`customerfileid` = customerfile.`id` and customerfile.corpId='"+sessionCorpID+"' LEFT OUTER JOIN `partnerpublic` partner ON partner.`partnerCode`=customerfile.billToCode AND partner.corpid  in ('"+sessionCorpID+"','TSFT') LEFT OUTER JOIN `partnerprivate` partnerprivate ON partnerprivate.`partnerpublicid` = partner.`id` AND partnerprivate.corpid ='"+sessionCorpID+"' LEFT OUTER JOIN `refmaster` refmaster_Cr on refmaster_Cr.`code` = partnerprivate.creditterms and refmaster_Cr.`parameter` ='CrTerms' and refmaster_Cr.`corpID` in ('TSFT','"+sessionCorpID+"') WHERE serviceorder.`corpID` = '"+sessionCorpID+"' AND serviceorder.`shipNumber`= '"+jobNumber+"' Group By customerfile.id";
	List list = this.getSession().createSQLQuery(query).list();
	if(!list.isEmpty()){
		crList = list.get(0).toString();
	}
	return crList;
}

public String surveyEmailListForRelo(String sessionCorpID,String parameters){
	String surveyList="";
	List list=this.getSession().createSQLQuery("select group_concat(distinct code) from refmaster where parameter='"+parameters+"' and flex1='Relo' and flex3='SURVEYEMAIL' and corpId in ('TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"','"+sessionCorpID+"')").list();
	if(list!=null && !list.isEmpty() && list.get(0)!=null && !list.get(0).toString().equalsIgnoreCase("")){
		surveyList = list.get(0).toString();
	}
	return surveyList;
}

public Map<String, String> findRefMasterFlex8Map(String sessionCorpID, String parameter) {
	String[] params = { sessionCorpID, parameter };
	List<RefMaster> list;		
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	try {
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter+ "' and flex8 is not null and flex8 !='' ORDER BY id");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(),
					refMaster.getFlex8());
		}
	}catch(Exception e){
		logger.error("Error executing query "+ e.getStackTrace()[0]);	
	}
	return parameterMap;
}

public Map<String, String> findRefMasterFlex7Map(String sessionCorpID, String parameter) {
	String[] params = { sessionCorpID, parameter };
	List<RefMaster> list;		
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	try {
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter+ "' and flex7 is not null and flex7 !='' ORDER BY id");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(),
					refMaster.getFlex7());
		}
	}catch(Exception e){
		logger.error("Error executing query "+ e.getStackTrace()[0]);	
	}
	return parameterMap;
}

public Map<String, String> findRefMasterFlex6Map(String sessionCorpID,String parameter) {
	String[] params = { sessionCorpID, parameter };
	List<RefMaster> list;		
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	try {
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter+ "' and flex6 is not null and flex6 !='' ORDER BY id");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(),
					refMaster.getFlex6());
		}
	}catch(Exception e){
		logger.error("Error executing query "+ e.getStackTrace()[0]);	
	}
	return parameterMap;
}

public Map<String, Map> findVatBillingGroupMap(String sessionCorpID, String parameter) {
	
	Map<String, Map> parameterMapData = new LinkedHashMap<String, Map>();
	List<RefMaster> list;		
	
	try {
		list = getHibernateTemplate().find("from RefMaster where  parameter= '" + parameter+ "'  ORDER BY id");
		for (RefMaster refMaster : list) {
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();	
			if(refMaster.getLabel1()!=null && (!(refMaster.getLabel1().toString().trim().equals("")))){
				String Flex1="NoData";
				String Flex5="NoData";
				if(refMaster.getflex1()!=null && (!(refMaster.getflex1().toString().trim().equals("")))  ){
					Flex1=refMaster.getflex1();
				}
				if(refMaster.getFlex5()!=null && (!(refMaster.getFlex5().toString().trim().equals("")))  ){
					Flex5=refMaster.getFlex5();
				}
				parameterMap.put(refMaster.getLabel1(),Flex1+"~"+Flex5 );	
			}
			
			if(refMaster.getLabel2()!=null && (!(refMaster.getLabel2().toString().trim().equals("")))){
				String Flex2="NoData";
				String Flex6="NoData";
				if(refMaster.getflex2()!=null && (!(refMaster.getflex2().toString().trim().equals("")))  ){
					Flex2=refMaster.getflex2();
				}
				if(refMaster.getFlex6()!=null && (!(refMaster.getFlex6().toString().trim().equals("")))  ){
					Flex6=refMaster.getFlex6();
				}
				parameterMap.put(refMaster.getLabel2(),Flex2+"~"+Flex6 );	
			}
			
			if(refMaster.getLabel3()!=null && (!(refMaster.getLabel3().toString().trim().equals("")))){
				String Flex3="NoData";
				String Flex7="NoData";
				if(refMaster.getflex3()!=null && (!(refMaster.getflex3().toString().trim().equals("")))  ){
					Flex3=refMaster.getflex3();
				}
				if(refMaster.getFlex7()!=null && (!(refMaster.getFlex7().toString().trim().equals("")))  ){
					Flex7=refMaster.getFlex7();
				}
				parameterMap.put(refMaster.getLabel3(),Flex3+"~"+Flex7 );	
			}
			
			if(refMaster.getLabel4()!=null && (!(refMaster.getLabel4().toString().trim().equals("")))){
				String Flex4="NoData";
				String Flex8="NoData";
				if(refMaster.getflex4()!=null && (!(refMaster.getflex4().toString().trim().equals("")))  ){
					Flex4=refMaster.getflex4();
				}
				if(refMaster.getFlex8()!=null && (!(refMaster.getFlex8().toString().trim().equals("")))  ){
					Flex8=refMaster.getFlex8();
				}
				parameterMap.put(refMaster.getLabel4(),Flex4+"~"+Flex8 );	
			}
			parameterMapData.put(refMaster.getCode(), parameterMap);
		}
	}catch(Exception e){
		logger.error("Error executing query "+ e.getStackTrace()[0]);	
	}
	return parameterMapData;
}

public Map<String, String> findByParameterforVat(String corpId, String parameter) {
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	try { 
		String query ="select code,description from  refmaster where  parameter= '" + parameter+ "' and corpID='" + corpId+ "' and language='en' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description";
		List list = this.getSession().createSQLQuery(query).list();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String description = "";
			 
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				description = row[1].toString();
			}  
			parameterMap.put(code, description);
			
		} 
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	 
	return parameterMap;
}
public Map<String, String> findByParameterWithCurrency(String corpID, String parameter) { 
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,description from refmaster where  parameter= '"+ parameter + "' and corpID= '"+corpID + "'  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("description", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String description = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				description = row[1].toString();
			}

			parameterMap.put(code, description);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}
public Map<String, Set<String>> getMandatoryFileMap(String sessionCorpID,String parameter){
	Map<String, Set<String>>  parameterMap = new LinkedHashMap<String, Set<String>>();
	Set<String> temp=null;
	String query ="select code,flex3 from  refmaster where  parameter= '" + parameter+ "' and corpID='" + sessionCorpID+ "' and language='en' and flex3 is not null AND flex3!='' and status='Active' ORDER BY code";
	List list = this.getSession().createSQLQuery(query).list();
	Iterator iterator = list.iterator();
	String code = "";
	String description = "";	
	while (iterator.hasNext()) {
		Object[] row = (Object[]) iterator.next();
		if (row[0] != null) {
			code = row[0].toString();
		}	
		if (row[1] != null) {
			description = row[1].toString();
		}
		for(String ss:description.split(",")){
			if(!parameterMap.isEmpty() && parameterMap.containsKey(ss)){
				temp=parameterMap.get(ss);
				temp.add(code);
				parameterMap.put(ss, temp);
			}else{
				temp=new HashSet<String>();
				temp.add(code);
				parameterMap.put(ss, temp);
			}
		}
	}
	return parameterMap;
}
public Map<String, String> findDefaultStateIsActiveList(String bucket2, String corpId){
	try {
		if (bucket2 == null || bucket2.equals("")) {
			bucket2 = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		String country = "";
		if (bucket2.equalsIgnoreCase("United States")) {
			country = "USA";
		} else if (bucket2.equalsIgnoreCase("India")) {
			country = "IND";
		} else if (bucket2.equalsIgnoreCase("Canada")) {
			country = "CAN";
		} else {
			country = bucket2;
		}
		List list = getSession()
				.createSQLQuery(
						"select code,description,status "
								+ "from refmaster "
								+ "where bucket2='"+country+"' and parameter ='STATE' order by case when sequenceNo is not null then sequenceNo else 99999 end,description").list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			Object[] row = (Object[]) it.next();
			parameterMap.put(((String) row[0]).toUpperCase()+"~"+row[2],
					((String) row[1]).toUpperCase());
		}
		return parameterMap;
	} catch (Exception e) {
    	 e.printStackTrace();
	}
	return null;
}

public Map<String, String> findByParameterOnlyDescription(String sessionCorpID, String parameter) {  
	List<RefMaster> list;
	Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
	try {
		
			list = getHibernateTemplate().getSessionFactory()
					.getCurrentSession().createCriteria(RefMaster.class)
					.add(Restrictions.eq("parameter", parameter))
					.addOrder(Order.asc("description"))
					.add(Restrictions.eq("language", "en"))
					.list(); 
		
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getDescription(),
						refMaster.getDescription());
			}
		
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return parameterMap; 
}

public List findByParameterFlex1(String sessionCorpID, String parameter) { 
		try {
			return getHibernateTemplate().find(
					"select description from RefMaster where  parameter= '"
							+ parameter + "' and (language='en' or language='' or language is null ) and flex1='Approved' ORDER BY flex3 ");
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;


}

public Map<String, String> findEuvateDescWithFlex(String sessionCorpID,	String parameter){
	Map<String, String> parameterMap = new LinkedHashMap<String, String>();
	//description,label2,label3,label4,flex1,flex2,flex3,flex4 
	List<RefMaster> list;
	try {
	list = getHibernateTemplate().find("from RefMaster where  parameter='"+parameter+"' and corpID= '"+sessionCorpID + "' ");
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(),refMaster.getDescription()+"~"+refMaster.getLabel2()+"~"+refMaster.getLabel3()+"~"+refMaster.getLabel4()+"~"+refMaster.getflex1()+"~"+refMaster.getflex2()+"~"+refMaster.getflex3()+"~"+refMaster.getflex4());
		}
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	
	
	return parameterMap;
}

public Map<String, String> findByParentParameterValues(String sessionCorpID,	String parameter){
	try {
		List list;
		list = getSession()
				.createSQLQuery(
						"select code,description from refmaster where  parameter= '"
								+ parameter + "' and corpID in ('TSFT','"
								+ hibernateUtil.getParentCorpID(sessionCorpID) + "','"
								+ sessionCorpID + "') and code<>''  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description")
				.addScalar("code", Hibernate.STRING)
				.addScalar("description", Hibernate.STRING).list();
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			String code = "";
			String description = "";
			Object[] row = (Object[]) iterator.next();
			if (row[0] != null) {
				code = row[0].toString();
			}
			if (row[1] != null) {
				description = row[1].toString();
			}

			parameterMap.put(code, description);
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public String getValueFromParameterControl(String sessionCorpID,String parameter) {
	//getHibernateTemplate().setMaxResults(100);
	String str="";
	String query="select if(childParameter=null,'',childParameter) from parametercontrol where parameter= '"+ parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID) + "','"+ sessionCorpID + "')";
	List al=this.getSession().createSQLQuery(query).list();
	if((al!=null)&&(!al.isEmpty())&&(al.get(0)!=null)&&(!al.get(0).toString().trim().equalsIgnoreCase(""))){
		str=al.get(0).toString().trim();
	}
	return str;
}

public List getParentParameterList(String sessionCorpID,String parameter,String instctionCode){
	try{
		return getHibernateTemplate().find("select childParameterValues from RefMaster where code='"+instctionCode+"' and parameter= '"+ parameter + "' and corpID ='"+sessionCorpID+"' and childParameterValues is not null and childParameterValues<>'' and status='Active'  ");
	}
	catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
	
}

public List findOnlyCodeByOpenperiod(String parameter, String corpId) {
	List codeList = new ArrayList();
	try {
		return getHibernateTemplate().find(
				"select code from RefMaster where parameter='" + parameter
						+ "' and (language='en' or language='' or language is null ) and status='Active'  order by case when sequenceNo is not null then sequenceNo else 99999 end,code");
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return codeList;
}


public List getParentParameterListAjax(String sessionCorpID,String parameter,String serviceCode,String instructionCode){
	String a1="";
	String childParameterValues="";
	String code="";
	try{
	a1="select childParameterValues from refmaster  where code='"+serviceCode+"' and parameter= '"+ parameter + "' and corpID ='"+sessionCorpID+"' and childParameterValues is not null and childParameterValues<>'' and status='Active' ";
    List al=this.getSession().createSQLQuery(a1).list();
	if(al!=null && (!(al.isEmpty())) && al.get(0)!=null && (!(al.get(0).toString().trim().equals("")))){
		childParameterValues=al.get(0).toString().trim();
	}
	String countparam[]=childParameterValues.split(",");
	for(int i=0;i<countparam.length;i++){
		if(code.equals("")){
			code = "'"+countparam[i].toString()+"'";
			//.replaceAll("\\s","")
		}
		else{
			code = code+","+"'"+ltrim(countparam[i].toString())+"'";
		}
	}
	if(code.equals("")){
		code="'"+code+"'";
	}
	a1="select CONCAT(code,'#',replace(replace(description,',',';'),'@','~')) from refmaster where (language='en' or language='' or language is null) and code in("+code+")  and corpID ='"+sessionCorpID+"' and parameter= 'WOINSTR' and status='Active' ORDER BY parameter,if(sequenceNo is not null,sequenceNo,99999),description";
	List  list = this.getSession().createSQLQuery(a1).list();
	if(list!=null && !list.isEmpty() && list.size()>0){
	List  list1 = this.getSession().createSQLQuery("select CONCAT(code,'#',replace(replace(description,',',';'),'@','~')) from refmaster where (language='en' or language='' or language is null) and corpID  ='"+sessionCorpID+"'and code='"+instructionCode+"' and parameter='WOINSTR' ORDER BY if(sequenceNo is not null,sequenceNo,99999),description").list();	
	list.addAll(list1);
		return list;
	}
	else{
		String a2="select CONCAT(code,'#',replace(replace(description,',',';'),'@','~')) from refmaster where (language='en' or language='' or language is null) and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') and parameter= 'WOINSTR' and status='Active' ORDER BY parameter,if(sequenceNo is not null,sequenceNo,99999),description";
		List  list1 = this.getSession().createSQLQuery(a2).list();
		List  list2 = this.getSession().createSQLQuery("(select CONCAT(code,'#',replace(replace(description,',',';'),'@','~')), description "+
		        "from refmaster where (language='en' or language='' or language is null) and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') and code='"+instructionCode+"' and parameter='WOINSTR' "+
		        "ORDER BY description)").list();
		list1.addAll(list2);
		return list1;  
	}
	
	
	}
	catch(Exception e){
		String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
  	  	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		e.printStackTrace();
	}
	return null;  
}

public static String ltrim(String s) {
    int i = 0;
    while (i < s.length() && Character.isWhitespace(s.charAt(i))) {
        i++;
    }
    return s.substring(i);
}

public String findBucket2Value(String code, String parameter) {
	 try {
		String Bucket2 = "";
		List Bucket2List = new ArrayList();
		Bucket2List = getHibernateTemplate().find("select bucket2  from RefMaster where code='" +code+ "' and parameter='" +parameter+ "' ");
		if (Bucket2List != null && (!(Bucket2List.isEmpty()))
				&& Bucket2List.get(0) != null) {
			Bucket2 = Bucket2List.get(0).toString();
		}
		return Bucket2;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public Map<String, String> getCodeWithDecription(String sessionCorpID){
	List list;
	 list = getSession().createSQLQuery("select concat(code,'~',parameter) as code, description from refmaster where corpid='" +sessionCorpID+ "' and  parameter in ('PAYTYPE','CRTERMS','VATBILLINGGROUP') order by parameter")
	 .addScalar("code", Hibernate.STRING)
	 .addScalar("description", Hibernate.STRING)
	 .list(); 
	 Map<String, String> parameterMap = new LinkedHashMap<String, String>(); 
    Iterator iterator =list.iterator();
	  while(iterator.hasNext())
	  {
		  String code="";
		  String description="";
		  Object [] row=(Object[])iterator.next();
		  if(row[0]!=null) {
			  code=row[0].toString();  
		  }
		  if(row[1]!=null){
			  description = row[1].toString();  
		  }
		 
			parameterMap.put(code, description);
	 }
	
	return parameterMap;
	
}

public Map<String, String> findByParameterFromServiceorder(String corpId, String parameter, String job) {
	try {
		//String[] params = { corpId, parameter };
		List<RefMaster> list = new ArrayList<RefMaster>();
		list = getHibernateTemplate().find(
				"from RefMaster where  parameter= '" + parameter+ "' and flex1 ='"+job+"' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end,description");
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		for (RefMaster refMaster : list) {
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
		}
		return parameterMap;
	} catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
	return null;
}

public List findMaxIdByParameter(String parameter,String sessionCorpID){
	List idList = new ArrayList();
	String query = "select max(id) from refmaster where parameter='"+parameter+"' and corpid in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "')";
	idList = getSession().createSQLQuery(query).list();
	return idList;
}

	public List findFlex5ByDescription(String countryName,String sessionCorpID,String parameterName){
		try {
			List list;
			list = getSession()
					.createSQLQuery(
							"select distinct flex5 from refmaster where  parameter='"+parameterName+"' and description='"
									+ countryName+ "' and corpid in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') and language='en'")
					.list();
			return list;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List getDescriptionBylanguage(String parameter,String language,String sessionCorpID) {  
		try {
			List descriptionList = new ArrayList();
			String query="select description from refmaster where  parameter= '"+ parameter + "' and language= '"+ language + "' and corpid in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') ";
			descriptionList = getSession().createSQLQuery(query).list();
			if(descriptionList!=null && !descriptionList.isEmpty() && descriptionList.get(0)!=null){
				return descriptionList;
			}else{
				descriptionList = getSession().createSQLQuery(
				"select description from refmaster where  parameter= '"+ parameter + "' and corpid in ('TSFT','"+ hibernateUtil.getParentCorpID(sessionCorpID)+ "','"+ sessionCorpID+ "') ").list();
				return descriptionList;
			}
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}
	
	public List getServiceTypeListByWarehouse(String corpId, String parameter, String wareHouse )
	{
			List typeServiceList = new ArrayList();
			typeServiceList = getHibernateTemplate().find(
					  "select code from RefMaster where  parameter= '" + parameter	+ "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"	+ corpId + "')  "
					  + " and bucket='Y' and serviceWarehouse like ('%"+wareHouse+"%') order by 	code ");
			return typeServiceList;
	}
	
	public List getTaskPlannerListAjax(String parameter,String extTaskCode){
		String childParameterValues="";
		String code="";
		try{
			String a1="select concat(code,'~',description) from refmaster where (language='en' or language='' or language is null) and parameter= '"+parameter+"' and status='Active' and flex1='"+extTaskCode+"' ORDER BY parameter,if(sequenceNo is not null,sequenceNo,99999),description";
		System.out.println("Query "+a1);
		List  list = this.getSession().createSQLQuery(a1).list();
		if(list!=null && !list.isEmpty() && list.size()>0){
			return list;
		}
		}
		catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;  
	}
	public List getAssigneeListAjax(String parameter,String assignee){
		String childParameterValues="";
		String code="";
		try{
			String a1="select concat(flex5,'~',description) from refmaster where (language='en' or language='' or language is null) and parameter= '"+parameter+"' and status='Active' and flex1='"+assignee+"' ORDER BY description";
		System.out.println("Query "+a1);
		List  list = this.getSession().createSQLQuery(a1).list();
		if(list!=null && !list.isEmpty() && list.size()>0){
			return list;
		}
		}
		catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;  
	}
	
	public String getCompanyUUID(String code,String parameter){
		String uuidCode="";
		try{
			String a1="select code from refmaster where (language='en' or language='' or language is null) and parameter= '"+parameter+"' and status='Active' and bucket='"+code+"' ORDER BY null";
		System.out.println("Query "+a1);
		List  list = this.getSession().createSQLQuery(a1).list();
		if(list!=null && !list.isEmpty() && list.size()>0){
			uuidCode=list.get(0).toString();
			return uuidCode;
		}
		}
		catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;
	}
	public String findDescriptionByFlex5(String code,String parameter){
		String desc="";
		try{
			String a1="select description from refmaster where (language='en' or language='' or language is null) and parameter= '"+parameter+"' and status='Active' and flex5='"+code+"' ORDER BY description";
			System.out.println("Query fro assignee desc "+a1);
		    List  list = this.getSession().createSQLQuery(a1).list();
			if(list!=null && !list.isEmpty() && list.size()>0){
				desc=list.get(0).toString();
				return desc;
			}
		}
		catch(Exception e){
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	  	  	String logMethod =   e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;
	}

	public Map findCodeOnleByParameterCountry(String corpID, String parameter) { 

		try {
			//String[] params = { corpId, parameter };
			
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			System.out.println("select code  from refmaster  where  parameter= '" + parameter + "' and (corpID in (select corpID  from company where allowAgentInvoiceUpload=true) or corpID ='TSFT')  order by code");
			if(corpID.equals("TSFT"))
			{
			List list = getSession()
					.createSQLQuery("select code  from refmaster  where  parameter= '" + parameter + "' and (corpID in (select corpID  from company where allowAgentInvoiceUpload=true) or corpID ='TSFT')  order by code")
					.addScalar("code", Hibernate.STRING).list();
			if ((list != null) && (!list.isEmpty()) && (list.get(0) != null)
					&& (!list.get(0).equals(""))) {
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object row = (Object) it.next();
					parameterMap.put(row.toString(), row.toString());
				}
			}}
			else
			{
				List list = getSession()
				.createSQLQuery("select code  from refmaster  where  parameter= '" + parameter + "' and corpID in('"+ corpID +"','TSFT','"+hibernateUtil.getParentCorpID(corpID)+"') order by code")
				.addScalar("code", Hibernate.STRING).list();
		if ((list != null) && (!list.isEmpty()) && (list.get(0) != null)
				&& (!list.get(0).equals(""))) {
			Iterator it = list.iterator();
			while (it.hasNext()) {
				Object row = (Object) it.next();
				parameterMap.put(row.toString(), row.toString());
			}
		}	
			}
			return parameterMap;
		} catch (Exception e) {
	    	 e.printStackTrace();
		}
		return null;

}

	public String findPartnerVatBillingFlexCode(String corpId, String vatdescription) {
		// TODO Auto-generated method stub
		try {
			String vatCode = "";
				List vatCodeList = new ArrayList();
				String query = "select group_CONCAT(flex1,',',flex2) from refmaster where (replace(replace(replace(replace(description,'\r\n',''),'\r',''),'\t',''),'\"',''))='"+vatdescription+"' AND  corpID='"+corpId +"' AND parameter='VATBILLINGGROUP' ";
				vatCodeList = this.getSession().createSQLQuery(query).list();
				if (vatCodeList != null && (!(vatCodeList.isEmpty()))) {
					vatCode = vatCodeList.get(0).toString();
				}
				if(vatCode.lastIndexOf(",") == vatCode.length() - 1)
				{
					vatCode = vatCode.substring(0, vatCode.length() - 1);
				}
				return vatCode;
			} catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
			return null;
	}


	public List findPartnerVatCodeDescription(String flexcode, String defaultVatDec, String corpId,String flag) {
		// TODO Auto-generated method stub
		try {
			if(flag.equals("Y")){
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String query="";
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
		
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				 query = "select CONCAT(code,'#',description) from refmaster where code in ('"+VatCode+ "','"+ defaultVatDec.replaceAll("~Active", "") + "') and parameter='EUVAT' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				}
				else{
					 query = "select CONCAT(code,'#',description) from refmaster where code in ('"+VatCode+ "') and parameter='EUVAT' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				}
			List list = this.getSession().createSQLQuery(query).list();
			return list;
			}
			
			else{
				String query = "select CONCAT(code,'#',description) from refmaster where  parameter='EUVAT' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				List list = this.getSession().createSQLQuery(query).list();
				return list;
				
			}
		

		} catch (Exception e) {
			String logMessage = "Exception:" + e.toString() + " at #" + e.getStackTrace()[0].toString();
			String logMethod = e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;
	}

	public String findPartnerVatBillingCode(String description, String corpId,String compDivis) {
		// TODO Auto-generated method stub
				try {
				String vatCode = "";
					List vatCodeList = new ArrayList();
					String query = "select flex1 from refmaster where description='"+description+"' and  parameter='VATBILLINGGROUP'  AND (bucket is null or bucket='' or bucket='"+compDivis+"')  AND corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') ";
					vatCodeList = this.getSession().createSQLQuery(query).list();
					if (vatCodeList != null && (!(vatCodeList.isEmpty()))
							&& vatCodeList.get(0) != null) {
						vatCode = vatCodeList.get(0).toString();
					}
					return vatCode;
				} catch (Exception e) {
					logger.error("Error executing query "+ e.getStackTrace()[0]);
				}
				return null;
	}

	public Map<String, String> findVatDescriptionList(String corpID, String parameter, String flexcode,
			String defaultVatDec,String partnerVatDec,String flagb) {
		// TODO Auto-generated method stub
		try {
			String query=""; 
			if(flexcode != null && (!(flexcode.trim().equals("")))){
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			

				query = "from RefMaster  where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "','"+ defaultVatDec + "','"+ partnerVatDec + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
			}else{
				query = "from RefMaster  where  parameter= '"+ parameter + "'  and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
			}
				
			 
			 
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim()+"~"+refMaster.getStatus().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}


	public List findAccountLinePayVatCodeDescription(String flexcode, String defaultVatDec, String corpId,String flag) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		try {

			if (flexcode !=null &&  (!(flexcode.equals("")))) {
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String query="";
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
		
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				query = "select CONCAT(code,'#',description) from refmaster where code in ('"+VatCode+ "','"+ defaultVatDec + "') and parameter='PAYVATDESC' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				}
				else{
					query = "select CONCAT(code,'#',description) from refmaster where code in ('"+VatCode+ "') and parameter='PAYVATDESC' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				}
			
			List list = this.getSession().createSQLQuery(query).list();
			return list;
    }
    else
    {
    	String query = "select CONCAT(code,'#',description) from refmaster where  parameter='PAYVATDESC' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
		List list = this.getSession().createSQLQuery(query).list();
		return list;
    }
		

		} catch (Exception e) {
			String logMessage = "Exception:" + e.toString() + " at #" + e.getStackTrace()[0].toString();
			String logMethod = e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;
	
	}

	public String findAccountLinePayVatBillingCode(String description, String corpId,String compDivis) {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		try {
		String vatCode = "";
			List vatCodeList = new ArrayList();
			String query = "select flex2 from refmaster where description='"+description+"' and  parameter='VATBILLINGGROUP'  AND (bucket is null or bucket='' or bucket='"+compDivis+"')  AND corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') ";
			vatCodeList = this.getSession().createSQLQuery(query).list();
			if (vatCodeList != null && (!(vatCodeList.isEmpty()))
					&& vatCodeList.get(0) != null) {
				vatCode = vatCodeList.get(0).toString();
			}
			return vatCode;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public List findAccountLineRecVatCodeDescription(String flexcode, String defaultVatDec,String partnerVatDec, String corpId,
			String flag) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		try {
			if (flexcode !=null &&  (!(flexcode.equals("")))) {
				String str = flexcode;
				String res[] = str.split(",");
				int i;
				String query = "";
				String newArray[] = new String[res.length];
				for (i = 0; i < res.length; i++) {
					newArray[i] = "'" + res[i] + "'";
				}
				String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
				if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
					VatCode = VatCode.substring(1, VatCode.length() - 1);
				}

				if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
					VatCode = VatCode.substring(0, VatCode.length() - 1);

				}
				if (defaultVatDec!=null && !defaultVatDec.equals("")) {
					query = "select CONCAT(code,'#',description) from refmaster where code in ('" + VatCode + "','"+defaultVatDec + "','"+partnerVatDec + "') and parameter='EUVAT' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				} else {
					query = "select CONCAT(code,'#',description) from refmaster where code in ('" + VatCode+ "','"+partnerVatDec + "') and parameter='EUVAT' and  corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				}
				List list = this.getSession().createSQLQuery(query).list();
				return list;
			} else {

				String query = "select CONCAT(code,'#',description) from refmaster where  parameter='EUVAT' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpId) + "','"+ corpId + "') order by description";
				List list = this.getSession().createSQLQuery(query).list();
				return list;
			}

		} catch (Exception e) {
			String logMessage = "Exception:" + e.toString() + " at #" + e.getStackTrace()[0].toString();
			String logMethod = e.getStackTrace()[0].getMethodName().toString();
			e.printStackTrace();
		}
		return null;

	}

	public Map<String, String> findPayVatDescriptionList(String corpID, String parameter, String flexcode,
			String defaultVatDec,String flag) {
		// TODO Auto-generated method stub
		try {
			String query="";
			if (flexcode !=null &&  (!(flexcode.equals("")))) {
			String str = flexcode;
			String res[] = str.split(",");
			int i;
		
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				query = "from RefMaster  where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "','"+ defaultVatDec + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
				}
				else{
					query = "from RefMaster where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
					
				}
			}else{
				query = "from RefMaster where  parameter='EUVAT' and corpID='"+ corpID + "' order by description";
			
			}
			 
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim()+"~"+refMaster.getStatus().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
			
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findPartnerVatDescriptionList(String corpID, String parameter, String flexcode, String defaultVatDec, String flagb) {
		try {
			String query="";
			if(flagb.equals("Y")){
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			
	
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				query = "from RefMaster where  parameter= '"+parameter+"' and code in ('"+VatCode+ "','"+ defaultVatDec.replaceAll("~Active", "") + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
				}
				else{
					query = "from RefMaster where  parameter= '"+parameter + "' and code in ('"+VatCode+ "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
					
				}
			}else{
				query = "from RefMaster where  parameter='"+parameter+"' and corpID='"+ corpID + "'  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
			
			}
			
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim()+"~"+refMaster.getStatus().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public String findPartnerVatBillingDescription(String corpId, String description) {
		// TODO Auto-generated method stub
		try {
			String vatCode = "";
				List vatCodeList = new ArrayList();
				String query = "select CONCAT(flex1,'#',flex1) from refmaster where  description ='"+description+"' AND  corpID='"+corpId +"' AND parameter='VATBILLINGGROUP' ";
				vatCodeList = this.getSession().createSQLQuery(query).list();
				if (vatCodeList != null && (!(vatCodeList.isEmpty()))
						&& vatCodeList.get(0) != null) {
					vatCode = vatCodeList.get(0).toString().replaceAll("#", ",");
				}
				vatCode=vatCode.replaceAll("[", "").replaceAll("]", "");
				if(vatCode.lastIndexOf(",") == vatCode.length() - 1)
				{
					vatCode = vatCode.substring(0, vatCode.length() - 1);
				}
				return vatCode;
			} catch (Exception e) {
				logger.error("Error executing query "+ e.getStackTrace()[0]);
			}
			return null;
	}

	public Map<String, String> findPartnerVatDescription(String corpID, String parameter, String flexcode,
			String defaultVatDec, String flagb) {
		// TODO Auto-generated method stub

		try {
			String query="";
			if(flagb.equals("Y")){
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if(VatCode.lastIndexOf(",") == VatCode.length() - 1)
			{
				VatCode = VatCode.substring(0,VatCode.length() - 1);
			}
	
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				query = "from RefMaster where  parameter= '"+parameter+"' and code in ('"+VatCode+ "','"+ defaultVatDec.replaceAll("~Active", "") + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
				}
				else{
					query = "from RefMaster where  parameter= '"+parameter + "' and code in ('"+VatCode+ "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
					
				}
			}else{
				query = "from RefMaster where  parameter='"+parameter+"' and corpID='"+ corpID + "'  ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
			
			}
			
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	
	}

	public Map<String, String> findVatDescriptionListForPricing(String corpID, String parameter, String flexcode, String defaultVatDec, String partnerVatDec, String flagb) {
		// TODO Auto-generated method stub
		try {
			String query=""; 
			if(flexcode != null && (!(flexcode.trim().equals("")))){
			String str = flexcode;
			String res[] = str.split(",");
			int i;
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			

				query = "from RefMaster  where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "','"+ defaultVatDec + "','"+ partnerVatDec + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
			}else{
				query = "from RefMaster  where  parameter= '"+ parameter + "'  and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
			}
				
			 
			 
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findPayVatDescription(String corpID, String parameter, String flexcode,
			String defaultVatDec,String flag) {
		// TODO Auto-generated method stub
		try {
			String query="";
			if (flexcode !=null &&  (!(flexcode.equals("")))) {
			String str = flexcode;
			String res[] = str.split(",");
			int i;
		
			String newArray[] = new String[res.length];
			for (i = 0; i < res.length; i++) {
				newArray[i] = "'" + res[i] + "'";
			}
			String VatCode = Arrays.toString(newArray).replaceAll("[\\[\\](){}]", "").trim();
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(1, VatCode.length() - 1);
			}
			if (VatCode.lastIndexOf(",") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if (VatCode.lastIndexOf("'") == VatCode.length() - 1) {
				VatCode = VatCode.substring(0, VatCode.length() - 1);

			}
			if(defaultVatDec!=null && !defaultVatDec.equals("")){
				query = "from RefMaster  where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "','"+ defaultVatDec + "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
				
				}
				else{
					query = "from RefMaster where  parameter= '"+ parameter + "' and code in ('"+VatCode+ "') and corpID='"+ corpID + "' ORDER BY case when sequenceNo is not null then sequenceNo else 99999 end, description";
					
				}
			}else{
				query = "from RefMaster where  parameter='EUVAT' and corpID='"+ corpID + "' order by description";
			
			}
			 
			List<RefMaster> list = getHibernateTemplate().find(query);
			Map<String, String> parameterMap = new LinkedHashMap<String, String>();
			for (RefMaster refMaster : list) {
				parameterMap.put(refMaster.getCode().trim(),refMaster.getDescription().trim());
			}
			return parameterMap;
			
		} catch (Exception e) {
			logger.error("Error executing query "+ e.getStackTrace()[0]);
		}
		return null;
	}

	public Map<String, String> findPartnerRecVatDescriptionList(String corpID, String parameter) {
		String[] params = { corpID, parameter }; 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try{ 
			String query="select description,group_CONCAT(flex1,',',flex2) from refmaster where parameter= '" + parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpID)+ "','"+ corpID+ "')  GROUP BY description";
			List list = getSession().createSQLQuery(query).list(); 
			if ((list != null) && (!list.isEmpty()) && (list.get(0) != null)&& (!list.get(0).equals(""))) {
				Iterator it = list.iterator();
				while (it.hasNext()) {
					Object[] row = (Object[]) it.next();
					parameterMap.put(row[0].toString().trim(), row[1].toString());				
					}
			}

		return parameterMap;
		}
	 catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
		return null;
	}

	public Map<String, String> findRecVatDescriptionData(String corpID, String parameter) {
		String[] params = { corpID, parameter };
		List<RefMaster> list=new ArrayList(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try{ 
	   list = getHibernateTemplate().find("from RefMaster where parameter= '" + parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpID)+ "','"+ corpID+ "') and flex1 <> '' and flex1 is not null ");
			 for (RefMaster refMaster : list) {
				 if(refMaster.getBucket()!=null && !(refMaster.getBucket().trim().equals(""))){
				parameterMap.put(refMaster.getDescription().trim()+"~"+refMaster.getBucket(),refMaster.getflex1().trim());
				 }else{
					 parameterMap.put(refMaster.getDescription().trim()+"~NODATA",refMaster.getflex1().trim()); 
				 }
			}
		  
		return parameterMap;
		}
	 catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
		return null;
	}

	public Map<String, String> findPayVatDescriptionData(String corpID, String parameter) {
		String[] params = { corpID, parameter };
		List<RefMaster> list=new ArrayList(); 
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		try{ 
			   list = getHibernateTemplate().find("from RefMaster where parameter= '" + parameter + "' and corpID in ('TSFT','"+ hibernateUtil.getParentCorpID(corpID)+ "','"+ corpID+ "') and flex2 <> '' and flex2 is not null ");
				 for (RefMaster refMaster : list) {
					 if(refMaster.getBucket()!=null && !(refMaster.getBucket().trim().equals(""))){
					parameterMap.put(refMaster.getDescription().trim()+"~"+refMaster.getBucket(),refMaster.getflex2().trim());
					 }else{
						 parameterMap.put(refMaster.getDescription().trim()+"~NODATA",refMaster.getflex2().trim()); 
					 }
				}
			  
			return parameterMap;
			}
	 catch (Exception e) {
		logger.error("Error executing query "+ e.getStackTrace()[0]);
	}
		return null;
	}



}

