<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>

<head>
<title>Dispatch Map</title>
<meta name="heading" content="Dispatch Map" />
<meta name="menu" content="UserIDHelp" />
<c:if test="${param.popup}">
	<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" />
</c:if>
<link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" />
<style>

#dmdsurvey {
	position: absolute;
	visibility: hidden;
	width: auto;
	min-width:150px;
	height: auto;	
	-moz-border-radius: 5px;
	-webkit-border-radius:5px;
	border-radius: 5px;
	background-color: #DFDFDF;
	border: 2px solid #3A8AB3;
	padding: 2px;
	color:#003366;
	font-family:arial,verdana;
	font-size:11px;
	font-weight:normal;
	text-decoration:none;
	z-index:900;
	top:160px;
	left:155px;
}


#close a{
	background:transparent url(images/close.gif) repeat scroll 0 0;
	border:0 none;
	display:block;
	float:right;
	font-size:0;
	height:15px;
	line-height:0;
	position:relative;
	text-decoration:none;
	width:15px;
	z-index:902;
}

#content {
	color:#003366;
	font-family:arial,verdana;
	font-size:11px;
	font-weight:normal;
	text-decoration:none;
	z-index:901;
}

.movebutton {
	-moz-border-radius-bottomleft:2px;
	-moz-border-radius-bottomright:2px;
	-moz-border-radius-topleft:2px;
	-moz-border-radius-topright:2px;
	background-color:#C6DEFF;
	background-image:url(images/buttonblue.png);
	border:1px solid #5279AD;
	color:#3A8AB0;
	display:block;
	float:right;
	font-family:arial;
	font-size:12px;
	font-weight:bold;
	margin:0 3px 0 0;
	padding:2px 10px;
}

#content-containertable.override{
	width:190px;
}

#content-containertable.override div{
	height:460px;
	overflow:scroll;
}

#content-containertable.override1{
	width:190px;
}

#content-containertable.override1 div{
	height:150px;
	overflow:scroll;
}

.listwhitetext-head {
	color:#FFFFFF;
	font-family:arial,verdana;
	font-size:11px;
	font-weight:normal;
	text-decoration:none;
}

#mainPopup {
	padding-left:5px;
	padding-right:5px;
	padding-top:5px;
	margin-top:-20px;
}

.curved {
	 border:1px dotted #219DD1;
	-moz-border-radius:8px;
	-webkit-border-radius:8px;
	behavior:url(border-radius.htc);
}
#priceinfotab_bg{background:#ffffff url(images/price_tab_bg.png) repeat-x;height:26px; !width:70%; width:90%; color: #3A8AB3;font:13px Arial, Helvetica, sans-serif ; font-weight:bold; cursor: pointer;padding:0px; border:1px solid #5FA0C0; border-left:none;}
.inner-tab{background:#ffffff url(images/basic-inner.png) repeat-x; height:20px;}
.inner-tab a, a:link a:active {
	color:#FFFFFF;text-decoration:none;font-weight:bold;font-family:arial,verdana; font-size:11px;
}
.price_tleft{float:left;padding:3px 2px 0px 8px;!margin-top:0px;}

.filter-head{color: #484848;font:11px arial,verdana ; font-weight:bold; cursor: pointer; }
.price-bg{background-image:url(images/ng-bg.png); background-repeat:repeat-x;margin:0px;padding:0px;}
.filter-bg{background-image:url(images/border-filter.png); background-repeat:no-repeat; text-align:left;margin:3px; height:77px; width:852px;}
</style>
</head>
<%--
<script src="http://maps.google.com/maps?file=api&v=2&key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=true&visible=100&amp&async=2;key=ABQIAAAAMnZRZZq8wqpIPnC_GJ7aghSdfGQtIvOX32cISAq4FBtKgtGobBTVOU5DiiQkpzAtYj-X1z5vIrlY3w" type="text/javascript"></script>
--%>
<script type="text/javascript" src="<c:url value='/scripts/dojo/dojo/dojo.js'/>" djConfig="parseOnLoad:true, isDebug:false"></script>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script language="javascript" type="text/javascript"><!--
function initLoader() { 
	 var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = "http://maps.google.com/maps?file=api&v=2&key="+googlekey+"&async=2&callback=loadGoogleGeoCode";
    //alert(script.src);
    document.body.appendChild(script);
    setTimeout('',6000);
    }
var geocoder;
var map;
function loadGoogleGeoCode() {
	map = new GMap2(document.getElementById("map"));
	geocoder = new GClientGeocoder();
	getDispatchMapPoints();		
	//setTimeout('',6000);
}

var originAdd = "";
var destinationAdd = "";
var bDate = "";
var eDate = "";
var estWeight = "";
var wService = "";
var wCustName = "";
var wTicket = "";

var dispatchDataMap = new Object();

function getDispatchMapPoints(){
dojo.xhrPost({
	       form: "dispatchMapForm" ,
	      // url:"dispathMapWorkTicketData.html",
	       timeout: 900000, // give up after 3 seconds
	       handleAs:"json",
	       load: function(jsonData){	        	
	        var d=document.getElementById("dispatchMapList");
	        d.innerHTML='';
	        var oldAddress="";
  			if(jsonData.count>=0)
  			{
	for(i=0; i < jsonData.dispathMapPoints.length; i++){
	var originAdd = jsonData.dispathMapPoints[i].address;
	dispatchDataMap[originAdd] = jsonData.dispathMapPoints[i];
						d.innerHTML+='<table width="" border="0" cellspacing="2" cellpadding="1" bgcolor="#E1EBEF" style="margin:0px; padding:0px;">'+
								'<tr><td width="100%" height="0" colspan="2" class="inner-tab" class="listwhitetext"><table style="margin:0px;padding:0px;"><tr><td valign="top" style="color:#FFFFFF;font-weight:bold;"></td><td><a style="text-decoration:underline; cursor:pointer;" onclick = javascript:getDispatchMapPointsOri("'+encodeURI(originAdd)+'")> T/K# : '+jsonData.dispathMapPoints[i].wTicket+' - '+jsonData.dispathMapPoints[i].wCustName+'-'+jsonData.dispathMapPoints[i].wService+'</a></td></tr></table></td></tr>'+
	           					'<tr><td valign="top" class="listwhitetext" width="200px" >'+originAdd.substring(0, originAdd.lastIndexOf(','))+'</td></tr>'+
	           					'<tr><td valign="top" class="listwhitetext" width="200px" > Work Day: '+jsonData.dispathMapPoints[i].bDate+ '-'+jsonData.dispathMapPoints[i].eDate+'</td></tr>'+
	           					'<tr><td valign="top" class="listwhitetext" width="200px" > Est. Wt.: '+jsonData.dispathMapPoints[i].eWeight+ ' Lbs. Service: '+jsonData.dispathMapPoints[i].wService+'</td></tr>'+
						  '</table>';
						  
			    console.log("originAddress: " + originAdd);
			    if(oldAddress.indexOf(originAdd)  < 0 ){
			    dispatchDataMap[originAdd] = jsonData.dispathMapPoints[i];
				geocoder.getLocations(originAdd, addToMapOrigin);
				}
				oldAddress= oldAddress+"~"+jsonData.dispathMapPoints[i].address;
				//setTimeout("testTime()",6000);	
			}
			}
		} 
		}); 
		}
function testTime(){}

function addToMapOrigin(response){          
	var fn
		if(!response.Placemark) return;
		place = response.Placemark[0];
		point = new GLatLng(place.Point.coordinates[1],place.Point.coordinates[0]);
		map.setCenter(point, 8);
		addBasicInfoToMapOrigin(point, dispatchDataMap[response.name]); 
		map.setUIToDefault();
		map.disableScrollWheelZoom();
		fn = markerClickFnPorts(point,dispatchDataMap[response.name]);
		GEvent.addListener(marker, "click", fn);
		
}
<%--
function addToMapDestin(response){
	var fn	
	if(!response.Placemark) return;
		place = response.Placemark[0];
		point = new GLatLng(place.Point.coordinates[1],place.Point.coordinates[0]);
		map.setCenter(point, 8);
		addBasicInfoToMapDestin(point, null); 
		map.setUIToDefault();
		map.disableScrollWheelZoom();
		fn = markerClickFnPorts(point,dispatchDataMap[response.name]);
		GEvent.addListener(marker, "click", fn);

}
--%>
String.prototype.trim = function() {
	return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
	return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
	return this.replace(/\s+$/,"");
}


function markerClickFnPorts(point, dispatchResults) {
		return function() {
		   if (!dispatchResults) return;
	       var address = dispatchResults.address;
	       var workDate = dispatchResults.eDate;
	       var weight = dispatchResults.eWeight;	       
	       var service = dispatchResults.wService;
	       var custName = dispatchResults.wCustName;
	       var ticket = dispatchResults.wTicket;
	       var infoHtml = '<div style="width:100%;"><h5><B>T/K #: </B>'+ticket+' - '+custName.ltrim()+'<Br>'+address+'<Br><B>Work Dates: </B>'+workDate+'<Br><B>Est. Wt.: </B>'+weight+' Lbs.  <B>Service: </B>'+service+'</h5></div>';
	       map.openInfoWindowHtml(point, infoHtml);
	   }
} 
  
function addBasicInfoToMapOrigin(point, partnerDetails){
	var greenIcon = new GIcon(G_DEFAULT_ICON);
	var iService=partnerDetails.wService;
	if(iService=='PK'|| iService=='LD'|| iService=='PL'){
	greenIcon.image = "${pageContext.request.contextPath}/images/markerO-green.png";
	}
	if(iService=='DL'|| iService=='DU'){
		greenIcon.image = "${pageContext.request.contextPath}/images/markerD-red.png";
	}
	markerOptions = { icon:greenIcon };
	marker = new GMarker(point, markerOptions); 
	map.addOverlay(marker, markerOptions);
	map.setCenter(point, 8);
	map.setUIToDefault();
}
<%--	  
function addBasicInfoToMapDestin(point, partnerDetails){
	var redIcon = new GIcon(G_DEFAULT_ICON);
	redIcon.image = "${pageContext.request.contextPath}/images/markerD-red.png";
	markerOptions = { icon:redIcon };
	marker = new GMarker(point, markerOptions); 
	map.addOverlay(marker, markerOptions);
	map.setCenter(point, 8);
	map.setUIToDefault();
}
--%>
function getDispatchMapPointsOri(oriAddress){
	var addORI = decodeURI(oriAddress);
	map = new GMap2(document.getElementById("map"));
	geocoder = new GClientGeocoder();
	map.clearOverlays(); 
	console.log("Origin---->: " + addORI);
	geocoder.getLocations(addORI, addToMapOrigin);		
} 
<%-- 
function getDispatchMapPointsDest(destAddress,dispatchData){
	var addDEST = decodeURI(destAddress);
	var dispatchDataDEST = decodeURI(dispatchData);
	
	map = new GMap2(document.getElementById("map"));
	geocoder = new GClientGeocoder();
	map.clearOverlays(); 
	console.log("Destination---->: " + addDEST);
	dispatchDataMap[addDEST] = dispatchDataDEST;
	geocoder.getLocations(addDEST, addToMapDestin);
}
--%>
function showOrHide(value) {
    if (value==0) {
       if (document.layers)
           document.layers["layerH"].visibility='hide';
        else
           document.getElementById("layerH").style.visibility='hidden';
   }
   else if (value==1) {
      if (document.layers)
          document.layers["layerH"].visibility='show';
       else
          document.getElementById("layerH").style.visibility='visible';
   }
}	

function userStatusCheck(target, target1){
	var ids = target1;
	if(target.checked == false){
   		var userCheckStatus = document.forms['dispatchMapForm'].elements['mapStates'].value;
   		if(userCheckStatus == ''){
			document.forms['dispatchMapForm'].elements['mapStates'].value = ids;
   		}else{
   			var userCheckStatus =	document.forms['dispatchMapForm'].elements['mapStates'].value = userCheckStatus + ',' + ids;
   			document.forms['dispatchMapForm'].elements['mapStates'].value = userCheckStatus.replace( ',,' , ',' );
   		}
   	}
 	if(target.checked){
  		var userCheckStatus = document.forms['dispatchMapForm'].elements['mapStates'].value;
  		var userCheckStatus=document.forms['dispatchMapForm'].elements['mapStates'].value = userCheckStatus.replace( ids , '' );
  		document.forms['dispatchMapForm'].elements['mapStates'].value = userCheckStatus.replace( ',,' , ',' );
  		
  	}
}

function refreshDispatchMap(){
var date1 = document.forms['dispatchMapForm'].elements['date1'].value;
var date2 = document.forms['dispatchMapForm'].elements['date2'].value;
var hubId=document.forms['dispatchMapForm'].elements['hubId'].value;
var originMap=document.forms['dispatchMapForm'].elements['originMap'].checked;
var destinMap=document.forms['dispatchMapForm'].elements['destinMap'].checked;
location.href="filterMap.html?date1="+date1+"&date2="+date2+"&hubId="+hubId+"&originMap="+originMap+"&destinMap="+destinMap+"&decorator=popup&popup=true&from=main","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes";		
}
function applyFilterMap(){
	var date1 = document.forms['dispatchMapForm'].elements['date1'].value;
	var date2 = document.forms['dispatchMapForm'].elements['date2'].value;
	var hubId=document.forms['dispatchMapForm'].elements['hubId'].value;
	var originMap=document.forms['dispatchMapForm'].elements['originMap'].checked;
	var destinMap=document.forms['dispatchMapForm'].elements['destinMap'].checked;
	location.href="filterMap.html?date1="+date1+"&date2="+date2+"&hubId="+hubId+"&originMap="+originMap+"&destinMap="+destinMap+"&decorator=popup&popup=true&from=main","forms","height=600,width=950,top=1, left=200, scrollbars=yes,resizable=yes";		
	}
</script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<s:form id="dispatchMapForm" name="dispatchMapForm" action='dispathMapWorkTicketData.html?decorator=popup&popup=true' method="post" validate="true">
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	
	<c:if test="${not empty date1}">
	<s:text id="mapDate1" name="${FormDateValue}"> <s:param name="value" value="date1" /></s:text>
	<s:hidden  name="date1" value="%{mapDate1}" />  
	</c:if>
	
	<c:if test="${not empty date2}">
	<s:text id="mapDate2" name="${FormDateValue}"> <s:param name="value" value="date2" /></s:text>
	<s:hidden  name="date2" value="%{mapDate2}" />  
	</c:if>
	<s:hidden name="from" value="map" />
	<s:hidden name="mapStates"/>
	<div id="priceinfotab_bg" style="width: 99.8%">
	<div class="price_tleft">Dispatch Map</div>
	</div>
	<table class="mainDetailTable" cellspacing="0" cellpadding="0" border="0" style="width: 100%;">
		<tbody>
			<tr>
				<td colspan="2" align="left" valign="middle" height="82" class="price-bg">
				<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
					<tr>
						<td width="15">&nbsp;</td>
						<td valign="top">
						<div class="curved">
						<table cellpadding="1" cellspacing="2" border="0" style="margin:0px;padding:0px;">
							<tr>
								<td colspan="9">
								<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
									<tr>										
										 <td align="left" width="35px" class="listwhitetext">Filters:</td>
										 <td align="right" width="40px" class="listwhitetext">Origin</td>
										 <c:set var="ischeckeOri" value="false" />
										 <c:if test="${originMap}">
										  <c:set var="ischeckeOri" value="true" />
										 </c:if>
										 <td class="listwhitetext" width="10px"><s:checkbox key="originMap" fieldValue="true" value="${ischeckeOri}"/></td>
										 <td align="right" width="55px" class="listwhitetext">Destination</td>
										 
										 <c:set var="ischeckeDest" value="false" />
										 <c:if test="${destinMap}">
											<c:set var="ischeckeDest" value="true" />
										 </c:if>
										 <td class="listwhitetext" width="10px"><s:checkbox key="destinMap" fieldValue="true" value="${ischeckeDest}"/></td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td colspan="9">
								<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
									<tr>
										<td width="15">&nbsp;HUB</td>
										<td><s:select name="hubId" list="%{opshub}" cssStyle="width:105px; !width:105px;" cssClass="list-menu" headerKey="" headerValue="" /></td>
										<td align="right"  class="listwhitetext">
											<input type="button" class="movebutton" value="Apply Filter" style="width:100px;" onclick="applyFilterMap();"/>
										</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</div>
						</td>
						<td width="5"></td>
						<td valign="top">
						<div class="curved">
						<table cellpadding="1" cellspacing="2" border="0" style="margin:0px;padding:0px;">
							<tr>
								<td colspan="9">
								<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
									<tr>
										<td width="15">&nbsp;</td>
										<td align="right" width="40px" class="listwhitetext"><img height="30px" align="right" alt="Origin" title="Origin" src="${pageContext.request.contextPath}/images/markerO-green.png"/></td>
										<td align="left" width="100px" class="listwhitetext">Origin Work</td>
										<td align="right" width="40px" class="listwhitetext"><img height="30px" align="right" alt="Destination" title="Destination" src="${pageContext.request.contextPath}/images/markerD-red.png"/></td>
										<td align="left" width="125px" valign="middle" class="listwhitetext">Destination Work</td>
										<td align="right"  class="listwhitetext">
											<input type="button" class="movebutton" value="Refresh Dispatch Map" style="width:163px;" onclick="refreshDispatchMap();"/>
										</td>
									</tr>
								</table>
								</td>
							</tr>
							<tr>
								<td colspan="9">
								<table cellpadding="2" cellspacing="1" border="0" style="margin:0px;padding:0px;">
									<tr>
										<td width="5">&nbsp;</td>
									</tr>
								</table>
								</td>
							</tr>
						</table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
			<tr>
				<td valign="top" align="center" style="margin:0px;padding: 0px; background-color:#F6F9FA;">			
					
					<table width="100%" style="margin:0px;padding:0px;" border="0" cellpadding="0" cellspacing="0" >    
				    	<tr>
				    		<td style="padding-left:1px;"></td>
						</tr>
					  	<tr>
				      		<td align="left">
				      			<div id="priceinfotab_bg"><div class="price_tleft">Origin & Destination:</div></div>
				       			<div id="content-containertable" class="override">
							 		<div id="dispatchMapList"></div>
							 	</div>
				      		</td>			      		
				      		
				      		<td width="100%" valign="top" style="margin:0px;padding: 0px;">
				<div id="dmdsurvey"><span id="close"><a href="javascript:setVisible('dmdsurvey')"><strong>close</strong></a></span>
				<div id="content"></div>
				</div>
				<table border="0" cellpadding="2" cellspacing="1" width="100%" style="margin:0px;padding: 0px;">
					<tbody>
						<tr>
							<td valign="top" style="margin:0px;padding: 0px;width:74%;border:1px solid #969694;">
							<div id="map" style="width: 100%; height: 500px;margin:0px;padding: 0px; z-index:1">
							</td>
						</tr>
					</tbody>
				</table>
				</td>
				    	</tr>
				  	</table>
				</td>
				<B>Note</B>:-If you are unable to view the map, this may be due to Firefox security. Please
click on the grey shield icon on the left hand corner of the browsers address
bar and accept to view "Mixed Mode Content" for this page.
			</tr>
		</tbody>
	</table>
	<DIV ID="layerH" style="position:absolute;left:400px;top:140px; z-index:910; ">
		<table cellspacing="0" cellpadding="0" border="0" width="100%" >
			<tr>
				<td align="center">
					<table cellspacing="0" cellpadding="3" align="center">
						<tr>
							<td height="200px"></td>
						</tr>
						<tr>
				       		<td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="300px">
				           		<font color="#0F0F0F" face="Arial" size="2px" weight="bold">Please wait, Map is loading.......</font>
				       		</td>
				       </tr>
       					<tr>
     			 			<td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
           						<img src="<c:url value='/images/ajax-loader.gif'/>" />     
       						</td>
       					</tr>
       				</table>
       			</td>
      		</tr>
       	</table>
	</div>
    <div id="mydiv" style="position:absolute; z-index:905;"></div>
</s:form>
<script>
setTimeout("showOrHide(0)",6000);
try{
	initLoader();	
}catch(e){
	
}


</script>
