<%@ include file="/common/taglibs.jsp"%>   
  <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
  
<head>   
    <title><fmt:message key="miscellaneousDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='miscellaneousDetail.heading'/>"/>


<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js">
</script>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->

<script type="text/javascript">

animatedcollapse.addDiv('services', 'fade=1,hide=1')
animatedcollapse.addDiv('state', 'fade=1,hide=1')
animatedcollapse.addDiv('sitorigin', 'fade=1,hide=1')
//animatedcollapse.addDiv('military', 'fade=1,hide=1')
animatedcollapse.addDiv('interstate', 'fade=1,hide=1')
animatedcollapse.init()

</script>
<style type="text/css">	
.inputTextDate {
font-family:arial,verdana;
font-size:12px;
color:rgb(00,00,00);
border:none;
height:15px;
text-decoration: none;
/*background-color: rgb(214,236,236);*/
background-color:none;}
</style>
<script language="JavaScript">

function completeTimeString(txtValue) {
	var stime1 = txtValue.value;
	if(stime1.substring(stime1.indexOf(":")+1,stime1.length) == "" || stime1.length==1 || stime1.length==2){
		if(stime1.length==1 || stime1.length==2){
			if(stime1.length==2){
				txtValue.value = stime1 + ":00";
			}
			if(stime1.length==1){
				txtValue.value = "0" + stime1 + ":00";
			}
		}else{
			txtValue.value = stime1 + "00";
		}
	}else{
		if(stime1.indexOf(":") == -1 && stime1.length==3){
			txtValue.value  = "0" + stime1.substring(0,1) + ":" + stime1.substring(1,stime1.length);
		}
		if(stime1.indexOf(":") == -1 && (stime1.length==4 || stime1.length==5) ){
			txtValue.value  = stime1.substring(0,2) + ":" + stime1.substring(2,4);
		}
		if(stime1.indexOf(":") == 1){
			txtValue.value = "0" + stime1;
		}
	}
	
}

function IsValidTime(clickType) {
	var timePat = /^(\d{1,2}):(\d{2})(:(\d{2}))?(\s?(AM|am|PM|pm))?$/;
	var timeStr = clickType.value;
	var matchArray = timeStr.match(timePat);
	if (matchArray == null) {
	alert("Time is not in a valid format. Please use HH:MM format");
	clickType.value='00:00'
	clickType.focus();
	return false;
	}
	hour = matchArray[1];
	minute = matchArray[2];
	second = matchArray[4];
	ampm = matchArray[6];
	if (second=="") { second = null; }
	if (ampm=="") { ampm = null }
	if (hour < 0  || hour > 23) {
	alert("Time must between 0 to 23:59 (Hrs)");
	clickType.value='00:00'
	clickType.focus();
	return false;
	}
	if (minute<0 || minute > 59) {
	alert ("Time must between 0 to 59 (Min)");
	clickType.value='00:00'
	clickType.focus();
	return false;
	}
	if (second != null && (second < 0 || second > 59)) {
	alert ("Time must between 0 to 59 (Sec)");
	clickType.value='00:00'
	clickType.focus();
	return false;
	}
}



</script> 
<script language="JavaScript">
function winOpenDest(){
	 <configByCorp:fieldVisibility componentId="component.field.vanline">
	 if('${serviceOrder.job}' =='DOM' || '${serviceOrder.job}' =='UVL' || '${serviceOrder.job}' == 'MVL'|| '${serviceOrder.job}' == 'VAI'){
		 openWindow('destinationPartnersVanline.html?flag=8&destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=miscellaneous.haulingAgentVanlineCode&fld_description=miscellaneous.haulerName&fld_code=miscellaneous.haulingAgentCode',1000,550);
	     }
	     else{
	     openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=miscellaneous.haulerName&fld_code=miscellaneous.haulingAgentCode');
	     }
	     document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].select();
    </configByCorp:fieldVisibility>
   <configByCorp:fieldVisibility componentId="component.field.vanlineSTAR">
       	openWindow('destinationPartners.html?destination=${serviceOrder.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=miscellaneous.haulerName&fld_code=miscellaneous.haulingAgentCode');
   </configByCorp:fieldVisibility>

}
</script>
<script language="JavaScript">
	
	
	function isNumeric(targetElement)
	{   var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        // Check that current character is number.
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid number");
	        targetElement.value="";
	        
	        return false;
	        }
	    }
	    //alert("Enter valid Number");
	    // All characters are numbers.
	    return true;
	}
	function onlyTimeFormatAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || (keyCode==59) ; 
	}
	
function forDays(){
 document.forms['miscellaneousForm'].elements['checkSitDestinationDaysClick'].value = '1';
}
	
function calcDays()
{
 var date2 = document.forms['miscellaneousForm'].elements['sitDestinationIn'].value;	 
 var date1 = document.forms['miscellaneousForm'].elements['sitDestinationA'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = (Math.round((sDate-eDate)/86400000))+1;
  
  document.forms['miscellaneousForm'].elements['sitDestinationDays'].value = daysApart; 
  if(daysApart<1)
  {
    alert("SIT Destination In Date Range must be less than to SIT Destination Out");
    document.forms['miscellaneousForm'].elements['sitDestinationDays'].value='';
    document.forms['miscellaneousForm'].elements['sitDestinationA'].value='';
  }
  if(document.forms['miscellaneousForm'].elements['sitDestinationDays'].value=='NaN')
   {
     document.forms['miscellaneousForm'].elements['sitDestinationDays'].value = '';
   }
    document.forms['miscellaneousForm'].getElementById("sitDestinationDays").focus();
 	document.forms['miscellaneousForm'].elements['checkSitDestinationDaysClick'].value = '';
}

	
</script>
<script>
function ContainerAutoSave(clickType){
	<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
		<c:set var="forwardingTabVal" value="Y" />
	</configByCorp:fieldVisibility>
	<c:set var="checkPropertyAmountComponent" value="N" />
	<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
		<c:set var="checkPropertyAmountComponent" value="Y" />
	</configByCorp:fieldVisibility>
	var checkMsgClickedValue = document.forms['miscellaneousForm'].elements['checkMsgClickedValue'].value;
	if ('${autoSavePrompt}' == 'No'){
	var noSaveAction = '<c:out value="${serviceOrder.id}"/>';
	    var id1 = document.forms['miscellaneousForm'].elements['serviceOrder.id'].value;
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                //noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'editServiceOrderUpdate.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
        }
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.accounting'){
                //noSaveAction = 'accountLineList.html?sid='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'accountLineList.html?sid='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
        }
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
    		noSaveAction = 'pricingList.html?sid='+id1;
    	}
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.containers'){
            <c:if test="${forwardingTabVal!='Y'}">
  				noSaveAction = 'containers.html?id='+id1;
  			</c:if>
	  		<c:if test="${forwardingTabVal=='Y'}">
	  			//noSaveAction = 'containersAjaxList.html?id='+id1;
	  			<c:if test="${checkPropertyAmountComponent!='Y'}">
					noSaveAction = 'containersAjaxList.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
	  		</c:if>
        }
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.billing'){
                //noSaveAction = 'editBilling.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'editBilling.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
        }
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.domestic'){
                noSaveAction = 'editMiscellaneous.html?id='+id1;
        }
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.OI'){
    		//noSaveAction = 'operationResource.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'operationResource.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
    	}
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.status'){
		    <c:if test="${serviceOrder.job =='RLO'}">
				noSaveAction = 'editDspDetails.html?id='+id1; 
			</c:if>
			<c:if test="${serviceOrder.job !='RLO'}">
				//noSaveAction =  'editTrackingStatus.html?id='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
					noSaveAction =  'editTrackingStatus.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		noSaveAction =  'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			</c:if>
		}
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.ticket'){
                //noSaveAction = 'customerWorkTickets.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'customerWorkTickets.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
        }

		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.claims'){
                //noSaveAction = 'claims.html?id='+id1;
			<c:if test="${checkPropertyAmountComponent!='Y'}">
				noSaveAction = 'claims.html?id='+id1;
			</c:if>
		  	<c:if test="${checkPropertyAmountComponent=='Y'}">
		  		noSaveAction = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
		  	</c:if>
        }
                
		if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
				var cidVal='${customerFile.id}';
                noSaveAction = 'editCustomerFile.html?id='+cidVal;
        }
        processAutoSave(document.forms['miscellaneousForm'], 'saveMiscellaneous!saveOnTabChange.html', noSaveAction);
    }else{

    if(!(clickType == 'save')){
    var id1 = document.forms['miscellaneousForm'].elements['miscellaneous.id'].value;
    var jobNumber = document.forms['miscellaneousForm'].elements['miscellaneous.shipNumber'].value;
    //var idc = document.forms['miscellaneousForm'].elements['customerFile.id'].value;

    if (document.forms['miscellaneousForm'].elements['formStatus'].value == '1'){
        var agree = confirm("Press OK to continue with saving or Press Cancel to Continue without saving the <fmt:message key='miscellaneousDetail.heading'/>");
        if(agree){
            document.forms['miscellaneousForm'].action = 'saveMiscellaneous!saveOnTabChange.html';
            document.forms['miscellaneousForm'].submit();
        }else{
            if(id1 != ''){

			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
                //location.href = 'editServiceOrderUpdate.html?id='+id1;
                <c:if test="${checkPropertyAmountComponent!='Y'}">
                	location.href = 'editServiceOrderUpdate.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
            }

			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.accounting'){
			                //location.href = 'accountLineList.html?sid='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
	            	location.href = 'accountLineList.html?sid='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
			    location.href = 'pricingList.html?sid='+id1;
			    }
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.containers'){
	                <c:if test="${forwardingTabVal!='Y'}">
	                	location.href = 'containers.html?id='+id1;
	  				</c:if>
		  			<c:if test="${forwardingTabVal=='Y'}">
		  				//location.href = 'containersAjaxList.html?id='+id1;
		  				<c:if test="${checkPropertyAmountComponent!='Y'}">
			            	location.href = 'containersAjaxList.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					  	</c:if>
		  			</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.billing'){
			                //location.href = 'editBilling.html?id='+id1;
					<c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'editBilling.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
			}
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.domestic'){
			                location.href = 'editMiscellaneous.html?id='+id1;
			                }
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.status'){
				    <c:if test="${serviceOrder.job =='RLO'}">
					    location.href = 'editDspDetails.html?id='+id1; 
					</c:if>
					<c:if test="${serviceOrder.job !='RLO'}">
						//location.href =  'editTrackingStatus.html?id='+id1;
						<c:if test="${checkPropertyAmountComponent!='Y'}">
			            	location.href = 'editTrackingStatus.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					  	</c:if>
					</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.OI'){
				//location.href =  'operationResource.html?id='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
	            	location.href = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.ticket'){
			                //location.href = 'customerWorkTickets.html?id='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
	            	location.href = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.claims'){
			                //location.href = 'claims.html?id='+id1;
	                <c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'claims.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
			}
			               
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
							var cidVal='${customerFile.id}';
			                location.href = 'editCustomerFile.html?id='+cidVal;
			}
        }
        }
    }else{
    if(id1 != ''){

			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.serviceorder'){
			                //location.href = 'editServiceOrderUpdate.html?id='+id1;
					<c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'editServiceOrderUpdate.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editServiceOrderUpdate.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
			}
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.accounting'){
			                //location.href = 'accountLineList.html?sid='+id1;
	                <c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'accountLineList.html?sid='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'accountLineList.html?sid='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.newAccounting'){
			    location.href = 'pricingList.html?sid='+id1;
			    }
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.containers'){
	                <c:if test="${forwardingTabVal!='Y'}">
	            		location.href = 'containers.html?id='+id1;
					</c:if>
	  				<c:if test="${forwardingTabVal=='Y'}">
	  					//location.href = 'containersAjaxList.html?id='+id1;
	  					<c:if test="${checkPropertyAmountComponent!='Y'}">
			            	location.href = 'containersAjaxList.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'containersAjaxList.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					  	</c:if>
	  				</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.billing'){
			                //location.href = 'editBilling.html?id='+id1;
	                <c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'editBilling.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editBilling.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.domestic'){
			                location.href = 'editMiscellaneous.html?id='+id1;
			}
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.status'){
			    <c:if test="${serviceOrder.job =='RLO'}">
				    location.href = 'editDspDetails.html?id='+id1; 
				</c:if>
				<c:if test="${serviceOrder.job !='RLO'}">
					//location.href =  'editTrackingStatus.html?id='+id1;
					<c:if test="${checkPropertyAmountComponent!='Y'}">
		            	location.href = 'editTrackingStatus.html?id='+id1;
					</c:if>
				  	<c:if test="${checkPropertyAmountComponent=='Y'}">
				  		location.href = 'editTrackingStatus.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
				  	</c:if>
				</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.OI'){
				//location.href =  'operationResource.html?id='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
	            	location.href = 'operationResource.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'operationResource.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.ticket'){
			                //location.href = 'customerWorkTickets.html?id='+id1;
				<c:if test="${checkPropertyAmountComponent!='Y'}">
	            	location.href = 'customerWorkTickets.html?id='+id1;
				</c:if>
			  	<c:if test="${checkPropertyAmountComponent=='Y'}">
			  		location.href = 'customerWorkTickets.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
			  	</c:if>
			}
			
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.claims'){
			                //location.href = 'claims.html?id='+id1;
		                <c:if test="${checkPropertyAmountComponent!='Y'}">
			            	location.href = 'claims.html?id='+id1;
						</c:if>
					  	<c:if test="${checkPropertyAmountComponent=='Y'}">
					  		location.href = 'claims.html?id='+id1+'&msgClicked='+checkMsgClickedValue;
					  	</c:if>
			}
			if(document.forms['miscellaneousForm'].elements['gotoPageString'].value == 'gototab.customerfile'){
							var cidVal='${customerFile.id}';
			                location.href = 'editCustomerFile.html?id='+cidVal;
			}
    }
    }
}
}
}


function changeStatus(){
    document.forms['miscellaneousForm'].elements['formStatus'].value = '1';
}

function setFlagValue(){
	document.forms['miscellaneousForm'].elements['hitFlag'].value = '1';
}
</script>
<script language="JavaScript">
function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	
		
}
</script>

<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
<script language="javascript" type="text/javascript">
function findTruckNumber() {
document.forms['miscellaneousForm'].elements['validateFlag'].value='OK'; 
 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=miscellaneous.tractorAgentVanLineCode&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=miscellaneous.carrierName&fld_code=miscellaneous.carrier&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].select();
 }
function findVanId() {	
	 	  openWindow('findTruckNumber.html?decorator=popup&popup=true&fld_sixthDescription=vanTruckDriverList&fld_fifthDescription=fifthDescription&fld_fourthDescription=miscellaneous.vanAgentVanLineCode&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=miscellaneous.vanName&fld_code=miscellaneous.vanID&fld_seventhDescription=seventhDescription',1024,500);
	 	  document.forms['miscellaneousForm'].elements['miscellaneous.vanID'].focus();	 
	 }
function findTruckNumber1() {
      document.forms['miscellaneousForm'].elements['validateFlagTrailer'].value='OK'; 
 	  openWindow('findTruckTrailer.html?decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.trailer&fld_seventhDescription=seventhDescription',1024,500);
 	  document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].select();
 } 
 function findDriverListFromTruck() {
 var truckNumber =document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;
 document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='OK'; 
 	  openWindow("truckDriveList.html?truckNumber="+truckNumber+"&decorator=popup&popup=true&fld_sixthDescription=truckDriverList&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.driverId",924,500);
 	  
 }
 function findPackingDriver(){
 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.packingDriverId&fld_ninthDescription=ninthDescription&fld_eigthDescription=eightDescription');
 }
 function findLoadingDriver(){
	 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.loadingDriverId&fld_ninthDescription=ninthDescription&fld_eigthDescription=eightDescription');
	 }
 function findDeliveryDriver(){
	 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.deliveryDriverId&fld_ninthDescription=ninthDescription&fld_eigthDescription=eightDescription');
	 }
 function findSitDestinationDriver(){
	 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=tenthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.sitDestinationDriverId&fld_ninthDescription=ninthDescription&fld_eigthDescription=eightDescription');
	 }
 function findDriverFromPartner(){
	 document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='OK'; 
	 openWindow('truckDriversList.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_tenthDescription=miscellaneous.driverName&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.driverId&fld_ninthDescription=miscellaneous.driverCell&fld_eigthDescription=eightDescription');
	 }
</script>

<script type="text/javascript">
 function goPrev() {
	var soIdNum =document.forms['miscellaneousForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['miscellaneousForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrders.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
   
 function goNext() {
	var soIdNum =document.forms['miscellaneousForm'].elements['serviceOrder.id'].value;
	var seqNm =document.forms['miscellaneousForm'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrders.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
     http5.onreadystatechange = handleHttpResponseOtherShip; 
     http5.send(null); 
   }
function handleHttpResponseOtherShip(){
             if (http5.readyState == 4)
             {
               var results = http5.responseText
               results = results.trim();
               if(results != ''){
                 location.href = 'editMiscellaneous.html?id='+results;
                } else {
                alert("You can't Move INT & JVS Job Type from Domestic Tab"); 
              }
       }  }  
function findCustomerOtherSO(position) {
 var sid=document.forms['miscellaneousForm'].elements['customerFile.id'].value;
 var soIdNum=document.forms['miscellaneousForm'].elements['serviceOrder.id'].value;
 var url="customerOtherSOByJob.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
  ajax_showTooltip(url,position);	
  } 
function goToUrl(id)
	{
      location.href = "editMiscellaneous.html?id="+id;
	}
function goToUrls(id){
		alert("You can't Move INT & JVS Job Type from Domestic Tab");
}
	
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http5 = getHTTPObject();
    var http22 = getHTTPObject();
    var http2 = getHTTPObject();
  
function getVanlineCode(targetField, position) {
	<configByCorp:fieldVisibility componentId="component.field.vanline">
  	 var partnerCode = document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value;
  	 if(partnerCode != ''){
			var job = document.forms['miscellaneousForm'].elements['serviceOrder.job'].value; 
    		if(job=='DOM'||job=='UVL'||job=='MVL'||job=='VAI'){
			   	position = document.getElementById('navigationOA'); 
			 	var url="findVanLineList.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode) +"&agentType="+ encodeURI(targetField);
			  	ajax_showTooltip(url,position);
			}
	}
	</configByCorp:fieldVisibility>
}

function goToUrlVanline(id, targetField){
        	document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentVanlineCode'].value = id;  
}


function getVanlineAgent(targetField){
	 var vanlineCode = document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentVanlineCode'].value;
    if(vanlineCode != ''){
    	var url="findVanLineAgent.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vanlineCode);
    	http22.open("GET", url, true);
    	http22.onreadystatechange = function() {handleHttpResponseVanlineAgent(targetField);};
    	http22.send(null);
    }
}

function handleHttpResponseVanlineAgent(targetField){
	if (http22.readyState == 4){
		var results = http22.responseText
        results = results.trim();

        if(results.length>1){
	        res = results.split("@");
	
	        targetElement = document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'];
	        
	        targetElement.length = res.length;
			for(i=0;i<res.length;i++){
				if(res.length == 2){
					stateVal = res[i].split("~");
					document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value = '';
					document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value = '';
				}else{
					stateVal = res[i].split("~");
					document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value = stateVal[0];
					document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value = stateVal[1];
				}
			}
        }else{
        	alert("Invalid vanline code");
        	document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentVanlineCode'].value='';
       	}
        
		
    }
}     

function checkVendorName(){
    var vendorId = document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value;
    if(vendorId == ''){
    	document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value='';
    }
    document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentVanlineCode'].value='';
    if(vendorId!=''){ 
	    var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(vendorId);
	    http2.open("GET", url, true);
	    http2.onreadystatechange = handleHttpResponse8;
	    http2.send(null);
    }
}

function handleHttpResponse8(){
		if (http2.readyState == 4){
                var results = http2.responseText
                results = results.trim();
                var res = results.split("#"); 
                if(res.length>2){
                	if(res[2] == 'Approved'){
           				document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value = res[1];
           				document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].select();
	           		}else{
	           			alert("Hauling Agent code is not approved" ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.haulerName'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value="";
				 	    document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].select();
				 	    showAddressImage();
	           		}
               	}
       }
}

function showPartnerAlert(display, code, position){
     if(code == ''){ 	
     	document.getElementById(position).style.display="none";
     	ajax_hideTooltip();
     }else if(code != ''){
     	getBAPartner(display,code,position);
     }      
}
function getBAPartner(display,code,position){
	var url = "findPartnerAlertList.html?ajax=1&decorator=simple&popup=true&notesId=" + encodeURI(code);
     httpPA.open("GET", url, true);
     httpPA.onreadystatechange = function() {handleHttpResponsePartner(display,code,position)};
     httpPA.send(null);
}
function handleHttpResponsePartner(display,code,position){
 	if (httpPA.readyState == 4){
		var results = httpPA.responseText
        results = results.trim();
        if(results.length > 555){
          	document.getElementById(position).style.display="block";
          	
          	if(display != 'onload'){
          		getPartnerAlert(code,position);
          	}
      	}else{
      		document.getElementById(position).style.display="none";
      		ajax_hideTooltip();
      	}
	}
}

var httpPA = getHTTPObjectPA();
function getHTTPObjectPA(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	

var httpNon = getHTTPObjectNon();
function getHTTPObjectNon(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpTrailer = getHTTPObjectTrailer();
function getHTTPObjectTrailer(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	
var httpSop = getHTTPObjectSop();
function getHTTPObjectSop(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	

var httpTruck = getHTTPObjectTruck();
function getHTTPObjectTruck(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	

var httpValidate = getHTTPObjectValidate();
function getHTTPObjectValidate(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}	
var httpValidateTrailer = getHTTPObjectValidateTrailer();
function getHTTPObjectValidateTrailer(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpValidateDriver = getHTTPObjectValidateDriver();
function getHTTPObjectValidateDriver(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpValidatePDriver = getHTTPObjectValidatePDriver();
function getHTTPObjectValidatePDriver(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}


function okValidateListFromTruck(){
document.forms['miscellaneousForm'].elements['validateFlag'].value='OK';
}
function okValidateListFromDriver(){
document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='OK';
}
function okValidateListFromTrailer(){
document.forms['miscellaneousForm'].elements['validateFlagTrailer'].value='OK';
}
function carrierListInDomestic()
{       
        var truckNumber=document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;
	    var url="carrierListInDomestic.html?ajax=1&decorator=simple&popup=true&truckNumber=" + encodeURI(truckNumber);
	    httpNon.open("GET", url, true);
	    httpNon.onreadystatechange = handleHttpResponseNon;
	    httpNon.send(null);
  
}

function handleHttpResponseNon(){
		if (httpNon.readyState == 4){
                var results = httpNon.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res.length>=1){
                var res1=res.split("~"); 
                		document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value = res1[0];
           				document.forms['miscellaneousForm'].elements['miscellaneous.tractorAgentVanLineCode'].value = res1[1];
           				validateListFromTruck();           				
	           		}else{
	           			alert("Carrier is not valid." ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.tractorAgentVanLineCode'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.tractorAgentName'].value="";
	           		}
               	}
       }
function vanListInDomestic()
{       
        var truckNumber=document.forms['miscellaneousForm'].elements['miscellaneous.vanID'].value;
        truckNumber=truckNumber.replace("'","");
	    var url="vanListDomesticName.html?ajax=1&decorator=simple&popup=true&truckNumber=" + encodeURI(truckNumber);
	    httpVan.open("GET", url, true);
	    httpVan.onreadystatechange = handleHttpResponseVan;
	    httpVan.send(null);
  
}

function handleHttpResponseVan(){
		if (httpVan.readyState == 4){
                var results = httpVan.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")
                 if(res.length>=1){
                var res1=res.split("~");    
           		document.forms['miscellaneousForm'].elements['miscellaneous.vanName'].value = res1[0];
           				document.forms['miscellaneousForm'].elements['miscellaneous.vanAgentVanLineCode'].value = res1[1];
           				findPartnerNameForVanAgent();           				
	           		}else{
	           			alert("Van# is not valid." ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.vanName'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.vanAgentVanLineCode'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.vanID'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.vanAgentName'].value="";
				 	   
	           		}
               	}
       }
var httpVan = getHTTPObjectVan();
function getHTTPObjectVan(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function trailerCodeInDomestic()
{       
        var truckNumber=document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value;
        //alert(truckNumber);
	    var url="trailerCodeInDomestic.html?ajax=1&decorator=simple&popup=true&truckNumber=" + encodeURI(truckNumber);
	    httpTrailer.open("GET", url, true);
	    httpTrailer.onreadystatechange = handleHttpResponseTrailerCode;
	    httpTrailer.send(null);
  
}

function handleHttpResponseTrailerCode(){
		if (httpTrailer.readyState == 4){
                var results = httpTrailer.responseText
                results = results.trim();
                //alert(results);
                results=results.replace("[","")
                var res=results.replace("]","")                
                if(res.length>2){
           				//document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value = res;
           				validateListFromTruckTrailer();           				
	           		}else{
	           			alert("Trailer Code is not valid." ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value="";
					    //document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
				 	   
	           		}
               	}
       }
function ownerOperatorListInDomestic()
{
        document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='OK'; 
        var partnerCode=document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value;
	    var url="findByOwnerListMiscellaneous.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode);
	    httpSop.open("GET", url, true);
	    httpSop.onreadystatechange = handleHttpResponseSop;
	    httpSop.send(null);
  
}

function handleHttpResponseSop(){
		if (httpSop.readyState == 4){
                var results = httpSop.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")  
                if(res.length>2){
                var result=res.split("#");
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value = result[0]+" "+result[1];
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverCell'].value = result[2];
           				validateListForDriver();
	           		}else{
	           			alert("Driver ID is not valid." ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value="";
				 	   document.forms['miscellaneousForm'].elements['miscellaneous.driverCell'].value="";
	           		}
               	}
       }
function findParticularTrailerFromTruck(){
      var truckNumber=document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;
      var url="findParticularTrailerFromTruck.html?ajax=1&decorator=simple&popup=true&truckNumber=" + encodeURI(truckNumber);
        httpPerticularTrailer.open("GET", url, true);
	    httpPerticularTrailer.onreadystatechange = handleParticularTrailerFromTruck;
	    httpPerticularTrailer.send(null);
}

function findPerticularDriverFromTruck()
{         
        var truckNumber=document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;
	    var url="findByOwnerListFromPerticularTruck.html?ajax=1&decorator=simple&popup=true&truckNumber=" + encodeURI(truckNumber);
	    httpPerticular.open("GET", url, true);
	    httpPerticular.onreadystatechange = handlePerticularDriverFromTruck;
	    httpPerticular.send(null);
  
}
function handleParticularTrailerFromTruck(){
		if (httpPerticularTrailer.readyState == 4){
                var results = httpPerticularTrailer.responseText
                 results = results.trim();                
                results=results.replace("[","")
                 results =results.replace("]","")
                  if(results!=""){              
                        document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value=results;
           				okValidateListFromTrailer();
	           		}else{  }
               	}
       }

function handlePerticularDriverFromTruck(){
		if (httpPerticular.readyState == 4){
                var results = httpPerticular.responseText
                results = results.trim();                
                results=results.replace("[","")
                var result=results.replace("]","")
                var res = result.split("#");             
                  if(res[0]!="" &&(res[1]!="" || res[2]!="")){
                        document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value = res[0];
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value = res[1]+" "+res[2]; 
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverCell'].value= res[3];
           				document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='OK';          				
           				validateListForDriverPerticular();
	           		}else{}
               	}
       }

function ownerOperatorListFromTruck(){
        var partnerCode=document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value;
        var truckNumber =document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;        
	    var url="findByOwnerListFromTruck.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(partnerCode)+ "&truckNumber="+ encodeURI(truckNumber);
	    httpTruck.open("GET", url, true);
	    httpTruck.onreadystatechange = handleHttpResponseTruck;
	    httpTruck.send(null);
}

function handleHttpResponseTruck(){
		if (httpTruck.readyState == 4){
                var results = httpTruck.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")
                var result=res.split('#');                 
                       if(result!='' && (result[0]!='' || result[1]!='')){
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value = result[0]+' '+result[1];
           				document.forms['miscellaneousForm'].elements['miscellaneous.driverCell'].value = result[2]
           				validateListForDriver();
	           		}else{
	           			alert("Driver ID is not valid." ); 
					    document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value="";
					    document.forms['miscellaneousForm'].elements['miscellaneous.driverCell'].value="";
				 	   
	           		}
               	}
       }
       
 function validateListFromTruck(){ 
 if(document.forms['miscellaneousForm'].elements['validateFlag'].value=='OK'){
        var parentId=document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value;
        var deliveryLastDay=document.forms['miscellaneousForm'].elements['deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidate.open("GET", url, true);
	    httpValidate.onreadystatechange = handleHttpResponseValidate;
	    httpValidate.send(null);	    
	    if('${serviceOrder.job}' =='HVY'){
	    findPerticularDriverFromTruck();
	    findParticularTrailerFromTruck();
	    }
	    findPartnerNameForTruckAgent();
	    }
}

function handleHttpResponseValidate(){
		if (httpValidate.readyState == 4){
                var results = httpValidate.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Carrier cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
			     document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value="";
			     document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].select();
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{
                 var agree =confirm("This Carrier has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     { 
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value="";
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].select();
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Carrier cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
			     document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value="";
			     document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].select();
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{                 
                 var agree =confirm("This Carrier has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     { 
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].value="";
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrierName'].value="";
			         document.forms['miscellaneousForm'].elements['miscellaneous.carrier'].select();
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 }
                 }               
      }
      document.forms['miscellaneousForm'].elements['validateFlag'].value='';
      
      }

function validateListFromTruckTrailer(){ 
 if(document.forms['miscellaneousForm'].elements['validateFlagTrailer'].value=='OK'){
        var parentId=document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value;
        var deliveryLastDay=document.forms['miscellaneousForm'].elements['deliveryLastDay'].value;                
	    var url="controlExpirationsListInDomestic.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateTrailer.open("GET", url, true);
	    httpValidateTrailer.onreadystatechange = handleHttpResponseTrailer;
	    httpValidateTrailer.send(null);	    
	      }
}

function handleHttpResponseTrailer(){
		if (httpValidateTrailer.readyState == 4){
                var results = httpValidateTrailer.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Trailer Code cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value="";
			     document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].select();
                 }else{
                 var agree =confirm("This Trailer Code has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     } 
			     else 
			     { 
			         document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value="";			        
			         document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].select();
			         
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Trailer Code cannot be selected due to the expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value="";			     
			     document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].select();			     
                 }else{                 
                 var agree =confirm("This Trailer Code has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {			     
			     } 
			     else 
			     { 
			         document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].value="";
			         document.forms['miscellaneousForm'].elements['miscellaneous.trailer'].select();			         
			     }
                 }
                 }
                 }               
      }
      document.forms['miscellaneousForm'].elements['validateFlagTrailer'].value='';
      
      }

function validateListForDriver(){ 
 if(document.forms['miscellaneousForm'].elements['validateFlagDriver'].value=='OK'){
        var parentId=document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value;
        var deliveryLastDay=document.forms['miscellaneousForm'].elements['deliveryLastDay'].value;
	    var url="findControlExpirationsListForDriver.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidateDriver.open("GET", url, true);
	    httpValidateDriver.onreadystatechange = handleHttpResponseDriver;
	    httpValidateDriver.send(null);	    
}
}
function handleHttpResponseDriver(){
		if (httpValidateDriver.readyState == 4){
                var results = httpValidateDriver.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Driver cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{
                 var agree =confirm("This Driver has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Driver cannot be selected due to the expirations: "+ExpiryFor);                 
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{                 
                 var agree =confirm("This Driver has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 }
                 }               
      }
      document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='';
      
      }

 
 function validateListForDriverPerticular(){ 
 if(document.forms['miscellaneousForm'].elements['validateFlagDriver'].value=='OK'){
        var parentId=document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value;
        var deliveryLastDay=document.forms['miscellaneousForm'].elements['deliveryLastDay'].value;
	    var url="findControlExpirationsListForDriver.html?ajax=1&decorator=simple&popup=true&parentId=" + encodeURI(parentId)+"&deliveryLastDay=" + encodeURI(deliveryLastDay);
	    httpValidatePDriver.open("GET", url, true);
	    httpValidatePDriver.onreadystatechange = handleHttpResponsePDriver;
	    httpValidatePDriver.send(null);	    
}
}
function handleHttpResponsePDriver(){
		if (httpValidatePDriver.readyState == 4){
                var results = httpValidatePDriver.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                               
                if(res !=''){        
                var totalElement= res.indexOf(',');                
                if(totalElement>0)
                {
                var res = res.split(",");
                var res11 = res[0].split("#");
                var res22 = res[1].split("#");  
                var res33 = "";
                if(res[2] != undefined)
                {
                	res33 = res[2].split("#");
                }
                var ExpiryFor = "";
                 if(res11[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res11[1]+":   "+res11[2];
                 }
                 if(res22[1] != "")
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res22[1]+":   "+res22[2];
                 }
                 if(res33[1] != "" &&  res33[1] != undefined)
                 {
                 	ExpiryFor = ExpiryFor+"\n"+res33[1]+":   "+res33[2];
                 }
                if(res11[0] =='Yes' || res22[0].trim() =='Yes')
                 {
                 alert("This Driver cannot be selected due to the folllowing expirations: "+ExpiryFor);
                 document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{
                 var agree =confirm("This Driver has the following expirations: "+ExpiryFor+"\nDo you wish to proceed?");                 
                 if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 } 
                 else{
                 var res = res.split("#");
                 var ExpiryFor = ""; 
                 ExpiryFor = ExpiryFor+"\n"+res[1]+":   "+res[2];               
                 if(res[0] =='Yes')
                 {
                 alert("This Driver cannot be selected due to the expirations: "+ExpiryFor);                 
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		 document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
                 }else{                 
                 var agree =confirm("This Driver has expirations: "+ExpiryFor+"\nDo you wish to proceed?");
			     if(agree) 
			     {
			     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
			     } 
			     else 
			     {   
			         document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value ="";
           		     document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].value ="";
			     }
                 }
                 }
                 }               
      }
      document.forms['miscellaneousForm'].elements['validateFlagDriver'].value='';
      
      }

function validatePackingContractor(){
    var ownerPayTo = document.forms['miscellaneousForm'].elements['miscellaneous.packingDriverId'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(ownerPayTo);
    httpPackingDriverId.open("GET", url, true);
    httpPackingDriverId.onreadystatechange = handleHttpResponse4;
    httpPackingDriverId.send(null);     
}

function handleHttpResponse4()
        {
             if (httpPackingDriverId.readyState == 4)
             {             
                var results = httpPackingDriverId.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){
 				  }
                 else{
                 alert("Entered Packing Contractor is incorrect.");
                 document.forms['miscellaneousForm'].elements['miscellaneous.packingDriverId'].value="";
                 document.forms['miscellaneousForm'].elements['miscellaneous.packingDriverId'].select();
                 }
                 }
       }     
var httpPackingDriverId = getHTTPObjectPacking();
function getHTTPObjectPacking(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function validateLoadingContractor(){
    var ownerPayTo = document.forms['miscellaneousForm'].elements['miscellaneous.loadingDriverId'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(ownerPayTo);
    httpLoadingDriverId.open("GET", url, true);
    httpLoadingDriverId.onreadystatechange = handleHttpResponse5;
    httpLoadingDriverId.send(null);     
}

function handleHttpResponse5()
        {
             if (httpLoadingDriverId.readyState == 4)
             {             
                var results = httpLoadingDriverId.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){
 				  }
                 else{
                 alert("Entered Loading Contractor is incorrect.");
                 document.forms['miscellaneousForm'].elements['miscellaneous.loadingDriverId'].value="";
                 document.forms['miscellaneousForm'].elements['miscellaneous.loadingDriverId'].select();
                 }
                 }
       }     
var httpLoadingDriverId = getHTTPObjectLoading();
function getHTTPObjectLoading(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function validateDeliveryContractor(){
    var ownerPayTo = document.forms['miscellaneousForm'].elements['miscellaneous.deliveryDriverId'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(ownerPayTo);
    httpDeliveryDriverId.open("GET", url, true);
    httpDeliveryDriverId.onreadystatechange = handleHttpResponse6;
    httpDeliveryDriverId.send(null);     
}

function handleHttpResponse6()
        {
             if (httpDeliveryDriverId.readyState == 4)
             {             
                var results = httpDeliveryDriverId.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){
 				  }
                 else{
                 alert("Entered Delivery Contractor is incorrect.");
                 document.forms['miscellaneousForm'].elements['miscellaneous.deliveryDriverId'].value="";
                 document.forms['miscellaneousForm'].elements['miscellaneous.deliveryDriverId'].select();
                 }
                 }
       }     
var httpDeliveryDriverId = getHTTPObjectDelivery();
function getHTTPObjectDelivery(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
function validateSitDestinationContractor(){
    var ownerPayTo = document.forms['miscellaneousForm'].elements['miscellaneous.sitDestinationDriverId'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(ownerPayTo);
    httpSitDestinationDriverId.open("GET", url, true);
    httpSitDestinationDriverId.onreadystatechange = handleHttpResponse7;
    httpSitDestinationDriverId.send(null);     
}

function handleHttpResponse7()
        {
             if (httpSitDestinationDriverId.readyState == 4)
             {             
                var results = httpSitDestinationDriverId.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){
 				  }
                 else{
                 alert("Entered Sit Destination Contractor is incorrect.");
                 document.forms['miscellaneousForm'].elements['miscellaneous.sitDestinationDriverId'].value="";
                 document.forms['miscellaneousForm'].elements['miscellaneous.sitDestinationDriverId'].select();
                 }
                 }
       }     
var httpSitDestinationDriverId = getHTTPObjectSitDestination();
function getHTTPObjectSitDestination(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}     
var httpPerticular = getHTTPObjectPerticular();
function getHTTPObjectPerticular(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  

var httpPerticularTrailer = getHTTPObjectPerticularTrailer();
function getHTTPObjectPerticularTrailer(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
var httpPartnerNameForTruckAgent = getHTTPObjectPartnerNameForTruckAgent();
function getHTTPObjectPartnerNameForTruckAgent(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

var httpPartnerNameForVanAgent = getHTTPObjectPartnerNameForVanAgent();
function getHTTPObjectPartnerNameForVanAgent(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

function findPartnerNameForTruckAgent()
{
	var truckVanLineCode=document.forms['miscellaneousForm'].elements['miscellaneous.tractorAgentVanLineCode'].value;
	if(truckVanLineCode!='')
		{
	var url="findTractorAgentVanCode.html?ajax=1&decorator=simple&popup=true&truckVanLineCode=" + encodeURI(truckVanLineCode);
    httpPartnerNameForTruckAgent.open("GET", url, true);
    httpPartnerNameForTruckAgent.onreadystatechange = handlePartnerNameForTruckAgent;
    httpPartnerNameForTruckAgent.send(null);
		}
}
function handlePartnerNameForTruckAgent()
{
	 if (httpPartnerNameForTruckAgent.readyState == 4)
     {             
        var results = httpPartnerNameForTruckAgent.responseText
        results = results.trim();
        results=results.replace("[","")
        var res=results.replace("]","") 
        document.forms['miscellaneousForm'].elements['miscellaneous.tractorAgentName'].value=res;
         }
	 //document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
}
function findPartnerNameForVanAgent()
{
	var truckVanLineCode=document.forms['miscellaneousForm'].elements['miscellaneous.vanAgentVanLineCode'].value;
	var url="findTractorAgentVanCode.html?ajax=1&decorator=simple&popup=true&truckVanLineCode=" + encodeURI(truckVanLineCode);
	httpPartnerNameForVanAgent.open("GET", url, true);
	httpPartnerNameForVanAgent.onreadystatechange = handlePartnerNameForVanAgent;
	httpPartnerNameForVanAgent.send(null);
}
function handlePartnerNameForVanAgent()
{
	 if (httpPartnerNameForVanAgent.readyState == 4)
     {             
        var results = httpPartnerNameForVanAgent.responseText
        results = results.trim();
        results=results.replace("[","")
        var res=results.replace("]","") 
        document.forms['miscellaneousForm'].elements['miscellaneous.vanAgentName'].value=res;
         }
	 //document.forms['miscellaneousForm'].elements['miscellaneous.driverName'].select();
}


function checkLength(targetValue){
	var vanLineCodeLength=targetValue.value;	
	if(vanLineCodeLength.length!=6){
		alert("Please enter six character data.");
	    document.getElementById(targetValue.id).value='';
        document.getElementById(targetValue.id).select();
		return false
	}
	return true
}
function isNumberKey(evt)
{
   var charCode = (evt.which) ? evt.which : event.keyCode
   if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;

   return true;
}
</script>

</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="miscellaneousForm" action="saveMiscellaneous" method="post" validate="true">
<c:set var="from" value="<%=request.getParameter("from")%>"/>
<c:set var="field" value="<%=request.getParameter("field")%>"/>
<s:hidden name="field" value="<%=request.getParameter("field")%>" />
<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
<c:set var="field1" value="<%=request.getParameter("field1") %>"/>
<c:set var="idOfWhom" value="${serviceOrder.id}" scope="session"/>
<c:set var="noteID" value="${serviceOrder.shipNumber}" scope="session"/>
<c:set var="noteFor" value="ServiceOrder" scope="session"/>
<c:set var="idOfTasks" value="${serviceOrder.id}" scope="session"/>
 <c:set var="tableName" value="miscellaneous" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
	<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<c:set var="checkPropertyAmountComponent" value="N" />
<configByCorp:fieldVisibility componentId="component.claim.claimStatus.propertyAmount">
	<c:set var="checkPropertyAmountComponent" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="checkMsgClickedValue" value="<%=request.getParameter("msgClicked") %>" />
<s:hidden name="msgClicked" id="msgClicked"/>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="miscellaneous.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.shipNumber" value="%{serviceOrder.shipNumber}"/>
    <s:hidden name="serviceOrder.registrationNumber" value="%{serviceOrder.registrationNumber}"/>
    <s:hidden name="serviceOrder.id" value="%{serviceOrder.id}"/> 
    <s:hidden name="customerFile.id"/>
    <s:hidden name="validateFlag"/>
    <s:hidden name="validateFlagDriver"/>
    <s:hidden name="validateFlagTrailer"/>
    <s:hidden name="miscellaneous.tripNumber" />
    <s:hidden name="trackingStatus.id" value="%{trackingStatus.id}"/>
    <s:hidden name="serviceOrder.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/>
    <s:hidden name="miscellaneous.sequenceNumber" value="%{serviceOrder.sequenceNumber}"/>
	<s:hidden name="serviceOrder.ship" value="%{serviceOrder.ship}"/>
	<s:hidden name="serviceOrder.createdOn" value="%{serviceOrder.createdOn}"/>
	<s:hidden name="miscellaneous.ship" value="%{serviceOrder.ship}"/>
			<s:hidden name="truckDriverList" />
			<s:hidden name="vanTruckDriverList" />
			<s:hidden name="shipSize" />
		<s:hidden name="minShip" />
		<s:hidden name="countShip"/>
   <s:hidden name="miscellaneous.id" value="<%=request.getParameter("id")%>"/>
   <s:hidden name="miscellaneous.ugwIntId" />
   <s:hidden name="id" value="<%=request.getParameter("id")%>"/>
   <s:hidden id="countInterStateNotes" name="countInterStateNotes" value="<%=request.getParameter("countInterStateNotes") %>"/>
   <c:set var="countInterStateNotes" value="<%=request.getParameter("countInterStateNotes") %>" />
   <s:hidden name="sid" value="%{serviceOrder.id}"/>
   <s:hidden id="checkSitDestinationDaysClick" name="checkSitDestinationDaysClick" />
   
   <s:hidden id="countForDomesticNotes" name="countForDomesticNotes" value="<%=request.getParameter("countForDomesticNotes") %>"/>
   
   <c:set var="countForDomesticNotes" value="<%=request.getParameter("countForDomesticNotes") %>" />
   
   <s:hidden id="countForDomesticServiceNotes" name="countForDomesticServiceNotes" value="<%=request.getParameter("countForDomesticServiceNotes") %>"/>
   
   <c:set var="countForDomesticServiceNotes" value="<%=request.getParameter("countForDomesticServiceNotes") %>" />
   
   <s:hidden id="countForDomesticStateCountryNotes" name="countForDomesticStateCountryNotes" value="<%=request.getParameter("countForDomesticStateCountryNotes") %>"/>
   
   <c:set var="countForDomesticStateCountryNotes" value="<%=request.getParameter("countForDomesticStateCountryNotes") %>" />
   
   <s:hidden name="gotoPageString" id="gotoPageString" value="" />
   <s:hidden name="formStatus" value=""/>
	<c:if test="${validateFormNav == 'OK'}" >
	<c:choose>
	<c:when test="${gotoPageString == 'gototab.serviceorder' }">
	    <c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.accounting' }">
	    <c:redirect url="/accountLineList.html?sid=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.newAccounting' }">
	    <c:redirect url="/pricingList.html?sid=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.billing' }">
	    <c:redirect url="/editBilling.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.domestic' }">
	    <c:redirect url="/editMiscellaneous.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.status' }">
			<c:if test="${serviceOrder.job =='RLO'}"> 
				 <c:redirect url="/editDspDetails.html?id=${serviceOrder.id}" />
			</c:if>
           <c:if test="${serviceOrder.job !='RLO'}"> 
				<c:redirect url="/editTrackingStatus.html?id=${serviceOrder.id}" />
			</c:if>  	    
	</c:when>
	<c:when test="${gotoPageString == 'gototab.OI' }">
	    <c:redirect url="/operationResource.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.ticket' }">
	    <c:redirect url="/customerWorkTickets.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.claims' }">
	    <c:redirect url="/claims.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.cartons' }">
	    <c:redirect url="/cartons.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.vehicles' }">
	    <c:redirect url="/vehicles.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.containers' }">
	<c:if test="${forwardingTabVal!='Y'}">
	    <c:redirect url="/containers.html?id=${serviceOrder.id}"/>
	  </c:if>
	  <c:if test="${forwardingTabVal=='Y'}">
		<c:redirect url="/containersAjaxList.html?id=${serviceOrder.id}"/>
	</c:if>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.servicepartners' }">
	    <c:redirect url="/servicePartnerss.html?id=${serviceOrder.id}"/>
	</c:when>
	<c:when test="${gotoPageString == 'gototab.customerfile' }">
	    <c:redirect url="/editCustomerFile.html?id=${customerFile.id}"/>
	</c:when>
	<c:otherwise>
	</c:otherwise>
	</c:choose>
	</c:if>
	<div id="Layer3" style="width:100%">
	<div id="newmnav" style="float: left;">       
         
             <ul>


	        <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	           <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
	             <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
               <li><a  href="redskyDashboard.html?sid=${serviceOrder.id}" ><span>Dashboard</span></a></li>
              </c:if>
               </c:if>
              </sec-auth:authComponent>
              </configByCorp:fieldVisibility>
             
              <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
              <li><a onclick="setReturnString('gototab.serviceorder');return ContainerAutoSave('none');" ><span>S/O Details</span></a></li>
              </c:if>
              <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
              <li><a onclick="setReturnString('gototab.serviceorder');return ContainerAutoSave('none');" ><span>Quotes</span></a></li>
              </c:if>
              <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit">
              	<li><a onclick="setReturnString('gototab.billing');return ContainerAutoSave('none');" ><span>Billing</span></a></li>
              </sec-auth:authComponent>
              <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
              <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
              <c:choose>
			    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
			      <li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
			    </c:when> --%>
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.accounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
	          </c:choose> 
	          </sec-auth:authComponent>
	          <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
	          <c:choose> 
			    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 			<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
				</c:when>
			   <c:otherwise> 
		          <li><a onclick="setReturnString('gototab.newAccounting');return ContainerAutoSave('none');"><span>Accounting</span></a></li>
		      </c:otherwise>
	          </c:choose> 
	          </sec-auth:authComponent> 
 <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
   	 		 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  		 <li><a onclick="setReturnString('gototab.OI');return ContainerAutoSave('none');"><span>O&I</span></a></li>
	         </sec-auth:authComponent>
	         </c:if>
	         	<c:if test="${serviceOrder.corpID!='CWMS' || (fn1:indexOf(oiJobList,serviceOrder.job)==-1 && serviceOrder.corpID=='CWMS')}"> 
              <li><a  onclick="setReturnString('gototab.containers'); return ContainerAutoSave('none');"><span>Forwarding</span></a></li>
             </c:if>
          	</c:if>
              <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
              <li id="newmnav1" style="background:#FFF"><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');" class="current"><span>Domestic<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
              </c:if>
              <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
	           <c:if test="${serviceOrder.job =='INT'}">
	             <li id="newmnav1" style="background:#FFF"><a onclick="setReturnString('gototab.domestic');return  ContainerAutoSave('none');" class="current"><span>Domestic<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	           </c:if>
	           </sec-auth:authComponent>
              <li><a onclick="setReturnString('gototab.status');return  ContainerAutoSave('none');"><span>Status</span></a></li>
              <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
              <li><a onclick="setReturnString('gototab.ticket');return ContainerAutoSave('none');"><span>Ticket</span></a></li>
              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
              <li><a onclick="setReturnString('gototab.claims');return  ContainerAutoSave('none');"><span>Claims</span></a></li>
              </configByCorp:fieldVisibility>
              </c:if>
              <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
			<c:if test="${voxmeIntergartionFlag=='true'}">
			<li><a href="inventoryDataList.html?cid=${customerFile.id}&id=${serviceOrder.id}"><span>Survey Details</span></a></li>
			 </c:if>
			 <configByCorp:fieldVisibility componentId="component.voxme.inventory">
		 		<li><a href="inventoryDetails.html?sid=${serviceOrder.id}"><span>Inventory Details</span></a></li>
	 		</configByCorp:fieldVisibility>
			</sec-auth:authComponent>
              <li><a onclick="setReturnString('gototab.customerfile');return ContainerAutoSave('none');"><span>Customer File</span></a></li>
              <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
              
              
              <!--<li><a onclick="openWindow('auditList.html?id=${miscellaneous.id}&tableName=miscellaneous&decorator=popup&popup=true',750,400)"><span>Audit</span></a></li>-->
                <li><a onclick="openWindow('auditList.html?id=${miscellaneous.id}&tableName=miscellaneous&decorator=popup&popup=true','audit','height=600,width=825,top=20, left=210, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
              <configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
  				<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
  			  </configByCorp:fieldVisibility>
              </ul>
        </div>
        <table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;"><tr>
	<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<a><img align="middle" src="images/navdisable_03.png"/></a>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<a><img align="middle" src="images/navdisable_04.png"/></a>
  		</c:if>
		</td>
		<td width="20px" align="left">
		<c:if test="${countShip != 1}" >
		<a><img class="openpopup" style="!vertical-align:bottom;" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> 
		</c:if>
		<c:if test="${countShip == 1}" >
  		<a><img align="middle" style="!vertical-align:bottom;" src="images/navdisable_05.png"/></a>
  		</c:if>
		</td>
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 1px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if>
		</tr></table>
        <div class="spn">&nbsp;</div>
        <div style="padding-bottom:0px;!padding-bottom:8px;"></div>
</div>
<div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">   
 <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%> 
<div id="newmnav">
<ul>
<li class="current"><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li></ul>
</div>
<div class="spn">&nbsp;</div>  
 <div style="padding-bottom:0px;!padding-bottom:4px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="1" cellpadding="0" border="0" style="width:100%;">
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="100%">
				<tbody>
					
					<tr>
					<td colspan="20">
					<table class="detailTabLabel" cellspacing="0" cellpadding="2" border="0" width="">
					<tr>
					<td align="right"  class="listwhitetext"><b>Target</b></td>
					<td align="left" class="listwhitetext">Begin Date</td>
					
					<td align="left" class="listwhitetext">End Date</td>
										
					<td align="left" class="listwhitetext" colspan="" style="padding-left:175px;">Actual Date</td>
					<td>&nbsp;</td>
					<td align="left" class="listwhitetext" colspan="" >Contractor</td>
						
						<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
							<c:if test="${serviceOrder.vip}">
								<div style="position:absolute;top:0px;margin-left:0px;!margin-left:-35px;">
									<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
								</div>
							</c:if>
						</td>  
					</tr>
					<tr>
						<td align="right" width="133" class="listwhitetext"><fmt:message key='trackingStatus.beginPacking'/></td>
						<c:if test="${not empty trackingStatus.beginPacking}">
						<s:text id="trackingStatusBeginPackingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginPacking"/></s:text>
						<td width="110"><s:textfield cssClass="input-text" id="beginPacking" name="beginPacking" value="%{trackingStatusBeginPackingFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" /><img id="beginPacking_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.beginPacking}">
						<td width="110"><s:textfield cssClass="input-text" id="beginPacking" name="beginPacking" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" /><img id="beginPacking_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
						
						<c:if test="${not empty trackingStatus.endPacking}">
						<s:text id="trackingStatusEndPackingFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.endPacking"/></s:text>
						<td style="width:110px"><s:textfield cssClass="input-text" id="endPacking" name="endPacking" value="%{trackingStatusEndPackingFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)" /><img id="endPacking_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.endPacking}">
						<td style="width:110px"><s:textfield cssClass="input-text" id="endPacking" name="endPacking" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"  /><img id="endPacking_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
					
						<c:if test="${not empty trackingStatus.packA}">
						<s:text id="trackingStatusPackAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.packA"/></s:text>
						<td width="90" colspan="" style="padding-left:175px; width:110px"><s:textfield cssClass="input-text" id="packA" name="packA" value="%{trackingStatusPackAFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="packA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.packA}">
						<td width="90" colspan="" style="padding-left:175px; width:110px"><s:textfield cssClass="input-text" id="packA" name="packA" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="packA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td width="60"> <s:textfield cssClass="input-text" id="packATime" name="packATime" required="true" cssStyle="width:40px;" size="4" maxlength="5" onchange="completeTimeString(this);return IsValidTime(this);" value="${packATime }" tabindex="1" /></td>						
						<td ><s:textfield cssClass="input-text" name="miscellaneous.packingDriverId"  cssStyle="width:65px;" size="9" maxlength="8" tabindex="2" onchange="valid(this,'special');validatePackingContractor();"/>
						<img align="top" class="openpopup" width="17" height="20" onclick="findPackingDriver();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.beginLoad'/></td>
						<c:if test="${not empty trackingStatus.beginLoad}">
						<s:text id="trackingStatusBeginLoadFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.beginLoad"/></s:text>
						<td><s:textfield cssClass="input-text" id="beginLoad" name="beginLoad" value="%{trackingStatusBeginLoadFormattedValue}" required="true" size="7"cssStyle="width:65px;"  maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="beginLoad_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.beginLoad}">
						<td><s:textfield cssClass="input-text" id="beginLoad" name="beginLoad" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="beginLoad_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
						
						<c:if test="${not empty trackingStatus.endLoad}">
						<s:text id="trackingStatusLoadEndFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.endLoad"/></s:text>
						<td><s:textfield cssClass="input-text" id="endLoad" name="endLoad" value="%{trackingStatusLoadEndFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="endLoad_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.endLoad}">
						<td><s:textfield cssClass="input-text" id="endLoad" name="endLoad" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="endLoad_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
						
						
					<c:if test="${not empty trackingStatus.loadA}">
						<s:text id="trackingStatusLoadAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.loadA"/></s:text>
						<td colspan="" style="padding-left:175px;"><s:textfield cssClass="input-text" id="loadA" name="loadA" value="%{trackingStatusLoadAFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="loadA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.loadA}">
						<td colspan="" style="padding-left:175px;"><s:textfield cssClass="input-text" id="loadA" name="loadA" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="loadA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td> <s:textfield cssClass="input-text" id="loadATime" name="loadATime" required="true" cssStyle="width:40px;" size="4" maxlength="5" onchange="completeTimeString(this);return IsValidTime(this);" value="${loadATime}" tabindex="3" /></td>
						<td ><s:textfield cssClass="input-text" name="miscellaneous.loadingDriverId"  cssStyle="width:65px;" size="9" maxlength="8" tabindex="4" onchange="valid(this,'special');validateLoadingContractor();" onkeypress="okValidateListFromDriver();"/>
						<img align="top" class="openpopup" width="17" height="20" onclick="findLoadingDriver();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='trackingStatus.deliveryShipper'/></td>
						<c:if test="${not empty trackingStatus.deliveryShipper}">
						<s:text id="trackingStatusDeliveryShipperFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryShipper"/></s:text>
						<td><s:textfield cssClass="input-text" id="deliveryShipper" name="deliveryShipper" value="%{trackingStatusDeliveryShipperFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryShipper_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryShipper}">
						<td><s:textfield cssClass="input-text" id="deliveryShipper" name="deliveryShipper" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryShipper_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						
						
						<c:if test="${not empty trackingStatus.deliveryLastDay}">
						<s:text id="trackingStatusDeliveryLastDayFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryLastDay"/></s:text>
						<td width="118px"><s:textfield cssClass="input-text" id="deliveryLastDay" name="deliveryLastDay" value="%{trackingStatusDeliveryLastDayFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryLastDay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryLastDay}">
						<td width="118px"><s:textfield cssClass="input-text" id="deliveryLastDay" name="deliveryLastDay" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryLastDay_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td align="left" width="255">
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2">
						<tr>
						<td class="listwhitetext" align="left" width="10">ETA</td>				
						<c:if test="${not empty trackingStatus.deliveryTA}">
						<s:text id="trackingStatusDeliveryTAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryTA"/></s:text>
						<td  align="left" width="96px" ><s:textfield cssClass="input-text" id="deliveryTA" name="deliveryTA" value="%{trackingStatusDeliveryTAFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryTA}">
						<td  align="left" width="96px"><s:textfield cssClass="input-text" id="deliveryTA" name="deliveryTA" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryTA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<td valign=""> <s:textfield cssClass="input-text" id="deliveryTATime" name="deliveryTATime" required="true" cssStyle="width:40px;" size="4" maxlength="5" onchange="completeTimeString(this);return IsValidTime(this);" value="${ deliveryTATime}" tabindex="5"/></td>
						<c:if test="${not empty trackingStatus.deliveryA}">
						<s:text id="trackingStatusDeliveryAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.deliveryA"/></s:text>
						<td width="95px"><s:textfield cssClass="input-text" id="deliveryA" name="deliveryA" value="%{trackingStatusDeliveryAFormattedValue}" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						<c:if test="${empty trackingStatus.deliveryA}">
						<td width="95px"><s:textfield cssClass="input-text" id="deliveryA" name="deliveryA" required="true" size="7" cssStyle="width:65px;" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="deliveryA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
						</c:if>
						</tr>
						</table>
						</td>
						<td> <s:textfield cssClass="input-text" id="deliveryATime" name="deliveryATime" required="true" cssStyle="width:40px;" size="4" maxlength="5" onchange="completeTimeString(this);return IsValidTime(this);" value="${deliveryATime }" tabindex="6"/></td>
						<td ><s:textfield cssClass="input-text" name="miscellaneous.deliveryDriverId"  cssStyle="width:65px;" size="9" maxlength="8" tabindex="7" onchange="valid(this,'special');validateDeliveryContractor();" onkeypress="okValidateListFromDriver();"/>
						<img align="top" class="openpopup" width="17" height="20" onclick="findDeliveryDriver();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					
						<c:if test="${empty serviceOrder.id}">
							<td align="right" width="80px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
						</c:if>
						<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when test="${countForDomesticNotes == '0' || countForDomesticNotes == '' || countForDomesticNotes == null}">
									<td align="right" width="80px"><img id="countForDomesticNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Domestic&imageId=countForDomesticNotesImage&fieldId=countForDomesticNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Domestic&imageId=countForDomesticNotesImage&fieldId=countForDomesticNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:when>
								<c:otherwise>
									<td align="right" width="80px"><img id="countForDomesticNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Domestic&imageId=countForDomesticNotesImage&fieldId=countForDomesticNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=Domestic&imageId=countForDomesticNotesImage&fieldId=countForDomesticNotes&decorator=popup&popup=true',800,600);" ></a></td>
								</c:otherwise>
							</c:choose> 
						</c:if>
					</tr>
					</table>
					
					</td>
					</tr>
					<tr>
					<td colspan="2"></td>
					<td colspan="5">
					<table class="detailTabLabel" style="">
					<tr>
					<td align="right" width="59" class="listwhitetext"></td>
					<td align="right" class="listwhitetext">Late Reason</td>
					<td>
					<c:if test="${not empty trackingStatus.lateReason}">
					<s:text id="trackingStatusLateReasonValue" name="${trackingStatus.lateReason}"><s:param name="value" value="trackingStatus.lateReason"/></s:text>
					<s:select cssClass="list-menu" name="lateReason" list="%{lateReasonList}"  value="%{trackingStatus.lateReason}" cssStyle="width:244px" headerKey="" headerValue="" tabindex="8" />
					</c:if>
					<c:if test="${empty trackingStatus.lateReason}">
					<s:select cssClass="list-menu" name="lateReason" list="%{lateReasonList}"  cssStyle="width:244px" headerKey="" headerValue="" tabindex="8" />
					</c:if>
					</td>
					<td align="right" class="listwhitetext"></td>
					<td align="right" class="listwhitetext" width="154">Party Responsible</td>
					<c:if test="${not empty trackingStatus.partyResponsible}">
					<s:text id="trackingStatusPartyResponsibleValue" name="${trackingStatus.partyResponsible}"><s:param name="value" value="trackingStatus.partyResponsible"/></s:text>
					<td colspan="2"><s:select cssClass="list-menu" name="partyResponsible" list="%{partyForResposible}" value="%{trackingStatus.partyResponsible}" cssStyle="width:70px" headerKey="" headerValue="" tabindex="9" />
					</c:if>
					<c:if test="${ empty trackingStatus.partyResponsible}">
					<td colspan="2"><s:select cssClass="list-menu" name="partyResponsible" list="%{partyForResposible}" cssStyle="width:70px" headerKey="" headerValue="" tabindex="9" />
					</c:if>
					</td>
					</tr>
					</table>
					</td>					
					</tr>
					
						<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
					<tr>	
					<td align="left" class="listwhitetext" colspan="12">
					<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2">	
					<tr>
					<td align="right" class="listwhitetext">Load&nbsp;Count</td>
					<td colspan="2"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;" name="miscellaneous.loadCount"  onkeypress="return isNumberKey(event);"  size="8" maxlength="7" tabindex="10" />
					<td align="right" class="listwhitetext">Delivery&nbsp;Count</td>
					<td colspan="2"><s:textfield cssClass="input-text" cssStyle="text-align:right;width:65px;" name="miscellaneous.deliveryCount" onkeypress="return isNumberKey(event);" size="8" maxlength="7" tabindex="11" />
					<c:set var="ischeckedG11" value="false" />
							<c:if test="${miscellaneous.g11}">
								<c:set var="ischeckedG11" value="true" />
							</c:if>
							<td align="right" class="listwhitetext" width="150">G11</td>
							<td class="listwhitetext" align="left"><s:checkbox key="miscellaneous.g11"  value="${ischeckedG11}" fieldValue="true" tabindex="16" /></td>
					</tr>
					<tr>
					<td align="right" class="listwhitetext" width="133">Load&nbsp;Signature</td>
					<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.loadSignature"  size="33" maxlength="25" tabindex="12" cssStyle="width:240px" />
					<td align="right" width="159" class="listwhitetext">Delivery&nbsp;Signature</td>
					<td colspan="4"><s:textfield cssClass="input-text" name="miscellaneous.deliverySignature"  size="33" maxlength="25" tabindex="13" cssStyle="width:241px" />
					</tr>
					</table>
					</td>
					</tr>
					
<tr>
<td colspan="12">
<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
		<td class="headtab_left" style="cursor: default;">
		</td>
		<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;SIT@Destination
		</td>
		<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
		<td class="headtab_bg_center" style="cursor: default;">&nbsp;
		</td>
		<td class="headtab_right" style="cursor: default;">
		</td>
		</tr>
	</table>

</td>

</tr>					
					
<tr>				<td align="left" class="listwhitetext" colspan="12">
					<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2">
					<tr>
					<td align="right" class="listwhitetext" width="133px"><fmt:message key='trackingStatus.sitDestinationIn'/></td>
					<c:if test="${not empty trackingStatus.sitDestinationIn}">
					<s:text id="trackingStatusSitDestinationInFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationIn"/></s:text>
					<td width="93px"><s:textfield cssClass="input-text" id="sitDestinationIn" name="sitDestinationIn" value="%{trackingStatusSitDestinationInFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/><img id="sitDestinationIn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
					</c:if>
					<c:if test="${empty trackingStatus.sitDestinationIn}">
					<td><s:textfield cssClass="input-text" id="sitDestinationIn" name="sitDestinationIn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="sitDestinationIn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="forDays(); return false;"/></td>
					</c:if>
							
										
					<td align="right" class="listwhitetext" width="102px"><fmt:message key='trackingStatus.sitDestinationA'/></td> 
					<td align="left" width="100" ><s:select cssClass="list-menu" name="sitDestinationTA" value="%{trackingStatus.sitDestinationTA}" list="%{tan}" cssStyle="width:89px" onchange="changeStatus();" tabindex="14" /></td>
					
					<c:if test="${not empty trackingStatus.sitDestinationA}">
					<s:text id="trackingStatusSitDestinationAFormattedValue" name="${FormDateValue}"><s:param name="value" value="trackingStatus.sitDestinationA"/></s:text>
					<td width="100px"><s:textfield cssClass="input-text" id="sitDestinationA" name="sitDestinationA" value="%{trackingStatusSitDestinationAFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
					<img id="sitDestinationA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['sitDestinationA'].focus(); forDays(); return false;"/></td>
					</c:if>
					<c:if test="${empty trackingStatus.sitDestinationA}">
					<td width="100px"><s:textfield cssClass="input-text" id="sitDestinationA" name="sitDestinationA" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/>
					<img id="sitDestinationA_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['sitDestinationA'].focus(); forDays(); return false;"/></td>
					</c:if>	
					
					<td align="right"><fmt:message key='trackingStatus.sitDestinationDays'/></td>
					<td align="left" width=""><s:textfield cssClass="input-text" id="sitDestDay" name="sitDestinationDays" value="%{trackingStatus.sitDestinationDays}" required="true" size="10" maxlength="5" tabindex="15" onselect="calcDays();" onchange="calcDays();"/></td>
					<td align="right" class="listwhitetext" style="width:57px;">Contractor</td>
					<td><s:textfield cssClass="input-text" name="miscellaneous.sitDestinationDriverId"  size="10" maxlength="8" tabindex="16" onchange="valid(this,'special');validateSitDestinationContractor();" onkeypress="okValidateListFromDriver();"/>
					<img align="top" class="openpopup" width="17" height="20" onclick="findSitDestinationDriver();" src="<c:url value='/images/open-popup.gif'/>" /></td>
					</tr>
					</table>
					</td>				
					
					</tr>	
									
					<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
						
						
					<tr>
					<td colspan="30">
					<table style="margin:0px;" cellspacing="0" cellpadding="2" border="0">
					
					<tr>
						<td align="right" width="133" class="listwhitetext">Hauling Agent Code</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.haulingAgentCode"  cssStyle="width:80px"  size="12" maxlength="8" tabindex="17" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'hidHAGCode');checkVendorName();getVanlineCode('OA', this);valid(this,'special');" onselect="showPartnerAlert('onchange',this.value,'hidHAGCode');" onblur="showPartnerAlert('onchange',this.value,'hidHAGCode');"/></td>
						<td><img align="top" class="openpopup" width="17" height="20" onclick="javascript:winOpenDest();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>	
						<td align="right" class="listwhitetext">Hauler Name</td>	
						<td colspan="3">
						<table class="detailTabLabel" cellpadding="0" cellspacing="0">
						<tr>
						<td><s:textfield cssClass="input-text" name="miscellaneous.haulerName" cssStyle="width:314px" size="72" maxlength="50" tabindex="18" /></td>
						<td align="left">
							<div id="hidHAGCode" style="vertical-align:middle;"><a><img class="openpopup" onclick="getPartnerAlert(document.forms['miscellaneousForm'].elements['miscellaneous.haulingAgentCode'].value,'hidHAGCode');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a></div>
						</td>
						<script type="text/javascript">
							showPartnerAlert('onload','${miscellaneous.haulingAgentCode}','hidHAGCode');
						</script>
						</tr>
						</table>
						</td>
					</tr>
					<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
					 	<c:if test="${serviceOrder.job=='DOM' || serviceOrder.job=='UVL' || serviceOrder.job == 'MVL' || serviceOrder.job == 'VAI'}">
						 	<tr>
									<td align="right" class="listwhitetext" >Hauling Agent Vanline Code</td>
									<td colspan="3"><s:textfield cssClass="input-text" key="miscellaneous.haulingAgentVanlineCode" cssStyle="width:80px"  size="12" maxlength="8" onchange="valid(this,'special');getVanlineAgent('OA');" tabindex="18"/>
									<a><img align="top" class="openpopup" id="navigationOA" onclick="getVanlineCode('OA', this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Vanline List" title="Vanline List" /></a> 
									</td>
							</tr>
						</c:if>
					</configByCorp:fieldVisibility>
					<!--<tr>
					<td align="right" class="listwhitetext">Truck/Carrier</td>
					<td><s:textfield cssClass="input-text" name="miscellaneous.carrier" id="validCarrier" size="12" maxlength="15" tabindex="" onchange="valid(this,'special');carrierListInDomestic();" onkeypress="okValidateListFromTruck();"/>
					<img align="top" class="openpopup" width="17" height="20" onclick="findTruckNumber();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					<td align="right" class="listwhitetext">Carrier Name</td>
					<td><s:textfield cssClass="input-text" name="miscellaneous.carrierName" readonly="true" size="40" maxlength="50" tabindex=""/></td>
					</tr>					
					-->
					<tr>
					<c:if test="${serviceOrder.job!='HVY'}">
						<td align="right" class="listwhitetext">Driver ID</td>
						<td width=""><s:textfield cssClass="input-text" name="miscellaneous.driverId"  cssStyle="width:80px"  size="12" maxlength="8" tabindex="19" onchange="valid(this,'special');ownerOperatorListInDomestic();" onkeypress="okValidateListFromDriver();"/></td>
						<td><img align="top" class="openpopup" width="17" height="20" onclick="findDriverFromPartner();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="right" class="listwhitetext">Driver Name</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.driverName"  cssStyle="width:170px" size="30" maxlength="50" tabindex="20"/></td>
						
						<td colspan="">						
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin:0px;">
						<tr>
						<td align="right" class="listwhitetext" width="60">Assign</td>						
						<%-- <s:textfield cssClass="input-text" id="assignDate" name="miscellaneous.assignDate"  size="7" maxlength="11" tabindex="20" onkeydown="return onlyDel(event,this)"/><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/> --%>
						<c:if test="${not empty miscellaneous.assignDate}">
					<s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"><s:param name="value" value="miscellaneous.assignDate"/></s:text>
					<td><s:textfield cssClass="input-text" id="assignDate" name="miscellaneous.assignDate" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" required="true" cssStyle="width:70px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
					<td><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['assignDate'].focus(); forDays(); return false;"/></td>
					</c:if>
					<c:if test="${empty miscellaneous.assignDate}">
					<td><s:textfield cssClass="input-text" id="assignDate" name="miscellaneous.assignDate" required="true" cssStyle="width:70px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
					<td><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['miscellaneous.assignDate'].focus(); forDays(); return false;"/></td>
					</c:if>							
						</tr>
						</table>
						</td>
						
						<td colspan="2">						
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin:0px;">
						<tr>
						<td align="right" class="listwhitetext" style="width:35px;!width:36px;">Cell&nbsp;#</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.driverCell"  cssStyle="width:107px;" size="18" maxlength="20" tabindex="21"/></td>
						</tr>
						</table>
						</td>
					</c:if>
					<c:if test="${serviceOrder.job=='HVY'}">
						<td align="right" class="listwhitetext">Driver ID</td>
						<td width=""><s:textfield cssClass="input-text" name="miscellaneous.driverId"  cssStyle="width:80px"  size="12" maxlength="8" tabindex="19"  onchange="valid(this,'special');ownerOperatorListFromTruck();" onkeypress="okValidateListFromDriver();"/></td>
						<td><img align="top" class="openpopup" width="17" height="20" onclick="findDriverListFromTruck();" src="<c:url value='/images/open-popup.gif'/>" /></td>
						<td align="right" class="listwhitetext">Driver Name</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.driverName"  cssStyle="width:170px" size="30" maxlength="50" tabindex="20"/></td>
						<td colspan="">						
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin:0px;">
						<tr>
						<td align="right" class="listwhitetext" width="60">Assign</td>
							<c:if test="${not empty miscellaneous.assignDate}">
					<s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"><s:param name="value" value="miscellaneous.assignDate"/></s:text>
					<td><s:textfield cssClass="input-text" id="assignDate" name="miscellaneous.assignDate" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" required="true" cssStyle="width:70px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
					<td><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['assignDate'].focus(); forDays(); return false;"/></td>
					</c:if>
					<c:if test="${empty miscellaneous.assignDate}">
					<td><s:textfield cssClass="input-text" id="assignDate" name="miscellaneous.assignDate" required="true" cssStyle="width:70px" maxlength="11" onkeydown="return onlyDel(event,this)" onselect="calcDays()"/></td>
					<td><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20 onclick="document.forms['miscellaneousForm'].elements['miscellaneous.assignDate'].focus(); forDays(); return false;"/></td>
					</c:if>
					</tr>
						</table>
						</td>	
						<%-- <s:textfield cssClass="input-text" id="assignDate"  name="miscellaneous.assignDate"  size="7" maxlength="11" tabindex="20" onkeydown="return onlyDel(event,this)"/><img id="assignDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/> --%></td>
					    <td colspan="2">
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin:0px;">
						<tr>
					    <td align="right" class="listwhitetext" width="35">Cell&nbsp;#</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.driverCell"  cssStyle="width:107px;" size="18" maxlength="20" tabindex="21"/></td>
						</tr>
						</table>
						</td>
					</c:if>
					<td colspan="2" style="min-width:29px;">
						<configByCorp:fieldVisibility componentId="component.button.SSCW.showAdvances">
							<input type="button" class="cssbuttonA" style="width:95px; height:21px;font-size:11px;"  name="Show Advances"  value="Show Advances" onclick="getAdvDtls(this)" />
						</configByCorp:fieldVisibility>
					</td>
					</tr>
					<%--
					<tr>
					<td align="right" class="listwhitetext">Last Location</td>
					<td align="left" colspan="2" class="listwhitetext">
						<table cellspacing="0" cellpadding="0" border="0" class="detailTabLabel" width="100%">
						<tbody><tr>			
					<td width="50%"><s:textfield cssClass="input-textUpper" name="miscellaneous.vanLastLocation"  size="24" readonly="true"/></td>
					<td align="right"  class="listwhitetext">Report On</td>					
						</tr>
						</tbody></table>
						</td>						
					<c:if test="${not empty miscellaneous.vanLastReportOn}">
					<s:text id="miscellaneousvanLastLocationFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.vanLastReportOn"/></s:text>
					<td width="100px"><s:textfield cssClass="inputTextDate" id="vanLastReportOn" name="miscellaneous.vanLastReportOn"
					 value="%{miscellaneousvanLastLocationFormattedValue}"  required="true" cssStyle="width:56px" maxlength="11" readonly="true"/>
					 <s:textfield cssClass="inputTextDate" name="miscellaneous.vanLastReportTime" readonly="true"/></td>
					</c:if>
					<c:if test="${empty miscellaneous.vanLastReportOn}">
					<td width="100px"><s:textfield cssClass="inputTextDate" id="vanLastReportOn" name="miscellaneous.vanLastReportOn"  
					required="true" cssStyle="width:56px" maxlength="11" readonly="true"/>
					<s:textfield cssClass="inputTextDate" name="miscellaneous.vanLastReportTime" readonly="true"/></td>
					</c:if>					
					<td align="right" class="listwhitetext">Avail.&nbsp;Cube</td>
					<td colspan="3"><s:textfield cssClass="input-textUpper" name="miscellaneous.vanAvailCube"  size="12" readonly="true"/></td>
					</tr>
					 --%>
					 <tr>
					<td align="right" class="listwhitetext">Truck Agent</td>
					<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.tractorAgentVanLineCode" onchange="return checkLength(this),findPartnerNameForTruckAgent();" cssStyle="width:80px"  size="12"  maxlength="8" tabindex="22" />
					<td align="right" class="listwhitetext">Truck Agent Name</td>					
					<td colspan="7">
					<table class="detailTabLabel" cellpadding="0" cellspacing="0">
					<tr>					
					<td align="left"><s:textfield cssClass="input-textUpper" name="miscellaneous.tractorAgentName" readonly="true" cssStyle="width:170px" size="30" tabindex="23"/>
					<td>&nbsp;</td>
					<td align="right" class="listwhitetext">Truck/Carrier&nbsp;</td>
					<td align="left"><s:textfield cssClass="input-text" name="miscellaneous.carrier" id="validCarrier" cssStyle="width:70px"  size="10" maxlength="15" tabindex="24" onchange="valid(this,'special');carrierListInDomestic();" onkeypress="okValidateListFromTruck();"/>
					<td>&nbsp;<img align="top" class="openpopup" width="17" height="20" onclick="findTruckNumber();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					<td align="right" class="listwhitetext" style="width:75px;!width:70px;">Carrier Name&nbsp;</td>
					<td align="left"><s:textfield cssClass="input-textUpper" cssStyle="width:186px" name="miscellaneous.carrierName" readonly="true" size="25" maxlength="50" tabindex="25"/>
					</tr>
					</table>
					</td>					
					 </tr>
					 
					 <tr><td align="right" class="listwhitetext">Van Agent</td>
					<td colspan="2"><s:textfield cssClass="input-text" cssStyle="width:80px"  name="miscellaneous.vanAgentVanLineCode" onchange="return checkLength(this),findPartnerNameForVanAgent();" size="12" maxlength="8" tabindex="26" />
					<td align="right" class="listwhitetext">Van Agent Name</td>
					<td colspan="8">
					<table class="detailTabLabel" cellpadding="0" cellspacing="0">
					<tr>					
					<td align="left"><s:textfield cssClass="input-textUpper" name="miscellaneous.vanAgentName"  cssStyle="width:170px" size="30" readonly="true" tabindex="27" />
					<td style="width:43px;">&nbsp;</td>
					<td align="right" class="listwhitetext">Van#&nbsp;</td>
					<td align="left"><s:textfield cssClass="input-text" name="miscellaneous.vanID" cssStyle="width:70px" size="10" onchange="valid(this,'special');vanListInDomestic();" tabindex="28"/>
					<td>&nbsp;<img align="top" class="openpopup" width="17" height="20" onclick="findVanId();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					<td style="width:22px;!width:15px;">&nbsp;</td>
					<td align="right" class="listwhitetext">Van&nbsp;Name&nbsp;</td>
					<td align="left"><s:textfield cssClass="input-textUpper" cssStyle="width:186px"  name="miscellaneous.vanName" readonly="true" size="25" maxlength="8"  tabindex="29" />
					 </tr>
					</table>
					</td>
					 </tr>
					 
					<tr>
					<td class="listwhitetext" align="right">Settlement Status</td>
					<td  align="left" colspan="2"><s:select cssClass="list-menu" name="miscellaneous.status" list="%{status}" cssStyle="width:85px" onchange="changeStatus();" tabindex="30" headerKey="" headerValue=""/></td>
					
					<td align="right" class="listwhitetext">Settlement Date</td>
	<c:if test="${not empty miscellaneous.settledDate}">
			<s:text id="miscellaneoussettledDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.settledDate"/></s:text>
			<td width="100px"><s:textfield cssClass="input-textUpper" id="settledDate" name="miscellaneous.settledDate" value="%{miscellaneoussettledDateFormattedValue}"  required="true" cssStyle="width:65px" maxlength="11" readonly="true"/><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<c:if test="${empty miscellaneous.settledDate}">
			<td width="100px"><s:textfield cssClass="input-textUpper" id="settledDate" name="miscellaneous.settledDate"  required="true" cssStyle="width:65px" maxlength="11" readonly="true"/><img id="settledDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
					
					</tr>
					<tr>
					<c:if test="${serviceOrder.job=='HVY'}">
					<td align="right" class="listwhitetext" width="133px"><fmt:message key='miscellaneous.trailer'/></td>
	                <td><s:textfield cssClass="input-text" name="miscellaneous.trailer"  cssStyle="width:80px"  size="12" maxlength="15" onchange="valid(this,'special');trailerCodeInDomestic();" onkeypress="okValidateListFromTrailer();" tabindex="31"/></td>
	                <td><img align="top" class="openpopup" width="17" height="20" onclick="findTruckNumber1();changeStatus()" id="openpopup4.img" src="<c:url value='/images/open-popup.gif'/>" /></td>
					</c:if>
					<c:if test="${serviceOrder.job!='HVY'}">
					<td width="133px"></td><td></td><td></td>
					</c:if>
					<td></td><td></td>
					<td colspan="5">
					<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="2" style="margin-left:12px">
					<tr>
						
					<td style="width:120px;">&nbsp;</td>
					<td align="right" class="listwhitetext">Self Haul</td>
					<td><s:textfield cssClass="input-text" name="miscellaneous.selfHaul"  cssStyle="width:90px"  size="14" maxlength="5" tabindex="32"/></td>
					</tr>
					</table>
					</td>
					</tr>
					
					</table>
					</td>
					</tr>
					
					<tr>
						<tr><td align="center" colspan="10" class="vertlinedata"></td></tr>
					</tr>
					
					<tr>
					<td colspan="30">
					<table style="margin:0px;" cellspacing="0" cellpadding="2" border="0">
					
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.domesticInstruction'/></td>
						<td class="listwhitetext" colspan="2" align="left">
						<table class="detailTabLabel" cellspacing="0" border="0" cellpadding="0" width="100%"> 
						<tr>
						<td align="left" width="50%"><s:select cssClass="list-menu" name="miscellaneous.domesticInstruction" list="%{DOMINST}" headerKey="" headerValue="" cssStyle="width:165px" onchange="changeStatus();" tabindex="33" /></td>
						<td align="right"><fmt:message key='miscellaneous.vanLinePickupNumber'/></td>
						</tr>
						</table>
						</td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.vanLinePickupNumber"  cssStyle="width:105px;" size="18" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="34" /></td>
						<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.codeHauling'/></td>
						<td><s:textfield cssClass="input-text" name="miscellaneous.codeHauling"  cssStyle="width:90px;" size="12" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="35" /></td>
					</tr>


<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.atlasDown'/></td>
	<c:if test="${not empty miscellaneous.atlasDown}">
			<s:text id="miscellaneousAtlasDownFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.atlasDown"/></s:text>
			<td width="100px"><s:textfield cssClass="input-text" id="atlasDown" name="miscellaneous.atlasDown" value="%{miscellaneousAtlasDownFormattedValue}"  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="atlasDown_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>

			</c:if>
			<c:if test="${empty miscellaneous.atlasDown}">
			<td width="100px"><s:textfield cssClass="input-text" id="atlasDown" name="miscellaneous.atlasDown"  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="atlasDown_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
			<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.vanLineNationalCode'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.vanLineNationalCode"  cssStyle="width:105px;" size="18" maxlength="14" tabindex="36" /></td>
	
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.pendingNumber'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.pendingNumber" cssStyle="width:90px;" size="12" maxlength="11" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="37" /></td>
	
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.shipmentRegistrationDate'/></td>
		<c:if test="${not empty miscellaneous.shipmentRegistrationDate}">
			<s:text id="customerFileMoveDateFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.shipmentRegistrationDate"/></s:text>
			<td><s:textfield cssClass="input-text" id="shipmentRegistrationDate" name="miscellaneous.shipmentRegistrationDate" value="%{customerFileMoveDateFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="shipmentRegistrationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>

			</c:if>
			<c:if test="${empty miscellaneous.shipmentRegistrationDate}">
			<td><s:textfield cssClass="input-text" id="shipmentRegistrationDate" name="miscellaneous.shipmentRegistrationDate"  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="shipmentRegistrationDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
			</c:if>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.paperWork'/></td>
	<td colspan="5"><s:textfield cssClass="input-text" name="miscellaneous.paperWork" cssStyle="width:385px" size="81" maxlength="50"  tabindex="38" /></td>		
	<!--<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.vanLineOrderNumber'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.vanLineOrderNumber" size="18" maxlength="12"  tabindex="" /></td>
	--></tr>
	<tr>
	<td align="right" class="listwhitetext" width=""><fmt:message key='miscellaneous.vanLineCODAmount'/></td>
	<td ><s:textfield cssClass="input-text" name="miscellaneous.vanLineCODAmount"  cssStyle="width:87px" size="7" maxlength="7" onchange="onlyFloat(this);" tabindex="39" /></td>
	<td align="right" class="listwhitetext" width="100px">Estimated&nbsp;Line&nbsp;Haul</td>
	<td ><s:textfield cssClass="input-text" name="miscellaneous.estimatedRevenue"  cssStyle="width:105px;" size="18" maxlength="15" onchange="onlyFloat(this);" tabindex="40"/></td>
	<s:hidden name="miscellaneous.totalDiscountPercentage" />
	
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.transDocument'/></td>
	<c:if test="${not empty miscellaneous.transDocument}">
	<s:text id="miscellaneousTransDocumentFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.transDocument"/></s:text>
	<td><s:textfield cssClass="input-text" id="transDocument" name="miscellaneous.transDocument" value="%{miscellaneousTransDocumentFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="transDocument_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	<c:if test="${empty miscellaneous.transDocument}">
	<td><s:textfield cssClass="input-text" id="transDocument" name="miscellaneous.transDocument"  required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="transDocument_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
	</c:if>
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.tarif'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.tarif" cssStyle="width:87px" size="7" maxlength="4" onkeydown="return onlyAlphaNumericAllowed(event)" tabindex="41" /></td>
	
	<td align="right" class="listwhitetext" width="163px">Total&nbsp;Estimated&nbsp;Revenue</td>
	<td ><s:textfield cssClass="input-text" name="miscellaneous.estimatedExpense"  cssStyle="width:105px;" size="18" maxlength="15" onchange="onlyFloat(this);" tabindex="40"/></td>
	<td align="right" class="listwhitetext" width="177px"><fmt:message key='miscellaneous.needWaiveEstimate'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.needWaiveEstimate" list="%{yesno}" cssStyle="width:95px" onchange="changeStatus();" tabindex="42" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.section'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.section" size="7" maxlength="2" onchange="onlyNumeric(this);" tabindex="43" /></td>
	
	</tr>
	<tr>
	<td align="right" class="listwhitetext" width="133px">Actual&nbsp;Line&nbsp;Haul</td>
	<td ><s:textfield cssClass="input-text" name="miscellaneous.actualLineHaul"  size="7" maxlength="5" cssStyle="text-align:right;width:87px;" onchange="onlyFloat(this);" tabindex="40"/></td>
	<td align="right" class="listwhitetext" width="100px">Total&nbsp;Actual&nbsp;Revenue</td>
	<td ><s:textfield cssClass="input-text" name="miscellaneous.totalActualRevenue"  cssStyle="width:105px;" size="18" maxlength="15" onchange="onlyFloat(this);" tabindex="40"/></td>
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.needWaiveSurvey'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.needWaiveSurvey" list="%{yesno}" cssStyle="width:91px" onchange="changeStatus();" tabindex="44" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.extraStopYN'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.extraStopYN" list="%{yesno}" cssStyle="width:109px" onchange="changeStatus();" tabindex="45" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.needWaiveSRA'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.needWaiveSRA" list="%{yesno}" cssStyle="width:95px" onchange="changeStatus();" tabindex="46" /></td>
	</tr>
	
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.mile'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.mile" cssStyle="width:87px" size="7" maxlength="7" onchange="onlyFloat(this);" tabindex="47" /></td>
	
	<configByCorp:fieldVisibility componentId="component.field.discountCWMS">
	<td  class="listwhitetext" align="right"  >Discount&nbsp;%</td>
 	<td><s:textfield cssClass="input-text" name="miscellaneous.discount"  cssStyle="width:105px;" size="12" maxlength="5"  onkeydown="return onlyFloatNumsAllowed(event)"/></td>
	</configByCorp:fieldVisibility>
	<%--<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.origin24Hr'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.origin24Hr" list="%{yesno}" cssStyle="width:65px" onchange="changeStatus();" tabindex="" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.destination24HR'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.destination24HR" list="%{yesno}" cssStyle="width:65px" onchange="changeStatus();" tabindex="" /></td>
	
	--%>
	</tr>
	
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.trailerVolumeWeight'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.trailerVolumeWeight" cssStyle="width:87px" size="7" maxlength="7" onchange="onlyFloat(this);" tabindex="48" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.agentPerformNonperform'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.agentPerformNonperform" list="%{AGTPER}" cssStyle="width:109px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="49" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.peakRate'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.peakRate" list="%{Peak}" cssStyle="width:95px" onchange="changeStatus();" tabindex="50" /></td>

	<td align="right" width="80px"></td>
	</tr>
	
	</table>
	</td>
	</tr>
	
<c:if test="${serviceOrder.job =='GST' || serviceOrder.job =='VAI' || serviceOrder.job =='UVL' || serviceOrder.job =='MVL' || serviceOrder.job =='LOC' || serviceOrder.job =='LOF' || serviceOrder.job =='LOF' || serviceOrder.job =='LOI'}">
 	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%;margin: 0px;">
	<tbody>
	<tr>
	<td height="10" width="100%" align="left" style="margin: 0px;" >
	<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('interstate')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Interstate</a></div>
	--><div  onClick="javascript:animatedcollapse.toggle('interstate')" style="margin: 0px;">
 		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px;">
		<tr>
		<td class="headtab_left">
		</td>
		<td NOWRAP class="headtab_center">&nbsp;Interstate
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
	</table>
		</div>
	<div  id="interstate">
	<table class="detailTabLabel" cellspacing="3" cellpadding="0" border="0" style="width:86%">
	<tbody>	
<tr>

<td align="right" width="130px" class="listwhitetext"><fmt:message key='miscellaneous.recivedPO'/></td>
<c:if test="${not empty miscellaneous.recivedPO}">
<s:text id="miscellaneousRecivedPOFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.recivedPO"/></s:text>
<td width="70px"><s:textfield cssClass="input-text" id="recivedPO" name="miscellaneous.recivedPO" value="%{miscellaneousRecivedPOFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedPO_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.recivedPO}">
<td width="70px"><s:textfield cssClass="input-text" id="recivedPO" name="miscellaneous.recivedPO" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedPO_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td width=""></td>
<td align="right" width="244px" class="listwhitetext"><fmt:message key='miscellaneous.recivedHVI'/></td>
<c:if test="${not empty miscellaneous.recivedHVI}">
<s:text id="miscellaneousRecivedHVIFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.recivedHVI"/></s:text>
<td width="70px"><s:textfield cssClass="input-text" id="recivedHVI" name="miscellaneous.recivedHVI" value="%{miscellaneousRecivedHVIFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedHVI_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.recivedHVI}">
<td width="70px"><s:textfield cssClass="input-text" id="recivedHVI" name="miscellaneous.recivedHVI" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedHVI_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
</tr>
<tr>

<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.confirmOriginAgent'/></td>
<c:if test="${not empty miscellaneous.confirmOriginAgent}">
<s:text id="miscellaneousConfirmOriginAgentFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.confirmOriginAgent"/></s:text>
<td><s:textfield cssClass="input-text" id="confirmOriginAgent" name="miscellaneous.confirmOriginAgent" value="%{miscellaneousConfirmOriginAgentFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="confirmOriginAgent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.confirmOriginAgent}">
<td><s:textfield cssClass="input-text" id="confirmOriginAgent" name="miscellaneous.confirmOriginAgent" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="confirmOriginAgent_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>

</tr>
<tr>

<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.quoteOn'/></td>
<c:if test="${not empty miscellaneous.quoteOn}">
<s:text id="miscellaneousQuoteOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.quoteOn"/></s:text>
<td><s:textfield cssClass="input-text" id="quoteOn" name="miscellaneous.quoteOn" value="%{miscellaneousQuoteOnFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="quoteOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.quoteOn}">
<td><s:textfield cssClass="input-text" id="quoteOn" name="miscellaneous.quoteOn" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="quoteOn_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td width="100px"></td>
<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.recivedPayment'/></td>
<c:if test="${not empty miscellaneous.recivedPayment}">
<s:text id="miscellaneousRecivedPaymentFormattedValue" name="${FormDateValue}"><s:param name="value" value="miscellaneous.recivedPayment"/></s:text>
<td><s:textfield cssClass="input-text" id="recivedPayment" name="miscellaneous.recivedPayment" value="%{miscellaneousRecivedPaymentFormattedValue}" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedPayment_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<c:if test="${empty miscellaneous.recivedPayment}">
<td><s:textfield cssClass="input-text" id="recivedPayment" name="miscellaneous.recivedPayment" required="true" cssStyle="width:65px" maxlength="11" onkeydown="return onlyDel(event,this)"/></td><td><img id="recivedPayment_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
</c:if>
<td></td><td></td>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countInterStateNotes == '0' || countInterStateNotes == '' || countInterStateNotes == null}">
<td align="left" width="140px"><img id="countInterStateNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=InterState&imageId=countInterStateNotesImage&fieldId=countInterStateNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=InterState&imageId=countInterStateNotesImage&fieldId=countInterStateNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="left" width="140px"><img id="countInterStateNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=InterState&imageId=countInterStateNotesImage&fieldId=countInterStateNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=InterState&imageId=countInterStateNotesImage&fieldId=countInterStateNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
</tr>

	</tbody></table>
	</div>
	
	</td>
	</tr>

	</tbody>
	</table>
	</c:if>
	
	
	
	
	
	
<s:hidden name="miscellaneous.millitaryShipment"/>
<s:hidden name="miscellaneous.dp3"/>
<s:hidden name="miscellaneous.shortFuse"/>
<s:hidden name="miscellaneous.fromNonTempStorage"/>
<s:hidden name="miscellaneous.weaponsIncluded"/>
<s:hidden name="miscellaneous.originDocsToBase"/>
<s:hidden name="miscellaneous.dd1840"/>
<s:hidden name="miscellaneous.millitarySurveyDate"/>
<s:hidden name="miscellaneous.millitarySurveyResults"/>
<s:hidden name="miscellaneous.preApprovalDate1"/>
<s:hidden name="miscellaneous.preApprovalRequest1"/>
<s:hidden name="miscellaneous.preApprovalDate2"/>
<s:hidden name="miscellaneous.preApprovalRequest2"/>
<s:hidden name="miscellaneous.preApprovalDate3"/>
<s:hidden name="miscellaneous.preApprovalRequest3"/>
<s:hidden name="miscellaneous.printGbl"/>
<s:hidden name="miscellaneous.actualPickUp"/>
<s:hidden name="miscellaneous.millitaryActualWeightDate"/>
<s:hidden name="miscellaneous.millitarySitDestinationDate"/>
<s:hidden name="miscellaneous.millitaryDeliveryDate"/>
	
<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%;">
	<tbody>
	<tr>
	<td height="10" width="100%" align="left" >
	<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('services')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;Services</a></div>
	--><div  onClick="javascript:animatedcollapse.toggle('services')" style="margin: 0px">
  		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
			<tr>
			<td class="headtab_left">
			</td>
			<td NOWRAP class="headtab_center">&nbsp;Services
			</td>
			<td width="28" valign="top" class="headtab_bg"></td>
			<td class="headtab_bg_center">&nbsp;
			</td>
			<td class="headtab_right">
			</td>
			</tr>
		</table>
			</div>
	<div  id="services">
	<table class="detailTabLabel" cellspacing="3" cellpadding="0" border="0" style="margin-left:41px; width:909px">
	<tbody>	
	<tr>
	<td align="right" class="listwhitetext" width="94px"><fmt:message key='miscellaneous.bookerOwnAuthority'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.bookerOwnAuthority" list="%{yesno}" cssStyle="width:70px" onchange="changeStatus();" tabindex="51" /></td>
	<td></td><td align="right" class="listwhitetext" width="71px"><fmt:message key='miscellaneous.packingBulky'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.packingBulky" list="%{yesno}" cssStyle="width:70px" onchange="changeStatus();" tabindex="52" /></td>
	<td></td><td align="right" class="listwhitetext" width="70px"><fmt:message key='miscellaneous.packAuthorize'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.packAuthorize" list="%{Howpack}" cssStyle="width:75px" headerKey="" headerValue=""  onchange="changeStatus();" tabindex="53" /></td>
	<td align="right" class="listwhitetext" width="133px"><fmt:message key='miscellaneous.applicationNumber3rdParty'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.applicationNumber3rdParty" list="%{yesno}" cssStyle="width:70px" onchange="changeStatus();" tabindex="54" /></td>
	</tr>
	<tr>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.bookerSelfPacking'/></td>
	<td width="50px"><s:textfield cssClass="input-text" name="miscellaneous.bookerSelfPacking" cssStyle="width:65px" size="7" maxlength="8" onchange="onlyNumeric(this);" tabindex="55" /></td>
	<td width="10px"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('originPartners.html?origin=${serviceOrder.originCountryCode}&partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=miscellaneous.bookerSelfPacking');" src="<c:url value='/images/open-popup.gif'/>" /></td>
	<td align="right" class="listwhitetext"><fmt:message key='miscellaneous.gate'/></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.gate" cssStyle="width:65px" size="7" maxlength="3" tabindex="56" /></td>
	<td></td><td align="right" class="listwhitetext"><fmt:message key='miscellaneous.unPack'/></td>
	<td><s:select cssClass="list-menu" name="miscellaneous.unPack" list="%{Howpack}" cssStyle="width:75px" headerKey="" headerValue="" onchange="changeStatus();" tabindex="57" /></td>
	<td></td>
	<td></td>
	<c:if test="${empty serviceOrder.id}">
<td align="right" width="152px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countForDomesticServiceNotes == '0' || countForDomesticServiceNotes == '' || countForDomesticServiceNotes == null}">
<td align="right" width="152px"><img id="countForDomesticServiceNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomService&imageId=countForDomesticServiceNotesImage&fieldId=countForDomesticServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomService&imageId=countForDomesticServiceNotesImage&fieldId=countForDomesticServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="152px"><img id="countForDomesticServiceNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomService&imageId=countForDomesticServiceNotesImage&fieldId=countForDomesticServiceNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomService&imageId=countForDomesticServiceNotesImage&fieldId=countForDomesticServiceNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
	</tr>
	</tbody></table>
	</div>
	
	</td>
	</tr>
	
	<tr>
	<td>
	<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" style="width:100%">
	<tbody>
		<tr>
			
	<td height="10" width="100%" align="left" >
	<!--<div class="subcontent-tab" onClick="javascript:animatedcollapse.toggle('state')" style="cursor: pointer"><a href="javascript:;"><img src="${pageContext.request.contextPath}/images/collapse_close.jpg" align="absmiddle" border="0"/></span>&nbsp;&nbsp;State/Country</a></div>
	--><div  onClick="javascript:animatedcollapse.toggle('state')" style="margin: 0px">
 		<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
		<td class="headtab_left">
		</td>
		<td NOWRAP class="headtab_center">&nbsp;State/Country
		</td>
		<td width="28" valign="top" class="headtab_bg"></td>
		<td class="headtab_bg_center">&nbsp;
		</td>
		<td class="headtab_right">
		</td>
		</tr>
	</table>
		</div>
	
	<div  id="state">
	<table class="detailTabLabel" cellspacing="3" cellpadding="0" border="0" style="width:950px; margin-left:0px">
	<tbody>
	<tr>
	<td align="right" class="listwhitebox" width="137px"><fmt:message key='miscellaneous.Origin'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.originStateCode" size="3" maxlength="5" tabindex="58" /></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.originState" size="2" maxlength="2" tabindex="59" /></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.originCountyCode" size="3" maxlength="5"  tabindex="60" /></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.originCounty" size="3" maxlength="5"  tabindex="61" /></td>
	
	<td align="right" colspan="2" class="listwhitebox" style="width:157px"><fmt:message key='miscellaneous.Destination'/></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.destinationStateCode" size="3" maxlength="5" tabindex="62" /></td>
	<td colspan="2"><s:textfield cssClass="input-text" name="miscellaneous.destinationState" size="2" maxlength="2"  tabindex="63" /></td>
	<td><s:textfield cssClass="input-text" name="miscellaneous.destinationCountyCode" size="3" maxlength="5" tabindex="64" /></td>
	<td width=""><s:textfield cssClass="input-text" name="miscellaneous.destinationCounty" size="3" maxlength="5" tabindex="65" /></td>
	
<c:if test="${empty serviceOrder.id}">
<td align="right" width="60px"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();"/></td>
</c:if>
<c:if test="${not empty serviceOrder.id}">
<c:choose>
<c:when test="${countForDomesticStateCountryNotes == '0' || countForDomesticStateCountryNotes == '' || countForDomesticStateCountryNotes == null}">
<td align="right" width="60px"><img id="countForDomesticStateCountryNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomStateCountry&imageId=countForDomesticStateCountryNotesImage&fieldId=countForDomesticStateCountryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomStateCountry&imageId=countForDomesticStateCountryNotesImage&fieldId=countForDomesticStateCountryNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:when>
<c:otherwise>
<td align="right" width="60px"><img id="countForDomesticStateCountryNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50 onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomStateCountry&imageId=countForDomesticStateCountryNotesImage&fieldId=countForDomesticStateCountryNotes&decorator=popup&popup=true',800,600);"/><a onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=DomStateCountry&imageId=countForDomesticStateCountryNotesImage&fieldId=countForDomesticStateCountryNotes&decorator=popup&popup=true',800,600);" ></a></td>
</c:otherwise>
</c:choose> 
</c:if>
	</tr>
	
	
	</tbody>
</table>
	
</div>
</td>								
	
	</tbody>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
	
	</tbody>
	</table>
	</div>

<div class="bottom-header" style="margin-top:30px;!margin-top:45px;"><span></span></div>
</div>
</div> 
	
	<table>
	
<tbody>
                <tr>
                <td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='miscellaneous.createdOn'/></b></td>
							<td style="width:120px">
								<fmt:formatDate var="miscellaneousCreatedOnFormattedValue" value="${miscellaneous.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="miscellaneous.createdOn" value="${miscellaneousCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${miscellaneous.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>	
							<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='miscellaneous.createdBy' /></b></td>
							<c:if test="${not empty miscellaneous.id}">
								<s:hidden name="miscellaneous.createdBy"/>
								<td style="width:90px"><s:label name="createdBy" value="%{miscellaneous.createdBy}"/></td>
							</c:if>
							<c:if test="${empty miscellaneous.id}">
								<s:hidden name="miscellaneous.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:90px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.updatedOn'/></b></td>
								<fmt:formatDate var="miscellaneousupdatedOnFormattedValue" value="${miscellaneous.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="miscellaneous.updatedOn" value="${miscellaneousupdatedOnFormattedValue}"/>
							<td style="width:120px"><fmt:formatDate value="${miscellaneous.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							
							<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='serviceOrder.updatedBy' /></b></td>
							<c:if test="${not empty miscellaneous.id}">
							<s:hidden name="miscellaneous.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{miscellaneous.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty miscellaneous.id}">
							<s:hidden name="miscellaneous.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
					</table>
             </td>
             </tr>
             </tbody>
             </table>
             </div>
     <s:submit cssClass="cssbuttonA" cssStyle="width:55px; height:25px" method="save" key="button.save" tabindex="66"/>
     
     <td></td>
	<s:reset type="button" cssClass="cssbutton1" tabindex="67" cssStyle="width:55px; height:25px" key="Reset" onmousemove="myDate();"/>
	     
 <s:hidden name="description" />
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="eightDescription" />
<s:hidden name="tenthDescription" /><s:hidden name="ninthDescription" />
<s:hidden name="miscellaneous.rwghGross"/>
<s:hidden name="miscellaneous.actualGrossWeight"/>
<s:hidden name="miscellaneous.estimateGrossWeight"/>
<s:hidden name="miscellaneous.entitleGrossWeight"/>
<s:hidden name="miscellaneous.entitleTareWeight"/>
<s:hidden name="miscellaneous.estimateTareWeight"/>
<s:hidden name="miscellaneous.actualTareWeight"/>
<s:hidden name="miscellaneous.rwghTare"/>
<s:hidden name="miscellaneous.unit1"/>
<s:hidden name="miscellaneous.unit2"/>
<s:hidden name="miscellaneous.entitleNetWeight"/>
<s:hidden name="miscellaneous.estimatedNetWeight"/>
<s:hidden name="miscellaneous.actualNetWeight"/>
<s:hidden name="miscellaneous.rwghNet"/>
<s:hidden name="miscellaneous.entitleNumberAuto"/>
<s:hidden name="miscellaneous.estimateAuto"/>
<s:hidden name="miscellaneous.actualAuto"/>
<s:hidden name="miscellaneous.actualBoat"/>
<s:hidden name="miscellaneous.entitleAutoWeight"/>
<s:hidden name="miscellaneous.estimatedAutoWeight"/>
<s:hidden name="miscellaneous.actualAutoWeight"/>
<s:hidden name="miscellaneous.rwghCubicFeet"/>
<s:hidden name="miscellaneous.rwghNetCubicFeet"/>
<s:hidden name="miscellaneous.chargeableCubicFeet"/>
<s:hidden name="miscellaneous.chargeableNetCubicFeet"/>
<s:hidden name="miscellaneous.estimateCubicFeet"/>
<s:hidden name="miscellaneous.entitleCubicFeet"/>
<s:hidden name="miscellaneous.actualCubicFeet"/>
<s:hidden name="miscellaneous.netEstimateCubicFeet"/>
<s:hidden name="miscellaneous.netEntitleCubicFeet"/>
<s:hidden name="miscellaneous.netActualCubicFeet"/>
<s:hidden name="miscellaneous.chargeableGrossWeight"/>
<s:hidden name="miscellaneous.chargeableNetWeight"/>
<s:hidden name="miscellaneous.chargeableTareWeight"/>
<s:hidden name="miscellaneous.equipment"/>

<s:hidden name="miscellaneous.entitleGrossWeightKilo"/>
<s:hidden name="miscellaneous.entitleNetWeightKilo"/>
<s:hidden name="miscellaneous.estimateGrossWeightKilo"/>
<s:hidden name="miscellaneous.estimateTareWeightKilo"/>
<s:hidden name="miscellaneous.estimatedNetWeightKilo"/>
<s:hidden name="miscellaneous.actualGrossWeightKilo"/>
<s:hidden name="miscellaneous.actualTareWeightKilo"/>
<s:hidden name="miscellaneous.actualNetWeightKilo"/>

<s:hidden name="miscellaneous.entitleTareWeightKilo"/>
<s:hidden name="miscellaneous.chargeableGrossWeightKilo"/>
<s:hidden name="miscellaneous.chargeableTareWeightKilo"/>
<s:hidden name="miscellaneous.chargeableNetWeightKilo"/>
<s:hidden name="miscellaneous.rwghTareKilo"/>
<s:hidden name="miscellaneous.rwghNetKilo"/>
<s:hidden name="miscellaneous.entitleCubicMtr"/>
<s:hidden name="miscellaneous.netEntitleCubicMtr"/>
<s:hidden name="miscellaneous.estimateCubicMtr"/>
<s:hidden name="miscellaneous.netEstimateCubicMtr"/>
<s:hidden name="miscellaneous.actualCubicMtr"/>
<s:hidden name="miscellaneous.netActualCubicMtr"/>
<s:hidden name="miscellaneous.chargeableCubicMtr"/>
<s:hidden name="miscellaneous.chargeableNetCubicMtr"/>
<s:hidden name="miscellaneous.rwghCubicMtr"/>
<s:hidden name="miscellaneous.rwghNetCubicMtr"/>
<s:hidden name="miscellaneous.rwghGrossKilo"/>

<s:hidden name="miscellaneous.originScore"/> 



   </s:form>
 <script type="text/javascript"> 
    var fieldName = document.forms['miscellaneousForm'].elements['field'].value;
	var fieldName1 = document.forms['miscellaneousForm'].elements['field1'].value;
	if(fieldName!=''){
	document.forms['miscellaneousForm'].elements[fieldName].className = 'rules-textUpper';
	}
	if(fieldName1!=''){
	document.forms['miscellaneousForm'].elements[fieldName1].className = 'rules-textUpper';
	}	
</script>
<script type="text/javascript"> 
<c:if test="${hitFlag == 1}" >
	<c:if test="${checkPropertyAmountComponent!='Y'}">
		<c:redirect url="/editMiscellaneous.html?id=${miscellaneous.id}&redirected=yes"  />
	</c:if>
	<c:if test="${checkPropertyAmountComponent=='Y'}">
		<c:redirect url="/editMiscellaneous.html?id=${miscellaneous.id}&redirected=yes&msgClicked=${msgClicked}"  />
	</c:if>
</c:if>  
</script>

<script type="text/javascript">
	setOnSelectBasedMethods(["calcDays()"]);
	setCalendarFunctionality();

	function getAdvDtls(position){
		var driver = document.forms['miscellaneousForm'].elements['miscellaneous.driverId'].value;
		var url="showAdvances.html?sequenceNum=${miscellaneous.shipNumber}&driver="+driver+"&decorator=simple&popup=true";
		ajax_SoTooltip(url,position);	
	}
</script>
<script type="text/javascript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode; 
  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110)||( keyCode==109); 
}
</script>
