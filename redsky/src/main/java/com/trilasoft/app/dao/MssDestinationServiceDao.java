package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.MssDestinationService;

public interface MssDestinationServiceDao extends GenericDao<MssDestinationService, Long>{

}
