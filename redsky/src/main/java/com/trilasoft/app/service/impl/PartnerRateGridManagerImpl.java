package com.trilasoft.app.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.AccountLineDao;
import com.trilasoft.app.dao.PartnerRateGridDao;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.PartnerRateGrid;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.PartnerRateGridManager;

public class PartnerRateGridManagerImpl extends GenericManagerImpl<PartnerRateGrid, Long> implements PartnerRateGridManager {
	PartnerRateGridDao partnerRateGridDao;
	
	public PartnerRateGridManagerImpl(PartnerRateGridDao partnerRateGridDao) { 
        super(partnerRateGridDao); 
        this.partnerRateGridDao = partnerRateGridDao; 
    }

	public List getPartnerRateGridList(String sessionCorpID, String partnerCode) {
		
		return partnerRateGridDao.getPartnerRateGridList(sessionCorpID, partnerCode);
	}

	public List getRateGridTemplates(String sessionCorpID) {
		
		return partnerRateGridDao.getRateGridTemplates(sessionCorpID);
	}

	public Map<String, String> findStateList(String terminalCountryCode, String sessionCorpID) {
		
		return partnerRateGridDao.findStateList(terminalCountryCode, sessionCorpID);
	}

	public List getStandardInclusionsNote(String corpID) {
		
		return partnerRateGridDao.getStandardInclusionsNote(corpID);
	}

	public List getStandardDestinationNote(String corpID) { 
		return partnerRateGridDao.getStandardDestinationNote(corpID);
	}

	public int updateRateGridStatus(Long id, String status, String sessionCorpID, String user) {
		
		return partnerRateGridDao.updateRateGridStatus(id, status, sessionCorpID, user);
	}

	public int findSubmitCount(String partnerCode, String sessionCorpID, String tariffApplicability) {
		
		return partnerRateGridDao.findSubmitCount(partnerCode, sessionCorpID,tariffApplicability);
	}

	public int findUniqueCount(String sessionCorpID, String partnerCode, String tariffApplicability, String metroCity, BigDecimal serviceRangeMiles, String effectiveDate) {
		
		return partnerRateGridDao.findUniqueCount(sessionCorpID, partnerCode, tariffApplicability, metroCity, serviceRangeMiles, effectiveDate);
	}

	public List getRateGridRules(String corpID) {
		return partnerRateGridDao.getRateGridRules(corpID);
	}

	public List findPartnerRateMatrix(String sessionCorpID, String partnerCode, String partnerName, String terminalCountry, String tariffApplicability, String status, boolean publishTariff) {
		return partnerRateGridDao.findPartnerRateMatrix(sessionCorpID, partnerCode, partnerName, terminalCountry, tariffApplicability, status, publishTariff);
	}

	public int updatePublisStatus(Long id, String publishTariff) {
		return partnerRateGridDao.updatePublisStatus(id, publishTariff);
		
	}

	public List getStandardCountryHauling(String corpID) {
		return partnerRateGridDao.getStandardCountryHauling(corpID);
	}

	public List getStandardCountryCharges(String corpID) {
		return partnerRateGridDao.getStandardCountryCharges(corpID);
	}

	public List searchBaseCurrency(String sessionCorpID) {
		
		return partnerRateGridDao.searchBaseCurrency(sessionCorpID);
	}

	public List getPartnerRateGridListView(String sessionCorpID, String partnerCode) {
		
		return partnerRateGridDao.getPartnerRateGridListView(sessionCorpID, partnerCode);
	} 
	
}
