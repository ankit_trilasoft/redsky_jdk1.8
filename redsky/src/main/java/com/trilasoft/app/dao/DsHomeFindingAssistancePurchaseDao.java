package com.trilasoft.app.dao;

import java.util.List;
import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.DsHomeFindingAssistancePurchase;

public interface DsHomeFindingAssistancePurchaseDao extends GenericDao<DsHomeFindingAssistancePurchase, Long>{
	List getListById(Long id);
}


