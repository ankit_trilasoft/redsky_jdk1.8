package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import java.util.Date;
import java.util.List;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.DataSecurityFilter;
import com.trilasoft.app.model.RoleBasedComponentPermission;
import com.trilasoft.app.service.DataSecurityFilterManager;
import com.trilasoft.app.service.DataSecurityFilterService;

public class DataSecurityFilterAction extends BaseAction{
	private Long id;
	private String sessionCorpID;
	private DataSecurityFilter dataSecurityFilter;
	private DataSecurityFilterManager dataSecurityFilterManager;
	private List dataSecurityFilterList;
	private List tableList;
	private String tableName;
	private List fieldList;
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(DataSecurityFilterAction.class);

	 public DataSecurityFilter getDataSecurityFilter() {
		return dataSecurityFilter;
	}
	public void setDataSecurityFilter(DataSecurityFilter dataSecurityFilter) {
		this.dataSecurityFilter = dataSecurityFilter;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public DataSecurityFilterAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		tableList=dataSecurityFilterManager.getTableList(sessionCorpID);
		fieldList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	 }
	
	public String fieldList()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//System.out.println("\n\ntableName-->>"+tableName);
		fieldList=dataSecurityFilterManager.getFieldList(tableName);
		//System.out.println("\n\nfieldList-->>"+fieldList);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String list(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		dataSecurityFilterList=dataSecurityFilterManager.getAll();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String edit(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		
		if (id != null)
		{
			dataSecurityFilter=dataSecurityFilterManager.get(id);
			fieldList=dataSecurityFilterManager.getFieldList(dataSecurityFilter.getTableName());
		}
		else
		{
			dataSecurityFilter=new DataSecurityFilter();
			dataSecurityFilter.setCreatedOn(new Date());
			dataSecurityFilter.setCreatedBy(getRequest().getRemoteUser());
		}
		dataSecurityFilter.setUpdatedOn(new Date());
		dataSecurityFilter.setUpdatedBy(getRequest().getRemoteUser());
		dataSecurityFilter.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String save() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		boolean isNew = (dataSecurityFilter.getId() == null);
		
		fieldList=dataSecurityFilterManager.getFieldList(dataSecurityFilter.getTableName());	
		dataSecurityFilter.setCorpID(sessionCorpID);	
		dataSecurityFilter=dataSecurityFilterManager.save(dataSecurityFilter);
		String key = (isNew) ? "DataSecurityFilter has been added successfully" : "DataSecurityFilter has been updated successfully";
		saveMessage(getText(key));
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		if (!isNew) {   
        	return SUCCESS;   
        } else {   
        	
            return SUCCESS;
        }
		
	}
	
	
	public String dataSecurityFilterSearch()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if(dataSecurityFilter!=null){
			boolean name=(dataSecurityFilter.getName() == null);
			boolean tableName = (dataSecurityFilter.getTableName() == null);
			boolean fieldName = (dataSecurityFilter.getFieldName() == null);
			boolean filterValues = (dataSecurityFilter.getFilterValues() == null);
			if(!name|| !tableName || !fieldName || !filterValues) {
				dataSecurityFilterList=dataSecurityFilterManager.search(dataSecurityFilter.getName(), dataSecurityFilter.getTableName(), dataSecurityFilter.getFieldName(), dataSecurityFilter.getFilterValues());
			}
		  }
		else{
			dataSecurityFilterList=dataSecurityFilterManager.getAll();
		    }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	
	
	
	
	public List getDataSecurityFilterList() {
		return dataSecurityFilterList;
	}
	public void setDataSecurityFilterList(List dataSecurityFilterList) {
		this.dataSecurityFilterList = dataSecurityFilterList;
	}
	public void setDataSecurityFilterManager(DataSecurityFilterManager dataSecurityFilterManager) {
		this.dataSecurityFilterManager = dataSecurityFilterManager;
	}
	public List getTableList() {
		return tableList;
	}
	public void setTableList(List tableList) {
		this.tableList = tableList;
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public List getFieldList() {
		return fieldList;
	}
	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}
}
