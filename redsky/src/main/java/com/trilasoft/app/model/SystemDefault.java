package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.*;

import org.appfuse.model.BaseObject;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;

import javax.persistence.Table;

import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

@Entity
@Table(name = "systemdefault")
@FilterDef(name = "corpID", parameters = { @ParamDef(name = "forCorpID", type = "string") })
@Filter(name = "corpID", condition = "corpID = :forCorpID")

@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)

public class SystemDefault extends BaseObject {

	private Long id;

	private String baseCurrency;

	private String companyCode;

	private String corpID;

	private BigDecimal lastSequence;

	private String miscVl;

	private String storage;

	private String company;

	private String address1;

	private String address2;

	private String address3;

	private String city;

	private String state;

	private String zip;

	private String phone;

	private String tin;

	private String export;

	private String IMPORT;

	private String domestic;

	private String other;

	private String daCode;

	private int byClient;

	private String vanLine;

	private BigDecimal miles;

	private Date lastStars;

	private int backup;

	private String beginStars;

	private String CMPYABBRV;

	private String dispatch;

	private String fax;

	private BigDecimal LASTEQUIP;

	private BigDecimal lastMaster;

	private String lateBegin;

	private String lateEnd;

	private String loadAtLast;

	private BigDecimal lastLoss;

	private BigDecimal otherPcPay;

	private String packScreen;

	private String trackPath;

	private Date willAdvis;

	private int workDymension;

	private String persionalPath;

	private String localTemp;

	private Date postDate;

	private int sCloseCSO;

	private Date sTestDate;

	private String oInterval;

	private String userPath;

	private String spCode;

	private String email;

	private String coordinator;

	private String vlContract;

	private String doRegistrationNumber;

	private String dorecncl;

	private String scoreForm;
	
	private String workDay;

	private String dayAt2;

	private String dayAt1;

	private String lunch2;

	private String weekdays2;

	private String lunch1;

	private String weekdays1;

	private Date srunDate;

	private boolean thirdParty;

	private int numDays;
	private int maxAttempt;
	private String doagntcde;

	private String special2;

	private String special1;

	private String minPayCode;

	private String longDtypes;

	private String cmpy;

	private int doIndex;

	private String pdfprinter;

	private String runReports;

	private BigDecimal midPc;

	private String createdBy;

	private String updatedBy;

	private Date createdOn;

	private Date updatedOn;

	private Date postDate1;

	private String serviceInclude;

	private String serviceExclude;

	private String weightUnit;

	private String volumeUnit;

	private String isUnigroup;

	private String uvl;

	private String mvl;
	private String accountingSystem;
	private String accountingInterface;
	private String filterCoordinator;
	private String invExtractSeq;
	private String payExtractSeq;
	private String prtExtractSeq;
	private String subContcExtractSeq;
	private String minimumHoursGurantee;
	private String salesCommisionRate;
	private String grossMarginThreshold;
	
	private String toDoRuleExecutionTime;
	private Date lastRunDate;
	
	private String dailyOperationalLimit;
	private String minServiceDay;
	private String dailyOpLimitPC;
	private String website;
	private String excludeIPs;
	private String toDoRuleExecutionTime2;
	private String surveyExternalIPs;
	
	private BigDecimal healthWelfareRate;
	
	private String standardCountryHauling;
	
	private String standardCountryCharges;
	private String rateDeptEmail;
	private String agentIdRegistrationDistribution;
	private String commissionable;
	
	//Enhancement for Cportal
	
	private boolean claimShowCportal;
	private String claimContactPerson;
	private String claimEmail;
	private String claimFax;
	private String claimPhone;
	private String valuationDetails1;
	private String valuationDetails2;
	private String valuationDetails3;
	private String valuationDetails4;
	private String valuationDetails5;
	private String commissionJob;
	private BigDecimal currencyMargin;
	private String currencyPullScheduling;
	private String companyDivisionAcctgCodeUnique;
	private String networkCoordinator;
	private String country;
	private String defaultDistanceCalc;
	
	private Boolean vatCalculation;

	private Date pensionEffectiveDate1;
	private Date pensionEffectiveDate2;
	private Date pensionEffectiveDate3;
	private Date pensionEffectiveDate4;
	private BigDecimal pensionRate1;
	private BigDecimal pensionRate2;
	private BigDecimal pensionRate3;
	private BigDecimal pensionRate4;
	private Boolean costElement;
	private String transDoc;
	private String minRec;
	private String maxRec;
	private String minPay;
	private String maxPay;
	private String suspenseDefault; 
	private String currentRec;
	private String currentPay;
	private String vanLineCompanyDivision;
	private String uvlContract;
	private String uvlBillToCode;
	private String uvlBillToName;
	private String mvlContract;
	private String mvlBillToCode;
	private String mvlBillToName;
	private String intBillToCode;
	private String intBillToName;
	private String intContract;
// Journal validation field	
	private String validation;
	private String unitVariable;
	private String truckRequired;
	private Boolean autoPayablePosting;
	private Boolean automaticReconcile;
	private BigDecimal vanlineMinimumAmount;
	private Boolean billingCheck;
	private Boolean POS;
	private String miscellaneousdefaultdriver;
	private Boolean vanlineSettleColourStatus;
	private String dosPartnerCode;
	private String dosscac;
	private Boolean costingDetailShow;
	private String operationMgmtBy;
	private Boolean rating = new Boolean(false);
	private Boolean distribution  = new Boolean(false);
	private String driverCrewType; 
	private Boolean driverCommissionSetUp= new Boolean(false);
	private Boolean noInsurance;
	private String insurancePerUnit; 
	private String unitForInsurance;
	private String minReplacementvalue;
	private String minInsurancePerUnit;
	private Boolean detailedList;
	private Boolean noInsuranceCportal;
	private Boolean lumpSum;
	@Column
	private String storageEmail;
	private BigDecimal vanlineMaximumAmount;
	private String vanlineExceptionChargeCode;
	private Boolean ownbillingVanline = false;
	private String paymentApplicationExtractSeq;
	private String ownbillingBilltoCodes;
	private String soExtractSeq;
	@Column
	private String customerSurveyEmail;
	private Boolean accountLineAccountPortalFlag = false;
	private Boolean passwordPolicy= new Boolean(false);
	private String contractChargesMandatory = "0";
	private Date maxUpdatedOn;
	private String hubWarehouseOperationalLimit;
	private String localJobs;
	private String prefix;
	private String sequence; 
	private String receivableVat;
	private String payableVat;
	private String reciprocityjobtype;
	private String service;
	private String warehouse;
	private int daystoManageAlert;
	private String payableActual;
	private String receivableActual;
	private String payableAccrued;
	private String receivableAccrued;
	private String defaultDivisionCharges;
	private Boolean currentOpenPeriod=false;
	private Boolean passwordPolicyForExternalUser= new Boolean(false);
	private String storageBilling;
	private String SITBilling;
	private String TPSBilling;
	private String SCSBilling;
    private Boolean agentSearchValidation =false;
	private String usGovJobs;
	private Boolean accountSearchValidation =false;
	private BigDecimal vatSumThreshhold= new BigDecimal(0);
	private String ediBillToCode;
	private String employeeExpensesJob;
	private Date commissionCloseDate;
	
	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("baseCurrency", baseCurrency)
				.append("companyCode", companyCode)
				.append("corpID", corpID)
				.append("lastSequence", lastSequence)
				.append("miscVl", miscVl)
				.append("storage", storage)
				.append("company", company)
				.append("address1", address1)
				.append("address2", address2)
				.append("address3", address3)
				.append("city", city)
				.append("state", state)
				.append("zip", zip)
				.append("phone", phone)
				.append("tin", tin)
				.append("export", export)
				.append("IMPORT", IMPORT)
				.append("domestic", domestic)
				.append("other", other)
				.append("daCode", daCode)
				.append("byClient", byClient)
				.append("vanLine", vanLine)
				.append("miles", miles)
				.append("lastStars", lastStars)
				.append("backup", backup)
				.append("beginStars", beginStars)
				.append("CMPYABBRV", CMPYABBRV)
				.append("dispatch", dispatch)
				.append("fax", fax)
				.append("LASTEQUIP", LASTEQUIP)
				.append("lastMaster", lastMaster)
				.append("lateBegin", lateBegin)
				.append("lateEnd", lateEnd)
				.append("loadAtLast", loadAtLast)
				.append("lastLoss", lastLoss)
				.append("otherPcPay", otherPcPay)
				.append("packScreen", packScreen)
				.append("trackPath", trackPath)
				.append("willAdvis", willAdvis)
				.append("workDymension", workDymension)
				.append("persionalPath", persionalPath)
				.append("localTemp", localTemp)
				.append("postDate", postDate)
				.append("sCloseCSO", sCloseCSO)
				.append("sTestDate", sTestDate)
				.append("oInterval", oInterval)
				.append("userPath", userPath)
				.append("spCode", spCode)
				.append("email", email)
				.append("coordinator", coordinator)
				.append("vlContract", vlContract)
				.append("doRegistrationNumber", doRegistrationNumber)
				.append("dorecncl", dorecncl)
				.append("scoreForm", scoreForm)
				.append("workDay", workDay)
				.append("dayAt2", dayAt2)
				.append("dayAt1", dayAt1)
				.append("lunch2", lunch2)
				.append("weekdays2", weekdays2)
				.append("lunch1", lunch1)
				.append("weekdays1", weekdays1)
				.append("srunDate", srunDate)
				.append("thirdParty", thirdParty)
				.append("numDays", numDays)
				.append("maxAttempt", maxAttempt)
				.append("doagntcde", doagntcde)
				.append("special2", special2)
				.append("special1", special1)
				.append("minPayCode", minPayCode)
				.append("longDtypes", longDtypes)
				.append("cmpy", cmpy)
				.append("doIndex", doIndex)
				.append("pdfprinter", pdfprinter)
				.append("runReports", runReports)
				.append("midPc", midPc)
				.append("createdBy", createdBy)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updatedOn", updatedOn)
				.append("postDate1", postDate1)
				.append("serviceInclude", serviceInclude)
				.append("serviceExclude", serviceExclude)
				.append("weightUnit", weightUnit)
				.append("volumeUnit", volumeUnit)
				.append("isUnigroup", isUnigroup)
				.append("uvl", uvl)
				.append("mvl", mvl)
				.append("accountingSystem", accountingSystem)
				.append("accountingInterface", accountingInterface)
				.append("filterCoordinator", filterCoordinator)
				.append("invExtractSeq", invExtractSeq)
				.append("payExtractSeq", payExtractSeq)
				.append("prtExtractSeq", prtExtractSeq)
				.append("subContcExtractSeq", subContcExtractSeq)
				.append("minimumHoursGurantee", minimumHoursGurantee)
				.append("salesCommisionRate", salesCommisionRate)
				.append("grossMarginThreshold", grossMarginThreshold)
				.append("toDoRuleExecutionTime", toDoRuleExecutionTime)
				.append("lastRunDate", lastRunDate)
				.append("dailyOperationalLimit", dailyOperationalLimit)
				.append("minServiceDay", minServiceDay)
				.append("dailyOpLimitPC", dailyOpLimitPC)
				.append("website", website)
				.append("excludeIPs", excludeIPs)
				.append("toDoRuleExecutionTime2", toDoRuleExecutionTime2)
				.append("surveyExternalIPs", surveyExternalIPs)
				.append("healthWelfareRate", healthWelfareRate)
				.append("standardCountryHauling", standardCountryHauling)
				.append("standardCountryCharges", standardCountryCharges)
				.append("rateDeptEmail", rateDeptEmail)
				.append("agentIdRegistrationDistribution",
						agentIdRegistrationDistribution)
				.append("commissionable", commissionable)
				.append("claimShowCportal", claimShowCportal)
				.append("claimContactPerson", claimContactPerson)
				.append("claimEmail", claimEmail)
				.append("claimFax", claimFax)
				.append("claimPhone", claimPhone)
				.append("valuationDetails1", valuationDetails1)
				.append("valuationDetails2", valuationDetails2)
				.append("valuationDetails3", valuationDetails3)
				.append("valuationDetails4", valuationDetails4)
				.append("valuationDetails5", valuationDetails5)
				.append("commissionJob", commissionJob)
				.append("currencyMargin", currencyMargin)
				.append("currencyPullScheduling", currencyPullScheduling)
				.append("companyDivisionAcctgCodeUnique",
						companyDivisionAcctgCodeUnique)
				.append("networkCoordinator", networkCoordinator)
				.append("country", country)
				.append("defaultDistanceCalc", defaultDistanceCalc)
				.append("vatCalculation", vatCalculation)
				.append("pensionEffectiveDate1", pensionEffectiveDate1)
				.append("pensionEffectiveDate2", pensionEffectiveDate2)
				.append("pensionEffectiveDate3", pensionEffectiveDate3)
				.append("pensionEffectiveDate4", pensionEffectiveDate4)
				.append("pensionRate1", pensionRate1)
				.append("pensionRate2", pensionRate2)
				.append("pensionRate3", pensionRate3)
				.append("pensionRate4", pensionRate4)
				.append("costElement", costElement)
				.append("transDoc", transDoc)
				.append("minRec", minRec)
				.append("maxRec", maxRec)
				.append("minPay", minPay)
				.append("maxPay", maxPay)
				.append("suspenseDefault", suspenseDefault)
				.append("currentRec", currentRec)
				.append("currentPay", currentPay)
				.append("vanLineCompanyDivision", vanLineCompanyDivision)
				.append("uvlContract", uvlContract)
				.append("uvlBillToCode", uvlBillToCode)
				.append("uvlBillToName", uvlBillToName)
				.append("mvlContract", mvlContract)
				.append("mvlBillToCode", mvlBillToCode)
				.append("mvlBillToName", mvlBillToName)
				.append("intBillToCode", intBillToCode)
				.append("intBillToName", intBillToName)
				.append("intContract", intContract)
				.append("validation", validation)
				.append("unitVariable", unitVariable)
				.append("truckRequired", truckRequired)
				.append("autoPayablePosting", autoPayablePosting)
				.append("automaticReconcile", automaticReconcile)
				.append("vanlineMinimumAmount", vanlineMinimumAmount)
				.append("billingCheck", billingCheck)
				.append("POS", POS)
				.append("miscellaneousdefaultdriver",
						miscellaneousdefaultdriver)
				.append("vanlineSettleColourStatus", vanlineSettleColourStatus)
				.append("dosPartnerCode", dosPartnerCode)
				.append("dosscac", dosscac)
				.append("costingDetailShow", costingDetailShow)
				.append("operationMgmtBy", operationMgmtBy)
				.append("rating", rating)
				.append("distribution", distribution)
				.append("driverCrewType", driverCrewType)
				.append("driverCommissionSetUp", driverCommissionSetUp)
				.append("noInsurance", noInsurance)
				.append("insurancePerUnit", insurancePerUnit)
				.append("unitForInsurance", unitForInsurance)
				.append("minReplacementvalue", minReplacementvalue)
				.append("minInsurancePerUnit", minInsurancePerUnit)
				.append("detailedList", detailedList)
				.append("noInsuranceCportal", noInsuranceCportal)
				.append("lumpSum", lumpSum)
				.append("storageEmail", storageEmail)
				.append("vanlineMaximumAmount", vanlineMaximumAmount)
				.append("vanlineExceptionChargeCode",
						vanlineExceptionChargeCode)
				.append("ownbillingVanline", ownbillingVanline)
				.append("paymentApplicationExtractSeq",
						paymentApplicationExtractSeq)
				.append("ownbillingBilltoCodes", ownbillingBilltoCodes)
				.append("soExtractSeq", soExtractSeq)
				.append("customerSurveyEmail", customerSurveyEmail)
				.append("accountLineAccountPortalFlag",
						accountLineAccountPortalFlag)
				.append("passwordPolicy", passwordPolicy)
				.append("contractChargesMandatory", contractChargesMandatory)
				.append("maxUpdatedOn", maxUpdatedOn)
				.append("hubWarehouseOperationalLimit",
						hubWarehouseOperationalLimit)
				.append("localJobs", localJobs).append("prefix", prefix)
				.append("sequence", sequence)
				.append("receivableVat", receivableVat)
				.append("payableVat", payableVat)
				.append("reciprocityjobtype", reciprocityjobtype)
				.append("service", service).append("warehouse", warehouse)
				.append("daystoManageAlert", daystoManageAlert)
				.append("payableActual", payableActual)
				.append("receivableActual", receivableActual)
				.append("payableAccrued", payableAccrued)
				.append("receivableAccrued", receivableAccrued)
				.append("defaultDivisionCharges", defaultDivisionCharges)
				.append("currentOpenPeriod", currentOpenPeriod)
				.append("passwordPolicyForExternalUser",passwordPolicyForExternalUser)
				.append("storageBilling",storageBilling)
				.append("SITBilling",SITBilling) 
				.append("TPSBilling",TPSBilling)
				.append("SCSBilling",SCSBilling)
				.append("agentSearchValidation",agentSearchValidation)
				.append("usGovJobs",usGovJobs)
				.append("accountSearchValidation",accountSearchValidation)
				.append("vatSumThreshhold",vatSumThreshhold)
				.append("ediBillToCode",ediBillToCode)
				.append("employeeExpensesJob",employeeExpensesJob)
				.append("commissionCloseDate",commissionCloseDate) 
				.toString();
	}

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof SystemDefault))
			return false;
		SystemDefault castOther = (SystemDefault) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(baseCurrency, castOther.baseCurrency)
				.append(companyCode, castOther.companyCode)
				.append(corpID, castOther.corpID)
				.append(lastSequence, castOther.lastSequence)
				.append(miscVl, castOther.miscVl)
				.append(storage, castOther.storage)
				.append(company, castOther.company)
				.append(address1, castOther.address1)
				.append(address2, castOther.address2)
				.append(address3, castOther.address3)
				.append(city, castOther.city)
				.append(state, castOther.state)
				.append(zip, castOther.zip)
				.append(phone, castOther.phone)
				.append(tin, castOther.tin)
				.append(export, castOther.export)
				.append(IMPORT, castOther.IMPORT)
				.append(domestic, castOther.domestic)
				.append(other, castOther.other)
				.append(daCode, castOther.daCode)
				.append(byClient, castOther.byClient)
				.append(vanLine, castOther.vanLine)
				.append(miles, castOther.miles)
				.append(lastStars, castOther.lastStars)
				.append(backup, castOther.backup)
				.append(beginStars, castOther.beginStars)
				.append(CMPYABBRV, castOther.CMPYABBRV)
				.append(dispatch, castOther.dispatch)
				.append(fax, castOther.fax)
				.append(LASTEQUIP, castOther.LASTEQUIP)
				.append(lastMaster, castOther.lastMaster)
				.append(lateBegin, castOther.lateBegin)
				.append(lateEnd, castOther.lateEnd)
				.append(loadAtLast, castOther.loadAtLast)
				.append(lastLoss, castOther.lastLoss)
				.append(otherPcPay, castOther.otherPcPay)
				.append(packScreen, castOther.packScreen)
				.append(trackPath, castOther.trackPath)
				.append(willAdvis, castOther.willAdvis)
				.append(workDymension, castOther.workDymension)
				.append(persionalPath, castOther.persionalPath)
				.append(localTemp, castOther.localTemp)
				.append(postDate, castOther.postDate)
				.append(sCloseCSO, castOther.sCloseCSO)
				.append(sTestDate, castOther.sTestDate)
				.append(oInterval, castOther.oInterval)
				.append(userPath, castOther.userPath)
				.append(spCode, castOther.spCode)
				.append(email, castOther.email)
				.append(coordinator, castOther.coordinator)
				.append(vlContract, castOther.vlContract)
				.append(doRegistrationNumber, castOther.doRegistrationNumber)
				.append(dorecncl, castOther.dorecncl)
				.append(scoreForm, castOther.scoreForm)
				.append(workDay, castOther.workDay)
				.append(dayAt2, castOther.dayAt2)
				.append(dayAt1, castOther.dayAt1)
				.append(lunch2, castOther.lunch2)
				.append(weekdays2, castOther.weekdays2)
				.append(lunch1, castOther.lunch1)
				.append(weekdays1, castOther.weekdays1)
				.append(srunDate, castOther.srunDate)
				.append(thirdParty, castOther.thirdParty)
				.append(numDays, castOther.numDays)
				.append(maxAttempt, castOther.maxAttempt)
				.append(doagntcde, castOther.doagntcde)
				.append(special2, castOther.special2)
				.append(special1, castOther.special1)
				.append(minPayCode, castOther.minPayCode)
				.append(longDtypes, castOther.longDtypes)
				.append(cmpy, castOther.cmpy)
				.append(doIndex, castOther.doIndex)
				.append(pdfprinter, castOther.pdfprinter)
				.append(runReports, castOther.runReports)
				.append(midPc, castOther.midPc)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(postDate1, castOther.postDate1)
				.append(serviceInclude, castOther.serviceInclude)
				.append(serviceExclude, castOther.serviceExclude)
				.append(weightUnit, castOther.weightUnit)
				.append(volumeUnit, castOther.volumeUnit)
				.append(isUnigroup, castOther.isUnigroup)
				.append(uvl, castOther.uvl)
				.append(mvl, castOther.mvl)
				.append(accountingSystem, castOther.accountingSystem)
				.append(accountingInterface, castOther.accountingInterface)
				.append(filterCoordinator, castOther.filterCoordinator)
				.append(invExtractSeq, castOther.invExtractSeq)
				.append(payExtractSeq, castOther.payExtractSeq)
				.append(prtExtractSeq, castOther.prtExtractSeq)
				.append(subContcExtractSeq, castOther.subContcExtractSeq)
				.append(minimumHoursGurantee, castOther.minimumHoursGurantee)
				.append(salesCommisionRate, castOther.salesCommisionRate)
				.append(grossMarginThreshold, castOther.grossMarginThreshold)
				.append(toDoRuleExecutionTime, castOther.toDoRuleExecutionTime)
				.append(lastRunDate, castOther.lastRunDate)
				.append(dailyOperationalLimit, castOther.dailyOperationalLimit)
				.append(minServiceDay, castOther.minServiceDay)
				.append(dailyOpLimitPC, castOther.dailyOpLimitPC)
				.append(website, castOther.website)
				.append(excludeIPs, castOther.excludeIPs)
				.append(toDoRuleExecutionTime2,
						castOther.toDoRuleExecutionTime2)
				.append(surveyExternalIPs, castOther.surveyExternalIPs)
				.append(healthWelfareRate, castOther.healthWelfareRate)
				.append(standardCountryHauling,
						castOther.standardCountryHauling)
				.append(standardCountryCharges,
						castOther.standardCountryCharges)
				.append(rateDeptEmail, castOther.rateDeptEmail)
				.append(agentIdRegistrationDistribution,
						castOther.agentIdRegistrationDistribution)
				.append(commissionable, castOther.commissionable)
				.append(claimShowCportal, castOther.claimShowCportal)
				.append(claimContactPerson, castOther.claimContactPerson)
				.append(claimEmail, castOther.claimEmail)
				.append(claimFax, castOther.claimFax)
				.append(claimPhone, castOther.claimPhone)
				.append(valuationDetails1, castOther.valuationDetails1)
				.append(valuationDetails2, castOther.valuationDetails2)
				.append(valuationDetails3, castOther.valuationDetails3)
				.append(valuationDetails4, castOther.valuationDetails4)
				.append(valuationDetails5, castOther.valuationDetails5)
				.append(commissionJob, castOther.commissionJob)
				.append(currencyMargin, castOther.currencyMargin)
				.append(currencyPullScheduling,
						castOther.currencyPullScheduling)
				.append(companyDivisionAcctgCodeUnique,
						castOther.companyDivisionAcctgCodeUnique)
				.append(networkCoordinator, castOther.networkCoordinator)
				.append(country, castOther.country)
				.append(defaultDistanceCalc, castOther.defaultDistanceCalc)
				.append(vatCalculation, castOther.vatCalculation)
				.append(pensionEffectiveDate1, castOther.pensionEffectiveDate1)
				.append(pensionEffectiveDate2, castOther.pensionEffectiveDate2)
				.append(pensionEffectiveDate3, castOther.pensionEffectiveDate3)
				.append(pensionEffectiveDate4, castOther.pensionEffectiveDate4)
				.append(pensionRate1, castOther.pensionRate1)
				.append(pensionRate2, castOther.pensionRate2)
				.append(pensionRate3, castOther.pensionRate3)
				.append(pensionRate4, castOther.pensionRate4)
				.append(costElement, castOther.costElement)
				.append(transDoc, castOther.transDoc)
				.append(minRec, castOther.minRec)
				.append(maxRec, castOther.maxRec)
				.append(minPay, castOther.minPay)
				.append(maxPay, castOther.maxPay)
				.append(suspenseDefault, castOther.suspenseDefault)
				.append(currentRec, castOther.currentRec)
				.append(currentPay, castOther.currentPay)
				.append(vanLineCompanyDivision,
						castOther.vanLineCompanyDivision)
				.append(uvlContract, castOther.uvlContract)
				.append(uvlBillToCode, castOther.uvlBillToCode)
				.append(uvlBillToName, castOther.uvlBillToName)
				.append(mvlContract, castOther.mvlContract)
				.append(mvlBillToCode, castOther.mvlBillToCode)
				.append(mvlBillToName, castOther.mvlBillToName)
				.append(intBillToCode, castOther.intBillToCode)
				.append(intBillToName, castOther.intBillToName)
				.append(intContract, castOther.intContract)
				.append(validation, castOther.validation)
				.append(unitVariable, castOther.unitVariable)
				.append(truckRequired, castOther.truckRequired)
				.append(autoPayablePosting, castOther.autoPayablePosting)
				.append(automaticReconcile, castOther.automaticReconcile)
				.append(vanlineMinimumAmount, castOther.vanlineMinimumAmount)
				.append(billingCheck, castOther.billingCheck)
				.append(POS, castOther.POS)
				.append(miscellaneousdefaultdriver,
						castOther.miscellaneousdefaultdriver)
				.append(vanlineSettleColourStatus,
						castOther.vanlineSettleColourStatus)
				.append(dosPartnerCode, castOther.dosPartnerCode)
				.append(dosscac, castOther.dosscac)
				.append(costingDetailShow, castOther.costingDetailShow)
				.append(operationMgmtBy, castOther.operationMgmtBy)
				.append(rating, castOther.rating)
				.append(distribution, castOther.distribution)
				.append(driverCrewType, castOther.driverCrewType)
				.append(driverCommissionSetUp, castOther.driverCommissionSetUp)
				.append(noInsurance, castOther.noInsurance)
				.append(insurancePerUnit, castOther.insurancePerUnit)
				.append(unitForInsurance, castOther.unitForInsurance)
				.append(minReplacementvalue, castOther.minReplacementvalue)
				.append(minInsurancePerUnit, castOther.minInsurancePerUnit)
				.append(detailedList, castOther.detailedList)
				.append(noInsuranceCportal, castOther.noInsuranceCportal)
				.append(lumpSum, castOther.lumpSum)
				.append(storageEmail, castOther.storageEmail)
				.append(vanlineMaximumAmount, castOther.vanlineMaximumAmount)
				.append(vanlineExceptionChargeCode,
						castOther.vanlineExceptionChargeCode)
				.append(ownbillingVanline, castOther.ownbillingVanline)
				.append(paymentApplicationExtractSeq,
						castOther.paymentApplicationExtractSeq)
				.append(ownbillingBilltoCodes, castOther.ownbillingBilltoCodes)
				.append(soExtractSeq, castOther.soExtractSeq)
				.append(customerSurveyEmail, castOther.customerSurveyEmail)
				.append(accountLineAccountPortalFlag,
						castOther.accountLineAccountPortalFlag)
				.append(passwordPolicy, castOther.passwordPolicy)
				.append(contractChargesMandatory,
						castOther.contractChargesMandatory)
				.append(maxUpdatedOn, castOther.maxUpdatedOn)
				.append(hubWarehouseOperationalLimit,
						castOther.hubWarehouseOperationalLimit)
				.append(localJobs, castOther.localJobs)
				.append(prefix, castOther.prefix)
				.append(sequence, castOther.sequence)
				.append(receivableVat, castOther.receivableVat)
				.append(payableVat, castOther.payableVat)
				.append(reciprocityjobtype, castOther.reciprocityjobtype)
				.append(service, castOther.service)
				.append(warehouse, castOther.warehouse)
				.append(daystoManageAlert, castOther.daystoManageAlert)
				.append(payableActual, castOther.payableActual)
				.append(receivableActual, castOther.receivableActual)
				.append(payableAccrued, castOther.payableAccrued)
				.append(receivableAccrued, castOther.receivableAccrued)
				.append(defaultDivisionCharges,	castOther.defaultDivisionCharges)
				.append(currentOpenPeriod, castOther.currentOpenPeriod)
				.append(passwordPolicyForExternalUser, castOther.passwordPolicyForExternalUser)
				.append(storageBilling, castOther.storageBilling)
				.append(SITBilling, castOther.SITBilling)
				.append(TPSBilling, castOther.TPSBilling)
				.append(SCSBilling, castOther.SCSBilling)
				.append(agentSearchValidation, castOther.agentSearchValidation)
				.append(usGovJobs, castOther.usGovJobs)
				.append("accountSearchValidation",castOther.accountSearchValidation)
					.append("vatSumThreshhold",castOther.vatSumThreshhold).append("ediBillToCode",castOther.ediBillToCode)
					.append("employeeExpensesJob",castOther.employeeExpensesJob)
					.append("commissionCloseDate",castOther.commissionCloseDate)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(baseCurrency)
				.append(companyCode).append(corpID).append(lastSequence)
				.append(miscVl).append(storage).append(company)
				.append(address1).append(address2).append(address3)
				.append(city).append(state).append(zip).append(phone)
				.append(tin).append(export).append(IMPORT).append(domestic)
				.append(other).append(daCode).append(byClient).append(vanLine)
				.append(miles).append(lastStars).append(backup)
				.append(beginStars).append(CMPYABBRV).append(dispatch)
				.append(fax).append(LASTEQUIP).append(lastMaster)
				.append(lateBegin).append(lateEnd).append(loadAtLast)
				.append(lastLoss).append(otherPcPay).append(packScreen)
				.append(trackPath).append(willAdvis).append(workDymension)
				.append(persionalPath).append(localTemp).append(postDate)
				.append(sCloseCSO).append(sTestDate).append(oInterval)
				.append(userPath).append(spCode).append(email)
				.append(coordinator).append(vlContract)
				.append(doRegistrationNumber).append(dorecncl)
				.append(scoreForm).append(workDay).append(dayAt2)
				.append(dayAt1).append(lunch2).append(weekdays2).append(lunch1)
				.append(weekdays1).append(srunDate).append(thirdParty)
				.append(numDays).append(maxAttempt).append(doagntcde)
				.append(special2).append(special1).append(minPayCode)
				.append(longDtypes).append(cmpy).append(doIndex)
				.append(pdfprinter).append(runReports).append(midPc)
				.append(createdBy).append(updatedBy).append(createdOn)
				.append(updatedOn).append(postDate1).append(serviceInclude)
				.append(serviceExclude).append(weightUnit).append(volumeUnit)
				.append(isUnigroup).append(uvl).append(mvl)
				.append(accountingSystem).append(accountingInterface)
				.append(filterCoordinator).append(invExtractSeq)
				.append(payExtractSeq).append(prtExtractSeq)
				.append(subContcExtractSeq).append(minimumHoursGurantee)
				.append(salesCommisionRate).append(grossMarginThreshold)
				.append(toDoRuleExecutionTime).append(lastRunDate)
				.append(dailyOperationalLimit).append(minServiceDay)
				.append(dailyOpLimitPC).append(website).append(excludeIPs)
				.append(toDoRuleExecutionTime2).append(surveyExternalIPs)
				.append(healthWelfareRate).append(standardCountryHauling)
				.append(standardCountryCharges).append(rateDeptEmail)
				.append(agentIdRegistrationDistribution).append(commissionable)
				.append(claimShowCportal).append(claimContactPerson)
				.append(claimEmail).append(claimFax).append(claimPhone)
				.append(valuationDetails1).append(valuationDetails2)
				.append(valuationDetails3).append(valuationDetails4)
				.append(valuationDetails5).append(commissionJob)
				.append(currencyMargin).append(currencyPullScheduling)
				.append(companyDivisionAcctgCodeUnique)
				.append(networkCoordinator).append(country)
				.append(defaultDistanceCalc).append(vatCalculation)
				.append(pensionEffectiveDate1).append(pensionEffectiveDate2)
				.append(pensionEffectiveDate3).append(pensionEffectiveDate4)
				.append(pensionRate1).append(pensionRate2).append(pensionRate3)
				.append(pensionRate4).append(costElement).append(transDoc)
				.append(minRec).append(maxRec).append(minPay).append(maxPay)
				.append(suspenseDefault).append(currentRec).append(currentPay)
				.append(vanLineCompanyDivision).append(uvlContract)
				.append(uvlBillToCode).append(uvlBillToName)
				.append(mvlContract).append(mvlBillToCode)
				.append(mvlBillToName).append(intBillToCode)
				.append(intBillToName).append(intContract).append(validation)
				.append(unitVariable).append(truckRequired)
				.append(autoPayablePosting).append(automaticReconcile)
				.append(vanlineMinimumAmount).append(billingCheck).append(POS)
				.append(miscellaneousdefaultdriver)
				.append(vanlineSettleColourStatus).append(dosPartnerCode)
				.append(dosscac).append(costingDetailShow)
				.append(operationMgmtBy).append(rating).append(distribution)
				.append(driverCrewType).append(driverCommissionSetUp)
				.append(noInsurance).append(insurancePerUnit)
				.append(unitForInsurance).append(minReplacementvalue)
				.append(minInsurancePerUnit).append(detailedList)
				.append(noInsuranceCportal).append(lumpSum)
				.append(storageEmail).append(vanlineMaximumAmount)
				.append(vanlineExceptionChargeCode).append(ownbillingVanline)
				.append(paymentApplicationExtractSeq)
				.append(ownbillingBilltoCodes).append(soExtractSeq)
				.append(customerSurveyEmail)
				.append(accountLineAccountPortalFlag).append(passwordPolicy)
				.append(contractChargesMandatory).append(maxUpdatedOn)
				.append(hubWarehouseOperationalLimit).append(localJobs)
				.append(prefix).append(sequence).append(receivableVat)
				.append(payableVat).append(reciprocityjobtype).append(service)
				.append(warehouse).append(daystoManageAlert)
				.append(payableActual).append(receivableActual)
				.append(payableAccrued).append(receivableAccrued)
				.append(defaultDivisionCharges).append(currentOpenPeriod)
				.append(passwordPolicyForExternalUser)
				.append(storageBilling)
				.append(SITBilling) 
				.append(TPSBilling)
				.append(SCSBilling)
				.append(agentSearchValidation)
				.append(usGovJobs)
				.append(accountSearchValidation)
				.append(vatSumThreshhold)
				.append(ediBillToCode)
				.append(employeeExpensesJob) 
				.append(commissionCloseDate) 
				.toHashCode();
	}

	@Column(length = 10)
	public String getGrossMarginThreshold() {
		return grossMarginThreshold;
	}

	public void setGrossMarginThreshold(String grossMarginThreshold) {
		this.grossMarginThreshold = grossMarginThreshold;
	}
	@Column(length = 10)
	public String getSalesCommisionRate() {
		return salesCommisionRate;
	}

	public void setSalesCommisionRate(String salesCommisionRate) {
		this.salesCommisionRate = salesCommisionRate;
	}

	@Column(length = 10)
	public String getMinimumHoursGurantee() {
		return minimumHoursGurantee;
	}

	public void setMinimumHoursGurantee(String minimumHoursGurantee) {
		this.minimumHoursGurantee = minimumHoursGurantee;
	}
	
	@Column(length = 35)
	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	@Column(length = 35)
	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	@Column(length = 35)
	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	@Column(length = 4)
	public int getBackup() {
		return backup;
	}

	public void setBackup(int backup) {
		this.backup = backup;
	}

	@Column(length = 5)
	public String getBeginStars() {
		return beginStars;
	}

	public void setBeginStars(String beginStars) {
		this.beginStars = beginStars;
	}
	@Column(length = 4)
	public int getByClient() {
		return byClient;
	}

	public void setByClient(int byClient) {
		this.byClient = byClient;
	}

	@Column(length = 20)
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(length = 10)
	public String getCmpy() {
		return cmpy;
	}

	public void setCmpy(String cmpy) {
		this.cmpy = cmpy;
	}

	@Column(length = 20)
	public String getCMPYABBRV() {
		return CMPYABBRV;
	}

	public void setCMPYABBRV(String cmpyabbrv) {
		CMPYABBRV = cmpyabbrv;
	}

	@Column(length = 45)
	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	@Column(length = 15)
	public String getCoordinator() {
		return coordinator;
	}

	public void setCoordinator(String coordinator) {
		this.coordinator = coordinator;
	}

	@Column(length = 15)
	public String getCorpID() {
		return corpID;
	}

	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}

	@Column(length = 150)
	public String getDaCode() {
		return daCode;
	}

	public void setDaCode(String daCode) {
		this.daCode = daCode;
	}

	@Column(length = 6)
	public String getDispatch() {
		return dispatch;
	}

	@Column(length = 6)
	public void setDispatch(String dispatch) {
		this.dispatch = dispatch;
	}

	@Column(length = 12)
	public String getDoagntcde() {
		return doagntcde;
	}

	public void setDoagntcde(String doagntcde) {
		this.doagntcde = doagntcde;
	}

	@Column(length = 1)
	public int getDoIndex() {
		return doIndex;
	}

	public void setDoIndex(int doIndex) {
		this.doIndex = doIndex;
	}

	@Column(length = 20)
	public String getDomestic() {
		return domestic;
	}

	public void setDomestic(String domestic) {
		this.domestic = domestic;
	}

	@Column(length = 12)
	public String getDorecncl() {
		return dorecncl;
	}

	public void setDorecncl(String dorecncl) {
		this.dorecncl = dorecncl;
	}

	@Column(length = 12)
	public String getDoRegistrationNumber() {
		return doRegistrationNumber;
	}

	public void setDoRegistrationNumber(String doRegistrationNumber) {
		this.doRegistrationNumber = doRegistrationNumber;
	}

	@Column(length = 65)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 20)
	public String getExport() {
		return export;
	}

	public void setExport(String export) {
		this.export = export;
	}

	@Column(length = 15)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	@Column(length = 20)
	public String getIMPORT() {
		return IMPORT;
	}

	public void setIMPORT(String import1) {
		IMPORT = import1;
	}

	@Column(length = 5)
	public String getOInterval() {
		return oInterval;
	}

	public void setOInterval(String oInterval) {
		this.oInterval = oInterval;
	}
	@Column(length = 4)
	public String getLateBegin() {
		return lateBegin;
	}

	public void setLateBegin(String lateBegin) {
		this.lateBegin = lateBegin;
	}

	@Column(length = 4)
	public String getLateEnd() {
		return lateEnd;
	}

	public void setLateEnd(String lateEnd) {
		this.lateEnd = lateEnd;
	}

	@Column(length = 20)
	public BigDecimal getLASTEQUIP() {
		return LASTEQUIP;
	}

	public void setLASTEQUIP(BigDecimal lastequip) {
		LASTEQUIP = lastequip;
	}
	@Column(length = 20)
	public BigDecimal getLastLoss() {
		return lastLoss;
	}

	public void setLastLoss(BigDecimal lastLoss) {
		this.lastLoss = lastLoss;
	}

	@Column(length = 20)
	public BigDecimal getLastMaster() {
		return lastMaster;
	}

	public void setLastMaster(BigDecimal lastMaster) {
		this.lastMaster = lastMaster;
	}
	@Column(length = 20)
	public BigDecimal getLastSequence() {
		return lastSequence;
	}

	public void setLastSequence(BigDecimal lastSequence) {
		this.lastSequence = lastSequence;
	}

	@Column()
	public Date getLastStars() {
		return lastStars;
	}

	public void setLastStars(Date lastStars) {
		this.lastStars = lastStars;
	}

	@Column(length = 8)
	public String getLoadAtLast() {
		return loadAtLast;
	}

	public void setLoadAtLast(String loadAtLast) {
		this.loadAtLast = loadAtLast;
	}

	@Column(length = 60)
	public String getLocalTemp() {
		return localTemp;
	}

	public void setLocalTemp(String localTemp) {
		this.localTemp = localTemp;
	}

	@Column(length = 55)
	public String getLongDtypes() {
		return longDtypes;
	}

	public void setLongDtypes(String longDtypes) {
		this.longDtypes = longDtypes;
	}
	@Column(length = 20)
	public BigDecimal getMidPc() {
		return midPc;
	}

	public void setMidPc(BigDecimal midPc) {
		this.midPc = midPc;
	}

	@Column(length = 20)
	public BigDecimal getMiles() {
		return miles;
	}

	public void setMiles(BigDecimal miles) {
		this.miles = miles;
	}
	@Column(length = 7)
	public String getMinPayCode() {
		return minPayCode;
	}

	public void setMinPayCode(String minPayCode) {
		this.minPayCode = minPayCode;
	}

	@Column(length = 55)
	public String getMiscVl() {
		return miscVl;
	}

	public void setMiscVl(String miscVl) {
		this.miscVl = miscVl;
	}
	@Column(length = 5)
	public int getNumDays() {
		return numDays;
	}

	public void setNumDays(int numDays) {
		this.numDays = numDays;
	}

	@Column(length = 20)
	public String getOther() {
		return other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	@Column(length = 20)
	public BigDecimal getOtherPcPay() {
		return otherPcPay;
	}

	public void setOtherPcPay(BigDecimal otherPcPay) {
		this.otherPcPay = otherPcPay;
	}

	@Column(length = 28)
	public String getPackScreen() {
		return packScreen;
	}

	public void setPackScreen(String packScreen) {
		this.packScreen = packScreen;
	}

	@Column(length = 45)
	public String getPdfprinter() {
		return pdfprinter;
	}

	public void setPdfprinter(String pdfprinter) {
		this.pdfprinter = pdfprinter;
	}

	@Column(length = 60)
	public String getPersionalPath() {
		return persionalPath;
	}

	public void setPersionalPath(String persionalPath) {
		this.persionalPath = persionalPath;
	}

	@Column(length = 15)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column()
	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	@Column(length = 5)
	public String getRunReports() {
		return runReports;
	}

	public void setRunReports(String runReports) {
		this.runReports = runReports;
	}

	@Column(length = 4)
	public int getSCloseCSO() {
		return sCloseCSO;
	}

	public void setSCloseCSO(int closeCSO) {
		sCloseCSO = closeCSO;
	}

	@Column(length = 20)
	public String getScoreForm() {
		return scoreForm;
	}

	public void setScoreForm(String scoreForm) {
		this.scoreForm = scoreForm;
	}
@Column(length = 21)
	public String getSpCode() {
		return spCode;
	}

	public void setSpCode(String spCode) {
		this.spCode = spCode;
	}

	@Column(length = 20)
	public String getSpecial1() {
		return special1;
	}

	public void setSpecial1(String special1) {
		this.special1 = special1;
	}

	@Column(length = 20)
	public String getSpecial2() {
		return special2;
	}

	public void setSpecial2(String special2) {
		this.special2 = special2;
	}

	@Column()
	public Date getSrunDate() {
		return srunDate;
	}

	public void setSrunDate(Date srunDate) {
		this.srunDate = srunDate;
	}

	@Column(length = 2)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(length = 100)
	public String getStorage() {
		return storage;
	}

	public void setStorage(String storage) {
		this.storage = storage;
	}

	@Column()
	public Date getSTestDate() {
		return sTestDate;
	}

	public void setSTestDate(Date testDate) {
		sTestDate = testDate;
	}
	@Column(length = 25)
	public String getTin() {
		return tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	@Column(length = 60)
	public String getTrackPath() {
		return trackPath;
	}

	public void setTrackPath(String trackPath) {
		this.trackPath = trackPath;
	}
	@Column(length = 50)
	public String getUserPath() {
		return userPath;
	}

	public void setUserPath(String userPath) {
		this.userPath = userPath;
	}
	@Column(length = 12)
	public String getVanLine() {
		return vanLine;
	}

	public void setVanLine(String vanLine) {
		this.vanLine = vanLine;
	}

	@Column(length = 20)
	public String getVlContract() {
		return vlContract;
	}

	public void setVlContract(String vlContract) {
		this.vlContract = vlContract;
	}
	@Column()
	public Date getWillAdvis() {
		return willAdvis;
	}

	public void setWillAdvis(Date willAdvis) {
		this.willAdvis = willAdvis;
	}
	@Column(length = 5)
	public int getWorkDymension() {
		return workDymension;
	}

	public void setWorkDymension(int workDymension) {
		this.workDymension = workDymension;
	}

	@Column(length = 11)
	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	//New Field 

	@Column(length = 82)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column
	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	@Column(length = 82)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length = 25)
	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	@Column(length = 25)
	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	@Column(length = 25)
	public String getDayAt1() {
		return dayAt1;
	}

	public void setDayAt1(String dayAt1) {
		this.dayAt1 = dayAt1;
	}

	@Column(length = 25)
	public String getDayAt2() {
		return dayAt2;
	}

	public void setDayAt2(String dayAt2) {
		this.dayAt2 = dayAt2;
	}
	@Column(length = 25)
	public String getLunch1() {
		return lunch1;
	}

	public void setLunch1(String lunch1) {
		this.lunch1 = lunch1;
	}

	@Column(length = 25)
	public String getLunch2() {
		return lunch2;
	}

	public void setLunch2(String lunch2) {
		this.lunch2 = lunch2;
	}
	@Column(length = 25)
	public String getWeekdays1() {
		return weekdays1;
	}

	public void setWeekdays1(String weekdays1) {
		this.weekdays1 = weekdays1;
	}

	@Column(length = 25)
	public String getWeekdays2() {
		return weekdays2;
	}

	public void setWeekdays2(String weekdays2) {
		this.weekdays2 = weekdays2;
	}

	@Column(length = 25)
	public String getWorkDay() {
		return workDay;
	}

	public void setWorkDay(String workDay) {
		this.workDay = workDay;
	}

	@Column
	public boolean isThirdParty() {
		return thirdParty;
	}

	public void setThirdParty(boolean thirdParty) {
		this.thirdParty = thirdParty;
	}

	@Column()
	public Date getPostDate1() {
		return postDate1;
	}

	public void setPostDate1(Date postDate1) {
		this.postDate1 = postDate1;
	}

	@Column(length = 3000)
	public String getServiceExclude() {
		return serviceExclude;
	}

	public void setServiceExclude(String serviceExclude) {
		this.serviceExclude = serviceExclude;
	}

	@Column(length = 5000)
	public String getServiceInclude() {
		return serviceInclude;
	}

	public void setServiceInclude(String serviceInclude) {
		this.serviceInclude = serviceInclude;
	}

	@Column(length = 10)
	public String getVolumeUnit() {
		return volumeUnit;
	}

	public void setVolumeUnit(String volumeUnit) {
		this.volumeUnit = volumeUnit;
	}

	@Column(length = 10)
	public String getWeightUnit() {
		return weightUnit;
	}

	public void setWeightUnit(String weightUnit) {
		this.weightUnit = weightUnit;
	}

	public String getIsUnigroup() {
		return isUnigroup;
	}

	public void setIsUnigroup(String isUnigroup) {
		this.isUnigroup = isUnigroup;
	}

	public String getMvl() {
		return mvl;
	}

	public void setMvl(String mvl) {
		this.mvl = mvl;
	}

	public String getUvl() {
		return uvl;
	}

	public void setUvl(String uvl) {
		this.uvl = uvl;
	}
	@Column(length = 2)
	public String getAccountingInterface() {
		return accountingInterface;
	}

	public void setAccountingInterface(String accountingInterface) {
		this.accountingInterface = accountingInterface;
	}
	@Column(length = 50)
	public String getAccountingSystem() {
		return accountingSystem;
	}

	public void setAccountingSystem(String accountingSystem) {
		this.accountingSystem = accountingSystem;
	}
	@Column(length =3)
	public String getFilterCoordinator() {
		return filterCoordinator;
	}

	public void setFilterCoordinator(String filterCoordinator) {
		this.filterCoordinator = filterCoordinator;
	}
	@Column(length =5)
	public String getInvExtractSeq() {
		return invExtractSeq;
	}

	public void setInvExtractSeq(String invExtractSeq) {
		this.invExtractSeq = invExtractSeq;
	}
	@Column(length =5)
	public String getPayExtractSeq() {
		return payExtractSeq;
	}

	public void setPayExtractSeq(String payExtractSeq) {
		this.payExtractSeq = payExtractSeq;
	}
	@Column(length =5)
	public String getPrtExtractSeq() {
		return prtExtractSeq;
	}

	public void setPrtExtractSeq(String prtExtractSeq) {
		this.prtExtractSeq = prtExtractSeq;
	}
	@Column(length =5)
	public String getSubContcExtractSeq() {
		return subContcExtractSeq;
	}

	public void setSubContcExtractSeq(String subContcExtractSeq) {
		this.subContcExtractSeq = subContcExtractSeq;
	}
	@Column
	public Date getLastRunDate() {
		return lastRunDate;
	}

	public void setLastRunDate(Date lastRunDate) {
		this.lastRunDate = lastRunDate;
	}
	@Column(length=20)
	public String getToDoRuleExecutionTime() {
		return toDoRuleExecutionTime;
	}

	public void setToDoRuleExecutionTime(String toDoRuleExecutionTime) {
		this.toDoRuleExecutionTime = toDoRuleExecutionTime;
	}
	@Column(length=10)
	public String getDailyOperationalLimit() {
		return dailyOperationalLimit;
	}

	public void setDailyOperationalLimit(String dailyOperationalLimit) {
		this.dailyOperationalLimit = dailyOperationalLimit;
	}
	@Column(length=10)
	public String getMinServiceDay() {
		return minServiceDay;
	}

	public void setMinServiceDay(String minServiceDay) {
		this.minServiceDay = minServiceDay;
	}
	@Column(length=10)
	public String getDailyOpLimitPC() {
		return dailyOpLimitPC;
	}

	public void setDailyOpLimitPC(String dailyOpLimitPC) {
		this.dailyOpLimitPC = dailyOpLimitPC;
	}

	/**
	 * @return the website
	 */
	@Column(length=100)
	public String getWebsite() {
		return website;
	}

	/**
	 * @param website the website to set
	 */
	public void setWebsite(String website) {
		this.website = website;
	}

	/**
	 * @return the maxAttempt
	 */
	@Column(length = 5)
	public int getMaxAttempt() {
		return maxAttempt;
	}

	/**
	 * @param maxAttempt the maxAttempt to set
	 */
	public void setMaxAttempt(int maxAttempt) {
		this.maxAttempt = maxAttempt;
	}
	@Column(length = 100)
	public String getExcludeIPs() {
		return excludeIPs;
	}

	public void setExcludeIPs(String excludeIPs) {
		this.excludeIPs = excludeIPs;
	}

	public String getToDoRuleExecutionTime2() {
		return toDoRuleExecutionTime2;
	}

	public void setToDoRuleExecutionTime2(String toDoRuleExecutionTime2) {
		this.toDoRuleExecutionTime2 = toDoRuleExecutionTime2;
	}
	@Column(length = 225)
	public String getSurveyExternalIPs() {
		return surveyExternalIPs;
	}

	public void setSurveyExternalIPs(String surveyExternalIPs) {
		this.surveyExternalIPs = surveyExternalIPs;
	}

	public BigDecimal getHealthWelfareRate() {
		return healthWelfareRate;
	}

	public void setHealthWelfareRate(BigDecimal healthWelfareRate) {
		this.healthWelfareRate = healthWelfareRate;
	}

	public String getStandardCountryCharges() {
		return standardCountryCharges;
	}

	public void setStandardCountryCharges(String standardCountryCharges) {
		this.standardCountryCharges = standardCountryCharges;
	}

	public String getStandardCountryHauling() {
		return standardCountryHauling;
	}

	public void setStandardCountryHauling(String standardCountryHauling) {
		this.standardCountryHauling = standardCountryHauling;
	}
	@Column
	public String getRateDeptEmail() {
		return rateDeptEmail;
	}

	public void setRateDeptEmail(String rateDeptEmail) {
		this.rateDeptEmail = rateDeptEmail;
	}
	@Column(length = 65)
	public String getAgentIdRegistrationDistribution() {
		return agentIdRegistrationDistribution;
	}

	public void setAgentIdRegistrationDistribution(
			String agentIdRegistrationDistribution) {
		this.agentIdRegistrationDistribution = agentIdRegistrationDistribution;
	}
	@Column(length = 82)
	public String getClaimContactPerson() {
		return claimContactPerson;
	}

	public void setClaimContactPerson(String claimContactPerson) {
		this.claimContactPerson = claimContactPerson;
	}
	@Column(length = 65)
	public String getClaimEmail() {
		return claimEmail;
	}

	public void setClaimEmail(String claimEmail) {
		this.claimEmail = claimEmail;
	}
	@Column(length = 20)
	public String getClaimFax() {
		return claimFax;
	}

	public void setClaimFax(String claimFax) {
		this.claimFax = claimFax;
	}
	@Column(length = 20)
	public String getClaimPhone() {
		return claimPhone;
	}

	public void setClaimPhone(String claimPhone) {
		this.claimPhone = claimPhone;
	}
    @Column
	public boolean isClaimShowCportal() {
		return claimShowCportal;
	}

	public void setClaimShowCportal(boolean claimShowCportal) {
		this.claimShowCportal = claimShowCportal;
	}
	@Column(length = 50)
	public String getValuationDetails1() {
		return valuationDetails1;
	}

	public void setValuationDetails1(String valuationDetails1) {
		this.valuationDetails1 = valuationDetails1;
	}
	@Column(length = 50)
	public String getValuationDetails2() {
		return valuationDetails2;
	}

	public void setValuationDetails2(String valuationDetails2) {
		this.valuationDetails2 = valuationDetails2;
	}
	@Column(length = 50)
	public String getValuationDetails3() {
		return valuationDetails3;
	}

	public void setValuationDetails3(String valuationDetails3) {
		this.valuationDetails3 = valuationDetails3;
	}
	@Column(length = 50)
	public String getValuationDetails4() {
		return valuationDetails4;
	}

	public void setValuationDetails4(String valuationDetails4) {
		this.valuationDetails4 = valuationDetails4;
	}
	@Column(length = 100)
	public String getValuationDetails5() {
		return valuationDetails5;
	}

	public void setValuationDetails5(String valuationDetails5) {
		this.valuationDetails5 = valuationDetails5;
	}

	@Column(length = 55)
	public String getCommissionJob() {
		return commissionJob;
	}

	public void setCommissionJob(String commissionJob) {
		this.commissionJob = commissionJob;
	}
	@Column(length = 20)
	public BigDecimal getCurrencyMargin() {
		return currencyMargin;
	}

	public void setCurrencyMargin(BigDecimal currencyMargin) {
		this.currencyMargin = currencyMargin;
	}
	@Column(length = 20)
	public String getCurrencyPullScheduling() {
		return currencyPullScheduling;
	}

	public void setCurrencyPullScheduling(String currencyPullScheduling) {
		this.currencyPullScheduling = currencyPullScheduling;
	}
	@Column(length = 3)
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompanyDivisionAcctgCodeUnique() {
		return companyDivisionAcctgCodeUnique;
	}

	public void setCompanyDivisionAcctgCodeUnique(
			String companyDivisionAcctgCodeUnique) {
		this.companyDivisionAcctgCodeUnique = companyDivisionAcctgCodeUnique;
	}

	@Column( length=82 )
	public String getNetworkCoordinator() {
		return networkCoordinator;
	}

	public void setNetworkCoordinator(String networkCoordinator) {
		this.networkCoordinator = networkCoordinator;
	}
	@Column
	public Boolean getVatCalculation() {
		return vatCalculation;
	}

	public void setVatCalculation(Boolean vatCalculation) {
		this.vatCalculation = vatCalculation;
	}
	@Column(length=5)
	public String getDefaultDistanceCalc() {
		return defaultDistanceCalc;
	}

	public void setDefaultDistanceCalc(String defaultDistanceCalc) {
		this.defaultDistanceCalc = defaultDistanceCalc;
	}
	@Column
	public Date getPensionEffectiveDate1() {
	    return pensionEffectiveDate1;
	}

	public void setPensionEffectiveDate1(Date pensionEffectiveDate1) {
	    this.pensionEffectiveDate1 = pensionEffectiveDate1;
	}
	@Column
	public Date getPensionEffectiveDate2() {
	    return pensionEffectiveDate2;
	}

	public void setPensionEffectiveDate2(Date pensionEffectiveDate2) {
	    this.pensionEffectiveDate2 = pensionEffectiveDate2;
	}
	@Column
	public Date getPensionEffectiveDate3() {
	    return pensionEffectiveDate3;
	}

	public void setPensionEffectiveDate3(Date pensionEffectiveDate3) {
	    this.pensionEffectiveDate3 = pensionEffectiveDate3;
	}
	@Column
	public Date getPensionEffectiveDate4() {
	    return pensionEffectiveDate4;
	}

	public void setPensionEffectiveDate4(Date pensionEffectiveDate4) {
	    this.pensionEffectiveDate4 = pensionEffectiveDate4;
	}
	@Column(length = 20)
	public BigDecimal getPensionRate1() {
	    return pensionRate1;
	}
	
	public void setPensionRate1(BigDecimal pensionRate1) {
	    this.pensionRate1 = pensionRate1;
	}
	@Column(length = 20)
	public BigDecimal getPensionRate2() {
	    return pensionRate2;
	}

	public void setPensionRate2(BigDecimal pensionRate2) {
	    this.pensionRate2 = pensionRate2;
	}
	@Column(length = 20)
	public BigDecimal getPensionRate3() {
	    return pensionRate3;
	}

	public void setPensionRate3(BigDecimal pensionRate3) {
	    this.pensionRate3 = pensionRate3;
	}
	@Column(length = 20)
	public BigDecimal getPensionRate4() {
	    return pensionRate4;
	}

	public void setPensionRate4(BigDecimal pensionRate4) {
	    this.pensionRate4 = pensionRate4;
	}
	@Column
	public Boolean getCostElement() {
		return costElement;
	}

	public void setCostElement(Boolean costElement) {
		this.costElement = costElement;
	}
	@Column(length = 125)
	public String getTransDoc() {
		return transDoc;
	}

	public void setTransDoc(String transDoc) {
		this.transDoc = transDoc;
	}

	@Column
	public String getMinRec() {
		return minRec;
	}

	public void setMinRec(String minRec) {
		this.minRec = minRec;
	}

	@Column
	public String getMaxRec() {
		return maxRec;
	}

	public void setMaxRec(String maxRec) {
		this.maxRec = maxRec;
	}

	@Column
	public String getMinPay() {
		return minPay;
	}

	public void setMinPay(String minPay) {
		this.minPay = minPay;
	}

	@Column
	public String getMaxPay() {
		return maxPay;
	}

	public void setMaxPay(String maxPay) {
		this.maxPay = maxPay;
	}

	@Column
	public String getSuspenseDefault() {
		return suspenseDefault;
	}

	public void setSuspenseDefault(String suspenseDefault) {
		this.suspenseDefault = suspenseDefault;
	}

	@Column
	public String getCurrentRec() {
		return currentRec;
	}

	public void setCurrentRec(String currentRec) {
		this.currentRec = currentRec;
	}

	@Column
	public String getCurrentPay() {
		return currentPay;
	}

	public void setCurrentPay(String currentPay) {
		this.currentPay = currentPay;
	}
	@Column
	public String getVanLineCompanyDivision() {
		return vanLineCompanyDivision;
	}

	public void setVanLineCompanyDivision(String vanLineCompanyDivision) {
		this.vanLineCompanyDivision = vanLineCompanyDivision;
	}
	@Column
	public String getUvlContract() {
		return uvlContract;
	}

	public void setUvlContract(String uvlContract) {
		this.uvlContract = uvlContract;
	}
	@Column
	public String getUvlBillToCode() {
		return uvlBillToCode;
	}

	public void setUvlBillToCode(String uvlBillToCode) {
		this.uvlBillToCode = uvlBillToCode;
	}
	@Column
	public String getUvlBillToName() {
		return uvlBillToName;
	}

	public void setUvlBillToName(String uvlBillToName) {
		this.uvlBillToName = uvlBillToName;
	}
	@Column
	public String getMvlContract() {
		return mvlContract;
	}

	public void setMvlContract(String mvlContract) {
		this.mvlContract = mvlContract;
	}
	@Column
	public String getMvlBillToCode() {
		return mvlBillToCode;
	}

	public void setMvlBillToCode(String mvlBillToCode) {
		this.mvlBillToCode = mvlBillToCode;
	}
	@Column
	public String getMvlBillToName() {
		return mvlBillToName;
	}

	public void setMvlBillToName(String mvlBillToName) {
		this.mvlBillToName = mvlBillToName;
	}

	@Column
	public String getCommissionable() {
		return commissionable;
	}

	public void setCommissionable(String commissionable) {
		this.commissionable = commissionable;
	}

	@Column
	public String getValidation() {
		return validation;
	}

	public void setValidation(String validation) {
		this.validation = validation;
	}

	public String getUnitVariable() {
		return unitVariable;
	}

	public void setUnitVariable(String unitVariable) {
		this.unitVariable = unitVariable;
	}

	public String getTruckRequired() {
		return truckRequired;
	}

	public void setTruckRequired(String truckRequired) {
		this.truckRequired = truckRequired;
	}
	@Column
	public Boolean getAutoPayablePosting() {
		return autoPayablePosting;
	}

	public void setAutoPayablePosting(Boolean autoPayablePosting) {
		this.autoPayablePosting = autoPayablePosting;
	}
	@Column
	public Boolean getAutomaticReconcile() {
		return automaticReconcile;
	}

	public void setAutomaticReconcile(Boolean automaticReconcile) {
		this.automaticReconcile = automaticReconcile;
	}
	@Column
	public BigDecimal getVanlineMinimumAmount() {
		return vanlineMinimumAmount;
	}

	public void setVanlineMinimumAmount(BigDecimal vanlineMinimumAmount) {
		this.vanlineMinimumAmount = vanlineMinimumAmount;
	}
	@Column
	public Boolean getBillingCheck() {
		return billingCheck;
	}

	public void setBillingCheck(Boolean billingCheck) {
		this.billingCheck = billingCheck;
	}
	@Column
	public Boolean getPOS() {
		return POS;
	}

	public void setPOS(Boolean pOS) {
		POS = pOS;
	}
	@Column
	public String getMiscellaneousdefaultdriver() {
		return miscellaneousdefaultdriver;
	}

	public void setMiscellaneousdefaultdriver(String miscellaneousdefaultdriver) {
		this.miscellaneousdefaultdriver = miscellaneousdefaultdriver;
	}

	public Boolean getVanlineSettleColourStatus() {
		return vanlineSettleColourStatus;
	}

	public void setVanlineSettleColourStatus(Boolean vanlineSettleColourStatus) {
		this.vanlineSettleColourStatus = vanlineSettleColourStatus;
	}
	
	@Column(length = 150)
	public String getDosPartnerCode() {
		return dosPartnerCode;
	}

	public void setDosPartnerCode(String dosPartnerCode) {
		this.dosPartnerCode = dosPartnerCode;
	}
	@Column(length = 4)
	public String getDosscac() {
		return dosscac;
	}
	
	public void setDosscac(String dosscac) {
		this.dosscac = dosscac;
	}
	@Column
	public Boolean getCostingDetailShow() {
		return costingDetailShow;
	}

	public void setCostingDetailShow(Boolean costingDetailShow) {
		this.costingDetailShow = costingDetailShow;
	}

	public String getOperationMgmtBy() {
		return operationMgmtBy;
	}

	public void setOperationMgmtBy(String operationMgmtBy) {
		this.operationMgmtBy = operationMgmtBy;
	}

	@Column
	public Boolean getRating() {
		return rating;
	}

	public void setRating(Boolean rating) {
		this.rating = rating;
	}
	
	@Column
	public Boolean getDistribution() {
		return distribution;
	}

	public void setDistribution(Boolean distribution) {
		this.distribution = distribution;
	}
	@Column(length = 100)
	public String getDriverCrewType() {
		return driverCrewType;
	}

	public void setDriverCrewType(String driverCrewType) {
		this.driverCrewType = driverCrewType;
	}

	@Column
	public Boolean getDriverCommissionSetUp() {
		return driverCommissionSetUp;
	}

	public void setDriverCommissionSetUp(Boolean driverCommissionSetUp) {
		this.driverCommissionSetUp = driverCommissionSetUp;
	}
	@Column
	public Boolean getNoInsurance() {
		return noInsurance;
	}

	public void setNoInsurance(Boolean noInsurance) {
		this.noInsurance = noInsurance;
	}
	@Column
	public String getInsurancePerUnit() {
		return insurancePerUnit;
	}

	public void setInsurancePerUnit(String insurancePerUnit) {
		this.insurancePerUnit = insurancePerUnit;
	}
	@Column
	public String getUnitForInsurance() {
		return unitForInsurance;
	}

	public void setUnitForInsurance(String unitForInsurance) {
		this.unitForInsurance = unitForInsurance;
	}
	@Column
	public String getMinReplacementvalue() {
		return minReplacementvalue;
	}

	public void setMinReplacementvalue(String minReplacementvalue) {
		this.minReplacementvalue = minReplacementvalue;
	}
	@Column
	public String getMinInsurancePerUnit() {
		return minInsurancePerUnit;
	}

	public void setMinInsurancePerUnit(String minInsurancePerUnit) {
		this.minInsurancePerUnit = minInsurancePerUnit;
	}
	
	public String getStorageEmail() {
		return storageEmail;
	}

	public void setStorageEmail(String storageEmail) {
		this.storageEmail = storageEmail;
	}
	@Column
	public Boolean getDetailedList() {
		return detailedList;
	}

	public void setDetailedList(Boolean detailedList) {
		this.detailedList = detailedList;
	}
	@Column
	public Boolean getNoInsuranceCportal() {
		return noInsuranceCportal;
	}

	public void setNoInsuranceCportal(Boolean noInsuranceCportal) {
		this.noInsuranceCportal = noInsuranceCportal;
	}
	@Column
	public Boolean getLumpSum() {
		return lumpSum;
	}

	public void setLumpSum(Boolean lumpSum) {
		this.lumpSum = lumpSum;
	}

	@Column
	public BigDecimal getVanlineMaximumAmount() {
		return vanlineMaximumAmount;
	}

	public void setVanlineMaximumAmount(BigDecimal vanlineMaximumAmount) {
		this.vanlineMaximumAmount = vanlineMaximumAmount;
	}

	@Column
	public String getVanlineExceptionChargeCode() {
		return vanlineExceptionChargeCode;
	}

	public void setVanlineExceptionChargeCode(String vanlineExceptionChargeCode) {
		this.vanlineExceptionChargeCode = vanlineExceptionChargeCode;
	}

	@Column
	public Boolean getOwnbillingVanline() {
		return ownbillingVanline;
	}

	public void setOwnbillingVanline(Boolean ownbillingVanline) {
		this.ownbillingVanline = ownbillingVanline;
	}
	@Column
	public String getPaymentApplicationExtractSeq() {
		return paymentApplicationExtractSeq;
	}

	public void setPaymentApplicationExtractSeq(String paymentApplicationExtractSeq) {
		this.paymentApplicationExtractSeq = paymentApplicationExtractSeq;
	}
	@Column
	public String getOwnbillingBilltoCodes() {
		return ownbillingBilltoCodes;
	}

	public void setOwnbillingBilltoCodes(String ownbillingBilltoCodes) {
		this.ownbillingBilltoCodes = ownbillingBilltoCodes;
	}

	@Column
	public String getSoExtractSeq() {
		return soExtractSeq;
	}

	public void setSoExtractSeq(String soExtractSeq) {
		this.soExtractSeq = soExtractSeq;
	}
	@Column
	public String getCustomerSurveyEmail() {
		return customerSurveyEmail;
	}

	public void setCustomerSurveyEmail(String customerSurveyEmail) {
		this.customerSurveyEmail = customerSurveyEmail;
	}
	@Column
	public Boolean getAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}

	public void setAccountLineAccountPortalFlag(Boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}

	@Column
	public Boolean getPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(Boolean passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}
	@Column(name="maxupdatedon")
	public Date getMaxUpdatedOn() {
		return maxUpdatedOn;
	}

	public void setMaxUpdatedOn(Date maxUpdatedOn) {
		this.maxUpdatedOn = maxUpdatedOn;
	}

	public String getHubWarehouseOperationalLimit() {
		return hubWarehouseOperationalLimit;
	}

	public void setHubWarehouseOperationalLimit(String hubWarehouseOperationalLimit) {
		this.hubWarehouseOperationalLimit = hubWarehouseOperationalLimit;
	}

	@Column
	public String getLocalJobs() {
		return localJobs;
	}

	public void setLocalJobs(String localJobs) {
		this.localJobs = localJobs;
	}
	@Column
	public String getReceivableVat() {
		return receivableVat;
	}

	public void setReceivableVat(String receivableVat) {
		this.receivableVat = receivableVat;
	}
	@Column
	public String getPayableVat() {
		return payableVat;
	}

	public void setPayableVat(String payableVat) {
		this.payableVat = payableVat;
	}
	@Column
	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	@Column
	public String getSequence() {
		return sequence;
	}

	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
    @Column
	public String getReciprocityjobtype() {
		return reciprocityjobtype;
	}

	public void setReciprocityjobtype(String reciprocityjobtype) {
		this.reciprocityjobtype = reciprocityjobtype;
	}
	 @Column
	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}
	 @Column
	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public int getDaystoManageAlert() {
		return daystoManageAlert;
	}

	public void setDaystoManageAlert(int daystoManageAlert) {
		this.daystoManageAlert = daystoManageAlert;
	}
     @Column
	public String getContractChargesMandatory() {
		return contractChargesMandatory;
	}

	public void setContractChargesMandatory(String contractChargesMandatory) {
		this.contractChargesMandatory = contractChargesMandatory;
	}
	 @Column
	public String getPayableActual() {
		return payableActual;
	}

	public void setPayableActual(String payableActual) {
		this.payableActual = payableActual;
	}
	 @Column
	public String getReceivableActual() {
		return receivableActual;
	}

	public void setReceivableActual(String receivableActual) {
		this.receivableActual = receivableActual;
	}
	 @Column
	public String getPayableAccrued() {
		return payableAccrued;
	}

	public void setPayableAccrued(String payableAccrued) {
		this.payableAccrued = payableAccrued;
	}
	 @Column
	public String getReceivableAccrued() {
		return receivableAccrued;
	}

	public void setReceivableAccrued(String receivableAccrued) {
		this.receivableAccrued = receivableAccrued;
	}

	public String getIntBillToCode() {
		return intBillToCode;
	}

	public void setIntBillToCode(String intBillToCode) {
		this.intBillToCode = intBillToCode;
	}

	public String getIntBillToName() {
		return intBillToName;
	}

	public void setIntBillToName(String intBillToName) {
		this.intBillToName = intBillToName;
	}

	public String getIntContract() {
		return intContract;
	}

	public void setIntContract(String intContract) {
		this.intContract = intContract;
	}

	public String getDefaultDivisionCharges() {
		return defaultDivisionCharges;
	}

	public void setDefaultDivisionCharges(String defaultDivisionCharges) {
		this.defaultDivisionCharges = defaultDivisionCharges;
	}

	public Boolean getCurrentOpenPeriod() {
		return currentOpenPeriod;
	}

	public void setCurrentOpenPeriod(Boolean currentOpenPeriod) {
		this.currentOpenPeriod = currentOpenPeriod;
	}
	 @Column
	public Boolean getPasswordPolicyForExternalUser() {
		return passwordPolicyForExternalUser;
	}

	public void setPasswordPolicyForExternalUser(
			Boolean passwordPolicyForExternalUser) {
		this.passwordPolicyForExternalUser = passwordPolicyForExternalUser;
	}
	
    @Column
	public String getStorageBilling() {
		return storageBilling;
	}

	public void setStorageBilling(String storageBilling) {
		this.storageBilling = storageBilling;
	}

	 @Column
	public String getSITBilling() {
		return SITBilling;
	}

	public void setSITBilling(String sITBilling) {
		SITBilling = sITBilling;
	}

	 @Column
	public String getTPSBilling() {
		return TPSBilling;
	}

	public void setTPSBilling(String tPSBilling) {
		TPSBilling = tPSBilling;
	}

	@Column
	public String getSCSBilling() {
		return SCSBilling;
	}

	public void setSCSBilling(String sCSBilling) {
		SCSBilling = sCSBilling;
	}
	@Column
	public Boolean getAgentSearchValidation() {
		return agentSearchValidation;
	}

	public void setAgentSearchValidation(Boolean agentSearchValidation) {
		this.agentSearchValidation = agentSearchValidation;
	}
   
	@Column
	public String getUsGovJobs() {
		return usGovJobs;
	}

	public void setUsGovJobs(String usGovJobs) {
		this.usGovJobs = usGovJobs;
	}
	@Column
	public Boolean getAccountSearchValidation() {
		return accountSearchValidation;
	}

	public void setAccountSearchValidation(Boolean accountSearchValidation) {
		this.accountSearchValidation = accountSearchValidation;
	}
	@Column
	public BigDecimal getVatSumThreshhold() {
		return vatSumThreshhold;
	}

	public void setVatSumThreshhold(BigDecimal vatSumThreshhold) {
		this.vatSumThreshhold = vatSumThreshhold;
	}

	/**
	 * @return the ediBillToCode
	 */
	@Column
	public String getEdiBillToCode() {
		return ediBillToCode;
	}

	/**
	 * @param ediBillToCode the ediBillToCode to set
	 */
	public void setEdiBillToCode(String ediBillToCode) {
		this.ediBillToCode = ediBillToCode;
	}

	@Column
	public String getEmployeeExpensesJob() {
		return employeeExpensesJob;
	}

	public void setEmployeeExpensesJob(String employeeExpensesJob) {
		this.employeeExpensesJob = employeeExpensesJob;
	}

	@Column
	public Date getCommissionCloseDate() {
		return commissionCloseDate;
	}

	public void setCommissionCloseDate(Date commissionCloseDate) {
		this.commissionCloseDate = commissionCloseDate;
	}
	
}
