package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.StandardDeductionsDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.StandardDeductions;
import com.trilasoft.app.model.SubcontractorCharges;

public class StandardDeductionsDaoHibernate extends GenericDaoHibernate<StandardDeductions, Long> implements StandardDeductionsDao{
	public StandardDeductionsDaoHibernate() {
		super(StandardDeductions.class);
	}
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}
	public List getSDListByPartnerCode(String partnerCode) {
		return getHibernateTemplate().find("from StandardDeductions where partnerCode = '"+partnerCode+"'");
	}

	public void deleteSubContChargesForStandardDeduction(String companyDivision, String corpID) {
		this.getSession().createSQLQuery("delete from subcontractorcharges where companyDivision = '"+companyDivision+"' AND deductTempFlag = true AND corpID = '"+corpID+"'").executeUpdate();
	}

	public List deuctionPreviewList(Date fromDate, String companyDivision, String sessionCorpID, String process, String ownerPayTo) {
		String dateFrom = "";
		String deductionType = "";
		if (!(fromDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(fromDate));
			dateFrom = nowYYYYMMDD1.toString();
		}
		
		if(process.equalsIgnoreCase("DedProcess")){
			deductionType = "ch.deductionType = 'Per Schedule' AND ";
		}else if(process.equalsIgnoreCase("DriverProcess")){
			deductionType = "ch.deductionType IN ('Per Trip','Per Pay','Per Schedule') AND ";
		}else{
			deductionType ="";
		}
		
		String ownerPayToQuery = "";
		
		if(ownerPayTo.trim().length() >0)
			ownerPayToQuery = " AND sd.partnerCode = '"+ownerPayTo+"' ";
		
		String queryString ="SELECT distinct sd.id as sid, sd.chargeCode, ch.frequency, sd.amount, sd.deduction, " + //0,1,2,3,4
							"sd.rate, sd.maxDeductAmt, sd.lastDeductionDate, r.companyDivision, " + //5,6,7,8
							"ch.priceType, ch.quantity2preset, ch.priceFormula, a.partnerCode, a.lastName, " +//9,10,11,12,13
							"ch.id as cid, b.driverAgency " + //14,15
							"FROM partnerpublic a, partnerprivate b, standarddeductions sd, charges ch, partneraccountref r " +
							"WHERE a.partnerCode = sd.partnerCode " +
							"AND a.partnerCode = b.partnerCode " +
							"AND sd.chargeCode = ch.charge " +
							"AND ch.corpID ='"+sessionCorpID+"' "  +
							"AND "+deductionType+" ch.contract = 'Driver Settlement' " +
							"AND r.partnerCode = a.partnerCode and r.corpID IN ('"+sessionCorpID+"') " +
							"AND r.companyDivision = '"+companyDivision+"' " +
							"AND a.status = 'Approved' " +
							"AND a.corpID IN ('"+sessionCorpID+"','TSFT','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') " +
							"AND a.isOwnerOp is true " + ownerPayToQuery +
							"AND ((sd.lastDeductionDate <= '"+dateFrom+"') OR sd.lastDeductionDate is null) " +
							"AND sd.status = 'Open' GROUP BY sd.id ORDER BY a.partnerCode";
		
		
		List list = this.getSession().createSQLQuery(queryString).list();
		
		return list;
	}
	
	public List getStandardDeductionPreviewList(Date fromDate, String companyDivision, String corpID, String process,String ownerPayTo){
		List <Object> previewList = new ArrayList<Object>();
		String queryString="";
		if(ownerPayTo.equalsIgnoreCase("")){
			queryString = "SELECT a.id, a.vendorCode, a.estimateVendorName, a.distributionAmount, a.actualExpense, 0, " +
			"a.description, a.chargeCode, a.recAccDate , '', 'Approved', date_format(a.payPostDate,'%d-%b-%Y'), " +
			"a.payAccDate, p.driverAgency as branch, '' as parentId, s.registrationnumber as regNum,'' as balanceForward, '' as tempFlag, m.carrier as tractTrailer,s.shipNumber as shipNum " +
			"FROM accountline a " +
			"inner join partner p on a.vendorcode = p.partnercode " +
			"inner join miscellaneous m on m.id = a.serviceorderid " +
			//"inner join truck t on t.localNumber=m.carrier " +
			"inner join serviceorder s on s.id = a.serviceorderid " +
			"WHERE a.corpID = '"+corpID+"' AND a.actualExpense <> 0 and a.settledDate is null AND a.payingStatus = 'A' and a.status is true " +
			"AND p.isOwnerOp is true AND m.status = 'Ready to Settle' " +
			"UNION " +
			"SELECT s.id, s.personId, s.personName, 0, 0, s.amount, s.description, s.glCode, '' as acDate, date_format(s.approved,'%d-%b-%Y'), " +
			"'' as ppDate, date_format(s.post,'%d-%b-%Y'), '' paDate, s.branch, s.parentId , '' as regNum, if(s.balanceForward=1,'true','false'), if(s.deductTempFlag=1,'true','false') ,pp.truckID as tractTrailer,'' as shipNum " +
			"FROM  subcontractorcharges s " +
			"inner join partneraccountref r on r.partnerCode = s.personId and r.corpID IN ('"+corpID+"')"+
			"inner join partnerprivate pp on pp.partnerCode = s.personId and pp.corpID IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"')"+
			"WHERE s.personType = 'D' AND r.companyDivision = '"+companyDivision+"' "+
			"AND s.corpID = '"+corpID+"' AND s.isSettled is false ";
		}else{
		   queryString = "SELECT a.id, a.vendorCode, a.estimateVendorName, a.distributionAmount, a.actualExpense, 0, " +
			"a.description, a.chargeCode, a.recAccDate , '', 'Approved', date_format(a.payPostDate,'%d-%b-%Y'), " +
			"a.payAccDate, p.driverAgency as branch, '' as parentId, s.registrationnumber as regNum,'' as balanceForward, '' as tempFlag, m.carrier as tractTrailer , s.shipNumber as shipNum " +
			"FROM accountline a " +
			"inner join partner p on a.vendorcode = p.partnercode " +
			"inner join miscellaneous m on m.id = a.serviceorderid " +
			//"inner join truck t on t.localNumber=m.carrier " +
			"inner join serviceorder s on s.id = a.serviceorderid " +
			"WHERE a.corpID = '"+corpID+"' AND a.actualExpense <> 0 and a.settledDate is null AND a.payingStatus = 'A' and a.status is true  " +
			"AND p.isOwnerOp is true AND m.status = 'Ready to Settle' and a.vendorCode='"+ownerPayTo+"' " +
			"UNION " +
			"SELECT s.id, s.personId, s.personName, 0, 0, s.amount, s.description, s.glCode, '' as acDate, date_format(s.approved,'%d-%b-%Y'), " +
			"'' as ppDate, date_format(s.post,'%d-%b-%Y'), '' paDate, s.branch, s.parentId , '' as regNum, if(s.balanceForward=1,'true','false'), if(s.deductTempFlag=1,'true','false') ,pp.truckID as tractTrailer ,'' as shipNum " +
			"FROM  subcontractorcharges s " +
			"inner join partneraccountref r on r.partnerCode = s.personId and r.corpID IN ('"+corpID+"')"+
			"inner join partnerprivate pp on pp.partnerCode = s.personId and pp.corpID IN ('"+corpID+"','"+hibernateUtil.getParentCorpID(corpID)+"') "+
			"WHERE s.personType = 'D' AND r.companyDivision = '"+companyDivision+"' "+
			"AND s.corpID = '"+corpID+"' AND s.isSettled is false " +
			"AND s.personId = '"+ownerPayTo+"' and (s.serviceOrder in(SELECT s.shipNumber FROM accountline a " +
			"inner join serviceorder s on s.id = a.serviceorderid inner join miscellaneous m on m.id = a.serviceorderid " +
			"WHERE a.corpID = '"+corpID+"' AND a.actualExpense <> 0 AND a.payingStatus = 'A' and a.status is true " +
			"AND m.status = 'Ready to Settle' and a.vendorCode='"+ownerPayTo+"' ) or  ((s.serviceOrder = ''  or  s.serviceOrder is null) and s.onAccount is true) or s.description like 'Std. Ded.:%') ";
		}
		
		List list = this.getSession().createSQLQuery(queryString).list();
		Iterator it = list.iterator();
		PreviewDTO dto = null;
		while(it.hasNext()){
			Object [] row = (Object[])it.next();
			dto = new PreviewDTO();
			dto.setId(row[0]);
			dto.setCode(row[1]);
			dto.setName(row[2]);
			dto.setDistAmount(row[3]);
			dto.setActualExpense(row[4]);
			dto.setAmount(row[5]);
			dto.setDescription(row[6]);
			dto.setChargeCode(row[7]);
			dto.setRecAccDate(row[8]);
			dto.setApprovedDate(row[9]);
			dto.setApproved(row[10]);
			dto.setPayPostDate(row[11]);
			dto.setPayAccDate(row[12]);
			dto.setBranch(row[13]);
			dto.setParentId(row[14]);
			dto.setRegistrationNo(row[15]);
			if(row[16] == null || row[16].equals("")){
				dto.setBalanceForward("false");
			}else{
				dto.setBalanceForward(row[16]);
			}
			if(row[17] == null || row[17].equals("")){
				dto.setTempFlag("false");
			}else{
				dto.setTempFlag(row[17]);
			}
			dto.setTractTrailer(row[18]);
			dto.setShipNum(row[19]);
			previewList.add(dto);
		}
		return previewList;
	}
	
	public  class PreviewDTO {		
		private Object id;
		private Object code;
		private Object name; 
		private Object distAmount;
		private Object actualExpense;
		private Object amount; 
		private Object description;
		private Object chargeCode; 
		private Object recAccDate;
		private Object approvedDate;
		private Object approved;
		private Object payPostDate; 
		private Object payAccDate;
		private Object branch;
		private Object parentId;
		private Object registrationNo;
		private Object balanceForward;
		private Object tempFlag;
		private Object tractTrailer;
		private Object shipNum;
		public Object getActualExpense() {
			return actualExpense;
		}
		public void setActualExpense(Object actualExpense) {
			this.actualExpense = actualExpense;
		}
		public Object getAmount() {
			return amount;
		}
		public void setAmount(Object amount) {
			this.amount = amount;
		}
		public Object getApproved() {
			return approved;
		}
		public void setApproved(Object approved) {
			this.approved = approved;
		}
		public Object getApprovedDate() {
			return approvedDate;
		}
		public void setApprovedDate(Object approvedDate) {
			this.approvedDate = approvedDate;
		}
		public Object getChargeCode() {
			return chargeCode;
		}
		public void setChargeCode(Object chargeCode) {
			this.chargeCode = chargeCode;
		}
		public Object getCode() {
			return code;
		}
		public void setCode(Object code) {
			this.code = code;
		}
		public Object getDescription() {
			return description;
		}
		public void setDescription(Object description) {
			this.description = description;
		}
		public Object getDistAmount() {
			return distAmount;
		}
		public void setDistAmount(Object distAmount) {
			this.distAmount = distAmount;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getName() {
			return name;
		}
		public void setName(Object name) {
			this.name = name;
		}
		public Object getPayAccDate() {
			return payAccDate;
		}
		public void setPayAccDate(Object payAccDate) {
			this.payAccDate = payAccDate;
		}
		public Object getPayPostDate() {
			return payPostDate;
		}
		public void setPayPostDate(Object payPostDate) {
			this.payPostDate = payPostDate;
		}
		public Object getRecAccDate() {
			return recAccDate;
		}
		public void setRecAccDate(Object recAccDate) {
			this.recAccDate = recAccDate;
		}
		public Object getBranch() {
			return branch;
		}
		public void setBranch(Object branch) {
			this.branch = branch;
		}
		public Object getParentId() {
			return parentId;
		}
		public void setParentId(Object parentId) {
			this.parentId = parentId;
		}
		public Object getRegistrationNo() {
			return registrationNo;
		}
		public void setRegistrationNo(Object registrationNo) {
			this.registrationNo = registrationNo;
		}
		public Object getBalanceForward() {
			return balanceForward;
		}
		public void setBalanceForward(Object balanceForward) {
			this.balanceForward = balanceForward;
		}
		public Object getTempFlag() {
			return tempFlag;
		}
		public void setTempFlag(Object tempFlag) {
			this.tempFlag = tempFlag;
		}
		public Object getTractTrailer() {
			return tractTrailer;
		}
		public void setTractTrailer(Object tractTrailer) {
			this.tractTrailer = tractTrailer;
		}
		public Object getShipNum() {
			return shipNum;
		}
		public void setShipNum(Object shipNum) {
			this.shipNum = shipNum;
		}
		
	}

	public void updateSubContChargesForStandardDeduction(String personId,Long id,String userName) {
		Date nowDate = new Date();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(nowDate));
		String dateNOW = nowYYYYMMDD2.toString();
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set post = '"+dateNOW+"'  WHERE personId = '"+personId+"' and id='"+id+"' and post is null and isSettled is false ");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set approved = '"+dateNOW+"'  WHERE personId = '"+personId+"' and id='"+id+"' and approved is null and isSettled is false ");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set isSettled = true WHERE personId = '"+personId+"' and id='"+id+"' AND deductTempFlag is false and balanceForward is true");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set isSettled = true WHERE personId = '"+personId+"' and id='"+id+"' AND balanceForward is false");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set deductTempFlag = false WHERE personId = '"+personId+"' and id='"+id+"' ");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set balanceForward = false WHERE personId = '"+personId+"' and id='"+id+"' AND balanceForward is true and isSettled is true ");
		getHibernateTemplate().bulkUpdate("update SubcontractorCharges set updatedBy = '"+userName+"',updatedOn=now() WHERE personId = '"+personId+"' and id='"+id+"' ");
		
	}

	public void updateStandardDeduction(Long id, BigDecimal dedAmount,BigDecimal balancedAmount, Date lastDeductionDate, String status){
		String dateFrom = "";
		String appendQuery = "";
		if (lastDeductionDate != null){
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(lastDeductionDate));
			dateFrom = nowYYYYMMDD1.toString();
		}
		if(status.equalsIgnoreCase("Closed")){
			appendQuery = ", status = 'Closed'";
		}
		getHibernateTemplate().bulkUpdate("update StandardDeductions set amtDeducted = '"+dedAmount+"' , balanceAmount = '"+balancedAmount+"' , lastDeductionDate = '"+dateFrom+"' "+appendQuery+" WHERE id = "+id+" and status!='Cancel' ");
	}

	public List getTripCount(Date fromDate, String partnerCode, String corpID){
		String queryString = "select count(distinct a.shipnumber) from accountline a  inner join miscellaneous m on m.id = a.serviceorderid where a.vendorcode = '"+partnerCode+"' and a.payingstatus = 'A' and a.status is true  AND a.actualExpense <> 0  and a.corpID = '"+corpID+"' AND m.status = 'Ready To Settle'";
		return this.getSession().createSQLQuery(queryString).list();
	}

	public void updateDomestic(String partnerCode) {
		Date nowDate = new Date();
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder nowYYYYMMDD2 = new StringBuilder(dateformatYYYYMMDD1.format(nowDate));
		String dateNOW = nowYYYYMMDD2.toString();
		getHibernateTemplate().bulkUpdate("update AccountLine set settledDate = '"+dateNOW+"' WHERE serviceOrderId in (select id from Miscellaneous where status = 'Ready To Settle') and vendorCode = '"+partnerCode+"' and settledDate is null " );
		getHibernateTemplate().bulkUpdate("update Miscellaneous set status = 'Ready To Transfer', settledDate = '"+dateNOW+"' WHERE status = 'Ready To Settle' and id in (select serviceOrderId from AccountLine where vendorCode = '"+partnerCode+"')");
	}

	public List<SubcontractorCharges> getSubContChargesForFinalize(String partnerCode) {
		return getHibernateTemplate().find("from SubcontractorCharges where personId = '"+partnerCode+"' AND source = 'Standard Deduction' and deductTempFlag = true");
	}

	public void updateSubContractorCharges(Long id) {
		getHibernateTemplate().update("updatge SubcontractorCharges set description = 'Standard Deduction : Previous Carry Forward Balanace' where id = "+id+"");
	}
	
	public List validatePayTo(String ownerPayTo, String corpId){
		return getHibernateTemplate().find("select partnerCode from PartnerPublic where isOwnerOp is true and partnerCode='"+ownerPayTo+"' and status='Approved'");
		
	}
	
	public void updatePartnerPrivate(String ownerPayTo, String corpId,BigDecimal personalEscrowAmount){
		getHibernateTemplate().bulkUpdate("update PartnerPrivate set personalEscrow='"+personalEscrowAmount+"' where partnerCode='"+ownerPayTo+"' and corpID IN ('"+corpId+"','"+hibernateUtil.getParentCorpID(corpId)+"')  ");
	}
	
	public List partnerPrivateObject(String ownerPayTo, String corpId){
		return getHibernateTemplate().find("select id from PartnerPrivate where partnerCode='"+ownerPayTo+"' and corpID IN ('"+corpId+"','"+hibernateUtil.getParentCorpID(corpId)+"')  ");
	}
	public List<SubcontractorCharges> getSubContChargesForPersonalEscrow(String partnerCode) {
		return getHibernateTemplate().find("from SubcontractorCharges where personId = '"+partnerCode+"' AND issettled is false and personalEscrow is true ");
	} 
	public List getUpdatedRecord(Long id){
		return this.getSession().createSQLQuery("select s.amtDeducted from standarddeductions s where s.id='"+id+"' ").list();
		}
	public List partnerPrivatePersonalEscrow(String ownerPayTo, String corpId){
		return this.getSession().createSQLQuery("select if(personalescrow is null,'0.00',personalescrow) from partnerprivate where partnerCode='"+ownerPayTo+"' and corpID IN ('"+corpId+"','"+hibernateUtil.getParentCorpID(corpId)+"')  ").list();
	}
	
	public List subContractorChargesFinalList(String ownerPayTo, String corpId, String companyDivision){
		return this.getSession().createSQLQuery("SELECT s.id FROM  subcontractorcharges s " +
			"inner join partneraccountref r on r.partnerCode = s.personId and r.corpID IN ('"+corpId+"')"+
			"inner join partnerprivate pp on pp.partnerCode = s.personId and pp.corpID IN ('"+corpId+"','"+hibernateUtil.getParentCorpID(corpId)+"') "+
			"WHERE s.personType = 'D' AND r.companyDivision = '"+companyDivision+"' "+
			"AND s.corpID = '"+corpId+"' AND s.isSettled is false " +
			"AND s.personId = '"+ownerPayTo+"' and (s.serviceOrder in(SELECT s.shipNumber FROM accountline a " +
			"inner join serviceorder s on s.id = a.serviceorderid inner join miscellaneous m on m.id = a.serviceorderid " +
			"WHERE a.corpID = '"+corpId+"' AND a.actualExpense <> 0 AND a.payingStatus = 'A' and a.status is true " +
			"AND m.status = 'Ready to Settle' and a.vendorCode='"+ownerPayTo+"' ) or  ((s.serviceOrder = ''  or  s.serviceOrder is null) and s.onAccount is true) or s.description like 'Std. Ded.:%' ) ").list();
	}
	
	public List chargesObject(String chargeCode, String corpId){
		return getHibernateTemplate().find("select id from Charges where charge='"+chargeCode+"' and corpID='"+corpId+"' ");
	}
}
