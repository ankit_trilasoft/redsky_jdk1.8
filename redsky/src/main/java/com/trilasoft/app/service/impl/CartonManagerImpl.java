/**
 * @Class Name  Carton Manager Impl
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */


package com.trilasoft.app.service.impl;

import java.util.Date;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.CartonDao;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.service.CartonManager;

public class CartonManagerImpl extends GenericManagerImpl<Carton, Long> implements CartonManager { 
	CartonDao cartonDao; 
    
 
    public CartonManagerImpl(CartonDao cartonDao) { 
        super(cartonDao); 
        this.cartonDao = cartonDao; 
    } 
 
    public List<Carton> findByLastName(String lastName) { 
        return cartonDao.findByLastName(lastName); 
    } 
    public List findMaxId() {
		return cartonDao.findMaxId();
	} 
	
    public List checkById(Long id) { 
        return cartonDao.checkById(id); 
    }
    public List findMaximumIdNumber(String shipNum){
    	return cartonDao.findMaximumIdNumber(shipNum);
    }
    public List getGross(String shipNumber){
    	return cartonDao.getGross(shipNumber);
    }
    public List getTare(String shipNumber){
    	return cartonDao.getTare(shipNumber);
    }
    public List getNet(String shipNumber){
    	return cartonDao.getNet(shipNumber);
    }
    public List getVolume(String shipNumber){
    	return cartonDao.getVolume(shipNumber);
    }
    public List getPieces(String shipNumber){
    	return cartonDao.getPieces(shipNumber);
    }
    public int updateMisc(String shipNumber,String actualGrossWeight,String actualNetWeight,String actualTareWeight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
    	return cartonDao.updateMisc(shipNumber,actualGrossWeight,actualNetWeight,actualTareWeight,weightUnit,volumeUnit,updatedBy, sessionCorpID);
    }
    public int updateMiscVolume(String shipNumber,String actualCubicFeet,String corpID,String updatedBy){
    	return cartonDao.updateMiscVolume(shipNumber, actualCubicFeet,corpID,updatedBy);
    }
	public List getContainerNumber(String shipNumber) {
		return cartonDao.getContainerNumber(shipNumber);
	}

	public List getVolumeUnit(String shipNumber) {
		
		return cartonDao.getVolumeUnit(shipNumber);
	}

	public List getWeightUnit(String shipNumber) {
		
		return cartonDao.getWeightUnit(shipNumber);
	}
	public int updateDeleteStatus(Long ids){
		return cartonDao.updateDeleteStatus(ids);
	}
	public List getGrossKilo(String shipNumber)
    {
    	return cartonDao.getGrossKilo(shipNumber);
    }
    public List getTareKilo(String shipNumber){
    	return cartonDao.getTareKilo(shipNumber);
    }
    public List getNetKilo(String shipNumber){
    	return cartonDao.getNetKilo(shipNumber);
    }
    public List getVolumeCbm(String shipNumber){
    	return cartonDao.getVolumeCbm(shipNumber);
    }
    public int updateMiscKilo(String shipNumber,String actualGrossWeightKilo,String actualNetWeightKilo,String actualTareWeightKilo,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID){
    	return cartonDao.updateMiscKilo(shipNumber, actualGrossWeightKilo, actualNetWeightKilo, actualTareWeightKilo, weightUnit, volumeUnit,updatedBy, sessionCorpID);
    }
    public int updateMiscVolumeCbm(String shipNumber,String actualCubicMtr,String corpID,String updatedBy){
    	return cartonDao.updateMiscVolumeCbm(shipNumber, actualCubicMtr,corpID,updatedBy);
    }

	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return cartonDao.goNextSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return cartonDao.goPrevSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum) {
		return cartonDao.goSOChild(sidNum, sessionCorpID, soIdNum);
	}

	public List findCountChild(String shipNm) {
		return cartonDao.findCountChild(shipNm);
	}

	public List findMaximumChild(String shipNm) {
		return cartonDao.findMaximumChild(shipNm);
	}

	public List findMinimumChild(String shipNm) {
		return cartonDao.findMinimumChild(shipNm);
	}

	public List cartonList(Long serviceOrderId) {
		return cartonDao.cartonList(serviceOrderId);
	}

	public Long findRemoteCarton(String idNumber, Long serviceOrderId) {
		return cartonDao.findRemoteCarton(idNumber, serviceOrderId);
	}

	public void deleteNetworkCarton(Long serviceOrderId, String idNumber, String cartonStatus) {
		cartonDao.deleteNetworkCarton(serviceOrderId, idNumber,  cartonStatus);
	}
	public List findCartonBySid(Long sofid, String sessionCorpID){
		return cartonDao.findCartonBySid(sofid, sessionCorpID);
	}

	public List getGrossNetwork(String shipNumber, String corpID) {
		return cartonDao.getGrossNetwork(shipNumber, corpID);
	}

	public List getNetNetwork(String shipNumber, String corpID) {
		return cartonDao.getNetNetwork(shipNumber, corpID);
	}

	public List getVolumeNetwork(String shipNumber, String corpID) {
		return cartonDao.getVolumeNetwork(shipNumber, corpID);
	}

	public List getPiecesNetwork(String shipNumber, String corpID) {
		return cartonDao.getPiecesNetwork(shipNumber, corpID);
	}

	public List getTareNetwork(String shipNumber, String corpID) {
		return cartonDao.getTareNetwork(shipNumber, corpID);
	}
	public List cartonListotherCorpId(Long sid){
		return cartonDao.cartonListotherCorpId(sid);
	}
	public List findCartonTypeValue(String sessionCorpID){
		return cartonDao.findCartonTypeValue(sessionCorpID);
	}
	public String findUnitValuesByCarton(String cartonVal,String sessionCorpID){
		return cartonDao.findUnitValuesByCarton(cartonVal,sessionCorpID);
	}
	public void updateCartonNetWeightDetails(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldGrossWeight,String fieldGrossWeightVal,String fieldGrossWeightKilo,String fieldGrossWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName){
		cartonDao.updateCartonNetWeightDetails(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldGrossWeight,fieldGrossWeightVal,fieldGrossWeightKilo,fieldGrossWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateCartonDetailsTareWt(Long id,String fieldNetWeight,String fieldNetWeightVal,String fieldNetWeightKilo,String fieldNetWeightKiloVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String tableName,String sessionCorpID,String userName){
		cartonDao.updateCartonDetailsTareWt(id,fieldNetWeight,fieldNetWeightVal,fieldNetWeightKilo,fieldNetWeightKiloVal,fieldEmptyConWeight,fieldEmptyConWeightVal,fieldemptyContWeightKilo,fieldemptyContWeightKiloVal,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,tableName,sessionCorpID, userName);
	}
	public void updateCartonDetailsDensity(Long id,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldTypeName,String fieldTypeVal,String tableName,String sessionCorpID,String userName){
		cartonDao.updateCartonDetailsDensity(id,fieldVolume,fieldVolumeVal,fieldVolumeCbm,fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,fieldTypeName,fieldTypeVal,tableName,sessionCorpID, userName);
	}
	public void updateValueByCartonType(Long id,String fieldUnit1,String fieldUnit1Val,String fieldUnit2,String fieldUnit2Val,String fieldUnit3,String fieldUnit3Val,String fieldLength,String fieldLengthVal,String fieldWidth,String fieldWidthVal,String fieldHeight,String fieldHeightVal,String fieldEmptyConWeight,String fieldEmptyConWeightVal,String fieldemptyContWeightKilo,String fieldemptyContWeightKiloVal,String fieldVolume,String fieldVolumeVal,String fieldVolumeCbm,String fieldVolumeCbmVal,String fieldDensity,String fieldDensityVal,String fieldDensityMetric,String fieldDensityMetricVal,String fieldValueCartonVal,String tableName,String sessionCorpID,String userName){
		cartonDao.updateValueByCartonType(id, fieldUnit1, fieldUnit1Val, fieldUnit2, fieldUnit2Val, fieldUnit3, fieldUnit3Val, fieldLength, fieldLengthVal, fieldWidth, fieldWidthVal, fieldHeight, fieldHeightVal, fieldEmptyConWeight, fieldEmptyConWeightVal, fieldemptyContWeightKilo, fieldemptyContWeightKiloVal, fieldVolume, fieldVolumeVal, fieldVolumeCbm, fieldVolumeCbmVal,fieldDensity,fieldDensityVal,fieldDensityMetric,fieldDensityMetricVal,fieldValueCartonVal, tableName, sessionCorpID, userName);
	}

	public void updateStatus(Long id, String cartonStatus) {
		cartonDao.updateStatus(id, cartonStatus);
		
	}
	
}

//End of Class.   
