// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

delete from errorlog;


ALTER TABLE `redsky`.`parametercontrol` ADD COLUMN `description` VARCHAR(255) AFTER    `childParameter`;

ALTER TABLE `redsky`.`parametercontrol` ADD COLUMN `nameonform` VARCHAR(255) AFTER    `description`;
ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `revisionHours` DOUBLE DEFAULT 1 ,  
ADD COLUMN `revisionComment` VARCHAR(200) ;

ALTER TABLE `redsky`.`operationsintelligence` ADD COLUMN `revisedQuantity` INTEGER DEFAULT 0 ;

Insert into refmaster
(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby,bucket2)values
('CWMS', 'OfficeMove', 'Office Move', 50, 'NOTESUBTYPE','en',now(),'CWMS_SETUP',now(),'CWMS_SETUP','Service Order');


ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `compensationYear` INT(4) AFTER `doNotInvoice`;

ALTER TABLE `redsky`.`charges` ADD COLUMN `compensation` BIT(1) AFTER `servicedetail`;

CREATE TABLE `redsky`.`compensationsetup` (
`id` BIGINT(20) NOT NULL AUTO_INCREMENT,
`contract` VARCHAR(100),
`corpid` VARCHAR(15),
`contractid` BIGINT(20) NOT NULL,
`compensationyear` INT(4),
`commissionpercentage` decimal(19,2),
PRIMARY KEY(`id`)
);

ALTER TABLE `redsky`.`compensationsetup`
ADD COLUMN `createdBy` VARCHAR(82) AFTER `commissionpercentage`,
ADD COLUMN `createdOn` DATETIME AFTER `createdBy`,
ADD COLUMN `updatedBy` VARCHAR(82) AFTER `createdOn`,
ADD COLUMN `updatedOn` DATETIME AFTER `updatedBy`;

alter table serviceorder add surveyCoordinator varchar(2);

update partnerprivate p
inner join partnerpublic pp on p.partnercode=pp.partnercode
and p.corpId=pp.corpId and pp.isAccount is true
set p.compensationYear='2014',p.updatedby='Backend_Upd',p.updatedon =now()
where p. corpId='HOLL'

ALTER TABLE `redsky`.`dspdetails` DROP COLUMN `TEN_billingCycleMonthUtility` , DROP COLUMN `TEN_billingCycleUtility` , DROP 
COLUMN `TEN_allowanceCurrencyUtility` , DROP COLUMN `TEN_actualCostUtility` , DROP COLUMN `TEN_vendorRefUtility` , DROP
COLUMN `TEN_vendorNameUtility` , DROP COLUMN `TEN_allowanceAmountUtility` ;

ALTER TABLE `redsky`.`dspdetails` ADD COLUMN `TEN_contributionAmount` DECIMAL(19,2) NULL DEFAULT '0.00'  AFTER `TEN_assigneeContributionAmount` , 
ADD COLUMN `TEN_Utility_Gas_Water` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_contributionAmount` , 
ADD COLUMN `TEN_Utility_Gas_Electricity` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_Gas_Water` , 
ADD COLUMN `TEN_Utility_TV_Internet_Phone` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_Gas_Electricity` , 
ADD COLUMN `TEN_Utility_mobilePhone` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_TV_Internet_Phone` , 
ADD COLUMN `TEN_Utility_furnitureRental` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_mobilePhone` , 
ADD COLUMN `TEN_Utility_cleaningServices` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_furnitureRental` , 
ADD COLUMN `TEN_Utility_parkingPermit` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_cleaningServices` ,
ADD COLUMN `TEN_Utility_communityTax` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_parkingPermit` , 
ADD COLUMN `TEN_Utility_insurance` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_communityTax` , 
ADD COLUMN `TEN_Utility_gardenMaintenance` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_insurance` ;

CREATE TABLE `tenancyutilityservice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `corpId` varchar(10) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `createdBy` varchar(82) DEFAULT NULL,
  `updatedOn` datetime DEFAULT NULL,
  `updatedBy` varchar(82) DEFAULT NULL,
  `parentFieldName` varchar(45) DEFAULT NULL,
  `TEN_utilityVendorName` varchar(80) DEFAULT NULL,
  `TEN_utilityVendorRef` varchar(30) DEFAULT NULL,
  `TEN_utilityAllowanceAmount` decimal(19,2) DEFAULT NULL,
  `TEN_utilityAllowanceCurrency` varchar(3) DEFAULT NULL,
  `TEN_billingCycleUtility` varchar(2) DEFAULT NULL,
  `parentId` bigint(20) DEFAULT NULL,
  `userUpdatedRow` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

ALTER TABLE `redsky`.`claim` ADD COLUMN `responsibleAgent` VARCHAR(255) AFTER `claimantCity`,
 ADD COLUMN `location` VARCHAR(45) AFTER `responsibleAgent`,
 ADD COLUMN `factor` VARCHAR(45) AFTER `location`,
 ADD COLUMN `occurence` VARCHAR(45) AFTER `factor`,
 ADD COLUMN `avoidable` VARCHAR(3) AFTER `occurence`,
 ADD COLUMN `culpable` VARCHAR(3) AFTER `avoidable`,
 ADD COLUMN `remediationClosingDate` DATETIME AFTER `culpable`,
 ADD COLUMN `actiontoPreventRecurrence` VARCHAR(600) AFTER `remediationClosingDate`,
 ADD COLUMN `remediationComment` VARCHAR(600) AFTER `actiontoPreventRecurrence`;
 
 ALTER TABLE `redsky`.`charges` ADD COLUMN `incrementalStep` DECIMAL(3,2) AFTER `compensation`;
 
 ALTER TABLE `redsky`.`charges` MODIFY COLUMN `incrementalStep` DECIMAL(5,2);
 
 ALTER TABLE `redsky`.`scheduleresourceoperation` ADD COLUMN `assignedCrewForTruck` VARCHAR(50) NULL  AFTER `integrationTool` ;

 ALTER TABLE `redsky`.`dspdetails` 
ADD COLUMN `TEN_Gas` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_gardenMaintenance` , 
ADD COLUMN `TEN_Electricity` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Gas` , 
ADD COLUMN `TEN_Miscellaneous` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Electricity` , 
ADD COLUMN `TEN_Utility_Gas` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Miscellaneous` , 
ADD COLUMN `TEN_Utility_Electricity` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_Gas` , 
ADD COLUMN `TEN_Utility_Miscellaneous` BIT(1) NULL DEFAULT b'0'  AFTER `TEN_Utility_Electricity` 

ALTER TABLE `redsky`.`dspdetails` DROP COLUMN `TEN_Utility_Gas_Electricity` , DROP COLUMN `TEN_Gas_Electricity` ;

ALTER TABLE `redsky`.`tenancyutilityservice` DROP COLUMN `TEN_utilityVendorRef` ;

ALTER TABLE `redsky`.`tenancyutilityservice` ADD COLUMN `TEN_utilityVendorCode` VARCHAR(8) NULL  AFTER `userUpdatedRow` , 
ADD COLUMN `TEN_utilityVatDesc` VARCHAR(30) NULL  AFTER `TEN_utilityVendorCode` , ADD COLUMN `TEN_utilityVatPercent` 
VARCHAR(6) NULL  AFTER `TEN_utilityVatDesc` , ADD COLUMN `TEN_utilityBillThrough` DATETIME NULL  AFTER  `TEN_utilityVatPercent` ;
 
 /* Move to below give trigger on prod . 
file path is 
--trigger
Project Explorer=>sql=>accountline_Trigger.sql
Project Explorer=>sql=>operationsintelligence_Trigger.sql



*/

 