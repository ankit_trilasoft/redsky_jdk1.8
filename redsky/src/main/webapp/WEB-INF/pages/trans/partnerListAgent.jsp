<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<%@page import="org.acegisecurity.Authentication"%>
<%@page import="org.acegisecurity.context.SecurityContextHolder"%>
<%@page import="org.appfuse.model.User"%>
<%@page import="org.appfuse.model.Role"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Iterator"%>
<%
Authentication auth = SecurityContextHolder.getContext().getAuthentication();
User user = (User)auth.getPrincipal();
Set<Role> roles  = user.getRoles();
Role role=new Role();
Iterator it = roles.iterator();
String userRole = "";

while(it.hasNext()) {
	role=(Role)it.next();
	userRole = role.getName();
	if(userRole.equalsIgnoreCase("ROLE_AGENT_ADMIN")){
		userRole=role.getName();
		
		break;
	}
	
}
%>
<head>   
    <title><fmt:message key="partnerListAgent.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerListAgent.heading'/>"/>   
    
<script language="javascript" type="text/javascript">
function clear_fields(){
		document.forms['partnerViewListForm'].elements['partner.lastName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.firstName'].value = '';
		document.forms['partnerViewListForm'].elements['partner.partnerCode'].value = '';
		document.forms['partnerViewListForm'].elements['countryCodeSearch'].value = '';
		document.forms['partnerViewListForm'].elements['stateSearch'].value = '';
		document.forms['partnerViewListForm'].elements['countrySearch'].value = '';
		document.forms['partnerViewListForm'].elements['partner.status'].value = '';
}
</script>
<style>
span.pagelinks 
{display:block;
font-size:0.85em;
margin-bottom:2px;
!margin-bottom:0px;
margin-top:-32px;
!margin-top:-52px;
padding:2px 0px;
padding-left: 67%;
!padding-left: 63%;
text-align:left;
width:51%;
!width:48%;
}
</style>
</head>
 
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>   

<s:form id="partnerViewListForm" action="agentListSearch" method="post" >  
<div id="otabs">
		<ul>
			<li><a class="current"><span>Search</span></a></li>
		</ul>
</div><div class="spnblk" style="clear:both;">&nbsp;</div> 

<table class="table" border="0">
	<thead>
		<tr>
			<th><fmt:message key="partner.partnerCode"/></th>
			<th><fmt:message key="partner.firstName"/></th>
			<th>Last Name/Company Name</th>
			<th>Country Code</th>
			<th>Country Name</th>
			<th><fmt:message key="partner.billingState"/></th>
			<th><fmt:message key="partner.status"/></th>
		</tr>
	</thead>	
	<tbody>
		<tr>
			<td><s:textfield name="partner.partnerCode" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="partner.firstName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="partner.lastName" size="20" cssClass="input-text" /></td>
			<td><s:textfield name="countryCodeSearch" size="10" cssClass="input-text"/></td>
			<td><s:textfield name="countrySearch" size="20" cssClass="input-text"/></td>
			<td><s:textfield name="stateSearch" size="5" cssClass="input-text"/></td>
			<td><s:select cssClass="list-menu" name="partner.status" list="%{partnerStatus}" cssStyle="width:100px" headerKey="" headerValue=""/></td>
		</tr>
		<tr>
			<td colspan="6"></td>
			<td width="130px" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
</s:form>

<div id="newmnav">   
 	<ul>
 		<li id="newmnav1" style="background:#FFF"><a class="current"><span>Agent List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div><div class="spnblk" style="!margin-bottom:10px; margin-bottom:5px;">&nbsp;</div><br>
<s:set name="partnerAgentList" value="partnerAgentList" scope="request"/>  
<display:table name="partnerAgentList" class="table" requestURI="" id="partnerList" export="true" defaultsort="1" pagesize="10">   
	<%if(userRole.equalsIgnoreCase("ROLE_AGENT_ADMIN")){ %>
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" url="/editPartnerPublic.html?partnerType=AG" paramId="id" paramProperty="id" />   		  
	<%}else{ %>	
		<display:column property="partnerCode" sortable="true" titleKey="partner.partnerCode" url="/findPartnerProfileList.html?partnerType=AG" paramId="id" paramProperty="id"/>   		  
	<%}%>	
		<display:column titleKey="partner.name" sortable="true" style="width:390px"><c:out value="${partnerList.firstName} ${partnerList.lastName}" /></display:column>
		<display:column property="terminalCountryCode" sortable="true" titleKey="partner.billingCountryCode" style="width:65px"/>
		<display:column property="terminalState" sortable="true" titleKey="partner.billingState" style="width:65px"/>
		<display:column property="terminalCity" sortable="true" titleKey="partner.billingCity" style="width:150px"/>	
    
   	 	<display:column property="status" sortable="true" titleKey="partner.status" style="width:140px"/>
 	
	    <display:setProperty name="paging.banner.item_name" value="partner"/>   
	    <display:setProperty name="paging.banner.items_name" value="partners"/>
	       
	  	<display:setProperty name="export.excel.filename" value="Partner List.xls"/>   
	    <display:setProperty name="export.csv.filename" value="Partner List.csv"/>   
	    <display:setProperty name="export.pdf.filename" value="Partner List.pdf"/>   
</display:table>  
<c:set var="isTrue" value="false" scope="session"/>