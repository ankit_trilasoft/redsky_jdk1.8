package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.SQLExtract;

public interface SQLExtractDao extends GenericDao<SQLExtract, Long> {

	public List getAllList(String sessionCorpID);
	public List getAllUserList(String sessionCorpID);
	public String testSQLQuery(String sqlQuery);

	public List sqlDataExtract(String sqlText);

	public List getAllCorpId();

	public List getUniquename(String fileName);

	public List getSearch(String fileName, String corpID);

	public List getSchedulerQueryList();

	public List getSqlExtraxtSearch(String fileName, String description,String sessionCorpID);
	
	public List getAllSqlExtractFiles();
	
	public List getAllListForTSFT();
	
	public List getSqlExtractByID(Long id,String sessionCorpID );
	
	public int deleteSqlExtract(Long id);
}
