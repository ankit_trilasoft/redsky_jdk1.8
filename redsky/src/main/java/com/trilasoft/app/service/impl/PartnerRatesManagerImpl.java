package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PartnerRatesDao;
import com.trilasoft.app.model.PartnerRates;
import com.trilasoft.app.service.PartnerRatesManager;

public class PartnerRatesManagerImpl extends GenericManagerImpl<PartnerRates, Long> implements PartnerRatesManager{
	PartnerRatesDao partnerRatesDao;   
	  
    public PartnerRatesManagerImpl(PartnerRatesDao partnerRatesDao) {   
        super(partnerRatesDao);   
        this.partnerRatesDao = partnerRatesDao;   
    }   
    public List findMaximum() {
    	return partnerRatesDao.findMaximum();
    }
  
}
