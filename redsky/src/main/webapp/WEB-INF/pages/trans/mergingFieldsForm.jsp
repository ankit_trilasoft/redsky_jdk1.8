<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head> 
    <title>Merge <c:out value="${incomingSO.shipNumber }" /> To <c:out value="${yourSO.shipNumber }" /></title>   
    <meta name="heading" content="merge"/>   
        <style type="text/css">
	 #overlayForLinkSo {
	filter:alpha(opacity=70);
	-moz-opacity:0.7;
	-khtml-opacity: 0.7;
	opacity: 0.7;
	position:fixed; width:100%; height:100%;left:0px;top:0px; z-index:999; margin:0px auto -1px auto;
	background:url(images/over-load.png);
	}
    </style>
 <script language="javascript">
 
 function selectAllFields(obj,val,modalName,targetId){
	 if(document.getElementById(targetId).checked){
		 document.getElementById(obj.id).checked = false;
		 return;
	 }
	if(obj.checked){
		<c:forEach var="filedsMap" items="${resultMap}">
			<c:forEach var="filedsList" items="${ filedsMap.value}">
				if(modalName=='${ filedsMap.key}'){
					<c:set var="dataParts" value="${fn:split(filedsList, '~')}" />
					var id= val+"_${ filedsMap.key}_${dataParts[1]}";
					document.getElementById(id).checked = true ;
				}
			</c:forEach>
		</c:forEach>
	}else{
		<c:forEach var="filedsMap" items="${resultMap}">
			<c:forEach var="filedsList" items="${ filedsMap.value}">
				if(modalName=='${ filedsMap.key}'){
					<c:set var="dataParts" value="${fn:split(filedsList, '~')}" />
					var id= val+"_${ filedsMap.key}_${dataParts[1]}";
					document.getElementById(id).checked = false ;
				}
			</c:forEach>
		</c:forEach>
	}
 }
 
 function checkSelectedValue(targetId,id,selectAllId){
	 if(document.getElementById(targetId).checked){
		document.getElementById(id).checked = false;
	 }
	 if(document.getElementById(id).checked){
	 	if(document.getElementById(id).name == "inComing_CustomerFile_bookingAgentCode"){
			document.getElementById("inComing_CustomerFile_bookingAgentName").checked= true;
			document.getElementById("your_CustomerFile_bookingAgentName").checked = false;
		}else if(document.getElementById(id).name == "inComing_CustomerFile_bookingAgentName"){
			document.getElementById("inComing_CustomerFile_bookingAgentCode").checked= true;
			document.getElementById("your_CustomerFile_bookingAgentCode").checked = false;
		}else if(document.getElementById(id).name == "your_CustomerFile_bookingAgentCode"){
			document.getElementById("inComing_CustomerFile_bookingAgentName").checked= false;
			document.getElementById("your_CustomerFile_bookingAgentName").checked = true;
		}else if(document.getElementById(id).name == "your_CustomerFile_bookingAgentName"){
			document.getElementById("inComing_CustomerFile_bookingAgentCode").checked= false;
			document.getElementById("your_CustomerFile_bookingAgentCode").checked = true;
		}else if(document.getElementById(id).name == "inComing_ServiceOrder_bookingAgentName"){
			document.getElementById("inComing_ServiceOrder_bookingAgentCode").checked= true;
			document.getElementById("your_ServiceOrder_bookingAgentCode").checked = false;
		}else if(document.getElementById(id).name == "inComing_ServiceOrder_bookingAgentCode"){
			document.getElementById("inComing_ServiceOrder_bookingAgentName").checked= true;
			document.getElementById("your_ServiceOrder_bookingAgentName").checked = false;
		}else if(document.getElementById(id).name == "your_ServiceOrder_bookingAgentName"){
			document.getElementById("inComing_ServiceOrder_bookingAgentCode").checked= false;
			document.getElementById("your_ServiceOrder_bookingAgentCode").checked = true;
		}else if(document.getElementById(id).name == "your_ServiceOrder_bookingAgentCode"){
			document.getElementById("inComing_ServiceOrder_bookingAgentName").checked= false;
			document.getElementById("your_ServiceOrder_bookingAgentName").checked = true;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_destinationAgentCode"){
			document.getElementById("inComing_TrackingStatus_destinationAgent").checked= true;
			document.getElementById("your_TrackingStatus_destinationAgent").checked = false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_destinationAgent"){
			document.getElementById("inComing_TrackingStatus_destinationAgentCode").checked= true;
			document.getElementById("your_TrackingStatus_destinationAgentCode").checked = false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_destinationAgent"){
			document.getElementById("your_TrackingStatus_destinationAgentCode").checked= true;
			document.getElementById("inComing_TrackingStatus_destinationAgentCode").checked = false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_destinationAgentCode"){
			document.getElementById("your_TrackingStatus_destinationAgent").checked= true;
			document.getElementById("inComing_TrackingStatus_destinationAgent").checked = false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_destinationAgentCode"){
			document.getElementById("your_TrackingStatus_destinationAgent").checked= true;
			document.getElementById("inComing_TrackingStatus_destinationAgent").checked = false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_originAgentCode"){
			document.getElementById("your_TrackingStatus_originAgent").checked= true;
			document.getElementById("inComing_TrackingStatus_originAgent").checked = false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_originAgent"){
			document.getElementById("your_TrackingStatus_originAgentCode").checked= true;
			document.getElementById("inComing_TrackingStatus_originAgentCode").checked = false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_originAgentCode"){
			document.getElementById("your_TrackingStatus_originAgent").checked= false;
			document.getElementById("inComing_TrackingStatus_originAgent").checked = true;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_originAgent"){
			document.getElementById("your_TrackingStatus_originAgentCode").checked= false;
			document.getElementById("inComing_TrackingStatus_originAgentCode").checked = true;
		}
 	}else{
 		if(document.getElementById(id).name == "inComing_CustomerFile_bookingAgentCode"){
			document.getElementById("inComing_CustomerFile_bookingAgentName").checked= false;
		}else if(document.getElementById(id).name == "inComing_CustomerFile_bookingAgentName"){
			document.getElementById("inComing_CustomerFile_bookingAgentCode").checked = false;
		}else if(document.getElementById(id).name == "your_CustomerFile_bookingAgentCode"){
			document.getElementById("your_CustomerFile_bookingAgentName").checked = false;
		}else if(document.getElementById(id).name == "your_CustomerFile_bookingAgentName"){
			document.getElementById("your_CustomerFile_bookingAgentCode").checked = false;
		}else if(document.getElementById(id).name == "inComing_ServiceOrder_bookingAgentName"){
			document.getElementById("inComing_ServiceOrder_bookingAgentCode").checked= false;
		}else if(document.getElementById(id).name == "inComing_ServiceOrder_bookingAgentCode"){
			document.getElementById("inComing_ServiceOrder_bookingAgentName").checked= false;
		}else if(document.getElementById(id).name == "your_ServiceOrder_bookingAgentName"){
			document.getElementById("your_ServiceOrder_bookingAgentCode").checked = false;
		}else if(document.getElementById(id).name == "your_ServiceOrder_bookingAgentCode"){
			document.getElementById("your_ServiceOrder_bookingAgentName").checked = false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_destinationAgentCode"){
			document.getElementById("inComing_TrackingStatus_destinationAgent").checked= false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_destinationAgent"){
			document.getElementById("inComing_TrackingStatus_destinationAgentCode").checked= false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_destinationAgent"){
			document.getElementById("your_TrackingStatus_destinationAgentCode").checked= false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_destinationAgentCode"){
			document.getElementById("your_TrackingStatus_destinationAgent").checked= false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_originAgentCode"){
			document.getElementById("your_TrackingStatus_originAgent").checked= false;
		}else if(document.getElementById(id).name == "your_TrackingStatus_originAgent"){
			document.getElementById("your_TrackingStatus_originAgentCode").checked= false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_originAgentCode"){
			document.getElementById("inComing_TrackingStatus_originAgent").checked= false;
		}else if(document.getElementById(id).name == "inComing_TrackingStatus_originAgent"){
			document.getElementById("inComing_TrackingStatus_originAgentCode").checked= false;
		}
 	}
	/*  if(!document.getElementById(id).checked && document.getElementById(selectAllId).checked ){
		 document.getElementById(selectAllId).checked = false;
	 } */
 }
 function alertFillCheck(){
	 alert("Please Select Value For All Fields");
	 document.getElementById("overlayForLinkSo").style.display = "none";
 }
 
 function chkForwardingVal(id,controlName){
	 if(document.getElementById(id).checked){
			var tempVal = document.forms['linkingFieldsForms'].elements[controlName].value  ;
			if(tempVal==''){
				document.forms['linkingFieldsForms'].elements[controlName].value = id;
			}else{
				document.forms['linkingFieldsForms'].elements[controlName].value = tempVal + ","+id;	
			}
		}/* else{
			alertFillCheck();
			return false;
		} */
	 return true;
 }
 function mergeFields(){
	 document.getElementById("overlayForLinkSo").style.display = "block";
	 document.forms['linkingFieldsForms'].elements['fieldValues'].value = '';
	 document.forms['linkingFieldsForms'].elements['contIDs'].value='';
	 document.forms['linkingFieldsForms'].elements['cartonIDs'].value ='';
	 document.forms['linkingFieldsForms'].elements['vehicleIDs'].value='';
	 document.forms['linkingFieldsForms'].elements['servicePartnerIDs'].value='';
	 <c:forEach var="filedsMap" items="${resultMap}">
		<c:forEach var="filedsList" items="${ filedsMap.value}">
			<c:set var="dataParts" value="${fn:split(filedsList, '~')}" />
			var id= "inComing_${ filedsMap.key}_${dataParts[1]}";
			if(document.getElementById(id).checked){
				var tempVal = document.forms['linkingFieldsForms'].elements['fieldValues'].value  ;
				if(tempVal==''){
					document.forms['linkingFieldsForms'].elements['fieldValues'].value = "${ filedsMap.key}.${dataParts[1]} = ${dataParts[0]}";
				}else{
					document.forms['linkingFieldsForms'].elements['fieldValues'].value = tempVal + "~${ filedsMap.key}.${dataParts[1]} = ${dataParts[0]}";	
				}
			}else{
				var id= "your_${ filedsMap.key}_${dataParts[1]}";
				if(!document.getElementById(id).checked){
					alertFillCheck();
					return false;
				}
			}
		</c:forEach>
	</c:forEach>
	
	var chkFlag;
	var i = 0;
	<c:forEach var="container" items="${ contList}">
		if(i==0){
			chkFlag = chkForwardingVal('${container.id}','contIDs');
		 	if(!chkFlag)
				i=1;
		}
	</c:forEach>
	
	if(i==1){
		return false;
	}
	
	if(chkFlag){
		i = 0;
		<c:forEach var="carton" items="${ pieceCountList}">
			if(i==0){
				chkFlag = chkForwardingVal('${carton.id}','cartonIDs');
				if(!chkFlag)
					i=1;
			}
		</c:forEach>
 	}
	
	if(i==1){
		return false;
	}
	
	if(chkFlag){
	i = 0;
	<c:forEach var="vehicle" items="${ vehicleList}">
		if(i==0){
			chkFlag = chkForwardingVal('${vehicle.id}','vehicleIDs');
			 if(!chkFlag)
				 i=1;
		}
	</c:forEach>
	
	}
	
	if(i==1){
		return false;
	}
	if(chkFlag){
		i = 0;
	<c:forEach var="servicePartner" items="${ servPartnerList}">
	if(i==0){
		if(chkFlag){
			chkFlag = chkForwardingVal('${servicePartner.id}','servicePartnerIDs');
			 if(!chkFlag)
				i=1; 
		}
		/*  var id = '${servicePartner.id}';
		if(document.getElementById(id).checked){
				var controlName =   "new_"+id;
				var temp =document.forms['linkingFieldsForms'].elements['newIdNum'].value  ;
				var newId = document.forms['linkingFieldsForms'].elements[controlName].value;
				if(newId!=''){
					if(temp==''){
						document.forms['linkingFieldsForms'].elements['newIdNum'].value = "${servicePartner.id}="+newId ;
					}else{
						document.forms['linkingFieldsForms'].elements['newIdNum'].value = temp + ",${servicePartner.id}="+newId ;	
					}
				}else{
					alert("Please Fill New# Of Selected Routing.");
					return false;
				}
			} */
		} 
	</c:forEach>
	}	
	
	if(i==1){
		return false;
	}
	document.forms['linkingFieldsForms'].elements['closeWindowFlag'].value='1';
	document.forms['linkingFieldsForms'].action="mergeIncomingOrder.html";
	document.forms['linkingFieldsForms'].submit();   
	
	
 }
 
  function closeMyWindow(){
	    var msg = '${successMessage}';
		if(msg!=''){
			alert(msg);
		}
		var count='${hitFlag}';
		if(count=="Y"){
			window.opener.location.href = "toDos.html?fromUser=${userFirstName}";
			//parent.window.opener.document.location.reload();
			document.forms['linkingFieldsForms'].elements['closeWindowFlag'].value='1';
			window.close();
		}
	 }
  function checkReadOnly(target,id){
	  var controlName =   "new_"+id;
	  if(target.checked){
		 document.forms['linkingFieldsForms'].elements[controlName].readOnly = false;
		 document.forms['linkingFieldsForms'].elements[controlName].className="input-text";
	  }else{
		  document.forms['linkingFieldsForms'].elements[controlName].readOnly = true; 
		  document.forms['linkingFieldsForms'].elements[controlName].className="input-textUpper"
	  }
  }
  
  function closeWindow(){
		window.opener.location.href = window.opener.location.href;
		 window.opener.closeBoxDetails();
		window.close();
  }
  
  function onlyNumeric(targetElement){   
	var i;
	    var s = targetElement.value;
	    for (i = 0; i < s.length; i++)
	    {   
	        var c = s.charAt(i);
	        if (((c < "0") || (c > "9"))) {
	        alert("Enter valid Number");
	        targetElement.value="";
	        return false;
	        }
	    }
	    return true;
	}
  
  function checkForwarding(obj,model,target){
	  if(document.getElementById(target).checked){
		  obj.checked = false;
		  return;
	  } 
	  if(obj.checked){
		if(model=='Container'){
			  <c:forEach var="container" items="${ contList}">
			  	var name = obj.name + "_${container.id}";
			  	if(document.getElementById('${container.id}').name == name){
			  		document.getElementById('${container.id}').checked = true;
			  	}
			  </c:forEach>
		  }
		 if(model=='Carton'){
			  <c:forEach var="carton" items="${ pieceCountList}">
			  	var name = obj.name + "_${carton.id}";
			  	if(document.getElementById('${carton.id}').name == name){
			  		document.getElementById('${carton.id}').checked = true;
			  	}
			  </c:forEach>
		}
		if(model=='Vehicle'){
			  <c:forEach var="vehicle" items="${ vehicleList}">
			  	var name = obj.name + "_${vehicle.id}";
			  	if(document.getElementById('${vehicle.id}').name == name){
			  		document.getElementById('${vehicle.id}').checked = true;
			  	}
			  </c:forEach>
		}
		if(model=='ServicePartner'){
			  <c:forEach var="servPartner" items="${ servPartnerList}">
			  	var name = obj.name + "_${servPartner.id}";
			  	if(document.getElementById('${servPartner.id}').name == name){
			  		document.getElementById('${servPartner.id}').checked = true;
			  	}
			  </c:forEach>
		}
 	 }else{
 		if(model=='Container'){
			  <c:forEach var="container" items="${ contList}">
			  	var name = obj.name + "_${container.id}";
			  	if(document.getElementById('${container.id}').name == name){
			  		document.getElementById('${container.id}').checked = false;
			  	}
			  </c:forEach>
		} 
 		if(model=='Carton'){
			  <c:forEach var="carton" items="${ pieceCountList}">
			  	var name = obj.name + "_${carton.id}";
			  	if(document.getElementById('${carton.id}').name == name){
			  		document.getElementById('${carton.id}').checked = false;
			  	}
			  </c:forEach>
		}
 		if(model=='Vehicle'){
			  <c:forEach var="vehicle" items="${ vehicleList}">
			  	var name = obj.name + "_${vehicle.id}";
			  	if(document.getElementById('${vehicle.id}').name == name){
			  		document.getElementById('${vehicle.id}').checked = false;
			  	}
			  </c:forEach>
		}
 		if(model=='ServicePartner'){
			  <c:forEach var="servPartner" items="${ servPartnerList}">
			  	var name = obj.name + "_${servPartner.id}";
			  	if(document.getElementById('${servPartner.id}').name == name){
			  		document.getElementById('${servPartner.id}').checked = false;
			  	}
			  </c:forEach>
		}
 	 }
	 
 }
 function defaultSelectMethod(){
	 try{
		// Default Check for Container
		 if('${countContainer}'!= 0){
			 document.getElementById('yourContainer').checked = true;
			 checkForwarding(document.getElementById('yourContainer'),'Container','incomingContainer'); 
		 }else{
			 document.getElementById('incomingContainer').checked = true;
			 checkForwarding(document.getElementById('incomingContainer'),'Container','yourContainer'); 
		 }
	 }catch(e){}
		// Default Check for Carton
		try{
			 if('${countCarton}'!= 0){
				 document.getElementById('yourCarton').checked = true;
				 checkForwarding(document.getElementById('yourCarton'),'Carton','incomingCarton'); 
			 }else{
				 document.getElementById('incomingCarton').checked = true;
				 checkForwarding(document.getElementById('incomingCarton'),'Carton','yourCarton');  
			 }
		}catch(e){}
		// Default Check for Vehicle
		try{
			 if('${countVehicle}'!= 0){
				 document.getElementById('yourVehicle').checked = true;
				 checkForwarding(document.getElementById('yourVehicle'),'Vehicle','incomingVehicle');
			 }else{
				 document.getElementById('incomingVehicle').checked = true;
				 checkForwarding(document.getElementById('incomingVehicle'),'Vehicle','yourVehicle');
			 }
		}catch(e){}
		// Default Check for ServicePartner
		try{
		 if('${countServicePartner}'!= 0){
			 document.getElementById('yourServicePartner').checked = true;
			 checkForwarding(document.getElementById('yourServicePartner'),'ServicePartner','incomingServicePartner');
		 }else{
			 document.getElementById('incomingServicePartner').checked = true;
			 checkForwarding(document.getElementById('incomingServicePartner'),'ServicePartner','yourServicePartner');
		 }
	 	}catch(e){}
	 
 }
 window.onbeforeunload = function (evt) {
		if(document.forms['linkingFieldsForms'].elements['closeWindowFlag'].value!='1'){
		  var message = 'Are you sure you want to leave?';
		  if (typeof evt == 'undefined') {
			 
		    evt = window.event;
		  }
		  if (evt) {
			  
			  window.opener.closeBoxDetails();
		  }
		  
		  //return message;
		}
	}
 </script>
 </head>
<form name="linkingFieldsForms" id="linkingFieldsForms" method="post">
	<s:hidden name="fieldValues" />
	<s:hidden name="contIDs" />
	<s:hidden name="cartonIDs" />
	<s:hidden name="vehicleIDs" />
	<s:hidden name="servicePartnerIDs" />
	<s:hidden name="newIdNum" />
	<s:hidden name="closeWindowFlag" value=""></s:hidden>
	<s:hidden name="soUGWWID" value="<%=request.getParameter("soUGWWID") %>" />
	<c:set var="soUGWWID" value="<%=request.getParameter("soUGWWID") %>" />
	<s:hidden name="mergingSOID" value="<%=request.getParameter("mergingSOID") %>" />
	<c:set var="mergingSOID" value="<%=request.getParameter("mergingSOID") %>" />
	<s:hidden name="agentType" value="<%=request.getParameter("agentType") %>" />
	<c:set var="agentType" value="<%=request.getParameter("agentType") %>" />
	<s:hidden name="childAgentType" value="<%=request.getParameter("childAgentType") %>" />
	<c:set var="childAgentType" value="<%=request.getParameter("childAgentType") %>" />
	<s:hidden name="childAgentCode" value="<%=request.getParameter("childAgentCode") %>" />
	<c:set var="childAgentCode" value="<%=request.getParameter("childAgentCode") %>" />
	<c:set var="hitFlag" value="${hitFlag}" />
	<div class="bgblue"><p style="padding:7px 0px;">Select correct value for each field</p></div>
	<c:set var="disable" value="false" />
	<c:forEach var="filedsMap" items="${resultMap}">
		<c:if test="${ filedsMap.value !=null &&  filedsMap.value!='' && filedsMap.value!='[]'}">
			<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
				<tr>
					<td class="headtab_left" style="cursor: default;"></td>
					<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;${ filedsMap.key}</td>
					<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
					<td class="headtab_bg_center" style="cursor: default;width:40%;">&nbsp;</td>
					<td class="headtab_right" style="cursor: default;"></td>
				</tr>
			</table>
			<table class="table">
				<thead>
				<tr>
					<c:if test="${filedsMap.key== 'Miscellaneous'}" ><c:set var="disable" value="true" /></c:if>
					<th><s:checkbox name = "inComingFields_${filedsMap.key}" id="inComingFields_${filedsMap.key}" onclick="selectAllFields(this,'inComing','${ filedsMap.key}','yourFields_${ filedsMap.key}')" />&nbsp;Select&nbsp;All  </th>
					<th>Incoming Order</th>
					<th>Field Name</th>
					<th>Your Order</th>
					<th><s:checkbox name = "yourFields_${filedsMap.key}" id="yourFields_${filedsMap.key}" onclick="selectAllFields(this,'your','${ filedsMap.key}','inComingFields_${ filedsMap.key}')"/>&nbsp;Select&nbsp;All</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="filedsList" items="${ filedsMap.value}">
						<tr class="">
							<c:set var="dataParts" value="${fn:split(filedsList, '~')}" />
							<td><s:checkbox name = "inComing_${ filedsMap.key}_${dataParts[1]}" id="inComing_${ filedsMap.key}_${dataParts[1]}" onclick="checkSelectedValue('your_${ filedsMap.key}_${dataParts[1]}',this.id,'inComingFields_${filedsMap.key}');" disabled="${disable}"/></td>
							<td>${dataParts[0]}</td>
							<td>
								<%-- <fmt:message key='${fn:toLowerCase(fn:substring(filedsMap.key, 0, 1))}${fn:substring(filedsMap.key, 1,fn:length(filedsMap.key))}.${dataParts[1]}'/>  --%>
								<fmt:message key='${filedsMap.key }.${dataParts[1]}'/>
							</td>
							<td>${dataParts[2]}</td>
							<td><s:checkbox name = "your_${ filedsMap.key}_${dataParts[1]}" id="your_${ filedsMap.key}_${dataParts[1]}" onclick="checkSelectedValue('inComing_${ filedsMap.key}_${dataParts[1]}',this.id,'yourFields_${filedsMap.key}');" disabled="${disable}" /></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</c:forEach>
	
	
	<c:if test="${ contList !=null &&  contList!='' && contList!='[]'}">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
			<td class="headtab_left" style="cursor: default;"></td>
			<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;Container</td>
			<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
			<td class="headtab_bg_center" style="cursor: default;width:40%;">&nbsp;</td>
			<td class="headtab_right" style="cursor: default;"></td>
		</tr>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>Incoming Order<s:checkbox name = "incomingContainer" id="incomingContainer" onclick="checkForwarding(this,'Container','yourContainer')" /></th>
				<th>ID#</th>
				<th>SS Container#</th>
				<th>Seal#</th>
				<th>Size</th>
				<th>Gross Weight (${wUnit })</th>
				<th>Tare (${wUnit })</th>
				<th>Net Weight (${wUnit })</th>
				<th>Volume (${vUnit })</th>
				<th>
					Density
							<c:if test = "${vUnit=='Cbm'}" >
								(Metric Units)
							</c:if>
							<c:if test = "${vUnit=='Cft'}" >
								(Pounds & Cft)
							</c:if>
				</th>
				<th>Pieces</th>
				<th>Your Order <s:checkbox name = "yourContainer" id="yourContainer"  onclick="checkForwarding(this,'Container','incomingContainer')" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="container" items="${ contList}">
				<tr>
					<td>
						<c:if test ="${container.corpID != orgId }">
							<s:checkbox name = "incomingContainer_${container.id}" id="${container.id}"  disabled="true"/>
						</c:if>
					</td>
					<td>${container.idNumber}</td>
					<td>${container.containerNumber }</td>
					<td>${container.sealNumber }</td>
					<td>${container.size }</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${container.grossWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${container.grossWeight }
						</c:if>
					</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${container.emptyContWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${container.emptyContWeight }
						</c:if>
					</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${container.netWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${container.netWeight }
						</c:if>
					</td>
					<td>
						<c:if test ="${vUnit ne null && vUnit=='Cbm' }">
							${container.volumeCbm }
						</c:if>
						<c:if test ="${vUnit ne null && vUnit=='Cft' }">
							${container.volume }
						</c:if>
					</td>
					<td>
						<c:if test ="${vUnit ne null && vUnit=='Cbm' }">
							${container.densityMetric }
						</c:if>
						<c:if test ="${vUnit ne null && vUnit=='Cft' }">
							${container.density }
						</c:if>
					</td>
					<td>${container.pieces }</td>
					<td>
						<c:if test ="${container.corpID == orgId }"> 
							<s:checkbox name = "yourContainer_${container.id}" id="${container.id}"  disabled="true"  />
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
	<c:if test="${ pieceCountList !=null &&  pieceCountList!='' && pieceCountList!='[]'}">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
			<td class="headtab_left" style="cursor: default;"></td>
			<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;Piece&nbsp;Count</td>
			<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
			<td class="headtab_bg_center" style="cursor: default;width:40%;">&nbsp;</td>
			<td class="headtab_right" style="cursor: default;"></td>
		</tr>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>Incoming Order<s:checkbox name = "incomingCarton" id="incomingCarton" onclick="checkForwarding(this,'Carton','yourCarton')"/></th>
				<th>ID#</th>
				<th>Type</th>
				<th>Container#</th>
				<th>Pieces</th>
				<th>Gross Weight(${wUnit })</th>
				<th>Tare(${wUnit })</th>
				<th>Net Weight(${wUnit })</th>
				<th>Length(Unit)</th>
				<th>Width(Unit)</th>
				<th>Height(Unit)</th>
				<th>Volume(${vUnit })</th>
				<th>Density
					<c:if test = "${vUnit=='Cbm'}" >
						(Metric Units)
					</c:if>
					<c:if test = "${vUnit=='Cft'}" >
						(Pounds & Cft)
					</c:if>
				</th>
				<th>Your Order <s:checkbox name = "yourCarton" id="yourCarton" onclick="checkForwarding(this,'Carton','incomingCarton')" /></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="carton" items="${ pieceCountList}">
				<tr>
					<td>
						<c:if test ="${carton.corpID != orgId }">
							<s:checkbox name = "incomingCarton_${carton.id}" id="${carton.id}"  disabled="true"/>
						</c:if>
					</td>
					<td>${carton.idNumber }</td>
					<td>${carton.cartonType }</td>
					<td>${carton.cntnrNumber }</td>
					<td>${carton.pieces }</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${carton.grossWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${carton.grossWeight }
						</c:if>
					</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${carton.emptyContWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${carton.emptyContWeight }
						</c:if>
					</td>
					<td>
						<c:if test ="${wUnit ne null && wUnit=='Kgs' }">
							${carton.netWeightKilo }
						</c:if>
						<c:if test ="${wUnit ne null && wUnit=='Lbs' }">
							${carton.netWeight }
						</c:if>
					</td>
					<td>
						<c:if test="${carton.length ne null && carton.length!='' }">
							${carton.length } (${carton.unit3 })
						</c:if>
					</td>
					<td>
						<c:if test="${carton.width ne null && carton.width!='' }">
							${carton.width } (${carton.unit3 })
						</c:if>
					</td>
					<td>
						<c:if test="${carton.height ne null && carton.height!='' }">
							${carton.height } (${carton.unit3 })
						</c:if>
					</td>
					<td>
						<c:if test ="${vUnit ne null && vUnit=='Cbm' }">
							${carton.volumeCbm }
						</c:if>
						<c:if test ="${vUnit ne null && vUnit=='Cft' }">
							${carton.volume }
						</c:if>
					</td>
					<td>
						<c:if test ="${vUnit ne null && vUnit=='Cbm' }">
							${carton.densityMetric }
						</c:if>
						<c:if test ="${vUnit ne null && vUnit=='Cft' }">
							${carton.density }
						</c:if>
					</td>
					<td>
						<c:if test ="${carton.corpID == orgId }">
							<s:checkbox name = "yourCarton_${carton.id}" id="${carton.id}"  disabled="true" />
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
	<c:if test="${ vehicleList !=null &&  vehicleList!='' && vehicleList!='[]'}">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
			<td class="headtab_left" style="cursor: default;"></td>
			<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;Vehicle</td>
			<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
			<td class="headtab_bg_center" style="cursor: default;width:40%;">&nbsp;</td>
			<td class="headtab_right" style="cursor: default;"></td>
		</tr>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>Incoming Order<s:checkbox name = "incomingVehicle" id="incomingVehicle" onclick="checkForwarding(this,'Vehicle','yourVehicle')" /></th>
				<th>ID#</th>
				<th>Vehicle Year</th>
				<th>SS Container#</th>
				<th>Make</th>
				<th>Model</th>
				<th>Volume (Unit)</th>
				<th>Length (Unit)</th>
				<th>Width (Unit)</th>
				<th>Height (Unit)</th>
				<th>Doors</th>
				<th>Cylinders</th>
				<th>Your Order <s:checkbox name = "yourVehicle" id="yourVehicle" onclick="checkForwarding(this,'Vehicle','incomingVehicle')"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="vehicle" items="${ vehicleList}">
				<tr>
					<td>
						<c:if test ="${vehicle.corpID != orgId }">
							<s:checkbox name = "incomingVehicle_${vehicle.id}" id="${vehicle.id}"  disabled="true" />
						</c:if>
					</td>
					<td>${vehicle.idNumber}</td>
					<td>${vehicle.year }</td>
					<td>${vehicle.cntnrNumber }</td>
					<td>${vehicle.make }</td>
					<td>${vehicle.model }</td>
					<td>
						<c:if test="${vehicle.volume ne null && vehicle.volume!='' }">
							${vehicle.volume } (${vehicle.unit2 })
						</c:if>
					</td>
					<td>
						<c:if test="${vehicle.length ne null && vehicle.length!='' }">
							${vehicle.length }  (${vehicle.unit3 })
						</c:if>
					</td>
					<td>
						<c:if test="${vehicle.width ne null && vehicle.width!='' }">
							${vehicle.width }  (${vehicle.unit3 })
						</c:if>
					</td>
					<td>
						<c:if test="${vehicle.height ne null && vehicle.height!='' }">
							${vehicle.height }  (${vehicle.unit3 })
						</c:if>
					</td>
					<td>${vehicle.doors }</td>
					<td>${vehicle.cylinders }</td>
					<td>
						<c:if test ="${vehicle.corpID == orgId }">
							<s:checkbox name = "yourVehicle_${vehicle.id}" id="${vehicle.id}" disabled="true" />
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
	<c:if test="${ servPartnerList !=null &&  servPartnerList!='' && servPartnerList!='[]'}">
	<table cellpadding="0" cellspacing="0" width="100%" style="margin: 0px">
		<tr>
			<td class="headtab_left" style="cursor: default;"></td>
			<td NOWRAP class="headtab_center" style="cursor: default;">&nbsp;Routing</td>
			<td width="28" valign="top" class="headtab_bg" style="cursor: default;"></td>
			<td class="headtab_bg_center" style="cursor: default;width:40%;">&nbsp;</td>
			<td class="headtab_right" style="cursor: default;"></td>
		</tr>
	</table>
	<table class="table">
		<thead>
			<tr>
				<th>Incoming Order <s:checkbox name = "incomingServicePartner" id="incomingServicePartner"  onclick="checkForwarding(this,'ServicePartner','yourServicePartner')"/></th>
				<!-- <th>New#</th> -->
				<th>ID#</th>
				<th>Carrier</th>
				<th>POL</th>
				<th>ETD</th>
				<th>ATD</th>
				<th>POE</th>
				<th>ETA</th>
				<th>ATA</th>
				<th>T-ship</th>
				<th>Container #</th>
				<th>Booking #</th>
				<th>BL #/AWB #</th>
				<th>Your Order <s:checkbox name = "yourServicePartner" id="yourServicePartner"   onclick="checkForwarding(this,'ServicePartner','incomingServicePartner')"/></th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="servicePartner" items="${ servPartnerList}">
				<tr>
					<td>
						<c:if test ="${servicePartner.corpID != orgId }">
							<s:checkbox name = "incomingServicePartner_${servicePartner.id}" id="${servicePartner.id}"  disabled="true" />
						</c:if>
					</td>
					<%--   <td>
						<s:textfield id ="new_${servicePartner.id}"  name="new_${servicePartner.id}" maxlength="2" size="2" readonly="true" cssClass="input-textUpper" onkeyup="return onlyNumeric(this)"/>
					</td> --%>
					<td>${servicePartner.carrierNumber }</td>
					<td>${servicePartner.carrierName }</td>
					<td>${servicePartner.carrierDeparture }</td>
					<td>${servicePartner.etDepart }</td>
					<td>${servicePartner.atDepart }</td>
					<td>${servicePartner.carrierArrival }</td>
					<td>${servicePartner.etArrival }</td>
					<td>${servicePartner.atArrival }</td>
					<td>
						<c:if test="${servicePartner.transhipped=='true' }">
							<img src="${pageContext.request.contextPath}/images/tick01.gif" /> 
    					</c:if>
					</td>
					<td>${servicePartner.cntnrNumber }</td>
					<td>${servicePartner.bookNumber }</td>
					<td>${servicePartner.blNumber }</td>
					<td>
						<c:if test ="${servicePartner.corpID == orgId }">
							<s:checkbox name = "yourServicePartner_${servicePartner.id}" id="${servicePartner.id}"   disabled="true" />
						</c:if>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</c:if>
	<input type="button" name="Merge" value="Merge" id="merge" onclick ="return mergeFields()" class="cssbutton" />
	<input type="button" name="close" value="Close" id="close" onclick ="closeWindow()" class="cssbutton" />
	<div id="overlayForLinkSo" style="display:none">
<div id="layerLoading">
<table cellspacing="0" cellpadding="0" border="0" width="100%" >
<tr>
<td align="center">
<table cellspacing="0" cellpadding="3" align="center">
<tr>
<td height="200px"></td>
</tr>

       <tr>
      <td style="background-color:#DFDFDF;"  class="rounded-bottom" width="280px" align="center" height="35px">
       <img src="<c:url value='/images/animation.png'/>" />       
       </td>
       </tr>
       <tr>
       <td  style="background-color:#DFDFDF; padding-top:3px;"  align="center" class="rounded-top" width="280px">
           <font color="#0F0F0F" face="Arial" size="2px" weight="bold">Processing...Please Wait</font>
       </td>
       </tr>
     </table>
   </td>
  </tr>
</table>  
   </div>   
   </div>
</form>
<script>
try{
	closeMyWindow();
	defaultSelectMethod();
}catch(e){}
</script>