<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="RevenueBillByDestinCountryGraph"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="595"
		 pageHeight="842"
		 columnWidth="595"
		 columnSpacing="0"
		 leftMargin="0"
		 rightMargin="0"
		 topMargin="2"
		 bottomMargin="2"
		 whenNoDataType="AllSectionsNoDetail"
		 isFloatColumnFooter="true"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<parameter name="Corporate ID" isForPrompting="true" class="java.lang.String"/>
	<parameter name="Bill To" isForPrompting="true" class="java.lang.String"/>
	<queryString><![CDATA[select  "RevenueBillByDestinationCountryGraph-SSCW.jrxml",
   if((serviceorder.`destinationCountrycode` is null || trim(serviceorder.`destinationCountrycode`)=''),"Other",serviceorder.`destinationCountrycode`) AS "Destination Country",
   round(sum(accountline.actualRevenue),0) AS "Actual Revenue",
   DATE_FORMAT(concat(year(now())-1,'-01-01'),'%d %M %Y') AS "Start Date",
   DATE_FORMAT(concat(year(now())-1,'-12-31'),'%d %M %Y') AS "End Date",
   accountline.`billtocode` AS "Bill To",
   round(sum(accountline.actualRevenue/1000),0) "ActualRevenue",
if((serviceorder.`destinationCountrycode` is null || trim(serviceorder.`destinationCountrycode`)='' || trim(serviceorder.`destinationCountrycode`) not in ('USA','JPN','NGA','FJI','SYC','GBR','VEN')),"Other",serviceorder.`destinationCountrycode`)  DestinationCountry
FROM accountline accountline, serviceorder serviceorder,billing billing
WHERE accountline.`serviceOrderId` = serviceorder.`id`
AND serviceorder.`id`=billing.`id`
and serviceorder.`status` not in ('CNCL','DWNLD')
and accountline.`billtocode`=$P{Bill To}
and DATE_FORMAT(billing.`revenuerecognition`,'%d %m %Y') between (concat(year(now())-1,'-01-01') and concat(year(now())-1,'-12-31'))
and accountline.`status` is true
and accountline.`actualRevenue` <> 0
and accountline.`receivedInvoiceDate` is not null
and serviceorder.corpId = $P{Corporate ID}
group by destinationCountrycode
order by ActualRevenue desc]]></queryString>

	<field name="RevenueBillByDestinationCountryGraph-SSCW.jrxml" class="java.lang.String"/>
	<field name="Destination Country" class="java.lang.String"/>
	<field name="Actual Revenue" class="java.math.BigDecimal"/>
	<field name="Start Date" class="java.lang.String"/>
	<field name="End Date" class="java.lang.String"/>
	<field name="Bill To" class="java.lang.String"/>
	<field name="ActualRevenue" class="java.math.BigDecimal"/>
	<field name="DestinationCountry" class="java.lang.String"/>

	<sortField name="ActualRevenue" order="Descending" />

	<variable name="Destin" class="java.math.BigDecimal" resetType="Group" resetGroup="Destin" calculation="Sum">
		<variableExpression><![CDATA[$F{ActualRevenue}]]></variableExpression>
	</variable>

		<group  name="Destin" >
			<groupExpression><![CDATA[$F{DestinationCountry}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="68"  isSplitAllowed="true" >
				<staticText>
					<reportElement
						x="101"
						y="22"
						width="392"
						height="25"
						key="staticText-1"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="14" isBold="true" isUnderline="true"/>
					</textElement>
				<text><![CDATA[Revenue Analysis by Destination Country]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="25"
						y="67"
						width="535"
						height="0"
						key="line-2"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
				<staticText>
					<reportElement
						x="152"
						y="48"
						width="101"
						height="19"
						key="staticText-8"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Revenue Period]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="253"
						y="48"
						width="118"
						height="19"
						key="textField-2"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Start Date}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="371"
						y="48"
						width="19"
						height="19"
						key="staticText-9"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[to]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="dd-MMM-yyyy" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="390"
						y="48"
						width="143"
						height="19"
						key="textField-3"/>
					<box></box>
					<textElement>
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{End Date}]]></textFieldExpression>
				</textField>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</pageFooter>
		<summary>
			<band height="327"  isSplitAllowed="true" >
				<pieChart>
					<chart evaluationTime="Report"  hyperlinkTarget="Self" >
					<reportElement
						x="0"
						y="3"
						width="300"
						height="300"
						key="element-1"
						isPrintRepeatedValues="false"/>
					<box></box>
						<chartLegend textColor="#000000" backgroundColor="#FFFFFF" >
					</chartLegend>
					</chart>
					<pieDataset>
						<dataset resetType="None" >
						</dataset>
						<keyExpression><![CDATA[$F{DestinationCountry}]]></keyExpression>
						<valueExpression><![CDATA[$V{Destin}]]></valueExpression>
						<labelExpression><![CDATA[$F{DestinationCountry} + ": " + "$" +$V{Destin}]]></labelExpression>
				<sectionHyperlink >
				</sectionHyperlink>
					</pieDataset>
					<piePlot 
isCircular="true" >
						<plot backcolor="#FFFFFF" />
					</piePlot>
				</pieChart>
				<staticText>
					<reportElement
						x="30"
						y="308"
						width="508"
						height="19"
						forecolor="#FF0033"
						key="staticText-10"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Arial" pdfFontName="Helvetica-Bold" size="8" isBold="true"/>
					</textElement>
				<text><![CDATA[Note: Values are in $000s]]></text>
				</staticText>
				<stackedBar3DChart>
					<chart  hyperlinkTarget="Self" >
					<reportElement
						x="300"
						y="3"
						width="295"
						height="300"
						key="element-2"/>
					<box></box>
						<chartLegend textColor="#000000" backgroundColor="#FFFFFF" >
					</chartLegend>
					</chart>
					<categoryDataset>
						<dataset incrementType="Group" incrementGroup="Destin" >
						</dataset>
						<categorySeries>
							<seriesExpression><![CDATA[$F{DestinationCountry}]]></seriesExpression>
							<categoryExpression><![CDATA[$F{DestinationCountry}]]></categoryExpression>
							<valueExpression><![CDATA[$V{Destin}]]></valueExpression>
							<labelExpression><![CDATA[$F{DestinationCountry} + ": " + "$" +$V{Destin}]]></labelExpression>
				<itemHyperlink >
				</itemHyperlink>
						</categorySeries>
					</categoryDataset>
					<bar3DPlot >
						<plot />
						<categoryAxisFormat>
							<axisFormat >
							</axisFormat>
						</categoryAxisFormat>
						<valueAxisFormat>
							<axisFormat >
							</axisFormat>
						</valueAxisFormat>
					</bar3DPlot>
				</stackedBar3DChart>
			</band>
		</summary>
</jasperReport>
