<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="partnerDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='partnerDetail.heading'/>"/>  
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	fieldset {
			  padding: 0.0em;	
			  position: relative;
			  margin-right:75em
			  width: 90%;  
			  }

	</style>
	<script language="javascript" type="text/javascript">
		function onLoad() {
			var i;
			for(i=0;i<=80;i++)
			{
					document.forms['partnerRemarksForm'].elements[i].disabled = true;
			}
		}
	</script>
	<script language="JavaScript">
	function onlyNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) ; 
	}
	function onlyFloatNumsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= '0'.charCodeAt() && keyCode <= '9'.charCodeAt()) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190); 
	}
	
	function onlyCharsAllowed(evt)
	{
	  var keyCode = evt.which ? evt.which : evt.keyCode;
	  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) ; 
	}
</script>
</head>
<s:form id="partnerRemarksForm" action="savePartner" method="post" validate="true">   
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
</c:if>
<c:set var="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partnerType" value="<%= request.getParameter("partnerType")%>" />
<s:hidden name="partner.id" value="%{partner.id}"/> 
<div id="newmnav">
				<ul>
				  <c:if test="${param.popup}" >
				    <li><a href="searchPartner.html?partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>List</span></a></li>
			  	    <li><a href="editPartner.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Address</span></a></li>
			  		<li><a href="editPartnerDetail.html?id=<%=request.getParameter("id") %>&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}" ><span>Details</span></a></li>
			  		<li id="newmnav1" style="background:#FFF "><a class="current"><span>Remarks</span></a></li>
			  		<c:if test="${partnerType == 'AC'}">
			  			<li><a href="editNewAccountProfile.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Profile</span></a></li>
			  			<li><a href="accountContactList.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Account Contact</span></a></li>
			  			<li><a href="editContractPolicy.html?id=${partner.id}&partnerType=${partnerType}&firstName=&lastName=&decorator=popup&popup=true&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"><span>Policy</span></a></li>
			  		</c:if> 
			  	  </c:if>
			  	</ul>
		</div><div class="spn">&nbsp;</div><br>  
<div id="Layer1">

<table class="mainDetailTable" cellspacing="1" cellpadding="0"
	border="0">
	<tbody>
		<tr>
			<td>
				<table cellspacing="0" cellpadding="3" border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext"></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerPrefix'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.firstName'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.middleInitial'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.lastName'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerSuffix'/></td>
							<td align="left" class="listwhitetext"><fmt:message key='partner.partnerCode'/></td>
						</tr>
						<tr>
							<td width="4" align="left" class="listwhitetext"></td>
							<td align="left" class="listwhitetext"> <s:textfield name="partner.partnerPrefix" size="4" required="true"
							cssClass="text medium" maxlength="10"  onfocus="onLoad();" onkeydown="return onlyCharsAllowed(event)"/></td>
							<td align="left" class="listwhitetext"> <s:textfield name="partner.firstName" required="true"
							cssClass="text medium" maxlength="25" onfocus="checkDate();" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.middleInitial"
							required="true" cssClass="text medium" maxlength="1" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.lastName" required="true"
							cssClass="text medium" maxlength="40" onkeydown="return onlyCharsAllowed(event)"/> </td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerSuffix" size="4" required="true"
							cssClass="text small" maxlength="10" onkeydown="return onlyCharsAllowed(event)"/></td>
							<td align="left" class="listwhitetext"> <s:textfield key="partner.partnerCode"
							required="true" cssClass="text medium" maxlength="8" onkeydown="return onlyAlphaNumericAllowed(event)"/> </td>
						</tr>
					</tbody>
				</table>
				<fieldset>
					<table>
					<tr><td colspan="3">
					<dl>
					<dt><font size="1" color="blue">Remarks/Fees....</font></dt>
					</dl>
					</td></tr>
					<tr><td>Per Diem Costs</td></tr>
					<tr><td /><td align="right" class="listwhite"><fmt:message key='partner.perDiemFreeDays'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemFreeDays"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td></tr>
					<tr><td /><td align="right" class="listwhite"><fmt:message key='partner.perDiemDays2'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemDays2"  size="10" maxlength="11" onkeydown="return onlyNumsAllowed(event)"/></td></tr>
					<tr><td /><td align="right" class="listwhite"><fmt:message key='partner.perDiemCost1'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemCost1"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td></tr>
					<tr><td /><td align="right" class="listwhite"><fmt:message key='partner.perDiemCost2'/></td><td><s:textfield cssClass="input-text" name="partner.perDiemCost2"  size="10" maxlength="7" onkeydown="return onlyFloatNumsAllowed(event)"/></td></tr>
					<tr height="9" />
					<tr><td>Five Lines Of Remarks</td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks1" cssClass="input-text"  size="65" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks2" cssClass="input-text"  size="65" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks3" cssClass="input-text"  size="65" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks4" cssClass="input-text"  size="65" maxlength="50"/></td></tr>
					<tr><td /><td colspan="2"><s:textfield name="partner.remarks5" cssClass="input-text"  size="65" maxlength="50"/></td></tr>
					</table>
				</fieldset>
			</td>
		</tr>
	</tbody>
</table>
</div>
<table style="width:700px">
<tr>
<td align="center">
<table>
<tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdOn'/></td>
						<c:if test="${not empty partner.createdOn}">
							<s:text id="createdOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="partner.createdOn"/></s:text>
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" value="%{createdOnFormattedValue}" /></td>
						</c:if>
						<c:if test="${empty partner.createdOn}">
							<td><s:hidden cssClass="input-text" id="createdOn" name="partner.createdOn" /></td>
						</c:if>
						<td style="width:130px"><fmt:formatDate value="${partner.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.createdBy' /></td>
						<c:if test="${not empty partner.id}">
								<s:hidden name="partner.createdBy"/>
								<td><s:label name="createdBy" value="%{partner.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partner.id}">
								<s:hidden name="partner.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedOn'/></td>
						<s:hidden name="partner.updatedOn"/>
						<td style="width:130px"><fmt:formatDate value="${partner.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><fmt:message key='partner.updatedBy' /></td>
						<s:hidden name="partner.updatedBy" value="${pageContext.request.remoteUser}"  />
						<td style="width:100px"><s:label name="partner.updatedBy" value="${pageContext.request.remoteUser}"/></td>
					</tr>
</tbody>
</table>  



</td>
</tr>
</table>
<s:hidden key="partner.isAccount"/>
<s:hidden key="partner.isAgent"/>
<s:hidden key="partner.isBroker"/>
<s:hidden key="partner.isVendor"/>
<s:hidden key="partner.isCarrier"/>
<s:hidden key="partner.isDriver"/>
<s:hidden key="partner.qc"/>
<s:hidden key="partner.doNotUse"  />
<s:hidden key="partner.lastUpdate"  />
<s:hidden key="partner.updateBy"  />
<s:hidden key="partner.rank" />
<s:hidden key="partner.opened"  />
<s:hidden key="partner.openBy"  />
<s:hidden key="partner.abbreviation"  />
<s:hidden key="partner.warehouse" />
<s:hidden key="partner.zone"  />
<s:hidden key="partner.billToGroup"/>
<s:hidden key="partner.invoicePercentage"  />
<s:hidden key="partner.coordinator" />
<s:hidden key="partner.billPayType"/>
<s:hidden key="partner.billPayOption"/>
<s:hidden key="partner.salesMan"/>
<s:hidden key="partner.scheduleCode" />
<s:hidden key="partner.billingInstructionCode"/>
<s:hidden key="partner.commitionType"/>
<s:hidden key="partner.effectiveDate" />
<s:hidden key="partner.billingInstruction" />
<s:hidden key="partner.driverAgency"  />
<s:hidden key="partner.shortPercentage"  />
<s:hidden key="partner.longPercentage"  />
<s:hidden key="partner.localPercentage"  />
<s:hidden key="partner.client4"/>
<s:hidden key="partner.validNationalCode"/>
<s:hidden key="partner.client5" />
<s:hidden key="partner.idCode"/>
<s:hidden key="partner.client6"/>

<s:hidden key="partner.client7"/>
<s:hidden key="partner.client1"/>
<s:hidden key="partner.client8"/>
<s:hidden key="partner.client2" />
<s:hidden key="partner.client9"/>
<s:hidden key="partner.client3" />
<s:hidden key="partner.client10"/>
<s:hidden name="partner.mailingAddress1"/>
<s:hidden name="partner.mailingCity"/>
<s:hidden name="partner.mailingFax"/>
<s:hidden name="partner.mailingAddress2"/>
<s:hidden name="partner.mailingState" />
<s:hidden name="partner.mailingPhone"/>
<s:hidden name="partner.mailingAddress3"/>
<s:hidden name="partner.mailingZip"/>
<s:hidden name="partner.mailingTelex"/>
<s:hidden name="partner.mailingAddress4"/>
<s:hidden name="partner.mailingCountryCode"/>
<s:hidden name="partner.mailingCountry"/>
<s:hidden name="partner.mailingEmail" />
<s:hidden name="partner.terminalAddress1"/>
<s:hidden name="partner.terminalCity"/>
<s:hidden name="partner.terminalFax"/>
<s:hidden name="partner.terminalAddress2"/>
<s:hidden name="partner.terminalState" />
<s:hidden name="partner.terminalPhone"/>
<s:hidden name="partner.terminalAddress3"/>
<s:hidden name="partner.terminalZip"/>
<s:hidden name="partner.terminalTelex"/>
<s:hidden name="partner.terminalAddress4"/>
<s:hidden name="partner.terminalCountryCode"/>
<s:hidden name="partner.terminalCountry"/>
<s:hidden name="partner.terminalEmail" />
<s:hidden name="partner.billingAddress1"/>
<s:hidden name="partner.billingCity"/>
<s:hidden name="partner.billingFax"/>
<s:hidden name="partner.billingAddress2"/>
<s:hidden name="partner.billingState" />
<s:hidden name="partner.billingPhone"/>
<s:hidden name="partner.billingAddress3"/>
<s:hidden name="partner.billingZip"/>
<s:hidden name="partner.billingTelex"/>
<s:hidden name="partner.billingAddress4"/>
<s:hidden name="partner.billingCountryCode"/>
<s:hidden name="partner.billingCountry"/>
<s:hidden name="partner.billingEmail" />

<c:if test="${ empty param.popup}" >
           
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="save" key="button.save"/>   
        <c:if test="${not empty partner.id}">    
            <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="delete" key="button.delete" onclick="return confirmDelete('partner')"/>   
        </c:if>   
        <s:submit cssClass="cssbutton" cssStyle="width:70px; height:25px " method="cancel" key="button.cancel"/>   
  
</c:if>
</s:form>

<script type="text/javascript">   
try{
<c:if test="${not empty param.popup}" >
	for(i=0;i<=80;i++)
			{
					document.forms['partnerRemarksForm'].elements[i].disabled = true;
			}
</c:if>
}
catch(e){}
	Form.focusFirstElement($("partnerRemarksForm"));   
</script>  
				