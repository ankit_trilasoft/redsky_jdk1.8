package com.trilasoft.app.webapp.action;




import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.FrequentlyAskedQuestions;
import com.trilasoft.app.model.PartnerPublic;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.FrequentlyAskedQuestionsManager;
import com.trilasoft.app.service.PartnerPublicManager;
import com.trilasoft.app.service.RefMasterManager;




public class FrequentlyAskedQuestionsAction extends BaseAction implements Preparable {
	
	private Long id;	
	private FrequentlyAskedQuestions frequentlyAskedQuestions;
	private FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager;
	private String sessionCorpID;
	private List frequentlyAskedQuestionsList;
	private String partnerCode;
	private String partnerType;
	private Long partnerId;
	private String lastName;
	private String status;
    private String usertype; 
    private PartnerPublic partner;
    private PartnerPublicManager partnerPublicManager;
	private RefMasterManager refMasterManager;
    private Map<String, String> language;
	private Company company;
	private CompanyManager companyManager;
	private Boolean checkTransfereeInfopackage;
	
	public void prepare() throws Exception { 
		language = refMasterManager.findByParameter(sessionCorpID, "LANGUAGE");
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		checkTransfereeInfopackage=company.getTransfereeInfopackage();
	}
    public FrequentlyAskedQuestionsAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID();
		usertype=user.getUserType();
	}    
    private String dummQuestion;
    private String dummAnswer;
    private String dummLanguage;
    private Long dummSeqNumber;
   	public String edit() { 
		if (id != null) {
	    	frequentlyAskedQuestions = frequentlyAskedQuestionsManager.getForOtherCorpid(id);
	        dummQuestion=frequentlyAskedQuestions.getQuestion();
	        dummAnswer=frequentlyAskedQuestions.getAnswer();
	        dummLanguage=frequentlyAskedQuestions.getLanguage();
	        dummSeqNumber=frequentlyAskedQuestions.getSequenceNumber();
	        partner = partnerPublicManager.get(partnerId);

		} else {
			frequentlyAskedQuestions = new FrequentlyAskedQuestions(); 
			frequentlyAskedQuestions.setCreatedOn(new Date());
			frequentlyAskedQuestions.setUpdatedOn(new Date());
			partner = partnerPublicManager.get(partnerId);
		}
		return SUCCESS;
	}
  	public String editFrequentlyAsked() { 
		if (id != null) {
	    	frequentlyAskedQuestions = frequentlyAskedQuestionsManager.getForOtherCorpid(id);
	        dummQuestion=frequentlyAskedQuestions.getQuestion();
	        dummAnswer=frequentlyAskedQuestions.getAnswer();
	        dummLanguage=frequentlyAskedQuestions.getLanguage();
	        dummSeqNumber=frequentlyAskedQuestions.getSequenceNumber();
	        //partner = partnerPublicManager.get(partnerId);

		} else {
			frequentlyAskedQuestions = new FrequentlyAskedQuestions(); 
			frequentlyAskedQuestions.setCreatedOn(new Date());
			frequentlyAskedQuestions.setUpdatedOn(new Date());
			//partner = partnerPublicManager.get(partnerId);
		}
		return SUCCESS;
	}
   	private List maxSequenceNumber; 
     private Long autoSequenceNumber;
 
	public String save() throws Exception {
		PartnerPublic partnerPublicFaqAdd = partnerPublicManager.getPartnerByCode(partnerCode);
		boolean isNew = (frequentlyAskedQuestions.getId() == null);  
				if (isNew) { 
					frequentlyAskedQuestions.setCreatedOn(new Date());
					frequentlyAskedQuestions.setCreatedBy(getRequest().getRemoteUser());
		        }
				if(partnerCode==null || partnerCode.equalsIgnoreCase(""))
				{
				partnerCode=frequentlyAskedQuestions.getPartnerCode();
				}
				if(partnerId==null)
				{
				partnerId=frequentlyAskedQuestions.getPartnerId();
				}
				if(partnerPublicFaqAdd.getPartnerType()!=null && (!(partnerPublicFaqAdd.getPartnerType().toString().trim().equals("")))  
						&& partnerPublicFaqAdd.getStatus().equalsIgnoreCase("Approved") 
						&& (partnerPublicFaqAdd.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicFaqAdd.getPartnerType().equalsIgnoreCase("DMM"))){
					sessionCorpID = partnerPublicFaqAdd.getCorpID();
		    	}
				int i=frequentlyAskedQuestionsManager.checkFrequentlyAskedQuestions(frequentlyAskedQuestions.getQuestion(),frequentlyAskedQuestions.getAnswer(),frequentlyAskedQuestions.getLanguage(),partnerCode,sessionCorpID);
                int j=frequentlyAskedQuestionsManager.checkFrequentlySequence(frequentlyAskedQuestions.getLanguage(),partnerCode,frequentlyAskedQuestions.getSequenceNumber(),sessionCorpID);
                if(j>0 && isNew ){
					 errorMessage(" FAQ with the same language and sequence # already exists.");
		    		return INPUT;	
				      }
                  Long seqNumberDumm=frequentlyAskedQuestions.getSequenceNumber();
                   if(j>0 && !isNew && !dummSeqNumber.equals(seqNumberDumm)){
					 errorMessage(" FAQ with the same language and sequence # already exists.");
		    		return INPUT;	
				      }
                   if(i>0 && isNew ){
			        	errorMessage("Question,Answer and  Language already exist For this FAQ.");
			    		return INPUT;
			                       }
			        String questionDumm=frequentlyAskedQuestions.getQuestion();
			        String answerDumm=frequentlyAskedQuestions.getAnswer();
			        String languageDumm=frequentlyAskedQuestions.getLanguage();
			        if((!dummQuestion.equals(questionDumm))||(!dummAnswer.equals(answerDumm))||(!dummLanguage.equals(languageDumm))){
					   if(i>0){
						 errorMessage("Question,Answer and  Language already exist For this FAQ.");
			    		return INPUT;	
					     }
					 }				
				try
				{
					if (isNew) {
						if(frequentlyAskedQuestions.getSequenceNumber()==null)
						{
							if(partnerPublicFaqAdd.getPartnerType()!=null && (!(partnerPublicFaqAdd.getPartnerType().toString().trim().equals("")))  && partnerPublicFaqAdd.getStatus().equalsIgnoreCase("Approved") && (partnerPublicFaqAdd.getPartnerType().equalsIgnoreCase("CMM") || partnerPublicFaqAdd.getPartnerType().equalsIgnoreCase("DMM"))){
								maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(frequentlyAskedQuestions.getLanguage(),partnerCode,partnerPublicFaqAdd.getCorpID());
					    	}else{
					    		maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximum(frequentlyAskedQuestions.getLanguage(),partnerCode,sessionCorpID);
					    	}
							if (maxSequenceNumber.get(0) == null) {
								frequentlyAskedQuestions.setSequenceNumber(1L);
							} else {
								autoSequenceNumber = Long.parseLong(maxSequenceNumber.get(0).toString()) + 1;
								frequentlyAskedQuestions.setSequenceNumber(autoSequenceNumber);
							}
						}
					}
					partner = partnerPublicManager.get(partnerId);
					frequentlyAskedQuestions.setUpdatedOn(new Date());
			    	frequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser()); 
			    	if(partner.getPartnerType()!=null && (!(partner.getPartnerType().toString().trim().equals("")))  && partner.getStatus().equalsIgnoreCase("Approved") && (partner.getPartnerType().equalsIgnoreCase("CMM") || partner.getPartnerType().equalsIgnoreCase("DMM"))){
			    		frequentlyAskedQuestions.setCorpId(partner.getCorpID());
			    	}else{
			    		frequentlyAskedQuestions.setCorpId(sessionCorpID);
			    	}
			    	frequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(frequentlyAskedQuestions);  
			    	String key = (isNew) ? "frequentlyAskedQuestions.added" : "frequentlyAskedQuestions.updated";
				    saveMessage(getText(key));
					return SUCCESS;
				}
				catch(Exception e)
				{
					e.printStackTrace();
					errorMessage("Question,Answer and  Language already exist For this FAQ");
					return INPUT;
				}				
				
				
	     }	
	public String saveAdminQuestions() throws Exception {
		//PartnerPublic partnerPublicFaqAdd = partnerPublicManager.getPartnerByCode(partnerCode);
		boolean isNew = (frequentlyAskedQuestions.getId() == null);  
				if (isNew) { 
					frequentlyAskedQuestions.setCreatedOn(new Date());
					frequentlyAskedQuestions.setCreatedBy(getRequest().getRemoteUser());
		        }
				int i=frequentlyAskedQuestionsManager.checkFrequentlyAskedQuestions(frequentlyAskedQuestions.getQuestion(),frequentlyAskedQuestions.getAnswer(),frequentlyAskedQuestions.getLanguage(),"",sessionCorpID);
                int j=frequentlyAskedQuestionsManager.checkFrequentlySequence(frequentlyAskedQuestions.getLanguage(),"",frequentlyAskedQuestions.getSequenceNumber(),sessionCorpID);
                if(j>0 && isNew ){
					 errorMessage(" FAQ with the same language and sequence # already exists.");
		    		return INPUT;	
				      }
                  Long seqNumberDumm=frequentlyAskedQuestions.getSequenceNumber();
                   if(j>0 && !isNew && !dummSeqNumber.equals(seqNumberDumm)){
					 errorMessage(" FAQ with the same language and sequence # already exists.");
		    		return INPUT;	
				      }
                   if(i>0 && isNew ){
			        	errorMessage("Question,Answer and  Language already exist For this FAQ.");
			    		return INPUT;
			                       }
			        String questionDumm=frequentlyAskedQuestions.getQuestion();
			        String answerDumm=frequentlyAskedQuestions.getAnswer();
			        String languageDumm=frequentlyAskedQuestions.getLanguage();
			        if((!dummQuestion.equals(questionDumm))||(!dummAnswer.equals(answerDumm))||(!dummLanguage.equals(languageDumm))){
					   if(i>0){
						 errorMessage("Question,Answer and  Language already exist For this FAQ.");
			    		return INPUT;	
					     }
					 }					
				try
				{
					if (isNew) {
						if(frequentlyAskedQuestions.getSequenceNumber()==null)
						{
					    maxSequenceNumber = frequentlyAskedQuestionsManager.findMaximumNumber(frequentlyAskedQuestions.getLanguage(),sessionCorpID);
					 
							if (maxSequenceNumber.get(0) == null) {
								frequentlyAskedQuestions.setSequenceNumber(1L);
							} else {
								autoSequenceNumber = Long.parseLong(maxSequenceNumber.get(0).toString()) + 1;
								frequentlyAskedQuestions.setSequenceNumber(autoSequenceNumber);
							}
						}
					}
					//partner = partnerPublicManager.get(partnerId);
					frequentlyAskedQuestions.setUpdatedOn(new Date());
			    	frequentlyAskedQuestions.setUpdatedBy(getRequest().getRemoteUser()); 
			    	
			    		frequentlyAskedQuestions.setCorpId(sessionCorpID);
			    		if(frequentlyAskedQuestions.getPartnerId()==null){			    			
			    			frequentlyAskedQuestions.setPartnerId(0L);
			    		}
			 
			    	frequentlyAskedQuestions=frequentlyAskedQuestionsManager.save(frequentlyAskedQuestions);  
			    	String key = (isNew) ? "frequentlyAskedQuestions.added" : "frequentlyAskedQuestions.updated";
				    saveMessage(getText(key));
					return SUCCESS;
				}
				catch(Exception e)
				{
					errorMessage("Question,Answer and  Language already exist For this FAQ");
					return INPUT;
				}				
				
				
	     }
	private Boolean excludeFPU;
	private String partnerFaqId;
	public String list()
	{ 
		partner = partnerPublicManager.get(partnerId);
		PartnerPublic partnerFaq = partnerPublicManager.getPartnerByCode(partner.getAgentParent());
		//partnerFaqId=partnerFaq.getId();
		if(partnerFaq.getId()!=null){
			partnerFaqId=partnerFaq.getId().toString();
		}
		else{
			partnerFaqId="";
		}
		if(partner.getPartnerType()!=null && (!(partner.getPartnerType().toString().trim().equals("")))  && partner.getStatus().equalsIgnoreCase("Approved") && (partner.getPartnerType().equalsIgnoreCase("CMM") || partner.getPartnerType().equalsIgnoreCase("DMM"))){
			if(usertype.equals("ACCOUNT")){
				frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getFAQByCMMDMMPartnerCode(partner.getPartnerCode(),partner.getCorpID());	
				if(!sessionCorpID.equals(partner.getCorpID()) && (!partnerId.toString().equals(partnerFaqId))){
					  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByCMMDMMPartnerCode(partnerCode,sessionCorpID);
		    		  if(oldFaqList!=null && !oldFaqList.isEmpty()){
			        			Iterator it = oldFaqList.iterator();
			        			while(it.hasNext()){
			        				frequentlyAskedQuestionsList.add(it.next());
			        			}
		    			}
					}
			}
			else{
				frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getFAQByPartnerCode(partner.getPartnerCode(),partner.getCorpID());	  
				if(!sessionCorpID.equals(partner.getCorpID())){
					  List oldFaqList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
		      		  if(oldFaqList!=null && !oldFaqList.isEmpty()){
		 	        			Iterator it = oldFaqList.iterator();
		 	        			while(it.hasNext()){
		 	        				frequentlyAskedQuestionsList.add(it.next());
		 	        			}
		      			}
			}
				
			}
			
			
			
			/**if(!sessionCorpID.equals(partner.getCorpID())){
				  List oldFaqList = frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,partnerFaq.getCorpID());
	      		  if(oldFaqList!=null && !oldFaqList.isEmpty()){
	 	        			Iterator it = oldFaqList.iterator();
	 	        			while(it.hasNext()){
	 	        				frequentlyAskedQuestionsList.add(it.next());
	 	        			}
	      			}
			  }*/
	   	}
	   	else{
	   		
	   		if(usertype.equals("ACCOUNT")){
	   		frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getNewFaqByPartnerCode(partnerCode,sessionCorpID);
	   		if(!sessionCorpID.equals(partnerFaq.getCorpID())){
				  List oldFaqList = frequentlyAskedQuestionsManager.getNewFaqByPartnerCode(partnerCode,partnerFaq.getCorpID());
	      		  if(oldFaqList!=null && !oldFaqList.isEmpty()){
	 	        			Iterator it = oldFaqList.iterator();
	 	        			while(it.hasNext()){
	 	        				frequentlyAskedQuestionsList.add(it.next());
	 	        			}
	      			}
			  }
	   		
	   		}
	   		else{
	   			frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
	   			if(!sessionCorpID.equals(partnerFaq.getCorpID())){
					  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerFaq.getCorpID());
		      		  if(oldFaqList!=null && !oldFaqList.isEmpty()){
		 	        			Iterator it = oldFaqList.iterator();
		 	        			while(it.hasNext()){
		 	        				frequentlyAskedQuestionsList.add(it.next());
		 	        			}
		      			}
				  }
	   		
	   		}
	   		/**if(!sessionCorpID.equals(partnerFaq.getCorpID())){
				  List oldFaqList = frequentlyAskedQuestionsManager.getFAQByPartnerCode(partnerCode,partnerFaq.getCorpID());
	      		  if(!oldFaqList.isEmpty()){
	 	        			Iterator it = oldFaqList.iterator();
	 	        			while(it.hasNext()){
	 	        				frequentlyAskedQuestionsList.add(it.next());
	 	        			}
	      			}
			  }
			  */
	   	}
		//frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getFaqByPartnerCode(partnerCode,sessionCorpID);
		excludeFPU=frequentlyAskedQuestionsManager.getExcludeFPU(partnerCode,sessionCorpID);
		if(excludeFPU == null){
			excludeFPU = false;
		}
		return SUCCESS;
	}
	
	public String frequentlyAskedQuestionsList(){
		frequentlyAskedQuestionsList=frequentlyAskedQuestionsManager.getFAQByAdmin(sessionCorpID);
		return SUCCESS;
	}
	public String delete()
	{		
		frequentlyAskedQuestionsManager.deleteChildDocument(id,sessionCorpID);
		frequentlyAskedQuestionsManager.removeForOtherCorpid(id);    
		partner = partnerPublicManager.get(partnerId);
	      return SUCCESS;		    
	}
	public String deleteAdmin()
	{		
		frequentlyAskedQuestionsManager.remove(id);
	      return SUCCESS;		    
	}

   
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public FrequentlyAskedQuestions getFrequentlyAskedQuestions() {
		return frequentlyAskedQuestions;
	}


	public void setFrequentlyAskedQuestions(
			FrequentlyAskedQuestions frequentlyAskedQuestions) {
		this.frequentlyAskedQuestions = frequentlyAskedQuestions;
	}


	public FrequentlyAskedQuestionsManager getFrequentlyAskedQuestionsManager() {
		return frequentlyAskedQuestionsManager;
	}


	public void setFrequentlyAskedQuestionsManager(
			FrequentlyAskedQuestionsManager frequentlyAskedQuestionsManager) {
		this.frequentlyAskedQuestionsManager = frequentlyAskedQuestionsManager;
	}


	public String getSessionCorpID() {
		return sessionCorpID;
	}


	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}


	public List getFrequentlyAskedQuestionsList() {
		return frequentlyAskedQuestionsList;
	}


	public void setFrequentlyAskedQuestionsList(List frequentlyAskedQuestionsList) {
		this.frequentlyAskedQuestionsList = frequentlyAskedQuestionsList;
	}


	public String getPartnerCode() {
		return partnerCode;
	}


	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}


	public String getPartnerType() {
		return partnerType;
	}


	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}


	public Long getPartnerId() {
		return partnerId;
	}


	public void setPartnerId(Long partnerId) {
		this.partnerId = partnerId;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getUsertype() {
		return usertype;
	}


	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}


	public Map<String, String> getLanguage() {
		return language;
	}


	public void setLanguage(Map<String, String> language) {
		this.language = language;
	}


	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public Boolean getExcludeFPU() {
		return excludeFPU;
	}
	public void setExcludeFPU(Boolean excludeFPU) {
		this.excludeFPU = excludeFPU;
	}
	public String getDummQuestion() {
		return dummQuestion;
	}
	public void setDummQuestion(String dummQuestion) {
		this.dummQuestion = dummQuestion;
	}
	public String getDummAnswer() {
		return dummAnswer;
	}
	public void setDummAnswer(String dummAnswer) {
		this.dummAnswer = dummAnswer;
	}
	public String getDummLanguage() {
		return dummLanguage;
	}
	public void setDummLanguage(String dummLanguage) {
		this.dummLanguage = dummLanguage;
	}
	public List getMaxSequenceNumber() {
		return maxSequenceNumber;
	}
	public void setMaxSequenceNumber(List maxSequenceNumber) {
		this.maxSequenceNumber = maxSequenceNumber;
	}
	public Long getAutoSequenceNumber() {
		return autoSequenceNumber;
	}
	public void setAutoSequenceNumber(Long autoSequenceNumber) {
		this.autoSequenceNumber = autoSequenceNumber;
	}
	public Long getDummSeqNumber() {
		return dummSeqNumber;
	}
	public void setDummSeqNumber(Long dummSeqNumber) {
		this.dummSeqNumber = dummSeqNumber;
	}
	public PartnerPublic getPartner() {
		return partner;
	}
	public void setPartner(PartnerPublic partner) {
		this.partner = partner;
	}
	public PartnerPublicManager getPartnerPublicManager() {
		return partnerPublicManager;
	}
	public void setPartnerPublicManager(PartnerPublicManager partnerPublicManager) {
		this.partnerPublicManager = partnerPublicManager;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public Boolean getCheckTransfereeInfopackage() {
		return checkTransfereeInfopackage;
	}
	public void setCheckTransfereeInfopackage(Boolean checkTransfereeInfopackage) {
		this.checkTransfereeInfopackage = checkTransfereeInfopackage;
	}
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	
	
	}
