<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="notesList.title"/></title>   
    <meta name="heading" content="<fmt:message key='notesList.heading'/>"/>   
<script language="javascript" type="text/javascript">

function clear_fields(){
// document.forms['notesListForm'].elements['notes.notesId'].value = "";
document.forms['notesListForm'].elements['notes.noteSubType'].value = "";
document.forms['notesListForm'].elements['notes.noteStatus'].value = "";
}
</script>
<style>
 span.pagelinks {
display:block;
font-size:0.85em;
margin-bottom:5px;
!margin-bottom:4px;
margin-top:-21px;
!margin-top:-23px;
padding:2px 0px;
text-align:right;
width:100%;
!width:100%;
}
</style>
</head>
<c:set var="noteFrom" value="<%=request.getParameter("noteFrom") %>"/>
<s:hidden name="noteFrom" value="<%=request.getParameter("noteFrom") %>"/>
<s:form id="notesListForm" action="searchReleventNotes.html?noteFrom=${noteFrom}" method="post" >  
<c:set var="customerNumber" value="<%=request.getParameter("customerNumber") %>"/>
<s:hidden name="customerNumber" value="<%=request.getParameter("customerNumber") %>" />
<c:if test="${myFileFor=='CF'}">
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
	<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
	<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
</c:if>
<c:if test="${myFileFor!='CF'}"> 
	<c:if test="${empty serviceOrder.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty serviceOrder.id}">
		<c:set var="isTrue" value="true" scope="request"/>
		<s:hidden name="fileNameFor"  id= "fileNameFor" value="SO"/>
		<s:hidden name="fileID" id ="fileID" value="%{serviceOrder.id}" />
	</c:if>	
</c:if>
<s:hidden name="id" value="<%=request.getParameter("id") %>" />
<s:hidden name="id1" value="<%=request.getParameter("id") %>" />
<s:hidden name="customerFile.id" />	
<s:hidden name="serviceOrder.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="claim.id" />
<s:hidden name="customerFile.sequenceNumber" />	
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="claim.claimNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<c:set var="noteFrom" value="<%=request.getParameter("noteFrom") %>"/>
<c:set var="noteFor1" value="<%=request.getParameter("noteFor") %>" scope="request"/>
<s:hidden name="noteFor1" value="<%=request.getParameter("noteFor") %>" />
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" method="searchRelevent" key="button.search"/>   
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px" onclick="clear_fields();"/> 
</c:set>  
<c:set var="buttons"> 
	<c:choose>
		<c:when test="${not empty serviceOrder.id && not empty workTicket.id}">
			<input type="button" class="cssbutton" style="width:55px; height:25px font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForWorkTicket.html?id=${workTicket.id } "/>'"  
        	value="<fmt:message key="button.add"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && not empty claim.id}">
			<input type="button" class="cssbutton" style="width:55px; height:25px font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForClaim.html?id=${claim.id } "/>'"  
        	value="<fmt:message key="button.add"/>"/> 
		</c:when>
		<c:when test="${not empty serviceOrder.id && empty workTicket.id && empty claim.id}">
			<input type="button" class="cssbutton" style="width:55px; height:25px font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForServiceOrder.html?id=${serviceOrder.id } "/>'"  
        	value="<fmt:message key="button.add"/>"/> 
		</c:when>
		<c:when test="${not empty partnerPrivate.id}">
			<input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForPartnerPrivate.html?id=${partnerPrivate.id } "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> 
		</c:when>
		<c:otherwise>
			<input type="button" class="cssbutton" style="width:55px; height:25px font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNote.html?id=${customerFile.id } "/>'"  
        	value="<fmt:message key="button.add"/>"/> 
		</c:otherwise>
	 </c:choose> 
      </c:set>  
<c:if test="${!param.popup}">
<c:choose>
	<c:when test="${noteFrom=='WorkTicket' }">
		<div id="newmnav">
				  <ul><c:if test="${serviceOrder.job !='RLO'}">
				    	<li id="newmnav1" style="background:#FFF "><a href="editWorkTicketUpdate.html?id=${workTicket.id}" class="current"><span>Ticket<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	</c:if>
						<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
				  </ul>
		</div><div class="spn">&nbsp;</div>
	</c:when>
	<c:when test="${noteFrom=='ServiceOrder' || noteFrom=='Claim' }">
	<div id="newmnav">
		  <ul>
		   <sec-auth:authComponent componentId="module.tab.notes.serviceOrderDetailsTab">
		  <li id="newmnav1" style="background:#FFF "><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}" class="current"><span>S/O Details<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.notes.billingTab">
		  <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
		  <li><a href="editBilling.html?id=${serviceOrder.id}" ><span>Billing</span></a></li>
		  </sec-auth:authComponent>
		  </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.notes.accountingTab">
		  <li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
		  <li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
		  </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.notes.forwardingTab">
		   <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="containers.html?id=${serviceOrder.id}" ><span>Forwarding</span></a></li>
		  </c:if>
		   </sec-auth:authComponent>
		    <sec-auth:authComponent componentId="module.tab.notes.domesticTab">
		  <c:if test="${serviceOrder.job !='INT'}">
		  <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		  </c:if>
		  </c:if>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
               <c:if test="${serviceOrder.job =='INT'}">
                <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
               </c:if>
               </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.notes.statusTab">
		   <c:if test="${serviceOrder.job =='RLO'}">
		  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  </c:if>
		   <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
		  </c:if>
		  </sec-auth:authComponent>
		   <sec-auth:authComponent componentId="module.tab.notes.ticketTab">
		   <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="customerWorkTickets.html?id=${serviceOrder.id}" ><span>Ticket</span></a></li>
		  </c:if>
		 </sec-auth:authComponent>
		 <configByCorp:fieldVisibility componentId="component.standard.claimTab">
		  <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
		  <c:if test="${serviceOrder.job !='RLO'}">
		  <li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
		  </c:if>
		 </sec-auth:authComponent>
		 </configByCorp:fieldVisibility>
		  <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
		  <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		  </sec-auth:authComponent>
		  <sec-auth:authComponent componentId="module.tab.notes.reportTab">
		  <li><a onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=Notes&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
          </sec-auth:authComponent>
          <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
           	 	<li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
          </sec-auth:authComponent>
		</ul>
		</div><div class="spn">&nbsp;</div>
</c:when>
	<c:otherwise>
		<div id="Layer5" style="width:100%">
		<div id="newmnav">
		 <c:if test="${noteFrom=='CustomerFile'}">	  
				  <ul>
			 <sec-auth:authComponent componentId="module.tab.notes.customerFileTab">
				    <li id="newmnav1" style="background:#FFF "><a href="editCustomerFile.html?id=${customerFile.id}" class="current"><span>Customer File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			</sec-auth:authComponent>
			<sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">		    
				    <li><a href="customerServiceOrders.html?id=${customerFile.id} "><span>Service Orders</span></a></li>
			</sec-auth:authComponent>
			 <sec-auth:authComponent componentId="module.tab.notes.rateRequestTab">
				    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.notes.surveysTab">
				    <li><a href="surveysList.html?id1=${customerFile.id} "><span>Surveys</span></a></li>
			</sec-auth:authComponent>
			  <sec-auth:authComponent componentId="module.tab.notes.reportTab">
			  <li><a onclick="window.open('sortCustomerReportss.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&preferredLanguage=${customerFile.customerLanguagePreference}&jobNumber=&reportModule=customerFile&reportSubModule=customerFile&formReportFlag=F&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes');"><span>Forms</span></a></li>
		    </sec-auth:authComponent>
				 </ul>
			</c:if>
				  <c:if test="${noteFrom=='agentQuotes' || noteFrom=='agentQuotesSO'}">
				  <ul>
				  <sec-auth:authComponent componentId="module.tab.notes.quotationFileTab">
				    <li id="newmnav1" style="background:#FFF "><a href="QuotationFileForm.html?id=${customerFile.id}"  class="current"><span>Quotation File<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				   </sec-auth:authComponent>
				    <sec-auth:authComponent componentId="module.tab.notes.serviceOrdersTab">
				    <li><a href="quotationServiceOrders.html?id=${customerFile.id} "><span>Service Orders</span></a></li>
				   </sec-auth:authComponent> 
				  </ul>
		</c:if>
		</div><div class="spn">&nbsp;</div>
	
		 </div>
	</c:otherwise>
</c:choose> 
<c:choose>
	<c:when test="${noteFrom=='WorkTicket' || noteFrom=='ServiceOrder' || noteFrom=='Claim' }">
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width:90%">
			<tbody>
			<tr><td align="left" class="listwhitebox" style="width:80%">
				<table class="detailTabLabel" border="0" style="width:100%">
				  <tbody>  	
				  	<tr><td align="left" height="5px"></td></tr>
				  	<tr><td align="right"><fmt:message key="billing.shipper"/></td>
				  	<td align="left" colspan="1"><s:textfield name="serviceOrder.firstName"  size="17"  readonly="true" cssClass="input-textUpper"  onfocus="checkDate();"/>
				  	<td align="left" colspan="2"><s:textfield name="serviceOrder.lastName" cssClass="input-textUpper"  size="14" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.originCountry"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCityCode"  cssClass="input-textUpper"  size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.originCountryCode" cssClass="input-textUpper"   size="1" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.Type"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.Job" cssClass="input-textUpper"  size="1" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.commodity"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.commodity" cssClass="input-textUpper"   size="3" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.routing"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.routing" cssClass="input-textUpper"  size="1" readonly="true"/></td>
				  	</tr>
				  	<tr><td align="right"><fmt:message key="billing.jobNo"/></td>
				  	<td align="left"><s:textfield cssClass="input-textUpper" name="customerFileNumber" value="${serviceOrder.sequenceNumber}-${serviceOrder.ship}"  size="17" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.registrationNo"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.registrationNumber" cssClass="input-textUpper"   size="7" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.destination"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCityCode" cssClass="input-textUpper"   size="15" readonly="true"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.destinationCountryCode" cssClass="input-textUpper"  size="1" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.mode"/></td>
				  	<td align="left"><s:textfield name="serviceOrder.mode" cssClass="input-textUpper"  size="1" readonly="true"/></td>
				  	<td align="right"><fmt:message key="billing.AccName"/></td>
				  	<td align="left" colspan="3"><s:textfield name="serviceOrder.billToName" cssClass="input-textUpper"  size="30" readonly="true"/></td>
				  	</tr>
				  
		   		  </tbody>
			  </table>
			  </td></tr>
			</tbody>
		 </table> 
		 </div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</c:when>
	<c:otherwise>
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
		<table class="" cellspacing="1" cellpadding="0" border="0" style="width:95%">
			<tbody>
			<tr><td align="left" class="listwhitebox" style="width:100%">
				<table class="detailTabLabel" border="0" style="width:100%">
				  <tbody>  	
				  	<tr>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.firstName" required="true" size="20" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.lastName" required="true" size="20" readonly="true"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.originCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.originCountryCode" required="true" size="2" readonly="true"/></td>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.job" required="true" size="4" readonly="true"/></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.destinationCityCode" required="true" size="25" readonly="true"/></td>
						<td><s:textfield cssClass="input-textUpper" name="customerFile.destinationCountryCode" required="true" size="2" readonly="true"/></td>
					</tr>
				  </tbody>
				 </table>
				</td>
			</tr>
			</tbody>
		</table>
		</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
	</c:otherwise>
</c:choose>
<div id="Layer1" style="width:100%"> 
				<div id="otabs">
				  <ul>
				    <li><a class="current"><span>Search</span></a></li>
				  </ul>
				</div>
				<div class="spnblk">&nbsp;</div>
				<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
   <div class="center-content">
				<table class="table" width="99%" >
				<thead>
				<tr>
				<th><fmt:message key="notes.notesId"/></th>
				<th><fmt:message key="notes.noteSubType"/></th>
				<th><fmt:message key="notes.forwardStatus"/></th>
				<th ></th>
				</tr></thead>	
						<tbody>
						<tr>
							<td>
							    <s:textfield cssClass="input-text"name="notes.notesId" required="true" readonly="true" value="${customerFile.sequenceNumber}"/>
							</td>
							<td>
								<s:select cssClass="list-menu"   name="notes.noteSubType" list="%{notesubtype}" headerKey="" headerValue="" cssStyle="width:150px"/>
							    
							</td>
							<td>
								<s:select cssClass="list-menu"   name="notes.noteStatus" list="%{notestatus}" headerKey="" headerValue="" cssStyle="width:150px"/>
							    
							</td>
							<td width="130px">
								<c:out value="${searchbuttons}" escapeXml="false" />   
							</td>
						</tr>
						</tbody>
					</table>
					</div>
<div class="bottom-header"><span></span></div>
</div>
</div>			
	
	<div id="newmnav">
		<ul>
		<sec-auth:authComponent componentId="module.tab.notes.notesListTab">
			<li><a href="notess.html?id=${idOfWhom }&notesId=${customerNumber}&noteFor=${noteFrom }"><span>List</span></a></li>
				</sec-auth:authComponent>
				<li ><a href="allNotess.html?id=${idOfWhom}&notesId=${customerNumber}&noteFor=${noteFor1}"><span>ALL List</span></a></li>
			
			<li><a href="raleventNotess.html?id=${idOfWhom }&customerNumber=${customerNumber}&noteFor=${noteFor1}&noteFrom=${noteFrom}"><span>Related Notes</span></a></li>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Partner Notes</span></a></li>
		</ul>
	</div>
	<div style="width: 770px"><div class="spn spnSF">&nbsp;</div>
	</div>
</c:if>
<c:if test="${param.popup}"> 
	<c:set var="imageId" value="<%=request.getParameter("imageId") %>" />
	<s:hidden name="imageId" value="<%=request.getParameter("imageId") %>" />
	<c:set var="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<s:hidden name="fieldId" value="<%=request.getParameter("fieldId") %>" />
	<c:set var="subType" value="<%=request.getParameter("subType") %>" />
	<s:hidden name="subType" value="<%=request.getParameter("subType") %>" />
</c:if>
<c:if test="${param.popup}">
	<div id="newmnav">
		<ul>
		<c:if test="${noteFor1!='Partner11' }">
			<li><a href="notess.html?id=${idOfWhom }&notesId=${customerNumber}&noteFor=${noteFrom }&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>List</span></a></li>
			<li><a href="allNotess.html?id=${idOfWhom}&notesId=${customerNumber}&noteFor=${noteFor1}&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>ALL List</span></a></li>
			<li><a href="raleventNotess.html?id=${idOfWhom }&customerNumber=${customerNumber}&noteFor=${noteFor1}&noteFrom=${noteFrom}&subType=${subType}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true"><span>Related Notes</span></a></li>
			</c:if>
			<li id="newmnav1" style="background:#FFF "><a class="current"><span>Partner Notes</span></a></li>
		</ul>
	</div>
</c:if>
<div style="width: 770px"><div class="spn spnSF">&nbsp;</div>
	</div>
		

			
				<s:set name="notess" value="notess" scope="request"/>  
				<c:if test="${!param.popup}">
				<display:table name="notess" class="table" requestURI="" id="notesList" export="false"  pagesize="10" style="width:99%;margin-top:1px;margin-left:5px;!margin-top:10px;" > 
					<c:if test="${notess!='[]' }">
					<display:column property="notesId" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartnerPrivate.html?from=list&id1=${notesList.notesKeyId}&notesId=${customerNumber}&noteFor=${noteFrom}&isRal=true" paramId="id" paramProperty="id"/>
					<display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType"/>
				    <display:column property="subject" sortable="true" titleKey="notes.subject"/>
				    <display:column property="note" sortable="true" title="Notes" style="width: 550px;"/>
				    <display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
						<c:if test="${notesList.noteStatus == 'NEW'}" >
						</c:if>
						<c:if test="${notesList.noteStatus != 'NEW'}" >
							<fmt:timeZone value="${sessionTimeZone}" >
								<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
								</fmt:timeZone>
						</c:if>
					</display:column>
				    <display:setProperty name="paging.banner.item_name" value="customerfilenotes"/>   
				    <display:setProperty name="paging.banner.items_name" value="notes"/>   
				
				    <display:setProperty name="export.excel.filename" value="CustomerFileNotes List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="CustomerFileNotes List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="CustomerFileNotes List.pdf"/>   
				</c:if>
				 <c:if test="${notess=='[]' }">
				 <display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" />
				    <display:column property="noteType" sortable="true" titleKey="notes.noteType" style="width:100px"/>
				    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType" style="width:80px"/>
				    <display:column property="subject" sortable="true" titleKey="notes.subject" maxLength="10" style="width:80px"/>
				    <display:column property="note" sortable="true" title="Note" maxLength="17"/>
				    <display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
						<c:if test="${notesList.noteStatus == 'NEW'}" >
						</c:if>
						<c:if test="${notesList.noteStatus != 'NEW'}" >
							<fmt:timeZone value="${sessionTimeZone}" >
								<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
								</fmt:timeZone>
						</c:if>
					</display:column>
					<display:column property="updatedOn" sortable="true" titleKey="notes.updatedOn" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
				    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"  style="width:45px"/>
				</c:if>
				</display:table> 
				</c:if>
				<c:if test="${param.popup}"> 
				<display:table name="notess" class="table" requestURI="" id="notesList" export="false"  pagesize="10" style="width:100%;margin-top:2px;"> 
				 <c:if test="${notess!='[]' }">
				<display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" url="/editNewNoteForPartnerPrivate.html?from=list&id1=${notesList.notesKeyId}&imageId=${imageId}&fieldId=${fieldId}&decorator=popup&popup=true" paramId="id" paramProperty="id"/>

				    <display:column property="noteType" sortable="true" titleKey="notes.noteType" style="width:100px"/>
				    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType" style="width:80px"/>
				    <display:column property="subject" sortable="true" titleKey="notes.subject" maxLength="10" style="width:80px"/>
				    <display:column property="note" sortable="true" title="Note" maxLength="17"/>
				    <display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
						<c:if test="${notesList.noteStatus == 'NEW'}" >
						</c:if>
						<c:if test="${notesList.noteStatus != 'NEW'}" >
							<fmt:timeZone value="${sessionTimeZone}" >
								<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
								</fmt:timeZone>
						</c:if>
					</display:column>
					<display:column property="updatedOn" sortable="true" titleKey="notes.updatedOn" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
				    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"  style="width:45px"/>
				    
				    <display:setProperty name="paging.banner.item_name" value="customerfilenotes"/>   
				    <display:setProperty name="paging.banner.items_name" value="notes"/>   
				
				    <display:setProperty name="export.excel.filename" value="CustomerFileNotes List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="CustomerFileNotes List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="CustomerFileNotes List.pdf"/>   
				</c:if>
				 <c:if test="${notess=='[]' }">
				 <display:column property="notesId" style="width:20px" sortable="true" titleKey="notes.notesId" />
				    <display:column property="noteType" sortable="true" titleKey="notes.noteType" style="width:100px"/>
				    <display:column property="noteSubType" sortable="true" titleKey="notes.noteSubType" style="width:80px"/>
				    <display:column property="subject" sortable="true" titleKey="notes.subject" maxLength="10" style="width:80px"/>
				    <display:column property="note" sortable="true" title="Note" maxLength="17"/>
				    <display:column  sortable="true" title="Follow&nbsp;Up&nbsp;Date" >
						<c:if test="${notesList.noteStatus == 'NEW'}" >
						</c:if>
						<c:if test="${notesList.noteStatus != 'NEW'}" >
							<fmt:timeZone value="${sessionTimeZone}" >
								<fmt:formatDate value="${notesList.forwardDate}" pattern="dd-MMM-yyyy"/>
								</fmt:timeZone>
						</c:if>
					</display:column>
					<display:column property="updatedOn" sortable="true" titleKey="notes.updatedOn" style="width:100px" format="{0,date,dd-MMM-yyyy}" />
				    <display:column property="updatedBy" sortable="true" title="Modified&nbsp;By"  style="width:45px"/>
				</c:if>
				</display:table>  
			</c:if> 
</div><!-- 
 <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab"> 

 <c:if test="${not empty serviceOrder.id}"> 
		<input type="button" class="cssbutton" style="margin-right: 5px;margin-left:20px;height: 25px;width:90px; font-size: 15"  
        	onclick="location.href='<c:url value="/editNewNoteForServiceOrder.html?id=${serviceOrder.id }&isRal=true "/>'"  
        	value="<fmt:message key="button.addNewNote"/>"/> </c:if>

  </sec-auth:authComponent>
--></s:form>
 
<script type="text/javascript">   
    highlightTableRows("notesList");   
    Form.focusFirstElement($("notesListForm")); 
</script>