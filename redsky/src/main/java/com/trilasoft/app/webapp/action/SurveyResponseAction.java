package com.trilasoft.app.webapp.action;

import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.log4j.Logger;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;


import com.trilasoft.app.model.SurveyResponse;

import com.trilasoft.app.service.SurveyResponseManager;

public class SurveyResponseAction  extends BaseAction{
	
	private SurveyResponseManager surveyResponseManager;
	private SurveyResponse surveyResponse;
	private List surveyResponseList;
	private String sessionCorpID;
	private Long id;
	static final Logger logger = Logger.getLogger(SurveyResponse.class);
	public SurveyResponseAction()
	{
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User)auth.getPrincipal();
		this.sessionCorpID=user.getCorpID();	
	}
	public SurveyResponse getSurveyResponse() {
		return surveyResponse;
	}
	public void setSurveyResponse(SurveyResponse surveyResponse) {
		this.surveyResponse = surveyResponse;
	}
	public List getSurveyResponseList() {
		return surveyResponseList;
	}
	public void setSurveyResponseList(List surveyResponseList) {
		this.surveyResponseList = surveyResponseList;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setSurveyResponseManager(SurveyResponseManager surveyResponseManager) {
		this.surveyResponseManager = surveyResponseManager;
	}	
}
