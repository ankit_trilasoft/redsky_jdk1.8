package com.trilasoft.app.webapp.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.trilasoft.app.service.PayrollManager;

public class PersonalDayUsedUpdate extends QuartzJobBean {
	
	private static final String APPLICATION_CONTEXT_KEY = "applicationContext";
	private ApplicationContext appCtx;
	public static final String DATE_FORMAT_NOW = "yyyy-MM-dd";
	private PayrollManager payrollManager; 
	static final Logger logger = Logger.getLogger(PersonalDayUsedUpdate.class);
	
	private ApplicationContext getApplicationContext(JobExecutionContext context) throws Exception{
		ApplicationContext appCtx = null;
		appCtx = (ApplicationContext) context.getScheduler().getContext().get(
				APPLICATION_CONTEXT_KEY);
		if (appCtx == null) {
			throw new JobExecutionException(
					"No application context available in scheduler context for key \""
							+ APPLICATION_CONTEXT_KEY + "\"");
		}
		return appCtx;
	}
	
	
	protected void executeInternal(JobExecutionContext context)	throws JobExecutionException {
		try {
		/*	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd ");
			Date date = new Date();
			String currentDate=dateFormat.format(date); 
			String [] currentDateArray=currentDate.split("-");
			int month=Integer.parseInt(currentDateArray[1]);
			int todayDate=Integer.parseInt(currentDateArray[2]);
			if(month==01 && todayDate==01){
				
			}*/
			appCtx = getApplicationContext(context);
			payrollManager = (PayrollManager) appCtx.getBean("payrollManager");
			payrollManager.updateSickPersonalDay();
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.warn("payroll updater Exception ::"+e);
		}
	}

	public void setPayrollManager(PayrollManager payrollManager) {
		this.payrollManager = payrollManager;
	}
}
