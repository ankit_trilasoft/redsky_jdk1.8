DELIMITER $$
create trigger redsky.trigger_add_history_cportalresourcemgmt
BEFORE UPDATE on redsky.cportalresourcemgmt
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";

      


      
      IF (OLD.billToCode <> NEW.billToCode ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.billToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToCode,'~') into newValueList;
   END IF;



      
      IF (OLD.billToName <> NEW.billToName ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.billToName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToName,'~') into newValueList;
   END IF;



      
      IF (OLD.destinationCountry <> NEW.destinationCountry ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.destinationCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.destinationCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.destinationCountry,'~') into newValueList;
   END IF;



      
      IF (OLD.originCountry <> NEW.originCountry ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.originCountry~') into fieldNameList;
       select CONCAT(oldValueList,OLD.originCountry,'~') into oldValueList;
       select CONCAT(newValueList,NEW.originCountry,'~') into newValueList;
   END IF;

      IF (OLD.mode <> NEW.mode ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.mode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.mode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.mode,'~') into newValueList;
   END IF;

      
      IF (OLD.documentType <> NEW.documentType ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.documentType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.documentType,'~') into newValueList;
   END IF;



      
      IF (OLD.documentName <> NEW.documentName ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.documentName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.documentName,'~') into newValueList;
   END IF;



      
      IF (OLD.fileContentType <> NEW.fileContentType ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.fileContentType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileContentType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fileContentType,'~') into newValueList;
   END IF;



      
      IF (OLD.fileFileName <> NEW.fileFileName ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.fileFileName~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileFileName,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fileFileName,'~') into newValueList;
   END IF;



      
      IF (OLD.fileType <> NEW.fileType ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.fileType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fileType,'~') into newValueList;
   END IF;



      
      IF (OLD.documentLocation <> NEW.documentLocation ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.documentLocation~') into fieldNameList;
       select CONCAT(oldValueList,OLD.documentLocation,'~') into oldValueList;
       select CONCAT(newValueList,NEW.documentLocation,'~') into newValueList;
   END IF;



      
      IF (OLD.corpid <> NEW.corpid ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.corpid~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
       select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
   END IF;



      
      IF (OLD.billToExcludes <> NEW.billToExcludes ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.billToExcludes~') into fieldNameList;
       select CONCAT(oldValueList,OLD.billToExcludes,'~') into oldValueList;
       select CONCAT(newValueList,NEW.billToExcludes,'~') into newValueList;
   END IF;



      
      IF (OLD.language <> NEW.language ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.language~') into fieldNameList;
       select CONCAT(oldValueList,OLD.language,'~') into oldValueList;
       select CONCAT(newValueList,NEW.language,'~') into newValueList;
   END IF;



      
      IF (OLD.infoPackage <> NEW.infoPackage ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.infoPackage~') into fieldNameList;
       select CONCAT(oldValueList,OLD.infoPackage,'~') into oldValueList;
       select CONCAT(newValueList,NEW.infoPackage,'~') into newValueList;
   END IF;



      
      IF (OLD.fileSize <> NEW.fileSize ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.fileSize~') into fieldNameList;
       select CONCAT(oldValueList,OLD.fileSize,'~') into oldValueList;
       select CONCAT(newValueList,NEW.fileSize,'~') into newValueList;
   END IF;



      
      IF (OLD.jobType <> NEW.jobType ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.jobType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.jobType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.jobType,'~') into newValueList;
   END IF;


      
      IF (OLD.childBillToCode <> NEW.childBillToCode ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.childBillToCode~') into fieldNameList;
       select CONCAT(oldValueList,OLD.childBillToCode,'~') into oldValueList;
       select CONCAT(newValueList,NEW.childBillToCode,'~') into newValueList;
   END IF;

   
      
      IF (OLD.docSequenceNumber <> NEW.docSequenceNumber ) THEN
       select CONCAT(fieldNameList,'cportalresourcemgmt.docSequenceNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.docSequenceNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.docSequenceNumber,'~') into newValueList;
   END IF;


    



    
   CALL add_tblHistory (OLD.id,"cportalresourcemgmt", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$

