package com.trilasoft.app.dao.hibernate;
import java.util.List;

import org.apache.log4j.Logger;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.AgentDao;

import com.trilasoft.app.model.Agent;

public class AgentDaoHibernate extends GenericDaoHibernate<Agent, Long> implements AgentDao
{   
	Logger logger = Logger.getLogger(AgentDaoHibernate.class);
    public AgentDaoHibernate() 
    {   
        super(Agent.class);   
    
    }
    public List<Agent> findByLastName(String lastName) {
    	try{
        return getHibernateTemplate().find("from Agent where lastName=?", lastName);   
    }catch (Exception e) {
		logger.error("Error executing query"+ e,e);
	}
	return null;
    }
    
    
    public List findBillingMaxId(Long shipNumber) {
    	try{
    	return getHibernateTemplate().find("select max(id) from Agent");
    	}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
    }
    
    public List findMaximumCarrierNumber(String shipNum) {
    	try{
    	return getHibernateTemplate().find("select max(carrierNumber) from ServicePartner where shipNumber=?", shipNum);
    	}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
    }
    
    
    public List checkById(Long id) {
    	try{
    	
    	return getHibernateTemplate().find("select id from Agent where id=?",id);
    	}catch (Exception e) {
    		logger.error("Error executing query"+ e,e);
    	}
    	return null;
    }
}
