<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Tool Tip</title>   
<meta name="heading" content="Tool Tip"/> 
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b></b></td>
	<td align="right"  style="width:30px;">
		<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" />
	</td>
</tr>
</table>  
<display:table name="tooltipList" class="table" requestURI="" id="tooltipList" export="false" >
<c:if test="${multiCurrency=='Y'}">
	<display:column title="Details" ><c:out value="${tooltipList.payStatus}" />
	</display:column>
</c:if>
<c:if test="${multiCurrency!='Y'}">
	<display:column title="Payment Status" ><c:out value="${tooltipList.payStatus}" />
	</display:column>
</c:if>

</display:table>