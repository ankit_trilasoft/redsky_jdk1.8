DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`FindsumofActualRevenue` $$
CREATE DEFINER=`root`@`%` FUNCTION `FindsumofActualRevenue`(v1 VARCHAR(50)) RETURNS double
BEGIN
           DECLARE i1,avg Double ;
     select case when (recinvoicenumber is not null or recinvoicenumber!='') then sum(actualrevenue) else '0' 
     end as 'InvoiceNumber' INTO i1 from accountline where recinvoicenumber !='' and shipnumber=v1 ;

    SET avg = i1;
    
  RETURN avg;
END $$

DELIMITER ;