CREATE TABLE  `redsky`.`entitlement` (`id` bigint(20) NOT NULL AUTO_INCREMENT,
`eoption` varchar(200) DEFAULT NULL,`edescription` varchar(500) DEFAULT NULL,
`updatedOn` datetime DEFAULT NULL,`updatedBy` varchar(45) DEFAULT NULL,
`createdOn` datetime DEFAULT NULL,`createdBy` varchar(45) DEFAULT '',
`partnerPrivateId` bigint(20) DEFAULT '0',`corpID` varchar(45) DEFAULT NULL,
`customerFileId` bigint(20) DEFAULT NULL,`partnerCode` varchar(20) DEFAULT NULL,
PRIMARY KEY (`id`));

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `billingCurrency` VARCHAR(4) AFTER `transPort`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `contact` BIT(1) DEFAULT b'0' AFTER `basedAtName`;

ALTER TABLE `redsky`.`app_user` ADD COLUMN `jobFunction` VARCHAR(45) AFTER `basedAtName`;

ALTER TABLE `redsky`.`policydocument` ADD COLUMN `fileName` VARCHAR(375) AFTER `language`;

ALTER TABLE `redsky`.`customerfile` ADD COLUMN `costCenter` VARCHAR(50) AFTER `originAgentCode`,ADD COLUMN `partnerEntitle` VARCHAR(200) AFTER `costCenter`;

ALTER TABLE `redsky`.`contractaccount` ADD COLUMN `published` VARCHAR(2) AFTER `parentAccountCode`;

ALTER TABLE `redsky`.`app_user` CHANGE COLUMN `passwordexpiryDate` `pwdexpiryDate` DATETIME;

ALTER TABLE `redsky`.`policydocument` ADD COLUMN `fileLocation` VARCHAR(254) AFTER `fileName`;

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `soNetworkGroup` BIT(1) ; 

insert  into networkdatafields (modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault)
values('Billing','billToCode' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billToName' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','contract' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billTo1Point' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billingInstructionCodeWithDesc' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','creditTerms' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billingInstruction' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billTo2Code' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billTo2Name' ,'Create','amishra',now(),'amishra',now(),false), 
('Billing','billTo2Authority' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','billTo2Reference' ,'Create','amishra',now(),'amishra',now(),false), 
('Billing','billTo2Point' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','privatePartyBillingCode' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','privatePartyBillingName' ,'Create','amishra',now(),'amishra',now(),false),
('Billing','payMethod' ,'Create','amishra',now(),'amishra',now(),false);

insert  into networkdatafields (modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault)
values('Billing','billToCode' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billToName' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','contract' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billTo1Point' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billingInstructionCodeWithDesc' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','creditTerms' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billingInstruction' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billTo2Code' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billTo2Name' ,'Update','amishra',now(),'amishra',now(),false), 
('Billing','billTo2Authority' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billTo2Reference' ,'Update','amishra',now(),'amishra',now(),false), 
('Billing','billTo2Point' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','privatePartyBillingCode' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','privatePartyBillingName' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','payMethod' ,'Update','amishra',now(),'amishra',now(),false);

ALTER TABLE `redsky`.`networkdatafields` ADD COLUMN `toModelName` VARCHAR(45) DEFAULT NULL AFTER `useSysDefault`,ADD COLUMN `toFieldName` VARCHAR(60) DEFAULT NULL AFTER `toModelName`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName)
values('AccountLine','category' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','status' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','chargeCode' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateDate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estCurrency' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','basis' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateQuantity' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateSellRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateSellRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estimateRate'),
('AccountLine','estimateRevenueAmount' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateRevenueAmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estimateExpense'),
('AccountLine','revisionCurrency' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionQuantity' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionSellRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionSellRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionRate'),
('AccountLine','revisionRevenueAmount' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionRevenueAmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionExpense'),
('AccountLine','billToCode' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','billToName' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recQuantity' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateCurrency' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recCurrencyRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','country'),
('AccountLine','actualRevenueForeign' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','localAmount');

ALTER TABLE `redsky`.`trackingstatus` ADD COLUMN `accNetworkGroup` BIT(1) DEFAULT b'0';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName)
values('AccountLine','estExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateExchange' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','actualRevenueForeign' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','actualRevenue' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractRate' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateExchange' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','exchangeRate');

ALTER TABLE `redsky`.`accountline` ADD COLUMN `networkSynchedId` BIGINT(20) DEFAULT NULL AFTER `contractValueDate`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName)
values('AccountLine','category' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','status' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','chargeCode' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateDate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estCurrency' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','basis' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateQuantity' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateSellRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateSellRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estimateRate'),
('AccountLine','estimateRevenueAmount' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','estimateRevenueAmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estimateExpense'),
('AccountLine','revisionCurrency' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionQuantity' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionSellRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionSellRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionRate'),
('AccountLine','revisionRevenueAmount' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionRevenueAmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionExpense'),
('AccountLine','billToCode' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','billToName' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recQuantity' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateCurrency' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recCurrencyRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','country'),
('AccountLine','actualRevenueForeign' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','localAmount'),
('AccountLine','estExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','revisionExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateExchange' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','actualRevenueForeign' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','actualRevenue' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractRate' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','contractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','recRateExchange' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','exchangeRate');

ALTER TABLE `redsky`.`accountline` MODIFY COLUMN `invoiceCreatedBy` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL;

ALTER TABLE `redsky`.`networkdatafields` ADD COLUMN `type` VARCHAR(3) DEFAULT NULL AFTER `toFieldName`;

update networkdatafields set type='CMM' where modelName='Billing' and fieldName in('billToCode','billToName','billTo2Code','billTo2Name','billTo2Authority','billTo2Reference','billTo2Point','privatePartyBillingCode','privatePartyBillingName','payMethod')

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','contractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','racValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','racValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','valueDate','CMM');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','contractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','racValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','racValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','valueDate','CMM');

delete from networkdatafields where modelName='AccountLine' and fieldName in('estCurrency','estExchangeRate','revisionCurrency','revisionExchangeRate')

update networkdatafields set type='CMM' where modelName='AccountLine' and fieldName in
('contractExchangeRate','recRateExchange','exchangeRate','billToCode','billToName','recCurrencyRate','actualRevenueForeign','recRate','actualRevenue','contractExchangeRate','recRateExchange')

ALTER TABLE `redsky`.`accountline` ADD COLUMN `agentFeesNo` VARCHAR(20) DEFAULT '' AFTER `networkSynchedId`;

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','recVatDescr' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','recVatPercent' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','recVatAmt' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'), 
('AccountLine','actualRevenue' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','actualExpense','CMM'),
('AccountLine','recVatDescr' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','recVatPercent' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','recVatAmt' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','actualRevenue' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','actualExpense','CMM'),
('AccountLine','recVatDescr' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payVatDescr','CMM'),
('AccountLine','recVatPercent' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payVatPercent','CMM'),
('AccountLine','recVatAmt' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payVatAmt','CMM'),
('AccountLine','recVatDescr' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payVatDescr','CMM'),
('AccountLine','recVatPercent' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payVatPercent','CMM'),
('AccountLine','recVatAmt' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payVatAmt','CMM');

insert  into networkdatafields (modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault) values('Billing','insuranceHas' ,'Create','amishra',now(),'amishra',now(),false),('Billing','insuranceHas' ,'Update','amishra',now(),'amishra',now(),false);

ALTER TABLE `redsky`.`policydocument` ADD COLUMN `contentFileType` VARCHAR(45) AFTER `fileLocation`;

ALTER TABLE `redsky`.`company` ADD COLUMN `justSayYes` bit(1) DEFAULT b'0' AFTER `TransfereeInfopackage`, ADD COLUMN `qualitySurvey` bit(1) DEFAULT b'0' AFTER `justSayYes`;
 
ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `customerFeedback` VARCHAR(45) AFTER `ratingScale`;
 
ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `description1` VARCHAR(50) AFTER `customerFeedback`, ADD COLUMN `description2` VARCHAR(50) AFTER `description1`, ADD COLUMN `description3` VARCHAR(50) AFTER `description2`, ADD COLUMN `link1` TEXT AFTER `description3`,ADD COLUMN `link2` TEXT AFTER `link1`, ADD COLUMN `link3` TEXT AFTER `link2`;

ALTER TABLE `redsky`.`contract` ADD COLUMN `payableContractCurrency` VARCHAR(3) AFTER `published`;

ALTER TABLE `redsky`.`charges` ADD COLUMN `payableContractCurrency` VARCHAR(3) AFTER `buyDependSell`;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `payableContractCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `payableContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000' ,ADD COLUMN `payableContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00' ,ADD COLUMN `payableContractValueDate` DATETIME DEFAULT NULL ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `estSellCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `estSellLocalAmount` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `estSellExchangeRate` DECIMAL(19,4) DEFAULT '0.0000', ADD COLUMN `estSellValueDate` DATETIME DEFAULT NULL , ADD COLUMN `estSellLocalRate` DECIMAL(19,2) DEFAULT '0.00' ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `revisionSellCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `revisionSellLocalAmount` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `revisionSellExchangeRate` DECIMAL(19,4) DEFAULT '0.0000' , ADD COLUMN `revisionSellValueDate` DATETIME DEFAULT NULL , ADD COLUMN `revisionSellLocalRate` DECIMAL(19,2) DEFAULT '0.00' ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `estiamteContractCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `estiamteContractRate` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `estiamteContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000' , ADD COLUMN `estiamteContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00' ,ADD COLUMN `estiamteContractValueDate` DATETIME DEFAULT NULL ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `estiamtePayableContractCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `estiamtePayableContractRate` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `estiamtePayableContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000' `,  ADD COLUMN `estiamtePayableContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00' ,ADD COLUMN `estiamtePayableContractValueDate` DATETIME DEFAULT NULL ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `revisionContractCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `revisionContractRate` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `revisionContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000' , ADD COLUMN `revisionContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00' ,ADD COLUMN `revisionContractValueDate` DATETIME DEFAULT NULL ;

ALTER TABLE `redsky`.`accountline` ADD COLUMN `revisionPayableContractCurrency` VARCHAR(3) DEFAULT NULL , ADD COLUMN `revisionPayableContractRate` DECIMAL(19,2) DEFAULT '0.00' , ADD COLUMN `revisionPayableContractExchangeRate` DECIMAL(19,4) DEFAULT '0.0000', ADD COLUMN `revisionPayableContractRateAmmount` DECIMAL(19,2) DEFAULT '0.00' ,ADD COLUMN `revisionPayableContractValueDate` DATETIME DEFAULT NULL ;

CREATE TABLE `redsky`.`qualitysurveysettings` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `surveyId` BIGINT(20) DEFAULT NULL,
  `accountId` VARCHAR(8),
  `surveyEmailRecipientId` BIGINT(20) DEFAULT NULL,
  `surveyEmailSubjectId` BIGINT(20) DEFAULT NULL,
  `surveyEmailBodyId` BIGINT(20) DEFAULT NULL,
  `surveyEmailSignatureId` BIGINT(20) DEFAULT NULL,
  `mode` VARCHAR(20),
  `delayInterval` INTEGER DEFAULT NULL,
  `sendLimit` INTEGER DEFAULT NULL,
  `timeSent` INTEGER DEFAULT NULL,
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  `shipNumber` VARCHAR(15),
  PRIMARY KEY(`id`)
);

CREATE TABLE `redsky`.`refsurveyemailrecipient` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `recipient` VARCHAR(200),
  `description` VARCHAR(1000),
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);

CREATE TABLE `redsky`.`refsurveyemailsubject` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `subject` VARCHAR(200),
  `description` VARCHAR(200),
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);

CREATE TABLE `redsky`.`refsurveyemailbody` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `emailBody` VARCHAR(1000),
  `description` VARCHAR(200),
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);

CREATE TABLE `redsky`.`refsurveyemailsignature` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `signature` VARCHAR(100),
  `description` VARCHAR(200),
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);

CREATE TABLE `redsky`.`surveyemailaudit` (
  `id` BIGINT(20) NOT NULL auto_increment,
  `accountId` VARCHAR(8),
  `surveyEmailId` BIGINT(20) DEFAULT NULL,
  `lastSent` DATETIME,
  `numberSent` INTEGER DEFAULT NULL,
  `recievedResponse` bit(1) DEFAULT b'0',
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  `shipNumber` VARCHAR(15),
  PRIMARY KEY(`id`)
);
 
ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludeFromParentUpdate` BIT(1) DEFAULT b'0' AFTER `ratingScale`;

insert  into networkdatafields (modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault)
values('Billing','postGrate' ,'Create','amishra',now(),'amishra',now(),false),('Billing','postGrate' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','onHand' ,'Create','amishra',now(),'amishra',now(),false),('Billing','onHand' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','charge' ,'Create','amishra',now(),'amishra',now(),false),('Billing','charge' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','cycle' ,'Create','amishra',now(),'amishra',now(),false),('Billing','cycle' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','storagePerMonth' ,'Create','amishra',now(),'amishra',now(),false),('Billing','storagePerMonth' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','storageOut' ,'Create','amishra',now(),'amishra',now(),false),('Billing','storageOut' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','billThrough' ,'Create','amishra',now(),'amishra',now(),false),('Billing','billThrough' ,'Update','amishra',now(),'amishra',now(),false),
('Billing','storageMeasurement' ,'Create','amishra',now(),'amishra',now(),false),('Billing','storageMeasurement' ,'Update','amishra',now(),'amishra',now(),false);  

ALTER TABLE `redsky`.`costelement` ADD UNIQUE `costElementUnique`(`corpId`, `costElement`);

ALTER TABLE `redsky`.`partnerpublic` ADD COLUMN `bankCode` VARCHAR(20) AFTER `billingCurrency`, ADD COLUMN `bankAccountNumber` VARCHAR(20) AFTER `bankCode`, ADD COLUMN `vatNumber` VARCHAR(25) AFTER `bankAccountNumber`;
 
INSERT INTO parametercontrol(corpID,parameter,fieldLength,active,createdon,createdby,updatedon,updatedby,tsftFlag,hybridFlag,customType) VALUES('TSFT','FieldType',50,'',now(),'amishra',now(),'amishra',1,0,'');

INSERT INTO refmaster (corpID,parameter, code, description,fieldLength,createdOn,createdBy) VALUES ('TSFT','FieldType','', '',50,now(),'amishra'),('TSFT','FieldType','Required','Required',50,now(),'amishra'),('TSFT','FieldType','Unique (Key as auto generated #s)', 'Unique (Key as auto generated #s)',50,now(),'amishra'),('TSFT','FieldType','VanLine', 'VanLine',50,now(),'amishra'),('TSFT','FieldType','MultiCurrencyBilling', 'MultiCurrencyBilling',50,now(),'amishra'),('TSFT','FieldType','Redundant?', 'Redundant?',50,now(),'amishra');

ALTER TABLE `redsky`.`datacatalog` MODIFY COLUMN `usDomestic` VARCHAR(50) DEFAULT '';

INSERT into refmaster (corpid, code, description, fieldlength,parameter) values ('TSFT', 'KGA', 'KGA', 3, 'AgentType');

ALTER TABLE `redsky`.`company` ADD COLUMN `workticketQueue` bit(1) DEFAULT b'0' AFTER qualitySurvey;

ALTER TABLE `redsky`.`partnerprivate` ADD COLUMN `excludeFromParentPolicyUpdate` BIT(1) DEFAULT b'0' AFTER `link3`,ADD COLUMN `excludeFromParentFaqUpdate` BIT(1) DEFAULT b'0' AFTER `excludeFromParentPolicyUpdate`;

ALTER TABLE `redsky`.`accountcontact` ADD COLUMN `basedAt` VARCHAR(8) AFTER `userName`, ADD COLUMN `basedAtName` VARCHAR(225) AFTER `basedAt`;

CREATE TABLE `redsky`.`surveyresponse` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `qualitySurveyId` BIGINT(20) DEFAULT 0,
  `status` VARCHAR(45) DEFAULT '',
  `score` BIGINT(20) DEFAULT 0,
  `submittedDate` DATETIME,
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);
 
CREATE TABLE `redsky`.`surveyresponsedtl` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `surveyResponseId` BIGINT(20),
  `questionId` BIGINT(20),
  `answerId` BIGINT(20),
  `answer` VARCHAR(200),
  `answerVersion` BIGINT(20),
  `submittedDate` DATETIME,
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);
 
CREATE TABLE `redsky`.`keysurveyquestionmap` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `surveyQuestionId` BIGINT(20),
  `questionText` VARCHAR(20),
  `type` VARCHAR(20),
  `corpId` VARCHAR(15),
  `createdBy` VARCHAR(82),
  `createdOn` DATETIME,
  `updatedBy` VARCHAR(82),
  `updatedOn` DATETIME,
  PRIMARY KEY(`id`)
);

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','estiamteContractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractCurrency','CMM'),
('AccountLine','estiamteContractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractValueDate','CMM'),
('AccountLine','estiamteContractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractExchangeRate','CMM'),
('AccountLine','estiamteContractRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractRate','CMM'),
('AccountLine','estiamteContractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractRateAmmount','CMM'),
('AccountLine','estSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estCurrency','CMM'),
('AccountLine','estSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estValueDate','CMM'),
('AccountLine','estSellExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estExchangeRate','CMM'),
('AccountLine','estSellLocalRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellLocalRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estLocalRate','CMM'),
('AccountLine','estSellLocalAmount' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellLocalAmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','estLocalAmount','CMM'),
('AccountLine','revisionContractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractCurrency','CMM'),
('AccountLine','revisionContractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractValueDate','CMM'),
('AccountLine','revisionContractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractExchangeRate','CMM'),
('AccountLine','revisionContractRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractRate','CMM'),
('AccountLine','revisionContractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractRateAmmount','CMM'),
('AccountLine','revisionSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionCurrency','CMM'),
('AccountLine','revisionSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionValueDate','CMM'),
('AccountLine','revisionSellExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionExchangeRate','CMM'),
('AccountLine','revisionSellLocalRate' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellLocalRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionLocalRate','CMM'),
('AccountLine','revisionSellLocalAmount' ,'Create','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellLocalAmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','revisionLocalAmount','CMM'),
('AccountLine','contractCurrency' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractCurrency','CMM'), 
('AccountLine','contractValueDate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractValueDate','CMM'),
('AccountLine','contractExchangeRate' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractExchangeRate','CMM'),
('AccountLine','contractRateAmmount' ,'Create','amishra',now(),'amishra',now(),false,'AccountLine','payableContractRateAmmount','CMM');

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName,type)
values('AccountLine','estiamteContractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractCurrency','CMM'),
('AccountLine','estiamteContractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractValueDate','CMM'),
('AccountLine','estiamteContractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractExchangeRate','CMM'),
('AccountLine','estiamteContractRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractRate','CMM'),
('AccountLine','estiamteContractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estiamteContractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estiamtePayableContractRateAmmount','CMM'),
('AccountLine','estSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estCurrency','CMM'),
('AccountLine','estSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estValueDate','CMM'),
('AccountLine','estSellExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estExchangeRate','CMM'),
('AccountLine','estSellLocalRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellLocalRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estLocalRate','CMM'),
('AccountLine','estSellLocalAmount' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','estSellLocalAmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','estLocalAmount','CMM'),
('AccountLine','revisionContractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractCurrency','CMM'),
('AccountLine','revisionContractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractValueDate','CMM'),
('AccountLine','revisionContractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractExchangeRate','CMM'),
('AccountLine','revisionContractRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractRate','CMM'),
('AccountLine','revisionContractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionContractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionPayableContractRateAmmount','CMM'),
('AccountLine','revisionSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionCurrency','CMM'),
('AccountLine','revisionSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionValueDate','CMM'),
('AccountLine','revisionSellExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionExchangeRate','CMM'),
('AccountLine','revisionSellLocalRate' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellLocalRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionLocalRate','CMM'),
('AccountLine','revisionSellLocalAmount' ,'Update','amishra',now(),'amishra',now(),false,'','','CMM'),
('AccountLine','revisionSellLocalAmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','revisionLocalAmount','CMM'),
('AccountLine','contractCurrency' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractCurrency','CMM'), 
('AccountLine','contractValueDate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractValueDate','CMM'),
('AccountLine','contractExchangeRate' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractExchangeRate','CMM'),
('AccountLine','contractRateAmmount' ,'Update','amishra',now(),'amishra',now(),false,'AccountLine','payableContractRateAmmount','CMM');

update contract set payablecontractcurrency='USD' where corpid='SSCW';
update contract set payablecontractcurrency='INR' where corpid='STAR';
update contract set payablecontractcurrency='EUR' where corpid='VOER';
update contract set payablecontractcurrency='EUR' where corpid='VORU';
update contract set payablecontractcurrency='EUR' where corpid='VOCZ';
update contract set payablecontractcurrency='NOK' where corpid='ADAM';
update contract set payablecontractcurrency='EUR' where corpid='UTSI';
update contract set payablecontractcurrency='HKD' where corpid='UGHK';
update contract set payablecontractcurrency='GBP' where corpid='BOUR';
update contract set payablecontractcurrency='USD' where corpid='JOHN';
update contract set payablecontractcurrency='CNY' where corpid='UGCN';
update contract set payablecontractcurrency='EUR' where corpid='VOAO';
update contract set payablecontractcurrency='USD' where corpid='CWMS';

update charges set payablecontractcurrency='USD' where corpid='SSCW';
update charges set payablecontractcurrency='INR' where corpid='STAR';
update charges set payablecontractcurrency='EUR' where corpid='VOER';
update charges set payablecontractcurrency='EUR' where corpid='VORU';
update charges set payablecontractcurrency='EUR' where corpid='VOCZ';
update charges set payablecontractcurrency='NOK' where corpid='ADAM';
update charges set payablecontractcurrency='EUR' where corpid='UTSI';
update charges set payablecontractcurrency='HKD' where corpid='UGHK';
update charges set payablecontractcurrency='GBP' where corpid='BOUR';
update charges set payablecontractcurrency='USD' where corpid='JOHN';
update charges set payablecontractcurrency='CNY' where corpid='UGCN';
update charges set payablecontractcurrency='EUR' where corpid='VOAO';
update charges set payablecontractcurrency='USD' where corpid='CWMS';
update charges set payablecontractcurrency='MYR' where corpid='UGMY';
update charges set payablecontractcurrency='SGD' where corpid='UGSG';

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault) values('ServiceOrder','bookingAgentCode' ,'Update','amishra',now(),'amishra',now(),false);

insert  into networkdatafields(modelName, fieldName, transactionType, createdBy, createdOn, updatedBy, updatedOn, useSysDefault,toModelName,toFieldName)
values('AccountLine','quoteDescription' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','description' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','note' ,'Create','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','quoteDescription' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','description' ,'Update','amishra',now(),'amishra',now(),false,'',''),
('AccountLine','note' ,'Update','amishra',now(),'amishra',now(),false,'','');

ALTER TABLE `redsky`.`contractpolicy` ADD COLUMN `parentId` BIGINT(20) AFTER `documentName`;

ALTER TABLE `redsky`.`policydocument` ADD COLUMN `parentId` BIGINT(20) AFTER `contentFileType`;

ALTER TABLE `redsky`.`frequentlyaskedquestions` ADD COLUMN `parentId` BIGINT(20) AFTER `language`;









