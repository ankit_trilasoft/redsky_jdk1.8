DELIMITER $$

DROP FUNCTION IF EXISTS `redsky`.`findnetworkcorpid` $$
CREATE DEFINER=`redsky`@`localhost` FUNCTION `findnetworkcorpid`(code varchar(50)) RETURNS varchar(10) CHARSET utf8
    DETERMINISTIC
BEGIN
DECLARE Name varchar(10) DEFAULT 0;

SELECT distinct agentcorpid INTO Name FROM networkpartners WHERE
agentpartnercode=code;

RETURN Name;

END $$

DELIMITER ;