<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
 <head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>   
<s:form id="partnerDetailsForm" method="post" > 

<table class="detailTabLabel" cellspacing="0" cellpadding="0" style="margin:0px;width:100%;">
<tr>
 <td valign="top">
 <div style="overflow-y:auto;max-height:215px;">
 <display:table name="partnerDetailsAutoComplete" class="table" id="partnerDetailsAutoComplete" style="width:100%;"> 
   	<display:column title="Code" >
    	<c:set var="d" value="'"/>
     <c:set var="des" value="${fn:replace(partnerDetailsAutoComplete.lastname,d, '~')}" />
   	<a onclick="copyPartnerPricingDetails('${partnerDetailsAutoComplete.partnercode}','${des}','${partnerNameId}','${paertnerCodeId}','${autocompleteDivId}','${pricingAccountLineId}');">${partnerDetailsAutoComplete.partnercode}</a>
   	</display:column>
  	<display:column property="lastname" title="Name" ></display:column>
	<display:column property="billingcountry" title="Country" ></display:column>	
	<display:column property="billingstate" title="State" ></display:column>
	<display:column property="billingcity" title="City" ></display:column>
	<c:if test="${acctRefVisiable=='Yes'}"> 
 	<display:column title="Acct Ref #" titleKey="partner.rank" style="width:35px">
    	<a><img align="middle" title="Acct Ref #" onclick="findUserPermission('${partnerDetailsAutoComplete.partnercode}',this);" style="margin: 0px 0px 0px 8px;" src="${pageContext.request.contextPath}/images/user_permission.png"/></a>
    </display:column>
    </c:if>
	<display:column  title="Type" style="width:90px; whitespace: nowrap;" >
	<c:choose>
	<c:when test="${not empty partnerDetailsAutoComplete.bookingAgentCode}">
	<c:out value="${partnerDetailsAutoComplete.partnerType}"></c:out>
	<img class="openpopup" style="text-align:right;vertical-align:top;" src="${pageContext.request.contextPath}/images/rslogo_small_14.png" />
	</c:when>
	<c:otherwise>
	<c:out value="${partnerDetailsAutoComplete.partnerType}"></c:out>
	</c:otherwise>
	</c:choose>
	</display:column>
	<display:column property="aliasName" title="Alias Name"></display:column>
</display:table>
</div>
</td>
<td  align="right" valign="top"><img align="right" class="openpopup" style="position:absolute;top:1px;right:2px;" onclick="closeMypricingDiv('${autocompleteDivId}','${partnerNameId}','${paertnerCodeId}');" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>  
                                <c:if test="${not empty partnerDetailsAutoComplete}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="false"/>
								</c:if>
								<c:if test="${empty partnerDetailsAutoComplete}">
								<s:hidden id="partnerDetailsAutoCompleteListSize" value="true"/>
								</c:if>
</s:form>