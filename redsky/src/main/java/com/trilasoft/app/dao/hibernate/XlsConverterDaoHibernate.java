package com.trilasoft.app.dao.hibernate;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.sun.rowset.internal.Row;
import com.trilasoft.app.dao.XlsConverterDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.Location;
import com.trilasoft.app.model.PartnerAccountRef;
import com.trilasoft.app.model.StorageLibrary;
import com.trilasoft.app.service.DefaultAccountLineManager;
import com.trilasoft.app.service.LocationManager;
import com.trilasoft.app.service.StorageLibraryManager;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.service.RateGridManager;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Query;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class XlsConverterDaoHibernate extends GenericDaoHibernate<Location, Long > implements XlsConverterDao {
    private LocationManager locationManager;
    private HibernateUtil hibernateUtil;
	private RateGridManager rateGridManager;
    private StorageLibraryManager storageLibraryManager;
    private DefaultAccountLineManager defaultAccountLineManager;
	public XlsConverterDaoHibernate() {
		super(Location.class);

	}

	@SuppressWarnings("unchecked")
	public String saveData(String sessionCorpID, String file, String userName) {
		FileInputStream fis = null;
        int record=0;
        int existRecords=0;
        int rejectRecords=0;
        String totRecords=null;
        try{
        	List<String> locationId=getHibernateTemplate().find("select locationId from Location where corpID=?", sessionCorpID);
        	List<String> locationType=getHibernateTemplate().find("select code from RefMaster where corpID in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND parameter='LOCTYPE' ");
        	List<String> warehouseType=getHibernateTemplate().find("select code from RefMaster where corpID in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND parameter='HOUSE' ");
			List myList = new ArrayList();
			List myLocationId = new ArrayList();
			List myLocationType = new ArrayList();
			List myWarehouseType = new ArrayList();
			List mycapacityList = new ArrayList();
			List<String> capacityType = new ArrayList<String>();
			capacityType.add("1");
			capacityType.add("3");
			
			fis = new FileInputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
            	try {
                HSSFRow row = (HSSFRow) rows.next();
                if(row.getRowNum()==0){
            	  continue;
            	}
                Iterator cells = row.cellIterator();
                myList.clear();
                while (cells.hasNext()) {
                	HSSFCell cell = (HSSFCell) cells.next();
                    int type = cell.getCellType();
                    int cellNum = cell.getCellNum();
                    
                    if (cellNum == 0) {
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                        	myLocationId.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add((int)cell.getNumericCellValue());
                        	myLocationId.add((int)cell.getNumericCellValue());
                    	}
                    }
                    else if (cellNum == 1) {
                    	if(type == HSSFCell.CELL_TYPE_STRING ){
                    	myList.add(cell.getRichStringCellValue());
                    	myWarehouseType.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    	myList.add((int)cell.getNumericCellValue());
                    	myWarehouseType.add((int)cell.getNumericCellValue());
                    	}
                    }  
                    else if (cellNum == 2) {
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                        	myLocationType.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add((int)cell.getNumericCellValue());
                        	myLocationType.add((int)cell.getNumericCellValue());
                    	}
                    }    
                    else if (cellNum == 3) {
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    		mycapacityList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add((int)cell.getNumericCellValue());
                        	mycapacityList.add((int)cell.getNumericCellValue());
                    	}
                    }                   
                }  
                if(locationId.contains(myList.get(0).toString())==false && warehouseType.contains(myList.get(1).toString())==true && 
                	locationType.contains(myList.get(2).toString())==true && capacityType.contains(myList.get(3).toString())==true)
                { 
                	record++;
        			Location location= new Location();
                    location.setCorpID(sessionCorpID);
                    location.setLocationId(myList.get(0).toString());
                    location.setWarehouse(myList.get(1).toString());
                    location.setType(myList.get(2).toString());
                    location.setCapacity(myList.get(3).toString());                            
                    location.setCubicFeet(new BigDecimal(0));
                    location.setCubicMeter(new BigDecimal(0));
                    location.setUtilizedVolCft(new BigDecimal(0));
                    location.setUtilizedVolCbm(new BigDecimal(0));
                    location.setOccupied("");
                    location.setCreatedBy(userName);
                    location.setCreatedOn(new Date());
                    location.setUpdatedBy(userName);
                    location.setUpdatedOn(new Date());
                    location= locationManager.save(location);        
                	}
                   }
                   catch (Exception e) {
    	            e.printStackTrace();
    	        } 
		       } 
                for(int i=0;i< myLocationId.size();i++){
                	if(locationType.contains(myLocationType.get(i).toString())==false){
                		rejectRecords = rejectRecords + 1;	
                	}
                	else if(capacityType.contains(mycapacityList.get(i).toString())==false){
                		rejectRecords = rejectRecords + 1;	
                    }
                	else if(warehouseType.contains(myWarehouseType.get(i).toString())==false){
                			rejectRecords = rejectRecords + 1;	
                	}else{
                		if((locationId.contains(myLocationId.get(i).toString()))){
	                		existRecords = existRecords + 1;
	                	}
                	}
                }
               try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
            
        }catch (Exception e) {
            e.printStackTrace();
        }
        if(rejectRecords == 0 && existRecords == 0){
        totRecords= record+" Record(s) saved successfully.";
        }else if(existRecords == 0 && rejectRecords !=0){
		totRecords= record+" Record(s) saved successfully and "+rejectRecords+" record(s) rejected.";
        }else if(rejectRecords == 0 && existRecords != 0 ){
        	totRecords= record+" Record(s) saved successfully and "+existRecords+ " record(s) already exist.";
        }else
        totRecords= record+" Record(s) saved successfully and "+existRecords+ " record(s) already exist and "+rejectRecords+" record(s) rejected.";
		return totRecords;
	
	}

	public void setLocationManager(LocationManager locationManager) {
		this.locationManager = locationManager;
	}

	@SuppressWarnings("unchecked")
	public String saveStorageData(String sessionCorpID, String file,String userName) {
		 FileInputStream fis = null;
	        int record=0;
	        int existRecords=0;
	        int rejectRecords=0;
	        String totRecords=null;
	        try{
	        	List<String> storageId=this.getSession().createSQLQuery("select storageId from storagelibrary where corpId in('"+sessionCorpID+"')").list();
	        	List<String> storageCode=getHibernateTemplate().find("select code from RefMaster where corpID in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND parameter='STORAGE_TYPE' ");
				List myList = new ArrayList();
				List myStorageId = new ArrayList();
				List myStorageCode = new ArrayList();
				List volumeCftList = new ArrayList();
				List volumeCbmList = new ArrayList();
				List myStorageModeList = new ArrayList();
				List<String> storageMode = new ArrayList<String>();
				storageMode.add("1");
				storageMode.add("3");
				storageMode.add("5");
				
	        	fis = new FileInputStream(file);
	            HSSFWorkbook workbook = new HSSFWorkbook(fis);
	            HSSFSheet sheet = workbook.getSheetAt(0);
	            Iterator rows = sheet.rowIterator();
	            while (rows.hasNext()) {
	            	try {
	                HSSFRow row = (HSSFRow) rows.next();
	                if(row.getRowNum()==0){
	            	  continue;
	            	}
	                Iterator cells = row.cellIterator();
	                myList.clear();
	                while (cells.hasNext()) {
	                	HSSFCell cell = (HSSFCell) cells.next();
	                    int type = cell.getCellType();
	                    int cellNum = cell.getCellNum();
	                    
	                    if (cellNum == 0) {
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
		                    	myList.add(cell.getRichStringCellValue());
		                    	myStorageId.add(cell.getRichStringCellValue());	
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add((int)cell.getNumericCellValue());	
	                    		myStorageId.add((int)cell.getNumericCellValue());
	                    	}
	                    	
	                    }else if (cellNum == 1) {
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
	                    		myList.add(cell.getRichStringCellValue());
		                    	myStorageCode.add(cell.getRichStringCellValue());
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add((int)cell.getNumericCellValue());	
	                    		myStorageCode.add((int)cell.getNumericCellValue());
	                    	}
	                    	
	                    }  
	                    else if (cellNum == 2) {
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
	                    		myList.add(cell.getRichStringCellValue());	
	                    		myStorageModeList.add(cell.getRichStringCellValue());	
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add((int)cell.getNumericCellValue());	
	                    		myStorageModeList.add((int)cell.getNumericCellValue());	
	                    	}
	                    }   
	                    else if(cellNum == 3){
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
	                    		myList.add(cell.getRichStringCellValue());	
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add((int)cell.getNumericCellValue());	
	                    	}
	                    } 
	                    else if(cellNum == 4){
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
	                    		myList.add(cell.getRichStringCellValue());	
	                    		volumeCftList.add(cell.getRichStringCellValue());	
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add(cell.getNumericCellValue());	
	                    		volumeCftList.add(cell.getNumericCellValue());	
	                    	}
	                    }
	                    else if(cellNum == 5){
	                    	if(type == HSSFCell.CELL_TYPE_STRING){
	                    		myList.add(cell.getRichStringCellValue());	
	                    		volumeCbmList.add(cell.getRichStringCellValue());	
	                    	}
	                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
	                    		myList.add(cell.getNumericCellValue());	
	                    		volumeCbmList.add(cell.getNumericCellValue());	
	                    	}
	                    }
	                }  

	                if(storageId.contains(myList.get(0).toString())==false && storageCode.contains(myList.get(1).toString())==true && storageMode.contains(myList.get(2).toString())==true){
	                     if(((myList.get(4).equals(0.00)==true )&& ((myList.get(5).equals(0.00))==true))){
	                    	 
	                     }else {
	                    	    record = record + 1;
	                	        StorageLibrary storageLibrary= new StorageLibrary();
	                	        storageLibrary.setCorpId(sessionCorpID);
	                	        storageLibrary.setStorageId(myList.get(0).toString());
	                	        storageLibrary.setStorageType(myList.get(1).toString());
	                	        List<String> storageTypeDes=getHibernateTemplate().find("select description from RefMaster where corpID in ('"+sessionCorpID+"','"+hibernateUtil.getParentCorpID(sessionCorpID)+"') AND code='"+myList.get(1).toString()+"' AND parameter='STORAGE_TYPE' ");
	                	        if(storageTypeDes!=null && !storageTypeDes.isEmpty()){
	                	        storageLibrary.setStorageDes(storageTypeDes.get(0).toString());
	                	        }else{
	                	        storageLibrary.setStorageDes("");
	                	        }
	                	        storageLibrary.setStorageMode(myList.get(2).toString());
	                	        storageLibrary.setStorageDescription(myList.get(3).toString());
	                	        if(myList.get(4).equals(0.00)){
	                	        	BigDecimal volumeCbm = new BigDecimal(myList.get(5).toString());
	                	        	BigDecimal volumeCft = volumeCbm.multiply(new BigDecimal(35.3146));
	                	        	storageLibrary.setVolumeCft(volumeCft);
	                	        	storageLibrary.setAvailVolumeCft(volumeCft);
	                	        }
	                	        else{
	                	        	storageLibrary.setVolumeCft(new BigDecimal(myList.get(4).toString()));
	                	        	storageLibrary.setAvailVolumeCft(new BigDecimal(myList.get(4).toString()));
	                	        }
	                	        
	                	        if(myList.get(5).equals(0.00)){
	                	        	BigDecimal volumeCft = new BigDecimal(myList.get(4).toString());
	                	        	BigDecimal volumeCbm = volumeCft.multiply(new BigDecimal(0.0283168466));
	                	        	storageLibrary.setVolumeCbm(volumeCbm);
	                	        	storageLibrary.setAvailVolumeCbm(volumeCbm);
	                	        } else{
	                	        	storageLibrary.setVolumeCbm(new BigDecimal(myList.get(5).toString()));
	                	        	storageLibrary.setAvailVolumeCbm(new BigDecimal(myList.get(5).toString()));
	                	        }
	                	       
	                	        storageLibrary.setUsedVolumeCft(new BigDecimal(0));
	                	        storageLibrary.setUsedVolumeCbm(new BigDecimal(0));
	                	        storageLibrary.setStorageLocked(false);
	                	        storageLibrary.setStorageAssigned(false);
	                	        storageLibrary.setCreatedBy(userName);
	                	        storageLibrary.setCreatedOn(new Date());
	                	        storageLibrary.setUpdatedBy(userName);
	                	        storageLibrary.setUpdatedOn(new Date());
	                            storageLibraryManager.save(storageLibrary);	
                    		}
	                     }
	                    }
	                   catch (Exception e) {
	    	            e.printStackTrace();
	    	        } 
			       } 
	                for(int i=0;i< myStorageId.size();i++){
                       if(storageCode.contains(myStorageCode.get(i).toString())==false){
	                	    rejectRecords = rejectRecords + 1;	
	                	}
	                	else{
	                		if((storageId.contains(myStorageId.get(i).toString()))){
		                		if(((volumeCftList.get(i).equals(0.00)==true )&& ((volumeCbmList.get(i).equals(0.00))==true))||(storageMode.contains(myStorageModeList.get(i).toString())==false)){
		                	    rejectRecords = rejectRecords + 1;
		                		}
		                		else
			     	            existRecords = existRecords + 1;
		                	}else{
		                		if(((volumeCftList.get(i).equals(0.00)==true )&& ((volumeCbmList.get(i).equals(0.00))==true))||(storageMode.contains(myStorageModeList.get(i).toString())==false)){
			                	    rejectRecords = rejectRecords + 1;
			                     }
		                      }
		                	}	
	                   }
	               try {
						fis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
	            
	        }catch (Exception e) {
	            e.printStackTrace();
	        }
	        if(rejectRecords == 0 && existRecords == 0){
	        totRecords= record+" Record(s) saved successfully.";
	        }else if(existRecords == 0 && rejectRecords !=0){
			totRecords= record+" Record(s) saved successfully and "+rejectRecords+" record(s) rejected.";
	        }else if(rejectRecords == 0 && existRecords != 0 ){
	        	totRecords= record+" Record(s) saved successfully and "+existRecords+ " record(s) already exist.";
	        }else if(rejectRecords != 0 && existRecords != 0 && record == 0 ){
	        	 totRecords= existRecords+ " record(s) already exist and "+rejectRecords+" record(s) rejected.";
	        }else
	        totRecords= record+" Record(s) saved successfully and "+existRecords+ " record(s) already exist and "+rejectRecords+" record(s) rejected.";
			return totRecords;
	}

	public void setStorageLibraryManager(StorageLibraryManager storageLibraryManager) {
		this.storageLibraryManager = storageLibraryManager;
	}
	@SuppressWarnings("unchecked")
	public String saveRateGridData(String sessionCorpID, String file, String userName,String charge, String contractName, String fileExt) {
		FileInputStream fis = null;
        int record=0;
        String totRecords=null;
        if(fileExt.equalsIgnoreCase("xls")){
            try{
            	List myList = new ArrayList(); 
    			fis = new FileInputStream(file);
                HSSFWorkbook workbook = new HSSFWorkbook(fis);
                HSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rows = sheet.rowIterator();
                while (rows.hasNext()) {
                	try {
                    HSSFRow row = (HSSFRow) rows.next();
                    if(row.getRowNum()==0){
                	  continue;
                	}
                    Iterator cells = row.cellIterator();
                    myList.clear();
                    while (cells.hasNext()) {
                    	HSSFCell cell = (HSSFCell) cells.next();
                        int type = cell.getCellType();
                        int cellNum = cell.getCellNum();
                        
                        if (cellNum == 0) {
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        }
                        else if (cellNum == 1) {
                        	if(type == HSSFCell.CELL_TYPE_STRING ){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        	myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        }  
                        else if (cellNum == 2) {
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        } 
                        else if(cellNum == 3){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getRichStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        }
                                     
                    }  
                   		RateGrid rateGrid= new RateGrid();
            			rateGrid.setCorpID(sessionCorpID);
            			rateGrid.setCreatedBy(userName);
            			rateGrid.setCreatedOn(new Date());
            			rateGrid.setUpdatedBy(userName);
            			rateGrid.setUpdatedOn(new Date());
            			rateGrid.setCharge(charge);
            			rateGrid.setContractName(contractName);
            			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
            			rateGrid.setQuantity1(Double.parseDouble((myList.get(1).toString())));
            			}else{
            				rateGrid.setQuantity1(0);	
            			}
            			String rate="0";
            			Double rateNumaric= new Double("0");
            			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
            				rate=myList.get(2).toString();
            				try{
            				rateNumaric=Double.parseDouble(myList.get(2).toString());
            				DecimalFormat df = new DecimalFormat("0.0000");
            				rate = df.format(rateNumaric);
            				}catch(Exception e){
            					
            				}
            				rateGrid.setRate1(rate);
                			}else{
                				rateGrid.setRate1("0");	
                		}
            			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
                			rateGrid.setQuantity2(Double.parseDouble((myList.get(0).toString())));
                			}else{
                				rateGrid.setQuantity2(0);	
                		}                          
            			String buyRate="0";
            			Double buyRateNumaric= new Double("0");
            			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
            				buyRate=myList.get(3).toString();
            				try{
            				buyRateNumaric=Double.parseDouble(myList.get(3).toString());
            				DecimalFormat df = new DecimalFormat("0.0000");
            				buyRate = df.format(buyRateNumaric);
            				}catch(Exception e){
            					
            				}
            				rateGrid.setBuyRate(buyRate);
                			}else{
                				rateGrid.setBuyRate("0");	
                		}	        			
            			rateGrid= rateGridManager.save(rateGrid); 
            			record++;
                    	
                       }
                       catch (Exception e) {
        	            e.printStackTrace();
        	        } 
    		       } 

                   try {
    					fis.close();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
                
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
    if(fileExt.equalsIgnoreCase("xlsx")){
			       try{
			    	   	List myList = new ArrayList(); 
						fis = new FileInputStream(file);
			           XSSFWorkbook workbook = new XSSFWorkbook(fis);
			            XSSFSheet sheet = workbook.getSheetAt(0);
			            Iterator rows = sheet.rowIterator();
			            while (rows.hasNext()) {
			            	try {
			            	XSSFRow row = (XSSFRow) rows.next();
			                if(row.getRowNum()==0){
			            	  continue;
			            	}
			                Iterator cells = row.cellIterator();
			                myList.clear();
			                while (cells.hasNext()) {
			                	XSSFCell cell = (XSSFCell) cells.next();
			                    int type = cell.getCellType();
			                    int cellNum = cell.getColumnIndex();
			                    
			                    if (cellNum == 0) {
			                    	if(type == XSSFCell.CELL_TYPE_STRING){
			                    		myList.add(cell.getRichStringCellValue());
			                    	}
			                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
			                    		myList.add(cell.getNumericCellValue());
			                    	}
			                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
		                        		myList.add(cell.getNumericCellValue()); 
		                        	}
			                     }
			                    else if (cellNum == 1) {
			                    	if(type == XSSFCell.CELL_TYPE_STRING ){
			                    	myList.add(cell.getRichStringCellValue());
			                    	}
			                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
			                    	myList.add(cell.getNumericCellValue()); 
			                    	}
			                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
		                        		myList.add(cell.getNumericCellValue()); 
		                        	}
			                    }  
			                    else if (cellNum == 2) {
			                    	if(type == XSSFCell.CELL_TYPE_STRING){
			                    		myList.add(cell.getRichStringCellValue()); 
			                    	}
			                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
			                    		myList.add(cell.getNumericCellValue()); 
			                    	}
			                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
		                        		myList.add(cell.getNumericCellValue()); 
		                        	}
			                    }
			                    else if(cellNum == 3){
		                        	if(type == XSSFCell.CELL_TYPE_STRING){
		                        		myList.add(cell.getRichStringCellValue()); 
		                        	}
		                        	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
		                        		myList.add(cell.getNumericCellValue()); 
		                        	}
		                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
		                        		myList.add(cell.getNumericCellValue()); 
		                        	}
		                        }
			                                 
			                }  
			               		RateGrid rateGrid= new RateGrid();
			        			rateGrid.setCorpID(sessionCorpID);
			        			rateGrid.setCreatedBy(userName);
			        			rateGrid.setCreatedOn(new Date());
			        			rateGrid.setUpdatedBy(userName);
			        			rateGrid.setUpdatedOn(new Date());
			        			rateGrid.setCharge(charge);
			        			rateGrid.setContractName(contractName);
			        			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
			        			rateGrid.setQuantity1(Double.parseDouble((myList.get(1).toString())));
			        			}else{
			        				rateGrid.setQuantity1(0);	
			        			}
			        			String rate="0";
			        			Double rateNumaric= new Double("0");
			        			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
			        				rate=myList.get(2).toString();
			        				try{
			        				rateNumaric=Double.parseDouble(myList.get(2).toString());
			        				DecimalFormat df = new DecimalFormat("0.0000");
			        				rate = df.format(rateNumaric);
			        				}catch(Exception e){
			        					
			        				}
			        				rateGrid.setRate1(rate);
			            			}else{
			            				rateGrid.setRate1("0");	
			            		}
			        			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
			            			rateGrid.setQuantity2(Double.parseDouble((myList.get(0).toString())));
			            			}else{
			            				rateGrid.setQuantity2(0);	
			            		} 
			            		String buyRate="0";
		            			Double buyRateNumaric= new Double("0");
		            			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
		            				buyRate=myList.get(3).toString();
		            				try{
		            				buyRateNumaric=Double.parseDouble(myList.get(3).toString());
		            				DecimalFormat df = new DecimalFormat("0.0000");
		            				buyRate = df.format(buyRateNumaric);
		            				}catch(Exception e){
		            					
		            				}
		            				rateGrid.setBuyRate(buyRate);
		                		}else{
		                			rateGrid.setBuyRate("0");	
		                		}                        
			        				        			
			        			rateGrid= rateGridManager.save(rateGrid); 
			        			record++;
			                	
			                   }
			                   catch (Exception e) {
			    	            e.printStackTrace();
			    	        } 
					       } 
			
			               try {
								fis.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
			            
			        }catch (Exception e) {
			            e.printStackTrace();
			        }
        }
        totRecords=record+" Record(s) saved successfully.";
		return totRecords;
	
	}
	public String saveDelRateGridData(String sessionCorpID, String file,String userName, String charge, String contractName, String tempIds,String fileExt){
		
		FileInputStream fis = null;
        int record=0;
        String totRecords=null;
        if(fileExt.equalsIgnoreCase("xls")){
        try{
        	List myList = new ArrayList(); 
			fis = new FileInputStream(file);
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            HSSFSheet sheet = workbook.getSheetAt(0);
            Iterator rows = sheet.rowIterator();
            while (rows.hasNext()) {
            	try {
                HSSFRow row = (HSSFRow) rows.next();
                if(row.getRowNum()==0){
            	  continue;
            	}
                Iterator cells = row.cellIterator();
                myList.clear();
                while (cells.hasNext()) {
                	HSSFCell cell = (HSSFCell) cells.next();
                    int type = cell.getCellType();
                    int cellNum = cell.getCellNum();
                    
                    if (cellNum == 0) {
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    }
                    else if (cellNum == 1) {
                    	if(type == HSSFCell.CELL_TYPE_STRING ){
                    	myList.add(cell.getRichStringCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    	myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    }  
                    else if (cellNum == 2) {
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    } else if(cellNum == 3){
                    	if(type == HSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    }  
                                 
                }  
               		RateGrid rateGrid= new RateGrid();
        			rateGrid.setCorpID(sessionCorpID);
        			rateGrid.setCreatedBy(userName);
        			rateGrid.setCreatedOn(new Date());
        			rateGrid.setUpdatedBy(userName);
        			rateGrid.setUpdatedOn(new Date());
        			rateGrid.setCharge(charge);
        			rateGrid.setContractName(contractName);
        			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
        			rateGrid.setQuantity1(Double.parseDouble((myList.get(1).toString())));
        			}else{
        				rateGrid.setQuantity1(0);	
        			}
        			String rate="0";
        			Double rateNumaric= new Double("0");
        			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
        				rate=myList.get(2).toString();
        				try{
        				rateNumaric=Double.parseDouble(myList.get(2).toString());
        				DecimalFormat df = new DecimalFormat("0.0000");
        				rate = df.format(rateNumaric);
        				}catch(Exception e){
        					
        				}
        				rateGrid.setRate1(rate);
            			}else{
            				rateGrid.setRate1("0");	
            		}
        			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
            			rateGrid.setQuantity2(Double.parseDouble((myList.get(0).toString())));
            			}else{
            				rateGrid.setQuantity2(0);	
            		}
        			String buyRate="0";
        			Double buyRateNumaric= new Double("0");
        			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
        				buyRate=myList.get(3).toString();
        				try{
        				buyRateNumaric=Double.parseDouble(myList.get(3).toString());
        				DecimalFormat df = new DecimalFormat("0.0000");
        				buyRate = df.format(buyRateNumaric);
        				}catch(Exception e){
        					
        				}
        				rateGrid.setBuyRate(buyRate);
            		}else{
            			rateGrid.setBuyRate("0");	
            		}  
        				        			
        			rateGrid= rateGridManager.save(rateGrid); 
        			record++;
                	
                   }
                   catch (Exception e) {
    	            e.printStackTrace();
    	        } 
		       } 

               try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
            
        }catch (Exception e) {
            e.printStackTrace();
        }
	}
        
    
      if(fileExt.equalsIgnoreCase("xlsx")){
		       try{
		        	List myList = new ArrayList(); 
					fis = new FileInputStream(file);
		           XSSFWorkbook workbook = new XSSFWorkbook(fis);
		            XSSFSheet sheet = workbook.getSheetAt(0);
		            Iterator rows = sheet.rowIterator();
		            while (rows.hasNext()) {
		            	try {
		                XSSFRow row = (XSSFRow) rows.next();
		                if(row.getRowNum()==0){
		            	  continue;
		            	}
		                Iterator cells = row.cellIterator();
		                myList.clear();
		                while (cells.hasNext()) {
		                	XSSFCell cell = (XSSFCell) cells.next();
		                    int type = cell.getCellType();
		                    int cellNum = cell.getColumnIndex();
		                    
		                    if (cellNum == 0) {
		                    	if(type == XSSFCell.CELL_TYPE_STRING){
		                    		myList.add(cell.getRichStringCellValue());
		                    	}
		                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
		                    		myList.add(cell.getNumericCellValue());
		                    	}
		                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
	                        		myList.add(cell.getNumericCellValue()); 
	                        	}
		                    }
		                    else if (cellNum == 1) {
		                    	if(type == XSSFCell.CELL_TYPE_STRING ){
		                    	myList.add(cell.getRichStringCellValue());
		                    	}
		                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
		                    	myList.add(cell.getNumericCellValue()); 
		                    	}
		                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
	                        		myList.add(cell.getNumericCellValue()); 
	                        	}
		                    }  
		                    else if (cellNum == 2) {
		                    	if(type == XSSFCell.CELL_TYPE_STRING){
		                    		myList.add(cell.getRichStringCellValue()); 
		                    	}
		                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
		                    		myList.add(cell.getNumericCellValue()); 
		                    	}
		                    	else if(type == HSSFCell.CELL_TYPE_FORMULA){
	                        		myList.add(cell.getNumericCellValue()); 
	                        	}
		                   }else if(cellNum == 3){
	                        	if(type == XSSFCell.CELL_TYPE_STRING){
	                        		myList.add(cell.getRichStringCellValue()); 
	                        	}
	                        	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
	                        		myList.add(cell.getNumericCellValue()); 
	                        	}
	                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
	                        		myList.add(cell.getNumericCellValue()); 
	                        	}
	                        }   
		                                 
		                }  
		               		RateGrid rateGrid= new RateGrid();
		        			rateGrid.setCorpID(sessionCorpID);
		        			rateGrid.setCreatedBy(userName);
		        			rateGrid.setCreatedOn(new Date());
		        			rateGrid.setUpdatedBy(userName);
		        			rateGrid.setUpdatedOn(new Date());
		        			rateGrid.setCharge(charge);
		        			rateGrid.setContractName(contractName);
		        			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
		        			rateGrid.setQuantity1(Double.parseDouble((myList.get(1).toString())));
		        			}else{
		        				rateGrid.setQuantity1(0);	
		        			}
		        			String rate="0";
		        			Double rateNumaric= new Double("0");
		        			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
		        				rate=myList.get(2).toString();
		        				try{
		        				rateNumaric=Double.parseDouble(myList.get(2).toString());
		        				DecimalFormat df = new DecimalFormat("0.0000");
		        				rate = df.format(rateNumaric);
		        				}catch(Exception e){
		        					
		        				}
		        				rateGrid.setRate1(rate);
		            			}else{
		            				rateGrid.setRate1("0");	
		            		}
		        			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
		            			rateGrid.setQuantity2(Double.parseDouble((myList.get(0).toString())));
		            			}else{
		            				rateGrid.setQuantity2(0);	
		            		} 
		        			String buyRate="0";
		        			Double buyRateNumaric= new Double("0");
		        			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
		        				buyRate=myList.get(3).toString();
		        				try{
		        				buyRateNumaric=Double.parseDouble(myList.get(3).toString());
		        				DecimalFormat df = new DecimalFormat("0.0000");
		        				buyRate = df.format(buyRateNumaric);
		        				}catch(Exception e){
		        					
		        				}
		        				rateGrid.setBuyRate(buyRate);
		            		}else{
		            			rateGrid.setBuyRate("0");	
		            		}
		        				        			
		        			rateGrid= rateGridManager.save(rateGrid); 
		        			record++;
		                	
		                   }
		                   catch (Exception e) {
		    	            e.printStackTrace();
		    	        } 
				       } 
		
		               try {
							fis.close();
						} catch (IOException e) {
							e.printStackTrace();
						}
		            
		        }catch (Exception e) {
		            e.printStackTrace();
		        }
 }  
       
        
        totRecords=record+" Record(s) saved successfully.";
        if((tempIds!=null) && (!(tempIds.equalsIgnoreCase("")))  ){
        getHibernateTemplate().bulkUpdate("delete from RateGrid where id in("+tempIds+")");
        }
		return totRecords;
        }
	
	@SuppressWarnings("unchecked")
	public String saveXlsFileDefaultAccountline(String sessionCorpID, String file, String userName, String fileExt,Boolean costElementFlag) {
		FileInputStream fis = null;
        int record=0;
        String totRecords=null;
        if(fileExt.equalsIgnoreCase("xls")){
            try{
            	List myList = new ArrayList(); 
    			fis = new FileInputStream(file);
                HSSFWorkbook workbook = new HSSFWorkbook(fis);
                HSSFSheet sheet = workbook.getSheetAt(0);
                Iterator rows = sheet.rowIterator();
                int maxNumOfCells = sheet.getRow(0).getLastCellNum();
                while (rows.hasNext()) {
                	try {
                    HSSFRow row = (HSSFRow) rows.next();
                    if(row.getRowNum()==0){
                	  continue;
                	}
                    myList.clear();
                    for( int cellCounter = 0; cellCounter < maxNumOfCells; cellCounter ++){ // Loop through cells

                        HSSFCell cell;
                        if(row.getCell(0)!=null){
                        if( row.getCell(cellCounter ) == null ){
                            cell = row.createCell(cellCounter);
                        } else {
                            cell = row.getCell(cellCounter);
                        }
                        int type = cell.getCellType();
                        int cellNum = cell.getCellNum();
                        
                        if (cellNum == 0) {
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if (cellNum == 1) {
                        	if(type == HSSFCell.CELL_TYPE_STRING ){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        	myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }  
                        else if (cellNum == 2) {
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        } 
                        else if(cellNum == 3){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 4){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 5){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 6){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 7){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 8){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 9){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 10){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 11){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 12){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 13){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 14){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 15){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 16){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 17){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 18){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 19){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 20){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 21){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                        else if(cellNum == 22){
                        	if(type == HSSFCell.CELL_TYPE_STRING){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_NUMERIC){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_FORMULA){
                        		myList.add(cell.getNumericCellValue()); 
                        	}
                        	else if(type == HSSFCell.CELL_TYPE_BLANK){
                        		myList.add(cell.getStringCellValue()); 
                        	}
                        }
                    } } 
                    try{
                   		DefaultAccountLine defaultAccountLine= new DefaultAccountLine();
                   		defaultAccountLine.setCorpID(sessionCorpID);
                   		defaultAccountLine.setCreatedBy(userName);
                   		defaultAccountLine.setCreatedOn(new Date());
                   		defaultAccountLine.setUpdatedBy(userName);
                   		defaultAccountLine.setUpdatedOn(new Date());
                   		String jobType="";
            			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
            				jobType=myList.get(0).toString().trim();
            				defaultAccountLine.setJobType(jobType);
            			}else{
            				defaultAccountLine.setJobType("");	
            			}
            			String contract="";
            			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
            				contract=myList.get(1).toString().trim();
            				defaultAccountLine.setContract(contract);
            			}else{
            				defaultAccountLine.setContract("");	
            			}
            			String mode="";
            			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
            				mode=myList.get(2).toString().trim();
            				defaultAccountLine.setMode(mode);
            			}else{
            				defaultAccountLine.setMode("");	
            			}
            			String billToCode="";
            			String parseData[]=null;
            			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
            				billToCode=myList.get(3).toString();
            				try{
            				parseData=billToCode.split("\\.");
            				}catch(Exception e){
            					e.printStackTrace();
            				}
            				try{
            				if(parseData.length>0 && parseData!=null &&(!parseData[0].toString().equals(""))){
            					if(parseData[0]!=null)
            				defaultAccountLine.setBillToCode(parseData[0].toString());
            				}else{
            					defaultAccountLine.setBillToCode(billToCode);
            				}
            				}catch(Exception e){
            					e.printStackTrace();
            				}
            			}else{
            				defaultAccountLine.setBillToCode("");	
            			}
            			String route="";
            			if(myList.get(4)!=null && (!(myList.get(4).toString().equals("")))){
            				route=myList.get(4).toString().trim();
            				defaultAccountLine.setRoute(route);
            			}else{
            				defaultAccountLine.setRoute("");	
            			}
            			String packMode="";
            			if(myList.get(5)!=null && (!(myList.get(5).toString().equals("")))){
            				packMode=myList.get(5).toString().trim();
            				defaultAccountLine.setPackMode(packMode);
            			}else{
            				defaultAccountLine.setPackMode("");	
            			}
            			String commodity="";
            			if(myList.get(6)!=null && (!(myList.get(6).toString().equals("")))){
            				commodity=myList.get(6).toString().trim();
            				defaultAccountLine.setCommodity(commodity);
            			}else{
            				defaultAccountLine.setCommodity("");	
            			}
            			String service="";
            			if(myList.get(7)!=null && (!(myList.get(7).toString().equals("")))){
            				service=myList.get(7).toString().trim();
            				defaultAccountLine.setServiceType(service);
            			}else{
            				defaultAccountLine.setServiceType("");	
            			}
            			String companyDivision="";
            			if(myList.get(8)!=null && (!(myList.get(8).toString().equals("")))){
            				companyDivision=myList.get(8).toString().trim();
            				defaultAccountLine.setCompanyDivision(companyDivision);
            			}else{
            				defaultAccountLine.setCompanyDivision("");	
            			}
            			String equipMent="";
            			if(myList.get(9)!=null && (!(myList.get(9).toString().equals("")))){
            				equipMent=myList.get(9).toString().trim();
            				String []equipMentArr = equipMent.split("\\.");
            				if(equipMentArr[0]!=null && !equipMentArr[0].equalsIgnoreCase("")){
            					defaultAccountLine.setEquipment(equipMentArr[0]);
            				}else{
            					defaultAccountLine.setEquipment("");
            				}
            			}else{
            				defaultAccountLine.setEquipment("");	
            			}
            			String originCountry="";
            			if(myList.get(10)!=null && (!(myList.get(10).toString().equals("")))){
            				originCountry=myList.get(10).toString().trim();
            				defaultAccountLine.setOriginCountry(originCountry);
            			}else{
            				defaultAccountLine.setOriginCountry("");	
            			}
            			String destinationCountry="";
            			if(myList.get(11)!=null && (!(myList.get(11).toString().equals("")))){
            				destinationCountry=myList.get(11).toString().trim();
            				defaultAccountLine.setDestinationCountry(destinationCountry);
            			}else{
            				defaultAccountLine.setDestinationCountry("");	
            			}
            			
            			if(myList.get(12)!=null && (!(myList.get(12).toString().equals("")))){
            				String orderNum=myList.get(12).toString();
            				String []orderNumArr = orderNum.split("\\.");
            				if(orderNumArr[0]!=null && !orderNumArr[0].equalsIgnoreCase("")){
            					defaultAccountLine.setOrderNumber(Integer.parseInt(orderNumArr[0]));
            				}else{
            					defaultAccountLine.setOrderNumber(0);
            				}
            			}else{
            				defaultAccountLine.setOrderNumber(0);	
            			}
            			String categories="";
            			if(myList.get(13)!=null && (!(myList.get(13).toString().equals("")))){
            				categories=myList.get(13).toString().trim();
            				defaultAccountLine.setCategories(categories);
            			}else{
            				defaultAccountLine.setCategories("");	
            			}
            			String basis="";
            			if(myList.get(14)!=null && (!(myList.get(14).toString().equals("")))){
            				basis=myList.get(14).toString().trim();
            				defaultAccountLine.setBasis(basis);
            			}else{
            				defaultAccountLine.setBasis("");	
            			}
            			String vendorCode="";
            			String[] parseData2=null;
            			if(myList.get(15)!=null && (!(myList.get(15).toString().equals("")))){
            				vendorCode=myList.get(15).toString();
            				try{
            					parseData2=vendorCode.split("\\.");
            				}catch(Exception e){
            					e.printStackTrace();
            				}
            				try{
            				if(parseData2.length>0 && parseData2!=null &&(!parseData2[0].toString().equals(""))){
            					if(parseData2[0]!=null)
            				defaultAccountLine.setVendorCode(parseData2[0].toString());
            				}else{
            					defaultAccountLine.setVendorCode(vendorCode);
            				}
            				}catch(Exception e){
            					e.printStackTrace();
            				}
            			}else{
            				defaultAccountLine.setVendorCode("");	
            			}
            			String chargeCode="";
            			if(myList.get(16)!=null && (!(myList.get(16).toString().equals("")))){
            				chargeCode=myList.get(16).toString();
            				defaultAccountLine.setChargeCode(chargeCode);
            			}else{
            				defaultAccountLine.setChargeCode("");	
            			}
            			
            			Double quantity= new Double("0");
            			if(myList.get(17)!=null && (!(myList.get(17).toString().equals("")))){
            				try{
            					quantity=Double.parseDouble(myList.get(17).toString());
            				/*DecimalFormat df = new DecimalFormat("0.00");
            				rateNumaric = df.format(rateNumaric);*/
            				}catch(Exception e){
            					
            				}
            				defaultAccountLine.setQuantity(quantity);
                			}else{
                				defaultAccountLine.setQuantity(0.0);	
                		}
            			String estVatDesc="";
            			if(myList.get(18)!=null && (!(myList.get(18).toString().equals("")))){
            				estVatDesc=myList.get(18).toString().trim();
            				defaultAccountLine.setEstVatDescr(estVatDesc);
            			}else{
            				defaultAccountLine.setEstVatDescr("");	
            			}
            			
            			BigDecimal sellRate= new BigDecimal("0");
            			if(myList.get(19)!=null && (!(myList.get(19).toString().equals("")))){
            				try{
            					sellRate=BigDecimal.valueOf(Double.parseDouble(myList.get(19).toString()));
            				}catch(Exception e){
            					
            				}
            				defaultAccountLine.setSellRate(sellRate);
                			}else{
                				defaultAccountLine.setSellRate(sellRate);	
                		}
            			
            			Integer markUp= new Integer("0");
            			if(myList.get(20)!=null && (!(myList.get(20).toString().equals("")))){
            				try{
            					String []markUpArr = myList.get(20).toString().split("\\.");
            					if(markUpArr[0]!=null && !markUpArr[0].equalsIgnoreCase("")){
            						markUp=Integer.parseInt(markUpArr[0]);
            					}
            				}catch(Exception e){
            					
            				}
            				defaultAccountLine.setMarkUp(markUp);
                			}else{
                				defaultAccountLine.setMarkUp(0);	
                		}
            			
            			String estExpVatDesc="";
            			if(myList.get(21)!=null && (!(myList.get(21).toString().equals("")))){
            				estExpVatDesc=myList.get(21).toString().trim();
            				defaultAccountLine.setEstExpVatDescr(estExpVatDesc);
            			}else{
            				defaultAccountLine.setEstExpVatDescr("");	
            			}
            			
            			Double rate= new Double("0");
            			if(myList.get(22)!=null && (!(myList.get(22).toString().equals("")))){
            				try{
            					rate=Double.parseDouble(myList.get(22).toString());
            				}catch(Exception e){
            					
            				}
            				defaultAccountLine.setRate(rate);
                			}else{
                				defaultAccountLine.setRate(0.0); 	
                		}
            			defaultAccountLine.setAmount(0.0);
            			defaultAccountLine.setEstimatedRevenue(sellRate);
            			defaultAccountLine.setEstVatAmt(sellRate);
            			defaultAccountLine.setUploaddataflag(true);
            			defaultAccountLine= defaultAccountLineManager.save(defaultAccountLine);
            			record++;
                    }catch(Exception et){
                    	et.printStackTrace();
        	            String logMessage =  "Exception:"+ et.toString()+" at #"+et.getStackTrace()[0].toString();
        	            System.out.println("Exception--"+logMessage);
                    }
                       }
                       catch (Exception e) {
        	            e.printStackTrace();
        	            String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
        	            System.out.println("Exception--"+logMessage);
        	        }
    		       } 

                   try {
    					fis.close();
    				} catch (IOException e) {
    					e.printStackTrace();
    				}
                
            }catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(fileExt.equalsIgnoreCase("xlsx")){
        try{
        	List myList = new ArrayList(); 
			fis = new FileInputStream(file);
           XSSFWorkbook workbook = new XSSFWorkbook(fis);
            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator rows = sheet.iterator();
            int maxNumOfCells = sheet.getRow(0).getLastCellNum();
            while (rows.hasNext()) {
            	try {
                XSSFRow row = (XSSFRow) rows.next();
                if(row.getRowNum()==0){
            	  continue;
            	}
                myList.clear();
                for( int cellCounter = 0; cellCounter < maxNumOfCells; cellCounter ++){ // Loop through cells
                	
                	XSSFCell cell;
                	if(row.getCell(0)!=null){
                	if( row.getCell(cellCounter ) == null ){
                        cell = row.createCell(cellCounter);
                    } else {
                        cell = row.getCell(cellCounter);
                    }
                    int type = cell.getCellType();
                    int cellNum = cell.getColumnIndex();
                    
                    if (cellNum == 0) {
                    	if(type == XSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_BLANK){
                    		myList.add(cell.getStringCellValue()); 
                    	}
                    }
                    else if (cellNum == 1) {
                    	if(type == XSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_BLANK){
                    		myList.add(cell.getStringCellValue()); 
                    	}
                    }  
                    else if (cellNum == 2) {
                    	if(type == XSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_BLANK){
                    		myList.add(cell.getStringCellValue()); 
                    	}
                   }else if(cellNum == 3){
                	   if(type == XSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_BLANK){
                    		myList.add(cell.getStringCellValue()); 
                    	}
                 }else if(cellNum == 4){
                	 	if(type == XSSFCell.CELL_TYPE_STRING){
                    		myList.add(cell.getRichStringCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                    		myList.add(cell.getNumericCellValue());
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                    		myList.add(cell.getNumericCellValue()); 
                    	}
                    	else if(type == XSSFCell.CELL_TYPE_BLANK){
                    		myList.add(cell.getStringCellValue()); 
                    	}
                }
                else if(cellNum == 5){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 6){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 7){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 8){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 9){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 10){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 11){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 12){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 13){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 14){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 15){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 16){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 17){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 18){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 19){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 20){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 21){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
                else if(cellNum == 22){
                	if(type == XSSFCell.CELL_TYPE_STRING){
                		myList.add(cell.getRichStringCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_NUMERIC){
                		myList.add(cell.getNumericCellValue());
                	}
                	else if(type == XSSFCell.CELL_TYPE_FORMULA){
                		myList.add(cell.getNumericCellValue()); 
                	}
                	else if(type == XSSFCell.CELL_TYPE_BLANK){
                		myList.add(cell.getStringCellValue()); 
                	}
                }
            } } 
            try{
           		DefaultAccountLine defaultAccountLine= new DefaultAccountLine();
           		defaultAccountLine.setCorpID(sessionCorpID);
           		defaultAccountLine.setCreatedBy(userName);
           		defaultAccountLine.setCreatedOn(new Date());
           		defaultAccountLine.setUpdatedBy(userName);
           		defaultAccountLine.setUpdatedOn(new Date());
           		String jobType="";
    			if(myList.get(0)!=null && (!(myList.get(0).toString().equals("")))){
    				jobType=myList.get(0).toString().trim();
    				defaultAccountLine.setJobType(jobType);
    			}else{
    				defaultAccountLine.setJobType("");	
    			}
    			String contract="";
    			if(myList.get(1)!=null && (!(myList.get(1).toString().equals("")))){
    				contract=myList.get(1).toString().trim();
    				defaultAccountLine.setContract(contract);
    			}else{
    				defaultAccountLine.setContract("");	
    			}
    			String mode="";
    			if(myList.get(2)!=null && (!(myList.get(2).toString().equals("")))){
    				mode=myList.get(2).toString().trim();
    				defaultAccountLine.setMode(mode);
    			}else{
    				defaultAccountLine.setMode("");	
    			}
    			String billToCode="";
    			String parseData[]=null;
    			if(myList.get(3)!=null && (!(myList.get(3).toString().equals("")))){
    				billToCode=myList.get(3).toString();
    				try{
    				parseData=billToCode.split("\\.");
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    				try{
    				if(parseData.length>0 && parseData!=null &&(!parseData[0].toString().equals(""))){
    					if(parseData[0]!=null)
    				defaultAccountLine.setBillToCode(parseData[0].toString());
    				}else{
    					defaultAccountLine.setBillToCode(billToCode);
    				}
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    			}else{
    				defaultAccountLine.setBillToCode("");	
    			}
    			String route="";
    			if(myList.get(4)!=null && (!(myList.get(4).toString().equals("")))){
    				route=myList.get(4).toString().trim();
    				defaultAccountLine.setRoute(route);
    			}else{
    				defaultAccountLine.setRoute("");	
    			}
    			String packMode="";
    			if(myList.get(5)!=null && (!(myList.get(5).toString().equals("")))){
    				packMode=myList.get(5).toString().trim();
    				defaultAccountLine.setPackMode(packMode);
    			}else{
    				defaultAccountLine.setPackMode("");	
    			}
    			String commodity="";
    			if(myList.get(6)!=null && (!(myList.get(6).toString().equals("")))){
    				commodity=myList.get(6).toString().trim();
    				defaultAccountLine.setCommodity(commodity);
    			}else{
    				defaultAccountLine.setCommodity("");	
    			}
    			String service="";
    			if(myList.get(7)!=null && (!(myList.get(7).toString().equals("")))){
    				service=myList.get(7).toString().trim();
    				defaultAccountLine.setServiceType(service);
    			}else{
    				defaultAccountLine.setServiceType("");	
    			}
    			String companyDivision="";
    			if(myList.get(8)!=null && (!(myList.get(8).toString().equals("")))){
    				companyDivision=myList.get(8).toString().trim();
    				defaultAccountLine.setCompanyDivision(companyDivision);
    			}else{
    				defaultAccountLine.setCompanyDivision("");	
    			}
    			String equipMent="";
    			if(myList.get(9)!=null && (!(myList.get(9).toString().equals("")))){
    				equipMent=myList.get(9).toString().trim();
    				defaultAccountLine.setEquipment(equipMent);
    			}else{
    				defaultAccountLine.setEquipment("");	
    			}
    			String originCountry="";
    			if(myList.get(10)!=null && (!(myList.get(10).toString().equals("")))){
    				originCountry=myList.get(10).toString().trim();
    				defaultAccountLine.setOriginCountry(originCountry);
    			}else{
    				defaultAccountLine.setOriginCountry("");	
    			}
    			String destinationCountry="";
    			if(myList.get(11)!=null && (!(myList.get(11).toString().equals("")))){
    				destinationCountry=myList.get(11).toString().trim();
    				defaultAccountLine.setDestinationCountry(destinationCountry);
    			}else{
    				defaultAccountLine.setDestinationCountry("");	
    			}
    		
    			if(myList.get(12)!=null && (!(myList.get(12).toString().equals("")))){
    				String orderNum=myList.get(12).toString();
    				String []orderNumArr = orderNum.split("\\.");
    				if(orderNumArr[0]!=null && !orderNumArr[0].equalsIgnoreCase("")){
    					defaultAccountLine.setOrderNumber(Integer.parseInt(orderNumArr[0]));
    				}else{
    					defaultAccountLine.setOrderNumber(0);
    				}
    			}else{
    				defaultAccountLine.setOrderNumber(0);	
    			}
    			String categories="";
    			if(myList.get(13)!=null && (!(myList.get(13).toString().equals("")))){
    				categories=myList.get(13).toString().trim();
    				defaultAccountLine.setCategories(categories);
    			}else{
    				defaultAccountLine.setCategories("");	
    			}
    			String basis="";
    			if(myList.get(14)!=null && (!(myList.get(14).toString().equals("")))){
    				basis=myList.get(14).toString().trim();
    				defaultAccountLine.setBasis(basis);
    			}else{
    				defaultAccountLine.setBasis("");	
    			}
    			String vendorCode="";
    			String[] parseData1=null;
    			if(myList.get(15)!=null && (!(myList.get(15).toString().equals("")))){
    				vendorCode=myList.get(15).toString();
    				try{
    					parseData1=vendorCode.split("\\.");
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    				try{
    				if(parseData1.length>0 && parseData1!=null &&(!parseData1[0].toString().equals(""))){
    					if(parseData1[0]!=null)
    				defaultAccountLine.setVendorCode(parseData1[0].toString());
    				}else{
    					defaultAccountLine.setVendorCode(vendorCode);
    				}
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    			}else{
    				defaultAccountLine.setVendorCode("");	
    			}
    			String chargeCode="";
    			if(myList.get(16)!=null && (!(myList.get(16).toString().equals("")))){
    				chargeCode=myList.get(16).toString();
    				defaultAccountLine.setChargeCode(chargeCode);
    			}else{
    				defaultAccountLine.setChargeCode("");	
    			}
    			
    			Double quantity= new Double("0");
    			if(myList.get(17)!=null && (!(myList.get(17).toString().equals("")))){
    				try{
    					quantity=Double.parseDouble(myList.get(17).toString());
    				/*DecimalFormat df = new DecimalFormat("0.00");
    				rateNumaric = df.format(rateNumaric);*/
    				}catch(Exception e){
    					
    				}
    				defaultAccountLine.setQuantity(quantity);
        			}else{
        				defaultAccountLine.setQuantity(0.0);	
        		}
    			String estVatDesc="";
    			if(myList.get(18)!=null && (!(myList.get(18).toString().equals("")))){
    				estVatDesc=myList.get(18).toString().trim();
    				defaultAccountLine.setEstVatDescr(estVatDesc);
    			}else{
    				defaultAccountLine.setEstVatDescr("");	
    			}
    			
    			BigDecimal sellRate= new BigDecimal("0");
    			if(myList.get(19)!=null && (!(myList.get(19).toString().equals("")))){
    				try{
    					sellRate=BigDecimal.valueOf(Double.parseDouble(myList.get(19).toString()));
    				}catch(Exception e){
    					
    				}
    				defaultAccountLine.setSellRate(sellRate);
        			}else{
        				defaultAccountLine.setSellRate(sellRate);	
        		}
    			
    			Integer markUp= new Integer("0");
    			if(myList.get(20)!=null && (!(myList.get(20).toString().equals("")))){
    				try{
    					markUp=Integer.parseInt(myList.get(20).toString());
    				}catch(Exception e){
    					
    				}
    				defaultAccountLine.setMarkUp(markUp);
        			}else{
        				defaultAccountLine.setMarkUp(0);	
        		}
    			
    			String estExpVatDesc="";
    			if(myList.get(21)!=null && (!(myList.get(21).toString().equals("")))){
    				estExpVatDesc=myList.get(21).toString().trim();
    				defaultAccountLine.setEstExpVatDescr(estExpVatDesc);
    			}else{
    				defaultAccountLine.setEstExpVatDescr("");	
    			}
    			
    			Double rate= new Double("0");
    			if(myList.get(22)!=null && (!(myList.get(22).toString().equals("")))){
    				try{
    					rate=Double.parseDouble(myList.get(22).toString());
    				}catch(Exception e){
    					
    				}
    				defaultAccountLine.setRate(rate);
        			}else{
        				defaultAccountLine.setRate(0.0);	
        		}
    			defaultAccountLine.setAmount(0.0);
    			defaultAccountLine.setEstimatedRevenue(sellRate);
    			defaultAccountLine.setEstVatAmt(sellRate);
    			defaultAccountLine.setUploaddataflag(true);
    			defaultAccountLine= defaultAccountLineManager.save(defaultAccountLine); 
        		record++;
               }
                   catch (Exception e) {
    	            e.printStackTrace();
    	        } 
		        

               try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
            
        }catch (Exception e) {
            e.printStackTrace();
        }
       }
            
    }catch (Exception e) {
        e.printStackTrace();
    }
        }
   
   
        totRecords=record+" Record(s) saved successfully.";
		return totRecords;
	
	}
	
	public void updateDefaultAccountlinethroughProcedure(String sessionCorpID,Boolean costElementFlag,String userName){
		
		int i=0;
		 try {
			 Query q = this.getSession().createSQLQuery("{ call UploadFunctionalityAccountingTemplate(?,?,?) }");
			 q.setParameter(0,sessionCorpID);
			 q.setParameter(1,costElementFlag);
			 q.setParameter(2,userName);
			 i=q.executeUpdate();
			 System.out.println("Update Procedure"+i);
		 } catch (Exception e) {
				e.printStackTrace();
		}
		
	}
	 
	
	 public void setRateGridManager(RateGridManager rateGridManager) {
			this.rateGridManager = rateGridManager;
		}

	public void setHibernateUtil(HibernateUtil hibernateUtil) {
		this.hibernateUtil = hibernateUtil;
	}

	public void setDefaultAccountLineManager(
			DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}
	
	
}
