package com.trilasoft.app.service.impl;

import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.GenericSurveyAnswerDao;
import com.trilasoft.app.model.GenericSurveyAnswer;
import com.trilasoft.app.service.GenericSurveyAnswerManager;


public class GenericSurveyAnswerManagerImpl extends GenericManagerImpl<GenericSurveyAnswer, Long> implements GenericSurveyAnswerManager{
	
	GenericSurveyAnswerDao genericSurveyAnswerDao;
	public GenericSurveyAnswerManagerImpl(GenericSurveyAnswerDao genericSurveyAnswerDao) {
		super(genericSurveyAnswerDao);
		this.genericSurveyAnswerDao = genericSurveyAnswerDao;
	}

	public List getSurveyAnswerList(Long parentId, String sessionCorpID){
		return genericSurveyAnswerDao.getSurveyAnswerList(parentId, sessionCorpID);
	}
}
