delimiter $$

CREATE trigger redsky.trigger_add_history_rolebased_comp_permission
BEFORE UPDATE on redsky.rolebased_comp_permission
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;




   IF (OLD.mask <> NEW.mask ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.mask~') into fieldNameList;
      select CONCAT(oldValueList,OLD.mask,'~') into oldValueList;
      select CONCAT(newValueList,NEW.mask,'~') into newValueList;
  END IF;









   IF (OLD.corpid <> NEW.corpid ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.corpid~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpid,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpid,'~') into newValueList;
  END IF;




   IF (OLD.componentId <> NEW.componentId ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.componentId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.componentId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.componentId,'~') into newValueList;
  END IF;




   IF (OLD.role <> NEW.role ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.role~') into fieldNameList;
      select CONCAT(oldValueList,OLD.role,'~') into oldValueList;
      select CONCAT(newValueList,NEW.role,'~') into newValueList;
  END IF;




   IF (OLD.url <> NEW.url ) THEN
      select CONCAT(fieldNameList,'rolebased_comp_permission.url~') into fieldNameList;
      select CONCAT(oldValueList,OLD.url,'~') into oldValueList;
      select CONCAT(newValueList,NEW.url,'~') into newValueList;
  END IF;


 


  CALL add_tblHistory (OLD.id,"rolebased_comp_permission", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;