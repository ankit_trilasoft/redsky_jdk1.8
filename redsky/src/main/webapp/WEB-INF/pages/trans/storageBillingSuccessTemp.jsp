<%@ include file="/common/taglibs.jsp"%> 

<head> 

<title><fmt:message key="executeRules.title"/></title> 
<meta name="heading" content="<fmt:message key='executeRules.heading'/>"/>


</head> 

<script language="javascript" type="text/javascript">
var p_val = opener.document.storageBillingForm.storageErrorFlagValue.value;
if(p_val == 'Error'){
alert("Cannot Invoice as preview still has outstanding issues which have not been fixed, please fix them before invoicing.");
self.close();

}
</script>
<s:form id="storageBillingSuccess" name="storageBillingSuccess" action="" method="post" validate="true">   
 <div id="Layer1" align="center" style="width:100%; margin: -5px; padding: 0px; background-color:#678EB6; background-image: url('images/bg_storage.png'); size:10px; background-repeat: no-repeat; ">
<table class='searchLabel' cellspacing="0" cellpadding="0" border="0">

<tbody>
<tr><td height="10px"></td></tr>
	<tr>
      <td align="center" class="listwhitetext" style="font: 22px bold ;">Your job has been scheduled successfully.</td>
     </tr>
    <tr>
      <td align="center" class="listwhitetext" style="font:22px bold ; ">Please wait for invoicing to start, once it starts </td>
     </tr>
     <tr>
      <td align="center" class="listwhitetext" style="font:22px bold ; ">then a progress bar will be displayed.</td>
     </tr>
     <tr><td height="15px"></td></tr>
     <tr>
     <td align="center" style="margin: 0px; padding: 0px">
     <iframe src ="previewSuccess.html?ajax=1&decorator=simple&popup=true"  WIDTH=450 HEIGHT=200 FRAMEBORDER=0>
	  <p>Your browser does not support iframes.</p>
	</iframe>
	</td>
	</tr>
     <TR>
     	<td align="right" style="padding-right: 10px; padding-bottom: 5px;">
     		<a href="javascript:window.close()">Close</a>
     	</td>
     </TR>
</tbody></table>
</div>      
</s:form>
<script language="javascript" type="text/javascript">setTimeout("location.reload();",12000);</script>


