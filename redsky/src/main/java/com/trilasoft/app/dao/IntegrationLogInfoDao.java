package com.trilasoft.app.dao;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.IntegrationLogInfo;

public interface IntegrationLogInfoDao extends GenericDao<IntegrationLogInfo, Long>{
	public String findSendMoveCloudStatus(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudStatusCrew(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudStatusClaim(String sessionCorpID ,String fileNumber);
	public String findSendMoveCloudGlobalStatus(String sessionCorpID ,String fileNumber,String taskType);
}
