
DELIMITER $$
create trigger redsky.trigger_add_history_notes BEFORE UPDATE on redsky.notes
for each row BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
 
   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
   
   IF (OLD.note <> NEW.note ) THEN
       select CONCAT(fieldNameList,'notes.note~') into fieldNameList;
       select CONCAT(oldValueList,OLD.note,'~') into oldValueList;
       select CONCAT(newValueList,NEW.note,'~') into newValueList;
   END IF;

   IF (OLD.forwardDate <> NEW.forwardDate ) THEN
       select CONCAT(fieldNameList,'notes.forwardDate~') into fieldNameList;
       IF(OLD.forwardDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.forwardDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.forwardDate is not null) THEN
        	select CONCAT(oldValueList,OLD.forwardDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.forwardDate is not null) THEN
        	select CONCAT(newValueList,NEW.forwardDate,'~') into newValueList;
      	END IF;
   END IF;

   IF (OLD.forwardStatus <> NEW.forwardStatus ) THEN
       select CONCAT(fieldNameList,'notes.forwardStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.forwardStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.forwardStatus,'~') into newValueList;
   END IF;

  IF (OLD.forwardToUser <> NEW.forwardToUser ) THEN
       select CONCAT(fieldNameList,'notes.forwardToUser~') into fieldNameList;
       select CONCAT(oldValueList,OLD.forwardToUser,'~') into oldValueList;
       select CONCAT(newValueList,NEW.forwardToUser,'~') into newValueList;
   END IF;

   IF (OLD.noteSubType <> NEW.noteSubType ) THEN
       select CONCAT(fieldNameList,'notes.noteSubType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.noteSubType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.noteSubType,'~') into newValueList;
   END IF;

   IF (OLD.noteType <> NEW.noteType ) THEN
       select CONCAT(fieldNameList,'notes.noteType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.noteType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.noteType,'~') into newValueList;
   END IF;

   IF (OLD.subject <> NEW.subject ) THEN
       select CONCAT(fieldNameList,'notes.subject~') into fieldNameList;
       select CONCAT(oldValueList,OLD.subject,'~') into oldValueList;
       select CONCAT(newValueList,NEW.subject,'~') into newValueList;
   END IF;

   IF (OLD.noteStatus <> NEW.noteStatus ) THEN
       select CONCAT(fieldNameList,'notes.noteStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.noteStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.noteStatus,'~') into newValueList;
   END IF;

   IF (OLD.remindTime <> NEW.remindTime ) THEN
       select CONCAT(fieldNameList,'notes.remindTime~') into fieldNameList;
       select CONCAT(oldValueList,OLD.remindTime,'~') into oldValueList;
       select CONCAT(newValueList,NEW.remindTime,'~') into newValueList;
   END IF;

    IF (OLD.reminderStatus <> NEW.reminderStatus ) THEN
       select CONCAT(fieldNameList,'notes.reminderStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.reminderStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.reminderStatus,'~') into newValueList;
   END IF;
   
   IF (OLD.followUpFor <> NEW.followUpFor) THEN
       select CONCAT(fieldNameList,'notes.followUpFor~') into fieldNameList;
       select CONCAT(oldValueList,OLD.followUpFor,'~') into oldValueList;
       select CONCAT(newValueList,NEW.followUpFor,'~') into newValueList;
   END IF;
   
   IF (OLD.customerNumber <> NEW.customerNumber ) THEN
       select CONCAT(fieldNameList,'notes.customerNumber~') into fieldNameList;
       select CONCAT(oldValueList,OLD.customerNumber,'~') into oldValueList;
       select CONCAT(newValueList,NEW.customerNumber,'~') into newValueList;
   END IF;
   
   IF (OLD.remindInterval <> NEW.remindInterval ) THEN
       select CONCAT(fieldNameList,'notes.remindInterval~') into fieldNameList;
       select CONCAT(oldValueList,OLD.remindInterval,'~') into oldValueList;
       select CONCAT(newValueList,NEW.remindInterval,'~') into newValueList;
   END IF;
   
   IF (OLD.noteGroup <> NEW.noteGroup ) THEN
       select CONCAT(fieldNameList,'notes.noteGroup~') into fieldNameList;
       select CONCAT(oldValueList,OLD.noteGroup,'~') into oldValueList;
       select CONCAT(newValueList,NEW.noteGroup,'~') into newValueList;
   END IF;
   
    IF (OLD.notesActivity <> NEW.notesActivity ) THEN
       select CONCAT(fieldNameList,'notes.notesActivity~') into fieldNameList;
       select CONCAT(oldValueList,OLD.notesActivity,'~') into oldValueList;
       select CONCAT(newValueList,NEW.notesActivity,'~') into newValueList;
   END IF;
   
   IF (OLD.name <> NEW.name ) THEN
       select CONCAT(fieldNameList,'notes.name~') into fieldNameList;
       select CONCAT(oldValueList,OLD.name,'~') into oldValueList;
       select CONCAT(newValueList,NEW.name,'~') into newValueList;
   END IF;
   
   IF (OLD.myFileId <> NEW.myFileId ) THEN
       select CONCAT(fieldNameList,'notes.myFileId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.myFileId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.myFileId,'~') into newValueList;
   END IF;
   
   IF (OLD.linkedTo <> NEW.linkedTo ) THEN
       select CONCAT(fieldNameList,'notes.linkedTo~') into fieldNameList;
       select CONCAT(oldValueList,OLD.linkedTo,'~') into oldValueList;
       select CONCAT(newValueList,NEW.linkedTo,'~') into newValueList;
   END IF;
   
   IF (OLD.issueType <> NEW.issueType ) THEN
       select CONCAT(fieldNameList,'notes.issueType~') into fieldNameList;
       select CONCAT(oldValueList,OLD.issueType,'~') into oldValueList;
       select CONCAT(newValueList,NEW.issueType,'~') into newValueList;
   END IF;
   
    IF (OLD.supplier <> NEW.supplier ) THEN
       select CONCAT(fieldNameList,'notes.supplier~') into fieldNameList;
       select CONCAT(oldValueList,OLD.supplier,'~') into oldValueList;
       select CONCAT(newValueList,NEW.supplier,'~') into newValueList;
   END IF;
   
   IF (OLD.grading <> NEW.grading ) THEN
       select CONCAT(fieldNameList,'notes.grading~') into fieldNameList;
       select CONCAT(oldValueList,OLD.grading,'~') into oldValueList;
       select CONCAT(newValueList,NEW.grading,'~') into newValueList;
   END IF;
   
   IF (OLD.networkLinkId <> NEW.networkLinkId ) THEN
       select CONCAT(fieldNameList,'notes.networkLinkId~') into fieldNameList;
       select CONCAT(oldValueList,OLD.networkLinkId,'~') into oldValueList;
       select CONCAT(newValueList,NEW.networkLinkId,'~') into newValueList;
   END IF;
   
    IF (OLD.category <> NEW.category ) THEN
       select CONCAT(fieldNameList,'notes.category~') into fieldNameList;
       select CONCAT(oldValueList,OLD.category,'~') into oldValueList;
       select CONCAT(newValueList,NEW.category,'~') into newValueList;
   END IF;
   
    IF (OLD.uvlSentStatus <> NEW.uvlSentStatus ) THEN
       select CONCAT(fieldNameList,'notes.uvlSentStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.uvlSentStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.uvlSentStatus,'~') into newValueList;
   END IF;
   
   IF (OLD.memoUploadStatus <> NEW.memoUploadStatus ) THEN
       select CONCAT(fieldNameList,'notes.memoUploadStatus~') into fieldNameList;
       select CONCAT(oldValueList,OLD.memoUploadStatus,'~') into oldValueList;
       select CONCAT(newValueList,NEW.memoUploadStatus,'~') into newValueList;
   END IF;
   
   IF (OLD.issue <> NEW.issue ) THEN
       select CONCAT(fieldNameList,'notes.issue~') into fieldNameList;
       select CONCAT(oldValueList,OLD.issue,'~') into oldValueList;
       select CONCAT(newValueList,NEW.issue,'~') into newValueList;
   END IF;
   
    IF (OLD.resolution <> NEW.resolution ) THEN
       select CONCAT(fieldNameList,'notes.resolution~') into fieldNameList;
       select CONCAT(oldValueList,OLD.resolution,'~') into oldValueList;
       select CONCAT(newValueList,NEW.resolution,'~') into newValueList;
   END IF;
   
    IF (OLD.roleCausedBy <> NEW.roleCausedBy ) THEN
       select CONCAT(fieldNameList,'notes.roleCausedBy~') into fieldNameList;
       select CONCAT(oldValueList,OLD.roleCausedBy,'~') into oldValueList;
       select CONCAT(newValueList,NEW.roleCausedBy,'~') into newValueList;
   END IF;
   
   
   IF (OLD.systemDate <> NEW.systemDate ) THEN
       select CONCAT(fieldNameList,'notes.systemDate~') into fieldNameList;
       IF(OLD.systemDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.systemDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.systemDate is not null) THEN
        	select CONCAT(oldValueList,OLD.systemDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.systemDate is not null) THEN
        	select CONCAT(newValueList,NEW.systemDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.dateOfContact <> NEW.dateOfContact ) THEN
       select CONCAT(fieldNameList,'notes.dateOfContact~') into fieldNameList;
       IF(OLD.dateOfContact is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.dateOfContact is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.dateOfContact is not null) THEN
        	select CONCAT(oldValueList,OLD.dateOfContact,'~') into oldValueList;
     	END IF;
      	IF(NEW.dateOfContact is not null) THEN
        	select CONCAT(newValueList,NEW.dateOfContact,'~') into newValueList;
      	END IF;
   END IF;
   
   
   IF (OLD.complitionDate <> NEW.complitionDate ) THEN
       select CONCAT(fieldNameList,'notes.complitionDate~') into fieldNameList;
       IF(OLD.complitionDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.complitionDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.complitionDate is not null) THEN
        	select CONCAT(oldValueList,OLD.complitionDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.complitionDate is not null) THEN
        	select CONCAT(newValueList,NEW.complitionDate,'~') into newValueList;
      	END IF;
   END IF;
   
    IF (OLD.memoLastUploadedDate <> NEW.memoLastUploadedDate ) THEN
       select CONCAT(fieldNameList,'notes.memoLastUploadedDate~') into fieldNameList;
       IF(OLD.memoLastUploadedDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.memoLastUploadedDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.memoLastUploadedDate is not null) THEN
        	select CONCAT(oldValueList,OLD.memoLastUploadedDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.memoLastUploadedDate is not null) THEN
        	select CONCAT(newValueList,NEW.memoLastUploadedDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.requestDate <> NEW.requestDate ) THEN
       select CONCAT(fieldNameList,'notes.requestDate~') into fieldNameList;
       IF(OLD.requestDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.requestDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.requestDate is not null) THEN
        	select CONCAT(oldValueList,OLD.requestDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.requestDate is not null) THEN
        	select CONCAT(newValueList,NEW.requestDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.submittedDate <> NEW.submittedDate ) THEN
       select CONCAT(fieldNameList,'notes.submittedDate~') into fieldNameList;
       IF(OLD.submittedDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.submittedDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.submittedDate is not null) THEN
        	select CONCAT(oldValueList,OLD.submittedDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.submittedDate is not null) THEN
        	select CONCAT(newValueList,NEW.submittedDate,'~') into newValueList;
      	END IF;
   END IF;
   
   IF (OLD.displaySurvey <> NEW.displaySurvey ) THEN
	    select CONCAT(fieldNameList,'notes.displaySurvey~') into fieldNameList;
	    IF(OLD.displaySurvey = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displaySurvey = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displaySurvey = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displaySurvey = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.accPortal <> NEW.accPortal ) THEN
	    select CONCAT(fieldNameList,'notes.accPortal~') into fieldNameList;
	    IF(OLD.accPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.accPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.accPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.accPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
	IF (OLD.partnerPortal <> NEW.partnerPortal ) THEN
	    select CONCAT(fieldNameList,'notes.partnerPortal~') into fieldNameList;
	    IF(OLD.partnerPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.partnerPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.partnerPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.partnerPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
	IF (OLD.bookingAgent <> NEW.bookingAgent ) THEN
	    select CONCAT(fieldNameList,'notes.bookingAgent~') into fieldNameList;
	    IF(OLD.bookingAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.bookingAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.bookingAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.bookingAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.originAgent <> NEW.originAgent ) THEN
	    select CONCAT(fieldNameList,'notes.originAgent~') into fieldNameList;
	    IF(OLD.originAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.originAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.originAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.originAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
	IF (OLD.destinationAgent <> NEW.destinationAgent ) THEN
	    select CONCAT(fieldNameList,'notes.destinationAgent~') into fieldNameList;
	    IF(OLD.destinationAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.destinationAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.destinationAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.destinationAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.subOriginAgent <> NEW.subOriginAgent ) THEN
	    select CONCAT(fieldNameList,'notes.subOriginAgent~') into fieldNameList;
	    IF(OLD.subOriginAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.subOriginAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.subOriginAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.subOriginAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
	IF (OLD.subDestinationAgent <> NEW.subDestinationAgent ) THEN
	    select CONCAT(fieldNameList,'notes.subDestinationAgent~') into fieldNameList;
	    IF(OLD.subDestinationAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.subDestinationAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.subDestinationAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.subDestinationAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.networkAgent <> NEW.networkAgent ) THEN
	    select CONCAT(fieldNameList,'notes.networkAgent~') into fieldNameList;
	    IF(OLD.networkAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.networkAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.networkAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.networkAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
	
	IF (OLD.displayTicket <> NEW.displayTicket ) THEN
	    select CONCAT(fieldNameList,'notes.displayTicket~') into fieldNameList;
	    IF(OLD.displayTicket = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displayTicket = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displayTicket = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displayTicket = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   
IF (OLD.displayInvoice <> NEW.displayInvoice ) THEN
	    select CONCAT(fieldNameList,'notes.displayInvoice~') into fieldNameList;
	    IF(OLD.displayInvoice = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displayInvoice = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displayInvoice = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displayInvoice = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
   	
    IF (OLD.displayPortal <> NEW.displayPortal ) THEN
	    select CONCAT(fieldNameList,'notes.displayPortal~') into fieldNameList;
	    IF(OLD.displayPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displayPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displayPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displayPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	 IF (OLD.displayQuote <> NEW.displayQuote ) THEN
	    select CONCAT(fieldNameList,'notes.displayQuote~') into fieldNameList;
	    IF(OLD.displayQuote = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.displayQuote = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.displayQuote = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.displayQuote = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
		 IF (OLD.negativeComment <> NEW.negativeComment ) THEN
	    select CONCAT(fieldNameList,'notes.negativeComment~') into fieldNameList;
	    IF(OLD.negativeComment = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.negativeComment = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.negativeComment = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.negativeComment = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF (OLD.reportToAgent <> NEW.reportToAgent ) THEN
	    select CONCAT(fieldNameList,'notes.reportToAgent~') into fieldNameList;
	    IF(OLD.reportToAgent = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.reportToAgent = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.reportToAgent = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.reportToAgent = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
	END IF;
	
	IF ((OLD.actualValue <> NEW.actualValue) or (OLD.actualValue is null and NEW.actualValue is not null)
      or (OLD.actualValue is not null and NEW.actualValue is null)) THEN
        select CONCAT(fieldNameList,'notes.actualValue~') into fieldNameList;
        IF(OLD.actualValue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.actualValue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.actualValue is not null) then
            select CONCAT(oldValueList,OLD.actualValue,'~') into oldValueList;
        END IF;
        IF(NEW.actualValue is not null) then
            select CONCAT(newValueList,NEW.actualValue,'~') into newValueList;
        END IF;
    END IF;
    
    IF ((OLD.requestedValue <> NEW.requestedValue) or (OLD.requestedValue is null and NEW.requestedValue is not null)
      or (OLD.requestedValue is not null and NEW.requestedValue is null)) THEN
        select CONCAT(fieldNameList,'notes.requestedValue~') into fieldNameList;
        IF(OLD.requestedValue is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.requestedValue is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.requestedValue is not null) then
            select CONCAT(oldValueList,OLD.requestedValue,'~') into oldValueList;
        END IF;
        IF(NEW.requestedValue is not null) then
            select CONCAT(newValueList,NEW.requestedValue,'~') into newValueList;
        END IF;
    END IF;
	
	
   CALL add_tblHistory (OLD.id,"notes", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());
END
$$
