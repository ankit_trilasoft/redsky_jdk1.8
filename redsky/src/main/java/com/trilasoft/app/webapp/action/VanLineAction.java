package com.trilasoft.app.webapp.action;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.math.BigDecimal;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.dao.hibernate.ServiceOrderDaoHibernate.ChargeTpDetailDTO;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Charges;
import com.trilasoft.app.model.Commission;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.RateGrid;
import com.trilasoft.app.model.RefMasterDTO;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.SubcontractorCharges;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.VanLine;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.ChargesManager;
import com.trilasoft.app.service.CommissionManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ExchangeRateManager;
import com.trilasoft.app.service.ExpressionManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.PartnerManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.SubcontractorChargesManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.VanLineGLTypeManager;
import com.trilasoft.app.service.VanLineManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class VanLineAction extends BaseAction implements Preparable {

	private List vanLineList;

	private Long id;

	private VanLineManager vanLineManager;

	private VanLineGLTypeManager vanLineGLTypeManager;
	private ServiceOrderManager serviceOrderManager;
	private ServiceOrder serviceOrder; 
	private AccountLineManager accountLineManager;
	private AccountLine accountLine;
	private AccountLine accLineNew;
	private BillingManager billingManager;
	private Billing billing;
	private ChargesManager chargesManager;
	private CompanyDivisionManager companyDivisionManager;
	private Boolean costElementFlag;	
	private String recAccDateTo;
	private String payAccDateTo;
	private Date recAccDateToFormat;
	private Date payAccDateToFormat;
	private List maxLineNumber;
	private String accountLineNumber;
	private Long autoLineNumber;
	private String sessionCorpID;
	private String chkWith;
	private String chkcategorypage;

	private VanLine vanLine;

	private VanLine vanLinelistData;
	
	private List downloadXMLList;

	private String flagvanLineData;


	private List chargeAllocationList;
	private String accID;
	private String accVenderDetails;
	private String venderDetailsList;
	private String fileNoVenderDetails;
	private String chargeAllocation;

	private RefMasterManager refMasterManager;

	private ExpressionManager expressionManager;

	private  Map<String, String> glCode;

	private  Map<String, String> glType;
	private Map<String, String> chargeCodeList= new HashMap<String, String>();

	private String gotoPageString;

	private String agent;

	private double dueFormAmount;

	private Date weekEnding;

	private double totalAmount;

	private List vanLineCodeList;

	private List weekendingDateList;

	private String validateFormNav;
	private String saveValidateForm;

	private  List<VanLine> showVanLines;

	private  List<VanLine> splitCharge;

	private String regNum;
	private String statementCategory;

	private List subcontractorChargesExt;

	private ExtendedPaginatedList vanLineListExt;
	private ExtendedPaginatedList SOListExt;

	private PagingLookupManager pagingLookupManager;

	private PaginateListFactory paginateListFactory;
	private boolean accountLineAccountPortalFlag=false; 
	private String expressionFormula;
    private String fileName;
	private String totalRevenue;
	private String vanRegNum;
	private List vanLineRegNumList;
	private boolean automaticReconcile=false;
	private BigDecimal vanlineMinimumAmount=new BigDecimal(0.00);
	private BigDecimal vanlineMaximumAmount=new BigDecimal(0.00);
	private String vanlineExceptionChargeCode;
	private boolean ownbillingVanline = false;
	private boolean vanlineSaveFlag= false;
	private String uvlBillToCode;
	private List vanLineExtractList;
	private List soList;
	private List processedXMLList;
	private String shipNum;
	private String lastName;
	private String firstName ;
	Date currentdate = new Date();
	public Long sid; 
	private  Map<String, String> categoryList;
	private String weekEnd;
	private List<SystemDefault> sysDefaultDetail;
	private String  removeFileVal;
	private String  removeAccVal;
	private String glCodeFlag;
	private List companyDivis = new ArrayList(); 
	private SubcontractorCharges subcontractorCharges;
	private SubcontractorChargesManager subcontractorChargesManager;
	private String companyDivisionAcctgCodeUnique;
	private String baseCurrencyCompanyDivision="";
	private  ExchangeRateManager exchangeRateManager;
	private boolean SplitChargeShowFlag;
	private Map<String,String> division;
	private String ownbillingBilltoCodes="";
	private String vanLineIdCheck;
	private String vanLineStatementCategoryCheck;
	private SystemDefault systemDefault;
	private  TrackingStatus trackingStatus;
	private  Miscellaneous miscellaneous;
	private MiscellaneousManager miscellaneousManager;
	private TrackingStatusManager trackingStatusManager;
	static final Logger logger = Logger.getLogger(VanLineAction.class);


	@SkipValidation
	public String vanLineSettForm() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineCodeList = vanLineManager.getVanLineCodeCompDiv(sessionCorpID);
		weekendingDateList = vanLineManager.getWeekendingList(agent, sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	 private String divisionFlag;
	@SkipValidation
	public String vanLineListWithReconcile() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		 //sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
		    String uvlBillToCode="";
    	    String uvlBillToName="";
    	    String mvlBillToCode ="";
    	    String mvlBillToName ="";
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					automaticReconcile=	systemDefault.getAutomaticReconcile();
					uvlBillToCode="";
					if(systemDefault.getUvlBillToCode()!=null){
					uvlBillToCode=systemDefault.getUvlBillToCode();
					}
					uvlBillToCode=systemDefault.getUvlBillToCode();
 					uvlBillToName=systemDefault.getUvlBillToName();
 					mvlBillToCode=systemDefault.getMvlBillToCode();
 					mvlBillToName=systemDefault.getMvlBillToName();
					vanlineMinimumAmount=systemDefault.getVanlineMinimumAmount();
					if(systemDefault.getVanlineMaximumAmount()!=null){
						vanlineMaximumAmount=systemDefault.getVanlineMaximumAmount();
					}
					if(systemDefault.getVanlineExceptionChargeCode()!=null){
						vanlineExceptionChargeCode =systemDefault.getVanlineExceptionChargeCode();
					}
					if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
						ownbillingVanline=systemDefault.getOwnbillingVanline();
					}
					recAccDateTo=new String("");
					if(systemDefault.getPostDate()!=null){
						recAccDateTo=systemDefault.getPostDate().toString();
					}
					if(systemDefault.getOwnbillingBilltoCodes()!=null){
					ownbillingBilltoCodes=systemDefault.getOwnbillingBilltoCodes();
					}
				}
				
			}
		if(ownbillingVanline){ 
			if (agent.indexOf(":") == 0 || agent.indexOf("'") == 0) {
				int i = vanLineManager.ownBillingVanLineListForReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID,uvlBillToCode,vanlineMaximumAmount,vanlineExceptionChargeCode);
			} else {
				int i = vanLineManager.ownBillingVanLineListForReconcile(agent.replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID,uvlBillToCode,vanlineMaximumAmount,vanlineExceptionChargeCode);
			} 	
		}else{
		if (agent.indexOf(":") == 0 || agent.indexOf("'") == 0) {
			int i = vanLineManager.vanLineListForReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID,uvlBillToCode);
		} else {
			int i = vanLineManager.vanLineListForReconcile(agent.replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID,uvlBillToCode);
		}
		}
	if(automaticReconcile){
		if(ownbillingVanline){

			String loginUser = getRequest().getRemoteUser();
			
				//change for posting date flexiblity Start	
				try{
					//company=companyManager.findByCorpID(sessionCorpID).get(0);
		    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    		Date dt1=new Date();
			    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			    		String dt11=sdfDestination.format(dt1);
			    		dt1=sdfDestination.parse(dt11);
			    		Date dt2=sdfDestination.parse(recAccDateTo);
			    		if(dt1.compareTo(dt2)>0){
			    			recAccDateTo=sdfDestination.format(dt2);
			    		}else{
			    			recAccDateTo=sdfDestination.format(dt1);
			    		}
		    		}
					}catch(Exception e){e.printStackTrace();}			
					//change for posting date flexiblity End				
			 
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try{ 
			if(!recAccDateTo.equals("")){	
				recAccDateToFormat=sdf.parse(recAccDateTo); 
				recAccDateTo=sdf.format(recAccDateToFormat); 
				}else{
					recAccDateToFormat =null; 
					recAccDateTo="null";
				}
			}
			catch( Exception e)
			{
				e.printStackTrace();
			}	
		  List vanlineregNum=vanLineManager.getOwnBillingRegNumVanline(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding);
		  if(vanlineregNum!=null && (!vanlineregNum.isEmpty())){ 
				 Iterator regNumIterator=vanlineregNum.iterator();
				 String previousShipnumber="";
				 //String recInvoiceNumber = "";
				 
				 while(regNumIterator.hasNext()){
					 boolean vanlineStatus=true;
					 boolean vanlineMaximumAmountStatus=true;
					 boolean vanlineExceptionChargeCodeStatus=true;
					 String idData=regNumIterator.next().toString();
					  Long  id = Long.parseLong(idData);
					  vanLine=vanLineManager.get(id);
					  
					  if(vanlineExceptionChargeCode!=null && (!(vanlineExceptionChargeCode.equals(""))) && vanlineExceptionChargeCode.contains(vanLine.getGlType()) ){
						  vanlineExceptionChargeCodeStatus=false;    
					  }
					  if(vanLine.getReconcileAmount()==null || ((vanLine.getReconcileAmount().toString().equals(""))) ||  vanLine.getReconcileAmount().doubleValue()==0.0 ){
							vanLine.setReconcileAmount(vanLine.getAmtDueAgent());	
						}
					  if(vanLine.getReconcileAmount()!=null && ((vanLine.getReconcileAmount().doubleValue()>vanlineMaximumAmount.doubleValue()) || (vanLine.getReconcileAmount().doubleValue()< (vanlineMaximumAmount.doubleValue()*-1)))){
						  vanlineMaximumAmountStatus=false;  
					  }
					  if(vanLine.getReconcileAmount()!=null && (!(vanLine.getReconcileAmount().toString().equals(""))) &&  vanLine.getReconcileAmount().doubleValue()!=0.0 && vanlineMaximumAmountStatus && vanlineExceptionChargeCodeStatus ){
					List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
					if(SOidList!=null && (!SOidList.isEmpty())){
						Iterator SOidIterator=SOidList.iterator();
						
						 while(SOidIterator.hasNext()){
							 String SoIdData=SOidIterator.next().toString();
							  Long  SoId = Long.parseLong(SoIdData)  ; 
							  serviceOrder = serviceOrderManager.get(SoId); 
							  billing=billingManager.get(SoId);
							  trackingStatus=trackingStatusManager.get(SoId);
							  miscellaneous = miscellaneousManager.get(SoId);
							  List chargeid=new ArrayList();
							  String accRecGl="";
							  try{
									if(!costElementFlag){
									 chargeid=chargesManager.findChargeId(vanLine.getGlType(),billing.getContract());
									 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
										 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
	                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
	                                    	  accRecGl= charge.getGl(); 
	                                      } 
									 } 
									}else{
										 
										chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),vanLine.getGlType(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
										String chargeStr=""; 
										if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
											  chargeStr= chargeid.get(0).toString();
										  }
										   if(!chargeStr.equalsIgnoreCase("")){
											  String [] chrageDetailArr = chargeStr.split("#");
											  try{
											  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
											  accRecGl=(chrageDetailArr[1]); 
											  }
											  }catch(Exception e){
												  e.printStackTrace();
											  }
										   }
									}
							  } catch(Exception e){
								  e.printStackTrace();
							  }
							  
							  if((!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())))&& serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) ){ 
						    	  //Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
								  //recInvoiceNumber=invoiceNumber.toString();  
								  previousShipnumber=serviceOrder.getShipNumber();
							  }
	                         
						      if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) ){
						    	  
								  //String loginUser = getRequest().getRemoteUser();	
						    	  accountLine = new AccountLine();
								  accountLine.setActivateAccPortal(true); 
								  boolean activateAccPortal =true;
									try{
									if(accountLineAccountPortalFlag){	
									activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
									accountLine.setActivateAccPortal(activateAccPortal);
									}
									}catch(Exception e){
										e.printStackTrace();
									}
								  accountLine.setBasis("");
								  accountLine.setCategory("Revenue");
								  accountLine.setChargeCode(vanLine.getGlType());
								  accountLine.setContract(billing.getContract());
									try {
										String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
										if(!ss.equalsIgnoreCase("")){
											accountLine.setAccountLineCostElement(ss.split("~")[0]);
											accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
										}
									} catch (Exception e1) {
										e1.printStackTrace();
									}
								  
								  if(vanLine.getBillToCode()!=null && (!(vanLine.getBillToCode().toString().trim().equals("")))){
									  accountLine.setBillToCode(vanLine.getBillToCode());
									  accountLine.setBillToName(vanLine.getBillToName());  
								  }else{
								  accountLine.setBillToCode(billing.getBillToCode());
								  accountLine.setBillToName(billing.getBillToName());
								  }
								  if(vanLine.getDescription()!=null && (!(vanLine.getDescription().toString().equals("")))){
									  accountLine.setDescription(vanLine.getDescription());
								  }else{
									   accountLine.setDescription(vanLine.getDistributionCodeDescription());  
								   }
								  accountLine.setRecQuantity(new BigDecimal(1));
								  accountLine.setActualRevenue(vanLine.getReconcileAmount()) ;
								  accountLine.setRecRate(vanLine.getReconcileAmount()) ;
								  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
								  accountLine.setDistributionAmount(vanLine.getReconcileAmount());
								  String  invDate ="";
								  try{
								  if(vanLine.getWeekEnding()!=null) {
					        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
					        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
								  }}catch(Exception e){
									e.printStackTrace();  
								  } 
								  accountLine.setExternalReference("UVL "+invDate);
								  accountLine.setPaymentStatus("Fully Paid");
								  accountLine.setPaymentSent(null);
								  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
								  accountLine.setStatusDate(vanLine.getWeekEnding());
								  accountLine.setCorpID(sessionCorpID);
								  accountLine.setStatus(true);
								  accountLine.setServiceOrder(serviceOrder);
								  accountLine.setServiceOrderId(serviceOrder.getId());
								  String companyCode="";
								  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
								  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
								  if(companyCode.trim().equals("")){
									  companyCode=serviceOrder.getCompanyDivision();  
								  }
								  }else{
								  companyCode=serviceOrder.getCompanyDivision();
								  }
								  accountLine.setCompanyDivision(companyCode);
								  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
								  accountLine.setShipNumber(serviceOrder.getShipNumber());
								  accountLine.setCreatedOn(new Date());
								  accountLine.setCreatedBy("Vanline : "+loginUser);
								  accountLine.setUpdatedOn(new Date());
								  accountLine.setUpdatedBy("Vanline : "+loginUser);
								  //accountLine.setRecPostDate(recAccDateToFormat);
								  //accountLine.setRecInvoiceNumber(recInvoiceNumber);
								  //accountLine.setReceivedInvoiceDate(new Date());
								 // accountLine.setInvoiceCreatedBy(loginUser);
								  
							///Code for AccountLine division
					    	if((divisionFlag!=null) && (!divisionFlag.equalsIgnoreCase(""))){
					    		 accountLine.setDivision("03");
					    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
					    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
					    		try{  
					    			String roles[] = agentRoleValue.split("~");
					    		  if(roles[3].toString().equals("hauler")){
					    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
					    				   accountLine.setDivision("01");
					    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
					    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
					    					   accountLine.setDivision("01");
					    				   }else{ 
					    					   String divisionTemp="";
					    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
					    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
					    							}
					    							if(!divisionTemp.equalsIgnoreCase("")){
					    								accountLine.setDivision(divisionTemp);
					    							} 
					    				   }
					    			   }else{
					    				   String divisionTemp="";
					    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
					    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
					    						}
					    						if(!divisionTemp.equalsIgnoreCase("")){
					    							accountLine.setDivision(divisionTemp);
					    						}
					    			   }
					    		  }else{
					    			  String divisionTemp="";
					    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
					    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
					    					}
					    					if(!divisionTemp.equalsIgnoreCase("")){
					    						accountLine.setDivision(divisionTemp);
					    					}
					    		  }
					    		}catch (Exception e1) {
					    			// TODO Auto-generated catch block
					    			e1.printStackTrace();
					    		}
				    		}else{
				    			  accountLine.setDivision(null);
				    		}
							///Code for AccountLine division 
									 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
							         if ( maxLineNumber.get(0) == null ) {          
							         	accountLineNumber = "001";
							         }else {
							         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
							             if((autoLineNumber.toString()).length() == 2) {
							             	accountLineNumber = "0"+(autoLineNumber.toString());
							             }
							             else if((autoLineNumber.toString()).length() == 1) {
							             	accountLineNumber = "00"+(autoLineNumber.toString());
							             } 
							             else {
							             	accountLineNumber=autoLineNumber.toString();
							             }
							         }
							         accountLine.setAccountLineNumber(accountLineNumber);
								  try{
									    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
									  if(!costElementFlag){
										Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

										
											accountLine.setRecGl(charge.getGl());
											accountLine.setPayGl(charge.getExpGl());
										}else{
											String chargeStr="";
											  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
											if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
												  chargeStr= chargeid.get(0).toString();
											  }
											   if(!chargeStr.equalsIgnoreCase("")){
												  String [] chrageDetailArr = chargeStr.split("#");
												  accountLine.setRecGl(chrageDetailArr[1]);
												  accountLine.setPayGl(chrageDetailArr[2]);
												}else{
												  accountLine.setRecGl("");
												  accountLine.setPayGl("");
											  }
										}
								  } catch(Exception e){
									  e.printStackTrace();
								  }
								  accountLine= accountLineManager.save(accountLine);
								  sid=serviceOrder.getId();
								  String jobName="";
								  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
								    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
								    	jobName = tempCommissionJob.get(0).toString();
								    }
								    if(jobName.contains(serviceOrder.getJob())){
								    	calculateCommisssionMethod();
								    }
								  
						      }else{
						    	
						    	  vanlineStatus=false;
						      } 			      
						   
						 }
					
					}
					if(vanlineStatus){
					int k=vanLineManager.updateVanLineWithReconcile(vanLine.getId(),loginUser);	
					 
					}
				 }
				 }
				 
			 }
		  
		  List accIdList= new ArrayList();
		  accIdList=vanLineManager.checkInvoiceOwnBillingDateForWithReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID, uvlBillToCode,vanlineMaximumAmount,vanlineExceptionChargeCode);
		  Iterator iterator=accIdList.iterator();
		  //String previousSoId="";
		  //String previousBilltocode="";
		  String recInvoiceNumber1 = "";
		  //String creditInvoiceNumber="";
		  String key = new String("");
		  String samekey = new String("");
		  BigDecimal sumActualRevenue = new BigDecimal(0.0);
		  BigDecimal sumDistributionAmount = new BigDecimal(0.0); 
		  SortedMap level1Map = new TreeMap();
		  SortedMap level4Map = new TreeMap();
		  while (iterator.hasNext()) {
			  String idData=iterator.next().toString();
			  String [] idDataArr = idData.split("~");
			  try{ 
			  Long  id = Long.parseLong(idDataArr[0]) ; 
			  Long  soId = Long.parseLong(idDataArr[1].trim()) ;  
			  String billtocode=idDataArr[2];  
			  BigDecimal distributionAmount =new BigDecimal(idDataArr[3]); 
			  BigDecimal actualRevenue =new BigDecimal(0.00); 
			  if(idDataArr[4]!=null && (!(idDataArr[4].equals("")))){
			  actualRevenue =new BigDecimal(idDataArr[4]);
			  }
			  String chargeCode=idDataArr[5];  
			  Long  vId = Long.parseLong(idDataArr[6].trim()) ;  
			  
			  
			  key = soId + "~" + billtocode.trim(); 
				if (key.equals(samekey)) {
					sumActualRevenue = actualRevenue.add(sumActualRevenue);
					sumDistributionAmount=distributionAmount.add(sumDistributionAmount);
				} else if (!key.equals(samekey)) { 
					sumActualRevenue = actualRevenue;
					sumDistributionAmount=distributionAmount;
				}

				samekey = soId + "~" + billtocode.trim();
				List level4List = (List)level4Map.get(key);
				if (level4List == null || level4List.isEmpty()) {
					level4List = new ArrayList();
				}			
				try {
					String Level1MapValue = sumActualRevenue.toString() + "==" + sumDistributionAmount.toString()  ;
					String Level4MapValue = id + "==" + chargeCode ;
					level4List.add(Level4MapValue);
					level1Map.put(key, Level1MapValue);
					level4Map.put(key, level4List);
				} catch (Exception e) {
					e.printStackTrace();
				}
			/*  if((!(previousSoId.equalsIgnoreCase(idDataArr[1]))) || (!(previousBilltocode.equalsIgnoreCase(billtocode)))){
				  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
				  recInvoiceNumber1=invoiceNumber.toString(); 
				  previousSoId=idDataArr[1];
				  previousBilltocode=billtocode;
			  }*/
			  // accLineNew=accountLineManager.get(id);
			  // accLineNew.setUpdatedOn(new Date());
			   //accLineNew.setUpdatedBy("Vanline : "+getRequest().getRemoteUser());		
			   //accLineNew.setRecPostDate(recAccDateToFormat);
			   //accLineNew.setRecInvoiceNumber(recInvoiceNumber1);
			  // accLineNew.setReceivedInvoiceDate(new Date());
			  // accLineNew.setInvoiceCreatedBy(getRequest().getRemoteUser());
			  // accountLineManager.save(accLineNew);
			 //vanLineManager.updateAccRecInvoiceNumberWithReconcile(id,loginUser,recAccDateTo,recInvoiceNumber1); 
			  }catch(Exception e){
				  e.printStackTrace();
			  }
			  
		  }
		  
		  
		  Iterator mapIterator = level1Map.entrySet().iterator();
			while (mapIterator.hasNext()) {
				Map.Entry entry = (Map.Entry) mapIterator.next();
				String Invoicekey = (String) entry.getKey();
				String [] idDataArr = Invoicekey.split("~"); 
				Long  soId = Long.parseLong(idDataArr[0].trim()) ;
				String billtocode=idDataArr[1];  
				String detailLevel1 = (String) level1Map .get(Invoicekey);
				String arraydetailLevel1[] = detailLevel1.split("==");
				String totalActualRevenueShip = arraydetailLevel1[0];
				BigDecimal actualRevenue = new BigDecimal(totalActualRevenueShip);
				String totalDistributionAmountShip = arraydetailLevel1[1];
				BigDecimal distributionAmount = new BigDecimal(totalDistributionAmountShip); 
				if(distributionAmount.doubleValue()==actualRevenue.doubleValue()){
				double actualRevenueDouble=actualRevenue.doubleValue();
				//String recInvoiceNumber=arraydetailLevel1[2];
				String creditInvoiceNumber="NA";
				List level4List = (List)level4Map.get(Invoicekey);
				Iterator listIterator = level4List.iterator();
				String idList="";
				String chargeCodeList="";
				while(listIterator.hasNext()){
					String detailLevel4 = (String) listIterator.next();	
					String arraydetailLevel4[] = detailLevel4.split("==");
					if(idList.equals("")){
						idList=arraydetailLevel4[0];
	    	    	}else{
	    	    		idList=idList+","+arraydetailLevel4[0];
	    	    	}
					if(chargeCodeList.equals("")){
						chargeCodeList=arraydetailLevel4[1];
	    	    	}else{
	    	    		if(!(chargeCodeList.contains(arraydetailLevel4[1]))){
	    	    		chargeCodeList=chargeCodeList+"~"+arraydetailLevel4[1];
	    	    		}
	    	    	}
				}
				if(actualRevenue.doubleValue()<0){
					creditInvoiceNumber	=accountLineManager.findCreditInvoice(soId,billtocode,actualRevenueDouble,chargeCodeList,sessionCorpID);
					if(creditInvoiceNumber.trim().equals("No")){
						creditInvoiceNumber="";	
					}
				}
				else {
					
				}
				if(!(creditInvoiceNumber.trim().equals(""))){
					Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
					String recInvoiceNumber=invoiceNumber.toString(); 
				vanLineManager.updateAccRecInvoiceNumberWithOwnBillingReconcile(idList,loginUser,recAccDateTo,recInvoiceNumber,creditInvoiceNumber,soId,sessionCorpID); 
				}
			}
			}
		  
	 	 vanLineManager.updateAccountLineWithOwnBillingReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID, uvlBillToCode,vanlineMaximumAmount,vanlineExceptionChargeCode ); 
	 	List vanlineregNumDSTR=vanLineManager.getDSTROwnBillingRegNumVanline(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,sessionCorpID);
		  if(vanlineregNumDSTR!=null && (!vanlineregNumDSTR.isEmpty())){ 
				 Iterator regNumIterator=vanlineregNumDSTR.iterator();
				 while(regNumIterator.hasNext()){
				 String regnum=regNumIterator.next().toString(); 
				 List  SOidList = vanLineManager.getShipNumber(regnum);
					if(SOidList!=null && (!SOidList.isEmpty())){
						Iterator SOidIterator=SOidList.iterator();
						while(SOidIterator.hasNext()){
							 String SoIdData=SOidIterator.next().toString();
							  Long  SoId = Long.parseLong(SoIdData)  ; 
							  serviceOrder = serviceOrderManager.get(SoId); 
							  billing=billingManager.get(SoId);
							  trackingStatus=trackingStatusManager.get(SoId);
							  miscellaneous = miscellaneousManager.get(SoId);
							  String previousShipnumber="";
							  String recInvoiceNumber = "";
							  String creditInvoiceNumber = "NA";
							  if(billing.getBillToCode()!=null && (!(billing.getBillToCode().toString().trim().equals(""))) && (ownbillingBilltoCodes.indexOf(billing.getBillToCode().toString().trim())>=0)){
							  List DSTRAccountLine=  accountLineManager.findDSTRAccountLine(weekEnding,sessionCorpID,SoId);
							  if(DSTRAccountLine !=null && (!(DSTRAccountLine.isEmpty())) && DSTRAccountLine.size()>0 ){
								  
							  }else{
							  List chargeid=new ArrayList();
							  String accRecGl="";
							  try{
									if(!costElementFlag){
									 chargeid=chargesManager.findChargeId("DSTR",billing.getContract());
									 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
										 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
	                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
	                                    	  accRecGl= charge.getGl(); 
	                                      } 
									 } 
									}else{
										 
										chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),"DSTR",sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
										String chargeStr=""; 
										if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
											  chargeStr= chargeid.get(0).toString();
										  }
										   if(!chargeStr.equalsIgnoreCase("")){
											  String [] chrageDetailArr = chargeStr.split("#");
											  try{
											  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
											  accRecGl=(chrageDetailArr[1]); 
											  }
											  }catch(Exception e){
												  e.printStackTrace();
											  }
										   }
									}
							  } catch(Exception e){
								  e.printStackTrace();
							  }
							  
							  List vanlineRegNumAmount=vanLineManager.getOwnBillingRegNumVanlineAmount(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,regnum,sessionCorpID); 
							  BigDecimal vanlineAmount = new BigDecimal((vanlineRegNumAmount).get(0).toString());
							  String billtocode=billing.getBillToCode();
							  if(vanLine.getAgent().indexOf("U")==0){
								  billtocode=uvlBillToCode; 
								}
								if(vanLine.getAgent().indexOf("M")==0){
									billtocode=mvlBillToCode; 
								}
							  if(vanlineAmount.doubleValue()!=0.0){
							  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && billtocode != null && (!(billtocode.toString().trim().equals("")))  ){ 
							  if(vanlineAmount.doubleValue()<0){
								  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
								  recInvoiceNumber=invoiceNumber.toString();
								  creditInvoiceNumber="";
								  creditInvoiceNumber	=accountLineManager.findCreditInvoice(serviceOrder.getId(),billtocode,vanlineAmount.doubleValue(),"DSTR",sessionCorpID);
							  }else{
							  //if(!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())) && serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) ){ 
						    	  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
								  recInvoiceNumber=invoiceNumber.toString();  
								  }
								  //previousShipnumber=serviceOrder.getShipNumber();
							 // }
							 
						      
							  
								   
								  accountLine = new AccountLine(); 
								  accountLine.setActivateAccPortal(true);
								  boolean activateAccPortal =true;
									try{
									if(accountLineAccountPortalFlag){	
									activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
									accountLine.setActivateAccPortal(activateAccPortal);
									}
									}catch(Exception e){
									e.printStackTrace();	
									}
								  accountLine.setBasis("");
								  accountLine.setCategory("Revenue");
								  accountLine.setChargeCode("DSTR");
									try {
										String ss=chargesManager.findPopulateCostElementData("DSTR", billing.getContract(),sessionCorpID);
										if(!ss.equalsIgnoreCase("")){
											accountLine.setAccountLineCostElement(ss.split("~")[0]);
											accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
										}
									} catch (Exception e1) {
										e1.printStackTrace();
									}
								  
								  accountLine.setContract(billing.getContract());
								  //accountLine.setBillToCode(billing.getBillToCode());
								  //accountLine.setBillToName(billing.getBillToName());
								  if(vanLine.getAgent().indexOf("U")==0){
										accountLine.setBillToCode(uvlBillToCode);
										accountLine.setBillToName(uvlBillToName);
									}
									if(vanLine.getAgent().indexOf("M")==0){
										accountLine.setBillToCode(mvlBillToCode);
										accountLine.setBillToName(mvlBillToName);
										
									}
								  accountLine.setRecQuantity(new BigDecimal(1));
								  accountLine.setActualRevenue(vanlineAmount) ;
								  accountLine.setRecRate(vanlineAmount) ;
								  accountLine.setVanlineSettleDate(weekEnding);
								  accountLine.setDistributionAmount(vanlineAmount);
								  
									///Code for AccountLine division
								  if((divisionFlag!=null) && (!divisionFlag.equalsIgnoreCase(""))){
							    		 accountLine.setDivision("03");
							    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
							    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
							    		try{  
							    			String roles[] = agentRoleValue.split("~");
							    		  if(roles[3].toString().equals("hauler")){
							    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
							    				   accountLine.setDivision("01");
							    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
							    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
							    					   accountLine.setDivision("01");
							    				   }else{ 
							    					   String divisionTemp="";
							    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
							    							}
							    							if(!divisionTemp.equalsIgnoreCase("")){
							    								accountLine.setDivision(divisionTemp);
							    							} 
							    				   }
							    			   }else{
							    				   String divisionTemp="";
							    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
							    						}
							    						if(!divisionTemp.equalsIgnoreCase("")){
							    							accountLine.setDivision(divisionTemp);
							    						}
							    			   }
							    		  }else{
							    			  String divisionTemp="";
							    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
							    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
							    					}
							    					if(!divisionTemp.equalsIgnoreCase("")){
							    						accountLine.setDivision(divisionTemp);
							    					}
							    		  }
							    		}catch (Exception e1) {
							    			// TODO Auto-generated catch block
							    			e1.printStackTrace();
							    		}
						    		}else{
						    			  accountLine.setDivision(null);
						    		}
									///Code for AccountLine division 
								    
								  String  invDate ="";
								  try{
								  if(weekEnding!=null) {
					        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
					        			invDate	= new StringBuilder(formats.format(weekEnding)).toString();
								  }}catch(Exception e){
									  e.printStackTrace();
								  } 
								  accountLine.setExternalReference("UVL "+invDate);
								  accountLine.setPaymentStatus("Fully Paid");
								  accountLine.setPaymentSent(null);
								  accountLine.setReceivedAmount(vanlineAmount);
								  accountLine.setStatusDate(weekEnding);
								  accountLine.setCorpID(sessionCorpID);
								  accountLine.setStatus(true);
								  accountLine.setServiceOrder(serviceOrder);
								  accountLine.setServiceOrderId(serviceOrder.getId());
								  /*String companyCode="";
								  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
								  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
								  if(companyCode.trim().equals("")){
									  companyCode=serviceOrder.getCompanyDivision();  
								  }
								  }else{
								  companyCode=serviceOrder.getCompanyDivision();
								  }*/
								  accountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
								  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
								  accountLine.setShipNumber(serviceOrder.getShipNumber());
								  accountLine.setCreatedOn(new Date());
								  accountLine.setCreatedBy("Vanline : "+loginUser);
								  accountLine.setUpdatedOn(new Date());
								  accountLine.setUpdatedBy("Vanline : "+loginUser);
								  accountLine.setRecPostDate(recAccDateToFormat);
								  accountLine.setRecInvoiceNumber(recInvoiceNumber);
								  if(creditInvoiceNumber.equals("NA")){
									  creditInvoiceNumber="";  
								  }
								  accountLine.setCreditInvoiceNumber(creditInvoiceNumber);
								  accountLine.setReceivedInvoiceDate(new Date());
								  accountLine.setInvoiceCreatedBy(loginUser); 
								  
								
									 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
							         if ( maxLineNumber.get(0) == null ) {          
							         	accountLineNumber = "001";
							         }else {
							         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
							             if((autoLineNumber.toString()).length() == 2) {
							             	accountLineNumber = "0"+(autoLineNumber.toString());
							             }
							             else if((autoLineNumber.toString()).length() == 1) {
							             	accountLineNumber = "00"+(autoLineNumber.toString());
							             } 
							             else {
							             	accountLineNumber=autoLineNumber.toString();
							             }
							         }
							         accountLine.setAccountLineNumber(accountLineNumber);
								  try{
									    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
									  if(!costElementFlag){
										Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

										
											accountLine.setRecGl(charge.getGl());
											accountLine.setPayGl(charge.getExpGl());
											accountLine.setDescription(charge.getDescription());
												   
											
										}else{
											String chargeStr="";
											  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
											if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
												  chargeStr= chargeid.get(0).toString();
											  }
											   if(!chargeStr.equalsIgnoreCase("")){
												  String [] chrageDetailArr = chargeStr.split("#");
												  accountLine.setRecGl(chrageDetailArr[1]);
												  accountLine.setPayGl(chrageDetailArr[2]);
												}else{
												  accountLine.setRecGl("");
												  accountLine.setPayGl("");
											  }
										}
								  } catch(Exception e){
									  e.printStackTrace();
								  }
								  accountLine= accountLineManager.save(accountLine); 
							  
						      } 
							  }
							  }
						}
						}
					}
				 }
		  } 
	 	 
		}else{
		String loginUser = getRequest().getRemoteUser();
		 
			//change for posting date flexiblity Start	
			try{
				//company=companyManager.findByCorpID(sessionCorpID).get(0);
	    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
		    		Date dt1=new Date();
		    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		    		String dt11=sdfDestination.format(dt1);
		    		dt1=sdfDestination.parse(dt11);
		    		Date dt2=sdfDestination.parse(recAccDateTo);
		    		if(dt1.compareTo(dt2)>0){
		    			recAccDateTo=sdfDestination.format(dt2);
		    		}else{
		    			recAccDateTo=sdfDestination.format(dt1);
		    		}
	    		}
				}catch(Exception e){e.printStackTrace();}			
				//change for posting date flexiblity End				
		 
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{ 
		if(!recAccDateTo.equals("")){	
			recAccDateToFormat=sdf.parse(recAccDateTo); 
			recAccDateTo=sdf.format(recAccDateToFormat); 
			}else{
				recAccDateToFormat =null; 
				recAccDateTo="null";
			}
		}
		catch( Exception e)
		{
			e.printStackTrace();
		}	
	  List vanlineregNum=vanLineManager.getRegNumVanline(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding);
	  if(vanlineregNum!=null && (!vanlineregNum.isEmpty())){ 
			 Iterator regNumIterator=vanlineregNum.iterator();
			 String previousShipnumber="";
			 String recInvoiceNumber = "";
			 
			 while(regNumIterator.hasNext()){
				 boolean vanlineStatus=true;
				 String idData=regNumIterator.next().toString();
				  Long  id = Long.parseLong(idData);
				  vanLine=vanLineManager.get(id);
				  if(vanLine.getReconcileAmount()==null || ((vanLine.getReconcileAmount().toString().equals(""))) ||  vanLine.getReconcileAmount().doubleValue()==0.0 ){
						vanLine.setReconcileAmount(vanLine.getAmtDueAgent());	
					}
				  if(vanLine.getReconcileAmount()!=null && (!(vanLine.getReconcileAmount().toString().equals(""))) &&  vanLine.getReconcileAmount().doubleValue()!=0.0){
				List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
				if(SOidList!=null && (!SOidList.isEmpty())){
					Iterator SOidIterator=SOidList.iterator();
					
					 while(SOidIterator.hasNext()){
						 String SoIdData=SOidIterator.next().toString();
						  Long  SoId = Long.parseLong(SoIdData)  ; 
						  serviceOrder = serviceOrderManager.get(SoId); 
						  billing=billingManager.get(SoId);
						  trackingStatus=trackingStatusManager.get(SoId);
						  miscellaneous = miscellaneousManager.get(SoId);
						  List chargeid=new ArrayList();
						  String accRecGl="";
						  try{
								if(!costElementFlag){
								 chargeid=chargesManager.findChargeId(vanLine.getGlType(),billing.getContract());
								 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
									 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
                                    	  accRecGl= charge.getGl(); 
                                      } 
								 } 
								}else{
									 
									chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),vanLine.getGlType(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
									String chargeStr=""; 
									if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
										  chargeStr= chargeid.get(0).toString();
									  }
									   if(!chargeStr.equalsIgnoreCase("")){
										  String [] chrageDetailArr = chargeStr.split("#");
										  try{
										  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
										  accRecGl=(chrageDetailArr[1]); 
										  }
										  }catch(Exception e){
											  e.printStackTrace();
										  }
									   }
								}
						  } catch(Exception e){
							  e.printStackTrace();
						  }
						  
						  if((!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())))&& serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && uvlBillToCode.equalsIgnoreCase(billing.getBillToCode())){ 
					    	  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
							  recInvoiceNumber=invoiceNumber.toString();  
							  previousShipnumber=serviceOrder.getShipNumber();
						  }
                         
					      if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && uvlBillToCode.equalsIgnoreCase(billing.getBillToCode())){
					    	  
							  //String loginUser = getRequest().getRemoteUser();	
					    	  accountLine = new AccountLine();
							  accountLine.setActivateAccPortal(true);  
							  boolean activateAccPortal =true;
								try{
								if(accountLineAccountPortalFlag){	
								activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
								accountLine.setActivateAccPortal(activateAccPortal);
								}
								}catch(Exception e){
									e.printStackTrace();
								}
							  accountLine.setBasis("");
							  accountLine.setCategory("Revenue");
							  accountLine.setChargeCode(vanLine.getGlType());
								try {
									String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
									if(!ss.equalsIgnoreCase("")){
										accountLine.setAccountLineCostElement(ss.split("~")[0]);
										accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
									}
								} catch (Exception e1) {
									e1.printStackTrace();
								}
							  
							  accountLine.setContract(billing.getContract());
							  accountLine.setBillToCode(billing.getBillToCode());
							  accountLine.setBillToName(billing.getBillToName());
							  if(vanLine.getDescription()!=null && (!(vanLine.getDescription().toString().equals("")))){
								  accountLine.setDescription(vanLine.getDescription());
							  }else{
								   accountLine.setDescription(vanLine.getDistributionCodeDescription());  
							   }
							  accountLine.setRecQuantity(new BigDecimal(1));
							  accountLine.setActualRevenue(vanLine.getReconcileAmount()) ;
							  accountLine.setRecRate(vanLine.getReconcileAmount()) ;
							  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
							  accountLine.setDistributionAmount(vanLine.getReconcileAmount());
							  String  invDate ="";
							  try{
							  if(vanLine.getWeekEnding()!=null) {
				        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
				        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
							  }}catch(Exception e){
								e.printStackTrace();  
							  } 
							  accountLine.setExternalReference("UVL "+invDate);
							  accountLine.setPaymentStatus("Fully Paid");
							  accountLine.setPaymentSent(null);
							  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
							  accountLine.setStatusDate(vanLine.getWeekEnding());
							  accountLine.setCorpID(sessionCorpID);
							  accountLine.setStatus(true);
							  accountLine.setServiceOrder(serviceOrder);
							  accountLine.setServiceOrderId(serviceOrder.getId());
							  String companyCode="";
							  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
							  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
							  if(companyCode.trim().equals("")){
								  companyCode=serviceOrder.getCompanyDivision();  
							  }
							  }else{
							  companyCode=serviceOrder.getCompanyDivision();
							  }
							  accountLine.setCompanyDivision(companyCode);
							  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
							  accountLine.setShipNumber(serviceOrder.getShipNumber());
							  accountLine.setCreatedOn(new Date());
							  accountLine.setCreatedBy("Vanline : "+loginUser);
							  accountLine.setUpdatedOn(new Date());
							  accountLine.setUpdatedBy("Vanline : "+loginUser);
							  accountLine.setRecPostDate(recAccDateToFormat);
							  accountLine.setRecInvoiceNumber(recInvoiceNumber);
							  accountLine.setReceivedInvoiceDate(new Date());
							  accountLine.setInvoiceCreatedBy(loginUser);
							  
						///Code for AccountLine division
							  
						    		accountLine.setDivision("03");
						    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
						    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
						    		try{  
						    			String roles[] = agentRoleValue.split("~");
						    		  if(roles[3].toString().equals("hauler")){
						    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
						    				   accountLine.setDivision("01");
						    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
						    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
						    					   accountLine.setDivision("01");
						    				   }else{ 
						    					   String divisionTemp="";
						    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    							}
						    							if(!divisionTemp.equalsIgnoreCase("")){
						    								accountLine.setDivision(divisionTemp);
						    							} 
						    				   }
						    			   }else{
						    				   String divisionTemp="";
						    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    						}
						    						if(!divisionTemp.equalsIgnoreCase("")){
						    							accountLine.setDivision(divisionTemp);
						    						}
						    			   }
						    		  }else{
						    			  String divisionTemp="";
						    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    					}
						    					if(!divisionTemp.equalsIgnoreCase("")){
						    						accountLine.setDivision(divisionTemp);
						    					}
						    		  }
						    		}catch (Exception e1) {
						    			// TODO Auto-generated catch block
						    			e1.printStackTrace();
						    		}
					    		
						///Code for AccountLine division 
								 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
						         if ( maxLineNumber.get(0) == null ) {          
						         	accountLineNumber = "001";
						         }else {
						         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
						             if((autoLineNumber.toString()).length() == 2) {
						             	accountLineNumber = "0"+(autoLineNumber.toString());
						             }
						             else if((autoLineNumber.toString()).length() == 1) {
						             	accountLineNumber = "00"+(autoLineNumber.toString());
						             } 
						             else {
						             	accountLineNumber=autoLineNumber.toString();
						             }
						         }
						         accountLine.setAccountLineNumber(accountLineNumber);
							  try{
								    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
								  if(!costElementFlag){
									Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

									
										accountLine.setRecGl(charge.getGl());
										accountLine.setPayGl(charge.getExpGl());
									}else{
										String chargeStr="";
										  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
										if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
											  chargeStr= chargeid.get(0).toString();
										  }
										   if(!chargeStr.equalsIgnoreCase("")){
											  String [] chrageDetailArr = chargeStr.split("#");
											  accountLine.setRecGl(chrageDetailArr[1]);
											  accountLine.setPayGl(chrageDetailArr[2]);
											}else{
											  accountLine.setRecGl("");
											  accountLine.setPayGl("");
										  }
									}
							  } catch(Exception e){
								e.printStackTrace();  
							  }
							  accountLine= accountLineManager.save(accountLine);
							  sid=serviceOrder.getId();
							  String jobName="";
							  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
							    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
							    	jobName = tempCommissionJob.get(0).toString();
							    }
							    if(jobName.contains(serviceOrder.getJob())){
							    	calculateCommisssionMethod();
							    }
							  
					      }else{
					    	
					    	  vanlineStatus=false;
					      }
					//	7554 - Display with Reconcile Start
						  /*  try{
								  String accIdList="";
								  if((vanLine.getGlType()!=null)&&(!vanLine.getGlType().equalsIgnoreCase(""))&&(vanLine.getRegNum()!=null)&&(!vanLine.getRegNum().equalsIgnoreCase(""))){
									  accIdList=refMasterManager.checkInvoiceDateForWithReconcile(sessionCorpID,vanLine.getGlType(),vanLine.getRegNum());
								  }
								  if(!accIdList.equalsIgnoreCase("")){
									  String  invDate1 ="";
									  try{
										  if(vanLine.getWeekEnding()!=null) {
							        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
							        			invDate1	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
										  }}catch(Exception e){
											  
										  }									  
									  String accId[]=accIdList.split(",");
									  for(int i=0;i<accId.length;i++){
										  AccountLine accLineNew=accountLineManager.get(Long.parseLong(accId[i]));
										  accLineNew.setExternalReference("UVL "+invDate1);
										  accLineNew.setUpdatedOn(new Date());
										  accLineNew.setUpdatedBy("Vanline : "+getRequest().getRemoteUser());		
										  accLineNew.setRecPostDate(recAccDateToFormat);
										  accLineNew.setRecInvoiceNumber(recInvoiceNumber);
										  accLineNew.setReceivedInvoiceDate(new Date());
										  accLineNew.setInvoiceCreatedBy(getRequest().getRemoteUser());
										  accountLineManager.save(accLineNew);
									  }
								  }
						    }catch(Exception e){}*/
							    
					//	7554 - Display with Reconcile End						      
					   
					 }
				
				}
				if(vanlineStatus){
				int k=vanLineManager.updateVanLineWithReconcile(vanLine.getId(),loginUser);	
				//vanLine.setReconcileStatus("Reconcile");
				//vanLine.setAccCreatedOn(new Date());
				//vanLine = vanLineManager.save(vanLine);
				}
			 }
			 }
			 
		 }
	  vanLineManager.updateAccountLineWithReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID, uvlBillToCode ); 
	  List accIdList= new ArrayList();
	  accIdList=vanLineManager.checkInvoiceDateForWithReconcile(agent.replaceAll(":", "''").replaceAll("'", "''"), weekEnding,vanlineMinimumAmount,automaticReconcile,sessionCorpID, uvlBillToCode);
	  Iterator iterator=accIdList.iterator();
	  String previousSoId="";
	  String previousBilltocode="";
	  String recInvoiceNumber1 = "";
 	  while (iterator.hasNext()) {
		  String idData=iterator.next().toString();
		  String [] idDataArr = idData.split("~");
		  try{ 
		  Long  id = Long.parseLong(idDataArr[0]) ; 
		  Long  soId = Long.parseLong(idDataArr[1]) ; 
		  String billtocode=idDataArr[2];
		  if(billtocode.toString().equalsIgnoreCase(uvlBillToCode)){
		  if((!(previousSoId.equalsIgnoreCase(idDataArr[1]))) || (!(previousBilltocode.equalsIgnoreCase(billtocode)))){
			  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
			  recInvoiceNumber1=invoiceNumber.toString();  
			  previousSoId=idDataArr[1];
			  previousBilltocode=billtocode;
		  }
		  // accLineNew=accountLineManager.get(id);
		  // accLineNew.setUpdatedOn(new Date());
		   //accLineNew.setUpdatedBy("Vanline : "+getRequest().getRemoteUser());		
		   //accLineNew.setRecPostDate(recAccDateToFormat);
		   //accLineNew.setRecInvoiceNumber(recInvoiceNumber1);
		  // accLineNew.setReceivedInvoiceDate(new Date());
		  // accLineNew.setInvoiceCreatedBy(getRequest().getRemoteUser());
		  // accountLineManager.save(accLineNew);
		 vanLineManager.updateAccRecInvoiceNumberWithReconcile(id,loginUser,recAccDateTo,recInvoiceNumber1);
		 sid=soId;
		 serviceOrder = serviceOrderManager.get(soId); 
		  String jobName="";
		  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
		    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
		    	jobName = tempCommissionJob.get(0).toString();
		    }
		    if(jobName.contains(serviceOrder.getJob())){
		    	calculateCommisssionMethod();
		    }
		  }
		  }catch(Exception e){
			  e.printStackTrace();
		  }
		  
	  }
	}
	}
	  vanLineList = vanLineManager.getVanLineListWithReconcile(agent, weekEnding,sessionCorpID);
	  if(vanLineList!=null && !vanLineList.isEmpty()){
			flagvanLineData="1";
		}
		else{
			flagvanLineData="0";
			
		}
	  chkWith="yes";
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String distributionRevenueListMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		SOListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private Map venderCodeDetails =new HashMap();
	@SkipValidation
	public String getDownloadAccList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		venderCodeDetails=vanLineManager.getvenderCodeDetails(shipNum,sessionCorpID);
		downloadXMLList = vanLineManager.getDownloadXMLList(shipNum,sessionCorpID,fileName);
		System.out.println("\nAccountlines : "+downloadXMLList.size());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String  getProcessedXML(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		processedXMLList = vanLineManager.getProcessedXMLList(shipNum,sessionCorpID); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String updateAccLineDowanloadMethod(){
		if(fileName!=null && !fileName.equals("") && !fileName.trim().equals(",")){
			fileName = fileName.replaceAll(",", "','");
			fileName = "'"+fileName+"'";
			if(venderDetailsList.trim().equalsIgnoreCase(",")){
				venderDetailsList="";
			}
			int i = vanLineManager.updateAccByFileName(fileName,fileNoVenderDetails,venderDetailsList,sessionCorpID ); 
			logger.warn(i+": accountlines download is true updated by file name");
		}
		/*if(removeFileVal!=null && !removeFileVal.equals("") && !removeFileVal.trim().equals(",")){
			removeFileVal = removeFileVal.replaceAll(",", "','");
			removeFileVal = "'"+removeFileVal+"'";
			int i = vanLineManager.updateDownloadByFileName(removeFileVal,sessionCorpID ); 
			logger.warn(i+": accountlines download is false updated by file name");
		}*/
		if(accID!=null && !accID.equals("") && !accID.trim().equals(",") ){
			if(venderDetailsList.trim().equalsIgnoreCase(",")){
				venderDetailsList="";
			}
			int i = vanLineManager.updateAccByID(accID,accVenderDetails,venderDetailsList,sessionCorpID );  
			logger.warn(i+": accountlines download is true updated by id");	
		}
		/*if(removeAccVal!=null && !removeAccVal.equals("") && !removeAccVal.trim().equals(",") ){
			int i = vanLineManager.updateDownloadByID(removeAccVal,sessionCorpID );  
			logger.warn(i+": accountlines download is false updated by id");	
		}*/
		searchServiceOrderMethod();
		return SUCCESS;
	}
	@SkipValidation
	public String searchServiceOrderMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		
	/*	shipNum="";
		regNum="";
		lastName="";
		firstName="";
		
		if(serviceOrder.getShipNumber()!=null){
			shipNum=serviceOrder.getShipNumber();
			shipNum = shipNum.replaceAll("-", "");
			shipNum = shipNum.replaceAll("'", "\\\\'");
		}
		
		if(serviceOrder.getRegistrationNumber()!=null){
			regNum= serviceOrder.getRegistrationNumber();
			regNum = regNum.replaceAll("'", "\\\\'");
		}
		
		if(serviceOrder.getLastName()!=null){
			lastName=serviceOrder.getLastName();
			lastName = lastName.replaceAll("'", "\\\\'");
		}
		
		if(serviceOrder.getFirstName()!=null){
			firstName=serviceOrder.getFirstName();
			firstName = firstName.replace("'", "\\\\'");
		}
			
			
		soList = vanLineManager.getSerachedSOList(shipNum,regNum,lastName,firstName,sessionCorpID);*/
		
		String registrationNum = CommonUtil.getRegistrationNum(serviceOrder.getRegistrationNumber(),"");
		
		SOListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		 List<SearchCriterion> searchCriteria = new ArrayList();
		 searchCriteria.add(new SearchCriterion("registrationNumber", registrationNum , SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
		 searchCriteria.add(new SearchCriterion("lastName", serviceOrder.getLastName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
         searchCriteria.add(new SearchCriterion("firstName", serviceOrder.getFirstName(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
         searchCriteria.add(new SearchCriterion("shipNumber", serviceOrder.getShipNumber().replaceAll("'", "").replace("-", "").trim(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
         pagingLookupManager.getAllRecordsPage(ServiceOrder.class, SOListExt, searchCriteria);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	@SkipValidation
	public String vanLineRegNum(){
		vanLineRegNumList=vanLineManager.getRegNumList(vanRegNum,agent,weekEnding);
		return SUCCESS;
	}
	@SkipValidation
	public String vanLineCategoryRegNumberListMethod(){
		vanLineRegNumList=vanLineManager.vanLineCategoryRegNumberListMethod(statementCategory,vanRegNum,agent,weekEnd);
		return SUCCESS;
	}
	@SkipValidation
	public String vanLineCategoryRegNumMethod(){
		vanLineRegNumList=vanLineManager.vanLineCategoryRegNum(vanRegNum,agent,weekEnd,sessionCorpID);
		return SUCCESS;
	}
	@SkipValidation
	public String  vanLineExtract(){
		
		return SUCCESS;
	}
	@SkipValidation
	public String vanLineExtractMgt()throws Exception {
		vanLineExtractList=vanLineManager.getvanLineExtract(agent, weekEnding,sessionCorpID);
		String str[]=new String[]{"RegistrationNumber","S/O #","DistributionCodeDescription","GL Type","Shipper Last Name","AmtDueAgent","ReconcileAmount","ReconcileStatus","RecInvoiceNumber","ActualRevenue","DistributionAmount"};
		
			ExcelCreator.getExcelCreator().createExcelForAttachment(getResponse(), "van Line Extract", str,vanLineExtractList, "vanLineExtract Info", ",");
	
		return SUCCESS;
	}
	@SkipValidation
	public String miscellaneousSettlementList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineList = vanLineManager.getMiscellaneousSettlement(agent, weekEnding,sessionCorpID);
		if(vanLineList!=null && !vanLineList.isEmpty()){
			flagvanLineData="1";
		}
		else{
			flagvanLineData="0";
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String categoryWithSettlementList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineList = vanLineManager.getCategoryWithSettlementList(agent, weekEnding,sessionCorpID);
		if(vanLineList!=null && !vanLineList.isEmpty()){
			flagvanLineData="1";
		}
		else{
			flagvanLineData="0";
			
		}
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String vanLineListWithoutReconcile() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		vanLineList = vanLineManager.getVanLineListWithoutReconcile(agent, weekEnding ,sessionCorpID);

		if(vanLineList!=null && !vanLineList.isEmpty()){
			flagvanLineData="1";
		}
		else{
			flagvanLineData="0";
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		chkWith="no";
		return SUCCESS;
	}

	public String saveOnTabChange() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		validateFormNav = "OK";
		String s = save();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return gotoPageString;
	}

	public VanLineAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}

	@SkipValidation
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		vanLineListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
		List<SearchCriterion> searchCriteria = new ArrayList();
		pagingLookupManager.getAllRecordsPage(VanLine.class, vanLineListExt, searchCriteria);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String weekendDateList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		vanLineList = vanLineManager.getVanLineListWithoutReconcile(agent, weekEnding,sessionCorpID);
		if(vanLineList!=null && !vanLineList.isEmpty()){
			flagvanLineData="1";
		}
		else{
			flagvanLineData="0";
			
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		return SUCCESS;
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			companyDivis=companyDivisionManager.findCompanyCodeListForVanLine(sessionCorpID);
			getComboList(sessionCorpID);
		}
		division=refMasterManager.findMapList(sessionCorpID,"JOB");
		chargeCodeList= vanLineGLTypeManager.findByParameter(sessionCorpID);
		sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
		automaticReconcile=	systemDefault.getAutomaticReconcile();
		if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
			ownbillingVanline=systemDefault.getOwnbillingVanline();
		}
		if(systemDefault.getAccountLineAccountPortalFlag() !=null ){
			accountLineAccountPortalFlag = 	systemDefault.getAccountLineAccountPortalFlag();
		}
		}
		}
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		logger.warn(" AJAX Call End: "+getRequest().getParameter("ajax")); 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	public String getComboList(String corpId) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		vanLineCodeList = vanLineManager.getVanLineCodeCompDiv(sessionCorpID);
		glType = vanLineGLTypeManager.findByParameter(sessionCorpID);
		/*glCode = vanLineGLTypeManager.findGlCode(sessionCorpID);*/
		glCode = refMasterManager.findByParameterOrderByCode(corpId, "GLCODES");
		if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
		categoryList=refMasterManager.findByCategoryCode("TSFT","VLSTMTCATEGORY");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 /*if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }*/
		if (id != null) {
			vanLine = vanLineManager.get(id);
			if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
				if(vanLine.getCompanyDivision()==null || vanLine.getCompanyDivision().toString().trim().equals("")){
				String companyDivision="";
				companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
				vanLine.setCompanyDivision(companyDivision);
				}
			}
			try{
				if(vanLine.getCompanyDivision()!=null && (!(vanLine.getCompanyDivision().equals("")))){	
				if(companyDivis.contains(vanLine.getCompanyDivision())){
					
				}else{
					companyDivis.add(vanLine.getCompanyDivision());	
				}
				}
				}catch(Exception e){
					e.printStackTrace();
				}
			if(vanLine.getDriverID()==null||vanLine.getDriverID().equalsIgnoreCase(""))
         	{
		    if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
		    for (SystemDefault systemDefault : sysDefaultDetail) {
		    	if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
					ownbillingVanline=systemDefault.getOwnbillingVanline();
				}
            if(systemDefault.getMiscellaneousdefaultdriver()!=null && (!systemDefault.getMiscellaneousdefaultdriver().equalsIgnoreCase(""))){
             vanLine.setDriverID(systemDefault.getMiscellaneousdefaultdriver());
        	 String driverLastName=partnerManager.findByOwnerOpLastNameByCode(systemDefault.getMiscellaneousdefaultdriver());
        	 if(driverLastName!=null){
        		 vanLine.setDriverName(driverLastName);	
        	 } 		 
             }  
         	 }
		     }
         	 }
			if(vanLine.getRegNum()!=null && !vanLine.getRegNum().equals("")){
				List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
				if(SOidList!=null && (!SOidList.isEmpty())){
					Iterator SOidIterator=SOidList.iterator();
					 while(SOidIterator.hasNext()){
						 String SoIdData=SOidIterator.next().toString();
						  Long  SoId = Long.parseLong(SoIdData)  ; 
						  //serviceOrder = serviceOrderManager.get(SoId);
						  billing=billingManager.get(SoId);
						  if(billing.getContract()!=null && !billing.equals("")){
					chargeCodeList=vanLineManager.getChargeCodeList(billing.getContract(),sessionCorpID);  
						  }
					 }
				}	
			}
		} else {
			vanLine = new VanLine();
			vanLine.setCreatedOn(new Date());
			vanLine.setUpdatedOn(new Date());
		}
		//getComboList(sessionCorpID);
		vanLine.setCorpID(sessionCorpID);
		splitCharge();
		totalAmount();
		if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String editSplit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			if (id != null) {
			vanLine = vanLineManager.get(id);
			try{
				if(vanLine.getCompanyDivision()!=null && (!(vanLine.getCompanyDivision().equals("")))){	
				if(companyDivis.contains(vanLine.getCompanyDivision())){
					
				}else{
					companyDivis.add(vanLine.getCompanyDivision());	
				}
				}
				}catch(Exception e){
					e.printStackTrace();
				}
		} else {
			vanLine = new VanLine();
			vanLine.setCreatedOn(new Date());
			vanLine.setUpdatedOn(new Date());
		}
		getComboList(sessionCorpID);
		vanLine.setCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String editMiscellaneousSattlementList()throws Exception {
		if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
		}
		vanLineIdCheck=vanLineIdCheck;
		vanLineStatementCategoryCheck=vanLineStatementCategoryCheck;
		if(vanLineIdCheck.equals("")||vanLineIdCheck.equals(","))
	  	  {
	  	  }
	  	 else
	  	  {
	  		vanLineIdCheck=vanLineIdCheck.trim();
	  	    if(vanLineIdCheck.indexOf(",")==0)
	  		 {
	  	    	vanLineIdCheck=vanLineIdCheck.substring(1);
	  		 }
	  		if(vanLineIdCheck.lastIndexOf(",")==vanLineIdCheck.length()-1)
	  		 {
	  			vanLineIdCheck=vanLineIdCheck.substring(0, vanLineIdCheck.length()-1);
	  		 }
	  	    String[] arrayVanLineId=vanLineIdCheck.split(",");
	  	  vanLine = vanLineManager.get(Long.parseLong(arrayVanLineId[0]));
	  	  }
		return SUCCESS;
	}
	
	@SkipValidation
	public String listSave()throws Exception {
		Boolean autuPopPostDate=false;
    	Date payDate=null;
    	String uvlBillToCode="";
    	String uvlBillToName="";
    	String mvlBillToCode ="";
    	String mvlBillToName ="";
		if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
			for (SystemDefault systemDefault : sysDefaultDetail) {
				automaticReconcile=	systemDefault.getAutomaticReconcile();
				uvlBillToCode=systemDefault.getUvlBillToCode();
				vanlineMinimumAmount=systemDefault.getVanlineMinimumAmount();
				if(systemDefault.getVanlineExceptionChargeCode()!=null){
					vanlineExceptionChargeCode =systemDefault.getVanlineExceptionChargeCode();
				}
				if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
					ownbillingVanline=systemDefault.getOwnbillingVanline();
				}
				autuPopPostDate=systemDefault.getAutoPayablePosting();
					payDate=systemDefault.getPostDate1();
					uvlBillToCode=systemDefault.getUvlBillToCode();
					uvlBillToName=systemDefault.getUvlBillToName();
					mvlBillToCode=systemDefault.getMvlBillToCode();
					mvlBillToName=systemDefault.getMvlBillToName();
					companyDivisionAcctgCodeUnique="";
					if(systemDefault.getCompanyDivisionAcctgCodeUnique()!=null){
						companyDivisionAcctgCodeUnique=systemDefault.getCompanyDivisionAcctgCodeUnique();	
					}
					baseCurrency="";
					if(systemDefault.getBaseCurrency()!=null){
						baseCurrency=systemDefault.getBaseCurrency();	
					}
					recAccDateTo=new String("");
				if(systemDefault.getPostDate()!=null){
					recAccDateTo=systemDefault.getPostDate().toString();
				}
				payAccDateTo=new String("");
				if(systemDefault.getPostDate1()!=null){
					payAccDateTo=systemDefault.getPostDate1().toString();
				}
			}
			}
		vanLineIdCheck=vanLineIdCheck;
		vanLineStatementCategoryCheck=vanLineStatementCategoryCheck;
		if(vanLineIdCheck.equals("")||vanLineIdCheck.equals(","))
	  	  {
	  	  }
	  	 else
	  	  {
	  		vanLineIdCheck=vanLineIdCheck.trim();
	  	    if(vanLineIdCheck.indexOf(",")==0)
	  		 {
	  	    	vanLineIdCheck=vanLineIdCheck.substring(1);
	  		 }
	  		if(vanLineIdCheck.lastIndexOf(",")==vanLineIdCheck.length()-1)
	  		 {
	  			vanLineIdCheck=vanLineIdCheck.substring(0, vanLineIdCheck.length()-1);
	  		 }
	  	    String[] arrayVanLineId=vanLineIdCheck.split(",");
	  	  int arrayLength = arrayVanLineId.length;
			for(int i=0;i<arrayLength;i++)
			 {
				try{
				vanLinelistData = vanLineManager.get(Long.parseLong(arrayVanLineId[i]));
				vanLinelistData.setStatementCategory(vanLine.getStatementCategory());
				vanLinelistData.setDriverID(vanLine.getDriverID());
				vanLinelistData.setDriverName(vanLine.getDriverName());
				vanLinelistData.setCompanyDivision(vanLine.getCompanyDivision());
				vanLinelistData.setReconcileStatus(vanLine.getReconcileStatus());
				vanLinelistData.setGlCode(vanLine.getGlCode());
				vanLinelistData.setNotes(vanLine.getNotes());
				vanLinelistData.setDivision(vanLine.getDivision());
				List findByOwnerList=null;
				if(vanLinelistData.getDriverID()!=null && (!(vanLinelistData.getDriverID().equals("") ))){
		    		findByOwnerList=vanLineManager.findByOwnerAndContact(vanLinelistData.getDriverID(), sessionCorpID);
		    	}
				if(vanLinelistData.getReconcileAmount()==null || ((vanLinelistData.getReconcileAmount().toString().equals(""))) ||  vanLinelistData.getReconcileAmount().doubleValue()==0.0 ){
					vanLinelistData.setReconcileAmount(vanLinelistData.getAmtDueAgent());	
				}
				if(vanLine.getStatementCategory()!=null && vanLine.getStatementCategory().toString().equalsIgnoreCase("MCLM") && (findByOwnerList==null || ((findByOwnerList.isEmpty()))) && vanLine.getDriverID()!=null && (!(vanLine.getDriverID().trim().equals("") ))){ 
					String key3 = "Driver ID is not valid.";
					errorMessage(getText(key3)); 
					saveValidateForm="NotOk";
					if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
						 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
					}
					return SUCCESS;
				
				}else{
					if(vanLinelistData.getAgent()!=null && (!(vanLinelistData.getAgent().equals("")))){
						if(vanLinelistData.getCompanyDivision()==null || vanLinelistData.getCompanyDivision().toString().trim().equals("")){
						String companyDivision="";
						companyDivision=vanLineManager.findCompanyDivision(vanLinelistData.getAgent(),sessionCorpID);
						vanLinelistData.setCompanyDivision(companyDivision);
						}
					}
					vanLinelistData= vanLineManager.save(vanLinelistData);
					 
					 
					 

					 if(((vanLinelistData.getRegNum()==null ) || (vanLinelistData.getRegNum().equals(""))) && ((vanLinelistData.getReconcileStatus()!=null) && (vanLinelistData.getReconcileStatus().equalsIgnoreCase("Approved"))) ){
					 
						subcontractorChargesExt = vanLineManager.splitCharge(Long.parseLong(arrayVanLineId[i]));
						if((subcontractorChargesExt.isEmpty())){
						if(automaticReconcile && ownbillingVanline){	
			        	subcontractorCharges=new SubcontractorCharges();  
			        	//vanLine=vanLineManager.get(vid);
			        	subcontractorCharges.setParentId(vanLinelistData.getId()); 
			        	subcontractorCharges.setDescription(vanLinelistData.getDescription());
			        	subcontractorCharges.setCorpID(vanLinelistData.getCorpID());
			        	subcontractorCharges.setBranch(vanLinelistData.getAgent());
			        	subcontractorCharges.setRegNumber(vanLinelistData.getRegNum()); 
			        	subcontractorCharges.setPersonType("A");
			        	subcontractorCharges.setVanLineNumber("A"+vanLinelistData.getId()); 
			        	subcontractorCharges.setGlCode(vanLinelistData.getGlCode());
			        	BigDecimal  multiplyValue = new BigDecimal(-1.0);
			        	if(vanLinelistData.getAmtDueAgent()!=null){ 
			        	subcontractorCharges.setAmount(vanLinelistData.getAmtDueAgent().multiply(multiplyValue));
			        	}
			        	if(vanLinelistData.getAgent()!=null && (!(vanLinelistData.getAgent().equals("")))){
						String companyDivision="";
						companyDivision=vanLineManager.findCompanyDivision(vanLinelistData.getAgent(),sessionCorpID);
						subcontractorCharges.setCompanyDivision(companyDivision);  
						if(vanLinelistData.getAgent().indexOf("U")==0){
							subcontractorCharges.setPersonId(uvlBillToCode);
							subcontractorCharges.setPersonName(uvlBillToName);
						}
						if(vanLinelistData.getAgent().indexOf("M")==0){
							subcontractorCharges.setPersonId(mvlBillToCode);
							subcontractorCharges.setPersonName(mvlBillToName);
							
						}
						}
			        	subcontractorCharges.setApproved(vanLinelistData.getWeekEnding());
			        	if(ownbillingVanline){
			        		try{
				        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
				        		String strDate = sm.format(new Date());
				        		Date dt = sm.parse(strDate);
					        	subcontractorCharges.setApproved(dt);
				        		}catch(Exception e){
				        			e.printStackTrace();
				        		}	
			        	}
			        	subcontractorCharges.setAdvanceDate(vanLinelistData.getWeekEnding());
			        	subcontractorCharges.setDivision(vanLinelistData.getDivision());
			        	
			 			 
							try{ 
					    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
					    			Date dt1=vanLinelistData.getWeekEnding();
						    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						    		String dt11=sdfDestination.format(dt1);
						    		Date dt2 =null;
						    		try{
						    		dt2 = new Date();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}				    		
						    		
						    		dt2=payDate;
						    		if(dt2!=null){
							    		if(dt1.compareTo(dt2)>0){
							    			payDate=dt2;
							    		}else{
							    			payDate=dt1;
							    		}
						    		}
					    		}
						}catch(Exception e){
							e.printStackTrace();
						}  	 
			 			 
			 			if(payDate!=null){ 
			 				subcontractorCharges.setPost(payDate); 
		 				} 
			        	subcontractorCharges.setCreatedOn(new Date());
			        	subcontractorCharges.setUpdatedOn(new Date());
			        	subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
			         	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
			         	subcontractorCharges.setDeductTempFlag(false);
			         	subcontractorCharges.setIsSettled(false);
			        	subcontractorChargesManager.save(subcontractorCharges);
			        	
						}
						if((vanLinelistData.getDriverID()!=null) && (!(vanLinelistData.getDriverID().toString().trim().equals("")))){
			        	SubcontractorCharges subcontractorCharges1=new SubcontractorCharges();  
			        	//vanLine=vanLineManager.get(vid);
			        	subcontractorCharges1.setParentId(vanLinelistData.getId()); 
			        	subcontractorCharges1.setDescription(vanLinelistData.getDescription());
			        	subcontractorCharges1.setCorpID(vanLinelistData.getCorpID());
			        	subcontractorCharges1.setBranch(vanLinelistData.getAgent());
			        	subcontractorCharges1.setRegNumber(vanLinelistData.getRegNum()); 
			        	subcontractorCharges1.setVanLineNumber("D"+vanLinelistData.getId());
			        	subcontractorCharges1.setPersonType("D");
			        	subcontractorCharges1.setPersonId(vanLinelistData.getDriverID());
			        	subcontractorCharges1.setPersonName(vanLinelistData.getDriverName());
			        	subcontractorCharges1.setGlCode(vanLinelistData.getGlCode());
			        	//BigDecimal  multiplyValue = new BigDecimal(-1.0);
			        	if(ownbillingVanline){
			        	if(vanLinelistData.getAmtDueAgent()!=null){ 
			        		subcontractorCharges1.setAmount(vanLinelistData.getAmtDueAgent());
			        	}
			        	}else{
			        		if(automaticReconcile){
			        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
				        	if(vanLinelistData.getAmtDueAgent()!=null){ 
				        	subcontractorCharges1.setAmount(vanLinelistData.getAmtDueAgent().multiply(multiplyValue));
				        	}
			        		}else{ 
			    	        	if(vanLinelistData.getAmtDueAgent()!=null){ 
			    	        		subcontractorCharges1.setAmount(vanLinelistData.getAmtDueAgent());
			    	        	} 
			        		}
			        	}
			        	subcontractorCharges1.setCompanyDivision(vanLinelistData.getCompanyDivision());
			        	
			        	subcontractorCharges1.setApproved(vanLinelistData.getWeekEnding());  
			        	if(ownbillingVanline){
			        		try{
				        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
				        		String strDate = sm.format(new Date());
				        		Date dt = sm.parse(strDate);
					        	subcontractorCharges1.setApproved(dt);
				        		}catch(Exception e){
				        			e.printStackTrace();
				        		}
				        }
			        	subcontractorCharges1.setAdvanceDate(vanLinelistData.getWeekEnding());
			        	subcontractorCharges1.setDivision(vanLinelistData.getDivision());
			 			 
							try{
								 
					    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
					    			Date dt1=vanLinelistData.getWeekEnding();
						    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						    		String dt11=sdfDestination.format(dt1);
						    		Date dt2 =null;
						    		try{
						    		dt2 = new Date();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}				    		
						    		
						    		dt2=payDate;
						    		if(dt2!=null){
							    		if(dt1.compareTo(dt2)>0){
							    			payDate=dt2;
							    		}else{
							    			payDate=dt1;
							    		}
						    		}
					    		}
						}catch(Exception e){
							e.printStackTrace();
						}  	 
			 			 
			 			if(payDate!=null){ 
			 				subcontractorCharges1.setPost(payDate); 
		 				} 
			 			subcontractorCharges1.setCreatedOn(new Date());
			 			subcontractorCharges1.setUpdatedOn(new Date());
			 			subcontractorCharges1.setCreatedBy(getRequest().getRemoteUser());
			 			subcontractorCharges1.setUpdatedBy(getRequest().getRemoteUser()); 
			 			subcontractorCharges1.setDeductTempFlag(false);
			 			subcontractorCharges1.setIsSettled(false);
			        	subcontractorChargesManager.save(subcontractorCharges1);
						} 
					}else{
						try{
						Iterator<SubcontractorCharges> 	iterator = subcontractorChargesExt.iterator();
						while(iterator.hasNext()){
						 subcontractorCharges =iterator.next();
						 if((subcontractorCharges.getVanLineNumber() !=null) && (!(subcontractorCharges.getVanLineNumber().toString().trim().equals(""))) && subcontractorCharges.getAccountSent()==null){
							
							 if(subcontractorCharges.getVanLineNumber().toString().trim().equals("A"+vanLinelistData.getId())){
									subcontractorCharges.setParentId(vanLinelistData.getId()); 
						        	subcontractorCharges.setDescription(vanLinelistData.getDescription());
						        	subcontractorCharges.setCorpID(vanLinelistData.getCorpID());
						        	subcontractorCharges.setBranch(vanLinelistData.getAgent());
						        	subcontractorCharges.setRegNumber(vanLinelistData.getRegNum()); 
						        	 
						        	subcontractorCharges.setGlCode(vanLinelistData.getGlCode());
						        	BigDecimal  multiplyValue = new BigDecimal(-1.0);
						        	if(vanLinelistData.getAmtDueAgent()!=null){ 
						        	subcontractorCharges.setAmount(vanLinelistData.getAmtDueAgent().multiply(multiplyValue));
						        	}
						        	if(vanLinelistData.getAgent()!=null && (!(vanLinelistData.getAgent().equals("")))){
									  
									 
									if(vanLinelistData.getAgent().indexOf("U")==0){
										 
									}
									if(vanLinelistData.getAgent().indexOf("M")==0){
										 
										
									}
									}
						        	subcontractorCharges.setApproved(vanLinelistData.getWeekEnding()); 
						        	if(ownbillingVanline){
						        		try{
							        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
							        		String strDate = sm.format(new Date());
							        		Date dt = sm.parse(strDate);
								        	subcontractorCharges.setApproved(dt);
							        		}catch(Exception e){
							        			e.printStackTrace();
							        		}	
							        }
						        	subcontractorCharges.setAdvanceDate(vanLinelistData.getWeekEnding());
						        	subcontractorCharges.setDivision(vanLinelistData.getDivision());
						 			//if(autuPopPostDate){
										//change for posting date flexiblity Start	
										try{
											//company=companyManager.findByCorpID(sessionCorpID).get(0);
								    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
								    			Date dt1=vanLinelistData.getWeekEnding();
									    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
									    		String dt11=sdfDestination.format(dt1);
									    		Date dt2 =null;
									    		try{
									    		dt2 = new Date();
									    		}catch(Exception e){
									    			e.printStackTrace();
									    		}				    		
									    		
									    		dt2=payDate;
									    		if(dt2!=null){
										    		if(dt1.compareTo(dt2)>0){
										    			payDate=dt2;
										    		}else{
										    			payDate=dt1;
										    		}
									    		}
								    		}
									}catch(Exception e){
										e.printStackTrace();
									}  	 
						 			//}
						 			if(payDate!=null){ 
						 				subcontractorCharges.setPost(payDate); 
					 				} 
						        	//subcontractorCharges.setCreatedOn(new Date());
						        	subcontractorCharges.setUpdatedOn(new Date());
						        	//subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
						         	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
						         	//subcontractorCharges.setDeductTempFlag(false);
						         	//subcontractorCharges.setIsSettled(false);
						        	subcontractorChargesManager.save(subcontractorCharges);
						        	
										
							}
							 if(subcontractorCharges.getVanLineNumber().toString().trim().equals("D"+vanLinelistData.getId())){
								 subcontractorCharges.setParentId(vanLinelistData.getId()); 
								 subcontractorCharges.setDescription(vanLinelistData.getDescription());
								 subcontractorCharges.setCorpID(vanLinelistData.getCorpID());
								 subcontractorCharges.setBranch(vanLinelistData.getAgent());
								 subcontractorCharges.setRegNumber(vanLinelistData.getRegNum()); 
								 subcontractorCharges.setVanLineNumber("D"+vanLinelistData.getId());
								 subcontractorCharges.setPersonType("D");
								 subcontractorCharges.setPersonId(vanLinelistData.getDriverID());
								 subcontractorCharges.setPersonName(vanLinelistData.getDriverName());
								 subcontractorCharges.setGlCode(vanLinelistData.getGlCode());
						        	//BigDecimal  multiplyValue = new BigDecimal(-1.0);
								 if(ownbillingVanline){
						        	if(vanLinelistData.getAmtDueAgent()!=null){ 
						        		subcontractorCharges.setAmount(vanLinelistData.getAmtDueAgent());
						        	}
								 }else{
									 if(automaticReconcile){
						        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
							        	if(vanLinelistData.getAmtDueAgent()!=null){ 
							        		subcontractorCharges.setAmount(vanLinelistData.getAmtDueAgent().multiply(multiplyValue));
							        	}
									 }else{ 
								      if(vanLinelistData.getAmtDueAgent()!=null){ 
								        		subcontractorCharges.setAmount(vanLinelistData.getAmtDueAgent());
								        	} 
									 }
								 }
						        	subcontractorCharges.setCompanyDivision(vanLinelistData.getCompanyDivision());
						        	
						        	subcontractorCharges.setApproved(vanLinelistData.getWeekEnding()); 
						        	if(ownbillingVanline){
						        		try{
							        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
							        		String strDate = sm.format(new Date());
							        		Date dt = sm.parse(strDate);
								        	subcontractorCharges.setApproved(dt);
							        		}catch(Exception e){
							        			e.printStackTrace();
							        		}
								    }
						        	subcontractorCharges.setAdvanceDate(vanLinelistData.getWeekEnding());
						        	subcontractorCharges.setDivision(vanLinelistData.getDivision());
						 			//if(autuPopPostDate){
										//change for posting date flexiblity Start	
										try{
											//company=companyManager.findByCorpID(sessionCorpID).get(0);
								    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
								    			Date dt1=vanLinelistData.getWeekEnding();
									    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
									    		String dt11=sdfDestination.format(dt1);
									    		Date dt2 =null;
									    		try{
									    		dt2 = new Date();
									    		}catch(Exception e){
									    			e.printStackTrace();
									    		}				    		
									    		
									    		dt2=payDate;
									    		if(dt2!=null){
										    		if(dt1.compareTo(dt2)>0){
										    			payDate=dt2;
										    		}else{
										    			payDate=dt1;
										    		}
									    		}
								    		}
									}catch(Exception e){
										e.printStackTrace();
									}  	 
						 			//}
						 			if(payDate!=null){ 
						 				subcontractorCharges.setPost(payDate); 
					 				} 
						 			 
						 			subcontractorCharges.setUpdatedOn(new Date());
						 			 
						 			subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
						 			 
						        	subcontractorChargesManager.save(subcontractorCharges);
									
								 
							 }
						 }
						}
						}catch(Exception e){
							e.printStackTrace();
						}
						
					}
					
					 
					 
					 
					 
				}
				}
				}catch(Exception e){
				e.printStackTrace();	
				}
			 }
			vanlineSaveFlag=true; 
	  	  }
		return SUCCESS;
	
	}
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		boolean isNew = (vanLine.getId() == null);
		Boolean autuPopPostDate=false;
    	Date payDate=null;
    	String uvlBillToCode="";
    	String uvlBillToName="";
    	String mvlBillToCode ="";
    	String mvlBillToName ="";
    	List findByOwnerList=null;
    	if(vanLine.getDriverID()!=null && (!(vanLine.getDriverID().equals("") ))){
    		findByOwnerList=vanLineManager.findByOwnerAndContact(vanLine.getDriverID(), sessionCorpID);
    	}
		if (isNew) {
			vanLine.setCreatedOn(new Date());
		}
		vanLine.setUpdatedOn(new Date());
		vanLine.setUpdatedBy(getRequest().getRemoteUser());
		vanLine.setCorpID(sessionCorpID);
		if (vanLine.getNotes().length() > 1500) {
			String key1 = "Notes cannot have more than 1500 characters.";
			errorMessage(getText(key1));
			if (!isNew) {
				splitChargeOnsave();
			}
			saveValidateForm="NotOk";
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			}
			return SUCCESS;
		} else if (vanLine.getAgent().indexOf(":") == 0) {
			String key2 = "Do not add ':' at first character on agency #";
			errorMessage(getText(key2));
			if (!isNew) {
				splitChargeOnsave();
			}
			saveValidateForm="NotOk";
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			}
			return SUCCESS;
		} else if (vanLine.getAgent().indexOf("'") > 0) {
			String key3 = "Do not add \' on agency #";
			errorMessage(getText(key3));
			if (!isNew) {
				splitChargeOnsave();
			}
			saveValidateForm="NotOk";
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			}
			return SUCCESS;
		}else if(vanLine.getStatementCategory()!=null && vanLine.getStatementCategory().toString().equalsIgnoreCase("MCLM") && (findByOwnerList==null || ((findByOwnerList.isEmpty()))) && vanLine.getDriverID()!=null && (!(vanLine.getDriverID().trim().equals("") ))){ 
			String key3 = "Driver ID is not valid.";
			errorMessage(getText(key3));
			if (!isNew) {
				splitChargeOnsave();
			}
			saveValidateForm="NotOk";
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			}
			return SUCCESS;
		
		} else {
			if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
				if(vanLine.getCompanyDivision()==null || vanLine.getCompanyDivision().toString().trim().equals("")){
				String companyDivision="";
				companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
				vanLine.setCompanyDivision(companyDivision);
				}
			} 
			vanlineSaveFlag=true; 
			vanLine = vanLineManager.save(vanLine);
			if(vanLine.getRegNum()!=null && !vanLine.getRegNum().equals("")){
				List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
				if(SOidList!=null && (!SOidList.isEmpty())){
					Iterator SOidIterator=SOidList.iterator();
					 while(SOidIterator.hasNext()){
						 String SoIdData=SOidIterator.next().toString();
						  Long  SoId = Long.parseLong(SoIdData)  ; 
						  //serviceOrder = serviceOrderManager.get(SoId);
						  billing=billingManager.get(SoId);
						  if(billing.getContract()!=null && !billing.equals("")){
					chargeCodeList=vanLineManager.getChargeCodeList(billing.getContract(),sessionCorpID);  
						  }
					 }
				}	
			}
			try{
			//sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					automaticReconcile=	systemDefault.getAutomaticReconcile();
					uvlBillToCode=systemDefault.getUvlBillToCode();
					vanlineMinimumAmount=systemDefault.getVanlineMinimumAmount();
					if(systemDefault.getVanlineExceptionChargeCode()!=null){
						vanlineExceptionChargeCode =systemDefault.getVanlineExceptionChargeCode();
					}
					if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
						ownbillingVanline=systemDefault.getOwnbillingVanline();
					}
					autuPopPostDate=systemDefault.getAutoPayablePosting();
 					payDate=systemDefault.getPostDate1();
 					uvlBillToCode=systemDefault.getUvlBillToCode();
 					uvlBillToName=systemDefault.getUvlBillToName();
 					mvlBillToCode=systemDefault.getMvlBillToCode();
 					mvlBillToName=systemDefault.getMvlBillToName();
 					companyDivisionAcctgCodeUnique="";
 					if(systemDefault.getCompanyDivisionAcctgCodeUnique()!=null){
 						companyDivisionAcctgCodeUnique=systemDefault.getCompanyDivisionAcctgCodeUnique();	
 					}
 					baseCurrency="";
 					if(systemDefault.getBaseCurrency()!=null){
 						baseCurrency=systemDefault.getBaseCurrency();	
 					}
 					recAccDateTo=new String("");
					if(systemDefault.getPostDate()!=null){
						recAccDateTo=systemDefault.getPostDate().toString();
					}
					payAccDateTo=new String("");
					if(systemDefault.getPostDate1()!=null){
						payAccDateTo=systemDefault.getPostDate1().toString();
					}
				}
				}
				
			if(automaticReconcile){
			if(ownbillingVanline){
				boolean vanlineExceptionChargeCodeStatus=true;
				boolean vanlineStatus=false;
				if(vanLine.getReconcileAmount()==null || ((vanLine.getReconcileAmount().toString().equals(""))) ||  vanLine.getReconcileAmount().doubleValue()==0.0 ){
					vanLine.setReconcileAmount(vanLine.getAmtDueAgent());	
				}
				if(vanlineExceptionChargeCode!=null && (!(vanlineExceptionChargeCode.equals(""))) && vanLine.getGlType()!=null &&  vanlineExceptionChargeCode.contains(vanLine.getGlType()) ){
					  vanlineExceptionChargeCodeStatus=false;    
				  }
				if(vanLine.getRegNum()!=null && (!(vanLine.getRegNum().equals(""))) && (vanLine.getReconcileStatus()!=null) && (vanLine.getReconcileStatus().equalsIgnoreCase("Approved")) &&  vanLine.getGlType()!=null && (!(vanLine.getGlType().equals(""))) && vanLine.getReconcileAmount()!=null && (!(vanLine.getReconcileAmount().toString().equals(""))) &&  vanLine.getReconcileAmount().doubleValue()!=0.0 && vanlineExceptionChargeCodeStatus  ){

					   
						//change for posting date flexiblity Start	
						try{
							//company=companyManager.findByCorpID(sessionCorpID).get(0);
				    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
					    		Date dt1=new Date();
					    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
					    		String dt11=sdfDestination.format(dt1);
					    		dt1=sdfDestination.parse(dt11);
					    		Date dt2=sdfDestination.parse(recAccDateTo);
					    		if(dt1.compareTo(dt2)>0){
					    			recAccDateTo=sdfDestination.format(dt2);
					    		}else{
					    			recAccDateTo=sdfDestination.format(dt1);
					    		}
				    		}
							}catch(Exception e){e.printStackTrace();}			
							//change for posting date flexiblity End				
						
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						try{ 
						if(!recAccDateTo.equals("")){	
							recAccDateToFormat=sdf.parse(recAccDateTo); 
							}else{
								recAccDateToFormat =null; 
							}
						}
						catch( Exception e)
						{
							e.printStackTrace();
						}
						
							List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
							if(SOidList!=null && (!SOidList.isEmpty())){
								Iterator SOidIterator=SOidList.iterator();
								String previousShipnumber="";
								String recInvoiceNumber = "";
								String creditInvoiceNumber = "NA";
								 while(SOidIterator.hasNext()){
									 String SoIdData=SOidIterator.next().toString();
									  Long  SoId = Long.parseLong(SoIdData)  ; 
									  serviceOrder = serviceOrderManager.get(SoId);
									  trackingStatus=trackingStatusManager.get(SoId);
									  miscellaneous = miscellaneousManager.get(SoId);
									  billing=billingManager.get(SoId);
									  List chargeid=new ArrayList();
									  String accRecGl="";
									  String accPayGl="";
									  try{
											if(!costElementFlag){
											 chargeid=chargesManager.findChargeId(vanLine.getGlType(),billing.getContract());
											 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
												 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
			                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
			                                    	  accRecGl= charge.getGl(); 
			                                      } 
			                                      if(charge.getExpGl()!=null && (!(charge.getExpGl().toString().trim().equals("")))) {
			                                    	  accPayGl= charge.getExpGl(); 
			                                      }
											 } 
											}else{
												 
												chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),vanLine.getGlType(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
												String chargeStr=""; 
												if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
													  chargeStr= chargeid.get(0).toString();
												  }
												   if(!chargeStr.equalsIgnoreCase("")){
													  String [] chrageDetailArr = chargeStr.split("#");
													  try{
													  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
													  accRecGl=(chrageDetailArr[1]); 
													  }
													  }catch(Exception e){
														  e.printStackTrace();
													  }
													  try{
														  if(chrageDetailArr[2]!=null && ((chrageDetailArr[2].toString().equals("")))) {	  
															  accPayGl=(chrageDetailArr[2]); 
														  }
														  }catch(Exception e){
															  e.printStackTrace();
														  }
												   }
											}
									  } catch(Exception e){
										  e.printStackTrace();
									  }
									  if(vanLine.getStatementCategory()!=null && vanLine.getStatementCategory().toString().equalsIgnoreCase("MCLM")){
										  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accPayGl.trim().equals(""))) && (vanLine.getAmtDueAgent()!=null ) && (!(vanLine.getAmtDueAgent().toString().equals(""))) &&  (vanLine.getAmtDueAgent().doubleValue()!=0.0 )){
									    	  
											  String loginUser = getRequest().getRemoteUser();
											  String actCode="";
									    	  accountLine = new AccountLine();
											  accountLine.setActivateAccPortal(true); 
											  boolean activateAccPortal =true;
												try{
												if(accountLineAccountPortalFlag){	
												activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
												accountLine.setActivateAccPortal(activateAccPortal);
												}
												}catch(Exception e){
												e.printStackTrace();	
												}
											  accountLine.setBasis("");
											  accountLine.setCategory("Transport");
											  accountLine.setChargeCode(vanLine.getGlType());
												try {
													String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
													if(!ss.equalsIgnoreCase("")){
														accountLine.setAccountLineCostElement(ss.split("~")[0]);
														accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
													}
												} catch (Exception e1) {
													e1.printStackTrace();
												}
											  											  
											  accountLine.setContract(billing.getContract());
											  if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
													String companyDivision="";
													companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
													accountLine.setCompanyDivision(companyDivision); 
													if(vanLine.getAgent().indexOf("U")==0){
														accountLine.setVendorCode(uvlBillToCode);
														accountLine.setEstimateVendorName(uvlBillToName);
													}
													if(vanLine.getAgent().indexOf("M")==0){
														accountLine.setVendorCode(mvlBillToCode);
														accountLine.setEstimateVendorName(mvlBillToName);
														
													}
													
													if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
											 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),accountLine.getCompanyDivision(),sessionCorpID);
											 		}else{
											 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
											 		}
											         accountLine.setActgCode(actCode);	
													}
											  if(actCode !=null && (!(actCode.trim().equals("")))){
								        		baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
								        		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
									    		{
									    		accountLine.setEstCurrency(baseCurrency);
								    			accountLine.setRevisionCurrency(baseCurrency);	
								    			accountLine.setCountry(baseCurrency);
								    			accountLine.setEstValueDate(new Date());
								    			accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								    			accountLine.setRevisionValueDate(new Date());
								    			accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
								    			accountLine.setValueDate(new Date());
								    			accountLine.setExchangeRate(new Double(1.0));
									    		}
									    		else
									    		{
									    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
								    			accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
								    			accountLine.setCountry(baseCurrencyCompanyDivision);
								    			accountLine.setEstValueDate(new Date());
								    			accountLine.setRevisionValueDate(new Date());
								    			accountLine.setValueDate(new Date());
								    			//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
								    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
								    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								    				try{
								    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								    				}catch(Exception e){
								    					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
									    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
									    				accountLine.setExchangeRate(new Double(1.0));
									    				e.printStackTrace();
								    				}
								    			}else{
								    				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								    				accountLine.setExchangeRate(new Double(1.0));
								    			}
									    		}
								        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
									        	if(vanLine.getAmtDueAgent()!=null){ 
									        		accountLine.setLocalAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
									        	}
									        	if(accountLine.getLocalAmount()!=null){
								        		if(accountLine.getExchangeRate()==1.00){
									            	 accountLine.setActualExpense(accountLine.getLocalAmount());
									             }else{
									            	 Double  D1= accountLine.getLocalAmount().doubleValue();
									            	 Double D2=D1/accountLine.getExchangeRate();
									            	 accountLine.setActualExpense(new BigDecimal(D2));
									             }	
									        	}
									        	String  invDate ="";
									            try{
												 if(vanLine.getWeekEnding()!=null) {
										        	 SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
										        	 invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
												 }}catch(Exception e){
													 e.printStackTrace();
												 }
									            String invoiceNumber=serviceOrder.getId()+accountLine.getVendorCode();
												 accountLine.setInvoiceNumber(invoiceNumber);
												 accountLine.setInvoiceDate(new Date());
												 accountLine.setReceivedDate(vanLine.getWeekEnding());
												 accountLine.setPayingStatus("A");
												 accountLine.setPayPayableStatus("Fully Paid");
												 accountLine.setPayPayableDate(vanLine.getWeekEnding()); 
												 accountLine.setPayPayableVia("UVL "+invDate);  
												 accountLine.setPayPayableAmount(vanLine.getAmtDueAgent().multiply(multiplyValue)); 
												 accountLine.setCorpID(sessionCorpID);
												 accountLine.setStatus(true);
												 accountLine.setServiceOrder(serviceOrder);
												 accountLine.setServiceOrderId(serviceOrder.getId());
												 accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
												 accountLine.setShipNumber(serviceOrder.getShipNumber());
												 accountLine.setCreatedOn(new Date());
												 accountLine.setCreatedBy("Vanline : "+loginUser);
												 accountLine.setUpdatedOn(new Date());
												 accountLine.setUpdatedBy("Vanline : "+loginUser);
												 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
										         if ( maxLineNumber.get(0) == null ) {          
										         	accountLineNumber = "001";
										         }else {
										         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
										             if((autoLineNumber.toString()).length() == 2) {
										             	accountLineNumber = "0"+(autoLineNumber.toString());
										             }
										             else if((autoLineNumber.toString()).length() == 1) {
										             	accountLineNumber = "00"+(autoLineNumber.toString());
										             } 
										             else {
										             	accountLineNumber=autoLineNumber.toString();
										             }
										         }
										         accountLine.setAccountLineNumber(accountLineNumber);
											  try{
												    
												  if(!costElementFlag){
													Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));  
														accountLine.setRecGl(charge.getGl());
														accountLine.setPayGl(charge.getExpGl());
														accountLine.setNote(charge.getDescription());
													}else{
														String chargeStr="";
														   
														if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
															  chargeStr= chargeid.get(0).toString();
														  }
														   if(!chargeStr.equalsIgnoreCase("")){
															  String [] chrageDetailArr = chargeStr.split("#");
															  accountLine.setRecGl(chrageDetailArr[1]);
															  accountLine.setPayGl(chrageDetailArr[2]);
															}else{
															  accountLine.setRecGl("");
															  accountLine.setPayGl("");
														  }
													}
											  } catch(Exception e){
												e.printStackTrace();  
											  }
											  accountLine= accountLineManager.save(accountLine); 
											  }
											 if(vanLine.getDriverID()!=null && (!(vanLine.getDriverID().toString().trim().equals("")))){  
											  AccountLine accountLine1 = new AccountLine();
											  accountLine1.setActivateAccPortal(true);  
											  //boolean activateAccPortal =true;
												try{
												if(accountLineAccountPortalFlag){	
												activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
												accountLine1.setActivateAccPortal(activateAccPortal);
												}
												}catch(Exception e){
												e.printStackTrace();
												}
											  accountLine1.setBasis("");
											  accountLine1.setCategory("Transport");
											  accountLine1.setChargeCode(vanLine.getGlType());
												try {
													String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
													if(!ss.equalsIgnoreCase("")){
														accountLine.setAccountLineCostElement(ss.split("~")[0]);
														accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
													}
												} catch (Exception e1) {
													e1.printStackTrace();
												}
											  
											  accountLine1.setContract(billing.getContract());
											  accountLine1.setCompanyDivision(vanLine.getCompanyDivision()); 
											  accountLine1.setVendorCode(vanLine.getDriverID());
											  accountLine1.setEstimateVendorName(vanLine.getDriverName());
											   String actCode1="";
											   if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
											 			actCode1=partnerManager.getAccountCrossReference(accountLine1.getVendorCode(),accountLine1.getCompanyDivision(),sessionCorpID);
											   }else{
											 			actCode1=partnerManager.getAccountCrossReferenceUnique(accountLine1.getVendorCode(),sessionCorpID);
											   }
											   accountLine1.setActgCode(actCode1);	
											   if(actCode1==null || (actCode1.trim().equals(""))){
												   try{
												   if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
													List actCodeList= partnerManager.findPartnerForActgCodeVanLine(accountLine1.getVendorCode(),sessionCorpID); 
													if(actCodeList!=null && (!(actCodeList.isEmpty())) && actCodeList.get(0)!=null && (!(actCodeList.get(0).toString().trim().equals("")))){
													String	actCodedata=actCodeList.get(0).toString().trim();
													String [] actCodeDetailArr = actCodedata.split("~");
													  actCode1=actCodeDetailArr[0];
													  String CompanyDivision=actCodeDetailArr[1];
													  if(!CompanyDivision.equals("NO")){
													   accountLine1.setCompanyDivision(CompanyDivision); 	  
													  }
													   
													}
												   }
											   }catch(Exception e){
												   e.printStackTrace();
											   }
											   }
											   if(actCode1 !=null && (!(actCode1.trim().equals("")))){	  
								        		baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine1.getCompanyDivision(),sessionCorpID);
								        		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
									    		{
								        		accountLine1.setEstCurrency(baseCurrency);
								        		accountLine1.setRevisionCurrency(baseCurrency);	
								        		accountLine1.setCountry(baseCurrency);
								        		accountLine1.setEstValueDate(new Date());
								        		accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
								        		accountLine1.setRevisionValueDate(new Date());
								        		accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000)); 
								        		accountLine1.setValueDate(new Date());
								        		accountLine1.setExchangeRate(new Double(1.0));
									    		}
									    		else
									    		{
									    		accountLine1.setEstCurrency(baseCurrencyCompanyDivision);
									    		accountLine1.setRevisionCurrency(baseCurrencyCompanyDivision);
									    		accountLine1.setCountry(baseCurrencyCompanyDivision);
									    		accountLine1.setEstValueDate(new Date());
									    		accountLine1.setRevisionValueDate(new Date());
									    		accountLine1.setValueDate(new Date());
								    			//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
								    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
								    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								    				try{
								    				accountLine1.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine1.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine1.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								    				}catch(Exception e){
								    					accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
								    					accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
								    					accountLine1.setExchangeRate(new Double(1.0));	
								    					e.printStackTrace();
								    				}
								    			}else{
								    				accountLine1.setEstExchangeRate(new BigDecimal(1.0000)); 
								    				accountLine1.setRevisionExchangeRate(new BigDecimal(1.0000));
								    				accountLine1.setExchangeRate(new Double(1.0));
								    			}
									    		}
								        		 
									        	if(vanLine.getAmtDueAgent()!=null){ 
									        		accountLine1.setLocalAmount(vanLine.getAmtDueAgent());
									        	}
									        	if(accountLine1.getLocalAmount()!=null){
								        		if(accountLine1.getExchangeRate()==1.00){
								        			accountLine1.setActualExpense(accountLine1.getLocalAmount());
									             }else{
									            	 Double  D1= accountLine1.getLocalAmount().doubleValue();
									            	 Double D2=D1/accountLine1.getExchangeRate();
									            	 accountLine1.setActualExpense(new BigDecimal(D2));
									             }	
									        	} 
									        	String  invDate ="";
									            try{
												 if(vanLine.getWeekEnding()!=null) {
										        	 SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
										        	 invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
												 }}catch(Exception e){
													 e.printStackTrace();
												 }
									             String invoiceNumber=serviceOrder.getId()+accountLine1.getVendorCode();
												 accountLine1.setInvoiceNumber(invoiceNumber);
												 accountLine1.setInvoiceDate(new Date());
												 accountLine1.setReceivedDate(vanLine.getWeekEnding());
												 accountLine1.setPayingStatus("A");
												 accountLine1.setPayPayableStatus("Fully Paid");
												 accountLine1.setPayPayableDate(vanLine.getWeekEnding()); 
												 accountLine1.setPayPayableVia("UVL "+invDate);  
												 accountLine1.setPayPayableAmount(vanLine.getAmtDueAgent()); 
												 accountLine1.setCorpID(sessionCorpID);
												 accountLine1.setStatus(true);
												 accountLine1.setServiceOrder(serviceOrder);
												 accountLine1.setServiceOrderId(serviceOrder.getId());
												 accountLine1.setSequenceNumber(serviceOrder.getSequenceNumber());
												 accountLine1.setShipNumber(serviceOrder.getShipNumber());
												 accountLine1.setCreatedOn(new Date());
												 accountLine1.setCreatedBy("Vanline : "+loginUser);
												 accountLine1.setUpdatedOn(new Date());
												 accountLine1.setUpdatedBy("Vanline : "+loginUser);
												 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
										         if ( maxLineNumber.get(0) == null ) {          
										         	accountLineNumber = "001";
										         }else {
										         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
										             if((autoLineNumber.toString()).length() == 2) {
										             	accountLineNumber = "0"+(autoLineNumber.toString());
										             }
										             else if((autoLineNumber.toString()).length() == 1) {
										             	accountLineNumber = "00"+(autoLineNumber.toString());
										             } 
										             else {
										             	accountLineNumber=autoLineNumber.toString();
										             }
										         }
										         accountLine1.setAccountLineNumber(accountLineNumber);
											  try{
												    
												  if(!costElementFlag){
													Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));  
													accountLine1.setRecGl(charge.getGl());
													accountLine1.setPayGl(charge.getExpGl());
													accountLine1.setNote(charge.getDescription());
													}else{
														String chargeStr="";
														   
														if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
															  chargeStr= chargeid.get(0).toString();
														  }
														   if(!chargeStr.equalsIgnoreCase("")){
															  String [] chrageDetailArr = chargeStr.split("#");
															  accountLine1.setRecGl(chrageDetailArr[1]);
															  accountLine1.setPayGl(chrageDetailArr[2]);
															}else{
																accountLine1.setRecGl("");
																accountLine1.setPayGl("");
														  }
													}
											  } catch(Exception e){
												  e.printStackTrace();
											  }
											  accountLine1= accountLineManager.save(accountLine1);
											 }
											 }
											  
											  
											  
											  
											  
											  
											  
										  }
									  }else if(vanLine.getStatementCategory()!=null && vanLine.getStatementCategory().toString().equalsIgnoreCase("ARCV")){
										  try{
												if(!costElementFlag){
												 chargeid=chargesManager.findChargeId("ARCV",billing.getContract());
												 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
													 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
				                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
				                                    	  accRecGl= charge.getGl(); 
				                                      } 
				                                      if(charge.getExpGl()!=null && (!(charge.getExpGl().toString().trim().equals("")))) {
				                                    	  accPayGl= charge.getExpGl(); 
				                                      }
												 } 
												}else{
													 
													chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),"ARCV",sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
													String chargeStr=""; 
													if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
														  chargeStr= chargeid.get(0).toString();
													  }
													   if(!chargeStr.equalsIgnoreCase("")){
														  String [] chrageDetailArr = chargeStr.split("#");
														  try{
														  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
														  accRecGl=(chrageDetailArr[1]); 
														  }
														  }catch(Exception e){
															  e.printStackTrace();
														  }
														  try{
															  if(chrageDetailArr[2]!=null && ((chrageDetailArr[2].toString().equals("")))) {	  
																  accPayGl=(chrageDetailArr[2]); 
															  }
															  }catch(Exception e){
																  e.printStackTrace();
															  }
													   }
												}
										  } catch(Exception e){
											  e.printStackTrace();
										  }
										  
										  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && (vanLine.getAmtDueAgent()!=null ) && (!(vanLine.getAmtDueAgent().toString().equals(""))) &&  (vanLine.getAmtDueAgent().doubleValue()!=0.0 )){
											  //String accountLineNumberVanLine="";
											  accountLine=null; 
											  String loginUser = getRequest().getRemoteUser();
											  try{
											  List accountlineList=new ArrayList();
											  if(vanLine.getAccountLineNumber()!=null && (!(vanLine.getAccountLineNumber().toString().trim().equals("")))){
											  accountlineList=accountLineManager.getAccountLineForVanLine(serviceOrder.getId(), vanLine.getAccountLineNumber(),sessionCorpID);
											  if(accountlineList!=null && (!(accountlineList.isEmpty()))){
												 Iterator iterator=accountlineList.iterator();
												 while (iterator.hasNext()) {
													 String idData=iterator.next().toString();
													 Long  id = Long.parseLong(idData)  ;
													 accountLine = accountLineManager.getForOtherCorpid(id); 
											     }
											  }
											  }
											  }catch(Exception e){
												  accountLine = null;  
												  e.printStackTrace();
											  }
											  if(accountLine !=null){
												  String  invDate ="";
												  try{
												  if(vanLine.getWeekEnding()!=null) {
									        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
									        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
												  }}catch(Exception e){
													  e.printStackTrace();
												  } 
												  accountLine.setExternalReference("UVL "+invDate);
												  if(accountLine.getDistributionAmount()!=null && vanLine.getAmtDueAgent()!=null &&  accountLine.getDistributionAmount().doubleValue()==vanLine.getAmtDueAgent().doubleValue() ){
												  accountLine.setPaymentStatus("Fully Paid");
												  }else{
												  accountLine.setPaymentStatus("Partially Paid");  
												  }
												  accountLine.setReceivedAmount(vanLine.getAmtDueAgent());
												  accountLine.setStatusDate(vanLine.getWeekEnding());
												  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
												  accountLine= accountLineManager.save(accountLine);
											  }else{
												  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && (vanLine.getAmtDueAgent()!=null ) && (!(vanLine.getAmtDueAgent().toString().equals(""))) &&  (vanLine.getAmtDueAgent().doubleValue()!=0.0 )){
													  String billtocode=billing.getBillToCode();
													  if(vanLine.getAgent().indexOf("U")==0){
														  billtocode=uvlBillToCode; 
														}
														if(vanLine.getAgent().indexOf("M")==0){
															billtocode=mvlBillToCode; 
														}
													  if(vanLine.getAmtDueAgent().doubleValue()<0){
														  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
														  recInvoiceNumber=invoiceNumber.toString();
														  creditInvoiceNumber="";
														  creditInvoiceNumber	=accountLineManager.findCreditInvoice(serviceOrder.getId(),billtocode,vanLine.getReconcileAmount().doubleValue(),vanLine.getGlType(),sessionCorpID);
													  }else{
													  if(!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())) && serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) ){ 
												    	  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
														  recInvoiceNumber=invoiceNumber.toString();  
														  previousShipnumber=serviceOrder.getShipNumber();
													  }
													  }
												  }	  
											   accountLine = new AccountLine();
											 
											  accountLine.setActivateAccPortal(true);  
											  boolean activateAccPortal =true;
												try{
												if(accountLineAccountPortalFlag){	
												activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
												accountLine.setActivateAccPortal(activateAccPortal);
												}
												}catch(Exception e){
													e.printStackTrace();
												}
											  accountLine.setBasis("");
											  accountLine.setCategory("Revenue");
											  accountLine.setChargeCode("ARCV");
												try {
													String ss=chargesManager.findPopulateCostElementData("ARCV", billing.getContract(),sessionCorpID);
													if(!ss.equalsIgnoreCase("")){
														accountLine.setAccountLineCostElement(ss.split("~")[0]);
														accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
													}
												} catch (Exception e1) {
													e1.printStackTrace();
												}
											  
											  accountLine.setContract(billing.getContract());
											  //accountLine.setBillToCode(billing.getBillToCode());
											  //accountLine.setBillToName(billing.getBillToName());
											  if(vanLine.getAgent().indexOf("U")==0){
													accountLine.setBillToCode(uvlBillToCode);
													accountLine.setBillToName(uvlBillToName);
												}
												if(vanLine.getAgent().indexOf("M")==0){
													accountLine.setBillToCode(mvlBillToCode);
													accountLine.setBillToName(mvlBillToName);
													
												}
											  if(vanLine.getDescription()!=null && (!(vanLine.getDescription().toString().equals("")))){
											  accountLine.setDescription(vanLine.getDescription());
											  }else{
											   accountLine.setDescription(vanLine.getDistributionCodeDescription());  
											  }
											  accountLine.setRecQuantity(new BigDecimal(1));
											  accountLine.setActualRevenue(vanLine.getAmtDueAgent()) ;
											  accountLine.setRecRate(vanLine.getAmtDueAgent()) ;
											  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
											  accountLine.setDistributionAmount(vanLine.getAmtDueAgent());
											  
												///Code for AccountLine division
											  if((divisionFlag!=null) && (!divisionFlag.equalsIgnoreCase(""))){
										    		 accountLine.setDivision("03");
										    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
										    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
										    		try{  
										    			String roles[] = agentRoleValue.split("~");
										    		  if(roles[3].toString().equals("hauler")){
										    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
										    				   accountLine.setDivision("01");
										    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
										    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
										    					   accountLine.setDivision("01");
										    				   }else{ 
										    					   String divisionTemp="";
										    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
										    							}
										    							if(!divisionTemp.equalsIgnoreCase("")){
										    								accountLine.setDivision(divisionTemp);
										    							} 
										    				   }
										    			   }else{
										    				   String divisionTemp="";
										    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
										    						}
										    						if(!divisionTemp.equalsIgnoreCase("")){
										    							accountLine.setDivision(divisionTemp);
										    						}
										    			   }
										    		  }else{
										    			  String divisionTemp="";
										    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
										    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
										    					}
										    					if(!divisionTemp.equalsIgnoreCase("")){
										    						accountLine.setDivision(divisionTemp);
										    					}
										    		  }
										    		}catch (Exception e1) {
										    			// TODO Auto-generated catch block
										    			e1.printStackTrace();
										    		}
									    		}else{
									    			  accountLine.setDivision(null);
									    		}
												///Code for AccountLine division 
											    
											  String  invDate ="";
											  try{
											  if(vanLine.getWeekEnding()!=null) {
								        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
								        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
											  }}catch(Exception e){
												  e.printStackTrace();
											  } 
											  accountLine.setExternalReference("UVL "+invDate);
											  accountLine.setPaymentStatus("Fully Paid");
											  accountLine.setPaymentSent(null);
											  accountLine.setReceivedAmount(vanLine.getAmtDueAgent());
											  accountLine.setStatusDate(vanLine.getWeekEnding());
											  accountLine.setCorpID(sessionCorpID);
											  accountLine.setStatus(true);
											  accountLine.setServiceOrder(serviceOrder);
											  accountLine.setServiceOrderId(serviceOrder.getId());
											  String companyCode="";
											  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
											  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
											  if(companyCode.trim().equals("")){
												  companyCode=serviceOrder.getCompanyDivision();  
											  }
											  }else{
											  companyCode=serviceOrder.getCompanyDivision();
											  }
											  accountLine.setCompanyDivision(companyCode);
											  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
											  accountLine.setShipNumber(serviceOrder.getShipNumber());
											   
											  accountLine.setCreatedOn(new Date());
											  accountLine.setCreatedBy("Vanline : "+loginUser);
											  
											  accountLine.setUpdatedOn(new Date());
											  accountLine.setUpdatedBy("Vanline : "+loginUser);
											  accountLine.setRecPostDate(recAccDateToFormat);
											  accountLine.setRecInvoiceNumber(recInvoiceNumber);
											  if(creditInvoiceNumber.equals("NA")){
												  creditInvoiceNumber="";  
											  }
											  accountLine.setCreditInvoiceNumber(creditInvoiceNumber);
											  accountLine.setReceivedInvoiceDate(new Date());
											  accountLine.setInvoiceCreatedBy(loginUser); 
											  
											 
												 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
										         if ( maxLineNumber.get(0) == null ) {          
										         	accountLineNumber = "001";
										         }else {
										         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
										             if((autoLineNumber.toString()).length() == 2) {
										             	accountLineNumber = "0"+(autoLineNumber.toString());
										             }
										             else if((autoLineNumber.toString()).length() == 1) {
										             	accountLineNumber = "00"+(autoLineNumber.toString());
										             } 
										             else {
										             	accountLineNumber=autoLineNumber.toString();
										             }
										         }
										         accountLine.setAccountLineNumber(accountLineNumber);
											 
											  try{
												    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
												  if(!costElementFlag){
													Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

													
														accountLine.setRecGl(charge.getGl());
														accountLine.setPayGl(charge.getExpGl());
													}else{
														String chargeStr="";
														  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
														if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
															  chargeStr= chargeid.get(0).toString();
														  }
														   if(!chargeStr.equalsIgnoreCase("")){
															  String [] chrageDetailArr = chargeStr.split("#");
															  accountLine.setRecGl(chrageDetailArr[1]);
															  accountLine.setPayGl(chrageDetailArr[2]);
															}else{
															  accountLine.setRecGl("");
															  accountLine.setPayGl("");
														  }
													}
											  } catch(Exception e){
												e.printStackTrace();  
											  }
											  accountLine= accountLineManager.save(accountLine);
											  }
										  }
									  }else{
									  vanlineStatus=true;
									  String billtocode="";
									  String billtoName="";
									  billtocode = vanLine.getBillToCode();
									  billtoName=vanLine.getBillToName();
									  if(billtocode !=null && (!(billtocode.trim().equals("")))){
										  
									  }else{
										  billtocode = billing.getBillToCode();
										  billtoName=billing.getBillToName();
									  }
								      if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && billtocode != null && (!(billtocode.toString().trim().equals("")))  ){
								    	  accountLine=null;
								    	  //String accountLineNumberVanLine="";
										  String loginUser = getRequest().getRemoteUser();
										  try{
										  List accountlineList=new ArrayList();
										  if(vanLine.getAccountLineNumber()!=null && (!(vanLine.getAccountLineNumber().toString().trim().equals("")))){
										  accountlineList=accountLineManager.getAccountLineForVanLine(serviceOrder.getId(), vanLine.getAccountLineNumber(),sessionCorpID);
										  if(accountlineList!=null && (!(accountlineList.isEmpty()))){
											 Iterator iterator=accountlineList.iterator();
											 while (iterator.hasNext()) {
												 String idData=iterator.next().toString();
												 Long  id = Long.parseLong(idData)  ;
												 accountLine = accountLineManager.getForOtherCorpid(id); 
										     }
										  }
										  }
										  }catch(Exception e){
											  accountLine = null;  
											  e.printStackTrace();
										  }
										  if(accountLine !=null){
											  String  invDate ="";
											  try{
											  if(vanLine.getWeekEnding()!=null) {
								        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
								        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
											  }}catch(Exception e){
												  e.printStackTrace();
											  } 
											  accountLine.setExternalReference("UVL "+invDate);
											  if(accountLine.getDistributionAmount()!=null && vanLine.getReconcileAmount()!=null &&  accountLine.getDistributionAmount().doubleValue()==vanLine.getReconcileAmount().doubleValue() ){
											  accountLine.setPaymentStatus("Fully Paid");
											  }else{
											  accountLine.setPaymentStatus("Partially Paid");  
											  }
											  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
											  accountLine.setStatusDate(vanLine.getWeekEnding());
											  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
											  accountLine= accountLineManager.save(accountLine);
										  }else{
											  if(vanLine.getReconcileAmount().doubleValue()<0){
												  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
												  recInvoiceNumber=invoiceNumber.toString();
												  creditInvoiceNumber="";
												  creditInvoiceNumber	=accountLineManager.findCreditInvoice(serviceOrder.getId(),billtocode,vanLine.getReconcileAmount().doubleValue(),vanLine.getGlType(),sessionCorpID);
											  }else{
											  if(!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())) && serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) ){ 
										    	  String accInvoiceNumber=accountLineManager.getOwnbillingAccInvoiceNumber(serviceOrder.getShipNumber(),sessionCorpID,billtocode);
												  if(accInvoiceNumber!=null && (!(accInvoiceNumber.equals("")))){
													  recInvoiceNumber =accInvoiceNumber; 
												  }else{
										    	  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
												  recInvoiceNumber=invoiceNumber.toString();  
												  }
												  previousShipnumber=serviceOrder.getShipNumber();
											  }
											  }
										  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && billtocode != null && (!(billtocode.toString().trim().equals(""))) && (!(creditInvoiceNumber.equals(""))) ){	  
										   accountLine = new AccountLine();
										 
										  accountLine.setActivateAccPortal(true); 
										  boolean activateAccPortal =true;
											try{
											if(accountLineAccountPortalFlag){	
											activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
											accountLine.setActivateAccPortal(activateAccPortal);
											}
											}catch(Exception e){
												e.printStackTrace();
											}
										  accountLine.setBasis("");
										  accountLine.setCategory("Revenue");
										  accountLine.setChargeCode(vanLine.getGlType());
											try {
												String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
												if(!ss.equalsIgnoreCase("")){
													accountLine.setAccountLineCostElement(ss.split("~")[0]);
													accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
												}
											} catch (Exception e1) {
												e1.printStackTrace();
											}
										  accountLine.setContract(billing.getContract());
										  accountLine.setBillToCode(billtocode);
										  accountLine.setBillToName(billtoName);
										  if(vanLine.getDescription()!=null && (!(vanLine.getDescription().toString().equals("")))){
										  accountLine.setDescription(vanLine.getDescription());
										  }else{
										   accountLine.setDescription(vanLine.getDistributionCodeDescription());  
										  }
										  accountLine.setRecQuantity(new BigDecimal(1));
										  accountLine.setActualRevenue(vanLine.getReconcileAmount()) ;
										  accountLine.setRecRate(vanLine.getReconcileAmount()) ;
										  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
										  accountLine.setDistributionAmount(vanLine.getReconcileAmount());
										  
											///Code for AccountLine division
										  if((divisionFlag!=null) && (!divisionFlag.equalsIgnoreCase(""))){
									    		 accountLine.setDivision("03");
									    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
									    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
									    		try{  
									    			String roles[] = agentRoleValue.split("~");
									    		  if(roles[3].toString().equals("hauler")){
									    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
									    				   accountLine.setDivision("01");
									    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
									    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
									    					   accountLine.setDivision("01");
									    				   }else{ 
									    					   String divisionTemp="";
									    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
									    							}
									    							if(!divisionTemp.equalsIgnoreCase("")){
									    								accountLine.setDivision(divisionTemp);
									    							} 
									    				   }
									    			   }else{
									    				   String divisionTemp="";
									    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
									    						}
									    						if(!divisionTemp.equalsIgnoreCase("")){
									    							accountLine.setDivision(divisionTemp);
									    						}
									    			   }
									    		  }else{
									    			  String divisionTemp="";
									    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
									    					}
									    					if(!divisionTemp.equalsIgnoreCase("")){
									    						accountLine.setDivision(divisionTemp);
									    					}
									    		  }
									    		}catch (Exception e1) {
									    			// TODO Auto-generated catch block
									    			e1.printStackTrace();
									    		}
								    		}else{
								    			  accountLine.setDivision(null);
								    		}
											///Code for AccountLine division 
										    
										  String  invDate ="";
										  try{
										  if(vanLine.getWeekEnding()!=null) {
							        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
							        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
										  }}catch(Exception e){
											  e.printStackTrace();
										  } 
										  accountLine.setExternalReference("UVL "+invDate);
										  accountLine.setPaymentStatus("Fully Paid");
										  accountLine.setPaymentSent(null);
										  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
										  accountLine.setStatusDate(vanLine.getWeekEnding());
										  accountLine.setCorpID(sessionCorpID);
										  accountLine.setStatus(true);
										  accountLine.setServiceOrder(serviceOrder);
										  accountLine.setServiceOrderId(serviceOrder.getId());
										  String companyCode="";
										  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
										  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
										  if(companyCode.trim().equals("")){
											  companyCode=serviceOrder.getCompanyDivision();  
										  }
										  }else{
										  companyCode=serviceOrder.getCompanyDivision();
										  }
										  accountLine.setCompanyDivision(companyCode);
										  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
										  accountLine.setShipNumber(serviceOrder.getShipNumber());
										 
										  accountLine.setCreatedOn(new Date());
										  accountLine.setCreatedBy("Vanline : "+loginUser);
										 
										  accountLine.setUpdatedOn(new Date());
										  accountLine.setUpdatedBy("Vanline : "+loginUser);
										  accountLine.setRecPostDate(recAccDateToFormat);
										  accountLine.setRecInvoiceNumber(recInvoiceNumber);
										  if(creditInvoiceNumber.equals("NA")){
											  creditInvoiceNumber="";  
										  }
										  accountLine.setCreditInvoiceNumber(creditInvoiceNumber);
										  accountLine.setReceivedInvoiceDate(new Date());
										  accountLine.setInvoiceCreatedBy(loginUser); 
										  
										 
											 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
									         if ( maxLineNumber.get(0) == null ) {          
									         	accountLineNumber = "001";
									         }else {
									         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
									             if((autoLineNumber.toString()).length() == 2) {
									             	accountLineNumber = "0"+(autoLineNumber.toString());
									             }
									             else if((autoLineNumber.toString()).length() == 1) {
									             	accountLineNumber = "00"+(autoLineNumber.toString());
									             } 
									             else {
									             	accountLineNumber=autoLineNumber.toString();
									             }
									         }
									         accountLine.setAccountLineNumber(accountLineNumber);
										  
										  try{
											    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
											  if(!costElementFlag){
												Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

												
													accountLine.setRecGl(charge.getGl());
													accountLine.setPayGl(charge.getExpGl());
												}else{
													String chargeStr="";
													  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
													if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
														  chargeStr= chargeid.get(0).toString();
													  }
													   if(!chargeStr.equalsIgnoreCase("")){
														  String [] chrageDetailArr = chargeStr.split("#");
														  accountLine.setRecGl(chrageDetailArr[1]);
														  accountLine.setPayGl(chrageDetailArr[2]);
														}else{
														  accountLine.setRecGl("");
														  accountLine.setPayGl("");
													  }
												}
										  } catch(Exception e){
											  e.printStackTrace();
										  }
										  accountLine= accountLineManager.save(accountLine);
										  }
										  }
										  /*sid=serviceOrder.getId();
										  String jobName="";
										  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
										    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
										    	jobName = tempCommissionJob.get(0).toString();
										    }
										    if(jobName.contains(serviceOrder.getJob())){
										    	calculateCommisssionMethod();
										    } */
								      }else{
								    	
								    	  vanlineStatus=false;
								      }
								      
									  }
								 }
							
							}
							if(vanlineStatus){
							vanLine.setReconcileStatus("Reconcile");
							vanLine.setAccCreatedOn(new Date());
							vanLine = vanLineManager.save(vanLine);
							}
						 }
						 
				
			}else{
			boolean vanlineStatus=false;
			if(vanLine.getReconcileAmount()==null || ((vanLine.getReconcileAmount().toString().equals(""))) ||  vanLine.getReconcileAmount().doubleValue()==0.0 ){
				vanLine.setReconcileAmount(vanLine.getAmtDueAgent());	
			}
			if(vanLine.getRegNum()!=null && (!(vanLine.getRegNum().equals(""))) && (vanLine.getReconcileStatus()!=null) && (vanLine.getReconcileStatus().equalsIgnoreCase("Approved")) &&  vanLine.getGlType()!=null && (!(vanLine.getGlType().equals(""))) && vanLine.getReconcileAmount()!=null && (!(vanLine.getReconcileAmount().toString().equals(""))) &&  vanLine.getReconcileAmount().doubleValue()!=0.0 ){

				   
					//change for posting date flexiblity Start	
					try{
						//company=companyManager.findByCorpID(sessionCorpID).get(0);
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
				    		Date dt1=new Date();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		dt1=sdfDestination.parse(dt11);
				    		Date dt2=sdfDestination.parse(recAccDateTo);
				    		if(dt1.compareTo(dt2)>0){
				    			recAccDateTo=sdfDestination.format(dt2);
				    		}else{
				    			recAccDateTo=sdfDestination.format(dt1);
				    		}
			    		}
						}catch(Exception e){e.printStackTrace();}			
						//change for posting date flexiblity End				
					 
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					try{ 
					if(!recAccDateTo.equals("")){	
						recAccDateToFormat=sdf.parse(recAccDateTo); 
						}else{
							recAccDateToFormat =null; 
						}
					}
					catch( Exception e)
					{
						e.printStackTrace();
					}
					
						List  SOidList = vanLineManager.getShipNumber(vanLine.getRegNum());
						if(SOidList!=null && (!SOidList.isEmpty())){
							Iterator SOidIterator=SOidList.iterator();
							String previousShipnumber="";
							String recInvoiceNumber = "";
							 while(SOidIterator.hasNext()){
								 String SoIdData=SOidIterator.next().toString();
								  Long  SoId = Long.parseLong(SoIdData)  ; 
								  serviceOrder = serviceOrderManager.get(SoId);
								  trackingStatus=trackingStatusManager.get(SoId);
								  miscellaneous = miscellaneousManager.get(SoId);
								  billing=billingManager.get(SoId);
								  List chargeid=new ArrayList();
								  String accRecGl="";
								  String accPayGl="";
								  try{
										if(!costElementFlag){
										 chargeid=chargesManager.findChargeId(vanLine.getGlType(),billing.getContract());
										 if (chargeid!=null && (!(chargeid.isEmpty())) && chargeid.get(0)!=null && (!(chargeid.get(0).toString().equals("")))){
											 Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));
		                                      if(charge.getGl()!=null && (!(charge.getGl().toString().trim().equals("")))) {
		                                    	  accRecGl= charge.getGl(); 
		                                      }
		                                      if(charge.getExpGl()!=null && (!(charge.getExpGl().toString().trim().equals("")))) {
		                                    	  accPayGl= charge.getExpGl(); 
		                                      }
										 } 
										}else{
											 
											chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),vanLine.getGlType(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
											String chargeStr=""; 
											if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
												  chargeStr= chargeid.get(0).toString();
											  }
											   if(!chargeStr.equalsIgnoreCase("")){
												  String [] chrageDetailArr = chargeStr.split("#");
												  try{
												  if(chrageDetailArr[1]!=null && ((chrageDetailArr[1].toString().equals("")))) {	  
												  accRecGl=(chrageDetailArr[1]); 
												  }
												  }catch(Exception e){
													  e.printStackTrace();
												  }
												  try{
													  if(chrageDetailArr[2]!=null && ((chrageDetailArr[2].toString().equals("")))) {	  
														  accPayGl=(chrageDetailArr[2]); 
													  }
													  }catch(Exception e){
														  e.printStackTrace();
													  }
											   }
										}
								  } catch(Exception e){
									  e.printStackTrace();
								  }
								  if(vanLine.getStatementCategory()!=null && vanLine.getStatementCategory().toString().equalsIgnoreCase("MCLM")){
									  if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accPayGl.trim().equals(""))) && (vanLine.getAmtDueAgent()!=null ) && (!(vanLine.getAmtDueAgent().toString().equals(""))) &&  (vanLine.getAmtDueAgent().doubleValue()!=0.0 )){
										  if(vanLine.getDriverID()!=null && (!(vanLine.getDriverID().toString().trim().equals("")))){  
											  String loginUser = getRequest().getRemoteUser();	
											  String actCode="";
											  accountLine = new AccountLine();
											  accountLine.setActivateAccPortal(true); 
											  boolean activateAccPortal =true;
												try{
												if(accountLineAccountPortalFlag){	
												activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
												accountLine.setActivateAccPortal(activateAccPortal);
												}
												}catch(Exception e){
													e.printStackTrace();
												}
											  accountLine.setBasis("");
											  accountLine.setCategory("Transport");
											  accountLine.setChargeCode(vanLine.getGlType());
												try {
													String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
													if(!ss.equalsIgnoreCase("")){
														accountLine.setAccountLineCostElement(ss.split("~")[0]);
														accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
													}
												} catch (Exception e1) {
													e1.printStackTrace();
												}
											  
								    		accountLine.setDivision("03");
								    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
								    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
								    		try{  
								    			String roles[] = agentRoleValue.split("~");
								    		  if(roles[3].toString().equals("hauler")){
								    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
								    				   accountLine.setDivision("01");
								    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
								    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
								    					   accountLine.setDivision("01");
								    				   }else{ 
								    					   String divisionTemp="";
								    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
								    							}
								    							if(!divisionTemp.equalsIgnoreCase("")){
								    								accountLine.setDivision(divisionTemp);
								    							} 
								    				   }
								    			   }else{
								    				   String divisionTemp="";
								    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
								    						}
								    						if(!divisionTemp.equalsIgnoreCase("")){
								    							accountLine.setDivision(divisionTemp);
								    						}
								    			   }
								    		  }else{
								    			  String divisionTemp="";
								    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
								    					}
								    					if(!divisionTemp.equalsIgnoreCase("")){
								    						accountLine.setDivision(divisionTemp);
								    					}
								    		  }
								    		}catch (Exception e1) {
								    			// TODO Auto-generated catch block
								    			e1.printStackTrace();
								    		}
									    		
											  accountLine.setContract(billing.getContract());
											  accountLine.setCompanyDivision(vanLine.getCompanyDivision()); 
											  accountLine.setVendorCode(vanLine.getDriverID());
											  accountLine.setEstimateVendorName(vanLine.getDriverName());
											   
											   if(companyDivisionAcctgCodeUnique.equalsIgnoreCase("Y")){
											 			actCode=partnerManager.getAccountCrossReference(accountLine.getVendorCode(),accountLine.getCompanyDivision(),sessionCorpID);
											   }else{
											 			actCode=partnerManager.getAccountCrossReferenceUnique(accountLine.getVendorCode(),sessionCorpID);
											   }
											   accountLine.setActgCode(actCode);	
											   if(actCode !=null && (!(actCode.trim().equals("")))){	  
								        		baseCurrencyCompanyDivision=companyDivisionManager.searchBaseCurrencyCompanyDivision(accountLine.getCompanyDivision(),sessionCorpID);
								        		if(baseCurrencyCompanyDivision==null||  baseCurrencyCompanyDivision.equals(""))
									    		{
								        		accountLine.setEstCurrency(baseCurrency);
								        		accountLine.setRevisionCurrency(baseCurrency);	
								        		accountLine.setCountry(baseCurrency);
								        		accountLine.setEstValueDate(new Date());
								        		accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								        		accountLine.setRevisionValueDate(new Date());
								        		accountLine.setRevisionExchangeRate(new BigDecimal(1.0000)); 
								        		accountLine.setValueDate(new Date());
								        		accountLine.setExchangeRate(new Double(1.0));
									    		}
									    		else
									    		{
									    		accountLine.setEstCurrency(baseCurrencyCompanyDivision);
									    		accountLine.setRevisionCurrency(baseCurrencyCompanyDivision);
									    		accountLine.setCountry(baseCurrencyCompanyDivision);
									    		accountLine.setEstValueDate(new Date());
									    		accountLine.setRevisionValueDate(new Date());
									    		accountLine.setValueDate(new Date());
								    			//accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision); 
								    			List accExchangeRateList=exchangeRateManager.findAccExchangeRate(sessionCorpID,baseCurrencyCompanyDivision);
								    			if(accExchangeRateList != null && (!(accExchangeRateList.isEmpty())) && accExchangeRateList.get(0)!= null && (!(accExchangeRateList.get(0).toString().equals("")))){
								    				try{
								    				accountLine.setEstExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine.setRevisionExchangeRate(new BigDecimal(accExchangeRateList.get(0).toString()));
								    				accountLine.setExchangeRate(new Double(accExchangeRateList.get(0).toString()));	
								    				}catch(Exception e){
								    					accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								    					accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								    					accountLine.setExchangeRate(new Double(1.0));	
								    					e.printStackTrace();
								    				}
								    			}else{
								    				accountLine.setEstExchangeRate(new BigDecimal(1.0000)); 
								    				accountLine.setRevisionExchangeRate(new BigDecimal(1.0000));
								    				accountLine.setExchangeRate(new Double(1.0));
								    			}
									    		}
								        		 
									        	if(vanLine.getAmtDueAgent()!=null){ 
									        		accountLine.setLocalAmount(vanLine.getAmtDueAgent());
									        	}
									        	if(accountLine.getLocalAmount()!=null){
								        		if(accountLine.getExchangeRate()==1.00){
								        			accountLine.setActualExpense(accountLine.getLocalAmount());
									             }else{
									            	 Double  D1= accountLine.getLocalAmount().doubleValue();
									            	 Double D2=D1/accountLine.getExchangeRate();
									            	 accountLine.setActualExpense(new BigDecimal(D2));
									             }	
									        	} 
									        	String  invDate ="";
									            try{
												 if(vanLine.getWeekEnding()!=null) {
										        	 SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
										        	 invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
												 }}catch(Exception e){
													 e.printStackTrace();
												 }
												 accountLine.setInvoiceNumber(vanLine.getAccountingAgent()+invDate);
												 accountLine.setInvoiceDate(new Date());
												 accountLine.setReceivedDate(vanLine.getWeekEnding());
												 accountLine.setPayingStatus("A");
												 if(autuPopPostDate){
												 try{
												 if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
													    Date dt1=new Date();
													    SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
													    String dt11=sdfDestination.format(dt1);
													    dt1=sdfDestination.parse(dt11);
													    Date dt2=sdfDestination.parse(payAccDateTo);
													    if(dt1.compareTo(dt2)>0){
													    	payAccDateTo=sdfDestination.format(dt2);
													    }else{
													    	payAccDateTo=sdfDestination.format(dt1);
													    }
												    	}
														}catch(Exception e){e.printStackTrace();}			
														SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
														try{ 
														if(!payAccDateTo.equals("")){	
															payAccDateToFormat=sdf1.parse(payAccDateTo); 
															}else{
																payAccDateToFormat =null; 
															}
														}
														catch( Exception e)
														{
															e.printStackTrace();
														}
												 accountLine.setPayPostDate(payAccDateToFormat);	 
												 }
												 accountLine.setPayPayableStatus("Fully Paid");
												 accountLine.setPayPayableDate(vanLine.getWeekEnding()); 
												 accountLine.setPayPayableVia("UVL "+invDate);  
												 accountLine.setPayPayableAmount(vanLine.getAmtDueAgent()); 
												 accountLine.setCorpID(sessionCorpID);
												 accountLine.setStatus(true);
												 accountLine.setServiceOrder(serviceOrder);
												 accountLine.setServiceOrderId(serviceOrder.getId());
												 accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
												 accountLine.setShipNumber(serviceOrder.getShipNumber());
												 accountLine.setCreatedOn(new Date());
												 accountLine.setCreatedBy("Vanline : "+loginUser);
												 accountLine.setUpdatedOn(new Date());
												 accountLine.setUpdatedBy("Vanline : "+loginUser);
												 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
										         if ( maxLineNumber.get(0) == null ) {          
										         	accountLineNumber = "001";
										         }else {
										         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
										             if((autoLineNumber.toString()).length() == 2) {
										             	accountLineNumber = "0"+(autoLineNumber.toString());
										             }
										             else if((autoLineNumber.toString()).length() == 1) {
										             	accountLineNumber = "00"+(autoLineNumber.toString());
										             } 
										             else {
										             	accountLineNumber=autoLineNumber.toString();
										             }
										         }
										         accountLine.setAccountLineNumber(accountLineNumber);
											  try{
												    
												  if(!costElementFlag){
													Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));  
													accountLine.setRecGl(charge.getGl());
													accountLine.setPayGl(charge.getExpGl());
													accountLine.setNote(charge.getDescription());
													}else{
														String chargeStr="";
														   
														if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
															  chargeStr= chargeid.get(0).toString();
														  }
														   if(!chargeStr.equalsIgnoreCase("")){
															  String [] chrageDetailArr = chargeStr.split("#");
															  accountLine.setRecGl(chrageDetailArr[1]);
															  accountLine.setPayGl(chrageDetailArr[2]);
															}else{
																accountLine.setRecGl("");
																accountLine.setPayGl("");
														  }
													}
											  } catch(Exception e){
												  e.printStackTrace();
											  }
											  accountLine= accountLineManager.save(accountLine);
											  sid=serviceOrder.getId();
											  String jobName="";
											  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
											    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
											    	jobName = tempCommissionJob.get(0).toString();
											    }
											    if(jobName.contains(serviceOrder.getJob())){
											    	calculateCommisssionMethod();
											    } 
											   }
											 }  
									  }
								  }else{
									  vanlineStatus=true;
								 
		                          
							      if(serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && uvlBillToCode.equalsIgnoreCase(billing.getBillToCode())){
							    	  accountLine=null;
							    	  //String accountLineNumberVanLine="";
									  String loginUser = getRequest().getRemoteUser();	
									  try{
									  List accountlineList=new ArrayList();
									  if(vanLine.getAccountLineNumber()!=null && (!(vanLine.getAccountLineNumber().toString().trim().equals("")))){
									  accountlineList=accountLineManager.getAccountLineForVanLine(serviceOrder.getId(), vanLine.getAccountLineNumber(),sessionCorpID);
									  if(accountlineList!=null && (!(accountlineList.isEmpty()))){
										 Iterator iterator=accountlineList.iterator();
										 while (iterator.hasNext()) {
											 String idData=iterator.next().toString();
											 Long  id = Long.parseLong(idData)  ;
											 accountLine = accountLineManager.getForOtherCorpid(id); 
									     }
									  }
									  }
									  }catch(Exception e){
										  accountLine = null; 
										  e.printStackTrace();
									  }
									  if(accountLine !=null){
										  String  invDate ="";
										  try{
										  if(vanLine.getWeekEnding()!=null) {
							        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
							        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
										  }}catch(Exception e){
											  e.printStackTrace();
										  } 
										  accountLine.setExternalReference("UVL "+invDate);
										  if(accountLine.getDistributionAmount()!=null && vanLine.getReconcileAmount()!=null &&  accountLine.getDistributionAmount().doubleValue()==vanLine.getReconcileAmount().doubleValue() ){
										  accountLine.setPaymentStatus("Fully Paid");
										  }else{
										  accountLine.setPaymentStatus("Partially Paid");  
										  }
										  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
										  accountLine.setStatusDate(vanLine.getWeekEnding());
										  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
										  accountLine= accountLineManager.save(accountLine);
									  }else{
										  if(!(previousShipnumber.equalsIgnoreCase(serviceOrder.getShipNumber())) && serviceOrder!=null && chargeid!=null && (!(chargeid.isEmpty())) && (!(accRecGl.trim().equals(""))) && uvlBillToCode.equalsIgnoreCase(billing.getBillToCode())){ 
									    	  String accInvoiceNumber=accountLineManager.getAccInvoiceNumber(serviceOrder.getShipNumber(),sessionCorpID);
											  if(accInvoiceNumber!=null && (!(accInvoiceNumber.equals("")))){
												  recInvoiceNumber =accInvoiceNumber; 
											  }else{
									    	  Long invoiceNumber=accountLineManager.findInvoiceNumber(sessionCorpID);
											  recInvoiceNumber=invoiceNumber.toString();  
											  }
											  previousShipnumber=serviceOrder.getShipNumber();
										  }	  
									   accountLine = new AccountLine();
									  
									   
									  accountLine.setActivateAccPortal(true);
									  boolean activateAccPortal =true;
										try{
										if(accountLineAccountPortalFlag){	
										activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
										accountLine.setActivateAccPortal(activateAccPortal);
										}
										}catch(Exception e){
											e.printStackTrace();
										}
									  accountLine.setBasis("");
									  accountLine.setCategory("Revenue");
									  accountLine.setChargeCode(vanLine.getGlType());
										try {
											String ss=chargesManager.findPopulateCostElementData(vanLine.getGlType(), billing.getContract(),sessionCorpID);
											if(!ss.equalsIgnoreCase("")){
												accountLine.setAccountLineCostElement(ss.split("~")[0]);
												accountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
											}
										} catch (Exception e1) {
											e1.printStackTrace();
										}
									  
									  accountLine.setContract(billing.getContract());
									  accountLine.setBillToCode(billing.getBillToCode());
									  accountLine.setBillToName(billing.getBillToName());
									  if(vanLine.getDescription()!=null && (!(vanLine.getDescription().toString().equals("")))){
									  accountLine.setDescription(vanLine.getDescription());
									  }else{
									   accountLine.setDescription(vanLine.getDistributionCodeDescription());  
									  }
									  accountLine.setRecQuantity(new BigDecimal(1));
									  accountLine.setActualRevenue(vanLine.getReconcileAmount()) ;
									  accountLine.setRecRate(vanLine.getReconcileAmount()) ;
									  accountLine.setVanlineSettleDate(vanLine.getWeekEnding());
									  accountLine.setDistributionAmount(vanLine.getReconcileAmount());
									  
										///Code for AccountLine division
									 
						    		accountLine.setDivision("03");
						    		systemDefault = (SystemDefault) serviceOrderManager.findSysDefault(sessionCorpID).get(0);
						    		String agentRoleValue = accountLineManager.getAgentCompanyDivCode(serviceOrder.getBookingAgentCode(), trackingStatus.getOriginAgentCode(), trackingStatus.getDestinationAgentCode(), miscellaneous.getHaulingAgentCode(),sessionCorpID);
						    		try{  
						    			String roles[] = agentRoleValue.split("~");
						    		  if(roles[3].toString().equals("hauler")){
						    			   if(roles[0].toString().equals("No") && roles[2].toString().equals("No") && roles[1].toString().equals("No")){
						    				   accountLine.setDivision("01");
						    			   }else if(accountLine.getChargeCode()!=null && !accountLine.getChargeCode().equalsIgnoreCase("")) {
						    				   if(systemDefault.getDefaultDivisionCharges().contains(accountLine.getChargeCode())){
						    					   accountLine.setDivision("01");
						    				   }else{ 
						    					   String divisionTemp="";
						    							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    							divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    							}
						    							if(!divisionTemp.equalsIgnoreCase("")){
						    								accountLine.setDivision(divisionTemp);
						    							} 
						    				   }
						    			   }else{
						    				   String divisionTemp="";
						    						if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    						divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    						}
						    						if(!divisionTemp.equalsIgnoreCase("")){
						    							accountLine.setDivision(divisionTemp);
						    						}
						    			   }
						    		  }else{
						    			  String divisionTemp="";
						    					if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
						    					divisionTemp=refMasterManager.findDivisionForReconcile(sessionCorpID,accountLine.getChargeCode(),serviceOrder.getRegistrationNumber());
						    					}
						    					if(!divisionTemp.equalsIgnoreCase("")){
						    						accountLine.setDivision(divisionTemp);
						    					}
						    		  }
						    		}catch (Exception e1) {
						    			// TODO Auto-generated catch block
						    			e1.printStackTrace();
						    		}
							    		
										///Code for AccountLine division 
									    
									  String  invDate ="";
									  try{
									  if(vanLine.getWeekEnding()!=null) {
						        			SimpleDateFormat formats = new SimpleDateFormat("dd/MM/yy");
						        			invDate	= new StringBuilder(formats.format(vanLine.getWeekEnding())).toString();
									  }}catch(Exception e){
										  e.printStackTrace();
									  } 
									  accountLine.setExternalReference("UVL "+invDate);
									  accountLine.setPaymentStatus("Fully Paid");
									  accountLine.setPaymentSent(null);
									  accountLine.setReceivedAmount(vanLine.getReconcileAmount());
									  accountLine.setStatusDate(vanLine.getWeekEnding());
									  accountLine.setCorpID(sessionCorpID);
									  accountLine.setStatus(true);
									  accountLine.setServiceOrder(serviceOrder);
									  accountLine.setServiceOrderId(serviceOrder.getId());
									  String companyCode="";
									  if(vanLine.getAccountingAgent()!=null && (!(vanLine.getAccountingAgent().toString().equals("")))){
									  companyCode=vanLineManager.getCompanyCode(vanLine.getAccountingAgent(),sessionCorpID);
									  if(companyCode.trim().equals("")){
										  companyCode=serviceOrder.getCompanyDivision();  
									  }
									  }else{
									  companyCode=serviceOrder.getCompanyDivision();
									  }
									  accountLine.setCompanyDivision(companyCode);
									  accountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
									  accountLine.setShipNumber(serviceOrder.getShipNumber());
									   
									  accountLine.setCreatedOn(new Date());
									  accountLine.setCreatedBy("Vanline : "+loginUser);
									  
									  accountLine.setUpdatedOn(new Date());
									  accountLine.setUpdatedBy("Vanline : "+loginUser);
									  accountLine.setRecPostDate(recAccDateToFormat);
									  accountLine.setRecInvoiceNumber(recInvoiceNumber);
									  accountLine.setReceivedInvoiceDate(new Date());
									  accountLine.setInvoiceCreatedBy(loginUser); 
									  
									  
										 maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber()); 
								         if ( maxLineNumber.get(0) == null ) {          
								         	accountLineNumber = "001";
								         }else {
								         	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
								             if((autoLineNumber.toString()).length() == 2) {
								             	accountLineNumber = "0"+(autoLineNumber.toString());
								             }
								             else if((autoLineNumber.toString()).length() == 1) {
								             	accountLineNumber = "00"+(autoLineNumber.toString());
								             } 
								             else {
								             	accountLineNumber=autoLineNumber.toString();
								             }
								         }
								         accountLine.setAccountLineNumber(accountLineNumber);
									   
									  try{
										    //chargeid=chargesManager.findChargeId(accountLine.getChargeCode(),billing.getContract());
										  if(!costElementFlag){
											Charges charge=chargesManager.get(Long.parseLong(chargeid.get(0).toString()));

											
												accountLine.setRecGl(charge.getGl());
												accountLine.setPayGl(charge.getExpGl());
											}else{
												String chargeStr="";
												  //chargeid = accountLineManager.findChargeDetailFromSO(billing.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob(),serviceOrder.getRouting(),serviceOrder.getCompanyDivision());
												if(chargeid!=null && !chargeid.isEmpty() && chargeid.get(0)!=null && !chargeid.get(0).toString().equalsIgnoreCase("NoDiscription")){
													  chargeStr= chargeid.get(0).toString();
												  }
												   if(!chargeStr.equalsIgnoreCase("")){
													  String [] chrageDetailArr = chargeStr.split("#");
													  accountLine.setRecGl(chrageDetailArr[1]);
													  accountLine.setPayGl(chrageDetailArr[2]);
													}else{
													  accountLine.setRecGl("");
													  accountLine.setPayGl("");
												  }
											}
									  } catch(Exception e){
										 e.printStackTrace();
										  
									  }
									  accountLine= accountLineManager.save(accountLine);
									  sid=serviceOrder.getId();
									  String jobName="";
									  List tempCommissionJob = serviceOrderManager.getCommissionableJobs(sessionCorpID);
									    if(tempCommissionJob!=null && !tempCommissionJob.isEmpty() && tempCommissionJob.get(0)!= null){
									    	jobName = tempCommissionJob.get(0).toString();
									    }
									    if(jobName.contains(serviceOrder.getJob())){
									    	calculateCommisssionMethod();
									    } 
								  }
							      }else{
							    	
							    	  vanlineStatus=false;
							      }
							   
							 }
							 }
						
						}
						if(vanlineStatus){
						vanLine.setReconcileStatus("Reconcile");
						vanLine.setAccCreatedOn(new Date());
						vanLine = vanLineManager.save(vanLine);
						}
					 }
					 
			}
			}
			if(((vanLine.getRegNum()==null ) || (vanLine.getRegNum().equals(""))) && ((vanLine.getReconcileStatus()!=null) && (vanLine.getReconcileStatus().equalsIgnoreCase("Approved"))) ){
				
					 
				subcontractorChargesExt = vanLineManager.splitCharge(id);
				if((subcontractorChargesExt.isEmpty())){
				if(automaticReconcile && ownbillingVanline){	
	        	subcontractorCharges=new SubcontractorCharges();  
	        	//vanLine=vanLineManager.get(vid);
	        	subcontractorCharges.setParentId(vanLine.getId()); 
	        	subcontractorCharges.setDescription(vanLine.getDescription());
	        	subcontractorCharges.setCorpID(vanLine.getCorpID());
	        	subcontractorCharges.setBranch(vanLine.getAgent());
	        	subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
	        	subcontractorCharges.setPersonType("A");
	        	subcontractorCharges.setVanLineNumber("A"+vanLine.getId()); 
	        	subcontractorCharges.setGlCode(vanLine.getGlCode());
	        	BigDecimal  multiplyValue = new BigDecimal(-1.0);
	        	if(vanLine.getAmtDueAgent()!=null){ 
	        	subcontractorCharges.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
	        	}
	        	if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
				String companyDivision="";
				companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
				subcontractorCharges.setCompanyDivision(companyDivision); 
				//List vanLineAgent = vanLineManager.findVanLineAgent(vanLine.getAgent(),sessionCorpID);
				/*if(!vanLineAgent.isEmpty()){
				try{	
				String	agentCodeName = vanLineAgent.get(0).toString();
				String [] agentCodeNameDetailArr = agentCodeName.split("~");
				subcontractorCharges.setPersonId(agentCodeNameDetailArr[0]);
				subcontractorCharges.setPersonName(agentCodeNameDetailArr[1]);
				}catch(Exception e){ }
				}*/ 
				 
				if(vanLine.getAgent().indexOf("U")==0){
					subcontractorCharges.setPersonId(uvlBillToCode);
					subcontractorCharges.setPersonName(uvlBillToName);
				}
				if(vanLine.getAgent().indexOf("M")==0){
					subcontractorCharges.setPersonId(mvlBillToCode);
					subcontractorCharges.setPersonName(mvlBillToName);
					
				}
				}
	        	subcontractorCharges.setApproved(vanLine.getWeekEnding());
	        	if(ownbillingVanline){
	        		try{
		        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		        		String strDate = sm.format(new Date());
		        		Date dt = sm.parse(strDate);
			        	subcontractorCharges.setApproved(dt);
		        		}catch(Exception e){
		        			e.printStackTrace();
		        		}	
	        	}
	        	subcontractorCharges.setAdvanceDate(vanLine.getWeekEnding());
	        	subcontractorCharges.setDivision(vanLine.getDivision());
	        	
	 			//if(autuPopPostDate){
					//change for posting date flexiblity Start	
					try{
						//company=companyManager.findByCorpID(sessionCorpID).get(0);
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    			Date dt1=vanLine.getWeekEnding();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		Date dt2 =null;
				    		try{
				    		dt2 = new Date();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}				    		
				    		
				    		dt2=payDate;
				    		if(dt2!=null){
					    		if(dt1.compareTo(dt2)>0){
					    			payDate=dt2;
					    		}else{
					    			payDate=dt1;
					    		}
				    		}
			    		}
				}catch(Exception e){
					e.printStackTrace();
				}  	 
	 			//}
	 			if(payDate!=null){ 
	 				subcontractorCharges.setPost(payDate); 
 				} 
	        	subcontractorCharges.setCreatedOn(new Date());
	        	subcontractorCharges.setUpdatedOn(new Date());
	        	subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
	         	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
	         	subcontractorCharges.setDeductTempFlag(false);
	         	subcontractorCharges.setIsSettled(false);
	        	subcontractorChargesManager.save(subcontractorCharges);
	        	
				}
				if((vanLine.getDriverID()!=null) && (!(vanLine.getDriverID().toString().trim().equals("")))){
	        	SubcontractorCharges subcontractorCharges1=new SubcontractorCharges();  
	        	//vanLine=vanLineManager.get(vid);
	        	subcontractorCharges1.setParentId(vanLine.getId()); 
	        	subcontractorCharges1.setDescription(vanLine.getDescription());
	        	subcontractorCharges1.setCorpID(vanLine.getCorpID());
	        	subcontractorCharges1.setBranch(vanLine.getAgent());
	        	subcontractorCharges1.setRegNumber(vanLine.getRegNum()); 
	        	subcontractorCharges1.setVanLineNumber("D"+vanLine.getId());
	        	subcontractorCharges1.setPersonType("D");
	        	subcontractorCharges1.setPersonId(vanLine.getDriverID());
	        	subcontractorCharges1.setPersonName(vanLine.getDriverName());
	        	subcontractorCharges1.setGlCode(vanLine.getGlCode());
	        	//BigDecimal  multiplyValue = new BigDecimal(-1.0);
	        	if(ownbillingVanline){
	        	if(vanLine.getAmtDueAgent()!=null){ 
	        		subcontractorCharges1.setAmount(vanLine.getAmtDueAgent());
	        	}
	        	}else{
	        		if(automaticReconcile){
	        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
		        	if(vanLine.getAmtDueAgent()!=null){ 
		        	subcontractorCharges1.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
		        	}
	        		}else{ 
	    	        	if(vanLine.getAmtDueAgent()!=null){ 
	    	        		subcontractorCharges1.setAmount(vanLine.getAmtDueAgent());
	    	        	} 
	        		}
	        	}
	        	subcontractorCharges1.setCompanyDivision(vanLine.getCompanyDivision());
	        	
	        	subcontractorCharges1.setApproved(vanLine.getWeekEnding());  
	        	if(ownbillingVanline){
	        		try{
		        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		        		String strDate = sm.format(new Date());
		        		Date dt = sm.parse(strDate);
			        	subcontractorCharges1.setApproved(dt);
		        		}catch(Exception e){
		        			e.printStackTrace();
		        		}
		        }
	        	subcontractorCharges1.setAdvanceDate(vanLine.getWeekEnding());
	        	subcontractorCharges1.setDivision(vanLine.getDivision());
	 			//if(autuPopPostDate){
					//change for posting date flexiblity Start	
					try{
						//company=companyManager.findByCorpID(sessionCorpID).get(0);
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    			Date dt1=vanLine.getWeekEnding();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		Date dt2 =null;
				    		try{
				    		dt2 = new Date();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}				    		
				    		
				    		dt2=payDate;
				    		if(dt2!=null){
					    		if(dt1.compareTo(dt2)>0){
					    			payDate=dt2;
					    		}else{
					    			payDate=dt1;
					    		}
				    		}
			    		}
				}catch(Exception e){
					e.printStackTrace();
				}  	 
	 			//}
	 			if(payDate!=null){ 
	 				subcontractorCharges1.setPost(payDate); 
 				} 
	 			subcontractorCharges1.setCreatedOn(new Date());
	 			subcontractorCharges1.setUpdatedOn(new Date());
	 			subcontractorCharges1.setCreatedBy(getRequest().getRemoteUser());
	 			subcontractorCharges1.setUpdatedBy(getRequest().getRemoteUser()); 
	 			subcontractorCharges1.setDeductTempFlag(false);
	 			subcontractorCharges1.setIsSettled(false);
	        	subcontractorChargesManager.save(subcontractorCharges1);
				}
	        	/*if(vanLine.getDriverID()==null||vanLine.getDriverID().equalsIgnoreCase(""))
	         	{
	         		//subcontractorCharges.setPersonId("");
	         		//subcontractorCharges.setPersonType("A");
	        		sysDefaultDetail=systemDefaultManager.findByCorpID(sessionCorpID);
	        		if(sysDefaultDetail!=null && (!(sysDefaultDetail.isEmpty())) && sysDefaultDetail.get(0)!=null){
	        			SystemDefault sd=sysDefaultDetail.get(0);
	        			if(sd.getMiscellaneousdefaultdriver()!=null && (!sd.getMiscellaneousdefaultdriver().equalsIgnoreCase(""))){
	        				subcontractorCharges.setPersonId(sysDefaultDetail.get(0).getMiscellaneousdefaultdriver());
	        				String driverLastName=partnerManager.findByOwnerOpLastNameByCode(sysDefaultDetail.get(0).getMiscellaneousdefaultdriver());
	        				if(driverLastName!=null){
	        					subcontractorCharges.setPersonName(driverLastName);	
	        				}
	        				subcontractorCharges.setPersonType("D");
	        			}else{
	        				subcontractorCharges.setPersonId("");
	        				subcontractorCharges.setPersonName("");	
	    	         		subcontractorCharges.setPersonType("D");
	        			}
	        		}
	         	}*/
	           /*if(vanLine.getDriverID()!=null && (!vanLine.getDriverID().equalsIgnoreCase("")))
	         	{
	         		subcontractorCharges.setPersonId(vanLine.getDriverID());
	         		subcontractorCharges.setPersonName(vanLine.getDriverName());
	         		subcontractorCharges.setPersonType("A");
	         	}*/
	        	
			
			}else{
				try{
				Iterator<SubcontractorCharges> 	iterator = subcontractorChargesExt.iterator();
				while(iterator.hasNext()){
				 subcontractorCharges =iterator.next();
				 if((subcontractorCharges.getVanLineNumber() !=null) && (!(subcontractorCharges.getVanLineNumber().toString().trim().equals(""))) && subcontractorCharges.getAccountSent()==null){
					
					 if(subcontractorCharges.getVanLineNumber().toString().trim().equals("A"+vanLine.getId())){
							subcontractorCharges.setParentId(vanLine.getId()); 
				        	subcontractorCharges.setDescription(vanLine.getDescription());
				        	subcontractorCharges.setCorpID(vanLine.getCorpID());
				        	subcontractorCharges.setBranch(vanLine.getAgent());
				        	subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
				        	//subcontractorCharges.setPersonType("A");
				        	//subcontractorCharges.setVanLineNumber("A"+vanLine.getId()); 
				        	subcontractorCharges.setGlCode(vanLine.getGlCode());
				        	BigDecimal  multiplyValue = new BigDecimal(-1.0);
				        	if(vanLine.getAmtDueAgent()!=null){ 
				        	subcontractorCharges.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
				        	}
				        	if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
							//String companyDivision="";
							//companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
							//subcontractorCharges.setCompanyDivision(companyDivision); 
							//List vanLineAgent = vanLineManager.findVanLineAgent(vanLine.getAgent(),sessionCorpID);
							/*if(!vanLineAgent.isEmpty()){
							try{	
							String	agentCodeName = vanLineAgent.get(0).toString();
							String [] agentCodeNameDetailArr = agentCodeName.split("~");
							subcontractorCharges.setPersonId(agentCodeNameDetailArr[0]);
							subcontractorCharges.setPersonName(agentCodeNameDetailArr[1]);
							}catch(Exception e){ }
							}*/ 
							 
							if(vanLine.getAgent().indexOf("U")==0){
								//subcontractorCharges.setPersonId(uvlBillToCode);
								//subcontractorCharges.setPersonName(uvlBillToName);
							}
							if(vanLine.getAgent().indexOf("M")==0){
								//subcontractorCharges.setPersonId(mvlBillToCode);
								//subcontractorCharges.setPersonName(mvlBillToName);
								
							}
							}
				        	subcontractorCharges.setApproved(vanLine.getWeekEnding()); 
				        	if(ownbillingVanline){
				        		try{
					        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
					        		String strDate = sm.format(new Date());
					        		Date dt = sm.parse(strDate);
						        	subcontractorCharges.setApproved(dt);
					        		}catch(Exception e){
					        			e.printStackTrace();
					        		}	
					        }
				        	subcontractorCharges.setAdvanceDate(vanLine.getWeekEnding());
				        	subcontractorCharges.setDivision(vanLine.getDivision());
				 			//if(autuPopPostDate){
								//change for posting date flexiblity Start	
								try{
									//company=companyManager.findByCorpID(sessionCorpID).get(0);
						    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
						    			Date dt1=vanLine.getWeekEnding();
							    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							    		String dt11=sdfDestination.format(dt1);
							    		Date dt2 =null;
							    		try{
							    		dt2 = new Date();
							    		}catch(Exception e){
							    			e.printStackTrace();
							    		}				    		
							    		
							    		dt2=payDate;
							    		if(dt2!=null){
								    		if(dt1.compareTo(dt2)>0){
								    			payDate=dt2;
								    		}else{
								    			payDate=dt1;
								    		}
							    		}
						    		}
							}catch(Exception e){
								e.printStackTrace();
							}  	 
				 			//}
				 			if(payDate!=null){ 
				 				subcontractorCharges.setPost(payDate); 
			 				} 
				        	//subcontractorCharges.setCreatedOn(new Date());
				        	subcontractorCharges.setUpdatedOn(new Date());
				        	//subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
				         	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
				         	//subcontractorCharges.setDeductTempFlag(false);
				         	//subcontractorCharges.setIsSettled(false);
				        	subcontractorChargesManager.save(subcontractorCharges);
				        	
								
					}
					 if(subcontractorCharges.getVanLineNumber().toString().trim().equals("D"+vanLine.getId())){
						 subcontractorCharges.setParentId(vanLine.getId()); 
						 subcontractorCharges.setDescription(vanLine.getDescription());
						 subcontractorCharges.setCorpID(vanLine.getCorpID());
						 subcontractorCharges.setBranch(vanLine.getAgent());
						 subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
						 subcontractorCharges.setVanLineNumber("D"+vanLine.getId());
						 subcontractorCharges.setPersonType("D");
						 subcontractorCharges.setPersonId(vanLine.getDriverID());
						 subcontractorCharges.setPersonName(vanLine.getDriverName());
						 subcontractorCharges.setGlCode(vanLine.getGlCode());
				        	//BigDecimal  multiplyValue = new BigDecimal(-1.0);
						 if(ownbillingVanline){
				        	if(vanLine.getAmtDueAgent()!=null){ 
				        		subcontractorCharges.setAmount(vanLine.getAmtDueAgent());
				        	}
						 }else{
							 if(automaticReconcile){
				        		BigDecimal  multiplyValue = new BigDecimal(-1.0);
					        	if(vanLine.getAmtDueAgent()!=null){ 
					        		subcontractorCharges.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
					        	}
							 }else{ 
						      if(vanLine.getAmtDueAgent()!=null){ 
						        		subcontractorCharges.setAmount(vanLine.getAmtDueAgent());
						        	} 
							 }
						 }
				        	subcontractorCharges.setCompanyDivision(vanLine.getCompanyDivision());
				        	
				        	subcontractorCharges.setApproved(vanLine.getWeekEnding()); 
				        	if(ownbillingVanline){
				        		try{
					        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
					        		String strDate = sm.format(new Date());
					        		Date dt = sm.parse(strDate);
						        	subcontractorCharges.setApproved(dt);
					        		}catch(Exception e){
					        			e.printStackTrace();
					        		}
						    }
				        	subcontractorCharges.setAdvanceDate(vanLine.getWeekEnding());
				        	subcontractorCharges.setDivision(vanLine.getDivision());
				 			//if(autuPopPostDate){
								//change for posting date flexiblity Start	
								try{
									//company=companyManager.findByCorpID(sessionCorpID).get(0);
						    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
						    			Date dt1=vanLine.getWeekEnding();
							    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
							    		String dt11=sdfDestination.format(dt1);
							    		Date dt2 =null;
							    		try{
							    		dt2 = new Date();
							    		}catch(Exception e){
							    			e.printStackTrace();
							    		}				    		
							    		
							    		dt2=payDate;
							    		if(dt2!=null){
								    		if(dt1.compareTo(dt2)>0){
								    			payDate=dt2;
								    		}else{
								    			payDate=dt1;
								    		}
							    		}
						    		}
							}catch(Exception e){
								e.printStackTrace();
							}  	 
				 			//}
				 			if(payDate!=null){ 
				 				subcontractorCharges.setPost(payDate); 
			 				} 
				 			 
				 			subcontractorCharges.setUpdatedOn(new Date());
				 			 
				 			subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
				 			 
				        	subcontractorChargesManager.save(subcontractorCharges);
							
						 
					 }
				 }
				}
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			}
			}catch(Exception e){
				e.printStackTrace();
			}
			}

			
			/*list();*/
			if (gotoPageString.equalsIgnoreCase("") || gotoPageString == null) {
				String key = (isNew) ? "Settlement has been added successfully" : "Settlement has been updated successfully";
				saveMessage(getText(key));
			}
		
		if (!isNew) {
			splitChargeOnsave();
			//totalAmount();
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
				 }
			return INPUT;
		} else {
			splitChargeOnsave();
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
				 }
			//totalAmount();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
		}
	}
	
	public String saveSplitCharge()  throws Exception {
		boolean isNew = (vanLine.getId() == null);
		Boolean autuPopPostDate=false;
    	Date payDate=null;
    	String uvlBillToCode="";
    	String uvlBillToName="";
    	String mvlBillToCode ="";
    	String mvlBillToName ="";
		if (isNew) {
			vanLine.setCreatedOn(new Date());
		}
		vanLine.setUpdatedOn(new Date());
		vanLine.setUpdatedBy(getRequest().getRemoteUser());
		vanLine.setCorpID(sessionCorpID);
		if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
			if(vanLine.getCompanyDivision()==null || vanLine.getCompanyDivision().toString().trim().equals("")){
			String companyDivision="";
			companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
			vanLine.setCompanyDivision(companyDivision);
			}
		}
		if(validateFormNav ==null || (!(validateFormNav.equals("OK")))){
			vanlineSaveFlag=true;
			}
		vanLine = vanLineManager.save(vanLine);
		SplitChargeShowFlag=true;
		try{
			//sysDefaultDetail = vanLineManager.findsysDefault(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault : sysDefaultDetail) {
					automaticReconcile=	systemDefault.getAutomaticReconcile();
					uvlBillToCode=systemDefault.getUvlBillToCode();
					vanlineMinimumAmount=systemDefault.getVanlineMinimumAmount();
					if(systemDefault.getVanlineExceptionChargeCode()!=null){
						vanlineExceptionChargeCode =systemDefault.getVanlineExceptionChargeCode();
					}
					if(systemDefault.getOwnbillingVanline()!=null && systemDefault.getOwnbillingVanline()){
						ownbillingVanline=systemDefault.getOwnbillingVanline();
					}
					autuPopPostDate=systemDefault.getAutoPayablePosting();
 					payDate=systemDefault.getPostDate1();
 					uvlBillToCode=systemDefault.getUvlBillToCode();
 					uvlBillToName=systemDefault.getUvlBillToName();
 					mvlBillToCode=systemDefault.getMvlBillToCode();
 					mvlBillToName=systemDefault.getMvlBillToName();
				}
				
			}
			if(((vanLine.getRegNum()==null ) || (vanLine.getRegNum().equals(""))) && ((vanLine.getReconcileStatus()!=null) && (vanLine.getReconcileStatus().equalsIgnoreCase("Approved"))) ){
				if(automaticReconcile && ownbillingVanline){
					 
				subcontractorChargesExt = vanLineManager.splitCharge(id);
				if((subcontractorChargesExt.isEmpty())){
	        	subcontractorCharges=new SubcontractorCharges();  
	        	//vanLine=vanLineManager.get(vid);
	        	subcontractorCharges.setParentId(vanLine.getId()); 
	        	subcontractorCharges.setDescription(vanLine.getDescription());
	        	subcontractorCharges.setCorpID(vanLine.getCorpID());
	        	subcontractorCharges.setBranch(vanLine.getAgent());
	        	subcontractorCharges.setRegNumber(vanLine.getRegNum()); 
	        	subcontractorCharges.setVanLineNumber("A"+vanLine.getId());
	        	subcontractorCharges.setPersonType("A");
	        	subcontractorCharges.setGlCode(vanLine.getGlCode());
	        	BigDecimal  multiplyValue = new BigDecimal(-1.0);
	        	if(vanLine.getAmtDueAgent()!=null){ 
	        	subcontractorCharges.setAmount(vanLine.getAmtDueAgent().multiply(multiplyValue));
	        	}
	        	if(vanLine.getAgent()!=null && (!(vanLine.getAgent().equals("")))){
				String companyDivision="";
				companyDivision=vanLineManager.findCompanyDivision(vanLine.getAgent(),sessionCorpID);
				subcontractorCharges.setCompanyDivision(companyDivision); 
				//List vanLineAgent = vanLineManager.findVanLineAgent(vanLine.getAgent(),sessionCorpID);
				/*if(!vanLineAgent.isEmpty()){
				try{	
				String	agentCodeName = vanLineAgent.get(0).toString();
				String [] agentCodeNameDetailArr = agentCodeName.split("~");
				subcontractorCharges.setPersonId(agentCodeNameDetailArr[0]);
				subcontractorCharges.setPersonName(agentCodeNameDetailArr[1]);
				}catch(Exception e){ }
				}*/ 
				 
				if(vanLine.getAgent().indexOf("U")==0){
					subcontractorCharges.setPersonId(uvlBillToCode);
					subcontractorCharges.setPersonName(uvlBillToName);
				}
				if(vanLine.getAgent().indexOf("M")==0){
					subcontractorCharges.setPersonId(mvlBillToCode);
					subcontractorCharges.setPersonName(mvlBillToName);
					
				}
				}
	        	subcontractorCharges.setApproved(vanLine.getWeekEnding());
	        	if(ownbillingVanline){
	        		try{
		        		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		        		String strDate = sm.format(new Date());
		        		Date dt = sm.parse(strDate);
			        	subcontractorCharges.setApproved(dt);
		        		}catch(Exception e){
		        			e.printStackTrace();
		        		}
			    }
	        	subcontractorCharges.setAdvanceDate(vanLine.getWeekEnding());
	        	subcontractorCharges.setDivision(vanLine.getDivision());
	        	
	 			//if(autuPopPostDate){
					//change for posting date flexiblity Start	
					try{
						//company=companyManager.findByCorpID(sessionCorpID).get(0);
			    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
			    			Date dt1=vanLine.getWeekEnding();
				    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				    		String dt11=sdfDestination.format(dt1);
				    		Date dt2 =null;
				    		try{
				    		dt2 = new Date();
				    		}catch(Exception e){
				    			e.printStackTrace();
				    		}				    		
				    		
				    		dt2=payDate;
				    		if(dt2!=null){
					    		if(dt1.compareTo(dt2)>0){
					    			payDate=dt2;
					    		}else{
					    			payDate=dt1;
					    		}
				    		}
			    		}
				}catch(Exception e){
					e.printStackTrace();
				}  	 
	 			//}
	 			if(payDate!=null){ 
	 				subcontractorCharges.setPost(payDate); 
 				} 
	        	subcontractorCharges.setCreatedOn(new Date());
	        	subcontractorCharges.setUpdatedOn(new Date());
	        	subcontractorCharges.setCreatedBy(getRequest().getRemoteUser());
	         	subcontractorCharges.setUpdatedBy(getRequest().getRemoteUser()); 
	         	subcontractorCharges.setDeductTempFlag(false);
	 			subcontractorCharges.setIsSettled(false);
	        	subcontractorChargesManager.save(subcontractorCharges);
				}
				}
			}
			splitCharge();
			totalAmount();
			if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
				 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
				 }		
		}catch(Exception e){
			e.printStackTrace();
		}
	return SUCCESS;	
	}
	
	private String baseCurrency;
	private Charges charges;
	private PartnerManager partnerManager;
	public String commLine1Id;
    public String commLine2Id;
    public List accListBycompDiv;
	public String isPayableRequired;
	Map <String,String > offGridSelected=new HashMap<String, String>();
    private Company company;
	private CompanyManager companyManager;	
	/* method for calculate commission for crown 
	 * Date : 28th of feb, 2012	
	 */
		@SkipValidation
		public void calculateCommisssionMethod(){
			int invCount =0;
			Map <String,BigDecimal > compDivComm  = new   HashMap<String,BigDecimal >();
			compDivComm.put(serviceOrder.getCompanyDivision(),new BigDecimal("0.00"));
			Map <String,BigDecimal > compDivRev = new   HashMap<String,BigDecimal >();
			compDivRev.put(serviceOrder.getCompanyDivision(),new BigDecimal("0.00"));
			Map <String,String > compDivAccId = new   HashMap<String,String >();
			compDivAccId.put(serviceOrder.getCompanyDivision(),"");
			serviceOrder = serviceOrderManager.get(sid);
			Map<String,String> commissionMap=new LinkedHashMap<String, String>();
			String aidList="";
			if((serviceOrder.getJob()!=null)&&(serviceOrder.getJob().trim().equalsIgnoreCase("INT"))){
				commissionMap=serviceOrderManager.checkShipEntryInCommission(sessionCorpID,"COMMISSION",serviceOrder.getShipNumber());
				String aidListTemp=serviceOrderManager.findAccIdNeedToBlock(sid,sessionCorpID);
				for(String str:aidListTemp.split(",")){
					if(commissionMap.containsKey(str)){
						if(aidList.equalsIgnoreCase("")){
							aidList="'"+str+"'";
						}else{
							aidList=aidList+",'"+str+"'";
						}
					}
				}
			}

			billing = billingManager.get(sid);
			trackingStatus=trackingStatusManager.get(sid);
			miscellaneous = miscellaneousManager.get(sid);
			baseCurrency=accountLineManager.searchBaseCurrency(sessionCorpID).get(0).toString();
			
			List accountIdList = serviceOrderManager.findAccountlines(sid);
			if((commissionMap!=null)&&(!commissionMap.isEmpty())){
				if(accountIdList!=null && !accountIdList.isEmpty()){
					List al=new ArrayList();
					Iterator itr1 =accountIdList.iterator();
					while(itr1.hasNext()){
						String s=itr1.next()+"".trim().toString();
						if(!commissionMap.containsKey(s)){
							al.add(s);
						}
					}
					accountIdList.clear();
					accountIdList.addAll(al);
				}						
			}			
			
			if(accountIdList!=null && !accountIdList.isEmpty()){
				Iterator itr1 =accountIdList.iterator();
			while(itr1.hasNext()){
				accountLine = accountLineManager.get(Long.parseLong(itr1.next().toString()));
				if(accountLine.getRecInvoiceNumber()!=null && !(accountLine.getRecInvoiceNumber().toString().trim().equals(""))){
					invCount++;
					break;
				}
			}
			}
			if(invCount > 0){
			List compDivList = serviceOrderManager.findAccountLineCompDiv(sid,sessionCorpID);
			
			if(compDivList!=null && !compDivList.isEmpty()){
				BigDecimal totalOffRev1 = new BigDecimal(0);
				BigDecimal totalOffExp1 = new BigDecimal(0);
				offGridSelected = new HashMap<String,String>();		
			Iterator itrCompDiv =compDivList.iterator();
				while(itrCompDiv.hasNext()){
						BigDecimal temp =  new BigDecimal("00.00");
						String tempCompDiv = itrCompDiv.next().toString();
						compDivComm.put(tempCompDiv, temp);
						compDivAccId.put(tempCompDiv,"");
					}
		
			
			//compDivComm.put(serviceOrder.getCompanyDivision(),new BigDecimal("0.00"));
			
			
			// Calculation for Third party charge code 
			
			BigDecimal rev1 = new BigDecimal(0);
			BigDecimal exp1 = new BigDecimal(0);
			String chargeCode1 = "";
			
			List chargeTpDetailList1 = serviceOrderManager.getTPChargeDetail(sid,serviceOrder.getJob(),"","",sessionCorpID,aidList);  
			Iterator it1 = chargeTpDetailList1.iterator();
			while (it1.hasNext()) {
				Object chargeItr1 = (Object) it1.next();
				if (((ChargeTpDetailDTO) chargeItr1).getRevenue() != null && (!((ChargeTpDetailDTO) chargeItr1).getRevenue().toString().trim().equals(""))) {
					rev1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getRevenue().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr1).getExpense() != null && (!((ChargeTpDetailDTO) chargeItr1).getExpense().toString().trim().equals(""))) {
					exp1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getExpense().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr1).getCharge() != null && (!((ChargeTpDetailDTO) chargeItr1).getCharge().toString().trim().equals(""))) {
					chargeCode1 = ((ChargeTpDetailDTO) chargeItr1).getCharge().toString();
				}
				
				// logic for reverse invoice
				List <AccountLine> reverseInvoiceIdsList = serviceOrderManager.getReverseInvoiceIds(chargeCode1,sessionCorpID,sid,aidList); 
				if(reverseInvoiceIdsList!=null && !reverseInvoiceIdsList.isEmpty()){
				String tempId1="";
				for(AccountLine accObj1 : reverseInvoiceIdsList){
					if(tempId1.equals(""))
						tempId1 = accObj1.getId().toString();
					else
						tempId1 = tempId1+","+accObj1.getId().toString();
				}
				List<AccountLine> accidList = serviceOrderManager.getAllTPAccId(chargeCode1,sessionCorpID,sid,aidList);
				String  compDiv ="";
					//String tempId = "";
					for(AccountLine accObj : accidList){
						if(accObj.getActualRevenue().compareTo(new BigDecimal(0))!=0){
							if(compDiv.equalsIgnoreCase("")){
								compDiv = accObj.getCompanyDivision();
							}else if(!compDiv.equalsIgnoreCase(accObj.getCompanyDivision())){
								 compDiv = serviceOrder.getCompanyDivision();
								break;
							}else{
								
							}
						}
					
					}
					String tempAccid = compDivAccId.get(compDiv).toString();
					
					if(tempAccid.equalsIgnoreCase(""))
						tempAccid =  tempId1 ;
					else
						tempAccid = tempAccid + "," +tempId1;
					
					compDivAccId.put(compDiv,tempAccid);
					
			}
			
			if(rev1.compareTo(new BigDecimal(0))!= 0 && exp1.compareTo(new BigDecimal(0))!= 0){
				itrCompDiv =compDivList.iterator();
				
				while(itrCompDiv.hasNext()){
				
					BigDecimal totalActualCommRevenue = new BigDecimal(0);
					BigDecimal totalActualCommExpence = new BigDecimal(0);
					BigDecimal commissionableProfitPer = new BigDecimal(0);
					BigDecimal commissionableProfitPer1 = new BigDecimal(0);
					BigDecimal commissionableRatePer = new BigDecimal(0);
					BigDecimal commissionForTP = new BigDecimal(0);
					int countRec = 0;
					int countPay = 0;
					String compDiv ="";
					compDiv =itrCompDiv.next().toString();
						
			List chargeTpDetailList = serviceOrderManager.getTPChargeDetail(sid,serviceOrder.getJob(),compDiv,chargeCode1,sessionCorpID,aidList);  
			Iterator it = chargeTpDetailList.iterator();
			while (it.hasNext()) {
			List<AccountLine> accidList = serviceOrderManager.getAllTPAccId(chargeCode1,sessionCorpID,sid,aidList);
				// compDiv ="";
				String tempId = "";
				/*for(AccountLine accObj : accidList){
					if(accObj.getActualRevenue().compareTo(new BigDecimal(0))!=0){
						if(compDiv.equalsIgnoreCase("")){
							compDiv = accObj.getCompanyDivision();
						}else if(!compDiv.equalsIgnoreCase(accObj.getCompanyDivision())){
							 compDiv = serviceOrder.getCompanyDivision();
							break;
						}else{
							
						}
					}
				
				}*/
				if(compDiv.equalsIgnoreCase("")){
					compDiv=serviceOrder.getCompanyDivision();
				}
				
				/*if(!tempId1.equals("")){
					tempId =tempId1;
				}*/
				 totalOffRev1 = new BigDecimal(0);
				 totalOffExp1 = new BigDecimal(0);
				
				for(AccountLine accObj : accidList){
					if(tempId.equals("")){
						tempId = accObj.getId().toString();
					}else{
						tempId = tempId+","+accObj.getId().toString();
					}
					totalOffRev1=totalOffRev1.add(accObj.getActualRevenue());
					totalOffExp1=totalOffExp1.add((accObj.getActualExpense()!=null && accObj.getActualExpense().doubleValue()!=0)?accObj.getActualExpense():accObj.getRevisionExpense());
				}
				
				Double OffRevExDif=totalOffRev1.doubleValue()-totalOffExp1.doubleValue();
				if(totalOffRev1.doubleValue()!=0){
					OffRevExDif=(OffRevExDif/totalOffRev1.doubleValue())*100;	
				}else{
					OffRevExDif=0.00;
				}
				
				BigDecimal temp = new BigDecimal(0);
				String tempAccid = compDivAccId.get(compDiv).toString();
				temp = (BigDecimal) compDivComm.get(compDiv);
				Object chargeItr = (Object) it.next();
				BigDecimal rev = new BigDecimal(0);
				BigDecimal exp = new BigDecimal(0);
				BigDecimal commission = new BigDecimal(0);
				String ChargeCode = "";
				if (((ChargeTpDetailDTO) chargeItr).getRevenue() != null && (!((ChargeTpDetailDTO) chargeItr).getRevenue().toString().trim().equals(""))) {
					rev = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getRevenue().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getExpense() != null && (!((ChargeTpDetailDTO) chargeItr).getExpense().toString().trim().equals(""))) {
					exp = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getExpense().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getCommission() != null && (!((ChargeTpDetailDTO) chargeItr).getCommission().toString().trim().equals(""))) {
					commission = new BigDecimal(((ChargeTpDetailDTO) chargeItr).getCommission().toString());
				}
				if (((ChargeTpDetailDTO) chargeItr).getCharge() != null && (!((ChargeTpDetailDTO) chargeItr).getCharge().toString().trim().equals(""))) {
					ChargeCode = ((ChargeTpDetailDTO) chargeItr).getCharge().toString();
				}
				
				try{
					BigDecimal tempComm = new BigDecimal(0);
					if(serviceOrder.getJob()!=null && !serviceOrder.getJob().equalsIgnoreCase("OFF")){
					tempComm = rev.subtract(exp);
					tempComm = tempComm.multiply(commission);
					tempComm= tempComm.divide(new BigDecimal(100),4);
					}
					try{
						 //11050 - TP commission for OFF for jobs  Start
						if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF")){
							String param="'OFFCOMMISSION'";
							List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,param);
							for (RefMasterDTO refObj : allParamValue) {
								if(OffRevExDif>=Double.parseDouble(refObj.getFlex1())&&OffRevExDif<=Double.parseDouble(refObj.getFlex2())){
									if(Double.parseDouble(refObj.getBucket2())!=0){
										if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Revenue")){
											tempComm=totalOffRev1.multiply(new BigDecimal(refObj.getBucket2()));
											tempComm= tempComm.divide(new BigDecimal(100),4);
											offGridSelected.put(ChargeCode, "Revenue"+"~"+refObj.getBucket2());
										}else if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Margin")){
											tempComm=totalOffRev1.subtract(totalOffExp1);
											tempComm=tempComm.multiply(new BigDecimal(refObj.getBucket2()));
											tempComm= tempComm.divide(new BigDecimal(100),4);
											offGridSelected.put(ChargeCode, "Margin"+"~"+refObj.getBucket2());
										}
									}else{
										tempComm = new BigDecimal(0);
									}
								}							
							}
						}
						//11050 - TP commission for OFF for jobs  End
						}catch(Exception e){e.printStackTrace();}
					commissionForTP = commissionForTP.add(tempComm); 
					temp = temp.add(commissionForTP);
					compDivComm.put(compDiv, temp);
					
					if(tempAccid.equalsIgnoreCase(""))
						tempAccid =  tempId ;
					else
						tempAccid = tempAccid + "," +tempId;
					
					compDivAccId.put(compDiv,tempAccid);
					
					serviceOrderManager.updateSetteledDateOfToCharge(ChargeCode,sid); 
				
				}catch(Exception e){
					System.out.println("\n\nError in calculating TP ammount. \n\nErrors : -"+e);
				}
				/*else if(rev.compareTo(new BigDecimal(0))!= 0 && exp.compareTo(new BigDecimal(0))== 0){
					List chargeTpDetailList1 = serviceOrderManager.getTPChargeDetail(sid,serviceOrder.getJob(),"","","",""); 
					Iterator it1 = chargeTpDetailList1.iterator();
					while (it1.hasNext()) {
						Object chargeItr1 = (Object) it1.next();
						BigDecimal rev1 = new BigDecimal(0);
						BigDecimal exp1 = new BigDecimal(0);
						BigDecimal commission1 = new BigDecimal(0);
						String ChargeCode1 = "";
						if (((ChargeTpDetailDTO) chargeItr1).getRevenue() != null && (!((ChargeTpDetailDTO) chargeItr1).getRevenue().toString().trim().equals(""))) {
							rev1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getRevenue().toString());
						}
						if (((ChargeTpDetailDTO) chargeItr1).getExpense() != null && (!((ChargeTpDetailDTO) chargeItr1).getExpense().toString().trim().equals(""))) {
							exp1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getExpense().toString());
						}
						if (((ChargeTpDetailDTO) chargeItr1).getCommission() != null && (!((ChargeTpDetailDTO) chargeItr1).getCommission().toString().trim().equals(""))) {
							commission1 = new BigDecimal(((ChargeTpDetailDTO) chargeItr1).getCommission().toString());
						}
						if (((ChargeTpDetailDTO) chargeItr1).getCharge() != null && (!((ChargeTpDetailDTO) chargeItr1).getCharge().toString().trim().equals(""))) {
							ChargeCode1 = ((ChargeTpDetailDTO) chargeItr1).getCharge().toString();
						}
						BigDecimal tempComm1 = new BigDecimal(0);
						if(rev1.compareTo(new BigDecimal(0))!= 0 && exp1.compareTo(new BigDecimal(0))!= 0){
							tempComm1 = rev1.subtract(exp);
							tempComm1 = tempComm1.multiply(commission1);
							tempComm1 = tempComm1.divide(new BigDecimal(100),3);
							commissionForTP = commissionForTP.add(tempComm);
							serviceOrderManager.updateSetteledDateOfToCharge(ChargeCode,sid);
						}
					}
				}*/
			/*else{
					System.out.println("\n\n\n\n\n\n\n Actual expence and actual revenue is zero" );
				}*/
			}
			}
			}
			
			
			}
			
			
			
			
			
			
			
			
			
			
			
						itrCompDiv =compDivList.iterator();
						while(itrCompDiv.hasNext()){
							
							BigDecimal totalActualCommRevenue = new BigDecimal(0);
							BigDecimal totalActualCommExpence = new BigDecimal(0);
							BigDecimal commissionableProfitPer = new BigDecimal(0);
							BigDecimal commissionableProfitPer1 = new BigDecimal(0);
							BigDecimal commissionableRatePer = new BigDecimal(0);
							BigDecimal commissionForTP = new BigDecimal(0);
							int countRec = 0;
							int countPay = 0;
							String compDiv ="";
							commLine1Id="";
							commLine2Id="";
							compDiv =itrCompDiv.next().toString();
							
							
							accListBycompDiv = null;
							accListBycompDiv = serviceOrderManager.findAccountLineListByCompDiv(sid,sessionCorpID,compDiv,aidList);
							if(accListBycompDiv!=null && !accListBycompDiv.isEmpty()){
								Iterator itr = accListBycompDiv.iterator(); 
						
							
						//if(invCount > 0){
						//Iterator itr =accountIdList.iterator();
						while(itr.hasNext()){
							accountLine = accountLineManager.get(Long.parseLong(itr.next().toString()));
							String contact = accountLine.getContract();
							String chargeCode = accountLine.getChargeCode();
							List tempList =  serviceOrderManager.getCommissionableValue(contact,chargeCode,sessionCorpID,serviceOrder.getJob()); 
							if(tempList != null && !(tempList.isEmpty())){
								charges = (Charges) tempList.get(0);
							}else{
								charges = null ;
							}
							if(charges != null && charges.getCommissionable() == true){
								if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
									if(!charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){
										BigDecimal tempComm = new BigDecimal(0);
										BigDecimal temp = new BigDecimal(0);
										temp = (BigDecimal) compDivComm.get(compDiv);
										tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
										tempComm = (tempComm.multiply(new BigDecimal(charges.getCommission().toString()))).divide(new BigDecimal(100),4);
										//commissionableProfitPer1 = commissionableProfitPer1.add(tempComm);
										temp = temp.add(tempComm);
										compDivComm.put(compDiv,temp);
										accountLine.setSettledDate(new Date());
										accountLine = accountLineManager.save(accountLine);
										
										String tempId = compDivAccId.get(compDiv).toString();
										if(tempId.equalsIgnoreCase(""))
											tempId = accountLine.getId().toString();
										else
											tempId = tempId+","+accountLine.getId().toString();
										
										compDivAccId.put(compDiv,tempId);
									}
								}else{
									BigDecimal temp = new BigDecimal(0);
									temp = (BigDecimal) compDivComm.get(compDiv);
									String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
									List contractNameList = serviceOrderManager.getInternalCostContract(compDiv,sessionCorpID,serviceOrder.getJob(),chargeCodeCom1); 
									String contractName="";
									if(contractNameList!=null && !(contractNameList.isEmpty())){
										contractName = contractNameList.get(0).toString();
									}
									
									if(accountLine.getActualRevenue().compareTo(new BigDecimal(0))!= 0 ){
										commissionableProfitPer=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
										commissionableProfitPer = commissionableProfitPer.divide(accountLine.getActualRevenue(),4);
										commissionableProfitPer = commissionableProfitPer.multiply(new BigDecimal(100));
										List <RateGrid> rateList =  serviceOrderManager.getRateGirdForComm(chargeCodeCom1,contractName,sessionCorpID);
										if((rateList!=null)&&(!rateList.isEmpty()))	{
								    		for (RateGrid rateGrid : rateList) {
								    			if(commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))== 0 || commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))==-1 ){
								    				commPercentage = rateGrid.getRate1().toString();
								    				break;
								    			}
								    		}
								    	}
										BigDecimal tempComm = new BigDecimal(0.00);
										try{
											 //11050 - TP commission for OFF for jobs  Start
											if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){/*
												BigDecimal marginValue = new BigDecimal(0);
												BigDecimal marginPersentageValue = new BigDecimal(0);
												marginValue=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
												marginPersentageValue=marginValue.divide(accountLine.getActualRevenue(),4);
												marginPersentageValue=marginPersentageValue.multiply(new BigDecimal(100));
												String param="'OFFCOMMISSION'";
												List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,param);
												for (RefMasterDTO refObj : allParamValue) {
													if(marginPersentageValue.doubleValue()>=Double.parseDouble(refObj.getFlex1())&&marginPersentageValue.doubleValue()<=Double.parseDouble(refObj.getFlex2())){
														if(Double.parseDouble(refObj.getBucket2())!=0){
															if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Revenue")){
																tempComm=accountLine.getActualRevenue().multiply(new BigDecimal(refObj.getBucket2()));
																tempComm= tempComm.divide(new BigDecimal(100),4);										
															}else if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Margin")){
																tempComm=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
																tempComm=tempComm.multiply(new BigDecimal(refObj.getBucket2()));
																tempComm= tempComm.divide(new BigDecimal(100),4);										
															}
														}else{
															tempComm = new BigDecimal(0);
														}
													}							
												}
											*/}else{
												if(commPercentage!=null && (new BigDecimal(commPercentage)).compareTo(new BigDecimal(0))!=0){
													//BigDecimal tempComm = (accountLine.getActualRevenue().multiply(new BigDecimal(commPercentage))).divide(new BigDecimal(100),2);
													
													if(serviceOrder.getJob()!=null && !serviceOrder.getJob().equalsIgnoreCase("OFF")){
													try{
														double tempDoubleVal = accountLine.getActualRevenue().doubleValue();
														double tempCommVal =  Double.parseDouble(commPercentage) ;
														tempDoubleVal = ((tempDoubleVal*100)*(tempCommVal/100))/100;
														tempComm = new BigDecimal(tempDoubleVal);
													}catch(Exception e){
														e.printStackTrace();
													}
													}
												}												
											}
											//9494 - OFF - New Commission Structure for Third party  End
											}catch(Exception e){e.printStackTrace();}										

											temp = temp.add(tempComm);
											compDivComm.put(compDiv,temp);
											accountLine.setSettledDate(new Date());
											accountLine = accountLineManager.save(accountLine);
										
										totalActualCommRevenue = totalActualCommRevenue.add(accountLine.getActualRevenue());
										compDivRev.put(compDiv, totalActualCommRevenue);
										

										String tempId = compDivAccId.get(compDiv).toString();
										if(tempId.equalsIgnoreCase(""))
											tempId = accountLine.getId().toString();
										else
											tempId = tempId+","+accountLine.getId().toString();
										
										compDivAccId.put(compDiv,tempId);
									}
									
									/*totalActualCommRevenue = totalActualCommRevenue.add(accountLine.getActualRevenue());
									totalActualCommExpence = totalActualCommExpence.add(accountLine.getActualExpense());*/
									accountLine.setSettledDate(new Date());
									accountLine = accountLineManager.save(accountLine);
								}
								
							}
						}
					}	
				}
							
						//if(totalActualCommRevenue.compareTo(new BigDecimal(0))== 1 && totalActualCommExpence.compareTo(new BigDecimal(0))== 1){
						/*if(totalActualCommRevenue.compareTo(new BigDecimal(0))!= 0 ){
							System.out.println("\n\n\n\n\n\n\nEx"+totalActualCommExpence+"\n\n\n\n\n\n\n\n\n\nRE"+totalActualCommRevenue);
							commissionableProfitPer=totalActualCommRevenue.subtract(totalActualCommExpence);
							System.out.println(commissionableProfitPer+"\n\n\n\n\n\n\n");
							commissionableProfitPer = commissionableProfitPer.divide(totalActualCommRevenue,3);
							System.out.println(commissionableProfitPer+"\n\n\n\n\n\n\n");
							commissionableProfitPer = commissionableProfitPer.multiply(new BigDecimal(100));
							System.out.println(commissionableProfitPer+"\n\n\n\n\n\n\n");
						}*/
							// Commission 1st line 
			}	
			
			
			
			if(compDivComm!=null && !compDivComm.isEmpty()){
				for (Map.Entry<String, BigDecimal> entry : compDivComm.entrySet()) {
					commLine1Id="";
					commLine2Id="";
				String vendorCode1="";
				String vendorName1="";
				String compDiv = entry.getKey().toString();
				List listUser1 = serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getSalesMan());
				
							
				if(listUser1!=null && !listUser1.isEmpty()){
					vendorCode1 = listUser1.get(0).toString();
				}
					if(vendorCode1!=null && !(vendorCode1.trim().equals(""))){
						AccountLine commAccountLine=null;
						String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
						List accIdList = serviceOrderManager.findAccountLineByCharge(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);
						if(accIdList!=null && !(accIdList.isEmpty()) ){
							String tempid= accIdList.get(0).toString();
							 commAccountLine =accountLineManager.get(Long.parseLong(tempid));
							 commAccountLine.setAccountLineNumber(commAccountLine.getAccountLineNumber());
							 accountLineNumber=commAccountLine.getAccountLineNumber();
						}else{
							
							
							 commAccountLine = new AccountLine();
							 boolean activateAccPortal =true;
								try{
								if(accountLineAccountPortalFlag){	
								activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
								commAccountLine.setActivateAccPortal(activateAccPortal);
								}
								}catch(Exception e){
									e.printStackTrace();
								}
							commAccountLine.setCreatedBy("COMM:"+getRequest().getRemoteUser());
							commAccountLine.setCreatedOn(new Date());
							maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
							if (maxLineNumber.get(0) == null) {
								accountLineNumber = "001";
							} else {
				             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
				                 if((autoLineNumber.toString()).length() == 2) {
				                 	accountLineNumber = "0"+(autoLineNumber.toString());
				                 }else if((autoLineNumber.toString()).length() == 1) {
				                 	accountLineNumber = "00"+(autoLineNumber.toString());
				                 } else {
				                 	accountLineNumber=autoLineNumber.toString();
				                 } 
							}
							commAccountLine.setAccountLineNumber(accountLineNumber);
							  ///Code for AccountLine division 
							String divisionTemp="";
							if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
								divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
							}
							if(!divisionTemp.equalsIgnoreCase("")){
								commAccountLine.setDivision(divisionTemp);
							}else{
								commAccountLine.setDivision(null);
							}
							  ///Code for AccountLine division 
						}
						BigDecimal totalCommisionExp1 = new BigDecimal(0);
						try{
						 totalCommisionExp1 = serviceOrderManager.findTotalExpenseOfChargeACC(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);  
						}catch(Exception e){e.printStackTrace();}
						commAccountLine.setUpdatedBy("COMM:"+getRequest().getRemoteUser());
						commAccountLine.setUpdatedOn(new Date());
						commAccountLine.setServiceOrderId(sid);
						commAccountLine.setCorpID(sessionCorpID);
						commAccountLine.setBasis("");
						//commAccountLine.setCompanyDivision(serviceOrder.getCompanyDivision());
						commAccountLine.setCompanyDivision(compDiv);
						commAccountLine.setSequenceNumber(serviceOrder.getSequenceNumber());
						commAccountLine.setShipNumber(serviceOrder.getShipNumber());
						commAccountLine.setCategory("Broker");
						commAccountLine.setNote("Commission");
						commAccountLine.setSettledDate(new Date());
						List contractNameList = serviceOrderManager.getInternalCostContract(compDiv,sessionCorpID,serviceOrder.getJob(),chargeCodeCom1); 
						String contractName="";
						if(contractNameList!=null && !(contractNameList.isEmpty())){
							contractName = contractNameList.get(0).toString();
						}
						commAccountLine.setContract(contractName);
						commAccountLine.setChargeCode(chargeCodeCom1);
						try {
							String ss=chargesManager.findPopulateCostElementData(chargeCodeCom1, contractName,sessionCorpID);
							if(!ss.equalsIgnoreCase("")){
								commAccountLine.setAccountLineCostElement(ss.split("~")[0]);
								commAccountLine.setAccountLineScostElementDescription(ss.split("~")[1]);
							}
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						
						/*List <RateGrid> rateList =  serviceOrderManager.getRateGirdForComm(chargeCodeCom1,contractName,sessionCorpID);
						if((rateList!=null)&&(!rateList.isEmpty()))	{
				    		for (RateGrid rateGrid : rateList) {
				    			if(commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))== 0 || commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))==-1 ){
				    				commissionableRatePer = new BigDecimal(rateGrid.getRate1().toString());
				    				break;
				    			}
				    		}
				    	}*/
						String payRecGl = serviceOrderManager.findPayRecgl(chargeCodeCom1,sessionCorpID,contractName);
				    	String recGl="";
				    	String payGl="";
						if(payRecGl!=null && !(payRecGl.equalsIgnoreCase("")))	{
				    		String [] glcode = payRecGl.split("~");
				    		recGl=glcode[0];
				    		payGl=glcode[1];
				    	}
						commAccountLine.setRecGl(recGl);
						commAccountLine.setPayGl(payGl);
						String accountLineNumberU="";
						accountLineNumberU=accountLineNumber;
						accountLineNumberU=accountLineNumberU.trim();
						while(accountLineNumberU.indexOf("0")==0){
							accountLineNumberU=accountLineNumberU.replace("0", "");	
						}
						accountLineNumberU=accountLineNumberU.trim();
						String invoiceNumber="COMM"+accountLineNumberU.trim()+sid;
						commAccountLine.setInvoiceNumber(invoiceNumber);
						commAccountLine.setInvoiceDate(new Date());
						commAccountLine.setValueDate(new Date());
						commAccountLine.setPayingStatus("A");
						commAccountLine.setServiceOrder(serviceOrder);
						commAccountLine.setCountry(baseCurrency);
						commAccountLine.setStatus(true);
						
						/*BigDecimal comm1ActualExp = new BigDecimal(0);
						if(totalActualCommRevenue.compareTo(new BigDecimal(0))!=0 && commissionableRatePer.compareTo(new BigDecimal(0))!=0){
							comm1ActualExp = (totalActualCommRevenue.multiply(commissionableRatePer)).divide(new BigDecimal(100),2);
						}
						comm1ActualExp = comm1ActualExp.add(commissionableProfitPer1);
						BigDecimal tempComm = (BigDecimal) compDivComm.get(compDiv);
						comm1ActualExp = comm1ActualExp.add(tempComm);*/
						BigDecimal comm1ActualExp = new BigDecimal(0);
						if(!aidList.equalsIgnoreCase("")){
							BigDecimal tempamt = commAccountLine.getActualExpense()!=null && !commAccountLine.getActualExpense().toString().equals("")? commAccountLine.getActualExpense() : new BigDecimal("0.00");
							comm1ActualExp =new BigDecimal( entry.getValue().toString());
							comm1ActualExp = comm1ActualExp.add(tempamt);
						}else{
							comm1ActualExp =new BigDecimal( entry.getValue().toString());
							comm1ActualExp = comm1ActualExp.subtract(totalCommisionExp1); 
						}
						totalCommisionExp1 = new BigDecimal(0);
						commAccountLine.setActualExpense(comm1ActualExp);
						commAccountLine.setLocalAmount(comm1ActualExp);
						
						/*String vendorCode1="";
						String vendorName1="";
						List listUser1 = serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getSalesMan());
						
									
						if(listUser1!=null && !listUser1.isEmpty()){
							vendorCode1 = listUser1.get(0).toString();
						}*/
						
						commAccountLine.setVendorCode(vendorCode1);
						
						if(!vendorCode1.equalsIgnoreCase("")){
							List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode1,sessionCorpID);
							if(!listPartnerName.isEmpty()){
								vendorName1=listPartnerName.get(0).toString();
							}
						}
						if(!vendorName1.equalsIgnoreCase("")){
							String [] vendorDetail = vendorName1.split("~");
							commAccountLine.setEstimateVendorName(vendorDetail[0]);
						}else{
							commAccountLine.setEstimateVendorName("");
						}
						 String actCode="";
				          String  companyDivisionAcctgCodeUnique1="";
				 		 List companyDivisionAcctgCodeUniqueList=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
				 		 if(companyDivisionAcctgCodeUniqueList!=null && (!(companyDivisionAcctgCodeUniqueList.isEmpty())) && companyDivisionAcctgCodeUniqueList.get(0)!=null){
				 			 companyDivisionAcctgCodeUnique1 =companyDivisionAcctgCodeUniqueList.get(0).toString();
				 		 }
				         if(companyDivisionAcctgCodeUnique1.equalsIgnoreCase("Y")){
				 			actCode=partnerManager.getAccountCrossReference(vendorCode1,compDiv,sessionCorpID);
				 		}else{
				 			actCode=partnerManager.getAccountCrossReferenceUnique(vendorCode1,sessionCorpID);
				 		}
				         commAccountLine.setActgCode(actCode);				        	
				       	 //if ("YES".equals(isPayableRequired)) {
							//change for posting date flexiblity Start	
							try{
								//company=companyManager.findByCorpID(sessionCorpID).get(0);
					    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
						    		Date dt1 =null;
						    		try{
						    		dt1 = new Date();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}
						    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
						    		String dt11=sdfDestination.format(dt1);
						    		dt1=sdfDestination.parse(dt11);
						    		
						    		Date dt2 =null;
						    		try{
						    		dt2 = new Date();
						    		}catch(Exception e){
						    			e.printStackTrace();
						    		}
						    		
						    	
						    		List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										dt2=systemDefault.getPostDate1();
									}
						    		if(dt1.compareTo(dt2)>0){
						    			commAccountLine.setPayPostDate(dt2);
						    		}else{
						    			commAccountLine.setPayPostDate(dt1);
						    		}
					    		}else{
						        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										commAccountLine.setPayPostDate(systemDefault.getPostDate1());
									}
					    		}
						}catch(Exception e){
				        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
				        	for (Object object : systemDefaultList) {
								SystemDefault systemDefault = (SystemDefault)object;
								commAccountLine.setPayPostDate(systemDefault.getPostDate1());
							}
				        	e.printStackTrace();
						}			
						//change for posting date flexiblity End			        	
					//	}				        	
				         if(contractNameList!=null && !(contractNameList.isEmpty()) && comm1ActualExp.compareTo(new BigDecimal(0))!=0 ){
				        	// commLine1Id = commAccountLine.getId().toString();
				        	 commAccountLine = accountLineManager.save(commAccountLine);
				        	 commLine1Id = commAccountLine.getId().toString();
				         }
				      /*// Method call for entry in commission table
							accListBycompDiv = null;
							accListBycompDiv = serviceOrderManager.findAccountLineListByCompDiv(sid,sessionCorpID,compDiv);
							if(accListBycompDiv!=null && !accListBycompDiv.isEmpty() )
								perLineCommissionEntry(sid,commLine1Id,commLine2Id,invCount,accListBycompDiv);	*/
				}else{
					String chargeCodeCom1 = "SACOM"+serviceOrder.getJob();
					List accIdList = serviceOrderManager.findAccountLineByCharge(chargeCodeCom1,sessionCorpID,serviceOrder.getShipNumber(),compDiv);
					if(accIdList!=null && !(accIdList.isEmpty())){
						String tempid= accIdList.get(0).toString();
						AccountLine commAccountLine =accountLineManager.get(Long.parseLong(tempid));
						commAccountLine.setStatus(false);
						 accountLineManager.save(commAccountLine);
					}
				}
					
						//	compDiv = entry.getKey().toString();
							// Commission 2nd line 
							
							String vendorCode="";
							String vendorName="";
							List listUser= serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getEstimator()); 
							if(!listUser.isEmpty()){
								vendorCode = listUser.get(0).toString();
							}
							
								if(vendorCode!=null && !(vendorCode.trim().equals(""))){
									BigDecimal totalCommisionExp1 = new BigDecimal(0);
									try{
									 totalCommisionExp1 = serviceOrderManager.findTotalExpenseOfChargeACC("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv); 
									}catch(Exception e){e.printStackTrace();}
								AccountLine commAccountLine1=null;
								List accIdList1 = serviceOrderManager.findAccountLineByCharge("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv);
								if(accIdList1!=null && !(accIdList1.isEmpty())){
									String tempid= accIdList1.get(0).toString();
									commAccountLine1 =accountLineManager.get(Long.parseLong(tempid));
									commAccountLine1.setAccountLineNumber(commAccountLine1.getAccountLineNumber());
									accountLineNumber=commAccountLine1.getAccountLineNumber();
								}else{
									commAccountLine1 = new AccountLine();
									boolean activateAccPortal =true;
									try{
									if(accountLineAccountPortalFlag){	
									activateAccPortal =accountLineManager.findActivateAccPortal(serviceOrder.getJob(),serviceOrder.getCompanyDivision(),sessionCorpID);
									commAccountLine1.setActivateAccPortal(activateAccPortal);
									}
									}catch(Exception e){
										e.printStackTrace();
									}
									commAccountLine1.setCreatedBy("COMM:"+getRequest().getRemoteUser());
									commAccountLine1.setCreatedOn(new Date());
									maxLineNumber = serviceOrderManager.findMaximumLineNumber(serviceOrder.getShipNumber());
									if (maxLineNumber.get(0) == null) {
										accountLineNumber = "001";
									} else {
						             	autoLineNumber = Long.parseLong((maxLineNumber).get(0).toString()) + 1; 
						                 if((autoLineNumber.toString()).length() == 2) {
						                 	accountLineNumber = "0"+(autoLineNumber.toString());
						                 }else if((autoLineNumber.toString()).length() == 1) {
						                 	accountLineNumber = "00"+(autoLineNumber.toString());
						                 } else {
						                 	accountLineNumber=autoLineNumber.toString();
						                 } 
									}
									commAccountLine1.setAccountLineNumber(accountLineNumber);
									  ///Code for AccountLine division 
									String divisionTemp="";
									if((serviceOrder.getJob()!=null)&&(!serviceOrder.getJob().equalsIgnoreCase(""))){
									divisionTemp=refMasterManager.findDivisionInBucket2(sessionCorpID,serviceOrder.getJob());
									}
									if(!divisionTemp.equalsIgnoreCase("")){
										commAccountLine1.setDivision(divisionTemp);
									}else{
										commAccountLine1.setDivision(null);
									}
									  ///Code for AccountLine division 
								}
								
								commAccountLine1.setUpdatedBy("COMM:"+getRequest().getRemoteUser());
								commAccountLine1.setUpdatedOn(new Date());
								commAccountLine1.setBasis("");
								commAccountLine1.setCorpID(sessionCorpID);
								commAccountLine1.setServiceOrder(serviceOrder);
								commAccountLine1.setServiceOrderId(sid);
								commAccountLine1.setSequenceNumber(serviceOrder.getSequenceNumber());
								commAccountLine1.setShipNumber(serviceOrder.getShipNumber());
								commAccountLine1.setCategory("Broker");
								commAccountLine1.setChargeCode("SACOM2");
								try {
									String ss=chargesManager.findPopulateCostElementData("SACOM2", billing.getContract(),sessionCorpID);
									if(!ss.equalsIgnoreCase("")){
										commAccountLine1.setAccountLineCostElement(ss.split("~")[0]);
										commAccountLine1.setAccountLineScostElementDescription(ss.split("~")[1]);
									}
								} catch (Exception e1) {
									e1.printStackTrace();
								}

								commAccountLine1.setNote("Commission");
								commAccountLine1.setSettledDate(new Date());
								String accountLineNumberU2="";
								accountLineNumberU2=accountLineNumber;
								accountLineNumberU2=accountLineNumberU2.trim();
								while(accountLineNumberU2.indexOf("0")==0){
									accountLineNumberU2=accountLineNumberU2.replace("0", "");	
								}
								accountLineNumberU2=accountLineNumberU2.trim(); 
								String invoiceNumber2="COMM"+accountLineNumberU2.trim()+sid;
								commAccountLine1.setInvoiceNumber(invoiceNumber2); 
								commAccountLine1.setInvoiceDate(new Date());
								commAccountLine1.setValueDate(new Date());
								commAccountLine1.setPayingStatus("A");
								commAccountLine1.setCountry(baseCurrency);
								//commAccountLine1.setCompanyDivision(serviceOrder.getCompanyDivision());
								commAccountLine1.setCompanyDivision(compDiv);
								
								String payRecGl1 = serviceOrderManager.findPayRecgl("SACOM2",sessionCorpID,billing.getContract());
						    	String recGl1="";
						    	String payGl1="";
								if(payRecGl1!=null && !(payRecGl1.equalsIgnoreCase("")))	{
						    		String [] glcode = payRecGl1.split("~");
						    		recGl1=glcode[0];
						    		payGl1=glcode[1];
						    	}
								commAccountLine1.setRecGl(recGl1);
								commAccountLine1.setPayGl(payGl1);
								
								/*String vendorCode="";
								String vendorName="";
								List listUser= serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getEstimator()); 
								if(!listUser.isEmpty()){
									vendorCode = listUser.get(0).toString();
								}
								*/
								commAccountLine1.setVendorCode(vendorCode);
								
								if(!vendorCode.equalsIgnoreCase("")){
									List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode,sessionCorpID);
									if(!listPartnerName.isEmpty()){
										vendorName=listPartnerName.get(0).toString();
									}
								}
								BigDecimal comm2ActualExp = new BigDecimal(0);
								try{
									if(!vendorName.equalsIgnoreCase("")){
										String [] vendorDetail = vendorName.split("~");
										commAccountLine1.setEstimateVendorName(vendorDetail[0]);
										BigDecimal totalActualCommRevenue = new BigDecimal(compDivRev.get(compDiv).toString().trim());
										comm2ActualExp = (totalActualCommRevenue.multiply(new BigDecimal(vendorDetail[1].toString()))).divide(new BigDecimal(100),4);
									}
								}catch(Exception e){
									e.printStackTrace();
								}
								if(!aidList.equalsIgnoreCase("")){
									BigDecimal tempamt = commAccountLine1.getActualExpense()!=null && !commAccountLine1.getActualExpense().toString().equals("")? commAccountLine1.getActualExpense() :new BigDecimal("0.00");
									//comm2ActualExp =new BigDecimal( entry.getValue().toString());
									comm2ActualExp = comm2ActualExp.add(tempamt);
								}else{
									//comm2ActualExp =new BigDecimal( entry.getValue().toString());
									comm2ActualExp = comm2ActualExp.subtract(totalCommisionExp1); 
								}
								totalCommisionExp1 = new BigDecimal(0);
								commAccountLine1.setActualExpense(comm2ActualExp);
								commAccountLine1.setLocalAmount(comm2ActualExp);
								commAccountLine1.setStatus(true);
								commAccountLine1.setContract(billing.getContract());
								 String actCode1="";
						          String  companyDivisionAcctgCodeUnique2="";
						 		 List companyDivisionAcctgCodeUniqueList1=serviceOrderManager.findCompanyDivisionAcctgCodeUnique(sessionCorpID);
						 		 if(companyDivisionAcctgCodeUniqueList1!=null && (!(companyDivisionAcctgCodeUniqueList1.isEmpty())) && companyDivisionAcctgCodeUniqueList1.get(0)!=null){
						 			 companyDivisionAcctgCodeUnique2 =companyDivisionAcctgCodeUniqueList1.get(0).toString();
						 		 }
						         if(companyDivisionAcctgCodeUnique2.equalsIgnoreCase("Y")){
						 			actCode1=partnerManager.getAccountCrossReference(vendorCode,compDiv,sessionCorpID);
						 		}else{
						 			actCode1=partnerManager.getAccountCrossReferenceUnique(vendorCode,sessionCorpID);
						 		}
						        commAccountLine1.setActgCode(actCode1);
						          	 //if ("YES".equals(isPayableRequired)) {
									//change for posting date flexiblity Start	
									try{
										//company=companyManager.findByCorpID(sessionCorpID).get(0);
							    		if((company.getPostingDateFlexibility()!=null) && (company.getPostingDateFlexibility())){
								    		Date dt1 =null;
								    		try{
								    		dt1 = new Date();
								    		}catch(Exception e){
								    			e.printStackTrace();
								    		}
								    		SimpleDateFormat sdfDestination = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
								    		String dt11=sdfDestination.format(dt1);
								    		dt1=sdfDestination.parse(dt11);
								    		Date dt2 =null;
								    		try{
								    		dt2 = new Date();
								    		}catch(Exception e){
								    			e.printStackTrace();
								    		}
								    		List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
								        	for (Object object : systemDefaultList) {
												SystemDefault systemDefault = (SystemDefault)object;
												dt2=systemDefault.getPostDate1();
											}
								    		if(dt1.compareTo(dt2)>0){
								    			commAccountLine1.setPayPostDate(dt2);
								    		}else{
								    			commAccountLine1.setPayPostDate(dt1);
								    		}
							    		}else{
								        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
								        	for (Object object : systemDefaultList) {
												SystemDefault systemDefault = (SystemDefault)object;
												commAccountLine1.setPayPostDate(systemDefault.getPostDate1());
											}
							    		}
								}catch(Exception e){
						        	List systemDefaultList = serviceOrderManager.findSysDefault(sessionCorpID);
						        	for (Object object : systemDefaultList) {
										SystemDefault systemDefault = (SystemDefault)object;
										commAccountLine1.setPayPostDate(systemDefault.getPostDate1());
									}
						        	e.printStackTrace();
								}			
								//change for posting date flexiblity End			        	
							//	}						        	
						        if(comm2ActualExp.compareTo(new BigDecimal(0))!=0 ){
						        	//commLine2Id = commAccountLine1.getId().toString();
						        	commAccountLine1 = accountLineManager.save(commAccountLine1);
						        	commLine2Id = commAccountLine1.getId().toString();
						        }
						   
							
							}else{
								
									AccountLine commAccountLine1=null;
									List accIdList1 = serviceOrderManager.findAccountLineByCharge("SACOM2",sessionCorpID,serviceOrder.getShipNumber(),compDiv);  
									if(accIdList1!=null && !(accIdList1.isEmpty())){
										String tempid= accIdList1.get(0).toString();
										commAccountLine1 =accountLineManager.get(Long.parseLong(tempid));
										commAccountLine1.setStatus(false);
										accountLineManager.save(commAccountLine1);
									}
									
							    }
								  // Method call for entry in commission table
								//accListBycompDiv = null;
								String tempAccId = compDivAccId.get(compDiv);
								if(!commLine1Id.equals("") || !commLine2Id.equals("") ){
									
								}
								// accListBycompDiv = serviceOrderManager.findAccountLineListByCompDiv(sid,sessionCorpID,compDiv);
								// accListBycompDiv = serviceOrderManager.findAccountLineListByCompDiv(sid,sessionCorpID,"");
								if(tempAccId!=null && tempAccId.length()>0 )
									perLineCommissionEntry(sid,commLine1Id,commLine2Id,invCount,tempAccId,aidList);	   
						}
					}
					
				}
								
			int i = serviceOrderManager.disableCommissionZeroEntry(sid,sessionCorpID);	
			System.out.println(i+" Lines deleted from commission table \n\n");
			int k =accountLineManager.updateSacom2ComDiv(sessionCorpID,sid,"");
					
		}
		
		
		public String commPercentage ;
		public String commPercentageLine2;
		private String commType;
		private String salesPersonParentCode;
		private String estimatorParentCode;
		public void perLineCommissionEntry(Long soid,String commLine1Id,String commLine2Id, int invCount, String  tempAccId,String aidList){
			Long StartTime = System.currentTimeMillis();
			if(invCount > 0){
				try{
				BigDecimal commissionableProfitPer = new BigDecimal(0);
				BigDecimal commissionableRatePer = new BigDecimal(0);
				BigDecimal totalActualCommExpence = new BigDecimal(0);
				
				serviceOrder = serviceOrderManager.get(soid);
				String [] accIdArray = tempAccId.split(",");
				try{
				for(int i = 0 ; i< accIdArray.length ; i++){
				//Iterator itr =accountIdList.iterator();
				//while(itr.hasNext()){
					BigDecimal tempComm = new BigDecimal(0);
					BigDecimal tempComm2 = new BigDecimal(0);
					commType="";
					commPercentage="";
					commPercentageLine2="";
					salesPersonParentCode = "";
					estimatorParentCode ="";
					accountLine = accountLineManager.get(Long.parseLong(accIdArray[i].toString()));
					
					List tempList =  serviceOrderManager.getCommissionableValue(accountLine.getContract(),accountLine.getChargeCode(),sessionCorpID,serviceOrder.getJob()); 
					if(tempList != null && !(tempList.isEmpty())){
						charges = (Charges) tempList.get(0);
					}else{
						charges = null ;
					}
					// Sales person commission calculation .
					String vendorCode1="";
					String vendorName1="";
					List listUser1 = serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getSalesMan());
					if(listUser1!=null && !listUser1.isEmpty()){
						vendorCode1 = listUser1.get(0).toString();
					}
					if(vendorCode1!=null && !(vendorCode1.trim().equals(""))){
						salesPersonParentCode = vendorCode1;
						
						if(charges != null && charges.getCommissionable() == true){
							try{
								if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){
									try{
										 //11050 - TP commission for OFF for jobs  Start
										if(serviceOrder.getJob()!=null && serviceOrder.getJob().equalsIgnoreCase("OFF") && offGridSelected!=null && !offGridSelected.isEmpty() && offGridSelected.get(accountLine.getChargeCode())!=null && !offGridSelected.get(accountLine.getChargeCode()).equalsIgnoreCase("")){
											String typeArr[]=offGridSelected.get(accountLine.getChargeCode()).split("~");
											String type=typeArr[0].toString();
											String per=typeArr[1].toString();
											if(type.equalsIgnoreCase("Revenue")){
												tempComm=accountLine.getActualRevenue().multiply(new BigDecimal(per));
												tempComm= tempComm.divide(new BigDecimal(100),4);										
											}else if(type.equalsIgnoreCase("Margin")){
												tempComm=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
												tempComm=tempComm.multiply(new BigDecimal(per));
												tempComm= tempComm.divide(new BigDecimal(100),4);										
											}		
											/*
											BigDecimal marginValue = new BigDecimal(0);
											BigDecimal marginPersentageValue = new BigDecimal(0);
											marginValue=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
											marginPersentageValue=marginValue.divide(accountLine.getActualRevenue(),4);
											marginPersentageValue=marginPersentageValue.multiply(new BigDecimal(100));
											String param="'OFFCOMMISSION'";
											List <RefMasterDTO> allParamValue = refMasterManager.getAllParameterValue(sessionCorpID,param);
											for (RefMasterDTO refObj : allParamValue) {
												if(marginPersentageValue.doubleValue()>=Double.parseDouble(refObj.getFlex1())&&marginPersentageValue.doubleValue()<=Double.parseDouble(refObj.getFlex2())){
													if(Double.parseDouble(refObj.getBucket2())!=0){
														if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Revenue")){
															tempComm=accountLine.getActualRevenue().multiply(new BigDecimal(refObj.getBucket2()));
															tempComm= tempComm.divide(new BigDecimal(100),4);										
														}else if(refObj.getDescription()!=null && refObj.getDescription().equalsIgnoreCase("Margin")){
															tempComm=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense().setScale(4,BigDecimal.ROUND_HALF_UP):accountLine.getRevisionExpense().setScale(4,BigDecimal.ROUND_HALF_UP)));
															tempComm=tempComm.multiply(new BigDecimal(refObj.getBucket2()));
															tempComm= tempComm.divide(new BigDecimal(100),4);										
														}
													}else{
														tempComm = new BigDecimal(0);
													}
												}							
											}
										*/}
										//11050 - TP commission for OFF for jobs  End
										}catch(Exception e){e.printStackTrace();}
										commType="TP";
								}else{
									if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
										if(!charges.getBucket().toString().trim().equalsIgnoreCase("TP") ){
											tempComm = new BigDecimal(0);
											tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
											tempComm = (tempComm.multiply(new BigDecimal(charges.getCommission().toString()))).divide(new BigDecimal(100),4);
											commPercentage = charges.getCommission().toString();
											commType="FL";
										}else{
											tempComm = accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
											tempComm = tempComm.multiply(new BigDecimal(charges.getCommission()));
											tempComm= tempComm.divide(new BigDecimal(100),4);
											commPercentage = charges.getCommission().toString();
											commType="TP";
									}
									}else{
										if(commLine1Id !=null && !commLine1Id.equals("") ){
										AccountLine acComLine1 = accountLineManager.get(new Long(commLine1Id));
										if(accountLine.getActualRevenue().compareTo(new BigDecimal(0))!= 0 ){
											commissionableProfitPer=accountLine.getActualRevenue().subtract((accountLine.getActualExpense().doubleValue()!=0?accountLine.getActualExpense():accountLine.getRevisionExpense()));
											commissionableProfitPer = commissionableProfitPer.divide(accountLine.getActualRevenue(),4);
											commissionableProfitPer = commissionableProfitPer.multiply(new BigDecimal(100));
											List <RateGrid> rateList =  serviceOrderManager.getRateGirdForComm(acComLine1.getChargeCode(),acComLine1.getContract(),sessionCorpID);
											if((rateList!=null)&&(!rateList.isEmpty()))	{
									    		for (RateGrid rateGrid : rateList) {
									    			if(commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))== 0 || commissionableProfitPer.compareTo(new BigDecimal(rateGrid.getQuantity1()))==-1 ){
									    				commPercentage = rateGrid.getRate1().toString();
									    				break;
									    			}
									    		}
									    	}
											if(commPercentage!=null && (new BigDecimal(commPercentage)).compareTo(new BigDecimal(0))!=0){
												tempComm = (accountLine.getActualRevenue().multiply(new BigDecimal(commPercentage))).divide(new BigDecimal(100),4);
											}
											commType="VA";
										}
									}
									}									
								}
								//9494 - OFF - New Commission Structure for Third party  End
								}catch(Exception e){e.printStackTrace();}
							
						}
					}
					
					// Estimator commission calculation  .
					if(charges != null && charges.getCommissionable() == true){
						if(charges.getCommission()!=null && !(charges.getCommission().equalsIgnoreCase("")) ){
						
						}else{
								String vendorCode="";
								String vendorName="";
								List listUser= serviceOrderManager.getParentAgentCodeFormUser(serviceOrder.getEstimator()); 
								if(!listUser.isEmpty()){
										vendorCode = listUser.get(0).toString();
								}
								if(vendorCode!=null && !(vendorCode.trim().equals(""))){
									estimatorParentCode = vendorCode;
									List listPartnerName = serviceOrderManager.getPartnerDetails(vendorCode,sessionCorpID);
									if(!listPartnerName.isEmpty() && listPartnerName.get(0)!=null){
										vendorName=listPartnerName.get(0).toString();
									}
									if(!vendorName.equalsIgnoreCase("")){
										String [] vendorDetail = vendorName.split("~");
										try{
											commPercentageLine2 = vendorDetail[1].toString();
											tempComm2 = (accountLine.getActualRevenue().multiply(new BigDecimal(vendorDetail[1].toString()))).divide(new BigDecimal(100),4);
										}catch(Exception e){
											System.out.println("Commission is not present in estimator parent code");
										}
									}
								}
						}
					}
					
					if(tempComm2.compareTo(new BigDecimal("0.00"))!=0 && (commType==null || commType.equals("")) )
						commType="Comm2";
					
					// Entry in commission table
					if(!accountLine.getChargeCode().contains("SACOM") && charges != null && charges.getCommissionable() == true)
						commissionEnrty(accountLine.getId(),tempComm,tempComm2,aidList);	
				}
				}catch(ArrayIndexOutOfBoundsException arr){
					arr.printStackTrace();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			}
			
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken to Entry in Commission table: "+timeTaken+"\n\n");
			
		}
		
		public CommissionManager commissionManager;
		private Commission commission;
		
		public void commissionEnrty(Long accId,BigDecimal line1CommAmount , BigDecimal line2CommAmount,String aidList){
			try{
				if(commType!=null && !commType.equalsIgnoreCase("") && accId!=null){
					AccountLine tempAccObj = accountLineManager.get(accId);
					if(!tempAccObj.getCompanyDivision().equalsIgnoreCase("CAJ")){
					List l1  = serviceOrderManager.getCommissionById(accId, sessionCorpID,commLine1Id,commLine2Id);
					String commissionId="";
					if(l1!=null && !l1.isEmpty() && l1.get(0)!=null && !l1.get(0).toString().equals("")){
					//if(count>0){
						commission = commissionManager.get(new Long(l1.get(0).toString()));
						commissionId= commission.getId().toString();
				}else{
					commission= new Commission();
					commission.setShipNumber(serviceOrder.getShipNumber());
					commission.setCorpId(sessionCorpID);
					commission.setCreatedBy(getRequest().getRemoteUser());
					commission.setCreatedOn(new Date());
				}
					commission.setAid(accId);
					commission.setSalesPerson(salesPersonParentCode);
					commission.setConsultant(estimatorParentCode);
					
					if(!commType.equalsIgnoreCase("Comm2"))
						commission.setCommissionType(commType);
					
				if(commLine1Id== null)
					commLine1Id="";
				
				if(commLine2Id== null)
					commLine2Id="";
				
				logger.warn("\n\n Sales person commission amount  value............  \n\n");
				
				BigDecimal tempComm1Amount = new BigDecimal(0.00);
				try{
				if(!commLine1Id.equals("")){
				
					List commissionAmountList = serviceOrderManager.getCommissionAmountTotalValue(sessionCorpID,"salesPersonAmount",accId,commissionId); 
					
					if(commissionAmountList!=null && !commissionAmountList.isEmpty() && commissionAmountList.get(0)!=null && !commissionAmountList.get(0).toString().equals("") )
						tempComm1Amount = new BigDecimal(commissionAmountList.get(0).toString());
					
					if(tempComm1Amount.compareTo(new BigDecimal(0.00))!=0)
						line1CommAmount = line1CommAmount.subtract(tempComm1Amount) ;
					
	 				if(!aidList.equalsIgnoreCase("")){
	 					commission.setSalesPersonAmount(commission.getSalesPersonAmount().add(line1CommAmount));
	 				}else{
	 					commission.setSalesPersonAmount(line1CommAmount);
	 				}

					commission.setSalesPersonPercentage(new BigDecimal(commPercentage));
					logger.warn("\n\n sales person commission amount  value....  \n\n");
					
					/*
				if(commission.getSalesPersonAmount()!= null && !commission.getSalesPersonAmount().toString().equals("") && commission.getSalesPersonAmount().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine1Id().toString().equals(commLine1Id)){
					if(commission.getSalesPersonSupplimentAmount1()!= null && !commission.getSalesPersonSupplimentAmount1().toString().equals("") && commission.getSalesPersonSupplimentAmount1().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine1Id().toString().equals(commLine1Id)){
						if(commission.getSalesPersonSupplimentAmount2()!= null && !commission.getSalesPersonSupplimentAmount2().toString().equals("") && commission.getSalesPersonSupplimentAmount2().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null  &&  !commission.getCommissionLine1Id().toString().equals(commLine1Id)){
							if(commission.getSalesPersonSupplimentAmount3()!= null && !commission.getSalesPersonSupplimentAmount3().toString().equals("") && commission.getSalesPersonSupplimentAmount3().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine1Id().toString().equals(commLine1Id)){
								logger.warn("\n\n Lines already calculate 3 time suppliment commission.\n Reseting 3rd suppliment value again. \n\n");
								commission.setSalesPersonSupplimentAmount3(line1CommAmount);
								commission.setSalesPersonSupplimentPercentage3(new BigDecimal(commPercentage));
								logger.warn("\n\n 3rd suppliment value reset ........ \n\n");
							}else{
								commission.setSalesPersonSupplimentAmount3(line1CommAmount);
								commission.setSalesPersonSupplimentPercentage3(new BigDecimal(commPercentage));
								logger.warn("\n\n 3rd suppliment value ..... \n\n");
							}
						}else{
							commission.setSalesPersonSupplimentAmount2(line1CommAmount);
							commission.setSalesPersonSupplimentPercentage2(new BigDecimal(commPercentage));
							logger.warn("\n\n 2nd suppliment value.....  \n\n");
						}
					}else{
						commission.setSalesPersonSupplimentAmount1(line1CommAmount);
						commission.setSalesPersonSupplimentPercentage1(new BigDecimal(commPercentage));
						logger.warn("\n\n 1st suppliment value....  \n\n");
					}
				}else{
					commission.setSalesPersonAmount(line1CommAmount);
					commission.setSalesPersonPercentage(new BigDecimal(commPercentage));
					logger.warn("\n\n commission amount  value....  \n\n");
				}
				*/}
				}catch(Exception e){
					e.printStackTrace();
				}
				logger.warn("\n\n Consultant commission amount  value............  \n\n");
				
				BigDecimal tempComm2Amount = new BigDecimal(0.00);
				try{
				if(!commLine2Id.equals("")){
					
					List commissionAmoutList = serviceOrderManager.getCommissionAmountTotalValue(sessionCorpID,"consultantAmount",accId,commissionId); 
					
					if(commissionAmoutList!=null && !commissionAmoutList.isEmpty() && commissionAmoutList.get(0)!=null && !commissionAmoutList.get(0).toString().equals("") )
						tempComm2Amount = new BigDecimal(commissionAmoutList.get(0).toString());
					
					if(tempComm2Amount.compareTo(new BigDecimal(0.00))!=0)
						line2CommAmount = line2CommAmount.subtract(tempComm2Amount) ;
				
	 				if(!aidList.equalsIgnoreCase("")){
	 					commission.setConsultantAmount(commission.getConsultantAmount().add(line2CommAmount));
	 				}else{
	 					commission.setConsultantAmount(line2CommAmount);
	 				}

					commission.setConsultantPercentage(new BigDecimal(commPercentageLine2));
					logger.warn("\n\n consultant  commission amount  value....  \n\n");
						/*
				if(commission.getConsultantAmount()!= null && !commission.getConsultantAmount().toString().equals("") && commission.getConsultantAmount().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null  && !commission.getCommissionLine2Id().toString().equals(commLine2Id)){
					if(commission.getConsultantSupplimentAmount1()!= null && !commission.getConsultantSupplimentAmount1().toString().equals("") && commission.getConsultantSupplimentAmount1().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine2Id().toString().equals(commLine2Id)){
						if(commission.getConsultantSupplimentAmount2()!= null && !commission.getConsultantSupplimentAmount2().toString().equals("") && commission.getConsultantSupplimentAmount2().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine2Id().toString().equals(commLine2Id)){
							if(commission.getConsultantSupplimentAmount3()!= null && !commission.getConsultantSupplimentAmount3().toString().equals("") && commission.getConsultantSupplimentAmount3().compareTo(new BigDecimal(0.00))!= 0 && commission.getCommissionLine1Id()!=null && !commission.getCommissionLine2Id().toString().equals(commLine2Id)){
								logger.warn("\n\n Lines already calculate 3 time suppliment commission.\n Reseting 3rd suppliment value again. \n\n");
								commission.setConsultantSupplimentAmount3(line2CommAmount);
								commission.setConsultantSupplimentPercentage3(new BigDecimal(commPercentageLine2));
								logger.warn("\n\n 3rd suppliment value reset ........ \n\n");
							}else{
								commission.setConsultantSupplimentAmount3(line2CommAmount);
								commission.setConsultantSupplimentPercentage3(new BigDecimal(commPercentageLine2));
								logger.warn("\n\n 3rd suppliment value ..... \n\n");
							}
						}else{
							commission.setConsultantSupplimentAmount2(line2CommAmount);
							commission.setConsultantSupplimentPercentage2(new BigDecimal(commPercentageLine2));
							logger.warn("\n\n 2nd suppliment value.....  \n\n");
						}
					}else{
						commission.setConsultantSupplimentAmount1(line2CommAmount);
						commission.setConsultantSupplimentPercentage1(new BigDecimal(commPercentageLine2));
						logger.warn("\n\n 1st suppliment value....  \n\n");
					}
				}else{
					commission.setConsultantAmount(line2CommAmount);
					commission.setConsultantPercentage(new BigDecimal(commPercentageLine2));
					logger.warn("\n\n commission amount  value....  \n\n");
				}
				*/}
				}catch(Exception e){
					e.printStackTrace();
				}
				commission.setUpdatedBy(getRequest().getRemoteUser());
				commission.setUpdatedOn(new Date());
				
				if(!commLine1Id.equals("") && line1CommAmount.compareTo(new BigDecimal(0.00))!=0 )
					commission.setCommissionLine1Id(commLine1Id);
				if(!commLine2Id.equals("") && line2CommAmount.compareTo(new BigDecimal(0.00))!=0)
					commission.setCommissionLine2Id(commLine2Id);
				
				commission.setSettledDate(new Date());
				commission.setCompanyDivision(tempAccObj.getCompanyDivision());
				commission.setActualExpense((tempAccObj.getActualExpense().doubleValue()!=0?tempAccObj.getActualExpense():tempAccObj.getRevisionExpense()));
				commission.setActualRevenue(tempAccObj.getActualRevenue());
				commission.setChargeCode(tempAccObj.getChargeCode());
				
				if(!commLine1Id.equals("") && line1CommAmount.compareTo(new BigDecimal(0.00))!=0 ){
					commissionManager.save(commission);
				}else{
					if(!commLine2Id.equals("") && line2CommAmount.compareTo(new BigDecimal(0.00))!=0){
						commissionManager.save(commission);
					}
			 }
			}}
			}catch(Exception e){
				e.printStackTrace();
			}}
	
	public String saveSubcontract() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String showLine() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		showVanLines = vanLineManager.showLine(regNum);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String splitChargeOnsave() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			try {
			//getComboList(sessionCorpID); 
			subcontractorChargesExt = vanLineManager.splitCharge(id); 
		} catch (Exception e) {
			e.printStackTrace();
		}
		totalAmount();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String splitCharge() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			try {
			//getComboList(sessionCorpID); 
			subcontractorChargesExt = vanLineManager.splitCharge(id);

		} catch (Exception e) {
		e.printStackTrace();

		}
		totalAmount();
		editSplit();
		if(glCodeFlag!=null && (glCodeFlag.equalsIgnoreCase("YES"))){
			 glCode = refMasterManager.findByParameterOrderByFlex1(sessionCorpID, "GLCODES");
			 }
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String chargeAllocation() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			try {
			//getComboList(sessionCorpID);
			subcontractorChargesExt = vanLineManager.splitCharge(id);
		} catch (Exception e) {
			e.printStackTrace();

		}
		totalAmount();
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String checkchargeAllocation() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			chargeAllocationList = vanLineManager.chargeAllocation(id);
		if (chargeAllocationList.isEmpty()) {
			chargeAllocation = "";
		} else if (!chargeAllocationList.isEmpty()) {
			chargeAllocation = chargeAllocationList.get(0).toString();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;

	}

	@SkipValidation
	public String totalAmount() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List totalamontList = vanLineManager.totalAmountSum(id);
		totalAmount = Double.parseDouble(totalamontList.get(0).toString());
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String searchVanLine() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean agent = (vanLine.getAgent() == null);
		boolean weekEnding = (vanLine.getWeekEnding() == null);
		boolean glType = (vanLine.getGlType() == null);
		boolean reconcileStatus = (vanLine.getReconcileStatus() == null);
		String statusDateNew = "";
		if (!agent || !weekEnding || !glType || !reconcileStatus) {
			vanLineListExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
			List<SearchCriterion> searchCriteria = new ArrayList();
			searchCriteria.add(new SearchCriterion("agent", vanLine.getAgent(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			searchCriteria.add(new SearchCriterion("glType", vanLine.getGlType(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			searchCriteria.add(new SearchCriterion("reconcileStatus", vanLine.getReconcileStatus(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));

			try {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				StringBuilder nowYYYYMMDD = new StringBuilder(dateformatYYYYMMDD.format(vanLine.getWeekEnding()));
				statusDateNew = nowYYYYMMDD.toString();

			} catch (Exception e) {

				e.printStackTrace();
			}
			searchCriteria.add(new SearchCriterion("weekEnding", statusDateNew, SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
			pagingLookupManager.getAllRecordsPage(VanLine.class, vanLineListExt, searchCriteria);
		}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String testFormula() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		String expression = expressionFormula;
		Map values = new HashMap();
		values.put("partner.driverAgency", "000061");
		values.put("partner.longPercentage", 60.00);
		values.put("accountline.branchCode", "000061");
		values.put("accountline.chargeCode", "071");
		values.put("billing.discount", 50.00);
		values.put("billing.otherDiscount", 10.00);
		values.put("miscellaneous.actualNetWeight", 1000);
		values.put("accountline.actualRevenue", new BigDecimal(5000));
		values.put("accountline.description", "ADJ:");

		values.put("accountline.distributionAmount", new BigDecimal(5000.00));
		values.put("vanLineCommissionType.pc", 10.00);
		values.put("serviceorder.bookingAgentCode", "000061");
		//values.put("vanlinegltype.glType","058");

		totalRevenue = expressionManager.executeTestExpression(expression, values);
		if (totalRevenue == null) {
			totalRevenue = "Formula is Correct";
		}
	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private List partners;
	@SkipValidation
	public String vanlineBillToCode(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List  SOidList = vanLineManager.getShipNumber(vanRegNum);
		if(SOidList!=null && (!SOidList.isEmpty())){
			Iterator SOidIterator=SOidList.iterator();
					while(SOidIterator.hasNext()){
						 String SoIdData=SOidIterator.next().toString();
						  Long  SoId = Long.parseLong(SoIdData)  ; 
						  serviceOrder = serviceOrderManager.get(SoId);
				 partners = new ArrayList(partnerManager.findByBillToCode(serviceOrder.getShipNumber()));
			}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
    	return SUCCESS; 
	}
	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public List getVanLineList() {
		return vanLineList;
	}

	public void setVanLineList(List vanLineList) {
		this.vanLineList = vanLineList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setVanLineManager(VanLineManager vanLineManager) {
		this.vanLineManager = vanLineManager;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public VanLine getVanLine() {
		return vanLine;
	}

	public void setVanLine(VanLine vanLine) {
		this.vanLine = vanLine;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public Map<String, String> getGlCode() {
		return glCode;
	}

	public void setGlCode(Map<String, String> glCode) {
		this.glCode = glCode;
	}

	public void setVanLineGLTypeManager(VanLineGLTypeManager vanLineGLTypeManager) {
		this.vanLineGLTypeManager = vanLineGLTypeManager;
	}

	public String getExpressionFormula() {
		return expressionFormula;
	}

	public void setExpressionFormula(String expressionFormula) {
		this.expressionFormula = expressionFormula;
	}

	public String getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(String totalRevenue) {
		this.totalRevenue = totalRevenue;
	}

	public void setExpressionManager(ExpressionManager expressionManager) {
		this.expressionManager = expressionManager;
	}

	public List<VanLine> getShowVanLines() {
		return showVanLines;
	}

	public void setShowVanLines(List<VanLine> showVanLines) {
		this.showVanLines = showVanLines;
	}

	public String getRegNum() {
		return regNum;
	}

	public void setRegNum(String regNum) {
		this.regNum = regNum;
	}

	public Map<String, String> getGlType() {
		return glType;
	}

	public void setGlType(Map<String, String> glType) {
		this.glType = glType;
	}

	public PaginateListFactory getPaginateListFactory() {
		return paginateListFactory;
	}

	public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
		this.paginateListFactory = paginateListFactory;
	}

	public ExtendedPaginatedList getVanLineListExt() {
		return vanLineListExt;
	}

	public void setVanLineListExt(ExtendedPaginatedList vanLineListExt) {
		this.vanLineListExt = vanLineListExt;
	}

	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
		this.pagingLookupManager = pagingLookupManager;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public Date getWeekEnding() {
		return weekEnding;
	}

	public void setWeekEnding(Date weekEnding) {
		this.weekEnding = weekEnding;
	}

	public List<VanLine> getSplitCharge() {
		return splitCharge;
	}

	public void setSplitCharge(List<VanLine> splitCharge) {
		this.splitCharge = splitCharge;
	}

	public List getSubcontractorChargesExt() {
		return subcontractorChargesExt;
	}

	public void setSubcontractorChargesExt(List subcontractorChargesExt) {
		this.subcontractorChargesExt = subcontractorChargesExt;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public double getDueFormAmount() {
		return dueFormAmount;
	}

	public void setDueFormAmount(double dueFormAmount) {
		this.dueFormAmount = dueFormAmount;
	}

	public List getChargeAllocationList() {
		return chargeAllocationList;
	}

	public void setChargeAllocationList(List chargeAllocationList) {
		this.chargeAllocationList = chargeAllocationList;
	}

	public String getChargeAllocation() {
		return chargeAllocation;
	}

	public void setChargeAllocation(String chargeAllocation) {
		this.chargeAllocation = chargeAllocation;
	}

	public List getVanLineCodeList() {
		return vanLineCodeList;
	}

	public void setVanLineCodeList(List vanLineCodeList) {
		this.vanLineCodeList = vanLineCodeList;
	}

	public List getWeekendingDateList() {
		return weekendingDateList;
	}

	public void setWeekendingDateList(List weekendingDateList) {
		this.weekendingDateList = weekendingDateList;
	}

	public String getVanRegNum() {
		return vanRegNum;
	}

	public void setVanRegNum(String vanRegNum) {
		this.vanRegNum = vanRegNum;
	}

	public List getVanLineRegNumList() {
		return vanLineRegNumList;
	}

	public void setVanLineRegNumList(List vanLineRegNumList) {
		this.vanLineRegNumList = vanLineRegNumList;
	}

	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}

	public ServiceOrder getServiceOrder() {
		return serviceOrder;
	}

	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = serviceOrder;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public AccountLine getAccountLine() {
		return accountLine;
	}

	public void setAccountLine(AccountLine accountLine) {
		this.accountLine = accountLine;
	}

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setChargesManager(ChargesManager chargesManager) {
		this.chargesManager = chargesManager;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public List<SystemDefault> getSysDefaultDetail() {
		return sysDefaultDetail;
	}

	public void setSysDefaultDetail(List<SystemDefault> sysDefaultDetail) {
		this.sysDefaultDetail = sysDefaultDetail;
	}

	public Long getSid() {
		return sid;
	}

	public void setSid(Long sid) {
		this.sid = sid;
	} 

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public void setPartnerManager(PartnerManager partnerManager) {
		this.partnerManager = partnerManager;
	}

	
	public String getCommLine1Id() {
		return commLine1Id;
	}

	public void setCommLine1Id(String commLine1Id) {
		this.commLine1Id = commLine1Id;
	}

	public String getCommLine2Id() {
		return commLine2Id;
	}

	public void setCommLine2Id(String commLine2Id) {
		this.commLine2Id = commLine2Id;
	}

	public String getCommPercentage() {
		return commPercentage;
	}

	public void setCommPercentage(String commPercentage) {
		this.commPercentage = commPercentage;
	}

	public String getCommPercentageLine2() {
		return commPercentageLine2;
	}

	public void setCommPercentageLine2(String commPercentageLine2) {
		this.commPercentageLine2 = commPercentageLine2;
	}

	public String getCommType() {
		return commType;
	}

	public void setCommType(String commType) {
		this.commType = commType;
	}

	public String getSalesPersonParentCode() {
		return salesPersonParentCode;
	}

	public void setSalesPersonParentCode(String salesPersonParentCode) {
		this.salesPersonParentCode = salesPersonParentCode;
	}

	public String getEstimatorParentCode() {
		return estimatorParentCode;
	}

	public void setEstimatorParentCode(String estimatorParentCode) {
		this.estimatorParentCode = estimatorParentCode;
	}

	public Commission getCommission() {
		return commission;
	}

	public void setCommission(Commission commission) {
		this.commission = commission;
	}

	public void setCommissionManager(CommissionManager commissionManager) {
		this.commissionManager = commissionManager;
	}

	public List getAccListBycompDiv() {
		return accListBycompDiv;
	}

	public void setAccListBycompDiv(List accListBycompDiv) {
		this.accListBycompDiv = accListBycompDiv;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public List getVanLineExtractList() {
		return vanLineExtractList;
	}

	public void setVanLineExtractList(List vanLineExtractList) {
		this.vanLineExtractList = vanLineExtractList;
	}

	public List getSoList() {
		return soList;
	}

	public Map<String, String> getCategoryList() {
		return categoryList;
	}



	public void setSoList(List soList) {
		this.soList = soList;
	}

	public String getShipNum() {
		return shipNum;
	}

	public void setShipNum(String shipNum) {
		this.shipNum = shipNum;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getAccID() {
		return accID;
	}

	public void setAccID(String accID) {
		this.accID = accID;
	}
	
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List getProcessedXMLList() {
		return processedXMLList;
	}

	public void setProcessedXMLList(List processedXMLList) {
		this.processedXMLList = processedXMLList;
	}

	public List getDownloadXMLList() {
		return downloadXMLList;
	}

	public void setDownloadXMLList(List downloadXMLList) {
		this.downloadXMLList = downloadXMLList;
	}

	public void setCategoryList(Map<String, String> categoryList) {
		this.categoryList = categoryList;
	}

	public String getWeekEnd() {
		return weekEnd;
	}

	public void setWeekEnd(String weekEnd) {
		this.weekEnd = weekEnd;
	}

	public String getStatementCategory() {
		return statementCategory;
	}

	public void setStatementCategory(String statementCategory) {
		this.statementCategory = statementCategory;
	}

	public String getChkWith() {
		return chkWith;
	}

	public void setChkWith(String chkWith) {
		this.chkWith = chkWith;
	}

	public String getChkcategorypage() {
		return chkcategorypage;
	}

	public void setChkcategorypage(String chkcategorypage) {
		this.chkcategorypage = chkcategorypage;
	}

	public String getFlagvanLineData() {
		return flagvanLineData;
	}

	public void setFlagvanLineData(String flagvanLineData) {
		this.flagvanLineData = flagvanLineData;
	}

	public String getRemoveFileVal() {
		return removeFileVal;
	}

	public void setRemoveFileVal(String removeFileVal) {
		this.removeFileVal = removeFileVal;
	}

	public String getRemoveAccVal() {
		return removeAccVal;
	}

	public void setRemoveAccVal(String removeAccVal) {
		this.removeAccVal = removeAccVal;
	}
	public String getDivisionFlag() {
		return divisionFlag;
	}
	public void setDivisionFlag(String divisionFlag) {
		this.divisionFlag = divisionFlag;
	}


	public ExtendedPaginatedList getSOListExt() {
		return SOListExt;
	}


	public void setSOListExt(ExtendedPaginatedList sOListExt) {
		SOListExt = sOListExt;
	}


	public AccountLine getAccLineNew() {
		return accLineNew;
	}


	public void setAccLineNew(AccountLine accLineNew) {
		this.accLineNew = accLineNew;
	}


	public String getGlCodeFlag() {
		return glCodeFlag;
	}


	public void setGlCodeFlag(String glCodeFlag) {
		this.glCodeFlag = glCodeFlag;
	}


	public Map<String, String> getChargeCodeList() {
		return chargeCodeList;
	}


	public void setChargeCodeList(Map<String, String> chargeCodeList) {
		this.chargeCodeList = chargeCodeList;
	}


	public List getCompanyDivis() {
		return companyDivis;
	}


	public void setCompanyDivis(List companyDivis) {
		this.companyDivis = companyDivis;
	}


	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}


	public SubcontractorCharges getSubcontractorCharges() {
		return subcontractorCharges;
	}


	public void setSubcontractorCharges(SubcontractorCharges subcontractorCharges) {
		this.subcontractorCharges = subcontractorCharges;
	}


	public void setSubcontractorChargesManager(
			SubcontractorChargesManager subcontractorChargesManager) {
		this.subcontractorChargesManager = subcontractorChargesManager;
	}


	public boolean isAutomaticReconcile() {
		return automaticReconcile;
	}


	public void setAutomaticReconcile(boolean automaticReconcile) {
		this.automaticReconcile = automaticReconcile;
	}


	public void setExchangeRateManager(ExchangeRateManager exchangeRateManager) {
		this.exchangeRateManager = exchangeRateManager;
	}


	public boolean isVanlineSaveFlag() {
		return vanlineSaveFlag;
	}


	public void setVanlineSaveFlag(boolean vanlineSaveFlag) {
		this.vanlineSaveFlag = vanlineSaveFlag;
	}


	public boolean isSplitChargeShowFlag() {
		return SplitChargeShowFlag;
	}


	public void setSplitChargeShowFlag(boolean splitChargeShowFlag) {
		SplitChargeShowFlag = splitChargeShowFlag;
	}


	public Map<String, String> getDivision() {
		return division;
	}


	public void setDivision(Map<String, String> division) {
		this.division = division;
	}


	public String getPayAccDateTo() {
		return payAccDateTo;
	}


	public void setPayAccDateTo(String payAccDateTo) {
		this.payAccDateTo = payAccDateTo;
	}


	public Date getPayAccDateToFormat() {
		return payAccDateToFormat;
	}


	public void setPayAccDateToFormat(Date payAccDateToFormat) {
		this.payAccDateToFormat = payAccDateToFormat;
	}


	public String getOwnbillingBilltoCodes() {
		return ownbillingBilltoCodes;
	}


	public void setOwnbillingBilltoCodes(String ownbillingBilltoCodes) {
		this.ownbillingBilltoCodes = ownbillingBilltoCodes;
	}


	public String getSaveValidateForm() {
		return saveValidateForm;
	}


	public void setSaveValidateForm(String saveValidateForm) {
		this.saveValidateForm = saveValidateForm;
	}


	public String getVanLineIdCheck() {
		return vanLineIdCheck;
	}


	public void setVanLineIdCheck(String vanLineIdCheck) {
		this.vanLineIdCheck = vanLineIdCheck;
	}


	public String getVanLineStatementCategoryCheck() {
		return vanLineStatementCategoryCheck;
	}


	public void setVanLineStatementCategoryCheck(
			String vanLineStatementCategoryCheck) {
		this.vanLineStatementCategoryCheck = vanLineStatementCategoryCheck;
	}


	public VanLine getVanLinelistData() {
		return vanLinelistData;
	}


	public void setVanLinelistData(VanLine vanLinelistData) {
		this.vanLinelistData = vanLinelistData;
	}


	public boolean isAccountLineAccountPortalFlag() {
		return accountLineAccountPortalFlag;
	}


	public void setAccountLineAccountPortalFlag(boolean accountLineAccountPortalFlag) {
		this.accountLineAccountPortalFlag = accountLineAccountPortalFlag;
	}
	public List getPartners() {
		return partners;
	}


	public void setPartners(List partners) {
		this.partners = partners;
	}
	public boolean isOwnbillingVanline() {
		return ownbillingVanline;
	}


	public void setOwnbillingVanline(boolean ownbillingVanline) {
		this.ownbillingVanline = ownbillingVanline;
	}


	public Map getVenderCodeDetails() {
		return venderCodeDetails;
	}


	public void setVenderCodeDetails(Map venderCodeDetails) {
		this.venderCodeDetails = venderCodeDetails;
	}


	public String getAccVenderDetails() {
		return accVenderDetails;
	}


	public void setAccVenderDetails(String accVenderDetails) {
		this.accVenderDetails = accVenderDetails;
	}


	public String getFileNoVenderDetails() {
		return fileNoVenderDetails;
	}


	public void setFileNoVenderDetails(String fileNoVenderDetails) {
		this.fileNoVenderDetails = fileNoVenderDetails;
	}


	public String getVenderDetailsList() {
		return venderDetailsList;
	}


	public void setVenderDetailsList(String venderDetailsList) {
		this.venderDetailsList = venderDetailsList;
	}


	public SystemDefault getSystemDefault() {
		return systemDefault;
	}


	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}


	public TrackingStatus getTrackingStatus() {
		return trackingStatus;
	}


	public void setTrackingStatus(TrackingStatus trackingStatus) {
		this.trackingStatus = trackingStatus;
	}


	public Miscellaneous getMiscellaneous() {
		return miscellaneous;
	}


	public void setMiscellaneous(Miscellaneous miscellaneous) {
		this.miscellaneous = miscellaneous;
	}


	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}


	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}
}
