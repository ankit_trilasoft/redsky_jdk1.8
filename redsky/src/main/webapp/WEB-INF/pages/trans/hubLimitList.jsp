<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>Operation Hub Limit</title>   
    <meta name="heading" content="Operation Hub Limit"/>      
<style>
.tab{border:1px solid #74B3DC;}
span.pagelinks{margin-top:-14px;}
</style> 
</head>
<s:form cssClass="form_magn" id="hubLimitListForm" action="" method="post" validate="true">	
	<div id="newmnav" class="nav_tabs"><!-- sandeep -->
				
				  <ul>
				  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Hub&nbsp;/&nbsp;WH List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				  	<%-- 
				  	<li><a href="operationsMgmt.html"><span>Operational Management</span></a></li>
				    <li><a href="dailyControlList.html"><span>Daily Limit List</span></a></li>
				    --%>
				    <c:if test="${hubWarehouseLimit=='Crw'}">
				    	<li><a href="crewCapacityList.html"><span>Crew Capacity</span></a></li>
				    </c:if>
				  </ul>				  
		</div>
		<div style="width: 700px" >
		<div class="spn">&nbsp;</div>
		<div style="padding-bottom: 1px"></div>
		</div>
<div id="Layer1" style="width:100%;"><!-- sandeep -->
<c:set var="buttons">   
    <input type="button" class="cssbutton" style="width:60px; height:25px" onclick="location.href='<c:url value="/editHubLimit.html"/>'" value="<fmt:message key="button.add"/>"/>   
</c:set>  

<table  width="100%" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td >				
				<s:set name="hubLimits" value="hubLimits" scope="request"/>			  
				
				<display:table name="hubLimits" class="table" requestURI="" id="hubLimits" pagesize="50" export="true"> 
				<c:if test="${operationChk=='H'}">
				<display:column   sortable="true" title="HubID">
					<a href="editHubLimit.html?id=${hubLimits.id}" >${hubLimits.hubID}</a>
					</display:column>
				</c:if>
				<c:if test="${operationChk=='W'}">
					<display:column   sortable="true" title="HubID">
					<a href="editHubLimit.html?id=${hubLimits.id}" >${hubLimits.flex1}</a>
					</display:column>
					
					<display:column  sortable="true" title="Warehouse">
					<a href="editHubLimit.html?id=${hubLimits.id}" >
					<c:forEach var="entry" items="${distinctHubDescription}">
					<c:if test="${hubLimits.code==entry.key}">
					<c:out value="${entry.value}" />
					</c:if>
					</c:forEach>
					</a>
					</display:column>
					</c:if>
					<c:if test="${hubWarehouseLimit=='Tkt'}">
						<display:column property="dailyOpHubLimit" headerClass="containeralign" style="text-align: right" sortable="true" title="Def&nbsp;Limit"/>
						<display:column property="dailyOpHubLimitPC" headerClass="containeralign" style="text-align: right" sortable="true" title="Threshold"/>
						<display:column property="minServiceDayHub" headerClass="containeralign" style="text-align: right" sortable="true" title="Min.&nbsp;Days"/>
						 <display:column headerClass="containeralign" title="Max&nbsp;Est&nbsp;Wt" style="width:100px; text-align: right"><div align="right">
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${hubLimits.dailyMaxEstWt}" /></div>
						</display:column>
						<display:column headerClass="containeralign" title="Max&nbsp;Est&nbsp;Vol" style="width:100px; text-align: right"><div align="right">
						<fmt:formatNumber type="number" maxFractionDigits="2" minFractionDigits="2" groupingUsed="true" value="${hubLimits.dailyMaxEstVol}" /></div>
						</display:column>
					</c:if>
					<c:if test="${hubWarehouseLimit=='Crw'}">
						<display:column property="dailyCrewLimit" headerClass="containeralign" style="text-align: right" sortable="true" title="Crew"/>
						<display:column property="dailyCrewTholdLimit" headerClass="containeralign" style="text-align: right" sortable="true" title="Threshold"/>
						<display:column property="minServiceDayHub" headerClass="containeralign" style="text-align: right" sortable="true" title="Min.&nbsp;Days"/>
					</c:if>
					
				    <display:setProperty name="paging.banner.item_name" value="Operations Hub Limits"/>   
				    <display:setProperty name="paging.banner.items_name" value="Operations Hub Limits"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Operations Hub Limits List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Operations Hub Limits List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Operations Hub Limits List.pdf"/>   
				</display:table>  
			</td>
		</tr>
	</tbody>
</table> 
<table> 
	<c:out value="${buttons}" escapeXml="false" /> 
	<tr>
		<td style="height:70px; !height:100px"></td>
	</tr></table>
</div>
</s:form>
