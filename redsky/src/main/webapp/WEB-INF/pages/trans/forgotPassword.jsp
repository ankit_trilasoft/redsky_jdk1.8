<%@ include file="/common/taglibs.jsp"%>
 
<head>

    <title><fmt:message key="userIDHelp.title"/></title>
    <meta name="heading" content="<fmt:message key='userIDHelp.heading'/>"/>
    <meta name="menu" content="UserIDHelp"/>
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/login-layout.css'/>" /> 
    <script language="javascript" type="text/javascript"> 
    </script>

<style>
div#branding{float: left;width: 100%;margin: 0;text-align: left;height:0px;}        
div#footer div#divider div {border-top:5px solid #EBF5FC;margin:1px 0 0;}
#clearfooter {height:0;}

body{ background-color:#dee9f6; text-align:left; margin:0px; padding:0px; background-image:url(images/indexbg-final.gif); background-repeat:repeat-x;}

#logincontainer{ width:925px; height:375px; background-image:url(images/loginbg-opt2.png); background-repeat:no-repeat; margin-top:-7px;}
#errormessage {width:300px; list-style:none; padding-right:20px; float:right; position:absolute; top:95px;!top:100px; left:520px;!left:510px;}
		
.cssbutton{width: 55px;height: 25px;padding:0px;font-family:'arial';font-weight:normal;color:#000;font-size:12px;background:url(/redsky/images/btn_bg.gif) repeat-x;
background-color:#e7e7e7;border:1px solid #cccccc;}
.cssbutton:hover{color: black; background:url(/redsky/images/btn_hoverbg.gif) repeat-x; background-color: #c1c2bf; }

.textbox_username {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/user.png') no-repeat;background-position: 1 1;padding-left: 19px;}
.textbox_password {background-color:#E7E7E7;border:1px solid #B2B2B2;color:#616161;font-family:Arial,Helvetica,sans-serif;font-size:12px;width:135px;
background: #E7E7E7 url('images/icon_password.png') no-repeat;background-position: 1 1;padding-left: 19px;}

div.error, span.error, li.error, div.message {-moz-background-clip:border;-moz-background-inline-policy:continuous;-moz-background-origin:padding;
background:#B3DAF4 none repeat scroll 0 0;border:1px solid #000000;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;margin:10px auto;
margin-top:80px;padding:3px;text-align:center;vertical-align:bottom;width:500px;z-index:1000;}

.supportalert{ background:#FFDFDF none repeat scroll 0%;border:1px solid #DF5353;color:#000000;font-family:Arial,Helvetica,sans-serif;font-weight:normal;
font-size:1em;padding:2px;text-align:center;vertical-align:bottom;margin-top: 5px;margin-bottom: 0px;}

#indexlogo {width:800px;float:left;	text-align:right;margin-right:0px;margin-top:10px;padding-top:0px;padding-right:40px;padding-bottom:0px;padding-left:0px;}

#leftcolumn {float:left;margin-top:107px;top:150px;width:370px;padding-right: 40px;}

#rightcolumn { width:450px; float:left; margin-top:110px;}

.login-heading {font-size: 15px;color:#4a4646;font-family:Arial,Helvetica,sans-serif;font-size:13px;font-weight:bold;margin:0 3px 0 10px;padding:5px 0 8px;
text-align:left;}

.signup-heading {color:#000000;font-family:Arial,Helvetica,sans-serif;font-size:16px;font-weight:bold;padding:5px 0 3px;text-align:right;}

.left-head {color:#71B9C3;font-family:Arial,Helvetica,sans-serif;font-size:28px;font-weight:bold;line-height:35px;}
  </style>
  <noscript>
  <style type="text/css">
  #overlay{ display:none !important;}
 .opaque{ display:none !important;}
 </style>
 </noscript>
    
<script> 
function sendOTPCode(){  
	 var username=	document.forms['userIDHelpForm'].elements['OTPusername'].value; 
		 if(username=='')
 		 {
			 alert("Please enter username");
			 return false;
		}else{ 
			document.getElementById("sendOTPCodeSubmit").style.display = "none";
			
			return true;
		}
 	  
} 
function disableSendOTPCodeSubmit(){  
	document.getElementById("sendOTPCodeSubmit").setAttribute("disabled", "disabled"); 
}

</script>
<script src="https://www.google.com/recaptcha/api.js"></script>
</head>
<body id="userIDHelp"/>
<div class="separator"></div>
<div id="wrapper"> 
<div id="logincontainer"> 
<div id="leftcolumn" >
  <table width="360" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
      <td align="right" class="left-head" colspan="2">&nbsp;<img src="images/redsky-text1.png" style="cursor: default;width: 200px;"/></td>
    </tr> 
    </table></td>
    </tr>
  </table>
</div> 
	<s:form name="userIDHelpForm" action="verifyCode.html?decorator=otpResetPass&otpResetPass=true" method="post" >
	<div id="rightcolumn" >
	<input type="hidden" class="textbox" id="bypass_sessionfilter" name="bypass_sessionfilter"   value="YES"/> 
	<table border="0" width="100%" cellpadding="3" cellspacing="1"  style="margin-bottom:0px">	
		<tbody>
	        <tr>
              <td  class="login-heading" colspan="4">Please enter username to receive verification pin.
                <span style="height:1px; background:#B2B2B2; width:289px;display:block;margin-top:9px;"></span>
              </td> 
            </tr>
			<tr> 
		      <td  style="text-align:left;border:none !important;font-size: 12px;width: 65px;color:#616161; padding-top:6px;" class="listwhitetext" >Username:</td>	
		      <td style="text-align:left;border:none !important;padding-top:6px;" colspan="2" ><s:textfield   cssStyle="width:215px;border:1px solid #B2B2B2; height:17px; padding-left:5px;" id="OTPusername" name="OTPusername" cssClass="input-text"  /></td>
			</tr> 	
		</tbody>
	</table>
	 <div class="g-recaptcha" data-sitekey="6LdW4p0UAAAAAAyiNM7pFxvINq8mzGf_lxZOEeIi"></div>  
    <table border="0" cellpadding="2"  style="margin:0px">
        <tbody>
            <tr>
             <div class="modal-footer" style="padding:0px;">
              <td style="border:none !important; width:67px;"></td>
              <td align="left" width="2" style="    padding-top: 9px;">
               <s:submit name="sendOTPCodeSubmit" id="sendOTPCodeSubmit" cssStyle="font-weight: normal; color: #000;  font-size: 12px;  background: url(/redsky/images/btn_bg.gif) repeat-x;width:80px;height: 25px; background-color: #e7e7e7; border: 1px solid #cccccc;" key="Continue" onclick="return sendOTPCode();" ></s:submit>
              </td>
             </div> 
			</tr> 
 
    </table>
    </div>
    </s:form>
