package com.trilasoft.app.webapp.action;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.commons.beanutils.converters.BigDecimalConverter;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ListCellRenderer;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.Role;
import org.appfuse.model.User;
import org.appfuse.service.UserManager;
import org.appfuse.webapp.action.BaseAction;
import org.bouncycastle.asn1.ocsp.Request;

import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.ValidationAware;
import com.trilasoft.app.dao.hibernate.UgwwActionTrackerDaoHibernate.SODashboardDTO;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.Billing;
import com.trilasoft.app.model.Carton;
import com.trilasoft.app.model.Claim;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyToDoRuleExecutionUpdate;
import com.trilasoft.app.model.ConsigneeInstruction;
import com.trilasoft.app.model.Container;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.DataCatalog;
import com.trilasoft.app.model.Miscellaneous;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.model.ServicePartner;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.model.ToDoResult;
import com.trilasoft.app.model.ToDoRule;
import com.trilasoft.app.model.TrackingStatus;
import com.trilasoft.app.model.UgwwActionTracker;
import com.trilasoft.app.model.Vehicle;
import com.trilasoft.app.model.WorkTicket;
import com.trilasoft.app.service.AuditTrailManager;
import com.trilasoft.app.service.BillingManager;
import com.trilasoft.app.service.CartonManager;
import com.trilasoft.app.service.CheckListManager;
import com.trilasoft.app.service.ClaimManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CompanyToDoRuleExecutionUpdateManager;
import com.trilasoft.app.service.ConsigneeInstructionManager;
import com.trilasoft.app.service.ContainerManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.ErrorLogManager;
import com.trilasoft.app.service.MiscellaneousManager;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.service.ServiceOrderManager;
import com.trilasoft.app.service.ServicePartnerManager;
import com.trilasoft.app.service.SystemDefaultManager;
import com.trilasoft.app.service.ToDoResultManager;
import com.trilasoft.app.service.ToDoRuleManager;
import com.trilasoft.app.service.RuleActionsManager;
import com.trilasoft.app.service.TrackingStatusManager;
import com.trilasoft.app.service.UgwwActionTrackerManager;
import com.trilasoft.app.service.VehicleManager;
import com.trilasoft.app.service.WorkTicketManager;

public class ToDoRuleAction extends BaseAction implements Preparable  {
	
    private String sessionCorpID;
    private ToDoRule toDoRule;
    private List ls; 
    private List nls;    
    private Long id;
    private Long id1;
    private Long soId;
	private Set toDoRules;	
	private CustomerFileManager customerFileManager;
	private CustomerFile customerFile;	
	private UserManager userManager;
	private User user;	
	private ServiceOrderManager serviceOrderManager;
	private WorkTicketManager workTicketManager;
	private ClaimManager claimManager;
	private WorkTicket workTicket;
	private Claim claim;
	private List<ServiceOrder> serviceOrder;	 
	private ToDoResultManager toDoResultManager;
	private static List<ToDoResult> toDoResult;
	private Set toDoResults;	
	private CheckListManager checkListManager;	
	private ToDoResult todoresult;
	private NotesManager notesManager;
	private List<Notes> note;
	private Set notes;
	private List nls1;
	private Set notes1;
	private List nls2;
	private Set notes2;
	private List auditTrailList;
	private Set toDayAlertList;
	private AuditTrailManager auditTrailManager;
	private int countAlertToday;
	private List lstoday;
	private Set toDoResultsToday;
	private List lsDueWeek;
	private Set toDoResultsDueWeek;
	private String entity;
	private int countToday;
	private int countDueWeek;
	private int countOverDue;	
	private List lsComingUp;
	private Set toDoResultsComingUp;
	private int countResultComingUp;	
	private int countResultToday;
	private int countResultDueWeek;
	private int countResultOverDue;	
	private int countNumberOfRules;
	private int countNumberOfResult;
	private Set<Role> roles;
	private String isSupervisor="false";
	private List checkListTodayList;
	private List checkListOverDueList;
	private List checkListTodaySet;
	private List agentTdrEmailSet;
	private List checkListOverDueSet;
	private Integer countCheckListTodayResult;
	private Integer countCheckListOverDueResult;
	Date currentdate = new Date();
	static final Logger logger = Logger.getLogger(ToDoRuleAction.class);
	private String newIdList;
	private static Map<String, String> ptest;
	private static Map<String, String> pfield;
	private static Map<String, String> pstest;
	private static List refMasters;
	private RefMasterManager refMasterManager;
	private ToDoRuleManager toDoRuleManager;
	private RuleActionsManager ruleActionsManager;
	private static Map<String, String> pentity;
	private static Map<String, String> pduration;
	private static Map<String, String> prulerole;
	private static Map<String, String> taskUser;
	private List userList;
	private String btn;
	private String docCheckListbtn;
	private static List selectedField;
	private static List<DataCatalog> selectedDateField;
	private Long maxId;
	private List messageList;
	private List ruleIdList;
	private ToDoRule addCopyToDoRule;
	private static Map<String, String> notetype;
	private static Map<String, String> notesubtype;
    private String tableName;
	private List fieldList;
	private String recordId;
	private String fieldToValidate;
    private String toDoRuleCheckEnable;
	private String historyId;
	private String setIsViewHistoryFlag="NO";
	
	private String ruleId;
	private String validateFormNav;
	private String gotoPageString;	
	private String ownerName;
	private String shipperName;
	private String param;
	private Map<String, String> toEmail;
	private List toEmailList;
	private String messageDisplayed;
	private Long userId;
	private String userFirstName;
	private String userName="";
	private String userType;
	private List filterCheck;
	private CompanyManager companyManager;
	private Company company;
	//private List networkFileList;
	private Integer networkLinkSize;
	private String chkAutomaticLink ;
	private BillingManager billingManager;
	private TrackingStatusManager trackingStatusManager;
	private MiscellaneousManager miscellaneousManager;
	private ConsigneeInstructionManager consigneeInstructionManager;
	private List maxSequenceNumber;
	private Long autoSequenceNumber;
	private String seqNum;
	private List checkSequenceNumberList;
	private List maxShip;
	private String ship;
	private Long autoShip;
	private String shipnumber;
	private ContainerManager containerManager;
	private VehicleManager vehicleManager;
	private CartonManager cartonManager;
	private ServicePartnerManager servicePartnerManager;
	private SystemDefaultManager systemDefaultManager;
	private ErrorLogManager errorLogManager;
	public String chkFlag;
	public String successMessage;
	private String bookingAgentName;
	private String bookingAgentcode;
	private SystemDefault systemDefault;
	private ServiceOrder soObj;
	private List linkingSequenceNumber;
	private List linkingSeqNumByName;
	public String hitFlag;
	private Long  soUGWWID;
	private Long mergingSOID;
	private List integrationErrorList;
	private String relatedTask="";
	private UgwwActionTracker ugwwActionTracker;
	private UgwwActionTrackerManager ugwwActionTrackerManager;
	private Map <String, String> sequenceNumberMap = new LinkedHashMap<String, String>() ;
	private Map <String, String> sequenceNumberMapByName = new LinkedHashMap<String, String>() ;
	private List toDoResults1;
	private List toDoResultsToday1;
	private List errorRuleNumberList = new ArrayList();
	private Map<String,List<String>> networkFields;
	private Map<String,List<String>> resultMap;
	private ServiceOrder incomingSO;
	private ServiceOrder yourSO;
	public List <Container> contList = new ArrayList<Container>();
	public List <Carton> pieceCountList= new ArrayList<Carton>();
	public List <Vehicle>  vehicleList= new ArrayList<Vehicle>();
	public List <ServicePartner> servPartnerList= new ArrayList<ServicePartner>();
	public String orgId;
	private CustomerFileAction customerFileAction;
	private String wUnit;
	private String vUnit;
	private String chkRulesRunning;
	private int countContainer;
	private int countCarton;
	private int countVehicle;
	private int countServicePartner;
	//Bug 8686 - Document Checklist - Revised
	private Map<String, String> docsList;
	// changes for Bug 8449 - Reassignment of Tasks
	private  Map<String, String> coord = new LinkedHashMap<String, String>();
	private Map<String, String> sale;
	private Map<String, String> billingList;
	private String tempUserForPage;
	private String coordSelected;
	private List lsEmail;
	private Integer countAgentTdrEmail;	
	private String voxmeIntergartionFlag;
	private String mmValidation = "NO";
	private Boolean isAgentTdr = false;
	private String flagForSupervisor="false";
	private boolean flagActivityMgmtVersion2=false;
	private String oiJobList;
	private List actionList;
	private boolean flagexternalCordinator=false;
	private CompanyToDoRuleExecutionUpdateManager companyToDoRuleExecutionUpdateManager;
	private CompanyToDoRuleExecutionUpdate companyToDoRuleExecutionUpdate;
	private boolean chkOwnTeamAll=false;
	
	public Map<String, String> getCoord() {
		return coord;
	}

	public void setCoord(Map<String, String> coord) {
		this.coord = coord;
	}

	public void prepare() throws Exception {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
        this.userId=user.getId();
        this.userFirstName=user.getUsername();
        this.userType=user.getUserType();
        tempUserForPage = userFirstName.toUpperCase();
        coord = refMasterManager.findUser(sessionCorpID,"ROLE_COORD");
      //  sale = refMasterManager.findUser(sessionCorpID, "ROLE_SALE");
      //  billingList = refMasterManager.findUser(sessionCorpID,"ROLE_BILLING");
        docsList = refMasterManager.findDocumentList(sessionCorpID, "DOCUMENT");
       
		Map filterMap=user.getFilterMap();
		String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
		boolean checkFieldVisibilityOwnTeamAndAll=false;
		checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if(checkFieldVisibilityOwnTeamAndAll || (filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter"))){
			////// new code runs///////
			chkOwnTeamAll=true;
				flagexternalCordinator=true;
		} 
        
        if(userType.equalsIgnoreCase("USER")){
        filterCheck=toDoRuleManager.getFilterName(sessionCorpID,userId);
        if( filterCheck!=null && !filterCheck.isEmpty()){
            ownerName=user.getUsername();
   		}
        }
        company= companyManager.findByCorpID(sessionCorpID).get(0);
        if(company.getAutomaticLinkup()!= null && company.getAutomaticLinkup()== true){
        	chkAutomaticLink= "Y";
        }else{
        	chkAutomaticLink= "N";
        }
        if(company.getRulesRunning()!= null && company.getRulesRunning()){
        	chkRulesRunning= "Y";
        }else{
        	chkRulesRunning= "N";
        }
        if(company!=null && company.getOiJob()!=null){
				oiJobList=company.getOiJob();  
		}
        flagActivityMgmtVersion2 = company.getActivityMgmtVersion2();
        
        systemDefault = systemDefaultManager.findByCorpID(sessionCorpID).get(0);
        if(systemDefault!=null){
        	wUnit= systemDefault.getWeightUnit();
        	vUnit = systemDefault.getVolumeUnit();
        }
        companyToDoRuleExecutionUpdate=companyToDoRuleExecutionUpdateManager.findByCorpID(sessionCorpID).get(0);
        if(companyToDoRuleExecutionUpdate.getRulesRunning()!= null && companyToDoRuleExecutionUpdate.getRulesRunning()){
        	chkRulesRunning= "Y";
        }else{
        	chkRulesRunning= "N";
        }
        
   }

	public ToDoRuleAction(){
    	
	}
	public List systemDefaults;
	@SkipValidation
	public String todoList(){
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal(); 
	try 
	{
	Map filterMap=user.getFilterMap();
		 List list3 = new ArrayList();
		 List list4 = new ArrayList();
		 String bookingAgentPopup="";
		 String billToCodePopup="";
		 List companydivisionBookingAgentList= new ArrayList();
		 String bookingAgentSet="";
		 String userLogged="";
				ownerName="";
				String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
				boolean checkFieldVisibilityOwnTeamAndAll=false;
				checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
	if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
		
			
			if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
				
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
	    		list4.add("'" + code + "'");
	    		
	    		}
	    			
	    	} 
			}
			bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
		    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list3.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
		    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
			
			Long StartTime = System.currentTimeMillis();
			 userLogged="'"+getRequest().getRemoteUser()+"'";
			userList=toDoRuleManager.getOwnersListExtCordinator(sessionCorpID,userLogged,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup);
			userList.remove("UNASSIGNED");
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken for owner list : "+timeTaken+"\n\n");
	        Long StartTimeMess = System.currentTimeMillis();
	        messageList=toDoRuleManager.getMessageListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
	        Long timeTakenMess =  System.currentTimeMillis() - StartTimeMess;
	    	logger.warn("\n\nTime taken for messa list : "+timeTakenMess+"\n\n");
	    	Long StartTimeRule = System.currentTimeMillis();
	    	ruleIdList=toDoRuleManager.getRuleIdListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
	    	Long timeTakenRule =  System.currentTimeMillis() - StartTimeRule;
	    	logger.warn("\n\nTime taken for rule list : "+timeTakenRule+"\n\n");
	    	//note = notesManager.getAll();
	    	Long StartTimeRole = System.currentTimeMillis();
	    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
	    	roles = user.getRoles();
	    	Long timeTakenRole =  System.currentTimeMillis() - StartTimeRole;
	    	logger.warn("\n\nTime taken for user role list : "+timeTakenRole+"\n\n");
	    	int daystoManageAlerts=systemDefault.getDaystoManageAlert();
	    	
	    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
	    		isSupervisor ="true";
	    	}*/		
	    	List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
	    	if(!checkSupervisor.isEmpty()){
	    		isSupervisor ="true";
	    	}

			//For Today's Note
			nls=toDoRuleManager.findByStatusExtCordinator("NEW",sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup);
			notes=new HashSet(nls);
			countToday=notes.size();
			
			// For OverDue Note
			nls1=toDoRuleManager.findByStatus1ExtCordinator("NEW",sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup);
			notes1=new HashSet(nls1);
			countOverDue=notes1.size();
			
			// For Due This Week Note
			nls2=toDoRuleManager.findByStatus2ExtCordinator("NEW",sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup);
			notes2=new HashSet(nls2);
			countDueWeek=notes2.size();
			
			checkListTodayList=getCheckListResultExtCordinator(sessionCorpID,userLogged,"today",ruleId,"",bookingAgentPopup,billToCodePopup,userType);
			checkListTodaySet = new ArrayList(checkListTodayList);
			checkListOverDueList=getCheckListResultExtCordinator(sessionCorpID,userLogged,"overDue", ruleId,"",bookingAgentPopup,billToCodePopup,userType);
			checkListOverDueSet= new ArrayList(checkListOverDueList);
			countCheckListTodayResult=checkListTodayList.size();
			countCheckListOverDueResult=checkListOverDueList.size();
		   	//For alert history
			auditTrailList = auditTrailManager.findByAlertListFilterForExtCoordinator(sessionCorpID,userLogged,daystoManageAlerts,bookingAgentPopup,billToCodePopup);
			toDayAlertList = new HashSet(auditTrailList);
			countAlertToday=toDayAlertList.size();		
			ls=toDoRuleManager.findBydurationUserExtCordinator(sessionCorpID,userLogged,messageDisplayed,ruleId,shipperName,user,bookingAgentPopup,billToCodePopup);
	    	toDoResults1 = new ArrayList(ls);
	    	countResultOverDue=toDoResults1.size();
	    	
	    	// For Result Today
	    	lstoday=toDoRuleManager.findBydurationdtodayUserExtCordinator(sessionCorpID,userLogged,messageDisplayed,ruleId,shipperName,user,bookingAgentPopup,billToCodePopup);
	    	toDoResultsToday1 = new ArrayList(lstoday);
	    	countResultToday=toDoResultsToday1.size();
	    	
	    	//For Agent TDR Email
	    	lsEmail=toDoRuleManager.findByAgentTdrEmailExtCordinator(sessionCorpID,userLogged,bookingAgentPopup,billToCodePopup,user.getUserType()); 
		   	countAgentTdrEmail=lsEmail.size();
		   	agentTdrEmailSet=new ArrayList(lsEmail);
		
			
	}else{
	//getComboList(sessionCorpID);
	Long StartTime = System.currentTimeMillis();
	userList=toDoRuleManager.getOwnersList(sessionCorpID,getRequest().getRemoteUser(),userId,userFirstName,userType);
	Long timeTaken =  System.currentTimeMillis() - StartTime;
	logger.warn("\n\nTime taken for owner list : "+timeTaken+"\n\n");
	Long StartTimeMess = System.currentTimeMillis();
	messageList=toDoRuleManager.getMessageList(sessionCorpID,getRequest().getRemoteUser(),userId,userType); 
	Long timeTakenMess =  System.currentTimeMillis() - StartTimeMess;
	logger.warn("\n\nTime taken for messa list : "+timeTakenMess+"\n\n");
	Long StartTimeRule = System.currentTimeMillis();
	ruleIdList=toDoRuleManager.getRuleIdList(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
	Long timeTakenRule =  System.currentTimeMillis() - StartTimeRule;
	logger.warn("\n\nTime taken for rule list : "+timeTakenRule+"\n\n");
	//note = notesManager.getAll();
	Long StartTimeRole = System.currentTimeMillis();
	user = userManager.getUserByUsername(getRequest().getRemoteUser());
	roles = user.getRoles();
	Long timeTakenRole =  System.currentTimeMillis() - StartTimeRole;
	logger.warn("\n\nTime taken for user role list : "+timeTakenRole+"\n\n");
	//String supervisor=user.getSupervisor();
	/*id = Long.parseLong(systemDefaultManager.findIdByCorpID(sessionCorpID).get(0).toString());
	if (id != null) {
		systemDefault = systemDefaultManager.get(id);
	}*/
	int daystoManageAlerts=systemDefault.getDaystoManageAlert();
	
	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
		isSupervisor ="true";
	}*/		
	List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
	if(!checkSupervisor.isEmpty()){
		isSupervisor ="true";
	}
	

	if(((roles.toString().indexOf("ROLE_ADMIN")) > -1) || ((roles.toString().indexOf("ROLE_EXEC")) > -1)){
		//For Today's Note 
		Long StartTimeRuleToday = System.currentTimeMillis();
		nls=toDoRuleManager.findByStatus("NEW",sessionCorpID);
		Long timeTakenRuleToday =  System.currentTimeMillis() - StartTimeRuleToday;
		logger.warn("\n\nTime taken for due list : "+timeTakenRuleToday+"\n\n");
		notes=new HashSet(nls);
		countToday=notes.size();
		// For OverDue Note  
		Long StartTimeOverdue = System.currentTimeMillis();
		nls1=toDoRuleManager.findByStatus1("NEW",sessionCorpID);
		Long timeTakenOverdue =  System.currentTimeMillis() - StartTimeOverdue;
		logger.warn("\n\nTime taken for overrdue list : "+timeTakenOverdue+"\n\n");
		notes1=new HashSet(nls1);
		countOverDue=notes1.size();
		// For Due This Week Note  
		Long StartTimeNote = System.currentTimeMillis();
		nls2=toDoRuleManager.findByStatus2("NEW",sessionCorpID);
		Long timeTakenNote =  System.currentTimeMillis() - StartTimeNote;
		logger.warn("\n\nTime taken for note list : "+timeTakenNote+"\n\n");
		notes2=new HashSet(nls2);
		countDueWeek=notes2.size();
		// For checkList Result Today		
		checkListTodayList=getCheckListResult(sessionCorpID,"","today",ruleId,"");
		checkListTodaySet = new ArrayList(checkListTodayList);
		countCheckListTodayResult=checkListTodayList.size();
		checkListOverDueList=getCheckListResult(sessionCorpID,"","overDue",ruleId,"");
		checkListOverDueSet= new ArrayList(checkListOverDueList);
		countCheckListOverDueResult=checkListOverDueList.size();
	   	//For alert history
		Long StartTimealert = System.currentTimeMillis();
		auditTrailList = auditTrailManager.getAlertHistoryList(sessionCorpID,daystoManageAlerts);
		Long timeTakenalert =  System.currentTimeMillis() - StartTimealert;
		logger.warn("\n\nTime taken for alert list : "+timeTakenalert+"\n\n");
		toDayAlertList = new HashSet(auditTrailList);
		countAlertToday=toDayAlertList.size();
	}else{
		//For Today's Note
		nls=toDoRuleManager.findByStatus("NEW",sessionCorpID,getRequest().getRemoteUser());
		notes=new HashSet(nls);
		countToday=notes.size();
		
		// For OverDue Note
		nls1=toDoRuleManager.findByStatus1("NEW",sessionCorpID,getRequest().getRemoteUser());
		notes1=new HashSet(nls1);
		countOverDue=notes1.size();
		
		// For Due This Week Note
		nls2=toDoRuleManager.findByStatus2("NEW",sessionCorpID,getRequest().getRemoteUser());
		notes2=new HashSet(nls2);
		countDueWeek=notes2.size();
		
		checkListTodayList=getCheckListResult(sessionCorpID,getRequest().getRemoteUser(),"today",ruleId,"");
		checkListTodaySet = new ArrayList(checkListTodayList);
		checkListOverDueList=getCheckListResult(sessionCorpID,getRequest().getRemoteUser(),"overDue", ruleId,"");
		checkListOverDueSet= new ArrayList(checkListOverDueList);
		countCheckListTodayResult=checkListTodayList.size();
		countCheckListOverDueResult=checkListOverDueList.size();
	   	//For alert history
		auditTrailList = auditTrailManager.findByAlertListFilter(sessionCorpID,getRequest().getRemoteUser(),daystoManageAlerts);
		toDayAlertList = new HashSet(auditTrailList);
		countAlertToday=toDayAlertList.size();				
	}
	
	if(((roles.toString().indexOf("ROLE_ADMIN")) > -1) || ((roles.toString().indexOf("ROLE_EXEC")) > -1)){
		// For Result Coming UP
	   	//lsComingUp = toDoRuleManager.findByDurationComingUp(sessionCorpID);
	   	//toDoResultsComingUp = new HashSet(lsComingUp);
	   	//countResultComingUp = toDoResultsComingUp.size();
	   	
	   	// For Result Due This Week
	   	//lsDueWeek=toDoRuleManager.findBydurationdueweek(sessionCorpID);
	   	//toDoResultsDueWeek=new HashSet(lsDueWeek);
	   	//countResultDueWeek=toDoResultsDueWeek.size();
		
	    //	 For Result OverDue
	   	ls=toDoRuleManager.findByduration(sessionCorpID);
	   	toDoResults1=new ArrayList(ls);
	   	countResultOverDue=toDoResults1.size();
	   	
	   	// For Result Today
	   	lstoday=toDoRuleManager.findBydurationdtoday(sessionCorpID);
	   	toDoResultsToday1=new ArrayList(lstoday);
	   	countResultToday=toDoResultsToday1.size();
	   	
	   	//For Agent TDR Email
	   	lsEmail=toDoRuleManager.findByAgentTdrEmail(sessionCorpID,"");
	   	countAgentTdrEmail=lsEmail.size();
	   	agentTdrEmailSet=new ArrayList(lsEmail);
	}else{    
		// For Result Coming UP
    	//lsComingUp = toDoRuleManager.findByDurationComingUpUser(sessionCorpID,getRequest().getRemoteUser(),messageDisplayed,ruleId,shipperName);
    	//toDoResultsComingUp = new HashSet(lsComingUp);
    	//countResultComingUp = toDoResultsComingUp.size();
		
		// For Result Due This Week
    	//lsDueWeek=toDoRuleManager.findBydurationdueweekUser(sessionCorpID,getRequest().getRemoteUser());
    	//toDoResultsDueWeek=new HashSet(lsDueWeek);
    	//countResultDueWeek=toDoResultsDueWeek.size();
    	
		// For Result OverDue
    	ls=toDoRuleManager.findBydurationUser(sessionCorpID,getRequest().getRemoteUser(),messageDisplayed,ruleId,shipperName,user);
    	toDoResults1 = new ArrayList(ls);
    	countResultOverDue=toDoResults1.size();
    	
    	// For Result Today
    	lstoday=toDoRuleManager.findBydurationdtodayUser(sessionCorpID,getRequest().getRemoteUser(),messageDisplayed,ruleId,shipperName,user);
    	toDoResultsToday1 = new ArrayList(lstoday);
    	countResultToday=toDoResultsToday1.size();
    	
    	//For Agent TDR Email
    	lsEmail=toDoRuleManager.findByAgentTdrEmail(sessionCorpID,getRequest().getRemoteUser()); 
	   	countAgentTdrEmail=lsEmail.size();
	   	agentTdrEmailSet=new ArrayList(lsEmail);
	 } 
	/* Linking file list for ugww 
	 * Bug# 6549 - 2Step_merging_and_linking
	 * reported by Maha Shatat 
	 * Code by Gautam Verma
	 */
	
		/* Long StartTime = System.currentTimeMillis();
		 networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
		 networkLinkSize = networkFileList.size();
		 Long timeTaken =  System.currentTimeMillis() - StartTime;
		 logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");*/
	
	}
	}
	catch(Exception e){
		
		
	}
	// method end of 2 step linking
    logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;   
}
	public Date getZeroTimeDate(Date dt) {
	    Date res = dt;
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTime(dt);
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	    res = calendar.getTime();
	    return res;
	}
	@SkipValidation
   public String tasksList(){
	   logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
			if(company.getVoxmeIntegration()!=null){
				voxmeIntergartionFlag=company.getVoxmeIntegration().toString();			
				
			}
			 if(company.getEnableMobileMover() && company.getMmStartDate()!=null && company.getMmStartDate().compareTo(getZeroTimeDate(new Date()))<=0 ){
				mmValidation= "Yes";
				if(company.getMmEndDate()!=null && company.getMmEndDate().compareTo(getZeroTimeDate(new Date()))<0 ){
					mmValidation= "No";
				}
			}
			}
		if(relatedTask.equalsIgnoreCase("relatedTask")){
			if(tableName.equalsIgnoreCase("serviceorder")|| tableName.equalsIgnoreCase("accountline")|| tableName.equalsIgnoreCase("trackingstatus")|| tableName.equalsIgnoreCase("miscellaneous")|| tableName.equalsIgnoreCase("billing") ){
				soObj=serviceOrderManager.get(id);
				customerFile= customerFileManager.get(soObj.getCustomerFileId());
				// For Due This Week Note  
				nls2=toDoRuleManager.getComingUpList(soObj.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(soObj.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(soObj.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(soObj.getId(),tableName,sessionCorpID,"relatedTask",soObj.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
			   	lstoday=toDoRuleManager.getResultToday(soObj.getSequenceNumber(),soObj.getId(),sessionCorpID,"relatedTask");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(soObj.getSequenceNumber(),soObj.getId(),sessionCorpID,"relatedTask");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(soObj.getSequenceNumber(),soObj.getId(),sessionCorpID, "","today","relatedTask");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(soObj.getSequenceNumber(),soObj.getId(),sessionCorpID,"","overDue","relatedTask");
				countCheckListOverDueResult=checkListOverDueList.size();
				 //For Agent TDR Email
   	            lsEmail=toDoRuleManager.findByAgentTdrEmail(soObj.getShipNumber(),sessionCorpID,"");
   	           countAgentTdrEmail=lsEmail.size();
   	          agentTdrEmailSet=new ArrayList(lsEmail);
			}
			if(tableName.equalsIgnoreCase("customerfile")){
				customerFile= customerFileManager.get(id);
				nls2=toDoRuleManager.getComingUpList(customerFile.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(customerFile.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(customerFile.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(customerFile.getId(),tableName,sessionCorpID,"relatedTask",customerFile.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
			   	lstoday=toDoRuleManager.getResultToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"relatedTask");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"relatedTask");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID, "","today","relatedTask");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"","overDue","relatedTask");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
			if(tableName.equalsIgnoreCase("workticket")){
				soObj=serviceOrderManager.get(soId);
				workTicket= workTicketManager.get(id);
				String workTicketId=workTicket.getId().toString();
				nls2=toDoRuleManager.getComingUpList(workTicket.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(workTicket.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(workTicket.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(workTicket.getId(),tableName,sessionCorpID,"relatedTask",workTicket.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
				String workTicketTicket=workTicket.getTicket().toString();
			   	lstoday=toDoRuleManager.getResultToday(workTicket.getSequenceNumber(),workTicket.getId(),sessionCorpID,"relatedTask");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(workTicket.getSequenceNumber(),workTicket.getId(),sessionCorpID,"relatedTask");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(workTicket.getSequenceNumber(),workTicket.getId(),sessionCorpID, "","today","relatedTask");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(workTicket.getSequenceNumber(),workTicket.getId(),sessionCorpID,"","overDue","relatedTask");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
			if(tableName.equalsIgnoreCase("claim")){
				soObj=serviceOrderManager.get(soId);
				claim= claimManager.get(id);
				String claimNumber=claim.getClaimNumber().toString();
				nls2=toDoRuleManager.getComingUpList(claim.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(claim.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(claim.getSequenceNumber(),sessionCorpID,"relatedTask");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(claim.getId(),tableName,sessionCorpID,"relatedTask",claim.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
				//String claimNumber=claim.getClaimNumber().toString();
			   	lstoday=toDoRuleManager.getResultToday(claim.getSequenceNumber(),claim.getId(),sessionCorpID,"relatedTask");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(claim.getSequenceNumber(),claim.getId(),sessionCorpID,"relatedTask");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(claim.getSequenceNumber(),claim.getId(),sessionCorpID, "","today","relatedTask");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(claim.getSequenceNumber(),claim.getId(),sessionCorpID,"","overDue","relatedTask");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
		}else{
			if(tableName.equalsIgnoreCase("serviceorder")|| tableName.equalsIgnoreCase("accountline")|| tableName.equalsIgnoreCase("trackingstatus")|| tableName.equalsIgnoreCase("miscellaneous")|| tableName.equalsIgnoreCase("billing") ){
				soObj=serviceOrderManager.get(id);
				customerFile= customerFileManager.get(soObj.getCustomerFileId());
				// For Due This Week Note  
				nls2=toDoRuleManager.getComingUpList(soObj.getShipNumber(),sessionCorpID,"");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(soObj.getShipNumber(),sessionCorpID,"");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(soObj.getShipNumber(),sessionCorpID,"");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(soObj.getId(),tableName,sessionCorpID,"",soObj.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
			   	lstoday=toDoRuleManager.getResultToday(soObj.getShipNumber(),soObj.getId(),sessionCorpID,"");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(soObj.getShipNumber(),soObj.getId(),sessionCorpID,"");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(soObj.getShipNumber(),soObj.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(soObj.getShipNumber(),soObj.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
				 //For Agent TDR Email
   	            lsEmail=toDoRuleManager.findByAgentTdrEmail(soObj.getShipNumber(),sessionCorpID,"");
   	           countAgentTdrEmail=lsEmail.size();
   	          agentTdrEmailSet=new ArrayList(lsEmail);
			}
			if(tableName.equalsIgnoreCase("customerfile")){
				customerFile= customerFileManager.get(id);
				nls2=toDoRuleManager.getComingUpList(customerFile.getSequenceNumber(),sessionCorpID,"");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(customerFile.getSequenceNumber(),sessionCorpID,"");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(customerFile.getSequenceNumber(),sessionCorpID,"");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(customerFile.getId(),tableName,sessionCorpID,"",customerFile.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
			   	lstoday=toDoRuleManager.getResultToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(customerFile.getSequenceNumber(),customerFile.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
			if(tableName.equalsIgnoreCase("workticket")){
				soObj=serviceOrderManager.get(soId);
				workTicket= workTicketManager.get(id);
				String workTicketId=workTicket.getTicket().toString();
				nls2=toDoRuleManager.getComingUpList(workTicketId,sessionCorpID,"");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(workTicketId,sessionCorpID,"");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(workTicketId,sessionCorpID,"");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(workTicket.getId(),tableName,sessionCorpID,"",workTicket.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
				String workTicketTicket=workTicket.getTicket().toString();
			   	lstoday=toDoRuleManager.getResultToday(workTicketTicket,workTicket.getId(),sessionCorpID,"");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(workTicketTicket,workTicket.getId(),sessionCorpID,"");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(workTicketTicket,workTicket.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(workTicketTicket,workTicket.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
			if(tableName.equalsIgnoreCase("claim")){
				soObj=serviceOrderManager.get(soId);
				claim= claimManager.get(id);
				String claimNumber=claim.getClaimNumber().toString();
				nls2=toDoRuleManager.getComingUpList(claimNumber,sessionCorpID,"");
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				//For Today's Note  
				nls=toDoRuleManager.getDueToday(claimNumber,sessionCorpID,"");
				notes=new HashSet(nls);
				countToday=notes.size();
				// For OverDue Note  
				nls1=toDoRuleManager.getOverDue(claimNumber,sessionCorpID,"");
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				//For alert history
				auditTrailList = auditTrailManager.getAlertTaskList(claim.getId(),tableName,sessionCorpID,"",claim.getSequenceNumber());
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				// For Result Today
				//String claimNumber=claim.getClaimNumber().toString();
			   	lstoday=toDoRuleManager.getResultToday(claimNumber,claim.getId(),sessionCorpID,"");
			   	toDoResultsToday=new HashSet(lstoday);
			   	countResultToday=toDoResultsToday.size();
			   	
				ls=toDoRuleManager.getToDoOverDue(claimNumber,claim.getId(),sessionCorpID,"");
			   	toDoResults=new HashSet(ls);
			   	countResultOverDue=toDoResults.size();
			   	//checkListTodayList=checkListManager.getResultList(sessionCorpID, "","today");
			   	checkListTodayList=toDoRuleManager.getResultListDueToday(claimNumber,claim.getId(),sessionCorpID, "","today","");
			   	countCheckListTodayResult=checkListTodayList.size();
				checkListOverDueList=toDoRuleManager.getResultListDueToday(claimNumber,claim.getId(),sessionCorpID,"","overDue","");
				countCheckListOverDueResult=checkListOverDueList.size();
			}
		}
		
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	   return SUCCESS; 
   }
	private String agentType;
	private String childAgentType;
	private String childAgentCode;
	private String agentCompDiv;
	private TrackingStatusAction trackingStatusAction;
	@SkipValidation
	public String linkServiceOrdersMethod(){
		Long StartTime = System.currentTimeMillis();
		soObj = serviceOrderManager.getForOtherCorpid(soId);
		linkingSequenceNumber = toDoRuleManager.linkSequenceNumberList(sessionCorpID,bookingAgentcode,agentType,childAgentType,childAgentCode,soObj,"");   
		if(linkingSequenceNumber!=null && !linkingSequenceNumber.isEmpty() && linkingSequenceNumber.get(0)!=null){
		Iterator itr = linkingSequenceNumber.iterator();
			while(itr.hasNext()){
				try{
				Long cid = new Long(itr.next().toString());
				CustomerFile customerFileTemp = customerFileManager.get(cid);
				String tempVal = customerFileTemp.getSequenceNumber()+", "+customerFileTemp.getFirstName()+" "+customerFileTemp.getLastName();
				sequenceNumberMap.put(customerFileTemp.getSequenceNumber(), tempVal);
				}catch(Exception e){
				e.printStackTrace();	
				}
			}
		}
		/*linkingSeqNumByName =   toDoRuleManager.linkSequenceNumberList(sessionCorpID,bookingAgentcode,agentType,childAgentType,childAgentCode,soObj,"Name");
		if(linkingSeqNumByName!=null && !linkingSeqNumByName.isEmpty() && linkingSeqNumByName.get(0)!=null){
			Iterator itr = linkingSeqNumByName.iterator();
				while(itr.hasNext()){
					try{
					Long cid = new Long(itr.next().toString());
					CustomerFile customerFileTemp = customerFileManager.get(cid);
					String tempVal = customerFileTemp.getSequenceNumber()+", "+customerFileTemp.getFirstName()+" "+customerFileTemp.getLastName();
					sequenceNumberMapByName.put(customerFileTemp.getSequenceNumber(), tempVal);
					}catch(Exception e){
					e.printStackTrace();	
					}
				}
			}*/
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to show linking SO list: "+timeTaken+"\n\n");
		return SUCCESS; 	
	}
	private List linkedSOMerge;
	private String seqNumber;
	private String filter;
	@SkipValidation
	public String linkingSOMethod(){
		Long StartTime = System.currentTimeMillis();
		soObj =  serviceOrderManager.getForOtherCorpid(soUGWWID);
		linkedSOMerge =  toDoRuleManager.findLinkSO(sessionCorpID,seqNumber,bookingAgentcode,agentType,soObj,filter);   
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to show Detail of service order for merge: "+timeTaken+"\n\n");
		return SUCCESS;
	}
	
	@SkipValidation
	public String linkWithSOMethod(){
		Long StartTime = System.currentTimeMillis();
		ServiceOrder serviceOrderUGWW = serviceOrderManager.getForOtherCorpid(soUGWWID);
		CustomerFile customerFileUGWW = serviceOrderUGWW.getCustomerFile();
		ServiceOrder linkServiceOrder = serviceOrderManager.get(mergingSOID);
		CustomerFile linkCustomerFile = linkServiceOrder.getCustomerFile();
		TrackingStatus  trackingStatusUGWW = trackingStatusManager.getForOtherCorpid(soUGWWID);
		TrackingStatus  linkTrackingStatus = trackingStatusManager.get(mergingSOID);
		
		chkFlag="Merge";
		insertIntoTracker(serviceOrderUGWW.getShipNumber(),"Merge");
		// QF CF check
		if(linkCustomerFile.getControlFlag().toString().trim().equals("Q")){
			linkCustomerFile.setControlFlag("C");
			linkCustomerFile.setQuotationStatus("Accepted");
			linkCustomerFile.setStatus("NEW");
			linkCustomerFile.setStatusDate(new Date());
			linkCustomerFile.setStatusNumber(1);
			linkCustomerFile.setSalesStatus("Booked");
			linkCustomerFile.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
			linkCustomerFile.setUpdatedOn(new Date());
			
		}
		linkCustomerFile.setIsNetworkRecord(true);
		// linkCustomerFile.setBookingAgentSequenceNumber(serviceOrderUGWW.getSequenceNumber());
		linkCustomerFile = customerFileManager.save(linkCustomerFile);
		
		// SO quotes check
		if(linkServiceOrder.getControlFlag().toString().trim().equals("Q")){
			linkServiceOrder.setControlFlag("C");
			linkServiceOrder.setStatus("NEW");
			linkServiceOrder.setStatusDate(new Date());
			linkServiceOrder.setStatusNumber(1);
			linkServiceOrder.setQuoteStatus("AC");
			linkServiceOrder.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
			linkServiceOrder.setUpdatedOn(new Date());
			linkServiceOrder.setQuoteAccept("A");
		}
		linkServiceOrder.setIsNetworkRecord(true);
		//linkServiceOrder.setBookingAgentShipNumber(serviceOrderUGWW.getShipNumber());
		linkServiceOrder = serviceOrderManager.save(linkServiceOrder);
		
		
		// Updating Tracking Status of  linked Corpid 
		
		if(agentType.equals("BA"))
			linkTrackingStatus.setBookingAgentExSO(serviceOrderUGWW.getShipNumber());
		
		if(agentType.equals("OA")){
			linkTrackingStatus.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
			linkTrackingStatus.setOriginAgentCode(trackingStatusUGWW.getOriginAgentCode());
			linkTrackingStatus.setOriginAgent(trackingStatusUGWW.getOriginAgent());
		}
		if(agentType.equals("DA")){
			linkTrackingStatus.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
			linkTrackingStatus.setDestinationAgentCode(trackingStatusUGWW.getDestinationAgentCode());
			linkTrackingStatus.setDestinationAgent(trackingStatusUGWW.getDestinationAgent());
		}
		if(agentType.equals("NwA")){
			linkTrackingStatus.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
			linkTrackingStatus.setNetworkPartnerCode(trackingStatusUGWW.getNetworkPartnerCode());
			linkTrackingStatus.setNetworkPartnerName(trackingStatusUGWW.getNetworkPartnerName());
			
		}
		if(agentType.equals("SOA")){
			linkTrackingStatus.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
			linkTrackingStatus.setOriginSubAgentCode(trackingStatusUGWW.getOriginSubAgentCode());
			linkTrackingStatus.setOriginSubAgent(trackingStatusUGWW.getOriginSubAgent());
			
		}
		if(agentType.equals("SDA")){
			linkTrackingStatus.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
			linkTrackingStatus.setDestinationSubAgentCode(trackingStatusUGWW.getDestinationSubAgentCode());
			linkTrackingStatus.setDestinationSubAgent(trackingStatusUGWW.getDestinationSubAgent());
			
		}
		linkTrackingStatus.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
		linkTrackingStatus.setUpdatedOn(new Date());
		linkTrackingStatus = trackingStatusManager.save(linkTrackingStatus);
		
		//Updating Tracking Status of UGWW 
		if(childAgentType.equals("BA")){
			trackingStatusUGWW.setBookingAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = serviceOrderUGWW.getBookingAgentCode();
		}
		
		if(childAgentType.equals("OA")){
			trackingStatusUGWW.setOriginAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = trackingStatusUGWW.getOriginAgentCode();
		}
		if(childAgentType.equals("DA")){
			trackingStatusUGWW.setDestinationAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = trackingStatusUGWW.getDestinationAgentCode();
		}
		if(childAgentType.equals("NwA")){
			trackingStatusUGWW.setNetworkAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = trackingStatusUGWW.getNetworkPartnerCode();
		}
		if(childAgentType.equals("SOA")){
			trackingStatusUGWW.setOriginSubAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = trackingStatusUGWW.getOriginSubAgentCode();
		}
		if(childAgentType.equals("SDA")){
			trackingStatusUGWW.setDestinationSubAgentExSO(linkServiceOrder.getShipNumber());
			childAgentCode = trackingStatusUGWW.getNetworkPartnerCode();
		}
		
		trackingStatusUGWW.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
		trackingStatusUGWW.setUpdatedOn(new Date());
		trackingStatusUGWW = trackingStatusManager.save(trackingStatusUGWW);
		
		
		// Updating UGWW orders
		customerFileUGWW.setIsNetworkRecord(true);
		customerFileUGWW.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
		customerFileUGWW.setUpdatedOn(new Date());
		customerFileUGWW = customerFileManager.save(customerFileUGWW);
		
		serviceOrderUGWW.setIsNetworkRecord(true);
		serviceOrderUGWW.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
		serviceOrderUGWW.setUpdatedOn(new Date());
		serviceOrderUGWW = serviceOrderManager.save(serviceOrderUGWW);
		
		// updating networkcontrol table
		toDoRuleManager.updateNetworkControlSO(sessionCorpID,linkServiceOrder.getShipNumber(),serviceOrderUGWW.getShipNumber(),"Done",childAgentCode);
				
		linkingSequenceNumber = toDoRuleManager.linkSequenceNumberList(sessionCorpID,bookingAgentcode,agentType,childAgentType,childAgentCode,serviceOrderUGWW,"");
		linkingSeqNumByName =   toDoRuleManager.linkSequenceNumberList(sessionCorpID,bookingAgentcode,agentType,childAgentType,childAgentCode,serviceOrderUGWW,"Name");
		hitFlag = "Y";
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken in merging So: "+timeTaken+"\n\n");
		return SUCCESS;
	}
	
/*	
	@SkipValidation
	public String integrationLogListMethod(){
		Long StartTime = System.currentTimeMillis();
		
		integrationErrorList = toDoRuleManager.getIntegrationLogInfoLog(); 
		
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to get log list: "+timeTaken+"\n\n");
		return SUCCESS;
	}*/
	
	
	@SkipValidation
	public String linkNetworkFileMethod() throws IllegalAccessException, InvocationTargetException {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long StartTime = System.currentTimeMillis();
		
		userName= getRequest().getRemoteUser();
		ServiceOrder serviceOrderUGWW = serviceOrderManager.getForOtherCorpid(soId);
		CustomerFile customerFileUGWW = serviceOrderUGWW.getCustomerFile();
		Billing billingUGWW = billingManager.getForOtherCorpid(soId);
		TrackingStatus trackingStatusUGWW = trackingStatusManager.getForOtherCorpid(soId);
		Miscellaneous miscellaneousUGWW = miscellaneousManager.getForOtherCorpid(soId);
		ServiceOrder linkServiceOrder = new ServiceOrder();
		String companyBillToCode="";
		String companyBillName="";
		if(serviceOrderUGWW.getCompanyDivision()!=null && serviceOrderUGWW.getCompanyDivision()!=""){		
		List companyCode=toDoRuleManager.getBillToCode(serviceOrderUGWW.getCompanyDivision(),serviceOrderUGWW.getCorpID());
		Iterator it=companyCode.iterator();
		while(it.hasNext()){
		Object [] row=(Object[])it.next(); 
		companyBillToCode=row[0].toString();
		companyBillName=row[1].toString();
		}
		}
		String forSection ="";
		if(agentType.equals("BA"))
			forSection="bookingAgentCode";
		else if(agentType.equals("OA"))
			forSection="origin";
		else if(agentType.equals("DA"))
			forSection="destination";
		else if(agentType.equals("NwA"))
			forSection="NetworkPartnerCode";
		else if(agentType.equals("SOA"))
			forSection="originSub";
		else if(agentType.equals("SDA"))
			forSection="destinationSub";
		// Creating CustomerFile 
		
		
		String networkCoordinator=customerFileManager.getNetworkCoordinator(sessionCorpID);

		List fieldToSync=customerFileManager.findFieldToSyncCreate("CustomerFile","","");
		
		CustomerFile customerFileNew= new CustomerFile();
		try{
			Iterator it1=fieldToSync.iterator();
			while(it1.hasNext()){
				String field=it1.next().toString(); 
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(customerFileNew,fieldTo.trim(),beanUtilsBean.getProperty(customerFileUGWW, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		
		customerFileNew.setId(null);
		try{
			if(customerFileNew.getMoveType()==null || (customerFileNew.getMoveType().toString().trim().equals(""))){
			if(customerFileNew.getControlFlag()==null ||(customerFileNew.getControlFlag().toString().trim().equals("")) || (customerFileNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
				customerFileNew.setMoveType("Quote");
			}else{
				customerFileNew.setMoveType("BookedMove");
			}
			}
			}catch(Exception e){
				
			}
		customerFileNew.setCorpID(sessionCorpID);
		maxSequenceNumber = customerFileManager.findMaximumSequenceNumber(sessionCorpID);
		if (maxSequenceNumber.get(0) == null) {
			customerFileNew.setSequenceNumber(sessionCorpID + "1000001");
		} else {
			autoSequenceNumber = Long.parseLong((maxSequenceNumber.get(0).toString()).replace(sessionCorpID, "")) + 1;
			seqNum = sessionCorpID + autoSequenceNumber.toString();
			customerFileNew.setSequenceNumber(seqNum);
		}
		customerFileNew.setIsNetworkRecord(true);
		customerFileNew.setBookingAgentSequenceNumber(customerFileUGWW.getSequenceNumber());
		customerFileNew.setCreatedBy("Networking:UGWW:"+userName);
		customerFileNew.setUpdatedBy("Networking:UGWW:"+userName);
		if(customerFileNew.getMoveType() == null || customerFileNew.getMoveType().toString().trim().equals("")){
			customerFileNew.setMoveType("BookedMove");
		}
		customerFileNew.setCreatedOn(new Date());
		customerFileNew.setUpdatedOn(new Date());
		customerFileNew.setIsNetworkGroup(false);
		customerFileNew.setContractType("");
		 /*change the logic of populating companydiv fron network partner to networkcontrol.
		  * for Bug 7895 - UGWW Company Division.
		  * changed on 2nd may 2013
		  * 
		*/
		/*if(childAgentType.equalsIgnoreCase("DA")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatusUGWW.getDestinationAgentCode(),"UGWW" ,sessionCorpID));
		}else if(childAgentType.equalsIgnoreCase("OA")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatusUGWW.getOriginAgentCode(),"UGWW" ,sessionCorpID));
		}else if(childAgentType.equalsIgnoreCase("BA")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(serviceOrderUGWW.getBookingAgentCode(),"UGWW" ,sessionCorpID));
		}else if(childAgentType.equalsIgnoreCase("NwA")){
			customerFileNew.setCompanyDivision(trackingStatusManager.getCompanyDivision(trackingStatusUGWW.getNetworkPartnerCode(),"UGWW" ,sessionCorpID));
		}else {
			customerFileNew.setCompanyDivision("");
		}*/
		customerFileNew.setCompanyDivision(agentCompDiv);
		
		/*customerFileNew.setBillToCode(customerFileNew.getBookingAgentCode());
		customerFileNew.setBillToName(customerFileNew.getBookingAgentName());*/
		customerFileNew.setBillToCode(companyBillToCode);
		customerFileNew.setBillToName(companyBillName);
		/*customerFileNew.setAccountCode(customerFileNew.getBookingAgentCode());#7303-comment no.4
		customerFileNew.setAccountName(customerFileNew.getBookingAgentName());*/
		customerFileNew.setAccountCode("");
		customerFileNew.setAccountName("");
		// changes for bug# 9448 
		if(coordSelected!=null && !coordSelected.equals("")){
			customerFileNew.setCoordinator(coordSelected);
		}else{
			if(coord.containsKey(getRequest().getRemoteUser().toUpperCase())){
				customerFileNew.setCoordinator(getRequest().getRemoteUser().toUpperCase());
			}else{
				String companyDivisionNetworkCoordinator=customerFileManager.getCompanyDivisionNetworkCoordinator(customerFileNew.getCompanyDivision(),sessionCorpID);
				if(companyDivisionNetworkCoordinator != null && (!(companyDivisionNetworkCoordinator.toString().trim().equals("")))){
					customerFileNew.setCoordinator(companyDivisionNetworkCoordinator);	
				}else{
					customerFileNew.setCoordinator(networkCoordinator);
			}
		}
		}
		customerFileNew.setBillPayMethod("CA"); 
		customerFileNew.setSystemDate(new Date());
		customerFileNew.setStatus("NEW");
		customerFileNew.setStatusDate(new Date());
		customerFileNew.setContract("");  
		// if(customerFileNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			 customerFileNew.setUgwIntId(customerFileNew.getSequenceNumber());
		//}
		 

		if(customerFileUGWW.getJob().equalsIgnoreCase("UGW")){
			if(customerFileUGWW.getOriginCountry()!=null && customerFileUGWW.getDestinationCountry() !=null && customerFileUGWW.getOriginCountry().equalsIgnoreCase(customerFileUGWW.getDestinationCountry()))
				customerFileNew.setJob("DOM");
			else
				customerFileNew.setJob("INT");
		}
		
		List personResponsiblesList = customerFileManager.findResponsiblePerson(customerFileNew.getJob(), customerFileNew.getCompanyDivision(), sessionCorpID);
		
		if(personResponsiblesList!=null && !personResponsiblesList.isEmpty() && personResponsiblesList.get(0)!=null && !personResponsiblesList.get(0).toString().equals("#####") ){
			String [] responsiblePersonArr = personResponsiblesList.get(0).toString().split("#");
			customerFileNew.setPersonBilling(responsiblePersonArr[0]);
			customerFileNew.setAuditor(responsiblePersonArr[1]);
			customerFileNew.setPersonPricing(responsiblePersonArr[2]);
			customerFileNew.setPersonPayable(responsiblePersonArr[3]);
			//customerFileNew.setCoordinator(responsiblePersonArr[4]);
			customerFileNew.setEstimator(responsiblePersonArr[5]);
		}
			
			
		//customerFileNew.setCoordinator(systemDefault.getNetworkCoordinator());
		
		customerFileNew=customerFileManager.save(customerFileNew);
		// CustomerFile Created 
		
		
		
		
		
		
		
		
		List fieldToSyncSO=customerFileManager.findFieldToSyncCreate("ServiceOrder","","");
		ServiceOrder serviceOrderNew= new ServiceOrder();
		Miscellaneous miscellaneousNew = new Miscellaneous();
		Billing billingNew = new Billing();
		TrackingStatus trackingStatusNew=new TrackingStatus();
		
	
				
		try{
			Iterator serviceOrderFields=fieldToSyncSO.iterator();
			while(serviceOrderFields.hasNext()){
				String field=serviceOrderFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
               		beanUtilsBean.setProperty(serviceOrderNew,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrderUGWW, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		serviceOrderNew.setId(null);
		try{
			if(serviceOrderNew.getMoveType()==null || (serviceOrderNew.getMoveType().toString().trim().equals(""))){
			if(serviceOrderNew.getControlFlag()==null ||(serviceOrderNew.getControlFlag().toString().trim().equals("")) || (serviceOrderNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
				serviceOrderNew.setMoveType("Quote");
			}else{
				serviceOrderNew.setMoveType("BookedMove");
			}
			}
			}catch(Exception e){
				
			}
		serviceOrderNew.setCorpID(customerFileNew.getCorpID());
		if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
				serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
		}
		maxShip = customerFileManager.findMaximumShipExternal(customerFileNew.getSequenceNumber(),customerFileNew.getCorpID());
		if (maxShip.get(0) == null) {
			ship = "01";

		} else {
			autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
			//System.out.println(autoShip);
			if ((autoShip.toString()).length() == 1) {
				ship = "0" + (autoShip.toString());
			} else {
				ship = autoShip.toString();
			}
	}
		
		shipnumber = customerFileNew.getSequenceNumber() + ship;
		serviceOrderNew.setShip(ship);
		serviceOrderNew.setShipNumber(shipnumber);
		serviceOrderNew.setSequenceNumber(customerFileNew.getSequenceNumber());
		serviceOrderNew.setCustomerFile(customerFileNew);
		/*serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
		serviceOrderNew.setBillToName(customerFileNew.getBillToName());*/
		serviceOrderNew.setBillToCode(companyBillToCode);
		serviceOrderNew.setBillToName(companyBillName);
		serviceOrderNew.setCreatedBy("Networking:UGWW:"+userName);
		serviceOrderNew.setUpdatedBy("Networking:UGWW:"+userName);
		serviceOrderNew.setCreatedOn(new Date());
		serviceOrderNew.setUpdatedOn(new Date());
		serviceOrderNew.setNetworkSO(true);
		//serviceOrderNew.setStatus("NEW");
		serviceOrderNew.setCustomerFileId(customerFileNew.getId());
		serviceOrderNew.setBookingAgentCode(serviceOrderUGWW.getBookingAgentCode());
		serviceOrderNew.setBookingAgentName(serviceOrderUGWW.getBookingAgentName());
		serviceOrderNew.setIsNetworkRecord(true);
		
		serviceOrderNew.setCompanyDivision(customerFileNew.getCompanyDivision());
		//serviceOrderNew.setJob(serviceOrder.getJob());
		if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
		if((serviceOrderUGWW.getRouting()!=null) && (serviceOrderUGWW.getRouting().equalsIgnoreCase("EXP"))){
			serviceOrderNew.setRouting("IMP");
		}
		else if((serviceOrderUGWW.getRouting()!=null) && (serviceOrderUGWW.getRouting().equalsIgnoreCase("IMP"))){
			serviceOrderNew.setRouting("EXP");
		}else{
		serviceOrderNew.setRouting(serviceOrderUGWW.getRouting());
		} 
		//serviceOrderNew.setStatus("NEW");
		serviceOrderNew.setStatusNumber(1);
		serviceOrderNew.setStatusDate(new Date());
		}
		 /*if(serviceOrderNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			 serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
		}*/
		if(serviceOrderUGWW.getMode().toString().trim().equalsIgnoreCase("FCL") || serviceOrderUGWW.getMode().toString().trim().equalsIgnoreCase("LCL"))
			serviceOrderNew.setMode("Sea");
		
		/*if(serviceOrderUGWW.getJob().equalsIgnoreCase("UGW")){
			if(serviceOrderUGWW.getOriginCountry().equalsIgnoreCase(serviceOrderUGWW.getDestinationCountry()))
				serviceOrderNew.setJob("DOM");
			else
				serviceOrderNew.setJob("INT");
		}*/
		serviceOrderNew.setJob(customerFileNew.getJob());
		serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
		serviceOrderNew.setEstimator(customerFileNew.getEstimator());
		serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
		serviceOrderNew.setBookingAgentShipNumber(serviceOrderUGWW.getShipNumber());
		
		
		serviceOrderNew=serviceOrderManager.save(serviceOrderNew);
		
		
		
		
		 //Miscellaneous miscellaneousNew = new Miscellaneous();
		 List miscellaneousFieldToSync=customerFileManager.findFieldToSyncCreate("Miscellaneous","","");
			Iterator miscellaneousFields=miscellaneousFieldToSync.iterator();
			while(miscellaneousFields.hasNext()){
				String field=miscellaneousFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(miscellaneousNew,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneousUGWW, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
		 
		 miscellaneousNew.setId(serviceOrderNew.getId());
		 miscellaneousNew.setCorpID(serviceOrderNew.getCorpID());
		 miscellaneousNew.setShipNumber(serviceOrderNew.getShipNumber());
		 miscellaneousNew.setShip(serviceOrderNew.getShip());
		 miscellaneousNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		 miscellaneousNew.setCreatedBy("Networking:UGWW:"+userName);
		 miscellaneousNew.setUpdatedBy("Networking:UGWW:"+userName);
		 miscellaneousNew.setCreatedOn(new Date());
		 miscellaneousNew.setUpdatedOn(new Date());
		 try{
			 String  unitData =customerFileManager.getNetworkUnit(serviceOrderNew.getCorpID());
			 String[] splitField=unitData.split("~");
			 String WeightUnit="";
			 String VolumeUnit="";
			 if(splitField[0]!=null && (!(splitField[0].trim().equalsIgnoreCase("No")))){
			 WeightUnit=	 splitField[0].trim();
			 }
			 if(splitField[1]!=null && (!(splitField[1].trim().equalsIgnoreCase("No")))){
			  VolumeUnit=splitField[1].trim();
			 }
			 if(WeightUnit!=null && (!(WeightUnit.equals("")))){
				 miscellaneousNew.setUnit1(WeightUnit)	; 
			 }
			 if(VolumeUnit!=null && (!(VolumeUnit.equals("")))){
				 miscellaneousNew.setUnit2(VolumeUnit)	; 
			 }
			 }catch(Exception e){
				 e.printStackTrace();
			 }
         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
         
        // if(miscellaneousNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
        	 miscellaneousNew.setUgwIntId(miscellaneousNew.getId().toString());
		//}
         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
         
		//TrackingStatus trackingStatusNew=new TrackingStatus();
		
		List trackingStatusFieldToSync=customerFileManager.findFieldToSyncCreate("TrackingStatus","","");
		Iterator trackingStatusFields=trackingStatusFieldToSync.iterator();
		while(trackingStatusFields.hasNext()){
			String field=trackingStatusFields.next().toString();
			String[] splitField=field.split("~");
			String fieldFrom=splitField[0].trim();
			String fieldTo=splitField[1].trim();
			PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
			try{
           		beanUtilsBean.setProperty(trackingStatusNew,fieldTo,beanUtilsBean.getProperty(trackingStatusUGWW, fieldFrom));
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
		}
		
		trackingStatusNew.setId(serviceOrderNew.getId());
		trackingStatusNew.setContractType("");
		trackingStatusNew.setCorpID(serviceOrderNew.getCorpID());
		trackingStatusNew.setServiceOrder(serviceOrderNew);
		trackingStatusNew.setMiscellaneous(miscellaneousNew);
		trackingStatusNew.setShipNumber(serviceOrderNew.getShipNumber());
		trackingStatusNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		trackingStatusNew.setSurveyTimeFrom(customerFileNew.getSurveyTime());
		trackingStatusNew.setSurveyTimeTo(customerFileNew.getSurveyTime2());
		trackingStatusNew.setSurveyDate(customerFileNew.getSurvey());
		//trackingStatusNew.setBookingAgentContact(serviceOrderUGWW.getCoordinator());		
		
		if(forSection.equalsIgnoreCase("destination")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("destinationSub")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("origin")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("originSub")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
			trackingStatusUGWW.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
		}else if(forSection.equalsIgnoreCase("bookingAgentCode")){
			trackingStatusNew.setSoNetworkGroup(false);
			trackingStatusNew.setAccNetworkGroup(false);
			trackingStatusNew.setUtsiNetworkGroup(false);
			trackingStatusNew.setAgentNetworkGroup(false);
			trackingStatusNew.setBookingAgentExSO(serviceOrderUGWW.getShipNumber()); 
			trackingStatusUGWW.setBookingAgentExSO(serviceOrderUGWW.getShipNumber());
			//trackingStatusNew.setBookingAgentContact(trackingStatusUGWW.getBookingAgentContact());
			/*try{
				trackingStatusNew.setBookingAgentEmail(customerFileManager.findCoordEmailAddress(trackingStatusUGWW.getBookingAgentContact()).get(0).toString());
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				} */
		}
		trackingStatusNew.setCreatedBy("Networking:UGWW:"+userName);
		trackingStatusNew.setUpdatedBy("Networking:UGWW:"+userName);
		trackingStatusNew.setCreatedOn(new Date());
		trackingStatusNew.setUpdatedOn(new Date());
		try{
			 if(trackingStatusNew.getForwarderCode()!=null && (!(trackingStatusNew.getForwarderCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getForwarderCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setForwarderCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 try{
			 if(trackingStatusNew.getBrokerCode()!=null && (!(trackingStatusNew.getBrokerCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getBrokerCode());
				if(checkVendorCode){
					
				}else{
					trackingStatusNew.setBrokerCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		
		trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
		trackingStatusUGWW = trackingStatusManager.save(trackingStatusUGWW);
		
		//if(trackingStatusNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			trackingStatusNew.setUgwIntId(trackingStatusNew.getId().toString());
		//}
		trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
		
		 try{
		 //Billing billingNew = new Billing(); 
	/*	 if(forSection.equalsIgnoreCase("NetworkPartnerCode") || forSection.equalsIgnoreCase("bookingAgentCode")){ 
		        List billingFieldToSync=new ArrayList();
		       
				Iterator billingFields=billingFieldToSync.iterator();
				while(billingFields.hasNext()){
					String field=billingFields.next().toString();
					String[] splitField=field.split("~");
    				String fieldFrom=splitField[0];
    				String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(billingNew,fieldTo.trim(),beanUtilsBean.getProperty(billingUGWW, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
				}
				
				
		 }*/
		// BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
		// beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
		// beanUtilsBean2.copyProperties(billingNew, billing);
		 //else{
		  
		 
	        List billingFieldToSync=new ArrayList();
	        /*if(billingCMMContractType){
	        	billingFieldToSync=customerFileManager.findBillingFieldToSyncCreate("Billing","CMM");
	        }
	        if(billingDMMContractType){*/
	        	billingFieldToSync=customerFileManager.findBillingFieldToSyncCreate("Billing","","");
	        //}
			Iterator billingFields=billingFieldToSync.iterator();
			while(billingFields.hasNext()){
				String field=billingFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0];
				String fieldTo=splitField[1];
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
				beanUtilsBean.setProperty(billingNew,fieldTo.trim(),beanUtilsBean.getProperty(billingUGWW, fieldFrom.trim()));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
			/*if(billingDMMContractType){
			if(forSection.equalsIgnoreCase("bookingAgentCode")){
				billingNew.setBillToCode(customerFileNew.getBillToCode());
				billingNew.setBillToName(customerFileNew.getBillToName());		
			}
			else{
			billingNew.setBillToCode(customerFile.getAccountCode());
			billingNew.setBillToName(customerFile.getAccountName());
				}
			}*/
		 /*billingNew.setBillToCode(customerFileNew.getBillToCode());
		 billingNew.setBillToName(customerFileNew.getBillToName());*/
		 billingNew.setBillToCode(companyBillToCode);
		 billingNew.setBillToName(companyBillName);
		 billingNew.setContract(customerFileNew.getContract());
		 billingNew.setBillTo1Point(customerFileNew.getBillPayMethod());
		 billingNew.setBillingInstructionCodeWithDesc("CON : As per Contract");
		 billingNew.setBillingInstruction("As per Contract");
		 billingNew.setSpecialInstruction("As per Quote");
		// }
		 billingNew.setId(serviceOrderNew.getId());
		 billingNew.setCorpID(serviceOrderNew.getCorpID());
		 billingNew.setShipNumber(serviceOrderNew.getShipNumber());
		 billingNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
		 billingNew.setServiceOrder(serviceOrderNew);
		 billingNew.setCreatedBy("Networking:UGWW:"+userName);
		 billingNew.setUpdatedBy("Networking:UGWW:"+userName);
		 billingNew.setSystemDate(new Date());
		 billingNew.setCreatedOn(new Date());
		 billingNew.setUpdatedOn(new Date());
		 billingNew.setAuditor(customerFileNew.getAuditor());
		 billingNew.setPersonBilling(customerFileNew.getPersonBilling());
		 billingNew.setPersonPayable(customerFileNew.getPersonPayable());
		 billingNew.setPersonPricing(customerFileNew.getPersonPricing());
		 try{
			 if(billingNew.getVendorCode()!=null && (!(billingNew.getVendorCode().equalsIgnoreCase("")))){
				boolean checkVendorCode= billingManager.findVendorCode(billingNew.getCorpID(),billingNew.getVendorCode());
				if(checkVendorCode){
					
				}else{
					billingNew.setVendorCode("");	
				}
			 }
			 }catch(Exception e){
				e.printStackTrace(); 
			 }
		 billingNew=billingManager.save(billingNew);
		// if(billingNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
			 billingNew.setUgwIntId(billingNew.getId().toString());
		//	}
		 billingNew=billingManager.save(billingNew);
		 }catch(Exception ex){
			 logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		 }
		try{ 
		 createContainerRecords(serviceOrderUGWW, serviceOrderNew); 
		 createPieceCountsRecords(serviceOrderUGWW, serviceOrderNew); 
		 createVehicleRecords(serviceOrderUGWW, serviceOrderNew); 
		 createRoutingRecords(serviceOrderUGWW, serviceOrderNew); 
		 trackingStatusAction.createConsigneeInstruction(serviceOrderUGWW, serviceOrderNew);
		 }catch(Exception ex){
			logger.warn("Exception in craeting Container,routing etc Exception: "+currentdate+" ## : "+ex.toString());
		}
		 customerFileManager.updateIsNetworkFlag(customerFileUGWW.getId(), customerFileUGWW.getCorpID(), serviceOrderUGWW.getId(),customerFileNew.getContractType(),customerFileNew.getIsNetworkGroup());
			
		
	} catch(Exception e){
		logger.warn("\n\n\nOoooppppppsssssssssssss Sorry!!!! got an Exception in creating ServiceOrder.\n And  Reported  Exception is "+e);
	}
		
	// Updating UGWW orders
		customerFileUGWW.setIsNetworkRecord(true);
		customerFileUGWW = customerFileManager.save(customerFileUGWW);
		serviceOrderUGWW.setIsNetworkRecord(true);
		serviceOrderUGWW.setNetworkSO(true);
		serviceOrderUGWW = serviceOrderManager.save(serviceOrderUGWW);
		/**
		 * Updated Regarding #9595 31 July 14
		 */
		String bookingAgentContactForSo="";
		String bookingAgentEmailForSo="";
		try{
			if(serviceOrderNew.getCoordinator()!=null && !serviceOrderNew.getCoordinator().equals("")){
				bookingAgentContactForSo=toDoRuleManager.getUserLastFirstName(serviceOrderNew.getCoordinator());
				bookingAgentEmailForSo=customerFileManager.findCoordEmailAddress(serviceOrderNew.getCoordinator()).get(0).toString();
			}
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		
		if(childAgentType.equals("BA")){
			trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());			
			trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
			trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());					
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);					
				
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}	 
		if(childAgentType.equals("OA")){
			trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
			trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
			 
		if(childAgentType.equals("DA")){
			trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
			trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);

			
			if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
			 
		if(childAgentType.equals("NwA")){
			trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
			trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
		if(childAgentType.equals("SOA")){
			trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
			trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);

			if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getNetworkAgentExSO())){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);

			}
		}
		if(childAgentType.equals("SDA")){
			trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
			trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
			trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getNetworkAgentExSO())){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
		
		
		trackingStatusUGWW = trackingStatusManager.save(trackingStatusUGWW);
		trackingStatusNew = trackingStatusManager.save(trackingStatusNew);
		
		
		toDoRuleManager.updateNetworkControlSO(sessionCorpID,serviceOrderNew.getShipNumber(),serviceOrderUGWW.getShipNumber(),"Done",childAgentCode);    
		
		try{
			if(serviceOrderNew.getIsNetworkRecord()){
				List linkedShipNumber= trackingStatusAction.findLinkerShipNumber(serviceOrderNew); 
				List<Object> miscellaneousRecords=trackingStatusAction.findMiscellaneousRecords(linkedShipNumber,serviceOrderNew); 
				trackingStatusAction.synchornizeMiscellaneous(miscellaneousRecords,miscellaneousNew,trackingStatusNew);
				List<Object> trackingStatusRecords=trackingStatusAction.findTrackingStatusRecords(linkedShipNumber,serviceOrderNew);
				trackingStatusAction.synchornizeTrackingStatus(trackingStatusRecords,trackingStatusNew,serviceOrderNew);
				List<Object> serviceOrderRecords=trackingStatusAction.findServiceOrderRecords(linkedShipNumber,serviceOrderNew);
				trackingStatusAction.synchornizeServiceOrder(serviceOrderRecords,serviceOrderNew,trackingStatusNew,customerFileNew);
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
		
		chkFlag="Accept";
		hitFlag = "Y";
		
		
		// creating SO dash board entry
		//createSODashBoard(customerFileNew, serviceOrderNew, miscellaneousNew,billingNew, trackingStatusNew);
		successMessage = "SO# "+serviceOrderUGWW.getShipNumber()+" has been accepted and SO# "+serviceOrderNew.getShipNumber()+" has been created successfully.";

		insertIntoTracker(serviceOrderUGWW.getShipNumber(),"Accept");
		
	
		todoList();
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to Accept the service order OF ugww : "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

/**
 * Code for New added SO dash board 
 * Papulation data from SO TS Misc Claim and billing in So dash Board
 * # 9382 Requrement.
 * @param customerFileNew
 * @param serviceOrderNew
 * @param miscellaneousNew
 * @param billingNew
 * @param trackingStatusNew
 *//*
	
	
private void createSODashBoard(CustomerFile customerFileNew,
		ServiceOrder serviceOrderNew, Miscellaneous miscellaneousNew,
		Billing billingNew, TrackingStatus trackingStatusNew) {
	ServiceOrderDashboard dashboard=serviceOrderDashboardManager.getByServiceOrderId(serviceOrderNew.getId());
	if(dashboard==null){
		dashboard = new ServiceOrderDashboard();
	}
	// Copying Form CF
	if(customerFileNew!=null && customerFileNew.getActualSurveyDate()!=null){					
		Date dt=new Date();	
		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
		try{
			 format=new SimpleDateFormat("yyyy-MM-dd");
			String dt1=format.format(customerFileNew.getActualSurveyDate());
		     dashboard.setSurvey(dt1);
			}catch(Exception e){
				e.printStackTrace();
				logger.warn("Error in setting dashboard Survey from CF ActualSurveyDate");
			}
		
		
	} 
	if(customerFileNew!=null && customerFileNew.getSurvey()!=null){
		Date dt=new Date();
		try{
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
			String dt1=format.format(customerFileNew.getSurvey());
		     dashboard.setSurveyDt(dt1);
			}catch(Exception e){
				e.printStackTrace();
				logger.warn("Error in setting dashboard SurveyDt from CF Survey");
			}
		
	}
	
	// copying form SO
	
	dashboard.setServiceOrderId(serviceOrderNew.getId());
	dashboard.setShipNumber(serviceOrderNew.getShipNumber());
	dashboard.setCorpID(serviceOrderNew.getCorpID());
	String role=dashboard.getRole();
	if(serviceOrderNew.getBookingAgentCode()!=null && !(serviceOrderNew.getBookingAgentCode().equals(""))){
		if(role!=null && !role.equals("")){
			if(role.indexOf("BA")==-1){
			role=role+"/BA";
			}
		}else{
		role="BA";
		}
	}
	dashboard.setRole(role);
	
	
	// copying from  miscellaneous
	
	if(systemDefault!=null && systemDefault.getWeightUnit().equals("Kgs") ){
		dashboard.setEstweight(miscellaneousNew.getEstimatedNetWeightKilo());
		dashboard.setActualweight(miscellaneousNew.getActualNetWeightKilo());
		dashboard.setEstvolume(miscellaneousNew.getNetEstimateCubicMtr());
		dashboard.setActVolume(miscellaneousNew.getNetActualCubicMtr());

	}else {
		dashboard.setEstweight(miscellaneousNew.getEstimatedNetWeight());
		dashboard.setActualweight(miscellaneousNew.getActualNetWeight());
		dashboard.setEstvolume(miscellaneousNew.getNetEstimateCubicFeet());
		dashboard.setActVolume(miscellaneousNew.getNetActualCubicFeet());
	}
		dashboard.setDriver(miscellaneousNew.getDriverName());
		dashboard.setTruck(miscellaneousNew.getCarrier());
	role=dashboard.getRole();
	if(role.indexOf("HA")==-1){	
		if(miscellaneousNew.getHaulingAgentCode()!=null && !(miscellaneousNew.getHaulingAgentCode().equals(""))){
			if(!role.equals("")){
				role=role+"/HA";	
			}else{
			role="HA";
			}
		}
	}
	dashboard.setRole(role);
	
	
	// copying form tracking Status
		if(trackingStatusNew.getLoadA()!=null){
				dashboard.setLdpkticket("Actualized");
				Date dt=new Date();	
				SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
				try{
					format=new SimpleDateFormat("yyyy-MM-dd");
					String dt1=format.format(trackingStatusNew.getLoadA());
				     dashboard.setLoading(dt1);
					}catch(Exception e){
						e.printStackTrace();
					}
			}
			if(trackingStatusNew.getBeginLoad()!=null){
				dashboard.setLdpkticket("Target");
				Date dt=new Date();	
				SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
				try{
					format=new SimpleDateFormat("yyyy-MM-dd");
					String dt1=format.format(trackingStatusNew.getBeginLoad());
				     dashboard.setLoadingDt(dt1);
					}catch(Exception e){
						e.printStackTrace();
					}
			}
			
			if(trackingStatusNew.getDeliveryA()!=null){
				dashboard.setDeliveryticket("Actualized");
				Date dt=new Date();	
				SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
				try{
					 format=new SimpleDateFormat("yyyy-MM-dd");
					String dt1=format.format(trackingStatusNew.getDeliveryA());
				     dashboard.setDelivery(dt1);
					}catch(Exception e){
						e.printStackTrace();
					}
			}
			if(trackingStatusNew.getDeliveryShipper()!=null){
				dashboard.setDeliveryticket("Target");
				Date dt=new Date();	
				SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
				try{
					 format=new SimpleDateFormat("yyyy-MM-dd");
					String dt1=format.format(trackingStatusNew.getDeliveryShipper());
				     dashboard.setDeliveryDt(dt1);
					}catch(Exception e){
						e.printStackTrace();
					} 
			}	
			if(trackingStatusNew.getClearCustom()!=null){						
				Date dt=new Date();	
				SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
				try{
					 format=new SimpleDateFormat("yyyy-MM-dd");
					String dt1=format.format(trackingStatusNew.getClearCustom());
				     dashboard.setCustomDate(dt1);
					}catch(Exception e){
						e.printStackTrace();
					} 
			}	
			if(trackingStatusNew.getClearCustomTA()!=null){			
				  dashboard.setCustomFlag(trackingStatusNew.getClearCustomTA());
					
			}else{
				 dashboard.setCustomFlag("");
			}
			role = dashboard.getRole();
			if(role==null){
				role="";
			}
			if(trackingStatusNew!=null){						
				if(role.indexOf("OA")==-1){	
					if(trackingStatusNew!=null){						
						if(trackingStatusNew.getOriginAgent()!=null && !(trackingStatusNew.getOriginAgent().equals(""))){
							if(role!=null && !role.equals("")){
								role=role+"/OA";
							}else{
								role="OA";
							}
						}
					}
				}
			if(role.indexOf("DA")==-1){	
					if(trackingStatusNew.getDestinationAgent()!=null && !(trackingStatusNew.getDestinationAgent().equals(""))){
						if(!role.equals("")){
							role=role+"/DA";	
						}else{
						role="DA";
						}
					}
			}
			if(role.indexOf("SO")==-1){	
					if(trackingStatusNew.getOriginSubAgent()!=null && !(trackingStatusNew.getOriginSubAgent().equals(""))){
						if(!role.equals("")){
							role=role+"/SO";	
						}else{
							role="SO";
						}
					}
				}
			if(role.indexOf("SD")==-1){	
					if(trackingStatusNew.getDestinationSubAgent()!=null && !(trackingStatusNew.getDestinationSubAgent().equals(""))){
						if(!role.equals("")){
							role=role+"/SD";	
						}else{
							role="SD";
						}
					}
				}
			if(role.indexOf("BR")==-1){	
				if(trackingStatusNew.getBrokerCode()!=null && !(trackingStatusNew.getBrokerCode().equals(""))){
					if(!role.equals("")){
						role=role+"/BR";	
					}else{
						role="BR";
					}
				}
			}
			if(role.indexOf("FR")==-1){	
				if(trackingStatusNew.getForwarderCode()!=null && !(trackingStatusNew.getForwarderCode().equals(""))){
					if(!role.equals("")){
						role=role+"/FR";	
					}else{
						role="FR";
					}
				}
			}
						
			if(role.indexOf("NA")==-1){	
				if(trackingStatusNew.getNetworkPartnerCode()!=null && !(trackingStatusNew.getNetworkPartnerCode().equals(""))){
					if(!role.equals("")){
						role=role+"/NA";	
					}else{
						role="NA";
					}
				}
			}
						
			}
		dashboard.setRole(role);
		
	// copying from billing
		if( billingNew.getAuditComplete()!=null){
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd");
			String auditDt=format.format(billingNew.getAuditComplete());				
		    dashboard.setQc(auditDt);
		}
		dashboard = serviceOrderDashboardManager.save(dashboard);
	
}

 * End of So dash board Enhance code
 
*/
	//dileep	Billing serviceOrderNew;
	public void  insertIntoTracker(String shipNumber,String action){
			
		UgwwActionTracker ugwwActionTracker = new UgwwActionTracker();
		
			ugwwActionTracker.setSonumber(shipNumber);
			ugwwActionTracker.setActiontime(new Date());
			ugwwActionTracker.setCorpid(sessionCorpID);
			ugwwActionTracker.setUsername(getRequest().getRemoteUser());
			ugwwActionTracker.setAction(action);
			ugwwActionTracker.setCreatedBy(getRequest().getRemoteUser());
			ugwwActionTracker.setCreatedOn(new Date());
			ugwwActionTracker.setUpdatedOn(new Date());
			ugwwActionTracker.setUpdatedBy(getRequest().getRemoteUser());
			ugwwActionTrackerManager.save(ugwwActionTracker);
		
			
		}
	
	private String mergingSeqNum;
	
	@SkipValidation
	public String createNewLinkSOMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Long StartTime = System.currentTimeMillis();
		userName= getRequest().getRemoteUser();
		ServiceOrder serviceOrderUGWW = serviceOrderManager.getForOtherCorpid(soUGWWID);
		CustomerFile customerFileUGWW = serviceOrderUGWW.getCustomerFile();
		Billing billingUGWW = billingManager.getForOtherCorpid(soUGWWID);
		TrackingStatus trackingStatusUGWW = trackingStatusManager.getForOtherCorpid(soUGWWID);
		Miscellaneous miscellaneousUGWW = miscellaneousManager.getForOtherCorpid(soUGWWID);
		List customerFileList = customerFileManager.findSequenceNumber(mergingSeqNum);
		if(customerFileList!=null && !customerFileList.isEmpty() && customerFileList.get(0)!=null){
			CustomerFile customerFileNew = (CustomerFile) customerFileList.get(0);
			ServiceOrder linkServiceOrder = new ServiceOrder();
			String companyBillToCode="";
			String companyBillName="";
			if(serviceOrderUGWW.getCompanyDivision()!=null && serviceOrderUGWW.getCompanyDivision()!=""){		
			List companyCode=toDoRuleManager.getBillToCode(serviceOrderUGWW.getCompanyDivision(),serviceOrderUGWW.getCorpID());
			Iterator it=companyCode.iterator();
			while(it.hasNext()){
			Object [] row=(Object[])it.next(); 
			companyBillToCode=row[0].toString();
			companyBillName=row[1].toString();
			}
			}
			String forSection ="";
			if(agentType.equals("BA"))
				forSection="bookingAgentCode";
			else if(agentType.equals("OA"))
				forSection="origin";
			else if(agentType.equals("DA"))
				forSection="destination";
			else if(agentType.equals("NwA"))
				forSection="NetworkPartnerCode";
			else if(agentType.equals("SOA"))
				forSection="originSub";
			else if(agentType.equals("SDA"))
				forSection="destinationSub";
			List fieldToSyncSO=customerFileManager.findFieldToSyncCreate("ServiceOrder","","");
			
			ServiceOrder serviceOrderNew= new ServiceOrder();
			Miscellaneous miscellaneousNew = new Miscellaneous();
			Billing billingNew = new Billing();
			TrackingStatus trackingStatusNew=new TrackingStatus();
					
			try{
				Iterator serviceOrderFields=fieldToSyncSO.iterator();
				while(serviceOrderFields.hasNext()){
					String field=serviceOrderFields.next().toString();
					String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
	               		beanUtilsBean.setProperty(serviceOrderNew,fieldTo.trim(),beanUtilsBean.getProperty(serviceOrderUGWW, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
				}
			serviceOrderNew.setId(null);
			try{
				if(serviceOrderNew.getMoveType()==null || (serviceOrderNew.getMoveType().toString().trim().equals(""))){
				if(serviceOrderNew.getControlFlag()==null ||(serviceOrderNew.getControlFlag().toString().trim().equals("")) || (serviceOrderNew.getControlFlag().trim().equalsIgnoreCase("Q")  )){
					serviceOrderNew.setMoveType("Quote");
				}else{
					serviceOrderNew.setMoveType("BookedMove");
				}
				}
				}catch(Exception e){
					
				}
			serviceOrderNew.setCorpID(customerFileNew.getCorpID());
			if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
				if(coordSelected!=null && !coordSelected.equals("")){
					serviceOrderNew.setCoordinator(coordSelected);
				}else{
					serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
				}
			}
			maxShip = customerFileManager.findMaximumShipExternal(customerFileNew.getSequenceNumber(),customerFileNew.getCorpID());
			if (maxShip.get(0) == null) {
				ship = "01";
	
			} else {
				autoShip = Long.parseLong((maxShip).get(0).toString()) + 1;
				//System.out.println(autoShip);
				if ((autoShip.toString()).length() == 1) {
					ship = "0" + (autoShip.toString());
				} else {
					ship = autoShip.toString();
				}
		}
			
			shipnumber = customerFileNew.getSequenceNumber() + ship;
			serviceOrderNew.setShip(ship);
			serviceOrderNew.setShipNumber(shipnumber);
			serviceOrderNew.setSequenceNumber(customerFileNew.getSequenceNumber());
			serviceOrderNew.setCustomerFile(customerFileNew);
			/*serviceOrderNew.setBillToCode(customerFileNew.getBillToCode());
			serviceOrderNew.setBillToName(customerFileNew.getBillToName());*/
			serviceOrderNew.setBillToCode(companyBillToCode);
			serviceOrderNew.setBillToName(companyBillName);
			serviceOrderNew.setCreatedBy("Networking:UGWW:"+userName);
			serviceOrderNew.setUpdatedBy("Networking:UGWW:"+userName);
			serviceOrderNew.setCreatedOn(new Date());
			serviceOrderNew.setUpdatedOn(new Date());
			serviceOrderNew.setNetworkSO(true);
			//serviceOrderNew.setStatus("NEW");
			serviceOrderNew.setCustomerFileId(customerFileNew.getId());
			serviceOrderNew.setBookingAgentCode(serviceOrderUGWW.getBookingAgentCode());
			serviceOrderNew.setBookingAgentName(serviceOrderUGWW.getBookingAgentName());
			serviceOrderNew.setIsNetworkRecord(true);
			
			serviceOrderNew.setCompanyDivision(customerFileNew.getCompanyDivision());
			//serviceOrderNew.setJob(serviceOrder.getJob());
			if((!(forSection.equalsIgnoreCase("bookingAgentCode"))) && (!(forSection.equalsIgnoreCase("networkPartnerCode")))){
			if((serviceOrderUGWW.getRouting()!=null) && (serviceOrderUGWW.getRouting().equalsIgnoreCase("EXP"))){
				serviceOrderNew.setRouting("IMP");
			}
			else if((serviceOrderUGWW.getRouting()!=null) && (serviceOrderUGWW.getRouting().equalsIgnoreCase("IMP"))){
				serviceOrderNew.setRouting("EXP");
			}else{
			serviceOrderNew.setRouting(serviceOrderUGWW.getRouting());
			} 
			//serviceOrderNew.setStatus("NEW");
			serviceOrderNew.setStatusNumber(1);
			serviceOrderNew.setStatusDate(new Date());
			}
			 /*if(serviceOrderNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				 serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
			}*/
			if(serviceOrderUGWW.getMode().toString().trim().equalsIgnoreCase("FCL") || serviceOrderUGWW.getMode().toString().trim().equalsIgnoreCase("LCL"))
				serviceOrderNew.setMode("Sea");
			
			/*if(serviceOrderUGWW.getJob().equalsIgnoreCase("UGW")){
			if(serviceOrderUGWW.getOriginCountry().equalsIgnoreCase(serviceOrderUGWW.getDestinationCountry()))
				serviceOrderNew.setJob("DOM");
			else
				serviceOrderNew.setJob("INT");
		}*/
		    serviceOrderNew.setJob(customerFileNew.getJob());
		    if(coordSelected!=null && !coordSelected.equals("")){
				serviceOrderNew.setCoordinator(coordSelected);
			}else{
				serviceOrderNew.setCoordinator(customerFileNew.getCoordinator());
			}
			serviceOrderNew.setEstimator(customerFileNew.getEstimator());
			serviceOrderNew.setBookingAgentShipNumber(serviceOrderUGWW.getShipNumber());
			
			
			serviceOrderNew=serviceOrderManager.save(serviceOrderNew);
			serviceOrderNew.setUgwIntId(serviceOrderNew.getShipNumber());
			
			
			List miscellaneousFieldToSync=customerFileManager.findFieldToSyncCreate("Miscellaneous","","");
				Iterator miscellaneousFields=miscellaneousFieldToSync.iterator();
				while(miscellaneousFields.hasNext()){
					String field=miscellaneousFields.next().toString();
					String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(miscellaneousNew,fieldTo.trim(),beanUtilsBean.getProperty(miscellaneousUGWW, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
				}
			 
			 miscellaneousNew.setId(serviceOrderNew.getId());
			 miscellaneousNew.setCorpID(serviceOrderNew.getCorpID());
			 miscellaneousNew.setShipNumber(serviceOrderNew.getShipNumber());
			 miscellaneousNew.setShip(serviceOrderNew.getShip());
			 miscellaneousNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			 miscellaneousNew.setCreatedBy("Networking:UGWW:"+userName);
			 miscellaneousNew.setUpdatedBy("Networking:UGWW:"+userName);
			 miscellaneousNew.setCreatedOn(new Date());
			 miscellaneousNew.setUpdatedOn(new Date());
			 try{
				 String  unitData =customerFileManager.getNetworkUnit(serviceOrderNew.getCorpID());
				 String[] splitField=unitData.split("~");
				 String WeightUnit="";
				 String VolumeUnit="";
				 if(splitField[0]!=null && (!(splitField[0].trim().equalsIgnoreCase("No")))){
				 WeightUnit=	 splitField[0].trim();
				 }
				 if(splitField[1]!=null && (!(splitField[1].trim().equalsIgnoreCase("No")))){
				  VolumeUnit=splitField[1].trim();
				 }
				 if(WeightUnit!=null && (!(WeightUnit.equals("")))){
					 miscellaneousNew.setUnit1(WeightUnit)	; 
				 }
				 if(VolumeUnit!=null && (!(VolumeUnit.equals("")))){
					 miscellaneousNew.setUnit2(VolumeUnit)	; 
				 }
				 }catch(Exception e){
					 e.printStackTrace();
				 }
	         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
	         
	        // if(miscellaneousNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
	        	 miscellaneousNew.setUgwIntId(miscellaneousNew.getId().toString());
			//}
	         miscellaneousNew=miscellaneousManager.save(miscellaneousNew);
	         
			
			List trackingStatusFieldToSync=customerFileManager.findFieldToSyncCreate("TrackingStatus","","");
			Iterator trackingStatusFields=trackingStatusFieldToSync.iterator();
			while(trackingStatusFields.hasNext()){
				String field=trackingStatusFields.next().toString();
				String[] splitField=field.split("~");
				String fieldFrom=splitField[0].trim();
				String fieldTo=splitField[1].trim();
				PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
				try{
	           		beanUtilsBean.setProperty(trackingStatusNew,fieldTo,beanUtilsBean.getProperty(trackingStatusUGWW, fieldFrom));
				}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
				}
			}
			
			trackingStatusNew.setId(serviceOrderNew.getId());
			trackingStatusNew.setContractType("");
			trackingStatusNew.setCorpID(serviceOrderNew.getCorpID());
			trackingStatusNew.setServiceOrder(serviceOrderNew);
			trackingStatusNew.setMiscellaneous(miscellaneousNew);
			trackingStatusNew.setShipNumber(serviceOrderNew.getShipNumber());
			trackingStatusNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			trackingStatusNew.setSurveyTimeFrom(customerFileNew.getSurveyTime());
			trackingStatusNew.setSurveyTimeTo(customerFileNew.getSurveyTime2());
			trackingStatusNew.setSurveyDate(customerFileNew.getSurvey());
			//trackingStatusNew.setBookingAgentContact(serviceOrderUGWW.getCoordinator());
			/*try{
				if(serviceOrderNew.getCoordinator()!=null && !serviceOrderNew.getCoordinator().equals("")){
					trackingStatusNew.setBookingAgentContact(toDoRuleManager.getUserLastFirstName(serviceOrderNew.getCoordinator()));
					trackingStatusNew.setBookingAgentEmail(customerFileManager.findCoordEmailAddress(serviceOrderNew.getCoordinator()).get(0).toString());
				}
			}catch(Exception ex){
					logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
			*/	
			
			if(forSection.equalsIgnoreCase("destination")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderUGWW.getShipNumber());
			}else if(forSection.equalsIgnoreCase("destinationSub")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderUGWW.getShipNumber());
			}else if(forSection.equalsIgnoreCase("origin")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderUGWW.getShipNumber());
			}else if(forSection.equalsIgnoreCase("originSub")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderUGWW.getShipNumber());
			}else if(forSection.equalsIgnoreCase("NetworkPartnerCode")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderUGWW.getShipNumber());
			}else if(forSection.equalsIgnoreCase("bookingAgentCode")){
				trackingStatusNew.setSoNetworkGroup(false);
				trackingStatusNew.setAccNetworkGroup(false);
				trackingStatusNew.setUtsiNetworkGroup(false);
				trackingStatusNew.setAgentNetworkGroup(false);
				trackingStatusNew.setBookingAgentExSO(serviceOrderUGWW.getShipNumber()); 
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderUGWW.getShipNumber());
			}
			trackingStatusNew.setCreatedBy("Networking:UGWW:"+userName);
			trackingStatusNew.setUpdatedBy("Networking:UGWW:"+userName);
			trackingStatusNew.setCreatedOn(new Date());
			trackingStatusNew.setUpdatedOn(new Date());
			
			try{
				 if(trackingStatusNew.getForwarderCode()!=null && (!(trackingStatusNew.getForwarderCode().equalsIgnoreCase("")))){
					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getForwarderCode());
					if(checkVendorCode){
						
					}else{
						trackingStatusNew.setForwarderCode("");	
					}
				 }
				 }catch(Exception e){
					e.printStackTrace(); 
				 }
			 try{
				 if(trackingStatusNew.getBrokerCode()!=null && (!(trackingStatusNew.getBrokerCode().equalsIgnoreCase("")))){
					boolean checkVendorCode= billingManager.findVendorCode(trackingStatusNew.getCorpID(),trackingStatusNew.getBrokerCode());
					if(checkVendorCode){
						
					}else{
						trackingStatusNew.setBrokerCode("");	
					}
				 }
				 }catch(Exception e){
					e.printStackTrace(); 
				 } 
			 
			trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
			trackingStatusUGWW=trackingStatusManager.save(trackingStatusUGWW);
			
			//if(trackingStatusNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				trackingStatusNew.setUgwIntId(trackingStatusNew.getId().toString());
			//}
			trackingStatusNew=trackingStatusManager.save(trackingStatusNew);
			
			 try{
		     List billingFieldToSync=new ArrayList();
		     Iterator billingFields=billingFieldToSync.iterator();
				while(billingFields.hasNext()){
					String field=billingFields.next().toString();
					String[] splitField=field.split("~");
					String fieldFrom=splitField[0];
					String fieldTo=splitField[1];
					PropertyUtilsBean beanUtilsBean = new PropertyUtilsBean();
					try{
					beanUtilsBean.setProperty(billingNew,fieldTo.trim(),beanUtilsBean.getProperty(billingUGWW, fieldFrom.trim()));
					}catch(Exception ex){
						logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
					}
				}
			 billingNew.setBillToCode(companyBillToCode);
			 billingNew.setBillToName(companyBillName);
			 billingNew.setContract(customerFileNew.getContract());
			 billingNew.setBillTo1Point(customerFileNew.getBillPayMethod());
			 billingNew.setBillingInstructionCodeWithDesc("CON : As per Contract");
			 billingNew.setBillingInstruction("As per Contract");
			 billingNew.setSpecialInstruction("As per Quote");
			 billingNew.setId(serviceOrderNew.getId());
			 billingNew.setCorpID(serviceOrderNew.getCorpID());
			 billingNew.setShipNumber(serviceOrderNew.getShipNumber());
			 billingNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
			 billingNew.setServiceOrder(serviceOrderNew);
			 billingNew.setCreatedBy("Networking:UGWW:"+userName);
			 billingNew.setUpdatedBy("Networking:UGWW:"+userName);
			 billingNew.setSystemDate(new Date());
			 billingNew.setCreatedOn(new Date());
			 billingNew.setUpdatedOn(new Date());
			 billingNew.setAuditor(customerFileNew.getAuditor());
			 billingNew.setPersonBilling(customerFileNew.getPersonBilling());
			 billingNew.setPersonPayable(customerFileNew.getPersonPayable());
			 billingNew.setPersonPricing(customerFileNew.getPersonPricing());
			 try{
				 if(billingNew.getVendorCode()!=null && (!(billingNew.getVendorCode().equalsIgnoreCase("")))){
					boolean checkVendorCode= billingManager.findVendorCode(billingNew.getCorpID(),billingNew.getVendorCode());
					if(checkVendorCode){
						
					}else{
						billingNew.setVendorCode("");	
					}
				 }
				 }catch(Exception e){
					e.printStackTrace(); 
				 }
			 billingNew=billingManager.save(billingNew);
			 //if(billingNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
				 billingNew.setUgwIntId(billingNew.getId().toString());
			// }
			 billingNew=billingManager.save(billingNew);
			 }catch(Exception ex){
				 logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			 }
			try{ 
			 createContainerRecords(serviceOrderUGWW, serviceOrderNew); 
			 createPieceCountsRecords(serviceOrderUGWW, serviceOrderNew); 
			 createVehicleRecords(serviceOrderUGWW, serviceOrderNew); 
			 createRoutingRecords(serviceOrderUGWW, serviceOrderNew); 
			 trackingStatusAction.createConsigneeInstruction(serviceOrderUGWW, serviceOrderNew);
			 }catch(Exception ex){
				logger.warn("Exception in craeting Container,routing etc Exception: "+currentdate+" ## : "+ex.toString());
			}
			 customerFileManager.updateIsNetworkFlag(customerFileUGWW.getId(), customerFileUGWW.getCorpID(), serviceOrderUGWW.getId(),customerFileNew.getContractType(),customerFileNew.getIsNetworkGroup());
				
			
		} catch(Exception e){
			logger.warn("\n\n\nOoooppppppsssssssssssss Sorry!!!! got an Exception in creating ServiceOrder.\n And  Reported  Exception is "+e);
		}
			
		// Updating UGWW orders
			customerFileUGWW.setIsNetworkRecord(true);
			customerFileUGWW = customerFileManager.save(customerFileUGWW);
			serviceOrderUGWW.setIsNetworkRecord(true);
			serviceOrderUGWW.setNetworkSO(true);
			serviceOrderUGWW = serviceOrderManager.save(serviceOrderUGWW);
			/**
			 * Updated Regarding #9595 31 July 14
			 */
			String bookingAgentContactForSo="";
			String bookingAgentEmailForSo="";
			try{
				if(serviceOrderNew.getCoordinator()!=null && !serviceOrderNew.getCoordinator().equals("")){
					bookingAgentContactForSo=toDoRuleManager.getUserLastFirstName(serviceOrderNew.getCoordinator());
					bookingAgentEmailForSo=customerFileManager.findCoordEmailAddress(serviceOrderNew.getCoordinator()).get(0).toString();
				}
			}catch(Exception ex){
				logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
			}
			if(childAgentType.equals("BA")){
				trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());			
				trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
				
				if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
					trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());					
					trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
					trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);					
					
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
					trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
					trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
					trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
					trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
				}
			}	 
			if(childAgentType.equals("OA")){
				trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
				
				if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
					trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
					trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
					trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
					trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
					trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
					trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
				}
			}
				 
			if(childAgentType.equals("DA")){
				trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);

				
				if(childAgentCode.equals(trackingStatusUGWW.getNetworkPartnerCode())){
					trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
					trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);

				}
				if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
					trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
					trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
					trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
					trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
				}
			}
				 
			if(childAgentType.equals("NwA")){
				trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
				trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
				
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
					trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
					trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
					trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);

				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
					trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
					trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
				}
			}
			if(childAgentType.equals("SOA")){
				trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);

				if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
					trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
					trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
					trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationSubAgentCode())){
					trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getNetworkAgentExSO())){
					trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
					trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);

				}
			}
			if(childAgentType.equals("SDA")){
				trackingStatusUGWW.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setDestinationSubAgentExSO(serviceOrderNew.getShipNumber());
				trackingStatusNew.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				trackingStatusNew.setSubDestinationAgentEmail(bookingAgentEmailForSo);
				
				if(childAgentCode.equals(trackingStatusUGWW.getDestinationAgentCode())){
					trackingStatusUGWW.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setDestinationAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setDestinationAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(serviceOrderUGWW.getBookingAgentCode())){
					trackingStatusUGWW.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setBookingAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setBookingAgentEmail(bookingAgentEmailForSo);

				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginAgentCode())){
					trackingStatusUGWW.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setOriginAgentEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getNetworkAgentExSO())){
					trackingStatusUGWW.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setNetworkContact(bookingAgentContactForSo);
					trackingStatusNew.setNetworkEmail(bookingAgentEmailForSo);
				}
				if(childAgentCode.equals(trackingStatusUGWW.getOriginSubAgentCode())){
					trackingStatusUGWW.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setOriginSubAgentExSO(serviceOrderNew.getShipNumber());
					trackingStatusNew.setSubOriginAgentContact(bookingAgentContactForSo);
					trackingStatusNew.setSubOriginAgentEmail(bookingAgentEmailForSo);
				}
			}
			
			
			trackingStatusUGWW = trackingStatusManager.save(trackingStatusUGWW);
			trackingStatusNew = trackingStatusManager.save(trackingStatusNew);
			
			
			toDoRuleManager.updateNetworkControlSO(sessionCorpID,serviceOrderNew.getShipNumber(),serviceOrderUGWW.getShipNumber(),"Done",childAgentCode);    
			
			try{
				if(serviceOrderNew.getIsNetworkRecord()){
					List linkedShipNumber= trackingStatusAction.findLinkerShipNumber(serviceOrderNew); 
					List<Object> miscellaneousRecords=trackingStatusAction.findMiscellaneousRecords(linkedShipNumber,serviceOrderNew); 
					trackingStatusAction.synchornizeMiscellaneous(miscellaneousRecords,miscellaneousNew,trackingStatusNew);
					List<Object> trackingStatusRecords=trackingStatusAction.findTrackingStatusRecords(linkedShipNumber,serviceOrderNew);
					trackingStatusAction.synchornizeTrackingStatus(trackingStatusRecords,trackingStatusNew,serviceOrderNew);
					List<Object> serviceOrderRecords=trackingStatusAction.findServiceOrderRecords(linkedShipNumber,serviceOrderNew);
					trackingStatusAction.synchornizeServiceOrder(serviceOrderRecords,serviceOrderNew,trackingStatusNew,customerFileNew);
				}
			}catch(Exception e ){
				e.printStackTrace();
			}
			
			
			toDoRuleManager.updateNetworkControlSO(sessionCorpID,serviceOrderNew.getShipNumber(),serviceOrderUGWW.getShipNumber(),"Done",childAgentCode);    
			chkFlag="Merge";
			hitFlag = "Y";
			// creating SO dash board entry
			//createSODashBoard(customerFileNew, serviceOrderNew, miscellaneousNew,billingNew, trackingStatusNew);
			successMessage = "SO# "+serviceOrderUGWW.getShipNumber()+" has been accepted and SO# "+serviceOrderNew.getShipNumber()+" has been created successfully.";
	
			insertIntoTracker(serviceOrderUGWW.getShipNumber(),"New SO by Merge");
			
		
			Long timeTaken =  System.currentTimeMillis() - StartTime;
			logger.warn("\n\nTime taken to create new service order  : "+timeTaken+"\n\n");
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String agentRole;
	@SkipValidation
	public String chkCFFlagMethod(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List customerFileList = customerFileManager.findSequenceNumber(seqNumber);
		if(customerFileList!=null && !customerFileList.isEmpty() && customerFileList.get(0)!=null){
			CustomerFile customerFileNew = (CustomerFile) customerFileList.get(0);
			agentRole = customerFileNew.getControlFlag();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public void createConsigneeInstruction(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List consigneeInstructionRecordList=consigneeInstructionManager.consigneeInstructionListOtherCorpId(serviceOrder.getId());
		if(!consigneeInstructionRecordList.isEmpty()){
			ConsigneeInstruction consigneeInstructionFrom= consigneeInstructionManager.getForOtherCorpid(new Long(consigneeInstructionRecordList.get(0).toString()));
			ConsigneeInstruction consigneeInstructionNew= new ConsigneeInstruction();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(consigneeInstructionNew, consigneeInstructionFrom);
				 
				 consigneeInstructionNew.setId(serviceOrderNew.getId());
				 consigneeInstructionNew.setCorpID(serviceOrderNew.getCorpID());
				 consigneeInstructionNew.setShipNumber(serviceOrderNew.getShipNumber());
				 consigneeInstructionNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 consigneeInstructionNew.setShip(serviceOrderNew.getShip());
				 consigneeInstructionNew.setServiceOrderId(serviceOrderNew.getId());
				 consigneeInstructionNew.setCreatedBy("Networking:UGWW:"+userName);
				 consigneeInstructionNew.setUpdatedBy("Networking:UGWW:"+userName);
				 consigneeInstructionNew.setCreatedOn(new Date());
				 consigneeInstructionNew.setUpdatedOn(new Date());
				 consigneeInstructionNew = consigneeInstructionManager.save(consigneeInstructionNew); 
				// if(consigneeInstructionNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 consigneeInstructionNew.setUgwIntId(consigneeInstructionNew.getId().toString());
				//	 }
				 consigneeInstructionNew= consigneeInstructionManager.save(consigneeInstructionNew);
					 
			}catch(Exception ex){
				logger.warn("Got an Error in Creating ConsigneeInstruction of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
			
		}
		
	}
	
	
	
	public void createRoutingRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List routingRecordList=servicePartnerManager.routingListOtherCorpId(serviceOrder.getId());
		Iterator it=routingRecordList.iterator();
		while(it.hasNext()){
			ServicePartner servicePartnerFrom= servicePartnerManager.getForOtherCorpid( new Long( it.next().toString()));
			ServicePartner servicePartnerNew= new ServicePartner();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(servicePartnerNew, servicePartnerFrom);
				 
				 servicePartnerNew.setId(null);
				 servicePartnerNew.setCorpID(serviceOrderNew.getCorpID());
				 servicePartnerNew.setCarrierCode("T10000");
				 servicePartnerNew.setShipNumber(serviceOrderNew.getShipNumber());
				 servicePartnerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 servicePartnerNew.setServiceOrder(serviceOrderNew);
				 servicePartnerNew.setServiceOrderId(serviceOrderNew.getId());
				 servicePartnerNew.setCreatedBy("Networking:UGWW:"+userName);
				 servicePartnerNew.setUpdatedBy("Networking:UGWW:"+userName);
				 servicePartnerNew.setCreatedOn(new Date());
				 servicePartnerNew.setUpdatedOn(new Date()); 
				 servicePartnerNew=servicePartnerManager.save(servicePartnerNew); 
				 
				// if(servicePartnerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 servicePartnerNew.setUgwIntId(servicePartnerNew.getId().toString());
				//	 }
				 servicePartnerNew= servicePartnerManager.save(servicePartnerNew);
				 servicePartnerManager.updateTrack(servicePartnerNew.getShipNumber(),servicePartnerNew.getBlNumber(),servicePartnerNew.getHblNumber(),getRequest().getRemoteUser());
			     servicePartnerManager.updateServiceOrder(servicePartnerNew.getCarrierDeparture(), servicePartnerNew.getCarrierArrival(), servicePartnerNew.getShipNumber(),"Networking:UGWW:"+userName);
			     servicePartnerManager.updateServiceOrderDate(servicePartnerNew.getServiceOrderId(),serviceOrderNew.getCorpID(),"Networking:UGWW:"+userName );
			     servicePartnerManager.updateServiceOrderActualDate(servicePartnerNew.getServiceOrderId(),serviceOrderNew.getCorpID(),serviceOrderNew.getCustomerFileId(),"Networking:UGWW:"+userName);
			}catch(Exception ex){
				logger.warn("Got an Error in Creating ServicePartner of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	
	public void createVehicleRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List vehicleRecordList=vehicleManager.vehicleListOtherCorpId(serviceOrder.getId());
		Iterator it=vehicleRecordList.iterator();
		while(it.hasNext()){
			Vehicle vehicleFrom= vehicleManager.getForOtherCorpid(new Long(it.next().toString()));
			Vehicle vehicleNew= new Vehicle();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(vehicleNew, vehicleFrom);
				 vehicleNew.setId(null);
				 vehicleNew.setCorpID(serviceOrderNew.getCorpID());
				 vehicleNew.setShipNumber(serviceOrderNew.getShipNumber());
				 vehicleNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 vehicleNew.setServiceOrder(serviceOrderNew);
				 vehicleNew.setServiceOrderId(serviceOrderNew.getId());
				 vehicleNew.setCreatedBy("Networking:UGWW:"+userName);
				 vehicleNew.setUpdatedBy("Networking:UGWW:"+userName);
				 vehicleNew.setCreatedOn(new Date());
				 vehicleNew.setUpdatedOn(new Date()); 
				 vehicleNew= vehicleManager.save(vehicleNew);
				 
				// if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
			//		 }
				 vehicleNew= vehicleManager.save(vehicleNew);
				 
			}catch(Exception ex){
				logger.warn("Got an Error in Creating Vehicle of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
		}
		
	}
	
	
	public void createPieceCountsRecords(ServiceOrder serviceOrder,ServiceOrder serviceOrderNew) {
		List cartonRecordList=cartonManager.cartonListotherCorpId(serviceOrder.getId());
		Iterator it=cartonRecordList.iterator();
		while(it.hasNext()){
			Carton cartonFrom= cartonManager.getForOtherCorpid(new Long(it.next().toString()));
			Carton cartonNew= new Carton();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(cartonNew, cartonFrom);
				 cartonNew.setId(null);
				 cartonNew.setCorpID(serviceOrderNew.getCorpID());
				 cartonNew.setShipNumber(serviceOrderNew.getShipNumber());
				 cartonNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 cartonNew.setServiceOrder(serviceOrderNew);
				 cartonNew.setServiceOrderId(serviceOrderNew.getId());
				 cartonNew.setCreatedBy("Networking:UGWW:"+userName);
				 cartonNew.setUpdatedBy("Networking:UGWW:"+userName);
				 cartonNew.setCreatedOn(new Date());
				 cartonNew.setUpdatedOn(new Date()); 
				 cartonNew=cartonManager.save(cartonNew);
				 System.out.println("\n\n\n\n netWeightKilo for UGWW carton id "+cartonFrom.getId() +"is :"+cartonFrom.getNetWeightKilo());
				 System.out.println("\n\n\n netWeightKilo for carton id "+cartonNew.getId() +"is :"+cartonNew.getNetWeightKilo());
				 
			//	 if(cartonNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 cartonNew.setUgwIntId(cartonNew.getId().toString());
			//		 }
				 cartonNew= cartonManager.save(cartonNew);
				 
				  
			}catch(Exception ex){
				logger.warn("Got an Error in Creating Carton of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	
	public void createContainerRecords(ServiceOrder serviceOrder, ServiceOrder serviceOrderNew) {
		List containerRecordList=containerManager.containerListOtherCorpId(serviceOrder.getId());
		Iterator it=containerRecordList.iterator();
		while(it.hasNext()){
			Container containerFrom= containerManager.getForOtherCorpid(new Long(it.next().toString()));
			Container containerNew= new Container();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(containerNew, containerFrom);
				
				 containerNew.setId(null);
				 containerNew.setCorpID(serviceOrderNew.getCorpID());
				 containerNew.setShipNumber(serviceOrderNew.getShipNumber());
				 containerNew.setSequenceNumber(serviceOrderNew.getSequenceNumber());
				 containerNew.setServiceOrder(serviceOrderNew);
				 containerNew.setServiceOrderId(serviceOrderNew.getId());
				 containerNew.setCreatedBy("Networking:UGWW:"+userName);
				 containerNew.setUpdatedBy("Networking:UGWW:"+userName);
				 containerNew.setCreatedOn(new Date());
				 containerNew.setUpdatedOn(new Date());
				 containerNew= containerManager.save(containerNew);
				 
				// if(containerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 containerNew.setUgwIntId(containerNew.getId().toString());
				//	 }
				 containerNew= containerManager.save(containerNew);
				 
			}catch(Exception ex){
				logger.warn("Got an Error in Creating Container of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
		}
	}
	
	
	@SkipValidation
	public String updateNetWorkControlMethod(){
		Long StartTime = System.currentTimeMillis();
		userFirstName= getRequest().getRemoteUser();
		toDoRuleManager.updateNetworkControlSO(sessionCorpID,"",shipnumber,"Done",childAgentCode);
		chkFlag="Decline";
		insertIntoTracker(shipnumber,"Done");
		todoList();
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to Decline the service order OF ugww : "+timeTaken+"\n\n");
		return SUCCESS;
	}
	
	
	
	
	@SkipValidation
	public String declineNetworkFileMethod(){
		Long StartTime = System.currentTimeMillis();
		userFirstName= getRequest().getRemoteUser();
		TrackingStatus trackingStatusUGWW = trackingStatusManager.getForOtherCorpid(soId);
		ServiceOrder soUGWW = serviceOrderManager.getForOtherCorpid(soId);
		
		//Updating Tracking Status of UGWW 
		if(childAgentType.equals("BA")){
			trackingStatusUGWW.setBookingAgentExSO("Order Decline");
			childAgentCode =  soUGWW.getBookingAgentCode();
		}
		
		if(childAgentType.equals("OA")){
			trackingStatusUGWW.setOriginAgentExSO("Order Decline");
			childAgentCode = trackingStatusUGWW.getOriginAgentCode();
		}
		
		if(childAgentType.equals("DA")){
			trackingStatusUGWW.setDestinationAgentExSO("Order Decline");
			childAgentCode = trackingStatusUGWW.getDestinationAgentCode();
		}
		
		if(childAgentType.equals("NwA")){
			trackingStatusUGWW.setNetworkAgentExSO("Order Decline");
			childAgentCode = trackingStatusUGWW.getNetworkPartnerCode();
		}
		if(childAgentType.equals("SOA")){
			trackingStatusUGWW.setOriginSubAgentExSO("Order Decline");
			childAgentCode =  trackingStatusUGWW.getOriginSubAgentCode();
		}
		if(childAgentType.equals("SDA")){
			trackingStatusUGWW.setDestinationSubAgentExSO("Order Decline");
			childAgentCode =  trackingStatusUGWW.getDestinationSubAgentCode();
		}
		
		trackingStatusUGWW.setUpdatedBy("Networking:UGWW:"+getRequest().getRemoteUser());
		trackingStatusUGWW.setUpdatedOn(new Date());
		trackingStatusUGWW = trackingStatusManager.save(trackingStatusUGWW);
		// updating networkcontrol table
		toDoRuleManager.updateNetworkControlSO(sessionCorpID,"",trackingStatusUGWW.getShipNumber(),"Done",childAgentCode);
		chkFlag="Decline";
		insertIntoTracker(trackingStatusUGWW.getShipNumber(),"Decline");
		todoList();
		
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to Decline the service order OF ugww : "+timeTaken+"\n\n");
		return SUCCESS;
	}
	private String toDoResultId;
	private String toDoResultUrl;
	private String toDoRecordId;
	private String roleForTransfer;
	private String roleType;
	@SkipValidation
	public String updateRoleTransfer(){
		toDoRuleManager.updateToDoResult(toDoResultId,toDoResultUrl,toDoRecordId,roleForTransfer,roleType,shipnumber);
		chkFlag="RoleTransfer";
		todoList();
		return SUCCESS;
	}
	
	public CustomerFile inComingCF;
	public CustomerFile yourCF;
	public Billing inComingBilling;
	public Billing yourBilling;
	public TrackingStatus inComingTS;
	public TrackingStatus yourTS;
	public Miscellaneous inComingMsc;
	public Miscellaneous yourMsc;
	public String fieldValues;
	public String contIDs;
	public String  cartonIDs;
	public String vehicleIDs;
	public String servicePartnerIDs;
	public String newIdNum;
	
	
	
	@SkipValidation
	public String mergeIncomingOrderMethod() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		orgId = sessionCorpID;
		networkFields = toDoRuleManager.getNetworkDataFields();
		logger.warn("Merging fields and its values : "+ fieldValues);
		logger.warn("UGWW SO Id : "+soUGWWID+"\n Your So id : "+mergingSOID);
		
		//SO objects 
		incomingSO = serviceOrderManager.getForOtherCorpid(soUGWWID);
		yourSO = serviceOrderManager.get(mergingSOID);
		
		// Cf objects
		inComingCF = incomingSO.getCustomerFile();
		yourCF = yourSO.getCustomerFile();
		
		// Billing objects
		inComingBilling = billingManager.getForOtherCorpid(soUGWWID);
		yourBilling = billingManager.get(mergingSOID);
		
		//TS Objects
		inComingTS = trackingStatusManager.getForOtherCorpid(soUGWWID);
		yourTS = trackingStatusManager.get(mergingSOID);
		
		//Misce Objects
		inComingMsc = miscellaneousManager.getForOtherCorpid(soUGWWID);
		yourMsc = miscellaneousManager.get(mergingSOID);
		
		Class noparams[] = {};
		for (Map.Entry<String,List<String>> entry : networkFields.entrySet()) {
			List<String> list = new ArrayList<String>();
			
			if("CustomerFile".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.CustomerFile");
				for (String fieldName : entry.getValue()) {
					Method method;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingCF, null);
					Object obj2 = method.invoke(yourCF, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.equals("")  && (obj2 == null || obj2.equals(""))){
						Field tempField = setFieldValue(cls,fieldName);
						tempField.set(yourCF, obj1);
					}
				}
			}else if("ServiceOrder".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.ServiceOrder");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(incomingSO, null);
					Object obj2 = method.invoke(yourSO, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("")  && (obj2 == null || obj2.toString().trim().equals(""))){
						Field tempField = setFieldValue(cls,fieldName);
						tempField.set(yourSO, obj1);
					}
				}
			}else if("Billing".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.Billing");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingBilling, null);
					Object obj2 = method.invoke(yourBilling, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("")  && (obj2 == null || obj2.toString().trim().equals(""))){
						Field tempField = setFieldValue(cls,fieldName);
						tempField.set(yourBilling, obj1);
					}
				}
			}else if("TrackingStatus".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.TrackingStatus");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingTS, null);
					Object obj2 = method.invoke(yourTS, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("")  && (obj2 == null || obj2.toString().trim().equals(""))){
						Field tempField = setFieldValue(cls,fieldName);
						tempField.set(yourTS, obj1);
					}
				}
			}else if("Miscellaneous".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.Miscellaneous");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingMsc, null);
					Object obj2 = method.invoke(yourMsc, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("")  && (obj2 == null || obj2.toString().trim().equals(""))){
						Field tempField = setFieldValue(cls,fieldName);
						tempField.set(yourMsc, obj1);
					}
				}
			}
		}
		String [] fieldsValues = fieldValues.split("~");
		for(int i = 0 ; i < fieldsValues.length ; i++){
			String fieldVal = fieldsValues[i].toString().trim();
			String monoFieldVal[] =  fieldVal.split("=");
			if(monoFieldVal[0].toString().trim().contains("CustomerFile")){
				Class cls = Class.forName("com.trilasoft.app.model.CustomerFile");
				String fieldName = monoFieldVal[0].toString().trim().replace("CustomerFile.", "");
				Method method ;
				try {
					method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
				} catch (Exception e) {
					method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					logger.error("Do not have property getter."+ e.getStackTrace()[0]);
				}
				Object obj1 = method.invoke(inComingCF, null);
				
				Field tempField = setFieldValue(cls,fieldName);
				tempField.set(yourCF, obj1);
			}else if(monoFieldVal[0].toString().trim().contains("ServiceOrder")){
				Class cls = Class.forName("com.trilasoft.app.model.ServiceOrder");
				String fieldName = monoFieldVal[0].toString().trim().replace("ServiceOrder.", "");
				Method method ;
				try {
					method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
				} catch (Exception e) {
					method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					logger.error("Do not have property getter."+ e.getStackTrace()[0]);
				}
				Object obj1 = method.invoke(incomingSO, null);
				Field tempField = setFieldValue(cls,fieldName);
				tempField.set(yourSO, obj1);
			}else if(monoFieldVal[0].toString().trim().contains("Billing")){
				Class cls = Class.forName("com.trilasoft.app.model.Billing");
				String fieldName = monoFieldVal[0].toString().trim().replace("Billing.", "");
				Method method ;
				try {
					method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
				} catch (Exception e) {
					method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					logger.error("Do not have property getter."+ e.getStackTrace()[0]);
				}
				Object obj1 = method.invoke(inComingBilling, null);
				Field tempField = setFieldValue(cls,fieldName);
				tempField.set(yourBilling, obj1);
			}else if(monoFieldVal[0].toString().trim().contains("TrackingStatus")){
				Class cls = Class.forName("com.trilasoft.app.model.TrackingStatus");
				String fieldName = monoFieldVal[0].toString().trim().replace("TrackingStatus.", "");
				Method method ;
				try {
					method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
				} catch (Exception e) {
					method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					logger.error("Do not have property getter."+ e.getStackTrace()[0]);
				}
				Object obj1 = method.invoke(inComingTS, null);
				Field tempField = setFieldValue(cls,fieldName);
				tempField.set(yourTS, obj1);
			}else if(monoFieldVal[0].toString().trim().contains("Miscellaneous")){
				Class cls = Class.forName("com.trilasoft.app.model.Miscellaneous");
				String fieldName = monoFieldVal[0].toString().trim().replace("Miscellaneous.", "");
				Method method ;
				try {
					method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
				} catch (Exception e) {
					method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					logger.error("Do not have property getter."+ e.getStackTrace()[0]);
				}
				Object obj1 = method.invoke(inComingMsc, null);
				Field tempField = setFieldValue(cls,fieldName);
				tempField.set(yourMsc, obj1);
			}
		}
		
		// Saving object with manager
		yourCF.setIsNetworkRecord(true);
		
		// setting org and dest city code of CF
		 if(yourCF.getOriginState()!=null && (!(yourCF.getOriginState().equals("")))){
			 yourCF.setOriginCityCode(yourCF.getOriginCity()+","+yourCF.getOriginState());
		}else{
			yourCF.setOriginCityCode(yourCF.getOriginCity());
		}
		if(yourCF.getDestinationState()!=null && (!(yourCF.getDestinationState().equals("")))){
			yourCF.setDestinationCityCode(yourCF.getDestinationCity()+","+yourCF.getDestinationState());
		}else{
			yourCF.setDestinationCityCode(yourCF.getDestinationCity());
		} 
		List ISO3CodeList  = refMasterManager.findCountryCode(yourCF.getOriginCountry(), sessionCorpID);
		if(ISO3CodeList!=null && !ISO3CodeList.isEmpty() && ISO3CodeList.get(0)!=null && !ISO3CodeList.get(0).toString().equals("") ){
			yourCF.setOriginCountryCode(ISO3CodeList.get(0).toString());
		}
		ISO3CodeList=null;
		ISO3CodeList  = refMasterManager.findCountryCode(yourCF.getDestinationCountry(), sessionCorpID);
		if(ISO3CodeList!=null && !ISO3CodeList.isEmpty() && ISO3CodeList.get(0)!=null && !ISO3CodeList.get(0).toString().equals("") ){
			yourCF.setDestinationCountryCode(ISO3CodeList.get(0).toString());
		}
		ISO3CodeList=null;
		
		yourCF.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		yourCF.setUpdatedOn(new Date());
		if(company.getAccessQuotationFromCustomerFile()==true){
		if(yourCF.getMoveType() != null && yourCF.getMoveType().toString().trim().equals("Quote") ){
			yourCF.setControlFlag("C");
			yourCF.setQuotationStatus("Accepted");
			yourCF.setStatus("NEW");
			yourCF.setStatusDate(new Date());
			yourCF.setStatusNumber(1);
			yourCF.setSalesStatus("Booked");	
			yourCF.setMoveType("BookedMove");
			}
			
		}
		
		if(yourCF.getControlFlag().toString().trim().equals("Q")){
			yourCF.setControlFlag("C");
			yourCF.setQuotationStatus("Accepted");
			yourCF.setStatus("NEW");
			yourCF.setStatusDate(new Date());
			yourCF.setStatusNumber(1);
			yourCF.setSalesStatus("Booked");
		}
		
		
		yourCF.setBookingAgentSequenceNumber(inComingCF.getSequenceNumber());
		yourCF = customerFileManager.save(yourCF);
		
		
		yourSO.setIsNetworkRecord(true);
		yourSO.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		yourSO.setUpdatedOn(new Date());
		yourSO.setNetworkSO(true);
		
		if(yourSO.getOriginState()!=null && (!(yourSO.getOriginState().equals("")))){
			yourSO.setOriginCityCode(yourSO.getOriginCity()+","+yourSO.getOriginState());
		}else{
			yourSO.setOriginCityCode(yourSO.getOriginCity());
		}
		if(yourSO.getDestinationState()!=null && (!(yourSO.getDestinationState().equals("")))){
			yourSO.setDestinationCityCode(yourSO.getDestinationCity()+","+yourSO.getDestinationState());
		}else{
			yourSO.setDestinationCityCode(yourSO.getDestinationCity());
		}
		ISO3CodeList  = refMasterManager.findCountryCode(yourSO.getOriginCountry(), sessionCorpID);
		if(ISO3CodeList!=null && !ISO3CodeList.isEmpty() && ISO3CodeList.get(0)!=null && !ISO3CodeList.get(0).toString().equals("") ){
			yourSO.setOriginCountryCode(ISO3CodeList.get(0).toString());
		}
		ISO3CodeList=null;
		ISO3CodeList  = refMasterManager.findCountryCode(yourSO.getDestinationCountry(), sessionCorpID);
		if(ISO3CodeList!=null && !ISO3CodeList.isEmpty() && ISO3CodeList.get(0)!=null && !ISO3CodeList.get(0).toString().equals("") ){
			yourSO.setDestinationCountryCode(ISO3CodeList.get(0).toString());
		}
		ISO3CodeList=null;
		yourSO.setUgwIntId(yourSO.getShipNumber());

		// SO quotes check
		if(yourSO.getControlFlag().toString().trim().equals("Q")){
			yourSO.setControlFlag("C");
			//yourSO.setStatus("NEW");
			//yourSO.setStatusDate(new Date());
			//yourSO.setStatusNumber(1);
			yourSO.setQuoteStatus("AC");
			yourSO.setQuoteAccept("A");
		}
		yourSO.setBookingAgentShipNumber(incomingSO.getShipNumber());
		yourSO = serviceOrderManager.save(yourSO);
		
		yourBilling.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		yourBilling.setUpdatedOn(new Date());
		yourBilling = billingManager.save(yourBilling);
		yourMsc.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		yourMsc.setUpdatedOn(new Date());
		yourMsc = miscellaneousManager.save(yourMsc);
		
	// Updating Tracking Status of  linked Corpid 
		
		if(agentType.equals("BA")){
			yourTS.setBookingAgentExSO(incomingSO.getShipNumber());
			inComingTS.setBookingAgentExSO(incomingSO.getShipNumber());
		}
		
		if(agentType.equals("OA")){
			yourTS.setOriginAgentExSO(incomingSO.getShipNumber());
			inComingTS.setOriginAgentExSO(incomingSO.getShipNumber());
			yourTS.setOriginAgentCode(inComingTS.getOriginAgentCode());
			yourTS.setOriginAgent(inComingTS.getOriginAgent());
		}
		if(agentType.equals("DA")){
			yourTS.setDestinationAgentExSO(incomingSO.getShipNumber());
			inComingTS.setDestinationAgentExSO(incomingSO.getShipNumber());
			yourTS.setDestinationAgentCode(inComingTS.getDestinationAgentCode());
			yourTS.setDestinationAgent(inComingTS.getDestinationAgent());
		}
		if(agentType.equals("NwA")){
			yourTS.setNetworkAgentExSO(incomingSO.getShipNumber());
			inComingTS.setNetworkAgentExSO(incomingSO.getShipNumber());
			yourTS.setNetworkPartnerCode(inComingTS.getNetworkPartnerCode());
			yourTS.setNetworkPartnerName(inComingTS.getNetworkPartnerName());
			
		}
		if(agentType.equals("SOA")){
			yourTS.setOriginSubAgentExSO(incomingSO.getShipNumber());
			inComingTS.setOriginSubAgentExSO(incomingSO.getShipNumber());
			yourTS.setOriginSubAgentCode(inComingTS.getOriginSubAgentCode());
			yourTS.setOriginSubAgent(inComingTS.getOriginSubAgent());
			
		}
		if(agentType.equals("SDA")){
			yourTS.setDestinationAgentExSO(incomingSO.getShipNumber());
			inComingTS.setDestinationAgentExSO(incomingSO.getShipNumber());
			yourTS.setDestinationSubAgentCode(inComingTS.getDestinationSubAgentCode());
			yourTS.setDestinationSubAgent(inComingTS.getDestinationSubAgent());
			
		}
		yourTS.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		yourTS.setUpdatedOn(new Date());
		
		/**
		 * Updated Regarding #9595 31 July 14
		 */
		String bookingAgentContactForSo="";
		String bookingAgentEmailForSo="";
		try{
			if(yourSO.getCoordinator()!=null && !yourSO.getCoordinator().equals("")){
				bookingAgentContactForSo=toDoRuleManager.getUserLastFirstName(yourSO.getCoordinator());
				bookingAgentEmailForSo=customerFileManager.findCoordEmailAddress(yourSO.getCoordinator()).get(0).toString();
			}
		}catch(Exception ex){
			logger.warn("Exception: "+currentdate+" ## : "+ex.toString());
		}
		if(childAgentType.equals("BA")){
			inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
			yourTS.setBookingAgentExSO(yourSO.getShipNumber());
			yourTS.setBookingAgentContact(bookingAgentContactForSo);
			yourTS.setBookingAgentEmail(bookingAgentEmailForSo);
			if(childAgentCode.equals(inComingTS.getNetworkPartnerCode())){
				inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkContact(bookingAgentContactForSo);
				yourTS.setNetworkEmail(bookingAgentEmailForSo);	
			}
			if(childAgentCode.equals(inComingTS.getOriginAgentCode())){
				inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentContact(bookingAgentContactForSo);
				yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationAgentCode())){
				inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentExSO(yourSO.getShipNumber()); 
				yourTS.setDestinationAgentContact(bookingAgentContactForSo);
				yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationSubAgentCode())){
				inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginSubAgentCode())){
				inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
				yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}	 
		if(childAgentType.equals("OA")){
			inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
			yourTS.setOriginAgentExSO(yourSO.getShipNumber());
			yourTS.setOriginAgentContact(bookingAgentContactForSo);
			yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(inComingTS.getNetworkPartnerCode())){
				inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkContact(bookingAgentContactForSo);
				yourTS.setNetworkEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(incomingSO.getBookingAgentCode())){
				inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentContact(bookingAgentContactForSo);
				yourTS.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationAgentCode())){
				inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentContact(bookingAgentContactForSo);
				yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationSubAgentCode())){
				inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginSubAgentCode())){
				inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
				yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);
				
			}
		}
			 
		if(childAgentType.equals("DA")){
			inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
			yourTS.setDestinationAgentExSO(yourSO.getShipNumber());
			yourTS.setDestinationAgentContact(bookingAgentContactForSo);
			yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(inComingTS.getNetworkPartnerCode())){
				inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkContact(bookingAgentContactForSo);
				yourTS.setNetworkEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(incomingSO.getBookingAgentCode())){
				inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentContact(bookingAgentContactForSo);
				yourTS.setBookingAgentEmail(bookingAgentEmailForSo);

			}
			if(childAgentCode.equals(inComingTS.getOriginAgentCode())){
				inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentContact(bookingAgentContactForSo);
				yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationSubAgentCode())){
				inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginSubAgentCode())){
				inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
				yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
			 
		if(childAgentType.equals("NwA")){
			inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
			yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
			yourTS.setNetworkContact(bookingAgentContactForSo);
			yourTS.setNetworkEmail(bookingAgentEmailForSo);
			if(childAgentCode.equals(inComingTS.getDestinationAgentCode())){
				inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentContact(bookingAgentContactForSo);
				yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(incomingSO.getBookingAgentCode())){
				inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentContact(bookingAgentContactForSo);
				yourTS.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginAgentCode())){
				inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentContact(bookingAgentContactForSo);
				yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationSubAgentCode())){
				inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginSubAgentCode())){
				inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
				yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
		if(childAgentType.equals("SOA")){
			inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
			yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
			yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
			yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);

			if(childAgentCode.equals(inComingTS.getDestinationAgentCode())){
				inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentContact(bookingAgentContactForSo);
				yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			
			}
			if(childAgentCode.equals(incomingSO.getBookingAgentCode())){
				inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentContact(bookingAgentContactForSo);
				yourTS.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginAgentCode())){
				inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentContact(bookingAgentContactForSo);
				yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getDestinationSubAgentCode())){
				inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
				yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getNetworkAgentExSO())){
				inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkContact(bookingAgentContactForSo);
				yourTS.setNetworkEmail(bookingAgentEmailForSo);

			}
		}
		if(childAgentType.equals("SDA")){
			inComingTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
			yourTS.setDestinationSubAgentExSO(yourSO.getShipNumber());
			yourTS.setSubDestinationAgentAgentContact(bookingAgentContactForSo);
			yourTS.setSubDestinationAgentEmail(bookingAgentEmailForSo);
			
			if(childAgentCode.equals(inComingTS.getDestinationAgentCode())){
				inComingTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentExSO(yourSO.getShipNumber());
				yourTS.setDestinationAgentContact(bookingAgentContactForSo);
				yourTS.setDestinationAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(incomingSO.getBookingAgentCode())){
				inComingTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentExSO(yourSO.getShipNumber());
				yourTS.setBookingAgentContact(bookingAgentContactForSo);
				yourTS.setBookingAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginAgentCode())){
				inComingTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginAgentContact(bookingAgentContactForSo);
				yourTS.setOriginAgentEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getNetworkAgentExSO())){
				inComingTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkAgentExSO(yourSO.getShipNumber());
				yourTS.setNetworkContact(bookingAgentContactForSo);
				yourTS.setNetworkEmail(bookingAgentEmailForSo);
			}
			if(childAgentCode.equals(inComingTS.getOriginSubAgentCode())){
				inComingTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setOriginSubAgentExSO(yourSO.getShipNumber());
				yourTS.setSubOriginAgentContact(bookingAgentContactForSo);
				yourTS.setSubOriginAgentEmail(bookingAgentEmailForSo);
			}
		}
		
		
		//end
		
		
		inComingTS = trackingStatusManager.save(inComingTS);
		yourTS = trackingStatusManager.save(yourTS);
		
		// Updating UGWW orders
		inComingCF.setIsNetworkRecord(true);
		inComingCF.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		inComingCF.setUpdatedOn(new Date());
		inComingCF = customerFileManager.save(inComingCF);
		
		incomingSO.setIsNetworkRecord(true);
		incomingSO.setUpdatedBy("Merge:"+getRequest().getRemoteUser());
		incomingSO.setUpdatedOn(new Date());
		incomingSO.setNetworkSO(true);
		incomingSO = serviceOrderManager.save(incomingSO);
		
		
		// Mergeging Containers
		
		String idNumber ="01";
		String maxIdNumber="";
		int i = toDoRuleManager.removeAll(yourSO.getShipNumber(),contIDs,"container");
		
		String contIds[] = contIDs.split(",");
		for (String id : contIds) {
			if(id.trim().equals(""))
				continue;
			logger.info("Getting object of container id ---->>>"+id);
			Container containerFrom = containerManager.getForOtherCorpid(new Long(id));
			if(!containerFrom.getCorpID().equals(sessionCorpID)){
			Container containerNew = new Container();
			try{
				 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
				 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
				 beanUtilsBean2.copyProperties(containerNew, containerFrom);
				
				 containerNew.setId(null);
				 containerNew.setCorpID(yourSO.getCorpID());
				 containerNew.setShipNumber(yourSO.getShipNumber());
				 containerNew.setSequenceNumber(yourSO.getSequenceNumber());
				 containerNew.setServiceOrder(yourSO);
				 containerNew.setServiceOrderId(yourSO.getId());
				 containerNew.setCreatedBy("Merge:UGWW:"+userName);
				 containerNew.setUpdatedBy("Merge:UGWW:"+userName);
				 containerNew.setCreatedOn(new Date());
				 containerNew.setUpdatedOn(new Date());
				 if(!"".equals(maxIdNumber)){
					try {
						Long autoId = Long.parseLong((maxIdNumber).toString()) + 1;
						    //System.out.println(autoId);
							if((autoId.toString()).length() == 1) {
						    	idNumber = "0"+(autoId.toString());
						    }else {
						    	idNumber=autoId.toString();
						
						    }
					} catch (Exception e) {
						e.printStackTrace();
					}
				 }
				 containerNew.setIdNumber(idNumber);
				 containerNew= containerManager.save(containerNew);
				 
				// if(containerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
					 containerNew.setUgwIntId(containerNew.getId().toString());
				// }
				 containerNew= containerManager.save(containerNew);
				 maxIdNumber= containerNew.getIdNumber().toString();
				 logger.info("new Container created at corpid "+sessionCorpID+" for shipnumber "+yourSO.getShipNumber()+"with id :"+containerNew.getId());
				 logger.info("removing Container with id :"+id);
				 containerManager.removeForOtherCorpid(new Long(id));
				 logger.info(" Container removed with  id :"+id);
				 
			}catch(Exception ex){
				logger.warn("Got an Error in Creating Container of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
			}
			}
		}
		i = toDoRuleManager.removeAll(incomingSO.getShipNumber(),"","container");
		
		// Container merge end
		
		// carton merge start
		i = toDoRuleManager.removeAll(yourSO.getShipNumber(),cartonIDs,"carton");
		
		idNumber ="01";
		maxIdNumber="";
		String [] cartonIds=cartonIDs.split(",");
		for (String id : cartonIds) {
			if(id.trim().equals(""))
				continue;
			logger.info("Getting object of carton id ---->>>"+id);
			Carton cartonFrom= cartonManager.getForOtherCorpid(new Long(id.toString()));
			if(!cartonFrom.getCorpID().equals(sessionCorpID)){
				Carton cartonNew= new Carton();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(cartonNew, cartonFrom);
					 cartonNew.setId(null);
					 cartonNew.setCorpID(yourSO.getCorpID());
					 cartonNew.setShipNumber(yourSO.getShipNumber());
					 cartonNew.setSequenceNumber(yourSO.getSequenceNumber());
					 cartonNew.setServiceOrder(yourSO);
					 cartonNew.setServiceOrderId(yourSO.getId());
					 cartonNew.setCreatedBy("Merge:UGWW:"+userName);
					 cartonNew.setUpdatedBy("Merge:UGWW:"+userName);
					 cartonNew.setCreatedOn(new Date());
					 cartonNew.setUpdatedOn(new Date()); 
					 if(!"".equals(maxIdNumber)){
							try {
								Long autoId = Long.parseLong((maxIdNumber).toString()) + 1;
								    //System.out.println(autoId);
									if((autoId.toString()).length() == 1) {
								    	idNumber = "0"+(autoId.toString());
								    }else {
								    	idNumber=autoId.toString();
								
								    }
							} catch (Exception e) {
								e.printStackTrace();
							}
						 }
					 cartonNew.setIdNumber(idNumber);
					 cartonNew=cartonManager.save(cartonNew);
					 //if(cartonNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 cartonNew.setUgwIntId(cartonNew.getId().toString());
					//	 }
					 cartonNew= cartonManager.save(cartonNew);
					 maxIdNumber= cartonNew.getIdNumber().toString();
					 logger.info("new carton created at corpid "+sessionCorpID+" for shipnumber "+yourSO.getShipNumber()+"with id :"+cartonNew.getId());
					 logger.info("removing carton with id :"+id);
					 cartonManager.removeForOtherCorpid(new Long(id));
					 logger.info(" carton removed with  id :"+id);
					  
				}catch(Exception ex){
					logger.warn("Got an Error in Creating Carton of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
				}
			}
		}
		
		i = toDoRuleManager.removeAll(incomingSO.getShipNumber(),"","carton");
		
		// carton merge end
		
		
		// vehicle merge start
		 i = toDoRuleManager.removeAll(yourSO.getShipNumber(),vehicleIDs,"vehicle");
		idNumber ="01";
		 maxIdNumber="";
		String [] vehicleIds=vehicleIDs.split(",");
			for (String id : vehicleIds) {
				if(id.trim().equals(""))
					continue;
				logger.info("Getting object of vehicle id ---->>>"+id);
				Vehicle vehicleFrom= vehicleManager.getForOtherCorpid(new Long(id.toString()));
				if(!vehicleFrom.getCorpID().equals(sessionCorpID)){
				Vehicle vehicleNew= new Vehicle();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(vehicleNew, vehicleFrom);
					 vehicleNew.setId(null);
					 vehicleNew.setCorpID(yourSO.getCorpID());
					 vehicleNew.setShipNumber(yourSO.getShipNumber());
					 vehicleNew.setSequenceNumber(yourSO.getSequenceNumber());
					 vehicleNew.setServiceOrder(yourSO);
					 vehicleNew.setServiceOrderId(yourSO.getId());
					 vehicleNew.setCreatedBy("Merge:UGWW:"+userName);
					 vehicleNew.setUpdatedBy("Merge:UGWW:"+userName);
					 vehicleNew.setCreatedOn(new Date());
					 vehicleNew.setUpdatedOn(new Date()); 
					 if(!"".equals(maxIdNumber)){
							try {
								Long autoId = Long.parseLong((maxIdNumber).toString()) + 1;
								    //System.out.println(autoId);
									if((autoId.toString()).length() == 1) {
								    	idNumber = "0"+(autoId.toString());
								    }else {
								    	idNumber=autoId.toString();
								
								    }
							} catch (Exception e) {
								e.printStackTrace();
							}
						 }
					 vehicleNew.setIdNumber(idNumber);
					 vehicleNew= vehicleManager.save(vehicleNew);
					 
					// if(vehicleNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 vehicleNew.setUgwIntId(vehicleNew.getId().toString());
					//	 }
					 vehicleNew= vehicleManager.save(vehicleNew);
					 maxIdNumber= vehicleNew.getIdNumber().toString();
					 logger.info("new vehicle created at corpid "+sessionCorpID+" for shipnumber "+yourSO.getShipNumber()+"with id :"+vehicleNew.getId());
					 logger.info("removing vehicle with id :"+id);
					 vehicleManager.removeForOtherCorpid(new Long(id));
					 logger.info(" vehicle removed with  id :"+id);
					  
					 
				}catch(Exception ex){
					logger.warn("Got an Error in Creating Vehicle of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
				}
			}
		}
		
		i = toDoRuleManager.removeAll(incomingSO.getShipNumber(),"","vehicle");
		
		// vehicle merge end
		
		
		// servicepartner merge start
		i = toDoRuleManager.removeAll(yourSO.getShipNumber(),servicePartnerIDs,"servicepartner");
		// comment for bug # 8289
		/*Map newIdNumMap = new HashMap();
		if(newIdNum!=null && !"".equals(newIdNum)){
			String newVal[] = newIdNum.split(",");
			for (String newStr : newVal) {
				String newMapVal[] = newStr.split("=");
				newIdNumMap.put(newMapVal[0], newMapVal[1]);
			}
		}*/
		idNumber ="01";
		maxIdNumber="";
		String [] servicePartnerIds=servicePartnerIDs.split(",");
		for (String id : servicePartnerIds) {
			if(id.trim().equals(""))
				continue;
			logger.info("Getting object of servicePartner id ---->>>"+id);
			ServicePartner servicePartnerFrom= servicePartnerManager.getForOtherCorpid( new Long( id.toString()));
			if(!servicePartnerFrom.getCorpID().equals(sessionCorpID)){
				ServicePartner servicePartnerNew= new ServicePartner();
				try{
					 BeanUtilsBean beanUtilsBean2 = BeanUtilsBean.getInstance();
					 beanUtilsBean2.getConvertUtils().register( new BigDecimalConverter(null), BigDecimal.class);
					 beanUtilsBean2.copyProperties(servicePartnerNew, servicePartnerFrom);
					 
					 servicePartnerNew.setId(null);
					 servicePartnerNew.setCorpID(yourSO.getCorpID());
					 servicePartnerNew.setCarrierCode("T10000");
					 servicePartnerNew.setShipNumber(yourSO.getShipNumber());
					 servicePartnerNew.setSequenceNumber(yourSO.getSequenceNumber());
					 servicePartnerNew.setServiceOrder(yourSO);
					 servicePartnerNew.setServiceOrderId(yourSO.getId());
					 servicePartnerNew.setCreatedBy("Merge:UGWW:"+userName);
					 servicePartnerNew.setUpdatedBy("Merge:UGWW:"+userName);
					 servicePartnerNew.setCreatedOn(new Date());
					 servicePartnerNew.setUpdatedOn(new Date());
					// servicePartnerNew.setCarrierNumber(newIdNumMap.get(id).toString());
					 servicePartnerNew=servicePartnerManager.save(servicePartnerNew); 
					 
					// if(servicePartnerNew.getCorpID().toString().trim().equalsIgnoreCase("UGWW")){
						 servicePartnerNew.setUgwIntId(servicePartnerNew.getId().toString());
					//	 }
					 servicePartnerNew= servicePartnerManager.save(servicePartnerNew);
					 maxIdNumber= servicePartnerNew.getId().toString();
					 logger.info("new routing created at corpid "+sessionCorpID+" for shipnumber "+yourSO.getShipNumber()+"with id :"+servicePartnerNew.getId());
					 logger.info("removing routing with id :"+id);
					 servicePartnerManager.removeForOtherCorpid(new Long(id));
					 logger.info(" routing removed with  id :"+id);
					 servicePartnerManager.updateTrack(servicePartnerNew.getShipNumber(),servicePartnerNew.getBlNumber(),servicePartnerNew.getHblNumber(),getRequest().getRemoteUser());
				     servicePartnerManager.updateServiceOrder(servicePartnerNew.getCarrierDeparture(), servicePartnerNew.getCarrierArrival(), servicePartnerNew.getShipNumber(),"Merge:UGWW:"+userName);
				     servicePartnerManager.updateServiceOrderDate(servicePartnerNew.getServiceOrderId(),sessionCorpID ,"Merge:UGWW:"+userName);
				     servicePartnerManager.updateServiceOrderActualDate(servicePartnerNew.getServiceOrderId(),sessionCorpID,yourSO.getCustomerFileId(),"Merge:UGWW:"+userName);
				}catch(Exception ex){
					logger.warn("Got an Error in Creating ServicePartner of linked SO. \n Reported Exception is : "+currentdate+" ## : "+ex.toString());
				}
			}
		}
		
		i = toDoRuleManager.removeAll(incomingSO.getShipNumber(),"","servicepartner");
		
		// servicepartner merge end
		
		
		
		try{
			if(yourSO.getIsNetworkRecord()){
				List linkedShipNumber= trackingStatusAction.findLinkerShipNumber(yourSO); 
				List<Object> miscellaneousRecords=trackingStatusAction.findMiscellaneousRecords(linkedShipNumber,yourSO); 
				trackingStatusAction.synchornizeMiscellaneous(miscellaneousRecords,yourMsc,yourTS);
				List<Object> trackingStatusRecords=trackingStatusAction.findTrackingStatusRecords(linkedShipNumber,yourSO);
				trackingStatusAction.synchornizeTrackingStatus(trackingStatusRecords,yourTS,yourSO);
				List<Object> serviceOrderRecords=trackingStatusAction.findServiceOrderRecords(linkedShipNumber,yourSO);
				trackingStatusAction.synchornizeServiceOrder(serviceOrderRecords,yourSO,yourTS,yourCF);
				try{ 
					 createContainerRecords(yourSO, incomingSO); 
					 createPieceCountsRecords(yourSO, incomingSO); 
					 createVehicleRecords(yourSO, incomingSO); 
					 createRoutingRecords(yourSO, incomingSO); 
					 trackingStatusAction.createConsigneeInstruction(yourSO, incomingSO);
					 }catch(Exception ex){
						logger.warn("Exception in craeting Container,routing etc Exception: "+currentdate+" ## : "+ex.toString());
					}
			}
		}catch(Exception e ){
			e.printStackTrace();
		}
		// updating networkcontrol table
		toDoRuleManager.updateNetworkControlSO(sessionCorpID,yourSO.getShipNumber(),incomingSO.getShipNumber(),"Done",childAgentCode);
		hitFlag = "Y";
		chkFlag="Merge";
		// creating SO dash board entry
		//createSODashBoard(yourCF, yourSO, yourMsc,yourBilling, yourTS);
		insertIntoTracker(incomingSO.getShipNumber(),"Merge");
		/*Long StartTime = System.currentTimeMillis();
		customerFileAction.createIntegrationLogInfo(incomingSO.getShipNumber(),"CREATE");
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken for creating line in integration log info table : "+timeTaken+"\n\n");*/
    
		todoList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	public String checkNull(Object obj){
		if(obj != null){
			return obj.toString();
		}else{
			return " ";
		}
	}
	
	public Field setFieldValue(Class cls,String fieldName){
		Field tempField = null;
		try {
			tempField = cls.getDeclaredField(fieldName.trim());
			tempField.setAccessible(true);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("error in setFieldValue "+e.getStackTrace()[0]);
		}
		return tempField;
	}
	
	@SkipValidation
	public String mergingFieldMethod() throws Exception{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		orgId = sessionCorpID;
		Long StartTime = System.currentTimeMillis();
		networkFields = toDoRuleManager.getNetworkDataFields();
		
		//SO objects 
		incomingSO = serviceOrderManager.getForOtherCorpid(soUGWWID);
		yourSO = serviceOrderManager.get(mergingSOID);
		
		// Cf objects
		inComingCF = incomingSO.getCustomerFile();
		yourCF = yourSO.getCustomerFile();
		
		// Billing objects
		inComingBilling = billingManager.getForOtherCorpid(soUGWWID);
		yourBilling = billingManager.get(mergingSOID);
		
		//TS Objects
		inComingTS = trackingStatusManager.getForOtherCorpid(soUGWWID);
		yourTS = trackingStatusManager.get(mergingSOID);
		
		//Misce Objects
		inComingMsc = miscellaneousManager.getForOtherCorpid(soUGWWID);
		yourMsc = miscellaneousManager.get(mergingSOID);
		
		Class noparams[] = {};
		resultMap = new HashMap<String, List<String>>();
		for (Map.Entry<String,List<String>> entry : networkFields.entrySet()) {
			List<String> list = new ArrayList<String>();
			
			if("CustomerFile".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.CustomerFile");
				for (String fieldName : entry.getValue()) {
					Method method;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingCF, null);
					Object obj2 = method.invoke(yourCF, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("") && obj2!=null && !obj2.toString().trim().equals("") && !obj1.toString().trim().equals(obj2.toString().trim()) ){
						list.add(checkNull(obj1)+"~"+fieldName.trim()+"~"+checkNull(obj2));
					}
				}
			}else if("ServiceOrder".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.ServiceOrder");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(incomingSO, null);
					Object obj2 = method.invoke(yourSO, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("") && obj2!=null && !obj2.toString().trim().equals("") && !obj1.toString().trim().equals(obj2.toString().trim()) ){
						list.add(checkNull(obj1)+"~"+fieldName.trim()+"~"+checkNull(obj2));
					}
				}
			}else if("Billing".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.Billing");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingBilling, null);
					Object obj2 = method.invoke(yourBilling, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("") && obj2!=null && !obj2.toString().trim().equals("") && !obj1.toString().trim().equals(obj2.toString().trim()) ){
						list.add(checkNull(obj1)+"~"+fieldName.trim()+"~"+checkNull(obj2));
					}
				}
			}else if("TrackingStatus".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.TrackingStatus");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingTS, null);
					Object obj2 = method.invoke(yourTS, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("") && obj2!=null && !obj2.toString().trim().equals("") && !obj1.toString().trim().equals(obj2.toString().trim()) ){
						list.add(checkNull(obj1)+"~"+fieldName.trim()+"~"+checkNull(obj2));
					}
				}
			}else if("Miscellaneous".equals(entry.getKey())){
				Class cls = Class.forName("com.trilasoft.app.model.Miscellaneous");
				for (String fieldName : entry.getValue()) {
					Method method ;
					try {
						method = cls.getDeclaredMethod( "get"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
					} catch (Exception e) {
						method = cls.getDeclaredMethod( "is"+fieldName.trim().replaceFirst(String.valueOf(fieldName.charAt(0)), String.valueOf(fieldName.charAt(0)).toUpperCase()).trim(), noparams);
						logger.error("Do not have property getter."+ e.getStackTrace()[0]);
					}
					Object obj1 = method.invoke(inComingMsc, null);
					Object obj2 = method.invoke(yourMsc, null);
					logger.warn("Method name is : -"+method +"Obj1 contain value : - "+obj1 +"Obj 2 contains value : - "+obj2);
					if(obj1!=null && !obj1.toString().trim().equals("") && obj2!=null && !obj2.toString().trim().equals("") && !obj1.toString().trim().equals(obj2.toString().trim()) ){
						list.add(checkNull(obj1)+"~"+fieldName.trim()+"~"+checkNull(obj2));
					}
				}
			}
			resultMap.put(entry.getKey(), list);
		}
		List <Long> l1 = toDoRuleManager.getModelIDsByShip("container",yourSO.getShipNumber(),incomingSO.getShipNumber()); 
		
		for (Long modelId : l1) {
			contList.add(containerManager.getForOtherCorpid(modelId));
		}
		
		l1 = toDoRuleManager.getModelIDsByShip("carton",yourSO.getShipNumber(),incomingSO.getShipNumber()); 
		
		for (Long modelId : l1) {
			pieceCountList.add(cartonManager.getForOtherCorpid(modelId));
		}
		
		l1 = toDoRuleManager.getModelIDsByShip("vehicle",yourSO.getShipNumber(),incomingSO.getShipNumber()); 
		
		for (Long modelId : l1) {
			vehicleList.add(vehicleManager.getForOtherCorpid(modelId));
		}
		
		l1 = toDoRuleManager.getModelIDsByShip("servicepartner",yourSO.getShipNumber(),incomingSO.getShipNumber()); 
		
		for (Long modelId : l1) {
			servPartnerList.add(servicePartnerManager.getForOtherCorpid(modelId));
		}
		//************************ Getting count of forwarding tab ***********************************
		countContainer = toDoRuleManager.getCount("container",yourSO.getShipNumber());
		countCarton = toDoRuleManager.getCount("carton",yourSO.getShipNumber());
		countVehicle = toDoRuleManager.getCount("vehicle",yourSO.getShipNumber());
		countServicePartner = toDoRuleManager.getCount("servicepartner",yourSO.getShipNumber());
		Long timeTaken =  System.currentTimeMillis() - StartTime;
		logger.warn("\n\nTime taken to display linking fileds: "+timeTaken+"\n\n");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	
	
	private List getCheckListResult(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List resultList= new ArrayList();
		resultList=checkListManager.getResultList(sessionCorpID, owner,duration, ruleId,messageDisplayed);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return resultList;
	}

	// for supervisor
	@SkipValidation
	public String getListForSuperVisor(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//getComboList(sessionCorpID);
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		try 
		{
		Map filterMap=user.getFilterMap();
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
		
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
				
				Long StartTime = System.currentTimeMillis();
				
		    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
		    	String supervisor=user.getSupervisor();
				List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
		    	
		    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
		    		isSupervisor ="true";
		    	}*/			
		    	if(!checkSupervisor.isEmpty()){
		    		isSupervisor ="true";
		    	}

		    	if(!checkSupervisor.isEmpty() && allradio.equalsIgnoreCase("Direct Report"))
				  {
					isSupervisor ="true";
					flagForSupervisor = "true" ;
					List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
					String tmep = userName.toString();
					String temp = userName.get(0).toString();
					tmep=tmep.replace("[", "");
					tmep=tmep.replace("]", "");
					temp=tmep.replace("'", "");
					temp=tmep.replace("'", "");
					//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
					userList=toDoRuleManager.getOwnersListExtCordinator(sessionCorpID,tmep,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup);
					userList.remove("UNASSIGNED");
					messageList=toDoRuleManager.getMessageListExtCordinator(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup);
			        ruleIdList=toDoRuleManager.getRuleIdListExtCordinator(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup);
			    	
					//For Today's Note // For Supervisor
					nls=toDoRuleManager.findByStatusForSupervisorExtCordinator("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup);
					notes=new HashSet(nls);
					countToday=notes.size();
					
					// For OverDue Note // For Supervisor
					nls1=toDoRuleManager.findByStatusForSupervisor1ExtCordinator("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup);
					notes1=new HashSet(nls1);
					countOverDue=notes1.size();
					
					// For Due This Week Note // For Supervisor
					nls2=toDoRuleManager.findByStatusForSupervisor2ExtCordinator("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup);
					notes2=new HashSet(nls2);
					countDueWeek=notes2.size();
							
					// For Result Overdue
					ls=toDoRuleManager.findBydurationForSupervisorExtCordinator(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,userType);
			    	toDoResults1=new ArrayList(ls);
			    	countResultOverDue=toDoResults1.size();
			    	
			    	// For Result Today
			    	lstoday=toDoRuleManager.findBydurationdtodayForSupervisorExtCordinator(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,userType);
			    	toDoResultsToday1 = new ArrayList(lstoday);
			    	countResultToday=toDoResultsToday1.size();
			    	
			    	// For Result Due This Week
			    	lsDueWeek=toDoRuleManager.findBydurationdueweekForSupervisorExtCordinator(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,userType);
			    	toDoResultsDueWeek=new HashSet(lsDueWeek);
			    	countResultDueWeek=toDoResultsDueWeek.size();
			    	
			    	// For Result checkList
			    	checkListTodayList=getCheckListResultExtCordinator(sessionCorpID,tmep,"today", ruleId,"",bookingAgentPopup,billToCodePopup,userType);
			    	checkListTodaySet = new ArrayList(checkListTodayList);
					checkListOverDueList=getCheckListResultExtCordinator(sessionCorpID,tmep,"overDue", ruleId,"",bookingAgentPopup,billToCodePopup,userType);
					checkListOverDueSet= new ArrayList(checkListOverDueList);
					countCheckListTodayResult=checkListTodayList.size();
					countCheckListOverDueResult=checkListOverDueList.size();
					//For Agent TDR Email
			    	lsEmail=toDoRuleManager.findByAgentTdrEmailExtCordinator(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,userType);
				   	countAgentTdrEmail=lsEmail.size();
				   	agentTdrEmailSet=new ArrayList(lsEmail);
					/* Linking file list for ugww 
					 * Bug# 6549 - 2Step_merging_and_linking
					 * reported by Maha Shatat 
					 * Code by Gautam Verma
					 */
					
						//Long StartTime = System.currentTimeMillis();
						 //networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
						 //networkLinkSize = networkFileList.size();
						 //Long timeTaken =  System.currentTimeMillis() - StartTime;
						 //logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");
					
					// method end of 2 step linking
				}else{
				}
		}
		
		
		
		
		
		
		
		
		else
		{
		userList=toDoRuleManager.getOwnersList(sessionCorpID,getRequest().getRemoteUser(),userId,userFirstName,userType);
		messageList=toDoRuleManager.getMessageList(sessionCorpID,getRequest().getRemoteUser(),userId,userType); 
		ruleIdList=toDoRuleManager.getRuleIdList(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		String supervisor=user.getSupervisor();
		List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
		/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			isSupervisor ="true";
		}*/		
		if(!checkSupervisor.isEmpty()){
			isSupervisor ="true";
			flagForSupervisor = "true" ;
			List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
			String tmep = userName.toString();
			String temp = userName.get(0).toString();
			tmep=tmep.replace("[", "");
			tmep=tmep.replace("]", "");
			temp=tmep.replace("'", "");
			temp=tmep.replace("'", "");
			//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
			
			//For Today's Note // For Supervisor
			nls=toDoRuleManager.findByStatusForSupervisor("NEW",sessionCorpID,tmep);
			notes=new HashSet(nls);
			countToday=notes.size();
			
			// For OverDue Note // For Supervisor
			nls1=toDoRuleManager.findByStatusForSupervisor1("NEW",sessionCorpID,tmep);
			notes1=new HashSet(nls1);
			countOverDue=notes1.size();
			
			// For Due This Week Note // For Supervisor
			nls2=toDoRuleManager.findByStatusForSupervisor2("NEW",sessionCorpID,tmep);
			notes2=new HashSet(nls2);
			countDueWeek=notes2.size();
					
			// For Result Overdue
			ls=toDoRuleManager.findBydurationForSupervisor(sessionCorpID,tmep);
	    	toDoResults1=new ArrayList(ls);
	    	countResultOverDue=toDoResults1.size();
	    	
	    	// For Result Today
	    	lstoday=toDoRuleManager.findBydurationdtodayForSupervisor(sessionCorpID,tmep);
	    	toDoResultsToday1 = new ArrayList(lstoday);
	    	countResultToday=toDoResultsToday1.size();
	    	
	    	// For Result Due This Week
	    	lsDueWeek=toDoRuleManager.findBydurationdueweekForSupervisor(sessionCorpID,tmep);
	    	toDoResultsDueWeek=new HashSet(lsDueWeek);
	    	countResultDueWeek=toDoResultsDueWeek.size();
	    	
	    	// For Result checkList
	    	checkListTodayList=getCheckListResult(sessionCorpID,temp,"today", ruleId,"");
	    	checkListTodaySet = new ArrayList(checkListTodayList);
			checkListOverDueList=getCheckListResult(sessionCorpID,temp,"overDue", ruleId,"");
			checkListOverDueSet= new ArrayList(checkListOverDueList);
			countCheckListTodayResult=checkListTodayList.size();
			countCheckListOverDueResult=checkListOverDueList.size();
			//For Agent TDR Email
	    	lsEmail=toDoRuleManager.findByAgentTdrEmail(sessionCorpID,temp);
		   	countAgentTdrEmail=lsEmail.size();
		   	agentTdrEmailSet=new ArrayList(lsEmail);
			/* Linking file list for ugww 
			 * Bug# 6549 - 2Step_merging_and_linking
			 * reported by Maha Shatat 
			 * Code by Gautam Verma
			 */
			
				//Long StartTime = System.currentTimeMillis();
				 //networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
				 //networkLinkSize = networkFileList.size();
				 //Long timeTaken =  System.currentTimeMillis() - StartTime;
				 //logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");
			
			// method end of 2 step linking
		}else{
		}}
		}
		catch(Exception e)
		{}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;   
	}
	
	@SkipValidation
	public String todoRuleList(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ls = toDoRuleManager.getByCorpID(sessionCorpID, isAgentTdr);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    toDoRules = new HashSet(ls);
    	return SUCCESS;   
    }

	@SkipValidation
	public String docCheckList(){ 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		ls = toDoRuleManager.getDocCheckListByCorpID(sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    toDoRules = new HashSet(ls);
    	return SUCCESS;   
    }
	
	public ToDoRuleManager getToDoRuleManager() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return toDoRuleManager;
	}
	public void setToDoRuleManager(ToDoRuleManager toDoRuleManager) {
		this.toDoRuleManager = toDoRuleManager;
	}
	@SkipValidation
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		try 
		{
		Map filterMap=user.getFilterMap();
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
				String userLogged="'"+getRequest().getRemoteUser()+"'";
				Long StartTime = System.currentTimeMillis();
				pentity = refMasterManager.findByParameter(corpId, "PENTITY");
				prulerole = refMasterManager.findByParameter(corpId, "PRULEROLE");
				userList=toDoRuleManager.getOwnersListExtCordinator(sessionCorpID,userLogged,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup);
				userList.remove("UNASSIGNED");
				messageList=toDoRuleManager.getMessageListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
		        ruleIdList=toDoRuleManager.getRuleIdListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
		    	notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
				notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");
				toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
				company= companyManager.findByCorpID(sessionCorpID).get(0);
		}
		else
		{
		pentity = refMasterManager.findByParameter(corpId, "PENTITY");
		prulerole = refMasterManager.findByParameter(corpId, "PRULEROLE");
		userList=toDoRuleManager.getOwnersList(sessionCorpID,getRequest().getRemoteUser(),userId,userFirstName,userType);
		messageList=toDoRuleManager.getMessageList(sessionCorpID,getRequest().getRemoteUser(),userId,userType); 
		ruleIdList=toDoRuleManager.getRuleIdList(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
		notetype = refMasterManager.findByParameter(corpId, "NOTETYPE");
		notesubtype = refMasterManager.findByParameter(corpId, "NOTESUBTYPE");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		company= companyManager.findByCorpID(sessionCorpID).get(0);
		}}
		catch(Exception e)
		{}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	 }

	private java.util.Date getCurrentDate() {
		DateFormat dfm = new SimpleDateFormat("yyyy-MM-dd");
		String dateStr = dfm.format(new Date());
		Date currentDate = null;
		try {
			currentDate = dfm.parse(dateStr);			
		} catch (java.text.ParseException e) {			
			e.printStackTrace();
		}
		return currentDate;		
	} 
	
	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
		notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
		notesubtype = refMasterManager.findByParameter(sessionCorpID, "NOTESUBTYPE");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
		if(btn.equalsIgnoreCase("Copy Rule")){
			addCopyToDoRule=toDoRuleManager.getForOtherCorpid(toDoRule.getId());
			toDoRule=new ToDoRule();
			entity=addCopyToDoRule.getEntitytablerequired();
			if(entity.equalsIgnoreCase("TrackingStatus")){		
				entity = "ServiceOrder";
			}
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);
			toDoRule.setCorpID(addCopyToDoRule.getCorpID());
			toDoRule.setCorpID(addCopyToDoRule.getCorpID());
			toDoRule.setRolelist(addCopyToDoRule.getRolelist());
			toDoRule.setCreatedOn(new Date());
			toDoRule.setSystemDate(new Date());		
			toDoRule.setUpdatedOn(new Date());
			toDoRule.setCreatedBy(getRequest().getRemoteUser());
			toDoRule.setUpdatedBy(getRequest().getRemoteUser());
			toDoRule.setEntitytablerequired(addCopyToDoRule.getEntitytablerequired());
			toDoRule.setNoteType(addCopyToDoRule.getNoteType());
			toDoRule.setNoteSubType(addCopyToDoRule.getNoteSubType());
			toDoRule.setFieldToValidate1(addCopyToDoRule.getFieldToValidate1());
			toDoRule.setFieldDisplay(addCopyToDoRule.getFieldDisplay());
			toDoRule.setTestdate(addCopyToDoRule.getTestdate());
			toDoRule.setMessagedisplayed(addCopyToDoRule.getMessagedisplayed());
			toDoRule.setDurationAddSub(addCopyToDoRule.getDurationAddSub());
			toDoRule.setExpression(addCopyToDoRule.getExpression());
			toDoRule.setStatus(addCopyToDoRule.getStatus());
			toDoRule.setUrl(addCopyToDoRule.getUrl());
			toDoRule.setFieldToValidate2(addCopyToDoRule.getFieldToValidate2());
			toDoRule.setPublishRule(addCopyToDoRule.getPublishRule());
			//toDoRule.setFieldToValidate3(addCopyToDoRule.getFieldToValidate3());			
			List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
			Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
			toDoRule.setRuleNumber(autoRuleNumber);
			toDoRule=toDoRuleManager.save(toDoRule);
			return SUCCESS;   
		}else{	
			boolean isNew = (toDoRule.getId() == null); 	
			if(isNew){
				toDoRule.setCreatedOn(new Date());
				List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
				if (maxRuleNumber.get(0) == null) {
					toDoRule.setRuleNumber(01L);
				} else {
					Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
					toDoRule.setRuleNumber(autoRuleNumber);
				}
		    } 
        Date todayDate = getCurrentDate();
		if((toDoRule.getEmailOn()==null) || ((todayDate.getTime() - toDoRule.getEmailOn().getTime())>(24 * 60 * 60 * 1000))){
			toDoRule.setEmailOn(new Date());
		}else{
			toDoRule.setEmailOn(toDoRule.getEmailOn());
		}
		toDoRule.setUpdatedOn(new Date());
		toDoRule.setUpdatedBy(getRequest().getRemoteUser());
		
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		
		if((toDoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder")) && (toDoRule.getFieldToValidate1().substring(0,14).equalsIgnoreCase("trackingStatus"))){		
			toDoRule.setUrl("editTrackingStatus");
		}else if((toDoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder")) && ((toDoRule.getFieldToValidate1().substring(0,12).equalsIgnoreCase("serviceOrder")) || (toDoRule.getFieldToValidate1().substring(0,13).equalsIgnoreCase("miscellaneous")))){		
			toDoRule.setUrl("editServiceOrderUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile")){		
			toDoRule.setUrl("editCustomerFile");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("QuotationFile")){		
			toDoRule.setUrl("QuotationFileForm");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("ServicePartner")){		
			toDoRule.setUrl("editServicePartner");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Vehicle")){		
			toDoRule.setUrl("editVehicle");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLine")){		
			toDoRule.setUrl("editAccountLine");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){		
			toDoRule.setUrl("editWorkTicketUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLineList")){		
			toDoRule.setUrl("accountLineList");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Billing")){		
			toDoRule.setUrl("editBilling");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Miscellaneous")){		
			toDoRule.setUrl("editMiscellaneous");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("DspDetails")){			
			toDoRule.setUrl("editDspDetails");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Claim")){			
			toDoRule.setUrl("editClaim");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus")){		
			toDoRule.setUrl("editTrackingStatus");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){		
			toDoRule.setUrl("editLeadCapture");
		}
		else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("CreditCard")){		
			toDoRule.setUrl("editBilling");
		}
		else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicketList")){		
			toDoRule.setUrl("customerWorkTickets");
		}
		if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){
			if(!toDoRule.getExpression().contains("customerfile.controlFlag = 'L'")){							
				toDoRule.setExpression(toDoRule.getExpression()+" and customerfile.controlFlag = 'L'");
			}
		}
		
		toDoRule= toDoRuleManager.save(toDoRule);
		String executeRules=toDoRuleManager.executeTestRule(toDoRule, sessionCorpID);	
		
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		else
			toDoRule.setCorpID(sessionCorpID);
		
		entity=toDoRule.getEntitytablerequired();
		if(entity.equalsIgnoreCase("TrackingStatus")){		
			entity = "ServiceOrder";
		}
		selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
		selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
		//getComboList(sessionCorpID);
		if(btn.equalsIgnoreCase("Test Condition")){
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				saveMessage("Rule has been executed successfully.");			
			}else{
				String sqlError = "";
				try{
					sqlError = executeRules;
					StringBuffer buffer = new StringBuffer();
					buffer.append(executeRules);
					buffer.append(" ");
					sqlError = buffer.toString();
				}catch(Exception ex){ }
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Rule cannot be executed. "+ sqlError);
			}
		}else if (btn.equalsIgnoreCase("Execute This Rule")){
			List executeThisRule= toDoRuleManager.executeThisRule(toDoRule, sessionCorpID,getRequest().getRemoteUser());
			if(executeThisRule == null ){
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}else{
			toDoRule.setStatus("Tested");
			toDoRuleManager.changestatus("Tested",toDoRule.getId());
			if(toDoRule.getDocType()!=null && !toDoRule.getDocType().equals("")){
			//createCheckResultRules(toDoRule.getRuleNumber(),toDoRule.getId());
				checkListManager.updateAndDel(sessionCorpID,toDoRule.getRuleNumber(),toDoRule.getId());
			int checkListEnterd = toDoRuleManager.findCheckListEnterd(sessionCorpID, toDoRule.getId());
				saveMessage("Rule has been executed successfully.  "   + checkListEnterd + " Record Entered");
			}else{
				toDoRuleManager.deleteUpdateByRuleNumber(toDoRule.getRuleNumber(),toDoRule.getCorpID());
				List numberOfResult=toDoRuleManager.findNumberOfResultEntered(sessionCorpID, toDoRule.getId(),toDoRule.getRolelist());
				countNumberOfResult=Integer.parseInt(numberOfResult.get(0).toString());
				saveMessage("Rule has been executed successfully.  "   + countNumberOfResult + " Record Entered");
			}
			}
		}else{
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
				String key = (isNew) ? "Rule.added" : "Rule.updated";
				saveMessage(getText(key));
				}
			}else{
				toDoRule.setCreatedOn(new Date());
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}	
		}
	    if (!isNew) {   
	        return INPUT;   
	    } else {   
	    	maxId = Long.parseLong(toDoRuleManager.findMaximumId().get(0).toString());
	    	toDoRule = toDoRuleManager.get(maxId);
	     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    } 
		}			
	}
	
	
	public String saveDocCheckList() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
		if(docCheckListbtn.equalsIgnoreCase("Copy Rule")){
			addCopyToDoRule=toDoRuleManager.getForOtherCorpid(toDoRule.getId());
			toDoRule=new ToDoRule();
			entity=addCopyToDoRule.getEntitytablerequired();
			if(entity.equalsIgnoreCase("TrackingStatus")){		
				entity = "ServiceOrder";
			}
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);
			toDoRule.setCorpID(addCopyToDoRule.getCorpID());
			toDoRule.setCorpID(addCopyToDoRule.getCorpID());
			toDoRule.setRolelist(addCopyToDoRule.getRolelist());
			toDoRule.setCreatedOn(new Date());
			toDoRule.setSystemDate(new Date());		
			toDoRule.setUpdatedOn(new Date());
			toDoRule.setCreatedBy(getRequest().getRemoteUser());
			toDoRule.setUpdatedBy(getRequest().getRemoteUser());
			toDoRule.setEntitytablerequired(addCopyToDoRule.getEntitytablerequired());
			toDoRule.setDocType(addCopyToDoRule.getDocType());
			toDoRule.setFieldDisplay(addCopyToDoRule.getFieldDisplay());
			toDoRule.setTestdate(addCopyToDoRule.getTestdate());
			toDoRule.setMessagedisplayed(addCopyToDoRule.getMessagedisplayed());
			toDoRule.setDurationAddSub(addCopyToDoRule.getDurationAddSub());
			toDoRule.setExpression(addCopyToDoRule.getExpression());
			toDoRule.setStatus(addCopyToDoRule.getStatus());
			toDoRule.setUrl(addCopyToDoRule.getUrl());
			toDoRule.setPublishRule(addCopyToDoRule.getPublishRule());
			//toDoRule.setFieldToValidate3(addCopyToDoRule.getFieldToValidate3());			
			List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
			Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
			toDoRule.setRuleNumber(autoRuleNumber);
			toDoRule=toDoRuleManager.save(toDoRule);
			return SUCCESS;   
		}else{	
			boolean isNew = (toDoRule.getId() == null); 	
			if(isNew){
				toDoRule.setCreatedOn(new Date());
				List maxRuleNumber = toDoRuleManager.findMaximum(sessionCorpID);
				if (maxRuleNumber.get(0) == null) {
					toDoRule.setRuleNumber(01L);
				} else {
					Long autoRuleNumber = Long.parseLong((maxRuleNumber.get(0).toString())) + 1;
					toDoRule.setRuleNumber(autoRuleNumber);
				}
		    } 
        Date todayDate = getCurrentDate();
		if((toDoRule.getEmailOn()==null) || ((todayDate.getTime() - toDoRule.getEmailOn().getTime())>(24 * 60 * 60 * 1000))){
			toDoRule.setEmailOn(new Date());
		}else{
			toDoRule.setEmailOn(toDoRule.getEmailOn());
		}
		toDoRule.setUpdatedOn(new Date());
		toDoRule.setUpdatedBy(getRequest().getRemoteUser());
		
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		
		 if(toDoRule.getEntitytablerequired().equalsIgnoreCase("ServiceOrder")){		
			toDoRule.setUrl("editServiceOrderUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("CustomerFile")){		
			toDoRule.setUrl("editCustomerFile");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("QuotationFile")){		
			toDoRule.setUrl("QuotationFileForm");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("ServicePartner")){		
			toDoRule.setUrl("editServicePartner");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Vehicle")){		
			toDoRule.setUrl("editVehicle");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLine")){		
			toDoRule.setUrl("editAccountLine");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicket")){		
			toDoRule.setUrl("editWorkTicketUpdate");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("AccountLineList")){		
			toDoRule.setUrl("accountLineList");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Billing")){		
			toDoRule.setUrl("editBilling");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Miscellaneous")){		
			toDoRule.setUrl("editMiscellaneous");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("DspDetails")){			
			toDoRule.setUrl("editDspDetails");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("Claim")){			
			toDoRule.setUrl("editClaim");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("TrackingStatus")){		
			toDoRule.setUrl("editTrackingStatus");
		}else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){		
			toDoRule.setUrl("editLeadCapture");
		}
		else if(toDoRule.getEntitytablerequired().equalsIgnoreCase("WorkTicketList")){		
			toDoRule.setUrl("customerWorkTickets");
		}
		if(toDoRule.getEntitytablerequired().equalsIgnoreCase("LeadCapture")){
			if(!toDoRule.getExpression().contains("customerfile.controlFlag = 'L'")){							
				toDoRule.setExpression(toDoRule.getExpression()+" and customerfile.controlFlag = 'L'");
			}
		}
		
		toDoRule= toDoRuleManager.save(toDoRule);
		String executeRules=toDoRuleManager.executeTestRule(toDoRule, sessionCorpID);	
		
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		else
			toDoRule.setCorpID(sessionCorpID);
		
		entity=toDoRule.getEntitytablerequired();
		if(entity.equalsIgnoreCase("TrackingStatus")){		
			entity = "ServiceOrder";
		}
		selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
		selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
		//getComboList(sessionCorpID);
		if(docCheckListbtn.equalsIgnoreCase("Test Condition")){
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				saveMessage("Doc Check List has been executed successfully.");			
			}else{
				String sqlError = "";
				try{
					sqlError = executeRules;
					StringBuffer buffer = new StringBuffer();
					buffer.append(executeRules);
					buffer.append(" ");
					sqlError = buffer.toString();
				}catch(Exception ex){ }
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Doc Check List cannot be executed. "+ sqlError);
			}
		}else if (docCheckListbtn.equalsIgnoreCase("Execute This Rule")){
			List executeThisRule= toDoRuleManager.executeThisRule(toDoRule, sessionCorpID,getRequest().getRemoteUser());
			if(executeThisRule == null ){
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}else{
			toDoRule.setStatus("Tested");
			toDoRuleManager.changestatus("Tested",toDoRule.getId());
				checkListManager.updateAndDel(sessionCorpID,toDoRule.getRuleNumber(),toDoRule.getId());
			int checkListEnterd = toDoRuleManager.findCheckListEnterd(sessionCorpID, toDoRule.getId());
				saveMessage("Doc Check List has been executed successfully.  "   + checkListEnterd + " Record Entered");
			}
		}else{
			if(executeRules==null){
				toDoRule.setStatus("Tested");
				toDoRuleManager.changestatus("Tested",toDoRule.getId());
				if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
				String key = (isNew) ? "Doc Check List has been added successfully" : "Doc Check List has been updated successfully";
				saveMessage(getText(key));
				}
			}else{
				toDoRule.setCreatedOn(new Date());
				toDoRule.setStatus("Error");
				toDoRuleManager.changestatus("Error",toDoRule.getId());
				errorMessage("Entered rule is not executable.");
			}	
		}
	    if (!isNew) {   
	        return INPUT;   
	    } else {   
	    	maxId = Long.parseLong(toDoRuleManager.findMaximumId().get(0).toString());
	    	toDoRule = toDoRuleManager.get(maxId);
	     	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        return SUCCESS;   
	    } 
		}			
	}
	
	
	@SkipValidation
	public String edit() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
		notetype = refMasterManager.findByParameter(sessionCorpID, "NOTETYPE");
		notesubtype = refMasterManager.findByParameter(sessionCorpID, "NOTESUBTYPE");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
		taskUser = refMasterManager.findUserNotes(sessionCorpID, "PRULEROLE");
		actionList=ruleActionsManager.getActionList(id);
		if (id != null){			
			toDoRule=toDoRuleManager.getForOtherCorpid(id);
			//toDoRule.setUpdatedOn(new Date());
			entity=toDoRule.getEntitytablerequired();
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);		
		}else{
			toDoRule=new ToDoRule();
			entity=toDoRule.getEntitytablerequired();
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);
			toDoRule.setRolelist("Coordinator");
			toDoRule.setCreatedOn(new Date());
			toDoRule.setSystemDate(new Date());		
			toDoRule.setUpdatedOn(new Date());
		}
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		else
			toDoRule.setCorpID(sessionCorpID);
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String editDocCheckList() { 
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		pentity = refMasterManager.findByParameter(sessionCorpID, "PENTITY");
		toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
		prulerole = refMasterManager.findByParameter(sessionCorpID, "PRULEROLE");
		if (id != null){			
			toDoRule=toDoRuleManager.getForOtherCorpid(id);
			//toDoRule.setUpdatedOn(new Date());
			entity=toDoRule.getEntitytablerequired();
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);		
		}else{
			toDoRule=new ToDoRule();
			entity=toDoRule.getEntitytablerequired();
			selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
			selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
			//getComboList(sessionCorpID);
			toDoRule.setRolelist("Coordinator");
			toDoRule.setCreatedOn(new Date());
			toDoRule.setSystemDate(new Date());		
			toDoRule.setUpdatedOn(new Date());
		}
		if(toDoRule.getPublishRule())
			toDoRule.setCorpID("TSFT");
		else
			toDoRule.setCorpID(sessionCorpID);
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String toDoRulePage(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");	
			if (id != null){
				entity=toDoRule.getEntitytablerequired();
				selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
				selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
				getComboList(sessionCorpID);
				toDoRule=toDoRuleManager.getForOtherCorpid(id);
			}else{
				toDoRule=new ToDoRule();
				entity=toDoRule.getEntitytablerequired();
				selectedField=toDoRuleManager.selectfield(entity,sessionCorpID);
				selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
				getComboList(sessionCorpID);
			}
			if(toDoRule.getPublishRule())
				toDoRule.setCorpID("TSFT");
			else
				toDoRule.setCorpID(sessionCorpID);
			
			toDoRule.setSystemDate(new Date());
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
	}
	@SkipValidation
	public String executeAllRules(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			 errorRuleNumberList=toDoRuleManager.executeRules(sessionCorpID); 
			int errorRules=errorRuleNumberList.size();
			List executeRules=toDoRuleManager.getAllRules(sessionCorpID);
			countNumberOfRules=executeRules.size();
			countNumberOfRules =countNumberOfRules-errorRules;
			List numberOfResult=toDoRuleManager.findNumberOfResult(sessionCorpID);
			countNumberOfResult=Integer.parseInt(numberOfResult.get(0).toString());
			toDoRuleManager.updateCompanyDivisionLastRunDate(sessionCorpID);
			errorLogManager.saveLogError(sessionCorpID,new Date(),"Manual executions of all rules","executeAllRules",getRequest().getRemoteUser() , "TDR");
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
			}catch(Exception ex){
				ex.printStackTrace();
				logger.error("Gettting error in execution of rules :", ex);
				errorMessage("Rules Can't be executed because of the Heavy traffic, please try after some time !! ");
				todoRuleList();
				return ERROR;
		 }
	}
	
	@SkipValidation
	public String executeAllDocCheckListRules(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
			 errorRuleNumberList=toDoRuleManager.executeRulesForDocCheckList(sessionCorpID);  
			int errorRules=errorRuleNumberList.size();
			List executeRules=checkListManager.getAllRules(sessionCorpID);
			countNumberOfRules=executeRules.size();
			countNumberOfRules =countNumberOfRules-errorRules;
			countNumberOfResult=toDoRuleManager.findCheckListEnterd(sessionCorpID, 0L);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			return SUCCESS;
			}catch(Exception ex){
				ex.printStackTrace();
				logger.error("Gettting error in execution of rules :", ex);
				errorMessage("Rules Can't be executed because of the Heavy traffic, please try after some time !! ");
				todoRuleList();
				return ERROR;
		 }
	}

		@SkipValidation
		public String getEmailList(){
			toEmailList = refMasterManager.findOnlyCode(param, sessionCorpID);
			//toEmail = refMasterManager.findByParameter(sessionCorpID, param);
		return SUCCESS;
		}
	
		@SkipValidation
		public String changeDropDownRule(){
		//entity=toDoRule.getEntitytablerequired();
	      fieldList=toDoRuleManager.selectfield(tableName,sessionCorpID);
		//selectedDateField=toDoRuleManager.selectDatefield(sessionCorpID);
		//getComboList(sessionCorpID);
		return SUCCESS;
		}
		
		private String fieldToValidateValue;
		@SkipValidation
		public String getDescriptionValue(){			
			int i =fieldToValidateValue.indexOf(".");
			String tableName=fieldToValidateValue.substring(0, i);
			String fieldname=fieldToValidateValue.substring(i+1, fieldToValidateValue.length());
			fieldList=toDoRuleManager.getDescription(tableName, fieldname,sessionCorpID );
			return SUCCESS;			
		}
	

		@SkipValidation
		public String search() {
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			boolean ruleNumber=(toDoRule.getRuleNumber() == null);
			boolean fieldToValidate = (toDoRule.getFieldToValidate1() == null);
			boolean testdate = (toDoRule.getTestdate() == null);
			boolean entity = (toDoRule.getEntitytablerequired() == null);
			boolean message = (toDoRule.getMessagedisplayed() == null);
			boolean status=(toDoRule.getStatus()== null);
			boolean checkEnable=(toDoRule.getCheckEnable()==null);
			if(!ruleNumber|| !fieldToValidate || !testdate || !entity || !message || !status || !checkEnable) {
				ls=toDoRuleManager.searchById(toDoRule.getRuleNumber(),toDoRule.getFieldToValidate1(),toDoRule.getTestdate(), toDoRule.getEntitytablerequired(), toDoRule.getMessagedisplayed(),toDoRule.getStatus(),toDoRuleCheckEnable,isAgentTdr);
				toDoRules = new HashSet(ls);
			}
			//toDoRules = new HashSet(ls);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		    return SUCCESS;   
		}
	
	@SkipValidation
	public String searchDocCheckList() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		boolean ruleNumber=(toDoRule.getRuleNumber() == null);
		boolean docType = (toDoRule.getDocType() == null);
		boolean testdate = (toDoRule.getTestdate() == null);
		boolean entity = (toDoRule.getEntitytablerequired() == null);
		boolean message = (toDoRule.getMessagedisplayed() == null);
		boolean status=(toDoRule.getStatus()== null);
		boolean checkEnable=(toDoRule.getCheckEnable()==null);
		if(!ruleNumber|| !docType || !testdate || !entity || !message || !status || !checkEnable) {
			ls=toDoRuleManager.searchByDocCheckListId(toDoRule.getRuleNumber(),toDoRule.getDocType(),toDoRule.getTestdate(), toDoRule.getEntitytablerequired(), toDoRule.getMessagedisplayed(),toDoRule.getStatus(),toDoRuleCheckEnable);
			toDoRules = new HashSet(ls);
		}
		//toDoRules = new HashSet(ls);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    return SUCCESS;   
	}
	
	public String saveOnTabChange() throws Exception {
		validateFormNav = "OK";
    	String s = save();
    	return gotoPageString;
    }
    private String allradio;
	@SkipValidation
	public String ownerResult(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		id = Long.parseLong(systemDefaultManager.findIdByCorpID(sessionCorpID).get(0).toString());
		if (id != null) {
			systemDefault = systemDefaultManager.get(id);
		}
		int daystoManageAlerts=systemDefault.getDaystoManageAlert();
		String checkownerName="";
		try 
		{
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user1 = (User) auth.getPrincipal(); 
			
			if(ownerName==null){
				ownerName="";
			}
			  checkownerName=ownerName;
				
		Map filterMap=user1.getFilterMap();
		
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
			
			
			
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
			    	String finalowner="";
				Long StartTime = System.currentTimeMillis();
				user = userManager.getUserByUsername(getRequest().getRemoteUser());
		    	String supervisor=user.getSupervisor();
				List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
				
		    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
		    		isSupervisor ="true";
		    	}*/	
				String tmep2="";
				String tmep = "";
				String temp ="";
				String temp1="";
		    	if(!checkSupervisor.isEmpty()){
		    		isSupervisor ="true";
		    	}
		    	if(ownerName==null){
					ownerName="''";
					 finalowner="'"+ownerName+"'";
				}
				  checkownerName=ownerName;

				  checkownerName="'"+ownerName+"'";
				  String userLogged="'"+getRequest().getRemoteUser()+"'";
						if(allradio.equalsIgnoreCase("Own"))	
						{
							userList=toDoRuleManager.getOwnersListExtCordinator(sessionCorpID,userLogged,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup);
							userList.remove("UNASSIGNED");
					        
					        messageList=toDoRuleManager.getMessageListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
					        
					    	ruleIdList=toDoRuleManager.getRuleIdListExtCordinator(sessionCorpID,userLogged,userId,userType,bookingAgentPopup,billToCodePopup);
					    	
							    	
						}
						if(allradio.equalsIgnoreCase("Direct Report"))	
						{
							
							List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
							 tmep = userName.toString();
							 temp = userName.get(0).toString();
							tmep=tmep.replace("[", "");
							tmep=tmep.replace("]", "");
							temp=tmep.replace("'", "");
							temp=tmep.replace("'", "");
							//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
							userList=toDoRuleManager.getOwnersListExtCordinator(sessionCorpID,tmep,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup);
							userList.remove("UNASSIGNED");
							messageList=toDoRuleManager.getMessageListExtCordinator(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup);
					        ruleIdList=toDoRuleManager.getRuleIdListExtCordinator(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup);
					    	
						}
						if(allradio.equalsIgnoreCase("Team") )	
						{
								
								/*List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
								if(userName!=null && !userName.isEmpty() && userName.get(0)!=null&& (!(userName.get(0).toString().trim().equals(""))))
				                {
								 tmep = userName.toString();
								 temp = userName.get(0).toString();
								tmep=tmep.replace("[", "");
								tmep=tmep.replace("]", "");
								temp=tmep.replace("'", "");
								temp=tmep.replace("'", "");
								finalowner=tmep;
								userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
								userList.remove("UNASSIGNED");
								messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
						        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
								
				                }
								else
								{*/
									List supervisorTeam=toDoRuleManager.getSupervisorForTeam(user.getUsername(),sessionCorpID);
									if(supervisorTeam!=null && !supervisorTeam.isEmpty() && supervisorTeam.get(0)!=null && (!(supervisorTeam.get(0).toString().trim().equals(""))))
					                {
					                
										 tmep2 = supervisorTeam.toString();
										 temp1 = supervisorTeam.get(0).toString();
										tmep2=tmep2.replace("[", "");
										tmep2=tmep2.replace("]", "");
										temp1=tmep2.replace("'", "");
										temp1=tmep2.replace("'", "");
										List userName=toDoRuleManager.findUserForSupervisor(temp1,sessionCorpID);
										 tmep = userName.toString();
										 temp = userName.get(0).toString();
										tmep=tmep.replace("[", "");
										tmep=tmep.replace("]", "");
										temp=tmep.replace("'", "");
										temp=tmep.replace("'", "");
										finalowner=tmep+","+tmep2;
										
										
								
								userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
								userList.remove("UNASSIGNED");
								messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
						        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
					                }
					               // }
						}
						
						if(allradio.equalsIgnoreCase("All"))
						{
							userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,"''",userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							userList.remove("UNASSIGNED");
							messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,"''",userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
					        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,"''",userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							
							
						}
						
						
						
				nls=toDoRuleManager.findByStatusExtCordinator("NEW",sessionCorpID,checkownerName,bookingAgentPopup,billToCodePopup);
				notes=new HashSet(nls);
				countToday=notes.size();
				
				// For OverDue Note
				nls1=toDoRuleManager.findByStatus1ExtCordinator("NEW",sessionCorpID,checkownerName,bookingAgentPopup,billToCodePopup);
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();

			   	//For alert history
				auditTrailList = auditTrailManager.findByAlertListFilterForExtCoordinator(sessionCorpID,checkownerName,daystoManageAlerts,bookingAgentPopup,billToCodePopup);
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();
				
				// For Due This Week Note
				nls2=toDoRuleManager.findByStatus2ExtCordinator("NEW",sessionCorpID,checkownerName,bookingAgentPopup,billToCodePopup);
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
				
				// For Result Coming UP
		    	//lsComingUp = toDoRuleManager.findByDurationComingUpUser(sessionCorpID,ownerName,messageDisplayed,ruleId,shipperName,user);
		    	//toDoResultsComingUp = new HashSet(lsComingUp);
		    	//countResultComingUp = toDoResultsComingUp.size();
				
				// For Result OverDue
				ls=toDoRuleManager.findBydurationUserExtCordinator(sessionCorpID,checkownerName,messageDisplayed,ruleId,shipperName,user,bookingAgentPopup,billToCodePopup);
				toDoResults1=new ArrayList(ls);
		    	countResultOverDue=toDoResults1.size();
		    	
		    	// For Result Today
		    	lstoday=toDoRuleManager.findBydurationdtodayUserExtCordinator(sessionCorpID,checkownerName,messageDisplayed,ruleId,shipperName,user,bookingAgentPopup,billToCodePopup);
		    	toDoResultsToday1=new ArrayList(lstoday);
		    	countResultToday=toDoResultsToday1.size();
		    	
		    	// For Result Due This Week
		    	//lsDueWeek=toDoRuleManager.findBydurationdueweekUser(sessionCorpID,ownerName);
		    	//toDoResultsDueWeek=new HashSet(lsDueWeek);
		    	//countResultDueWeek=toDoResultsDueWeek.size();
		    	
		    	checkListTodayList=getCheckListResultExtCordinator(sessionCorpID,checkownerName,"today",ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,user.getUserType());
				checkListOverDueList=getCheckListResultExtCordinator(sessionCorpID,checkownerName,"overDue",ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,user.getUserType());
				checkListTodaySet = new ArrayList(checkListTodayList);
				checkListOverDueSet= new ArrayList(checkListOverDueList);
				countCheckListTodayResult=checkListTodayList.size();
				countCheckListOverDueResult=checkListOverDueList.size();
				if(checkownerName==null || checkownerName.equals("")  ){
					ownerName="";
				}
				
				lsEmail=toDoRuleManager.findByAgentTdrEmailExtCordinator(sessionCorpID,checkownerName,bookingAgentPopup,billToCodePopup,userType);
			   	countAgentTdrEmail=lsEmail.size();
			   	agentTdrEmailSet=new ArrayList(lsEmail);	
				
				  
			  
			   	  
				  
					  
				  
				  
		}
		else
		{
			user = userManager.getUserByUsername(getRequest().getRemoteUser());
			String supervisor=user.getSupervisor();
			List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
			
			/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
				isSupervisor ="true";
			}*/	
			if(ownerName==null){
				ownerName="";
			}
			  checkownerName=ownerName;
			  if( filterCheck!=null && !filterCheck.isEmpty()){
		            ownerName=user.getUsername();
		   		}
			if(!checkSupervisor.isEmpty()){
				isSupervisor ="true";
			}

		nls=toDoRuleManager.findByStatus("NEW",sessionCorpID,ownerName);
		notes=new HashSet(nls);
		countToday=notes.size();
		
		// For OverDue Note
		nls1=toDoRuleManager.findByStatus1("NEW",sessionCorpID,ownerName);
		notes1=new HashSet(nls1);
		countOverDue=notes1.size();

	   	//For alert history
		auditTrailList = auditTrailManager.findByAlertListFilter(sessionCorpID,ownerName,daystoManageAlerts);
		toDayAlertList = new HashSet(auditTrailList);
		countAlertToday=toDayAlertList.size();
		
		// For Due This Week Note
		nls2=toDoRuleManager.findByStatus2("NEW",sessionCorpID,ownerName);
		notes2=new HashSet(nls2);
		countDueWeek=notes2.size();
		
		// For Result Coming UP
    	//lsComingUp = toDoRuleManager.findByDurationComingUpUser(sessionCorpID,ownerName,messageDisplayed,ruleId,shipperName,user);
    	//toDoResultsComingUp = new HashSet(lsComingUp);
    	//countResultComingUp = toDoResultsComingUp.size();
		
		// For Result OverDue
		ls=toDoRuleManager.findBydurationUser(sessionCorpID,ownerName,messageDisplayed,ruleId,shipperName,user);
		toDoResults1=new ArrayList(ls);
    	countResultOverDue=toDoResults1.size();
    	
    	// For Result Today
    	lstoday=toDoRuleManager.findBydurationdtodayUser(sessionCorpID,ownerName,messageDisplayed,ruleId,shipperName,user);
    	toDoResultsToday1=new ArrayList(lstoday);
    	countResultToday=toDoResultsToday1.size();
    	
    	// For Result Due This Week
    	//lsDueWeek=toDoRuleManager.findBydurationdueweekUser(sessionCorpID,ownerName);
    	//toDoResultsDueWeek=new HashSet(lsDueWeek);
    	//countResultDueWeek=toDoResultsDueWeek.size();
    	
    	checkListTodayList=getCheckListResult(sessionCorpID,ownerName,"today",ruleId,messageDisplayed);
		checkListOverDueList=getCheckListResult(sessionCorpID,ownerName,"overDue",ruleId,messageDisplayed);
		checkListTodaySet = new ArrayList(checkListTodayList);
		checkListOverDueSet= new ArrayList(checkListOverDueList);
		countCheckListTodayResult=checkListTodayList.size();
		countCheckListOverDueResult=checkListOverDueList.size();
		if(checkownerName==null || checkownerName.equals("")  ){
			ownerName="";
		}
		
		lsEmail=toDoRuleManager.findByAgentTdrEmail(sessionCorpID,ownerName);
	   	countAgentTdrEmail=lsEmail.size();
	   	agentTdrEmailSet=new ArrayList(lsEmail);	
		}}
		catch(Exception e)
		{
			
			
		}
		/* Linking file list for ugww 
		 * Bug# 6549 - 2Step_merging_and_linking
		 * reported by Maha Shatat 
		 * Code by Gautam Verma
		 */
		//Long StartTime = System.currentTimeMillis();
		 //networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
		 //networkLinkSize = networkFileList.size();
		 //Long timeTaken =  System.currentTimeMillis() - StartTime;
		 //logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");
		// method end of 2 step linking
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	@SkipValidation
	public String messageResult()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		String supervisor=user.getSupervisor();
		List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
		/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			isSupervisor ="true";
		}*/
		
		if(!checkSupervisor.isEmpty()){
			isSupervisor ="true";
		}
		getComboList(sessionCorpID);
		ls=toDoRuleManager.findMessageOverDue(sessionCorpID,messageDisplayed, user.getUsername());
    	toDoResults=new HashSet(ls);
    	countResultOverDue=toDoResults.size();
    	
    	// For Result Today
    	lstoday=toDoRuleManager.findMessageToday(sessionCorpID,messageDisplayed, user.getUsername());
    	toDoResultsToday=new HashSet(lstoday);
    	countResultToday=toDoResultsToday.size();
    	
    	checkListTodayList=getCheckListResult(sessionCorpID,"","today",ruleId,messageDisplayed);
		checkListOverDueList=getCheckListResult(sessionCorpID,"","overDue",ruleId,messageDisplayed);
		countCheckListTodayResult=checkListTodayList.size();
		countCheckListOverDueResult=checkListOverDueList.size();
		/* Linking file list for ugww 
		 * Bug# 6549 - 2Step_merging_and_linking
		 * reported by Maha Shatat 
		 * Code by Gautam Verma
		 */
		//Long StartTime = System.currentTimeMillis();
		 //networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
		 //networkLinkSize = networkFileList.size();
		// Long timeTaken =  System.currentTimeMillis() - StartTime;
		 //logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");
		
		// method end of 2 step linking
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}	

	@SkipValidation
	public String setIsViewFlag(){
		setIsViewHistoryFlag="NO";
		if(!historyId.equalsIgnoreCase("")){
			toDoRuleManager.setViewFieldOfHistory(Long.parseLong(historyId));
			setIsViewHistoryFlag="YES";
		}
		return SUCCESS;
	}
	@SkipValidation
	public String makeAsViewed()
	{
		setIsViewHistoryFlag="NO";	
		if(!newIdList.equalsIgnoreCase("")){
			toDoRuleManager.bulkUpdateViewFieldOfHistory(newIdList);
			setIsViewHistoryFlag="YES";
		}			
		return SUCCESS;
	}
	@SkipValidation
	public String idResult()
	{
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		user = userManager.getUserByUsername(getRequest().getRemoteUser());
		String supervisor=user.getSupervisor();
		List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
		/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			isSupervisor ="true";
		}*/		
		if(!checkSupervisor.isEmpty()){
			isSupervisor ="true";
		}
		getComboList(sessionCorpID);	
		ls=toDoRuleManager.findIDOverDue(sessionCorpID,ruleId, user.getUsername());
    	toDoResults=new HashSet(ls);
    	countResultOverDue=toDoResults.size();
    	
    	// For Result Today
    	lstoday=toDoRuleManager.findIdToday(sessionCorpID,ruleId, user.getUsername());
    	toDoResultsToday=new HashSet(lstoday);
    	countResultToday=toDoResultsToday.size();   	
    	checkListTodayList=getCheckListResult(sessionCorpID,"","today",ruleId,"");
		checkListOverDueList=getCheckListResult(sessionCorpID,"","overDue",ruleId,"");
		countCheckListTodayResult=checkListTodayList.size();
		countCheckListOverDueResult=checkListOverDueList.size();
		/* Linking file list for ugww 
		 * Bug# 6549 - 2Step_merging_and_linking
		 * reported by Maha Shatat 
		 * Code by Gautam Verma
		 */
		/*Long StartTime = System.currentTimeMillis();
		 networkFileList = toDoRuleManager.findNetwokLinkedFiles(sessionCorpID); 
		 networkLinkSize = networkFileList.size();
		 Long timeTaken =  System.currentTimeMillis() - StartTime;
		 logger.warn("\n\nTime taken to execute linking list: "+timeTaken+"\n\n");*/
		
		// method end of 2 step linking
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	@SkipValidation
	public void updateUserCheck(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			toDoRuleManager.updateUserCheck(recordId, getRequest().getRemoteUser(), fieldToValidate);
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	@SkipValidation
	public void updateUserCheckForCheckList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			toDoRuleManager.updateUserCheckForCheck(recordId, getRequest().getRemoteUser());
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
	
	private List toDoRulesSummary;
	@SkipValidation
	public String openGroupByRules(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		Map filterMap=user.getFilterMap();
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
			 String finalowner="";
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
			
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
			    	Long StartTime = System.currentTimeMillis();
					
			    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
			    	String supervisor=user.getSupervisor();
					List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
			    	
			    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			    		isSupervisor ="true";
			    	}*/	
					String tmep2="";
					String tmep = "";
					String temp ="";
					String temp1="";
			    	if(!checkSupervisor.isEmpty()){
			    		isSupervisor ="true";
			    	}
			   String 	userLogged="'"+getRequest().getRemoteUser()+"'";
			    	
			    	if(allradio.equalsIgnoreCase("Own"))	
					{
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesSummaryForGroupByRules(sessionCorpID, userLogged,bookingAgentPopup,billToCodePopup,"",userType) ;

						    	
					}
					if(allradio.equalsIgnoreCase("Direct Report"))	
					{
						
						List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
						tmep = userName.toString();
						temp = userName.get(0).toString();
						tmep=tmep.replace("[", "");
						tmep=tmep.replace("]", "");
						temp=tmep.replace("'", "");
						temp=tmep.replace("'", "");
						//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesSummaryForGroupByRules(sessionCorpID, tmep,bookingAgentPopup,billToCodePopup,"",userType) ;
				    	
					}
					if(allradio.equalsIgnoreCase("Team") )	
					{
							
							/*List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
							if(userName!=null && !userName.isEmpty() && userName.get(0)!=null&& (!(userName.get(0).toString().trim().equals(""))))
			                {
							 tmep = userName.toString();
							 temp = userName.get(0).toString();
							tmep=tmep.replace("[", "");
							tmep=tmep.replace("]", "");
							temp=tmep.replace("'", "");
							temp=tmep.replace("'", "");
							finalowner=tmep;
							userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							userList.remove("UNASSIGNED");
							messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
					        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							
			                }
							else
							{*/
								List supervisorTeam=toDoRuleManager.getSupervisorForTeam(user.getUsername(),sessionCorpID);
								if(supervisorTeam!=null && !supervisorTeam.isEmpty() && supervisorTeam.get(0)!=null && (!(supervisorTeam.get(0).toString().trim().equals(""))))
				                {
				                
									 tmep2 = supervisorTeam.toString();
									 temp1 = supervisorTeam.get(0).toString();
									tmep2=tmep2.replace("[", "");
									tmep2=tmep2.replace("]", "");
									temp1=tmep2.replace("'", "");
									temp1=tmep2.replace("'", "");
									List userName=toDoRuleManager.findUserForSupervisor(temp1,sessionCorpID);
									 tmep = userName.toString();
									 temp = userName.get(0).toString();
									tmep=tmep.replace("[", "");
									tmep=tmep.replace("]", "");
									temp=tmep.replace("'", "");
									temp=tmep.replace("'", "");
									finalowner=tmep+","+tmep2;
									
						    toDoRulesSummary=toDoRuleManager.getToDoRulesSummaryForGroupByRules(sessionCorpID, finalowner,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),userType) ;
				                }
				               // }
					}
					
					if(allradio.equalsIgnoreCase("All"))
					{
					    toDoRulesSummary=toDoRuleManager.getToDoRulesSummaryForGroupByRules(sessionCorpID, "''",bookingAgentPopup,billToCodePopup,"",userType) ;
					}
					
		}
		else
		{
		toDoRulesSummary=toDoRuleManager.getToDoRulesSummary(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	@SkipValidation
	public String openGroupByPerson(){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		Map filterMap=user.getFilterMap();
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			 String tmep2="";
				String tmep = "";
				String temp ="";
				String temp1="";
				String finalowner="";
				  String 	userLogged="'"+getRequest().getRemoteUser()+"'";
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
			
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
			    	Long StartTime = System.currentTimeMillis();
					
			    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
			    	String supervisor=user.getSupervisor();
					List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
			    	
			    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			    		isSupervisor ="true";
			    	}*/			
			    	if(!checkSupervisor.isEmpty()){
			    		isSupervisor ="true";
			    	}

			    	if(!checkSupervisor.isEmpty() && allradio.equalsIgnoreCase("Direct Report"))
					  {
						isSupervisor ="true";
						flagForSupervisor = "true" ;
						List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
						
					  }
			    	if(allradio.equalsIgnoreCase("Own"))	
					{
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesPersonSummaryForGroupByPerson(sessionCorpID, userLogged,bookingAgentPopup,billToCodePopup,"",userType) ;

						    	
					}
					if(allradio.equalsIgnoreCase("Direct Report"))	
					{
						
						List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
						tmep = userName.toString();
						temp = userName.get(0).toString();
						tmep=tmep.replace("[", "");
						tmep=tmep.replace("]", "");
						temp=tmep.replace("'", "");
						temp=tmep.replace("'", "");
						//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesPersonSummaryForGroupByPerson(sessionCorpID, tmep,bookingAgentPopup,billToCodePopup,"",userType) ;
				    	
					}
					if(allradio.equalsIgnoreCase("Team") )	
					{
							
							/*List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
							if(userName!=null && !userName.isEmpty() && userName.get(0)!=null&& (!(userName.get(0).toString().trim().equals(""))))
			                {
							 tmep = userName.toString();
							 temp = userName.get(0).toString();
							tmep=tmep.replace("[", "");
							tmep=tmep.replace("]", "");
							temp=tmep.replace("'", "");
							temp=tmep.replace("'", "");
							finalowner=tmep;
							userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							userList.remove("UNASSIGNED");
							messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
					        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,finalowner,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
							
			                }
							else
							{*/
								List supervisorTeam=toDoRuleManager.getSupervisorForTeam(user.getUsername(),sessionCorpID);
								if(supervisorTeam!=null && !supervisorTeam.isEmpty() && supervisorTeam.get(0)!=null && (!(supervisorTeam.get(0).toString().trim().equals(""))))
				                {
				                
									 tmep2 = supervisorTeam.toString();
									 temp1 = supervisorTeam.get(0).toString();
									tmep2=tmep2.replace("[", "");
									tmep2=tmep2.replace("]", "");
									temp1=tmep2.replace("'", "");
									temp1=tmep2.replace("'", "");
									List userName=toDoRuleManager.findUserForSupervisor(temp1,sessionCorpID);
									 tmep = userName.toString();
									 temp = userName.get(0).toString();
									tmep=tmep.replace("[", "");
									tmep=tmep.replace("]", "");
									temp=tmep.replace("'", "");
									temp=tmep.replace("'", "");
									finalowner=tmep+","+tmep2;
									
						    toDoRulesSummary=toDoRuleManager.getToDoRulesPersonSummaryForGroupByPerson(sessionCorpID, finalowner,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),userType) ;
				                }
				               // }
					}
					
					if(allradio.equalsIgnoreCase("All"))
					{
					    toDoRulesSummary=toDoRuleManager.getToDoRulesPersonSummaryForGroupByPerson(sessionCorpID, "''",bookingAgentPopup,billToCodePopup,"",userType) ;
					}
					
		}
		else
		{
		toDoRulesSummary=toDoRuleManager.getToDoRulesPersonSummary(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
		
		}return SUCCESS; 
	}
	@SkipValidation
	public String openGroupByShipper(){
		System.out.println(allradio);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		Map filterMap=user.getFilterMap();
			 List list3 = new ArrayList();
			 List list4 = new ArrayList();
			 String bookingAgentPopup="";
			 String billToCodePopup="";
			 List companydivisionBookingAgentList= new ArrayList();
			 String bookingAgentSet="";
			 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
			 boolean checkFieldVisibilityOwnTeamAndAll=false;
			 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
			 String tmep2="";
				String tmep = "";
				String temp ="";
				String temp1="";
				  String 	userLogged="'"+getRequest().getRemoteUser()+"'";
		if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
			
				
				if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list4.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
				bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
			    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
						
			    		List partnerCorpIDList = new ArrayList(); 
							HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
							if(valueMap!=null){
								Iterator mapIterator = valueMap.entrySet().iterator();
								while (mapIterator.hasNext()) {
									Map.Entry entry = (Map.Entry) mapIterator.next();
									String key = (String) entry.getKey(); 
								    partnerCorpIDList= (List)entry.getValue(); 
								}
							}
							
			    	Iterator listIterator= partnerCorpIDList.iterator();
			    	while (listIterator.hasNext()) {
			    		String code=listIterator.next().toString();
			    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
			    		list3.add("'" + code + "'");
			    		
			    		}
			    			
			    	} 
					}
			    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
			    	Long StartTime = System.currentTimeMillis();
			    	String finalowner="";
			    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
			    	String supervisor=user.getSupervisor();
					List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
			    	
			    	/*if(getRequest().getRemoteUser().equalsIgnoreCase(supervisor)){
			    		isSupervisor ="true";
			    	}*/			
			    	if(!checkSupervisor.isEmpty()){
			    		isSupervisor ="true";
			    	}

			    	if(!checkSupervisor.isEmpty() && allradio.equalsIgnoreCase("Direct Report"))
					  {
						isSupervisor ="true";
						flagForSupervisor = "true" ;
						List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
						
					  }
			    	if(allradio.equalsIgnoreCase("Own"))	
					{
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesShipperSummaryForGroupByShipper(sessionCorpID, userLogged,bookingAgentPopup,billToCodePopup,"",userType) ;

						    	
					}
					if(allradio.equalsIgnoreCase("Direct Report"))	
					{
						
						List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
						tmep = userName.toString();
						temp = userName.get(0).toString();
						tmep=tmep.replace("[", "");
						tmep=tmep.replace("]", "");
						temp=tmep.replace("'", "");
						temp=tmep.replace("'", "");
						//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
			    		toDoRulesSummary=toDoRuleManager.getToDoRulesShipperSummaryForGroupByShipper(sessionCorpID, tmep,bookingAgentPopup,billToCodePopup,"",userType) ;
				    	
					}
					if(allradio.equalsIgnoreCase("Team") )	
					{
							
							
								List supervisorTeam=toDoRuleManager.getSupervisorForTeam(user.getUsername(),sessionCorpID);
								if(supervisorTeam!=null && !supervisorTeam.isEmpty() && supervisorTeam.get(0)!=null && (!(supervisorTeam.get(0).toString().trim().equals(""))))
				                {
				                
									 tmep2 = supervisorTeam.toString();
									 temp1 = supervisorTeam.get(0).toString();
									tmep2=tmep2.replace("[", "");
									tmep2=tmep2.replace("]", "");
									temp1=tmep2.replace("'", "");
									temp1=tmep2.replace("'", "");
									List userName=toDoRuleManager.findUserForSupervisor(temp1,sessionCorpID);
									 tmep = userName.toString();
									 temp = userName.get(0).toString();
									tmep=tmep.replace("[", "");
									tmep=tmep.replace("]", "");
									temp=tmep.replace("'", "");
									temp=tmep.replace("'", "");
									finalowner=tmep+","+tmep2;

						    toDoRulesSummary=toDoRuleManager.getToDoRulesShipperSummaryForGroupByShipper(sessionCorpID, finalowner,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),userType) ;
				                }
				               // }
					}
					
					if(allradio.equalsIgnoreCase("All"))
					{
					    toDoRulesSummary=toDoRuleManager.getToDoRulesShipperSummaryForGroupByShipper(sessionCorpID, "''",bookingAgentPopup,billToCodePopup,"",userType) ;
					}
					
		}
		else{
		toDoRulesSummary=toDoRuleManager.getToDoRulesShipperSummary(sessionCorpID,getRequest().getRemoteUser(),userId,userType);
		
		}logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS; 
	}
	
	@SkipValidation
	public String createCheckResultRules(Long currentRuleNumber,Long ruleId){
		checkListManager.executeTdrCheckList(sessionCorpID,currentRuleNumber,ruleId); 
		checkListManager.updateAndDel(sessionCorpID,currentRuleNumber,ruleId);
		return SUCCESS;
	}
	private List getCheckListResultExtCordinator(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed, String bookingAgentPopup,String billToCodePopup,String userType) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List resultList= new ArrayList();
		resultList=checkListManager.getResultListExtCordinator(sessionCorpID, owner,duration, ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,userType);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return resultList;
	}
	
	private List getCheckListResultExtCordinatorForTeam(String sessionCorpID, String owner, String duration,String ruleId,String messageDisplayed, String bookingAgentPopup,String billToCodePopup,String userName,String allradio,String userType) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		List resultList= new ArrayList();
		resultList=checkListManager.getResultListExtCordinatorForTeam(sessionCorpID, owner,duration, ruleId,messageDisplayed,bookingAgentPopup,billToCodePopup,userName,allradio,userType);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return resultList;
	}

	@SkipValidation
	public String getListForTeam(){ 
	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	User user = (User) auth.getPrincipal(); 
	Map filterMap=user.getFilterMap();
		 List list3 = new ArrayList();
		 List list4 = new ArrayList();
		 String bookingAgentPopup="";
		 String billToCodePopup="";
		 List companydivisionBookingAgentList= new ArrayList();
		 String bookingAgentSet="";
		 String permKey = sessionCorpID +"-"+"component.todo.ownteamandall";
		 boolean checkFieldVisibilityOwnTeamAndAll=false;
		 checkFieldVisibilityOwnTeamAndAll=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;
	if((filterMap.containsKey("serviceOrderBookingAgentCodeFilter") || filterMap.containsKey("serviceOrderBillToCodeFilter")) || checkFieldVisibilityOwnTeamAndAll){
		
			
			if(filterMap.containsKey("serviceOrderBookingAgentCodeFilter")){
				
	    		List partnerCorpIDList = new ArrayList(); 
					HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBookingAgentCodeFilter");
					if(valueMap!=null){
						Iterator mapIterator = valueMap.entrySet().iterator();
						while (mapIterator.hasNext()) {
							Map.Entry entry = (Map.Entry) mapIterator.next();
							String key = (String) entry.getKey(); 
						    partnerCorpIDList= (List)entry.getValue(); 
						}
					}
					
	    	Iterator listIterator= partnerCorpIDList.iterator();
	    	while (listIterator.hasNext()) {
	    		String code=listIterator.next().toString();
	    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
	    		list4.add("'" + code + "'");
	    		
	    		}
	    			
	    	} 
			}
			bookingAgentPopup = list4.toString().replace("[","").replace("]", "");
		    	if(filterMap.containsKey("serviceOrderBillToCodeFilter")){
					
		    		List partnerCorpIDList = new ArrayList(); 
						HashMap valueMap=	(HashMap)filterMap.get("serviceOrderBillToCodeFilter");
						if(valueMap!=null){
							Iterator mapIterator = valueMap.entrySet().iterator();
							while (mapIterator.hasNext()) {
								Map.Entry entry = (Map.Entry) mapIterator.next();
								String key = (String) entry.getKey(); 
							    partnerCorpIDList= (List)entry.getValue(); 
							}
						}
						
		    	Iterator listIterator= partnerCorpIDList.iterator();
		    	while (listIterator.hasNext()) {
		    		String code=listIterator.next().toString();
		    		if(code!=null && (!(code.trim().equalsIgnoreCase("--NO-FILTER--"))) && (!(code.trim().equalsIgnoreCase("")))){ 
		    		list3.add("'" + code + "'");
		    		
		    		}
		    			
		    	} 
				}
		    	billToCodePopup = list3.toString().replace("[","").replace("]", "");
		    	String tmep2="";
				String temp1="";
				String tmep="";
				String temp="";
			Long StartTime = System.currentTimeMillis();
			
	    	user = userManager.getUserByUsername(getRequest().getRemoteUser());
	    	String supervisor=user.getSupervisor();
			List checkSupervisor=toDoRuleManager.checkSupervisor(user.getUsername());
			int daystoManageAlerts=systemDefault.getDaystoManageAlert();
	    	if(!checkSupervisor.isEmpty()){
	    		isSupervisor ="true";
	    	}

				/*List userName=toDoRuleManager.findUserForSupervisor(user.getUsername(),sessionCorpID);
				if(userName!=null && !userName.isEmpty() && userName.get(0)!=null && (!(userName.get(0).toString().trim().equals(""))))
                {
				 tmep = userName.toString();
				 temp = userName.get(0).toString();
				tmep=tmep.replace("[", "");
				tmep=tmep.replace("]", "");
				temp=tmep.replace("'", "");
				temp=tmep.replace("'", "");
                
				
				//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
				userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,tmep,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				userList.remove("UNASSIGNED");
				messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,tmep,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	
				//For Today's Note // For Supervisor
				nls=toDoRuleManager.findByStatusForSupervisorExtCordinatorForTeam("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes=new HashSet(nls);
				countToday=notes.size();
				
				// For OverDue Note // For Supervisor
				nls1=toDoRuleManager.findByStatusForSupervisor1ExtCordinatorForTeam("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				
				// For Due This Week Note // For Supervisor
				nls2=toDoRuleManager.findByStatusForSupervisor2ExtCordinatorForTeam("NEW",sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
						
				// For Result Overdue
				ls=toDoRuleManager.findBydurationForSupervisorExtCordinatorForTeam(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	toDoResults1=new ArrayList(ls);
		    	countResultOverDue=toDoResults1.size();
		    	
		    	// For Result Today
		    	lstoday=toDoRuleManager.findBydurationdtodayForSupervisorExtCordinatorForTeam(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	toDoResultsToday1 = new ArrayList(lstoday);
		    	countResultToday=toDoResultsToday1.size();
		    	
		    	// For Result Due This Week
		    	lsDueWeek=toDoRuleManager.findBydurationdueweekForSupervisorExtCordinatorForTeam(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	toDoResultsDueWeek=new HashSet(lsDueWeek);
		    	countResultDueWeek=toDoResultsDueWeek.size();
		    	
		    	// For Result checkList
		    	checkListTodayList=getCheckListResultExtCordinatorForTeam(sessionCorpID,tmep,"today", ruleId,"",bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	checkListTodaySet = new ArrayList(checkListTodayList);
				checkListOverDueList=getCheckListResultExtCordinatorForTeam(sessionCorpID,tmep,"overDue", ruleId,"",bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				checkListOverDueSet= new ArrayList(checkListOverDueList);
				countCheckListTodayResult=checkListTodayList.size();
				countCheckListOverDueResult=checkListOverDueList.size();
				//For Agent TDR Email
		    	lsEmail=toDoRuleManager.findByAgentTdrEmailExtCordinatorForTeam(sessionCorpID,tmep,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
			   	countAgentTdrEmail=lsEmail.size();
			   	agentTdrEmailSet=new ArrayList(lsEmail);
			   	////alert for team /////
			   String teamAlertUser="";
			   Iterator it=userList.iterator();
			   while(it.hasNext())
			   {
				   if(teamAlertUser.equalsIgnoreCase("")){
				   teamAlertUser="'"+ it.next().toString()+"'";
				   }
				   else{
						teamAlertUser=teamAlertUser+",'"+it.next().toString()+"'";
					}
			   }
			   	auditTrailList = auditTrailManager.findByAlertListFilterForExtCoordinator(sessionCorpID,teamAlertUser,daystoManageAlerts,bookingAgentPopup,billToCodePopup);
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();		
                }
			else{*/
                List supervisorTeam=toDoRuleManager.getSupervisorForTeam(user.getUsername(),sessionCorpID);
                if(supervisorTeam!=null && !supervisorTeam.isEmpty() && supervisorTeam.get(0)!=null && (!(supervisorTeam.get(0).toString().trim().equals(""))))
                {
				 tmep2 = supervisorTeam.toString();
				 temp1 = supervisorTeam.get(0).toString();
				tmep2=tmep2.replace("[", "");
				tmep2=tmep2.replace("]", "");
				temp1=tmep2.replace("'", "");
				temp1=tmep2.replace("'", "");
				List userName=toDoRuleManager.findUserForSupervisor(temp1,sessionCorpID);
				 tmep = userName.toString();
				 temp = userName.get(0).toString();
				tmep=tmep.replace("[", "");
				tmep=tmep.replace("]", "");
				temp=tmep.replace("'", "");
				temp=tmep.replace("'", "");
				String finaluser=tmep+","+tmep2;
				//String tmep = userNameTemp.substring(1, userNameTemp.length()-1);
				userList=toDoRuleManager.getOwnersListExtCordinatorForTeam(sessionCorpID,finaluser,userId,userFirstName,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				userList.remove("UNASSIGNED");
				messageList=toDoRuleManager.getMessageListExtCordinatorForTeam(sessionCorpID,finaluser,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		        ruleIdList=toDoRuleManager.getRuleIdListExtCordinatorForTeam(sessionCorpID,finaluser,userId,userType,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
		    	
				//For Today's Note // For Supervisor
				nls=toDoRuleManager.findByStatusForSupervisorExtCordinatorForTeam("NEW",sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes=new HashSet(nls);
				countToday=notes.size();
				
				// For OverDue Note // For Supervisor
				nls1=toDoRuleManager.findByStatusForSupervisor1ExtCordinatorForTeam("NEW",sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes1=new HashSet(nls1);
				countOverDue=notes1.size();
				
				// For Due This Week Note // For Supervisor
				nls2=toDoRuleManager.findByStatusForSupervisor2ExtCordinatorForTeam("NEW",sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio);
				notes2=new HashSet(nls2);
				countDueWeek=notes2.size();
						
				// For Result Overdue
				ls=toDoRuleManager.findBydurationForSupervisorExtCordinatorForTeam(sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
		    	toDoResults1=new ArrayList(ls);
		    	countResultOverDue=toDoResults1.size();
		    	
		    	// For Result Today
		    	lstoday=toDoRuleManager.findBydurationdtodayForSupervisorExtCordinatorForTeam(sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
		    	toDoResultsToday1 = new ArrayList(lstoday);
		    	countResultToday=toDoResultsToday1.size();
		    	
		    	// For Result Due This Week
		    	lsDueWeek=toDoRuleManager.findBydurationdueweekForSupervisorExtCordinatorForTeam(sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
		    	toDoResultsDueWeek=new HashSet(lsDueWeek);
		    	countResultDueWeek=toDoResultsDueWeek.size();
		    	
		    	// For Result checkList
		    	checkListTodayList=getCheckListResultExtCordinatorForTeam(sessionCorpID,finaluser,"today", ruleId,"",bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
		    	checkListTodaySet = new ArrayList(checkListTodayList);
				checkListOverDueList=getCheckListResultExtCordinatorForTeam(sessionCorpID,finaluser,"overDue", ruleId,"",bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
				checkListOverDueSet= new ArrayList(checkListOverDueList);
				countCheckListTodayResult=checkListTodayList.size();
				countCheckListOverDueResult=checkListOverDueList.size();
				//For Agent TDR Email
		    	lsEmail=toDoRuleManager.findByAgentTdrEmailExtCordinatorForTeam(sessionCorpID,finaluser,bookingAgentPopup,billToCodePopup,getRequest().getRemoteUser(),allradio,userType);
			   	countAgentTdrEmail=lsEmail.size();
			   	agentTdrEmailSet=new ArrayList(lsEmail);
				
                   ////alert for team /////
			   	String teamAlertUser="";
				   Iterator it=userList.iterator();
				   while(it.hasNext())
				   {
					   if(teamAlertUser.equalsIgnoreCase("")){
					   teamAlertUser="'"+ it.next().toString()+"'";
					   }
					   else{
							teamAlertUser=teamAlertUser+",'"+it.next().toString()+"'";
						}
				   }
			   	auditTrailList = auditTrailManager.findByAlertListFilterForExtCoordinator(sessionCorpID,teamAlertUser,daystoManageAlerts,bookingAgentPopup,billToCodePopup);
				toDayAlertList = new HashSet(auditTrailList);
				countAlertToday=toDayAlertList.size();	
				
			//}
	}
	
	}
	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS;   
	}
	
	
	public Set getToDoResults() {
		return toDoResults;
	}
	public void setToDoResults(Set toDoResults) {
		this.toDoResults = toDoResults;
	}
	public List getAuditTrailList() {
		return auditTrailList;
	}

	public void setAuditTrailList(List auditTrailList) {
		this.auditTrailList = auditTrailList;
	}

	public Set getToDayAlertList() {
		return toDayAlertList;
	}

	public void setToDayAlertList(Set toDayAlertList) {
		this.toDayAlertList = toDayAlertList;
	}

	public int getCountAlertToday() {
		return countAlertToday;
	}

	public void setCountAlertToday(int countAlertToday) {
		this.countAlertToday = countAlertToday;
	}

	public void setAuditTrailManager(AuditTrailManager auditTrailManager) {
		this.auditTrailManager = auditTrailManager;
	}
	
	public String getHistoryId() {
		return historyId;
	}

	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}

	public String getSetIsViewHistoryFlag() {
		return setIsViewHistoryFlag;
	}

	public void setSetIsViewHistoryFlag(String setIsViewHistoryFlag) {
		this.setIsViewHistoryFlag = setIsViewHistoryFlag;
	}
	
	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}
	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}
	public List getRefMasters() {
		return refMasters;
	}
	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}
	public Map<String, String> getPtest() {
		return ptest;
	}
	public void setPtest(Map<String, String> ptest) {
		this.ptest = ptest;
	}
	public Map<String, String> getPfield() {
		return pfield;
	}
	public void setPfield(Map<String, String> pfield) {
		this.pfield = pfield;
	}
	public Map<String, String> getPstest() {
		return pstest;
	}
	public void setPstest(Map<String, String> pstest) {
		this.pstest = pstest;
	}
	public Set getToDoRules() {
		return toDoRules;
	}	
	public Map<String, String> getPentity() {
		return pentity;
	}
	public void setPentity(Map<String, String> pentity) {
		this.pentity = pentity;
	}
	public Map<String, String> getPduration() {
		return pduration;
	}
	public void setPduration(Map<String, String> pduration) {
		this.pduration = pduration;
	}		
	public int getCountDueWeek() {
		return countDueWeek;
	}
	public void setCountDueWeek(int countDueWeek) {
		this.countDueWeek = countDueWeek;
	}
	public int getCountOverDue() {
		return countOverDue;
	}
	public void setCountOverDue(int countOverDue) {
		this.countOverDue = countOverDue;
	}
	public int getCountToday() {
		return countToday;
	}
	public void setCountToday(int countToday) {
		this.countToday = countToday;
	}
	public String getEntity() {
		return entity;
	}
	public void setEntity(String entity) {
		this.entity = entity;
	}
	public List getNls2() {
		return nls2;
	}
	public void setNls2(List nls2) {
		this.nls2 = nls2;
	}
	public Set getNotes2() {
		return notes2;
	}
	public void setNotes2(Set notes2) {
		this.notes2 = notes2;
	}
	public List getNls1() {
		return nls1;
	}
	public void setNls1(List nls1) {
		this.nls1 = nls1;
	}
	public Set getNotes1() {
		return notes1;
	}
	public void setNotes1(Set notes1) {
		this.notes1 = notes1;
	}
	public List<Notes> getNote() {
		return note;
	}
	public void setNote(List<Notes> note) {
		this.note = note;
	}
	public Set getNotes() {
		return notes;
	}
	public void setNotes(Set notes) {
		this.notes = notes;
	}
	public void setNotesManager(NotesManager notesManager) {
		this.notesManager = notesManager;
	}
	public List<ToDoResult> getToDoResult() {
		return toDoResult;
	}
	public void setToDoResult(List<ToDoResult> toDoResult) {
		this.toDoResult = toDoResult;
	}
	public void setToDoResultManager(ToDoResultManager toDoResultManager) {
		this.toDoResultManager = toDoResultManager;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ToDoRule getToDoRule() {
		return toDoRule;
	}
	public void setToDoRule(ToDoRule toDoRule) {
		this.toDoRule = toDoRule;
	}
	
	public List<ServiceOrder> getServiceOrder() {
		return serviceOrder;
	}
	public void setServiceOrder(ServiceOrder serviceOrder) {
		this.serviceOrder = (List<ServiceOrder>) serviceOrder;
	}
	public void setServiceOrderManager(ServiceOrderManager serviceOrderManager) {
		this.serviceOrderManager = serviceOrderManager;
	}
	public Map<String, String> getPrulerole() {
		return prulerole;
	}
	public void setPrulerole(Map<String, String> prulerole) {
		this.prulerole = prulerole;
	}
	public List getNls() {
		return nls;
	}
	public void setNls(List nls) {
		this.nls = nls;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public Long getId1() {
		return id1;
	}
	public void setId1(Long id1) {
		this.id1 = id1;
	}
	public List getLsDueWeek() {
		return lsDueWeek;
	}
	public void setLsDueWeek(List lsDueWeek) {
		this.lsDueWeek = lsDueWeek;
	}
	public List getLstoday() {
		return lstoday;
	}
	public void setLstoday(List lstoday) {
		this.lstoday = lstoday;
	}
	public Set getToDoResultsDueWeek() {
		return toDoResultsDueWeek;
	}
	public void setToDoResultsDueWeek(Set toDoResultsDueWeek) {
		this.toDoResultsDueWeek = toDoResultsDueWeek;
	}
	public Set getToDoResultsToday() {
		return toDoResultsToday;
	}
	public void setToDoResultsToday(Set toDoResultsToday) {
		this.toDoResultsToday = toDoResultsToday;
	}
	public String getBtn() {
		return btn;
	}
	public void setBtn(String btn) {
		this.btn = btn;
	}
	public List getSelectedField() {
		return selectedField;
	}
	public void setSelectedField(List selectedField) {
		this.selectedField = selectedField;
	}
	public List<DataCatalog> getSelectedDateField() {
		return selectedDateField;
	}
	public void setSelectedDateField(List<DataCatalog> selectedDateField) {
		this.selectedDateField = selectedDateField;
	}
	public int getCountResultDueWeek() {
		return countResultDueWeek;
	}
	public void setCountResultDueWeek(int countResultDueWeek) {
		this.countResultDueWeek = countResultDueWeek;
	}
	public int getCountResultOverDue() {
		return countResultOverDue;
	}
	public void setCountResultOverDue(int countResultOverDue) {
		this.countResultOverDue = countResultOverDue;
	}
	public int getCountResultToday() {
		return countResultToday;
	}
	public void setCountResultToday(int countResultToday) {
		this.countResultToday = countResultToday;
	}
	public int getCountNumberOfRules() {
		return countNumberOfRules;
	}
	public void setCountNumberOfRules(int countNumberOfRules) {
		this.countNumberOfRules = countNumberOfRules;
	}
	public int getCountNumberOfResult() {
		return countNumberOfResult;
	}
	public void setCountNumberOfResult(int countNumberOfResult) {
		this.countNumberOfResult = countNumberOfResult;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
	public Set<Role> getRoles() {
		return roles;
	}
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public String getIsSupervisor() {
		return isSupervisor;
	}

	public void setIsSupervisor(String isSupervisor) {
		this.isSupervisor = isSupervisor;
	}

	public String getGotoPageString() {
		return gotoPageString;
	}

	public void setGotoPageString(String gotoPageString) {
		this.gotoPageString = gotoPageString;
	}

	public String getValidateFormNav() {
		return validateFormNav;
	}

	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}

	public List getUserList() {
		return userList;
	}

	public void setUserList(List userList) {
		this.userList = userList;
	}

	
	public ToDoResult getTodoresult() {
		return todoresult;
	}

	public void setTodoresult(ToDoResult todoresult) {
		this.todoresult = todoresult;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getRecordId() {
		return recordId;
	}

	public void setRecordId(String recordId) {
		this.recordId = recordId;
	}

	public String getFieldToValidate() {
		return fieldToValidate;
	}

	public void setFieldToValidate(String fieldToValidate) {
		this.fieldToValidate = fieldToValidate;
	}

	public String getToDoRuleCheckEnable() {
		return toDoRuleCheckEnable;
	}

	public void setToDoRuleCheckEnable(String toDoRuleCheckEnable) {
		this.toDoRuleCheckEnable = toDoRuleCheckEnable;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public List getFieldList() {
		return fieldList;
	}

	public void setFieldList(List fieldList) {
		this.fieldList = fieldList;
	}

	public String getFieldToValidateValue() {
		return fieldToValidateValue;
	}

	public void setFieldToValidateValue(String fieldToValidateValue) {
		this.fieldToValidateValue = fieldToValidateValue;
	}

	public List getMessageList() {
		return messageList;
	}

	public void setMessageList(List messageList) {
		this.messageList = messageList;
	}

	public String getMessageDisplayed() {
		return messageDisplayed;
	}

	public void setMessageDisplayed(String messageDisplayed) {
		this.messageDisplayed = messageDisplayed;
	}

	public List getRuleIdList() {
		return ruleIdList;
	}

	public void setRuleIdList(List ruleIdList) {
		this.ruleIdList = ruleIdList;
	}

	public String getRuleId() {
		return ruleId;
	}

	public void setRuleId(String ruleId) {
		this.ruleId = ruleId;
	}

	public static Map<String, String> getNotesubtype() {
		return notesubtype;
	}

	public static void setNotesubtype(Map<String, String> notesubtype) {
		ToDoRuleAction.notesubtype = notesubtype;
	}

	public static Map<String, String> getNotetype() {
		return notetype;
	}

	public static void setNotetype(Map<String, String> notetype) {
		ToDoRuleAction.notetype = notetype;
	}

	public ToDoRule getAddCopyToDoRule() {
		return addCopyToDoRule;
	}

	public void setAddCopyToDoRule(ToDoRule addCopyToDoRule) {
		this.addCopyToDoRule = addCopyToDoRule;
	}

	public List getToDoRulesSummary() {
		return toDoRulesSummary;
	}

	public void setToDoRulesSummary(List toDoRulesSummary) {
		this.toDoRulesSummary = toDoRulesSummary;
	}

	public List getCheckListTodayList() {
		return checkListTodayList;
	}

	public void setCheckListTodayList(List checkListTodayList) {
		this.checkListTodayList = checkListTodayList;
	}

	public List getCheckListOverDueList() {
		return checkListOverDueList;
	}

	public void setCheckListOverDueList(List checkListOverDueList) {
		this.checkListOverDueList = checkListOverDueList;
	}

	public void setCheckListManager(CheckListManager checkListManager) {
		this.checkListManager = checkListManager;
	}

	public Integer getCountCheckListOverDueResult() {
		return countCheckListOverDueResult;
	}

	public void setCountCheckListOverDueResult(Integer countCheckListOverDueResult) {
		this.countCheckListOverDueResult = countCheckListOverDueResult;
	}

	public Integer getCountCheckListTodayResult() {
		return countCheckListTodayResult;
	}

	public void setCountCheckListTodayResult(Integer countCheckListTodayResult) {
		this.countCheckListTodayResult = countCheckListTodayResult;
	} 

	public String getShipperName() {
		return shipperName;
	}

	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}

	public List getLsComingUp() {
		return lsComingUp;
	}

	public void setLsComingUp(List lsComingUp) {
		this.lsComingUp = lsComingUp;
	}

	public Set getToDoResultsComingUp() {
		return toDoResultsComingUp;
	}

	public void setToDoResultsComingUp(Set toDoResultsComingUp) {
		this.toDoResultsComingUp = toDoResultsComingUp;
	}

	public int getCountResultComingUp() {
		return countResultComingUp;
	}

	public void setCountResultComingUp(int countResultComingUp) {
		this.countResultComingUp = countResultComingUp;
	}
	
	public String getNewIdList() {
		return newIdList;
	}

	public void setNewIdList(String newIdList) {
		this.newIdList = newIdList;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Map<String, String> getToEmail() {
		return toEmail;
	}

	public void setToEmail(Map<String, String> toEmail) {
		this.toEmail = toEmail;
	}

	public List getToEmailList() {
		return toEmailList;
	}

	public void setToEmailList(List toEmailList) {
		this.toEmailList = toEmailList;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}


	public String getUserFirstName() {
		return userFirstName;
	}


	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}	
	
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	/*public List getNetworkFileList() {
		return networkFileList;
	}

	public void setNetworkFileList(List networkFileList) {
		this.networkFileList = networkFileList;
	}
*/
	public Integer getNetworkLinkSize() {
		return networkLinkSize;
	}

	public void setNetworkLinkSize(Integer networkLinkSize) {
		this.networkLinkSize = networkLinkSize;
	}

	public String getChkAutomaticLink() {
		return chkAutomaticLink;
	}

	public void setChkAutomaticLink(String chkAutomaticLink) {
		this.chkAutomaticLink = chkAutomaticLink;
	}

	public Long getSoId() {
		return soId;
	}

	public void setSoId(Long soId) {
		this.soId = soId;
	}

	public void setBillingManager(BillingManager billingManager) {
		this.billingManager = billingManager;
	}

	public void setTrackingStatusManager(TrackingStatusManager trackingStatusManager) {
		this.trackingStatusManager = trackingStatusManager;
	}

	public void setMiscellaneousManager(MiscellaneousManager miscellaneousManager) {
		this.miscellaneousManager = miscellaneousManager;
	}

	public List getMaxSequenceNumber() {
		return maxSequenceNumber;
	}

	public void setMaxSequenceNumber(List maxSequenceNumber) {
		this.maxSequenceNumber = maxSequenceNumber;
	}

	public Long getAutoSequenceNumber() {
		return autoSequenceNumber;
	}

	public void setAutoSequenceNumber(Long autoSequenceNumber) {
		this.autoSequenceNumber = autoSequenceNumber;
	}

	public String getSeqNum() {
		return seqNum;
	}

	public void setSeqNum(String seqNum) {
		this.seqNum = seqNum;
	}

	public List getCheckSequenceNumberList() {
		return checkSequenceNumberList;
	}

	public void setCheckSequenceNumberList(List checkSequenceNumberList) {
		this.checkSequenceNumberList = checkSequenceNumberList;
	}

	public List getMaxShip() {
		return maxShip;
	}

	public void setMaxShip(List maxShip) {
		this.maxShip = maxShip;
	}

	public String getShip() {
		return ship;
	}

	public void setShip(String ship) {
		this.ship = ship;
	}

	public Long getAutoShip() {
		return autoShip;
	}

	public void setAutoShip(Long autoShip) {
		this.autoShip = autoShip;
	}

	public String getShipnumber() {
		return shipnumber;
	}

	public void setShipnumber(String shipnumber) {
		this.shipnumber = shipnumber;
	}

	public void setContainerManager(ContainerManager containerManager) {
		this.containerManager = containerManager;
	}

	public void setVehicleManager(VehicleManager vehicleManager) {
		this.vehicleManager = vehicleManager;
	}

	public String getChkFlag() {
		return chkFlag;
	}

	public void setChkFlag(String chkFlag) {
		this.chkFlag = chkFlag;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public void setCartonManager(CartonManager cartonManager) {
		this.cartonManager = cartonManager;
	}

	public void setServicePartnerManager(ServicePartnerManager servicePartnerManager) {
		this.servicePartnerManager = servicePartnerManager;
	}

	public String getBookingAgentName() {
		return bookingAgentName;
	}

	public void setBookingAgentName(String bookingAgentName) {
		this.bookingAgentName = bookingAgentName;
	}

	public String getBookingAgentcode() {
		return bookingAgentcode;
	}

	public void setBookingAgentcode(String bookingAgentcode) {
		this.bookingAgentcode = bookingAgentcode;
	}

	public ServiceOrder getSoObj() {
		return soObj;
	}

	public void setSoObj(ServiceOrder soObj) {
		this.soObj = soObj;
	}

	public List getLinkingSequenceNumber() {
		return linkingSequenceNumber;
	}

	public void setLinkingSequenceNumber(List linkingSequenceNumber) {
		this.linkingSequenceNumber = linkingSequenceNumber;
	}

	public List getLinkedSOMerge() {
		return linkedSOMerge;
	}

	public void setLinkedSOMerge(List linkedSOMerge) {
		this.linkedSOMerge = linkedSOMerge;
	}

	public String getSeqNumber() {
		return seqNumber;
	}

	public void setSeqNumber(String seqNumber) {
		this.seqNumber = seqNumber;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public Long getSoUGWWID() {
		return soUGWWID;
	}

	public void setSoUGWWID(Long soUGWWID) {
		this.soUGWWID = soUGWWID;
	}

	public Long getMergingSOID() {
		return mergingSOID;
	}

	public void setMergingSOID(Long mergingSOID) {
		this.mergingSOID = mergingSOID;
	}

	public String getAgentType() {
		return agentType;
	}

	public void setAgentType(String agentType) {
		this.agentType = agentType;
	}

	public String getChildAgentType() {
		return childAgentType;
	}

	public void setChildAgentType(String childAgentType) {
		this.childAgentType = childAgentType;
	}

	public String getChildAgentCode() {
		return childAgentCode;
	}

	public void setChildAgentCode(String childAgentCode) {
		this.childAgentCode = childAgentCode;
	}

	public TrackingStatusAction getTrackingStatusAction() {
		return trackingStatusAction;
	}

	public void setTrackingStatusAction(TrackingStatusAction trackingStatusAction) {
		this.trackingStatusAction = trackingStatusAction;
	}

	public void setConsigneeInstructionManager(
			ConsigneeInstructionManager consigneeInstructionManager) {
		this.consigneeInstructionManager = consigneeInstructionManager;
	}

	public SystemDefault getSystemDefault() {
		return systemDefault;
	}

	public void setSystemDefault(SystemDefault systemDefault) {
		this.systemDefault = systemDefault;
	}

	public void setSystemDefaultManager(SystemDefaultManager systemDefaultManager) {
		this.systemDefaultManager = systemDefaultManager;
	}

	public List getIntegrationErrorList() {
		return integrationErrorList;
	}

	public void setIntegrationErrorList(List integrationErrorList) {
		this.integrationErrorList = integrationErrorList;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Map<String, String> getSequenceNumberMap() {
		return sequenceNumberMap;
	}

	public void setSequenceNumberMap(Map<String, String> sequenceNumberMap) {
		this.sequenceNumberMap = sequenceNumberMap;
	}

	public void setWorkTicketManager(WorkTicketManager workTicketManager) {
		this.workTicketManager = workTicketManager;
	}

	public WorkTicket getWorkTicket() {
		return workTicket;
	}

	public void setWorkTicket(WorkTicket workTicket) {
		this.workTicket = workTicket;
	}

	public void setClaimManager(ClaimManager claimManager) {
		this.claimManager = claimManager;
	}

	public Claim getClaim() {
		return claim;
	}

	public void setClaim(Claim claim) {
		this.claim = claim;
	}

	public String getRelatedTask() {
		return relatedTask;
	}

	public void setRelatedTask(String relatedTask) {
		this.relatedTask = relatedTask;
	}

	public UgwwActionTracker getUgwwActionTracker() {
		return ugwwActionTracker;
	}

	public void setUgwwActionTracker(UgwwActionTracker ugwwActionTracker) {
		this.ugwwActionTracker = ugwwActionTracker;
	}

	public void setUgwwActionTrackerManager(
			UgwwActionTrackerManager ugwwActionTrackerManager) {
		this.ugwwActionTrackerManager = ugwwActionTrackerManager;
	}

	 

	public List getToDoResultsToday1() {
		return toDoResultsToday1;
	}

	public void setToDoResultsToday1(List toDoResultsToday1) {
		this.toDoResultsToday1 = toDoResultsToday1;
	}

	public String getMergingSeqNum() {
		return mergingSeqNum;
	}

	public void setMergingSeqNum(String mergingSeqNum) {
		this.mergingSeqNum = mergingSeqNum;
	}

	public String getAgentRole() {
		return agentRole;
	}

	public void setAgentRole(String agentRole) {
		this.agentRole = agentRole;
	}

	public Map<String, List<String>> getNetworkFields() {
		return networkFields;
	}

	public void setNetworkFields(Map<String, List<String>> networkFields) {
		this.networkFields = networkFields;
	}

	public ServiceOrder getIncomingSO() {
		return incomingSO;
	}

	public void setIncomingSO(ServiceOrder incomingSO) {
		this.incomingSO = incomingSO;
	}

	public ServiceOrder getYourSO() {
		return yourSO;
	}

	public void setYourSO(ServiceOrder yourSO) {
		this.yourSO = yourSO;
	}

	public CustomerFile getInComingCF() {
		return inComingCF;
	}

	public void setInComingCF(CustomerFile inComingCF) {
		this.inComingCF = inComingCF;
	}

	public CustomerFile getYourCF() {
		return yourCF;
	}

	public void setYourCF(CustomerFile yourCF) {
		this.yourCF = yourCF;
	}

	public Billing getInComingBilling() {
		return inComingBilling;
	}

	public void setInComingBilling(Billing inComingBilling) {
		this.inComingBilling = inComingBilling;
	}

	public Billing getYourBilling() {
		return yourBilling;
	}

	public void setYourBilling(Billing yourBilling) {
		this.yourBilling = yourBilling;
	}

	public TrackingStatus getInComingTS() {
		return inComingTS;
	}

	public void setInComingTS(TrackingStatus inComingTS) {
		this.inComingTS = inComingTS;
	}

	public TrackingStatus getYourTS() {
		return yourTS;
	}

	public void setYourTS(TrackingStatus yourTS) {
		this.yourTS = yourTS;
	}

	public Miscellaneous getInComingMsc() {
		return inComingMsc;
	}

	public void setInComingMsc(Miscellaneous inComingMsc) {
		this.inComingMsc = inComingMsc;
	}

	public Miscellaneous getYourMsc() {
		return yourMsc;
	}

	public void setYourMsc(Miscellaneous yourMsc) {
		this.yourMsc = yourMsc;
	}

	public Map<String, List<String>> getResultMap() {
		return resultMap;
	}

	public void setResultMap(Map<String, List<String>> resultMap) {
		this.resultMap = resultMap;
	}

	public String getFieldValues() {
		return fieldValues;
	}

	public void setFieldValues(String fieldValues) {
		this.fieldValues = fieldValues;
	}

	public List<Container> getContList() {
		return contList;
	}

	public void setContList(List<Container> contList) {
		this.contList = contList;
	}

	public List<Carton> getPieceCountList() {
		return pieceCountList;
	}

	public void setPieceCountList(List<Carton> pieceCountList) {
		this.pieceCountList = pieceCountList;
	}

	public List<Vehicle> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(List<Vehicle> vehicleList) {
		this.vehicleList = vehicleList;
	}

	public List<ServicePartner> getServPartnerList() {
		return servPartnerList;
	}

	public void setServPartnerList(List<ServicePartner> servPartnerList) {
		this.servPartnerList = servPartnerList;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getContIDs() {
		return contIDs;
	}

	public void setContIDs(String contIDs) {
		this.contIDs = contIDs;
	}

	public String getCartonIDs() {
		return cartonIDs;
	}

	public void setCartonIDs(String cartonIDs) {
		this.cartonIDs = cartonIDs;
	}

	public String getVehicleIDs() {
		return vehicleIDs;
	}

	public void setVehicleIDs(String vehicleIDs) {
		this.vehicleIDs = vehicleIDs;
	}

	public String getServicePartnerIDs() {
		return servicePartnerIDs;
	}

	public void setServicePartnerIDs(String servicePartnerIDs) {
		this.servicePartnerIDs = servicePartnerIDs;
	}

	public String getNewIdNum() {
		return newIdNum;
	}

	public void setNewIdNum(String newIdNum) {
		this.newIdNum = newIdNum;
	}

	public CustomerFileAction getCustomerFileAction() {
		return customerFileAction;
	}

	public void setCustomerFileAction(CustomerFileAction customerFileAction) {
		this.customerFileAction = customerFileAction;
	}

	public String getwUnit() {
		return wUnit;
	}

	public void setwUnit(String wUnit) {
		this.wUnit = wUnit;
	}

	public String getvUnit() {
		return vUnit;
	}

	public void setvUnit(String vUnit) {
		this.vUnit = vUnit;
	}
	public List getToDoResults1() {
		return toDoResults1;
	}

	public void setToDoResults1(List toDoResults1) {
		this.toDoResults1 = toDoResults1;
	}

	 

	public String getAgentCompDiv() {
		return agentCompDiv;
	}

	public void setAgentCompDiv(String agentCompDiv) {
		this.agentCompDiv = agentCompDiv;
	}

	public List getErrorRuleNumberList() {
		return errorRuleNumberList;
	}

	public void setErrorRuleNumberList(List errorRuleNumberList) {
		this.errorRuleNumberList = errorRuleNumberList;
	}
	public String getChkRulesRunning() {
		return chkRulesRunning;
	}

	public void setChkRulesRunning(String chkRulesRunning) {
		this.chkRulesRunning = chkRulesRunning;
	}

	public int getCountContainer() {
		return countContainer;
	}

	public void setCountContainer(int countContainer) {
		this.countContainer = countContainer;
	}

	public int getCountCarton() {
		return countCarton;
	}

	public void setCountCarton(int countCarton) {
		this.countCarton = countCarton;
	}

	public int getCountVehicle() {
		return countVehicle;
	}

	public void setCountVehicle(int countVehicle) {
		this.countVehicle = countVehicle;
	}

	public int getCountServicePartner() {
		return countServicePartner;
	}

	public void setCountServicePartner(int countServicePartner) {
		this.countServicePartner = countServicePartner;
	}

	public String getToDoResultUrl() {
		return toDoResultUrl;
	}

	public void setToDoResultUrl(String toDoResultUrl) {
		this.toDoResultUrl = toDoResultUrl;
	}

	public String getToDoRecordId() {
		return toDoRecordId;
	}

	public void setToDoRecordId(String toDoRecordId) {
		this.toDoRecordId = toDoRecordId;
	}

	public String getRoleForTransfer() {
		return roleForTransfer;
	}

	public void setRoleForTransfer(String roleForTransfer) {
		this.roleForTransfer = roleForTransfer;
	}

	public String getToDoResultId() {
		return toDoResultId;
	}

	public void setToDoResultId(String toDoResultId) {
		this.toDoResultId = toDoResultId;
	}

	public Map<String, String> getSale() {
		return sale;
	}

	public void setSale(Map<String, String> sale) {
		this.sale = sale;
	}

	public Map<String, String> getBillingList() {
		return billingList;
	}

	public void setBillingList(Map<String, String> billingList) {
		this.billingList = billingList;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public Map<String, String> getDocsList() {
		return docsList;
	}

	public void setDocsList(Map<String, String> docsList) {
		this.docsList = docsList;
	}

	public Map<String, String> getSequenceNumberMapByName() {
		return sequenceNumberMapByName;
	}

	public void setSequenceNumberMapByName(
			Map<String, String> sequenceNumberMapByName) {
		this.sequenceNumberMapByName = sequenceNumberMapByName;
	}

	public String getFilter() {
		return filter;
	}

	public void setFilter(String filter) {
		this.filter = filter;
	}

	public String getDocCheckListbtn() {
		return docCheckListbtn;
	}

	public void setDocCheckListbtn(String docCheckListbtn) {
		this.docCheckListbtn = docCheckListbtn;
	}

	public List getCheckListTodaySet() {
		return checkListTodaySet;
	}

	public void setCheckListTodaySet(List checkListTodaySet) {
		this.checkListTodaySet = checkListTodaySet;
	}

	public List getCheckListOverDueSet() {
		return checkListOverDueSet;
	}

	public void setCheckListOverDueSet(List checkListOverDueSet) {
		this.checkListOverDueSet = checkListOverDueSet;
	}

	public String getTempUserForPage() {
		return tempUserForPage;
	}

	public void setTempUserForPage(String tempUserForPage) {
		this.tempUserForPage = tempUserForPage;
	}

	public String getCoordSelected() {
		return coordSelected;
	}

	public void setCoordSelected(String coordSelected) {
		this.coordSelected = coordSelected;
	}

	public List getLsEmail() {
		return lsEmail;
	}

	public void setLsEmail(List lsEmail) {
		this.lsEmail = lsEmail;
	}

	public List getAgentTdrEmailSet() {
		return agentTdrEmailSet;
	}

	public void setAgentTdrEmailSet(List agentTdrEmailSet) {
		this.agentTdrEmailSet = agentTdrEmailSet;
	}

	public Integer getCountAgentTdrEmail() {
		return countAgentTdrEmail;
	}

	public void setCountAgentTdrEmail(Integer countAgentTdrEmail) {
		this.countAgentTdrEmail = countAgentTdrEmail;
	}

	public String getVoxmeIntergartionFlag() {
		return voxmeIntergartionFlag;
	}

	public void setVoxmeIntergartionFlag(String voxmeIntergartionFlag) {
		this.voxmeIntergartionFlag = voxmeIntergartionFlag;
	}

	public String getMmValidation() {
		return mmValidation;
	}

	public void setMmValidation(String mmValidation) {
		this.mmValidation = mmValidation;
	}

	public Boolean getIsAgentTdr() {
		return isAgentTdr;
	}

	public void setIsAgentTdr(Boolean isAgentTdr) {
		this.isAgentTdr = isAgentTdr;
	}

	public String getFlagForSupervisor() {
		return flagForSupervisor;
	}

	public void setFlagForSupervisor(String flagForSupervisor) {
		this.flagForSupervisor = flagForSupervisor;
	}

	public boolean isFlagActivityMgmtVersion2() {
		return flagActivityMgmtVersion2;
	}

	public void setFlagActivityMgmtVersion2(boolean flagActivityMgmtVersion2) {
		this.flagActivityMgmtVersion2 = flagActivityMgmtVersion2;
	}

	public void setErrorLogManager(ErrorLogManager errorLogManager) {
		this.errorLogManager = errorLogManager;
	}

	public String getOiJobList() {
		return oiJobList;
	}

	public void setOiJobList(String oiJobList) {
		this.oiJobList = oiJobList;
	}
	
	public List getActionList() {
		return actionList;
	}

	public void setActionList(List actionList) {
		this.actionList = actionList;
	}

	public void setRuleActionsManager(RuleActionsManager ruleActionsManager) {
		this.ruleActionsManager = ruleActionsManager;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	/**
	 * @return the companyToDoRuleExecutionUpdate
	 */
	public CompanyToDoRuleExecutionUpdate getCompanyToDoRuleExecutionUpdate() {
		return companyToDoRuleExecutionUpdate;
	}

	/**
	 * @param companyToDoRuleExecutionUpdate the companyToDoRuleExecutionUpdate to set
	 */
	public void setCompanyToDoRuleExecutionUpdate(
			CompanyToDoRuleExecutionUpdate companyToDoRuleExecutionUpdate) {
		this.companyToDoRuleExecutionUpdate = companyToDoRuleExecutionUpdate;
	}

	/**
	 * @param companyToDoRuleExecutionUpdateManager the companyToDoRuleExecutionUpdateManager to set
	 */
	public void setCompanyToDoRuleExecutionUpdateManager(
			CompanyToDoRuleExecutionUpdateManager companyToDoRuleExecutionUpdateManager) {
		this.companyToDoRuleExecutionUpdateManager = companyToDoRuleExecutionUpdateManager;
	}
	
	/**
	 * @return the taskUser
	 */
	public Map<String, String> getTaskUser() {
		return taskUser;
	}

	/**
	 * @param taskUser the taskUser to set
	 */
	public void setTaskUser(Map<String, String> taskUser) {
		this.taskUser = taskUser;
	}

	public boolean isFlagexternalCordinator() {
		return flagexternalCordinator;
	}

	public void setFlagexternalCordinator(boolean flagexternalCordinator) {
		this.flagexternalCordinator = flagexternalCordinator;
	}

	
	public String getAllradio() {
		return allradio;
	}

	public void setAllradio(String allradio) {
		this.allradio = allradio;
	}

	public boolean isChkOwnTeamAll() {
		return chkOwnTeamAll;
	}

	public void setChkOwnTeamAll(boolean chkOwnTeamAll) {
		this.chkOwnTeamAll = chkOwnTeamAll;
	}

	
}