/**
 * WSQuestion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public abstract class WSQuestion  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private com.keysurvey.api.v68.form.design.WSAnswerSortType answerSortType;

    private long formId;

    private long questionId;

    private java.lang.String text;

    public WSQuestion() {
    }

    public WSQuestion(
           long versionId,
           com.keysurvey.api.v68.form.design.WSAnswerSortType answerSortType,
           long formId,
           long questionId,
           java.lang.String text) {
        super(
            versionId);
        this.answerSortType = answerSortType;
        this.formId = formId;
        this.questionId = questionId;
        this.text = text;
    }


    /**
     * Gets the answerSortType value for this WSQuestion.
     * 
     * @return answerSortType
     */
    public com.keysurvey.api.v68.form.design.WSAnswerSortType getAnswerSortType() {
        return answerSortType;
    }


    /**
     * Sets the answerSortType value for this WSQuestion.
     * 
     * @param answerSortType
     */
    public void setAnswerSortType(com.keysurvey.api.v68.form.design.WSAnswerSortType answerSortType) {
        this.answerSortType = answerSortType;
    }


    /**
     * Gets the formId value for this WSQuestion.
     * 
     * @return formId
     */
    public long getFormId() {
        return formId;
    }


    /**
     * Sets the formId value for this WSQuestion.
     * 
     * @param formId
     */
    public void setFormId(long formId) {
        this.formId = formId;
    }


    /**
     * Gets the questionId value for this WSQuestion.
     * 
     * @return questionId
     */
    public long getQuestionId() {
        return questionId;
    }


    /**
     * Sets the questionId value for this WSQuestion.
     * 
     * @param questionId
     */
    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }


    /**
     * Gets the text value for this WSQuestion.
     * 
     * @return text
     */
    public java.lang.String getText() {
        return text;
    }


    /**
     * Sets the text value for this WSQuestion.
     * 
     * @param text
     */
    public void setText(java.lang.String text) {
        this.text = text;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSQuestion)) return false;
        WSQuestion other = (WSQuestion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.answerSortType==null && other.getAnswerSortType()==null) || 
             (this.answerSortType!=null &&
              this.answerSortType.equals(other.getAnswerSortType()))) &&
            this.formId == other.getFormId() &&
            this.questionId == other.getQuestionId() &&
            ((this.text==null && other.getText()==null) || 
             (this.text!=null &&
              this.text.equals(other.getText())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnswerSortType() != null) {
            _hashCode += getAnswerSortType().hashCode();
        }
        _hashCode += new Long(getFormId()).hashCode();
        _hashCode += new Long(getQuestionId()).hashCode();
        if (getText() != null) {
            _hashCode += getText().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSQuestion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSQuestion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("answerSortType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "answerSortType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSAnswerSortType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("questionId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "questionId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("text");
        elemField.setXmlName(new javax.xml.namespace.QName("", "text"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
