<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="additionalChargesDetails.title"/></title>   
    <meta name="heading" content="<fmt:message key='additionalChargesDetails.heading'/>"/>  
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>	

	<script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
	</script>
<script language="JavaScript">
function onlyFloatNumsAllowed(evt){
	var keyCode = evt.which ? evt.which : evt.keyCode;
	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39)||(keyCode==110) || (keyCode==109) || ( keyCode==190); 
}


function checkFields(){
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.value'].value.trim()==''){
		alert('Please enter value');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.tariffApplicability'].value==''){
		alert('Please select tariff applicability');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.parameter'].value.trim()==''){
		alert('Please enter parameter');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.grid'].value==''){
		alert('Please select grid');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.basis'].value==''){
		alert('Please select basis');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.label'].value.trim()==''){
		alert('Please enter label');
		return false;
	}
	if(document.forms['additionalChargeForm'].elements['partnerRefCharges.countryCode'].value==''){
		alert('Please select country');
		return false;
	}
}
</script>
</head>
<s:form id="additionalChargeForm" name="additionalChargeForm" action="saveAdditionalCharges" method="post" validate="true">   
<s:hidden name="partnerRefCharges.id" />
<s:hidden name="partnerRefCharges.corpID" />

<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    
<div id="newmnav">
	<ul>
		<li><a href="additionalCharges.html"><span>Additional Charges List</span></a></li>
	  	<li id="newmnav1" style="background:#FFF "><a class="current"><span>Additional Charges Details<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
	</ul>
</div>
<div style="width:100%">
<div class="spn">&nbsp;</div>
</div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top"><span></span></div>
   <div class="center-content">
				<table  border="0">
					<tbody>
						<tr>
							<td align="left" class="listwhite" colspan="7"></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.value'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefCharges.value"  cssStyle="width:115px;" maxlength="15" onchange="onlyFloat(this);"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.tariffApplicability'/></td>
							<td><s:select name="partnerRefCharges.tariffApplicability" list="{'Origin','Destination'}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:120px"/></td>
						</tr>	
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.parameter'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefCharges.parameter"  cssStyle="width:115px;" maxlength="50"/></td>
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.grid'/></td>
							<td><s:select name="partnerRefCharges.grid" list="%{grid}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:120px"/></td>
						</tr>	
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.basis'/></td>
							<td><s:select name="partnerRefCharges.basis" list="%{basisList}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:120px"/></td>
							
							
							<td align="right" class="listwhitetext" width="25px"></td>
							
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.label'/></td>
							<td><s:textfield cssClass="input-text" name="partnerRefCharges.label" maxlength="50" cssStyle="width:115px;" /></td>
						</tr>
						<tr>
							<td align="right" class="listwhitetext" width="15px"></td>
							<td align="right" class="listwhitetext"><fmt:message key='partnerRefCharges.countryCode'/></td>
							<td><s:select name="partnerRefCharges.countryCode" list="%{country}" headerKey="" headerValue="" cssClass="list-menu" cssStyle="width:120px"/></td>
						</tr>
						<tr>
							<td align="left" class="listwhite" colspan="7"></td>
						</tr>
					</tbody>
				</table>			
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</div>	
  
<table>
	<tbody>
		<tr>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefCharges.createdOn'/></b></td>
			<td valign="top"></td>
			<td style="width:120px">
				<fmt:formatDate var="haulingGridCreatedOnFormattedValue" value="${partnerRefCharges.createdOn}" pattern="${displayDateTimeEditFormat}"/>
				<s:hidden name="partnerRefCharges.createdOn" value="${haulingGridCreatedOnFormattedValue}"/>
				<fmt:formatDate value="${partnerRefCharges.createdOn}" pattern="${displayDateTimeFormat}"/>
			</td>		
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefCharges.createdBy' /></b></td>
			<c:if test="${not empty partnerRefCharges.id}">
				<s:hidden name="partnerRefCharges.createdBy"/>
				<td style="width:85px"><s:label name="createdBy" value="%{partnerRefCharges.createdBy}"/></td>
			</c:if>
			<c:if test="${empty partnerRefCharges.id}">
				<s:hidden name="partnerRefCharges.createdBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
			
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefCharges.updatedOn'/></b></td>
			<fmt:formatDate var="haulingGridUpdatedOnFormattedValue" value="${partnerRefCharges.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="partnerRefCharges.updatedOn" value="${haulingGridUpdatedOnFormattedValue}"/>
			<td style="width:120px"><fmt:formatDate value="${partnerRefCharges.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
			<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='partnerRefCharges.updatedBy' /></b></td>
			<c:if test="${not empty partnerRefCharges.id}">
				<s:hidden name="partnerRefCharges.updatedBy"/>
				<td style="width:85px"><s:label name="updatedBy" value="%{partnerRefCharges.updatedBy}"/></td>
			</c:if>
			<c:if test="${empty partnerRefCharges.id}">
				<s:hidden name="partnerRefCharges.updatedBy" value="${pageContext.request.remoteUser}"/>
				<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
		</tr>
	</tbody>
</table>               
<s:submit cssClass="cssbutton" method="save" key="button.save" cssStyle="width:60px;" onclick="return checkFields()"/>  
  
  <table><tr><td height="80px"></td></tr></table>
</s:form>
<script type="text/javascript">   
    Form.focusFirstElement($("additionalChargeForm"));   
    <c:if test="${hitflag == 1}" >
			<c:redirect url="/additionalCharges.html"/>
	</c:if>
</script>