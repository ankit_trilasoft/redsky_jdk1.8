package com.trilasoft.app.webapp.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.MssGrid;
import com.trilasoft.app.service.MssGridManager;

public class MssGridAction extends BaseAction implements Preparable{
	
	private Long id;
	private String sessionCorpID;
	private MssGrid mssGrid;
	private MssGridManager mssGridManager;
	private List transportTypeValue;
	
	
	public void prepare() throws Exception {		
	}
	
	public MssGridAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal(); 
		this.sessionCorpID = user.getCorpID(); 
		
	}
	
	public String getComboList(String corpID){
		transportTypeValue = new ArrayList();
		transportTypeValue.add("Origin");
		transportTypeValue.add("Destination");
		transportTypeValue.add("Both");
	 	 return SUCCESS;
	}
	
	public String edit() {
		getComboList(sessionCorpID);
		if (id != null) {
			mssGrid = mssGridManager.get(id);
		} else {			
			mssGrid = new MssGrid(); 
			mssGrid.setCreatedOn(new Date());
			mssGrid.setUpdatedOn(new Date());
		
		} 
		return SUCCESS;
	}
	public String save() throws Exception {
		getComboList(sessionCorpID);
		boolean isNew = (mssGrid.getId() == null);  
				if (isNew) { 
					mssGrid.setCreatedOn(new Date());
		        } 
				mssGrid.setCorpId(sessionCorpID);
				mssGrid.setUpdatedOn(new Date());
				mssGrid.setUpdatedBy(getRequest().getRemoteUser()); 
				mssGridManager.save(mssGrid);  
			    String key = (isNew) ?"Mss Grid have been saved." :"Mss Grid have been saved." ;
			    saveMessage(getText(key)); 
				return SUCCESS;
			
	}
	public String delete(){		
		mssGridManager.remove(id);    
	  return SUCCESS;		    
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public MssGrid getMssGrid() {
		return mssGrid;
	}

	public void setMssGrid(MssGrid mssGrid) {
		this.mssGrid = mssGrid;
	}

	public void setMssGridManager(MssGridManager mssGridManager) {
		this.mssGridManager = mssGridManager;
	}

	public List getTransportTypeValue() {
		return transportTypeValue;
	}

	public void setTransportTypeValue(List transportTypeValue) {
		this.transportTypeValue = transportTypeValue;
	}

}
