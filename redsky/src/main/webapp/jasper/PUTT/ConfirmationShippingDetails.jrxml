<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="ConfirmationShippingDetails" pageWidth="595" pageHeight="842" whenNoDataType="AllSectionsNoDetail" columnWidth="471" leftMargin="62" rightMargin="62" topMargin="20" bottomMargin="20" uuid="50e1b2c9-67d4-407e-beab-373cf7dcc10f">
	<property name="ireport.zoom" value="1.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="690"/>
	<property name="ireport.scriptlethandling" value="0"/>
	<property name="ireport.encoding" value="UTF-8"/>
	<import value="net.sf.jasperreports.engine.*"/>
	<import value="java.util.*"/>
	<import value="net.sf.jasperreports.engine.data.*"/>
	<parameter name="Corporate ID" class="java.lang.String"/>
	<parameter name="Service Order Number" class="java.lang.String"/>
	<queryString>
		<![CDATA[SELECT	'ConfirmationShippingDetails-F-DOC-PUTT.jrxml',
	DATE_FORMAT(now(),'%d-%b-%y') As currentDate,
	serviceorder.shipNumber AS serviceorder_shipNumber,
	Concat(if(customerfile.prefix is null || trim(customerfile.prefix)='',"",trim(refmaster_p.description)),"",if(customerfile.firstName is null || trim(customerfile.firstName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)=''),trim(customerfile.firstName),concat(" ",trim(customerfile.firstName)))),"",if(customerfile.lastName is null || trim(customerfile.lastName)='',"",if((customerfile.prefix is null || trim(customerfile.prefix)='') AND (customerfile.firstName is null || trim(customerfile.firstName)=''),trim(customerfile.lastName),concat(" ",trim(customerfile.lastName))))) AS Shipper,
	servicepartner.carrierName AS servicepartner_carrierName,
	DATE_FORMAT(trackingstatus.documentationCutOff,'%d-%b-%y') AS trackingstatus_documentationCutOffDate,
	servicepartner.carrierDeparture AS servicepartner_carrierDeparture,
	DATE_FORMAT(servicepartner.etDepart,'%d-%b-%y') AS servicepartner_etDepart,
	servicepartnerl.carrierArrival AS servicepartner_carrierArrival,
	DATE_FORMAT(servicepartnerl.etArrival,'%d-%b-%y') AS servicepartner_etArrival,
	concat(if(app_user.first_name is null || app_user.first_name='','',app_user.first_name),if(app_user.last_name is null || app_user.last_name='','',if(app_user.first_name is null || app_user.first_name='',app_user.last_name,concat(' ',app_user.last_name)))) AS Serviceorder_coordinator,
	if(systemdefault.company is null || trim(systemdefault.company)='',"",trim(systemdefault.company)) AS systemdefault_company,
	Concat(	if(systemdefault.address1 is null || trim(systemdefault.address1)='',"",trim(systemdefault.address1)),"",
		if(systemdefault.address2 is null || trim(systemdefault.address2)='',"",if(systemdefault.address1 is null || trim(systemdefault.address1)='',trim(systemdefault.address2),concat(", ",trim(systemdefault.address2)))),"",
		if(systemdefault.address3 is null || trim(systemdefault.address3)='',"",if((systemdefault.address1 is null || trim(systemdefault.address1)='') AND (systemdefault.address2 is null || trim(systemdefault.address2)=''),trim(systemdefault.address3),concat(", ",trim(systemdefault.address3))))
	) AS systemdefault_Address123,
	concat(	if(systemdefault.zip is null || trim(systemdefault.zip)='',"",trim(systemdefault.zip)),"",
		if(systemdefault.city is null || trim(systemdefault.city)='',"",if(systemdefault.zip is null || trim(systemdefault.zip)='',trim(systemdefault.city),concat(" ",trim(systemdefault.city))))
	) AS systemdefaultZipCity,
	Concat(	if(systemdefault.state is null || systemdefault.state='',"",trim(ref_State.description)),"",if(systemdefault.country is null || trim(systemdefault.country)='',"",if(systemdefault.state is null || systemdefault.state='',trim(refmaster_con.description),concat(" - ",trim(refmaster_con.description))))) AS systemdefault_StateCountry,
	concat(if(systemdefault.phone is null || trim(systemdefault.phone)='','',concat("Office: ",trim(systemdefault.phone))),"",if(systemdefault.fax is null || trim(systemdefault.fax)='','',if(systemdefault.phone is null || trim(systemdefault.phone)='',concat("Fax: ",trim(systemdefault.fax)),concat("   Fax: ",trim(systemdefault.fax),"    -    ")))) AS systemdefault_PhoneFax,
	if(systemdefault.website is null || trim(systemdefault.website)='','',trim(systemdefault.website)) AS systemdefault_Website
FROM serviceorder serviceorder 
	INNER JOIN `trackingstatus` trackingstatus ON serviceorder.`id` = trackingstatus.`id` AND trackingstatus.`corpID` = $P{Corporate ID}
	INNER JOIN customerfile customerfile ON serviceorder.sequencenumber = customerfile.sequenceNumber AND customerfile.corpID = $P{Corporate ID}
	INNER JOIN systemdefault systemdefault ON systemdefault.corpid=serviceorder.corpid AND systemdefault.`corpID`=$P{Corporate ID}
	LEFT OUTER JOIN `refmaster` refmaster_con on systemdefault.`country` =refmaster_con.`code` and refmaster_con.`parameter` = 'COUNTRY' and refmaster_con.`corpID` in ($P{Corporate ID},'TSFT') and refmaster_con.`language`='en'
	LEFT OUTER JOIN refmaster ref_State on systemdefault.state=ref_State.code and ref_State.bucket2=systemdefault.country and ref_State.parameter='STATE' and ref_State.language='en' and ref_State.corpID in ('TSFT',$P{Corporate ID})
	LEFT OUTER JOIN `refmaster` refmaster_p on customerfile.prefix= refmaster_p.code and refmaster_p.`parameter` = 'PREFFIX' and refmaster_p.`corpID` in ($P{Corporate ID},'TSFT') and refmaster_p.language=customerfile.customerLanguagePreference
	LEFT OUTER JOIN `app_user` app_user ON app_user.username=serviceorder.coordinator AND app_user.`corpID` = $P{Corporate ID}
	LEFT OUTER JOIN servicepartner servicepartner ON serviceorder.id = servicepartner.serviceOrderId and servicepartner.status is true and  servicepartner.corpid = $P{Corporate ID} and servicepartner.id = (select min(aa.id) from servicepartner aa where aa.serviceOrderId = servicepartner.serviceOrderId and aa.status is true group by servicepartner.serviceOrderId)
	LEFT OUTER JOIN servicepartner servicepartnerl ON serviceorder.id = servicepartnerl.serviceOrderId and servicepartnerl.status is true AND servicepartnerl.corpID =$P{Corporate ID} AND servicepartnerl.id = (select max(bb.id) from servicepartner bb where bb.serviceOrderId = servicepartnerl.serviceOrderId and bb.status is true group by servicepartnerl.serviceOrderId)
WHERE serviceorder.`corpID` = $P{Corporate ID}
AND serviceorder.`shipNumber`= $P{Service Order Number}]]>
	</queryString>
	<field name="ConfirmationShippingDetails-F-DOC-PUTT.jrxml" class="java.lang.String"/>
	<field name="currentDate" class="java.lang.String"/>
	<field name="serviceorder_shipNumber" class="java.lang.String"/>
	<field name="Shipper" class="java.lang.String"/>
	<field name="servicepartner_carrierName" class="java.lang.String"/>
	<field name="trackingstatus_documentationCutOffDate" class="java.lang.String"/>
	<field name="servicepartner_carrierDeparture" class="java.lang.String"/>
	<field name="servicepartner_etDepart" class="java.lang.String"/>
	<field name="servicepartner_carrierArrival" class="java.lang.String"/>
	<field name="servicepartner_etArrival" class="java.lang.String"/>
	<field name="Serviceorder_coordinator" class="java.lang.String"/>
	<field name="systemdefault_company" class="java.lang.String"/>
	<field name="systemdefault_Address123" class="java.lang.String"/>
	<field name="systemdefaultZipCity" class="java.lang.String"/>
	<field name="systemdefault_StateCountry" class="java.lang.String"/>
	<field name="systemdefault_PhoneFax" class="java.lang.String"/>
	<field name="systemdefault_Website" class="java.lang.String"/>
	<group name="SO">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band height="394" splitType="Stretch">
				<staticText>
					<reportElement key="staticText-165" positionType="Float" x="0" y="257" width="471" height="102" forecolor="#000000" backcolor="#FFFFFF" uuid="f4369ba5-931d-4ac0-b17c-570c5e96c262"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[After arrival at the port of entry, there is time needed for customs clearance.
Although we have sent all available documents to our agent at destination, we strongly suggest you to stay in close contact with them to prepare for customs clearance.

We stay at your disposal for any further information you might need. Please do not hesitate to contact us.

Best regard
]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-166" positionType="Float" x="0" y="111" width="471" height="33" forecolor="#000000" backcolor="#FFFFFF" uuid="cb9fee2d-3345-435b-9e00-d9b594c8391a"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Please note that we received confirmation of the shipping details for your removal goods.
You can find them below.]]></text>
				</staticText>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-160" isPrintRepeatedValues="false" mode="Transparent" x="0" y="82" width="471" height="18" isPrintInFirstWholeBand="true" uuid="3b56e411-7127-406e-97b5-33c00d82919b"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<textFieldExpression><![CDATA["Dear "+($F{Shipper}==null || $F{Shipper}.equals("")?"":$F{Shipper})]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-162" mode="Transparent" x="178" y="158" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="f54585a8-00b2-46a6-af36-dd56824ce6c3"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{servicepartner_carrierName}]]></textFieldExpression>
				</textField>
				<textField isBlankWhenNull="true">
					<reportElement key="textField-163" isPrintRepeatedValues="false" mode="Transparent" x="0" y="359" width="471" height="18" isPrintInFirstWholeBand="true" uuid="ed63ee8f-d07b-4ae3-894c-98c3acfa224b"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{Serviceorder_coordinator}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-201" positionType="Float" x="0" y="46" width="471" height="30" forecolor="#000000" backcolor="#FFFFFF" uuid="5105465f-1e08-4f16-a1d9-8ee9b66748e9"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="true" isUnderline="true" pdfFontName="Helvetica-Bold"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Confirmation Shipping Details]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-202" positionType="Float" x="0" y="144" width="471" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="2f1b5130-1d27-47fe-a51e-81f20ef62b92"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Shipping details:]]></text>
				</staticText>
				<staticText>
					<reportElement key="staticText-203" positionType="Float" x="0" y="158" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="17c58e5e-9a25-4b83-8c81-c11be51c6c51"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Name of Vessel]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-208" mode="Transparent" x="178" y="172" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="575f5bbc-41e5-4c33-86e0-8b53ea1fdd1d"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{trackingstatus_documentationCutOffDate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-204" positionType="Float" x="0" y="172" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="c5f77d4c-4d3a-4eea-9cdf-c3be5f5cb8ba"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Export customs clearance at origin]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-209" mode="Transparent" x="178" y="186" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="7e408315-d21e-441e-9298-27a3689a765c"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{servicepartner_carrierDeparture}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-205" positionType="Float" x="0" y="186" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="62b7b881-fe0c-4d2e-b13e-142856e09ea3"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Port of Origin]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-210" mode="Transparent" x="178" y="200" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="29fb29a0-e311-4cfd-aa03-f3e06e81385e"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{servicepartner_etDepart}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-206" positionType="Float" x="0" y="200" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="555e1397-9cf2-47bd-afcf-146e7e217323"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Estimated shipping date at origin]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-211" mode="Transparent" x="178" y="214" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="b0489126-92e3-40b6-8581-4a0b56e97bd2"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{servicepartner_carrierArrival}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-207" positionType="Float" x="0" y="214" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="88f24514-4b22-48e2-92cf-ef2a80f84f0b"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Port of Destination]]></text>
				</staticText>
				<textField isStretchWithOverflow="true" isBlankWhenNull="true">
					<reportElement key="textField-212" mode="Transparent" x="178" y="228" width="293" height="14" isPrintWhenDetailOverflows="true" uuid="5c66ea33-2564-4e55-be18-e6bfb8f9d8fc"/>
					<box>
						<pen lineWidth="0.0"/>
						<topPen lineWidth="0.0"/>
						<leftPen lineWidth="0.0"/>
						<bottomPen lineWidth="0.0"/>
						<rightPen lineWidth="0.0"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" isUnderline="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<textFieldExpression><![CDATA[$F{servicepartner_etArrival}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement key="staticText-208" positionType="Float" x="0" y="228" width="178" height="14" forecolor="#000000" backcolor="#FFFFFF" uuid="95535dbc-ec45-430b-a305-083aacfbc91b"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left" verticalAlignment="Top">
						<font fontName="Times New Roman" size="10" isBold="false" pdfFontName="Helvetica"/>
						<paragraph lineSpacing="Single"/>
					</textElement>
					<text><![CDATA[Estimated arrival at port of destination]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band splitType="Stretch"/>
		</groupFooter>
	</group>
	<group name="SIGNATURE">
		<groupExpression><![CDATA[]]></groupExpression>
		<groupHeader>
			<band splitType="Stretch"/>
		</groupHeader>
		<groupFooter>
			<band height="200" splitType="Stretch">
				<image scaleImage="RetainShape" hAlign="Left" vAlign="Middle" onErrorType="Blank">
					<reportElement key="image-4" x="0" y="0" width="166" height="65" uuid="e9a430c8-46db-4574-ac34-a2be81d12bd1"/>
					<imageExpression><![CDATA["../../images/PuttersRGB.JPG"]]></imageExpression>
				</image>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField-164" x="179" y="103" width="254" height="12" isRemoveLineWhenBlank="true" forecolor="#FF0000" backcolor="#FFFFFF" uuid="25643c5e-7d2f-4217-ab3b-43c7187a571d"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Calibri" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[(	($F{systemdefault_Website}==null || $F{systemdefault_Website}.equals("") ? "" : ($F{systemdefault_Website}))
	
)]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField-165" x="0" y="65" width="471" height="10" isRemoveLineWhenBlank="true" forecolor="#FF0000" backcolor="#FFFFFF" uuid="5f9f5d6b-33d3-4a4b-b55d-36664428c3a3"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Calibri" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[(	($F{systemdefault_company}==null || $F{systemdefault_company}.equals("") ? "" : ($F{systemdefault_company}))
)]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField-166" x="0" y="75" width="270" height="28" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="fd01b27e-8285-48be-9170-aca468485f70"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Calibri" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[(	($F{systemdefault_Address123}==null || $F{systemdefault_Address123}.equals("") ? "" : ($F{systemdefault_Address123}+"\n"))+
	($F{systemdefaultZipCity}==null || $F{systemdefaultZipCity}.equals("") ? "" : ($F{systemdefaultZipCity}+"\n"))+
	($F{systemdefault_StateCountry}==null || $F{systemdefault_StateCountry}.equals("") ? "" : ($F{systemdefault_StateCountry}))
)]]></textFieldExpression>
				</textField>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField-167" x="0" y="103" width="179" height="12" isRemoveLineWhenBlank="true" forecolor="#000000" backcolor="#FFFFFF" uuid="4cc0ca24-2699-4057-93cc-c77f39aee2ed"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Calibri" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA[(	($F{systemdefault_PhoneFax}==null || $F{systemdefault_PhoneFax}.equals("") ? "" : ($F{systemdefault_PhoneFax}))
)]]></textFieldExpression>
				</textField>
				<image onErrorType="Blank">
					<reportElement key="image-5" mode="Opaque" x="-28" y="120" width="150" height="55" uuid="e19a89be-f3ae-4ca5-b974-e2aec76fc0a8"/>
					<imageExpression><![CDATA["../../images/HarmonyLogoPUTT.png"]]></imageExpression>
				</image>
				<image onErrorType="Blank">
					<reportElement key="image-6" mode="Opaque" x="201" y="142" width="51" height="33" uuid="b3e25b87-65f1-4862-bd65-9e83c6cf079d"/>
					<imageExpression><![CDATA["../../images/FidiLogo.png"]]></imageExpression>
				</image>
				<image onErrorType="Blank">
					<reportElement key="image-7" mode="Opaque" x="133" y="142" width="68" height="33" uuid="90455e55-5ac0-4f80-b73b-af2442447554"/>
					<imageExpression><![CDATA["../../images/IAM.png"]]></imageExpression>
				</image>
				<textField pattern="" isBlankWhenNull="true">
					<reportElement key="textField-168" x="0" y="175" width="471" height="25" isRemoveLineWhenBlank="true" forecolor="#009933" backcolor="#FFFFFF" uuid="62d327b8-9b06-4408-9283-a15f8689f93b"/>
					<box>
						<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
						<bottomPen lineWidth="0.0" lineColor="#000000"/>
						<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					</box>
					<textElement textAlignment="Left">
						<font fontName="Calibri" size="7"/>
					</textElement>
					<textFieldExpression><![CDATA["Save Paper Save Trees - Consider the environment."+"\n"+
"Do you really need to print this email? If you need to print it. it might be sufficient to print the first page only..."]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
