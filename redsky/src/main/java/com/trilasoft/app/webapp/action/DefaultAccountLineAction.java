package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;
import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.webapp.action.BaseAction;

import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.init.AppInitServlet;
import com.trilasoft.app.model.AccountLine;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.Contract;
import com.trilasoft.app.model.DefaultAccountLine;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.AccountLineManager;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.ContractManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.DefaultAccountLineManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RefMasterManager;
import com.google.gson.Gson;
import org.appfuse.model.User;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class DefaultAccountLineAction extends BaseAction implements Preparable {
	
	private DefaultAccountLineManager defaultAccountLineManager;

	private List defaultAccountLines;

	private List billToCodeList;
	private List contractList;
	private String sessionCorpID;
	private String contract;
	private String contractForCopy;
	private String accountInterface;
	private String hitFlag;
	private String hitFlaglist="";
	private DefaultAccountLine defaultAccountLine;
    private AccountLineManager accountLineManager;
	private Long id;
	private String contractBillCode; 
	private String custJobType;
	private Date custCreatedOn;
    public Contract objContract;
	private List refMasters;
	private String showMessage="";
	private String searchcontract;
	private String searchjobType;
	private String searchroute;
	private String searchmode;
	private String searchserviceType;
	private String searchpackMode;
	private String searchcommodity;
	private String searchoriginCountry;
	private String searchdestinationCountry;
	private String searchequipment;
	private String searchcompanyDivision;
	private String searchbillToCode;
	private Date todayDate;
	private List contractListNew;
	private List contractListFromContract; 
	private List contractListFromTemplate;
	private DefaultAccountLine copyListFromDefaultAccountLIne;
	

	private RefMasterManager refMasterManager;

	private static Map<String, String> category;

	private static Map<String, String> basis;

	private static Map<String, String> job;

	private static Map<String, String> routing;

	private static List mode;

	private static Map<String, String> service;
	
	private Map<String, String> pkmode;
	
	private Map<String, String> commodit;
	
    private Map<String, String> commodits;
    
    private static Map<String, String> ocountry;
    private static Map<String, String> dcountry;
	private String listData;
	private List<SystemDefault> sysDefaultDetail;
	private SystemDefault systemDefault;
	private String listFieldNames;

	private String listFieldEditability;

	private String listFieldTypes;
	private Boolean costElementFlag;
	private String listIdField;
	private ContractManager contractManager;
	private String itemType;
    private String changerOrderList;
	private List ls;
	private List chargesByTemplate;

	private List contracts = new ArrayList();

	private int countAcctTemplate; 
	public String OrgId; 
	private  Map<String,String>euVatList;
	private String estVatFlag;
	private Company company; 
	private CompanyManager companyManager;
	private  Map<String,String>euVatPercentList;
	private Map<String, String> EQUIP;
	private String systemDefaultVatCalculation;
	private  Map<String,String>payVatList;
	private  Map<String,String>payVatPercentList;
	private Integer companyDivisionGlobal;
	private CompanyDivisionManager companyDivisionManager;
	private String searchName;
	private String populateDataField;
	private String categoryCode;
	private String pageCondition;
	private String fromContract;
	private String toContract;
	private String checkFlag="";
	private String checkStatus="";
	private List chargessTemplate;
	private String[] copyTemplateId;
	private String showException="";
	private List matchTemplateList;
	private List misMatchTemplateList;
	private String  flexcode ;
	private List vatDescription;
	private String  description;
	private String partnerCode;
	private String vatBillimngGroupCode;
	private String flex1;
	private String flex2;
	private String companyDivision;
	private String flag;
	private String estVatDescr;
	private String estExpVatDescr;
	private  Map<String,String>flex1RecCodeList;
	private  Map<String,String>flex2PayCodeList;
	private  Map<String, String> vatBillingGroups = new LinkedHashMap<String, String>();
	public Map<String, String> getVatBillingGroups() {
		return vatBillingGroups;
	}

	public void setVatBillingGroups(Map<String, String> vatBillingGroups) {
		this.vatBillingGroups = vatBillingGroups;
	}

	public String getListData() {
		return listData;
	}

	public void setListData(String listData) {
		this.listData = listData;
	}

	public String getListFieldEditability() {
		return listFieldEditability;
	}

	public void setListFieldEditability(String listFieldEditability) {
		this.listFieldEditability = listFieldEditability;
	}

	public String getListFieldNames() {
		return listFieldNames;
	}

	public void setListFieldNames(String listFieldNames) {
		this.listFieldNames = listFieldNames;
	}

	public String getListFieldTypes() {
		return listFieldTypes;
	}

	public void setListFieldTypes(String listFieldTypes) {
		this.listFieldTypes = listFieldTypes;
	}

	public String getListIdField() {
		return listIdField;
	}

	public void setListIdField(String listIdField) {
		this.listIdField = listIdField;
	}
	Date currentdate = new Date();

	static final Logger logger = Logger.getLogger(DefaultAccountLineAction.class);

	 

	public DefaultAccountLineAction() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		this.sessionCorpID = user.getCorpID();
	}
	
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		logger.warn(" AJAX Call : "+getRequest().getParameter("ajax")); 
		if(getRequest().getParameter("ajax") == null){  
			OrgId = sessionCorpID;
			company=companyManager.findByCorpID(sessionCorpID).get(0);
			 if(company!=null){
				 if(company.getEstimateVATFlag()!=null){
						estVatFlag=company.getEstimateVATFlag();
					} 
				}
			accountInterface = defaultAccountLineManager.findAccountInterface(sessionCorpID).get(0).toString();
			costElementFlag = refMasterManager.getCostElementFlag(sessionCorpID);
			sysDefaultDetail = customerFileManager.findDefaultBookingAgentDetail(sessionCorpID);
			if (!(sysDefaultDetail == null || sysDefaultDetail.isEmpty())) {
				for (SystemDefault systemDefault1 : sysDefaultDetail) {
					systemDefault=systemDefault1;
					
			}}
		   systemDefaultVatCalculation =accountLineManager.findVatCalculation(sessionCorpID).get(0).toString();
		}
	}
	
	public List getDefaultAccountLines() {
		return defaultAccountLines;
	}
	
	public String list() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		defaultAccountLine=new DefaultAccountLine();
		if(hitFlaglist.equals("true")){
			defaultAccountLine.setContract(searchcontract);
			defaultAccountLine.setJobType(searchjobType);
			defaultAccountLine.setMode(searchmode);
			defaultAccountLine.setRoute(searchroute);
			defaultAccountLine.setServiceType(searchserviceType);
			defaultAccountLine.setPackMode(searchpackMode);
			defaultAccountLine.setCommodity(searchcommodity);
			defaultAccountLine.setOriginCountry(searchoriginCountry);
			defaultAccountLine.setDestinationCountry(searchdestinationCountry);
			defaultAccountLine.setEquipment(searchequipment);
			defaultAccountLine.setBillToCode(searchbillToCode);
			defaultAccountLine.setCompanyDivision(searchcompanyDivision);
			defaultAccountLine.setCategories("");
			defaultAccountLines = defaultAccountLineManager.findDefaultAccountLines(defaultAccountLine.getJobType(), defaultAccountLine.getRoute(), defaultAccountLine.getMode(),defaultAccountLine.getServiceType(), defaultAccountLine.getCategories(),defaultAccountLine.getBillToCode(),defaultAccountLine.getContract(),defaultAccountLine.getPackMode(),defaultAccountLine.getCommodity(), defaultAccountLine.getCompanyDivision(), defaultAccountLine.getOriginCountry(), defaultAccountLine.getDestinationCountry(),defaultAccountLine.getEquipment());
		}
		else{
		defaultAccountLines = defaultAccountLineManager.getAllValues(sessionCorpID);
		}
		defaultAccountLine.setCorpID(sessionCorpID);
		contracts = customerFileManager.findCustJobContract(defaultAccountLine.getBillToCode(), defaultAccountLine.getJobType(), defaultAccountLine.getCreatedOn(), sessionCorpID,"","");
		companyDivisionGlobal=companyDivisionManager.findGlobalCompanyCode(sessionCorpID);
		todayDate=currentdate;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String acctTemplateList(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		objContract =contractManager.get(id) ;
		defaultAccountLines=defaultAccountLineManager.getAcctTemplateList(contract,sessionCorpID);
		countAcctTemplate=defaultAccountLines.size();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;	
	}
	
	@SkipValidation
	public String addAndCopyAcctTemplate() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		chargesByTemplate = defaultAccountLineManager.findChargesByTemplate(sessionCorpID);	
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
		
	}
	@SkipValidation
	public String  copyTemplates(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 copyContractFromDefaultAccountLineList();
		int tempList = defaultAccountLineManager.findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(getRequest().getRemoteUser(),contractForCopy,sessionCorpID,contract);
		if(tempList>0){
			 showMessage = tempList+ "Template Added Successfully.";

			} 
		else
		{
			showMessage="Charge Codes do not exist in To-Contract";
		
		}
		  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
			hitFlag="1";
		return SUCCESS;	
	}
	private String gotoPageString;

	private CustomerFileManager customerFileManager;

	public String getGotoPageString() {

		return gotoPageString;

	}

	public void setGotoPageString(String gotoPageString) {

		this.gotoPageString = gotoPageString;

	}

	public String saveOnTabChange() throws Exception {

		String s = save();
		return gotoPageString;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public DefaultAccountLine getDefaultAccountLine() {
		return defaultAccountLine;
	}

	public void setDefaultAccountLine(DefaultAccountLine defaultAccountLine) {
		this.defaultAccountLine = defaultAccountLine;
	}

	public String delete() {
		getComboList(sessionCorpID);
		defaultAccountLineManager.remove(id);
		
		saveMessage(getText("defaultAccountLine.deleted"));
		defaultAccountLines = new ArrayList(defaultAccountLineManager.getAll());
		todayDate=currentdate;
		return SUCCESS;
	}
    
	private List comDivis;
	public String edit() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		accountInterface = defaultAccountLineManager.findAccountInterface(sessionCorpID).get(0).toString();
		getComboList(sessionCorpID);
		if (id != null) {
			defaultAccountLine = defaultAccountLineManager.get(id);
			defaultAccountLine.setCorpID(sessionCorpID);
			String permKey = sessionCorpID +"-"+"component.partner.vatBillingGroup.edit";
			boolean visibilityForCorpId=false;
			 visibilityForCorpId=AppInitServlet.pageFieldVisibilityComponentMap.containsKey(permKey) ;	
		 if(visibilityForCorpId){
				String flag="";
				String partnerVatDec="";
				String vatBillimngGroupDes="";
				defaultAccountLine = defaultAccountLineManager.get(id); 
			 if(defaultAccountLine.getBillToCode()!=null && !defaultAccountLine.getBillToCode().equals("")){
					String vatBillimngGroupCode="";
					List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, defaultAccountLine.getBillToCode());
					Iterator listIterator= vatBillimngGroupCode1.iterator();
					while (listIterator.hasNext()) {
					 Object [] row=(Object[])listIterator.next();
						if(row[0]!=null) {
						 vatBillimngGroupCode=row[0].toString();  
						 }
						 if(row[1]!=null){
						partnerVatDec = row[1].toString();  
						}
						}
					if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
						vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);	
					}
				 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,defaultAccountLine.getCompanyDivision());
					if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex1!=null && (!(flex1.trim().equals(""))) ){
					  euVatList=refMasterManager.findPartnerVatDescription(sessionCorpID, "EUVAT", flex1, defaultAccountLine.getEstVatDescr(), "Y");  
				
					}
			 }
			 if(defaultAccountLine.getVendorCode()!=null && !defaultAccountLine.getVendorCode().equals("")){
					String vatBillimngGroupCode="";
					List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, defaultAccountLine.getVendorCode());
					Iterator listIterator= vatBillimngGroupCode1.iterator();
					while (listIterator.hasNext()) {
					 Object [] row=(Object[])listIterator.next();
						if(row[0]!=null) {
						 vatBillimngGroupCode=row[0].toString();  
						 }
						 if(row[1]!=null){
						partnerVatDec = row[1].toString();  
						}
						}
					if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
						vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);	
					}
				 flex2  = refMasterManager.findAccountLinePayVatBillingCode(vatBillimngGroupDes, sessionCorpID, defaultAccountLine.getCompanyDivision());
					if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("") && flex2!=null && (!(flex2.trim().equals("")))){
						 payVatList=refMasterManager.findPayVatDescription(sessionCorpID, "PAYVATDESC", flex2, defaultAccountLine.getEstExpVatDescr(), "Y");
					}
					 
			 }
			 }
		} else {
			defaultAccountLine = new DefaultAccountLine();
			//defaultAccountLine.setOrderNumber("001");
			defaultAccountLine.setCreatedOn(new Date());
			defaultAccountLine.setUpdatedOn(new Date());
			if(systemDefault.getReceivableVat()!=null && (!(systemDefault.getReceivableVat().toString().equals("")))){
				defaultAccountLine.setEstVatDescr(systemDefault.getReceivableVat());
				if(euVatPercentList.containsKey(systemDefault.getReceivableVat())){
					defaultAccountLine.setEstVatPercent(euVatPercentList.get(systemDefault.getReceivableVat()))	;
				}
			}
			if(systemDefault.getPayableVat()!=null && (!(systemDefault.getPayableVat().toString().equals("")))){
				defaultAccountLine.setEstExpVatDescr(systemDefault.getPayableVat());
				if(payVatPercentList.containsKey(systemDefault.getPayableVat())){
					defaultAccountLine.setEstExpVatPercent(payVatPercentList.get(systemDefault.getPayableVat()))	;
				}
			}
			
		}
		defaultAccountLine.setCorpID(sessionCorpID);
		contracts = customerFileManager.findCustJobContract(defaultAccountLine.getBillToCode(), defaultAccountLine.getJobType(), defaultAccountLine.getCreatedOn(), sessionCorpID,"","");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	public String save() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		if (cancel != null) {
			return "cancel";
		}

		if (delete != null) {
			return delete();
		}
		boolean isNew = (defaultAccountLine.getId() == null);
		if (isNew) {
			defaultAccountLine.setCreatedOn(new Date());
		}
		defaultAccountLine.setUpdatedOn(new Date());
		defaultAccountLine.setUpdatedBy(getRequest().getRemoteUser());
		defaultAccountLine.setCorpID(sessionCorpID);
		if(defaultAccountLine.getVendorCode()!=null && (!(defaultAccountLine.getVendorCode().toString().equals("")))){
	        List accountLineList;
	          accountLineList = accountLineManager.vendorName(defaultAccountLine.getVendorCode(),sessionCorpID,"",false);
	         if(!accountLineList.isEmpty()){
	          Iterator it = accountLineList.iterator();
	          while(it.hasNext()){
	        	  Object [] row=(Object[])it.next();
	        	  defaultAccountLine.setVendorName(row[0].toString());
	          }
	        }else{
	        	String message="The Vendor Name is not exist";
	        	errorMessage(getText(message));
	        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	        	return SUCCESS;
	        }
		}
		
		defaultAccountLine = defaultAccountLineManager.save(defaultAccountLine);
		String key = (isNew) ? "defaultAccountLine.added" : "defaultAccountLine.updated";
		saveMessage(getText(key));
		contracts = customerFileManager.findCustJobContract(defaultAccountLine.getBillToCode(), defaultAccountLine.getJobType(), defaultAccountLine.getCreatedOn(), sessionCorpID,"","");
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	@SkipValidation
	public String search() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
			try{
			boolean jobType = (defaultAccountLine.getJobType() == null);
			boolean route = (defaultAccountLine.getRoute() == null);
			boolean mode = (defaultAccountLine.getMode() == null);
			boolean serviceType = (defaultAccountLine.getServiceType() == null);
			boolean categories = (defaultAccountLine.getCategories() == null);
			boolean billToCode = (defaultAccountLine.getBillToCode() == null);
			boolean contract = (defaultAccountLine.getContract() == null);
			boolean packMode=(defaultAccountLine.getPackMode() == null);
			boolean commodity=(defaultAccountLine.getCommodity() == null);
			boolean companyDivi=(defaultAccountLine.getCompanyDivision() == null);
			boolean ocountry=(defaultAccountLine.getOriginCountry()== null);
			boolean dcountry=(defaultAccountLine.getDestinationCountry()== null);
			boolean equipment=(defaultAccountLine.getEquipment()== null);
			if (!jobType || !route || !mode || !serviceType || !categories || !billToCode || !contract ||!packMode||!commodity || !companyDivi || !ocountry || !dcountry || !equipment) {
				defaultAccountLines = defaultAccountLineManager.findDefaultAccountLines(defaultAccountLine.getJobType(), defaultAccountLine.getRoute(),defaultAccountLine.getMode(), defaultAccountLine.getServiceType(), defaultAccountLine.getCategories(), defaultAccountLine.getBillToCode(),defaultAccountLine.getContract(),defaultAccountLine.getPackMode(),defaultAccountLine.getCommodity(), defaultAccountLine.getCompanyDivision(), defaultAccountLine.getOriginCountry(), defaultAccountLine.getDestinationCountry(),defaultAccountLine.getEquipment());
			}
			contracts = customerFileManager.findCustJobContract(defaultAccountLine.getBillToCode(), defaultAccountLine.getJobType(), defaultAccountLine.getCreatedOn(), sessionCorpID,"","");
		}catch(Exception e){
			defaultAccountLines = defaultAccountLineManager.findDefaultAccountLines("", "", "","", "","","","","", "", "", "","");
			contracts = customerFileManager.findCustJobContract("", "", null, sessionCorpID,"","");
			e.printStackTrace();
		}
		companyDivisionGlobal=companyDivisionManager.findGlobalCompanyCode(sessionCorpID);
		todayDate=currentdate;
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String saveUpdates(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		//System.out.println(">>>>>>>>>>>>>>"+changerOrderList);
		try{
		if(!changerOrderList.equals("")){
		   String[] str=changerOrderList.split(",");
		  for(int i=0;i<str.length;i++){
			  String[] str1=str[i].split("#");
			  defaultAccountLineManager.saveBulkUpdates(str1[0],str1[1]);
		  }
		    String key =  "Order# has been updated ";
			saveMessage(getText(key));
		}	
			search() ;
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private String fieldname2;
    private String fieldValue2;
	@SkipValidation
	public String saveUpdatesAll(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{ 
			if(!changerOrderList.equals("")){
				String createdBy=getRequest().getRemoteUser();
			  defaultAccountLineManager.saveUpdatesList(changerOrderList,sessionCorpID,createdBy);
		  }
			search() ;
		}catch(Exception e){
			e.printStackTrace();
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	
	@SkipValidation
	public String findContracOnJobBasis() {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		contracts = defaultAccountLineManager.findContracOnJobBasis(contractBillCode, custJobType, custCreatedOn, sessionCorpID);
		if (id != null){ 
			defaultAccountLine = defaultAccountLineManager.get(id);
		//defaultAccountLine = new DefaultAccountLine();
		if(defaultAccountLine !=null &&  defaultAccountLine.getContract()!=null && !defaultAccountLine.getContract().equals(""))
		{
			if(!contracts.contains(defaultAccountLine.getContract()))
			{
				contracts.add(defaultAccountLine.getContract());
			}
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	public Long getId() {
		return id;
	}

	public void setDefaultAccountLineManager(DefaultAccountLineManager defaultAccountLineManager) {
		this.defaultAccountLineManager = defaultAccountLineManager;
	}

	public String getComboList(String corpID) {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		category = refMasterManager.findByParameter(corpID, "ACC_CATEGORY");
		basis = refMasterManager.findByParameter(corpID, "ACC_BASIS");
		job = refMasterManager.findByParameter(corpID, "JOB");
		routing = refMasterManager.findByParameter(corpID, "ROUTING");
		mode = refMasterManager.findByParameters(corpID, "MODE");
		service = refMasterManager.findByParameter(corpID, "SERVICE");
		pkmode = refMasterManager.findByParameter(corpID, "PKMODE");
		commodit = refMasterManager.findByParameter(corpID, "COMMODIT");
	 	commodits = refMasterManager.findByParameter(corpID, "COMMODITS");
	 	billToCodeList=defaultAccountLineManager.findBillToCode(corpID);
	 	contractList=defaultAccountLineManager.findContractList(corpID);
	 	comDivis=customerFileManager.findCompanyDivision(corpID);
	 	euVatList = refMasterManager.findByParameterWithoutParent(corpID, "EUVAT");
	 	euVatPercentList = refMasterManager.findVatPercentList(corpID, "EUVAT");
	 	ocountry = refMasterManager.findDocumentList(corpID, "COUNTRY");
		dcountry = refMasterManager.findDocumentList(corpID, "COUNTRY");
		EQUIP = refMasterManager.findByParameter(corpID, "EQUIP");
		payVatList = refMasterManager.findByParameterWithoutParent (corpID, "PAYVATDESC");
		payVatPercentList = refMasterManager.findVatPercentList(corpID, "PAYVATDESC");
		vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
	      flex1RecCodeList=refMasterManager.findRecVatDescriptionData(sessionCorpID, "vatBillingGroup");
		  flex2PayCodeList=refMasterManager.findPayVatDescriptionData(sessionCorpID, "vatBillingGroup");
	    contractListNew=defaultAccountLineManager.findContractListNEW(corpID);
	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	private List SearchforAutocompleteListDefaultAccountList;
	 private String SearchforGsonDataDefaultAccountList;  
	
	@SkipValidation 
	public String searchNameAutocompleteList(){
		try {
			
			if(searchName.length()>=3){
				SearchforAutocompleteListDefaultAccountList = defaultAccountLineManager.findSearchNameAutocompleteListFromDefaultAccountList(searchName,sessionCorpID,populateDataField);
			}
			SearchforGsonDataDefaultAccountList = new Gson().toJson(SearchforAutocompleteListDefaultAccountList);
		}
		catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		return SUCCESS;
	}
	
	private List SearchforAutocompleteListChargeCode;
	private String SearchforGsonDataChargeCode;
	private String chargeCodeId;
	private String autocompleteDivId;
	private String idCondition;
	private String originCountry;
	private String destinationCountry;
	private String serviceMode;
	private String checkListAll="";
	 
	@SkipValidation 
	public String searchChargeCodeAutocompleteList(){
		try {
			  
			/*if(!categoryCode.equalsIgnoreCase("Internal Cost") && pageCondition.equalsIgnoreCase("other")){
			if(categoryCode==null){
				categoryCode="";
			}else{
				 if(!costElementFlag)	{
					 categoryCode="";
				 }
			}
			}*/
			if(searchName.length()>=3){
				SearchforAutocompleteListChargeCode = defaultAccountLineManager.findSearchChargeCodeAutocompleteList(searchName,sessionCorpID,contract,pageCondition,searchcompanyDivision,"","","",categoryCode);
			}
			/*if(searchName.length()>=3 && categoryCode.equalsIgnoreCase("Internal Cost") && pageCondition.equalsIgnoreCase("AccountingTemplate")){
				SearchforAutocompleteListChargeCode = defaultAccountLineManager.findSearchChargeCodeAutocompleteList(searchName,sessionCorpID,contract,"AccountingTemplateInternal",searchcompanyDivision,"","","",categoryCode);
			}
			if(searchName.length()>=3 && categoryCode.equalsIgnoreCase("Internal Cost") && pageCondition.equalsIgnoreCase("other")){
				SearchforAutocompleteListChargeCode = defaultAccountLineManager.findSearchChargeCodeAutocompleteList(searchName,sessionCorpID,contract,"forminternal",searchcompanyDivision,"","","",categoryCode);
			}
			if(searchName.length()>=3 && !categoryCode.equalsIgnoreCase("Internal Cost") && pageCondition.equalsIgnoreCase("other")){
				SearchforAutocompleteListChargeCode = defaultAccountLineManager.findSearchChargeCodeAutocompleteList(searchName,sessionCorpID,contract,"formother",searchcompanyDivision,"","","",categoryCode);
			}*/
			SearchforGsonDataChargeCode = new Gson().toJson(SearchforAutocompleteListChargeCode);
			if(SearchforAutocompleteListChargeCode.isEmpty() && SearchforAutocompleteListChargeCode.size()<0){
				SearchforAutocompleteListChargeCode.add("EmptyValue");
			}
		}
		catch (Exception e) {
			String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	    	 e.printStackTrace();
	    	 return CANCEL;
		}
		return SUCCESS;
	}
	
	@SkipValidation 
	public String copyContractFromDefaultAccountLineList(){
		try {
			contractListFromContract=defaultAccountLineManager.findContractListFromContract(sessionCorpID);
			contractListFromTemplate=defaultAccountLineManager.findContractListFromTemplate(sessionCorpID);
		}
		catch (Exception e) {
				String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
				String logMethod =   e.getStackTrace()[0].getMethodName().toString();
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
				e.printStackTrace();
				return CANCEL;
		}
		return SUCCESS;
	}
	
	@SkipValidation
	public String copyAll() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{ 
			 copyContractFromDefaultAccountLineList();
			if((fromContract!=null && !fromContract.equals("")) && (toContract!=null && !toContract.equals("")))
			{
			int tempList = defaultAccountLineManager.findChargeCodeForMissingDefaultAccountLineContractWithoutValidation(getRequest().getRemoteUser(),fromContract,sessionCorpID,toContract);
			if(tempList>0){
				 showMessage = tempList+ "Template Added Successfully.";
				
				} 
			else
			{
				showMessage="Charge Codes do not exist in To-Contract";
			
			}
			
		    } 
		}catch(Exception e){ 
	    	  e.printStackTrace();
	    	  return ERROR;
		}
		
		
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}
	@SkipValidation
	public String copyDefaultAccountLine() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		try{
		if((checkFlag!=null && checkFlag.equals("")) && (checkStatus!=null && checkStatus.equals(""))){
			chargessTemplate = defaultAccountLineManager.findChargeCodeForMissingDefaultAccountLineContract(fromContract,sessionCorpID,toContract);
			 checkStatus="NO";
			 if(chargessTemplate.size()>0 && !chargessTemplate.isEmpty() && chargessTemplate!=null){
				 checkFlag="Y";
				 checkStatus="Y";
				 matchTemplateList = defaultAccountLineManager.findSameDefaultAccountLineContract(fromContract,sessionCorpID,toContract);
				 if(matchTemplateList==null){
					 matchTemplateList=new ArrayList();
				 }
					 if(matchTemplateList.size()==chargessTemplate.size()){
						 
					 }else{
						 misMatchTemplateList=new ArrayList(); 
						 misMatchTemplateList = defaultAccountLineManager.findChargeCodeForMisMatchFromCharges(fromContract,sessionCorpID,toContract);
						 hitFlag="5";
					 }
			 }
			 if(chargessTemplate!=null && chargessTemplate.size()>0){
			 Collections.sort(chargessTemplate, new ChargeCodeComp()); 
			 }
			 
		}
		if((checkStatus!=null && checkStatus.equals("NO"))){
			try{
			 matchTemplateList = defaultAccountLineManager.findSameDefaultAccountLineContract(fromContract,sessionCorpID,toContract);
			if(matchTemplateList.size()>0 && !matchTemplateList.isEmpty() && matchTemplateList!=null){
				hitFlag="4";
			 }else{
				 misMatchTemplateList = defaultAccountLineManager.findChargeCodeForMisMatchDefaultAccountLine(fromContract,sessionCorpID,toContract);
				 hitFlag="3";
			 }
			}
			
			catch(Exception e){
				  hitFlag="2";
				  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
		    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
		    	  showException=logMessage+logMethod;
		    	  copyContractFromDefaultAccountLineList();
		    	  e.printStackTrace();
		    	  return ERROR;
			}
			
			 
		 }
		if(misMatchTemplateList!=null && misMatchTemplateList.size()>0){
				Collections.sort(misMatchTemplateList, new ChargeCodeComp());
		}
		}catch(Exception e){
			  hitFlag="2";
			  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  showException=logMessage+logMethod;
	    	  copyContractFromDefaultAccountLineList();
	    	  e.printStackTrace();
	    	  return ERROR;
		}
		try{
		if(checkFlag!=null && checkFlag.equals("")){
			if(copyTemplateId!=null && copyTemplateId.length>0){
			for(int i=0;i<copyTemplateId.length;i++){
				if(checkListAll.equals("")){
					checkListAll = ""+copyTemplateId[i].toString()+"";
				}
				else{
					checkListAll = checkListAll+","+""+copyTemplateId[i].toString()+"";
				}
			}
			/*if(checkListAll!=null){
				checkListAll="'"+checkListAll+"'";
			}*/
				int templateList = defaultAccountLineManager.getChargeTemplateCopy(fromContract,sessionCorpID,checkListAll,toContract);
				  /*Iterator it = templateList.iterator();
				  while(it.hasNext()){
						DefaultAccountLine defaultAccountLinecharges = (DefaultAccountLine)it.next();
						defaultAccountLine = new DefaultAccountLine();
						
						defaultAccountLine.setCorpID(sessionCorpID);
						defaultAccountLine.setCreatedOn(new Date());
						defaultAccountLine.setCreatedBy(getRequest().getRemoteUser());
						defaultAccountLine.setUpdatedBy(getRequest().getRemoteUser());
						defaultAccountLine.setUpdatedOn(new Date());
						defaultAccountLine.setAmount(defaultAccountLinecharges.getAmount());
						defaultAccountLine.setQuantity(defaultAccountLinecharges.getQuantity());
						defaultAccountLine.setRate(defaultAccountLinecharges.getRate());
						defaultAccountLine.setBasis(defaultAccountLinecharges.getBasis());
						defaultAccountLine.setMode(defaultAccountLinecharges.getMode());
						defaultAccountLine.setJobType(defaultAccountLinecharges.getJobType());
						defaultAccountLine.setServiceType(defaultAccountLinecharges.getServiceType());
						defaultAccountLine.setChargeBack(defaultAccountLinecharges.getChargeBack());
						defaultAccountLine.setCategories(defaultAccountLinecharges.getCategories());
						defaultAccountLine.setRoute(defaultAccountLinecharges.getRoute());
						defaultAccountLine.setVendorCode(defaultAccountLinecharges.getVendorCode());
						defaultAccountLine.setVendorName(defaultAccountLinecharges.getVendorName());
						defaultAccountLine.setContract(toContract);
						defaultAccountLine.setChargeCode(defaultAccountLinecharges.getChargeCode());
						defaultAccountLine.setRecGl(defaultAccountLinecharges.getRecGl());
						defaultAccountLine.setPayGl(defaultAccountLinecharges.getPayGl());
						defaultAccountLine.setBillToCode(defaultAccountLinecharges.getBillToCode());
						defaultAccountLine.setBillToName(defaultAccountLinecharges.getBillToName());
						defaultAccountLine.setDescription(defaultAccountLinecharges.getDescription());
						defaultAccountLine.setCommodity(defaultAccountLinecharges.getCommodity());
						defaultAccountLine.setPackMode(defaultAccountLinecharges.getPackMode());
						defaultAccountLine.setOrderNumber(defaultAccountLinecharges.getOrderNumber());
						defaultAccountLine.setMarkUp(defaultAccountLinecharges.getMarkUp());
						defaultAccountLine.setSellRate(defaultAccountLinecharges.getSellRate());
						defaultAccountLine.setEstimatedRevenue(defaultAccountLinecharges.getEstimatedRevenue());
						defaultAccountLine.setCompanyDivision(defaultAccountLinecharges.getCompanyDivision());
						defaultAccountLine.setEstVatPercent(defaultAccountLinecharges.getEstVatPercent());
						defaultAccountLine.setEstVatDescr(defaultAccountLinecharges.getEstVatDescr());
						defaultAccountLine.setEstVatAmt(defaultAccountLinecharges.getEstVatAmt());
						defaultAccountLine.setOriginCountry(defaultAccountLinecharges.getOriginCountry());
						defaultAccountLine.setDestinationCountry(defaultAccountLinecharges.getDestinationCountry());
						defaultAccountLine.setEquipment(defaultAccountLinecharges.getEquipment());
						defaultAccountLine.setEstExpVatPercent(defaultAccountLinecharges.getEstExpVatPercent());
						defaultAccountLine.setEstExpVatDescr(defaultAccountLinecharges.getEstExpVatDescr());
						defaultAccountLineManager.save(defaultAccountLine);
						hitFlag="1";
				  }*/
				if(templateList>0){
					
				hitFlag="1";
				}
			
		}
		}
		}catch(Exception e){
			  hitFlag="2";
			  String logMessage =  "Exception:"+ e.toString()+" at #"+e.getStackTrace()[0].toString();
	    	  String logMethod =   e.getStackTrace()[0].getMethodName().toString();
	    	  showException=logMessage+logMethod;
	    	  copyContractFromDefaultAccountLineList();
	    	  e.printStackTrace();
	    	  return ERROR;
		}
		if(hitFlag!=null && hitFlag.equals("2")){
			checkStatus="";
			 showMessage =  "AccountLine Template Could not be Copied. Pls try Again.";
		}
		if(hitFlag!=null && hitFlag.equals("3")){
			 showMessage =  "Charge Code does not match. Accounting Template cannot be copied.";
		}
		if(hitFlag!=null && hitFlag.equals("5")){
			 showMessage =  "The following Charge Codes do not exist in To-Contract.";
		}
		if(hitFlag!=null && hitFlag.equals("4")){
			 checkStatus="";
			 showMessage =  "Accounting templates already exist in To-Contract. Please select different Contracts.";
		}
		copyContractFromDefaultAccountLineList();
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		return SUCCESS;
	}

	private PartnerPrivateManager partnerPrivateManager;
	public PartnerPrivateManager getPartnerPrivateManager() {
		return partnerPrivateManager;
	}

	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}

	//function to findoutRecVatDescription
	@SkipValidation
	public String  findRecVatDefaultAccountLine() {
	      try {
				logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
				
				String flag="";
				String vatBillimngGroupCode="";
				String vatBillimngGroupDes="";
				String partnerVatDec="";
				vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
				List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, partnerCode);
				Iterator listIterator= vatBillimngGroupCode1.iterator();
				while (listIterator.hasNext()) {
				 Object [] row=(Object[])listIterator.next();
					if(row[0]!=null) {
					 vatBillimngGroupCode=row[0].toString();  
					 }
					 if(row[1]!=null){
					partnerVatDec = row[1].toString();  
					}
					}
				if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
					vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);
				}
					 flex1  = refMasterManager.findPartnerVatBillingCode(vatBillimngGroupDes, sessionCorpID,companyDivision);
					if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("")){
					vatDescription=refMasterManager.findAccountLineRecVatCodeDescription(flex1, estVatDescr,partnerVatDec, sessionCorpID,"Y");
					}
					 else{
					vatDescription=refMasterManager.findAccountLineRecVatCodeDescription(flex1, estVatDescr,partnerVatDec, sessionCorpID,flag);
					}
	      }
	      catch (Exception e) {
				e.printStackTrace();
		    	 
			}
	      return SUCCESS;
				   
	}
	 //function to findoutPayVatDescription
	@SkipValidation
	public String  findPayVatDefaultAccountLine() {
         try {
        		String flag="";
        		String vatBillimngGroupCode="";
        		String vatBillimngGroupDes="";
        		String partnerVatDec="";
        		vatBillingGroups = refMasterManager.findByParameter(sessionCorpID, "vatBillingGroup");
        		List vatBillimngGroupCode1=partnerPrivateManager.findVatBillimngGroup(sessionCorpID, partnerCode);
        				Iterator listIterator= vatBillimngGroupCode1.iterator();
        				while (listIterator.hasNext()) {
        						 Object [] row=(Object[])listIterator.next();
        						  if(row[0]!=null) {
        							  vatBillimngGroupCode=row[0].toString();  
        						  }
        						  if(row[1]!=null){
        							  partnerVatDec = row[1].toString();  
        						  }
        					}
        				if(vatBillingGroups.containsKey(vatBillimngGroupCode)){
        					vatBillimngGroupDes =vatBillingGroups.get(vatBillimngGroupCode);
        				}
        				  flex2  = refMasterManager.findAccountLinePayVatBillingCode(vatBillimngGroupDes, sessionCorpID,companyDivision);
        				  if(vatBillimngGroupCode!=null && !vatBillimngGroupCode.equals("")){
        					  vatDescription=refMasterManager.findAccountLinePayVatCodeDescription(flex2, estExpVatDescr, sessionCorpID, "Y");
        				  }
        				  else{
        					  vatDescription=refMasterManager.findAccountLinePayVatCodeDescription(flex2, estExpVatDescr, sessionCorpID, flag);
        					  }
         
	           } catch (Exception e) {
                e.printStackTrace();
    	 
	}
	   
	    	return SUCCESS;
	}
	
	class ChargeCodeComp implements Comparator
	{

		public int compare(Object o1, Object o2) {
			DefaultAccountLine d1 = (DefaultAccountLine)o1;
			DefaultAccountLine d2 = (DefaultAccountLine)o2;
			return d1.getChargeCode().compareTo(d2.getChargeCode());
		}
		
	}
	
	

	public Map<String, String> getCategory() {
		return category;
	}

	public Map<String, String> getBasis() {
		return basis;
	}

	public Map<String, String> getJob() {
		return job;
	}

	public Map<String, String> getRouting() {
		return routing;
	}

	public Map<String, String> getService() {
		return service;
	}

	public void setRefMasterManager(RefMasterManager refMasterManager) {
		this.refMasterManager = refMasterManager;
	}

	public List getRefMasters() {
		return refMasters;
	}

	public void setRefMasters(List refMasters) {
		this.refMasters = refMasters;
	}

	public RefMasterManager getRefMasterManager() {
		return refMasterManager;
	}

	public void setDefaultAccountLines(List defaultAccountLines) {
		this.defaultAccountLines = defaultAccountLines;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public static void setBasis(Map<String, String> basis) {
		DefaultAccountLineAction.basis = basis;
	}

	public static void setCategory(Map<String, String> category) {
		DefaultAccountLineAction.category = category;
	}

	public static void setJob(Map<String, String> job) {
		DefaultAccountLineAction.job = job;
	}

	public static void setRouting(Map<String, String> routing) {
		DefaultAccountLineAction.routing = routing;
	}

	public static void setService(Map<String, String> service) {
		DefaultAccountLineAction.service = service;
	}

	public static List getMode() {
		return mode;
	}

	public static void setMode(List mode) {
		DefaultAccountLineAction.mode = mode;
	}

	public List getLs() {
		return ls;
	}

	public void setLs(List ls) {
		this.ls = ls;
	}

	public String getSessionCorpID() {
		return sessionCorpID;
	}

	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}

	public DefaultAccountLineManager getDefaultAccountLineManager() {
		return defaultAccountLineManager;
	}

	public String getAccountInterface() {
		return accountInterface;
	}

	public void setAccountInterface(String accountInterface) {
		this.accountInterface = accountInterface;
	}

	public List getContracts() {
		return contracts;
	}

	public void setContracts(List contracts) {
		this.contracts = contracts;
	}

	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}

	public Map<String, String> getPkmode() {
		return pkmode;
	}

	public void setPkmode(Map<String, String> pkmode) {
		this.pkmode = pkmode;
	}

	public Map<String, String> getCommodit() {
		return commodit;
	}

	public void setCommodit(Map<String, String> commodit) {
		this.commodit = commodit;
	}

	public Map<String, String> getCommodits() {
		return commodits;
	}

	public void setCommodits(Map<String, String> commodits) {
		this.commodits = commodits;
	}

	public List getBillToCodeList() {
		return billToCodeList;
	}

	public void setBillToCodeList(List billToCodeList) {
		this.billToCodeList = billToCodeList;
	}

	public List getContractList() {
		return contractList;
	}

	public void setContractList(List contractList) {
		this.contractList = contractList;
	}

	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}

	public int getCountAcctTemplate() {
		return countAcctTemplate;
	}

	public void setCountAcctTemplate(int countAcctTemplate) {
		this.countAcctTemplate = countAcctTemplate;
	}

	public List getChargesByTemplate() {
		return chargesByTemplate;
	}

	public void setChargesByTemplate(List chargesByTemplate) {
		this.chargesByTemplate = chargesByTemplate;
	}

	public String getHitFlag() {
		return hitFlag;
	}

	public void setHitFlag(String hitFlag) {
		this.hitFlag = hitFlag;
	}

	public String getContractForCopy() {
		return contractForCopy;
	}

	public void setContractForCopy(String contractForCopy) {
		this.contractForCopy = contractForCopy;
	}

	public void setAccountLineManager(AccountLineManager accountLineManager) {
		this.accountLineManager = accountLineManager;
	}

	public String getChangerOrderList() {
		return changerOrderList;
	}

	public void setChangerOrderList(String changerOrderList) {
		this.changerOrderList = changerOrderList;
	}

	public String getContractBillCode() {
		return contractBillCode;
	}

	public void setContractBillCode(String contractBillCode) {
		this.contractBillCode = contractBillCode;
	}

	public Date getCustCreatedOn() {
		return custCreatedOn;
	}

	public void setCustCreatedOn(Date custCreatedOn) {
		this.custCreatedOn = custCreatedOn;
	}

	public String getCustJobType() {
		return custJobType;
	}

	public void setCustJobType(String custJobType) {
		this.custJobType = custJobType;
	}

	public List getComDivis() {
		return comDivis;
	}

	public void setComDivis(List comDivis) {
		this.comDivis = comDivis;
	}

	public String getOrgId() {
		return OrgId;
	}

	public void setOrgId(String orgId) {
		OrgId = orgId;
	}

	public Contract getObjContract() {
		return objContract;
	}

	public void setObjContract(Contract objContract) {
		this.objContract = objContract;
	}
	public void setContractManager(ContractManager contractManager) {
		this.contractManager = contractManager;
	}

	public Map<String, String> getEuVatList() {
		return euVatList;
	}

	public void setEuVatList(Map<String, String> euVatList) {
		this.euVatList = euVatList;
	}

	public String getEstVatFlag() {
		return estVatFlag;
	}

	public void setEstVatFlag(String estVatFlag) {
		this.estVatFlag = estVatFlag;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}

	public Map<String, String> getEuVatPercentList() {
		return euVatPercentList;
	}

	public void setEuVatPercentList(Map<String, String> euVatPercentList) {
		this.euVatPercentList = euVatPercentList;
	}

	public Boolean getCostElementFlag() {
		return costElementFlag;
	}

	public void setCostElementFlag(Boolean costElementFlag) {
		this.costElementFlag = costElementFlag;
	}

	public static Map<String, String> getOcountry() {
		return ocountry;
	}

	public static Map<String, String> getDcountry() {
		return dcountry;
	}

	public static void setOcountry(Map<String, String> ocountry) {
		DefaultAccountLineAction.ocountry = ocountry;
	}

	public static void setDcountry(Map<String, String> dcountry) {
		DefaultAccountLineAction.dcountry = dcountry;
	}

	public Map<String, String> getEQUIP() {
		return EQUIP;
	}

	public void setEQUIP(Map<String, String> eQUIP) {
		EQUIP = eQUIP;
	}

	public String getSystemDefaultVatCalculation() {
		return systemDefaultVatCalculation;
	}

	public void setSystemDefaultVatCalculation(String systemDefaultVatCalculation) {
		this.systemDefaultVatCalculation = systemDefaultVatCalculation;
	}

	public Map<String, String> getPayVatList() {
		return payVatList;
	}

	public void setPayVatList(Map<String, String> payVatList) {
		this.payVatList = payVatList;
	}

	public Map<String, String> getPayVatPercentList() {
		return payVatPercentList;
	}

	public void setPayVatPercentList(Map<String, String> payVatPercentList) {
		this.payVatPercentList = payVatPercentList;
	}

	public void setFieldname2(String fieldname2) {
		this.fieldname2 = fieldname2;
	}

	public void setFieldValue2(String fieldValue2) {
		this.fieldValue2 = fieldValue2;
	}

	public String getShowMessage() {
		return showMessage;
	}

	public void setShowMessage(String showMessage) {
		this.showMessage = showMessage;
	}

	public String getSearchcontract() {
		return searchcontract;
	}

	public void setSearchcontract(String searchcontract) {
		this.searchcontract = searchcontract;
	}

	public String getSearchjobType() {
		return searchjobType;
	}

	public void setSearchjobType(String searchjobType) {
		this.searchjobType = searchjobType;
	}

	public String getSearchroute() {
		return searchroute;
	}

	public void setSearchroute(String searchroute) {
		this.searchroute = searchroute;
	}

	public String getSearchmode() {
		return searchmode;
	}

	public void setSearchmode(String searchmode) {
		this.searchmode = searchmode;
	}

	public String getSearchserviceType() {
		return searchserviceType;
	}

	public void setSearchserviceType(String searchserviceType) {
		this.searchserviceType = searchserviceType;
	}

	public String getSearchpackMode() {
		return searchpackMode;
	}

	public void setSearchpackMode(String searchpackMode) {
		this.searchpackMode = searchpackMode;
	}

	public String getSearchcommodity() {
		return searchcommodity;
	}

	public void setSearchcommodity(String searchcommodity) {
		this.searchcommodity = searchcommodity;
	}

	public String getSearchoriginCountry() {
		return searchoriginCountry;
	}

	public void setSearchoriginCountry(String searchoriginCountry) {
		this.searchoriginCountry = searchoriginCountry;
	}

	public String getSearchdestinationCountry() {
		return searchdestinationCountry;
	}

	public void setSearchdestinationCountry(String searchdestinationCountry) {
		this.searchdestinationCountry = searchdestinationCountry;
	}

	public String getSearchequipment() {
		return searchequipment;
	}

	public void setSearchequipment(String searchequipment) {
		this.searchequipment = searchequipment;
	}

	public String getSearchcompanyDivision() {
		return searchcompanyDivision;
	}

	public void setSearchcompanyDivision(String searchcompanyDivision) {
		this.searchcompanyDivision = searchcompanyDivision;
	}

	public String getSearchbillToCode() {
		return searchbillToCode;
	}

	public void setSearchbillToCode(String searchbillToCode) {
		this.searchbillToCode = searchbillToCode;
	}

	public String getHitFlaglist() {
		return hitFlaglist;
	}

	public void setHitFlaglist(String hitFlaglist) {
		this.hitFlaglist = hitFlaglist;
	}

	public Integer getCompanyDivisionGlobal() {
		return companyDivisionGlobal;
	}

	public void setCompanyDivisionGlobal(Integer companyDivisionGlobal) {
		this.companyDivisionGlobal = companyDivisionGlobal;
	}

	public CompanyDivisionManager getCompanyDivisionManager() {
		return companyDivisionManager;
	}

	public void setCompanyDivisionManager(
			CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}

	public String getPopulateDataField() {
		return populateDataField;
	}

	public void setPopulateDataField(String populateDataField) {
		this.populateDataField = populateDataField;
	}

	public String getSearchName() {
		return searchName;
	}

	public void setSearchName(String searchName) {
		this.searchName = searchName;
	}

	public String getSearchforGsonDataDefaultAccountList() {
		return SearchforGsonDataDefaultAccountList;
	}

	public void setSearchforGsonDataDefaultAccountList(
			String searchforGsonDataDefaultAccountList) {
		SearchforGsonDataDefaultAccountList = searchforGsonDataDefaultAccountList;
	}

	public Date getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(Date todayDate) {
		this.todayDate = todayDate;
	}

	public String getSearchforGsonDataChargeCode() {
		return SearchforGsonDataChargeCode;
	}

	public void setSearchforGsonDataChargeCode(String searchforGsonDataChargeCode) {
		SearchforGsonDataChargeCode = searchforGsonDataChargeCode;
	}

	public List getContractListNew() {
		return contractListNew;
	}

	public void setContractListNew(List contractListNew) {
		this.contractListNew = contractListNew;
	}

	public List getSearchforAutocompleteListChargeCode() {
		return SearchforAutocompleteListChargeCode;
	}

	public void setSearchforAutocompleteListChargeCode(
			List searchforAutocompleteListChargeCode) {
		SearchforAutocompleteListChargeCode = searchforAutocompleteListChargeCode;
	}

	public String getChargeCodeId() {
		return chargeCodeId;
	}

	public void setChargeCodeId(String chargeCodeId) {
		this.chargeCodeId = chargeCodeId;
	}

	public String getAutocompleteDivId() {
		return autocompleteDivId;
	}

	public void setAutocompleteDivId(String autocompleteDivId) {
		this.autocompleteDivId = autocompleteDivId;
	}

	public String getIdCondition() {
		return idCondition;
	}

	public void setIdCondition(String idCondition) {
		this.idCondition = idCondition;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getPageCondition() {
		return pageCondition;
	}

	public void setPageCondition(String pageCondition) {
		this.pageCondition = pageCondition;
	}

	public String getOriginCountry() {
		return originCountry;
	}

	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}

	public String getDestinationCountry() {
		return destinationCountry;
	}

	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}

	public String getServiceMode() {
		return serviceMode;
	}

	public void setServiceMode(String serviceMode) {
		this.serviceMode = serviceMode;
	}

	public List getContractListFromContract() {
		return contractListFromContract;
	}

	public void setContractListFromContract(List contractListFromContract) {
		this.contractListFromContract = contractListFromContract;
	}

	public List getContractListFromTemplate() {
		return contractListFromTemplate;
	}

	public void setContractListFromTemplate(List contractListFromTemplate) {
		this.contractListFromTemplate = contractListFromTemplate;
	}

	public String getFromContract() {
		return fromContract;
	}

	public void setFromContract(String fromContract) {
		this.fromContract = fromContract;
	}

	public String getToContract() {
		return toContract;
	}

	public void setToContract(String toContract) {
		this.toContract = toContract;
	}

	public DefaultAccountLine getCopyListFromDefaultAccountLIne() {
		return copyListFromDefaultAccountLIne;
	}

	public void setCopyListFromDefaultAccountLIne(
			DefaultAccountLine copyListFromDefaultAccountLIne) {
		this.copyListFromDefaultAccountLIne = copyListFromDefaultAccountLIne;
	}

	public String getCheckFlag() {
		return checkFlag;
	}

	public void setCheckFlag(String checkFlag) {
		this.checkFlag = checkFlag;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public List getChargessTemplate() {
		return chargessTemplate;
	}

	public void setChargessTemplate(List chargessTemplate) {
		this.chargessTemplate = chargessTemplate;
	}

	public String[] getCopyTemplateId() {
		return copyTemplateId;
	}

	public void setCopyTemplateId(String[] copyTemplateId) {
		this.copyTemplateId = copyTemplateId;
	}

	public String getShowException() {
		return showException;
	}

	public void setShowException(String showException) {
		this.showException = showException;
	}

	public List getMatchTemplateList() {
		return matchTemplateList;
	}

	public void setMatchTemplateList(List matchTemplateList) {
		this.matchTemplateList = matchTemplateList;
	}

	public List getMisMatchTemplateList() {
		return misMatchTemplateList;
	}

	public void setMisMatchTemplateList(List misMatchTemplateList) {
		this.misMatchTemplateList = misMatchTemplateList;
	}

	public String getCheckListAll() {
		return checkListAll;
	}

	public void setCheckListAll(String checkListAll) {
		this.checkListAll = checkListAll;
	}

	public String getEstExpVatDescr() {
		return estExpVatDescr;
	}

	public void setEstExpVatDescr(String estExpVatDescr) {
		this.estExpVatDescr = estExpVatDescr;
	}

	public String getEstVatDescr() {
		return estVatDescr;
	}

	public void setEstVatDescr(String estVatDescr) {
		this.estVatDescr = estVatDescr;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getCompanyDivision() {
		return companyDivision;
	}

	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}

	public String getPartnerCode() {
		return partnerCode;
	}

	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	public String getVatBillimngGroupCode() {
		return vatBillimngGroupCode;
	}

	public void setVatBillimngGroupCode(String vatBillimngGroupCode) {
		this.vatBillimngGroupCode = vatBillimngGroupCode;
	}
	public String getFlexcode() {
		return flexcode;
	}

	public void setFlexcode(String flexcode) {
		this.flexcode = flexcode;
	}

	public List getVatDescription() {
		return vatDescription;
	}

	public void setVatDescription(List vatDescription) {
		this.vatDescription = vatDescription;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Map<String, String> getFlex1RecCodeList() {
		return flex1RecCodeList;
	}

	public void setFlex1RecCodeList(Map<String, String> flex1RecCodeList) {
		this.flex1RecCodeList = flex1RecCodeList;
	}

	public Map<String, String> getFlex2PayCodeList() {
		return flex2PayCodeList;
	}

	public void setFlex2PayCodeList(Map<String, String> flex2PayCodeList) {
		this.flex2PayCodeList = flex2PayCodeList;
	}
}
