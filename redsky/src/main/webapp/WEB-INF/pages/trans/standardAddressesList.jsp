<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>   
<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript">
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
 </c:if>
</script>
<head>
<title><fmt:message key="standardaddresses.title" /></title>
<meta name="heading"
	content="<fmt:message key='standardaddresses.heading'/>" />


<script language="javascript" type="text/javascript">
function getState(target){
	var country = document.forms['standardaddresses'].elements['standardAddresses.countryCode'].value;
 	var countryCode = "";
	<c:forEach var="entry" items="${countryCod}">
	if(country=="${entry.value}"){
		countryCode="${entry.key}";
	}
	</c:forEach>
	var url="findStateList.html?ajax=1&decorator=simple&popup=true&bucket2=" + encodeURI(countryCode);
     http3.open("GET", url, true);
     http3.onreadystatechange = handleHttpResponse6;
     http3.send(null);
}

function handleHttpResponse6(){
		if (http3.readyState == 4){
                var results = http3.responseText
                results = results.trim();
                res = results.split("@");
                targetElement = document.forms['standardaddresses'].elements['standardAddresses.state'];
				targetElement.length = res.length;
 				for(i=0;i<res.length;i++){
					if(res[i] == ''){
						document.forms['standardaddresses'].elements['standardAddresses.state'].options[i].text = '';
						document.forms['standardaddresses'].elements['standardAddresses.state'].options[i].value = '';
					}else{
						stateVal = res[i].split("#");
						document.forms['standardaddresses'].elements['standardAddresses.state'].options[i].text = stateVal[1];
						document.forms['standardaddresses'].elements['standardAddresses.state'].options[i].value = stateVal[0];
					
						if (document.getElementById("state").options[i].value == '${standardAddresses.state}'){
						   document.getElementById("state").options[i].defaultSelected = true;
						}
					}
				}
				document.getElementById("state").value = '${standardAddresses.state}';
        }
}

var http3 = getHTTPObject();
function getHTTPObject(){
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
function clear_fields(){  		    	

	document.getElementById('job').value= "";
   	document.getElementById('addressline1').value ="";
    document.getElementById('countryCode').value = "";
    document.getElementById('state').value ="";
  document.getElementById('city').value ="";
}
function clear_fields1(){  		    	

   	document.getElementById('addressline1').value ="";
    document.getElementById('countryCode').value = "";
    document.getElementById('state').value ="";
  document.getElementById('city').value ="";
}

function autoPopulate_Country(targetElement) {		
		var country = targetElement.value;
		var enbState = '${enbState}';
		  var index = (enbState.indexOf(oriCon)> -1);
		  if(index != ''){
		  		document.forms['standardaddresses'].elements['standardAddresses.state'].disabled = false;
		  }else{
			  document.forms['standardaddresses'].elements['standardAddresses.state'].disabled = true;
		  }
}
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you want to remove this Address");
	var did = targetElement;
		if (agree){
			location.href="deleteStandardAddress.html?id="+did;
		}else{
			return false;
		}
}				
</script>
<style>
span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:2px;

margin-top:-18px;

padding:2px 0px;
text-align:right;
width:100%;
!width:98%;
}
</style>	
</head>
<c:set var="searchbuttons">   
	<s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px; !margin-bottom:10px;" align="top" method="" key="button.search"/>   
   <c:if test="${empty param.popup}">
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields();"/> 
	</c:if>
	<c:if test="${param.popup}">
    <input type="button" class="cssbutton" value="Clear" style="width:55px; height:25px;!margin-bottom:10px;" onclick="clear_fields1();"/> 
	</c:if>
</c:set>
<c:set var="residenceManagement" value="N" />
<configByCorp:fieldVisibility componentId="component.field.StandardAddress.residenceManagement">		
	<c:set var="residenceManagement" value="Y" />
</configByCorp:fieldVisibility> 
<s:form cssClass="form_magn" name="standardaddresses"  action='${empty param.popup?"searchStandardAddress.html":"searchStandardAddress.html?decorator=popup&popup=true"}' method="post">
<s:hidden  name="jobType"/>
<div id="otabs"><ul><li><a class="current"><span>Search</span></a></li></ul></div>
<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
   <div class="top" style="margin-top: 11px;!margin-top: -4px;"><span></span></div>
   <div class="center-content">
	<table class="table" style="width:100%;"  >
	<thead>
		<tr>
			<c:if test="${empty param.popup}">
			<th align="center"><fmt:message key="standardAddresses.job"/></th>
			</c:if>
			<c:if test="${residenceManagement=='N'}">
				<th align="center"><fmt:message key="standardAddresses.addressLine1"/></th>
			</c:if>
			<c:if test="${residenceManagement=='Y'}">
				<th align="center">Residence Name</th>
			</c:if>
			<th align="center"><fmt:message key="standardAddresses.city"/></th>
			<th align="center"><fmt:message key="standardAddresses.countryCode"/></th>
			<th align="center"><fmt:message key="standardAddresses.state"/></th>	
		
		</tr>
	</thead>	
	<tbody>
		<tr>
			<c:if test="${empty param.popup}">
			<td width="" align="left"><s:select cssClass="list-menu" id="job" name="standardAddresses.job" list="%{jobs}" cssStyle="width:140px"  onchange="changeStatus();" headerKey="" headerValue="" tabindex="23"/></td>
			</c:if>
			<td width="" align="left"><s:textfield name="standardAddresses.addressLine1" id="addressline1"  required="true" cssClass="input-text" size="20"/></td>
			<td width="" align="left"><s:textfield name="standardAddresses.city" id="city"  required="true" cssClass="input-text" size="20"/></td>
			
			<td ><s:select cssClass="list-menu" name="standardAddresses.countryCode" id="countryCode" list="%{country}" cssStyle="width:162px"  headerKey="" headerValue=""   onchange="getState(this);autoPopulate_Country(this);"/></td>
			<td width="" align="left"><s:select cssClass="list-menu" id="state" name="standardAddresses.state" list="%{states}" cssStyle="width:140px"  onchange="changeStatus();" headerKey="" headerValue="" tabindex="23"/></td>
			</tr>
		<tr >
		   	<c:if test="${empty param.popup}">
		   	<td align="left" colspan="4"></td>
		   	</c:if>
		   <c:if test="${param.popup}">
		   	<td align="left" colspan="3"></td>
		   	</c:if>
		  	<td width="130px" style="border-left: hidden;"><c:out value="${searchbuttons}" escapeXml="false"/></td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<c:if test="${param.popup}">  
	<s:hidden name="fld_code" value="${param.fld_code}" />
	<s:hidden name="fld_description" value="${param.fld_description}" />	
	<s:hidden name="fld_secondDescription" value="${param.fld_secondDescription}" />	
	<s:hidden name="fld_thirdDescription" value="${param.fld_thirdDescription}" />	
	<s:hidden name="fld_fourthDescription" value="${param.fld_fourthDescription}" />	
	<s:hidden name="fld_fifthDescription" value="${param.fld_fifthDescription}" />	
	<s:hidden name="fld_sixthDescription" value="${param.fld_sixthDescription}" />	
	<s:hidden name="fld_seventhDescription" value="${param.fld_seventhDescription}" />	
	<s:hidden name="fld_eigthDescription" value="${param.fld_eigthDescription}" />	
	<s:hidden name="fld_ninthDescription" value="${param.fld_ninthDescription}" />
	<s:hidden name="fld_tenthDescription" value="${param.fld_tenthDescription}" />
	<c:set var="fld_code" value="${param.fld_code}" />
	<c:set var="fld_description" value="${param.fld_description}" />
	<c:set var="fld_secondDescription" value="${param.fld_secondDescription}" />
	<c:set var="fld_thirdDescription" value="${param.fld_thirdDescription}" />
	<c:set var="fld_fourthDescription" value="${param.fld_fourthDescription}" />
	<c:set var="fld_fifthDescription" value="${param.fld_fifthDescription}" />
	<c:set var="fld_sixthDescription" value="${param.fld_sixthDescription}" />
	<c:set var="fld_seventhDescription" value="${param.fld_seventhDescription}" />
	<c:set var="fld_eigthDescription" value="${param.fld_eigthDescription}" />
	<c:set var="fld_ninthDescription" value="${param.fld_ninthDescription}" />
	<c:set var="fld_tenthDescription" value="${param.fld_tenthDescription}" />
</c:if>
<display:table name="standardaddresses" class="table" requestURI="" id="standardaddresses" defaultsort="1" decorator='${empty param.popup?null:"com.trilasoft.app.webapp.tags.ListLinkGenerator"}'  pagesize="10"  >   

<c:if test="${empty param.popup}">
	<c:if test="${residenceManagement=='N'}">
		<display:column property="addressLine1" title="AddressLine1" url="/addStandardaddress.html" paramId="id" paramProperty="id" sortable="true"/>
	</c:if>
	<c:if test="${residenceManagement=='Y'}">
		<display:column property="addressLine1" title="Residence Name" url="/addStandardaddress.html" paramId="id" paramProperty="id" sortable="true"/>
	</c:if>
</c:if> 
<c:if test="${param.popup}">  
    	<display:column property="listLinkParams" sortable="true" title="AddressLine1" />   
    </c:if>
<display:column property="job" titleKey="standardAddresses.job" sortable="true"/>
<display:column property="city" titleKey="standardAddresses.city" sortable="true"/>
<display:column property="countryCode" titleKey="standardAddresses.countryCode" sortable="true"/>
<display:column property="state" titleKey="standardAddresses.state" sortable="true"/>
<c:if test="${param.popup}">
<display:column url="/addStandardaddress.html?decorator=popup&popup=true&fld_tenthDescription=${fld_tenthDescription}&fld_ninthDescription=${fld_ninthDescription}&fld_eigthDescription=${fld_eigthDescription}&fld_seventhDescription=${fld_seventhDescription}&fld_sixthDescription=${fld_sixthDescription}&fld_fifthDescription=${fld_fifthDescription}&fld_fourthDescription=${fld_fourthDescription}&fld_thirdDescription=${fld_thirdDescription}&fld_secondDescription=${fld_secondDescription}&fld_description=${fld_description}&fld_code=${fld_code}"  paramId="id" paramProperty="id" title="View Detail">View Detail</display:column>
</c:if>
<c:if test="${empty param.popup}">
<display:column title="Remove" style="width:50px">
			<a><img title="Remove" align="middle" onclick="confirmSubmit(${standardaddresses.id});" style="margin: 0px 0px 0px 8px;" src="images/recycle.gif"/></a>
</display:column>
</c:if>
</display:table>				
<c:if test="${empty param.popup}">
 <input type="button" class="cssbuttonA"  onclick="location.href='<c:url value="/addStandardaddress.html"/>'" value="Add"/>
</c:if>		
</s:form>
<script language="javascript" type="text/javascript">
try{
var country =document.getElementById('countryCode').value;
}
catch(e){}
try{
var enbState = '${enbState}';
var oriCon=document.forms['standardaddresses'].elements['standardAddresses.countryCode'].value;
if(oriCon=="")	 {
		document.getElementById('state').disabled = true;
		document.getElementById('state').value ="";
}else{
 		if(enbState.indexOf(oriCon)> -1){
			document.getElementById('state').disabled = false; 
			document.getElementById('state').value='${standardAddresses.state}';
		}else{
			document.getElementById('state').disabled = true;
			document.getElementById('state').value ="";
		} 
	}		
}catch(e){}		
try{
if(document.getElementById('country').value==""){
document.getElementById('state').disabled = true;
 }
 }
 catch(e){}
 if(window.addEventListener)
		window.addEventListener('load',function(){
			getState(document.forms['standardaddresses'].elements['standardAddresses.countryCode']);
			},false);
</script>