DELIMITER $$
CREATE TRIGGER trigger_delete_exchangerate BEFORE DELETE ON exchangerate
FOR EACH ROW BEGIN
   DECLARE fieldNameList LONGTEXT;
   DECLARE oldValueList LONGTEXT;
   DECLARE newValueList LONGTEXT;
   DECLARE phistoryfx LONGTEXT;

   SET fieldNameList = " ";
   SET oldValueList = " ";
   SET newValueList = " ";
   SET phistoryfx = " ";


 IF (OLD.currency <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.currency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.currency,'~') into oldValueList;
   END IF;
 IF (OLD.corpID <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.corpID~') into fieldNameList;
       select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
   END IF;

 IF (OLD.baseCurrency <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.baseCurrency~') into fieldNameList;
       select CONCAT(oldValueList,OLD.baseCurrency,'~') into oldValueList;
   END IF;
 IF (OLD.manualUpdate <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.manualUpdate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.manualUpdate,'~') into oldValueList;
   END IF;
 IF (OLD.valueDate <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.valueDate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
   END IF;
 IF (OLD.baseCurrencyRate <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.baseCurrencyRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.baseCurrencyRate,'~') into oldValueList;
   END IF;
 IF (OLD.currencyBaseRate <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.currencyBaseRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.currencyBaseRate,'~') into oldValueList;
   END IF;
 IF (OLD.officialRate <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.officialRate~') into fieldNameList;
       select CONCAT(oldValueList,OLD.officialRate,'~') into oldValueList;
   END IF;
 IF (OLD.marginApplied <> '') THEN
       select CONCAT(fieldNameList,'exchangerate.marginApplied~') into fieldNameList;
       select CONCAT(oldValueList,OLD.marginApplied,'~') into oldValueList;
   END IF;

   select if(historyfx is true,'T','F') into phistoryfx from company where corpid=OLD.corpid;

  if(phistoryfx='T') then
insert into history_exchangerate(currency, corpID, createdBy, createdOn, updatedBy, updatedOn, valueDate, baseCurrency, baseCurrencyRate, currencyBaseRate, officialRate, marginApplied, manualUpdate)
values(OLD.currency,OLD.corpid,OLD.createdby,OLD.createdon,OLD.updatedby,OLD.updatedon,OLD.valuedate,OLD.basecurrency,OLD.basecurrencyrate,OLD.currencybaserate,OLD.officialrate,OLD.marginapplied,OLD.manualupdate);

    end if;
END $$
delimiter ;
