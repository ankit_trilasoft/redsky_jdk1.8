package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

@Entity
@Table(name="reffreightrates")
@FilterDef(name="corpID", parameters={ @ParamDef( name="forCorpID", type="string" ) } )
@Filter(name="corpID", condition="corpID = :forCorpID")
public class RefFreightRates extends BaseObject {
	private Long id;
	private String corpID;
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private String mode;
	private String carrier;
	private String originRegion;
	private String originCity;
	private String originCountry;
	private String originPortCity;
	private String originPortCountry;
	private String originRouting;
	private String destinationRegion;
	private String destinationCity;
	private String destinationCountry;
	private String destinationPortCity;
	private String destinationPortCountry;
	private String equipmentType;
	private String transitDays;
	private String numberofSailings;
	private String sailingFrequency;
	private String transshipment;
	private Date lineItemEffDate;
	private Date lineItemExpDate;
	private BigDecimal totalPrice;
	private String laneComments;
	private BigDecimal destArbitrariesChargeValue;
	private String destArbitrariesChargeDetails;
	private BigDecimal orgArbitrariesChargeValue;
	private String orgArbitrariesChargeDetails;
	private BigDecimal BUCValue;
	private String BUCDetails;
	private BigDecimal destTHCValue;
	private String destTHCDetails;
	private String serviceContractNo; 
	private String baseCurrency;
	private String contractType;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("corpID",
				corpID).append("createdBy", createdBy).append("updatedBy",
				updatedBy).append("createdOn", createdOn).append("updatedOn",
				updatedOn).append("mode", mode).append("carrier", carrier)
				.append("originRegion", originRegion).append("originCity",
						originCity).append("originCountry", originCountry)
				.append("originPortCity", originPortCity).append(
						"originPortCountry", originPortCountry).append(
						"originRouting", originRouting).append(
						"destinationRegion", destinationRegion).append(
						"destinationCity", destinationCity).append(
						"destinationCountry", destinationCountry).append(
						"destinationPortCity", destinationPortCity).append(
						"destinationPortCountry", destinationPortCountry)
				.append("equipmentType", equipmentType).append("transitDays",
						transitDays).append("numberofSailings",
						numberofSailings).append("sailingFrequency",
						sailingFrequency)
				.append("transshipment", transshipment).append(
						"lineItemEffDate", lineItemEffDate).append(
						"lineItemExpDate", lineItemExpDate).append(
						"totalPrice", totalPrice).append("laneComments",
						laneComments).append("destArbitrariesChargeValue",
						destArbitrariesChargeValue).append(
						"destArbitrariesChargeDetails",
						destArbitrariesChargeDetails).append(
						"orgArbitrariesChargeValue", orgArbitrariesChargeValue)
				.append("orgArbitrariesChargeDetails",
						orgArbitrariesChargeDetails).append("BUCValue",
						BUCValue).append("BUCDetails", BUCDetails).append(
						"destTHCValue", destTHCValue).append("destTHCDetails",
						destTHCDetails).append("serviceContractNo",
						serviceContractNo).append("baseCurrency", baseCurrency)
				.append("contractType", contractType).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof RefFreightRates))
			return false;
		RefFreightRates castOther = (RefFreightRates) other;
		return new EqualsBuilder().append(id, castOther.id).append(corpID,
				castOther.corpID).append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy).append(createdOn,
						castOther.createdOn).append(updatedOn,
						castOther.updatedOn).append(mode, castOther.mode)
				.append(carrier, castOther.carrier).append(originRegion,
						castOther.originRegion).append(originCity,
						castOther.originCity).append(originCountry,
						castOther.originCountry).append(originPortCity,
						castOther.originPortCity).append(originPortCountry,
						castOther.originPortCountry).append(originRouting,
						castOther.originRouting).append(destinationRegion,
						castOther.destinationRegion).append(destinationCity,
						castOther.destinationCity).append(destinationCountry,
						castOther.destinationCountry).append(
						destinationPortCity, castOther.destinationPortCity)
				.append(destinationPortCountry,
						castOther.destinationPortCountry).append(equipmentType,
						castOther.equipmentType).append(transitDays,
						castOther.transitDays).append(numberofSailings,
						castOther.numberofSailings).append(sailingFrequency,
						castOther.sailingFrequency).append(transshipment,
						castOther.transshipment).append(lineItemEffDate,
						castOther.lineItemEffDate).append(lineItemExpDate,
						castOther.lineItemExpDate).append(totalPrice,
						castOther.totalPrice).append(laneComments,
						castOther.laneComments).append(
						destArbitrariesChargeValue,
						castOther.destArbitrariesChargeValue).append(
						destArbitrariesChargeDetails,
						castOther.destArbitrariesChargeDetails).append(
						orgArbitrariesChargeValue,
						castOther.orgArbitrariesChargeValue).append(
						orgArbitrariesChargeDetails,
						castOther.orgArbitrariesChargeDetails).append(BUCValue,
						castOther.BUCValue).append(BUCDetails,
						castOther.BUCDetails).append(destTHCValue,
						castOther.destTHCValue).append(destTHCDetails,
						castOther.destTHCDetails).append(serviceContractNo,
						castOther.serviceContractNo).append(baseCurrency,
						castOther.baseCurrency).append(contractType,
						castOther.contractType).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpID)
				.append(createdBy).append(updatedBy).append(createdOn).append(
						updatedOn).append(mode).append(carrier).append(
						originRegion).append(originCity).append(originCountry)
				.append(originPortCity).append(originPortCountry).append(
						originRouting).append(destinationRegion).append(
						destinationCity).append(destinationCountry).append(
						destinationPortCity).append(destinationPortCountry)
				.append(equipmentType).append(transitDays).append(
						numberofSailings).append(sailingFrequency).append(
						transshipment).append(lineItemEffDate).append(
						lineItemExpDate).append(totalPrice)
				.append(laneComments).append(destArbitrariesChargeValue)
				.append(destArbitrariesChargeDetails).append(
						orgArbitrariesChargeValue).append(
						orgArbitrariesChargeDetails).append(BUCValue).append(
						BUCDetails).append(destTHCValue).append(destTHCDetails)
				.append(serviceContractNo).append(baseCurrency).append(
						contractType).toHashCode();
	}
	@Column(length=12)
	public String getBUCDetails() {
		return BUCDetails;
	}
	public void setBUCDetails(String details) {
		BUCDetails = details;
	}
	
	@Column(length=12)
	public BigDecimal getBUCValue() {
		return BUCValue;
	}
	public void setBUCValue(BigDecimal value) {
		BUCValue = value;
	}
	
	@Column(length=100)
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	
	@Column(length=10)
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	
	@Column(length=25)
	public String getDestArbitrariesChargeDetails() {
		return destArbitrariesChargeDetails;
	}
	public void setDestArbitrariesChargeDetails(String destArbitrariesChargeDetails) {
		this.destArbitrariesChargeDetails = destArbitrariesChargeDetails;
	}
	
	@Column(length=12)
	public BigDecimal getDestArbitrariesChargeValue() {
		return destArbitrariesChargeValue;
	}
	public void setDestArbitrariesChargeValue(BigDecimal destArbitrariesChargeValue) {
		this.destArbitrariesChargeValue = destArbitrariesChargeValue;
	}
	
	@Column(length=100)
	public String getDestinationCity() {
		return destinationCity;
	}
	public void setDestinationCity(String destinationCity) {
		this.destinationCity = destinationCity;
	}
	
	@Column(length=25)
	public String getDestinationCountry() {
		return destinationCountry;
	}
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	
	@Column(length=100)
	public String getDestinationPortCity() {
		return destinationPortCity;
	}
	public void setDestinationPortCity(String destinationPortCity) {
		this.destinationPortCity = destinationPortCity;
	}
	
	@Column(length=25)
	public String getDestinationPortCountry() {
		return destinationPortCountry;
	}
	public void setDestinationPortCountry(String destinationPortCountry) {
		this.destinationPortCountry = destinationPortCountry;
	}
	
	@Column(length=25)
	public String getDestinationRegion() {
		return destinationRegion;
	}
	public void setDestinationRegion(String destinationRegion) {
		this.destinationRegion = destinationRegion;
	}
	
	@Column(length=25)
	public String getDestTHCDetails() {
		return destTHCDetails;
	}
	public void setDestTHCDetails(String destTHCDetails) {
		this.destTHCDetails = destTHCDetails;
	}
	
	@Column(length=12)
	public BigDecimal getDestTHCValue() {
		return destTHCValue;
	}
	public void setDestTHCValue(BigDecimal destTHCValue) {
		this.destTHCValue = destTHCValue;
	}
	
	@Column(length=25)
	public String getEquipmentType() {
		return equipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		this.equipmentType = equipmentType;
	}
	
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(length=5000)
	public String getLaneComments() {
		return laneComments;
	}
	public void setLaneComments(String laneComments) {
		this.laneComments = laneComments;
	}
	
	
	
	@Column(length=3)
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	@Column(length=25)
	public String getNumberofSailings() {
		return numberofSailings;
	}
	public void setNumberofSailings(String numberofSailings) {
		this.numberofSailings = numberofSailings;
	}
	
	@Column(length=25)
	public String getOrgArbitrariesChargeDetails() {
		return orgArbitrariesChargeDetails;
	}
	public void setOrgArbitrariesChargeDetails(String orgArbitrariesChargeDetails) {
		this.orgArbitrariesChargeDetails = orgArbitrariesChargeDetails;
	}
	
	@Column(length=12)
	public BigDecimal getOrgArbitrariesChargeValue() {
		return orgArbitrariesChargeValue;
	}
	public void setOrgArbitrariesChargeValue(BigDecimal orgArbitrariesChargeValue) {
		this.orgArbitrariesChargeValue = orgArbitrariesChargeValue;
	}
	
	@Column(length=100)
	public String getOriginCity() {
		return originCity;
	}
	public void setOriginCity(String originCity) {
		this.originCity = originCity;
	}
	
	@Column(length=25)
	public String getOriginCountry() {
		return originCountry;
	}
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	
	@Column(length=100)
	public String getOriginPortCity() {
		return originPortCity;
	}
	public void setOriginPortCity(String originPortCity) {
		this.originPortCity = originPortCity;
	}
	
	@Column(length=25)
	public String getOriginPortCountry() {
		return originPortCountry;
	}
	public void setOriginPortCountry(String originPortCountry) {
		this.originPortCountry = originPortCountry;
	}
	
	@Column(length=25)
	public String getOriginRegion() {
		return originRegion;
	}
	public void setOriginRegion(String originRegion) {
		this.originRegion = originRegion;
	}
	
	@Column(length=25)
	public String getOriginRouting() {
		return originRouting;
	}
	public void setOriginRouting(String originRouting) {
		this.originRouting = originRouting;
	}
	
	@Column(length=25)
	public String getSailingFrequency() {
		return sailingFrequency;
	}
	public void setSailingFrequency(String sailingFrequency) {
		this.sailingFrequency = sailingFrequency;
	}
	
	@Column(length=12)
	public BigDecimal getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	
	@Column(length=25)
	public String getTransshipment() {
		return transshipment;
	}
	public void setTransshipment(String transshipment) {
		this.transshipment = transshipment;
	}
	
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
	@Column(length=100)
	public String getServiceContractNo() {
		return serviceContractNo;
	}
	public void setServiceContractNo(String serviceContractNo) {
		this.serviceContractNo = serviceContractNo;
	}
	
	@Column(length=10)
	public String getBaseCurrency() {
		return baseCurrency;
	}
	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	
	@Column(length=25)
	public String getContractType() {
		return contractType;
	}
	public void setContractType(String contractType) {
		this.contractType = contractType;
	}
	@Column
	public Date getLineItemEffDate() {
		return lineItemEffDate;
	}
	public void setLineItemEffDate(Date lineItemEffDate) {
		this.lineItemEffDate = lineItemEffDate;
	}
	@Column
	public Date getLineItemExpDate() {
		return lineItemExpDate;
	}
	public void setLineItemExpDate(Date lineItemExpDate) {
		this.lineItemExpDate = lineItemExpDate;
	} 
	@Column(length=25)
	public String getTransitDays() {
		return transitDays;
	}
	public void setTransitDays(String transitDays) {
		this.transitDays = transitDays;
	}
}
