<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>
<title>Paychex Extract</title>
<meta name="heading" content="Paychex Extract" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}


</style>
<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
<!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function fieldValidate(){
   var date1 = document.forms['paychexExtractForm'].elements['beginDate'].value;	 
   var date2 = document.forms['paychexExtractForm'].elements['endDate'].value; 
   var mySplitResult = date1.split("-");
   var day = mySplitResult[0];
   var month = mySplitResult[1];
   var year = mySplitResult[2];
  if(month == 'Jan')
   {
       month = "01";
   }
   else if(month == 'Feb')
   {
       month = "02";
   }
   else if(month == 'Mar')
   {
       month = "03"
   }
   else if(month == 'Apr')
   {
       month = "04"
   }
   else if(month == 'May')
   {
       month = "05"
   }
   else if(month == 'Jun')
   {
       month = "06"
   }
   else if(month == 'Jul')
   {
       month = "07"
   }
   else if(month == 'Aug')
   {
       month = "08"
   }
   else if(month == 'Sep')
   {
       month = "09"
   }
   else if(month == 'Oct')
   {
       month = "10"
   }
   else if(month == 'Nov')
   {
       month = "11"
   }
   else if(month == 'Dec')
   {
       month = "12";
   }
   var finalDate = month+"-"+day+"-"+year;
   var mySplitResult2 = date2.split("-");
   var day2 = mySplitResult2[0];
   var month2 = mySplitResult2[1];
   var year2 = mySplitResult2[2];
   if(month2 == 'Jan')
   {
       month2 = "01";
   }
   else if(month2 == 'Feb')
   {
       month2 = "02";
   }
   else if(month2 == 'Mar')
   {
       month2 = "03"
   }
   else if(month2 == 'Apr')
   {
       month2 = "04"
   }
   else if(month2 == 'May')
   {
       month2 = "05"
   }
   else if(month2 == 'Jun')
   {
       month2 = "06"
   }
   else if(month2 == 'Jul')
   {
       month2 = "07"
   }
   else if(month2 == 'Aug')
   {
       month2 = "08"
   }
   else if(month2 == 'Sep')
   {
       month2 = "09"
   }
   else if(month2 == 'Oct')
   {
       month2 = "10"
   }
   else if(month2 == 'Nov')
   {
       month2 = "11"
   }
   else if(month2 == 'Dec')
   {
       month2 = "12";
   }
  var finalDate2 = month2+"-"+day2+"-"+year2;
  date1 = finalDate.split("-");
  date2 = finalDate2.split("-");
  var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
  var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
  var daysApart = Math.round((eDate-sDate)/86400000);
  if(daysApart<0)
  {
    alert("End Date should be greater than  or equal to Begin Date");
    document.forms['paychexExtractForm'].elements['endDate'].value='';
    return false;
  }
	
	       if(document.forms['paychexExtractForm'].elements['beginDate'].value=='')
	       {
	       	alert("Please enter the begin date"); 
	       	return false;
	       }
	       if(document.forms['paychexExtractForm'].elements['endDate'].value==''){
	       alert("Please enter the end date "); 
	       	return false;
	       	} 
	        if(document.forms['paychexExtractForm'].elements['hub'].value==''){
	        alert("Please enter the Operations Hub"); 
	       	return false;
	       	} 
	       else
	       {
	       	document.forms['paychexExtractForm'].submit();
	         return true;
	       }
       
       }  
</script>
</head>

<s:form name="paychexExtractForm" id="paychexExtractForm" action="paychexExtract" method="post" validate="true">
	<%
String ua = request.getHeader( "User-Agent" );
boolean isFirefox = ( ua != null && ua.indexOf( "Firefox/" ) != -1 );
boolean isMSIE = ( ua != null && ua.indexOf( "MSIE" ) != -1 );
response.setHeader( "Vary", "User-Agent" );
%>

<% if( isFirefox ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>

<% if( isMSIE ){ %>
	<c:set var="FormDateValue" value="dd-NNN-yy"/>
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<% } %>
<div id="Layer1" style="width:85%;">
<div id="otabs">
			  <ul>
			    <li><a class="current"><span>Paychex Extract</span></a></li>
			  </ul>
			</div>
			<div class="spnblk">&nbsp;</div>
<div id="content" align="center" >
<div id="liquid-round">
<div class="top" style="margin-top: 10px;!margin-top:-4px"><span></span></div>
   <div class="center-content">
<table  border="0" class="" style="width:90%;" cellspacing="0" cellpadding="2">	
	<tr><td height="5px"></td></tr>	  		
  			<tr> 
  			<td class="listwhitetext" align="right">Begin&nbsp;Date<font color="red" size="2">*</font></td>
					<c:if test="${not empty beginDate}">
						<s:text id="beginDate" name="${FormDateValue}"><s:param name="value" value="beginDate" /></s:text>
						<td width="110px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" value="%{beginDate}" size="8" maxlength="11" readonly="true" /><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty beginDate}">
						<td width="110px"><s:textfield cssClass="input-text" id="beginDate" name="beginDate" required="true" size="8" maxlength="11" readonly="true" /><img id="beginDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td width="10px"></td>
				<td class="listwhitetext" align="right">End&nbsp;Date<font color="red" size="2">*</font></td>
		 			<c:if test="${not empty endDate}">
						<s:text id="endDate" name="${FormDateValue}"> <s:param name="value" value="endDate" /></s:text>
						<td width="110px"><s:textfield cssClass="input-text" id="endDate" name="endDate" value="%{endDate}" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<c:if test="${empty endDate}" >
						<td width="110px"><s:textfield cssClass="input-text" id="endDate" name="endDate" required="true" size="8" maxlength="11" readonly="true" /><img id="endDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
					</c:if>
					<td class="listwhitetext" align="right">Payroll&nbsp;Group<font color="red" size="2">*</font></td>
                	<td align="left"><s:select cssClass="list-menu" id="hub" name="hub" list="%{payrollGroup}" cssStyle="width:100px" /></td>
			 		  			 	
			</tr> 
			<tr><td height="20px"></td></tr>
	  		<tr>	
	  			<td width="80px"></td>		
				<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px" align="top"  method="paychexExtract" value="Extract" onclick=" return fieldValidate();"/></td>
	    	</tr>
	    	
	</table>
	</div>
	<div class="bottom-header"><span></span></div> 
</div>
</div>
</div>  

<s:text id="customerFileMoveDateFormattedValue" name="FormDateValue"><s:param name="value" value="refMaster.stopDate"/></s:text>
<s:hidden name="refMaster.stopDate" />

</s:form>

<script type="text/javascript"> 
    Form.focusFirstElement($("refMasterForm")); 
</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>
