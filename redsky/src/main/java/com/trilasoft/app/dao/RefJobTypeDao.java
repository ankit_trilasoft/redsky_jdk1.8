package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;
import com.trilasoft.app.model.RefJobType;

public interface RefJobTypeDao extends GenericDao<RefJobType, Long>  {
	
	public List isExisted(String job,String companydivision,String sessionCorpID);
	public List searchResult(String job,String companydivision,String surveytool, String sessionCorpID);
}
