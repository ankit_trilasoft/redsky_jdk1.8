package com.trilasoft.app.model;
import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.io.File;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="cportalresourcemgmt")
public class CPortalResourceMgmt extends BaseObject{
	private Long id;   
	private String corpId;   
	private String documentType;
	private String documentName;
	private String documentLocation;
    private File file;
    private String originCountry;
    private String destinationCountry;
    private String billToCode;
    private String billToName;
    private String createdBy;
    private String updatedBy;
    private Date createdOn;
    private Date updatedOn;
    private String fileContentType;
    private String fileFileName;
    private String fileType;
    private String billToExcludes;   
    private String language;
    private String infoPackage;
    private String fileSize;
	private Long docSequenceNumber ;
	private String jobType;
	private String childBillToCode;
	private String mode;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("corpId", corpId).append("documentType", documentType)
				.append("documentName", documentName)
				.append("documentLocation", documentLocation)
				.append("file", file).append("originCountry", originCountry)
				.append("destinationCountry", destinationCountry)
				.append("billToCode", billToCode)
				.append("billToName", billToName)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("fileContentType", fileContentType)
				.append("fileFileName", fileFileName)
				.append("fileType", fileType)
				.append("billToExcludes", billToExcludes)
				.append("language", language)
				.append("infoPackage", infoPackage)
				.append("fileSize", fileSize)
				.append("docSequenceNumber", docSequenceNumber)
				.append("jobType", jobType)
				.append("childBillToCode", childBillToCode)
				.append("mode", mode)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CPortalResourceMgmt))
			return false;
		CPortalResourceMgmt castOther = (CPortalResourceMgmt) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(documentType, castOther.documentType)
				.append(documentName, castOther.documentName)
				.append(documentLocation, castOther.documentLocation)
				.append(file, castOther.file)
				.append(originCountry, castOther.originCountry)
				.append(destinationCountry, castOther.destinationCountry)
				.append(billToCode, castOther.billToCode)
				.append(billToName, castOther.billToName)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(fileContentType, castOther.fileContentType)
				.append(fileFileName, castOther.fileFileName)
				.append(fileType, castOther.fileType)
				.append(billToExcludes, castOther.billToExcludes)
				.append(language, castOther.language)
				.append(infoPackage, castOther.infoPackage)
				.append(fileSize, castOther.fileSize)
				.append(docSequenceNumber, castOther.docSequenceNumber)
				.append(jobType, castOther.jobType)
				.append(childBillToCode, castOther.childBillToCode)
				.append(mode, castOther.mode)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId)
				.append(documentType).append(documentName)
				.append(documentLocation).append(file).append(originCountry)
				.append(destinationCountry).append(billToCode)
				.append(billToName).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(fileContentType)
				.append(fileFileName).append(fileType).append(billToExcludes)
				.append(language).append(infoPackage).append(fileSize)
				.append(docSequenceNumber).append(jobType)
				.append(childBillToCode).append(childBillToCode).append(mode)
				.toHashCode();
	}
	/**
	 * @return the updatedOn
	 */
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	/**
	 * @return the billToCode
	 */
	@Column(length=15)
	public String getBillToCode() {
		return billToCode;
	}
	/**
	 * @param billToCode the billToCode to set
	 */
	public void setBillToCode(String billToCode) {
		this.billToCode = billToCode;
	}
	/**
	 * @return the corpId
	 */
	@Column(length=15)
	public String getCorpId() {
		return corpId;
	}
	/**
	 * @param corpId the corpId to set
	 */
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	/**
	 * @return the createdBy
	 */
	@Column(length=82)
	public String getCreatedBy() {
		return createdBy;
	}
	/**
	 * @param createdBy the createdBy to set
	 */
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	/**
	 * @return the createdOn
	 */
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	/**
	 * @return the destinationCountry
	 */
	@Column(length=45)
	public String getDestinationCountry() {
		return destinationCountry;
	}
	/**
	 * @param destinationCountry the destinationCountry to set
	 */
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	/**
	 * @return the documentLocation
	 */
	@Column( length=254 )
	public String getDocumentLocation() {
		return documentLocation;
	}
	/**
	 * @param documentLocation the documentLocation to set
	 */
	public void setDocumentLocation(String documentLocation) {
		this.documentLocation = documentLocation;
	}
	/**
	 * @return the documentName
	 */
	@Column( length=375 )
	public String getDocumentName() {
		return documentName;
	}
	/**
	 * @param documentName the documentName to set
	 */
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	/**
	 * @return the documentType
	 */
	@Column( length=25 )
	public String getDocumentType() {
		return documentType;
	}
	/**
	 * @param documentType the documentType to set
	 */
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)	
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the originCountry
	 */
	@Column(length=45)
	public String getOriginCountry() {
		return originCountry;
	}
	/**
	 * @param originCountry the originCountry to set
	 */
	public void setOriginCountry(String originCountry) {
		this.originCountry = originCountry;
	}
	/**
	 * @return the updatedBy
	 */
	@Column(length=82)
	public String getUpdatedBy() {
		return updatedBy;
	}
	/**
	 * @param updatedBy the updatedBy to set
	 */
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
		
	/**
	 * @return the file
	 */
	@Column
	public File getFile() {
		return file;
	}
	/**
	 * @param file the file to set
	 */
	public void setFile(File file) {
		this.file = file;
	}
	/**
	 * @return the billToName
	 */
	@Column(length=255)
	public String getBillToName() {
		return billToName;
	}
	/**
	 * @param billToName the billToName to set
	 */
	public void setBillToName(String billToName) {
		this.billToName = billToName;
	}
	/**
	 * @return the fileContentType
	 */
	@Column(length=45)
	public String getFileContentType() {
		return fileContentType;
	}
	/**
	 * @param fileContentType the fileContentType to set
	 */
	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}
	/**
	 * @return the fileType
	 */
	@Column( length=25 )
	public String getFileType() {
		return fileType;
	}
	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	/**
	 * @return the fileFileName
	 */
	@Column(length=375)
	public String getFileFileName() {
		return fileFileName;
	}
	/**
	 * @param fileFileName the fileFileName to set
	 */
	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	/**
	 * @return the billToExcludes
	 */
	@Column(length=255)
	public String getBillToExcludes() {
		return billToExcludes;
	}
	/**
	 * @param billToExcludes the billToExcludes to set
	 */
	public void setBillToExcludes(String billToExcludes) {
		this.billToExcludes = billToExcludes;
	}
	@Column(length=20)
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	@Column(length=45)
	public String getInfoPackage() {
		return infoPackage;
	}
	public void setInfoPackage(String infoPackage) {
		this.infoPackage = infoPackage;
	}
	@Column
	public String getFileSize() {
		return fileSize;
	}
	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}
	public Long getDocSequenceNumber() {
		return docSequenceNumber;
	}
	public void setDocSequenceNumber(Long docSequenceNumber) {
		this.docSequenceNumber = docSequenceNumber;
	}
	@Column
	public String getJobType() {
		return jobType;
	}
	public void setJobType(String jobType) {
		this.jobType = jobType;
	}
	@Column
	public String getChildBillToCode() {
		return childBillToCode;
	}
	public void setChildBillToCode(String childBillToCode) {
		this.childBillToCode = childBillToCode;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
}