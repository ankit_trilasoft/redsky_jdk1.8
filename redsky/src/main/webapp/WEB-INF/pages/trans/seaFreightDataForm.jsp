<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<head>

<link href='<s:url value="/css/main.css"/>' rel="stylesheet"
	type="text/css" />
<s:head theme="ajax" />
<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style> 

<!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
    
<!-- Modification closed here -->

<script language="JavaScript">
	function checkCode(){ 
	var countryName=document.forms['seaFreightDataForm'].elements['refFreightRates.destinationPortCountry'].value;
	
	}

	
function trim(str) {
        return str.replace(/^\s+|\s+$/g,"");
    }
    function confirmValidate(){
		var serviceContractNo=document.forms['seaFreightDataForm'].elements['refFreightRates.serviceContractNo'].value;
		var serviceContractNo1;
		serviceContractNo1 = trim(serviceContractNo); 
		serviceContractNo1=serviceContractNo1.length;
		var serviceContractNoValue=document.forms['seaFreightDataForm'].elements['refFreightRates.serviceContractNo'].value;
		var mystr;
		mystr = trim(serviceContractNoValue); 
		document.forms['seaFreightDataForm'].elements['refFreightRates.serviceContractNo'].value=mystr;
		if(mystr=='')
		{
		alert('Please fill the mandatory field');
		return false;
		}
		if(mystr1==0){
		alert('Service Contract Number cannot be blank');
		return false;
		}
		
		
		else{
		return true;
}
}

	function onlyFloatNumsAllowed(evt){
		var keyCode = evt.which ? evt.which : evt.keyCode;
	  	return (keyCode >= 48 && keyCode <= 57) || (keyCode >= 96 && keyCode <= 105) || (keyCode==null) || (keyCode==0) || (keyCode==8) ||(keyCode== 9) || (keyCode==13) || (keyCode==27) || (keyCode==46) || (keyCode==37) || (keyCode==39) || ( keyCode==190)|| ( keyCode==110); 
	}
	function chkSelect(a){
	if(a == '2') {
	document.forms['seaFreightDataForm'].elements['refFreightRates.destArbitrariesChargeDetails'].select(); 	
	}
	if (checkFloatWithArth('seaFreightDataForm','refFreightRates.destTHCValue','Invalid data in Dest. THC Value') == false)
            {
              document.forms['seaFreightDataForm'].elements['refFreightRates.destTHCValue'].value='0'; 
              return false
            }
   if (checkFloatWithArth('seaFreightDataForm','refFreightRates.totalPrice','Invalid data in Total Price') == false)
            {
              document.forms['seaFreightDataForm'].elements['refFreightRates.totalPrice'].value='0'; 
              return false
            }         
     if (checkFloatWithArth('seaFreightDataForm','refFreightRates.destArbitrariesChargeValue','Invalid data in Arbitraries Charge (Destination) Value') == false)
            {
              document.forms['seaFreightDataForm'].elements['refFreightRates.destArbitrariesChargeValue'].value='0'; 
              return false
            } 
     if (checkFloatWithArth('seaFreightDataForm','refFreightRates.orgArbitrariesChargeValue','Invalid data in Arbitraries Charge (Origin) Value') == false)
            {
              document.forms['seaFreightDataForm'].elements['refFreightRates.orgArbitrariesChargeValue'].value='0'; 
              return false
            } 
      if (checkFloatWithArth('seaFreightDataForm','refFreightRates.BUCValue','Invalid data in BUC Value') == false)
            {
              document.forms['seaFreightDataForm'].elements['refFreightRates.BUCValue'].value='0'; 
              return false
            }       
                    
	
	}
function onlyFloat1(targetElement)
{   var i;
	var test;
	var s = targetElement;
	var count = 0;
	for (i = 0; i < s.length; i++)
    {   
        var c = s.charAt(i);
        
        if(c == '.')
        {
        	count = count+1
        }
        if (((c < '0') || (c > '9')) && (c != '.') || (count>'1')) 
        {
        	test='error';
        }
      }
	    return test;
}

function Validate()
{
	var orgAC = document.forms['seaFreightDataForm'].elements['refFreightRates.orgArbitrariesChargeValue'].value;
	var destAC = document.forms['seaFreightDataForm'].elements['refFreightRates.destArbitrariesChargeValue'].value;
	var BUC = document.forms['seaFreightDataForm'].elements['refFreightRates.BUCValue'].value;
	var destTHC = document.forms['seaFreightDataForm'].elements['refFreightRates.destTHCValue'].value;
	var totalPrice = document.forms['seaFreightDataForm'].elements['refFreightRates.totalPrice'].value;
	
	var result = onlyFloat1(orgAC);
	var result1 = onlyFloat1(destAC);
	var result2 = onlyFloat1(BUC);
	var result3 = onlyFloat1(destTHC);
	var result4 = onlyFloat1(totalPrice);
	
	
		if(result == 'error'){
		alert("Enter valid Number in Arbitraries Charge (Origin) Value");
		document.forms['seaFreightDataForm'].elements['refFreightRates.orgArbitrariesChargeValue'].value='';
		return false;
		}
	if(result1 == 'error'){
		alert("Enter valid Number in Arbitraries Charge (Destination) Value");
		document.forms['seaFreightDataForm'].elements['refFreightRates.destArbitrariesChargeValue'].value='';
		return false;
	}
	if(result2 == 'error'){
		alert("Enter valid Number in BUC Value");
		document.forms['seaFreightDataForm'].elements['refFreightRates.BUCValue'].value='';
		return false;
	}
	if(result3 == 'error'){
		alert("Enter valid Number in Dest. THC Value");
		document.forms['seaFreightDataForm'].elements['refFreightRates.destTHCValue'].value='';
		return false;
	}
		if(result4 == 'error'){
		alert("Enter valid Number in Total Price");
		document.forms['seaFreightDataForm'].elements['refFreightRates.totalPrice'].value='';
		return false;
	}
	
	else
	{
		return true;
	}
	

}

</script> 
 <title><fmt:message key="seaFreightDataForm.title" /></title>
 <meta name="heading" content="<fmt:message key='seaFreightDataForm.heading'/>" /> 
</head> 
<s:form name="seaFreightDataForm" id="seaFreightDataForm" action="saveSeaFreightDataForm.html" method="post" validate="true" onsubmit="return Validate();"> 
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
    <s:hidden name="id" value="<%=request.getParameter("id")%>" />
    <s:hidden name="refFreightRates.id" value="%{refFreightRates.id}" />
	<div id="layer4" style="width:100%;">
	<div id="newmnav">
		  <ul>
		      <li id="newmnav1" style="background:#FFF "><a class="current" ><span>Sea Freight Data Detail<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
              <li><a href="seaFreightDataList.html"><span>Search Records</span></a></li> 
         </ul>
   </div><div class="spn">&nbsp;</div> 
   </div>
   <div id="Layer1"  onkeydown="changeStatus();" style="width:100%;">
   <div id="content" align="center">
   <div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">  
   <table border="0" style="margin: 0px;padding: 0px;" >
    <tr>
        <td class="listwhitetext" align="right" >Service Contract #<font size="2" color="red">*</td>
	    <td colspan="2" >
	    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px; paddding:0px;">
	    <tr>
	    <td align="left"><s:textfield name="refFreightRates.serviceContractNo" size="30" maxlength="100"  cssClass="input-text" tabindex="1"/></td>
	    <td  width="95">&nbsp;</td>
	    <td class="listwhitetext"  align="right"><fmt:message key='refFreightRates.carrier'/></td>
	    </tr>
	    </table>	    
	    </td>
	    <td colspan="2" >
	    <table cellpadding="0" cellspacing="0" border="0" style="margin:0px; paddding:0px;">
	    <tr>
	    <td align="left" ><s:textfield name="refFreightRates.carrier" size="30" maxlength="100"  cssClass="input-text" tabindex="2" /></td> 
	    <td  width="80">&nbsp;</td>
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.mode'/></td>
	    </tr>
	    </table>	    
	    </td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.mode" list="%{mode}" cssStyle="width:125px" tabindex="3" /></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.baseCurrency'/></td>
	    <td align="left"><s:select cssClass="list-menu" key="refFreightRates.baseCurrency" list="%{currency}" cssStyle="width:100px" headerKey="" headerValue="" tabindex="4"/></td>
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.contractType'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.contractType" size="15" maxlength="25"  cssClass="input-text" tabindex="5" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.equipmentType'/></td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.equipmentType" list="%{equipmentTypeList}" cssStyle="width:125px" headerKey="" headerValue="" tabindex="6"/></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.transitDays'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.transitDays" size="15" maxlength="25"  cssClass="input-text" tabindex="7" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.numberofSailings'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.numberofSailings" size="15" maxlength="25"  cssClass="input-text" tabindex="8" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.sailingFrequency'/></td>
	 	<td align="left" ><s:textfield name="refFreightRates.sailingFrequency" size="20" maxlength="25"  cssClass="input-text" tabindex="9" /></td> 
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.transshipment'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.transshipment" size="15" maxlength="25"  cssClass="input-text" tabindex="10" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.LineItemEffDate'/> </td>
	    <c:if test="${not empty refFreightRates.lineItemEffDate}"> 
			 <s:text id="lineItemEffDateValue" name="${FormDateValue}"><s:param name="value" value="refFreightRates.lineItemEffDate"/></s:text>
			 <td width="130px"><div style="float:left;"><s:textfield id="lineItemEffDate" name="refFreightRates.lineItemEffDate" value="%{lineItemEffDateValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
	         </div><div style="float:left;">&nbsp;<img id="lineItemEffDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></div></td>				
	    </c:if>
	    <c:if test="${empty refFreightRates.lineItemEffDate}">
		<td width="130px"><div style="float:left;"><s:textfield id="lineItemEffDate" name="refFreightRates.lineItemEffDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
        </div><div style="float:left;">&nbsp;<img id="lineItemEffDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></div></td>				
        </c:if> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.LineItemExpDate'/></td>
	 	<c:if test="${not empty refFreightRates.lineItemExpDate}"> 
			 <s:text id="lineItemEffDateValue" name="${FormDateValue}"><s:param name="value" value="refFreightRates.lineItemExpDate"/></s:text>
			 <td width="130px"><div style="float:left;"><s:textfield id="lineItemExpDate" name="refFreightRates.lineItemExpDate" value="%{lineItemEffDateValue}" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
	         </div><div style="float:left;">&nbsp;<img id="lineItemExpDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></div></td>				
	    </c:if>
	    <c:if test="${empty refFreightRates.lineItemExpDate}">
		<td width="130px"><div style="float:left;"><s:textfield id="lineItemExpDate" name="refFreightRates.lineItemExpDate" onkeydown="return onlyDel(event,this)" readonly="true" cssClass="input-text" size="7"/>
        </div><div style="float:left;">&nbsp;<img id="lineItemExpDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></div></td>				
        </c:if>  
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.totalPrice'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.totalPrice" size="15" maxlength="12"  cssClass="input-text" tabindex="11" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.laneComments'/></td> 
	    <td colspan="3" align="left" class="listwhitetext"><s:textarea  name="refFreightRates.laneComments" cssStyle="width:404px;height:32px;" tabindex="12" cssClass="textarea"/></td> 
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.originRegion'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.originRegion" size="15" maxlength="25"  cssClass="input-text" tabindex="13" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.originCity'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.originCity" size="15" maxlength="100"  cssClass="input-text" tabindex="14" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.originCountry'/></td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.originCountry" list="%{country}" cssStyle="width:125px" headerKey="" headerValue="" tabindex="15"/></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.originRouting'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.originRouting" size="15" maxlength="100"  cssClass="input-text" tabindex="16" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.originPortCity'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.originPortCity" size="15" maxlength="100"  cssClass="input-text" tabindex="17" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.originPortCountry'/></td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.originPortCountry" list="%{country}" cssStyle="width:125px" headerKey="" headerValue="" tabindex="18"/></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destinationRegion'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.destinationRegion" size="15" maxlength="25"  cssClass="input-text" tabindex="19" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destinationCity'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.destinationCity" size="15" maxlength="100"  cssClass="input-text" tabindex="20" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.destinationCountry'/></td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.destinationCountry" list="%{country}" cssStyle="width:125px" headerKey="" headerValue="" tabindex="21" /></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ></td>
	    <td align="left" ></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destinationPortCity'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.destinationPortCity" size="15" maxlength="100"  cssClass="input-text" tabindex="22" /></td> 
	    <td class="listwhitetext"  align="right" ><fmt:message key='refFreightRates.destinationPortCountry'/></td>
	 	<td align="left"><s:select cssClass="list-menu" key="refFreightRates.destinationPortCountry" list="%{country}" cssStyle="width:125px" headerKey="" headerValue="" tabindex="23" onchange="checkCode();"/></td>
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.orgArbitrariesChargeValue'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.orgArbitrariesChargeValue" size="15" maxlength="12"  cssClass="input-text" tabindex="24"/></td> 
	    <td class="listwhitetext" align="right" width="200px"><fmt:message key='refFreightRates.orgArbitrariesChargeDetails'/></td>
	    <td align="left" colspan="2"><s:textfield name="refFreightRates.orgArbitrariesChargeDetails" size="50" maxlength="25"  cssClass="input-text" tabindex="25" /></td> 
	    
    </tr>
    <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destArbitrariesChargeValue'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.destArbitrariesChargeValue" size="15" maxlength="12"  cssClass="input-text" tabindex="26"/></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destArbitrariesChargeDetails'/></td>
	    <td align="left" colspan="2"><s:textfield name="refFreightRates.destArbitrariesChargeDetails" size="50" maxlength="25"  cssClass="input-text" tabindex="27" /></td> 
	    
    </tr>
     <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.BUCValue'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.BUCValue" size="15" maxlength="12"  cssClass="input-text" tabindex="28" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.BUCDetails'/></td>
	    <td align="left"colspan="2" ><s:textfield name="refFreightRates.BUCDetails" size="50" maxlength="25"  cssClass="input-text" tabindex="29" /></td> 
	    
    </tr>
     <tr>
        <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destTHCValue'/></td>
	    <td align="left" ><s:textfield name="refFreightRates.destTHCValue" size="15" maxlength="12"  cssClass="input-text" tabindex="30" /></td> 
	    <td class="listwhitetext" align="right" ><fmt:message key='refFreightRates.destTHCDetails'/></td>
	    <td align="left" colspan="2"><s:textfield name="refFreightRates.destTHCDetails" size="50" maxlength="25"  cssClass="input-text" tabindex="31" /></td> 
	    
    </tr>
    <tr><td height="15px"></td></tr>
   </table> 
					
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>  
 <table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="">
  <tbody>
	<tr>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
		<td valign="top"></td>
		<td style="width:120px">
			<fmt:formatDate var="portCreatedOnFormattedValue" value="${refFreightRates.createdOn}" pattern="${displayDateTimeEditFormat}"/>
			<s:hidden name="refFreightRates.createdOn" value="${portCreatedOnFormattedValue}"/>
			<fmt:formatDate value="${refFreightRates.createdOn}" pattern="${displayDateTimeFormat}"/>
	   </td> 		
	   <td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td> 
		 <c:if test="${not empty refFreightRates.id}">
			 <s:hidden name="refFreightRates.createdBy" />
			 <td ><s:label name="createdBy" value="%{refFreightRates.createdBy}" /></td>
		 </c:if>
		 <c:if test="${empty refFreightRates.id}">
			 <s:hidden name="refFreightRates.createdBy" value="${pageContext.request.remoteUser}" />
			 <td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
		 </c:if> 		
		 <fmt:formatDate var="portUpdatedOnFormattedValue" value="${refFreightRates.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
		 <td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
		 <s:hidden name="refFreightRates.updatedOn"  value="${portUpdatedOnFormattedValue}"/>
		 <td style="width:130px"><fmt:formatDate value="${refFreightRates.updatedOn}" pattern="${displayDateTimeFormat}"/>
		 <td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
		 <c:if test="${not empty refFreightRates.id}">
		 <s:hidden name="refFreightRates.updatedBy" />
		 <td ><s:label name="updatedBy" value="%{refFreightRates.updatedBy}" /></td>
		 </c:if>
		 <c:if test="${empty refFreightRates.id}">
			 <s:hidden name="refFreightRates.updatedBy" value="${pageContext.request.remoteUser}" />
			 <td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
		 </c:if>
						
	 </tr>
  </tbody>
 </table>	
 </div>
 <div style="padding-left:15px; padding-top:10px;">

<s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return confirmValidate();" tabindex="32" />

    <input type="button" class="cssbuttonA" style="width:55px; height:25px"  
        onclick="location.href='<c:url value="/editSeaFreightData.html"/>'"  
        value="<fmt:message key="button.add"/>"/> 
 </div>
</s:form>


<script type="text/javascript">
    var myimage = document.getElementsByTagName("img"); 
   	for (var i = 0; i < myimage.length; i++) {
	   	var idinms=myimage[i].getAttribute('id');
	   	if(idinms != null && (idinms.split('_')[1]=="trigger")){
		   	var args1=idinms.split('_');
		   	RANGE_CAL_1 = new Calendar({
		           inputField: args1[0],
		           dateFormat: "%d-%b-%y",
		           trigger: idinms,
		           bottomBar: true,
		           animation:true,
		           onSelect: function() {                             
		               this.hide();
		       }
		   });
	   	}
   	}
</script>

