package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;




@Entity
@Table (name="customerRelation")


public class CustomerRelation extends BaseObject {
	private Long id;
	private String corpId;
    private Long customerFileId;
	private String updatedBy;
	private Date createdOn;
	private Date updateOn;
	private String createdBy;
	private String workArea;
	private String position;
	private String reasonForTransfer;
	private Date loadBeginDate;
	private Date loadEndDate;
	private String frequencyOfMove;
	private String serviceExpectations;
	private String favColour;
	private String favCake;
	private String favHobby;
	private String specialItem;
	private String qualityRS;
	private String favPlace;
	private String timeOfReturn;
	

	@Override
	public String toString() {
		return new ToStringBuilder(this)
				.append("id", id)
				.append("corpId", corpId)
				.append("customerFileId", customerFileId)
				.append("updatedBy", updatedBy)
				.append("createdOn", createdOn)
				.append("updateOn", updateOn)
				.append("createdBy", createdBy)
				.append("workArea", workArea)
				.append("position", position)
				.append("reasonForTransfer", reasonForTransfer)
				.append("loadBeginDate", loadBeginDate)
				.append("loadEndDate", loadEndDate)
				.append("frequencyOfMove", frequencyOfMove)
				.append("serviceExpectations", serviceExpectations)
				.append("favColour", favColour)
				.append("favCake", favCake)
				.append("favHobby", favHobby)
				.append("specialItem", specialItem)
				.append("qualityRS", qualityRS)
				.append("favPlace", favPlace)
				.append("timeOfReturn", timeOfReturn)
				.toString();
	}

	

	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CustomerRelation))
			return false;
		CustomerRelation castOther = (CustomerRelation) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(corpId, castOther.corpId)
				.append(customerFileId, castOther.customerFileId)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updateOn, castOther.updateOn)
				.append(createdBy, castOther.createdBy)
				.append(workArea, castOther.workArea)
				.append(position, castOther.position)
				.append(reasonForTransfer, castOther.reasonForTransfer)
				.append(loadBeginDate, castOther.loadBeginDate)
				.append(loadEndDate, castOther.loadEndDate)
				.append(frequencyOfMove, castOther.frequencyOfMove)
				.append(serviceExpectations, castOther.serviceExpectations)
				.append(favColour, castOther.favColour)
				.append(favCake, castOther.favCake)
				.append(favHobby, castOther.favHobby)
				.append(specialItem, castOther.specialItem)
				.append(qualityRS, castOther.qualityRS)
				.append(favPlace, castOther.favPlace)
				.append(timeOfReturn, castOther.timeOfReturn)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(corpId).append(customerFileId)
				.append(updatedBy).append(createdOn).append(updateOn)
				.append(createdBy).append(workArea).append(position)
				.append(reasonForTransfer).append(loadBeginDate)
				.append(loadEndDate).append(frequencyOfMove).append(serviceExpectations)
				.append(favColour).append(favCake)
				.append(favHobby).append(specialItem)
				.append(qualityRS).append(favPlace)
				.append(timeOfReturn).toHashCode();
	}


	public String getFavPlace() {
		return favPlace;
	}



	public void setfavPlace(String favPlace) {
		this.favPlace = favPlace;
	}



	public String getTimeOfReturn() {
		return timeOfReturn;
	}



	public void setTimeOfReturn(String timeOfReturn) {
		this.timeOfReturn = timeOfReturn;
	}



	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getCorpId() {
		return corpId;
	}



	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}



	public Long getCustomerFileId() {
		return customerFileId;
	}



	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}



	public String getUpdatedBy() {
		return updatedBy;
	}



	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}



	public Date getCreatedOn() {
		return createdOn;
	}



	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public Date getUpdateOn() {
		return updateOn;
	}



	public void setUpdateOn(Date updateOn) {
		this.updateOn = updateOn;
	}



	public String getCreatedBy() {
		return createdBy;
	}



	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}



	public String getWorkArea() {
		return workArea;
	}



	public void setWorkArea(String workArea) {
		this.workArea = workArea;
	}



	public String getPosition() {
		return position;
	}



	public void setPosition(String position) {
		this.position = position;
	}



	public String getReasonForTransfer() {
		return reasonForTransfer;
	}



	public void setReasonForTransfer(String reasonForTransfer) {
		this.reasonForTransfer = reasonForTransfer;
	}



	public Date getLoadBeginDate() {
		return loadBeginDate;
	}



	public void setLoadBeginDate(Date loadBeginDate) {
		this.loadBeginDate = loadBeginDate;
	}



	public Date getLoadEndDate() {
		return loadEndDate;
	}



	public void setLoadEndDate(Date loadEndDate) {
		this.loadEndDate = loadEndDate;
	}



	public String getFrequencyOfMove() {
		return frequencyOfMove;
	}



	public void setFrequencyOfMove(String frequencyOfMove) {
		this.frequencyOfMove = frequencyOfMove;
	}



	public String getServiceExpectations() {
		return serviceExpectations;
	}



	public void setServiceExpectations(String serviceExpectations) {
		this.serviceExpectations = serviceExpectations;
	}



	public String getFavColour() {
		return favColour;
	}



	public void setFavColour(String favColour) {
		this.favColour = favColour;
	}



	public String getFavCake() {
		return favCake;
	}



	public void setFavCake(String favCake) {
		this.favCake = favCake;
	}



	public String getFavHobby() {
		return favHobby;
	}



	public void setFavHobby(String favHobby) {
		this.favHobby = favHobby;
	}



	public String getSpecialItem() {
		return specialItem;
	}



	public void setSpecialItem(String specialItem) {
		this.specialItem = specialItem;
	}



	public String getQualityRS() {
		return qualityRS;
	}



	public void setQualityRS(String qualityRS) {
		this.qualityRS = qualityRS;
	}
	
	
}
