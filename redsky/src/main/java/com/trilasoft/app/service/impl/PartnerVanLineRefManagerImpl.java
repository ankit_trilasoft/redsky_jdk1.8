package com.trilasoft.app.service.impl;

import java.util.List;
import org.appfuse.service.impl.GenericManagerImpl;
import com.trilasoft.app.dao.PartnerVanLineRefDao;
import com.trilasoft.app.model.PartnerVanLineRef;
import com.trilasoft.app.service.PartnerVanLineRefManager;

public class PartnerVanLineRefManagerImpl extends GenericManagerImpl<PartnerVanLineRef, Long> implements PartnerVanLineRefManager {

	PartnerVanLineRefDao partnerVanLineRefDao;

	public PartnerVanLineRefManagerImpl(PartnerVanLineRefDao partnerVanLineRefDao) {
		super(partnerVanLineRefDao);
		this.partnerVanLineRefDao = partnerVanLineRefDao;
	}

	public List getPartnerVanLineReferenceList(String corpId, String partnerCodeForRef) {
		return partnerVanLineRefDao.getPartnerVanLineReferenceList(corpId, partnerCodeForRef);

	}

	public List getpartner(String partnerCode, String sessionCorpID) {
		return partnerVanLineRefDao.getpartner(partnerCode, sessionCorpID);
	}

	public List checkJobType(String vanLineCode, String corpID, String jobType) {
		return partnerVanLineRefDao.checkJobType(vanLineCode, corpID, jobType);
	}

	public int updateDeleteStatus(Long ids) {
		return partnerVanLineRefDao.updateDeleteStatus(ids);
	}

}
