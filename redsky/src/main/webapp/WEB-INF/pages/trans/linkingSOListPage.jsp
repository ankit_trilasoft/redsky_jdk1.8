<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/struts-tags" prefix="s" %>
<%@ include file="/common/taglibs.jsp"%>
<%
    response.setContentType("text/xml");
	response.setHeader("Cache-Control", "no-cache");
%>



<display:table name="linkedSOMerge" class="table" id="linkedSOMerge">
	
	<display:column title="">
		<c:if test="${linkedSOMerge.ugwIntId == null || linkedSOMerge.ugwIntId==''  }">
			<input type="radio" name="linkSOId" value="${linkedSOMerge.id }" id="linkSOId" onclick="setLinkSOIdvalue('${linkedSOMerge.id }')"/>
		</c:if>
		<c:if test="${linkedSOMerge.ugwIntId ne null && linkedSOMerge.ugwIntId!='' }">
			<input type="radio" name="linkSOId" value="${linkedSOMerge.id }" id="linkSOId" disabled="disabled"/>
		</c:if>
	</display:column>
	<display:column property="shipNumber"  title="SO#"/>
	<display:column property="mode"  title="Mode"/>
	<display:column property="commodity"  title="Commodity"/>
	<display:column property="oCity"  title="Origin City"/>
	<display:column property="dCity"  title="Destin City"/>
	<display:column property="shipper"  title="Shipper"/>
</display:table>
<div style="position: absolute; float: right; text-align: right; left: 150px; margin-top: 3px;">
<td><input type="button" name="CreateSO" value="Create new SO" id="CreateSO" onclick ="createSO()" class="cssbutton"  /></td>
	<input type="button" name="Merge" value="Merge" id="merge" onclick ="mergeFields()" class="cssbutton" disabled="true" />
</div>

