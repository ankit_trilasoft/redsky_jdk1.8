<?xml version="1.0" encoding="UTF-8"  ?>
<!-- Created with iReport - A designer for JasperReports -->
<!DOCTYPE jasperReport PUBLIC "//JasperReports//DTD Report Design//EN" "http://jasperreports.sourceforge.net/dtds/jasperreport.dtd">
<jasperReport
		 name="UnionPension"
		 columnCount="1"
		 printOrder="Vertical"
		 orientation="Portrait"
		 pageWidth="612"
		 pageHeight="792"
		 columnWidth="576"
		 columnSpacing="0"
		 leftMargin="18"
		 rightMargin="18"
		 topMargin="20"
		 bottomMargin="20"
		 whenNoDataType="NoPages"
		 isTitleNewPage="false"
		 isSummaryNewPage="false">
	<property name="ireport.scriptlethandling" value="0" />
	<property name="ireport.encoding" value="UTF-8" />
	<import value="java.util.*" />
	<import value="net.sf.jasperreports.engine.*" />
	<import value="net.sf.jasperreports.engine.data.*" />

	<style 
		name="default"
		isDefault="true"
		fontName="Arial"
	>
	</style>

	<parameter name="begindate" isForPrompting="true" class="java.util.Date">
		<parameterDescription><![CDATA[Enter Begin Date]]></parameterDescription>
		<defaultValueExpression ><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<parameter name="enddate" isForPrompting="true" class="java.util.Date">
		<parameterDescription><![CDATA[Enter End Date]]></parameterDescription>
		<defaultValueExpression ><![CDATA[new Date()]]></defaultValueExpression>
	</parameter>
	<queryString><![CDATA[SELECT
     'UnionPension-R-SSCW.jrxml',
        CONCAT(crew.`lastName`,",",crew.`firstName`)AS Name,
	systemdefault.`pensionRate1` AS systemdefault_pensionRate1,
	systemdefault.`pensionRate2` AS systemdefault_pensionRate2,
	systemdefault.`pensionRate3` AS systemdefault_pensionRate3,
	systemdefault.`pensionRate4` AS systemdefault_pensionRate4,
        crew.`warehouse` AS WareHouse,
        date(timesheet.`workDate`) AS WorkDate,
        WEEK(timesheet.`workDate`) AS WorkDateWeek,
	MONTH(timesheet.`workDate`) AS WorkDateMonth,
        crew.`socialSecurityNumber` AS SocialSecurityNumber,
        IF(crew.`hired`<systemdefault.`pensionEffectiveDate1`,systemdefault.`pensionRate1`,if((crew.`hired` >=systemdefault.`pensionEffectiveDate2` and period_diff(date_format(now(),'%Y%m'),date_format(crew.`hired`,'%Y%m'))>36),systemdefault.`pensionRate2`,0)) AS PensionRate,
        if((IF(crew.`hired`<systemdefault.`pensionEffectiveDate1`,systemdefault.`pensionRate1`,if((crew.`hired` >=systemdefault.`pensionEffectiveDate2` and period_diff(date_format(now(),'%Y%m'),date_format(crew.`hired`,'%Y%m'))>36),systemdefault.`pensionRate2`,0)))!=0,if(((sum( if(timesheet.`action`='C'and timesheet.`workdate` >=crew.`begginingPension` and(crew.`begginingPension` IS NOT null or crew.`begginingPension`<>' '),round(if(timesheet.`paidRevenueShare`>0,if(timesheet.`regularHours`>8,8,timesheet.`regularHours`)+timesheet.`overTime`+timesheet.`doubleOverTime`,if(timesheet.`regularHours`>8,8,timesheet.`regularHours`)),1),0))))>40,40,((sum( if(timesheet.`action`='C'and timesheet.`workdate` >=crew.`begginingPension` and(crew.`begginingPension` IS NOT null or crew.`begginingPension`<>' '),round(if(timesheet.`paidRevenueShare`>0,if(timesheet.`regularHours`>8,8,timesheet.`regularHours`)+timesheet.`overTime`+timesheet.`doubleOverTime`,if(timesheet.`regularHours`>8,8,timesheet.`regularHours`)),1),0))))),0) as RegTTLbyPERSON1,
	round(if(sum(if(timesheet.`regularHours` is null,0.0,timesheet.`regularHours`))>8,8.0,sum(if(timesheet.`regularHours` is null,0.0,timesheet.`regularHours`))),1) AS RegTTLbyPERSON
FROM
     `crew` crew LEFT OUTER JOIN `timesheet` timesheet ON crew.`userName` = timesheet.`userName`
     LEFT OUTER JOIN `systemdefault` systemdefault ON crew.`corpID` = systemdefault.`corpID`
WHERE
     timesheet.`workDate` >= $P{begindate}
 AND timesheet.`workDate` <= $P{enddate}
 AND timesheet.`corpID` = "SSCW"
 AND timesheet.action NOT IN ('P','H','V','S','BE')
 AND timesheet.`warehouse` <> "15"
 and crew.`unionName` is not null
 AND timesheet.`regularHours` > 0
 AND timesheet.`regularHours` <> ''
 AND (crew.`terminatedDate` is null  OR crew.`terminatedDate` > $P{begindate})
 AND (IF(crew.`hired`<systemdefault.`pensionEffectiveDate1`,systemdefault.`pensionRate1`,if((crew.`hired` >=systemdefault.`pensionEffectiveDate2` and period_diff(date_format(now(),'%Y%m'),date_format(crew.`hired`,'%Y%m'))>36),systemdefault.`pensionRate2`,0)))
group by timesheet.`userName`, YEAR(timesheet.`workDate`) ,month(timesheet.`workDate`),week(timesheet.`workDate`),date(timesheet.`workDate`)
order BY timesheet.`userName`,YEAR(timesheet.`workDate`),month(timesheet.`workDate`),week(timesheet.`workDate`),date(timesheet.`workDate`)]]></queryString>

	<field name="UnionPension-R-SSCW.jrxml" class="java.lang.String"/>
	<field name="Name" class="java.lang.String"/>
	<field name="systemdefault_pensionRate1" class="java.math.BigDecimal"/>
	<field name="systemdefault_pensionRate2" class="java.math.BigDecimal"/>
	<field name="systemdefault_pensionRate3" class="java.math.BigDecimal"/>
	<field name="systemdefault_pensionRate4" class="java.math.BigDecimal"/>
	<field name="WareHouse" class="java.lang.String"/>
	<field name="WorkDate" class="java.sql.Date"/>
	<field name="WorkDateWeek" class="java.lang.Integer"/>
	<field name="WorkDateMonth" class="java.lang.Integer"/>
	<field name="SocialSecurityNumber" class="java.lang.String"/>
	<field name="PensionRate" class="java.math.BigDecimal"/>
	<field name="RegTTLbyPERSON1" class="java.math.BigDecimal"/>
	<field name="RegTTLbyPERSON" class="java.math.BigDecimal"/>

	<sortField name="PensionRate" />
	<sortField name="Name" />
	<sortField name="WorkDate" />

	<variable name="WeeklyHour" class="java.math.BigDecimal" resetType="Group" resetGroup="WorkDateWeek" calculation="Sum">
		<variableExpression><![CDATA[$F{RegTTLbyPERSON}]]></variableExpression>
	</variable>
	<variable name="MonthlyHour" class="java.math.BigDecimal" resetType="Group" incrementType="Group" incrementGroup="WorkDateWeek" resetGroup="WorkDateMonth" calculation="Sum">
		<variableExpression><![CDATA[($V{WeeklyHour}.subtract(new BigDecimal(40.0))).floatValue()>= 0 ? new BigDecimal(40.0) : $V{WeeklyHour}]]></variableExpression>
	</variable>
	<variable name="CrewHrMonth" class="java.math.BigDecimal" resetType="Group" resetGroup="Name" calculation="Nothing">
		<variableExpression><![CDATA[($V{MonthlyHour}.subtract(new BigDecimal(140.0))).floatValue()>= 0 ? new BigDecimal(140.0) : $V{MonthlyHour}]]></variableExpression>
	</variable>
	<variable name="PensionHrAllMonth" class="java.math.BigDecimal" resetType="Group" incrementType="Group" incrementGroup="Name" resetGroup="penrate" calculation="Sum">
		<variableExpression><![CDATA[$V{CrewHrMonth}]]></variableExpression>
	</variable>
	<variable name="PensionHrTotal" class="java.math.BigDecimal" resetType="Report" incrementType="Group" incrementGroup="penrate" calculation="Sum">
		<variableExpression><![CDATA[$V{PensionHrAllMonth}]]></variableExpression>
	</variable>
	<variable name="Pension" class="java.math.BigDecimal" resetType="Group" resetGroup="Name" calculation="Nothing">
		<variableExpression><![CDATA[$V{CrewHrMonth}.multiply($F{PensionRate})]]></variableExpression>
	</variable>
	<variable name="PensionAllMonth" class="java.math.BigDecimal" resetType="Group" incrementType="Group" incrementGroup="Name" resetGroup="penrate" calculation="Sum">
		<variableExpression><![CDATA[$V{Pension}]]></variableExpression>
	</variable>
	<variable name="PensionTotal" class="java.math.BigDecimal" resetType="Report" incrementType="Group" incrementGroup="penrate" calculation="Sum">
		<variableExpression><![CDATA[$V{PensionAllMonth}]]></variableExpression>
	</variable>

		<group  name="penrate" >
			<groupExpression><![CDATA[$F{PensionRate}]]></groupExpression>
			<groupHeader>
			<band height="24"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue() > 0.00)]]></printWhenExpression>
				<staticText>
					<reportElement
						style="default"
						x="7"
						y="2"
						width="43"
						height="19"
						forecolor="#0000CC"
						key="staticText-24"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Rate $]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="62"
						y="2"
						width="67"
						height="18"
						forecolor="#0000CC"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Center" verticalAlignment="Middle">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$F{PensionRate}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						style="default"
						x="7"
						y="23"
						width="122"
						height="0"
						key="line-3"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
			</groupHeader>
			<groupFooter>
			<band height="32"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue()> 0.00)]]></printWhenExpression>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="481"
						y="14"
						width="87"
						height="18"
						forecolor="#330066"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{PensionAllMonth}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="378"
						y="14"
						width="91"
						height="18"
						forecolor="#330066"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{PensionHrAllMonth}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						style="default"
						x="333"
						y="3"
						width="235"
						height="0"
						key="line-4"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
			</groupFooter>
		</group>
		<group  name="Name" >
			<groupExpression><![CDATA[$F{Name}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="18"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue() > 0.00)]]></printWhenExpression>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="0"
						width="167"
						height="18"
						forecolor="#000000"
						key="textField-11"/>
					<box></box>
					<textElement>
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{Name}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="168"
						y="0"
						width="94"
						height="18"
						forecolor="#000000"
						key="textField-12"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{SocialSecurityNumber}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="323"
						y="0"
						width="43"
						height="18"
						forecolor="#000000"
						key="textField-13"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA[$F{WareHouse}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.0" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="401"
						y="0"
						width="69"
						height="18"
						forecolor="#000000"
						key="textField-14"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{CrewHrMonth}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="488"
						y="0"
						width="80"
						height="18"
						forecolor="#000000"
						key="textField-15"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{Pension}]]></textFieldExpression>
				</textField>
			</band>
			</groupFooter>
		</group>
		<group  name="WorkDateMonth" >
			<groupExpression><![CDATA[$F{WorkDateMonth}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue() > 0.00)]]></printWhenExpression>
			</band>
			</groupFooter>
		</group>
		<group  name="WorkDateWeek" >
			<groupExpression><![CDATA[$F{WorkDateWeek}]]></groupExpression>
			<groupHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupHeader>
			<groupFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
			</groupFooter>
		</group>
		<background>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</background>
		<title>
			<band height="2"  isSplitAllowed="true" >
			</band>
		</title>
		<pageHeader>
			<band height="65"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="175"
						y="18"
						width="100"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{begindate}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="MM/dd/yyyy" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="312"
						y="18"
						width="100"
						height="18"
						key="textField"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Left">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[$P{enddate}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						x="276"
						y="18"
						width="37"
						height="18"
						key="staticText-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<text><![CDATA[  to  ]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="110"
						y="0"
						width="346"
						height="18"
						key="staticText-18"/>
					<box></box>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Union Pension Payment]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="1"
						y="42"
						width="83"
						height="13"
						key="staticText-19"/>
					<box></box>
					<textElement>
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Name]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="183"
						y="42"
						width="98"
						height="13"
						key="staticText-20"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Social Security#]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="481"
						y="42"
						width="83"
						height="13"
						key="staticText-21"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Pension Pay]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="390"
						y="42"
						width="83"
						height="13"
						key="staticText-22"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Regular Hours]]></text>
				</staticText>
				<staticText>
					<reportElement
						style="default"
						x="312"
						y="42"
						width="64"
						height="13"
						key="staticText-23"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" isBold="true" isUnderline="false"/>
					</textElement>
				<text><![CDATA[Warehouse]]></text>
				</staticText>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="61"
						width="576"
						height="0"
						forecolor="#666600"
						key="line-7"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="5.0"/>
</graphicElement>
				</line>
			</band>
		</pageHeader>
		<columnHeader>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnHeader>
		<detail>
			<band height="0"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue() > 0.00)]]></printWhenExpression>
			</band>
		</detail>
		<columnFooter>
			<band height="0"  isSplitAllowed="true" >
			</band>
		</columnFooter>
		<pageFooter>
			<band height="37"  isSplitAllowed="true" >
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="1"
						y="4"
						width="129"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-3"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.util.Date"><![CDATA[new Date()]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="540"
						y="4"
						width="36"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-4"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement>
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["" + $V{PAGE_NUMBER}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement
						style="default"
						x="201"
						y="6"
						width="147"
						height="17"
						forecolor="#000000"
						key="staticText-17"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Center">
						<font fontName="Times New Roman" size="8"/>
					</textElement>
				<text><![CDATA[Union Pension Report]]></text>
				</staticText>
				<textField isStretchWithOverflow="false" pattern="" isBlankWhenNull="false" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="393"
						y="3"
						width="146"
						height="19"
						forecolor="#000000"
						backcolor="#FFFFFF"
						key="textField-5"/>
					<box>					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
</box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" size="10"/>
					</textElement>
				<textFieldExpression   class="java.lang.String"><![CDATA["Page " + $V{PAGE_NUMBER} + " of "]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						x="0"
						y="4"
						width="576"
						height="0"
						forecolor="#00FFCC"
						key="line-5"/>
					<graphicElement stretchType="NoStretch"/>
				</line>
			</band>
		</pageFooter>
		<summary>
			<band height="44"  isSplitAllowed="true" >
				<printWhenExpression><![CDATA[new java.lang.Boolean($F{PensionRate}.floatValue() > 0.00)]]></printWhenExpression>
				<textField isStretchWithOverflow="false" pattern="#,##0.00" isBlankWhenNull="true" evaluationTime="Now" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="481"
						y="21"
						width="91"
						height="18"
						forecolor="#660000"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{PensionTotal}]]></textFieldExpression>
				</textField>
				<textField isStretchWithOverflow="false" pattern="#,##0.0" isBlankWhenNull="true" evaluationTime="Report" hyperlinkType="None"  hyperlinkTarget="Self" >
					<reportElement
						x="367"
						y="21"
						width="100"
						height="18"
						forecolor="#660000"
						key="textField"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-BoldOblique" size="12" isBold="true" isItalic="true"/>
					</textElement>
				<textFieldExpression   class="java.math.BigDecimal"><![CDATA[$V{PensionHrTotal}]]></textFieldExpression>
				</textField>
				<line direction="TopDown">
					<reportElement
						style="default"
						x="275"
						y="8"
						width="293"
						height="0"
						forecolor="#666600"
						key="line-6"/>
					<graphicElement stretchType="NoStretch">
					<pen lineWidth="2.0" lineStyle="Double"/>
</graphicElement>
				</line>
				<staticText>
					<reportElement
						style="default"
						x="220"
						y="20"
						width="92"
						height="18"
						forecolor="#660000"
						key="staticText-25"/>
					<box></box>
					<textElement textAlignment="Right">
						<font fontName="Times New Roman" pdfFontName="Helvetica-Bold" size="12" isBold="true"/>
					</textElement>
				<text><![CDATA[Summary]]></text>
				</staticText>
			</band>
		</summary>
</jasperReport>
