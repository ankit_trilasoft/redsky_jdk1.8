<%@ include file="/common/taglibs.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<head>
    <title>Integration</title>
    <meta name="heading" content="Integration"/>

   
  <style> 
span.pagelinks {
display:block; font-size:0.95em; margin-bottom:5px; !margin-bottom:2px; margin-top:-22px; padding:2px 0px; text-align:right;
width:100%; !width:100%;
}
</style>
</head>
<s:form id="UGWWListForm" action="" method="post" validate="true"> 
 <!-- <p style="font-size:11px; font-weight:bold; margin-bottom:10px">Integration XML Summary</p>   -->
 <p style="font: bold 12px/2.5em arial,verdana; margin-bottom: 10px;" class="bgblue">Integration Centre</p>
	<div id="layer5" style="width:100%">	
	<div id="newmnav">
		  <ul>
		
		        <c:if test="${vanlineEnabled==true }">
			 <li><a href="integrationCenterList.html"><span>Vanline / Docs Downloads</span></a></li>
		      <li><a href="centreVanlineUpload.html"><span>Vanline / Docs Uploads</span></a></li>
		      <li><a href="centerIntegrationLogs.html"><span>Vanline Integration Log</span></a></li>
		       </c:if>
		         <c:if test="${enableMSS==true }">
		       <li><a href="mssLogList.html"><span>MSS Orders</span></a></li>
		       </c:if>
		        <%--
		        <li style="background:#FFF " id="newmnav1"><a class="current"><span>UGWW</span></a></li>
		       <li><a href="centerintegrationLogList.html"><span>UGWW Log</span></a></li>
		        --%>
		         <c:if test="${voxmeIntegration==true }">
		          <li><a href="voxmeEstimatorList.html"><span>Voxme&nbsp;Estimator</span></a></li>
		         </c:if>
		          <c:if test="${enablePricePoint==true}">
		          <li><a href="pricePointList.html"><span>Price&nbsp;Point</span></a></li>
		          </c:if>
		           <c:if test="${enableMoveForYou==true }">
		        <li><a href="moveForUList.html"><span>Move4u</span></a></li>
		        </c:if>
		        <%--
		        <c:if test="${enableMoveForYou==true }">
		        <li><a href="lisaIntegrationLogList.html"><span>LISA&nbsp;Integration</span></a></li>
		        </c:if>
		        --%>
		  </ul>
	</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>
	<div class="spn" style="height:0px;!margin-bottom:1px;">&nbsp;</div>
<s:set name="UGWWListForIntegration" value="UGWWListForIntegration" scope="request"/>
<display:table name="UGWWListForIntegration" class="table" requestURI="" id="UGWWListForIntegration" export="true" pagesize="30" style="width:100%" defaultsort="2" defaultorder="descending" >
	<display:column property="message" sortable="true" title="XML"/>
    <display:column property="date" sortable="true" title="DATE" format="{0,date,dd-MMM-yyyy}"/>
    <display:column property="count" sortable="true" headerClass="containeralign" title="No Of XMLs / Files Downloaded"  style="text-align: right;width:200px;"/>
    <display:setProperty name="paging.banner.first" value=""/>
    <display:setProperty name="export.excel.filename" value="Weekly XML Summary.xls"/>
    <display:setProperty name="export.csv.filename" value="Weekly XML Summary.csv"/>
    <display:setProperty name="export.pdf.filename" value="Weekly XML Summary.pdf"/> 
</display:table>
</div>
</s:form>
