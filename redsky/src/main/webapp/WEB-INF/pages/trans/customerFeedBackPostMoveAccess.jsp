<%@ include file="/common/taglibs.jsp"%>
<html><head>
	<title>RedSky Post Move Survey</title>
	<meta name="heading" content="Customer Request Access"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

</head>
<body>

<script type="text/javascript">
function fix_width(id) //set width of id to 85% if >=1150px, 95% 900-1149px, 100%<900px
{
  if (window.innerWidth>=1150)
    document.getElementById(id).style.width="85%";
	if ((window.innerWidth>=900)&&(window.innerWidth<1150))
    document.getElementById(id).style.width="95%";
  if (window.innerWidth<900)
    document.getElementById(id).style.width="100%";	
}	

function adjust_for_width()
{
  fix_width('page');
	if (window.innerWidth<900)
	  document.getElementById('logo').src="images/SuddathLogo_2012_small.jpg";
}

function get_radio_value(ele)
{
  var rad_val = null;
	
  for (var i=0; i < ele.length; i++)
  {
    if (ele[i].checked)
    {
      rad_val = ele[i].value;
    }
  }
	return rad_val;
}

function verify_comments(b)
{
  var oForm = document.forms["customerPostMoveRequestAccess"]; 
	
	//this also sets the forms hidden inputs with radio button values
	oForm.elements["ADMIN"].value = get_radio_value(oForm.elements["custService"]);
	oForm.elements["ORIGIN"].value = get_radio_value(oForm.elements["originAgent"]);
	oForm.elements["DEST"].value = get_radio_value(oForm.elements["destinAgent"]);
	oForm.elements["OVERALL"].value = get_radio_value(oForm.elements["overall"]);
	oForm.elements["RECOMMEND"].value = get_radio_value(oForm.elements["recommend"]);
  if ((oForm.elements["ADMIN"].value==1)&&(oForm.elements["comment1"].value.length<5))
	{
	  alert('If you gave a Customer Service score of Poor, please leave us a comment to explain.');
		oForm.elements["comment1"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["ORIGIN"].value==1)&&(oForm.elements["comment2"].value.length<5))
	{
	  alert('If you gave an Origin score of Poor, please leave us a comment to explain.');
		oForm.elements["comment2"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["DEST"].value==1)&&(oForm.elements["comment3"].value.length<5))
	{
	  alert('If you gave a Destination score of Poor, please leave us a comment to explain.');
		oForm.elements["comment3"].style.borderColor="red";
		return false;
	}	
	if ((oForm.elements["OVERALL"].value==1)&&(oForm.elements["comment4"].value.length<5))
	{
	  alert('If you gave an Overall score of Poor, please leave us a comment to explain.');
		oForm.elements["comment4"].style.borderColor="red";
		return false;
	}	
	if((oForm.elements["CONTACT_YES"].checked==true)){
		if(oForm.elements["contactPhone"].value=='')
		{
			alert('Please fill your phone.');
			oForm.elements["contactPhone"].style.borderColor="red";
			return false;
		}
		if(oForm.elements["contactEmail"].value=='')
		{
			alert('Please fill your email.');
			oForm.elements["contactEmail"].style.borderColor="red";
			return false;
		}
		if(oForm.elements["contactEmail"].value!='')
		{
			var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!filter.test(email.value)) {
				alert('Please provide a valid email address');
				oForm.elements["contactEmail"].style.borderColor="red";
				return false;
			}
		}
	}
	
  b.value = 'Submitting...';
  b.disabled = true;
  b.form.submit();
	
	return true;
}

</script>


<div id="container" style="width:85%; margin:0 auto;">
<div style="clear:both; margin-bottom:16px;"></div>

<!---main bordered div--->	
<div id="page" style="border:2px solid #219dd1; padding:0px 20px 20px 20px; margin:0px auto; width:85%; color:#7f7f7f;"> 

<s:form id="customerPostMoveRequestAccess" name="customerPostMoveRequestAccess" action="saveCustomerFeedBackPostMoveSurvey.html?decorator=popup&popup=true&bypass_sessionfilter=YES&soId=${soId}&sessionCorpID=${sessionCorpID}" method="post" validate="true">
<input type=hidden name=SOURCE value="SURVEY_GLOBAL">
<input type=hidden name=ACCOUNT value="295867">
<input type=hidden name=BRANCH_CODE value="05-016-00">
<input type=hidden name=MOVES_ID value="251631">
<input type=hidden name=REG value="251631"><input type=hidden name=ORDER value="">
<input type=hidden name=MM_NAME value="Katie Schiffers">
<input type=hidden name=TYPE_OF_MOVE value="D/D - DOOR/DOOR">
<input type=hidden name=DATE_SUBMITTED value="28-Apr-2016">
<input type=hidden name=TRANSFEREE value="Willy Chen">
<input type=hidden name=WWID value="">
<input type=hidden name=shipNumber value="${shipNumber}">
<c:if test="${sessionCorpID=='HOLL'}">
<c:if test="${postFeedBack=='Y'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Your Feedback has already been submitted!!</h3>
    </td>
  </tr>
</c:if>
<c:if test="${overall=='Y'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Thank you for taking the time to provide us with your feedback! The information we receive is invaluable in letting us know how we are doing with our customers.
We would like to ask if you could also share your review on Google Plus by clicking on this link <a href="https://plus.google.com/102868674974736187972/about"><span style="color:#5d5d5d;text-decoration:underline;">https://plus.google.com/102868674974736187972/about</span></a><br> 

<img style="margin-top:3px;" src="<c:url value='/images/image001.jpg'/>"/><br> In return we would like to thank you by sending you a $15 Fandango Movie Gift Card

<br>Thank you in advance!</h3>
    </td>
  </tr>
</c:if>
<c:if test="${overall=='N'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Thank you for taking the time to provide us with your feedback! Your message has been sent to the Hollander Moving and Storage team.</h3>
    </td>
  </tr>
</c:if>
<c:if test="${postFeedBack=='N'}">
<div style="text-align:left; font:700 13px arial!important; color:#003366;">
<div style="width:40%; float:left;  padding-top:5px;">Transferee: ${customerName}<br>Move Counselor: ${counselor}</div>
<div style="width:15%; float:left;  padding-top:5px; text-align:center;"><img src="<c:url value='/images/HOLL_logo.png'/>"/></div>
<div style="width:40%; float:right; padding-top:5px; text-align:right;">Registration #: ${shipNumber}<br>Evaluation Date: ${evaluationDate}</div>
<div style="clear:both;"></div>
</div>

<input type="hidden" name="ADMIN">
<input type="hidden" name="ORIGIN">
<input type="hidden" name="DEST">
<input type="hidden" name="OVERALL">
<input type="hidden" name="RECOMMEND">



<table class="plain" style="border:1px solid #219DD1;margin-left:90px; width:20%;margin-bottom:0px;padding-bottom:5px;">
<tr>
	<td><input type="radio" name="overall" value="Y"/></td>
	<td><input type="radio" name="overall" value="N"/></td>
</tr>
<tr>
  <td>YES</td><td>NO</td></tr>
</table>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;font:700 13px arial!important; color:#003366;">Comments</div>
  <textarea name=comment2 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>



<center>
<button type=submit class="cssbutton" style="margin:15px auto 5px auto; width:80px; height:30px;" onclick="return(verify_comments(this));" default>Submit</button>
<!-- <button type="button" class="cssbutton" style="margin:15px auto 5px auto; width:80px; height:30px;" onClick="window.close();" >Cancel</button> -->
</center>
</c:if>
</c:if>


<c:if test="${sessionCorpID=='STVF'}">
<c:if test="${postFeedBack=='Y'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Your Feedback has already been submitted!!</h3>
    </td>
  </tr>
</c:if>
<c:if test="${overall=='Y'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Thank you for taking the time to provide us with your feedback! The information we receive is invaluable in letting us know how we are doing with our customers.
</h3>
    </td>
  </tr>
</c:if>
<c:if test="${overall=='N'}">
<tr>
    <td valign="middle" align="left" style="padding-left:24%"><label>
    <h3 class="listwhitetext" style="font-size:15px;"><b>Thank you for taking the time to provide us with your feedback! Your message has been sent to the Stevens International team.</h3>
    </td>
  </tr>
</c:if>
<c:if test="${postFeedBack=='N'}">
<div style="text-align:left; font:700 13px arial!important; color:#003366;">
<div style="width:40%; float:left;  padding-top:5px;">Transferee: ${customerName}<br>Move Counselor: ${counselor}</div>
<div style="width:15%; float:left;  padding-top:5px; text-align:center;"><img src="<c:url value='/images/STVF_logo.png'/>"/></div>
<div style="width:40%; float:right; padding-top:5px; text-align:right;">Registration #: ${shipNumber}<br>Evaluation Date: ${evaluationDate}</div>
<div style="clear:both;"></div>
</div>

<input type="hidden" name="ADMIN">
<input type="hidden" name="ORIGIN">
<input type="hidden" name="DEST">
<input type="hidden" name="OVERALL">
<input type="hidden" name="RECOMMEND">


<div style="border:1px solid #e7e7e7;margin-left:90px;padding: 5px;margin-top: 10px;width: 88.5%;">
<table class="plain" style="width:80%;margin-bottom:0px;padding-bottom:5px;text-align:left;">
<tr>
	<td width="165" class="listwhitelargetext"><b>Would you recommend us ?</b></td>
	<td width="20"><input type="radio" style="vertical-align:top;" name="overall" value="Y"/></td><td width="40">YES</td>
	<td width="20"><input type="radio" name="overall" value="N"/></td><td>NO</td>
</tr>
</table>
<table class="plain" style="margin-bottom:0px;padding-bottom:5px;text-align:left;">
<tr>
<td class="listwhitelargetext"><b>Rate Origin Packing crew performance with 5 being the best (1-5)</b></td>
<td></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="originAgent" value="1"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="originAgent" value="2"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="originAgent" value="3"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="originAgent" value="4"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="originAgent" value="5"/></td>
</tr>
</table>
<table class="plain" style="margin-bottom:0px;padding-bottom:5px;text-align:left;">
<tr>
<td class="listwhitelargetext"><b>Destination Crew performance with 5 being the best (1-5)</b></td>
<td></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="destinAgent" value="1"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="destinAgent" value="2"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="destinAgent" value="3"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="destinAgent" value="4"/></td>
<td width="20"><input type="radio" style="vertical-align:top;" name="destinAgent" value="5"/></td>
</tr>
</table>
</div>
<br>
<!--comment-->
<div style="padding:0 8px 0 90px;">
  <div style="float:left; margin-left:-90px; width:90px;font:700 13px arial!important; color:#003366;">Comments</div>
  <textarea name=comment2 style="width:100%; height:50px;border:1px solid rgb(33, 157, 209);" maxlength="499" onKeyPress="return ( this.value.length < 500 );"></textarea>
	<div style="clear:both; height:20px;"></div>
</div>



<center>
<button type=submit class="cssbutton" style="margin:15px auto 5px auto; width:80px; height:30px;" onclick="return(verify_comments(this));" default>Submit</button>
<!-- <button type="button" class="cssbutton" style="margin:15px auto 5px auto; width:80px; height:30px;" onClick="window.close();" >Cancel</button> -->
</center>
</c:if>
</c:if>

</s:form>
<br>
</div>
</div>
</body>
</html> 
