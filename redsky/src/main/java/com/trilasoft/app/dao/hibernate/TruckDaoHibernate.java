package com.trilasoft.app.dao.hibernate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.TruckDao;
import com.trilasoft.app.dao.hibernate.util.HibernateUtil;
import com.trilasoft.app.model.RefMaster;
import com.trilasoft.app.model.Truck;

public class TruckDaoHibernate extends GenericDaoHibernate<Truck, Long> implements TruckDao {

	public TruckDaoHibernate() {
		super(Truck.class);
	}
	private HibernateUtil hibernateUtil;
	
	public void setHibernateUtil(HibernateUtil hibernateUtil){
        this.hibernateUtil = hibernateUtil;
	}

	public List searchTruck(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus,String dotInspFlag) {
		if(dotInspFlag.equalsIgnoreCase("true")){
			return getHibernateTemplate().find("from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' and dotInspDue is not null ");
		}else{
			return getHibernateTemplate().find("from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' ");	 
		}
	}

	public List searchTruckDotInsp(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus) {
		return getHibernateTemplate().find("from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' and dotInspDue is not null");	 
	}
	
	public List searchTruckDueSixty(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo,String truckStatus ,int myListFor) {
		if(myListFor==60){
			return getHibernateTemplate().find(" from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' and dotInspDue is not null and (DATEDIFF(DATE_FORMAT(dotInspDue,'%Y-%m-%d'),DATE_FORMAT(now(),'%Y-%m-%d')) < 61) and (DATEDIFF(DATE_FORMAT(dotInspDue,'%Y-%m-%d'),DATE_FORMAT(now(),'%Y-%m-%d')) > 30)");	 
		}
		if(myListFor==30)
		{
			return getHibernateTemplate().find(" from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' and dotInspDue is not null and (DATEDIFF(DATE_FORMAT(dotInspDue,'%Y-%m-%d'),DATE_FORMAT(now(),'%Y-%m-%d')) < 31) and (DATEDIFF(DATE_FORMAT(dotInspDue,'%Y-%m-%d'),DATE_FORMAT(now(),'%Y-%m-%d')) > -1)");		 	
		}
		if(myListFor==1)
		{
			return getHibernateTemplate().find(" from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and truckStatus='"+truckStatus+"' and dotInspDue is not null and (DATEDIFF(DATE_FORMAT(dotInspDue,'%Y-%m-%d'),DATE_FORMAT(now(),'%Y-%m-%d')) < 0) ");	
		}
		return null;
		}
	public Map<String, String> findDefaultStateList(String bucket2, String corpId){
		if(bucket2 == null || bucket2.equals("")){
			bucket2 = "NoCountry";
		}
		Map<String, String> parameterMap = new LinkedHashMap<String, String>();
		List<RefMaster> list = getHibernateTemplate().find("from RefMaster where bucket2 like '"+bucket2+"%' order by description");
		for(RefMaster refMaster : list){
			parameterMap.put(refMaster.getCode(), refMaster.getDescription());
			}
		return parameterMap;
	}
	public List findDriverList(String bucket2, String corpId)
	{
		return this.getSession().createSQLQuery("select userName from crew where warehouse='"+bucket2+"' and corpID='"+corpId+"'").list();
	}
	
	public List  findOwnerOperatorList(String parentId,String corpId){
		if(parentId!=null){
			parentId = parentId.replaceAll("'", "''");
		}
		return getHibernateTemplate().find("from ControlExpirations where parentId='"+parentId+"' and corpID='"+corpId+"' ");
	}
	public List  findTruckDriverList(String partnerCode,String truckNumber,String corpId){
		return getHibernateTemplate().find("from TruckDrivers where corpID='"+corpId+"' and truckNumber=?" ,truckNumber);
	}
	public List  findTruckDescriptionType(String truckType, String corpId){
		return getHibernateTemplate().find("select description from RefMaster where code='"+truckType+"' and parameter='TRUCK_TYPE'");
	}
	public List  findCarrierListInDomestic(String truckNumber, String corpId){
		return this.getSession().createSQLQuery("select concat(description,'~',if(agency is not null and agency<>'',agency,'1')) from truck where localNumber='"+truckNumber+"' and corpID='"+corpId+"'  and truckStatus ='A' UNION select concat(lastName,'~','1') from partnerpublic where isCarrier is true and status='Approved' and surface is true and partnerCode='"+truckNumber+"' and corpID  IN ('"+corpId+"','TSFT','"+hibernateUtil.getParentCorpID(corpId)+"') ").list();
	}
	public List  findTrailerListInDomestic(String truckNumber, String corpId){
		return this.getSession().createSQLQuery("select * from truck where localNumber='"+truckNumber+"' and type in('TT','TRL') and corpID='"+corpId+"'  and truckStatus ='A' ").list();
	}
	public List findControlExpirationsListInDomestic(String parentId,Date deliveryLastDay,String corpId){
		if(deliveryLastDay!=null){
		Date date1Str = deliveryLastDay;
		SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );		
		return this.getSession().createSQLQuery("select concat(if(required is true,'Yes',1) ,'#',fieldName,'#',CAST(DATE_FORMAT(expiryDate,'%d-%b-%Y') AS CHAR)) from controlexpirations where parentId = '"+parentId+"'  and corpID='"+corpId+"' and DATEDIFF(expiryDate,'"+formatedDate1+"')<=0 ").list();
	}
		else{
			return this.getSession().createSQLQuery("select concat(if(required is true,'Yes',1) ,'#',fieldName,'#',CAST(DATE_FORMAT(expiryDate,'%d-%b-%Y') AS CHAR)) from controlexpirations where parentId = '"+parentId+"'  and corpID='"+corpId+"' and DATEDIFF(expiryDate,now())<=0 ").list();
		}
	}
	
	public List findControlExpirationsListForDriver(String parentId,Date deliveryLastDay,String corpId){
		if(deliveryLastDay!=null){
			Date date1Str = deliveryLastDay;
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder formatedDate1 = new StringBuilder( dateformatYYYYMMDD1.format( date1Str ) );		
			return this.getSession().createSQLQuery("select concat(if(required is true,'Yes',1) ,'#',fieldName,'#',CAST(DATE_FORMAT(expiryDate,'%d-%b-%Y') AS CHAR)) from controlexpirations where parentId = '"+parentId+"' and type='Driver' and corpID='"+corpId+"' and DATEDIFF(expiryDate,'"+formatedDate1+"')<=0 ").list();
		}
			else{
				return this.getSession().createSQLQuery("select concat(if(required is true,'Yes',1) ,'#',fieldName,'#',CAST(DATE_FORMAT(expiryDate,'%d-%b-%Y') AS CHAR)) from controlexpirations where parentId = '"+parentId+"' and type='Driver' and corpID='"+corpId+"' and DATEDIFF(expiryDate,now())<=0 ").list();
			}
	}
	public List findTruckTrailer(String corpId){
		return getHibernateTemplate().find("from Truck where type in('TT','TRL')   and truckStatus ='A' and corpID='"+corpId+"'");
	}
	public List searchTruckTrailer(String warehouse, String localNum, String desc, String tagNum, String corpId, String type,String state,String ownerPayTo)
	{
		return getHibernateTemplate().find("from Truck where warehouse like '%" + warehouse + "%' AND localNumber like '%" + localNum + "%' AND description like '%" + desc + "%' AND tagNumber like '%" + tagNum + "%' AND corpID = '" + corpId + "' and type like '%"+type+"%' and state like'%"+state+"%' and ownerPayTo like'%"+ownerPayTo+"%' and type in('TT','TRL')   and truckStatus ='A' ");
	}
	 public List findAgentVanName(String truckVanLineCode,String corpId){
		 return this.getSession().createSQLQuery("select p.lastname from partnerpublic p,partnervanlineref pv where p.partnerCode = pv.partnerCode AND pv.vanLineCode = '"+truckVanLineCode+"' AND pv.corpID in ('TSFT', '" + corpId + "') ").list();
	 }
	 public List  findVanListInDomestic(String truckNumber, String corpId){
		 truckNumber=truckNumber.replaceAll("'", "");
			return this.getSession().createSQLQuery("select concat(description,'~',if(agency is not null and agency<>'',agency,'1')) from truck where localNumber='"+truckNumber+"' and corpID='"+corpId+"'  and truckStatus ='A' ").list();
		}
	 public List isExisted(String truckNo, String sessionCorpID){		
			return this.getSession().createSQLQuery("select localNumber from truck where localNumber = '"+truckNo+"'  AND corpID = '"+sessionCorpID+"' and localNumber is not null and localNumber<>'' ").list();
		}
}
