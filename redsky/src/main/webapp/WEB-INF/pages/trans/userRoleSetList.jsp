<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>   
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="userListSecuritySet.title"/></title>   
    <meta name="heading" content="<fmt:message key='userListSecuritySet.heading'/>"/> 
    <style type="text/css"> 
.table tfoot td {
border:1px solid #E0E0E0;
}
.table2 thead th, table2HeaderTable td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0 0;
border-color:#99BBE8 #99BBE8 #99BBE8 -moz-use-text-color;
border-style:solid;
border-right:medium;
color:#99BBE8;
font-family:arial,verdana;
font-size:11px;
font-weight:bold;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

.table tfoot th, tfoot td {
background:#BCD2EF url(/redsky/images/collapsebg.gif) repeat scroll 0%;
border-color:#99BBE8 rgb(153, 187, 232) rgb(153, 187, 232) -moz-use-text-color;
border-style:solid solid solid none;
border-width:1px 1px 1px medium;
color:#15428B;
font-family:arial,verdana;
font-size:13px;
font-weight:bold;
font-color:#003366;
height:25px;
padding:2px 3px 3px 5px;
text-decoration:none;
}

</style> 
 
</head>
<c:if test="${userRoleSet!='[]'}"> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left" style="padding-left:5px;font-size:11px;"><b>Roles</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<s:form id="userRoleSet" action="" method="post">  
<s:set name="userRoleSet" value="userRoleSet" scope="request"/> 
<display:table name="userRoleSet" class="table" requestURI="" id="userRoleSet" style="width:200px" defaultsort="1" pagesize="100" >   
 		 <display:column property="roles" headerClass="headerColorInv" title="Roles" style="width:40px" />
 		 
</display:table>  
</s:form>
</c:if> 
<c:if test="${userRoleSet=='[]'}">
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tr valign="top"> 	
	<td align="left" style="padding-left:5px;font-size:11px;"><b>No Role assigned</b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
</c:if>
		  	