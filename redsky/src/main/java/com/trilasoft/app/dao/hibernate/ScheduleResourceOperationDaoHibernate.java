package com.trilasoft.app.dao.hibernate;

import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.ScheduleResourceOperationDao;
import com.trilasoft.app.model.ScheduleResourceOperation;

public class ScheduleResourceOperationDaoHibernate extends GenericDaoHibernate<ScheduleResourceOperation,Long> implements ScheduleResourceOperationDao {
	public ScheduleResourceOperationDaoHibernate() {
		super(ScheduleResourceOperation.class);
	}

	public List findTimeSheet(ScheduleResourceOperation scheduleResourceOperation) {
		String query="from TimeSheet where crewName like '%"+scheduleResourceOperation.getCrewName()+"%' " +
		"and workDate='"+scheduleResourceOperation.getWorkDate()+"' " +
		"and ticket='"+scheduleResourceOperation.getTicket()+"' and corpID='"+scheduleResourceOperation.getCorpID()+"'";
		return getHibernateTemplate().find(query); 
	}

	public List findTruckingOperation(ScheduleResourceOperation scheduleResourceOperation) {
		return getHibernateTemplate().find("from TruckingOperations where  " +
				"workDate='"+scheduleResourceOperation.getWorkDate()+"' " +
				"and ticket='"+scheduleResourceOperation.getTicket()+"' " +
				"and corpID='"+scheduleResourceOperation.getCorpID()+"' and localTruckNumber='"+scheduleResourceOperation.getLocaltruckNumber()+"'");
	}
	
}
