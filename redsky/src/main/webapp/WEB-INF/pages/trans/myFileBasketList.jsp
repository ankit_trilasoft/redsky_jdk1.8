<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
<title><fmt:message key="myFileList.title"/></title>   
<meta name="heading" content="<fmt:message key='myFileList.heading'/>"/>   
<c:if test="${param.popup}"> 
    <link rel="stylesheet" type="text/css" media="all" href="<c:url value='/styles/${appConfig["csstheme"]}/layout-popup.css'/>" /> 
</c:if>

<script>
function confirmSubmit(targetElement){
	var agree=confirm("Are you sure you wish to recover this file?");
	var cid = document.forms['myFileForm'].elements['customerFile.id'].value;
	var sid = document.forms['myFileForm'].elements['serviceOrder.id'].value;
	var pid = document.forms['myFileForm'].elements['partner.id'].value;
	var ppType = document.forms['myFileForm'].elements['ppType'].value;
	var tid = document.forms['myFileForm'].elements['truck.id'].value;
	var did = targetElement;
	if (agree){
		if(tid !='')
		{
		location.href="revocerDoc.html?id="+tid+"&did="+did+"&myFileFor=Truck&myFileFrom=Truck&relatedDocs=No&active=false&ppType="+ppType+"&forQuotation=${forQuotation}&PPID=${PPID}";
		}else{
	if(pid !='')
	{
	location.href="revocerDoc.html?id="+pid+"&did="+did+"&myFileFor=PO&myFileFrom=PO&relatedDocs=No&active=false&ppType="+ppType+"&forQuotation=${forQuotation}&PPID=${PPID}";
	}else{
		if(sid == ''){
			location.href="revocerDoc.html?id="+cid+"&did="+did+"&myFileFor=CF&myFileFrom=CF&relatedDocs=No&active=false&forQuotation=${forQuotation}&PPID=${PPID}";
		}else{
			location.href="revocerDoc.html?id="+sid+"&did="+did+"&myFileFor=SO&myFileFrom=SO&relatedDocs=No&active=false&forQuotation=${forQuotation}&PPID=${PPID}";
		}
		}
	}
	}else{
		return false;
	}
}

function generatePortalId() {
		var daReferrer = document.referrer; 
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to e-mail.');
		}else{
			window.open('attachMail.html?checkBoxId='+checkBoxId+'&decorator=popup&popup=true&from=file','','width=650,height=170');
		}
} 

function checkStatusId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse1;
        http22.send(null);		
} 

function handleHttpResponse1(){
      if (http22.readyState == 4){
           var result= http22.responseText         
      }
}	

function checkStatusAccId(rowId, targetElement) {
		var Status = targetElement.checked;
		var url="updateMyfileAccStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse2;
        http22.send(null);
} 
function handleHttpResponse2(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}

function checkStatusPartnerId(rowId, targetElement){
		var Status = targetElement.checked;
		var url="updateMyfilePartnerStatus.html?ajax=1&decorator=simple&popup=true&Status=" + encodeURI(Status)+"&ids=" + encodeURI(rowId);
		http22.open("GET", url, true);
        http22.onreadystatechange = handleHttpResponse3;
        http22.send(null);
}
function handleHttpResponse3(){
     if (http22.readyState == 4){
           var result= http22.responseText         
     }
}	


var http22 = getHTTPObject22();

function getHTTPObject22(){
    var xmlhttp;
    if(window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }else if (window.ActiveXObject){
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp){
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}

// 1 visible, 0 hidden   
function userStatusCheck(targetElement){
    	if(targetElement.checked){
      		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
      		if(userCheckStatus == ''){
	  			document.forms['myFileForm'].elements['userCheck'].value = targetElement.value;
      		}else{
      			var userCheckStatus=	document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus + ',' + targetElement.value;
      			document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
      		}
    	}
  	 	if(targetElement.checked==false){
     		var userCheckStatus = document.forms['myFileForm'].elements['userCheck'].value;
     		var userCheckStatus=document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( targetElement.value , '' );
     		document.forms['myFileForm'].elements['userCheck'].value = userCheckStatus.replace( ',,' , ',' );
   		}
}

function downloadDoc(){
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var partnerCode = document.forms['myFileForm'].elements['fileNameFor'].value;
		if(partnerCode=='Truck'){
			var seqNo = document.forms['myFileForm'].elements['truck.localNumber'].value;
			}else{
		if(partnerCode=='PO'){
		var seqNo = document.forms['myFileForm'].elements['partner.partnerCode'].value;
		}else{
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		}}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
		
} 
<%--
function downloadDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		var seqNo = document.forms['myFileForm'].elements['customerFile.sequenceNumber'].value;
		if(seqNo == '' ){
			seqNo = document.forms['myFileForm'].elements['serviceOrder.sequenceNumber'].value;
		}
		if(checkBoxId =='' || checkBoxId ==','){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=DWNLD&seqNo='+seqNo;
			location.href=url;
		}
}
--%>
function emailDoc() {
		var checkBoxId = document.forms['myFileForm'].elements['userCheck'].value;
		
		if(checkBoxId ==''){
			alert('Please select the one or more document to download.');
		}else{
			var url = 'ImageServletAction.html?id='+checkBoxId+'&param=EMAIL';
			location.href=url;
		}
}

function checkAll(){
	document.forms['myFileForm'].elements['userCheck'].value = "";
	document.forms['myFileForm'].elements['userCheck'].value = "";
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = true ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
}

function uncheckAll(){
	var len = document.forms['myFileForm'].elements['DD'].length;
	for (i = 0; i < len; i++){
		document.forms['myFileForm'].elements['DD'][i].checked = false ;
		userStatusCheck(document.forms['myFileForm'].elements['DD'][i]);
	}
	document.forms['myFileForm'].elements['userCheck'].value="";
}
function show(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'block';
     }
}
function hide(theTable){
     if (document.getElementById(theTable).style.display == 'none'){
          document.getElementById(theTable).style.display = 'none';
     }else{
          document.getElementById(theTable).style.display = 'none';
     }
}
</script>

   
<style>
   
#mainPopup {
padding-left:10px;
padding-right:10px;
}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:100%;
}
</style>
</head>


<s:form id="myFileForm" action="searchMyFiles" method="post" >  
<c:set var="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>
<s:hidden name="fileForId" value="<%=request.getParameter("fileForId") %>"/>

<s:hidden name="fileId" value="<%=request.getParameter("id") %>" />
<c:set var="fileId" value="<%=request.getParameter("id") %>"/>
<s:hidden name="ppType" id ="ppType" value="<%= request.getParameter("ppType")%>" />
<c:set var="ppType" value="<%= request.getParameter("ppType")%>"/>
<s:hidden name="fileNameFor"  value="<%=request.getParameter("myFileFor") %>" />
<c:set var="fileNameFor" value="<%=request.getParameter("myFileFor") %>" />
<s:hidden name="PPID" id="PPID" value="<%=request.getParameter("PPID") %>" />
<c:set var="PPID" value="<%=request.getParameter("PPID") %>"/>
<s:hidden name="userCheck"/> 
<s:hidden name="customerFile.id" />
<s:hidden name="workTicket.id" />
<s:hidden name="workTicket.ticket" />
<s:hidden name="partner.id" />
<s:hidden name="truck.id" /><s:hidden name="truck.localNumber" />
<s:hidden name="partner.partnerCode" />
<s:hidden name="serviceOrder.id" />
<s:hidden name="serviceOrder.shipNumber" />
<s:hidden name="serviceOrder.sequenceNumber" />
<s:hidden name="serviceOrder.ship" />
<s:hidden id="forQuotation" name="forQuotation" />
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<c:set var="salesPortalAccess" value="false" />
<s:hidden name="myFileFromVal" value="<%=request.getParameter("myFileFrom")%>" />
<s:hidden name="noteForVal" value="<%=request.getParameter("noteFor")%>" />
<s:hidden name="activeVal" value="<%=request.getParameter("active")%>" />
<s:hidden name="relatedDocsVal" value="<%=request.getParameter("relatedDocs")%>" />
<sec-auth:authComponent componentId="module.script.form.corpSalesScript">
<c:set var="salesPortalAccess" value="true" />
</sec-auth:authComponent>
<configByCorp:fieldVisibility componentId="component.field.forwardingTabAjax">		
	<c:set var="forwardingTabVal" value="Y" />
</configByCorp:fieldVisibility>
<configByCorp:fieldVisibility componentId="component.field.soQuickView">		
		<c:set var="soQuickView" value="Y" />
</configByCorp:fieldVisibility>
<s:hidden name="noteFor" value="<%=request.getParameter("noteFor") %>" />
<c:set var="noteFor" value="<%=request.getParameter("noteFor") %>"/>
<c:if test="${myFileFor=='CF'}">
	<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
	<c:set var="noteID" value="${TempnotesId}" scope="session"/>
	<c:set var="custID" value="" scope="session"/>
	<c:set var="noteFor" value="${noteFor}" scope="session"/>
	<c:if test="${empty customerFile.id}">
		<c:set var="isTrue" value="false" scope="request"/>
	</c:if>
	<c:if test="${not empty customerFile.id}">
		<c:set var="isTrue" value="true" scope="request"/>
	</c:if>
</c:if>

<c:if test="${myFileFor!='CF'}"> 
<c:set var="idOfWhom" value="<%=request.getParameter("id") %>" scope="session"/>
<c:set var="noteID" value="${TempnotesId}" scope="session"/>
<c:set var="noteFor" value="${noteFor}" scope="session"/>
<c:if test="${empty serviceOrder.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty serviceOrder.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>	
<s:hidden name="customerFile.sequenceNumber" />
<c:if test="${myFileFor!='PO'  && myFileFor!='Truck'}">
<div id="layer6" style="width:100%; ">
<div id="newmnav" style="float:left; ">
            <c:choose>
	        <c:when test="${forQuotation!='QC'}">
            <ul>
               <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
				<s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
				<c:set var="relocationServicesKey" value="" />
				<c:set var="relocationServicesValue" value="" /> 
			    <c:forEach var="entry" items="${relocationServices}">
					<c:if test="${relocationServicesKey==''}">
					<c:if test="${entry.key==serviceOrder.serviceType}">
					<c:set var="relocationServicesKey" value="${entry.key}" />
					<c:set var="relocationServicesValue" value="${entry.value}" /> 
					</c:if>
					</c:if> 
               </c:forEach>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.serviceorderTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>S/O Details</span></a></li>
	            </c:if>
	            <c:if test="${not empty serviceOrder.moveType && serviceOrder.moveType=='Quote'}">
	            	<li><a href="editServiceOrderUpdate.html?id=${serviceOrder.id}"><span>Quotes</span></a></li>
	            </c:if>
			    </sec-auth:authComponent>
	            
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.billingTab">
		             <sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >	
		             	<li><a href="editBilling.html?id=${serviceOrder.id}"><span>Billing</span></a></li>
		             </sec-auth:authComponent>
	            </sec-auth:authComponent>
	            <sec-auth:authComponent componentId="module.tab.trackingStatus.accountingTab">
		             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose>
					    <%-- <c:when test='${serviceOrder.status == "CNCL" || serviceOrder.status == "DWND" || serviceOrder.status == "DWNLD"}'>
					      	<li><a onclick="javascript:alert('You cannot access Accounting as the service order is canceled or not accepted.')"><span>Accounting</span></a></li>	
					    </c:when> --%>
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="accountLineList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			     <sec-auth:authComponent componentId="module.tab.serviceorder.newAccountingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
		             <c:choose> 
					    <c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 					<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
						</c:when>
					    <c:otherwise> 
					    	<li><a href="pricingList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
				        </c:otherwise>
				     </c:choose>
				     </c:if> 
			     </sec-auth:authComponent>
			        <sec-auth:authComponent componentId="module.tab.serviceorder.accountingPortalTab">	
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	              <li><a href="accountLineSalesPortalList.html?sid=${serviceOrder.id}"><span>Accounting</span></a></li>
	              </c:if>
	              </sec-auth:authComponent>
			   
			  <%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
 	 <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	         	 <li><a href="operationResource.html?id=${serviceOrder.id}"><span>O&I</span></a></li>
	         	 </sec-auth:authComponent>
	          	 </c:if>
			     <sec-auth:authComponent componentId="module.tab.trackingStatus.forwardingTab">
			     <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
			     <c:if test="${serviceOrder.job !='RLO'}"> 
			     		<c:if test="${forwardingTabVal!='Y'}"> 
	   						<li><a href="containers.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
	  					<c:if test="${forwardingTabVal=='Y'}">
	  						<li><a href="containersAjaxList.html?id=${serviceOrder.id}"><span>Forwarding</span></a></li>
	  					</c:if>
			     </c:if>
			     </c:if>	
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.domesticTab">
		             <c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		             <c:if test="${serviceOrder.job !='RLO'}"> 
		             	<li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
		             </c:if>	
		             </c:if>
	             </sec-auth:authComponent>
	             <sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
                 <c:if test="${serviceOrder.job =='INT'}">
                   <li><a href="editMiscellaneous.html?id=${serviceOrder.id}"><span>Domestic</span></a></li>
                 </c:if>
                 </sec-auth:authComponent>
	             <c:if test="${serviceOrder.job =='RLO'}">  
                  <li><a href="editDspDetails.html?id=${serviceOrder.id}"><span>Status</span></a></li>
                 </c:if>
                 <c:if test="${serviceOrder.job !='RLO'}"> 
	             <li><a href="editTrackingStatus.html?id=${serviceOrder.id}"><span>Status</span></a></li>
	            </c:if>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.ticketTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="customerWorkTickets.html?id=${serviceOrder.id}"><span>Ticket</span></a></li>
	             </c:if>
	             </c:if>	
	             </sec-auth:authComponent>
	              <configByCorp:fieldVisibility componentId="component.standard.claimTab">
	             <sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	             <c:if test="${empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove'}">
	             <c:if test="${serviceOrder.job !='RLO'}"> 
	             	<li><a href="claims.html?id=${serviceOrder.id}"><span>Claims</span></a></li>
	             </c:if>
	             </c:if>	
	             </sec-auth:authComponent>
	             </configByCorp:fieldVisibility>
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.customerfileTab">
	             	<li><a href="editCustomerFile.html?id=${customerFile.id}"><span>Customer File</span></a></li>
	             </sec-auth:authComponent>
	             
	             <sec-auth:authComponent componentId="module.tab.trackingStatus.costingTab">
	           	 	<li><a href="costingDetail.html?sid=${serviceOrder.id}"><span>Costing</span></a></li>
	           	 </sec-auth:authComponent>
	       </ul>
	       </c:when>
	       <c:otherwise>
		   <ul>
		    <li ><a href="QuotationFileForm.html?id=${serviceOrder.customerFileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${serviceOrder.customerFileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li>  	
		   </ul>
		</c:otherwise></c:choose>
</div>
<table cellpadding="0" cellspacing="0" style="margin:0px; padding:0px;float: none;">
 		<tr>
		<c:if test="${not empty serviceOrder.id}">
		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="vertical-align: text-bottom; padding-left: 5px; padding-top: 4px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400);" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>
		</c:if></tr></table>
<div class="spn">&nbsp;</div>
<div style="!margin-top:8px; ">
      <%@ include file="/WEB-INF/pages/trans/serviceOrderHeader.jsp"%>
      </c:if>
</div>
</div>
</c:if>
<c:if test="${myFileFor=='CF'}"> 
 <div id="Layer5" style="width:95%">	
	<c:choose>
	<c:when test="${forQuotation!='QC'}">
	<div id="newmnav">
		   <ul>
		  <c:if test="${customerFile.controlFlag=='A'}">
		    <li><a href="editOrderManagement.html?id=${customerFile.id}" ><span>Order Detail</span></a></li>
		    </c:if> 
		    <c:if test="${customerFile.controlFlag!='A'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
		     <c:if test="${empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Service Orders</span></a></li>
		    </c:if>
		    <c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
		    <li><a href="customerServiceOrders.html?id=${customerFile.id}" ><span>Quotes</span></a></li>
		    </c:if>
		    <c:if test="${salesPortalAccess=='false'}"> 
		    <li><a href="customerRateOrders.html?id=${customerFile.id}"><span>Rate Request</span></a></li>
		    <!-- <li><a href="surveysList.html?id=${customerFile.id} "><span>Surveys</span></a></li> -->
		    <li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li> 
		  	<li><a onclick="window.open('subModuleReports.html?id=${customerFile.id}&custID=${customerFile.sequenceNumber}&jobNumber=${customerFile.sequenceNumber}&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>  
		    </c:if>
		    </c:if> 
          </ul>
		</div>
	 </c:when>
	 <c:otherwise>
		<div id="newmnav">
            <ul>
		    <li ><a href="QuotationFileForm.html?id=${fileId}&forQuotation=QC" ><span>Quotation File</span></a></li>
		    <li ><a href="quotationServiceOrders.html?id=${fileId}&forQuotation=QC"><span>Quotes</span></a></li>
		    <li><a><span>Forms</span></a></li>  
		    <li><a><span>Audit</span></a></li> 		  	
		  </ul>
		</div></c:otherwise></c:choose><div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
 <div id="content" align="center" >
<div id="liquid-round">
    <div class="top" style="!margin-top:3px;"><span></span></div>
   <div class="center-content">
<table class=""  cellspacing="1" cellpadding="0"	border="0" style="width:90%">
	<tbody>
		<tr>
			<td>
				<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
					<tbody>
					<tr>
						<td align="right" class="listwhitebox">Cust#</td>
						<td><s:textfield name="customerFile.sequenceNumber" size="21" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Shipper</td>
						<td><s:textfield name="customerFile.firstName" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.lastName" required="true" size="18" readonly="true" cssClass="input-textUpper"/></td>
						<td align="right" class="listwhitebox">Origin</td>
						<td><s:textfield name="customerFile.originCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td ><s:textfield name="customerFile.originCountryCode" required="true" size="13" readonly="true" cssClass="input-textUpper"/></td>
					</tr>
					<tr>
						<td align="right" class="listwhitebox">Type</td>
						<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitebox">Destination</td>
						<td><s:textfield name="customerFile.destinationCityCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
						<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="18" readonly="true" cssClass="input-textUpper" /></td>
						<td align="left" class="listwhitebox"><fmt:message key='customerFile.billToCode'/></td>
						<td colspan="2"><s:textfield name="customerFile.billToName" required="true" size="35" readonly="true" cssClass="input-textUpper" /></td>
					</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
</c:if>  
<div id="Layer1" style="width:100%;">
<div id="Layer4" style="width:100%;">
<div id="fc-newmnav">
				  <ul>
				        <c:if test="${myFileFor!='PO' && myFileFor!='Truck'}">
				        <li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li><a href="relatedFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&noteFor=${noteFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Related Docs</span></a></li>
				    	<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="basketFiles.html?id=${fileId}&noteFor=${noteFor}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Waste Basket</span></a></li>
				    	<sec-auth:authComponent componentId="module.tab.myfile.securedocTab">
				    	<li><a href="secureFiles.html?id=${fileId}&myFileFor=${fileNameFor}&noteFor=${noteFor}&active=true&secure=true&forQuotation=${forQuotation}"><span>Secure List</span></a></li>
				    	</sec-auth:authComponent>
				    	<li><a href="checkListFiles.html?id=${fileId}&myFileFrom=${fileNameFor}&myFileFor=${fileNameFor}&noteFor=${noteFor}&relatedDocs=No&active=false&forQuotation=${forQuotation}"><span>Check List</span></a></li>
				    	
				    	<%-- <li><a href="myFilesDocType.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&secure=false&forQuotation=${forQuotation}"><span>Document Centre<img src="images/navarrow.gif" align="absmiddle" /></span></a></li> --%>
				    	</c:if>
				    	<c:if test="${myFileFor=='Truck'}">
				    	<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&ppType=${ppType}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}"><span>Waste Basket</span></a></li>
				        <li><a href="editTruck.html?id=${fileId}" ><span>Truck Details</span></a></li>
					    </c:if>
				    	<c:if test="${myFileFor=='PO'}">
				    	<li><a href="myFiles.html?id=${fileId}&myFileFor=${fileNameFor}&active=true&ppType=${ppType}&PPID=${PPID}"><span>Document List<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
				    	<li id="fc-newmnav1" style="background:#FFF "><a class="current" href="basketFiles.html?id=${fileId}&myFileFor=${fileNameFor}&relatedDocs=No&active=false&ppType=${ppType}&PPID=${PPID}"><span>Waste Basket</span></a></li>
				        <c:if test="${!param.popup && ppType=='AG'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Agent Detail</span></a></li>
					    </c:if>
				        <c:if test="${!param.popup && ppType=='AC'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Account Detail</span></a></li>
					    </c:if>
					    <c:if test="${!param.popup && ppType=='PP'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Private Party Detail</span></a></li>
					    </c:if>
					    <c:if test="${!param.popup && ppType=='CR'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Carrier Detail</span></a></li>
					</c:if>
					<c:if test="${!param.popup && ppType=='VN'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Vendor Detail</span></a></li>
					</c:if>
					<c:if test="${!param.popup && ppType=='OO'}"> 
						<li><a href="editPartnerPublic.html?id=${PPID}&partnerType=${ppType}" ><span>Owner Ops</span></a></li>
					</c:if>
				        </c:if>		
					</ul>
		</div>
		<div id="chkAllButton"  class="listwhitetext" style="display:none; float: left; !position: absolute; !margin-left: 450px; !margin-top: -25px;" >
								<input type="radio"  name="chk" onClick="checkAll()" /><strong>Check All</strong>
								<input type="radio"  name="chk" onClick="uncheckAll()"  /><strong>Uncheck All</strong>
							</div>
		<div class="spn">&nbsp;</div>
		 <div style="padding-bottom:0px;"></div>
		 </div>


<table class="" cellspacing="0" cellpadding="0" border="0" style="width:1100px;!padding-top:3px;">
	<tbody>
		<tr>
			<td>
				<s:set name="myFiles" value="myFiles" scope="request"/>  
				<display:table name="myFiles" class="table-fc" requestURI="" id="myFileList" export="true" pagesize="25" style="width:100%;" defaultsort="${fieldName}"  defaultorder="${sortOrder}" >   
				    <display:column title=" " style="width: 15px;"><input type="checkbox" style="margin-left:10px;" id="checkboxId" name="DD" value="${myFileList.id}" onclick="userStatusCheck(this)"/></display:column>
				    <display:column property="fileType" sortable="true" title="Document&nbsp;Type" style="width:75px;"/>
				    <display:column  sortable="true" sortProperty="description" maxLength="40" title="Description" style="width:60px;">
						<a onclick="downloadSelectedFile('${myFileList.id}');">
						<c:out value="${myFileList.description}" escapeXml="false"/></a>
						</display:column>	
				    <display:column property="fileSize" sortable="true" title="Size" style="width:60px;"></display:column>				    
				    <c:if test="${fn1:indexOf(transDocSysDefault,transDocJobType)>=0}">
					         <display:column title="TransDoc&nbsp;Status" sortable="true" sortProperty="transDocStatus">
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOADED'}">
					    	Received&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='READY_TO_UPLOAD'}">
					    	Ready&nbsp;to&nbsp;transfer&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />				    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOADED'}">
					    	Sent&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='UPLOAD_FAILED'}">
					    	Send&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if>
					    	<c:if test="${myFileList.transDocStatus!=null && myFileList.transDocStatus=='DOWNLOAD_FAILED'}">
					    	Receive&nbsp;Failed&nbsp;by&nbsp;<c:out value="${myFileList.transferredBy} on "> </c:out><fmt:formatDate pattern="dd MMM yyyy" value="${myFileList.docSent}" />					    	
					    	</c:if></display:column>
					   	 	</c:if>
					<display:column property="createdOn" sortable="true" title="Uploaded&nbsp;On" format="{0,date,dd-MMM-yyyy}" style="width:80px;"/>   	 	
				    <display:column property="createdBy" sortable="true" title="Uploaded&nbsp;By" style="width:70px;"/>
				    
					   	 	
				    <display:column title="Cust Portal" style="width:25px;">
				    <c:if test="${myFileList.isCportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isCportal == false || myFileList.isCportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Acc Portal" style="width:25px;">
				    <c:if test="${myFileList.isAccportal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isAccportal == false || myFileList.isAccportal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusAccId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    
				    <display:column title="Partner Portal" style="width:25px;">
				    <c:if test="${myFileList.isPartnerPortal == true}">
				    <input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" checked disabled="disabled"/>
				    </c:if>
					<c:if test="${myFileList.isPartnerPortal == false || myFileList.isPartnerPortal == null}">
					<input type="checkbox" style="margin-left:10px;" id="checkboxId" value="${myFileList.id}" onclick="checkStatusPartnerId(${myFileList.id},this)" disabled="disabled"/>
					</c:if>
				    </display:column>
				    <display:column title="Recover" style="width:45px;">
				    	<a><img align="middle" onclick="confirmSubmit(${myFileList.id});" title="Recover" style="margin: 0px 0px 0px 8px;" src="images/recover.png"/></a>
				    </display:column>
				     
				    
				    <display:setProperty name="paging.banner.item_name" value="document"/>   
				    <display:setProperty name="paging.banner.items_name" value="documents"/>   
				  
				    <display:setProperty name="export.excel.filename" value="Document List.xls"/>   
				    <display:setProperty name="export.csv.filename" value="Document List.csv"/>   
				    <display:setProperty name="export.pdf.filename" value="Document List.pdf"/>   
				</display:table>  
  
  
			</td>
		</tr>
	</tbody>
</table>
</div>  
<c:out value="${buttons}" escapeXml="false" />
<%-- <input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:65px; font-size: 15" value="Email" onclick="emailDoc();"/> --%>
<input type="button" class="cssbutton" style="margin-right: 5px;height: 28px;width:70px; font-size: 15" name="dwnldBtn"  value="Download" onclick="downloadDoc();"/>
</s:form>

<script type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-2.0.3.min.js"></script>

<script>
$(document).ready(function(){
$("#Layer1 div.exportlinks").addClass('CFEXP_left');
$("#Layer1 div.exportlinks").after('<div class="CFEXP_right"></div>');
$("#Layer1 div.exportlinks a").first().css('margin-left','65px');
$("#Layer1 div.exportlinks").css('padding-top','7px'); 
$("#Layer1 div.exportlinks").css('text-transform','uppercase');
$("#Layer1 div.exportlinks a").css('text-transform','capitalize'); 
})
</script>

<script type="text/javascript"> 
   try{
    var len = document.forms['myFileForm'].elements['DD'].length;
   }
   catch(e){}
    try{
    if(len>1){
    	show('chkAllButton');
    }
    }
    catch(e){}
    function downloadSelectedFile(id){
    	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
    	var fieldVal = document.forms['myFileForm'].elements['myFileFromVal'].value;
    	var fieldVal1 = document.forms['myFileForm'].elements['noteForVal'].value;
    	var fieldVal2 = document.forms['myFileForm'].elements['activeVal'].value;
    	var fieldVal3 = document.forms['myFileForm'].elements['relatedDocsVal'].value;
    	var fileNameFor = document.forms['myFileForm'].elements['fileNameFor'].value;
    	var myFileJspName = "basketFiles";
    	if(fileNameFor=='SO'){
    		var url="ImageServletAction.html?id="+id+"&myFileForVal="+fileNameFor+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&relatedDocsVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileFromVal="+fieldVal+"&myFileJspName="+myFileJspName+"";
    	}else if(fieldVal=='CF'){
    		var url="ImageServletAction.html?id="+id+"&myFileForVal="+fileNameFor+"&noteForVal="+fieldVal1+"&activeVal="+fieldVal2+"&relatedDocsVal="+fieldVal3+"&fileIdVal="+fileIdVal+"&myFileFromVal="+fieldVal+"&myFileJspName="+myFileJspName+"";
    	}else if(fileNameFor=='PO'){
    		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
    		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
    		url="ImageServletAction.html?id="+id+"&myFileForVal="+fileNameFor+"&relatedDocsVal="+fieldVal3+"&activeVal="+fieldVal2+"&fileIdVal="+fileIdVal+"&myFileJspName="+myFileJspName+"&ppTypeVal="+ppTypeVal+"&ppIdVal="+ppIdVal+"";
    	}
    	location.href=url;
    }
    var fileVal = '${resultType}';
    if ((fileVal!=null && fileVal!='') && (fileVal=='errorNoFile')){
    	alert('This File is Temporarily Unavailable.')
    	var replaceURL = "";
    	var myFileFromVal =  document.forms['myFileForm'].elements['myFileFromVal'].value;
    	var noteForVal = document.forms['myFileForm'].elements['noteForVal'].value;
    	var activeVal = document.forms['myFileForm'].elements['activeVal'].value;
    	var relatedDocsVal = document.forms['myFileForm'].elements['relatedDocsVal'].value;
    	var fileIdVal = document.forms['myFileForm'].elements['fileId'].value;
    	var myFileFor = document.forms['myFileForm'].elements['fileNameFor'].value;
    	if(myFileFromVal=='SO' || myFileFromVal=='CF'){
    		replaceURL = "basketFiles.html?id="+fileIdVal+"&myFileFrom="+myFileFromVal+"&myFileFor="+myFileFor+"&noteFor="+noteForVal+"&active="+activeVal+"&relatedDocs="+relatedDocsVal+"";
    	}else if(myFileFor=='PO'){
    		var ppTypeVal = document.forms['myFileForm'].elements['ppType'].value;
    		var ppIdVal = document.forms['myFileForm'].elements['PPID'].value;
    		replaceURL ="basketFiles.html?id="+fileIdVal+"&myFileFor="+myFileFor+"&relatedDocs="+relatedDocsVal+"&active="+activeVal+"&ppType="+ppTypeVal+"&PPID="+ppIdVal+"";
    	}
    	window.location.replace(replaceURL);
    }
</script>
