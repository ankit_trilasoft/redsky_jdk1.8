package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.BrokerDetails;
public interface BrokerDetailsDao extends GenericDao<BrokerDetails, Long> {
	public List getByShipNumber(String shipNumber);

}
