update customerfile set origincountry = '' where createdby ='Ugww' and origincountry is null;
update customerfile set destinationcountry = '' where createdby ='Ugww' and destinationcountry is null;
Insert into refmaster(corpid,code,description,fieldlength,parameter,language,createdon,createdby,updatedon,updatedby)
values('TSFT', 'MM Counselor', 'MM Counselor', 20, 'PRULEROLE','en',now(),'rchandra',now(),'rchandra');
ALTER TABLE `redsky`.`company` ADD COLUMN `email` VARCHAR(250) DEFAULT NULL;
ALTER TABLE `redsky`.`company` ADD COLUMN `rate2`DECIMAL(4,2) DEFAULT NULL;
ALTER TABLE `redsky`.`cportalresourcemgmt` ADD COLUMN `childBillToCode` VARCHAR(255) DEFAULT NULL AFTER `jobType`; 
ALTER TABLE `redsky`.`contractpolicy` ADD COLUMN `jobType` VARCHAR(5) DEFAULT NULL AFTER `docSequenceNumber`;

CREATE  TABLE `redsky`.`commissionstructure` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `grossMarginFrom` decimal(19,2) DEFAULT '0.00',
  `grossMarginTo` decimal(19,2) DEFAULT '0.00',
  `salesBa` decimal(19,2) DEFAULT '0.00',
  `backOffice` decimal(19,2) DEFAULT '0.00',
  `forwarding` decimal(19,2) DEFAULT '0.00',
  `contractName` varchar(100),
  `chargeCode` VARCHAR(25),
  `corpId` VARCHAR(10) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  PRIMARY KEY (`id`) );

  update networkdatafields set transactiontype='Create' where modelName='serviceorder' and fieldName='job';
  SELECT * FROM networkdatafields where modelName='customerFile' and fieldName='job';
  delete from networkdatafields where modelName='customerFile' and fieldName='job' and id=991;
  ALTER TABLE `redsky`.`myfile` ADD COLUMN `documentCategory` VARCHAR(35) AFTER `emailStatus`;
    
  
  CREATE  TABLE `redsky`.`salespersoncommission` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `salesBa` decimal(19,2) DEFAULT '0.00',
  `commissionPercentage` decimal(19,2) DEFAULT '0.00',
  `contractName` varchar(100),
  `chargeCode` VARCHAR(25),
  `corpId` VARCHAR(10) NULL ,
  `createdOn` DATETIME NULL ,
  `createdBy` VARCHAR(80) NULL ,
  `updatedOn` DATETIME NULL ,
  `updatedBy` VARCHAR(80) NULL ,
  PRIMARY KEY (`id`) );

  ALTER TABLE `redsky`.`contractpolicy` MODIFY COLUMN `jobType` VARCHAR(65);
  
  