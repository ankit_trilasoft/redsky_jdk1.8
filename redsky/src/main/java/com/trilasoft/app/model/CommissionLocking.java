package com.trilasoft.app.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;


@Entity
@Table(name="commissionlocking")

public class CommissionLocking extends BaseObject{
	private Long id;
	private String commissionLineId;
	private String commissionGiven;
	private String commissionPerson;
	private BigDecimal  commissionAmount = new BigDecimal("0.00");
	private BigDecimal  commissionPercentage = new BigDecimal("0.00");
	private String chargeCode;
	private String commissionAccountLineId;
	private BigDecimal  totalGrossMargin = new BigDecimal("0.00");
	private String createdBy;
	private String updatedBy;
	private Date createdOn;
	private Date updatedOn;
	private BigDecimal  actualRevenue = new BigDecimal("0.00");
	private BigDecimal  actualExpense = new BigDecimal("0.00");
	private String corpId;
	private String shipNumber;
	private String companyDivision;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("commissionLineId", commissionLineId)
				.append("commissionGiven", commissionGiven)
				.append("commissionPerson", commissionPerson)
				.append("commissionAmount", commissionAmount)
				.append("commissionPercentage", commissionPercentage)
				.append("chargeCode", chargeCode)
				.append("commissionAccountLineId", commissionAccountLineId)
				.append("totalGrossMargin", totalGrossMargin)
				.append("createdBy", createdBy).append("updatedBy", updatedBy)
				.append("createdOn", createdOn).append("updatedOn", updatedOn)
				.append("actualRevenue", actualRevenue)
				.append("actualExpense", actualExpense)
				.append("corpId", corpId).append("shipNumber", shipNumber)
				.append("companyDivision", companyDivision).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof CommissionLocking))
			return false;
		CommissionLocking castOther = (CommissionLocking) other;
		return new EqualsBuilder()
				.append(id, castOther.id)
				.append(commissionLineId, castOther.commissionLineId)
				.append(commissionGiven, castOther.commissionGiven)
				.append(commissionPerson, castOther.commissionPerson)
				.append(commissionAmount, castOther.commissionAmount)
				.append(commissionPercentage, castOther.commissionPercentage)
				.append(chargeCode, castOther.chargeCode)
				.append(commissionAccountLineId,
						castOther.commissionAccountLineId)
				.append(totalGrossMargin, castOther.totalGrossMargin)
				.append(createdBy, castOther.createdBy)
				.append(updatedBy, castOther.updatedBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedOn, castOther.updatedOn)
				.append(actualRevenue, castOther.actualRevenue)
				.append(actualExpense, castOther.actualExpense)
				.append(corpId, castOther.corpId)
				.append(shipNumber, castOther.shipNumber)
				.append(companyDivision, castOther.companyDivision).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(commissionLineId)
				.append(commissionGiven).append(commissionPerson)
				.append(commissionAmount).append(commissionPercentage)
				.append(chargeCode).append(commissionAccountLineId)
				.append(totalGrossMargin).append(createdBy).append(updatedBy)
				.append(createdOn).append(updatedOn).append(actualRevenue)
				.append(actualExpense).append(corpId).append(shipNumber)
				.append(companyDivision).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getCommissionLineId() {
		return commissionLineId;
	}
	public void setCommissionLineId(String commissionLineId) {
		this.commissionLineId = commissionLineId;
	}
	@Column
	public String getCommissionGiven() {
		return commissionGiven;
	}
	public void setCommissionGiven(String commissionGiven) {
		this.commissionGiven = commissionGiven;
	}
	@Column
	public String getCommissionPerson() {
		return commissionPerson;
	}
	public void setCommissionPerson(String commissionPerson) {
		this.commissionPerson = commissionPerson;
	}
	@Column
	public BigDecimal getCommissionAmount() {
		return commissionAmount;
	}
	public void setCommissionAmount(BigDecimal commissionAmount) {
		this.commissionAmount = commissionAmount;
	}
	@Column
	public BigDecimal getCommissionPercentage() {
		return commissionPercentage;
	}
	public void setCommissionPercentage(BigDecimal commissionPercentage) {
		this.commissionPercentage = commissionPercentage;
	}
	@Column
	public String getChargeCode() {
		return chargeCode;
	}
	public void setChargeCode(String chargeCode) {
		this.chargeCode = chargeCode;
	}
	@Column
	public String getCommissionAccountLineId() {
		return commissionAccountLineId;
	}
	public void setCommissionAccountLineId(String commissionAccountLineId) {
		this.commissionAccountLineId = commissionAccountLineId;
	}
	@Column
	public BigDecimal getTotalGrossMargin() {
		return totalGrossMargin;
	}
	public void setTotalGrossMargin(BigDecimal totalGrossMargin) {
		this.totalGrossMargin = totalGrossMargin;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public BigDecimal getActualRevenue() {
		return actualRevenue;
	}
	public void setActualRevenue(BigDecimal actualRevenue) {
		this.actualRevenue = actualRevenue;
	}
	@Column
	public BigDecimal getActualExpense() {
		return actualExpense;
	}
	public void setActualExpense(BigDecimal actualExpense) {
		this.actualExpense = actualExpense;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public String getCompanyDivision() {
		return companyDivision;
	}
	public void setCompanyDivision(String companyDivision) {
		this.companyDivision = companyDivision;
	}
	
}