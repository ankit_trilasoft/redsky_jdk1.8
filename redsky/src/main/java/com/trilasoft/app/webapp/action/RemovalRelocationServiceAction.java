package com.trilasoft.app.webapp.action;


import java.util.Date;
import java.util.List;


import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.PartnerPrivate;
import com.trilasoft.app.model.RemovalRelocationService;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.PartnerPrivateManager;
import com.trilasoft.app.service.RemovalRelocationServiceManager;


public class RemovalRelocationServiceAction extends BaseAction{
	private String sessionCorpID;
    private Long id;
    private Long customerFileId;
    private CustomerFileManager customerFileManager;
    private RemovalRelocationServiceManager removalRelocationServiceManager;
    private RemovalRelocationService removalRelocationService;
    private List removalRelocationServices;
    private String submitCheck;
    private String partnerAbbreviation;
    private PartnerPrivate partnerPrivate;
    private PartnerPrivateManager partnerPrivateManager;
    private CustomerFile  customerFile;
	
	public String getPartnerAbbreviation() {
		return partnerAbbreviation;
	}
	public void setPartnerAbbreviation(String partnerAbbreviation) {
		this.partnerAbbreviation = partnerAbbreviation;
	}
	public void setPartnerPrivateManager(PartnerPrivateManager partnerPrivateManager) {
		this.partnerPrivateManager = partnerPrivateManager;
	}


	public PartnerPrivate getPartnerPrivate() {
		return partnerPrivate;
	}


	public void setPartnerPrivate(PartnerPrivate partnerPrivate) {
		this.partnerPrivate = partnerPrivate;
	}
	public RemovalRelocationServiceAction() {
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID=user.getCorpID();
	}	
	@SkipValidation
	public String edit(){
		 		
		   if (customerFileId != null) { 
			   removalRelocationService = removalRelocationServiceManager.get(customerFileId);
			   customerFile=customerFileManager.get(customerFileId);
			   try{
					partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, customerFile.getParentAgent()).get(0);
					partnerAbbreviation=partnerPrivate.getAbbreviation();
					}catch(Exception e){						
					}
		   }
		   else
		   {
			   removalRelocationService= new RemovalRelocationService(); 
			   removalRelocationService.setCreatedOn(new Date());
			   removalRelocationService.setCreatedBy(getRequest().getRemoteUser());			   			    
			       try{
					partnerPrivate = (PartnerPrivate) partnerPrivateManager.getPartnerPrivateListByPartnerCode(sessionCorpID, userManager.getUserByUsername(getRequest().getRemoteUser()).getParentAgent()).get(0);
					partnerAbbreviation=partnerPrivate.getAbbreviation();
					}catch(Exception e){						
					}
		   }
		   removalRelocationService.setCorpId(sessionCorpID); 
		   removalRelocationService.setUpdatedOn(new Date());
		   removalRelocationService.setUpdatedBy(getRequest().getRemoteUser());		   
	 return SUCCESS;
	}
	
	public String save() throws Exception { 		
		boolean isNew = (removalRelocationService.getId() == null);  
		if (isNew) { 
			removalRelocationService.setCreatedOn(new Date());
			removalRelocationService.setCreatedBy(getRequest().getRemoteUser());
			removalRelocationService.setCorpId(sessionCorpID); 
        }
	   
			try
				{ 
				removalRelocationService.setCorpId(sessionCorpID);
				removalRelocationService.setUpdatedOn(new Date());
				removalRelocationService.setUpdatedBy(getRequest().getRemoteUser());
				removalRelocationService=removalRelocationServiceManager.save(removalRelocationService);
						String key=(isNew)?"RemovalRelocationServiceAdded":"removalRelocationServiceUpdated";
						saveMessage(getText(key)); 
					}catch(Exception e){} 
		
    	return SUCCESS; 
    	}
	public String list()
	{ 
		removalRelocationServices=removalRelocationServiceManager.getAll();
		return SUCCESS;
	}	
	public String delete()
	{		
		removalRelocationServiceManager.remove(id);    
	      return SUCCESS;		    
	}
	public Long getCustomerFileId() {
		return customerFileId;
	}
	public void setCustomerFileId(Long customerFileId) {
		this.customerFileId = customerFileId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public RemovalRelocationService getRemovalRelocationService() {
		return removalRelocationService;
	}
	public void setRemovalRelocationService(
			RemovalRelocationService removalRelocationService) {
		this.removalRelocationService = removalRelocationService;
	}
	public List getRemovalRelocationServices() {
		return removalRelocationServices;
	}
	public void setRemovalRelocationServices(List removalRelocationServices) {
		this.removalRelocationServices = removalRelocationServices;
	}
	public String getSessionCorpID() {
		return sessionCorpID;
	}
	public void setSessionCorpID(String sessionCorpID) {
		this.sessionCorpID = sessionCorpID;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public void setRemovalRelocationServiceManager(
			RemovalRelocationServiceManager removalRelocationServiceManager) {
		this.removalRelocationServiceManager = removalRelocationServiceManager;
	}
	public String getSubmitCheck() {
		return submitCheck;
	}
	public void setSubmitCheck(String submitCheck) {
		this.submitCheck = submitCheck;
	}
	public CustomerFile getCustomerFile() {
		return customerFile;
	}
	public void setCustomerFile(CustomerFile customerFile) {
		this.customerFile = customerFile;
	}	
	

}
