<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>   
    <title><fmt:message key="chargeList.title"/></title>   
    <meta name="heading" content="<fmt:message key='chargeList.heading'/>"/> 
    
<style>
.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:2px 3px 3px 5px; height:15px; border:1px solid #99BBE8; border-right:none; border-left:none} 
a.dsphead{text-decoration:none;color:#000000;}
.dspchar2{padding-left:550px;}

span.pagelinks {
display:block;
font-size:0.95em;
margin-bottom:5px;
margin-top:-18px;
padding:2px 0;
text-align:right;
width:99%;
}

</style>

</head>
<s:form id="chargesListForm" action="" method="post" >
<s:hidden name="contract" value="<%=request.getParameter("contract")%>"/> 
<s:set name="contract" value="<%=request.getParameter("contract")%>"/> 
<s:hidden name="contracts.id" value="<%=request.getParameter("id")%>"/> 
<s:hidden name="id" value="<%=request.getParameter("id")%>"/> 
<s:set name="id" value="<%=request.getParameter("id")%>"/> 
<%--<c:set var="buttons">   
    <input type="button" style="margin-right: 5px"  
        onclick="location.href='<c:url value="/editCharges.html"/>'"  
        value="<fmt:message key="button.add"/>"/>   
</c:set>  
--%>
<c:out value="${button}" escapeXml="false" /> 

<div id="otabs">
	 <ul>
	 <li><a class="current"><span>Contract List</span></a></li>
	 </ul>
</div>
<div class="spnblk">&nbsp;</div> 

<s:set name="chargesByTemplate" value="chargesByTemplate" scope="request"/>  
<display:table name="chargesByTemplate" class="table" requestURI="" id="chargesByTemplate" defaultsort="2" pagesize="16" >   
   <display:column><input type="radio" name="dd"  onclick="countChargesByTemplate(this,'${chargesByTemplate.contract}','${chargesByTemplate.count}');" value="${chargesByTemplate.id}"/></display:column>
    <display:column property="contract"  title="Contract" />   
	 <display:column property="begin"  title="Begin" format="{0,date,dd-MMM-yyyy}"/> 
	<display:column property="ending" title="Ending" format="{0,date,dd-MMM-yyyy}"/> 
	<display:column property="count" title="Count"/> 
</display:table>  

<c:if test="${empty param.popup}">  
<c:out value="${buttons}" escapeXml="false" />   
</c:if>
<c:if test="${hitFlag == 1}" >
	<c:redirect url="/acctTemplateList.html?id=${id}&contract=${contract}"  />
</c:if>
</s:form>
<script language="javascript" type="text/javascript">
    function clear_fields(){
	document.forms['chargesListForm'].elements['charges.charge'].value = "";
	document.forms['chargesListForm'].elements['charges.description'].value = "";
	}
</script>   
<script language="javascript" type="text/javascript">
	function check1(value,value1,capacity){
			if( value1 == 'X' && (capacity == '' || capacity == '1' || capacity == '2')){
					document.forms['assignItemsForm'].elements['forwardBtn'].disabled=true;
					alert('This location is already occupied. Please select any other.');
				  return false;  
				}else{
					document.forms['assignItemsForm'].elements['forwardBtn'].disabled = false ;
				}
			return true;
		}
		
		
	function  countChargesByTemplate(position,contract,count){
		agree= confirm(contract+" "+"have"+" "+count+" "+"Templates. Do you want to copy these templates?");
		if(agree){
		var url = "copyTemplates.html?contractForCopy="+ encodeURI(contract);
		////alert(url);
		document.forms['chargesListForm'].action = url;
		document.forms['chargesListForm'].submit();
		window.refreshParent()
		setTimeout("window.self.close()", 700);;
		}
		else{
			return false;
		}	
	}
	
</script>
