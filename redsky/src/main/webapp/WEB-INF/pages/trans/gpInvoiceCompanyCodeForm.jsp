<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%> 
<head>
<title><fmt:message key="invoiceCompanyCodeForm.title" /></title>
<meta name="heading" content="<fmt:message key='invoiceCompanyCodeForm.heading'/>" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="">
.bgblue-left {
background:url("images/blue_band.jpg") no-repeat scroll 0 0 transparent;
color:#007A8D;
font-family:Arial,Helvetica,sans-serif;
font-size:12px;
font-weight:bold;
height:30px;
padding-left:40px;
padding-top:8px;

}
</style>

<script language="javascript" type="text/javascript">
	<%@ include file="/common/formCalender.js"%>
</script>
<script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
</script>  
<script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
</script> 

<script type="text/javascript">
function checkCompanyCode() 
{   
 var recPostingDate = document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value;  
 recPostingDate=recPostingDate.trim();  
 if(recPostingDate=='') { 
  document.forms['invoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=true;
  document.forms['invoiceCompanyCodeForm'].elements['Extract'].disabled=true; 
 }
if(recPostingDate!='')
{
document.forms['invoiceCompanyCodeForm'].elements['Extract'].disabled=false; 
}
}


function fillReverseInvoice(){ 
       var postDate = valButton(document.forms['invoiceCompanyCodeForm'].elements['radiobilling']);   
	    if (postDate == null) 
		   {
	          alert("Please select any radio button"); 
	       	  return false;
	       } 
	       else 
	       { 
	        postDate = postDate.replace('/','');  
	        document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value=postDate;
	        //var companyCode = document.forms['invoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value; 
            var recPostingDate = document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value; 
            //companyCode=companyCode.trim();
            recPostingDate=recPostingDate.trim(); 
            if(recPostingDate!='')
            { 
  	        var agree = confirm("Invoice data for posting date " +recPostingDate+ " is under processing.");
            if(agree)
             {
	          document.forms['invoiceCompanyCodeForm'].action = 'gpInvoiceExtracts.html';
              document.forms['invoiceCompanyCodeForm'].submit();
             }
             else {
             return false; 
             }
             document.forms['invoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=false; 
           }  
      }
      }
      
  function valButton(btn) {
    var cnt = -1; 
    var len = btn.length; 
    if(len >1)
    {
    for (var i=btn.length-1; i > -1; i--) {
	        if (btn[i].checked) {cnt = i; i = -1;}
	    }
	    if (cnt > -1) return btn[cnt].value;
	    else return null;
    	
    }
    else
    { 
    	return document.forms['invoiceCompanyCodeForm'].elements['radiobilling'].value; 
    } 
  }
  
  function refreshPostDate()
 { 
 document.forms['invoiceCompanyCodeForm'].action = 'gpInvoiceCompanyCode.html';
 document.forms['invoiceCompanyCodeForm'].submit();
 }
  function setCompanydiv(companydivision,temp)
  {
  //document.forms['invoiceCompanyCodeForm'].elements['invoiceCompanyCode'].value= companydivision 
  document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value= temp.value;  
  checkCompanyCode();
  }
  
</script>
</head>
<s:form id="invoiceCompanyCodeForm" name="invoiceCompanyCodeForm" action="gpInvoiceExtracts" method="post">
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>  
<s:hidden cssClass="input-textUpper" id="recPostingDate" name="recPostingDate"  />  
<div id="Layer1" style="width:100%;">
        <div id="content" align="center">
        <div id="liquid-round-top">
        <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
        <div class="center-content"> 
		<div class="bgblue-left">Invoice Data Extract</div>
		<div>
		<table border="0" style="margin-bottom: 3px">
			<tr> <td height="1px"></td></tr>
	<tr> 
	   <%--  <td width="5px"></td>
		<td align="right"  class="listwhitebox" width="150px">Company Group Code</td> 
		<td align="left" ><s:select list="%{invCompanyCodeList}" cssClass="list-menu" cssStyle="width:120px" headerKey=" " headerValue=" " id="invoiceCompanyCode"  name="invoiceCompanyCode"  onchange="checkCompanyCode();"/></td>
		--%>
		<td width="5px"></td>
		
	    <td align="left"><input type="button"  class="cssbutton1"  value="Refresh Date List for All Companies" name="refresh"  style="width:230px;" onclick="refreshPostDate();" />
		</td>
		</tr>
		</table>
		</div>
	</div>

       <div class="bottom-header" style="margin-top:40px;"><span></span></div>
       </div>
       </div>
 

<table  border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td></td>
  </tr>
  <c:if test="${InvoicePostDateList!='[]'}"> 
  <tr>
    <td>
	<table class="detailTabLabel" cellpadding="0" cellspacing="2" ><tr>
	<td align="right" class="listwhitetext"><b>The following posting dates are available, please select one to process</b></td> 
	</tr></table>
	</td>
  </tr>
  </c:if>
  <tr>
    <td>
	<table border="0" width="100%" style="margin:0px;">
		<tr><td>
		<s:set name="InvoicePostDateList" value="InvoicePostDateList" scope="request"/> 
        <display:table name="InvoicePostDateList" class="table" requestURI="" id="InvoicePostDateList" style="width:100%" defaultsort="1" pagesize="100" >   
 		 
		<display:column property="recPostDate" sortable="true" title="Posting Date" format="{0,date,dd-MMM-yyyy}"  style="width:70px" />
		<display:column property="recPostDateCount" sortable="true" title="# Recs" maxLength="15"  style="width:120px"/> 
			 	<display:column property="paymentSentCount" sortable="true" title="# Payment Sent" maxLength="15"  style="width:120px" />
	 	 	<display:column   title="Select" style="width:10px" >
 		<input style="vertical-align:bottom;" type="radio" name="radiobilling" id=${InvoicePostDateList.recPostDate} value=${InvoicePostDateList.recPostDate} onclick="setCompanydiv('${InvoicePostDateList.companyDivision}',this);"/>
 		 <s:hidden id="recPostingDate1"  name="recPostingDate1"  value="${InvoicePostDateList.recPostDate}"/>
 		</display:column>
	    
</display:table> 
</td>
</tr>
</table>
	</td>
  </tr>
  <tr>
    <td>
	<table><tbody> 
		<tr>
		
        <td align="left"><input type="button"  class="cssbutton1"  value="Extract" name="Extract"  style="width:70px;" onclick="return fillReverseInvoice();" />  
        </td>
        
      
        <td align="left" ><input type="button" class="cssbutton1" name="ExtractCustomer"  style="width:150px; " onclick="location.href='<c:url value="/gpCustomerPartExtracts.html"/>'"  
        value="Extract Customer Data"/>
        </td>
		</tr> 
	</tbody></table>
	</td>
  </tr>
  
</table>

</div>
</s:form>
<script type="text/javascript">  
try{

	
document.forms['invoiceCompanyCodeForm'].elements['radiobilling'].checked=true;  
if(document.forms['invoiceCompanyCodeForm'].elements['recPostingDate1'].value.trim()!='undefined'){
	document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value= document.forms['invoiceCompanyCodeForm'].elements['recPostingDate1'].value;
	document.forms['invoiceCompanyCodeForm'].elements['ExtractCustomer'].disabled=true;
}else{
document.forms['invoiceCompanyCodeForm'].elements['recPostingDate'].value='';
}
}
catch(e){}
try{
checkCompanyCode(); 
}
catch(e){}
</script>