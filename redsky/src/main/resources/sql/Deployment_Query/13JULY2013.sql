// After Every Restart of MySql Server Run This Query
SET GLOBAL log_bin_trust_function_creators = 1; 
//

update app_user set newsUpdateFlag=true ;

ALTER TABLE `redsky`.`systemdefault` ADD COLUMN `paymentApplicationExtractSeq` VARCHAR(5) AFTER `ownbillingVanline`;

ALTER TABLE `redsky`.`auditSetup` MODIFY COLUMN `tableName` VARCHAR(50)  DEFAULT NULL;

ALTER TABLE `redsky`.`reports` MODIFY COLUMN `emailBody` TEXT DEFAULT NULL;
