package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.appfuse.dao.hibernate.GenericDaoHibernate;

import com.trilasoft.app.dao.OperationsDailyLimitsDao;
import com.trilasoft.app.dao.hibernate.InventoryPackingDaoHibernate.InventoryDTO;
import com.trilasoft.app.model.OperationsDailyLimits;

public class OperationsDailyLimitsDaoHibernate extends GenericDaoHibernate<OperationsDailyLimits, Long> implements OperationsDailyLimitsDao{
	
	public OperationsDailyLimitsDaoHibernate() {
		super(OperationsDailyLimits.class);
	}
	
	String workLimitDate="";
	public List isExisted(Date workDate, String hub, String sessionCorpID){
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		return this.getSession().createSQLQuery("select * from operationsdailylimits where workDate = '"+workLimitDate+"' AND hubID = '"+hub+"' AND corpID = '"+sessionCorpID+"'").list();
	}
	

	public class InventoryDTO{
		private Object id;
		private Object dailyLimits;
		private Object workDate;
		private Object hubID;
		private Object dailyMaxEstWt;
		private Object dailyMaxEstVol;
		private Object comment;
		private Object item;
		private Object dailyCrewLimit;
		
		public Object getItem() {
			return item;
		}
		public void setItem(Object item) {
			this.item = item;
		}
		public Object getId() {
			return id;
		}
		public void setId(Object id) {
			this.id = id;
		}
		public Object getDailyLimits() {
			return dailyLimits;
		}
		public void setDailyLimits(Object dailyLimits) {
			this.dailyLimits = dailyLimits;
		}
		public Object getWorkDate() {
			return workDate;
		}
		public void setWorkDate(Object workDate) {
			this.workDate = workDate;
		}
		public Object getHubID() {
			return hubID;
		}
		public void setHubID(Object hubID) {
			this.hubID = hubID;
		}
		public Object getDailyMaxEstWt() {
			return dailyMaxEstWt;
		}
		public void setDailyMaxEstWt(Object dailyMaxEstWt) {
			this.dailyMaxEstWt = dailyMaxEstWt;
		}
		public Object getDailyMaxEstVol() {
			return dailyMaxEstVol;
		}
		public void setDailyMaxEstVol(Object dailyMaxEstVol) {
			this.dailyMaxEstVol = dailyMaxEstVol;
		}
		public Object getComment() {
			return comment;
		}
		public void setComment(Object comment) {
			this.comment = comment;
		}
		public Object getDailyCrewLimit() {
			return dailyCrewLimit;
		}
		public void setDailyCrewLimit(Object dailyCrewLimit) {
			this.dailyCrewLimit = dailyCrewLimit;
		}
	}

	public List getDailyLimitsByHub(String hub, Date newWorkDate, String sessionCorpID, String category, String resource) {		
		/*	if(newWorkDate!=null )
			{
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newWorkDate));
			newWorkDate = nowYYYYMMDD1.toString();
			}*/
		String query="";
		List hubList = new ArrayList();
		if(newWorkDate!=null){
						
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(newWorkDate));
			
			if(category!=null && !category.equalsIgnoreCase("") && resource!=null && !resource.equalsIgnoreCase("") ){
				query = ("select op.id ,op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where op.corpId='"+sessionCorpID+"' and op.hubid='"+hub+"' and op.workdate in (select rg.workdate from resourcegrid rg where rg.hubID = '"+hub+"' and rg.category='"+category+"' and rg.resource='"+resource+"' and rg.workDate like '"+nowYYYYMMDD1+"%' and rg.corpId='"+sessionCorpID+"') order by op.workdate");
			}else{
				if(category!=null && !category.equalsIgnoreCase("")){
					query = ("select op.id ,op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where op.corpId='"+sessionCorpID+"' and op.hubid='"+hub+"' and op.workdate in (select rg.workdate from resourcegrid rg where rg.hubID = '"+hub+"' and rg.category='"+category+"' and rg.workDate like '"+nowYYYYMMDD1+"%' and rg.corpId='"+sessionCorpID+"')order by op.workdate");
				}
				if(resource!=null && !resource.equalsIgnoreCase("")){
					query = ("select op.id ,op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where op.corpId='"+sessionCorpID+"' and op.hubid='"+hub+"' and op.workdate in (select rg.workdate from resourcegrid rg where rg.hubID = '"+hub+"'  and rg.resource='"+resource+"' and rg.workDate like '"+nowYYYYMMDD1+"%' and rg.corpId='"+sessionCorpID+"')order by op.workdate");
				}
			}
						
			if( category == null && resource == null  ){
				query = ("select distinct (op.id ),op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where  op.hubID = '"+hub+"' and op.workDate like '"+nowYYYYMMDD1+"%' AND op.corpId='"+sessionCorpID+"' order by op.workdate ");
			}
			else if( category.equals("") && resource.equals("")){
				query = ("select distinct (op.id ),op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where  op.hubID = '"+hub+"' and op.workDate like '"+nowYYYYMMDD1+"%' AND op.corpId='"+sessionCorpID+"'  ");
			}
			
		}else{
			if(category!=null && !category.equalsIgnoreCase("") && resource != null && !resource.equalsIgnoreCase("") ){
				query = ("select op.id ,op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where op.corpId='"+sessionCorpID+"' and op.hubid='"+hub+"' and op.workdate in (select rg.workdate from resourcegrid rg where rg.hubID = '"+hub+"' and rg.category='"+category+"' and rg.corpId='"+sessionCorpID+"' and rg.resource='"+resource+"') order by op.workdate");
			}else{
				if(category!=null && !category.equalsIgnoreCase("")){
					query = ("select op.id ,op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op where op.corpId='"+sessionCorpID+"' and op.hubid='"+hub+"' and op.workdate in (select rg.workdate from resourcegrid rg where rg.hubID = '"+hub+"' and rg.category='"+category+"' and rg.corpId='"+sessionCorpID+"')order by op.workdate");
				}
				if(resource != null &&!resource.equalsIgnoreCase("")){
					query = ("select distinct (op.id ),op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op  , resourcegrid rg where rg.hubID =op.hubID and rg.corpId='"+sessionCorpID+"' and op.hubID = '"+hub+"' AND op.corpId='"+sessionCorpID+"' and rg.resource='"+resource+"' order by op.workdate");
				}
			}
			
			if((category== null  && resource == null) ){
				query = ("select distinct (op.id ),op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op  where op.hubID = '"+hub+"' AND op.corpId='"+sessionCorpID+"' order by op.workdate");
			}
			else if( category.equals("") && resource.equals("")){
				query = ("select distinct (op.id ),op.dailyLimits,op.workDate,op.hubID,op.dailyMaxEstWt,op.dailyMaxEstVol,op.comment,op.dailyCrewCapacityLimit from operationsdailylimits op  where op.hubID = '"+hub+"' AND op.corpId='"+sessionCorpID+"' order by op.workdate");
			}
		}
		List list =this.getSession().createSQLQuery(query).list();
		Iterator itr=list.iterator();
		while(itr.hasNext()){
			String listVal ="";
			InventoryDTO inventoryDTO= new InventoryDTO(); 
			Object[] object=(Object[]) itr.next();
			inventoryDTO.setId(object[0]);
			inventoryDTO.setDailyLimits(object[1]);
			inventoryDTO.setWorkDate(object[2]);
			inventoryDTO.setHubID(object[3]);
			inventoryDTO.setDailyMaxEstWt(object[4]);
			inventoryDTO.setDailyMaxEstVol(object[5]);
			inventoryDTO.setComment(object[6]);	
			inventoryDTO.setDailyCrewLimit(object[7]);
			
			List tempItem =this.getSession().createSQLQuery("select category,resource,resourceLimit from resourcegrid where workDate='"+object[2]+"' and hubID='"+object[3]+"' AND corpId='"+sessionCorpID+"'").list(); 
			Iterator itrItem=tempItem.iterator();
			while(itrItem.hasNext()){
				Object[] obj = (Object[]) itrItem.next();
				if(listVal.equals("")){
					listVal=obj[0]+"~"+obj[1]+"~"+obj[2];
				}else{
					listVal=listVal+"^"+obj[0]+"~"+obj[1]+"~"+obj[2];
				}
			}
			inventoryDTO.setItem(listVal);
			hubList.add(inventoryDTO);
		}
		return  hubList;
	}
	public void updateWorkDate(String hub, String newWorkDate,Long id, String sessionCorpID){
		String query="update ResourceGrid set workDate='"+newWorkDate+"' where hubID ='"+hub+"' and parentid='"+id+"' and corpId='"+sessionCorpID+"'";
		getHibernateTemplate().bulkUpdate(query);
	}
	public List getListById(Long id, String sessionCorpID){
		List list = getHibernateTemplate().find("from OperationsDailyLimits where parentId='"+id+"' and corpid='"+sessionCorpID+"' ");
		return list;
	}
	public void updateDailyLimitParent(Integer tempDailyLimit,Integer tempDailyLimitNew,Integer tempDailyLimitOld, String sessionCorpID,String hubID,Date workDate){
		String query="";
		String query1="";
		String tempParentName="";
		String tempDailyLimitValue="";
		int  totalDailyLimit=0;
		List dailyLimitsValue= new ArrayList();
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyLimitsValue  = this.getSession().createSQLQuery("select dailyLimits from operationsdailylimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ").list();
		}
		if(dailyLimitsValue!=null && !dailyLimitsValue.isEmpty() && dailyLimitsValue.get(0)!=null) {
			tempDailyLimitValue = dailyLimitsValue.get(0).toString().trim();
			if(tempDailyLimitOld < tempDailyLimitNew){
				totalDailyLimit=Integer.parseInt(tempDailyLimitValue)+(tempDailyLimit);
			 }else{
				 totalDailyLimit=Integer.parseInt(tempDailyLimitValue)-(tempDailyLimit);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationsdailylimits set dailyLimits ='"+totalDailyLimit+"' where hubID ='"+tempParentName+"' and corpID='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";			
					this.getSession().createSQLQuery(query).executeUpdate();
				
				query1="update operationsdailylimits set dailyLimits ='"+totalDailyLimit+"' where hubID ='0' and corpID='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";			
    				this.getSession().createSQLQuery(query1).executeUpdate();
			}
	}
	public void updateDailyMaxEstWtParent(BigDecimal tempDailymaxWt,BigDecimal tempDailyMaxWtNew,BigDecimal tempDailyMaxWtOld, String sessionCorpID,String hubID,Date workDate){
		String query="";
		String query1="";
		String tempParentName="";
		String dailyMaxEstWtValue="";
		List dailyLimitsValue= new ArrayList();
		BigDecimal  totalDailyMaxEstWt=new BigDecimal(0.00);
		int res;
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyLimitsValue  = this.getSession().createSQLQuery("select dailyMaxEstWt from operationsdailylimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ").list();
		}
		if(dailyLimitsValue!=null && !dailyLimitsValue.isEmpty() && dailyLimitsValue.get(0)!=null) {
			dailyMaxEstWtValue = dailyLimitsValue.get(0).toString().trim();
			res=tempDailyMaxWtOld.compareTo( tempDailyMaxWtNew);
			 BigDecimal bd= new BigDecimal (dailyMaxEstWtValue); 
			 if(res== -1){
				 totalDailyMaxEstWt=(bd).add(tempDailymaxWt);
			 }else if( res == 1 ){
				 totalDailyMaxEstWt=(bd).subtract(tempDailymaxWt);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationsdailylimits set dailyMaxEstWt ='"+totalDailyMaxEstWt+"' where hubID ='"+tempParentName+"' and corpID='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";			
					this.getSession().createSQLQuery(query).executeUpdate();
					
				query1="update operationsdailylimits set dailyMaxEstWt ='"+totalDailyMaxEstWt+"' where hubID ='0' and corpid='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";
					this.getSession().createSQLQuery(query1).executeUpdate();
			}
	}
	public void updateDailyMaxEstVolParent(BigDecimal tempDailyMaxEstVol,BigDecimal tempDailyMaxEstVolNew,BigDecimal tempDailyMaxEstVolOld, String sessionCorpID,String hubID,Date workDate){
		String query="";
		String query1="";
		String tempParentName="";
		String dailyMaxEstWtValue="";
		List dailyLimitsValue= new ArrayList();
		BigDecimal  totalDailyMaxEstVol=new BigDecimal(0.00);
		int res;
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyLimitsValue  = this.getSession().createSQLQuery("select dailyMaxEstVol from operationsdailylimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ").list();
		}
		if(dailyLimitsValue!=null && !dailyLimitsValue.isEmpty() && dailyLimitsValue.get(0)!=null) {
			dailyMaxEstWtValue = dailyLimitsValue.get(0).toString().trim();
			res=tempDailyMaxEstVolOld.compareTo( tempDailyMaxEstVolNew);
			 BigDecimal bd= new BigDecimal (dailyMaxEstWtValue); 
			 if(res== -1){
				 totalDailyMaxEstVol=(bd).add(tempDailyMaxEstVol);
			 }else if( res == 1 ){
				 totalDailyMaxEstVol=(bd).subtract(tempDailyMaxEstVol);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationsdailylimits set dailyMaxEstVol ='"+totalDailyMaxEstVol+"' where hubID ='"+tempParentName+"' and corpID='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";			
					this.getSession().createSQLQuery(query).executeUpdate();
				
				query1="update operationsdailylimits set dailyMaxEstVol ='"+totalDailyMaxEstVol+"' where hubID ='0' and corpid='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}
	}
	public void updateDailyCrewLimitParent(Integer tempDailyCrewLimit,Integer tempDailyCrewLimitNew,Integer tempDailyCrewLimitOld, String sessionCorpID,String hubID,Date workDate){
		String query="";
		String query1="";
		String tempParentName="";
		String tempDailyCrewValue="";
		int  totalDailyLimit=0;;
		List dailyCrewLimitValue= new ArrayList();
		if (!(workDate == null)) {
			SimpleDateFormat dateformatYYYYMMDD1 = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder nowYYYYMMDD1 = new StringBuilder(dateformatYYYYMMDD1.format(workDate));
			workLimitDate = nowYYYYMMDD1.toString();
		}
		
		List parentName  = this.getSession().createSQLQuery("select flex1 from refmaster where parameter = 'HOUSE' and corpID = '"+sessionCorpID+"' and code = '"+hubID+"' ").list();
		if(parentName!=null && !parentName.isEmpty() && parentName.get(0)!=null){
			 tempParentName = parentName.get(0).toString().trim();
			 dailyCrewLimitValue  = this.getSession().createSQLQuery("select dailyCrewCapacityLimit from operationsdailylimits where hubID = '"+tempParentName+"' and corpID = '"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ").list();
		}
		if(dailyCrewLimitValue!=null && !dailyCrewLimitValue.isEmpty() && dailyCrewLimitValue.get(0)!=null) {
			tempDailyCrewValue = dailyCrewLimitValue.get(0).toString().trim();
			if(tempDailyCrewLimitOld < tempDailyCrewLimitNew){
				totalDailyLimit=Integer.parseInt(tempDailyCrewValue)+(tempDailyCrewLimit);
			 }else{
				 totalDailyLimit=Integer.parseInt(tempDailyCrewValue)-(tempDailyCrewLimit);
			 }
		}
		if(!(tempParentName.equalsIgnoreCase(""))){
				query="update operationsdailylimits set dailyCrewCapacityLimit ='"+totalDailyLimit+"' where hubID ='"+tempParentName+"' and corpID='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";			
					this.getSession().createSQLQuery(query).executeUpdate();
				
				query1="update operationsdailylimits set dailyCrewCapacityLimit ='"+totalDailyLimit+"' where hubID ='0' and corpid='"+sessionCorpID+"' and workDate = '"+workLimitDate+"' ";
					this.getSession().createSQLQuery(query1).executeUpdate();
		}
	}
}
