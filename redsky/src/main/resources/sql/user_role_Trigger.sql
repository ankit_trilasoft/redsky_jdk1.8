delimiter $$
CREATE  trigger redsky.trigger_add_history_user_role
before delete on redsky.user_role
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;
  DECLARE oldCorpid LONGTEXT;
  DECLARE newUser LONGTEXT;
  DECLARE newRoleName LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";
  SET oldCorpid = " ";
  SET newUser = " ";
  SET newRoleName = " ";

select corpid into oldCorpid from role where id=OLD.role_id limit 1;

IF (OLD.role_id <> '' or OLD.role_id is not null) THEN
    select name into newRoleName from role where id=OLD.role_id;
       select CONCAT(fieldNameList,'user_role.role_id~') into fieldNameList;
       select CONCAT(newValueList,newRoleName,'~') into oldValueList;
   END IF;
CALL add_tblHistory (OLD.user_id,"user_role", fieldNameList, oldValueList, newValueList, OLD.updatedby, oldCorpid, now());


END $$
delimiter;