<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<title>Ref Job Document Type Details</title>
<meta name="heading"
	content="RefJob DocumentType Details" />

<style type="text/css">	
legend {
font-family:arial,verdana,sans-serif;
font-size:11px;
font-weight:bold;
margin:0;
}
div#content {padding:0px 0px; min-height:50px; margin-left:0px;}

</style>

<style><%@ include file="/common/calenderStyle.css"%></style>

<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/masks.js"></script>
<script type="text/javascript" src="scripts/prototype.js"></script>
<script type="text/javascript" src="scripts/effects.js"></script>
<script type="text/javascript" src="scripts/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<script type="text/javascript" src="scripts/animatedcollapse.js"></script>
<script type="text/javascript">
function checkAll(temp){
    document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = "";
    var len = document.forms['refJobDocumentTypeForm'].elements['checkV'].length;
    if(temp.checked){
    for (i = 0; i < len; i++){

        document.forms['refJobDocumentTypeForm'].elements['checkV'][i].checked = true ;
        selectColumn(document.forms['refJobDocumentTypeForm'].elements['checkV'][i]);
    }
    }
    else{
    for (i = 0; i < len; i++){

        document.forms['refJobDocumentTypeForm'].elements['checkV'][i].checked = false ;
        selectColumn(document.forms['refJobDocumentTypeForm'].elements['checkV'][i]);
    }
    }
}
function unCheckAll(){
 document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = "";
}
function selectColumn(targetElement) 
   {  
   var rowId=targetElement.value;  
   if(targetElement.checked)
     {
      var userCheckStatus = document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value;
      if(userCheckStatus == '')
      {
	  	document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = rowId;
      }
      else
      {
       var userCheckStatus=	document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = userCheckStatus + '#' + rowId;
      document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = userCheckStatus.replace( '##' , '#' );
      }
    }
   if(targetElement.checked==false)
    {
     var userCheckStatus = document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value;
     var userCheckStatus=document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = userCheckStatus.replace( rowId , '' );
     document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = userCheckStatus.replace( '##' , '#' );
     } 
 
    }
function checkValidate(){
	if(document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.jobType'].value == ''){
		alert('Please select job.');
		return false;
		}
} 

function checkColumn(){
var userCheckStatus ="";
  userCheckStatus = document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value; 
if(userCheckStatus!=""){ 
document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value="";
var column = userCheckStatus.split(",");  
for(i = 0; i<column.length ; i++)
{    
     var userCheckStatus = document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value;  
     var userCheckStatus=	document.forms['refJobDocumentTypeForm'].elements['refJobDocumentType.refJobDocs'].value = userCheckStatus + '#' +column[i];   
     document.getElementById(column[i]).checked=true;
} 
}
if(userCheckStatus==""){

}
}


  

</script>
</head>

<s:form name="refJobDocumentTypeForm" id="refJobDocumentTypeForm" action="saveRefJobDocumentType" >

<s:hidden name="refJobDocumentType.id" value="${refJobDocumentType.id}"/> 
<div id="Layer1" style="width:100%;">

	<div id="layer4" style="width:100%;">
		<div id="newmnav">
			<ul>
				<li id="newmnav1" style="background:#FFF "><a class="current"><span>Backup Docs<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>				
				<li><a href="refJobDocumentType1.html"><span>Ref Job Document Type List</span></a></li>	  	
			</ul>
		</div>
		<div class="spn">&nbsp;</div>
	</div>
	<div id="content" align="center">
<div id="liquid-round">
   <div class="top"><span></span></div>
   <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" width="100%" >
		<tbody>
		<tr><td align="right" width="24"  class="listwhitetext" valign="top"></td>
		<td colspan="7" class="bgblue">BackUp Documents </td></tr>
		<tr>
			<td align="right" width="24"  class="listwhitetext" valign="top"></td>
                      <s:hidden  name="refJobDocumentType.refJobDocs"   />
                     <td class="listwhitetext" valign="top"><input type="checkbox" style="margin-left:136px;vertical-align:middle;" name="checkAllDoc"  onclick="checkAll(this)" />Check&nbsp;All&nbsp;</td>
					 <td width="100" class="listwhitetext">Select Job Type:- <font color="red" size="2">*</font></td>
							<c:if test="${empty refJobDocumentType.id}">
					 		<td><s:select name="refJobDocumentType.jobType" id="refJobDocumentType.jobType" cssClass="list-menu" list="%{jobs}" cssStyle="width:140px" headerKey="" headerValue="" /></td>                     
							</c:if>
							<c:if test="${not empty refJobDocumentType.id}">
								<td><s:textfield readonly="true" cssClass="input-textUpper" name="refJobDocumentType.jobType"  cssStyle="width:140px"/></td>
							</c:if>
                     </tr>
                     <tr>
                     <td></td>
                      <td colspan="3" valign="top">
                     <fieldset style="margin:3px 0 0 0; padding:2px 0 0 2px; width:50%;">                      
                     <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
                     <tr>
                      <td width="53%" valign="top">
                        <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${documentList1}">
                           <tr>
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" /> </td> 
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                           
                          </tr>
                          </c:forEach>
                          </table> 
                       </td>
                       <td valign="top">
                       <table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0"> 
                          <c:forEach var="entry" items="${documentList2}">
                           <tr> 
                           <td><input type="checkbox" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" /> </td>
                           <td width="5px"></td>
                           <td align="left" class="listwhitetext" > ${entry.value}</td>
                          </tr>
                          </c:forEach>
                          </table>                    
                       </td>
                       </tr>
                       </table>
                          </fieldset>
                       </td>
                        
                       </tr>
 </table>
 </div>
 <div class="bottom-header" style="margin-top:50px; z-index:0"><span></span></div>
</div>
	  <table border="0" style="width:721px">
	<tbody>
	  <tr><td align="left" rowspan="1"></td></tr>
	  <tr>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdOn'/></td>
		<fmt:formatDate var="containerCreatedOnFormattedValue" value="${refJobDocumentType.createdOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="refJobDocumentType.createdOn" value="${containerCreatedOnFormattedValue}" />
		 <td><fmt:formatDate value="${refJobDocumentType.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
		 <td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.createdBy' /></td>
		  <c:if test="${not empty refJobDocumentType.id}">
		    <s:hidden name="refJobDocumentType.createdBy"/>
		    <td><s:label name="createdBy" value="%{refJobDocumentType.createdBy}"/></td>
		 </c:if>
		 <c:if test="${empty refJobDocumentType.id}">
			 <s:hidden name="refJobDocumentType.createdBy" value="${pageContext.request.remoteUser}"/>
			 <td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
		</c:if>
		<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='refJobType.updatedOn'/></td>
		<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${refJobDocumentType.updatedOn}"  pattern="${displayDateTimeEditFormat}"/>
		<s:hidden name="refJobDocumentType.updatedOn" value="${containerUpdatedOnFormattedValue}" />
		<td><fmt:formatDate value="${refJobDocumentType.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
		<td align="right" class="listwhitetext" style="width:85px"><b><fmt:message key='refJobType.updatedBy' /></td>
			<c:if test="${not empty refJobDocumentType.id}">
			    <s:hidden name="refJobDocumentType.updatedBy"/>
			    <td><s:label name="updatedBy" value="%{refJobDocumentType.updatedBy}"/></td>
			 </c:if>
			 <c:if test="${empty refJobDocumentType.id}">
				 <s:hidden name="refJobDocumentType.updatedBy" value="${pageContext.request.remoteUser}"/>
				 <td><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
			</c:if>
    </tr>
   </tbody>
   </table>
<s:submit cssClass="cssbuttonA" method="save" key="button.save" cssStyle="width:55px; height:25px" onclick="return checkValidate();"/>   
<s:reset cssClass="cssbuttonA" key="Reset" onclick="unCheckAll();" cssStyle="width:55px; height:25px" >	
	</s:reset>

</s:form>


<script type="text/javascript">

try{
checkColumn();
}
catch(e){}
</script>

