package com.trilasoft.app.service;


 import java.util.List;

import org.appfuse.service.GenericManager; 

import com.trilasoft.app.model.TableCatalog;


public interface TableCatalogManager extends GenericManager<TableCatalog, Long>{
	public List getTableListValue(String tableN,String tableD);
	public List getData(String tableName,String sessionCorpID);
} 
 
