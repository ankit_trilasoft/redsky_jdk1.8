<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
<title>Origin&nbsp;Address</title>   
<meta name="heading" content="Tool Tip"/> 
</head> 
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" width="180px">

<tr>
<td><b>Origin&nbsp;Address</b></td>
</tr>
<tr>
<td><b>City:</b> ${fullAddForOCity[0].originCity},<b>Zip:</b> ${fullAddForOCity[0].originZip}<br></td> 
</tr>
<tr>
<td><c:choose><c:when test='${fullAddForOCity[0].originState!=""}'><b>State:</b> ${fullAddForOCity[0].originState}</c:when><c:otherwise><b>Country:</b> ${fullAddForOCity[0].originCountry}</c:otherwise></c:choose></td>
</tr>
<tr>
<td><c:choose><c:when test='${fullAddForOCity[0].originState==""}'></c:when><c:otherwise><b>Country:</b> ${fullAddForOCity[0].originCountry}</c:otherwise></c:choose></td>
</tr>

</table>


