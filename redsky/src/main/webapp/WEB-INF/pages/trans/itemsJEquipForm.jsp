<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags" %>
 
<head>
    <title><fmt:message key="itemsJEquipDetail.title"/></title>
    <meta name="heading" content="<fmt:message key='itemsJEquipDetail.heading'/>"/>
    <style type="text/css">
		h2 {background-color: #FBBFFF}
	</style>
</head>
    
    <!-- 
    <link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
    <s:head theme="ajax" />
	<style type="text/css">
	h2 {background-color: #CCCCCC}
	</style>
     
</head> 
 -->

<s:form id="itemsJEquipForm" action="saveItemsJEquip" method="post" validate="true"> 
<s:hidden name="itemsJEquip.id" value="%{itemsJEquip.id}"/> 
<div id="Layer1">
<table cellspacing="0" cellpadding="6" border="0">
				<tbody>
					<tr>
						<td valign="top" align="left" class="listwhite">
						<table cellspacing="0" cellpadding="0" border="1">
							<tbody>
								<tr>
									<td class="subcontent-tab" colspan="2">Items Equipment Details</td>
									<td width="68%" colspan="3"></td>
								</tr>
								<tr>
									<td colspan="6">
									<table cellspacing="3" cellpadding="3" border="0">
										<tbody>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<!-- &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; -->												
												<td></td><td></td>
												<td>corpID</td>
												<td><s:textfield key="itemsJEquip.corpID" required="true" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="20" name="textfield71" /></td> -->
												<!-- <td>ID Number</td> -->
												<!--<td><s:textfield key="itemsJEquip.idNum" required="true" cssClass="text medium"/></td> -->
												<!-- <td><input type="text" size="20" name="textfield72" /></td> -->
												<td>Payment</td>
												<td><s:textfield key="itemsJEquip.payCat" required="true" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="20" name="textfield85" /></td>-->
												<td></td>
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
											<tr>
												<td>Created By</td>
												<td><s:textfield key="itemsJEquip.createdBy" required="true" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82172" /></td> -->
												<td>Updated By</td>
												<td><s:textfield key="itemsJEquip.updatedBy" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82173" /></td> -->
												<td>Created On</td>
												<td><s:textfield key="itemsJEquip.createdOn" required="true" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82174" /></td> -->
												<td>Updated On</td>
												<td><s:textfield key="itemsJEquip.updatedOn" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82175" /></td> -->
											</tr>
											<tr>
												<td>Quantity</td>
												<td><s:textfield key="itemsJEquip.qty" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield8222" /></td> -->
												<td>Type</td>
												<td><s:textfield key="itemsJEquip.type" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield8232" /></td> -->
												<td>Bucket</td>
												<td><s:textfield key="itemsJEquip.bucket" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82322" /></td> -->
												<td>Description</td>
												<td><s:textfield key="itemsJEquip.descript" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82222" /></td> -->
											</tr>
											<tr>
												<td>Cost</td>
												<td><s:textfield key="itemsJEquip.cost" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82223" /></td> -->
												<td>Charge</td>
												<td><s:textfield key="itemsJEquip.charge" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82224" /></td> -->
												<td>Destination Charge</td>
												<td><s:textfield key="itemsJEquip.dtCharge" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82225" /></td> -->
												<td>General</td>
												<td><s:textfield key="itemsJEquip.gl" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82226" /></td> -->
											</tr>
											<tr>
												<td>Bill Crew</td>
												<td><s:textfield key="itemsJEquip.billCrew" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82223" /></td> -->
												<td>General Type</td>
												<td><s:textfield key="itemsJEquip.glType" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82224" /></td> -->
												<td>Last Reset</td>
												<td><s:textfield key="itemsJEquip.lastReset" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82225" /></td> -->
												<td>Contract</td>
												<td><s:textfield key="itemsJEquip.contract" required="false" cssClass="text medium"/></td>
												<!-- <td><input type="text" size="10" name="textfield82226" /></td> -->
											</tr>
											<tr>
												<td>Pounds</td>
												<td><s:textfield key="itemsJEquip.pounds" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield82223" /></td> -->
												<td>Pack Labour</td>
												<td><s:textfield key="itemsJEquip.packLabour" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield82224" /></td> -->
												<td>Overtime Charge</td>
												<td><s:textfield key="itemsJEquip.otCharge" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield82225" /></td> -->
												<td>Crew Type</td>
												<td><s:textfield key="itemsJEquip.crewType" required="false" cssClass="text small"/></td>
												<!-- <td><input type="text" size="10" name="textfield82228" /></td> -->
											</tr>
											<tr>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
												<td></td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								
								<!-- <tr>
						<td height="10" align="left" class="listwhite">
						<div class="subcontent-tab"><a href="javascript:void(0)"
							class="dsphead" onclick="dsp(this)"> Origin/ Destination<span
							class="dspchar">+</span></a></div>
							</td></tr> -->
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
</div>

<!-- <table border="1">
<tr>
   <td colspan= "5"><h2>Items Detail </h2></td> 
   </tr>
<tr>
   <td><s:textfield key="itemsJEquip.corpID" required="true" cssClass="text medium"/></td>
   <td><s:textfield key="itemsJEquip.idNum" required="true" cssClass="text medium"/></td>
   <td><s:textfield key="itemsJEquip.payCat" required="true" cssClass="text medium"/></td>
</tr>
<tr>
   <td colspan= "5"><h2>User Information </h2></td> 
</tr>
<tr>
   <td><s:textfield key="itemsJEquip.createdBy" required="true" cssClass="text large"/></td>
   <td><s:textfield key="itemsJEquip.updatedBy" required="false" cssClass="text large"/>  </td> 
   <td> </td>
</tr>
<tr>
   <td colspan= "5"><h2>Datewise Creation Information </h2></td> 
</tr>
<tr>
   <td><s:textfield key="itemsJEquip.createdOn" required="true" cssClass="text large"/></td>
   <td><s:textfield key="itemsJEquip.updatedOn" required="false" cssClass="text large"/>  </td> 
   <td> </td>
</tr>
<tr>
   <td colspan= "5"><h2>Item Information </h2></td> 
</tr>
<tr>
   <td><s:textfield key="itemsJEquip.qty" required="false" cssClass="text small"/>  </td> 
   <td><s:textfield key="itemsJEquip.type" required="false" cssClass="text small"/>  </td> 
   <td><s:textfield key="itemsJEquip.descript" required="false" cssClass="text medium"/> </td> 
   <td> </td>
 </tr>
 <tr>
   <td colspan= "5"><h2>Item Equipment Charge</h2></td> 
 </tr>
 <tr>
   <td><s:textfield key="itemsJEquip.cost" required="false" cssClass="text medium"/>  </td>
   <td><s:textfield key="itemsJEquip.charge" required="false" cssClass="text medium"/> </td> 
   <td><s:textfield key="itemsJEquip.dtCharge" required="false" cssClass="text medium"/> </td>
 </tr>
 <tr>
   <td colspan= "5"><h2>Item Job Details</h2></td> 
 </tr>
 <tr>
  <td><s:textfield key="itemsJEquip.bucket" required="false" cssClass="text medium"/></td> 
  <td><s:textfield key="itemsJEquip.contract" required="false" cssClass="text medium"/></td> 
  <td><s:textfield key="itemsJEquip.gl" required="false" cssClass="text medium"/></td>
 </tr>
 <tr>
   <td colspan= "5"><h2>Details</h2></td> 
 </tr>
 <tr>
   <td><s:textfield key="itemsJEquip.billCrew" required="false" cssClass="text medium"/></td> 
   <td><s:textfield key="itemsJEquip.glType" required="false" cssClass="text medium"/></td> 
   <td><s:textfield key="itemsJEquip.lastReset" required="false" cssClass="text medium"/></td> 
 </tr>
 <tr>
   <td colspan= "5"><h2>Crew Details</h2></td> 
 </tr> 
 <tr>
   <td><s:textfield key="itemsJEquip.crewType" required="false" cssClass="text small"/></td> 
   <td><s:textfield key="itemsJEquip.pounds" required="false" cssClass="text small"/></td> 
   <td><s:textfield key="itemsJEquip.packLabour" required="false" cssClass="text small"/></td> 
   <td><s:textfield key="itemsJEquip.otCharge" required="false" cssClass="text small"/></td> 
 </tr>
 </table>
 </div>
  -->
  
    <li class="buttonBar bottom">         
        <s:submit cssClass="button" method="save" key="button.save"/> 
        <c:if test="${not empty itemsJEquip.id}">  
            <s:submit cssClass="button" method="delete" key="button.delete" onclick="return confirmDelete('itemsJEquip')"/> 
        </c:if> 
        <s:submit cssClass="button" method="cancel" key="button.cancel"/> 
    </li>
</s:form>
 
<script type="text/javascript"> 
    Form.focusFirstElement($("itemsJEquipForm")); 
</script> 