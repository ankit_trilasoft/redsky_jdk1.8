/**
 * @Class Name  Vehicle Dao
 * @Author      Sunil Kumar Singh
 * @Version     V01.0
 * @Since       1.0
 * @Date        01-Dec-2008
 */

package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.Vehicle;

public interface VehicleDao extends GenericDao<Vehicle, Long>
{
	public List<Vehicle> findByLastName(String lastName);
	public List findMaxId();
	public List getContainerNumber(String shipNumber);
	public List checkById(Long id);
	public List findMaximumIdNumber(String shipNum);
	public int updateDeleteStatus(Long ids);
	public List goNextSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goPrevSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List goSOChild(Long sidNum, String sessionCorpID, Long soIdNum);
	public List findMaximumChild(String shipNm);
    public List findMinimumChild(String shipNm);
    public List findCountChild(String shipNm);
    public List vehicleList(Long serviceOrderId); 
    public Long findRemoteVehicle(String idNumber, Long serviceOrderId);
    public void deleteNetworkVehicle(Long serviceOrderId, String idNumber, String vehicleStatus);
    public List findVehicleBySid(Long sofid, String sessionCorpID);
    public List vehicleListOtherCorpId(Long sid);
    public void updateStatus(Long id, String vehicleStatus,String updatedBy);
    /*
     * Bug 15084 - Changes on the EDI Report for ONLY customers using EDI Reporting
     */
    public List getWeight(String shipNumber);
	public List getVolume(String shipNumber);
	public List getVolumeUnit(String shipNumber);
	public List getWeightUnit(String shipNumber);
	public int updateMisc(String shipNumber,String weight,String weightUnit,String volumeUnit,String updatedBy,String sessionCorpID);
	public int updateMiscVolume(String shipNumber,String volume,String weightUnit,String corpID,String updatedBy);
	/*
     * End
     */
}
//End of Class. 