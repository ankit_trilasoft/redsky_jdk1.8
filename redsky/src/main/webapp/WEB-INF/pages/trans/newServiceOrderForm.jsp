<%@ include file="/common/taglibs.jsp"%> 

<head>
	<title>Service Order Form</title>   
    <meta name="heading" content="Service Order Form"/> 
    <jsp:include flush="true" page="newSoScript.jsp"></jsp:include>  
</head>
<body>
<!-- tab Div -->
<div id="tab">
<jsp:include page="/common/tabs.jsp" flush="true"></jsp:include>
</div>
<!-- Detail Div -->
<div id="detail">
<s:form id="serviceOrderForm" name="serviceOrderForm" action="saveServiceOrder" onsubmit="return toCheckCountryFields();" method="post" validate="true" >
	<input type="hidden" name="tempId1" id="tempId1" value="">
	<s:hidden name="quesansIds" id="aqid" value=""/>
	<s:hidden name="serviceOrder.isSOExtract"/>
	<input type="hidden" name="tempId2" id="tempId2" value="">
	<s:hidden name="corpID" value="${customerFile.corpID}" />
	<s:hidden name="cfContract" value="${customerFile.contract}" />
	<s:hidden name="tempJob" />
	<configByCorp:fieldVisibility componentId="component.field.Alternative.Division">
	<s:hidden name="divisionFlag" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.SO.Extarct">
	<s:hidden name="isDecaExtract" value="1"/>
	</configByCorp:fieldVisibility>
		<configByCorp:fieldVisibility componentId="component.field.Alternative.actgCodeFlag">
		<s:hidden name="actgCodeFlag" value="YES" />
	</configByCorp:fieldVisibility>
	<s:hidden name="customerFileJob" value="%{customerFile.job}"/>  
	<c:set var="customerFileJob" value="${customerFile.job}"/>
	<s:hidden name="addAddressState" /> 
	<s:hidden name="pricingButton" value="<%=request.getParameter("pricingButton") %>"/>
    <c:set var="pricingButton"  value="<%=request.getParameter("pricingButton") %>"/>
    <s:hidden name="redirectPricingButton" value="<%=request.getParameter("redirectPricingButton") %>"/>
    <c:set var="redirectPricingButton"  value="<%=request.getParameter("redirectPricingButton") %>"/> 
	<s:hidden name="forwarding" value="<%= request.getParameter("forwarding")%>"/>
	<s:hidden name="sofid" value="<%= request.getParameter("sid")%>"/>
	<s:hidden name="unit" id ="unit" value="%{dummyDistanceUnit}" />	
	<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}" />
	<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy" />
	       <s:hidden id="relocationServicesKey" name="relocationServicesKey" value="" />
            <s:hidden id="relocationServicesValue" name="relocationServicesValue"  />
            <c:set var="relocationServicesKey" value="" />
            <c:set var="relocationServicesValue" value="" /> 
            <c:forEach var="entry" items="${relocationServices}">
            <c:if test="${relocationServicesKey==''}">
            <c:if test="${entry.key==serviceOrder.serviceType}">
            <c:set var="relocationServicesKey" value="${entry.key}" />
            <c:set var="relocationServicesValue" value="${entry.value}" /> 
            </c:if>
           </c:if> 
           </c:forEach>
     <s:hidden name="weightTab"  id= "weightTab" value="1"/>
     <s:hidden name="weightTabSub1"  id= "weightTabSub1" value="1"/>
     <s:hidden name="weightTabSub2"  id= "weightTabSub2" value="1"/>       
	<s:hidden name="serviceOrder.customerFileId" value="%{customerFile.id}"/>
	<s:hidden name="serviceOrder.isNetworkRecord" />
	<s:hidden name="serviceOrder.networkSO" />
	<s:hidden name="serviceOrder.bookingAgentShipNumber" />
	<s:hidden name="serviceOrder.ugwIntId" />
	<s:hidden name="accessAllowed" />
	<s:hidden name="transferDateFlag" />
	<s:hidden name="serviceOrder.id" value="%{serviceOrder.id}" />
	<s:hidden name="soid" value="${serviceOrder.id}" />
	<s:hidden name="serviceOrder.defaultAccountLineStatus" /> 
	<s:hidden name="serviceOrder.bookingAgentCheckedBy"value="%{serviceOrder.bookingAgentCheckedBy}" />
	<s:hidden name="vanlineseqNum"  />
	<s:hidden name="miscellaneous.status"  value="%{miscellaneous.status}" />
	<s:hidden name="shipSize" />
	<s:hidden name="minShip" />
	<s:hidden name="countShip"/>	
	<s:hidden name="serviceOrder.revised" /> 
	<s:hidden name="serviceOrder.carrierDeparture" /> 
	<s:hidden name="serviceOrder.carrierArrival" />
	<s:hidden name="serviceOrder.brokerCode" />
	<s:hidden name="serviceOrder.originAgentCode" />
	<s:hidden name="serviceOrder.originSubAgentCode" />
	<s:hidden name="serviceOrder.destinationAgentCode" />
	<s:hidden name="checkConditionForOriginDestin" />
	 <s:hidden name="stdAddDestinState" />
	 <s:hidden name="stdAddOriginState" />
	 <s:hidden name="defaultTemplate" id="defaultTemplate"/>
	 <s:hidden name="originCountryFlex3Value"  id="originCountryFlex3Value"/>
	 <s:hidden name="DestinationCountryFlex3Value" id="DestinationCountryFlex3Value" />
	 <configByCorp:fieldVisibility componentId="component.field.ServiceOrder.checkCompanyBAA">
	<s:hidden name="checkCompanyBA" value="YES"/>
	</configByCorp:fieldVisibility>
	 <configByCorp:fieldVisibility componentId="component.field.Description.ChargeCode">
	<s:hidden name="setDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
	<configByCorp:fieldVisibility componentId="component.field.Description.DefaultChargeCode">
	<s:hidden name="setDefaultDescriptionChargeCode" value="YES"/>
	</configByCorp:fieldVisibility>
	 <c:if test="${not empty miscellaneous.assignDate}">
		 <s:text id="trackingStatusSitDestinationAFormattedValueassignDate" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.assignDate" /></s:text>
			 <s:hidden  name="miscellaneous.assignDate" value="%{trackingStatusSitDestinationAFormattedValueassignDate}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.assignDate}">
		 <s:hidden   name="miscellaneous.assignDate"/> 
	 </c:if>
	 <c:if test="${serviceOrder.job =='RLO'}">
	 <s:hidden  name="reloServiceType" value="${serviceOrder.serviceType}" />
	 </c:if>
	 <c:if test="${serviceOrder.job !='RLO'}">
	 <s:hidden  name="reloServiceType" value="" />
	 </c:if>
      <s:hidden name="serviceOrder.grpID"/>
	 <c:if test="${not empty miscellaneous.settledDate}">
		 <s:text id="customerFileSettledDateFormattedValue" name="${FormDateValue}"> <s:param name="value" value="miscellaneous.settledDate" /></s:text>
			 <s:hidden  name="miscellaneous.settledDate" value="%{customerFileSettledDateFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty miscellaneous.settledDate}">
		 <s:hidden   name="miscellaneous.settledDate"/> 
	 </c:if>	 
	  <c:if test="${not empty serviceOrder.redskyBillDate}">
		 <s:text id="customerFileSurveyFormattedValue" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.redskyBillDate" /></s:text>
			 <s:hidden  name="serviceOrder.redskyBillDate" value="%{customerFileSurveyFormattedValue}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.redskyBillDate}">
		 <s:hidden   name="serviceOrder.redskyBillDate"/> 
	 </c:if>
	 	<s:hidden name="serviceOrder.quoteAccept"/> 
	 <s:hidden  name="serviceOrder.incExcServiceType"/>
	 <s:hidden  name="servIncExcDefault"/> 
	<s:hidden name="serviceOrder.destinationSubAgentCode" />
	<s:hidden name="serviceOrder.vendorCode" />
	<s:hidden name="serviceOrder.forwarderCode" />
	<s:hidden name="miscellaneous.unit1"/>
    <s:hidden name="miscellaneous.unit2"/>
    <s:hidden name="miscellaneous.totalActualRevenue" />	
	<s:hidden name="oldStatus" value="%{serviceOrder.status}" /> 
	<s:hidden name="jobtypeSO" value="%{serviceOrder.job}" /> 
	<s:hidden name="routingSO" value="${serviceOrder.routing}" />
    <s:hidden name="modeSO" value="${serviceOrder.mode}"/>
    <s:hidden name="packingModeSO" value="${serviceOrder.packingMode}"/>
    <s:hidden name="commoditySO" value="${serviceOrder.commodity}"/>
    <s:hidden name="serviceTypeSO" value="${serviceOrder.serviceType}"/>
    <s:hidden name="companyDivisionSO" value="${serviceOrder.companyDivision}"></s:hidden>
    <s:hidden name="accountIdCheck"/>
	<s:hidden name="jobtypeCheck" value="" /> 
	<s:hidden name="accId" />
    <s:hidden name="accEstimateQuantity" />
    <s:hidden name="accEstimateRate" />
    <s:hidden name="accEstimateExpense" />
    <s:hidden name="accEstimateSellRate" />
    <s:hidden name="accEstimateRevenueAmount" />
    <s:hidden name="accEstSellLocalAmount" />
	<s:hidden name="oldStatusNumber" value="%{serviceOrder.statusNumber}" /> 
	<s:hidden  name="billing.contract" />
	<s:hidden name="salesCommisionRate" value="${salesCommisionRate}"/>
	<s:hidden name="grossMarginThreshold" value="${grossMarginThreshold}"/>
	<s:hidden name="inEstimatedTotalExpense" value="${serviceOrder.estimatedTotalExpense}"/>
	<s:hidden name="inEstimatedTotalRevenue" value="${serviceOrder.estimatedTotalRevenue}"/>
	<s:hidden name="inRevisedTotalExpense" value="${serviceOrder.revisedTotalExpense}"/>
	<s:hidden name="inRevisedTotalRevenue" value="${serviceOrder.revisedTotalRevenue}"/>
	<s:hidden name="inActualExpense" value="${serviceOrder.actualExpense}"/>
	<s:hidden name="inActualRevenue" value="${serviceOrder.actualRevenue}"/>
	<s:hidden name="inComptetive" value="${customerFile.comptetive}"/>
	<s:hidden name="miscellaneous.tripNumber"/>
	<s:hidden name="buyDependSell" />
	<s:hidden name="serviceOrder.CAR_vendorCode" />
	<s:hidden name="serviceOrder.COL_vendorCode" />
	<s:hidden name="serviceOrder.TRG_vendorCode" />
	<s:hidden name="serviceOrder.HOM_vendorCode" />
	<s:hidden name="serviceOrder.RNT_vendorCode" />
	<s:hidden name="serviceOrder.SET_vendorCode" />
	<s:hidden name="serviceOrder.LAN_vendorCode" />
	<s:hidden name="serviceOrder.MMG_vendorCode" />
	<s:hidden name="serviceOrder.ONG_vendorCode" />
	<s:hidden name="serviceOrder.PRV_vendorCode" />
	<s:hidden name="serviceOrder.AIO_vendorCode" />
	<s:hidden name="serviceOrder.EXP_vendorCode" />
	<s:hidden name="serviceOrder.RPT_vendorCode" />
	<s:hidden name="serviceOrder.SCH_schoolSelected" />
	<s:hidden name="serviceOrder.TAX_vendorCode" />
	<s:hidden name="serviceOrder.TAC_vendorCode" />
	<s:hidden name="serviceOrder.TEN_vendorCode" />
	<s:hidden name="serviceOrder.VIS_vendorCode" />
	<s:hidden name="serviceOrder.WOP_vendorCode" />
	<s:hidden name="serviceOrder.REP_vendorCode" />
 <s:hidden name="serviceOrder.RLS_vendorCode" />
 <s:hidden name="serviceOrder.CAT_vendorCode" />
 <s:hidden name="serviceOrder.CLS_vendorCode" />
 <s:hidden name="serviceOrder.CHS_vendorCode" />
 <s:hidden name="serviceOrder.DPS_vendorCode" />
 <s:hidden name="serviceOrder.HSM_vendorCode" />
 <s:hidden name="serviceOrder.PDT_vendorCode" />
 <s:hidden name="serviceOrder.RCP_vendorCode" />
 <s:hidden name="serviceOrder.SPA_vendorCode" />
 <s:hidden name="serviceOrder.TCS_vendorCode" />
  <s:hidden name="serviceOrder.MTS_vendorCode" />
 <s:hidden name="serviceOrder.DSS_vendorCode" />
 <s:hidden name="serviceOrder.customerRating" />
 <s:hidden name="userQuoteServices" />
	<s:if test="${serviceOrder.job =='RLO'}">
	 <s:hidden name="serviceOrder.estimator"></s:hidden>	
	</s:if>
	<c:if test="${not empty serviceOrder.id}">
	<c:if test="${not empty billing.billComplete}">
	 <s:text id="billingBillingComplete" name="${FormDateValue}"> <s:param name="value" value="billing.billComplete" /></s:text>
	 <s:hidden  name="billing.billComplete" value="%{billingBillingComplete}" />  
	</c:if>
	<c:if test="${empty billing.billComplete}">
	<s:hidden  name="billing.billComplete" />
	</c:if>
	<s:hidden name="billing.billCompleteA" value="%{billing.billCompleteA}" />
	<c:if test="${not empty trackingStatus.deliveryA}">
	<s:text id="trackingStatusDeliveryA" name="${FormDateValue}"> <s:param name="value" value="trackingStatus.deliveryA" /></s:text>
	<s:hidden  name="trackingStatus.deliveryA" value="%{trackingStatusDeliveryA}" />  
	</c:if>
	<c:if test="${empty trackingStatus.deliveryA}">
	<s:hidden  name="trackingStatus.deliveryA" />  
	</c:if>
	<s:hidden  name="claimCount" value="${claimCount}"/> 
	<s:hidden name="invoiceCount" value="${invoiceCount}"/> 
	</c:if>
	<c:if test="${not empty serviceOrder.id && serviceOrder.job=='RLO'}">
	<c:if test="${not empty dspDetails.serviceCompleteDate}">
	 <s:text id="dspDetailsServiceCompleteDate" name="${FormDateValue}"> <s:param name="value" value="dspDetails.serviceCompleteDate" /></s:text>
	 <s:hidden  name="dspDetails.serviceCompleteDate" value="%{dspDetailsServiceCompleteDate}" />
	</c:if>
	<c:if test="${empty dspDetails.serviceCompleteDate}">
	<s:hidden  name="dspDetails.serviceCompleteDate" />
	</c:if>
	</c:if>
	<s:hidden name="calOpener" value="notOPen" />
	<c:if test="${not empty serviceOrder.ETD}">
		 <s:text id="serviceOrderETD" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ETD" /></s:text>
		<s:hidden name="serviceOrder.ETD" value="%{serviceOrderETD}" />  
	 </c:if>
	 <c:if test="${empty serviceOrder.ETD}">
		 <s:hidden name="serviceOrder.ETD"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.ATD}">
		 <s:text id="serviceOrderATD" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ATD" /></s:text>
			 <s:hidden  name="serviceOrder.ATD" value="%{serviceOrderATD}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ATD}">
		 <s:hidden   name="serviceOrder.ATD"/> 
	 </c:if>
	<c:if test="${not empty serviceOrder.ETA}">
		 <s:text id="serviceOrderETA" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ETA" /></s:text>
			 <s:hidden  name="serviceOrder.ETA" value="%{serviceOrderETA}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ETA}">
		 <s:hidden name="serviceOrder.ETA"/> 
	 </c:if>
	 <c:if test="${not empty serviceOrder.ATA}">
		 <s:text id="serviceOrderATA" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.ATA" /></s:text>
			 <s:hidden  name="serviceOrder.ATA" value="%{serviceOrderATA}" /> 
	 </c:if>
	 <c:if test="${empty serviceOrder.ATA}">
		 <s:hidden   name="serviceOrder.ATA"/> 
	 </c:if>		
	<s:hidden name="vanlineCodeForRef"  />  
	<s:hidden name="id" value="<%=request.getParameter("id") %>" />
	<s:hidden id="countOriginDetailNotes" name="countOriginDetailNotes" value="<%=request.getParameter("countOriginDetailNotes") %>" />
	<s:hidden id="countDestinationDetailNotes" name="countDestinationDetailNotes" value="<%=request.getParameter("countDestinationDetailNotes") %>" />
	<s:hidden id="countWeightDetailNotes" name="countWeightDetailNotes"	 value="<%=request.getParameter("countWeightDetailNotes") %>" />
	<s:hidden id="countVipDetailNotes" name="countVipDetailNotes"  	value="<%=request.getParameter("countVipDetailNotes") %>" />
	<s:hidden id="countServiceOrderNotes" name="countServiceOrderNotes" value="<%=request.getParameter("countServiceOrderNotes") %>" />
	<s:hidden id="countCustomerFeedbackNotes" name="countCustomerFeedbackNotes" value="<%=request.getParameter("countCustomerFeedbackNotes") %>" />
	<c:set var="countOriginDetailNotes"
		value="<%=request.getParameter("countOriginDetailNotes") %>" />
	<c:set var="countDestinationDetailNotes"
		value="<%=request.getParameter("countDestinationDetailNotes") %>" />
	<c:set var="countWeightDetailNotes"
		value="<%=request.getParameter("countWeightDetailNotes") %>" />
	<c:set var="countVipDetailNotes"
		value="<%=request.getParameter("countVipDetailNotes") %>" />
	<c:set var="countServiceOrderNotes"
		value="<%=request.getParameter("countServiceOrderNotes") %>" />
	<c:set var="countCustomerFeedbackNotes"
		value="<%=request.getParameter("countCustomerFeedbackNotes") %>" />
	<c:set var="from" value="<%=request.getParameter("from") %>"/>
	<c:set var="field" value="<%=request.getParameter("field") %>"/>
	<s:hidden name="field" value="<%=request.getParameter("field") %>" />
	<s:hidden name="field1" value="<%=request.getParameter("field1") %>" />
	<c:set var="field1" value="<%=request.getParameter("field1") %>"/> 
	<s:hidden name="gotoPageString" id="gotoPageString" value="" />
	<s:hidden name="formStatus" value="" />

	<div class="spn">&nbsp;</div>
	<div style="padding-bottom:0px;!padding-bottom:6px;"></div>
<div id="Layer1" onkeydown="changeStatus();" style="width:100%">
<table border="0" cellpadding="0" cellspacing="0" width="100%" >
	<tr>
	<td>
  		<div id="content" align="center" >
<div id="liquid-round-top">
    <div class="top"><span></span></div>
    <div class="center-content">
<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%" >
	<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0" width="912px" >
				<tbody>
				<tr>
						<td height="5px"></td>
						</tr>
					<tr>
						<td align="right" class="listwhitetext" style="width:85px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.prefix' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom">Title / Rank</td>
						<td align="left" class="listwhitetext" width="5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.firstName' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="center" class="listwhitetext" valign="bottom"><fmt:message key='serviceOrder.mi' /></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><fmt:message	key='serviceOrder.lastName' /><font color="red" size="2">*</font></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="left" class="listwhitetext" valign="bottom"><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber"><fmt:message key='serviceOrder.socialSecurityNumber'/></configByCorp:fieldVisibility></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" class="listwhitetext" style="width:5px"></td>
						<td align="right" width="66" valign="middle" class="listwhitetext" rowspan="2">
						<%-- Added by kunal for ticket number: 6092 and 6218(later) --%>
						<c:set var="isVipFlag" value="false" />
						<c:if test="${serviceOrder.vip}">
							<c:set var="isVipFlag" value="true" />
						</c:if>
						<div style="position:absolute;top:10px;margin-left:20px;!margin-left:-35px;">
						<img id="vipImage" src="${pageContext.request.contextPath}/images/vip_icon.png" />
						</div>
						</td>
						</tr>
					<tr>
						<td align="left" style="width:85px"></td>
						<td align="left" style="width:5px"><s:textfield cssClass="input-textUpper" name="serviceOrder.prefix" size="1" maxlength="15" onfocus="myDate();" readonly="true" tabindex="1"/></td>
						<td align="left" style="width:3px">
						<td align="left" class="listwhitetext" ><s:textfield cssClass="input-textUpper" name="serviceOrder.rank" size="7" maxlength="7" readonly="true" tabindex="2"/> </td>
						<td align="left" style="width:3px">
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.firstName" size="28" maxlength="80" readonly="true" tabindex="3" /></td>
						<td align="left" style="width:3px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper" name="serviceOrder.mi" cssStyle="width:13px" maxlength="1" readonly="true" tabindex="4" /></td>
						<td align="left" style="width:3px"></td>
						<td align="left"><s:textfield cssClass="input-textUpper upper-case" onblur="titleCase(this)" onkeypress="" name="serviceOrder.lastName" size="29" maxlength="80" required="true" readonly="true" tabindex="5" /></td>
						<td align="left" style="width:3px"></td>
						     <td align="left" class="listwhitetext" ><configByCorp:fieldVisibility componentId="customerFile.socialSecurityNumber">
					        <s:textfield cssClass="input-textUpper" key="serviceOrder.socialSecurityNumber" size="7" maxlength="9" readonly="true" onkeydown="return onlyAlphaNumericAllowed(event, this, 'special')" tabindex="6"/>
					        </configByCorp:fieldVisibility></td>
						<td align="right"><s:checkbox id="serviceOrder.vip" name="serviceOrder.vip" value="${isVipFlag}" fieldValue="true" onchange="changeStatus();" onclick="setVipImage();" tabindex="7" /></td>
						<td align="left" class="listwhitebox" valign="middle"><fmt:message key='serviceOrder.vip' /></td>
						<td width="85px"></td>
						<c:if test="${empty serviceOrder.id}">
							<td width="90px" align="left"><img src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
						</c:if>
						<c:if test="${not empty serviceOrder.id}">
							<c:choose>
								<c:when
									test="${countVipDetailNotes == '0' || countVipDetailNotes == '' || countVipDetailNotes == null}">
									<td width="90px" align="left"><img id="countVipDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_empty1.jpg" HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:when>
								<c:otherwise>
									<td width="90px" align="left"><img id="countVipDetailNotesImage" src="${pageContext.request.contextPath}/images/notes_open1.jpg" HEIGHT=17 WIDTH=50
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);" /><a
										onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=VipPerson&imageId=countVipDetailNotesImage&fieldId=countVipDetailNotes&decorator=popup&popup=true',800,600);"></a></td>
								</c:otherwise>
							</c:choose>
						</c:if>
					</tr>
					</tbody>
					</table>
					</td>
					</tr>
					<tr>
					 <td>
					   <table class="detailTabLabel" cellspacing="0" cellpadding="0" border="0">
						<tbody>
						<tr>
						<td height="5px"></td>
						</tr>
							<tr>
								<td style="width:85px"></td>
								<td align="left" class="listwhitetext">S/O#</td>
								<td align="right" class="listwhitetext" style="width:7px"></td>								
								<td align="left" class="listwhitetext">Status</td>
								<c:if test="${checkAccessQuotation}">
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" class="listwhitetext" valign="bottom"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.quoteStatus',this);return false" onmouseout="ajax_hideTooltip()"><fmt:message key='serviceOrder.quoteStatus'/></a></td>								
								</c:if>
								</c:if>
								<td align="right" class="listwhitetext" style="width:7px"></td>
								<td align="left" class="listwhitetext">Status Date</td>
								<td align="left" class="listwhitetext" style="width:7px"></td>
								<td id="reasonHid" align="left" class="listwhitetext">Status Reason</td>
								<td id="reasonHid2" align="left" class="listwhitetext">On Hold Until</td>
							</tr>
							</tr>
							<tr>
								<td align="left" style="width:85px"></td>
								<td align="left" style="width:7px">
								<s:textfield cssClass="input-textUpper" name="serviceOrder.shipNumber" size="22" maxlength="15" readonly="true" tabindex="8" /></td>
								<td align="left" style="width:7px"></td>
								<c:if test="${ empty customerFile.moveType || customerFile.moveType=='BookedMove'}">
								<td><s:select cssClass="list-menu" name="serviceOrder.status" list="%{JOB_STATUS}" headerKey=""	headerValue=""  onchange=" changeStatus(), checkStatus();" cssStyle="width:100px" tabindex="9" />&nbsp;&nbsp;</td>
						          </c:if>
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td><s:select cssClass="list-menu" name="serviceOrder.status" list="%{JOB_STATUS}" headerKey=""	headerValue="" disabled="true" onchange=" changeStatus(), checkStatus();"  cssStyle="width:100px" tabindex="9" />&nbsp;&nbsp;</td>
								</c:if>
								<c:if test="${checkAccessQuotation}">							
								<c:if test="${not empty customerFile.moveType && customerFile.moveType=='Quote'}">
								<td align="left" style=""><s:select cssClass="list-menu" key="serviceOrder.quoteStatus"  list="%{QUOTESTATUS}" cssStyle="width:100px" onchange="checkQuotation();changeStatus();" tabindex="8" /></td>
								</c:if>
								
								</c:if>
								<c:if test="${not empty serviceOrder.statusDate}">
									<s:text id="statusDate" name="${FormDateValue}"> <s:param name="value" value="serviceOrder.statusDate"  />
									</s:text>
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" value="%{statusDate}" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="9" /></td>
								</c:if>
								<c:if test="${empty serviceOrder.statusDate}">
									<td align="left" style="width:7px"></td>
									<td><s:textfield cssClass="input-text" name="serviceOrder.statusDate" cssStyle="width:65px" maxlength="11" readonly="true" tabindex="9" /></td>
								</c:if>
								<td align="left" class="listwhitetext"></td>
								<td id="reasonHid1"><s:select cssClass="list-menu" key="serviceOrder.statusReason" list="%{statusReason}" onchange="changeStatus();" cssStyle="width:150px" tabindex="10"/></td>
								<td id="reasonHid3">
									<c:if test="${not empty serviceOrder.nextCheckOn}">
										<s:text id="trackingStatusNextCheckOnFormattedValue" name="${FormDateValue}"><s:param name="value" value="serviceOrder.nextCheckOn"/></s:text>
										<s:textfield cssClass="input-text" id="nextCheckOn" name="serviceOrder.nextCheckOn" value="%{trackingStatusNextCheckOnFormattedValue}" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/>
											<img id="nextCheckOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>	
									<c:if test="${empty serviceOrder.nextCheckOn}">
										<s:textfield cssClass="input-text" id="nextCheckOn" name="serviceOrder.nextCheckOn" required="true" size="7" maxlength="11" onkeydown="return onlyDel(event,this)"/><img id="nextCheckOn-trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/></td>
									</c:if>
								</td>
								<script type="text/javascript">
                                 cheackStatusReasonLoad();
                             	</script>                              
							</tr>
						</tbody>
					</table>
					</td>
					</tr>
					<tr>
						<td height="5px"></td>
						</tr>
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">Origin&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
									<td align="left" class="listwhitetext" style="width:7px"></td>
									<td align="left" class="listwhitetext">Destination&nbsp;City/State</td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext" style="">Country</td>
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:85px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" name="serviceOrder.originCityCode" size="22" maxlength="30" readonly="true" tabindex="11" /></td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext">
									<s:textfield cssClass="input-textUpper" name="serviceOrder.originCountryCode" cssStyle="width:37px" maxlength="30" readonly="true" tabindex="12" /></td>
									<td align="right" class="listwhitetext" style="width:40px"></td>
									<td align="left" class="listwhitetext" >
									<s:textfield cssClass="input-textUpper" key="serviceOrder.destinationCityCode" size="22" maxlength="30" readonly="true" tabindex="13" /></td>
									<td width="7px"></td>
									<td align="left" class="listwhitetext"><s:textfield cssClass="input-textUpper" name="serviceOrder.destinationCountryCode" cssStyle="width:37px" maxlength="30" readonly="true" tabindex="14" /></td>
									<td width="165px"></td>									
									<c:if test="${not empty serviceOrder.id}">
							 		<c:if test="${jobTypes !='UVL' && jobTypes !='MVL' && jobTypes !='ULL' && jobTypes !='MLL' && jobTypes !='DOM' && jobTypes !='LOC' && serviceOrder.originCountryCode != serviceOrder.destinationCountryCode}">
									<td><a><img src="images/viewcustom2.jpg" onclick="findCustomInfo(this);" alt="Customs" title="Customs" /></a></td>
						     		</c:if>
						    		</c:if>
						    
						     		<c:if test="${not empty serviceOrder.id}">
							 		<td><a><img src="images/viewdoc2.jpg" onclick="findDocInfo(this);" alt="Docs" title="Docs" /></a></td>
						     		</c:if>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>					
						<tr>
						<td>
						<table class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
							<tbody>
							<tr>
							<td style="width:85px; !width: 85px"></td>
							<td align="left" class="listwhitetext">Code</td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px">Name</td>
							<td></td>
							<td></td>
							<td align="left" class="listwhitetext" style="width:10px"><c:if  test="${companies == 'Yes'}">Company&nbsp;Division<font color="red" size="2">*</font></c:if></td>
							</tr>
								<tr>
									<td align="right" class="listwhitetext" style="">Booking&nbsp;Agent:&nbsp;</td>
									<td><s:textfield cssClass="input-text" id="soBookingAgentCodeId" name="serviceOrder.bookingAgentCode" size="23" maxlength="8" onchange="valid(this,'special');showPartnerAlert('onchange',this.value,'noteNavigations');findBookingAgentName(), getVanlineCode('AG', this);" onblur="findCompanyDivisionByBookAg('onchange');showPartnerAlert('onchange',this.value,'noteNavigations');" onselect="getVanlineCode('AG', this);showPartnerAlert('onchange',this.value,'noteNavigations');" tabindex="15"/></td>
									<td style="min-width:5px"><img class="openpopup" width="" height="" id="serviceOrder.bookingAgentCode.img" onclick="openBookingAgentPopWindow(); document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].select();" src="<c:url value='/images/open-popup.gif'/>" /></td>
									<td align="left"><s:textfield cssClass="input-text" id="soBookingAgentNameId" name="serviceOrder.bookingAgentName" onkeyup="findPartnerDetails('soBookingAgentNameId','soBookingAgentCodeId','soBookingAgentNameDivId',' and (isAccount=true or isAgent=true or isVendor=true )');"  size="44" maxlength="250" tabindex="16" />
									<div id="soBookingAgentNameDivId" class="autocomplete" style="z-index:9999;position:absolute;margin-top:2px;"></div>
									</td>
									<td align="left">
										<a><img class="openpopup" id="noteNavigations" onclick="getBigPartnerAlert(document.forms['serviceOrderForm'].elements['serviceOrder.bookingAgentCode'].value,'noteNavigations');" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Note List" title="Note List" /></a> 
									</td>
									<td width="12px"></td>
									<c:if  test="${companies == 'Yes'}">
										<td align="left"><s:select cssClass="list-menu" id="companyDivision" name="serviceOrder.companyDivision" list="%{companyDivis}"  cssStyle="width:106px" onchange="getJobList('unload');changeStatus();checkTransferDate(); changebAgentCheckedBy();" headerKey="" headerValue="" tabindex="17" /></td>
									</c:if>
									<c:if  test="${companies != 'Yes'}">
										<s:hidden name="serviceOrder.companyDivision"/>
									</c:if>
								</tr>
							</tbody>
						</table>
						</td>
					</tr>
					<configByCorp:fieldVisibility componentId="component.link.trackingStatus.vanline">
						<tr>
							<td>
								<table id="hidVanLine" class="detailTabLabel" border="0" cellpadding="0" cellspacing="0" >
									<tbody>
										<tr>
												<td height="5px"></td>
										</tr>
										<tr>
												<td style="width:85px; !width: 85px"></td>
												<td align="left" class="listwhitetext">Booking Agent Vanline Code</td>
										</tr>
									 	<tr>
												<td style="width:85px; !width: 85px"></td>
												<td><s:textfield cssClass="input-text" key="serviceOrder.bookingAgentVanlineCode" size="23" maxlength="8" onchange="valid(this,'special')getVanlineAgent('BA');" tabindex="18"/></td>
												<td colspan="2" align="left">
													<a><img class="openpopup" id="navigations" onclick="getVanlineCode('AG', this)" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Vanline List" title="Vanline List" /></a> 
												</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</configByCorp:fieldVisibility>
					<tr>
						<td>
						<table class="detailTabLabel" border="0	" cellpadding="0" cellspacing="0" width="875px">
							<tbody>
							<tr><td height="5px"></td></tr>
									<tr> 
									 
									<td align="right" class="listwhitetext" style="width:85px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
									<td align="left" class="listwhitetext" valign="bottom" style=""><div id="registrationHideLabel">
									<fmt:message key='serviceOrder.registrationNumber' /></div></td>
									<td align="right" class="listwhitetext" style="width:15px"></td>
									<td align="left" class="listwhitetext" valign="bottom" style="">
									<fmt:message key='serviceOrder.job' /><font color="red" size="2">*</font></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="routingLabel"><a href="#" style="cursor:help" onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.routing',this);return false">
										<fmt:message key='serviceOrder.routing' /></a></div></td>
									<td align="right" class="listwhitetext" style=""></td>
								</tr>
								<tr>
									<td align="right">
									<div id="hidRegNum">
									<img id="rateImage"  class="openpopup" width="16" height="16" align="top" src="${pageContext.request.contextPath}/images/arrow-loadern.gif" onclick="findJobSeq()"/> 
									</div>
									</td> 
									<td align="left" style=""><div id="registrationHide"><s:textfield cssClass="input-text" name="serviceOrder.registrationNumber" id="regist" onchange="validateRegistraionNumberAC(this.id)" onkeyup="valid(this,'special')"
										size="23" maxlength="20" required="true" tabindex="19" /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>									
									<td align="left" style="width:200px">
									<s:select id="jobService" cssClass="list-menu" key="serviceOrder.job" list="%{job}"
										cssStyle="width:251px" headerKey="" headerValue=""
										onchange="populateInclusionExclusionData('noDefault');findService();changeStatus();validateRegistrationNumber();getNewCoordAndSale();checkJobType(this);getCommodity();getServiceTypeByJob();fillCommodity();showMmCounselorByJob();checkJobTypes();"  onclick="fillCheckJob(this);" tabindex="20"/></td>
									<td align="right" class="listwhitetext" style="width:10px"></td>
									<script type="text/javascript">
									try{
                                     checkJobTypeOnLoad();
									}catch(e){}
                                    </script>
									<td align="left" ><div id="routingFild"> <s:select cssClass="list-menu" name="serviceOrder.routing" list="%{routing}" cssStyle="width:145px" onchange="populateInclusionExclusionData('noDefault');changeStatus();" tabindex="21" /></div></td>
								<td width="5px"></td>
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="labelEstimator"><fmt:message key='serviceOrder.estimator' />
									<div</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="labelservice"><fmt:message key='serviceOrder.serviceType' /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><div id="modeLabel"> <a
										href="#" style="cursor:help"
										onclick="ajax_showTooltip('findtoolTip.html?ajax=1&decorator=simple&popup=true&tableFieldName=serviceOrder.mode',this);return false"
										><fmt:message
										key='serviceOrder.mode' /></a> </div>  </td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
								</tr>
								<tr>
									<td align="left" style="width:15px"></td>
									<td align="left" style="width:10px">
									<div id="EstimatorService1">
									<s:select cssClass="list-menu" name="serviceOrder.estimator"
										list="%{sale}" id="estimator" cssStyle="width:145px" headerKey=""
										headerValue="" onchange="changeStatus();" tabindex="22" />
									</div>
										</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left">
									<div id="Service1">
									<s:select cssClass="list-menu" name="serviceOrder.serviceType" list="%{service}" cssStyle="width:251px" onchange="changeStatus();" tabindex="23" />
								    </div>
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left">
									 <div id="modeFild">
									<s:select cssClass="list-menu" key="serviceOrder.mode" list="%{mode}"
										cssStyle="width:145px" onchange="populateInclusionExclusionData('noDefault');validateDistanceUnit();changeStatus();getCarrier();fillUsaFlag();"
										tabindex="24" /> </div>
									</td>
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.flagCarrier">
										<td align="right" class="listwhitetext" id="carrierFildLabel">
          <fmt:message key='trackingStatus.flagCarrier'/>&nbsp</td>
         <td align="left" colspan="2"><s:select cssClass="list-menu" id="carrierFild"  list="%{flagCarrierList}" name="serviceOrder.serviceOrderFlagCarrier"  headerKey="" headerValue="" cssStyle="width:80px"  onchange="changeStatus();" tabindex="25s"/></td>
   			</configByCorp:fieldVisibility>	
								</tr>
                                <tr><td height="2px"></td></tr>
						<tr>
							<td width="85px"></td>
							<td align="left" colspan="15">
							<table id="Service2" style="margin:0px;padding:0px;" cellpadding="0" cellspacing="0">
							<tr>							
							<td class="listwhitetext" align="left">Relocation Services</td>
							</tr>
							<tr>							
							<td valign="top">
							<fieldset style="margin:0px 0 0 0; padding:2px 0 0 2px;">                      
                        									<table class="detailTabLabel" cellspacing="1" cellpadding="0" border="0">
                        									<tr>
                          										<c:forEach var="entry" items="${reloService}" varStatus="rowCounter">
                          										<c:if test="${entry.value!=''}">    
                          										<td class="listwhitetext" style="width:650px"><input type="checkbox" style="vertical-align:middle;" name="checkV" id="${entry.key}" value="${entry.key}" onclick="selectColumn(this)" />${entry.value} &nbsp;</td>
                          										</c:if>
                          										<c:choose>
														          <c:when test="${rowCounter.count % 4 == 0 && rowCounter.count != 0}">
														          </tr><tr>
														          </c:when>
														         <c:when test="${rowCounter.count != -1}">
														          </c:when>
															          <c:otherwise>
															          </c:otherwise>
															        </c:choose>
                          										</c:forEach>
    														   </tr>             										
                          									</table> 
                          	</fieldset>
							</td>
							</tr>
							</table>
                          	</td>
						</tr>
						<tr><td height="4px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom" ><div id="hidStatusSalesLabel"><fmt:message
										key='serviceOrder.salesMan' /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom">
									<div id="hidStatusCommoditLabel"><fmt:message key='serviceOrder.commodity' /><font color="red" size="2">*</font></div>
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><div id="packLabel"> <fmt:message
          key='serviceOrder.packingMode' /></div></td>
          
										<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
									<td align="left" class="listwhitetext" valign="bottom" colspan="4">Packing Service</td>
									</configByCorp:fieldVisibility>
								</tr>
								<tr>
									<td align="left" style="width:25px"></td>
									<td ><div id="hidStatusSalesText"><s:select cssClass="list-menu"
										name="serviceOrder.salesMan" id="salesMan" list="%{estimatorList}"
										cssStyle="width:145px" headerKey="" headerValue=""
										onchange="changeStatus();checkSalesMan();" tabindex="25" /></div></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<c:choose>
										<c:when test="${serviceOrder.job =='STO' || serviceOrder.job =='STL' ||serviceOrder.job =='STF' ||serviceOrder.job =='TPS'}">
											<td align="left"><div id="hidStatusCommoditText"><s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodits}" cssStyle="width:251px"
												onchange="changeStatus();" tabindex="26" /></div></td>
										</c:when>
										<c:otherwise>
											<td align="left"><div id="hidStatusCommoditText"><s:select
												cssClass="list-menu" name="serviceOrder.commodity"
												list="%{commodit}" cssStyle="width:250px"
												onchange="validAutoBoat(),changeStatus();" tabindex="26" /></div></td>
										</c:otherwise>
									</c:choose>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left">
									<div id="packFild"><s:select cssClass="list-menu" name="serviceOrder.packingMode"
										list="%{pkmode}" cssStyle="width:145px"
										onchange="changeStatus();" tabindex="27" /></div>
								    </td>								 	
								 	<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.packingService">
								 	<td align="left" colspan="4">
									<s:select cssClass="list-menu" name="serviceOrder.packingService"
										list="%{packingServiceList}" cssStyle="width:145px"
										onchange="changeStatus();" tabindex="27" />
										</td>
										</configByCorp:fieldVisibility>
										
								</tr>
								<tr><td height="5px"></td></tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:77px"></td>
									<td align="left" class="listwhitetext" valign="bottom"><fmt:message
										key='serviceOrder.coordinator' /><font color="red" size="2">*</font></td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" style="">
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
									<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
									<tr>	
									<td align="left" class="listwhitetext" width="5"><fmt:message key='miscellaneous.millitaryShipment'/></td>
									<td  width="19"></td>
									<td align="left" class="listwhitetext" width="5">Shipment&nbsp;Type</td>
									</tr>
									</table>
									</configByCorp:fieldVisibility>									
									</td>
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" width="" id="mmCounselorLabel">MM&nbsp;Counselor</td>
									<td align="left" class="listwhitetext" valign="bottom"></td>
									<td align="left" class="listwhitetext" style=""></td>
									
								</tr>
								<tr>
									<td align="right" class="listwhitetext" style="width:25px"></td>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.coordinator" id="coordinator" list="%{coordinatorList}"
										cssStyle="width:145px" headerKey="" headerValue=""
										onchange="changeStatus();" tabindex="28" /></td>									
									<td align="right" class="listwhitetext" style="width:5px"></td>
									<td align="left" class="listwhitetext" style="">
									<configByCorp:fieldVisibility componentId="component.tab.serviceOrder.millitaryShipment">
									<table cellpadding="0" cellspacing="0" border="0" style="margin:0px;padding:0px;">
									<tr>		                            
	                                <td width="70px">
	                                <s:select cssClass="list-menu" name="miscellaneous.millitaryShipment" list="%{yesno}" cssStyle="width:67px" onchange="changeStatus();" tabindex="29" />
	                                </td>
	                                <td width="25px"></td>
	                                <td width="70px">
	                                <s:select cssClass="list-menu" name="serviceOrder.shipmentType" list="%{shipmentType}" headerKey="" headerValue="" cssStyle="width:110px" onchange="changeStatus();" tabindex="30" />
	                                </td>
									<td width="5px"></td>
									</tr>
									</table>
									</configByCorp:fieldVisibility>	
									</td>
									<td align="right" class="listwhitetext" style="width:25px"></td>
									<td><s:select cssClass="list-menu"
										name="serviceOrder.mmCounselor" id="mmCounselor" list="%{coordinatorList}"
										cssStyle="width:145px" headerKey="" headerValue=""
										onchange="changeStatus();" /></td>								
									<td align="left"></td>
									<td align="left" class="listwhitetext" >Groupage&nbsp;<s:checkbox name="serviceOrder.grpPref" tabindex="31" cssStyle="vertical-align: middle;margin:0px;" onchange="" /></td> 
									
									<td width=""></td>
								<c:if test="${empty serviceOrder.id}">
										<td align="right"><img
											src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
											HEIGHT=17 WIDTH=50 onclick="notExists();" /></td>
									</c:if>
									<c:if test="${not empty serviceOrder.id}">
										<c:choose>
											<c:when
												test="${countServiceOrderNotes == '0' || countServiceOrderNotes == '' || countServiceOrderNotes == null}">
												<td align="right"><img id="countServiceOrderNotesImage"
													src="${pageContext.request.contextPath}/images/notes_empty1.jpg"
													HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:when>
											<c:otherwise>
												<td align="right"><img id="countServiceOrderNotesImage"
													src="${pageContext.request.contextPath}/images/notes_open1.jpg"
													HEIGHT=17 WIDTH=50
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);" /><a
													onclick="javascript:openWindow('notess.html?id=${serviceOrder.id }&notesId=${serviceOrder.shipNumber }&noteFor=ServiceOrder&subType=ServiceOrder&imageId=countServiceOrderNotesImage&fieldId=countServiceOrderNotes&decorator=popup&popup=true',800,600);"></a></td>
											</c:otherwise>
										</c:choose>
									</c:if>
								</tr>
								<tr><td height="5px"></td></tr>
							</tbody>
						</table>
					</tr>
					</td>
    <jsp:include flush="true" page="serviceOrderFormSec.jsp"></jsp:include>					
<c:if test="${not empty serviceOrder.id}">
<c:if test="${pricingButton=='yes'}"> 
<c:redirect url="/editServiceOrderUpdate.html?id=${serviceOrder.id}&redirectPricingButton=yes"/>
</c:if></c:if>
</s:form> 
</div>
</body>