package com.trilasoft.app.service;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.XmlParser;

public interface XmlParserManager extends GenericManager<XmlParser, Long> {
	public String saveData(String sessionCorpID, String file, String userName);

	public String saveNokRate(String sessionCorpID, String file, String userName);
	//public String saveUsdRate(String sessionCorpID, String file, String userName);
	//public String saveGBPRate(String sessionCorpID, String file, String userName);
	//public String saveCNYRate(String sessionCorpID, String file, String userName);
	//public String saveINRRate(String sessionCorpID, String file, String userName);
	//public String saveBRLRate(String sessionCorpID, String file, String userName);
	//public String saveKRWRate(String sessionCorpID, String file, String userName);
	public String saveCADRate(String sessionCorpID, String file, String userName);
	//public String saveSGDRate(String sessionCorpID, String file, String userName);
	//public String saveJODRate(String sessionCorpID, String file, String userName);
	//public String saveIDRRate(String sessionCorpID, String file, String userName);
	public String saveDynamicRate(String sessionCorpID, String file, String userName,String baseCurrency);
}
