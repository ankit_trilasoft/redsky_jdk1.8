package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;
import com.opensymphony.xwork2.Preparable;


public class AjaxTestAction extends BaseAction implements Preparable {
    private static final long serialVersionUID = 378605805550104346L;

    private List<User> persons;

    private Long id;
    Date currentdate = new Date();

    static final Logger logger = Logger.getLogger(AjaxTestAction.class);

    @Override
    public String execute() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	log.debug("just getting the stuf");
        persons = (List<User>) getRequest().getAttribute("persons");
        if (persons == null) {
            log.debug("just ones please");
            persons = userManager.getUsers(null);
            getRequest().setAttribute("persons", persons);
        } else {
            log.debug("persons" + persons.size());
        }
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }

    public List<User> getPersons() {
        return persons;
    }

    public void setPersons(List<User> persons) {
        this.persons = persons;
    }

    public String remove() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        log.debug("do some removing here when i feel like it id:" + id);
        if (persons != null) {
            log.debug("before persons" + persons.size());
            persons.remove((id.intValue() - 1));
            log.debug("after persons" + persons.size());
        }
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }

    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        log.debug("do some saving here when i feel like it");
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return execute();
    }

    public String ajax() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
        log.debug("ajax is doing something id:"+id);
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return "ajax";
    }

    public String edit() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	log.debug("do some editing here when i feel like it");
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return execute();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void prepare() throws Exception {
        log.debug("i'm getting prepared!!!");

    }
}
