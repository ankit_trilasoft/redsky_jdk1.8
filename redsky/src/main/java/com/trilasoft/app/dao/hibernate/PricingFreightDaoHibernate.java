package com.trilasoft.app.dao.hibernate;

import java.math.BigDecimal;
import java.util.List;
import org.appfuse.dao.hibernate.GenericDaoHibernate;
import com.trilasoft.app.dao.PricingFreightDao;
import com.trilasoft.app.model.PricingFreight;

public class PricingFreightDaoHibernate extends GenericDaoHibernate<PricingFreight, Long> implements PricingFreightDao {

	public PricingFreightDaoHibernate() {
		super(PricingFreight.class);
	}

	public List checkById(Long id) {
		return null;
	}

	public List getFreightList(Long pricingControlId) {
		String sql="select r.carrier, originPortCode, destinationPortCode,reffreightId,pf.id as pricingFreightId  " +
				"FROM reffreightrates r, pricingfreight pf " +
				"where r.id=pf.reffreightId and pf.pricingControlId="+pricingControlId+"";
		
		List freightList= this.getSession().createSQLQuery(sql).list();
		return freightList;
	}

	public List getMarkUpValue(BigDecimal price, String contract, String mode) {
		String markupSQl="select concat(profitFlat,'#', profitPercent)  from freightmarkupcontrol where "+price+">=lowCost and "+price+"<=highCost and contract='"+contract+"' and mode='"+mode+"'";
		return this.getSession().createSQLQuery(markupSQl).list();
	}

	public List getPricingFreightRecord(Long id, String sessionCorpID) {
		return getHibernateTemplate().find("from PricingFreight where pricingControlID="+id+" and freightSelection=true"); 
	}

}
