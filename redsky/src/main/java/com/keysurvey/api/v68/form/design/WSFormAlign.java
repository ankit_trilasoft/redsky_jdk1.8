/**
 * WSFormAlign.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.design;

public class WSFormAlign implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected WSFormAlign(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _LEFT = "LEFT";
    public static final java.lang.String _HORIZONTAL_CENTER = "HORIZONTAL_CENTER";
    public static final java.lang.String _RIGHT = "RIGHT";
    public static final java.lang.String _NON_VISIBLE_LEFT = "NON_VISIBLE_LEFT";
    public static final java.lang.String _NON_VISIBLE_HORIZONTAL_CENTER = "NON_VISIBLE_HORIZONTAL_CENTER";
    public static final java.lang.String _NON_VISIBLE_RIGHT = "NON_VISIBLE_RIGHT";
    public static final java.lang.String _JUSTIFY = "JUSTIFY";
    public static final java.lang.String _NON_VISIBLE_JUSTIFY = "NON_VISIBLE_JUSTIFY";
    public static final java.lang.String _TOP = "TOP";
    public static final java.lang.String _BASELINE = "BASELINE";
    public static final java.lang.String _VERTICAL_CENTER = "VERTICAL_CENTER";
    public static final java.lang.String _BOTTOM = "BOTTOM";
    public static final java.lang.String _NON_VISIBLE_TOP = "NON_VISIBLE_TOP";
    public static final java.lang.String _NON_VISIBLE_VERTICAL_CENTER = "NON_VISIBLE_VERTICAL_CENTER";
    public static final java.lang.String _NON_VISIBLE_BASELINE = "NON_VISIBLE_BASELINE";
    public static final java.lang.String _NON_VISIBLE_BOTTOM = "NON_VISIBLE_BOTTOM";
    public static final WSFormAlign LEFT = new WSFormAlign(_LEFT);
    public static final WSFormAlign HORIZONTAL_CENTER = new WSFormAlign(_HORIZONTAL_CENTER);
    public static final WSFormAlign RIGHT = new WSFormAlign(_RIGHT);
    public static final WSFormAlign NON_VISIBLE_LEFT = new WSFormAlign(_NON_VISIBLE_LEFT);
    public static final WSFormAlign NON_VISIBLE_HORIZONTAL_CENTER = new WSFormAlign(_NON_VISIBLE_HORIZONTAL_CENTER);
    public static final WSFormAlign NON_VISIBLE_RIGHT = new WSFormAlign(_NON_VISIBLE_RIGHT);
    public static final WSFormAlign JUSTIFY = new WSFormAlign(_JUSTIFY);
    public static final WSFormAlign NON_VISIBLE_JUSTIFY = new WSFormAlign(_NON_VISIBLE_JUSTIFY);
    public static final WSFormAlign TOP = new WSFormAlign(_TOP);
    public static final WSFormAlign BASELINE = new WSFormAlign(_BASELINE);
    public static final WSFormAlign VERTICAL_CENTER = new WSFormAlign(_VERTICAL_CENTER);
    public static final WSFormAlign BOTTOM = new WSFormAlign(_BOTTOM);
    public static final WSFormAlign NON_VISIBLE_TOP = new WSFormAlign(_NON_VISIBLE_TOP);
    public static final WSFormAlign NON_VISIBLE_VERTICAL_CENTER = new WSFormAlign(_NON_VISIBLE_VERTICAL_CENTER);
    public static final WSFormAlign NON_VISIBLE_BASELINE = new WSFormAlign(_NON_VISIBLE_BASELINE);
    public static final WSFormAlign NON_VISIBLE_BOTTOM = new WSFormAlign(_NON_VISIBLE_BOTTOM);
    public java.lang.String getValue() { return _value_;}
    public static WSFormAlign fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        WSFormAlign enumeration = (WSFormAlign)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static WSFormAlign fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSFormAlign.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://design.form.v68.api.keysurvey.com", "WSFormAlign"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
