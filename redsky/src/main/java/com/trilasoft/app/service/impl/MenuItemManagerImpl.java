package com.trilasoft.app.service.impl;

import java.util.Collection;
import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.MenuItemDao;
import com.trilasoft.app.model.MenuItem;
import com.trilasoft.app.service.MenuItemManager;



public class MenuItemManagerImpl extends GenericManagerImpl<MenuItem, Long> implements MenuItemManager {
	MenuItemDao menuItemDao; 
    
	 
    public MenuItemManagerImpl(MenuItemDao menuItemDao) { 
        super(menuItemDao); 
        this.menuItemDao = menuItemDao; 
    }

    public List<MenuItem> findByCorpID(String corpId){
    	return menuItemDao.findByCorpID(corpId);
    }
    public List findPermissions(String recipient,String corpId,String parentN,String menuN,String titleName,String urlN) {
		return menuItemDao.findPermissions(recipient,corpId,parentN,menuN,titleName,urlN);
	}

	public List countForMenuItem(Long id) {
		return menuItemDao.countForMenuItem(id);
	}
	
	public Boolean checkPermissions(Long menuItemId,int permission, String recipient,String sessionCorpID){
		return menuItemDao.checkPermissions(menuItemId, permission, recipient,sessionCorpID);
	}
	public Boolean permissionExists(Long menuItemId,int permission, String recipient,String sessionCorpID,String url){
		return menuItemDao.permissionExists(menuItemId, permission, recipient,sessionCorpID,url);
	}

	public List <MenuItem>getMenuList(String corpId) {
		return menuItemDao.getMenuList(corpId);
	}
	public List allMenuItems(){
		return menuItemDao.allMenuItems();
	}
	public List findCorpId(){
		return menuItemDao.findCorpId();	
	}

	public List findMenuItem(String corp, String parName, String nm, String tit) {
		return menuItemDao.findMenuItem(corp, parName, nm, tit);
	}

	public List findParent() {
		return menuItemDao.findParent();
	}

	public List findSeqNumList(String corp, String parent) {
			return menuItemDao.findSeqNumList(corp, parent);
		}
	public List checkTitleAvailability(String corp, String parent,String titleN){
		return menuItemDao.checkTitleAvailability(corp, parent,titleN);
		
	}
	public List findParByCorp(String corp){
		return menuItemDao.findParByCorp(corp);
	}

	public Collection findPageActions(String corp, String menuItemUrl) {
		return menuItemDao.findPageActions(corp,menuItemUrl);
	}
	public List checkRolePermission(String url,String corpId){
		return menuItemDao.checkRolePermission(url, corpId);
	}
	public List getPageActionListByUrl(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl){
		return menuItemDao.getPageActionListByUrl(url, menuName, corpId,pageMenu,pageDesc,pageUrl);
	}
	public List getPageActionListByUrlNew(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl){
		return menuItemDao.getPageActionListByUrlNew(url, menuName, corpId, pageMenu, pageDesc, pageUrl);
	}
	public List findPagePermissions(String url,String menuName,String corpId,String pageMenu,String pageDesc,String pageUrl,String role){
		return menuItemDao.findPagePermissions(url, menuName, corpId,pageMenu,pageDesc,pageUrl,role);
	}
	public void updatePageActionPermission(Long pageId,Long menuId,int permission,String corpId,String role,String url){
		menuItemDao.updatePageActionPermission(pageId, menuId, permission,corpId,role,url);
	}
	public List getModuleActionListByURL(String module,String role,String corpId,String moduleDescr,String moduleUrl){
		return menuItemDao.getModuleActionListByURL(module, role, corpId,moduleDescr,moduleUrl);
	}
	public void updateModuleActionPermission(String sidL,String corpId,String oldSidL){
		menuItemDao.updateModuleActionPermission(sidL,corpId,oldSidL);
	}
	public List getModuleList(String corpId){
		return menuItemDao.getModuleList(corpId);
	}
	public void updatePageActionListByUrl(Long id,int permission,String corpId,String role){
		menuItemDao.updatePageActionListByUrl(id, permission, corpId,role);
	}
	public Long getMenuItemPermissionId(Long menuId,String role,String corpId){
		return menuItemDao.getMenuItemPermissionId(menuId, role, corpId);
	}
	public Boolean pagePermissionExists(Long menuItemPermissionId, int permission,String recipient,String childFlag,String corpId){
		return menuItemDao.pagePermissionExists(menuItemPermissionId, permission, recipient,childFlag,corpId);
	}
	public String getPageActionIdByUrl(String url,String menuName,String corpId){
		return menuItemDao.getPageActionIdByUrl(url, menuName, corpId);
	}
	public void updatePageActionPermissionByList(String idList,Long menuItemPermisionid,int permisson,String role,String sessionCorpID){
		menuItemDao.updatePageActionPermissionByList(idList, menuItemPermisionid, permisson,role,sessionCorpID);
	}
	public List listAllMenuItems(String corpId,String parentN,String menuN,String titleName,String urlN,String pageUrl){
		return menuItemDao.listAllMenuItems(corpId,parentN,menuN,titleName,urlN,pageUrl);
	}
	public void updateMenuActionPermissionByList(Long menuId,String role,String sessionCorpID,int permission,String url){
		menuItemDao.updateMenuActionPermissionByList(menuId, role, sessionCorpID, permission,url);
	}
	public Boolean isSecurityEnabled(String corpId){
		return menuItemDao.isSecurityEnabled(corpId);
	}
	public void updatePageActionUrlList(String pageDescription,String sessionCorpId,String pageMethod,String pageClass,String pageModule,String pageUrl,String menuId,String pageId){
		menuItemDao.updatePageActionUrlList(pageDescription, sessionCorpId, pageMethod, pageClass, pageModule, pageUrl, menuId, pageId);
	}
	public String getPageRecById(String pageId){
		return menuItemDao.getPageRecById(pageId);
	}
	public void removePageActionUrlList(String pageId){
		menuItemDao.removePageActionUrlList(pageId);
	}
	public void updateCurrentUrlPermission(String sessionCorpID){
		menuItemDao.updateCurrentUrlPermission(sessionCorpID);
	}
}
