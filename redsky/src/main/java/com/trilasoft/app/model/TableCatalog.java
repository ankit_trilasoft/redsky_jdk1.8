package com.trilasoft.app.model;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
@Entity
@Table(name="tablecatalog")



public class TableCatalog extends BaseObject{
	
	private Long id;
	private String tableName;
	private String tableDescription;
	private Integer totalField;
	private String foreignkeyField1;
	private String foreignkeyTable1;
	private String foreignkeyField2;
	private String foreignkeyTable2;
	private String foreignkeyField3;
	private String foreignkeyTable3;
	private String foreignkeyField4;
	private String foreignkeyTable4;
	private String foreignkeyField5;
	private String foreignkeyTable5;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id)
				.append("tableName", tableName)
				.append("tableDescription", tableDescription)
				.append("totalField", totalField)
				.append("foreignkeyField1", foreignkeyField1)
				.append("foreignkeyTable1", foreignkeyTable1)
				.append("foreignkeyField2", foreignkeyField2)
				.append("foreignkeyTable2", foreignkeyTable2)
				.append("foreignkeyField3", foreignkeyField3)
				.append("foreignkeyTable3", foreignkeyTable3)
				.append("foreignkeyField4", foreignkeyField4)
				.append("foreignkeyTable4", foreignkeyTable4)
				.append("foreignkeyField5", foreignkeyField5)
				.append("foreignkeyTable5", foreignkeyTable5)
				.append("createdOn", createdOn).append("createdBy", createdBy)
				.append("updatedOn", updatedOn).append("updatedBy", updatedBy)
				.toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof TableCatalog))
			return false;
		TableCatalog castOther = (TableCatalog) other;
		return new EqualsBuilder().append(id, castOther.id)
				.append(tableName, castOther.tableName)
				.append(tableDescription, castOther.tableDescription)
				.append(totalField, castOther.totalField)
				.append(foreignkeyField1, castOther.foreignkeyField1)
				.append(foreignkeyTable1, castOther.foreignkeyTable1)
				.append(foreignkeyField2, castOther.foreignkeyField2)
				.append(foreignkeyTable2, castOther.foreignkeyTable2)
				.append(foreignkeyField3, castOther.foreignkeyField3)
				.append(foreignkeyTable3, castOther.foreignkeyTable3)
				.append(foreignkeyField4, castOther.foreignkeyField4)
				.append(foreignkeyTable4, castOther.foreignkeyTable4)
				.append(foreignkeyField5, castOther.foreignkeyField5)
				.append(foreignkeyTable5, castOther.foreignkeyTable5)
				.append(createdOn, castOther.createdOn)
				.append(createdBy, castOther.createdBy)
				.append(updatedOn, castOther.updatedOn)
				.append(updatedBy, castOther.updatedBy).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(tableName)
				.append(tableDescription).append(totalField)
				.append(foreignkeyField1).append(foreignkeyTable1)
				.append(foreignkeyField2).append(foreignkeyTable2)
				.append(foreignkeyField3).append(foreignkeyTable3)
				.append(foreignkeyField4).append(foreignkeyTable4)
				.append(foreignkeyField5).append(foreignkeyTable5)
				.append(createdOn).append(createdBy).append(updatedOn)
				.append(updatedBy).toHashCode();
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column(length=45)	
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	@Column(length=450)
	public String getTableDescription() {
		return tableDescription;
	}
	public void setTableDescription(String tableDescription) {
		this.tableDescription = tableDescription;
	}
	@Column(length=5)	
	public Integer getTotalField() {
		return totalField;
	}
	public void setTotalField(Integer totalField) {
		this.totalField = totalField;
	}
	@Column(length=45)
	public String getForeignkeyField1() {
		return foreignkeyField1;
	}
	public void setForeignkeyField1(String foreignkeyField1) {
		this.foreignkeyField1 = foreignkeyField1;
	}
	@Column(length=45)
	public String getForeignkeyTable1() {
		return foreignkeyTable1;
	}
	public void setForeignkeyTable1(String foreignkeyTable1) {
		this.foreignkeyTable1 = foreignkeyTable1;
	}
	@Column(length=45)
	public String getForeignkeyField2() {
		return foreignkeyField2;
	}
	public void setForeignkeyField2(String foreignkeyField2) {
		this.foreignkeyField2 = foreignkeyField2;
	}
	@Column(length=45)
	public String getForeignkeyTable2() {
		return foreignkeyTable2;
	}
	public void setForeignkeyTable2(String foreignkeyTable2) {
		this.foreignkeyTable2 = foreignkeyTable2;
	}
	@Column(length=45)
	public String getForeignkeyField3() {
		return foreignkeyField3;
	}
	public void setForeignkeyField3(String foreignkeyField3) {
		this.foreignkeyField3 = foreignkeyField3;
	}
	@Column(length=45)
	public String getForeignkeyTable3() {
		return foreignkeyTable3;
	}
	public void setForeignkeyTable3(String foreignkeyTable3) {
		this.foreignkeyTable3 = foreignkeyTable3;
	}
	@Column(length=45)
	public String getForeignkeyField4() {
		return foreignkeyField4;
	}
	public void setForeignkeyField4(String foreignkeyField4) {
		this.foreignkeyField4 = foreignkeyField4;
	}
	@Column(length=45)
	public String getForeignkeyTable4() {
		return foreignkeyTable4;
	}
	public void setForeignkeyTable4(String foreignkeyTable4) {
		this.foreignkeyTable4 = foreignkeyTable4;
	}
	@Column(length=45)
	public String getForeignkeyField5() {
		return foreignkeyField5;
	}
	public void setForeignkeyField5(String foreignkeyField5) {
		this.foreignkeyField5 = foreignkeyField5;
	}
	@Column(length=45)
	public String getForeignkeyTable5() {
		return foreignkeyTable5;
	}
	public void setForeignkeyTable5(String foreignkeyTable5) {
		this.foreignkeyTable5 = foreignkeyTable5;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column(length=45)
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column(length=45)
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	

}
