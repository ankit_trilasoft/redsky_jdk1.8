/**
 * WSFormStatistics.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.keysurvey.api.v68.form.result;

public class WSFormStatistics  extends com.keysurvey.api.v68.WSObject  implements java.io.Serializable {
    private long abandonedMail;

    private long completeCount;

    private long completeMail;

    private long formId;

    private long incompleteCount;

    private long incompleteMail;

    private long respondentCount;

    private long viewCount;

    public WSFormStatistics() {
    }

    public WSFormStatistics(
           long versionId,
           long abandonedMail,
           long completeCount,
           long completeMail,
           long formId,
           long incompleteCount,
           long incompleteMail,
           long respondentCount,
           long viewCount) {
        super(
            versionId);
        this.abandonedMail = abandonedMail;
        this.completeCount = completeCount;
        this.completeMail = completeMail;
        this.formId = formId;
        this.incompleteCount = incompleteCount;
        this.incompleteMail = incompleteMail;
        this.respondentCount = respondentCount;
        this.viewCount = viewCount;
    }


    /**
     * Gets the abandonedMail value for this WSFormStatistics.
     * 
     * @return abandonedMail
     */
    public long getAbandonedMail() {
        return abandonedMail;
    }


    /**
     * Sets the abandonedMail value for this WSFormStatistics.
     * 
     * @param abandonedMail
     */
    public void setAbandonedMail(long abandonedMail) {
        this.abandonedMail = abandonedMail;
    }


    /**
     * Gets the completeCount value for this WSFormStatistics.
     * 
     * @return completeCount
     */
    public long getCompleteCount() {
        return completeCount;
    }


    /**
     * Sets the completeCount value for this WSFormStatistics.
     * 
     * @param completeCount
     */
    public void setCompleteCount(long completeCount) {
        this.completeCount = completeCount;
    }


    /**
     * Gets the completeMail value for this WSFormStatistics.
     * 
     * @return completeMail
     */
    public long getCompleteMail() {
        return completeMail;
    }


    /**
     * Sets the completeMail value for this WSFormStatistics.
     * 
     * @param completeMail
     */
    public void setCompleteMail(long completeMail) {
        this.completeMail = completeMail;
    }


    /**
     * Gets the formId value for this WSFormStatistics.
     * 
     * @return formId
     */
    public long getFormId() {
        return formId;
    }


    /**
     * Sets the formId value for this WSFormStatistics.
     * 
     * @param formId
     */
    public void setFormId(long formId) {
        this.formId = formId;
    }


    /**
     * Gets the incompleteCount value for this WSFormStatistics.
     * 
     * @return incompleteCount
     */
    public long getIncompleteCount() {
        return incompleteCount;
    }


    /**
     * Sets the incompleteCount value for this WSFormStatistics.
     * 
     * @param incompleteCount
     */
    public void setIncompleteCount(long incompleteCount) {
        this.incompleteCount = incompleteCount;
    }


    /**
     * Gets the incompleteMail value for this WSFormStatistics.
     * 
     * @return incompleteMail
     */
    public long getIncompleteMail() {
        return incompleteMail;
    }


    /**
     * Sets the incompleteMail value for this WSFormStatistics.
     * 
     * @param incompleteMail
     */
    public void setIncompleteMail(long incompleteMail) {
        this.incompleteMail = incompleteMail;
    }


    /**
     * Gets the respondentCount value for this WSFormStatistics.
     * 
     * @return respondentCount
     */
    public long getRespondentCount() {
        return respondentCount;
    }


    /**
     * Sets the respondentCount value for this WSFormStatistics.
     * 
     * @param respondentCount
     */
    public void setRespondentCount(long respondentCount) {
        this.respondentCount = respondentCount;
    }


    /**
     * Gets the viewCount value for this WSFormStatistics.
     * 
     * @return viewCount
     */
    public long getViewCount() {
        return viewCount;
    }


    /**
     * Sets the viewCount value for this WSFormStatistics.
     * 
     * @param viewCount
     */
    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof WSFormStatistics)) return false;
        WSFormStatistics other = (WSFormStatistics) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            this.abandonedMail == other.getAbandonedMail() &&
            this.completeCount == other.getCompleteCount() &&
            this.completeMail == other.getCompleteMail() &&
            this.formId == other.getFormId() &&
            this.incompleteCount == other.getIncompleteCount() &&
            this.incompleteMail == other.getIncompleteMail() &&
            this.respondentCount == other.getRespondentCount() &&
            this.viewCount == other.getViewCount();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        _hashCode += new Long(getAbandonedMail()).hashCode();
        _hashCode += new Long(getCompleteCount()).hashCode();
        _hashCode += new Long(getCompleteMail()).hashCode();
        _hashCode += new Long(getFormId()).hashCode();
        _hashCode += new Long(getIncompleteCount()).hashCode();
        _hashCode += new Long(getIncompleteMail()).hashCode();
        _hashCode += new Long(getRespondentCount()).hashCode();
        _hashCode += new Long(getViewCount()).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(WSFormStatistics.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://result.form.v68.api.keysurvey.com", "WSFormStatistics"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abandonedMail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "abandonedMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completeCount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "completeCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("completeMail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "completeMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("formId");
        elemField.setXmlName(new javax.xml.namespace.QName("", "formId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incompleteCount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incompleteCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("incompleteMail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "incompleteMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respondentCount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respondentCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("viewCount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "viewCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
