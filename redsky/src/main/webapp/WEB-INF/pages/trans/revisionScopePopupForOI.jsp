<%@ include file="/common/taglibs.jsp"%>  
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>
<title>Scope of Work Order</title>
<meta name="heading" content="Scope For Work Order" />
<style>
.pr-f11{font-family:arial,verdana;font-size:11px;}
.subcontent-tab {background: url("images/basic-inner-blue.png") repeat-x scroll 0 0 #3dafcb;border-bottom: 1px solid #3dafcb;    border-top: 1px solid #3dafcb; color: #fff;font-family: arial,verdana;font-size: 13px;font-weight: bold;
    height: 29px; padding: 1px 4px 3px;text-decoration: none;text-shadow:0 1px #444444;
}
</style>
</head>
<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jquery-1.8.2.js"></script> 
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
 	<title><fmt:message key="operationResource.title" /></title>
  <meta name="heading" content="<fmt:message key='operationResource.heading'/>" />
<script type="text/javascript" src="scripts/jquery-1.2.2.pack.js"></script>
<body>
<script language="javascript" type="text/javascript">
function limitText() {
	limitField=document.getElementById("scopeOfWorkOrder");
	limitCount=document.getElementById("countdown");
	limitNum=500;
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0,limitNum);
	} else {
		limitCount.value = limitNum - limitField.value.length;
	}
	}
function closeWIN(){
	window.opener.location.reload(true);
     window.close();
}
function setScopeOfWO(){
	var tempScopeOfWo = document.getElementById("scopeOfWorkOrder").value;
	var numberOfComputer = document.getElementById("numberOfComputer").value;
	var numberOfEmployee = document.getElementById("numberOfEmployee").value;
	var salesTax = document.getElementById("salesTax").value;
	if(salesTax == null || salesTax == '' || salesTax =='NaN'){
		salesTax=0.0;
	}
	var consumables = document.getElementById("consumables").value;
	if(consumables == null || consumables == '' ){
		consumables=0.00;
	}
	var aB5Surcharge = document.getElementById("aB5Surcharge").value;
	if(aB5Surcharge == null || aB5Surcharge == '' ){
		aB5Surcharge=0.00;
	}
	var consumablePercentageCheck = document.getElementById("revisionConsumablePercentage").checked;
	var editAB5Surcharge = document.getElementById("revisionAB5SurchargePercentageFlag").checked;
	var revisionScopeValue = "revisionTrue";
	 $.ajax({
		  type: "POST",
		  url: "setScopeOfWO.html?ajax=1&decorator=simple&popup=true",
		  data: { workorder: '${operationsIntelligence.workorder}',shipNumber:'${operationsIntelligence.shipNumber}', tempScopeOfWo:tempScopeOfWo,ticket:'${operationsIntelligence.ticket}',consumables:consumables,salesTax:salesTax,numberOfEmployee:numberOfEmployee,numberOfComputer:numberOfComputer,revisionScopeValue:revisionScopeValue,consumablePercentageCheck:consumablePercentageCheck,aB5Surcharge:aB5Surcharge,editAB5Surcharge:editAB5Surcharge},
		  success: function (data, textStatus, jqXHR) {
			 /*  showOrHide(0); */
			  closeWIN();
		  },
	    error: function (data, textStatus, jqXHR) {
			    	/* showOrHide(0); */
				}
		}
	 );
	
}

function onlyNumberAllowed(target) {
    var patterns = /[^0-9]/g;
    if (target.value.match(patterns) != null) {
        alert("Please Enter Only Numbers");
        document.getElementById(target.id).value = '';
        return false;
    }
}


function onlyFloatAllowed(target) {
    var letters = /[^0-9]/g;
    var decimal = /^[-+]?[0-9]+\.[0-9]+$/;
    if ((target.value.match(letters) != null)
            && (target.value.match(decimal) == null)) {
        alert("Please Enter Valid Value")
        document.getElementById(target.id).value = '';
        return false;
    }
}
function enableTextBox(checkBox)
{
	if(checkBox.checked){
		document.getElementById('consumables').readOnly =false;
		document.getElementById('revisionAB5SurchargePercentageFlag').readOnly =false;
	}
else {
	document.getElementById('consumables').readOnly =true;
	document.getElementById('revisionAB5SurchargePercentageFlag').readOnly =true;
}
}
</script>
<s:form action="" name="revisionScopePopupOI">
<table class="notesDetailTable" cellspacing="0" cellpadding="1" border="0" height="150" width="750" style="!margin-top:5px;margin-bottom:2px;">
	
	<tbody>
	<tr><td class="subcontent-tab"><font style="padding-left:10px ">Revision Scope of &nbsp;${operationsIntelligence.workorder}</font></td></tr>
		<tr>
		<td valign="top">			
	<table class="" cellspacing="0" cellpadding="0" border="0" style="width:100%;padding-top:10px;" >
	<tbody>
	<tr>
	<td style="width:11px;margin-top:10px;"> </td>
	<td>
	 <s:textarea cssStyle="width:420px;height:70px; " cssClass="input-text pr-f11" id="scopeOfWorkOrder" name="operationsIntelligence.revisionScopeOfWorkOrder" onkeydown="limitText()" onkeyup="limitText()"> 
	</s:textarea> 
	<div style="float:right;margin-right:10px;"><s:textfield readonly="true" id="countdown" name="countdown" value="${sizeOfScope}" cssStyle="width: 18px; font-size: 10px; background: none repeat scroll 0% 0% transparent; border: medium none; color: rgb(34, 34, 34);"  />characters&nbsp;left. </div>
	</td>
	</tr>
	</tbody>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
	<c:if test="${usertype != 'ACCOUNT' }">
	<table class="notesDetailTable" cellspacing="0" cellpadding="3" border="0" height="70" width="750" style="margin-bottom:10px; padding:4px 0px 0px 9px;">
    <tr>
    <td class="listwhitetext-hcrew"># Computers</td>
    <td class="listwhitetext-hcrew"># Employee</td>
    </tr>
    <tr><td>
    <s:textfield id="numberOfComputer" name="operationsIntelligence.revisionScopeOfNumberOfComputer" maxlength="6" cssClass="input-text pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield>
    </td>
    <td><s:textfield id="numberOfEmployee" name="operationsIntelligence.revisionScopeOfNumberOfEmployee" maxlength="6" cssClass="input-text pr-f11" onchange="onlyNumberAllowed(this)"></s:textfield></td>
    </tr>
    
     <tr>
    <td class="listwhitetext-hcrew">Sales Tax</td>
    <td class="listwhitetext-hcrew">Equipment Rental</td>
    <td class="listwhitetext-hcrew">&nbsp;Edit&nbsp;Equipment Rental</td>
    <td class="listwhitetext-hcrew">&nbsp; AB5 Surcharge</td>
    <td class="listwhitetext-hcrew">&nbsp;Edit&nbsp; AB5 Surcharge</td>
     </tr>
   
    <tr><td>
    <s:textfield id="salesTax" name="operationsIntelligence.revisionScopeOfSalesTax" maxlength="15" cssClass="input-text pr-f11" onchange="onlyFloat(this)"></s:textfield>
    </td>
    <td>
    <c:choose>
    <c:when test="${operationsIntelligence.type=='C' || operationsIntelligence.type=='V'}">
    	<s:textfield id="consumables" name="operationsIntelligence.revisionScopeOfConsumables" maxlength="15" cssClass="input-text pr-f11" readonly="readonly"></s:textfield>
    </c:when>
    <c:otherwise>
    	<s:textfield id="consumables" name="operationsIntelligence.revisionScopeOfConsumables" maxlength="15" cssClass="input-text pr-f11" onchange="onlyFloat(this)"></s:textfield>
    </c:otherwise>
    </c:choose>
    </td>
    <c:set var="revisionPercentFlag" value="false"/>
	<c:if test="${operationsIntelligence.revisionConsumablePercentage}">
		<c:set var="revisionPercentFlag" value="true"/>
	</c:if>
	<td align="left" width="" valign="top" >
	<c:choose>
    <c:when test="${operationsIntelligence.type!='C' && operationsIntelligence.type!='V'}">
    	<s:checkbox id="revisionConsumablePercentage" key="operationsIntelligence.revisionConsumablePercentage" value="${revisionPercentFlag}" cssStyle="margin-left:0px;" disabled="true"/>
    </c:when>
    <c:otherwise>
    	<s:checkbox id="revisionConsumablePercentage" key="operationsIntelligence.revisionConsumablePercentage" value="${revisionPercentFlag}"  cssStyle="margin-left:0px;" onclick="enableTextBox(this)"/>
    </c:otherwise>
    </c:choose>
       <td>
    <c:choose>
    <c:when test="${operationsIntelligence.type=='C' || operationsIntelligence.type=='V'}">
    	<s:textfield id="aB5Surcharge" name="operationsIntelligence.revisionScopeOfAB5Surcharge" maxlength="15" cssClass="input-text pr-f11" readonly="readonly"></s:textfield>
    </c:when>
    <c:otherwise>
    	<s:textfield id="aB5Surcharge" name="operationsIntelligence.revisionScopeOfAB5Surcharge" maxlength="15" cssClass="input-text pr-f11" onchange="onlyFloat(this)"></s:textfield>
    </c:otherwise>
    </c:choose>
    </td>
    <c:set var="revisionAB5SurchargeFlag" value="false"/>
	<c:if test="${operationsIntelligence.revisionaeditAB5Surcharge}">
		<c:set var="revisionAB5SurchargeFlag" value="true"/>
	</c:if>
	<td align="left" width="" valign="top" >
	<c:choose>
    <c:when test="${operationsIntelligence.type!='C' && operationsIntelligence.type!='V'}">
    	<s:checkbox id="revisionAB5SurchargePercentageFlag" key="operationsIntelligence.revisionaeditAB5Surcharge" value="${revisionAB5SurchargeFlag}" cssStyle="margin-left:0px;" disabled="true"/>
    </c:when>
    <c:otherwise>
    	<s:checkbox id="revisionAB5SurchargePercentageFlag" key="operationsIntelligence.revisionaeditAB5Surcharge" value="${revisionAB5SurchargeFlag}"  cssStyle="margin-left:0px;" onclick="enableTextBox(this)"/>
    </c:otherwise>
    </c:choose>
    </tr>
    <tbody>
   
    </tbody>
    </table>
	</c:if>
	<c:if test="${usertype == 'ACCOUNT' }">
	<s:hidden name="salesTax" id ="salesTax" value="%{salesTax}" />
	<s:hidden name="consumables" id ="consumables" value="%{consumables}" />
	<s:hidden name="numberOfEmployee" id ="numberOfEmployee" value="%{numberOfEmployee}" />
	<s:hidden name="numberOfComputer" id ="numberOfComputer" value="%{numberOfComputer}" />
	</c:if>
	
</s:form >
<input type="button"  Class="cssbutton" Style="width:55px;" id="okButton"  value="Save" onclick="setScopeOfWO();"/>
<input type="button"  Class="cssbutton" Style="width:60px; margin-left:10px;"  value="Cancel" onclick="self.close()"/>
</body>
