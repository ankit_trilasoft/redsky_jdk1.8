package com.trilasoft.app.model;

import java.util.Date;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class MaterialDto {
	
	private Long id;
	private String qty;
	private String actualQty;
	private String soldQty;
	private String actual;
	private String returned;
	private String descript;
	private String charge;
	private String otCharge;
	private String dtCharge;
	private String cost;
	private String packLabour;
	private String flag;
	private String type;
	private String checkType;
	private Long count;
	private String estHour;
	private String comments;
	private String branch;
	private String location;
	private String freeQty;
	private String totalCost;
	private String minLevel;
	private String ooprice;
	private String salesPrice;
	private String custName;
	private String emailId;
	private String customerName;
	private String payMethod;
	private Long html;
	private String unitCost;
	private String invoiceSeqNumber;
	private String contractRate;
	public String getContractRate() {
		return contractRate;
	}
	public void setContractRate(String contractRate) {
		this.contractRate = contractRate;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	private String maxResourceLimit;
	private String custCell;
	private String refferedBy;
	private Long equipMaterialsId;
	private String createdOn;
	private String division;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("qty", qty).append("comments",comments)
				.append("actualQty", actualQty).append("actual", actual)
				.append("returned", returned).append("descript", descript)
				.append("charge", charge).append("otCharge", otCharge)
				.append("dtCharge", dtCharge).append("cost", cost)
				.append("packLabour", packLabour).append("flag", flag)
				.append("type", type).append("checkType", checkType).append("estHour",estHour)
				.append("count", count).append("minLevel",minLevel).append("branch", branch).append("location", location).append("freeQty", freeQty).append("totalCost", totalCost).
				append("ooprice",ooprice).append("salesPrice",salesPrice).append("maxResourceLimit",maxResourceLimit).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof MaterialDto))
			return false;
		MaterialDto castOther = (MaterialDto) other;
		return new EqualsBuilder().append(id, castOther.id).append(comments,castOther.comments)
				.append(qty, castOther.qty).append(estHour,castOther.estHour)
				.append(actualQty, castOther.actualQty)
				.append(actual, castOther.actual)
				.append(returned, castOther.returned)
				.append(descript, castOther.descript)
				.append(charge, castOther.charge)
				.append(otCharge, castOther.otCharge)
				.append(dtCharge, castOther.dtCharge)
				.append(cost, castOther.cost)
				.append(packLabour, castOther.packLabour)
				.append(flag, castOther.flag).append(type, castOther.type)
				.append(checkType, castOther.checkType)
				.append(count, castOther.count)
				.append(location, castOther.location)
				.append(freeQty, castOther.freeQty)
				.append(totalCost, castOther.totalCost)
				.append(branch, castOther.branch)
				.append(minLevel, castOther.minLevel)
				.append(ooprice, castOther.ooprice)
				.append(salesPrice, castOther.salesPrice)
				.append("maxResourceLimit",maxResourceLimit)
				.isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(id).append(qty).append(actualQty).append(estHour).append(comments)
				.append(actual).append(returned).append(descript)
				.append(charge).append(otCharge).append(dtCharge).append(cost)
				.append(packLabour).append(flag).append(type).append(checkType)
				.append(count)
				.append(branch)
				.append(location)
				.append(freeQty)
				.append(totalCost)
				.append(minLevel)
				.append(ooprice)
				.append(salesPrice)
				.append(maxResourceLimit)
				.toHashCode();
	}
	public String getActual() {
		return actual;
	}
	public void setActual(String actual) {
		this.actual = actual;
	}
	public String getActualQty() {
		return actualQty;
	}
	public void setActualQty(String actualQty) {
		this.actualQty = actualQty;
	}
	public String getCharge() {
		return charge;
	}
	public void setCharge(String charge) {
		this.charge = charge;
	}
	public String getCost() {
		return cost;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public String getDescript() {
		return descript;
	}
	public void setDescript(String descript) {
		this.descript = descript;
	}
	public String getDtCharge() {
		return dtCharge;
	}
	public void setDtCharge(String dtCharge) {
		this.dtCharge = dtCharge;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getOtCharge() {
		return otCharge;
	}
	public void setOtCharge(String otCharge) {
		this.otCharge = otCharge;
	}
	public String getPackLabour() {
		return packLabour;
	}
	public void setPackLabour(String packLabour) {
		this.packLabour = packLabour;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public String getReturned() {
		return returned;
	}
	public void setReturned(String returned) {
		this.returned = returned;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCheckType() {
		return checkType;
	}
	public void setCheckType(String checkType) {
		this.checkType = checkType;
	}
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getEstHour() {
		return estHour;
	}
	public void setEstHour(String estHour) {
		this.estHour = estHour;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getFreeQty() {
		return freeQty;
	}
	public void setFreeQty(String freeQty) {
		this.freeQty = freeQty;
	}
	public String getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(String totalCost) {
		this.totalCost = totalCost;
	}
	public String getMinLevel() {
		return minLevel;
	}
	public void setMinLevel(String minLevel) {
		this.minLevel = minLevel;
	}
	public String getOoprice() {
		return ooprice;
	}
	public void setOoprice(String ooprice) {
		this.ooprice = ooprice;
	}
	public String getSalesPrice() {
		return salesPrice;
	}
	public void setSalesPrice(String salesPrice) {
		this.salesPrice = salesPrice;
	}
	public String getMaxResourceLimit() {
		return maxResourceLimit;
	}
	public void setMaxResourceLimit(String maxResourceLimit) {
		this.maxResourceLimit = maxResourceLimit;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getCustCell() {
		return custCell;
	}
	public void setCustCell(String custCell) {
		this.custCell = custCell;
	}
	public String getRefferedBy() {
		return refferedBy;
	}
	public void setRefferedBy(String refferedBy) {
		this.refferedBy = refferedBy;
	}
	public Long getEquipMaterialsId() {
		return equipMaterialsId;
	}
	public void setEquipMaterialsId(Long equipMaterialsId) {
		this.equipMaterialsId = equipMaterialsId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPayMethod() {
		return payMethod;
	}
	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}
	public String getSoldQty() {
		return soldQty;
	}
	public void setSoldQty(String soldQty) {
		this.soldQty = soldQty;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public Long getHtml() {
		return html;
	}
	public void setHtml(Long html) {
		this.html = html;
	}
	public String getUnitCost() {
		return unitCost;
	}
	public void setUnitCost(String unitCost) {
		this.unitCost = unitCost;
	}
	public String getInvoiceSeqNumber() {
		return invoiceSeqNumber;
	}
	public void setInvoiceSeqNumber(String invoiceSeqNumber) {
		this.invoiceSeqNumber = invoiceSeqNumber;
	}
	
	
	
	
	
	
}
	