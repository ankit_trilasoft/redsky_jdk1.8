package com.trilasoft.app.service.impl;

import java.util.List;
import java.util.Map;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.NotesDao;
import com.trilasoft.app.model.CustomerFile;
import com.trilasoft.app.model.Notes;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.NotesManager;

public class NotesManagerImpl extends GenericManagerImpl<Notes, Long> implements NotesManager {

	NotesDao notesDao;

	public NotesManagerImpl(NotesDao notesDao) {
		super(notesDao);
		this.notesDao = notesDao;
	}

	public List<Notes> findByNotes(String noteStatus, String noteSubType, String notesId) {
		return notesDao.findByNotes(noteStatus, noteSubType, notesId);
	}

	public List<Notes> findByReleventNotes(String noteStatus, String noteSubType, String noteId) {
		return notesDao.findByReleventNotes(noteStatus, noteSubType, noteId);
	}

	public List findMaxId() {
		return notesDao.findMaxId();	
	}

	public List getListByNotesId(String notesId, String subType) {
		return notesDao.getListByNotesId(notesId, subType);
	}

	public List getListByCustomerNumber(String customerNumber, String subType) {
		return notesDao.getListByCustomerNumber(customerNumber, subType);
	}

	public List countForNotes(String seqNum) {
		return notesDao.countForNotes(seqNum);
	}

	public List countForOriginNotes(String seqNum) {
		return notesDao.countForOriginNotes(seqNum);
	}

	public List countForDestinationNotes(String seqNum) {
		return notesDao.countForDestinationNotes(seqNum);
	}

	public List countForSurveyNotes(String seqNum) {
		return notesDao.countForSurveyNotes(seqNum);
	}

	public List countForVipNotes(String seqNum) {
		return notesDao.countForVipNotes(seqNum);
	}

	public List countForOriginDetailNotes(String shipNum) {
		return notesDao.countForOriginDetailNotes(shipNum);
	}

	public List countForDestinationDetailNotes(String shipNum) {
		return notesDao.countForDestinationDetailNotes(shipNum);
	}

	public List countForWeightDetailNotes(String shipNum) {
		return notesDao.countForWeightDetailNotes(shipNum);
	}

	public List countForVipDetailNotes(String seqNum) {
		return notesDao.countForVipDetailNotes(seqNum);
	}

	public List countForBillingNotes(String seqNum) {
		return notesDao.countForBillingNotes(seqNum);
	}

	public List countForEntitlementNotes(String seqNum) {
		return notesDao.countForEntitlementNotes(seqNum);
	}

	public List countForContactNotes(String seqNum) {
		return notesDao.countForContactNotes(seqNum);
	}

	public List countForSpouseNotes(String seqNum) {
		return notesDao.countForSpouseNotes(seqNum);
	}

	public List countForCportalNotes(String seqNum) {
		return notesDao.countForCportalNotes(seqNum);
	}

	public List countForServiceOrderNotes(String shipNum) {
		return notesDao.countForServiceOrderNotes(shipNum);
	}

	public List countForServiceNotes(Long id) {
		return notesDao.countForServiceNotes(id);
	}

	public List countForCarrierNotes(Long id) {
		return notesDao.countForCarrierNotes(id);

	}

	public List countForCategoryTypeNotes(Long id) {
		return notesDao.countForCategoryTypeNotes(id);
	}

	public List countForEntitleDetailNotes(Long id) {
		return notesDao.countForEntitleDetailNotes(id);
	}

	public List countForEstimateDetailNotes(Long id) {
		return notesDao.countForEstimateDetailNotes(id);
	}

	public List countForRevisionDetailNotes(Long id) {
		return notesDao.countForRevisionDetailNotes(id);
	}

	public List countForPayableDetailNotes(Long id) {
		return notesDao.countForPayableDetailNotes(id);
	}

	public List countForReceivableDetailNotes(Long id) {
		return notesDao.countForReceivableDetailNotes(id);
	}

	public List countForContainerNotes(Long id) {
		return notesDao.countForContainerNotes(id);
	}

	public List countForVehicleNotes(Long id) {
		return notesDao.countForVehicleNotes(id);
	}

	public List countForCartonNotes(Long id) {
		return notesDao.countForCartonNotes(id);
	}

	public List countOriginPackoutNotes(String shipNum) {
		return notesDao.countOriginPackoutNotes(shipNum);
	}

	public List countSitOriginNotes(String shipNum) {
		return notesDao.countSitOriginNotes(shipNum);
	}

	public List countOriginForwardingNotes(String shipNum) {
		return notesDao.countOriginForwardingNotes(shipNum);
	}

	public List countInterStateNotes(String shipNum) {
		return notesDao.countInterStateNotes(shipNum);
	}

	public List countGSTForwardingNotes(String shipNum) {
		return notesDao.countGSTForwardingNotes(shipNum);
	}

	public List countDestinationStatusNotes(String shipNum) {
		return notesDao.countDestinationStatusNotes(shipNum);
	}

	public List countSitDestinationNotes(String shipNum) {
		return notesDao.countSitDestinationNotes(shipNum);
	}

	public List countDestinationImportNotes(String shipNum) {
		return notesDao.countDestinationImportNotes(shipNum);
	}

	public List countServiceOrderNotes(String shipNum) {
		return notesDao.countServiceOrderNotes(shipNum);
	}
	
	public List<Notes> countNotesByShipNumAndSubType(String shipNum,String subType){
		return notesDao.countNotesByShipNumAndSubType(shipNum,subType);
	}

	public List countForClaimNotes(Long claimNum) {
		return notesDao.countForClaimNotes(claimNum);
	}

	public List countClaimValuationNotes(Long claimNum) {
		return notesDao.countClaimValuationNotes(claimNum);
	}

	public List countLossNotes(Long claimNum) {
		return notesDao.countLossNotes(claimNum);
	}

	public List countLossTicketNotes(Long claimNum) {
		return notesDao.countLossTicketNotes(claimNum);
	}

	public List countForDomesticNotes(String shipNum) {
		return notesDao.countForDomesticNotes(shipNum);
	}

	public List countForDomesticServiceNotes(String shipNum) {
		return notesDao.countForDomesticServiceNotes(shipNum);
	}

	public List countForDomesticStateCountryNotes(String shipNum) {
		return notesDao.countForDomesticStateCountryNotes(shipNum);
	}

	public List countForTicketBillingNotes(Long ticket) {
		return notesDao.countForTicketBillingNotes(ticket);
	}

	public List countForTicketStaffNotes(Long ticket) {
		return notesDao.countForTicketStaffNotes(ticket);
	}

	public List countForTicketSchedulingNotes(Long ticket) {
		return notesDao.countForTicketSchedulingNotes(ticket);
	}

	public List countForTicketOriginNotes(Long ticket) {
		return notesDao.countForTicketOriginNotes(ticket);
	}

	public List countForTicketDestinationNotes(Long ticket) {
		return notesDao.countForTicketDestinationNotes(ticket);
	}

	public List countForTicketNotes(Long ticket) {
		return notesDao.countForTicketNotes(ticket);
	}

	public List countTicketLocationNotes(Long ticket) {
		return notesDao.countTicketLocationNotes(ticket);
	}

	public List countTicketStorageNotes(Long ticket) {
		return notesDao.countTicketStorageNotes(ticket);
	}

	public List countBillingAuthorizedNotes(String shipNum) {
		return notesDao.countBillingAuthorizedNotes(shipNum);
	}

	public List countBillingValuationNotes(String shipNum) {
		return notesDao.countBillingValuationNotes(shipNum);
	}

	public List countBillingSecondaryNotes(String shipNum) {
		return notesDao.countBillingSecondaryNotes(shipNum);
	}

	public List countBillingPrivatePartyNotes(String shipNum) {
		return notesDao.countBillingPrivatePartyNotes(shipNum);
	}

	public List countBillingPrimaryNotes(String shipNum) {
		return notesDao.countBillingPrimaryNotes(shipNum);
	}

	public List countBillingDomesticNotes(String shipNum) {
		return notesDao.countBillingDomesticNotes(shipNum);
	}

	public List countCreditCardNotes(Long creditCardId) {
		return notesDao.countCreditCardNotes(creditCardId);
	}

	public List notesBillingCompleteStatus(String shipNum) {
		return notesDao.notesBillingCompleteStatus(shipNum);
	}

	public List<CustomerFile> getCustomerFilesDetails(String customerNumber) {
		return notesDao.getCustomerFilesDetails(customerNumber);
	}
	
	public List<ServiceOrder> getServiceOrederDetils(String notesId){
		return notesDao.getServiceOrederDetils(notesId);
	}

	public void updateNotesFromRules(String note,String subject, Long id, String forwardUser, String updatedBy, String remindInterval,String noteType, String noteSubType, String linkedTo,String supplier, String grading, String issueType)
 {
		notesDao.updateNotesFromRules(note,subject,id,forwardUser,updatedBy,remindInterval,noteType,noteSubType, linkedTo,supplier, grading, issueType);

	}

	public void dismissReminder(Long id) {
		notesDao.dismissReminder(id);
	}

	public void updateForwardDateFromRules(String forwardDate, String remindTime, Long id) {
		notesDao.updateForwardDateFromRules(forwardDate, remindTime, id);
	}

	public List findByUserActivity(String userName, String timeZone) {
		return notesDao.findByUserActivity(userName, timeZone);
	}

	public List countAccountProfileNotes(String partnerCode, String sessionCorpID) {
		return notesDao.countAccountProfileNotes(partnerCode, sessionCorpID);
	}

	public List countAccountContactNotes(String partnerCode, String sessionCorpID,Long kId) {
		return notesDao.countAccountContactNotes(partnerCode, sessionCorpID,kId);
	}

	public List getListByAccountNotesId(String notesId, String subType, String type, Long kId) {
		return notesDao.getListByAccountNotesId(notesId, subType, type, kId);
	}

	public List countForIMFEntitlementNotes(String shipNum) {
		return notesDao.countForIMFEntitlementNotes(shipNum);
	}

	public List countCustomerFeedbackNotes(String shipNum) {
		return notesDao.countCustomerFeedbackNotes(shipNum);
	}

	public List getRulesNotes(String fileNumber, String tdrID, String sessionCorpID) {
		return notesDao.getRulesNotes(fileNumber, tdrID, sessionCorpID);
	}

	public void updateResult(String resultRecordId, String sessionCorpID) {
		notesDao.updateResult(resultRecordId, sessionCorpID);
	}
	public void updateCheckListResult(String resultRecordId, String sessionCorpID)
	{
		notesDao.updateCheckListResult(resultRecordId, sessionCorpID);
	}

	public List userSignatureNotes(String username) {
		return notesDao.userSignatureNotes(username);
	}

	public List findMaximumId(String fileNumber) {
		return notesDao.findMaximumId(fileNumber);
	}

	public List findMinimumId(String fileNumber) {
		return notesDao.findMinimumId(fileNumber);
	}

	public List findCountId(String fileNumber) {
		return notesDao.findCountId(fileNumber);
	}

	public List goNotes(String fileNumber, Long id, String corpID) {
		return notesDao.goNotes(fileNumber, id, corpID);
	}

	public List goNextNotes(String fileNumber, Long id, String corpID) {
		return notesDao.goNextNotes(fileNumber, id, corpID);
	}

	public List goPrevNotes(String fileNumber, Long id, String corpID) {
		return notesDao.goPrevNotes(fileNumber, id, corpID);
	}

	public List notesNextPrev(String fileNumber, Long id, String corpID) {
		return notesDao.notesNextPrev(fileNumber, id, corpID);
	}

	public List findMaximumIdIsRal(String fileNumber) {
		return notesDao.findMaximumIdIsRal(fileNumber);
	}

	public List findMinimumIdIsRal(String fileNumber) {
		return notesDao.findMinimumIdIsRal(fileNumber);
	}

	public List findCountIdIsRal(String fileNumber) {
		return notesDao.findCountIdIsRal(fileNumber);
	}

	public List goNextNotesIsRal(String fileNumber, Long id, String corpID) {
		return notesDao.goNextNotesIsRal(fileNumber, id, corpID);
	}

	public List goPrevNotesIsRal(String fileNumber, Long id, String corpID) {
		return notesDao.goPrevNotesIsRal(fileNumber, id, corpID);
	}

	public List notesNextPrevIsRal(String fileNumber, Long id, String corpID) {
		return notesDao.notesNextPrevIsRal(fileNumber, id, corpID);
	}

	public List countForPartnerNotes(String partnerCode) {
		return notesDao.countForPartnerNotes(partnerCode);
	}

	public List getPartnerNotesListByCustomerNumber(CustomerFile customerFile) {
		return notesDao.getPartnerNotesListByCustomerNumber(customerFile);
	}

	public List findPartnerAlertList(String notesId) {
		return notesDao.findPartnerAlertList(notesId);
	}
	
	public List countForCrewNotes(Long id){
		return notesDao.countForCrewNotes(id);
	}
	public List countForDSNotes(String sequenceNumber) {
		return notesDao.countForDSNotes(sequenceNumber);
	}

	public List countForDSPreviewTripNotes(String sequenceNumber) {
		return notesDao.countForDSPreviewTripNotes(sequenceNumber);
	}
	public List countForDSTemporaryAccommodationNotes(String sequenceNumber) {
		return notesDao.countForDSTemporaryAccommodationNotes(sequenceNumber);
	}
	
	public List countForDSColaServiceNotes(String sequenceNumber) {
		return notesDao.countForDSColaServiceNotes(sequenceNumber);
	}
	public List countForDspDetailsNotes(String sequenceNumber,String dspNoteType) {
		return notesDao.countForDspDetailsNotes(sequenceNumber,dspNoteType);
	}
	
	public List countForDSAutomobileAssistanceNotes(String sequenceNumber) {
		return notesDao.countForDSAutomobileAssistanceNotes(sequenceNumber);
	}
	
	
	public List countForDSMoveMgmtNotes(String sequenceNumber) {
		return notesDao.countForDSMoveMgmtNotes(sequenceNumber);
	}
	public List countForDSTenancyManagementNotes(String sequenceNumber){
		return notesDao.countForDSTenancyManagementNotes(sequenceNumber);
	}
	
	public List countForDSOngoingSupportNotes(String sequenceNumber){
		return notesDao.countForDSOngoingSupportNotes(sequenceNumber);
	}
	public List countForDSSchoolEducationalCounselingNotes(String sequenceNumber) {
		return notesDao.countForDSSchoolEducationalCounselingNotes(sequenceNumber);
	}	
		
	public List countForDSRelocationExpenseManagementNotes(String sequenceNumber) {
		return notesDao.countForDSRelocationExpenseManagementNotes(sequenceNumber);
	}

	
	public List countForDSHomeFindingAssistancePurchaseNotes(String sequenceNumber) {
		return notesDao.countForDSHomeFindingAssistancePurchaseNotes(sequenceNumber);
	}
	
	

	public List countForDSVisaImmigrationNotes(String sequenceNumber) {
		return notesDao.countForDSVisaImmigrationNotes(sequenceNumber);
	}

	public List countRelocationAreaInfoOrientationNotes(String shipNumber) {
		return notesDao.countRelocationAreaInfoOrientationNotes(shipNumber);
	}

	public List countForDSTaxServicesNotes(String shipNumber) {
		return notesDao.countForDSTaxServicesNotes(shipNumber);
	}

	public List countForDSHomeRentalNotes(String shipNumber) {
		return notesDao.countForDSHomeRentalNotes(shipNumber);
	}

	public List countForCountDSRepatriationNotes(String shipNumber) {
		return notesDao.countForCountDSRepatriationNotes(shipNumber);
	}

	public List countForDSCrossCulturalTrainingNotes(String shipNumber) {
			return notesDao.countForDSCrossCulturalTrainingNotes(shipNumber);
	}

	public List countForDSLanguageNotes(String shipNumber) {
			return notesDao.countForDSLanguageNotes(shipNumber);
	}

	public List countForAccountPortalNotes(String sequenceNumber) {
		return notesDao.countForAccountPortalNotes(sequenceNumber);
	}
	public void updateNoteFileList(Long id ,String myFileIds, String sessionCorpID){
		 notesDao.updateNoteFileList(id,myFileIds,sessionCorpID);
	}
	
	public List getListById(Long id, String sessionCorpID){
	 	return notesDao.getListById(id, sessionCorpID);
	}
	public List getMyFileList(Long id){
		return notesDao.getMyFileList(id);
	}
	public Map<String, String> findSupplier(Long id1){
		return notesDao.findSupplier(id1);
	}
	
	public String findDistinctAgentName(String corpId, String spNumber, String cid){
		return notesDao.findDistinctAgentName(corpId, spNumber,cid);
	}
	public List getServiceOrderNotesList(String shipNumber, String type, Boolean bookingAgent, Boolean networkAgent, Boolean originAgent, Boolean subOriginAgent, Boolean destAgent, Boolean subDestAgent){
		return notesDao.getServiceOrderNotesList(shipNumber, type, bookingAgent, networkAgent, originAgent, subOriginAgent, destAgent, subDestAgent);
	}
	public void upDateNetworkLinkId(String networkLinkId, Long id){
		notesDao.upDateNetworkLinkId(networkLinkId, id);
	}
	public List findlinkedIdList(String networkLinkId){
		return notesDao.findlinkedIdList(networkLinkId);
	}
	public List getSurveyNotes(String sequenceNumber, Long cid, String corpID){
		return notesDao.getSurveyNotes(sequenceNumber, cid, corpID);
	}
	public String findSubjectDescForNotes(Long id, String sessionCorpID){
		return notesDao.findSubjectDescForNotes(id, sessionCorpID);
	}
	public String findNoteDescForNotes(Long id, String sessionCorpID){
		return notesDao.findNoteDescForNotes(id, sessionCorpID);
	}
	public List countForMssNotes(Long id){
		return notesDao.countForMssNotes(id);
	}
	public List getListForMss(Long notesId, String subType){
		return notesDao.getListForMss(notesId,subType);
	}

	public List<Notes> getNoteByNotesId(Long notesId, String corpId,String notesSubType) {
		return  notesDao.getNoteByNotesId(notesId, corpId, notesSubType);
	}
	public List getLinkedId(String networkLinkId, String sessionCorpID){
		return notesDao.getLinkedId(networkLinkId, sessionCorpID );
	}

	public Map<String, String> countSoDetailNotes(String shipNumber, String noteSubType,String sessionCorpID) {
		return notesDao.countSoDetailNotes(shipNumber, noteSubType,sessionCorpID);
	}
	public Map<String, String> getSubCategoryRlo(String shipNumber,String sessionCorpID){
		return notesDao.getSubCategoryRlo(shipNumber,sessionCorpID);
	}
	public Map<String, String> getNoteGrading(String cfBillToCode,String sessionCorpID){
		return notesDao.getNoteGrading(cfBillToCode,sessionCorpID);
	}
	public Map<String, String> getNoteGradinglinkedTo(String spNumber,String sessionCorpID){
		return notesDao.getNoteGradinglinkedTo(spNumber,sessionCorpID);
	}
	public List getALLListByNotesId(String notesId, String subType) {
		return notesDao.getALLListByNotesId(notesId, subType);
	}
	public void disableNotesForGivenOrder(String corpId,String idList){
		notesDao.disableNotesForGivenOrder(corpId, idList);
	}
	
	public List<Notes> findByNotesAll(String noteStatus, String noteSubType, String notesId) {
		return notesDao.findByNotesAll(noteStatus, noteSubType, notesId);
	}
	
	public List findPartnerAlertListForCf(String notesId)
	{
		return notesDao.findPartnerAlertListForCf(notesId);
	}
	
	public List countForOINotes(Long sId){
		return notesDao.countForOINotes(sId);
	}
	
	public List getListForOI(Long id, String subType){
		return notesDao.getListForOI(id, subType);
	}
	//13674 - Create dashboard
	public String getNotesCount(String sessionCorpID,String shipNumber,Long id) {
		
		return notesDao.getNotesCount(sessionCorpID,shipNumber,id);
	}

	public String getnoteDestCount(String sessionCorpID, String shipNumber, Long id) {
	
		return notesDao.getNoteDestCount(sessionCorpID, shipNumber, id);
	}
	public String getNoteForwardCount(String sessionCorpID,  Long id) {
		
		return notesDao.getNoteForwardCount(sessionCorpID,  id);
	}

	@Override
	public String accountLineNotesStatus(String accId, String sessionCorpID) {
		// TODO Auto-generated method stub
		return notesDao.accountLineNotesStatus(accId, sessionCorpID);
	}
	public List getALLListByFamilyDetails(String notesId, String subType) {
		return notesDao.getALLListByFamilyDetails(notesId, subType);
	}
 	//End  dashboard
}
