<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
<title>Standard Deductions</title>
<meta name="heading" content="Standard Deductions" />	
<link href='<s:url value="/css/main.css"/>' rel="stylesheet" type="text/css" />
<s:head theme="ajax" />
<style type="text/css"> h2 {background-color: #CCCCCC} </style>
<style> <%@ include file="/common/calenderStyle.css"%> </style>
<style type="text/css">
	.subcontent-tab {font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #15428B;text-decoration: none;background:url(images/collapsebg.gif) #BCD2EF;padding:4px 3px 1px 5px; height:15px;width:598px; border:1px solid #99BBE8; border-right:none; border-left:none} 
	a.dsphead{text-decoration:none;color:#000000;}
	.dspchar2{padding-left:0px;}
</style>
<style>
	.input-textarea{
		border:1px solid #219DD1;
		color:#000000;
		font-family:arial,verdana;
		font-size:12px;
		height:45px;
		text-decoration:none;
	}
	.bgblue{background:url(images/blue_band.jpg); height: 30px; width:630px; background-repeat: no-repeat;font-family: Arial, Helvetica, sans-serif;font-size: 12px;font-weight: bold;color: #007a8d; padding-left: 40px; }
</style>
 <!-- Modified By Kunal Sharma at 13-Jan-2012 --> 
   
    <script language="javascript" type="text/javascript">
		<%@ include file="/common/formCalender.js"%>
	</script> 
   
    <script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/jscal2.js"></script>
	<script language="JavaScript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/en.js"></script>
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/jscal2.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/border-radius.css'/>" />
    <link rel="stylesheet" type="text/css"  href="<c:url value='/styles/redsky/steel.css'/>" />
   <script language="javascript" type="text/javascript">
       var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();   
   </script>
   <!-- Modification closed here -->
<script language="javascript" type="text/javascript">
function fieldValidate(){
   if(document.forms['standardDeductionForm'].elements['fromDate'].value==''){
      	alert("Please select the from date"); 
     	return false;
   }if(document.forms['standardDeductionForm'].elements['companyDivision'].value==''){
       	alert("Please select the company division"); 
       	return false;
   }
   showOrHide(1);
}

function dedProcessing(){
	var fromDate = document.forms['standardDeductionForm'].elements['fromDate'].value;
	var companyDivision = document.forms['standardDeductionForm'].elements['companyDivision'].value;
	var ownerPayTo= document.forms['standardDeductionForm'].elements['ownerPayTo'].value;
	if(fromDate == ''){
      	alert("Please select the from date"); 
     	return false;
   }if(companyDivision == ''){
       	alert("Please select the company division"); 
       	return false;
   }
   if(fromDate != '' && companyDivision != ''){
   		showOrHide(1);
   		location.href='dedProcessing.html?fromDate='+fromDate+'&companyDivision='+companyDivision+'&ownerPayTo='+ownerPayTo;
   }else{
		return false;
   }
}  

function validateOwnerPayTo(){
    var ownerPayTo = document.forms['standardDeductionForm'].elements['ownerPayTo'].value;
    var url="validatePayTo.html?ajax=1&decorator=simple&popup=true&ownerPayTo=" + encodeURI(ownerPayTo);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse4;
     http2.send(null);
     
}

function handleHttpResponse4()
        {

             if (http2.readyState == 4)
             {
             
                var results = http2.responseText
                results = results.trim();
                results=results.replace("[","")
                var res=results.replace("]","")                            
                if(res !=''){        
		
 				  }
                 else{
                 alert("Entered Pay To Code is Incorrect");
                 document.forms['standardDeductionForm'].elements['ownerPayTo'].value="";
                 document.forms['standardDeductionForm'].elements['ownerPayTo'].select();
                 }
                 
                 }
       }
                 
 function getHTTPObject()
         {
                var xmlhttp;
               if(window.XMLHttpRequest){
              xmlhttp = new XMLHttpRequest();
             }
               else if (window.ActiveXObject) {
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)       {
             xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}  
var http2 = getHTTPObject();          
</script>
</head>

<s:form name="standardDeductionForm" id="standardDeductionForm" action="previewDeduction.html?process=preview" method="post" validate="true">
<c:set var="FormDateValue" value="dd-NNN-yy"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="formStatus" value=""/>
<div id="layer1" style="width:100%;">
	<div id="otabs" class="form_magn">
		<ul>
			<li><a class="current"><span>Standard Deductions</span></a></li>
		</ul>
	</div>
	<div class="spnblk">&nbsp;</div>
	<div id="content" align="center" >
		<div id="liquid-round">
			<div class="top" style="margin-top: 10px;!margin-top:-4px;"><span></span></div>
			<div class="center-content">
				<table class="" cellspacing="1" cellpadding="0" border="0">	
					<tr><td height="5px">&nbsp;</td></tr>	
					<tr>		  		
						<td align="right" width="" class="listwhitetext" style="padding-bottom:5px">Deductions Period Uptill</td>	
					  	<td width="" style="padding-left:5px;"> 	 
					  		<s:textfield cssClass="input-text" id="fromDate" name="fromDate" value="%{fromDate}" size="8" maxlength="11"  readonly="true" onkeydown="return onlyDel(event,this);" cssStyle="width:120px;" /> 
					  		<img id="fromDate_trigger" style="vertical-align:bottom" src="${pageContext.request.contextPath}/images/calender.png" HEIGHT=20 WIDTH=20/>
					  	</td>
						<td width="" align="right" class="listwhitetext">Company Division</td>
						<td align="left" style="padding-left:5px;"><s:select cssClass="list-menu" id="companyDivision" name="companyDivision" list="%{companyDivis}" cssStyle="width:150px;" headerKey="" headerValue=""/></td>		 	
					
					  <td align="right" width="100" class="listwhitetext">Pay&nbsp;To&nbsp;</td>
 					<td align="left" class="listwhitetext"><s:textfield cssClass="input-text" name="ownerPayTo" id="ownerPayTo" required="true"  size="10" maxlength="8" onchange="validateOwnerPayTo()" cssStyle="width:150px;"/></td>
	                <td align="left"><img class="openpopup" width="17" height="20" onclick="javascript:openWindow('searchVendorListInTruck.html?partnerType=OO&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_seventhDescription=seventhDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=description&fld_code=ownerPayTo');" src="<c:url value='/images/open-popup.gif'/>" /></td>
									
					</tr>
					<tr><td height="20px"></td></tr>
					<tr>	
					  	<td width=""></td>	
					  	<td><input type="button" class="cssbutton" style="margin-right: 5px;height: 25px;width:155px; font-size: 15" value="Deductions Processing" onclick="return dedProcessing();"/></td>		
						<td colspan="4"><s:submit cssClass="cssbutton" cssStyle="width:145px; height:25px; margin-left:90px;" align="top" value="Driver Processing" onclick="return fieldValidate();"/></td>
						
					</tr>
					<tr><td height="20"></td></tr>	 				
				</table> 
			</div>
			<div class="bottom-header"><span></span></div>
		</div>
	</div>
</div>    
<s:hidden name="secondDescription" />    
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<s:hidden name="firstDescription" />
<s:hidden name="seventhDescription" />
<s:hidden name="description" />
</s:form>
<script>
function showOrHide(value) {
    if (value==0) {
       	if (document.layers)
           document.layers["overlay"].visibility='hide';
        else
           document.getElementById("overlay").style.visibility='hidden';
   	}else if (value==1) {
   		if (document.layers)
          document.layers["overlay"].visibility='show';
       	else
          document.getElementById("overlay").style.visibility='visible';
   	}
}    
showOrHide(0);

</script>

<script type="text/javascript">
	setOnSelectBasedMethods([]);
	setCalendarFunctionality();
</script>