<%@ include file="/common/taglibs.jsp"%>  
 <%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title><fmt:message key="serviceOrderList.title"/></title>   
    <meta name="heading" content="<fmt:message key='serviceOrderList.heading' />"/>   
</head>
<table class="detailTabLabel" cellpadding="0" cellspacing="0" border="0" style="width:200px;">
<tr valign="top"> 	
	<td align="left"><b>SO Container List </b></td>
	<td align="right"  style="width:30px;">
	<img align="right" class="openpopup" onclick="ajax_hideTooltip()" src="<c:url value='/images/closetooltip.gif'/>" /></td>
</tr>
</table>
<display:table name="contaiSO" class="table" requestURI="" id="containerList" export="false" defaultsort="1" pagesize="100" style="width:100%;margin-top:2px;">   
	<display:column title="Container#"><a href="#" onclick="goToUrlChild(${containerList.id});" ><c:out value="${containerList.idNumber}" /></a></display:column>
    <display:column  property="containerNumber"  titleKey="container.containerNumber"/>
	<display:column headerClass="containeralign" property="sealNumber" titleKey="container.sealNumber" style="text-align: right"/>
	<display:column  property="size"  titleKey="container.size"/>
</display:table>