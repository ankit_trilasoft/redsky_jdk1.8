

delimiter $$

CREATE trigger redsky.trigger_add_history_itemsjequip
BEFORE UPDATE on redsky.itemsjequip
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";





   IF (OLD.corpId <> NEW.corpId ) THEN
      select CONCAT(fieldNameList,'itemsjequip.corpId~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpId,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpId,'~') into newValueList;
  END IF;
    


    







   IF (OLD.contract <> NEW.contract ) THEN
      select CONCAT(fieldNameList,'itemsjequip.contract~') into fieldNameList;
      select CONCAT(oldValueList,OLD.contract,'~') into oldValueList;
      select CONCAT(newValueList,NEW.contract,'~') into newValueList;
  END IF;
    


    





   IF (OLD.gl <> NEW.gl ) THEN
      select CONCAT(fieldNameList,'itemsjequip.gl~') into fieldNameList;
      select CONCAT(oldValueList,OLD.gl,'~') into oldValueList;
      select CONCAT(newValueList,NEW.gl,'~') into newValueList;
  END IF;
    


    





   IF (OLD.bucket <> NEW.bucket ) THEN
      select CONCAT(fieldNameList,'itemsjequip.bucket~') into fieldNameList;
      select CONCAT(oldValueList,OLD.bucket,'~') into oldValueList;
      select CONCAT(newValueList,NEW.bucket,'~') into newValueList;
  END IF;
    


    





   IF (OLD.idNum <> NEW.idNum ) THEN
      select CONCAT(fieldNameList,'itemsjequip.idNum~') into fieldNameList;
      select CONCAT(oldValueList,OLD.idNum,'~') into oldValueList;
      select CONCAT(newValueList,NEW.idNum,'~') into newValueList;
  END IF;
    


    





   IF (OLD.billCrew <> NEW.billCrew ) THEN
      select CONCAT(fieldNameList,'itemsjequip.billCrew~') into fieldNameList;
      select CONCAT(oldValueList,OLD.billCrew,'~') into oldValueList;
      select CONCAT(newValueList,NEW.billCrew,'~') into newValueList;
  END IF;
    


    





   IF (OLD.lastReset <> NEW.lastReset ) THEN
      select CONCAT(fieldNameList,'itemsjequip.lastReset~') into fieldNameList;
      select CONCAT(oldValueList,OLD.lastReset,'~') into oldValueList;
      select CONCAT(newValueList,NEW.lastReset,'~') into newValueList;
  END IF;
    


    





   IF (OLD.payCat <> NEW.payCat ) THEN
      select CONCAT(fieldNameList,'itemsjequip.payCat~') into fieldNameList;
      select CONCAT(oldValueList,OLD.payCat,'~') into oldValueList;
      select CONCAT(newValueList,NEW.payCat,'~') into newValueList;
  END IF;
    


    





   IF (OLD.crewType <> NEW.crewType ) THEN
      select CONCAT(fieldNameList,'itemsjequip.crewType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.crewType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.crewType,'~') into newValueList;
  END IF;
    


    





   IF (OLD.descript <> NEW.descript ) THEN
      select CONCAT(fieldNameList,'itemsjequip.descript~') into fieldNameList;
      select CONCAT(oldValueList,OLD.descript,'~') into oldValueList;
      select CONCAT(newValueList,NEW.descript,'~') into newValueList;
  END IF;
    


    





   IF (OLD.glType <> NEW.glType ) THEN
      select CONCAT(fieldNameList,'itemsjequip.glType~') into fieldNameList;
      select CONCAT(oldValueList,OLD.glType,'~') into oldValueList;
      select CONCAT(newValueList,NEW.glType,'~') into newValueList;
  END IF;
    


    





   IF (OLD.idNumNew <> NEW.idNumNew ) THEN
      select CONCAT(fieldNameList,'itemsjequip.idNumNew~') into fieldNameList;
      select CONCAT(oldValueList,OLD.idNumNew,'~') into oldValueList;
      select CONCAT(newValueList,NEW.idNumNew,'~') into newValueList;
  END IF;
    


    





   IF (OLD.orderNew <> NEW.orderNew ) THEN
      select CONCAT(fieldNameList,'itemsjequip.orderNew~') into fieldNameList;
      select CONCAT(oldValueList,OLD.orderNew,'~') into oldValueList;
      select CONCAT(newValueList,NEW.orderNew,'~') into newValueList;
  END IF;
    


    





   IF (OLD.qty <> NEW.qty ) THEN
      select CONCAT(fieldNameList,'itemsjequip.qty~') into fieldNameList;
      select CONCAT(oldValueList,OLD.qty,'~') into oldValueList;
      select CONCAT(newValueList,NEW.qty,'~') into newValueList;
  END IF;
    


    





   IF (OLD.type <> NEW.type ) THEN
      select CONCAT(fieldNameList,'itemsjequip.type~') into fieldNameList;
      select CONCAT(oldValueList,OLD.type,'~') into oldValueList;
      select CONCAT(newValueList,NEW.type,'~') into newValueList;
  END IF;
    




   IF (OLD.flag <> NEW.flag ) THEN
      select CONCAT(fieldNameList,'itemsjequip.flag~') into fieldNameList;
      select CONCAT(oldValueList,OLD.flag,'~') into oldValueList;
      select CONCAT(newValueList,NEW.flag,'~') into newValueList;
  END IF;
    




   IF (OLD.resourceCategory <> NEW.resourceCategory ) THEN
      select CONCAT(fieldNameList,'itemsjequip.resourceCategory~') into fieldNameList;
      select CONCAT(oldValueList,OLD.resourceCategory,'~') into oldValueList;
      select CONCAT(newValueList,NEW.resourceCategory,'~') into newValueList;
  END IF;
    


  
   


    IF ((OLD.charge <> NEW.charge) or (OLD.charge is null and NEW.charge is not null)
      or (OLD.charge is not null and NEW.charge is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.charge~') into fieldNameList;
        IF(OLD.charge is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.charge is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.charge is not null) then
            select CONCAT(oldValueList,OLD.charge,'~') into oldValueList;
        END IF;
        IF(NEW.charge is not null) then
            select CONCAT(newValueList,NEW.charge,'~') into newValueList;
        END IF;
   END IF;     


   


    IF ((OLD.cost <> NEW.cost) or (OLD.cost is null and NEW.cost is not null)
      or (OLD.cost is not null and NEW.cost is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.cost~') into fieldNameList;
        IF(OLD.cost is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.cost is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.cost is not null) then
            select CONCAT(oldValueList,OLD.cost,'~') into oldValueList;
        END IF;
        IF(NEW.cost is not null) then
            select CONCAT(newValueList,NEW.cost,'~') into newValueList;
        END IF;
   END IF;     


   


    IF ((OLD.dtCharge <> NEW.dtCharge) or (OLD.dtCharge is null and NEW.dtCharge is not null)
      or (OLD.dtCharge is not null and NEW.dtCharge is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.dtCharge~') into fieldNameList;
        IF(OLD.dtCharge is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.dtCharge is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.dtCharge is not null) then
            select CONCAT(oldValueList,OLD.dtCharge,'~') into oldValueList;
        END IF;
        IF(NEW.dtCharge is not null) then
            select CONCAT(newValueList,NEW.dtCharge,'~') into newValueList;
        END IF;
   END IF;     


   


    IF ((OLD.packLabour <> NEW.packLabour) or (OLD.packLabour is null and NEW.packLabour is not null)
      or (OLD.packLabour is not null and NEW.packLabour is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.packLabour~') into fieldNameList;
        IF(OLD.packLabour is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.packLabour is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.packLabour is not null) then
            select CONCAT(oldValueList,OLD.packLabour,'~') into oldValueList;
        END IF;
        IF(NEW.packLabour is not null) then
            select CONCAT(newValueList,NEW.packLabour,'~') into newValueList;
        END IF;
   END IF;     



   


    IF ((OLD.otCharge <> NEW.otCharge) or (OLD.otCharge is null and NEW.otCharge is not null)
      or (OLD.otCharge is not null and NEW.otCharge is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.otCharge~') into fieldNameList;
        IF(OLD.otCharge is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.otCharge is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.otCharge is not null) then
            select CONCAT(oldValueList,OLD.otCharge,'~') into oldValueList;
        END IF;
        IF(NEW.otCharge is not null) then
            select CONCAT(newValueList,NEW.otCharge,'~') into newValueList;
        END IF;
   END IF;     




   


    IF ((OLD.pounds <> NEW.pounds) or (OLD.pounds is null and NEW.pounds is not null)
      or (OLD.pounds is not null and NEW.pounds is null)) THEN
        select CONCAT(fieldNameList,'itemsjequip.pounds~') into fieldNameList;
        IF(OLD.pounds is null) THEN
            select CONCAT(oldValueList,"0",'~') into oldValueList;
        END IF;
        IF(NEW.pounds is null) THEN
            select CONCAT(newValueList,"0",'~') into newValueList;
        END IF;

        IF(OLD.pounds is not null) then
            select CONCAT(oldValueList,OLD.pounds,'~') into oldValueList;
        END IF;
        IF(NEW.pounds is not null) then
            select CONCAT(newValueList,NEW.pounds,'~') into newValueList;
        END IF;
   END IF;     







  IF (OLD.controlled <> NEW.controlled ) THEN
       select CONCAT(fieldNameList,'itemsjequip.controlled~') into fieldNameList;
       IF(OLD.controlled = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.controlled = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.controlled = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.controlled = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;








  IF (OLD.template <> NEW.template ) THEN
       select CONCAT(fieldNameList,'itemsjequip.template~') into fieldNameList;
       IF(OLD.template = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.template = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.template = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.template = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;




   
   IF (OLD.printOnForm <> NEW.printOnForm ) THEN
       select CONCAT(fieldNameList,'itemsjequip.printOnForm~') into fieldNameList;
       IF(OLD.printOnForm = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.printOnForm = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.printOnForm = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.printOnForm = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;
  
  
  
  IF (OLD.frequentlyUsedResources <> NEW.frequentlyUsedResources ) THEN
       select CONCAT(fieldNameList,'itemsjequip.frequentlyUsedResources~') into fieldNameList;
       IF(OLD.frequentlyUsedResources = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.frequentlyUsedResources = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.frequentlyUsedResources = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.frequentlyUsedResources = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
  IF (OLD.resourceForAccPortal <> NEW.resourceForAccPortal ) THEN
       select CONCAT(fieldNameList,'itemsjequip.resourceForAccPortal~') into fieldNameList;
       IF(OLD.resourceForAccPortal = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.resourceForAccPortal = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.resourceForAccPortal = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.resourceForAccPortal = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF; 
   
   


 CALL add_tblHistory (OLD.id,"itemsjequip", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;
