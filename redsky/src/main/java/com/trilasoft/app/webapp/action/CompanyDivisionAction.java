package com.trilasoft.app.webapp.action;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.appfuse.Constants;
import org.appfuse.model.User;
import org.appfuse.service.GenericManager;
import org.appfuse.util.StringUtil;
import org.appfuse.webapp.action.BaseAction;
import com.google.gson.Gson;
import com.opensymphony.xwork2.Preparable;
import com.trilasoft.app.model.AuthorizationNo;
import com.trilasoft.app.model.Company;
import com.trilasoft.app.model.CompanyDivision;
import com.trilasoft.app.model.ServiceOrder;
import com.trilasoft.app.service.CompanyDivisionManager;
import com.trilasoft.app.service.CompanyManager;
import com.trilasoft.app.service.CustomerFileManager;
import com.trilasoft.app.service.PagingLookupManager;
import com.trilasoft.app.service.RefMasterManager;
import com.trilasoft.app.webapp.helper.ExtendedPaginatedList;
import com.trilasoft.app.webapp.helper.PaginateListFactory;
import com.trilasoft.app.webapp.helper.SearchCriterion;

public class CompanyDivisionAction extends BaseAction implements Preparable{
    private CompanyDivisionManager companyDivisionManager;
    private List companyDivisions;
    private List bookingAgentCodeList=new ArrayList();
    private List companyCodeList=new ArrayList();
    private String sessionCorpID;
    private List ls;
    private Long id; 
	private CompanyDivision companyDivision;
	private static Map<String, String> job;
	private static Map<String, String> countryDesc;
	private static Map<String, String> bstates;
	private static Map<String, String> coord = new LinkedHashMap<String, String>();
	private Map<String,String> driverAgencyList;
	private List<String> opshub;
	private RefMasterManager refMasterManager;
	private CustomerFileManager customerFileManager;
	private String enbState;
    private Map<String, String> countryCod;
	Date currentdate = new Date();
	private boolean cportalAccessCompanyDivisionLevel=false;
	private CompanyManager companyManager;
	private Company company; 
	private String jsonCountryCodeText;
	private boolean generateInvoiceByCompanyDivision=false;
	static final Logger logger = Logger.getLogger(CompanyDivisionAction.class);
	 

	public CompanyDivisionAction(){
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User)auth.getPrincipal();
        this.sessionCorpID = user.getCorpID();
	}
	public void prepare() throws Exception {
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		getComboList(sessionCorpID);
		enbState = customerFileManager.enableStateList(sessionCorpID);
		 company=companyManager.findByCorpID(sessionCorpID).get(0);
		if(company!=null){
		if(company.getCportalAccessCompanyDivisionLevel()!=null){
			cportalAccessCompanyDivisionLevel=company.getCportalAccessCompanyDivisionLevel();
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+cportalAccessCompanyDivisionLevel+" End");
			   }
		if(company.getGenerateInvoiceBy()!=null && (!(company.getGenerateInvoiceBy().trim().equals(""))) && company.getGenerateInvoiceBy().trim().equalsIgnoreCase("CompanyDivision")){
			generateInvoiceByCompanyDivision=true;
		}
		}
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	}
    
    public List getCompanyDivisions() {
        return companyDivisions;
    }

    public String list() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
        companyDivisions = companyDivisionManager.getAll();
       logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }
    private String gotoPageString;

    public String getGotoPageString() {

    return gotoPageString;

    }

    public void setGotoPageString(String gotoPageString) {

    this.gotoPageString = gotoPageString;

    }
    public void setRefMasterManager(RefMasterManager refMasterManager) {
        this.refMasterManager = refMasterManager;
    }
    private String validateFormNav;
    public String saveOnTabChange() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	validateFormNav = "OK";
        String s = save();
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return gotoPageString;
    }
    private ExtendedPaginatedList companyDivisionsExt;

    private PagingLookupManager pagingLookupManager;
    private PaginateListFactory paginateListFactory;
	private String bookCode;
	private String companyCode;
    public String getCompanyCode() {
		return companyCode;
	}


	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public void setPagingLookupManager(PagingLookupManager pagingLookupManager) {
        this.pagingLookupManager = pagingLookupManager;
    }

    public PaginateListFactory getPaginateListFactory() {
        return paginateListFactory;
    }

    public void setPaginateListFactory(PaginateListFactory paginateListFactory) {
        this.paginateListFactory = paginateListFactory;
    }


public ExtendedPaginatedList getCompanyDivisionsExt() {
		return companyDivisionsExt;
	}

	public void setCompanyDivisionsExt(ExtendedPaginatedList companyDivisionsExt) {
		this.companyDivisionsExt = companyDivisionsExt;
	}
	private Map<String, String> baseCurrency = new HashMap<String, String>();
	
	public String getComboList(String corpId){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		 Gson gson = new Gson();
		driverAgencyList=refMasterManager.findByParameter(sessionCorpID,"DRIVERAGENCY"); 
		 baseCurrency = refMasterManager.findCodeOnleByParameter(corpId, "CURRENCY");
	 	 job = refMasterManager.findByParameter(corpId, "JOB");
	 	 countryDesc = refMasterManager.findCountry(corpId, "COUNTRY");
	 	 coord = refMasterManager.findUser(corpId, "ROLE_COORD"); 
	 	 opshub=refMasterManager.occupied("OPSHUB", corpId);
	 	countryCod = refMasterManager.findByParameter(sessionCorpID, "COUNTRY");
	 	jsonCountryCodeText = gson.toJson(countryCod);
	 	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		 return SUCCESS; 
	    }
	
	
    @SkipValidation
    public String search()
      {   
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	  boolean companyCode = (companyDivision.getCompanyCode() == null);
      boolean description = (companyDivision.getDescription() == null);
      boolean accountingCode = (companyDivision.getAccountingCode() == null);
      boolean bookingAgentCode = (companyDivision.getBookingAgentCode() == null);
      boolean vanLineCode = (companyDivision.getVanLineCode() == null);
      boolean vanlinedefaultJobtype = (companyDivision.getVanlinedefaultJobtype() == null);
         if(!companyCode || !description || !accountingCode || !bookingAgentCode || !vanLineCode  || !vanlinedefaultJobtype) {
        	 companyDivisionsExt = paginateListFactory.getPaginatedListFromRequest(getRequest());
             List<SearchCriterion> searchCriteria = new ArrayList();
  		   searchCriteria.add(new SearchCriterion("companyCode", companyDivision.getCompanyCode(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           searchCriteria.add(new SearchCriterion("description", companyDivision.getDescription(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           searchCriteria.add(new SearchCriterion("accountingCode", companyDivision.getAccountingCode(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           searchCriteria.add(new SearchCriterion("bookingAgentCode", companyDivision.getBookingAgentCode(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           searchCriteria.add(new SearchCriterion("vanLineCode", companyDivision.getVanLineCode(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           searchCriteria.add(new SearchCriterion("vanlinedefaultJobtype", companyDivision.getVanlinedefaultJobtype(), SearchCriterion.FIELD_TYPE_STRING, SearchCriterion.OPERATOR_LIKE));
           pagingLookupManager.getAllRecordsPage(CompanyDivision.class, companyDivisionsExt, searchCriteria);
      }
        	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
      return SUCCESS;     
      } 
    
    
    
    public String edit() {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
        if (id != null) {
        	companyDivision = companyDivisionManager.get(id);
        	bstates = companyDivisionManager.findDefaultStateList(companyDivision.getBillingCountryCode(), sessionCorpID);
        	companyDivision.setCorpID(sessionCorpID);
        } else {
            companyDivision = new CompanyDivision();
            companyDivision.setCorpID(sessionCorpID);
            bstates = companyDivisionManager.findDefaultStateList("", sessionCorpID);
            companyDivision.setCreatedOn(new Date());
            companyDivision.setUpdatedOn(new Date());
            companyDivision.setUpdatedBy(getRequest().getRemoteUser());
        }
        logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
        return SUCCESS;
    }

    public String save() throws Exception {
    	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
    	//getComboList(sessionCorpID);
    	/*Boolean encrypt = (Boolean) getConfiguration().get(Constants.ENCRYPT_PASSWORD);

        if ("true".equals(getRequest().getParameter("encryptPass")) && (encrypt != null && encrypt)) {
            String algorithm = (String) getConfiguration().get(Constants.ENC_ALGORITHM);

            if (algorithm == null) { // should only happen for test case
                log.debug("assuming testcase, setting algorithm to 'SHA'");
                algorithm = "SHA";
            }
            String pass=companyDivision.getResourcePassword();
            companyDivision.setResourcePassword(StringUtil.encodePassword(pass, algorithm));
           
        }*/
    
        boolean isNew = (companyDivision.getId() == null);
        if(isNew)
        {
        	companyDivision.setCreatedOn(new Date());
        }
        companyDivision.setUpdatedOn(new Date());
        companyDivision.setUpdatedBy(getRequest().getRemoteUser());
        companyDivision.setCorpID(sessionCorpID);
        bstates = companyDivisionManager.findDefaultStateList(companyDivision.getBillingCountryCode(), sessionCorpID);
        List  companyCodeList=companyDivisionManager.checkCompayCode(companyDivision.getCompanyCode(),sessionCorpID);
        List bookingAgentCodeList=companyDivisionManager.checkBookingAgentCode(companyDivision.getBookingAgentCode(),sessionCorpID);
        if(isNew)
        {
        if(bookingAgentCodeList.isEmpty()&&companyCodeList.isEmpty())
        {
        companyDivision = companyDivisionManager.save(companyDivision); 
            if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
             	String key = (isNew) ? "Company Division  has been added successfully" : "Company Division has been updated successfully";
             	saveMessage(getText(key));
             } 
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return SUCCESS; 
        }
        else {
            if(!companyCodeList.isEmpty())
            {
        	 String message="Code already exists, please select another.";
        	 errorMessage(getText(message));
            }
            if(!bookingAgentCodeList.isEmpty())
            {
             String messageForBooking="Booking Agent Code already exists, please select another.";
             errorMessage(getText(messageForBooking));
            }
            logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
            return "validation";
        } 
       }
       else
       {
    	   companyDivision = companyDivisionManager.save(companyDivision); 
           if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
            	String key = (isNew) ? "Company Division  has been added successfully" : "Company Division has been updated successfully";
            	saveMessage(getText(key));
            } 
           	logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
           return SUCCESS; 
       }
    }
    
//	 Method to get list of BookingAgent related to BookingAgent calling from  companyDivision form.
	@SkipValidation
	public String checkBookAgentExists(){
	bookingAgentCodeList=companyDivisionManager.checkBookingAgentCode(bookCode.trim(),sessionCorpID);
	return SUCCESS; 
	}
//	 Method to get list of BookingAgent related to BookingAgent calling from  companyDivision form.
	@SkipValidation
	public String checkCompanyCodeExists(){
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
		companyCodeList=companyDivisionManager.checkCompayCode(companyCode.trim(),sessionCorpID);
		logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
	return SUCCESS; 
	}
	public CompanyDivision getCompanyDivision() {
		return companyDivision;
	}


	public void setCompanyDivisionManager(CompanyDivisionManager companyDivisionManager) {
		this.companyDivisionManager = companyDivisionManager;
	}


	public void setCompanyDivision(CompanyDivision companyDivision) {
		this.companyDivision = companyDivision;
	}


	public Long getId() {
		return id;
	}
	public  Map<String, String> getJob() {
		return job;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setCompanyDivisions(List companyDivisions) {
		this.companyDivisions = companyDivisions;
	}


	public String getValidateFormNav() {
		return validateFormNav;
	}


	public void setValidateFormNav(String validateFormNav) {
		this.validateFormNav = validateFormNav;
	}


	public static Map<String, String> getCountryDesc() {
		return countryDesc;
	}


	public static void setCountryDesc(Map<String, String> countryDesc) {
		CompanyDivisionAction.countryDesc = countryDesc;
	}


	public static Map<String, String> getBstates() {
		return bstates;
	}


	public static void setBstates(Map<String, String> bstates) {
		CompanyDivisionAction.bstates = bstates;
	}


	public static Map<String, String> getCoord() {
		return coord;
	}


	public static void setCoord(Map<String, String> coord) {
		CompanyDivisionAction.coord = coord;
	}


	public List getBookingAgentCodeList() {
		return bookingAgentCodeList;
	}


	public void setBookingAgentCodeList(List bookingAgentCodeList) {
		this.bookingAgentCodeList = bookingAgentCodeList;
	}


	public String getBookCode() {
		return bookCode;
	}


	public void setBookCode(String bookCode) {
		this.bookCode = bookCode;
	}


	public List getCompanyCodeList() {
		return companyCodeList;
	}


	public void setCompanyCodeList(List companyCodeList) {
		this.companyCodeList = companyCodeList;
	}


	public List<String> getOpshub() {
		return opshub;
	}


	public void setOpshub(List<String> opshub) {
		this.opshub = opshub;
	}

	public Map<String, String> getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Map<String, String> baseCurrency) {
		this.baseCurrency = baseCurrency;
	} 
	public Map<String, String> getDriverAgencyList() {
		return driverAgencyList;
	}
	public void setDriverAgencyList(Map<String, String> driverAgencyList) {
		this.driverAgencyList = driverAgencyList;
	}
	public String getEnbState() {
		return enbState;
	}
	public void setEnbState(String enbState) {
		this.enbState = enbState;
	}
	public Map<String, String> getCountryCod() {
		return countryCod;
	}
	public void setCountryCod(Map<String, String> countryCod) {
		this.countryCod = countryCod;
	}
	public void setCustomerFileManager(CustomerFileManager customerFileManager) {
		this.customerFileManager = customerFileManager;
	}
	public boolean isCportalAccessCompanyDivisionLevel() {
		return cportalAccessCompanyDivisionLevel;
	}
	public Company getCompany() {
		return company;
	}
	public void setCportalAccessCompanyDivisionLevel(
			boolean cportalAccessCompanyDivisionLevel) {
		this.cportalAccessCompanyDivisionLevel = cportalAccessCompanyDivisionLevel;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	
	public void setCompanyManager(CompanyManager companyManager) {
		this.companyManager = companyManager;
	}
	public String getJsonCountryCodeText() {
		return jsonCountryCodeText;
	}
	public void setJsonCountryCodeText(String jsonCountryCodeText) {
		this.jsonCountryCodeText = jsonCountryCodeText;
	}
	public boolean isGenerateInvoiceByCompanyDivision() {
		return generateInvoiceByCompanyDivision;
	}
	public void setGenerateInvoiceByCompanyDivision(
			boolean generateInvoiceByCompanyDivision) {
		this.generateInvoiceByCompanyDivision = generateInvoiceByCompanyDivision;
	}
	
}