<%@ include file="/common/taglibs.jsp"%>  
 <head>  
    <title>vanLine List</title>   
    <meta name="heading" content="Driver Acc List"/> 
 </head>   
<s:form id="driverForm" name="DriverForm" method="post" validate="true"> 
<display:table id="driverPreviewAccDetails" name="driverPreviewAccDetails" class="table">
<display:column property="chargeCode" title="Charge Code"></display:column>
<display:column property="distributionAmount" title="Total Revenue" style="text-align: right;" ></display:column>
<display:column property="actualExpense" title="Total Paid" style="text-align: right;" ></display:column>
<display:column property="amtDueAgent" title="Statement Revenue" style="text-align: right;" ></display:column>
<display:column property="total" title="Total" style="text-align: right;" ></display:column>
<display:column property="variance" title="Variance"style="text-align: right;" ></display:column>
<display:column property="status" title="Status"></display:column>
<display:column property="description" title="Description"></display:column>
<c:choose>
<c:when test="${driverPreviewAccDetails.variance=='0.00' || driverPreviewAccDetails.variance=='0' }">
<display:column title="Action"><s:select  id="action${driverPreviewAccDetails.chargeCode}" cssStyle="width:100px" disabled="true" value=""  cssClass="list-menu" list="{''}"  /></display:column>
</c:when>
<c:otherwise>
<display:column title="Action">
<s:select  id="action${driverPreviewAccDetails.chargeCode}" cssStyle="width:100px" onchange="driverList('${driverPreviewAccDetails.chargeCode}','${driverPreviewAccDetails.variance}',this),accDetailsList('${driverPreviewAccDetails.chargeCode}','${driverPreviewAccDetails.soId}','${driverPreviewAccDetails.chargeCode}','${driverPreviewAccDetails.vendorCode}','${driverPreviewAccDetails.estimateVendorName}','${driverPreviewAccDetails.glType}',this);"   value=""  cssClass="list-menu" list="{'','Approved','Dispute','Adjust'}"  />
</display:column>
</c:otherwise>
</c:choose>
<c:choose>
<c:when test="${driverPreviewAccDetails.variance=='0.00' || driverPreviewAccDetails.variance=='0' }">
<display:column title="Amount"><s:textfield id="amount${driverPreviewAccDetails.chargeCode}" disabled="true" cssStyle="width:80px" ></s:textfield></display:column>
</c:when>
<c:otherwise>
<display:column title="Amount"><s:textfield id="amount${driverPreviewAccDetails.chargeCode}" onchange="onlyFloatDriver(this);amountOnchange('${driverPreviewAccDetails.chargeCode}','${driverPreviewAccDetails.soId}','${driverPreviewAccDetails.chargeCode}','${driverPreviewAccDetails.vendorCode}','${driverPreviewAccDetails.estimateVendorName}','${driverPreviewAccDetails.glType}',this);" cssStyle="width:80px"  ></s:textfield></display:column>

</c:otherwise>
</c:choose>
</display:table>
</s:form>