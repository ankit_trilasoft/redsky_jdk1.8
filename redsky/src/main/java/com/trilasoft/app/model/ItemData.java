package com.trilasoft.app.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.FilterDefs;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;


@Entity
@Table(name="itemdata")
public class ItemData extends BaseObject{

	private Long id;
	private Integer workTicketId;
	private Integer serviceOrderId;
	private String itemDesc;
	private Integer quantityReceived;
	private String warehouse;
	private Integer line;
	private Double volume;
	private Double weight;
	private String itemUomid;
	private Double totalWeight;
	private String notes;
	private String corpId;
	private String createdBy;
	private Date createdOn;
	private String updatedBy;
	private Date updatedOn;
	private String qtyExpected;
	private String itemQtyExpected;
	private String itemQtyReceived;
	private Double totalVolume;
	private String shipNumber;
	private Integer ticket;
	private String unitOfVolume;
	private String unitOfWeight;
	private Date released;
	private String packId;
	private String hoTicket;
	private Integer quantityOrdered;
	private Integer quantityShipped;
	private Double weightReceived;
	private Double volumeReceived;
	private String shippingPackId;
	private String totalActQuantity;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((corpId == null) ? 0 : corpId.hashCode());
		result = prime * result
				+ ((createdBy == null) ? 0 : createdBy.hashCode());
		result = prime * result
				+ ((createdOn == null) ? 0 : createdOn.hashCode());
		result = prime * result
				+ ((hoTicket == null) ? 0 : hoTicket.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((itemDesc == null) ? 0 : itemDesc.hashCode());
		result = prime * result
				+ ((itemQtyExpected == null) ? 0 : itemQtyExpected.hashCode());
		result = prime * result
				+ ((itemQtyReceived == null) ? 0 : itemQtyReceived.hashCode());
		result = prime * result
				+ ((itemUomid == null) ? 0 : itemUomid.hashCode());
		result = prime * result + ((line == null) ? 0 : line.hashCode());
		result = prime * result + ((notes == null) ? 0 : notes.hashCode());
		result = prime * result + ((packId == null) ? 0 : packId.hashCode());
		result = prime * result
				+ ((qtyExpected == null) ? 0 : qtyExpected.hashCode());
		result = prime * result
				+ ((quantityOrdered == null) ? 0 : quantityOrdered.hashCode());
		result = prime
				* result
				+ ((quantityReceived == null) ? 0 : quantityReceived.hashCode());
		result = prime * result
				+ ((quantityShipped == null) ? 0 : quantityShipped.hashCode());
		result = prime * result
				+ ((released == null) ? 0 : released.hashCode());
		result = prime * result
				+ ((serviceOrderId == null) ? 0 : serviceOrderId.hashCode());
		result = prime * result
				+ ((shipNumber == null) ? 0 : shipNumber.hashCode());
		result = prime * result
				+ ((shippingPackId == null) ? 0 : shippingPackId.hashCode());
		result = prime * result + ((ticket == null) ? 0 : ticket.hashCode());
		result = prime
				* result
				+ ((totalActQuantity == null) ? 0 : totalActQuantity.hashCode());
		result = prime * result
				+ ((totalVolume == null) ? 0 : totalVolume.hashCode());
		result = prime * result
				+ ((totalWeight == null) ? 0 : totalWeight.hashCode());
		result = prime * result
				+ ((unitOfVolume == null) ? 0 : unitOfVolume.hashCode());
		result = prime * result
				+ ((unitOfWeight == null) ? 0 : unitOfWeight.hashCode());
		result = prime * result
				+ ((updatedBy == null) ? 0 : updatedBy.hashCode());
		result = prime * result
				+ ((updatedOn == null) ? 0 : updatedOn.hashCode());
		result = prime * result + ((volume == null) ? 0 : volume.hashCode());
		result = prime * result
				+ ((volumeReceived == null) ? 0 : volumeReceived.hashCode());
		result = prime * result
				+ ((warehouse == null) ? 0 : warehouse.hashCode());
		result = prime * result + ((weight == null) ? 0 : weight.hashCode());
		result = prime * result
				+ ((weightReceived == null) ? 0 : weightReceived.hashCode());
		result = prime * result
				+ ((workTicketId == null) ? 0 : workTicketId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemData other = (ItemData) obj;
		if (corpId == null) {
			if (other.corpId != null)
				return false;
		} else if (!corpId.equals(other.corpId))
			return false;
		if (createdBy == null) {
			if (other.createdBy != null)
				return false;
		} else if (!createdBy.equals(other.createdBy))
			return false;
		if (createdOn == null) {
			if (other.createdOn != null)
				return false;
		} else if (!createdOn.equals(other.createdOn))
			return false;
		if (hoTicket == null) {
			if (other.hoTicket != null)
				return false;
		} else if (!hoTicket.equals(other.hoTicket))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (itemDesc == null) {
			if (other.itemDesc != null)
				return false;
		} else if (!itemDesc.equals(other.itemDesc))
			return false;
		if (itemQtyExpected == null) {
			if (other.itemQtyExpected != null)
				return false;
		} else if (!itemQtyExpected.equals(other.itemQtyExpected))
			return false;
		if (itemQtyReceived == null) {
			if (other.itemQtyReceived != null)
				return false;
		} else if (!itemQtyReceived.equals(other.itemQtyReceived))
			return false;
		if (itemUomid == null) {
			if (other.itemUomid != null)
				return false;
		} else if (!itemUomid.equals(other.itemUomid))
			return false;
		if (line == null) {
			if (other.line != null)
				return false;
		} else if (!line.equals(other.line))
			return false;
		if (notes == null) {
			if (other.notes != null)
				return false;
		} else if (!notes.equals(other.notes))
			return false;
		if (packId == null) {
			if (other.packId != null)
				return false;
		} else if (!packId.equals(other.packId))
			return false;
		if (qtyExpected == null) {
			if (other.qtyExpected != null)
				return false;
		} else if (!qtyExpected.equals(other.qtyExpected))
			return false;
		if (quantityOrdered == null) {
			if (other.quantityOrdered != null)
				return false;
		} else if (!quantityOrdered.equals(other.quantityOrdered))
			return false;
		if (quantityReceived == null) {
			if (other.quantityReceived != null)
				return false;
		} else if (!quantityReceived.equals(other.quantityReceived))
			return false;
		if (quantityShipped == null) {
			if (other.quantityShipped != null)
				return false;
		} else if (!quantityShipped.equals(other.quantityShipped))
			return false;
		if (released == null) {
			if (other.released != null)
				return false;
		} else if (!released.equals(other.released))
			return false;
		if (serviceOrderId == null) {
			if (other.serviceOrderId != null)
				return false;
		} else if (!serviceOrderId.equals(other.serviceOrderId))
			return false;
		if (shipNumber == null) {
			if (other.shipNumber != null)
				return false;
		} else if (!shipNumber.equals(other.shipNumber))
			return false;
		if (shippingPackId == null) {
			if (other.shippingPackId != null)
				return false;
		} else if (!shippingPackId.equals(other.shippingPackId))
			return false;
		if (ticket == null) {
			if (other.ticket != null)
				return false;
		} else if (!ticket.equals(other.ticket))
			return false;
		if (totalActQuantity == null) {
			if (other.totalActQuantity != null)
				return false;
		} else if (!totalActQuantity.equals(other.totalActQuantity))
			return false;
		if (totalVolume == null) {
			if (other.totalVolume != null)
				return false;
		} else if (!totalVolume.equals(other.totalVolume))
			return false;
		if (totalWeight == null) {
			if (other.totalWeight != null)
				return false;
		} else if (!totalWeight.equals(other.totalWeight))
			return false;
		if (unitOfVolume == null) {
			if (other.unitOfVolume != null)
				return false;
		} else if (!unitOfVolume.equals(other.unitOfVolume))
			return false;
		if (unitOfWeight == null) {
			if (other.unitOfWeight != null)
				return false;
		} else if (!unitOfWeight.equals(other.unitOfWeight))
			return false;
		if (updatedBy == null) {
			if (other.updatedBy != null)
				return false;
		} else if (!updatedBy.equals(other.updatedBy))
			return false;
		if (updatedOn == null) {
			if (other.updatedOn != null)
				return false;
		} else if (!updatedOn.equals(other.updatedOn))
			return false;
		if (volume == null) {
			if (other.volume != null)
				return false;
		} else if (!volume.equals(other.volume))
			return false;
		if (volumeReceived == null) {
			if (other.volumeReceived != null)
				return false;
		} else if (!volumeReceived.equals(other.volumeReceived))
			return false;
		if (warehouse == null) {
			if (other.warehouse != null)
				return false;
		} else if (!warehouse.equals(other.warehouse))
			return false;
		if (weight == null) {
			if (other.weight != null)
				return false;
		} else if (!weight.equals(other.weight))
			return false;
		if (weightReceived == null) {
			if (other.weightReceived != null)
				return false;
		} else if (!weightReceived.equals(other.weightReceived))
			return false;
		if (workTicketId == null) {
			if (other.workTicketId != null)
				return false;
		} else if (!workTicketId.equals(other.workTicketId))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ItemData [id=" + id + ", workTicketId=" + workTicketId
				+ ", serviceOrderId=" + serviceOrderId + ", itemDesc="
				+ itemDesc + ", quantityReceived=" + quantityReceived
				+ ", warehouse=" + warehouse + ", line=" + line + ", volume="
				+ volume + ", weight=" + weight + ", itemUomid=" + itemUomid
				+ ", totalWeight=" + totalWeight + ", notes=" + notes
				+ ", corpId=" + corpId + ", createdBy=" + createdBy
				+ ", createdOn=" + createdOn + ", updatedBy=" + updatedBy
				+ ", updatedOn=" + updatedOn + ", qtyExpected=" + qtyExpected
				+ ", itemQtyExpected=" + itemQtyExpected + ", itemQtyReceived="
				+ itemQtyReceived + ", totalVolume=" + totalVolume
				+ ", shipNumber=" + shipNumber + ", ticket=" + ticket
				+ ", unitOfVolume=" + unitOfVolume + ", unitOfWeight="
				+ unitOfWeight + ", released=" + released + ", packId="
				+ packId + ", hoTicket=" + hoTicket + ", quantityOrdered="
				+ quantityOrdered + ", quantityShipped=" + quantityShipped
				+ ", weightReceived=" + weightReceived + ", volumeReceived="
				+ volumeReceived + ", shippingPackId=" + shippingPackId
				+ ", totalActQuantity=" + totalActQuantity + "]";
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public Integer getWorkTicketId() {
		return workTicketId;
	}
	public void setWorkTicketId(Integer workTicketId) {
		this.workTicketId = workTicketId;
	}
	@Column
	public Integer getServiceOrderId() {
		return serviceOrderId;
	}
	public void setServiceOrderId(Integer serviceOrderId) {
		this.serviceOrderId = serviceOrderId;
	}
	@Column
	public String getItemDesc() {
		return itemDesc;
	}
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}
	@Column
	public Integer getQuantityReceived() {
		return quantityReceived;
	}
	public void setQuantityReceived(Integer quantityReceived) {
		this.quantityReceived = quantityReceived;
	}
	@Column
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	@Column
	public Integer getLine() {
		return line;
	}
	public void setLine(Integer line) {
		this.line = line;
	}
	@Column
	public Double getVolume() {
		return volume;
	}
	public void setVolume(Double volume) {
		this.volume = volume;
	}
	@Column
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	@Column
	public String getItemUomid() {
		return itemUomid;
	}
	public void setItemUomid(String itemUomid) {
		this.itemUomid = itemUomid;
	}
	@Column
	public Double getTotalWeight() {
		return totalWeight;
	}
	public void setTotalWeight(Double totalWeight) {
		this.totalWeight = totalWeight;
	}
	@Column
	public String getNotes() {
		return notes;
	}
	@Column
	public void setNotes(String notes) {
		this.notes = notes;
	}
	@Column
	public String getCorpId() {
		return corpId;
	}
	public void setCorpId(String corpId) {
		this.corpId = corpId;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getQtyExpected() {
		return qtyExpected;
	}
	public void setQtyExpected(String qtyExpected) {
		this.qtyExpected = qtyExpected;
	}
	@Column
	public String getItemQtyExpected() {
		return itemQtyExpected;
	}
	public void setItemQtyExpected(String itemQtyExpected) {
		this.itemQtyExpected = itemQtyExpected;
	}
	
	@Column
	public String getItemQtyReceived() {
		return itemQtyReceived;
	}
	public void setItemQtyReceived(String itemQtyReceived) {
		this.itemQtyReceived = itemQtyReceived;
	}
	@Column
	public Double getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(Double totalVolume) {
		this.totalVolume = totalVolume;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Integer getTicket() {
		return ticket;
	}
	public void setTicket(Integer ticket) {
		this.ticket = ticket;
	}
	@Column
	public String getUnitOfVolume() {
		return unitOfVolume;
	}
	public void setUnitOfVolume(String unitOfVolume) {
		this.unitOfVolume = unitOfVolume;
	}
	@Column
	public String getUnitOfWeight() {
		return unitOfWeight;
	}
	public void setUnitOfWeight(String unitOfWeight) {
		this.unitOfWeight = unitOfWeight;
	}
	@Column
	public Date getReleased() {
		return released;
	}
	public void setReleased(Date released) {
		this.released = released;
	}
	@Column
	public String getPackId() {
		return packId;
	}
	
	public void setPackId(String packId) {
		this.packId = packId;
	}
	@Column
	public String getHoTicket() {
		return hoTicket;
	}
	public void setHoTicket(String hoTicket) {
		this.hoTicket = hoTicket;
	}
	@Column
	public Integer getQuantityOrdered() {
		return quantityOrdered;
	}
	public void setQuantityOrdered(Integer quantityOrdered) {
		this.quantityOrdered = quantityOrdered;
	}
	@Column
	public Integer getQuantityShipped() {
		return quantityShipped;
	}
	public void setQuantityShipped(Integer quantityShipped) {
		this.quantityShipped = quantityShipped;
	}
	@Column
	public Double getWeightReceived() {
		return weightReceived;
	}
	public void setWeightReceived(Double weightReceived) {
		this.weightReceived = weightReceived;
	}
	@Column
	public Double getVolumeReceived() {
		return volumeReceived;
	}
	public void setVolumeReceived(Double volumeReceived) {
		this.volumeReceived = volumeReceived;
	}
	
	@Column
	public String getShippingPackId() {
		return shippingPackId;
	}
	public void setShippingPackId(String shippingPackId) {
		this.shippingPackId = shippingPackId;
	}
	@Column
	public String getTotalActQuantity() {
		return totalActQuantity;
	}
	public void setTotalActQuantity(String totalActQuantity) {
		this.totalActQuantity = totalActQuantity;
	}
}
