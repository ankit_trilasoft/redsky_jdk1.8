package com.trilasoft.app.service;

import java.util.List;

import org.appfuse.service.GenericManager;
import com.trilasoft.app.model.InventoryPacking;

public interface InventoryPackingManager extends GenericManager<InventoryPacking, Long>{
	List searchCriteria(Long sid,String  room,String  article,String sessionCorpID);
	List seachInventoryThroughPieceId(Long sid,String pieceID,String sessionCorpID);
	public List findArticleByPieceId(Long sid, String pieceID,String sessionCorpID);
	
}
