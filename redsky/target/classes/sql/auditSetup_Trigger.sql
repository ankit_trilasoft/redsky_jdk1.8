delimiter $$

CREATE trigger redsky.trigger_add_history_auditSetup
BEFORE UPDATE on redsky.auditSetup
for each row
BEGIN
  DECLARE fieldNameList LONGTEXT;
  DECLARE oldValueList LONGTEXT;
  DECLARE newValueList LONGTEXT;

  SET fieldNameList = " ";
  SET oldValueList = " ";
  SET newValueList = " ";



  IF (OLD.fieldName <> NEW.fieldName ) THEN
      select CONCAT(fieldNameList,'auditSetup.fieldName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.fieldName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.fieldName,'~') into newValueList;
  END IF;





IF ((date_format(OLD.valueDate,'%Y-%m-%d') <> date_format(NEW.valueDate,'%Y-%m-%d')) or (date_format(OLD.valueDate,'%Y-%m-%d') is null and date_format(NEW.valueDate,'%Y-%m-%d') is not null)
      or (date_format(OLD.valueDate,'%Y-%m-%d') is not null and date_format(NEW.valueDate,'%Y-%m-%d') is null)) THEN

       select CONCAT(fieldNameList,'auditSetup.valueDate~') into fieldNameList;
       IF(OLD.valueDate is null) THEN
        	select CONCAT(oldValueList," ",'~') into oldValueList;
      	END IF;
      	IF(NEW.valueDate is null) THEN
        	select CONCAT(newValueList," ",'~') into newValueList;
      	END IF;

    	IF(OLD.valueDate is not null) THEN
        	select CONCAT(oldValueList,OLD.valueDate,'~') into oldValueList;
     	END IF;
      	IF(NEW.valueDate is not null) THEN
        	select CONCAT(newValueList,NEW.valueDate,'~') into newValueList;
      	END IF;

   END IF;



   IF (OLD.tableName <> NEW.tableName ) THEN
      select CONCAT(fieldNameList,'auditSetup.tableName~') into fieldNameList;
      select CONCAT(oldValueList,OLD.tableName,'~') into oldValueList;
      select CONCAT(newValueList,NEW.tableName,'~') into newValueList;
  END IF;



   IF (OLD.auditable <> NEW.auditable ) THEN
      select CONCAT(fieldNameList,'auditSetup.auditable~') into fieldNameList;
      select CONCAT(oldValueList,OLD.auditable,'~') into oldValueList;
      select CONCAT(newValueList,NEW.auditable,'~') into newValueList;
  END IF;




   IF (OLD.corpID <> NEW.corpID ) THEN
      select CONCAT(fieldNameList,'auditSetup.corpID~') into fieldNameList;
      select CONCAT(oldValueList,OLD.corpID,'~') into oldValueList;
      select CONCAT(newValueList,NEW.corpID,'~') into newValueList;
  END IF;




   IF (OLD.description <> NEW.description ) THEN
      select CONCAT(fieldNameList,'auditSetup.description~') into fieldNameList;
      select CONCAT(oldValueList,OLD.description,'~') into oldValueList;
      select CONCAT(newValueList,NEW.description,'~') into newValueList;
  END IF;




   IF (OLD.alertRole <> NEW.alertRole ) THEN
      select CONCAT(fieldNameList,'auditSetup.alertRole~') into fieldNameList;
      select CONCAT(oldValueList,OLD.alertRole,'~') into oldValueList;
      select CONCAT(newValueList,NEW.alertRole,'~') into newValueList;
  END IF;




   IF (OLD.alertValue <> NEW.alertValue ) THEN
      select CONCAT(fieldNameList,'auditSetup.alertValue~') into fieldNameList;
      select CONCAT(oldValueList,OLD.alertValue,'~') into oldValueList;
      select CONCAT(newValueList,NEW.alertValue,'~') into newValueList;
  END IF;



    IF (OLD.isAlert <> NEW.isAlert ) THEN
       select CONCAT(fieldNameList,'auditSetup.isAlert~') into fieldNameList;
       IF(OLD.isAlert = false) THEN
	    	select CONCAT(oldValueList,'FALSE','~') into oldValueList;
	    END IF;
	    IF(OLD.isAlert = true) THEN
	    	select CONCAT(oldValueList,'TRUE','~') into oldValueList;
	    END IF;
	    IF(NEW.isAlert = false) THEN
	    	select CONCAT(newValueList,'FALSE','~') into newValueList;
	    END IF;
	    IF(NEW.isAlert = true) THEN
	    	select CONCAT(newValueList,'TRUE','~') into newValueList;
	    END IF;
   END IF;







  CALL add_tblHistory (OLD.id,"auditSetup", fieldNameList, oldValueList, newValueList, NEW.updatedby, OLD.corpID, now());

END $$


delimiter;