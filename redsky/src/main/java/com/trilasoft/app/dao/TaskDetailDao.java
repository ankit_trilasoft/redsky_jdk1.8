package com.trilasoft.app.dao;

import java.util.List;

import org.appfuse.dao.GenericDao;

import com.trilasoft.app.model.OperationsIntelligence;
import com.trilasoft.app.model.TaskDetail;

public interface TaskDetailDao extends GenericDao<TaskDetail, Long> {
	
public	void saveTaskDetails(Long id, String fieldName, String fieldValue, String sessionCorpID, String selectedWO,String userType);
public List getTaskFromWorkOrder(String shipNumber, String selectedWO);
public String getValueFromTaskDetails(Long id, String shipNumber);
public void updateTransferField(Long id, String shipNumber);

}
