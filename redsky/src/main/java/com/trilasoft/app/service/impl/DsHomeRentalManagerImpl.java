package com.trilasoft.app.service.impl;


import java.util.List;

import org.appfuse.service.impl.GenericManagerImpl;

import com.trilasoft.app.dao.DsHomeRentalDao;
import com.trilasoft.app.model.DsHomeRental;
import com.trilasoft.app.service.DsHomeRentalManager;

public class DsHomeRentalManagerImpl extends GenericManagerImpl<DsHomeRental, Long> implements DsHomeRentalManager {
	
	DsHomeRentalDao dsHomeRentalDao;
	
	public DsHomeRentalManagerImpl(DsHomeRentalDao dsHomeRentalDao) {
		super(dsHomeRentalDao);
		this.dsHomeRentalDao=dsHomeRentalDao;
	}

	public List getListById(Long id) {
		return dsHomeRentalDao.getListById(id);
	}


	
}
