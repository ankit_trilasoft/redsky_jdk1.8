<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>
  
<head>   
    <title><fmt:message key="quoteDetail.title"/></title>   
    <meta name="heading" content="<fmt:message key='quoteDetail.heading'/>"/>  
    <link href="<s:url value="/css/main.css"/>" rel="stylesheet" type="text/css"/>
    <style type="text/css">
	h2 {background-color: #CCCCCC}
	fieldset {		
	padding: 0.0em;
	position: relative;
	width: 70%;  
	}
	</style>
	<style>
<%@ include file="/common/calenderStyle.css"%>
</style>
<script language="javascript" type="text/javascript">
<%@ include file="/common/formCalender.js"%>
</script>

	<script type="text/javascript">
		function calculate() {
			var x=((document.forms['quoteForm'].elements['quote.quantity'].value*document.forms['quoteForm'].elements['quote.rate'].value)*(100-document.forms['quoteForm'].elements['quote.discount'].value))/100;
			if(document.forms['quoteForm'].elements['quote.divideOrMultiply'].value=="/") {
				document.forms['quoteForm'].elements['quote.amount'].value= x/document.forms['quoteForm'].elements['quote.per'].value;
			}else if (document.forms['quoteForm'].elements['quote.divideOrMultiply'].value=="*"){
				document.forms['quoteForm'].elements['quote.amount'].value= x*document.forms['quoteForm'].elements['quote.per'].value;
			}
		}
	</script>	
<script type="text/javascript">
	function autoFilter_PartnerType(targetElement) {
		var partnerType=targetElement.options[targetElement.selectedIndex].value;
		//document.forms['quoteForm'].submit();
		//targetElement.form.elements['customerFile.originCountry'].value=originCountryCode.substring(originCountryCode.indexOf(":")+2,originCountryCode.length);
	}

</script>

<SCRIPT LANGUAGE="JavaScript">


function checkVendorName()
{
    var vendorId = 
document.forms['quoteForm'].elements['partnerQuote.vendorCode'].value;
    //alert(vendorId);
    var url="vendorNamenEmail.html?ajax=1&decorator=simple&popup=true&partnerCode=" + 
encodeURI(vendorId);
     http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse2;
     http2.send(null);
}

String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}

function handleHttpResponse2()
        {

             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                //alert(results);
                var res = results.split('#');
                //alert(res.length);
                if(results.length>5)
                {
              
 					document.forms['quoteForm'].elements['partnerQuote.vendorMail'].value = res[2];
 					document.forms['quoteForm'].elements['partnerQuote.vendorName'].value = res[1];
 				}
                 else
                 {
                 	alert("Invalid Vendor code, please select another");
                    
 	document.forms['quoteForm'].elements['partnerQuote.vendorName'].value="";
	document.forms['quoteForm'].elements['partnerQuote.vendorMail'].value="";
 	document.forms['quoteForm'].elements['partnerQuote.vendorCode'].select();
                 }
             }
        }

function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    
</script>
<script>
function openPopWindow(){
var quoteType = document.forms['quoteForm'].elements['partnerQuote.quoteType'].value;
if(quoteType=='')
{
alert('Please Select Quote Type.')
}

else if(quoteType=='Origin'){javascript:openWindow('originPartners.html?origin=${customerFile.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=partnerQuote.vendorMail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerQuote.vendorName&fld_code=partnerQuote.vendorCode');}
else{
	if(quoteType=='Destination'){javascript:openWindow('destinationPartners.html?destination=${customerFile.destinationCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=partnerQuote.vendorMail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerQuote.vendorName&fld_code=partnerQuote.vendorCode');}
	else{
		if(quoteType=='Freight'){javascript:openWindow('carrierPartners.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=partnerQuote.vendorMail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerQuote.vendorName&fld_code=partnerQuote.vendorCode');}
		else{
			if(quoteType=='Origin + Freight'){javascript:openWindow('freightPartners.html?origin=${customerFile.originCountryCode}&partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=partnerQuote.vendorMail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerQuote.vendorName&fld_code=partnerQuote.vendorCode');}
			else{
				javascript:openWindow('partners.html?partnerType=AG&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=partnerQuote.vendorMail&fld_fourthDescription=fourthDescription&fld_thirdDescription=thirdDescription&fld_secondDescription=secondDescription&fld_description=partnerQuote.vendorName&fld_code=partnerQuote.vendorCode');
			}
		}
	}
}
}
</script>
</head>
<s:hidden name="fileNameFor"  id= "fileNameFor" value="CF"/>
<s:hidden name="fileID" id ="fileID" value="%{customerFile.id}" />
<c:set var="fileID" value="%{customerFile.id}"/>
<s:hidden name="ppType" id ="ppType" value="" />
<c:set var="ppType" value=""/>
<s:form id="quoteForm" name="quoteForm" action="savePartnerQuote" method="post" validate="true"> 
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
    <s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<s:hidden name="partnerQuote.corpID" />
<div id="Layer1" style="width:95%">
<s:hidden name="id" value="<%=request.getParameter("id")%>" />
	<s:hidden name="customerFile.id" />
		<div id="newmnav">
		  <ul>
		  <c:if test="${customerFile.controlFlag=='C'}">
		    <li><a href="editCustomerFile.html?id=${customerFile.id}" ><span>Customer File</span></a></li>
			<li><a href="customerServiceOrders.html?id=${customerFile.id} " ><span>Service Orders</span></a></li>
			</c:if>
			<c:if test="${customerFile.controlFlag!='C'}">
			<li><a href="QuotationFileForm.html?id=<%=request.getParameter("id") %>" ><span>Quotation File</span></a></li>
			<li><a href="quotationServiceOrders.html?id=<%=request.getParameter("id") %> " ><span>Quotes</span></a></li>
			</c:if>
			<li id="newmnav1" style="background:#FFF "><a href="customerRateOrders.html?id=<%=request.getParameter("id") %>" class="current"><span>Rate Request<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
			<li><a href="showAccountPolicy.html?id=${customerFile.id}&code=${customerFile.billToCode}" ><span>Account Policy</span></a></li>
			<li><a onclick="window.open('subModuleReports.html?id=<%=request.getParameter("id")%>&billToCode=${customerFile.billToCode}&jobType=${customerFile.job}&companyDivision=${customerFile.companyDivision}&preferredLanguage=${customerFile.customerLanguagePreference}&reportModule=customerFile&reportSubModule=Rate Request&decorator=popup&popup=true','forms','height=650,width=750,top=1, left=200, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>			
		  </ul>
		</div><div class="spn">&nbsp;</div>
		
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
	<table cellspacing="1" cellpadding="0" border="0" style="width:700px">
		<tbody>
		<tr>
			<td>
			<table class="detailTabLabel" cellspacing="0" cellpadding="3" border="0">
			<tbody>
			<tr>
				<td align="right" class="listwhitebox">Cust#</td>
				<td><s:textfield name="seqnumber" size="21" value="%{customerFile.sequenceNumber}" readonly="true" cssClass="input-textUpper" /></td>
				<td align="right" class="listwhitebox">Shipper</td>
				<td><s:textfield name="customerFile.firstName" required="true" size="20" readonly="true" cssClass="input-textUpper" /></td>
				<td><s:textfield name="customerFile.lastName" required="true" size="20" readonly="true" cssClass="input-textUpper"/></td>
				<td align="right" class="listwhitebox">Origin</td>
				<td><s:textfield name="customerFile.originCityCode" required="true" size="20" readonly="true" cssClass="input-textUpper" /></td>
				<td><s:textfield name="customerFile.originCountryCode" required="true" size="5" readonly="true" cssClass="input-textUpper"/></td>
			</tr>
			<tr>
				<td align="right" class="listwhitebox">Type</td>
				<td><s:textfield name="customerFile.job" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
				<td align="right" class="listwhitebox">Destination</td>
				<td><s:textfield name="customerFile.destinationCityCode" required="true" size="20" readonly="true" cssClass="input-textUpper" /></td>
				<td><s:textfield name="customerFile.destinationCountryCode" required="true" size="15" readonly="true" cssClass="input-textUpper" /></td>
				<td align="right" class="listwhitebox"></td>
				<td align="right" class="listwhitebox"></td>
				<td align="right" class="listwhitebox"></td>
			</tr>
		</tbody>
	</table>
	</td>
	</tr>
	</tbody>
	</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>	
<div id="otabs">
	<ul>
	<li><a href="customerRateOrders.html?id=<%=request.getParameter("id") %>" class="current"><span>Vendor list</span></a></li>
	</ul>
</div>
<div class="spnblk">&nbsp;</div> 
<div id="content" align="center" >
<div id="liquid-round-top">
<div class="top"><span></span></div>
<div class="center-content">
<table class="" cellspacing="1" cellpadding="0"	border="0">
	<tbody>
		<tr>
			<td>
			<table cellspacing="0" cellpadding="3" border="0">
				<tbody>
				<s:hidden name="partnerQuote.id" />
					<tr>
						<td align="right" class="listwhitetext" width="90px"><fmt:message key='partnerQuote.sequenceNumber'/></td>
						<td><s:textfield key="customerFile.sequenceNumber"   size="15"  readonly="true" cssClass="input-textUpper" /></td>
						<td align="right" class="listwhitetext" width="80px"><fmt:message key='partnerQuote.quote'/></td>
						<td><s:textfield key="partnerQuote.quote" required="true" size="18" maxlength="15" readonly="true" cssClass="input-textUpper" /> </td>
						<td align="right" class="listwhitetext" width="80px"><fmt:message key='partnerQuote.quoteType'/><font color="red" size="2">*</font></td>
						<td><s:select name="partnerQuote.quoteType" list="%{quoteTypes}"   cssStyle="width:117px" onchange="autoFilter_PartnerType(this);" cssClass="list-menu"  headerKey="" headerValue="" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.vendorCode'/><font color="red" size="2">*</font></td>
						<td><s:textfield name="partnerQuote.vendorCode"  size="15" maxlength="50"  required="true" cssClass="input-textUpper" onchange="checkVendorName();"/>
						<img class="openpopup" width="17" height="20" onclick="openPopWindow();" align="top" src="<c:url value='/images/open-popup.gif'/>" />
					</td>
					<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.vendorName'/><font color="red" size="2">*</font></td>
					<td><s:textfield name="partnerQuote.vendorName"  size="35" maxlength="50" required="true" cssClass="input-textUpper" /></td>
					<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.vendorMail'/><font color="red" size="2">*</font></td>
					<td align="left" ><s:textfield name="partnerQuote.vendorMail" size="31" maxlength="50" required="true" cssClass="input-text" /></td>
					</tr>
					<tr>
						<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.sentDate'/></td>
						<c:if test="${not empty partnerQuote.sentDate}">
						<td align="left" ><s:text id="FormatedSentDate" name="${FormDateValue}"><s:param name="value" value="partnerQuote.sentDate" /></s:text> 
						<s:textfield name="partnerQuote.sentDate" value="%{FormatedSentDate}" size="15" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-textUpper"/></td>
						</c:if>
						<c:if test="${empty partnerQuote.sentDate}">
						<td align="left" ><s:textfield name="partnerQuote.sentDate" size="15" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-textUpper"/></td>
						</c:if>
						<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.reminderDate'/></td>
						<c:if test="${not empty partnerQuote.reminderDate}">
						<td align="left" class="listwhite"><s:text id="FormatedReminderDate" name="${FormDateValue}"><s:param name="value" value="partnerQuote.reminderDate" /></s:text>
						<s:textfield name="partnerQuote.reminderDate" value="%{FormatedReminderDate}" size="18" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-textUpper"/></td>
						</c:if>
						<c:if test="${empty partnerQuote.reminderDate}">
						<td align="left" ><s:textfield name="partnerQuote.reminderDate" size="18" maxlength="11" readonly="true" onkeydown="return onlyDel(event,this)" cssClass="input-textUpper"/></td>
						</c:if>
						<td align="right" class="listwhitetext"><fmt:message key='partnerQuote.quoteStatus'/></td>
						<c:if test="${ empty partnerQuote.id }">
							<td><s:select name="partnerQuote.quoteStatus" list="{'New','Requested','Processing','Submitted'}" cssStyle="width:107px" cssClass="list-menu" headerKey="" headerValue=""  /></td>
						</c:if>
						<c:if test="${not empty partnerQuote.id}">
							<td><s:textfield name="partnerQuote.quoteStatus" cssStyle="width:107px" cssClass="input-textUpper" readonly="true"  /></td>
						</c:if>
						<s:hidden name="partnerQuote.ship"  />	
					</tr>
				</tbody>
			</table>
		
			</td>
		</tr>
	</tbody>
</table>
	</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<table width="750px">
		<tbody>
			<tr>
			<td align="right" class="listwhitetext" style="width:70px"><b><fmt:message key='container.createdOn'/></td>
						<fmt:formatDate var="containerCreatedOnFormattedValue" value="${partnerQuote.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerQuote.createdOn" value="${containerCreatedOnFormattedValue}" />
						<td width="120px"><fmt:formatDate value="${partnerQuote.createdOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='container.createdBy' /></td>
						
						<c:if test="${not empty partnerQuote.id}">
								<s:hidden name="partnerQuote.createdBy"/>
								<td><s:label name="createdBy" value="%{partnerQuote.createdBy}"/></td>
							</c:if>
							<c:if test="${empty partnerQuote.id}">
								<s:hidden name="partnerQuote.createdBy" value="${pageContext.request.remoteUser}"/>
								<td><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedOn'/></td>
						<fmt:formatDate var="containerUpdatedOnFormattedValue" value="${partnerQuote.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
						<s:hidden name="partnerQuote.updatedOn" value="${containerUpdatedOnFormattedValue}" />
						<td width="120px"><fmt:formatDate value="${partnerQuote.updatedOn}" pattern="${displayDateTimeFormat}"/></td>		
						<td align="right" class="listwhitetext" style="width:90px"><b><fmt:message key='container.updatedBy' /></td>
						<c:if test="${not empty partnerQuote.id}">
							<s:hidden name="partnerQuote.updatedBy"/>
							<td style="width:85px"><s:label name="updatedBy" value="%{partnerQuote.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty partnerQuote.id}">
							<s:hidden name="partnerQuote.updatedBy" value="${pageContext.request.remoteUser}"/>
							<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>		
			</tr>
		</tbody>
	</table>
</div>
<s:hidden name="partnerQuote.originCity" />
<s:hidden name="partnerQuote.destinationCity" />
<s:hidden name="secondDescription"/>
<s:hidden name="thirdDescription"/>
<s:hidden name="fourthDescription"/>
<s:hidden name="fifthDescription"/>
<s:hidden name="sixthDescription"/>	
<s:submit cssClass="cssbutton" method="save" key="button.save"  cssStyle="width:55px; height:25px" />  
<input type="button" value="Add"  class="cssbutton" style="width:55px; height:25px" onclick="location.href='<c:url value="/editNewPartnerQuote.html?id=${customerFile.id}"/>'" />   
 <s:reset type="button" key="Reset" cssClass="cssbutton" cssStyle="width:55px; height:25px" />
</s:form>

<s:form id="cancelForm" action="quotes" method="post" validate="true"> 
<!-- 	<s:hidden name="customerFile.sequenceNumber"/>  
	<s:hidden name="quote.id"/>
	<table>
		<tr>
			<td>
				<s:submit cssClass="button" method="listQuotes" key="button.cancel"/> 
			</td>
			<td>
				<c:if test="${not empty quote.id}">    
        			<s:submit cssClass="button" method="delete" key="button.delete" onclick="return confirmDelete('quote')"/>   
    			</c:if>    
    		</td>
    	</tr>
    </table>    -->
    <c:set var="idOfWhom" value="${customerFile.id}" scope="session"/>
<c:set var="noteID" value="${customerFile.sequenceNumber}" scope="session"/>
<c:set var="noteFor" value="CustomerFile" scope="session"/>
<c:if test="${empty customerFile.id}">
	<c:set var="isTrue" value="false" scope="request"/>
</c:if>
<c:if test="${not empty customerFile.id}">
	<c:set var="isTrue" value="true" scope="request"/>
</c:if>
</s:form>

<script type="text/javascript">   
	Form.focusFirstElement($("quoteForm"));   
</script>  