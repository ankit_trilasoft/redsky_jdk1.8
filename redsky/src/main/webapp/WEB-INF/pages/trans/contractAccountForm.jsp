<%@ include file="/common/taglibs.jsp"%> 
<%@ taglib prefix="s" uri="/struts-tags" %>

<head>
<meta name="heading" content="<fmt:message key='contractAccountForm.title'/>"/> 
<title><fmt:message key="contractAccountForm.heading"/></title> 

</head>
<s:form id="contractAccountForm" name="contractAccountForm"  method="post" validate="true">
<div id="Layer1" style="width: 100%">
<c:set var="id" value="<%=request.getParameter("contractid")%>" scope="session"/>
<s:hidden name="contractid" value="<%=request.getParameter("contractid")%>"/>
<s:hidden name="contractAccount.contract"/>

<div id="newmnav">
		  <ul>
		    <li id="newmnav1" style="background:#FFF "><a class="current"><span>Contract Account Form<img src="images/navarrow.gif" align="absmiddle" /></span></a></li>
		    <li><a href="contractAccountList.html?id=${id}"><span>Contract Account List</span></a></li>
		  </ul>
		</div><div class="spn">&nbsp;</div>
	
	<div id="content" align="center">
<div id="liquid-round-top">
   <div class="top" ><span></span></div>
   <div class="center-content">	
	<table class="" cellspacing="1" cellpadding="1" border="0" style="width:600px">
  <tbody>
  <tr>
  	<td align="left" class="listwhitetext">
  	<table class="detailTabLabel" border="0">
		  <tbody>  	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>		  	
		  	<tr>
		  	<td align="left"><fmt:message key="contractAccount.accountCode"/><font color="red" size="2">*</font></td>
		  	<td align="left"><s:textfield name="contractAccount.accountCode"   size="6"   cssClass="input-text" readonly="false" onchange="contractAccounts('fromField');" /></td>
		  	<td align="left">
		  	<c:if test="${contract.owner ==  contractAccount.corpID}" >
		  	<img class="openpopup" width="17" height="20"
			onclick="javascript:openWindow('partnersPopup.html?partnerType=AC&decorator=popup&popup=true&fld_sixthDescription=sixthDescription&fld_fifthDescription=fifthDescription&fld_fourthDescription=fourthDescription&fld_secondDescription=secondDescription&fld_description=contractAccount.accountName&fld_thirdDescription=thirdDescription&fld_code=contractAccount.accountCode');"
			src="<c:url value='/images/open-popup.gif'/>" />
			</c:if></td>
		  	<td align="left"><fmt:message key="contractAccount.accountName"/></td>
		  	<td align="left"><s:textfield name="contractAccount.accountName" size="50"   cssClass="input-textUpper" readonly="true"/></td>
		  	</tr>
			</tbody>
		  </table>
		  
		  
		 </td>
		</tr>
		<tr>
		  		<td  height="5px"></td>
		  	</tr>
	</tbody>
</table>
</div>
<div class="bottom-header"><span></span></div>
</div>
</div>
<s:hidden name="secondDescription" />
<s:hidden name="thirdDescription" />
<s:hidden name="fourthDescription" />
<s:hidden name="fifthDescription" />
<s:hidden name="sixthDescription" />
<td background="<c:url value='/images/bg-right.jpg'/>" align="left"></td> 
		  <table class="detailTabLabel" border="0" >
		  
		  </table>
		  
			<table border="0" width="750px;">
					<tbody>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" width="30px"></td>
													<td colspan="5"></td>
						</tr>
						<tr>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='contractAccount.createdOn'/></b></td>
							
							<td valign="top">
							
							</td>
							<td style="width:280px">
							<fmt:formatDate var="customerFileCreatedOnFormattedValue" value="${contractAccount.createdOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="contractAccount.createdOn" value="${customerFileCreatedOnFormattedValue}"/>
							<fmt:formatDate value="${contractAccount.createdOn}" pattern="${displayDateTimeFormat}"/>
							</td>		
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='contractAccount.createdBy' /></b></td>
							<c:if test="${not empty contractAccount.id}">
								<s:hidden name="contractAccount.createdBy"/>
								<td style="width:100px"><s:label name="createdBy" value="%{contractAccount.createdBy}"/></td>
							</c:if>
							<c:if test="${empty contractAccount.id}">
								<s:hidden name="contractAccount.createdBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="createdBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
							
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='contractAccount.updatedOn'/></b></td>
							<fmt:formatDate var="customerFileupdatedOnFormattedValue" value="${contractAccount.updatedOn}" 
							pattern="${displayDateTimeEditFormat}"/>
							<s:hidden name="contractAccount.updatedOn" value="${customerFileupdatedOnFormattedValue}"/>
							<td style="width:290px"><fmt:formatDate value="${contractAccount.updatedOn}" pattern="${displayDateTimeFormat}"/></td>
							<td align="left" class="listwhitetext" style="width:75px"><b><fmt:message key='contractAccount.updatedBy' /></b></td>
							<c:if test="${not empty contractAccount.id}">
								<s:hidden name="contractAccount.updatedBy"/>
								<td style="width:100px"><s:label name="updatedBy" value="%{contractAccount.updatedBy}"/></td>
							</c:if>
							<c:if test="${empty contractAccount.id}">
								<s:hidden name="contractAccount.updatedBy" value="${pageContext.request.remoteUser}"/>
								<td style="width:100px"><s:label name="updatedBy" value="${pageContext.request.remoteUser}"/></td>
							</c:if>
						</tr>
					</tbody>
				</table>
		<table class="detailTabLabel" border="0">
		  <tbody> 	
		  	<tr>
		  		<td align="left" height="3px"></td>
		  	</tr>	  	
		   <tr>
		  		<td align="left">
		  		<input type="button" class="cssbutton1" style="width:65px;"  value="Save" onclick="return checkCode();"/> 
        	  
        		</td>
       
        		<td align="right">
        		<s:reset cssClass="cssbutton1"  type="button" key="Reset"/> 
        		</td>
        		
        		<td align="right">
        		<c:if test="${not empty contractAccount.id}">
		       <input type="button" class="cssbutton1" value="Add New Account" style="width:120px;" onclick="location.href='<c:url value="/editcontractAccountForm.html?contractid=${contractid}"/>'" />   
	      		</c:if>
        		
       	  	</tr>		  	
		 </tbody>
	</table>
</div>
<s:hidden name="contractAccount.id" />
</s:form>

<script language="JavaScript">
function savePage(){
	 document.forms['contractAccountForm'].action ="saveContractAccountForm.html";
	 document.forms['contractAccountForm'].submit();
	 }
function checkCode(){
	 if(document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value==""){
	   alert("Please fill the value in the Agent Code");
	   return false;
	 }
	 if(document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value.search("'")!=-1){
	   alert("Please fill the valid value in the Agent Code");
	   document.forms['contractAccountForm'].elements['contractAccount.accountName'].value='';
	   return false;
	 }
	 contractAccounts('save');
	}
	

</script>

<script>

function contractAccounts(temp){
     var code = document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value;
     code=code.trim();
     if(code!="'" && code!=""){
		 var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(code);
		 http2.open("GET", url, true);
	     http2.onreadystatechange = function(){handleHttpResponsegetAccounts(temp);};
	     http2.send(null);
     }else{
    	 document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value="";
    	 document.forms['contractAccountForm'].elements['contractAccount.accountName'].value="";
     }
}
function handleHttpResponsegetAccounts(temp)
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                 var res = results.split("#");
               if(res.length>1)
                {
 					document.forms['contractAccountForm'].elements['contractAccount.accountName'].value = res[1];
 					if(temp=="save"){ 
 					savePage();
 					}
 					return true;				
                 }
                 else
                 {
                	 alert("Account Code is not valid." ); 
                    document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value="";
                    document.forms['contractAccountForm'].elements['contractAccount.accountName'].value="";
                    return false; 
                  }
             }}
             
function getContractAccount(){
     var code = document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value;
	 var url="vendorName.html?ajax=1&decorator=simple&popup=true&partnerCode=" + encodeURI(code);
	 http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponsegetAccount;
     http2.send(null);

}


String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g,"");
}
String.prototype.ltrim = function() {
    return this.replace(/^\s+/,"");
}
String.prototype.rtrim = function() {
    return this.replace(/\s+$/,"");
}
String.prototype.lntrim = function() {
    return this.replace(/^\s+/,"","\n");
}

function handleHttpResponsegetAccount()
        {
             if (http2.readyState == 4)
             {
                var results = http2.responseText
                results = results.trim();
                 var res = results.split("#");
               if(res.length>1)
                {
 					document.forms['contractAccountForm'].elements['contractAccount.accountName'].value = res[1];
                 }
                 else
                 {
                     alert("Invalid Account code, please select another");
					 document.forms['contractAccountForm'].elements['contractAccount.accountCode'].value="";
                 }
             }
        }
function getHTTPObject()
{
    var xmlhttp;
    if(window.XMLHttpRequest)
    {
        xmlhttp = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        if (!xmlhttp)
        {
            xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    return xmlhttp;
}
    var http2 = getHTTPObject();
    function checkReadOnly(){
        var chkCorpId= '${contract.owner}';
        var corpId='${contractAccount.corpID}';
        if(corpId!=chkCorpId){
          var len = document.forms['contractAccountForm'].length;
          for (var i=0 ;i<len ;i++){ 
        	  document.forms['contractAccountForm'].elements[i].disabled = true;
          }
       }
    }
</script>

<script type="text/javascript">
	try{
	var f = document.getElementById('contractAccountForm'); 
	f.setAttribute("autocomplete", "off"); 
	}
	catch(e){}
	try{checkReadOnly();}
	catch(e){}
</script>