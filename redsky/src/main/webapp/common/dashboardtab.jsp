		<%@ include file="/common/taglibs.jsp"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ include file="/common/tooltip.jsp"%>
<%@ include file="/WEB-INF/pages/trans/googleKey.jsp" %>
<%@ include file="/WEB-INF/pages/trans/soDashBoardJS.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn1"%>
<s:hidden name="shipSize" />
<s:hidden name="minShip" />
<s:hidden name="countShip"/>
<s:form id="dashboard">
		<div id="newmnav" style="float: left;">
	<ul><c:if test="${not empty serviceOrder.id}">		  
   
	  <configByCorp:fieldVisibility componentId="component.Dynamic.DashBoard">
	         <sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	         	  <c:if test="${(fn1:indexOf(dashBoardHideJobsList,serviceOrder.job)==-1)}">
       <c:if test ="${(empty serviceOrder.moveType || serviceOrder.moveType=='BookedMove') && serviceOrder.corpID!='STVF'}">
			<li id="newmnav1" style="background:#FFF"><a  href="redskyDashboard.html?sid=${serviceOrder.id}" class="current"><span>Dashboard</span></a>
      </c:if>
		</c:if>
		</sec-auth:authComponent>
		  </configByCorp:fieldVisibility>
		<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<li ><a  id="editServiceOrderUpdate" onclick="redskyservice(this.id);"
			><span>S/O Details</span></a>
		</li>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.billingTab">
		<sec-auth:authComponent componentId="module.serviceOrderbillingTab.edit" >
			<li><a id="editBilling"  onclick="redskyservice(this.id);"><span>Billing</span></a></li>
		</sec-auth:authComponent>
		</sec-auth:authComponent>
		<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
		<c:choose>
			<c:when test='${serviceOrder.job == "" || serviceOrder.job == null }'>
		 		<li><a onclick="javascript:alert('Job type is blank, please select job type in the service order detail page.')"><span>Accounting</span></a></li>	
			</c:when>
			<c:otherwise> 
		       <li><a id="accountLineList"  onclick="redskyservice(this.id);"><span>Accounting</span></a>
		       </li>
		    </c:otherwise>
	  </c:choose>
	  </sec-auth:authComponent>
<c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      
	  <sec-auth:authComponent componentId="module.tab.serviceorder.operationResourceTab">
	  <li><a id="operationResource"  onclick="redskyservice(this.id);"><span>O&I</span></a></li>
	  </sec-auth:authComponent>
	  </c:if>
	  
<%--   <c:if test="${serviceOrder.job =='OFF'}"> --%>	
  <c:if test="${fn1:indexOf(oiJobList,serviceOrder.job)>=0}">   
      
      <sec-auth:authComponent componentId="module.accountingPortalTab.serviceorder.operationResourceTab">
      <li><a  id="operationResource"  onclick="redskyservice(this.id);"><span>O&I</span></a></li>
      </sec-auth:authComponent>
      </c:if>
	  <sec-auth:authComponent componentId="module.tab.serviceorder.forwardingTab">
	  <c:if test="${usertype!='ACCOUNT'}">
	    <c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a id="containersAjaxList"  onclick="redskyservice(this.id);"><span>Forwarding</span></a></li>
	  </c:if>
	  </c:if>
	  <c:if test="${usertype=='ACCOUNT'}">
	  <c:if test="${serviceOrder.job !='RLO'}"> 
	  <li><li><a id="containersAjaxList"  onclick="redskyservice(this.id);"><span>Forwarding</span></a></li></li>
	  </c:if>
	  </c:if>
	  </sec-auth:authComponent>
		 <sec-auth:authComponent componentId="module.tab.serviceorder.domesticTab">
		<c:if test="${serviceOrder.job !='INT' && serviceOrder.job !='JVS'}">
		<c:if test="${serviceOrder.job !='RLO'}">  
			<li><a id="editMiscellaneous"  onclick="redskyservice(this.id);"><span>Domestic</span></a></li>
		</c:if>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.INTdomesticTab">
      <c:if test="${serviceOrder.job =='INT'}">
         <li><a id="editMiscellaneous"  onclick="redskyservice(this.id);"><span>Domestic</span></a></li>
      </c:if>
      </sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.statusTab">
		<li><a id="editTrackingStatus"  onclick="redskyservice(this.id);"><span>Status</span></a></li>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.ticketTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 
		<li><a id="customerWorkTickets"  onclick="redskyservice(this.id);"><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.Rlo.claimsTab">
	<c:if test="${serviceOrder.job =='RLO'}"> 	
		<li><a id="claims"  onclick="redskyservice(this.id);"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent>
	
	<sec-auth:authComponent componentId="module.tab.serviceorder.ticketTab">
	<c:if test="${serviceOrder.job !='RLO'}"> 
		<li><a id="customerWorkTickets"  onclick="redskyservice(this.id);"><span>Ticket</span></a></li>
	</c:if>
	</sec-auth:authComponent>	
	<sec-auth:authComponent componentId="module.tab.serviceorder.claimsTab">
	<c:if test="${serviceOrder.job !='RLO'}"> 	
		<li><a id="claims"  onclick="redskyservice(this.id);"><span>Claims</span></a></li>
	</c:if> 
	</sec-auth:authComponent>
<sec-auth:authComponent componentId="module.tab.serviceorder.accountingTab">
	<c:if test="${voxmeIntergartionFlag=='true'}">		
		<li><a  id="inventoryDataList"  onclick="redskyservice(this.id);"><span>Survey Details</span></a></li>
	</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.serviceorderTab">
		<c:if test="${ usertype=='AGENT' && surveyTab}">
			<li><a  id="inventoryDataList"  onclick="redskyservice(this.id);"><span>Survey Details</span></a></li>
		</c:if>
	</sec-auth:authComponent>
	<sec-auth:authComponent componentId="module.tab.serviceorder.customerFileTab">	
		<li><a id="editCustomerFile"  onclick="redskyservice(this.id);"><span>Customer File</span></a></li>
	</sec-auth:authComponent>
	
	<sec-auth:authComponent componentId="module.tab.serviceorder.reportTab">	
			<li><a onmouseover="return chkSelect();" onclick="window.open('subModuleReports.html?id=${serviceOrder.id}&jobNumber=${serviceOrder.shipNumber}&regNumber=${serviceOrder.registrationNumber}&companyDivision=${serviceOrder.companyDivision}&jobType=${serviceOrder.job}&modes=${serviceOrder.mode}&billToCode=${billing.billToCode}&reportModule=serviceOrder&reportSubModule=serviceOrder&decorator=popup&popup=true','forms','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Forms</span></a></li>
	</sec-auth:authComponent>
<%-- 	<sec-auth:authComponent componentId="module.tab.serviceorder.auditTab">			
			<li><a onmouseover="return chkSelect();" onclick="window.open('auditList.html?id=${serviceOrder.id}&tableName=serviceorder,miscellaneous&decorator=popup&popup=true','audit','height=400,width=750,top=20, left=610, scrollbars=yes,resizable=yes')"><span>Audit</span></a></li>
	</sec-auth:authComponent> --%>
	 <sec-auth:authComponent componentId="module.tab.trackingStatus.documentTab">
         <li><a href="accountFiles.html?sid=${serviceOrder.id}&seqNum=${serviceOrder.sequenceNumber}"><span>Document</span></a></li>
 	 </sec-auth:authComponent>
 	  	<c:if test="${usertype=='USER'}">
	 	<configByCorp:fieldVisibility componentId="component.emailSetUpTemplateTab">
	  		<li><a href="findEmailSetupTemplateByModuleNameSO.html?sid=${serviceOrder.id}"><span>View Emails</span></a></li>
	  	</configByCorp:fieldVisibility>
  	</c:if>
      	</c:if>
		<c:if test="${not empty serviceOrder.id}">
	 	<td width="20px" align="right">
	 	<c:if test="${serviceOrder.ship > minShip}" >
	 	<li class="arrows">
  		<a><img align="middle" onclick="goPrev();" alt="Previous" title="Previous" src="images/navarrows_03.png"/></a></li>
  		</c:if>
  		<c:if test="${serviceOrder.ship == minShip}" >
  		<li class="arrows"><a><img align="middle" src="images/navdisable_03.png"/></a></li>
  		</c:if>
  		</td>
  		<%-- <td style=" border-right:medium solid #99BBE8; "><a><img class="openpopup" onclick="goCSO();" height="50" width="25" src="images/previous.png" alt="Customer SO List" title="Customer SO List" /></a></td> --%> 
     	<td width="20px" align="left">
  		<c:if test="${serviceOrder.ship < shipSize}" >
  		<li class="arrows"><a><img align="middle" onclick="goNext();" alt="Next" title="Next" src="images/navarrows_04.png"/></a></li>
		</c:if>
		<c:if test="${serviceOrder.ship == shipSize}" >
  		<li class="arrows"><a><img align="middle" src="images/navdisable_04.png"/></a></li>
  		</c:if>
		</td>
		<c:if test="${countShip != 1}" >
		<td width="20px" align="left" valign="top" style="vertical-align:top;!padding-top:1px;">		
		<li class="arrows"><a><img class="openpopup" onclick="findCustomerOtherSO(this);" src="${pageContext.request.contextPath}/images/navarrows_05.png" alt="Customer SO List" title="Customer SO List" /></a> </li>
		</td>
		</c:if>
		<c:if test="${countShip == 1}" >
		<td width="20px" align="left" style="!vertical-align:top;">
  		<li class="arrows"><a><img align="middle" src="images/navdisable_05.png"/></a></li>
  		</td>
  		</c:if>
  		<c:if test="${soQuickView=='Y'}">
  		<td width="20px" align="left" style="padding-left:5px;vertical-align: bottom; padding-bottom: 0px;">
			<a><img class="openpopup" onclick="javascript:openWindow('serviceOrderQuickView.html?id=${serviceOrder.id}&decorator=popup&popup=true',950,400)" src="${pageContext.request.contextPath}/images/icon-soqview.png" alt="SO Quick View" title="SO Quick View" /></a> 
		</td>
		</c:if>		
		</c:if>
	</ul>
	</div>



   <s:hidden value="${serviceOrder.id}" id="id"/>
   <s:hidden value="${serviceOrder.shipNumber}" id="shipNumber"/>
      <s:hidden name="serviceOrder.sequenceNumber"/>
</s:form>
    
<script LANGUAGE="JavaScript">

function goPrev() {
	progressBarAutoSave('1');
		var soIdNum =${serviceOrder.id};
		var seqNm =document.forms['dashboard'].elements['serviceOrder.sequenceNumber'].value;
	var url="editPrevServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
   http5.onreadystatechange = handleHttpResponseOtherShip; 
   http5.send(null); 
 } 
function goNext() {
	progressBarAutoSave('1');
	var soIdNum =${serviceOrder.id}
	var seqNm =document.forms['dashboard'].elements['serviceOrder.sequenceNumber'].value;
	var url="editNextServiceOrder.html?ajax=1&decorator=simple&popup=true&soIdNum="+encodeURI(soIdNum)+"&seqNm="+encodeURI(seqNm);
	 http5.open("GET", url, true); 
   http5.onreadystatechange = handleHttpResponseOtherShip; 
   http5.send(null); 
 } 
function handleHttpResponseOtherShip(){
           if (http5.readyState == 4) {
             var results = http5.responseText
             results = results.trim();
             location.href = 'editServiceOrderUpdate.html?id='+results;
           }   }  
var http5 = getHTTPObject50();
function getHTTPObject50() {
var xmlhttp;
if(window.XMLHttpRequest)  {
    xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)  {
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
    if (!xmlhttp)  {
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
}
return xmlhttp;
}
function findCustomerOtherSO(position) {
	 var sid=${customerFile.id};
	 var soIdNum=${serviceOrder.id};
	 var url="customerOtherSO.html?ajax=1&decorator=simple&popup=true&id=" + encodeURI(sid)+"&soIdNum="+encodeURI(soIdNum);
	  ajax_showTooltip(url,position);	
	  }

function goToUrl(id)
{
	location.href = "editServiceOrderUpdate.html?id="+id;
}
</script>
