<%@ include file="/common/taglibs.jsp" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<head>   
    <title>NetworkDataFields Form</title>   
    <meta name="heading" content="networkDataField Form"/> 
    <style><%@ include file="/common/calenderStyle.css"%></style>
    <script language="javascript" type="text/javascript">
       <%@ include file="/common/formCalender.js"%>
     var cal = new CalendarPopup();
       cal.showYearNavigation();
       cal.showYearNavigationInput();
    </script>
   
   <script language="javascript" type="text/javascript" SRC="${pageContext.request.contextPath}/scripts/calendar.js">
   </script> 
    <script language="javascript" type="text/javascript">
		var cal = new CalendarPopup(); 
		cal.showYearNavigation(); 
		cal.showYearNavigationInput();  
function notExists(){
	alert("The Form information has not been saved yet, please save Form to continue");
}
    function authUpdatedDate() {
		var mydate=new Date();
		var daym;
		var year=mydate.getFullYear()
		var y=""+year;
		if (year < 1000)
		year+=1900
		var day=mydate.getDay()
		var month=mydate.getMonth()+1
		if(month == 1)month="Jan";
		if(month == 2)month="Feb";
		if(month == 3)month="Mar";
		if(month == 4)month="Apr";
		if(month == 5)month="May";
		if(month == 6)month="Jun";
		if(month == 7)month="Jul";
		if(month == 8)month="Aug";
		if(month == 9)month="Sep";
		if(month == 10)month="Oct";
		if(month == 11)month="Nov";
		if(month == 12)month="Dec";
		var daym=mydate.getDate()
		if (daym<10)
		daym="0"+daym;
		var datam = daym+"-"+month+"-"+y.substring(2,4);
		document.forms['gauravForm'].elements['gaurav.updatedOn'].value=datam;
	}
    function myDate() {
	var mydate=new Date()
	var year=mydate.getYear()
	if (year < 1000)
	year+=1900
	var day=mydate.getDay()
	var month=mydate.getMonth()+1
	if (month<10)
	month="0"+month
	var daym=mydate.getDate()
	if (daym<10)
	daym="0"+daym
	var datam = month+"/"+daym+"/"+year;
	}
	function focusDate(target){
	document.forms['networkDataFieldsForm'].elements[target].focus();
}


function ContainerAutoSave(clickType){
    if(!(clickType == 'save')){
if(document.forms['networkDataFieldsForm'].elements['gotoPageString'].value == 'gototab.gaurav_2'){
                
                location.href = 'networkDataFieldList.html';
    }
    }
   }   

function changeStatus()
{
    document.forms['networkDataFieldsForm'].elements['formStatus'].value = '1';
}

window.onload = function() {
	var corpid = document.getElementById("sessionCorpID").value;
	if(corpid != 'TSFT'){
		document.getElementById("networkDataFields.modelName").disabled = true;
		document.getElementById("networkDataFields.fieldName").disabled = true;
		document.getElementById("networkDataFields.toModelName").readOnly = true;
		document.getElementById("networkDataFields.toFieldName").readOnly = true;
		document.getElementById("networkDataFields.transactionType").disabled = true;
		document.getElementById("networkDataFields.type").disabled = true;
		document.getElementById("networkDataFields.useSysDefault").disabled = true;
		document.getElementById("mydiv").style.display="none";
	}
}

function changeStatus(){
	document.forms['networkDataFieldsForm'].elements['formStatus'].value = '1';
}

function getContract(targetElement) {
	var tableNames = targetElement.value;
	// alert(tableNames);
	var url="findfieldListAudit.html?ajax=1&decorator=simple&popup=true&tableNames=" + encodeURI(tableNames);
    // alert(url);
    	http2.open("GET", url, true);
     http2.onreadystatechange = handleHttpResponse3;
     http2.send(null);
}

function handleHttpResponse3()
{
     if (http2.readyState == 4)
     {
        var results = http2.responseText
        results = results.trim();
        // alert(results);
        res = results.split("@");
        targetElement = document.forms['networkDataFieldsForm'].elements['networkDataFields.fieldName'];
		targetElement.length = res.length;
			for(i=0;i<res.length;i++)
			{
			if(res[i] == ''){
			document.forms['networkDataFieldsForm'].elements['networkDataFields.fieldName'].options[i].text = '';
			document.forms['networkDataFieldsForm'].elements['networkDataFields.fieldName'].options[i].value = '';
			}else{
			// contractVal = res[i].split("#");
			document.forms['networkDataFieldsForm'].elements['networkDataFields.fieldName'].options[i].text = res[i];
			document.forms['networkDataFieldsForm'].elements['networkDataFields.fieldName'].options[i].value = res[i];
			}
			document.getElementById("networkDataFields.fieldName").value = '${networkDataFields.fieldName}';
			}
     }
}

function getHTTPObject()
{
var xmlhttp;
if(window.XMLHttpRequest)
{
xmlhttp = new XMLHttpRequest();
}
else if (window.ActiveXObject)
{
xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
if (!xmlhttp)
{
    xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
}
}
return xmlhttp;
}
var http2 = getHTTPObject();

function onlyCharsAllowed(evt)
{
  var keyCode = evt.which ? evt.which : evt.keyCode;
  return (keyCode >= 0 && keyCode <= 32) || (keyCode >= 65 && keyCode <= 90) || (keyCode==222) || (keyCode==35) || (keyCode==36) || (keyCode==37) || (keyCode==39) || ( keyCode==190) || (keyCode==46); 
}

</script>
</head> 
<s:form id="networkDataFieldsForm" name="networkDataFieldsForm" action="networkDataFieldSave" > 
<s:hidden name="networkDataFields.id"/>
<s:hidden name="gotoPageString" id="gotoPageString" value="" />
<s:hidden name="formStatus" value=""/>
<s:hidden name="sessionCorpID"></s:hidden>
<s:hidden name="formStatus" value=""/>
<c:if test="${validateFormNav == 'OK'}" >
<c:choose>
<c:when test="${gotoPageString == 'gototab.gaurav_2' }">
    <c:redirect url="/networkDataFieldList.html?id=${networkDataFields.id}"/>
</c:when>
</c:choose>
</c:if>
 <c:set var="var" value="0" />
     <c:if test="${(networkDataFields.useSysDefault=='false')}">
        <c:set var="var" value="0" />
      </c:if>
       <c:if test="${(networkDataFields.useSysDefault=='true')}">
        <c:set var="var" value="1" />
      </c:if>
<c:set var="FormDateValue" value="{0,date,dd-MMM-yy}"/>
<s:hidden id="dateFormat" name="dateFormat" value="dd-NNN-yy"/>
<div id="layer4" style="width:100%;">
      <div id="newmnav">
		  <ul>
		  	<li id="newmnav1" ><a class="current"  onclick="setReturnString('gototab.gaurav_1');return autoSave();"><span>Form<img src="images/navarrow.gif" align="absmiddle"/></span></a></li>
		  	<li><a onclick="setReturnString('gototab.gaurav_2');return  ContainerAutoSave('none');"><span>List</span></a></li>
		  </ul>
	  </div>
	  <div class="spnblk">&nbsp;</div>
	  <div id="content" align="center">
  <div id="liquid-round">
  <div class="top" style="margin-top:10px;!margin-top:-4px;"><span></span></div>
  <div class="center-content">      
     <table class=""  cellspacing="0" cellpadding="2" border="0" width="800px">
			<tr>
	  		     <td>
	  	             <table cellpadding="0" cellspacing="0" border="0" width="50%" style="margin: 0px;cursor:pointer;">
				       <tr>
				        
				       </tr>
			        </table>
	  		    </td>
	  	     </tr>
		         <tr>
		           <td align="right" class="listwhitetext">Model Name<font color="red" size="2">*</font></td>
		           <td align="left" class="listwhitetext"><s:select  name="networkDataFields.modelName" id="networkDataFields.modelName" list="%{combo1Map}" headerKey=" " headerValue=" "  cssClass="list-menu" tabindex="3" cssStyle="width:120px" onchange="changeStatus();getContract(this);"/></td>  
		           <td align="right" class="listwhitetext" width="120">Field Name <font color="red" size="2">*</font></td>  
		           <td align="left" class="listwhitetext"><s:select name="networkDataFields.fieldName" id="networkDataFields.fieldName" list="%{fieldList}" headerKey=" " headerValue=" "  cssClass="list-menu" tabindex="2" cssStyle="width:160px" onkeydown="return onlyCharsAllowed(event)" onchange="changeStatus();"/></td>
		           <%--<td align="left" colspan="2"><s:select list="%{fieldList}" id="fieldName" cssStyle="width:160px;"  headerKey=" " headerValue=" " name="auditSetup.fieldName"   onkeydown="return onlyCharsAllowed(event)" cssClass="list-menu"  onblur="copyToDiscription();" onchange="changeStatus();copyToDiscription();"/></td>
		           <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" size="40" name="networkDataFields.fieldName" id="networkDataFields.fieldName"  tabindex="2" /></td> --%> 
		         </tr>
		         
		        <tr>
	                 <td align="right" class="listwhitetext" width="120">To&nbsp;Model&nbsp;Name</td>  
		            <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" size="30" name="networkDataFields.toModelName" id="networkDataFields.toModelName"  tabindex="2" /></td> 
		         	 <td align="right" class="listwhitetext" width="120">To&nbsp;Field&nbsp;Name</td>  
		            <td align="left" class="listwhitetext"><s:textfield cssClass="input-text" size="40" name="networkDataFields.toFieldName" id="networkDataFields.toFieldName" tabindex="2" /></td> 
		     
		       </tr>
		          <tr>
		          <td align="right" class="listwhitetext" style="padding-left:5px;">Transaction Type<font color="red" size="2">*</font></td>
	                <td align="left" ><s:select  name="networkDataFields.transactionType" id="networkDataFields.transactionType" list="%{trType}" headerKey="" headerValue=""  cssClass="list-menu" tabindex="3" cssStyle="width:120px"/></td> 
	                <td align="right" class="listwhitetext" style="padding-left:5px;">Type</td>
	                <td align="left" ><s:select  name="networkDataFields.type" id="networkDataFields.type" list="%{type}" headerKey="" headerValue=""  cssClass="list-menu" tabindex="3" cssStyle="width:120px"/></td> 
	            </tr>
	            <tr>
	            	<td align="right" class="listwhitetext" style="padding-left:5px;">useSysDefault</td>
	                <td><s:checkbox name="networkDataFields.useSysDefault" id="networkDataFields.useSysDefault" value="${var}" /> </td>
	            </tr>
		        <tr>
                    <td align="right" class="listwhitetext"></td>
                </tr>   
   </table>  
 </div>
 <div class="bottom-header"><span></span></div> 
 </div></div></div>
 
 <table class="detailTabLabel" cellpadding="0" cellspacing="3" border="0" style="width:800px;">
		   <tbody>
					<tr>
						<td align="right" class="listwhitetext" style="width:75px"><b><fmt:message key='customerFile.createdOn'/></b></td>
						<td valign="top"></td>
						
						<td style="width:120px">
								<fmt:formatDate var="portCreatedOnFormattedValue" value="${networkDataFields.createdOn}" pattern="${displayDateTimeEditFormat}"/>
								<s:hidden name="networkDataFields.createdOn" value="${portCreatedOnFormattedValue}"/>
								<fmt:formatDate value="${networkDataFields.createdOn}" pattern="${displayDateTimeFormat}"/>
						</td>
						
						<td align="right" class="listwhitetext" width="70"><b><fmt:message key='customerFile.createdBy' /></b></td>
						
						<c:if test="${not empty networkDataFields.id}">
							<s:hidden name="networkDataFields.createdBy" />
							<td ><s:label name="createdBy" value="%{networkDataFields.createdBy}" /></td>
						</c:if>
						<c:if test="${empty networkDataFields.id}">
							<s:hidden name="networkDataFields.createdBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="createdBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
						<fmt:formatDate var="portUpdatedOnFormattedValue" value="${networkDataFields.updatedOn}" pattern="${displayDateTimeEditFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedOn'/></b></td>
						<s:hidden name="networkDataFields.updatedOn"  value="${portUpdatedOnFormattedValue}"/>
						<td style="width:130px"><fmt:formatDate value="${networkDataFields.updatedOn}" pattern="${displayDateTimeFormat}"/>
						<td style="width:70px" align="right" class="listwhitetext"><b><fmt:message key='customerFile.updatedBy' /></b></td>
						<c:if test="${not empty networkDataFields.id}">
							<s:hidden name="networkDataFields.updatedBy" />
							<td ><s:label name="updatedBy" value="%{networkDataFields.updatedBy}" /></td>
						</c:if>
						<c:if test="${empty networkDataFields.id}">
							<s:hidden name="networkDataFields.updatedBy" value="${pageContext.request.remoteUser}" />
							<td ><s:label name="updatedBy" 	value="${pageContext.request.remoteUser}" /></td>
						</c:if>
						
					</tr>
				</tbody>
			</table>
<div id="mydiv">				 
  <s:submit cssClass="cssbutton" cssStyle="width:55px; height:25px"  method="save"  key="button.save" />   
  <s:reset cssClass="cssbutton" cssStyle="width:55px; height:25px" key="Reset"  tabindex="6"/>
 </div> 							
 </s:form>  