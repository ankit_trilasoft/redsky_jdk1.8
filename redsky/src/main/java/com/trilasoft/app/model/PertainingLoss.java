package com.trilasoft.app.model;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.appfuse.model.BaseObject;   


import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.ToStringBuilder; 
@Entity
@Table(name="pertainingloss")

public class PertainingLoss extends BaseObject
{
	  private String corpID;
	  private Long id;
	  
	  private String damageByAction;
	  private String lossAction;
	  private String itmClmsItemSeqNbr;
	  private String agentCode;
	  private String itmDnlRsnCode;
	  private String itrRespExcpCode;
	  private BigDecimal itrAssessdLiabilityAmt;
	  private BigDecimal itrTotalLiabilityAmt;
	  private BigDecimal chargeBackAmount;
	  private BigInteger itrPmntId;
	  
	  private String sequenceNumber;
	  private String ship;
	  private String shipNumber;
	  private Long claimNumber;
	  private String lossNumber;
	  
	  private String createdBy;
	  private Date createdOn;
	  private String updatedBy;
	  private Date updatedOn;
	  private String partnerCode;
	  private String partnerName;
	@Override
	public String toString() {
		return new ToStringBuilder(this).append("corpID", corpID)
				.append("id", id).append("damageByAction", damageByAction)
				.append("lossAction", lossAction)
				.append("itmClmsItemSeqNbr", itmClmsItemSeqNbr)
				.append("agentCode", agentCode)
				.append("itmDnlRsnCode", itmDnlRsnCode)
				.append("itrRespExcpCode", itrRespExcpCode)
				.append("itrAssessdLiabilityAmt", itrAssessdLiabilityAmt)
				.append("itrTotalLiabilityAmt", itrTotalLiabilityAmt)
				.append("chargeBackAmount", chargeBackAmount)
				.append("itrPmntId", itrPmntId)
				.append("sequenceNumber", sequenceNumber).append("ship", ship)
				.append("shipNumber", shipNumber)
				.append("claimNumber", claimNumber)
				.append("lossNumber", lossNumber)
				.append("createdBy", createdBy).append("createdOn", createdOn)
				.append("updatedBy", updatedBy).append("updatedOn", updatedOn)
				.append("partnerCode", partnerCode)
				.append("partnerName", partnerName).toString();
	}
	@Override
	public boolean equals(final Object other) {
		if (!(other instanceof PertainingLoss))
			return false;
		PertainingLoss castOther = (PertainingLoss) other;
		return new EqualsBuilder()
				.append(corpID, castOther.corpID)
				.append(id, castOther.id)
				.append(damageByAction, castOther.damageByAction)
				.append(lossAction, castOther.lossAction)
				.append(itmClmsItemSeqNbr, castOther.itmClmsItemSeqNbr)
				.append(agentCode, castOther.agentCode)
				.append(itmDnlRsnCode, castOther.itmDnlRsnCode)
				.append(itrRespExcpCode, castOther.itrRespExcpCode)
				.append(itrAssessdLiabilityAmt,
						castOther.itrAssessdLiabilityAmt)
				.append(itrTotalLiabilityAmt, castOther.itrTotalLiabilityAmt)
				.append(chargeBackAmount, castOther.chargeBackAmount)
				.append(itrPmntId, castOther.itrPmntId)
				.append(sequenceNumber, castOther.sequenceNumber)
				.append(ship, castOther.ship)
				.append(shipNumber, castOther.shipNumber)
				.append(claimNumber, castOther.claimNumber)
				.append(lossNumber, castOther.lossNumber)
				.append(createdBy, castOther.createdBy)
				.append(createdOn, castOther.createdOn)
				.append(updatedBy, castOther.updatedBy)
				.append(updatedOn, castOther.updatedOn)
				.append(partnerCode, castOther.partnerCode)
				.append(partnerName, castOther.partnerName).isEquals();
	}
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(corpID).append(id)
				.append(damageByAction).append(lossAction)
				.append(itmClmsItemSeqNbr).append(agentCode)
				.append(itmDnlRsnCode).append(itrRespExcpCode)
				.append(itrAssessdLiabilityAmt).append(itrTotalLiabilityAmt)
				.append(chargeBackAmount).append(itrPmntId)
				.append(sequenceNumber).append(ship).append(shipNumber)
				.append(claimNumber).append(lossNumber).append(createdBy)
				.append(createdOn).append(updatedBy).append(updatedOn)
				.append(partnerCode).append(partnerName).toHashCode();
	}
	@Column
	public String getCorpID() {
		return corpID;
	}
	public void setCorpID(String corpID) {
		this.corpID = corpID;
	}
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Column
	public String getDamageByAction() {
		return damageByAction;
	}
	public void setDamageByAction(String damageByAction) {
		this.damageByAction = damageByAction;
	}
	@Column
	public String getLossAction() {
		return lossAction;
	}
	public void setLossAction(String lossAction) {
		this.lossAction = lossAction;
	}
	@Column
	public String getItmClmsItemSeqNbr() {
		return itmClmsItemSeqNbr;
	}
	public void setItmClmsItemSeqNbr(String itmClmsItemSeqNbr) {
		this.itmClmsItemSeqNbr = itmClmsItemSeqNbr;
	}
	@Column
	public String getAgentCode() {
		return agentCode;
	}
	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	@Column
	public String getItmDnlRsnCode() {
		return itmDnlRsnCode;
	}
	public void setItmDnlRsnCode(String itmDnlRsnCode) {
		this.itmDnlRsnCode = itmDnlRsnCode;
	}
	@Column
	public String getItrRespExcpCode() {
		return itrRespExcpCode;
	}
	public void setItrRespExcpCode(String itrRespExcpCode) {
		this.itrRespExcpCode = itrRespExcpCode;
	}
	@Column
	public BigDecimal getItrAssessdLiabilityAmt() {
		return itrAssessdLiabilityAmt;
	}
	public void setItrAssessdLiabilityAmt(BigDecimal itrAssessdLiabilityAmt) {
		this.itrAssessdLiabilityAmt = itrAssessdLiabilityAmt;
	}
	@Column
	public BigDecimal getItrTotalLiabilityAmt() {
		return itrTotalLiabilityAmt;
	}
	public void setItrTotalLiabilityAmt(BigDecimal itrTotalLiabilityAmt) {
		this.itrTotalLiabilityAmt = itrTotalLiabilityAmt;
	}
	@Column
	public BigDecimal getChargeBackAmount() {
		return chargeBackAmount;
	}
	public void setChargeBackAmount(BigDecimal chargeBackAmount) {
		this.chargeBackAmount = chargeBackAmount;
	}
	@Column
	public BigInteger getItrPmntId() {
		return itrPmntId;
	}
	public void setItrPmntId(BigInteger itrPmntId) {
		this.itrPmntId = itrPmntId;
	}
	@Column
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	@Column
	public String getShip() {
		return ship;
	}
	public void setShip(String ship) {
		this.ship = ship;
	}
	@Column
	public String getShipNumber() {
		return shipNumber;
	}
	public void setShipNumber(String shipNumber) {
		this.shipNumber = shipNumber;
	}
	@Column
	public Long getClaimNumber() {
		return claimNumber;
	}
	public void setClaimNumber(Long claimNumber) {
		this.claimNumber = claimNumber;
	}
	@Column
	public String getLossNumber() {
		return lossNumber;
	}
	public void setLossNumber(String lossNumber) {
		this.lossNumber = lossNumber;
	}
	@Column
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	@Column
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	@Column
	public Date getUpdatedOn() {
		return updatedOn;
	}
	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	@Column
	public String getPartnerCode() {
		return partnerCode;
	}
	public void setPartnerCode(String partnerCode) {
		this.partnerCode = partnerCode;
	}
	@Column
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	  
	
}
