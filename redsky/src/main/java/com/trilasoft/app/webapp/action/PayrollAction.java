//---Created By Bibhash----

package com.trilasoft.app.webapp.action;
import static java.lang.System.out;

import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.DTOFile;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.DTOPaychexExtract;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.DTOPayrollExtract;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.DTOPayrollWorkDayExtract;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.GenralDTO;
import com.trilasoft.app.dao.hibernate.PayrollDaoHibernate.HWDTO;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.appfuse.model.User;
import org.appfuse.webapp.action.BaseAction;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import com.trilasoft.app.model.Payroll;
import com.trilasoft.app.model.SystemDefault;
import com.trilasoft.app.service.NotesManager;
import com.trilasoft.app.service.PayrollManager;
import com.trilasoft.app.service.RefMasterManager;

public class PayrollAction extends BaseAction{

		private Long id;
		private Long maxId;
		private Date beginDate;
		private Date endDate;
		private String sessionCorpID;
		private PayrollManager payrollManager; 
		private Payroll payroll; 
		private Set PayrollSet;
		private List searchList;
		private List refMasters;
		private String jobTypes;
		private String conditionA1;
		private Date openfromdate;
		private Date opentodate;
		private Date closeddatefrom;
		private Date closeddateto;
		private Date settlementdatefrom;
		private Date settlementdateto;
		
		private String warehouseS;
		private String typeS;
		
		private RefMasterManager refMasterManager;
		private Map<String, String> house;
		private Map<String, String> dept;
		private Map<String, String> typeofWork;
		private Map<String, String> labor;
		private Map<String, String> drvr_cls;
		public  Map<String, String> jobtype= new HashMap<String, String>();
		private List<Payroll> payrolllist;
		private List payrollGroup;
		private Map<String, String> fileNameMap;
		private Map<String, String> compDevision;
		private Map<String, String> loctype;
		private String chekcflag="";
		
		private String countPayNotes;
		private NotesManager notesManager;
		private Map<String, String> ophub;
		private Map<String, String> stotype;
		private Map<String, String> integrationToolMap ;
		private Map<String, String> mmDevice ;
		
		
		static final Logger logger = Logger.getLogger(PayrollAction.class);
		 Date currentdate = new Date();
		
		public List<Payroll> getPayrolllist() {
			return payrolllist;
		}  
		public void setPayrolllist(List<Payroll> payrolllist) {
			this.payrolllist = payrolllist;
		} 
		public List getRefMasters() {
			return refMasters;
		} 
		private List payrollGroupFlex5;
		private List supervisorList;
		private boolean salesPortalAccess=false;
		public String getComboList(String corpId){
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();
			if(user.getSalesPortalAccess()){
				salesPortalAccess=true;
				jobtype.put("INT", "International");
				jobtype.put("STO", "Storage");
			}else{
			jobtype = refMasterManager.findByParameter(corpId, "JOB");	
			}			
			house= refMasterManager.findByParameter(corpId, "HOUSE");
			dept=refMasterManager.findByParameter(corpId, "DEPT");
			typeofWork=refMasterManager.findByParameter(corpId, "CREWTYPE");
			labor=refMasterManager.findByParameter(corpId, "LABOR");
			drvr_cls=refMasterManager.findByParameter(corpId, "DRVRCLS");
			compDevision=payrollManager.getOpenCompnayDevisionCode(corpId);
			loctype = refMasterManager.findByParameter(corpId, "LOCTYPE");
			stotype = refMasterManager.findByParameter(corpId, "STORAGE_TYPE");
			ophub = refMasterManager.findByParameter(corpId, "OPSHUB");
			payrollGroup=refMasterManager.findByParameterFlex3(corpId, "HOUSE"); 
			payrollGroupFlex5=refMasterManager.findByParameterFlex5(corpId, "HOUSE"); 
			integrationToolMap=refMasterManager.findByParameter(corpId, "INTEGRATION-TOOL");
			mmDevice = refMasterManager.getFlex1MapByParameter(corpId, "MM-DEVICE");
			supervisorList = payrollManager.getAllCrewName(sessionCorpID);
			return SUCCESS;
		 }

		public void setRefMasters(List refMasters) {
			this.refMasters = refMasters;
		}

		public void setRefMasterManager(RefMasterManager refMasterManager) {
			this.refMasterManager = refMasterManager;
		}

		public String claimExtract(){
			getComboList(sessionCorpID);		
			return SUCCESS; 
	    }
		private String lossactionCondt;
		public String getLossactionCondt() {
			return lossactionCondt;
		}
		public void setLossactionCondt(String lossactionCondt) {
			this.lossactionCondt = lossactionCondt;
		}
		private String salesPortalFlag="";
		private String claimPropertyFlag;
		private String recievedFormCheckFlag;
		// claim extract start here...................
		public void claimExtractForm(){
			StringBuffer query = new StringBuffer();			
			boolean isAnd = false;			
			
			StringBuffer jobBuffer = new StringBuffer();
			if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
			} else {
				if (jobTypes.indexOf(",") == 0) {
					jobTypes = jobTypes.substring(1);
				}
				if (jobTypes.lastIndexOf(",") == jobTypes.length() - 1) {
					jobTypes = jobTypes.substring(0, jobTypes.length() - 1);
				}
				String[] arrayJobType = jobTypes.split(",");
				int arrayLength = arrayJobType.length;
				for (int i = 0; i < arrayLength; i++) {
					jobBuffer.append("'");
					jobBuffer.append(arrayJobType[i].trim());
					jobBuffer.append("'");
					jobBuffer.append(",");
				}
			}
			String jobtypeMultiple = new String(jobBuffer);
			if (!jobtypeMultiple.equals("")) {
				jobtypeMultiple = jobtypeMultiple.substring(0, jobtypeMultiple.length() - 1);
			} else if (jobtypeMultiple.equals("")) {
				jobtypeMultiple = new String("''");
			}
			
			
			String openFrom = "";				
			if (openfromdate!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				openFrom = new String(dateformatYYYYMMDD.format(openfromdate));

			}
			String closeFrom = "";
			if (closeddatefrom != null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				closeFrom = new String(dateformatYYYYMMDD.format(closeddatefrom));

			}
			
			String openTo = "";	
			if (opentodate!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				openTo = new String(dateformatYYYYMMDD.format(opentodate));

			}
			
			String settlementFrom = "";
			if (settlementdatefrom != null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				settlementFrom = new String(dateformatYYYYMMDD.format(settlementdatefrom));
			}
			
			String settlementTo = "";	
			if (settlementdateto!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				settlementTo = new String(dateformatYYYYMMDD.format(settlementdateto));
			}
			
			String closeTo = "";
			if (closeddateto!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				closeTo = new String(dateformatYYYYMMDD.format(closeddateto));
			}
			
				
			if((lossactionCondt!=null)&&(lossactionCondt.equalsIgnoreCase("YES"))){
				if (conditionA1.equalsIgnoreCase("Equal to")) {
					query.append(" s.job in ");
					query.append("(" + jobtypeMultiple + ") and ");
				}
				if (conditionA1.equalsIgnoreCase("Not Equal to")) 
				{
					query.append(" s.job not in ");
					query.append("(" + jobtypeMultiple + ") and ");
				}
				
			}else{
				if (conditionA1.equalsIgnoreCase("Equal to")) {
					if(recievedFormCheckFlag==null || recievedFormCheckFlag.equalsIgnoreCase("")){
						query.append(" l.lossaction not in ('C','W') and s.job in ");
					}else{
						query.append(" s.job in ");
					}
					query.append("(" + jobtypeMultiple + ") and ");
				}
				if (conditionA1.equalsIgnoreCase("Not Equal to")){
					if(recievedFormCheckFlag==null || recievedFormCheckFlag.equalsIgnoreCase("")){
						query.append(" l.lossaction not in ('C','W') and s.job not in ");
					}else{
						query.append(" s.job not in ");
					}
					query.append("(" + jobtypeMultiple + ") and ");
				}				
			}
				
				if(!openFrom.equals("")&&!openTo.equals("")){
					if(recievedFormCheckFlag==null || recievedFormCheckFlag.equalsIgnoreCase("")){
						query.append(" DATE_FORMAT(c.createdon,'%Y-%m-%d') between" +  "'" + openFrom + "'");
					}else{
						query.append(" DATE_FORMAT(c.formRecived,'%Y-%m-%d') between" +  "'" + openFrom + "'");
					}
					query.append("and " +  "'" + openTo + "'" +" and ");				
				}
				
				if(!closeFrom.equals("")&&!closeTo.equals(""))
				{
					query.append(" DATE_FORMAT(c.closedate,'%Y-%m-%d') between " + "'" + closeFrom + "'");
					query.append("and " +  "'" + closeTo + "'" +" and ");
				}
				
				if(!settlementFrom.equals("")&&!settlementTo.equals(""))
				{
					query.append(" DATE_FORMAT(c.settlementForm,'%Y-%m-%d') between" +  "'" + settlementFrom + "'");
					query.append("and " +  "'" + settlementTo + "'" +" and ");				
				}

				if (jobTypes == null || jobTypes.equals("") || jobTypes.equals(",")) {
					query.append("  c.corpid =  '" + sessionCorpID +"'" );
				}
				else {
					query.append(" c.corpid =  '" + sessionCorpID +"'" );	
				}
				
				if(salesPortalFlag!=null && !salesPortalFlag.equals("") && salesPortalFlag.equalsIgnoreCase("Yes")){
					Authentication auth = SecurityContextHolder.getContext().getAuthentication();
					User user = (User) auth.getPrincipal();
					if(user.getSalesPortalAccess()){
						query.append(" and " + " s.bookingAgentCode =  '" + user.getParentAgent() +"'" );	
					}else{
						query.append(" and " + " s.salesMan =  '" + user.getUsername()+"'" );
					}
				}
				
			List imfEstimate = payrollManager.findsClaimsExtracts(query.toString(), sessionCorpID,recievedFormCheckFlag);
			String header=new String();
			try {
				File file1 = new File("claim");
				String state = new String();
				String service = new String();
				
				if (!imfEstimate.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					if(claimPropertyFlag!=null && claimPropertyFlag.equalsIgnoreCase("Yes")){
						header = " JobNumber" + "\t" +" ClaimNumber" + "\t" +"First Name "+ "\t" +"Last Name" + "\t" +" Job " + "\t" + " BillToCode" + "\t" + "BillToName" + "\t" + "Opened" + "\t" + " Closed" + "\t" + " Cancelled" + "\t" + "Currency" + "\t" + " RequestedAmt" + "\t" + " ReceievedAmt"
								+ "\t" + " PaidShipper " + "\t" + " Paid3rdParty " + "\t" + " Warehouse  "+ "\t" + " LossAction " + "\t" + " LossType "+ "\t" + " DamagedByAction"+ "\t" +" ChargeBackTo"+ "\t"+ " ChargeBackAmount"+ "\t"+ " ItemDescription"+"\t"+ " LossComment "+"\t"+ " TotalPaid "+"\t"+ " Reimbursement "+"\t"+ " Received Reimbursement "+"\t"+ " UVL Amount "+"\t"+ " Reimbursement Requested "+"\t"+ " Reimbursement Received "+"\t"+ " Settlement "
								+"\t" + "Routing" + "\t" + "Company Division" + "\t" + "Account Name" + "\t" + "Millitary Shipment" + "\t" + "Booking agent code" + "\t" + "Booking agent" + "\t" + "Origin Agent Code" + "\t" + "Origin Agent" + "\t" + "Destination Agent code" + "\t" + "Destination Agent" + "\t" + "Sub Origin Agent code" + "\t" + "Sub Origin Agent" 
								+ "\t" + "Sub Destination Agent Code" + "\t" + "Sub Destination Agent"+"\t"+ "Self Blame" + "\t" + "Claim Handler" +"\t" + "Request Form"+"\t"+ "Sent Form" + "\t" + "Received Form" + "\t" + "Property Damage Amt" + "\t" + "Customer SVC Amt" + "\t" + "Lost/Damaged Items Amt" + "\t" + "Total Paid to the Customer" + "\t" + "Insurer Code" + "\t" + "Insurer Name" + "\n";
					}else{
						header = " JobNumber" + "\t" +" ClaimNumber" + "\t" +"First Name "+ "\t" +"Last Name" + "\t" +" Job " + "\t" + " BillToCode" + "\t" + "BillToName" + "\t" + "Opened" + "\t" + " Closed" + "\t" + " Cancelled" + "\t" + "Currency" + "\t" + " RequestedAmt" + "\t" + " ReceievedAmt"
						 + "\t" + " PaidShipper " + "\t" + " Paid3rdParty " + "\t" + " Warehouse  "+ "\t" + " LossAction " + "\t" + " LossType "+ "\t" + " DamagedByAction"+ "\t" +" ChargeBackTo"+ "\t"+ " ChargeBackAmount"+ "\t"+ " ItemDescription"+"\t"+ " LossComment "+"\t"+ " TotalPaid "+"\t"+ " Reimbursement "+"\t"+ " Received Reimbursement "+"\t"+ " UVL Amount "+"\t"+ " Reimbursement Requested "+"\t"+ " Reimbursement Received "+"\t"+ " Settlement "
						+"\t" + "Routing" + "\t" + "Company Division" + "\t" + "Account Name" + "\t" + "Millitary Shipment" + "\t" + "Booking agent code" + "\t" + "Booking agent" + "\t" + "Origin Agent Code" + "\t" + "Origin Agent" + "\t" + "Destination Agent code" + "\t" + "Destination Agent" + "\t" + "Sub Origin Agent code" + "\t" + "Sub Origin Agent" + "\t" + "Sub Destination Agent Code" + "\t" + "Sub Destination Agent"+"\t"+ "Self Blame" + "\t" + "Claim Handler" +"\t" + "Request Form"+"\t"+ "Sent Form" + "\t" + "Received Form" + "\t" + "Insurer Code" + "\t" + "Insurer Name" + "\n";
					}
					
					outputStream.write(header.getBytes());

					Iterator it = imfEstimate.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						
						if (row[0] != null) {
							String str="'"+row[0].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[1] != null) {
							outputStream.write((row[1].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[2] != null) {
							outputStream.write((row[2].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[3] != null) {
							String str="'"+row[3].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}


						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					
						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						
						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						
						if (row[14] != null) {
							outputStream.write((row[14].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						
						if (row[15] != null) {
							outputStream.write((row[15].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[18] != null) {
							outputStream.write((row[18].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[19] != null) {
							outputStream.write((row[19].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[20] != null) {
							outputStream.write((row[20].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[21] != null) {
							outputStream.write((row[21].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[22] != null) {
							outputStream.write((row[22].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[23] != null) {
							outputStream.write((row[23].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[24] != null) {
							outputStream.write((row[24].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[25] != null) {
							outputStream.write((row[25].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[26] != null) {
							outputStream.write((row[26].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[27] != null) {
							outputStream.write((row[27].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[28] != null) {
							outputStream.write((row[28].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[29] != null) {
							outputStream.write((row[29].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[30] != null) {
							outputStream.write((row[30].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[31] != null) {
							outputStream.write((row[31].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[32] != null) {
							outputStream.write((row[32].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[33] != null) {
							outputStream.write((row[33].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}if (row[34] != null && !row[34].toString().trim().equals("")) {
						    
						        String str="'"+row[34].toString()+"'";
							outputStream.write((str + "\t").getBytes());
						    	//outputStream.write((row[33].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[35] != null) {
							outputStream.write((row[35].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[36] != null) {
						    String str="'"+row[36].toString()+"'";
							outputStream.write((str + "\t").getBytes());
							//outputStream.write((row[35].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}if (row[37] != null) {
							outputStream.write((row[37].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[38] != null) {
						    String str="'"+row[38].toString()+"'";
							outputStream.write((str + "\t").getBytes());
							//outputStream.write((row[37].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[39] != null) {
							outputStream.write((row[39].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[40] != null && !row[40].toString().trim().equals("")) {
							String str="'"+row[40].toString()+"'";
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[41] != null) {
							outputStream.write((row[41].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[42] != null && !row[42].toString().trim().equals("")) {
							String str="'"+row[42].toString()+"'";
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[43] != null) {
							outputStream.write((row[43].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[44] != null) {
							outputStream.write((row[44].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[45] != null) {
							outputStream.write((row[45].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());	
						}if (row[46] != null) {
							outputStream.write((row[46].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[47] != null) {
							outputStream.write((row[47].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}if (row[48] != null) {
							outputStream.write((row[48].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if(claimPropertyFlag!=null && claimPropertyFlag.equalsIgnoreCase("Yes")){
							if (row[49] != null) {
								outputStream.write((row[49].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());							
							}
							if (row[50] != null) {
								outputStream.write((row[50].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());							
							}
							if (row[51] != null) {
								outputStream.write((row[51].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());							
							}
							if (row[52] != null) {
								outputStream.write((row[52].toString() + "\t").getBytes());
							} else {
								outputStream.write("\t".getBytes());							
							}
					       }
						if (row[53] != null) {
							outputStream.write((row[53].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						if (row[54] != null) {
							outputStream.write((row[54].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());							
						}
						outputStream.write("\n".getBytes());
					}

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = " JobNumber" + "\t" +" ClaimNumber" + "\t" +"First Name "+ "\t" +"Last Name" + "\t" +" Job " + "\t" + " BillToCode" + "\t" + "BillToName" + "\t" + "Opened" + "\t" + " Closed" + "\t" + " Cancelled" + "\t" + "Currency" + "\t" + " RequestedAmt" + "\t" + " ReceievedAmt"
					+ "\t" + " PaidShipper " + "\t" + " Paid3rdParty " + "\t" + " Warehouse  "+ "\t" + " LossAction " + "\t" + " LossType "+ "\t" + " DamagedByAction"+ "\t" +" ChargeBackTo"+ "\t"+ " ChargeBackAmount"+ "\t"+ " ItemDescription"+"\t"+ " LossComment "+"\t"+ " TotalPaid "+"\t"+ " Reimbursement "+"\t"+ " Received Reimbursement "+"\t"+ " UVL Amount "+"\t"+ " Reimbursement Requested "+"\t"+ " Reimbursement Received "+"\t"+ " Settlement "
					+"\t" + "Routing" + "\t" + "Company Division" + "\t" + "Account Name" + "\t" + "Millitary Shipment" + "\t" + "Booking agent code" + "\t" + "Booking agent" + "\t" + "Origin Agent Code" + "\t" + "Origin Agent" + "\t" + "Destination Agent code" + "\t" + "Destination Agent" + "\t" + "Sub Origin Agent code" + "\t" + "Sub Origin Agent" + "\t" + "Sub Destination Agent Code" + "\t" + "Sub Destination Agent"+"\t"+ "Self Blame"+ "\t" + "Claim Handler" + "\t" + "Request Form"+"\t"+ "Sent Form" + "\t" + "Received Form" + "\t" + "Insurer Code" + "\t" + "Insurer Name" + "\n";
					
					outputStream.write(header.getBytes());

				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
			
		}
		
		
		// complaint extract start here...................
		
		public String complaintsExtract(){
			return SUCCESS; 
	    }
		
		public void complaintExtractForm(){
			
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" Start");
			StringBuffer query = new StringBuffer();			
			boolean isAnd = false;			
			String openFrom = "";				
			if (openfromdate!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				openFrom = new String(dateformatYYYYMMDD.format(openfromdate));
			}
						
			String openTo = "";	
			if (opentodate!= null) {
				SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("yyyy-MM-dd");
				openTo = new String(dateformatYYYYMMDD.format(opentodate));
			}
				if(!openFrom.equals("")&&!openTo.equals(""))
				{
					query.append(" and DATE_FORMAT(n.createdon,'%Y-%m-%d') between" +  "'" + openFrom + "'");
					query.append("and " +  "'" + openTo + "'");				
				}
											
				query.append(" and " + " n.corpid =  '" + sessionCorpID +"' and n.noteType='Issue Resolution' and n.notesid=s.shipnumber" );	
			
			List imfEstimate = payrollManager.findsComplaintsExtracts(query.toString(), sessionCorpID);
			String header=new String();
			try {
				File file1 = new File("ComplaintsExtract");
				String state = new String();
				String service = new String();
				
				if (!imfEstimate.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = " Name Client" + "\t" +" UTS Job" + "\t" +"From -> To"+ "\t" +"Involved Member" + "\t" +" Date Job Delivered " 
					+ "\t" + " Date Received Complaint" + "\t" + "Grading" + "\t" + "Type of Complaints" + "\t" + " Remarks" + "\t" 
					+ " Solved?" + "\t" + "Follow Up UTS" + "\t" + " Improvement Measure UTS" + "\n";
					outputStream.write(header.getBytes());

					Iterator it = imfEstimate.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();
						
						if (row[0] != null) {
							String str="'"+row[0].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[1] != null) {
							outputStream.write((row[1].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[2] != null) {
							outputStream.write((row[2].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[3] != null) {
							String str="'"+row[3].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[4] != null) {
							outputStream.write((row[4].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[5] != null) {
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					
						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[8] != null) {
							outputStream.write((row[8].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[10] != null) {
							outputStream.write((row[10].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						outputStream.write("\n".getBytes());
					}

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = " Name Client" + "\t" +" UTS Job" + "\t" +"From -> To"+ "\t" +"Involved Member" + "\t" +" Date Job Delivered " 
					+ "\t" + " Date Received Complaint" + "\t" + "Grading" + "\t" + "Type of Complaints" + "\t" + " Remarks" + "\t" 
					+ " Solved?" + "\t" + "Follow Up UTS" + "\t" + " Improvement Measure UTS" + "\n";
					
					outputStream.write(header.getBytes());
				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
			logger.warn("USER AUDIT: ID: "+currentdate+" ## User: "+getRequest().getRemoteUser()+" End");
		}
		
		public String payrollList(){
			getComboList(sessionCorpID);
			payrolllist=payrollManager.getActiveList();
			PayrollSet=new HashSet(payrolllist);
			chekcflag ="1";
			return SUCCESS; 
	    }
		
		public PayrollAction(){
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        User user = (User)auth.getPrincipal();
	        this.sessionCorpID = user.getCorpID();
		}
		
		public String edit(){ 
			if (id != null)
			{
				payroll=payrollManager.get(id);
				getComboList(sessionCorpID);
				try{
					if(payroll !=null && payroll.getCompanyDivision()!=null && (!(payroll.getCompanyDivision().toString().trim().equals("")))){
						
						if(compDevision.containsKey(payroll.getCompanyDivision())){
							
						}else{
							compDevision=payrollManager.getOpenCompnayDevisionCode(sessionCorpID);
						}
						}
					}
				catch (Exception exception) {
					exception.printStackTrace();
				}
			}
			else
			{
				payroll=new Payroll();
				getComboList(sessionCorpID);
				payroll.setCreatedOn(new Date());
				payroll.setValueDate(new Date());		
				payroll.setUpdatedOn(new Date());
			}
			payroll.setCorpID(sessionCorpID); 
			getNotesForIconChange();
			return SUCCESS;
			
	 } 
		
		@SkipValidation
		public String save() throws Exception {
			boolean isNew = (payroll.getId() == null);  
			getNotesForIconChange();
			List isexisted=payrollManager.checkUserName(payroll.getUserName());
			if(!isexisted.isEmpty() && isNew){
				errorMessage("User Name already exist");
				getComboList(sessionCorpID); 
				return SUCCESS; 
			}
		  else{ 
			if(isNew)
		    {   
				if(payroll.getWarehouse()==null){
					payroll.setWarehouse("");
				}
		    }
		      else
		        {
		        	 if(payroll.getWarehouse()==null){
						payroll.setWarehouse("");
					}
		        } 
			
			getComboList(sessionCorpID);
			payroll.setCorpID(sessionCorpID);
			payroll.setUpdatedBy(getRequest().getRemoteUser());
			if(isNew){
				payroll.setCreatedOn(new Date());
	        }
	       
			payroll.setUpdatedOn(new Date());
			payroll= payrollManager.save(payroll);
			if(gotoPageString.equalsIgnoreCase("") || gotoPageString==null){
			String key = (isNew) ? "Payroll has been added successfully" : "Payroll has been updated successfully";
			saveMessage(getText(key));
			}
			if (!isNew) {   
	        	return INPUT;   
	        } else {   
	        	maxId = Long.parseLong(payrollManager.findMaximumId().get(0).toString());
	        	payroll = payrollManager.get(maxId);
	          return SUCCESS;
	        } 
		 }		
	} 
		
		@SkipValidation
		public String getNotesForIconChange() {
			List noteCrew =  notesManager.countForCrewNotes(payroll.getId());
			
			if (noteCrew.isEmpty()) {
				countPayNotes = "0";
			} else {
				countPayNotes = ((noteCrew).get(0)).toString();
			}
			return SUCCESS;
		}
		
		
		@SkipValidation
		public String sickPersonal(){
			return SUCCESS;
		}
		
		private String fromDate;
		private String toDate;
		private String userName;
		@SkipValidation
		public String updateSickPersonal(){
			int vactionUpdatedSize=payrollManager.updateVacation(fromDate,toDate, sessionCorpID);
			int updatedSize  = payrollManager.updateSickPersonal(fromDate,toDate, sessionCorpID);
			//List count=payrollManager.getCountSickPersonalUpdate(fromDate,toDate, sessionCorpID);
			String key = updatedSize + " crew records for Sick/Personal days has been updated successfully";
			String key1 = vactionUpdatedSize + " crew records for Vaction hours has been updated successfully";
			saveMessage(getText(key));
			saveMessage(getText(key1));
			return SUCCESS;
		}
		private List timeHours;
		private String hub;
		private String locWarehouse;
		private String typeSto;
		@SkipValidation
		public String getSickHours(){
			
			timeHours=payrollManager.getSickHours(userName, sessionCorpID);
			
			return SUCCESS;
		}
		
		public String getPersonalHours(){
			
			timeHours=payrollManager.getPersonalHours(userName, sessionCorpID);
			return SUCCESS;
		}
		
		public String getVactionHours(){
			
			timeHours=payrollManager.getVacationHours(userName, sessionCorpID);
			return SUCCESS;
		}
		
		@SkipValidation
		public String findStorageExtract(){     
			getComboList(sessionCorpID);
			return SUCCESS;
		}
		@SkipValidation
		public void storageExtracts(){   
			StringBuffer fileName = new StringBuffer();
			String header = new String(); 
			List imfEstimate = payrollManager.findsStorageExtracts(warehouseS, typeS, sessionCorpID); 
			try {

				File file1 = new File("storagelocation");
				if (!imfEstimate.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("text/csv");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = " Locationid" + "\t" +" Row # " + "\t" +" Box # " + "\t" + " location" + "\t" + "Occupied" + "\t" + "Type" + "\t" + " ShipNumber" + "\t" + " Description" + "\t" + " ItemTag" + "\t" + " ItemNumber"
								+ "\t" + " ContainerId "+ "\t" + " StorageId " + "\t" + " First Name " + "\t" +"Last Name" + "\t" + " BillToCode " + "\t" + " Job Type " + "\t" + " STG_CREATED "+ "\t" + " Insurance "+ "\t" + " Storage_Out"+ "\n";
					outputStream.write(header.getBytes());

					Iterator it = imfEstimate.iterator();
					while (it.hasNext()) {
						Object[] row = (Object[]) it.next();

						if (row[0] != null) {
							String str="'"+row[0].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						// according ticket#2147
						if (row[14] != null) {
							String str="'"+row[14].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[15] != null) {
							String str="'"+row[15].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						//END

						if (row[1] != null) {
							outputStream.write((row[1].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[2] != null) {
							outputStream.write((row[2].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[3] != null) {
							outputStream.write((row[3].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					
						if (row[4] != null) {
							outputStream.write(("'"+(row[4].toString() + "\t")).getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}	
							
						if (row[5] != null) {
							
							String originAddressLine1 = row[5].toString();					
							originAddressLine1 = originAddressLine1.replaceAll("[^a-zA-Z0-9'-_&,;. `()]", " ");				  
						    row[5]=(String)originAddressLine1;
							outputStream.write((row[5].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}	
							
						if (row[6] != null) {
							outputStream.write((row[6].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[7] != null) {
							outputStream.write((row[7].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}	
						if (row[8] != null) {							
							String str=row[8].toString();
							str="'"+str;
							//System.out.println("\n row[8] \t"+str);
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
							
						if (row[9] != null) {
							outputStream.write((row[9].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}	

							
						if (row[10] != null) {
							String str="'"+row[10].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						if (row[11] != null) {
							outputStream.write((row[11].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if (row[12] != null) {
							outputStream.write((row[12].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[18] != null) {
							String str="'"+row[18].toString();
							outputStream.write((str + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[13] != null) {
							outputStream.write((row[13].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[16] != null) {
							outputStream.write((row[16].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						if (row[17] != null) {
							outputStream.write((row[17].toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						outputStream.write("\n".getBytes());
					}

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					//header=" SAP"+"\t"+" Invnum"+"\t"+"Staff Name"+"\t"+" Ship Type"+"\t"+" Ship Mode"+"\t"+" UGWW"+"\t"+" Shipnum"+"\t"+" InvoiceAmount"+"\t"+" Booker "+"\n";
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());

				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}

		
			
		}
		
		
		
		@SkipValidation
		public String findStorageLibraryExtract(){     
			getComboList(sessionCorpID);
			return SUCCESS;
		}
		
		@SkipValidation
		public void storageLibExtract(){   
			StringBuffer fileName = new StringBuffer();
			String header = new String(); 
			List storageLib = payrollManager.findStorageLibraryExtract(locWarehouse, typeSto, sessionCorpID); 
			try {
					if (!storageLib.isEmpty()) {
						HttpServletResponse response = getResponse();
						ServletOutputStream outputStream = response.getOutputStream();
						File file1 = new File("StorageLibraryExtract");
						response.setContentType("application/vnd.ms-excel");
						//response.setHeader("Cache-Control", "no-cache");
						response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
						response.setHeader("Pragma", "public");
						response.setHeader("Cache-Control", "max-age=0");
						header = "Storage Id" + "\t" +" Storage Type" + "\t" +" Capacity Cft" + "\t" +" Occ Cft" + "\t" +" Avail Cft" + "\t" +" Owner" + "\t" +" Capacity Cbm" + "\t" +" Occ Cbm" + "\t" +" Avail Cbm" + "\t" +" Ticket No" + "\t" +" Volume of Ticket" + "\t" +" Description" + "\t" +" Item Tag" + "\t" +" Item Number" + "\t" +" Container Id" + "\t" +" Occupied" + "\t" +" Warehouse" + "\t" +" Location" + "\t" +"Location Type" + "\t" +" Ship Number" + "\t" +" First Name" + "\t" +" Last Name" + "\t" +" Bill To Code " + "\t" +" STG_Created" + "\n";
						outputStream.write(header.getBytes());
						Iterator it = storageLib.iterator();
						
						
					while(it.hasNext())
				       {
						Object []row= (Object [])it.next();
							
						if(row[0]!=null)				
						{
							outputStream.write((row[0].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						
						if(row[1]!=null)				
						{
							outputStream.write((row[1].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						
						if(row[2]!=null)				
						{
							outputStream.write((row[2].toString()+"\t").getBytes()) ;
						}				
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						
						if(row[3]!=null)				
						{
									outputStream.write((row[3].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						if(row[4]!=null)				
						{
							outputStream.write((row[4].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						if(row[5]!=null)				
						{
							outputStream.write((row[5].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
						}
						
						
						if(row[6]!=null)
						{							
							outputStream.write((row[6].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						
						
						if(row[7]!=null)
						{							
							outputStream.write((row[7].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						
						if(row[8]!=null)
						{							
							outputStream.write((row[8].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						
						if(row[22]!=null)
						{							
							outputStream.write((row[22].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[23]!=null)
						{							
							outputStream.write((row[23].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						
						if(row[9]!=null)
						{							
							outputStream.write((row[9].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[10]!=null)
						{							
							outputStream.write((row[10].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[11]!=null)
						{							
							outputStream.write((row[11].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[12]!=null)
						{							
							outputStream.write((row[12].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[13]!=null)
						{							
							outputStream.write((row[13].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[14]!=null)
						{							
							outputStream.write((row[14].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[15]!=null)
						{							
							outputStream.write((row[15].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[16]!=null)
						{							
							outputStream.write((row[16].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[17]!=null)
						{							
							outputStream.write((row[17].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[18]!=null)
						{							
							outputStream.write((row[18].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[19]!=null)
						{							
							outputStream.write((row[19].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[20]!=null)
						{							
							outputStream.write((row[20].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						if(row[21]!=null)
						{							
							outputStream.write((row[21].toString()+"\t").getBytes()) ;
						}
						else
						{
							outputStream.write("\t".getBytes());
										
						}
						outputStream.write("\n".getBytes()) ;
								
				       	}			

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					File file1 = new File("StorageLibraryExtract");
					response.setContentType("application/vnd.ms-excel");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());

				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}	
		}
		
		@SkipValidation
		public String findPayrollExtract()
		{  
			getComboList(sessionCorpID);
			return SUCCESS;
		}
		/**
		 * WorkDay extract 
		 * @return
		 */
		
		@SkipValidation
		public String findPayrollWorkDayExtract()
		{  
			getComboList(sessionCorpID);
			return SUCCESS;
		}
		/**
		 * End Work Day
		 * @return
		 */
		
		@SkipValidation
		public String findPaychexExtract(){
			getComboList(sessionCorpID);
			return SUCCESS;	
		}
		public void payrollInternalExtract() throws ServletException,IOException
		{ 	
			StringBuffer fileName = new StringBuffer();			
			String header = new String(); 
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder beginDates = new StringBuilder(formats.format(beginDate));
	        StringBuilder endDates = new StringBuilder(formats.format(endDate)); 
	        SimpleDateFormat batchformats = new SimpleDateFormat("yyyyMMdd");
	        StringBuilder batchDates = new StringBuilder(batchformats.format(endDate));
	        StringBuffer batchDates1 = new StringBuffer("PAY_");
	        batchDates1.append(batchDates);
	        File file=new File(batchDates1.toString()); 
			List payrollInternalList=payrollManager.findPayrollInternalList(beginDates.toString(),endDates.toString(),sessionCorpID);		
		   try {

				//File file1 = new File("storage");
				if (!payrollInternalList.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					outputStream.write("Warehouse".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("EmployeeNo".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("CrewName".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("workdate".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Action".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("TempDept".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("RegHours".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("OTHours".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Earnings3Code".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Earnings3Amount".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Hours3Code".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Hours3Amount".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("RateCode".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("PayHour".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("TypeofWork".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("ST Pay".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("OT Pay".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Total Pay".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("PaidRevenueShare".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("AdjustmentToRevenue".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Ticket #".getBytes());
					outputStream.write("\t".getBytes());
					
					outputStream.write("Service ".getBytes());
					outputStream.write("\t".getBytes());
					
					outputStream.write("Distribution Code".getBytes());
					outputStream.write("\t".getBytes());
					
					outputStream.write("Job-type".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("workticket-warehouse".getBytes());
					outputStream.write("\t".getBytes());
					outputStream.write("Earnings3hours".getBytes());
					outputStream.write("\r\n".getBytes());

					Iterator it = payrollInternalList.iterator();
					while (it.hasNext()) {
					Object[] row = (Object[]) it.next();

					
					
					if (row[0] != null) {
						outputStream.write((row[0].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
                   
					if (row[1] != null) {
						outputStream.write((row[1].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
                  
					//System.out.println("\n\n row[4]::"+row[4].toString());
					if (row[2] != null) {
						outputStream.write((row[2].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[3] != null) {
						outputStream.write((row[3].toString() + "\t").getBytes());
						/*Date date=(Date)row[3];
						SimpleDateFormat dateformatYYYYMMDD = new SimpleDateFormat("MM/dd/yy");
				        String  nowYYYYMMDD = new String( dateformatYYYYMMDD.format( date ) );				         
						outputStream.write((nowYYYYMMDD + "\t").getBytes());*/
					} else {
						outputStream.write("\t".getBytes());
					}
				
					if (row[4] != null) {
						outputStream.write((row[4].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}	
						
					if (row[5] != null) {
						outputStream.write((row[5].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}	
						
					if (row[6] != null) {
						outputStream.write((row[6].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[7] != null) {
						outputStream.write((row[7].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}	
					if (row[8] != null) {
						outputStream.write((row[8].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
						
					if (row[9] != null) {
						outputStream.write((row[9].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}	

						
					if (row[10] != null) {
						
						outputStream.write((row[10].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}

					if (row[11] != null) {
						outputStream.write((row[11].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[12] != null) {
						outputStream.write((row[12].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[13] != null) {
						outputStream.write((row[13].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[14] != null) {
						outputStream.write((row[14].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					double reghours=0.00;
					double payhours=0.00;
					double hours3Amount=0.00;
					if (row[6] != null &&(!(row[6].toString().equals("")))) {
						reghours=Double.parseDouble((row[6].toString())); 
					} else {
						reghours=0.00;
					}
					if (row[13] != null && (!(row[13].toString().equals("")))) {
						payhours=Double.parseDouble((row[13].toString())); 
					} else {
						payhours=0.00;
					}
					if (row[11] != null && (!(row[11].toString().equals("")))) {
						hours3Amount=Double.parseDouble((row[11].toString())); 
					} else {
						hours3Amount=0.00;
					}
					double stPay=(reghours*payhours)+(hours3Amount*payhours);
					double stPayRound=0.00;
					DecimalFormat twoDForm = new DecimalFormat("#.##");
					stPayRound= Double.valueOf(twoDForm.format(stPay)); 
					outputStream.write((""+stPayRound + "\t").getBytes());
					
					double othours=0.00;
					if (row[7] != null && (!(row[7].toString().equals("")))) {
						othours=Double.parseDouble((row[7].toString())); 
					} else {
						othours=0.00;
					}
					double OTPay=(othours*1.5)*payhours;
					double OTPayRound=0.00;
					OTPayRound= Double.valueOf(twoDForm.format(OTPay)); 
					outputStream.write((""+OTPayRound + "\t").getBytes());
					double Earnings3Code=0.00;
					if (row[9] != null && (!(row[9].toString().equals("")))) {
						Earnings3Code=Double.parseDouble((row[9].toString()));
					} else {
						Earnings3Code=0.00;
					}
					double totalPay=stPay+OTPay+Earnings3Code; 
					double totalPayRound=0.00;
					totalPayRound= Double.valueOf(twoDForm.format(totalPay)); 
					outputStream.write((""+totalPayRound + "\t").getBytes());
					if (row[21] != null) {
						outputStream.write((row[21].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[22] != null) {
						outputStream.write((row[22].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[15] != null) {
						outputStream.write((row[15].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					
					if (row[17] != null) {
						outputStream.write((row[17].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[18] != null) {
						outputStream.write((row[18].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[19] != null) {
						outputStream.write((row[19].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[16] != null) {
						outputStream.write((row[16].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					if (row[20] != null) {
						outputStream.write((row[20].toString() + "\t").getBytes());
					} else {
						outputStream.write("\t".getBytes());
					}
					outputStream.write("\n".getBytes());}

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					//header=" SAP"+"\t"+" Invnum"+"\t"+"Staff Name"+"\t"+" Ship Type"+"\t"+" Ship Mode"+"\t"+" UGWW"+"\t"+" Shipnum"+"\t"+" InvoiceAmount"+"\t"+" Booker "+"\n";
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());

				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
			
		}
		public void payrollExtract()  throws ServletException,IOException
		{
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder beginDates = new StringBuilder(formats.format(beginDate));
	        StringBuilder endDates = new StringBuilder(formats.format(endDate)); 
	        SimpleDateFormat batchformats = new SimpleDateFormat("yyyyMMdd");
	        StringBuilder batchDates = new StringBuilder(batchformats.format(endDate));
	        StringBuffer batchDates1 = new StringBuffer("PAYDATA_");
	        batchDates1.append(batchDates);
	        if(hub.equalsIgnoreCase("MR3")){
	        List<GenralDTO> payrollExtractList= payrollManager.findPayrollExtractList(beginDates.toString(),endDates.toString(),sessionCorpID,hub);
			Map<String, DTOFile> masterDtoFile= new HashMap<String, DTOFile>();		
			Map<String, Double> hoursCodeMap= new HashMap<String, Double>();
			Map<String, Double> hoursValueMap= new HashMap<String, Double>();
			Integer size=1;
			for (GenralDTO listObject : payrollExtractList) {
				masterDtoFile=listObject.getMasterDtoFile();
				hoursCodeMap=listObject.getHoursCodeMap();
				hoursValueMap=listObject.getHoursValueMap();
				size=listObject.getSize();				
				}
			List totalHour3Code =new ArrayList<String>();
				totalHour3Code.add("RS");
				totalHour3Code.add("DT");
				totalHour3Code.add("H");
				totalHour3Code.add("P");
				totalHour3Code.add("V");
				totalHour3Code.add("S");
				totalHour3Code.add("BE");
			
			HttpServletResponse response = getResponse();
	        response.setContentType("application/vnd.ms-excel");
			ServletOutputStream outputStream = response.getOutputStream();
			File file=new File(batchDates1.toString());
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".xls");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			out.println("filename="+file.getName()+".xls"+"\t The number of line  \t"+payrollExtractList.size()+"\t is extracted");	
			//outputStream.write("Co Code\t".getBytes());
			//outputStream.write("Batch ID\t".getBytes());
			outputStream.write("Employee #\t".getBytes());
			outputStream.write("Employee\t".getBytes());
			//outputStream.write("Special Action \t".getBytes());
			//outputStream.write("Pay #\t".getBytes());
			//outputStream.write("Tax Frequency\t".getBytes());			
			outputStream.write("Reg Hours\t".getBytes());
			outputStream.write("O/T Hours\t".getBytes());
			//outputStream.write("Hours 3 Code\t".getBytes());
			outputStream.write("EIP Hours\t".getBytes());
			outputStream.write("Reg Earnings\t".getBytes());			
			outputStream.write("O/T Earnings\t".getBytes());						
			//outputStream.write("Earnings 3 Code\t".getBytes());
			outputStream.write("EIP Amount\t".getBytes());		
			if(size>1){
				for(int k=1;k<size;k++){
				//	outputStream.write("Hours 3 Code\t".getBytes());
					outputStream.write("EIP Hours\t".getBytes());
				//	outputStream.write("Earnings 3 Code\t".getBytes());
					outputStream.write("EIP Amount\t".getBytes()); 
				}
			}			
			outputStream.write("\r\n".getBytes());
	        Float f11;
	        DecimalFormat df11;	        
			for(Map.Entry<String, DTOFile> record : masterDtoFile.entrySet()){
				DTOFile dtoTemp=record.getValue();
				/*if(((DTOFile)dtoTemp).getCoCode()!=null){
					outputStream.write((((DTOFile)dtoTemp).getCoCode().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(((DTOFile)dtoTemp).getBatchID()!=null){
					outputStream.write((((DTOFile)dtoTemp).getBatchID().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}*/
				if(record.getKey()!=null){
					outputStream.write((record.getKey().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				
				
				if(((DTOFile)dtoTemp).getEmployee()!=null){
					outputStream.write((((DTOFile)dtoTemp).getEmployee().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				

				/*if(((DTOFile)dtoTemp).getSpecialAction()!=null){
					outputStream.write((((DTOFile)dtoTemp).getSpecialAction().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(((DTOFile)dtoTemp).getPay()!=null){
					outputStream.write((((DTOFile)dtoTemp).getPay().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}*/
				/*if(((DTOFile)dtoTemp).getTaxFrequency()!=null){
					outputStream.write((((DTOFile)dtoTemp).getTaxFrequency().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}*/
				if(Double.parseDouble(((DTOFile)dtoTemp).getRegHours().toString())!=0){
					outputStream.write((((DTOFile)dtoTemp).getRegHours().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(Double.parseDouble(((DTOFile)dtoTemp).getoTHours().toString())!=0){
					outputStream.write((((DTOFile)dtoTemp).getoTHours().toString()+"\t").getBytes());	
				}else{
					outputStream.write("\t".getBytes());
				}
				
								
/*			if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("S")){
					String hour3CodeVal = "SIC";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("P")){
					String hour3CodeVal = "PER";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("V")){
					String hour3CodeVal = "VAC";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("H")){
					String hour3CodeVal = "HOL";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("BE")){
					String hour3CodeVal = "BER";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("RS")){
					String hour3CodeVal = "RSH";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else{
					outputStream.write((((DTOFile)dtoTemp).getHours3Code().toString()+"\t").getBytes());
				}*/
				String str=record.getKey().toString()+"~"+((DTOFile)dtoTemp).getHours3Code().toString();
				Double str1 = hoursCodeMap.get(str);
				if(str1!=null){
					outputStream.write((str1+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(Double.parseDouble(((DTOFile)dtoTemp).getRegEarnings().toString())!=0){
					f11=Float.parseFloat(((DTOFile)dtoTemp).getRegEarnings().toString());
					f11 = (float) Math.round(f11*100)/100; 
					outputStream.write((f11+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(Double.parseDouble(((DTOFile)dtoTemp).getoTEarnings().toString())!=0){
	            	 f11=Float.parseFloat(((DTOFile)dtoTemp).getoTEarnings().toString());
	            	 f11 = (float) Math.round(f11*100)/100; 
	            	 outputStream.write((f11+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				
/*				if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("S")){
					String earn3CodeVal = "SIC";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("P")){
					String earn3CodeVal = "PER";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("V")){
					String earn3CodeVal = "VAC";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("H")){
					String earn3CodeVal = "HOL";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("BE")){
					String earn3CodeVal = "BER";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getEarnings3Code().toString().equalsIgnoreCase("RS")){
					String earn3CodeVal = "RSH";
					outputStream.write((earn3CodeVal+"\t").getBytes());
				}else{
					outputStream.write((((DTOFile)dtoTemp).getEarnings3Code().toString()+"\t").getBytes());
				}*/
				
				String str2=record.getKey().toString()+"~"+((DTOFile)dtoTemp).getEarnings3Code().toString();
				Double str3=hoursValueMap.get(str2);
				if(str3!=null){
					outputStream.write((str3+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
					
				if(size>1){
					Iterator<String> it=totalHour3Code.iterator();
					while (it.hasNext()) {
						String type=it.next();
						if(!type.equalsIgnoreCase(((DTOFile)dtoTemp).getHours3Code().toString())){
							str=record.getKey().toString()+"~"+type;
							str1 = hoursCodeMap.get(str);
							if(str1!=null){
								//outputStream.write((type+"\t").getBytes());
								/*if(type.equalsIgnoreCase("S")){
									String type3CodeVal = "SIC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("P")){
									String type3CodeVal = "PER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("V")){
									String type3CodeVal = "VAC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("H")){
									String type3CodeVal = "HOL";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("BE")){
									String type3CodeVal = "BER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("RS")){
									String type3CodeVal = "RSH";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else{
									outputStream.write((type+"\t").getBytes());
								}	*/							
								outputStream.write((str1+"\t").getBytes());
								/*if(type.equalsIgnoreCase("S")){
									String type3CodeVal = "SIC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("P")){
									String type3CodeVal = "PER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("V")){
									String type3CodeVal = "VAC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("H")){
									String type3CodeVal = "HOL";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("BE")){
									String type3CodeVal = "BER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("RS")){
									String type3CodeVal = "RSH";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else{
									outputStream.write((type+"\t").getBytes());
								}*/
								outputStream.write((hoursValueMap.get(str)+"\t").getBytes());
							}
						}
					}
				}
				
				outputStream.write("\r\n".getBytes());
			}		
		}else{
			List<GenralDTO> payrollExtractList= payrollManager.findPayrollExtractList(beginDates.toString(),endDates.toString(),sessionCorpID,hub);
			Map<String, DTOFile> masterDtoFile= new HashMap<String, DTOFile>();		
			Map<String, Double> hoursCodeMap= new HashMap<String, Double>();
			Map<String, Double> hoursValueMap= new HashMap<String, Double>();
			Integer size=1;
			for (GenralDTO listObject : payrollExtractList) {
				masterDtoFile=listObject.getMasterDtoFile();
				hoursCodeMap=listObject.getHoursCodeMap();
				hoursValueMap=listObject.getHoursValueMap();
				size=listObject.getSize();				
				}
			List totalHour3Code =new ArrayList<String>();
				totalHour3Code.add("RS");
				totalHour3Code.add("DT");
				totalHour3Code.add("H");
				totalHour3Code.add("P");
				totalHour3Code.add("V");
				totalHour3Code.add("S");
				totalHour3Code.add("BE");
			
			HttpServletResponse response = getResponse();
	        response.setContentType("application/vnd.ms-excel");
			ServletOutputStream outputStream = response.getOutputStream();
			File file=new File(batchDates1.toString());
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".xls");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			out.println("filename="+file.getName()+".xls"+"\t The number of line  \t"+payrollExtractList.size()+"\t is extracted");	
			outputStream.write("Co Code\t".getBytes());
			outputStream.write("Batch ID\t".getBytes());
			outputStream.write("file #\t".getBytes());
			outputStream.write("Pay #\t".getBytes());			
			outputStream.write("Reg Hours\t".getBytes());
			outputStream.write("O/T Hours\t".getBytes());
			outputStream.write("Hours 3 Code\t".getBytes());
			outputStream.write("Hours 3 Amount\t".getBytes());					
			if(size>1){
				for(int k=1;k<size;k++){
					outputStream.write("Hours 3 Code\t".getBytes());
					outputStream.write("Hours 3 Amount\t".getBytes());
					
				}
			}			
			outputStream.write("\r\n".getBytes());
	        Float f11;
	        DecimalFormat df11;	        
			for(Map.Entry<String, DTOFile> record : masterDtoFile.entrySet()){
				DTOFile dtoTemp=record.getValue();
				if(((DTOFile)dtoTemp).getCoCode()!=null){
					outputStream.write((((DTOFile)dtoTemp).getCoCode().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(((DTOFile)dtoTemp).getBatchID()!=null){
					outputStream.write((((DTOFile)dtoTemp).getBatchID().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(record.getKey()!=null){
					outputStream.write((record.getKey().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(((DTOFile)dtoTemp).getPay()!=null){
					outputStream.write((((DTOFile)dtoTemp).getPay().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				
				if(Double.parseDouble(((DTOFile)dtoTemp).getRegHours().toString())!=0){
					outputStream.write((((DTOFile)dtoTemp).getRegHours().toString()+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				if(Double.parseDouble(((DTOFile)dtoTemp).getoTHours().toString())!=0){
					outputStream.write((((DTOFile)dtoTemp).getoTHours().toString()+"\t").getBytes());	
				}else{
					outputStream.write("\t".getBytes());
				}
				if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("S")){
					String hour3CodeVal = "SIC";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("P")){
					String hour3CodeVal = "PER";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("V")){
					String hour3CodeVal = "VAC";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("H")){
					String hour3CodeVal = "HOL";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("BE")){
					String hour3CodeVal = "BER";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else if(((DTOFile)dtoTemp).getHours3Code().toString().equalsIgnoreCase("RS")){
					String hour3CodeVal = "RSH";
					outputStream.write((hour3CodeVal+"\t").getBytes());
				}else{
					outputStream.write((((DTOFile)dtoTemp).getHours3Code().toString()+"\t").getBytes());
				}
				String str=record.getKey().toString()+"~"+((DTOFile)dtoTemp).getHours3Code().toString();
				Double str1 = hoursCodeMap.get(str);
				if(str1!=null){
					outputStream.write((str1+"\t").getBytes());
				}else{
					outputStream.write("\t".getBytes());
				}
				
					
				if(size>1){
					Iterator<String> it=totalHour3Code.iterator();
					while (it.hasNext()) {
						String type=it.next();
						if(!type.equalsIgnoreCase(((DTOFile)dtoTemp).getHours3Code().toString())){
							str=record.getKey().toString()+"~"+type;
							str1 = hoursCodeMap.get(str);
							if(str1!=null){
								//outputStream.write((type+"\t").getBytes());
								if(type.equalsIgnoreCase("S")){
									String type3CodeVal = "SIC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("P")){
									String type3CodeVal = "PER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("V")){
									String type3CodeVal = "VAC";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("H")){
									String type3CodeVal = "HOL";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("BE")){
									String type3CodeVal = "BER";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else if(type.equalsIgnoreCase("RS")){
									String type3CodeVal = "RSH";
									outputStream.write((type3CodeVal+"\t").getBytes());
								}else{
									outputStream.write((type+"\t").getBytes());
								}
								outputStream.write((str1+"\t").getBytes());								
							}
						}
					}
				}				
				outputStream.write("\r\n".getBytes());
			}		
		}
		}
		
		/**
		 * Workday extract 
		 * @throws ServletException
		 * @throws IOException
		 */
		
		public void payrollWorkDayExtract()  throws ServletException,IOException
		{
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder beginDates = new StringBuilder(formats.format(beginDate));
	        StringBuilder endDates = new StringBuilder(formats.format(endDate)); 
	        SimpleDateFormat batchformats = new SimpleDateFormat("yyyyMMdd");
	        StringBuilder batchDates = new StringBuilder(batchformats.format(endDate));
	        StringBuffer batchDates1 = new StringBuffer("WORKDAYDATA_");
	        batchDates1.append(batchDates);
	        List payrollExtractList= payrollManager.findPayrollWorkDayExtractList(beginDates.toString(),endDates.toString(),sessionCorpID,hub);
						
			HttpServletResponse response = getResponse();
	        response.setContentType("application/vnd.ms-excel");
			ServletOutputStream outputStream = response.getOutputStream();
			File file=new File(batchDates1.toString());
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".xls");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			out.println("filename="+file.getName()+".xls"+"\t The number of line  \t"+payrollExtractList.size()+"\t is extracted");	
			outputStream.write("Id\t".getBytes());
			outputStream.write("employee_id\t".getBytes());
			outputStream.write("Employee_First\t".getBytes());
			outputStream.write("Employee_Last\t".getBytes());
			outputStream.write("Cust_First\t".getBytes());
			outputStream.write("Cust_Last\t".getBytes());			
			outputStream.write("Order_Num\t".getBytes());
			outputStream.write("MS_PriKey\t".getBytes());
			outputStream.write("ServiceDate\t".getBytes());
			outputStream.write("ServiceID\t".getBytes());
			outputStream.write("Job_ID\t".getBytes());			
			outputStream.write("Job_Description\t".getBytes());						
			outputStream.write("PunchTime\t".getBytes());
			outputStream.write("PunchType\t".getBytes());
			outputStream.write("PunchDescription\t".getBytes());
			outputStream.write("TimeZone\t".getBytes());
			outputStream.write("Region\t".getBytes());
			outputStream.write("JobStart\t".getBytes());
			outputStream.write("JobEnd\t".getBytes());
			outputStream.write("ESTJobStart\t".getBytes());
			outputStream.write("ESTJobEnd\t".getBytes());
			outputStream.write("RoundedStart\t".getBytes());
			outputStream.write("RoundedEnd\t".getBytes());
			outputStream.write("LSPriKey\t".getBytes());
			outputStream.write("Active\t".getBytes());
			outputStream.write("PunchIn_ID\t".getBytes());
			outputStream.write("CTAP_Confirmed\t".getBytes());
			outputStream.write("MVP_Confirmed\t".getBytes());
			outputStream.write("Comments\t".getBytes());
			outputStream.write("SystemDate\t".getBytes());
			outputStream.write("WDPunchResponse\t".getBytes());
			outputStream.write("LaborType\t".getBytes());
			outputStream.write("MSPunchResponse\t".getBytes());
			outputStream.write("Branch\t".getBytes());
			outputStream.write("\r\n".getBytes());
	        Float f11;
	        DecimalFormat df11;	        
	        
	        Iterator it = payrollExtractList.iterator();
	        
	        while (it.hasNext()) {DTOPayrollWorkDayExtract dtoPayWDayExt = (DTOPayrollWorkDayExtract) it.next();
        	
        	if(dtoPayWDayExt.getId()!=null){
        		outputStream.write((dtoPayWDayExt.getId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getEmployeeId()!=null){
        		outputStream.write((dtoPayWDayExt.getEmployeeId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getFirstName()!=null){
        		outputStream.write((dtoPayWDayExt.getFirstName().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLastName()!=null){
        		outputStream.write((dtoPayWDayExt.getLastName().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCustFirst()!=null){
        		outputStream.write((dtoPayWDayExt.getCustFirst().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCustLast()!=null){
        		outputStream.write((dtoPayWDayExt.getCustLast().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getOrderNum()!=null){
        		outputStream.write((dtoPayWDayExt.getOrderNum().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMsPriKey()!=null){
        		outputStream.write((dtoPayWDayExt.getMsPriKey().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getServiceDate()!=null){
        		outputStream.write((dtoPayWDayExt.getServiceDate().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getServiceId()!=null){
        		outputStream.write((dtoPayWDayExt.getServiceId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getJobId()!=null){
        		outputStream.write((dtoPayWDayExt.getJobId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getJobDescription()!=null){
        		outputStream.write((dtoPayWDayExt.getJobDescription().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}        	
        	if(dtoPayWDayExt.getPunchType()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchType().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}        	
        	if(dtoPayWDayExt.getPunchDescription()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchDescription().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getTimeZone()!=null){
        		outputStream.write((dtoPayWDayExt.getTimeZone().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getRegion()!=null){
        		outputStream.write((dtoPayWDayExt.getRegion().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getJobStart()!=null){
        		outputStream.write((dtoPayWDayExt.getJobStart().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getJobEnd()!=null){
        		outputStream.write((dtoPayWDayExt.getJobEnd().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getEstJobStart()!=null){
        		outputStream.write((dtoPayWDayExt.getEstJobStart().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getEstJobEnd()!=null){
        		outputStream.write((dtoPayWDayExt.getEstJobEnd().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getRoundedStart()!=null){
        		outputStream.write((dtoPayWDayExt.getRoundedStart().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getRoundedEnd()!=null){
        		outputStream.write((dtoPayWDayExt.getRoundedEnd().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLsPriKey()!=null){
        		outputStream.write((dtoPayWDayExt.getLsPriKey().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getActive()!=null){
        		outputStream.write((dtoPayWDayExt.getActive().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchInID()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchInID().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCTAPConfirmed()!=null){
        		outputStream.write((dtoPayWDayExt.getCTAPConfirmed().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMVPConfirmed()!=null){
        		outputStream.write((dtoPayWDayExt.getMVPConfirmed().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getComments()!=null){
        		outputStream.write((dtoPayWDayExt.getComments().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getSystemDate()!=null){
        		outputStream.write((dtoPayWDayExt.getSystemDate().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getWDPunchResponse()!=null){
        		outputStream.write((dtoPayWDayExt.getWDPunchResponse().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLaborType()!=null){
        		outputStream.write((dtoPayWDayExt.getLaborType().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMSPunchResponse()!=null){
        		outputStream.write((dtoPayWDayExt.getMSPunchResponse().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getBranch()!=null){
        		outputStream.write((dtoPayWDayExt.getBranch().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	outputStream.write("\r\n".getBytes());
        	
        	// for punch out line 
        	
        	if(dtoPayWDayExt.getId()!=null){
        		outputStream.write((dtoPayWDayExt.getId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getEmployeeId()!=null){
        		outputStream.write((dtoPayWDayExt.getEmployeeId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getFirstName()!=null){
        		outputStream.write((dtoPayWDayExt.getFirstName().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLastName()!=null){
        		outputStream.write((dtoPayWDayExt.getLastName().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCustFirst()!=null){
        		outputStream.write((dtoPayWDayExt.getCustFirst().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCustLast()!=null){
        		outputStream.write((dtoPayWDayExt.getCustLast().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getOrderNum()!=null){
        		outputStream.write((dtoPayWDayExt.getOrderNum().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMsPriKey()!=null){
        		outputStream.write((dtoPayWDayExt.getMsPriKey().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getServiceDate()!=null){
        		outputStream.write((dtoPayWDayExt.getServiceDate().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getServiceId()!=null){
        		outputStream.write((dtoPayWDayExt.getServiceId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getJobId()!=null){
        		outputStream.write((dtoPayWDayExt.getJobId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getJobDescription()!=null){
        		outputStream.write((dtoPayWDayExt.getJobDescription().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getPunchType()!=null){
        		outputStream.write(("Punched Out"+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	
        	if(dtoPayWDayExt.getPunchDescription()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchDescription().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getTimeZone()!=null){
        		outputStream.write((dtoPayWDayExt.getTimeZone().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getRegion()!=null){
        		outputStream.write((dtoPayWDayExt.getRegion().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchOutTime()!=null){
        		outputStream.write((dtoPayWDayExt.getPunchOutTime().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLsPriKey()!=null){
        		outputStream.write((dtoPayWDayExt.getLsPriKey().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getActive()!=null){
        		outputStream.write((dtoPayWDayExt.getActive().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getPunchInID()!=null){
        		outputStream.write((dtoPayWDayExt.getId().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getCTAPConfirmed()!=null){
        		outputStream.write((dtoPayWDayExt.getCTAPConfirmed().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMVPConfirmed()!=null){
        		outputStream.write((dtoPayWDayExt.getMVPConfirmed().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getComments()!=null){
        		outputStream.write((dtoPayWDayExt.getComments().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getSystemDate()!=null){
        		outputStream.write((dtoPayWDayExt.getSystemDate().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getWDPunchResponse()!=null){
        		outputStream.write((dtoPayWDayExt.getWDPunchResponse().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getLaborType()!=null){
        		outputStream.write((dtoPayWDayExt.getLaborType().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getMSPunchResponse()!=null){
        		outputStream.write((dtoPayWDayExt.getMSPunchResponse().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	if(dtoPayWDayExt.getBranch()!=null){
        		outputStream.write((dtoPayWDayExt.getBranch().toString()+"\t").getBytes());
        	}else{
				outputStream.write("\t".getBytes());
			}
        	outputStream.write("\r\n".getBytes());
        }
				
		}
		
		/**
		 * End workday extract
		 * @throws ServletException
		 * @throws IOException
		 */
		
		public void paychexExtract() throws ServletException,IOException{
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
	        StringBuilder beginDates = new StringBuilder(formats.format(beginDate));
	        StringBuilder endDates = new StringBuilder(formats.format(endDate)); 
	        SimpleDateFormat batchformats = new SimpleDateFormat("yyyyMMdd");
	        StringBuilder batchDates = new StringBuilder(batchformats.format(endDate));
	        String batchDates1 = new String("PAY_");
	        fileNameMap=refMasterManager.findByParameterFlex4(sessionCorpID, "HOUSE");
	        //System.out.println("          \n\n\n\n\n\n"+hub);
	        //System.out.println("          \n\n\n\n\n\n"+fileNameMap);
	       
	        if(fileNameMap.containsKey(hub)){
	        	batchDates1=fileNameMap.get(hub);
	        }else{
	        	batchDates1 = new String("PAY_");
	        	batchDates1=batchDates1.concat(batchDates.toString());
	        } 
	        List payrollExtractList=payrollManager.findPaychexExtractList(beginDates.toString(),endDates.toString(),sessionCorpID,hub);
	        HttpServletResponse response = getResponse();
	        response.setContentType("text/csv");
			ServletOutputStream outputStream = response.getOutputStream();
			File file=new File(batchDates1.toString());
			response.setHeader("Content-Disposition", "attachment; filename="+file.getName()+".txt");
			response.setHeader("Pragma", "public");
			response.setHeader("Cache-Control", "max-age=0");
			out.println("filename="+file.getName()+".txt"+"\t The number of line  \t"+payrollExtractList.size()+"\t is extracted");
			Iterator itss=payrollExtractList.iterator();
			while(itss.hasNext())
			{
				Object extract=(Object)itss.next();
				if(((DTOPaychexExtract)extract).getValueID()!=null && (!(((DTOPaychexExtract)extract).getValueID().toString().trim()).equals(""))){
				if((((DTOPaychexExtract)extract).getRegularHours()!=null) && (!(((DTOPaychexExtract)extract).getRegularHours().toString().equals(""))) && (Double.parseDouble(((DTOPaychexExtract)extract).getRegularHours().toString())>0)&& (((DTOPaychexExtract)extract).getAction()!= null) && (((DTOPaychexExtract)extract).getAction().toString().equalsIgnoreCase("C"))){
				

					String text="";
					if(((DTOPaychexExtract)extract).getValueID()!=null)
					{
						text=((DTOPaychexExtract)extract).getValueID().toString().trim();	
						
					}else{
						text="      ";
					}
					text=text+"                         ";
					if(((DTOPaychexExtract)extract).getValueDC()!=null)
					{
						text=text+((DTOPaychexExtract)extract).getValueDC().toString().trim();	
						
					}else{
						text=text+"      ";
					}
					text=text+"             ";
					text=text+"E";
					text=text+" "+"1";
					text=text+"         ";
					if(((DTOPaychexExtract)extract).getValueRH()!=null)
					{
						String valueRh=((DTOPaychexExtract)extract).getValueRH().toString();
						if(valueRh.indexOf(".") == -1){
							valueRh=valueRh+".00";
						    } 
						    if((valueRh.indexOf(".")+3 != valueRh.length())){
						    	valueRh=valueRh+"0";
						    }
						int countWeight=valueRh.length(); 
						int diff = (8-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueRh;	
						
					}else{
						text=text+"       ";
					}
					text=text+"          "; 
					text=text+"     0.00"; 
					text=text+"     ";
					if(((DTOPaychexExtract)extract).getValueWH()!=null)
					{
						
						int countWeight=((DTOPaychexExtract)extract).getValueWH().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueWH().toString().trim();	
						
					}else{
						text=text+"  ";
					}
					text=text+"   ";
					text=text+"113";
					text=text+"             ";
					if(((DTOPaychexExtract)extract).getValueRC()!=null && ((Double.parseDouble(((DTOPaychexExtract)extract).getValueRC().toString()))>0))
					{ 	
						text=text+((DTOPaychexExtract)extract).getValueRC().toString().trim();	
						
					}else{
						text=text+" ";
					}
					text=text+"           ";
					String value63=text.substring(63, 70);
					value63=value63.trim();
					String value81=text.substring(81, 89);
					value81=value81.trim();
					if(Double.parseDouble(value63)>0 || Double.parseDouble(value81)>0){
					outputStream.write(text.getBytes());
					outputStream.write("\r\n".getBytes());
					}
					
				}
				if((((DTOPaychexExtract)extract).getOverTime()!=null) && (!(((DTOPaychexExtract)extract).getOverTime().toString().equals(""))) && (Double.parseDouble(((DTOPaychexExtract)extract).getOverTime().toString())>0)){
				 
					String text="";
					if(((DTOPaychexExtract)extract).getValueID()!=null)
					{
						text=((DTOPaychexExtract)extract).getValueID().toString().trim();	
						
					}else{
						text="      ";
					}
					text=text+"                         ";
					if(((DTOPaychexExtract)extract).getValueDC()!=null)
					{
						text=text+((DTOPaychexExtract)extract).getValueDC().toString().trim();	
						
					}else{
						text=text+"      ";
					}
					text=text+"             ";
					text=text+"E";
					text=text+" "+"2";
					text=text+"         ";
					if(((DTOPaychexExtract)extract).getValueOH()!=null)
					{
						String valueOh=((DTOPaychexExtract)extract).getValueOH().toString();
						if(valueOh.indexOf(".") == -1){
							valueOh=valueOh+".00";
						    } 
						    if((valueOh.indexOf(".")+3 != valueOh.length())){
						    	valueOh=valueOh+"0";
						    }
						int countWeight=valueOh.length();  
						int diff = (8-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueOh;	
						
					}else{
						text=text+"       ";
					} 
					text=text+"          ";
					text=text+"     0.00";  
					text=text+"     ";
					if(((DTOPaychexExtract)extract).getValueWH()!=null)
					{
						
						int countWeight=((DTOPaychexExtract)extract).getValueWH().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueWH().toString().trim();	
						
					}else{
						text=text+"  ";
					}
					text=text+"   ";
					text=text+"113";
					text=text+"             ";
					if(((DTOPaychexExtract)extract).getValueRC()!=null && ((Double.parseDouble(((DTOPaychexExtract)extract).getValueRC().toString()))>0))
					{ 	
						text=text+((DTOPaychexExtract)extract).getValueRC().toString().trim();	
						
					}else{
						text=text+" ";
					}
					text=text+"           ";
					String value63=text.substring(63, 70);
					value63=value63.trim();
					String value81=text.substring(81, 89);
					value81=value81.trim();
					if(Double.parseDouble(value63)>0 || Double.parseDouble(value81)>0){
					outputStream.write(text.getBytes());
					outputStream.write("\r\n".getBytes());
					}
				
				
					
					
				
				}
				if((((DTOPaychexExtract)extract).getDoubleOverTime()!=null) && (!(((DTOPaychexExtract)extract).getDoubleOverTime().toString().equals(""))) && (Double.parseDouble(((DTOPaychexExtract)extract).getDoubleOverTime().toString())>0)){
					

					 
					String text="";
					if(((DTOPaychexExtract)extract).getValueID()!=null)
					{
						text=((DTOPaychexExtract)extract).getValueID().toString().trim();	
						
					}else{
						text="      ";
					}
					text=text+"                         ";
					if(((DTOPaychexExtract)extract).getValueDC()!=null)
					{
						text=text+((DTOPaychexExtract)extract).getValueDC().toString().trim();	
						
					}else{
						text=text+"      ";
					}
					text=text+"             ";
					text=text+"E";
					text=text+" "+"3";
					text=text+"         "; 
					String doubleOverTimevalue=((DTOPaychexExtract)extract).getDoubleOverTime().toString();
					if(doubleOverTimevalue.indexOf(".") == -1){
						doubleOverTimevalue=doubleOverTimevalue+".00";
					    } 
					    if((doubleOverTimevalue.indexOf(".")+3 != doubleOverTimevalue.length())){
					    	doubleOverTimevalue=doubleOverTimevalue+"0";
					    }
					int countWeight=doubleOverTimevalue.length();   
					int diff = (8-countWeight);
					String temp="";
					for(int i=1;i<=diff;i++)
					{
						temp=temp+" ";
						
					}
					text=text+temp+doubleOverTimevalue;	 
					text=text+"          ";
				/*	if((((DTOPaychexExtract)extract).getPaidRevenueShare()!=null) && (!(((DTOPaychexExtract)extract).getPaidRevenueShare().toString().equals(""))) && (Double.parseDouble(((DTOPaychexExtract)extract).getPaidRevenueShare().toString())>0)){
					if(((DTOPaychexExtract)extract).getValueEA()!=null)
					{
						String valueEa=((DTOPaychexExtract)extract).getValueEA().toString();
						if(valueEa.indexOf(".") == -1){
							valueEa=valueEa+".00";
						    } 
						    if((valueEa.indexOf(".")+3 != valueEa.length())){
						    	valueEa=valueEa+"0";
						    }
						int countWeight1=valueEa.length();   
						int diff1 = (9-countWeight1);
						String temp1="";
						for(int i=1;i<=diff1;i++)
						{
							temp1=temp1+" ";
							
						}
						text=text+temp1+valueEa;	
						
					}else{
						text=text+"         ";
					}
					}else{

						if(((DTOPaychexExtract)extract).getValueRA()!=null)
						{
							
							int countWeight1=((DTOPaychexExtract)extract).getValueRA().toString().length();
							int diff1 = (8-countWeight1);
							String temp1="";
							for(int i=1;i<=diff1;i++)
							{
								temp1=temp1+" ";
								
							}
							text=text+temp1+((DTOPaychexExtract)extract).getValueRA().toString().trim();	
							
						}else{
							text=text+"        ";
						}
							
					}*/
					text=text+"     0.00";
					text=text+"     ";
					if(((DTOPaychexExtract)extract).getValueWH()!=null)
					{
						
						int countWeight1=((DTOPaychexExtract)extract).getValueWH().toString().length();
						int diff1 = (2-countWeight1);
						String temp1="";
						for(int i=1;i<=diff1;i++)
						{
							temp1=temp1+" ";
							
						}
						text=text+temp1+((DTOPaychexExtract)extract).getValueWH().toString().trim();	
						
					}else{
						text=text+"  ";
					}
					text=text+"   ";
					text=text+"113";
					text=text+"             ";
					if(((DTOPaychexExtract)extract).getValueRC()!=null && ((Double.parseDouble(((DTOPaychexExtract)extract).getValueRC().toString()))>0))
					{ 	
						text=text+((DTOPaychexExtract)extract).getValueRC().toString().trim();	
						
					}else{
						text=text+" ";
					}
					text=text+"           ";
					String value63=text.substring(63, 70);
					value63=value63.trim();
					String value81=text.substring(81, 89);
					value81=value81.trim();
					if(Double.parseDouble(value63)>0 || Double.parseDouble(value81)>0){
					outputStream.write(text.getBytes());
					outputStream.write("\r\n".getBytes());
					}
					
				}
				
				if((((DTOPaychexExtract)extract).getPaidRevenueShare()!=null) && (!(((DTOPaychexExtract)extract).getPaidRevenueShare().toString().equals(""))) && (Double.parseDouble(((DTOPaychexExtract)extract).getPaidRevenueShare().toString())>0)){
					if((((DTOPaychexExtract)extract).getValueED()!=null) &&(!(((DTOPaychexExtract)extract).getValueED().toString().equals(""))) )
					{
					String text="";
					if(((DTOPaychexExtract)extract).getValueID()!=null)
					{
						text=((DTOPaychexExtract)extract).getValueID().toString().trim();	
						
					}else{
						text="      ";
					}
					text=text+"                         ";
					if(((DTOPaychexExtract)extract).getValueDC()!=null)
					{
						text=text+((DTOPaychexExtract)extract).getValueDC().toString().trim();	
						
					}else{
						text=text+"      ";
					}
					text=text+"             ";
					text=text+"E";
					if((((DTOPaychexExtract)extract).getValueED()!=null) &&(!(((DTOPaychexExtract)extract).getValueED().toString().equals(""))) )
					{
						int countWeight=((DTOPaychexExtract)extract).getValueED().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueED().toString().trim();	
						
					}else{
						text=text+"  ";
					} 
					text=text+"         ";
					if((((DTOPaychexExtract)extract).getValueED()!=null) &&(!(((DTOPaychexExtract)extract).getValueED().toString().equals(""))) && ("H,P,S,V,BE".indexOf(((DTOPaychexExtract)extract).getValueED().toString().trim())>=0)){
						String valueRa=((DTOPaychexExtract)extract).getValueRA().toString();
						if(valueRa.indexOf(".") == -1){
							valueRa=valueRa+".00";
						    } 
						    if((valueRa.indexOf(".")+3 != valueRa.length())){
						    	valueRa=valueRa+"0";
						    }
						int countWeight=valueRa.length();
						int diff = (8-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueRa.trim();	
							
					}else{
					text=text+"    0.00";
					}
					text=text+"          ";
					if((((DTOPaychexExtract)extract).getValueED()!=null) &&(!(((DTOPaychexExtract)extract).getValueED().toString().equals(""))) && ((((DTOPaychexExtract)extract).getValueED().toString().equalsIgnoreCase("RS"))) ){
					if(((DTOPaychexExtract)extract).getValueEA()!=null)
					{
						String valueEa=((DTOPaychexExtract)extract).getValueEA().toString();
						if(valueEa.indexOf(".") == -1){
							valueEa=valueEa+".00";
						    } 
						    if((valueEa.indexOf(".")+3 != valueEa.length())){
						    	valueEa=valueEa+"0";
						    }
						int countWeight=valueEa.length();    
						int diff = (9-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueEa;	
						
					}else{
						text=text+"         ";
					}
					}else{
						text=text+"     0.00";	
					}
					text=text+"     ";
					if(((DTOPaychexExtract)extract).getValueWH()!=null)
					{
						
						int countWeight=((DTOPaychexExtract)extract).getValueWH().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueWH().toString().trim();	
						
					}else{
						text=text+"  ";
					}
					text=text+"   ";
					text=text+"113";
					text=text+"             ";
					if(((DTOPaychexExtract)extract).getValueRC()!=null && ((Double.parseDouble(((DTOPaychexExtract)extract).getValueRC().toString()))>0))
					{ 	
						text=text+((DTOPaychexExtract)extract).getValueRC().toString().trim();	
						
					}else{
						text=text+" ";
					}
					text=text+"           ";
					String value63=text.substring(63, 70);
					value63=value63.trim();
					String value81=text.substring(81, 89);
					value81=value81.trim();
					if(Double.parseDouble(value63)>0 || Double.parseDouble(value81)>0){
					outputStream.write(text.getBytes());
					outputStream.write("\r\n".getBytes());
					}
				
				}
				}else{
					if((((DTOPaychexExtract)extract).getValueTD()!=null) &&(!(((DTOPaychexExtract)extract).getValueTD().toString().equals(""))) )
					{
					String text="";
					if(((DTOPaychexExtract)extract).getValueID()!=null)
					{
						text=((DTOPaychexExtract)extract).getValueID().toString().trim();	
						
					}else{
						text="      ";
					}
					text=text+"                         ";
					if(((DTOPaychexExtract)extract).getValueDC()!=null)
					{
						text=text+((DTOPaychexExtract)extract).getValueDC().toString().trim();	
						
					}else{
						text=text+"      ";
					}
					text=text+"             ";
					text=text+"E";
					if((((DTOPaychexExtract)extract).getValueTD()!=null)&& (!(((DTOPaychexExtract)extract).getValueTD().toString().equals(""))))
					{
						int countWeight=((DTOPaychexExtract)extract).getValueTD().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueTD().toString().trim();	
						
					}else{
						text=text+"  ";
					} 
					text=text+"         ";
					if((((DTOPaychexExtract)extract).getValueTD()!=null) &&(!(((DTOPaychexExtract)extract).getValueTD().toString().equals(""))) && ("H,P,S,V,BE".indexOf(((DTOPaychexExtract)extract).getValueTD().toString().trim())>=0)){
						String valueRa=((DTOPaychexExtract)extract).getValueRA().toString();
						if(valueRa.indexOf(".") == -1){
							valueRa=valueRa+".00";
						    } 
						    if((valueRa.indexOf(".")+3 != valueRa.length())){
						    	valueRa=valueRa+"0";
						    }
						int countWeight=valueRa.length();
						int diff = (8-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueRa.trim();	
							
					}else{
					text=text+"    0.00";
					}
					text=text+"          ";
					if((((DTOPaychexExtract)extract).getValueTD()!=null)&& (!(((DTOPaychexExtract)extract).getValueTD().toString().equals(""))) && ((((DTOPaychexExtract)extract).getValueTD().toString().equalsIgnoreCase("RS")))){
					if(((DTOPaychexExtract)extract).getValueRA()!=null)
					{
						String valueRa=((DTOPaychexExtract)extract).getValueRA().toString();
						if(valueRa.indexOf(".") == -1){
							valueRa=valueRa+".00";
						    } 
						    if((valueRa.indexOf(".")+3 != valueRa.length())){
						    	valueRa=valueRa+"0";
						    }
						int countWeight=valueRa.length();   
						int diff = (9-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+valueRa;	
						
					}else{
						text=text+"         ";
					}
					}else{
						text=text+"     0.00";
					}
					text=text+"     ";
					if(((DTOPaychexExtract)extract).getValueWH()!=null)
					{
						
						int countWeight=((DTOPaychexExtract)extract).getValueWH().toString().length();
						int diff = (2-countWeight);
						String temp="";
						for(int i=1;i<=diff;i++)
						{
							temp=temp+" ";
							
						}
						text=text+temp+((DTOPaychexExtract)extract).getValueWH().toString().trim();	
						
					}else{
						text=text+"  ";
					}
					text=text+"   ";
					text=text+"113";
					text=text+"             ";
					if(((DTOPaychexExtract)extract).getValueRC()!=null && ((Double.parseDouble(((DTOPaychexExtract)extract).getValueRC().toString()))>0))
					{ 	
						text=text+((DTOPaychexExtract)extract).getValueRC().toString().trim();	
						
					}else{
						text=text+" ";
					}
					text=text+"           ";
					String value63=text.substring(63, 70);
					value63=value63.trim();
					String value81=text.substring(81, 89);
					value81=value81.trim();
					if(Double.parseDouble(value63)>0 || Double.parseDouble(value81)>0){
					outputStream.write(text.getBytes());
					outputStream.write("\r\n".getBytes());
					}
				
				}
				}
			}
			}
				
		}
		
		private String union;
		@SkipValidation
		  public String search(){
			getComboList(sessionCorpID);
		     payrolllist = payrollManager.searchPayroll(payroll.getFirstName(),payroll.getLastName(),payroll.getWarehouse(),payroll.getActive(),payroll.getUserName(),union,sessionCorpID);
		   	 return SUCCESS;     
		} 
		
		
		private String gotoPageString;
		private String validateFormNav;
		public String saveOnTabChange() throws Exception {
			validateFormNav = "OK";
			String s = save();
	    		return gotoPageString;
		}
		
		
		@SkipValidation
		public String editHealthWelfare(){
			return SUCCESS;
		}
		
		@SkipValidation
		public void healthWelfareExtract(){   
			SimpleDateFormat formats = new SimpleDateFormat("yyyy-MM-dd");
			StringBuilder beginDates = new StringBuilder(formats.format( beginDate ));
			StringBuilder endDates = new StringBuilder(formats.format( endDate ));
			List list = payrollManager.getHealthWelfareExtract(beginDates.toString(), endDates.toString(), sessionCorpID);
			String header = new String(); 
			try {

				File file1 = new File("Health&WelfareExtract");
				if (!list.isEmpty()) {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = " Crew Name " + "\t" +" Social Sec # " + "\t" +" Warehouse " + "\t" + " Reg.Hours " + "\t" + "Health and Welfare " + "\n";

					outputStream.write(header.getBytes());
					
					Iterator it = list.iterator();
					while (it.hasNext()) {
						Object listObject = it.next();

						if(((HWDTO) listObject).getCrewName() != null){
							outputStream.write((((HWDTO) listObject).getCrewName().toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if(((HWDTO) listObject).getSSNumber() != null){
							outputStream.write((((HWDTO) listObject).getSSNumber().toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}

						if(((HWDTO) listObject).getWarehouse() != null){
							outputStream.write((((HWDTO) listObject).getWarehouse().toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
					
						if(((HWDTO) listObject).getFinalHrs() != null){
							outputStream.write((((HWDTO) listObject).getFinalHrs().toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}	
							
						if(((HWDTO) listObject).getHwAmt() != null){
							outputStream.write((((HWDTO) listObject).getHwAmt().toString() + "\t").getBytes());
						} else {
							outputStream.write("\t".getBytes());
						}
						
						outputStream.write("\n".getBytes());
					}

				} else {
					HttpServletResponse response = getResponse();
					ServletOutputStream outputStream = response.getOutputStream();
					response.setContentType("application/vnd.ms-excel");
					//response.setHeader("Cache-Control", "no-cache");
					response.setHeader("Content-Disposition", "attachment; filename=" + file1.getName() + ".xls" );
					response.setHeader("Pragma", "public");
					response.setHeader("Cache-Control", "max-age=0");
					header = "NO Record Found , thanks";
					outputStream.write(header.getBytes());

				}
			} catch (Exception e) {
				System.out.println("ERROR:" + e);
			}
		}

 
		private String active;
		
		private String firstName; 
		
		private String lastName;	
		
		private String warehouse;
		 
		public String getActive() {
			return active;
		} 
		public void setActive(String active) {
			this.active = active;
		} 
		public String getFirstName() {
			return firstName;
		} 
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		} 
		public String getLastName() {
			return lastName;
		} 
		public void setLastName(String lastName) {
			this.lastName = lastName;
		} 
		public String getWarehouse() {
			return warehouse;
		} 
		public void setWarehouse(String warehouse) {
			this.warehouse = warehouse;
		} 
		public Payroll getPayroll() {
			return payroll;
		}

		public void setPayroll(Payroll payroll) {
			this.payroll = payroll;
		} 
		public void setPayrollManager(PayrollManager payrollManager) {
			this.payrollManager = payrollManager;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getSessionCorpID() {
			return sessionCorpID;
		} 
		public void setSessionCorpID(String sessionCorpID) {
			this.sessionCorpID = sessionCorpID;
		} 
		public List getSearchList() {
			return searchList;
		} 
		public void setSearchList(List searchList) {
			this.searchList = searchList;
		} 
		public Set getPayrollSet() {
			return PayrollSet;
		} 
		public void setPayrollSet(Set payrollSet) {
			PayrollSet = payrollSet;
		}
		public Map<String, String> getHouse() {
			return house;
		} 
		public void setHouse(Map<String, String> house) {
			this.house = house;
		} 
		public Map<String, String> getDept() {
			return dept;
		} 
		public void setDept(Map<String, String> dept) {
			this.dept = dept;
		} 
		public Map<String, String> getDrvr_cls() {
			return drvr_cls;
		} 
		public void setDrvr_cls(Map<String, String> drvr_cls) {
			this.drvr_cls = drvr_cls;
		} 
		public Map<String, String> getLabor() {
			return labor;
		} 
		public void setLabor(Map<String, String> labor) {
			this.labor = labor;
		} 
		public Map<String, String> getTypeofWork() {
			return typeofWork;
		} 
		public void setTypeofWork(Map<String, String> typeofWork) {
			this.typeofWork = typeofWork;
		} 
		public String getGotoPageString() {
			return gotoPageString;
		} 
		public void setGotoPageString(String gotoPageString) {
			this.gotoPageString = gotoPageString;
		} 
		public String getValidateFormNav() {
			return validateFormNav;
		} 
		public void setValidateFormNav(String validateFormNav) {
			this.validateFormNav = validateFormNav;
		} 
		public Map<String, String> getCompDevision() {
			return compDevision;
		} 
		public void setCompDevision(Map<String, String> compDevision) {
			this.compDevision = compDevision;
		} 
		public Date getBeginDate() {
			return beginDate;
		} 
		public void setBeginDate(Date beginDate) {
			this.beginDate = beginDate;
		} 
		public Date getEndDate() {
			return endDate;
		} 
		public void setEndDate(Date endDate) {
			this.endDate = endDate;
		}
	
		public Date getBeginDate1() {
			return opentodate 	;
		}
		public void setBeginDate1(Date opentodate 	) {
			this.opentodate 	 = opentodate 	;
		}
		public Date getBeginDateFrom() {
			return openfromdate 	;
		}
		public void setBeginDateFrom(Date openfromdate 	) {
			this.openfromdate 	 = openfromdate 	;
		}
		public Date getEndDate1() {
			return closeddateto;
		}
		public void setEndDate1(Date closeddateto) {
			this.closeddateto = closeddateto;
		}
		public Date getEndDateFrom() {
			return closeddatefrom;
		}
		public void setEndDateFrom(Date closeddatefrom) {
			this.closeddatefrom = closeddatefrom;
		}
		public String getConditionA1() {
			return conditionA1;
		}
		public void setConditionA1(String conditionA1) {
			this.conditionA1 = conditionA1;
		}
		public String getJobTypes() {
			return jobTypes;
		}
		public void setJobTypes(String jobTypes) {
			this.jobTypes = jobTypes;
		}
		public Date getCloseddatefrom() {
			return closeddatefrom;
		}
		public void setCloseddatefrom(Date closeddatefrom) {
			this.closeddatefrom = closeddatefrom;
		}
		public Date getCloseddateto() {
			return closeddateto;
		}
		public void setCloseddateto(Date closeddateto) {
			this.closeddateto = closeddateto;
		}
		public Date getOpenfromdate() {
			return openfromdate;
		}
		public void setOpenfromdate(Date openfromdate) {
			this.openfromdate = openfromdate;
		}
		public Date getOpentodate() {
			return opentodate;
		}
		public void setOpentodate(Date opentodate) {
			this.opentodate = opentodate;
		}
		public Map<String, String> getLoctype() {
			return loctype;
		}
		public void setLoctype(Map<String, String> loctype) {
			this.loctype = loctype;
		}
		public String getTypeS() {
			return typeS;
		}
		public void setTypeS(String typeS) {
			this.typeS = typeS;
		}
		public String getWarehouseS() {
			return warehouseS;
		}
		public void setWarehouseS(String warehouseS) {
			this.warehouseS = warehouseS;
		}
		public String getChekcflag() {
			return chekcflag;
		}
		public void setChekcflag(String chekcflag) {
			this.chekcflag = chekcflag;
		}
		public String getFromDate() {
			return fromDate;
		}
		public void setFromDate(String fromDate) {
			this.fromDate = fromDate;
		}
		public String getToDate() {
			return toDate;
		}
		public void setToDate(String toDate) {
			this.toDate = toDate;
		}
		public List getTimeHours() {
			return timeHours;
		}
		public void setTimeHours(List timeHours) {
			this.timeHours = timeHours;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public void setNotesManager(NotesManager notesManager) {
			this.notesManager = notesManager;
		}

		public String getCountPayNotes() {
			return countPayNotes;
		}
		public void setCountPayNotes(String countPayNotes) {
			this.countPayNotes = countPayNotes;
		}
		public String getUnion() {
			return union;
		}
		public void setUnion(String union) {
			this.union = union;
		}
		public String getHub() {
			return hub;
		}
		public void setHub(String hub) {
			this.hub = hub;
		}
		public Map<String, String> getOphub() {
			return ophub;
		}
		public void setOphub(Map<String, String> ophub) {
			this.ophub = ophub;
		}
		public String getLocWarehouse() {
			return locWarehouse;
		}
		public void setLocWarehouse(String locWarehouse) {
			this.locWarehouse = locWarehouse;
		}
		public String getTypeSto() {
			return typeSto;
		}
		public void setTypeSto(String typeSto) {
			this.typeSto = typeSto;
		}
		public Map<String, String> getStotype() {
			return stotype;
		}
		public void setStotype(Map<String, String> stotype) {
			this.stotype = stotype;
		}
		public List getPayrollGroup() {
			return payrollGroup;
		}
		public void setPayrollGroup(List payrollGroup) {
			this.payrollGroup = payrollGroup;
		}
		public Map<String, String> getFileNameMap() {
			return fileNameMap;
		}
		public void setFileNameMap(Map<String, String> fileNameMap) {
			this.fileNameMap = fileNameMap;
		}
		public Date getSettlementdatefrom() {
			return settlementdatefrom;
		}
		public Date getSettlementdateto() {
			return settlementdateto;
		}
		public void setSettlementdatefrom(Date settlementdatefrom) {
			this.settlementdatefrom = settlementdatefrom;
		}
		public void setSettlementdateto(Date settlementdateto) {
			this.settlementdateto = settlementdateto;
		}
		public List getPayrollGroupFlex5() {
			return payrollGroupFlex5;
		}
		public void setPayrollGroupFlex5(List payrollGroupFlex5) {
			this.payrollGroupFlex5 = payrollGroupFlex5;
		}
		public Map<String, String> getIntegrationToolMap() {
			return integrationToolMap;
		}
		public void setIntegrationToolMap(Map<String, String> integrationToolMap) {
			this.integrationToolMap = integrationToolMap;
		}
		public Map<String, String> getMmDevice() {
			return mmDevice;
		}
		public void setMmDevice(Map<String, String> mmDevice) {
			this.mmDevice = mmDevice;
		}
		public List getSupervisorList() {
			return supervisorList;
		}
		public void setSupervisorList(List supervisorList) {
			this.supervisorList = supervisorList;
		}
		public String getSalesPortalFlag() {
			return salesPortalFlag;
		}
		public void setSalesPortalFlag(String salesPortalFlag) {
			this.salesPortalFlag = salesPortalFlag;
		}
		public Map<String, String> getJobtype() {
			return jobtype;
		}
		public void setJobtype(Map<String, String> jobtype) {
			this.jobtype = jobtype;
		}
		public boolean isSalesPortalAccess() {
			return salesPortalAccess;
		}
		public void setSalesPortalAccess(boolean salesPortalAccess) {
			this.salesPortalAccess = salesPortalAccess;
		}
		public String getClaimPropertyFlag() {
			return claimPropertyFlag;
		}
		public void setClaimPropertyFlag(String claimPropertyFlag) {
			this.claimPropertyFlag = claimPropertyFlag;
		}
		public String getRecievedFormCheckFlag() {
			return recievedFormCheckFlag;
		}
		public void setRecievedFormCheckFlag(String recievedFormCheckFlag) {
			this.recievedFormCheckFlag = recievedFormCheckFlag;
		}
		
		
	}


